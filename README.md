Delmar Portal
-------------

This is CRM for Delmar developed by **[http://www.progforce.com](http://www.progforce.com)** based on OpenERP 6.1.

[Changelog](http://gitlab.pfrus.com/delmar/delmar_openerp/blob/master/CHANGELOG.md)

About OpenERP
-------------

OpenERP is an OpenSource/Free software Enterprise Resource Planning and
Customer Relationship Management software. More info at **[http://www.openerp.com](http://www.openerp.com)**

System Requirements
-------------------

Ubunutu/Debian:
```
sudo aptitude install python-dateutil python-feedparser python-gdata python-ldap python-libxslt1 python-lxml python-mako python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi gunicorn python-psutil python-pip python-dev python-pyodbc freetds-common freetds-bin unixodbc php5-sybase unixodbc-dev g++ pgtune htop xvfb firefox
```

Python:
```
sudo pip install XlsxWriter pygeocoder fedex xlrd gnupg configparser xmltodict paramiko psutil ecdsa python-gnupg requests redis exrex funcsigs unidecode rjsmin rcssmin pyvirtualdisplay selenium sympy
sudo pip install --allow-external pyodbc --allow-unverified pyodbc pyodbc
```

For use pretty-yaml (sale_integration_xml):
```
sudo pip install --upgrade 'git+https://github.com/mk-fg/pretty-yaml.git#egg=pyaml'
```

Vagrant
-------

1. Install latest version of **[Vagrant](http://www.vagrantup.com/)** from [here](http://www.vagrantup.com/downloads.html) (initially was used v1.6.3).
1. Install latest version of **[VirtualBox](https://www.virtualbox.org)** from [here](https://www.virtualbox.org/wiki/Downloads) (initially was used v4.3.12).
1. Install plugin for auto update virtualbox guest modules on guest machine:
    `vagrant plugin install vagrant-vbguest`
If you skip this step - in future you had a problem with `vagrant up` command.
1. Move to folder with project and run `vagrant up` command. This will create vagrant box with **Ubuntu 12.04 LTS** server and install **PostgreSQL 9.3** on it.
1. Vagrant box can be accessed by `vagrant ssh` command. It also creates port bindings to localhost:
    - `127.0.0.1:2222 -> vagrantbox:22` - SSH connection (same as `vagrant ssh`);
    - `127.0.0.1:8080 -> vagrantbox:8069` - OpenERP connection;
    - `127.0.0.1:6543 -> vagrantbox:5432` - PostgreSQL connection;
1. By default **Delmar Portal** located in `~/delmar_openerp` and out of the box can be runned by `python ./openerp-server` or `python ./openerp-server --debug` (also can be run as gunicorn `gunicorn openerp:wsgi.core.application -c gunicorn.conf.py`
1. Login at [http://localhost:8080](http://localhost:8080)
1. Login as **admin** / **1**

**Check and update Database setting in ERP**. Go to Tools -> Configuration -> Database servers. For all connection produce:
1. Push `Reset Confirmation`
1. Update user / pass, connectiob settings.
1. Push `Test & confirm`

**Full information about vagrant setup** can be found in `Vagrantfile` and `scripts/local/bootstrap.sh` . On any questions about **Vagrant** please contact yuri@progforce.com)