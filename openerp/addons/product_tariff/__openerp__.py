{
    "name": "product_tariff",
    "version": "1.0.0",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "delmar_products"
    ],
    "description": """

    """,
    "category": "Project Management",
    "demo_xml": [],
    "update_xml": [
        'data/cron_data.xml',
        'view/product_tariff_view.xml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
