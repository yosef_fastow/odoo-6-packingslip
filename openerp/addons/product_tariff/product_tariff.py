import logging
from osv import osv, fields
from json import dumps as json_dumps

_logger = logging.getLogger(__name__)


class product_tariff_settings(osv.osv):
    _name = 'product.tariff.settings'
    _rec_name = 'metal_pattern'

    # hardcoded list of metal material categories
    metal_categories = [
        ('gold', 'Gold'),
        ('silver', 'Silver'),
        ('platinum', 'Platinum'),
    ]

    # hardcoded list of component material categories
    component_categories = [
        ('diamond', 'Diamond'),
        ('gemstone', 'Gemstone'),
        ('pearl', 'Pearl')
    ]

    # all categories
    product_categories = metal_categories + component_categories

    # to use for cron updates or not
    rules_types = [
        ('auto', 'Automatic'),
        ('manual', 'Manual'),
        ('override', 'Override')
    ]

    # result template dict
    check_result_tpl_dict = {
        'us': None,
        'ca': None,
        'hk': None,
        'tpl_id': None,
        'description': None
    }

    _columns = {
        'metal_pattern': fields.char('Metal pattern', size=256, required=False, ),
        'product_category': fields.many2one('product.category', 'Product category', ondelete='cascade', required=False, ),
        'include_only': fields.boolean('Has include only'),
        'include_component': fields.selection(component_categories, 'Include component', required=False, readonly=False, ),
        'include_pattern': fields.char('Include pattern', size=256, required=False, readonly=False, ),
        'exclude_component': fields.selection(component_categories, 'Exclude component', required=False, readonly=False, ),
        'exclude_pattern': fields.char('Exclude pattern', size=256, required=False, readonly=False, ),
        'description': fields.char('Tariff description', size=64, required=False, ),
        'us_tariff': fields.char('US Tariff', size=20, required=True, ),
        'ca_tariff': fields.char('CA Tariff', size=20, required=True, ),
        'hk_tariff': fields.char('HK Tariff', size=20, required=True, ),
        'priority': fields.integer('Priority', help="1 is the top priority. By default, all rules have priority 10.", ),
        'rule_type': fields.selection(rules_types, 'Rule type', select=True, required=True, readonly=False, ),
        'active': fields.boolean('Active', ),
    }

    _default = {
        'metal_pattern': None,
        'include_only': False,
        'include_component': None,
        'include_pattern': None,
        'exclude_component': None,
        'exclude_pattern': None,
        'priority': 10,
        'rule_type': 'auto',
        'active': True,
    }

    _order = 'priority asc, product_category asc, include_only desc, id asc'


    # DLMR-1326
    def add_sql_part(f):
        def wrapper(self, **kvargs):
            excl = f(self, **kvargs)
            return excl
        return wrapper

    @add_sql_part
    def add_metal_pattern(self, **kvargs):
        part = "AND pt.prod_metal ~* tt.metal_pattern"
        return [part]

    @add_sql_part
    def add_product_category(self, **kvargs):
        part = "AND pt.categ_id = tt.product_category"
        return [part]

    @add_sql_part
    def add_include_component(self, **kvargs):
        parts = []
        zero_params = ['diamond', 'gemstone', 'pearl']
        part = "AND (cmp.{}_qty > 0 OR lower(pt.name) like '%{}%')".format(kvargs.get('val'), kvargs.get('val'))
        parts.append(part)
        if 'include_only' in kvargs.get('kvargs', {}) and kvargs.get('kvargs', {}).get('include_only') and zero_params:
            zero_params.remove(kvargs.get('val', ''))
            inc_part = "AND cmp.{}_qty = 0 AND cmp.{}_qty = 0".format(zero_params[0], zero_params[1])
            parts.append(inc_part)
        return parts

    @add_sql_part
    def add_include_pattern(self, **kvargs):
        part = "AND pt.name ~* tt.include_pattern"
        return [part]

    @add_sql_part
    def add_exclude_component(self, **kvargs):
        part = "AND (cmp.{}_qty = 0 OR lower(pt.name) not like '%{}%')".format(kvargs.get('val'), kvargs.get('val'))
        return [part]

    @add_sql_part
    def add_exclude_pattern(self, **kvargs):
        part = "AND pt.name ~* tt.exclude_pattern"
        return [part]

    @add_sql_part
    def add_product(self, **kvargs):
        part = "AND pp.id = {}".format(kvargs.get('val'))
        return [part]

    @add_sql_part
    def add_rule(self, **kvargs):
        part = "AND tt.id = {}".format(kvargs.get('val'))
        return [part]

    # DLMR-1326
    def format_sql(f):
        def wrapper(self, **kvargs):
            sql = f(self, **kvargs)
            parts = []
            for param, val in kvargs.items():
                method_name = 'add_' + param
                if hasattr(self, method_name):
                    part_args = {'val': val, 'kvargs': kvargs}
                    method = getattr(self, method_name)
                    parts.extend(method(**part_args))
            # print "Parts: %s" % parts
            return sql.format("\n".join(parts))
        return wrapper

    @format_sql
    def do_sql(self, **kvargs):
        sql = """
            select
                coalesce(tt.us_tariff, pt.prod_tarif_classification) as us,
                coalesce(tt.ca_tariff, pt.prod_tarif_classification_ca) as ca,
                coalesce(tt.hk_tariff, pt.prod_tarif_classification_hk) as hk,
                pt.id,
                tt.description
            from product_product pp
                left join product_tariff_settings tt on tt.active is true
                left join product_template pt on pt.id = pp.product_tmpl_id and pt.prod_metal is not null
                left join product_category pc on pc.id=pt.categ_id
                left join (
                  select
                    ppl.parent_product_id,
                    sum(case when pt.categ_id in(11) then 1 else 0 end) as diamond_qty,
                    sum(case when pt.categ_id in(13) then 1 else 0 end) as gemstone_qty,
                    sum(case when pt.categ_id in (16) then 1 else 0 end) as pearl_qty
                  from product_pack_line ppl
                  left join product_product pp on pp.id=ppl.product_id
                  left join product_template pt on pp.product_tmpl_id=pt.id
                  group by ppl.parent_product_id
                ) cmp on cmp.parent_product_id=pp.id
            where 1=1
                and tt.active = true
                --and pt.type = 'product'
                --and pt.prod_metal is not null
                {}
                --and (
                --  coalesce(pt.prod_tarif_classification, '') != coalesce(tt.us_tariff, '')
                --  or coalesce(pt.prod_tarif_classification_ca, '') != coalesce(tt.ca_tariff, '')
                --)
            order by tt.priority ASC
            limit 1
        """
        return sql

    # DLMR-1326
    # Check defined rule can be applied to defined product
    def check_rule_for_product(self, cr, uid, rule=None, product_id=None, context=None):
        if not product_id or not rule:
            return json_dumps({})
        args = {'product': product_id, 'rule': rule.id}
        if rule.metal_pattern:
            args.update({'metal_pattern': rule.metal_pattern})
        if rule.product_category:
            args.update({'product_category': rule.product_category})
        if rule.include_component:
            args.update({'include_component': rule.include_component})
        if rule.include_only:
            args.update({'include_only': rule.include_only})
        if rule.include_pattern:
            args.update({'include_pattern': rule.include_pattern})
        if rule.exclude_component:
            args.update({'exclude_component': rule.exclude_component})
        if rule.exclude_pattern:
            args.update({'exclude_pattern': rule.exclude_pattern})
        sql = self.do_sql(**args)
        _logger.debug(sql)
        cr.execute(sql)
        res = cr.fetchone()
        res_dict = self.check_result_tpl_dict.copy()
        if res:
            res_dict.update({
                'us': res[0],
                'ca': res[1],
                'hk': res[2],
                'tpl_id': res[3],
                'description': res[4]
            })
        return res_dict

    # DLMR-1326
    # just for testing rules from UI!
    # TODO: Remove maybe?
    def apply_rule_to_product(self, cr, uid, product_id, context=None):
        for rule in self.browse(cr, uid, self.search(cr, uid, ['&', ('active', '=', True), ('rule_type', '=', 'auto')])):
            res = self.check_rule_for_product(cr, uid, rule=rule, product_id=product_id)
            if res.get('us', None) or res.get('ca', None) or res.get('hk', None) or res.get('tpl_id', None):
                return res.get('us'), res.get('ca'), res.get('hk'), rule.id, res.get('description')
        return None, None, None, None, None

    # DLMR-1326
    # Respond with json for xml-rpc
    def get_product_tariff_rpc(self, cr, uid, product_id=None, default_code=None, context=None):
        us, ca, hk, rule_id, descr = None, None, None, None, None
        if product_id or default_code:
            prod_obj = self.pool.get('product.product')
            if default_code and not product_id:
                product_ids = prod_obj.search(cr, uid, ['&', ('default_code', '=', str(default_code).upper()), ('type', '=', 'product')])
                if product_ids and product_ids[0]:
                    product_id = product_ids[0]

            # DLMR-1923
            # check product available to auto_update
            product = prod_obj.browse(cr, uid, product_id)
            if product.product_tmpl_id.override_auto_tariff:
                _logger.info("Tariff overrided by product data")
                try:
                    zero_ids = self.search(cr, uid, [('active', '=', True), ('rule_type', '=', 'override')], offset=0, limit=1)
                    zerotarif = self.browse(cr, uid, zero_ids[0])
                    _logger.info("Zerotariff id: {}, desc: {}".format(zerotarif.id, zerotarif.description))
                    if not zerotarif:
                        raise Exception("No zerotariff found in DB")
                    tariff_data = {
                        'us': product.product_tmpl_id.prod_tarif_classification,
                        'ca': product.product_tmpl_id.prod_tarif_classification_ca,
                        'hk': product.product_tmpl_id.prod_tarif_classification_hk,
                        'rule': zerotarif and zerotarif.id or None,
                        'description': zerotarif and product.short_description or ''
                    }
                    _logger.info("Return tariff data: %s" % tariff_data)
                    return json_dumps(tariff_data)
                except Exception, e:
                    _logger.warn("Cannot find zerotariff: {}".format(e.message))
                    return self.check_result_tpl_dict
            # END DLMR-1923

            for rule in self.browse(cr, uid, self.search(cr, uid, ['&', ('active', '=', True), ('rule_type', '=', 'auto')])):
                res = self.check_rule_for_product(cr, uid, rule=rule, product_id=product_id)
                if res.get('us', None) or res.get('ca', None) or res.get('hk', None):
                    us = res.get('us')
                    ca = res.get('ca')
                    hk = res.get('hk')
                    descr = res.get('description')
                    rule_id = rule.id
                    break

        return json_dumps({
            'us': us,
            'ca': ca,
            'hk': hk,
            'rule': rule_id,
            'description': descr
        })

    # DLMR-1326
    # Another process of updating tariff
    # by rules
    def update_items_tariff(self, cr, uid, context=None):
        prod_obj = self.pool.get('product.product')
        tpl_obj = self.pool.get('product.template')
        to_update = 0
        updated = 0
        # iterate over ALL products
        for product_id in prod_obj.search(cr, uid, [('type', '=', 'product')]):
            # DLMR-1923
            # check product available to auto_update
            product = prod_obj.browse(cr, uid, product_id)
            if product.product_tmpl_id.override_auto_tariff:
                continue
            # END DLMR-1923
            us, ca, hk, tpl_id = None, None, None, None
            for rule in self.browse(cr, uid, self.search(cr, uid, ['&', ('active', '=', True), ('rule_type', '=', 'auto')])):
                res = self.check_rule_for_product(cr, uid, rule=rule, product_id=product_id)
                if res.get('us', None) or res.get('ca', None) or res.get('hk', None) or res.get('tpl_id'):
                    us = res.get('us')
                    ca = res.get('ca')
                    hk = res.get('hk')
                    tpl_id = res.get('tpl_id')
                    break
            # if rule was found for update
            if us or ca or hk or tpl_id:
                # check tpl and compare values
                tpl = tpl_obj.browse(cr, uid, tpl_id)
                if us != tpl.prod_tarif_classification or ca != tpl.prod_tarif_classification_ca or hk != tpl.prod_tarif_classification_hk:
                    # update tariff in product_template
                    to_update += 1
                    try:
                        tpl_obj.write(cr, uid, tpl_id, {
                            'prod_tarif_classification': us,
                            'prod_tarif_classification_ca': ca,
                            'prod_tarif_classification_hk': hk
                        })
                        updated += 1
                    except Exception, e:
                        _logger.warn('Product not updated: %s' % e.message)
                        pass

        # finally, log and return true
        _logger.info("Total to update: %s, updated: %s" % (to_update, updated))
        return True

    # DEPRECATED
    # would be updated after new tariff rules
    # TODO: apply new rules
    def update_items_tariff_old(self, cr, uid, context=None):

        _logger.info("Started `Update item's tariff`")

        sql = """
            UPDATE product_template dd
            set prod_tarif_classification = vv.us,
                prod_tarif_classification_ca = vv.ca
            from (
                select
                    pt.id,
                    coalesce(tt.us_tariff, pt.prod_tarif_classification) as us,
                    case  when ((cmp.pearl_qty>0 or lower(pt.name) like '%pearl%')and (cmp.dia_gem_qty=0 or ((lower(pt.name) not like '%gemstone%'and lower(pt.name) not like '%diamond%')))and pt.categ_id in (4,7,86))  then '7116.10.00.00' else coalesce(tt.ca_tariff, pt.prod_tarif_classification_ca)  end as ca
                from product_tariff_settings tt
                    left join product_template pt on pt.prod_metal ~* tt.metal_pattern
                    left join product_product pp on pp.product_tmpl_id = pt.id
                    left join 
                    (select ppl.parent_product_id ,sum(case when pt.categ_id in(11,13) then 1 else 0 end) as dia_gem_qty
                    ,sum(case when pt.categ_id in (16) then 1 else 0 end) as pearl_qty
                    from product_pack_line ppl
                    left join product_product pp on pp.id =ppl.product_id
                    left join product_template pt on pp.product_tmpl_id =pt.id
                    group by ppl.parent_product_id) cmp on cmp.parent_product_id =pp.id
                where 1=1
                    and tt.active = true
                    and pt.type = 'product'
                    and (
                        coalesce(pt.prod_tarif_classification, '') != coalesce(tt.us_tariff, '')
                        or coalesce(pt.prod_tarif_classification_ca, '') != coalesce(tt.ca_tariff, '')
                        or ((cmp.pearl_qty>0 or lower(pt.name) like '%pearl%')and (cmp.dia_gem_qty=0 or ((lower(pt.name) not like '%gemstone%'and lower(pt.name) not like '%diamond%')))and pt.categ_id in (4,7,86))
                    )
                ) vv
            where dd.id = vv.id
        """

        cr.execute(sql)

        return True


product_tariff_settings()


class ProductTariffCheck(osv.osv_memory):
    _name = "product.tariff.settings.check"
    _columns = {
        'product': fields.char('Product', size=20, required=False),
        'description': fields.char('Tariff description', size=64, required=False, readonly=True, ),
        'us_tariff': fields.char('US Tariff', size=20, required=False, readonly=True, ),
        'ca_tariff': fields.char('CA Tariff', size=20, required=False, readonly=True, ),
        'hk_tariff': fields.char('HK Tariff', size=20, required=False, readonly=True, ),
        'state': fields.boolean("state"),
    }

    _defaults = {
        'state': False,
    }

    def action_check_product(self, cr, uid, ids, context=None):
        _logger.info("Run action_check_product, ids: %s" % ids)
        if not ids and not ids[0]:
            return False

        checker = self.browse(cr, uid, ids[0])

        _logger.info("Got product code: %s" % checker.product)

        prod_obj = self.pool.get('product.product')
        tariff_obj = self.pool.get('product.tariff.settings')

        # try to run sql-builder
        #params = tariff_obj.do_select_params(metal_pattern="?bronze")
        #_logger.info(params)

        product_ids = prod_obj.search(cr, uid, [('default_code', '=', str(checker.product).upper())])
        if product_ids and product_ids[0]:
            _logger.info("Found product ids: %s" % product_ids)
            product = prod_obj.browse(cr, uid, product_ids[0])
            tariff_us, tariff_ca, tariff_hk, rule_id, descr = tariff_obj.apply_rule_to_product(cr, uid, product.id)
            _logger.info("Got tariff: %s, %s" % (tariff_us, tariff_ca))
            self.write(cr, uid, checker.id, {'us_tariff': tariff_us, 'ca_tariff': tariff_ca, 'hk_tariff': tariff_hk, 'description': descr})
        else:
            return False

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'product_tariff', 'action_view_tariff_rule_test')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': checker.id,
            'context': context,
        })

        return act_win


ProductTariffCheck()
