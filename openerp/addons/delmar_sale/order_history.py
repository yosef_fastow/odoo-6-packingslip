# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import base64
from stock_picking import STOCK_PICKING_STATES
from sale import SALE_ORDER_STATES
import csv
from cStringIO import StringIO
import re
from tools.translate import _
from openerp.addons.delmar_sale.sale import SIGN_DEFAULT_VALUE
import logging
import time

ALL_STATES = STOCK_PICKING_STATES + [('so_' + st[0], st[1]) for st in SALE_ORDER_STATES]

_logger = logging.getLogger(__name__)

class order_history(osv.osv):
    _name = "order.history"

    def _get_oh_move(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        oh_ids = oh_obj.search(cr, uid, [('sm_id', 'in', ids)])
        return oh_ids

    def _get_oh_picking(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        move_ids = self.pool.get('stock.move').search(cr, uid, [('picking_id', 'in', ids)])
        oh_ids = oh_obj.search(cr, uid, [('sm_id', 'in', move_ids)])
        return oh_ids

    def _get_oh_sale(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        sales = self.pool.get('sale.order').browse(cr, uid, ids, context=context)
        pickings2d = [sale.picking_ids for sale in sales if sale.picking_ids]
        picking_ids = set([])
        for pickings in pickings2d:
            picking_ids |= set([pick.id for pick in pickings if pick.id])
        move_ids = self.pool.get('stock.move').search(cr, uid, [('picking_id', 'in', list(picking_ids))])
        oh_ids = oh_obj.search(cr, uid, ['|', ('so_id', 'in', ids), ('sm_id', 'in', move_ids)])
        return oh_ids

    def _get_oh_sale_line(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        so_lines = self.pool.get('sale.order.line').browse(cr, uid, ids, context=context)
        so_ids = list(set([so_line.order_id.id for so_line in so_lines if so_line]))
        oh_ids = oh_obj.search(cr, uid, [('so_id', 'in', so_ids)])
        return oh_ids

    def _get_oh_product(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        so_lines_ids = self.pool.get('sale.order.line').search(cr, uid, [('product_id', 'in', ids)])
        so_lines = self.pool.get('sale.order.line').browse(cr, uid, so_lines_ids)
        so_ids = list(set([so_line.order_id.id for so_line in so_lines if so_line]))
        move_ids = self.pool.get('stock.move').search(cr, uid, [('product_id', 'in', ids)])
        oh_ids = oh_obj.search(cr, uid, ['|', ('so_id', 'in', so_ids), ('sm_id', 'in', move_ids)])
        return oh_ids

    def _get_oh_location(self, cr, uid, ids, context=None):
        oh_obj = self.pool.get('order.history')
        move_ids = self.pool.get('stock.move').search(cr, uid, [('location_id', 'in', ids)])
        oh_ids = oh_obj.search(cr, uid, [('sm_id', 'in', move_ids)])
        return oh_ids

    def _parent_sale_order(self, cr, uid, ids, name, args, context=None):
        res = {}
        for oh in self.browse(cr, uid, ids):
            if oh.type == 'sale_order':
                res[oh.id] = oh.so_id.id
            elif oh.type == 'stock_move' and oh.sm_id.picking_id and oh.sm_id.picking_id.sale_id:
                res[oh.id] = oh.sm_id.picking_id.sale_id.id
            else:
                res[oh.id] = False
        return res

    def _order_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, order_name from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order' and oh.so_id:
                    res[oh.id] = oh.so_id.name
                elif oh.type == 'stock_move' and oh.sm_id and oh.sm_id.picking_id:
                    res[oh.id] = oh.sm_id.picking_id.name
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set order_name=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _ship2store(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, ship2store from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id.ship2store
                elif oh.type == 'stock_move' and oh.sm_id.picking_id and oh.sm_id.picking_id.sale_id:
                    res[oh.id] = oh.sm_id.picking_id.sale_id.ship2store
                else:
                    res[oh.id] = False
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set ship2store=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _update_date_expected(self, cr, uid, ids, field_name, field_value, arg, context):
        """Update Expected Shipped Date"""
        if field_value:
            sql_str = """update order_history set
                                date_expected='%s'
                            where
                                id=%s """ % (field_value, ids)
            cr.execute(sql_str)

        return True

    def _date_expected(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""SELECT id, date_expected from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                date_str = False
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id.latest_ship_date_order
                    if not res[oh.id] and oh.so_id.order_line:
                        date_str = max([line.expected_ship_date for line in oh.so_id.order_line])
                elif oh.type == 'stock_move' and oh.sm_id.picking_id and oh.sm_id.picking_id.sale_id:
                    res[oh.id] = oh.sm_id.picking_id.sale_id.latest_ship_date_order
                    if not res[oh.id]:
                        date_str = oh.sm_id.sale_line_id.expected_ship_date
                else:
                    res[oh.id] = False
                # Convert format like '20150714' to system-readable '2015-07-14'
                if date_str and date_str.isdigit() and len(date_str) == 8:
                    res[oh.id] = date_str[:4] + '-' + date_str[4:6] + '-' + date_str[6:]
                if old_res.get(oh.id) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""UPDATE order_history set date_expected=%s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        cr.execute("""UPDATE order_history set date_expected = NULL where id=%s""", (oh.id, ))
        return res

    def _do_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, do_name from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move' and oh.sm_id and oh.sm_id.picking_id:
                    res[oh.id] = oh.sm_id.picking_id.name
                elif oh.sm_id.is_set and oh.sm_id.set_parent_order_id:
                    res[oh.id] = oh.sm_id.set_parent_order_id.name
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set do_name=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _partner_id(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, partner_id from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_partner_id.id
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_partner_id.id
                else:
                    res[oh.id] = False
                if old_res.get(oh.id) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""update order_history set partner_id=%s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        if oh.sm_id.set_parent_order_id.id:
                            cr.execute("""update order_history set parent_sale_order = null where id=%s""",
                                       (oh.id,))
                        else:
                            cr.execute("""update order_history set partner_id=null where id=%s""", (oh.id,))
        return res

    def _partner_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, partner_name from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                res[oh.id] = oh.partner_id.name or ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set partner_name=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _state(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, state from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = 'so_' + oh.so_state or 'undefined'
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_state or 'undefined'
                else:
                    res[oh.id] = 'undefined'
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set state=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _picked_rate(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id.picked_rate or 0.0
                elif oh.type == 'stock_move' and oh.sm_id.picking_id and oh.sm_id.picking_id.sale_id:
                    res[oh.id] = oh.sm_id.picking_id.sale_id.picked_rate or 0.0
                else:
                    res[oh.id] = 0.0
        return res

    def _invoiced_rate(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id.invoiced_rate or 0.0
                elif oh.type == 'stock_move' and oh.sm_id.picking_id and oh.sm_id.picking_id.sale_id:
                    res[oh.id] = oh.sm_id.picking_id.sale_id.invoiced_rate or 0.0
                else:
                    res[oh.id] = 0.0
        return res

    def _date(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, date from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_date
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_date
                else:
                    res[oh.id] = oh.date_order
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set date=%s where id=%s""", (res[oh.id] or None, oh.id, ))
        return res

    def _shp_date(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, shp_date from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move' and oh.sm_id and oh.sm_id.picking_id:
                    res[oh.id] = oh.sm_id.picking_id.shp_date
                else:
                    res[oh.id] = False
                if old_res.get(oh.id) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""update order_history set shp_date=%s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        cr.execute("""update order_history set shp_date=null where id=%s""", (oh.id, ))
        return res

    def _carrier(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, carrier from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_carrier or ''
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_carrier or ''
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set carrier=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _delmar_ids(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, delmar_ids from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move' and oh.sm_product_id and oh.sm_product_id.default_code:
                    res[oh.id] = oh.sm_product_id.default_code + (oh.sm_size_id and '/' + oh.sm_size_id.name or '')
                elif oh.type == 'sale_order' and oh.so_id and oh.so_id.order_line:
                    prods = set([])
                    for line in oh.so_id.order_line:
                        prod = line.product_id and line.product_id.default_code and line.product_id.default_code + (line.size_id and '/' + line.size_id.name or '') or False
                        if prod:
                            prods.add(prod)
                    res[oh.id] = ', '.join(prods)
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set delmar_ids=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _option_sku(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, option_sku from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move' and oh.sm_id and oh.sm_id.sale_line_id:
                    res[oh.id] = oh.sm_id.sale_line_id.optionSku or ''
                elif oh.type == 'sale_order' and oh.so_id and oh.so_id.order_line:
                    o_SKUs = set([])
                    for line in oh.so_id.order_line:
                        o_SKU = line.optionSku or ''
                        if o_SKU:
                            o_SKUs.add(o_SKU)
                    res[oh.id] = ', '.join(o_SKUs)
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set option_sku=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _warehouse(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id,warehouse from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move':
                    res[oh.id] = self.pool.get('stock.location').get_warehouse(cr, uid, oh.sm_location_id.id) or None
                else:
                    res[oh.id] = None
                if old_res.get(oh.id, False) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""update order_history set warehouse=%s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        cr.execute("""update order_history set warehouse=null where id=%s""", (oh.id, ))
        return res

    def _address_id(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, address_id from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                res[oh.id] = False
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id and oh.so_id.partner_shipping_id and oh.so_id.partner_shipping_id.id or False
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_id and oh.sm_do_id.address_id and oh.sm_do_id.address_id.id or False

                if old_res.get(oh.id, False) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""update order_history set address_id = %s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        if oh.sm_id.set_parent_order_id.id:
                            cr.execute("""update order_history set parent_sale_order = null where id=%s""", (oh.id,))
                        else:
                            cr.execute("""update order_history set address_id = null where id=%s""", (oh.id,))

        return res

    def _shipping_country_id(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, address_id from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                res[oh.id] = False
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_id and oh.so_id.partner_shipping_id and oh.so_id.partner_shipping_id.country_id and oh.so_id.partner_shipping_id.country_id.id or False
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_id and oh.sm_do_id.address_id and oh.sm_do_id.address_id.country_id and oh.sm_do_id.address_id.country_id.id or False

                if old_res.get(oh.id, False) != res[oh.id]:
                    if res[oh.id]:
                        cr.execute("""update order_history set shipping_country_id = %s where id=%s""", (res[oh.id], oh.id, ))
                    else:
                        cr.execute("""update order_history set shipping_country_id = null where id=%s""", (oh.id, ))
        return res

    def _location_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id,location_name from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'stock_move' and oh.sm_location_id:
                    res[oh.id] = self.pool.get('stock.location').name_get(cr, uid, [oh.sm_location_id.id])[0][1] or ''
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set location_name=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _tracking_ref(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, tracking_ref from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_tracking_ref or ''
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_tracking_ref or ''
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set tracking_ref=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _product_qty(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, product_qty from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = None
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_product_qty or None
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set product_qty=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _note(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""select id, note from order_history where id in %s""", (tuple(ids), ))
            old_res = dict(cr.fetchall())
            for oh in self.browse(cr, uid, ids):
                if oh.type == 'sale_order':
                    res[oh.id] = oh.so_note or ''
                elif oh.type == 'stock_move':
                    res[oh.id] = oh.sm_do_note or ''
                else:
                    res[oh.id] = ''
                if old_res.get(oh.id) != res[oh.id]:
                    cr.execute("""update order_history set note=%s where id=%s""", (res[oh.id], oh.id, ))
        return res

    def _mp_data(self, cr, uid, ids, name, arg, context=None):
        res = {}
        mp_partner_ids = [48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43,
                          870, 881, 130, 208, 924, 936]
        for oh in self.browse(cr, uid, ids, context=context):
            res[oh.id] = False
            if oh.type == 'sale_order':
                res[oh.id] = True if oh.so_id.partner_id.id in mp_partner_ids else False
            elif oh.type == 'stock_move' and oh.sm_id.picking_id:
                res[oh.id] = True if oh.sm_id.picking_id.real_partner_id.id in mp_partner_ids else False

            # if oh.sm_id.sale_line_id.order_id.partner_id.id in mp_partner_ids:
            #     if oh.sm_id.sale_line_id.customerCost in [False, '', '0', '0.00', '0.0']:
            #         if oh.sm_id.sale_line_id.price_unit in [False, '', '0', '0.00', '0.0']:
            #             res[oh.id] = True
            # else:
            #     if oh.sm_id.sale_line_id.customerCost in [False, '', '0', '0.00', '0.0']:
            #         res[oh.id] = True

        return res

    def _mp_search(self, cr, uid, ids, name, args, context=None):
        ids = tuple()
        for search_item in args:
            if search_item[0] == 'is_market_place':
                sql = """
                SELECT oh.id FROM order_history oh
                INNER JOIN stock_move sm ON oh.sm_id = sm.id
                INNER JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                INNER JOIN sale_order so ON sol.order_id = so.id
                WHERE 1=1
                    AND so.partner_id not in (48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43,
                          870, 881, 130, 208, 924, 936)
                    AND (sol."customerCost" is null or sol."customerCost" in ('0.00','0.0', '0','')) and oh.state in ('draft', 'assigned','confirmed', 'waiting_split', 'incode_except','address_except','back')
                
                ;"""
                cr.execute(sql)
                ids = tuple([id[0] for id in cr.fetchall()])
        if ids:
            return [('id', 'in', ids)]
        return [('id', '=', '0')]

    _columns = {
        'type': fields.selection([
            ('sale_order', 'Sale order'),
            ('stock_move', 'Stock Move'),
            ], 'Type', readonly=True, select=True, required=True),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True),
        "tagging_ids": fields.many2many("tagging.order", "taggings_order", "order_id", "tag_id", string="Tags"),
        # SO columns
        'so_id': fields.many2one('sale.order', 'Sales Order', readonly=True, select=True, ondelete='cascade'),
        'so_partner_id': fields.related('so_id', 'partner_id', type='many2one', relation='res.partner', string='Partner', readonly=True, select=True),
        'so_state': fields.related('so_id', 'state', type='selection', string="Sale Order State", select=True, readonly=True, selection=SALE_ORDER_STATES),
        'so_date': fields.related('so_id', 'date_order', type="datetime", string="Order Date"),
        'so_carrier': fields.related('so_id', 'carrier', type="char", string="Carrier"),
        'so_tracking_ref': fields.related('so_id', 'tracking_ref', type='char', string="Tracking Ref"),
        'so_note': fields.related('so_id', 'note', type='text', string="Notes"),
        'so_address_id': fields.related('so_id', 'partner_shipping_id', type='many2one', relation="res.partner.address", string="Address"),
        # 'so_ship2store': fields.related('so_id', 'ship2store', type='boolean', string="Ship to Store"),
        # SM/DO columns
        'sm_id': fields.many2one('stock.move', 'Stock Move', select=True, readonly=True, ondelete='cascade'),
        'sm_do_id': fields.related('sm_id', 'picking_id', type='many2one', string="Delivery order", readonly=True, relation='stock.picking'),
        'sm_partner_id': fields.related('sm_id', 'picking_id', 'real_partner_id', type='many2one', relation='res.partner', string='Partner', readonly=True, select=True),
        # 'sm_state': fields.related('sm_id', type='selection', string="Stock Move State", select=True, readonly=True, selection=ALL_STATES),
        'sm_do_state': fields.related('sm_id', 'picking_id', 'state', type='selection', string="Delivery Order State", select=True, readonly=True, selection=STOCK_PICKING_STATES),
        'sm_do_date': fields.related('sm_id', 'picking_id', 'date', type="datetime", string="Order Date"),
        'sm_product_id': fields.related('sm_id', 'product_id', type='many2one', string="Product", readonly=True, relation='product.product'),
        'sm_size_id': fields.related('sm_id', 'size_id', type='many2one', string="Ring Size", readonly=True, relation='ring.size'),
        'sm_location_id': fields.related('sm_id', 'location_id', type='many2one', string="Location", readonly=True, relation='stock.location'),
        'sm_do_carrier': fields.related('sm_id', 'picking_id', 'carrier', type="char", string="Carrier"),
        'sm_do_tracking_ref': fields.related('sm_id', 'picking_id', 'tracking_ref', type='char', string="Tracking Ref"),
        'sm_note': fields.related('sm_id', 'note', type='text', string="Notes"),
        'sm_do_note': fields.related('sm_id', 'picking_id', 'note', type='text', string="Notes"),
        'sm_address_id': fields.related('sm_id', 'address_id', type='many2one', relation="res.partner.address", string="Address"),
        'sm_product_qty': fields.related('sm_id', 'product_qty', string="Qty"),
        'sm_export_ref_ca': fields.related('sm_id', 'export_ref_ca', string="Export Ref CA", type='text'),
        'sm_export_ref_us': fields.related('sm_id', 'export_ref_us', string="Export Ref US", type='text'),
        'sm_fc_invoice': fields.related('sm_id', 'fc_invoice', string="FC Invoice", type='text'),
        'sm_fc_order': fields.related('sm_id', 'fc_order', string="FC Order", type='text'),
        'sm_del_invoice': fields.related('sm_id', 'del_invoice', string="DEL Invoice", type='text'),
        'sm_del_order': fields.related('sm_id', 'del_order', string="DEL Order", type='text'),
        'sm_bill_of_lading': fields.related('sm_id', 'bill_of_lading', string="BILL OF LADING", type='text'),
        # self functional columns
        'parent_sale_order': fields.function(_parent_sale_order, string="Sale order", type='many2one', relation='sale.order', method=True, store=True),
        'order_name': fields.function(_order_name, string="Sale order name", type='char', method=True, store={
                'sale.order': (_get_oh_sale, ['name'], 10),
            }),
        'ship2store': fields.function(_ship2store, string="Ship to Store", type='boolean', method=True, store={
                'sale.order': (_get_oh_sale, ['ship2store'], 10),
            }),
        'do_name': fields.function(_do_name, string="Delivery order name", type='char', method=True, store={
                'stock.picking': (_get_oh_picking, ['name'], 10),
                'stock.move': (_get_oh_move, ['picking_id'], 10),
            }),
        'partner_id': fields.function(_partner_id, string="Partner", type='many2one', relation='res.partner', method=True, store={
                'sale.order': (_get_oh_sale, ['partner_id'], 10),
                'stock.picking': (_get_oh_picking, ['real_partner_id'], 10),
            }),
        'partner_name': fields.function(_partner_name, string="Partner", type='char', method=True, store={
                'sale.order': (_get_oh_sale, ['partner_id'], 10),
                'stock.picking': (_get_oh_picking, ['real_partner_id'], 10),
            }),
        'state': fields.function(_state, string="State", type='selection', method=True, selection=ALL_STATES, store={
                'sale.order': (_get_oh_sale, ['state'], 10),
                'stock.move': (_get_oh_move, ['picking_id'], 10),
                'stock.picking': (_get_oh_picking, ['state'], 10),
            }),
        'date': fields.function(_date, string="Date", type='datetime', method=True, store={
                'sale.order': (_get_oh_sale, ['date_order'], 10),
                'stock.move': (_get_oh_move, ['picking_id'], 10),
                'stock.picking': (_get_oh_picking, ['date'], 10),
            }),
        'shp_date': fields.function(_shp_date, string='Shipped Date', type='datetime', method=True, store={
                'stock.move': (_get_oh_move, ['picking_id'], 10),
                'stock.picking': (_get_oh_picking, ['shp_date'], 10),
            }),
        'date_expected': fields.function(_date_expected, string="Expected Ship date", type='date', method=True,
                fnct_inv=_update_date_expected, store={
                'sale.order': (_get_oh_sale, ['latest_ship_date_order'], 10),
                'sale.order.line': (_get_oh_sale_line, ['expected_ship_date'], 10),
            }),
        'carrier': fields.function(_carrier, string="Carrier", type='char', method=True, store={
                'sale.order': (_get_oh_sale, ['carrier'], 10),
                'stock.picking': (_get_oh_picking, ['carrier'], 10),
            }),
        'delmar_ids': fields.function(_delmar_ids, string="Products", type='char', method=True, store={
                'product.product': (_get_oh_product, ['default_code'], 10),
                'sale.order': (_get_oh_sale, ['order_line'], 10),
                'sale.order.line': (_get_oh_sale_line, ['product_id'], 10),
                'stock.move': (_get_oh_move, ['product_id'], 10),
            }),
        'option_sku': fields.function(_option_sku, string="Option SKU", type='char', method=True, store={
                'sale.order': (_get_oh_sale, ['order_line'], 10),
                'sale.order.line': (_get_oh_sale_line, ['product_id'], 10),
                'stock.move': (_get_oh_move, ['product_id'], 10),
            }),
        'warehouse': fields.function(_warehouse, string="Warehouse", type='many2one', relation='stock.warehouse', method=True, store={
                'stock.picking': (_get_oh_picking, ['state'], 10),
                'stock.move': (_get_oh_move, ['location_id', 'picking_id'], 10),
                'stock.location': (_get_oh_location, ['name'], 10),
            }),
        'location_name': fields.function(_location_name, string="Location", type='char', method=True, store={
                'stock.picking': (_get_oh_picking, ['state'], 10),
                'stock.move': (_get_oh_move, ['location_id'], 10),
                'stock.location': (_get_oh_location, ['name'], 10),
            }),
        'tracking_ref': fields.function(_tracking_ref, string="Tracking Ref", type='char', method=True, store={
                'sale.order': (_get_oh_sale, ['tracking_ref'], 10),
                'stock.picking': (_get_oh_picking, ['tracking_ref'], 10),
            }),
        'picked_rate': fields.function(_picked_rate, string="Picked", type='float', method=True, store=False),
        'invoiced_rate': fields.function(_invoiced_rate, string="Invoiced", type='float', method=True, store=False),
        'note': fields.function(_note, string="Notes", type='text', method=True, store={
                'stock.move': (_get_oh_move, ['note'], 10),
                'stock.picking': (_get_oh_picking, ['note'], 10),
                'sale.order': (_get_oh_sale, ['note'], 10),
            }),
        'address_id': fields.function(_address_id, string="Name and Address", type="many2one", relation="res.partner.address", method=True, store={
            'sale.order': (_get_oh_sale, ['partner_shipping_id'], 10),
            'stock.picking': (_get_oh_picking, ['address_id'], 10),
            }),
        'shipping_country_id': fields.function(_shipping_country_id, string="Ship to Country", type="many2one", relation="res.country", method=True, store={
            'sale.order': (_get_oh_sale, ['partner_shipping_id'], 10),
            'stock.picking': (_get_oh_picking, ['address_id'], 10),
            }),
        'product_qty': fields.function(_product_qty, string="Qty", type="float", method=True, store={
            'stock.move': (_get_oh_move, ['product_qty'], 10),
            }),
        'first_print_date': fields.datetime('First print date', readonly=True, select=True),
        'invoice_no': fields.related('sm_id', 'picking_id', 'invoice_no', type="char", string="Invoice#"),
        'customerCost': fields.related('sm_id', 'sale_line_id', 'customerCost', type="char", string="customerCost"),
        'is_market_place': fields.function(
            _mp_data,
            string='is_mp',
            fnct_search=_mp_search,
            type='boolean',
            method=True,
            store=False,
            help="is_market_place"
        ),
    }

    _long_searches = [
        'exceptions',
        'parent_sale_order.flash',
    ]

    def search_date(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            date_to_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'date' and arg[1] == '<=')).next()
            first_term_index = args.index(date_to_domain)
            if args[first_term_index-2] == '&' and args[first_term_index-1][0] == args[first_term_index][0] and args[first_term_index-1][1] == '>=':
                new_args = args[:first_term_index-2] + [args[first_term_index-1]] + args[first_term_index+1:]
                return new_args
            else:
                return args
        except:
            return args

    def search_tracking_ref(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            tracking_ref_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'tracking_ref')).next()
            first_term_index = args.index(tracking_ref_domain)
            new_args = args[:first_term_index] + ['|', ['so_tracking_ref', tracking_ref_domain[1], tracking_ref_domain[2]], ['sm_do_tracking_ref', tracking_ref_domain[1], tracking_ref_domain[2]]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def search_carrier(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            carrier_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'carrier')).next()
            first_term_index = args.index(carrier_domain)
            new_args = args[:first_term_index] + ['|', ['so_carrier', carrier_domain[1], carrier_domain[2]], ['sm_do_carrier', carrier_domain[1], carrier_domain[2]]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def search_partner_id(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            partner_id_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'partner_id')).next()
            first_term_index = args.index(partner_id_domain)
            new_args = args[:first_term_index] + ['|', ['so_partner_id', partner_id_domain[1], partner_id_domain[2]], ['sm_partner_id', partner_id_domain[1], partner_id_domain[2]]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def search_state(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            state_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'state')).next()
            first_term_index = args.index(state_domain)
            if state_domain[1] in ('=', 'like', 'ilike'):
                if 'so_' in state_domain[2]:
                    new_args = args[:first_term_index] + [['so_state', state_domain[1], state_domain[2].replace('so_', '')]] + args[first_term_index + 1:]
                else:
                    new_args = args[:first_term_index] + [['sm_do_state', state_domain[1], state_domain[2]]] + args[first_term_index+1:]
                return new_args
            elif state_domain[1] == 'in':
                so_states = (st.replace('so_', '') for st in state_domain[2] if 'so_' in st)
                sm_do_states = (st for st in state_domain[2] if 'so_' not in st)
                new_args = args[:first_term_index] + ['|', ['so_state', state_domain[1], so_states], ['sm_do_state', state_domain[1], sm_do_states]] + args[first_term_index+1:]
                return new_args
            return args
        except:
            return args

    def search_note(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            note_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'note')).next()
            first_term_index = args.index(note_domain)
            new_args = args[:first_term_index] + ['|', ['so_note', note_domain[1], note_domain[2]], ['sm_do_note', note_domain[1], note_domain[2]]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def search_sale_order(self, cr, uid, args, context=None):
        if not context:
            context = {}
        try:
            so_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'parent_sale_order')).next()
            ids_list = so_domain[2].replace('\n', ' ').replace('\t', ' ').replace(',', ' ')
            #ids_list = [x for x in ids_list.split(' ') if x]
            first_term_index = args.index(so_domain)
            new_args = args[:first_term_index] + [['parent_sale_order.name', 'ilike', ids_list]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def search_exceptions(self, cr, uid, args, context=None):
        try:
            ex_domain = (arg for arg in args if (isinstance(arg, (list, tuple)) and arg[0] == 'exceptions')).next()
            ids_list = self.filter_exceptions(cr, uid)
            first_term_index = args.index(ex_domain)
            new_args = args[:first_term_index] + [['id', 'in', ids_list]] + args[first_term_index+1:]
            return new_args
        except:
            return args

    def filter_flash(self, cr, uid, args, ids=None, context=None):
        result_ids = []

        if isinstance(ids, list) and not ids:
            return ids

        sql_query = """SELECT oh.id
            FROM order_history oh
            LEFT JOIN sale_order so on oh.parent_sale_order = so.id
            WHERE 1=1
                {}AND oh.id in %s
                AND so.flash {} {}
        """
        for arg in args:
            cr.execute(
                sql_query.format(
                    '--' if ids is None else '',
                    arg[1],
                    arg[2]
                ), (tuple(ids or []), )
            )
            res = cr.fetchall() or []
            result_ids = [row[0] for row in res]
        return result_ids

    def filter_exceptions(self, cr, uid, ids=None, context=None):
        result_ids = []

        if isinstance(ids, list) and not ids:
            return ids

        sp_states = list(set([x[0] for x in STOCK_PICKING_STATES]) - set(['assigned', 'picking', 'shipped', 'done', 'returned', 'final_cancel']))
        so_states = ['so_' + x for x in set([x[0] for x in SALE_ORDER_STATES]) - set(['done', 'progress', 'manual', 'cancel'])]

        sql_query = """SELECT oh.id
            FROM order_history oh
                LEFT JOIN sale_order so on oh.parent_sale_order = so.id
                LEFT JOIN stock_move sm on oh.sm_id = sm.id
                LEFT JOIN stock_picking sp on sm.picking_id = sp.id
            WHERE 1=1
                {}AND oh.id in %(ids)s
                AND oh.state in %(states)s
        """

        cr.execute(
            sql_query.format('--' if ids is None else ''),
            {
                'states': tuple(sp_states + so_states),
                'ids': tuple(ids or []),
            }
        )
        res = cr.fetchall() or []
        result_ids = [row[0] for row in res]

        return result_ids

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}
        if args:
            flash_args = []
            if len(args) == 1 and args[0][0] == 'parent_sale_order.flash':
                return []
            args = self.search_date(cr, uid, args, context=context)
            args = self.search_tracking_ref(cr, uid, args, context=context)
            args = self.search_carrier(cr, uid, args, context=context)
            # args = self.search_partner_id(cr, uid, args, context=context)
            args = self.search_state(cr, uid, args, context=context)
            args = self.search_note(cr, uid, args, context=context)
            args = self.search_sale_order(cr, uid, args, context=context)
            args = self.search_exceptions(cr, uid, args, context=context)

            for arg in args:
                if arg[0] == 'parent_sale_order.flash' and arg[1] in ('=', '!='):
                    flash_args.append(arg)
                    args.remove(arg)

            if order:
                order = order.replace('partner_id', 'partner_name')
                order = order.replace('parent_sale_order', 'order_name')
                order = order.replace('sm_do_id', 'do_name')
                order = order.replace('sm_location_id', 'location_name')


            if flash_args:
                res_ids = None
                if args:
                    res_ids = super(order_history, self).search(cr, uid, args, limit=None, context=context)
                if flash_args:
                    res_ids = self.filter_flash(cr, uid, flash_args, res_ids, context=context)

                if not limit and count:
                    return len(res_ids)

                args = [['id', 'in', res_ids]]

            return super(order_history, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []

    def create(self, cr, uid, vals, context=None):
        new_oh_id = super(order_history, self).create(cr, uid, vals, context=context)
        new_oh = self.browse(cr, uid, new_oh_id)
        wr_vals = {}
        wr_vals['parent_sale_order'] = new_oh._parent_sale_order(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['order_name'] = new_oh._order_name(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['ship2store'] = new_oh._ship2store(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['date_expected'] = new_oh._date_expected(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['do_name'] = new_oh._do_name(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['partner_id'] = new_oh._partner_id(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['partner_name'] = new_oh._partner_name(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['state'] = new_oh._state(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['picked_rate'] = new_oh._picked_rate(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['invoiced_rate'] = new_oh._invoiced_rate(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['date'] = new_oh._date(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['shp_date'] = new_oh._shp_date(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['carrier'] = new_oh._carrier(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['delmar_ids'] = new_oh._delmar_ids(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['option_sku'] = new_oh._option_sku(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['warehouse'] = new_oh._warehouse(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['location_name'] = new_oh._location_name(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['tracking_ref'] = new_oh._tracking_ref(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['note'] = new_oh._note(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['product_qty'] = new_oh._product_qty(cr, uid, [new_oh_id])[new_oh_id]
        wr_vals['address_id'] = new_oh._address_id(cr, uid, [new_oh_id])[new_oh_id]
        self.write(cr, uid, [new_oh_id], wr_vals)

        self._check_set(cr, uid,new_oh_id,wr_vals, vals, context)

        return new_oh_id

    def write(self, cr, uid, ids, vals, context=None):
        super(order_history, self).write(cr, uid, ids, vals, context=context)
        if vals.get('customerCost', False):
            oh_objs = self.browse(cr, uid, ids)
            for oh_obj in oh_objs:
                cr.execute("""
  select
        					sm.picking_id as id,
        					 sum((CASE WHEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT) > 0.0
	                                    THEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT)
	                                       WHEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT) = 0.0 AND sol.order_partner_id IN
	                                                                                                         (48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936)
	                                         THEN sol.price_unit
	                                       ELSE 0.0 END) * sm.product_qty) AS total_price, 
        					sum(( case when cast( coalesce( sol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( sol."customerCost", '0.0' ) as float ) else sol.price_unit end )* sm.product_qty ) as total_price_rule
        				from
        					stock_move sm left join sale_order_line sol on
        					sm.sale_line_id = sol.id
        				where
        					sm.id in(
        						select
        							r.id
        						from
        							(
        								select
        									id,
        									set_product_id,
        									product_id
        								from
        									stock_move
        								where
        									picking_id = %s
        							) r
        						where
        							r.set_product_id != r.product_id
        							or r.set_product_id is null
        					)
        				group by
        					sm.picking_id
                            """, (oh_obj.sm_id.picking_id.id,))
                for row in cr.dictfetchall():
                    cr.execute("""update stock_picking set total_price=%s, total_price_rules=%s where id=%s""", (row.get('total_price'),row.get('total_price_rule'), row.get('id'),))

        return True


    def _check_set(self, cr, uid, new_oh_id, wr_vals, vals, context):
        if new_oh_id and (vals.get('is_set', False) or self.browse(cr, uid, new_oh_id).sm_id.is_set):
            stock_move = self.browse(cr, uid, new_oh_id).sm_id
            set_parent_order_id = stock_move.picking_id.id
            parent_product_id = stock_move.product_id.id
            components_ids = self.pool.get('stock.move').search(cr, uid, [('set_product_id', '=', parent_product_id),
                                                                          ('set_parent_order_id', '=',
                                                                           set_parent_order_id)])
            if len(components_ids) > 0:
                cmps = self.pool.get('stock.move').browse(cr, uid, components_ids)
                for sm in cmps:
                    c_wr_vals = vals
                    c_wr_vals['delmar_ids'] = sm.product_id.default_code + (sm.size_id and '/' + sm.size_id.name or '')
                    c_wr_vals['sm_id'] = sm.id
                    c_wr_vals['product_qty'] = sm.product_qty
                    c_wr_vals['location_id'] = sm.location_id
                    c_wr_vals['do_name'] = wr_vals['do_name']
                    c_wr_vals['order_name'] = wr_vals['order_name']
                    c_wr_vals['date'] = wr_vals['date']
                    c_wr_vals['partner_id'] = wr_vals['partner_id']
                    c_wr_vals['parent_sale_order'] = wr_vals['parent_sale_order']
                    c_wr_vals['warehouse'] = wr_vals['warehouse']
                    c_wr_vals['address_id'] = wr_vals['address_id']
                    c_wr_vals['carrier'] = wr_vals['carrier']
                    c_wr_vals['tracking_ref'] = wr_vals['tracking_ref']
                    c_wr_vals['state'] = wr_vals['state']
                    c_wr_vals['partner_name'] = wr_vals['partner_name']
                    new_oh_comp_id = super(order_history, self).create(cr, uid, c_wr_vals, context=context)


    def force_create_sm_history(self, cr, uid, so_id, context=None):
        cr.execute("""
            delete from order_history
            where so_id = {order_id}
            """.format(order_id=so_id))
        cr.execute("""INSERT INTO order_history (
                create_uid,
                create_date,
                date,
                date_expected,
                partner_id,
                partner_name,
                parent_sale_order,
                order_name,
                type,
                sm_id,
                note,
                state,
                do_name,
                delmar_ids,
                carrier,
                product_qty,
                address_id,
                location_name,
                warehouse
            )
            select
                sm.create_uid,
                sm.create_date,
                sm.create_date,
                so.latest_ship_date_order,
                rp.id,
                rp.name,
                so.id,
                so.name,
                'stock_move',
                sm.id,
                sm.note,
                sm.state,
                sp.name,
                concat(pp.default_code, case when rs.id is null then '' else concat('/', rs.name) end),
                sp.carrier,
                sm.product_qty,
                sp.address_id,
                sl.complete_name,
                sl.warehouse_id
            from sale_order so
                left join res_partner rp on rp.id = so.partner_id
                left join stock_picking sp on so.id = sp.sale_id
                left join stock_move sm on sm.picking_id = sp.id
                left join stock_location sl on sm.location_id = sl.id
                left join product_product pp on pp.id = sm.product_id
                left join ring_size rs on rs.id = sm.size_id
            where so.id = {order_id} and sm.id is not null
        """.format(order_id=so_id))

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

order_history()


class order_history_export(osv.osv):
    _name = "order.history.export"
    _columns = {
        "csv_file": fields.binary(string="CSV Export", readonly=True),
        "csv_filename": fields.char("", size=256),
        'state': fields.boolean("state")
    }

    _defaults = {
        'state': False,
        'csv_file': ''
    }

    def action_order_history_export(self, cr, uid, ids, context=None):
        oh_ids = context.get('active_ids', [])
        res = [[
            "Type",
            "State",
            "Partner",
            "Sale order",
            "External Customer Line id",
            "Picked",
            "Invoiced",
            "Products",
            "Vendor SKU",
            "Overstock SKU",
            "Option Sku",
            "Qty",
            "Price unit",
            "Customer Cost",
            "Shipping Cost",
            "Warehouse",
            "Location",
            "Bin",
            "Tracking ref",
            "Date",
            "Shipped date",
            "Carrier",
            "Original Carrier",
            "Shipping Address",
            "Notes",
        ]]
        if oh_ids:
            for oh_id in oh_ids:
                oh_obj = self.pool.get("order.history").browse(cr, uid, oh_id)
                state = oh_obj.state and [st for st in ALL_STATES if st[0] == oh_obj.state]
                state_name = state and ALL_STATES[ALL_STATES.index(state[0])][1] or ''
                wh_id = oh_obj.type == 'stock_move' and oh_obj.sm_id and self.pool.get('stock.location').get_warehouse(cr, uid, oh_obj.sm_location_id.id) or False
                warehouse = wh_id and self.pool.get('stock.warehouse').browse(cr, uid, wh_id).name or ''
                os_product_sku = option_sku = vendorSku = ''
                os_product_sku_field_name = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('label', 'ilike', "Customer ID Delmar")], context=context)
                cf_obj = self.pool.get('product.multi.customer.fields')

                if os_product_sku_field_name and oh_obj.type == 'stock_move' and oh_obj.sm_id:
                    os_product_sku_field = cf_obj.search(cr, uid, ['&', ('field_name_id', 'in', os_product_sku_field_name), ('product_id', '=', oh_obj.sm_id.product_id.id)])
                    if os_product_sku_field:
                        os_product_sku = cf_obj.browse(cr, uid, os_product_sku_field[0]).value or ''
                elif os_product_sku_field_name and oh_obj.type == 'sale_order' and oh_obj.so_id:
                    values = set([])
                    for line in oh_obj.so_id.order_line:
                        os_product_sku_field = cf_obj.search(cr, uid, ['&', ('field_name_id', 'in', os_product_sku_field_name), ('product_id', '=', line.product_id.id)])
                        if os_product_sku_field:
                            current_os_product_sku = cf_obj.browse(cr, uid, os_product_sku_field[0]).value or ''
                            values.add(current_os_product_sku)
                    os_product_sku = ', '.join(values)

                if oh_obj.type == 'stock_move' and oh_obj.sm_id:
                    option_sku = oh_obj.sm_id.sale_line_id.optionSku or ''
                    vendorSku = oh_obj.sm_id.sale_line_id.vendorSku or ''
                elif oh_obj.type == 'sale_order' and oh_obj.so_id:
                    option_values = set([])
                    vendor_values = set([])
                    for line in oh_obj.so_id.order_line:
                        if line.optionSku:
                            option_values.add(line.optionSku)
                        if line.vendorSku:
                            vendor_values.add(line.vendorSku)
                    option_sku = ', '.join(option_values)
                    vendorSku = ', '.join(vendor_values)
                oh_row = [
                    oh_obj.type or '',
                    state_name,
                    oh_obj.partner_id and oh_obj.partner_id.name or oh_obj.partner_id or '',
                    oh_obj.parent_sale_order and oh_obj.parent_sale_order.name or oh_obj.parent_sale_order or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.sale_line_id.external_customer_line_id or '',
                    str(oh_obj.picked_rate).replace('.0', '') + '%' or '0%',
                    str(oh_obj.invoiced_rate).replace('.0', '') + '%' or '0%',
                    oh_obj.delmar_ids or '',
                    vendorSku,
                    os_product_sku,
                    option_sku,
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and str(oh_obj.sm_id.product_qty).replace('.000', '') or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.sale_line_id.price_unit or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.sale_line_id.customerCost or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.sale_line_id.shipCost or '',
                    warehouse,
                    oh_obj.sm_location_id and oh_obj.sm_location_id.name or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.bin or '',
                    oh_obj.tracking_ref or '',
                    oh_obj.date or '',
                    oh_obj.shp_date or '',
                    oh_obj.carrier or '',
                    oh_obj.parent_sale_order and self.pool.get('sale.order').browse(cr, uid, oh_obj.parent_sale_order.id).carrier or '',
                    oh_obj.type == 'stock_move' and oh_obj.sm_id and oh_obj.sm_id.picking_id.address_id and self.pool.get('res.partner.address').name_get(cr, uid, [oh_obj.sm_id.picking_id.address_id.id])[0][1] or '',
                    oh_obj.note or '',
                ]
                res.append(oh_row)

        fp = StringIO()
        writer = csv.writer(fp, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, lineterminator='\n')
        for data in res:
            writer.writerow([type(el) is unicode and el.encode('utf-8') or str(el).encode('utf-8') for el in data])
        fp.seek(0)
        data = fp.read()
        fp.seek(0)

        fp.truncate()
        fp.close()
        return self.write(cr, uid, ids, {'state': True, 'csv_file': base64.encodestring(data), 'csv_filename': 'OrderActivity.csv'}, context=context)

order_history_export()


class taggings_order(osv.osv):
    _inherit = "tagging.tags"
    _name = "tagging.order"

    _columns = {
        "order_ids": fields.many2many("order.history", "taggings_order", "tag_id", "order_id", string="Orders"),
        "order_list": fields.text("Orders list"),
        "order_list_confirm": fields.boolean("Add orders"),
        "order_list_delete": fields.boolean("Delete orders"),
        "not_founded": fields.text("Not Found Orders"),
        'fc_invoice': fields.char('FC Invoice', size=256,),
        'fc_order': fields.char('FC Order', size=256,),
        'del_invoice': fields.char('DEL Invoice', size=256,),
        'del_order': fields.char('DEL Order', size=256,),
        'export_ref_ca': fields.char('Export REF CA', size=256,),
        'export_ref_us': fields.char('Export REF US', size=256, ),
        'bill_of_lading': fields.char('BILL OF LADING', size=100, ),
        "override_existing": fields.boolean('Override existing', help='Override existing fields.', store=False, ),
    }

    _defaults = {
        "override_existing": False,
    }

    def action_update_transactions(self, cr, uid, ids, context=None):
        """Update transactions table for orders in given tag."""
        def _get_manifest_move_lines(manifest_ids):
            """Supplementary function to extract picking ids from manifest."""
            manifest_obj = self.pool.get('stock.picking.manifest')
            move_lines = []
            pick_ids_uniq = []
            for manifest in manifest_obj.browse(cr, uid, manifest_ids):
                for line in manifest.line_ids:
                    if line.picking_id and line.picking_id.id not in pick_ids_uniq:
                        pick_ids_uniq.append(line.picking_id.id)
                        for move_line in line.picking_id.move_lines:
                            move_lines.append(move_line)
            return move_lines

        # Check out if all required fields are filled
        param_list = ['bill_of_lading', 'export_ref_ca', 'export_ref_us', 'fc_invoice', 'fc_order', 'del_invoice', 'del_order']
        if not filter(lambda up_field: up_field in context, param_list):
            raise osv.except_osv(_('Error!'), _('Please fill in any field!'))

        # get required objects
        server_obj = self.pool.get('fetchdb.server')
        move_obj = self.pool.get('stock.move')

        # Get list of orders we update entries in transactions table
        if context.get('manifest'):
            obj_list = _get_manifest_move_lines(ids)
        else:
            taggings_orders = self.browse(cr, uid, ids)
            orders_list = taggings_orders and taggings_orders[0].order_ids
            order_ids = [int(x.id) for x in orders_list]

            # create list of main stock_move lines
            obj_list = map(lambda x: x.sm_id, orders_list)
            # get list of hidden moves
            hidden_sm_ids = move_obj.search(cr, uid, [('set_parent_order_id', 'in', order_ids)])
            # add hidden moves to full list
            obj_list += move_obj.browse(cr, uid, hidden_sm_ids)
        
        # check that we have records to update
        if len(obj_list) == 0:
            self.log(cr, uid, 1, _("No lines were found for update."))
            return False

        # mssql connection
        server_id = server_obj.get_main_servers(cr, uid, single=True)

        # going through the objects
        sm_updated = 0
        for record in obj_list:   
            # Get stock.move entries corresponding to order_history records
            warehouse = record.warehouse.name
            mssql_context = {'transaction_code': SIGN_DEFAULT_VALUE,
                             'erp_sm_id': record.id,
                             'order_state': record.picking_id.state,
                             }
            sm_context = {}
            for param in param_list:
                if (not record[param] or context.get('override_existing', False)) and context.get(param):
                    mssql_context.update({param: context.get(param)})
                    sm_context.update({param: context.get(param)})
            
            if len(sm_context) == 0:
                continue

            move_obj.update_transactions_table(cr, uid, server_id, server_obj, record, warehouse, mssql_context)
            move_obj.write(cr, uid, record.id, sm_context)
            sm_updated += 1

        self.log(cr, uid, 1, _("{} stock move lines were updated.".format(sm_updated)))
        return {}

    def onchange_order_list(self, cr, uid, ids, order_list, order_ids, order_tag_history_ids, context=None):
        if order_list:
            order_obj = self.pool.get('order.history')
            ids_list = self._get_list_orders(order_list)

            d_res = []
            for d_id in ids_list:
                d_res_1 = self._get_order_ids(d_id, cr, uid, context)
                d_res.extend(d_res_1)

            order_ids_set = set(order_ids[0][2])
            d_res_set = set(d_res)
            found_orders = order_obj.read(cr, uid, d_res, ['do_name'])

            not_found = self._get_not_found(d_res, ids_list, found_orders)
            were_in_tag = self._get_were_in_tag(order_ids_set, d_res_set, found_orders, ids_list)
            add_in_tag = self._get_add_remove_in_tag(order_ids_set, d_res_set, found_orders, ids_list)

            order_ids[0][2] = list(order_ids_set | d_res_set)

            result_info = ''
            if not_found:
                result_info += "%d orders not found in system; " % (len(not_found),)
            if were_in_tag:
                result_info += "%d orders were already in tag; " % (len(were_in_tag),)
            result_info += "%d orders has been added in tag.\n\n" % (len(add_in_tag),)
            if not_found:
                result_info += 'Not found in system:\n' + '\n'.join(not_found) + '\n\n'
            if were_in_tag:
                result_info += 'Were in tag:\n' + '\n'.join(were_in_tag) + '\n\n'
            if add_in_tag:
                result_info += 'New in tag:\n' + '\n'.join(add_in_tag)

            order_tag_history_ids = []
            if ids:
                for d_id in add_in_tag:
                    d_res = self._get_order_ids(d_id, cr, uid, context)
                    order_tag_history_ids.append((0, 0, {
                        'tag_id': self.pool.get('tagging.order').browse(cr, uid, ids)[0].id,
                        'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                        'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                        'order_id': d_res[0],
                        'order_name': self.pool.get('order.history').browse(cr, uid, d_res[0]).do_name,
                        'description': "Order has been added.",
                    }))
            if not ids:
                order_tag_history_ids.append((0, 0, {
                    'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                    'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'description': "Tag created.",
                }))
            return {'value': {'order_ids': order_ids, 'order_tag_history_ids': order_tag_history_ids, 'order_list_confirm': False, 'order_list': '', 'not_founded': result_info}}
        return {}

    def delete_order_list(self, cr, uid, ids, order_list, order_ids, order_tag_history_ids, context=None):
        if order_list:
            order_obj = self.pool.get('order.history')
            ids_list = self._get_list_orders(order_list)

            d_res = []
            for d_id in ids_list:
                d_res_1 = self._get_order_ids(d_id, cr, uid, context)
                d_res.extend(d_res_1)
            order_ids_set = set(order_ids[0][2])
            d_res_set = set(d_res)
            found_orders = order_obj.read(cr, uid, d_res, ['do_name'])

            not_found = self._get_not_found(d_res, ids_list, found_orders)
            remove_in_tag = self._get_were_in_tag(order_ids_set, d_res_set, found_orders, ids_list)
            not_in_tag = self._get_add_remove_in_tag(order_ids_set, d_res_set, found_orders, ids_list)

            order_ids[0][2] = list(order_ids_set - d_res_set)

            result_info = ''
            if not_found:
                result_info += "%d orders not found in system; " % (len(not_found),)
            if not_in_tag:
                result_info += "%d orders weren't in tag; " % (len(not_in_tag),)
            result_info += "%d orders to remove from tag.\n\n " % (len(remove_in_tag),)
            if not_found:
                result_info += 'Not found in system:\n' + '\n'.join(not_found) + '\n\n'
            if not_in_tag:
                result_info += "Weren't tag:\n" + '\n'.join(not_in_tag) + '\n\n'
            if remove_in_tag:
                result_info += 'To remove from tag:\n' + '\n'.join(remove_in_tag)

            order_tag_history_ids = []
            for d_id in remove_in_tag:
                d_res = self._get_order_ids(d_id, cr, uid, context)
                order_tag_history_ids.append((0, 0, {
                    'tag_id': self.pool.get('tagging.order').browse(cr, uid, ids)[0].id,
                    'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                    'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'order_id': d_res[0],
                    'order_name': self.pool.get('order.history').browse(cr, uid, d_res[0]).do_name,
                    'description': "Order has been deleted.",
                }))
            return {'value': {'order_ids': order_ids, 'order_tag_history_ids': order_tag_history_ids, 'order_list_confirm': False, 'order_list': '', 'not_founded': result_info}}
        return {}   

    def _get_list_orders(self, order_list):
        ids_list_str = order_list.replace('\n', ' ').replace('\t', ' ').replace(',', ' ')
        ids_list_str = re.sub(r' +', r' ', ids_list_str).strip()
        ids_list = ids_list_str.split(' ')
        return ids_list

    def _get_order_ids(self, d_id, cr, uid, context):
        d_res = self.pool.get('order.history').search(cr, uid, [('do_name', '=', d_id)], context=context)
        if not d_res:
            d_res = self.pool.get('order.history').search(cr, uid, [('do_name', 'ilike', d_id)], context=context)
        return d_res

    def _get_not_found(self, d_res, ids_list, found_orders):
        found_order_codes = []
        for order in found_orders:
            found_order_codes.append(order.get('do_name'))
        not_found = set([id_list for id_list in ids_list if id_list not in found_order_codes and id_list not in found_order_codes])
        return not_found

    def _get_were_in_tag(self, order_ids_set, d_res_set, found_orders, ids_list):
        were_in_tag_ids = list(order_ids_set & d_res_set)
        were_in_tag_orders = [order for order in found_orders if order.get('id') in were_in_tag_ids]
        were_in_order_tag = []
        for order in were_in_tag_orders:
            were_in_order_tag.append(order.get('do_name'))
        were_in_tag = set([id_list for id_list in ids_list if id_list in were_in_order_tag])
        return were_in_tag

    def _get_add_remove_in_tag(self, order_ids_set, d_res_set, found_orders, ids_list):
        add_in_tag_ids = list(d_res_set - order_ids_set)
        add_in_tag_orders = [order for order in found_orders if order.get('id') in add_in_tag_ids]
        add_in_order_tag = []
        for order in add_in_tag_orders:
            add_in_order_tag.append(order.get('do_name'))
        add_remove_in_tag = set([id_list for id_list in ids_list if id_list in add_in_order_tag])
        return add_remove_in_tag

taggings_order()
