from osv import fields, osv
from tools.translate import _
import time
import datetime
from pf_utils.utils.label_printer import LabelWizPrinter
import logging

_logger = logging.getLogger(__name__)


class internal_shipment_partial_line(osv.TransientModel):
    _name = "internal.shipment.partial.line"
    _rec_name = 'move_id'
    _columns = {
        'move_id': fields.many2one('stock.move', "Move", ondelete='CASCADE'),
        'picking_id': fields.many2one('stock.picking', "Order ID", ondelete='CASCADE'),
        'wizard_id': fields.many2one('internal.shipment.partial', string="Wizard", ondelete='CASCADE'),
        'source_wizard_id': fields.many2one('internal.shipment.partial', string="Wizard", ondelete='CASCADE'),
        'product_id': fields.related('move_id', 'product_id', type='many2one', relation='product.product', string='Product', readonly=True,),
        'size_id': fields.related('move_id', 'size_id', type='many2one', relation='ring.size', string='Size', readonly=True,),
        'product_qty': fields.related('move_id', 'product_qty', type='float', string='Qty', readonly=True,),
        'location_id': fields.related('move_id', 'location_id', type='many2one', relation='stock.location', string='Location', readonly=True,),
    }

    def move_lines_and_reload(self, cr, uid, ids, context=None):
        partial = self.pool.get('internal.shipment.partial')
        line = self.browse(cr, uid, ids)[0]
        wizard_id =line.wizard_id
        if not wizard_id:
            wizard_id = line.source_wizard_id

        results = self.move_lines(cr, uid, ids)
        return {
            'name': _("Products to Process"),
            'view_mode': 'form',
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'internal_shipment_partial_form')[1],
            'view_type': 'form',
            'res_model': 'internal.shipment.partial',
            'res_id': wizard_id.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]'
        }

    def move_lines(self, cr, uid, ids, context=None):
        for line in self.browse(cr, uid, ids):
            if line.wizard_id:
                self.write(cr, uid, line.id, {'wizard_id': None, 'source_wizard_id': line.wizard_id.id})
            else:
                self.write(cr, uid, line.id, {'wizard_id': line.source_wizard_id.id, 'source_wizard_id': None})
        return True

internal_shipment_partial_line()


class internal_shipment_partial(osv.osv_memory):
    _name = 'internal.shipment.partial'
    _rec_name = 'picking_id'
    _columns = {
        'shipment_id': fields.many2one('internal.shipment', 'Shipment', required=True, ondelete='CASCADE'),
        'picking_id': fields.many2one('stock.picking', 'Picking', required=True, ondelete='CASCADE'),
        'combined_id': fields.char('Combined Orders', size=500),
        'line_ids': fields.one2many('internal.shipment.partial.line', 'wizard_id', 'Product Moves'),
        'removed_line_ids': fields.one2many('internal.shipment.partial.line', 'source_wizard_id', 'Product Moves'),
        'warehouse_id': fields.many2one('stock.warehouse', "Warehouse", ),
        'remove_all': fields.boolean('Remove all', ),
        'revert_all': fields.boolean('Revert all', ),
        'refresh': fields.boolean('Refresh', ),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(internal_shipment_partial, self).default_get(cr, uid, fields, context=context)
        picking_id = context.get('picking_id', None)
        if 'combined_id' in fields:
            picking_ids = context.get('combined_id', None).split(", ")
            picking_ids = [int(x) for x in picking_ids]
            res.update(combined_id=context.get('combined_id', None))
        if not picking_id:
            return res
        if 'picking_id' in fields:
            res.update(picking_id=picking_id)
        if 'line_ids' in fields:
            picking_list = self.pool.get('stock.picking').read(cr, uid, picking_ids, ['move_lines'],  context=context)
            line_list = []
            for picking in picking_list:
                pick_id = picking['id']
                for m in picking['move_lines']:
                    line_list.append({'picking_id': pick_id, 'move_id': m})
            lines = line_list
            res.update(line_ids=lines)
        return res

    def _partial_line_for(self, cr, uid, move_id):
        partial_line = {
            'move_id': move_id,
        }
        return partial_line

    def do_partial(self, cr, uid, ids, context=None):
        shipment_id = False

        for wizard in self.browse(cr, uid, ids):

            if wizard.warehouse_id:
                raise osv.except_osv(_('Warning'), 'Please clean filters and check all active items. Only after that you will may confirm ship.')

            if not shipment_id:
                shipment_id = wizard.shipment_id.id
            move_id_list = self.get_move_ids(cr, uid, wizard.line_ids)
            note = ''
            for key in move_id_list:
                picking_id = self.pool.get('stock.picking').browse(cr, uid, int(key))
                move_ids = move_id_list[key]
                if len(picking_id.move_lines) == len(move_ids):
                    move_ids = None
                if wizard.shipment_id.action == 'ship':
                    note += self.pool.get('internal.shipment').action_ship(cr, uid, picking_id.name, picking_id, wizard.shipment_id, move_ids=move_ids, note=note)
                    if not move_ids:
                        new_wh_id = wizard.shipment_id.wh_to and wizard.shipment_id.wh_to.id or False
                        if new_wh_id:
                            self.pool.get('internal.shipment').update_FDX_HED_for_picking(cr, uid, picking_id.id, new_wh_id)

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_internal_shipment_form')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update(res_id=shipment_id)

        return act_win

    def get_move_ids(self, cr, uid, move_ids, context={}):
        move_id_list = {}
        for line in move_ids:
            if line.picking_id in move_id_list:
                move_id_list[line.picking_id].append(line.move_id.id)
            else:
                move_id_list[line.picking_id] = [line.move_id.id]
        return move_id_list

    def filter_active_list(self, cr, uid, ids, warehouse_id, context=None):
        value = {
            'line_ids': [[6, 0, []]]
        }

        if ids:
            wizard_id = ids[0]
            wizard = self.browse(cr, uid, wizard_id)
            removed_line_ids = [x.id for x in wizard.removed_line_ids]
            value['removed_line_ids'] = [[4, x, False] for x in removed_line_ids]

            domain = [('wizard_id', '=', wizard_id), ('id', 'not in', removed_line_ids)]
            active_line_ids = self.pool.get('internal.shipment.partial.line').search(cr, uid, domain)
            if warehouse_id and active_line_ids:
                cr.execute('''
                    select pl.id
                    from internal_shipment_partial_line pl
                        left join stock_move sm on pl.move_id = sm.id
                        left join stock_location sl on sm.location_id = sl.id
                    where sl.warehouse_id = %s and pl.id in %s
                ''', (warehouse_id, tuple(active_line_ids), ))
                sql_res = cr.fetchall() or []
                active_line_ids = [x[0] for x in sql_res]

            if active_line_ids:
                value['line_ids'] = [[4, x, False] for x in active_line_ids]

        return {'value': value}

    def move_lines(self, cr, uid, ids, warehouse_id, lines, context=None):
        line_obj = self.pool.get('internal.shipment.partial.line')
        line_ids = []
        for line in lines or []:
            if line[0] == 4:
                line_ids.append(line[1])
            elif line[0] == 6:
                line_ids += line[2]

        line_obj.move_lines(cr, uid, line_ids)

        res = self.filter_active_list(cr, uid, ids, warehouse_id, context=context)
        return res

internal_shipment_partial()


class internal_shipment(osv.osv):
    _name = "internal.shipment"

    def _get_move_ids_string(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            res = dict.fromkeys(ids, '')
            cr.execute("""
                select sh.id,
                    array_to_string(
                        array_agg(
                            concat(
                                pp.default_code
                                , case when rs.id is null then '' else concat('/', rs.name) end
                                , '(', sm.product_qty ,')'
                            )
                        )
                    ,', ')
                from internal_shipment sh
                    left join internal_shipment_move ss on ss.shipment_id = sh.id
                    left join stock_move sm on sm.id = ss.move_id
                    left join product_product pp on sm.product_id = pp.id
                    left join ring_size rs on rs.id = sm.size_id
                where 1=1
                    and sh.id in %s
                group by sh.id
                having count(sm.id) > 0
            """, (tuple(ids), ))
            sql_res = cr.fetchall() or []
            for line in sql_res:
                res[line[0]] = line[1]

        return res

    def _get_sm_id(self, cr, uid, ids, name, args, context=None):
        sm_obj = self.pool.get('stock.move')
        res = {}

        for internal_shipment_row in self.browse(cr, uid, ids):
            sm_id = internal_shipment_row.move_ids or \
                    sm_obj.search(cr, uid, ['|', ('picking_id', '=', internal_shipment_row.picking_id.id),
                                            ('set_parent_order_id', '=', internal_shipment_row.picking_id.id)])
            res[internal_shipment_row.id] = sm_id

        return res

    def _get_products_search(self, cr, uid, obj, name, args, context=None):
        product_id = None
        if args and args[0] and args[0][0] and args[0][0] == 'product_id' and args[0][2]:
            product_id = args[0][2]

        si_ids = []
        if product_id:
            cr.execute("""
                SELECT distinct si.id
                FROM internal_shipment as si
                    LEFT JOIN stock_picking as sp on si.picking_id = sp.id
                    LEFT JOIN internal_shipment_move as im on im.shipment_id = si.id
                    LEFT JOIN stock_move as sm on
                        sm.picking_id = sp.id
                        and (1 = case when im.move_id is null then 1 else 0 end
                            or
                            sm.id = im.move_id
                        )
                    left join product_product pp on sm.product_id = pp.id
                WHERE 1=1
                    and pp.id = %s
            """, (product_id,)
            )
            sql_res = cr.fetchall()
            if sql_res:
                si_ids = [x[0] for x in sql_res]

        res = [('id', 'in', si_ids)]

        return res

    def _auto_init(self, cursor, context=None):
        res = super(internal_shipment, self)._auto_init(cursor, context=context)
        cursor.execute('SELECT indexname \
                FROM pg_indexes \
                WHERE indexname = \'receive\'')
        stock_move_set_parent_order_id_idx = cursor.fetchone()
        if not stock_move_set_parent_order_id_idx:
            cursor.execute('CREATE UNIQUE INDEX receive ON internal_shipment (picking_id) WHERE (state=\'ship\');')
        return res

    def _list_actual_warehouses(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for ship in self.browse(cr, uid, ids, context=context):
            r = []
            if ship.picking_id and ship.picking_id.move_lines:
                for move in ship.picking_id.move_lines:
                    if move.location_id and move.location_id.warehouse_id:
                        r.append(move.location_id.warehouse_id.name)
            result[ship.id] = ', '.join(list(set(r)))
        return result

    _columns = {
        'name': fields.char('Order#', size=256,),
        'picking_id': fields.many2one('stock.picking', 'Delivery Order', readonly=True,),
        'ship_user': fields.many2one('res.users', 'Shipping user', readonly=True,),
        'receive_user': fields.many2one('res.users', 'Receiving user', readonly=True,),
        'ship_date': fields.datetime('Shipping Date', readonly=True,),
        'ship_date_from': fields.function(lambda *a, **k: {}, method=True, type='date', string='Date from'),
        'ship_date_to': fields.function(lambda *a, **k: {}, method=True, type='date', string='Date to'),
        'receive_date': fields.datetime('Receiving Date', readonly=True,),
        'wh_from': fields.many2one('stock.warehouse', 'From Warehouse', domain="[('virtual', '=', False)]"),
        'wh_from_actual': fields.function(_list_actual_warehouses, method=True, type='char', string="Actual From WH", size=250, ),
        'wh_to': fields.many2one('stock.warehouse', 'To Warehouse'),
        'state': fields.selection([
            ('draft', 'Start'),
            ('ship', 'Shipping'),
            ('scanned', 'Scanned'),
            ('done', 'Received'),
        ], 'State', readonly=True, select=True, required=True,),
        'customer': fields.related('picking_id', 'real_partner_id', type='many2one', relation='res.partner', string='Customer', readonly=True,),
        'action': fields.selection([
            ('ship', 'Ship'),
            ('receive', 'Receive'),
        ], 'Select an action', select=True, required=True,),
        'notes': fields.text('Notes',),
        'move_ids': fields.many2many(
            'stock.move', 'internal_shipment_move', 'shipment_id', 'move_id',
            string='Product Moves', readonly=True,),
        'move_ids_view': fields.function(_get_move_ids_string, string="Product Moves", type="text", method=True, store=False, ),
        'product_id': fields.function(lambda *a, **k: {}, fnct_search=_get_products_search, method=True, type='many2one', relation="product.product", string="Product"),
        'receiving_line_ids': fields.one2many('internal.shipment.receive.line', 'shipment_id', 'Products to Receive'),
        'receiving_location_id': fields.many2one('stock.location', 'Location to Receive'),
        'lot_stock_id': fields.many2one('stock.location', 'Stock Location'),
        'sm_id': fields.function(_get_sm_id, string="Stock Move", type='many2one', obj='stock.move', method=True, store=False),
        'sm_export_ref_ca': fields.related('sm_id', 'export_ref_ca', string="Export REF CA", type='text'),
        'sm_export_ref_us': fields.related('sm_id', 'export_ref_us', string="Export REF US", type='text'),
        'sm_fc_invoice': fields.related('sm_id', 'fc_invoice', string="FC Invoice", type='text'),
        'sm_fc_order': fields.related('sm_id', 'fc_order', string="FC Order", type='text'),
        'sm_del_invoice': fields.related('sm_id', 'del_invoice', string="DEL Invoice", type='text'),
        'sm_del_order': fields.related('sm_id', 'del_order', string="DEL Order", type='text'),
        'sm_bill_of_lading': fields.related('sm_id', 'bill_of_lading', string="BILL OF LADING", type='text'),
    }

    _defaults = {
        'state': 'draft',
    }

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        def is_default_list(data):
            if (len(data) != 1) or (type(data[0]) == str) or (data[0][0] != 'state'):
                return False
            if type(data[0][2]) == list and all(x in ['ship', 'done'] for x in data[0][2]):
                return True
            return False

        if args and (not is_default_list(args)):
            return super(internal_shipment, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []

    def _get_received_stock(self, cr, uid, order):
        res = {}
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            return res
        params = {'invoiceno': order.sale_id.po_number}
        query = """SELECT
                concat(id_delmar, '/', size),
                sum(adjusted_qty)
            FROM transactions
            WHERE invoiceno = %(invoiceno)s
                AND adjusted_qty > 0
                AND transactioncode = 'REC'
            GROUP BY id_delmar, size
        """
        query_res = server_data.make_query(
            cr, uid, server_id,
            query, params=params,
            select=True, commit=False
        ) or []
        for line in query_res:
            res[line[0]] = line[1]
        return res

    def _add_receiving_line(self, cr, uid, move_data, received_qty, shipment):
        warehouse = shipment.wh_to
        wh_name = warehouse.name
        location = warehouse and warehouse.lot_release_id
        bin_obj = self.pool.get('stock.bin')
        itemid = move_data['itemid']
        bin_names = bin_obj.get_mssql_bin_for_product(cr, uid, itemid, wh_name)
        bin_name = bin_names.get(itemid)
        line_obj = self.pool.get('internal.shipment.receive.line')
        qty_to_receive = move_data['qty'] - received_qty
        line_id = line_obj.create(cr, uid, {
            'move_id': move_data['move_id'],
            'product_id': move_data['product_id'],
            'size_id': move_data['size_id'],
            'shipment_id': shipment.id,
            'received_qty': received_qty,
            'receive_qty': qty_to_receive,
            'set_qty_multiplier': move_data['set_qty_multiplier'],
            'received': False,
            'location_id': location.id,
            'bin': bin_name,
        })
        return line_id

    def _get_order_moves(self, cr, uid, order):
        result = []
        prod_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        for move in order.move_lines:
            parent_id = move.product_id.id
            size_id = move.size_id and move.size_id.id
            size = size_obj.get_4digit_name(cr, uid, size_id)
            qty = move.product_qty
            is_set = move.is_set
            components = prod_obj.split_sets_to_components(cr, uid, parent_id,
                                                           size)
            for component in components:
                itemid = component['default_code'] + '/' + component['size']
                result.append({
                    'itemid': itemid,
                    'qty': component['qty'] * qty,
                    'move_id': move.id,
                    'product_id': component['product_id'],
                    'size_id': False if size == '0000' else size_id,
                    'set_qty_multiplier': component['qty'],
                    'is_set': is_set
                })
        return result

    def _get_not_received_items(self, cr, uid, order, shipment):
        received_items = self._get_received_stock(cr, uid, order)
        result = []
        for move in self._get_order_moves(cr, uid, order):
            if move['qty'] <= 0:
                continue
            itemid = move['itemid']
            if itemid not in received_items:
                received_items[itemid] = 0
            received_qty = received_items[itemid]
            if move['qty'] <= received_qty:
                received_items[itemid] -= move['qty']
                continue
            #if order.state == 'done' and move['is_set']:
            #    continue
            new_line_id = self._add_receiving_line(cr, uid, move, received_qty,
                                                   shipment)
            received_items[itemid] = 0
            result.append(new_line_id)
        return result

    def _add_receiving_transaction(self, cr, uid, line, receive_qty, bin_name):
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        order = line.move_id.picking_id
        size_id = line.size_id and line.size_id.id
        size = self.pool.get('ring.size').get_4digit_name(cr, uid, size_id)
        warehouse, stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, line.location_id.id
        )
        product = line.product_id
        unit_price = line.move_id and line.move_id.sale_line_id and line.move_id.sale_line_id.price_unit or 0
        params = {
            'transactioncode': 'REC',
            'qty': receive_qty,
            'warehouse': warehouse,
            'itemdescription': product.name,
            'bin': bin_name or line.bin,
            'stockid': stockid,
            'id_delmar': product.default_code,
            'sign': 1,
            'size': size,
            'customerid': order.real_partner_id and order.real_partner_id.ref,
            'invoiceno': order.sale_id.po_number,
            'unit_price': -unit_price,
            'export_ref_ca': line.move_id.export_ref_ca or None,
            'export_ref_us': line.move_id.export_ref_us or None,
            'fc_invoice': line.move_id.fc_invoice or None,
            'fc_order': line.move_id.fc_order or None,
            'del_invoice': line.move_id.del_invoice or None,
            'del_order': line.move_id.del_order or None,
            'bill_of_lading': line.move_id.bill_of_lading or None,
        }
        list_params, context = move_obj.prepare_transaction_params(cr, uid, params, server_id=server_id)
        if not list_params:
            return False
        move_obj.write_into_transactions_table(cr, uid, server_id, list_params, context=context)
        return True

    def receive_stock(self, cr, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        shipment = self.browse(cr, uid, ids[0], context=context)
        bin_create_obj = self.pool.get('stock.bin.create')
        result_log = ''
        if not shipment.receiving_line_ids:
            raise osv.except_osv(_('Warning'), 'Nothing to Receive')
        for line in shipment.receiving_line_ids:
            product = '{0}{1}'.format(
                line.product_id.default_code,
                line.size_id and '/' + line.size_id.name or ''
            )
            if line.received:
                continue
            location = line.location_id
            if not location:
                result_log += '\n{}: Not received, no location'.format(product)
                continue
            max_receive_qty = line.move_id.product_qty * line.set_qty_multiplier
            max_receive_qty -= line.received_qty
            receive_qty = min(max_receive_qty, line.receive_qty)
            if receive_qty <= 0:
                result_log += '\n{}: Not received, qty to receive = 0'.format(
                    product
                )
                continue
            bin_name = line.bin
            if not bin_name:
                prod_id = line.product_id.id
                size_id = line.size_id.id
                bin_name = bin_create_obj.create_bin(cr, uid, location.id,
                                                     location.name, prod_id,
                                                     size_id, receive_qty)
                if not bin_name:
                    line_log = '\n{}: Not received, cannot create a bin'
                    result_log += line_log.format(
                        product
                    )
                    continue

            self._add_receiving_transaction(cr, uid, line, receive_qty,
                                            bin_name)
            line.write({
                'received': True,
                'receive_qty': receive_qty,
                'received_qty': line.received_qty + receive_qty,
                'bin': bin_name,
            })
            result_log += '\n{product}: Received qty = {qty}'.format(
                product=product, qty=receive_qty,
            )
        if not result_log:
            raise osv.except_osv(_('Warning'), 'Nothing to Receive')
        shipment.write({'notes': shipment.notes + result_log})
        return True

    def scan_order(self, cr, uid, ids, context=None):
        picking_obj = self.pool.get('stock.picking')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        shipment = self.browse(cr, uid, ids[0], context=context)
        # Get order
        search_str = shipment.name or ''
        search_str = search_str.replace('\n', '').replace('\t', '').strip()
        if not search_str:
            raise osv.except_osv(_('Warning'), 'Nothing has been scanned')
        order_ids = picking_obj.search(cr, uid, [
            ('name', '=', search_str),
        ])
        #if not order_ids:
        #    order_ids = picking_obj.search(cr, uid, [
        #        ('name', '=ilike', search_str),
        #    ])
        if not order_ids:
            raise osv.except_osv(_('Warning'), 'Order not found.')

        order_id = order_ids[0]
        order = picking_obj.browse(cr, uid, order_id)

        if shipment.action == 'ship':
            duplicate_ids = self.search(cr, uid, [
                ('picking_id', '=', order_id),
                ('state', '=', 'ship'),
            ])
            if duplicate_ids:
                duplicate = self.browse(cr, uid, duplicate_ids[0])
                user = duplicate.ship_user and duplicate.ship_user.name
                date = duplicate.ship_date and duplicate.ship_date.split('.')[0]
                raise osv.except_osv(
                    _('Warning'),
                    'Order was shipped out already by %s date: %s' % (
                        user, date,
                    )
                )
            # DLMR-1183
            # if order.tracking_ref:
            #    raise osv.except_osv(
            #        _('Warning'),
            #        'Order already has tracking.'
            #    )
            if order.shipping_batch_id:
                return self.action_combined_scan(cr, uid, shipment.id, order.id)
            if len(order.move_lines) != 1:
                return self.action_partial_scan(cr, uid, shipment.id, order.id)
            self.action_ship(cr, uid, search_str, order, shipment)

        elif shipment.action == 'receive':
            self._action_receive(cr, uid, search_str, order, shipment)

        new_wh_id = shipment and shipment.wh_to and shipment.wh_to.id or False
        if new_wh_id:
            self.update_FDX_HED_for_picking(cr, uid, order_id, new_wh_id)
        return True

    def action_combined_scan(self, cr, uid, shipment_id, picking_id, context=None):
        _stock_picking = self.pool.get('stock.picking')
        _partial = self.pool.get("internal.shipment.partial")
        if context is None:
            context = {}
        scanned_orders = _stock_picking.browse(cr, uid, picking_id)
        if scanned_orders and scanned_orders.shipping_batch_id or False:
            batch_id =  scanned_orders.shipping_batch_id.id
            combined_ids = _stock_picking.search(cr, uid, [('shipping_batch_id', '=', batch_id)])
            combined_order = ', '.join([str(x) for x in combined_ids])
            partial_ids = ''

            if combined_order:
                context.update(combined_id=combined_order, shipment_id=shipment_id, picking_id=picking_id)
                partial_id = self.pool.get("internal.shipment.partial").create(
                    cr, uid, {'shipment_id': shipment_id}, context=context)

            context.update({'flags': {'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False}})
            return {
                'name': _("Combined Orders to Process"),
                'view_mode': 'form',
                'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'internal_shipment_partial_form')[1],
                'view_type': 'form',
                'res_model': 'internal.shipment.partial',
                'res_id': partial_id,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': context
            }

    def action_partial_scan(self, cr, uid, shipment_id, picking_id, context=None):
        if context is None:
            context = {}
        context.update(picking_id=picking_id, shipment_id=shipment_id, combined_id=str(picking_id))
        partial_id = self.pool.get("internal.shipment.partial").create(
            cr, uid, {'shipment_id': shipment_id}, context=context)

        context.update({'flags': {'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False}})
        return {
            'name': _("Products to Process"),
            'view_mode': 'form',
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'internal_shipment_partial_form')[1],
            'view_type': 'form',
            'res_model': 'internal.shipment.partial',
            'res_id': partial_id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': context
        }

    def action_ship_rpc(self, cr, uid, name, picking_id, shipment_id, move_ids=None, note='', context=None):
        order = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
        shipment = self.browse(cr, uid, shipment_id, context=context)
        return self.action_ship(cr, uid, name=name, order=order, shipment=shipment, move_ids=move_ids, note=note, context=context)

    def action_ship(self, cr, uid, name, order, shipment, move_ids=None, note='', context=None):
        picking_obj = self.pool.get('stock.picking')
        username = self.pool.get('res.users').read(cr, uid, uid, ['name']).get('name', uid)

        # DLMR-1183, check has set line and raise - DEPRECATED
        # DLMR-2210
        # prepare set components to be shipped
        check_lines = [x for x in move_ids] if move_ids else [x.id for x in order.move_lines]
        if order.state == 'done':
            resulted_move_ids = []
            for line in self.pool.get('stock.move').browse(cr, uid, check_lines):
                if line.is_set:
                    # raise osv.except_osv(_('Warning'), 'You cannot ship a set!') - NOT USED BECAUSE OF DLMR-2210
                    set_cmps = [set_line.id for set_line in order.set_parent_move_lines if
                                set_line.set_product_id.id == line.set_product_id.id and set_line.product_id.id != set_line.set_product_id.id]
                    resulted_move_ids.extend(set_cmps)
                resulted_move_ids.append(line.id)
            check_lines = resulted_move_ids

        # DLMR-1427, check lines with qty > 1
        qty_items = []
        qty_items_str = ''
        _logger.info("Lines to be shipped and added to note: %s" % check_lines)
        if check_lines:
            qty_ids = self.pool.get('stock.move').search(cr, uid, [('id', 'in', check_lines), ('product_qty', '>', 1)])
            for line in self.pool.get('stock.move').browse(cr, uid, qty_ids):
                qty_items.append("item {} with QTY {}".format("{}{}".format(line.product_id.default_code, line.size_id and "/{}".format(line.size_id.name) or ''), line.product_qty))
            if len(qty_items) > 0:
                qty_items_str = ": {}".format(", ".join(qty_items))
        # END DLMR-1427

        picking_obj.write(cr, uid, [order.id], {
            'report_history': (order.report_history and order.report_history + '\n' or '') + ("%s %s: Shipped from %s %s" % (
                time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                username,
                shipment.wh_from and shipment.wh_from.name or '...',
                shipment.wh_to and ('to ' + shipment.wh_to.name) or '',
            ))
        })
        self.create(cr, uid, {
            'picking_id': order.id,
            'state': 'ship',
            'name': name,
            'ship_user': uid,
            'ship_date': datetime.datetime.utcnow(),
            'wh_from': shipment.wh_from and shipment.wh_from.id or False,
            'wh_to': shipment.wh_to and shipment.wh_to.id or False,
            'move_ids': [(6, 0, check_lines)] if check_lines else None,
            'receiving_line_ids': [(6, 0, [])],
            'action': 'ship',
        })
        # DLMR-1427 - added qty's
        note += '\n Order %s has been marked as shipped to another warehouse%s' % (name, qty_items_str,)
        self.write(cr, uid, [shipment.id], {'notes': note, 'name': ''})
        return note

    def _check_received(self, cr, uid, order):
        receive_ids = self.search(cr, uid, [
            ('picking_id', '=', order.id),
            ('state', '=', 'done')
        ])
        if receive_ids:
            receive = self.browse(cr, uid, receive_ids[0])
            user = receive.receive_user and receive.receive_user.name
            date = receive.receive_date and receive.receive_date.split('.')[0]
            raise osv.except_osv(
                _('Warning'),
                'Order was received already by %s date: %s' % (user, date)
            )

    def _raise_has_tracking(self, cr, uid, history, username, order, shipment, ship_ids, params):
        picking_obj = self.pool.get('stock.picking')
        step_cr = self.pool.db.cursor()
        try:
            history += "%s %s: Received with tracking %s in %s" % (
                time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                username,
                order.tracking_ref,
                shipment.wh_to and shipment.wh_to.name or '...',
            )
            picking_obj.write(step_cr, uid, [order.id], {
                'report_history': history
            })
            self.write(step_cr, uid, ship_ids[:1], params)
            step_cr.commit()
        except Exception, e:
            raise e
        finally:
            step_cr.close()

        raise osv.except_osv(
            _('Warning'),
            "Order already has tracking. Don't ship it, put it back.",
        )

    def action_receive_rpc(self, cr, uid, name, picking_id, shipment_id):
        order = self.pool.get('stock.picking').browse(cr, uid, picking_id)
        shipment = self.browse(cr, uid, shipment_id)
        self._action_receive(cr, uid, name, order, shipment)
        return True

    def _action_receive(self, cr, uid, name, order, shipment):
        picking_obj = self.pool.get('stock.picking')
        user_obj = self.pool.get('res.users')
        username = user_obj.read(cr, uid, uid, ['name']).get('name', uid)
        ship_ids = self.search(cr, uid, [
            ('picking_id', '=', order.id),
            ('state', '=', 'ship')
        ])

        receiving_line_ids = self._get_not_received_items(cr, uid, order, shipment)
        _logger.info("Got not received lines: %s" % receiving_line_ids)

        if not ship_ids or not receiving_line_ids:
            self._check_received(cr, uid, order)
        wh_to = shipment.wh_to
        params = {
            'picking_id': order.id,
            'state': 'done',
            'receive_user': uid,
            'receive_date': datetime.datetime.utcnow(),
            'wh_to': wh_to and wh_to.id or False,
            'action': 'receive',
        }
        history = order.report_history and order.report_history + '\n' or ''
        # DLMR-1183
        # if order.tracking_ref:
        #    self._raise_has_tracking(cr, uid, history, username, order,
        #                             shipment, ship_ids, params)
        picking_obj.write(cr, uid, [order.id], {
            'report_history': history + ("%s %s: Received in %s" % (
                time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                username,
                shipment.wh_to and shipment.wh_to.name or '...',
            ))
        })
        note = 'Order %s has been marked as received' % (name,)
        if ship_ids:
            self.write(cr, uid, ship_ids[:1], params)
        else:
            params.update({
                'name': name,
                'wh_from': shipment.wh_from and shipment.wh_from.id or False,
            })
            self.create(cr, uid, params)
            note += ' without shipping from anywhere'
        lot_stock = wh_to and wh_to.lot_stock_id
        receive_loc = wh_to and wh_to.lot_release_id
        self.write(cr, uid, [shipment.id], {
            'notes': note,
            'name': '',
            'receiving_line_ids': [[6, 0, receiving_line_ids]],
            'lot_stock_id': lot_stock and lot_stock.id or False,
            'receiving_location_id': receive_loc and receive_loc.id or False,
            'state': 'scanned',
            'picking_id': order.id,
        })

    def change_receive_location(self, cr, uid, ids, receiving_location_id, receiving_line_ids, context=None):
        value = {
            'receiving_line_ids': [(1, line_id[1], {
                'location_id': receiving_location_id
            }) for line_id in receiving_line_ids if receiving_location_id]
        }
        return {'value': value}

    def update_FDX_HED_for_picking(self, cr, uid, picking_id, new_wh_id):
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id)
        new_wh = self.pool.get('stock.warehouse').browse(cr, uid, new_wh_id)
        server_data = self.pool.get('fetchdb.server')
        server_settings = picking.get_shipping_server_id()
        server_id = server_settings.get(picking.id, False)
        sql = 'SELECT WhLocation FROM dbo.FDX_HED WHERE BJ_NO = ?'
        fdx_hed_data = server_data.make_query(cr, uid, server_id, sql, params=[picking.name], select=True)
        fdx_hed_WhLocation = fdx_hed_data and fdx_hed_data[0] and fdx_hed_data[0][0] or False
        if fdx_hed_WhLocation and fdx_hed_WhLocation != new_wh.name:
            query = 'UPDATE dbo.FDX_HED SET WhLocation = ? WHERE BJ_NO = ?'
            params = [new_wh.name, picking.name]
            server_data.make_query(cr, uid, server_id, query, params=params, select=False, commit=True)
        else:
            picking.write({'warehouses': new_wh.id})
        picking.set_order_params({'wh_to_from_internal_shipment': new_wh.id})
        return True

    def print_qr_item_label_ca_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_shr_label_wiz(self, cr, uid, ids, context=None):
        return self.print_label_wiz(cr, uid, ids, template='ShowRoomLabel', context=context)

    def print_smr_label_wiz(self, cr, uid, ids, context=None):
        return self.print_label_wiz(cr, uid, ids, template='SampleRoomLabel', context=context)

    def print_label_wiz(self, cr, uid, ids, template=None, context=None):
        if not template:
            template = 'ShowRoomLabel'

        shipment = self.browse(cr, uid, ids[0])
        label_printer = LabelWizPrinter(self.pool, cr, uid)
        if not shipment.picking_id:
            raise osv.except_osv('Warning', 'Nothing to print.')
        return label_printer.print_packing_label(shipment.picking_id.id, template)


internal_shipment()


class internal_shipment_receive_line(osv.osv):
    _name = "internal.shipment.receive.line"

    _columns = {
        'move_id': fields.many2one('stock.move', "Move", ondelete='CASCADE'),
        'product_id': fields.many2one('product.product', "Product",
                                      ondelete='CASCADE'),
        'size_id': fields.many2one('ring.size', "Size", ondelete='CASCADE'),
        'shipment_id': fields.many2one('internal.shipment',
                                       string="Internal Shipment",
                                       ondelete='CASCADE'),
        'receive_qty': fields.float('Qty to Receive'),
        'received_qty': fields.float('Received Qty'),
        'set_qty_multiplier': fields.float('Components Qty in the Parent Set'),
        'received': fields.boolean('Received'),
        'location_id': fields.many2one('stock.location', string="Location",
                                       ondelete='CASCADE'),
        'bin': fields.char('Bin', size=256),
    }

    def print_qr_item_label_ca_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_shr_label_wiz(self, cr, uid, ids, context=None):
        return self.print_label_wiz(cr, uid, ids, template='ShowRoomLabel', context=context)

    def print_smr_label_wiz(self, cr, uid, ids, context=None):
        return self.print_label_wiz(cr, uid, ids, template='SampleRoomLabel', context=context)

    def print_label_wiz(self, cr, uid, ids, template=None, context=None):
        def _get_cafer_bin_name(cr, uid, line):
            """Check if line' size, bin and warehouse refers to CAFER.
            :param line - stock_move line.
            :return True if line belongs to CAFER."""
            import re
            bin_ids = []
            # if (line.product_id.sizeable or line.product_id.ring_size_ids) and line.size_id:
            bin_ids = list(self.pool.get('product.product').get_mssql_product_bins(cr, uid,
                                                                                   ids=[line.product_id.id],
                                                                                   loc_ids=[],
                                                                                   size_ids=[line.size_id.id],
                                                                                   context={'warehouse': 'CAFER'}
                                                                                   ))
            bin_names = filter(lambda x: re.compile('^\d{1,2}-[A-Z]-\d{1,2}$').match(x),
                               map(lambda x: x.name, self.pool.get('stock.bin').browse(cr, uid, bin_ids)))
            return bin_names and bin_names[0]

        if not template:
            template = 'ShowRoomLabel'

        line = self.browse(cr, uid, ids[0])
        user_name = self.pool.get('res.users').read(cr, uid, uid, ['name'])['name']
        label_printer = LabelWizPrinter(self.pool, cr, uid)
        return label_printer.print_label(line.product_id.id,
                                         line.size_id and line.size_id.id,
                                         template,
                                         force={
                                            'bin_name': line.bin,
                                            'cafer_bin_id': _get_cafer_bin_name(cr, uid, line),
                                            'user_name': user_name,
                                         })

internal_shipment_receive_line()
