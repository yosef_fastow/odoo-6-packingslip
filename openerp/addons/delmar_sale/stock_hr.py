from osv import fields, osv
from tools.translate import _

class hr_employee_category(osv.osv):
    _inherit = 'hr.employee.category'
    _category_already_exists_msg = 'This category already exists'

    def copy_data(self, cr, uid, id, default=None, context=None):
        result = osv.osv.copy_data(self, cr, uid, id, default, context)
        result['name'] += u' Duplicate'
        return result

    def write(self, cr, user, ids, vals, context=None):
        name = vals['name'].strip()

        parent_id = self.browse(cr, user, ids[0]).parent_id.id
        if not parent_id:
            parent_id = None

        search_ids = self.search(cr, user, [('name', '=', name), ('parent_id', '=', parent_id)])
        if search_ids:
            raise osv.except_osv(_('Warning!'), self._category_already_exists_msg)
        else:
            return osv.osv.write(self, cr, user, ids, vals, context)

    def create(self, cr, user, vals, context=None):
        name = vals['name'].strip()
        parent_id = vals['parent_id']
        if not parent_id:
            parent_id = None

        search_ids = self.search(cr, user, [('name', '=', name), ('parent_id', '=', parent_id)])
        if search_ids:
            raise osv.except_osv(_('Warning!'), self._category_already_exists_msg)
        else:
            return osv.osv.create(self, cr, user, vals, context)

hr_employee_category()