from osv import fields, osv
import logging

_logger = logging.getLogger(__name__)


class MoveUpdate(object):
    sync_id = None
    done_erp = False
    done_trn = False
    sm_ids = []
    invoice_numbers = []
    fc_invoice = None
    fc_order = None
    del_invoice = None
    del_order = None


class InvoiceSync(osv.osv_abstract):
    _name = "invoice.sync"

    # Queries
    query_unsynced = """
        SELECT * FROM dbo.Accpac_order_invoice
        WHERE imported_erp=%(erp_sign)s OR imported_trn=%(trn_sign)s
    """
    query_invoice_no = """
        SELECT 
            LTRIM(substring(ACCPAC_NO, 2, 8)) as acc, 
            invoiceNO
        FROM gmotoc.Invoice_Header 
        WHERE ACCPAC_NO IN {}
    """
    query_moves = """
        SELECT sm.id, sp.invoice_no FROM stock_move sm
        LEFT JOIN stock_picking sp ON (sp.id=sm.picking_id OR sp.id=sm.set_parent_order_id)
        WHERE sp.invoice_no IN {}
    """
    query_complete_line_for_both = """
        UPDATE dbo.Accpac_order_invoice
        SET imported_erp=1, imported_trn=1 WHERE id={}
    """
    query_complete_line_for_erp = """
        UPDATE dbo.Accpac_order_invoice
        SET imported_erp=1 WHERE id={}
    """
    query_complete_line_for_trn = """
        UPDATE dbo.Accpac_order_invoice
        SET imported_trn=1 WHERE id={}
    """

    def get_unsynced(self, cr, uid, context=None):
        server_obj, server_id = self.invoice_server(cr, uid, context)
        params = {'erp_sign': 0, 'trn_sign': 0}
        return server_obj.make_query(cr, uid, server_id, self.query_unsynced, params=params, select=True, commit=False)

    def get_mssql_invoices(self, cr, uid, orders, context=None):
        server_obj, server_id = self.invoice_server(cr, uid, context)
        query = self.query_invoice_no.format("('{}')".format("\',\'".join(map(lambda x: "O{: >8}".format(x), orders))))
        return server_obj.make_query(cr, uid, server_id, query, params={}, select=True, commit=False)

    def prepare_fc(self, cr, uid, unsynced_fc=None):
        if not unsynced_fc:
            return []
        update_objects = []
        for sync_data in unsynced_fc:
            move_ids = self.pool.get('stock.move').search(cr, uid, [('fc_invoice', '=', sync_data.get('invoice'))])
            update_obj = MoveUpdate()
            update_obj.sync_id = sync_data.get('id')
            update_obj.fc_order = sync_data.get('order')
            update_obj.fc_invoice = sync_data.get('invoice')
            update_obj.sm_ids = move_ids or []
            #update_obj.invoice_numbers = order_invoices.get(sync_data.get('order'))
            update_obj.done_erp = sync_data.get('imported_erp', False)
            update_obj.done_trn = sync_data.get('imported_trn', False)
            update_objects.append(update_obj)
        _logger.info("List of FC update objects: %s" % update_objects)
        # return a list of update objects
        return update_objects

    def prepare_del(self, cr, uid, unsynced_del=None, orders_del=None):
        if not (unsynced_del and orders_del):
            return []
        order_invoices = {}
        # get a dict of invoices in format: {'del_order': [list of invoice_no]}
        for x in self.get_mssql_invoices(cr, uid, orders_del):
            if x[0] not in order_invoices:
                order_invoices.update({x[0]: []})
            order_invoices.get(x[0]).append(x[1])
        # get a dict of move_ids in format: {'invoice_no': [list of move ids]}
        if not order_invoices:
            return []
        move_invoices = {}
        query = self.query_moves.format("('{}')".format("\',\'".join([str(y) for x in order_invoices.values() for y in x])))
        cr.execute(query)
        for move_row in cr.dictfetchall():
            if move_row.get('invoice_no') not in move_invoices:
                move_invoices.update({move_row.get('invoice_no'): []})
            move_invoices.get(move_row.get('invoice_no')).append(move_row.get('id'))
        # generate a list of move ids for del_order
        order_moves = {}
        for order in order_invoices:
            for invoice in order_invoices[order]:
                if invoice in move_invoices:
                    if order not in order_moves:
                        order_moves.update({order: []})
                    for move_id in move_invoices.get(invoice):
                        order_moves.get(order).append(move_id)
        _logger.info("List of moves for del_order: %s" % order_moves)
        # generate list to update
        update_objects = []
        for sync_data in unsynced_del:
            if sync_data.get('order') in order_moves:
                update_obj = MoveUpdate()
                update_obj.sync_id = sync_data.get('id')
                update_obj.del_order = sync_data.get('order')
                update_obj.del_invoice = sync_data.get('invoice')
                update_obj.sm_ids = order_moves.get(sync_data.get('order'), [])
                update_obj.invoice_numbers = order_invoices.get(sync_data.get('order'))
                update_obj.done_erp = sync_data.get('imported_erp', False)
                update_obj.done_trn = sync_data.get('imported_trn', False)
                update_objects.append(update_obj)
        _logger.info("List of DEL update objects: %s" % update_objects)
        # return a list of update objects
        return update_objects

    def sync(self, cr, uid, context=None):
        mssql_keys = ['cmp', 'order', 'invoice', 'id', 'imported_erp', 'imported_trn']
        mssql_data = self.get_unsynced(cr, uid)
        # generate lists of different invoice types
        unsynced_fc, unsynced_del, invoices_fc, orders_del = [], [], [], []
        # iterate mssql data
        for rec in mssql_data:
            line = dict(zip(mssql_keys, list(rec)))
            if line.get('cmp') == 'DIAMOND':
                unsynced_fc.append(line)
            elif line.get('cmp') == 'DELMARM':
                unsynced_del.append(line)
                orders_del.append(line.get('order', None))

        # operating FC
        fc_update_objects = self.prepare_fc(cr, uid, unsynced_fc)
        # operating DEL
        del_update_objects = self.prepare_del(cr, uid, unsynced_del, orders_del)

        # update records
        server_obj, server_id = self.invoice_server(cr, uid, context)
        for data in del_update_objects+fc_update_objects:
            _logger.info("Data to process: %s" % data.__dict__)
            # sync line
            erp_synced, trn_synced = False, False
            if not data.done_erp:
                erp_synced = self.sync_to_erp(cr, uid, obj=data)
            if not data.done_trn:
                trn_synced = self.sync_to_trn(cr, uid, obj=data)
            # confirm that line was synced
            if erp_synced and trn_synced:
                _logger.info("Synced to ERP and TRN")
                server_obj.make_query(cr, uid, server_id,
                                      self.query_complete_line_for_both.format(data.sync_id),
                                      params={}, select=False, commit=True)
            elif erp_synced:
                _logger.info("Synced to ERP")
                server_obj.make_query(cr, uid, server_id,
                                      self.query_complete_line_for_erp.format(data.sync_id),
                                      params={}, select=False, commit=True)
            elif trn_synced:
                _logger.info("Synced to TRN")
                server_obj.make_query(cr, uid, server_id,
                                      self.query_complete_line_for_trn.format(data.sync_id),
                                      params={}, select=False, commit=True)
        # return true after all done
        return True

    def sync_to_erp(self, cr, uid, obj=None, context=None):
        _logger.info("Run sync for ERP MOVES records")
        if obj.done_erp:
            return True
        try:
            # prepare objects for update
            sm_vals = {}
            if obj.fc_invoice:
                sm_vals.update({'fc_invoice': obj.fc_invoice})
            if obj.fc_order:
                sm_vals.update({'fc_order': obj.fc_order})
            if obj.del_invoice:
                sm_vals.update({'del_invoice': obj.del_invoice})
            if obj.del_order:
                sm_vals.update({'del_order': obj.del_order})
            # update move lines
            self.pool.get('stock.move').write(cr, uid, obj.sm_ids, sm_vals)
        except:
            pass
            _logger.warn("Line was not synced to ERP! sm_ids: %s, sync_line_id: %s" % (obj.sm_ids, obj.sync_id))
            return False
        return True

    def sync_to_trn(self, cr, uid, obj=None, context=None):
        _logger.info("Run sync for TRANSACTIONS records")
        if obj.done_trn:
            return True
        try:
            # prepare objects an data
            tr_context = {}
            server_obj, server_id = self.invoice_server(cr, uid)
            if obj.fc_invoice:
                tr_context.update({'fc_invoice': obj.fc_invoice})
            if obj.fc_order:
                tr_context.update({'fc_order': obj.fc_order})
            if obj.del_invoice:
                tr_context.update({'del_invoice': obj.del_invoice})
            if obj.del_order:
                tr_context.update({'del_order': obj.del_order})
            # iterate on move ids
            for record in self.pool.get('stock.move').browse(cr, uid, obj.sm_ids):
                self.pool.get('stock.move').update_transactions_table(cr, uid, server_id, server_obj, record=record, warehouse=record.warehouse.name, context=tr_context)
        except Exception as e:
            pass
            _logger.warn("Line was not synced to TRN! sm_ids: %s, sync_line_id: %s, error: %s" % (obj.sm_ids, obj.sync_id, e.message))
            return False
        return True

    def invoice_server(self, cr, uid, context=None):
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_invoice_servers(cr, uid, single=True)
        return server_obj, server_id


InvoiceSync()
