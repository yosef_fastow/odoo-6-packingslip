# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2013
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import cups
import datetime
import string
import os
import time
from tempfile import mkstemp
import base64
from osv import fields, osv
from tools.translate import _
from openerp.addons.delmar_sale.report.packing_list_batch_extraction import packing_export

class product_picking(osv.osv):
    _name = "product.picking"

    _rec_name = "product_id"

    def _get_orders(self, cr, uid, ids, name, args, context=None):
        res = {}
        for pp_id in ids:
            orders = []
            moves = self.browse(cr, uid, pp_id).line_ids
            for move in moves:
                order_name = move.picking_id.name

                if order_name and order_name not in orders:
                    orders.append(order_name)

            res[pp_id] = ", ".join(orders)

        return res
        

    _columns = {
        'create_date': fields.datetime('Creation date', readonly=True),
        'product_id': fields.many2one('product.product', 'Product',select=True),
        'size_id': fields.many2one('ring.size', 'Ring size',select=True),
        'product_qty': fields.float('Qty'),
        'state': fields.selection([('start', 'Start'), ('pick', 'Pick'), ('done', 'Done')], 'State', select=True, required=True, readonly=True),
        'line_ids': fields.one2many('stock.move', 'product_picking_id', 'Stock moves'),
        'orders': fields.function(_get_orders, string="Order ##", type="text", method=True),
        'wh_id': fields.many2one('stock.warehouse', 'Warehouse',select=True),
        'product_picking_list_id': fields.many2one('product.picking.list', 'Product Picking Lists'),
        'bin_id': fields.many2one('stock.bin', 'Bin',select=True),
        'assigned': fields.boolean('Assigned'),
        'username': fields.char('User Name', size=128),
        'pdf_ids': fields.one2many('export.picking', 'product_picking_id', 'Export picking'),
        'category': fields.char('Category', size=50,select=True),
    }

    _defaults = {
        'assigned': False,
        'state': 'start',
        'product_qty': 0,
    }

    _order = "create_date desc"

    #cron get product.picking orders
    def product_pick(self, cr, uid, ids, context=None):

        product_picking_obj = self.pool.get('product.picking')
        picking_obj=self.pool.get('stock.picking')
        move_obj= self.pool.get('stock.move')
        picking_ids = picking_obj.search(cr, uid, [('state', '=', 'assigned')])

        if picking_ids:
            move_ids=move_obj.search(cr, uid, [('picking_id', 'in',picking_ids)])
            for move in move_obj.browse(cr, uid, move_ids):
                is_321=False
                is_resize=False
                if not move.product_picking_id and move.state=='assigned' and (move.warehouse.name=='USCHP' or move.warehouse.name=='CAFER' or move.warehouse.name=='CAMTL' or move.warehouse.name=='CAFERNDP' or move.warehouse.name=='CAMTLSR'):
                    wh_id = move.warehouse.id
                    country_code=move.picking_id.address_id.country_id.code
                    customer_321=move.picking_id.real_partner_id.o321
                    if move.sale_line_id and move.size_id and move.product_id and (move.product_id != move.sale_line_id.product_id or move.size_id != move.sale_line_id.size_id) and not move.is_update_order and not move.is_no_resize:
                        is_resize=True
                    if country_code != 'CA' and move.picking_id.address_grp_price<800 and customer_321 and move.picking_id.total_price<800:
                        is_321=True
                    if is_resize:
                        product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', '=', move.product_id.id), ('size_id', '=', move.size_id.id), ('state', '=', 'start'), ('wh_id', '=', wh_id), ('category', '=', 'resize')])
                        if product_picking_ids:
                            product_picking = product_picking_obj.browse(cr, uid, product_picking_ids[0])
                            move_obj.write(cr, uid, [move.id], {'product_picking_id': product_picking.id})
                            product_picking_obj.write(cr, uid, [product_picking.id], {
                                'product_qty': product_picking.product_qty + move.product_qty,})
                        else:
                            if move.product_id.id and move.product_qty:
                                new_product_picking_id = product_picking_obj.create(cr, uid, {
                                    'product_id': move.product_id.id,
                                    'state': 'start',
                                    'product_qty': move.product_qty,
                                    'wh_id': wh_id,
                                    'bin_id': move.bin_id.id,
                                    'size_id': move.size_id.id,
                                    'category':'resize',
                                })
                                move_obj.write(cr, uid, [move.id], {'product_picking_id': new_product_picking_id})
                    elif country_code =='CA':
                        product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', '=', move.product_id.id), ('size_id', '=', move.size_id.id), ('state', '=', 'start'), ('wh_id', '=', wh_id), ('category', '=', 'ca')])
                        if product_picking_ids:
                            product_picking = product_picking_obj.browse(cr, uid, product_picking_ids[0])
                            move_obj.write(cr, uid, [move.id], {'product_picking_id': product_picking.id})
                            product_picking_obj.write(cr, uid, [product_picking.id], {
                                'product_qty': product_picking.product_qty + move.product_qty,})
                        else:
                            if move.product_id.id and move.product_qty:
                                new_product_picking_id = product_picking_obj.create(cr, uid, {
                                    'product_id': move.product_id.id,
                                    'state': 'start',
                                    'product_qty': move.product_qty,
                                    'wh_id': wh_id,
                                    'bin_id': move.bin_id.id,
                                    'size_id': move.size_id.id,
                                    'category':'ca',
                                })
                                move_obj.write(cr, uid, [move.id], {'product_picking_id': new_product_picking_id})
                    elif is_321:
                        product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', '=', move.product_id.id), ('size_id', '=', move.size_id.id), ('state', '=', 'start'), ('wh_id', '=', wh_id), ('category', '=', '321')])
                        if product_picking_ids:
                            product_picking = product_picking_obj.browse(cr, uid, product_picking_ids[0])
                            move_obj.write(cr, uid, [move.id], {'product_picking_id': product_picking.id})
                            product_picking_obj.write(cr, uid, [product_picking.id], {
                                'product_qty': product_picking.product_qty + move.product_qty,})
                        else:
                            if move.product_id.id and move.product_qty:
                                new_product_picking_id = product_picking_obj.create(cr, uid, {
                                    'product_id': move.product_id.id,
                                    'state': 'start',
                                    'product_qty': move.product_qty,
                                    'wh_id': wh_id,
                                    'bin_id': move.bin_id.id,
                                    'size_id': move.size_id.id,
                                    'category':'321',
                                })
                                move_obj.write(cr, uid, [move.id], {'product_picking_id': new_product_picking_id})
                    else:
                        product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', '=', move.product_id.id), ('size_id', '=', move.size_id.id), ('state', '=', 'start'), ('wh_id', '=', wh_id), ('category', '=', 'paps')])
                        if product_picking_ids:
                            product_picking = product_picking_obj.browse(cr, uid, product_picking_ids[0])
                            move_obj.write(cr, uid, [move.id], {'product_picking_id': product_picking.id})
                            product_picking_obj.write(cr, uid, [product_picking.id], {
                                'product_qty': product_picking.product_qty + move.product_qty,})
                        else:
                            if move.product_id.id and move.product_qty:
                                new_product_picking_id = product_picking_obj.create(cr, uid, {
                                    'product_id': move.product_id.id,
                                    'state': 'start',
                                    'product_qty': move.product_qty,
                                    'wh_id': wh_id,
                                    'bin_id': move.bin_id.id,
                                    'size_id': move.size_id.id,
                                    'category':'paps',
                                })
                                move_obj.write(cr, uid, [move.id], {'product_picking_id': new_product_picking_id})

        cr.execute(""" delete from product_picking where product_id is null""")

        #return True
        view_object=self.pool.get('ir.ui.view')
        view_ids = view_object.search(cr, uid, [('model', '=', 'product.picking'),('type', '=', 'tree')])

        return {
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'product.picking',
            'view_id': view_ids[0],
            'type': 'ir.actions.act_window',
            'context': context,
        }

    def get_shipping_label(self, cr, uid, ids, context=None):
        ids = self.search(cr, uid, [('state', '=', 'pick')])
        export_obj = self.pool.get('picking.export')
        orders_list = []
        for picking_id in ids:
            pp_object = self.browse(cr, uid, picking_id)
            orders = pp_object.orders.split(', ')
            for order in orders:
                orders_list.append(order)
        return self.pool.get('stock.picking').get_shipping_label(cr, uid, ids, orders_list, context=context)

    #cron print pdf
    def export_pdf(self, cr, uid, ids, context=None):
        cr.execute(""" delete from product_picking where product_id is null""")
        #import pdb; pdb.set_trace()
        product_picking_obj = self.pool.get('product.picking')
        export_obj = self.pool.get('picking.export')
        move_object=self.pool.get('stock.move')
        start_time = datetime.datetime.now()
        product_picking_ids=product_picking_obj.search(cr, uid, [('state', '=','start')])

        for product_picking in product_picking_obj.browse(cr, uid, product_picking_ids):
            od_spapss0y={}
            od_spapss1y={}
            od_spapss2y={}
            od_spapss3y={}
            
            od_spapss0n={}
            od_spapss1n={}
            od_spapss2n={}
            od_spapss3n={}

            od_s321s0y={}
            od_s321s1y={}
            od_s321s2y={}
            od_s321s3y={}
            
            od_s321s0n={}
            od_s321s1n={}
            od_s321s2n={}
            od_s321s3n={}

            od_sres0y={}
            od_sres1y={}
            od_sres2y={}
            od_sres3y={}
            
            od_sres0n={}
            od_sres1n={}
            od_sres2n={}
            od_sres3n={}

            od_mpapss0y={}
            od_mpapss1y={}
            od_mpapss2y={}
            od_mpapss3y={}
            
            od_mpapss0n={}
            od_mpapss1n={}
            od_mpapss2n={}
            od_mpapss3n={}

            od_m321s0y={}
            od_m321s1y={}
            od_m321s2y={}
            od_m321s3y={}
            
            od_m321s0n={}
            od_m321s1n={}
            od_m321s2n={}
            od_m321s3n={}

            od_mres0y={}
            od_mres1y={}
            od_mres2y={}
            od_mres3y={}
            
            od_mres0n={}
            od_mres1n={}
            od_mres2n={}
            od_mres3n={}
            
            od_scas0y={}
            od_scas1y={}
            od_scas2y={}
            od_scas3y={}
            
            od_scas0n={}
            od_scas1n={}
            od_scas2n={}
            od_scas3n={}

            od_mcas0y={}
            od_mcas1y={}
            od_mcas2y={}
            od_mcas3y={}
            
            od_mcas0n={}
            od_mcas1n={}
            od_mcas2n={}
            od_mcas3n={}
            origin_id=product_picking.id
            category=product_picking.category
            for move in product_picking.line_ids:
                picking_id=move.picking_id.id
                picking_count=move_object.search_count(cr,uid,[('picking_id','=',picking_id)])
                customer_name=move.picking_id.real_partner_id.name
                report_format=move.picking_id.real_partner_id.paper_format
                print_pslip=move.picking_id.real_partner_id.print_pslip
                check1=move.picking_id not in od_s321s0y and move.picking_id not in od_s321s1y and move.picking_id not in od_s321s2y and move.picking_id not in od_s321s3y
                check2=move.picking_id not in od_s321s0n and move.picking_id not in od_s321s1n and move.picking_id not in od_s321s2n and move.picking_id not in od_s321s3n
                check3=move.picking_id not in od_m321s0y and move.picking_id not in od_m321s1y and move.picking_id not in od_m321s2y and move.picking_id not in od_m321s3y
                check4=move.picking_id not in od_m321s0n and move.picking_id not in od_m321s1n and move.picking_id not in od_m321s2n and move.picking_id not in od_m321s3n
                check5=move.picking_id not in od_spapss0y and move.picking_id not in od_spapss1y and move.picking_id not in od_spapss2y and move.picking_id not in od_spapss3y
                check6=move.picking_id not in od_spapss0n and move.picking_id not in od_spapss1n and move.picking_id not in od_spapss2n and move.picking_id not in od_spapss3n
                check7=move.picking_id not in od_mpapss0y and move.picking_id not in od_mpapss1y and move.picking_id not in od_mpapss2y and move.picking_id not in od_mpapss3y
                check8=move.picking_id not in od_mpapss0n and move.picking_id not in od_mpapss1n and move.picking_id not in od_mpapss2n and move.picking_id not in od_mpapss3n
                check9=move.picking_id not in od_sres0y and move.picking_id not in od_sres1y and move.picking_id not in od_sres2y and move.picking_id not in od_sres3y
                check10=move.picking_id not in od_sres0n and move.picking_id not in od_sres1n and move.picking_id not in od_sres2n and move.picking_id not in od_sres3n
                check11=move.picking_id not in od_mres0y and move.picking_id not in od_mres1y and move.picking_id not in od_mres2y and move.picking_id not in od_mres3y
                check12=move.picking_id not in od_mres0n and move.picking_id not in od_mres1n and move.picking_id not in od_mres2n and move.picking_id not in od_mres3n
                check13=move.picking_id not in od_scas0y and move.picking_id not in od_scas1y and move.picking_id not in od_scas2y and move.picking_id not in od_scas3y
                check14=move.picking_id not in od_scas0n and move.picking_id not in od_scas1n and move.picking_id not in od_scas2n and move.picking_id not in od_scas3n
                check15=move.picking_id not in od_mcas0y and move.picking_id not in od_mcas1y and move.picking_id not in od_mcas2y and move.picking_id not in od_mcas3y
                check16=move.picking_id not in od_mcas0n and move.picking_id not in od_mcas1n and move.picking_id not in od_mcas2n and move.picking_id not in od_mcas3n
                if category == '321':
                    if move.picking_id and move.picking_id.state == 'assigned' and check1 and check2 and check3 and check4 and check5 and check6 and check7 and check8 and check9 and check10 and check11 and check12 and check13 and check14 and check15 and check16:
                        if (picking_count == 1) and  not report_format and print_pslip:
                            od_s321s0y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and print_pslip:
                            od_s321s1y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and print_pslip:
                            od_s321s2y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and print_pslip:
                            od_s321s3y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and print_pslip:
                            od_m321s0y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and print_pslip:
                            od_m321s1y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and print_pslip:
                            od_m321s2y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and print_pslip:
                            od_m321s3y[move.picking_id.id]=customer_name
                            
                        if (picking_count == 1) and  not report_format and not print_pslip:
                            od_s321s0n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and not print_pslip:
                            od_s321s1n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and not print_pslip:
                            od_s321s2n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and not print_pslip:
                            od_s321s3n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and not print_pslip:
                            od_m321s0n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and not print_pslip:
                            od_m321s1n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and not print_pslip:
                            od_m321s2n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and not print_pslip:
                            od_m321s3n[move.picking_id.id]=customer_name
                            
                if category == 'paps':
                    if move.picking_id and move.picking_id.state == 'assigned' and check1 and check2 and check3 and check4 and check5 and check6 and check7 and check8 and check9 and check10 and check11 and check12 and check13 and check14 and check15 and check16:
                        if (picking_count == 1) and  not report_format and print_pslip:
                            od_spapss0y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and print_pslip:
                            od_spapss1y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and print_pslip:
                            od_spapss2y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and print_pslip:
                            od_spapss3y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and print_pslip:
                            od_mpapss0y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and print_pslip:
                            od_mpapss1y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and print_pslip:
                            od_mpapss2y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and print_pslip:
                            od_mpapss3y[move.picking_id.id]=customer_name
                            
                        if (picking_count == 1) and  not report_format and not print_pslip:
                            od_spapss0n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and not print_pslip:
                            od_spaps1n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and not print_pslip:
                            od_spapss2n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and not print_pslip:
                            od_spapss3n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and not print_pslip:
                            od_mpapss0n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and not print_pslip:
                            od_mpapss1n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and not print_pslip:
                            od_mpapss2n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and not print_pslip:
                            od_mpapss3n[move.picking_id.id]=customer_name
                            
                if category == 'resize':
                    if move.picking_id and move.picking_id.state == 'assigned' and check1 and check2 and check3 and check4 and check5 and check6 and check7 and check8 and check9 and check10 and check11 and check12 and check13 and check14 and check15 and check16:
                        if (picking_count == 1) and  not report_format and print_pslip:
                            od_sres0y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and print_pslip:
                            od_sres1y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and print_pslip:
                            od_sres2y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and print_pslip:
                            od_sres3y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and print_pslip:
                            od_mres0y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and print_pslip:
                            od_mres1y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and print_pslip:
                            od_mres2y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and print_pslip:
                            od_mres3y[move.picking_id.id]=customer_name
                            
                        if (picking_count == 1) and  not report_format and not print_pslip:
                            od_sres0n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and not print_pslip:
                            od_sres1n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and not print_pslip:
                            od_sres2n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and not print_pslip:
                            od_sres3n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and not print_pslip:
                            od_mres0n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and not print_pslip:
                            od_mres1n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and not print_pslip:
                            od_mres2n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and not print_pslip:
                            od_mres3n[move.picking_id.id]=customer_name
                            
                            
                if category == 'ca':
                    if move.picking_id and move.picking_id.state == 'assigned' and check1 and check2 and check3 and check4 and check5 and check6 and check7 and check8 and check9 and check10 and check11 and check12 and check13 and check14 and check15 and check16:
                        if (picking_count == 1) and  not report_format and print_pslip:
                            od_scas0y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and print_pslip:
                            od_scas1y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and print_pslip:
                            od_scas2y[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and print_pslip:
                            od_scas3y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and print_pslip:
                            od_mcas0y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and print_pslip:
                            od_mcas1y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and print_pslip:
                            od_mcas2y[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and print_pslip:
                            od_mcas3y[move.picking_id.id]=customer_name

                        if (picking_count == 1) and  not report_format and not print_pslip:
                            od_scas0n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s1' and not print_pslip:
                            od_scas1n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s2' and not print_pslip:
                            od_scas2n[move.picking_id.id]=customer_name
                        if (picking_count == 1) and  report_format=='s3' and not print_pslip:
                            od_scas3n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  not report_format and not print_pslip:
                            od_mcas0n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s1' and not print_pslip:
                            od_mcas1n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s2' and not print_pslip:
                            od_mcas2n[move.picking_id.id]=customer_name
                        if (picking_count > 1) and  report_format=='s3' and not print_pslip:
                            od_mcas3n[move.picking_id.id]=customer_name
            check23= not od_spapss0y and not od_spapss1y and not od_spapss2y and not od_spapss3y and not od_spapss0n and not od_spapss1n and not od_spapss2n and not od_spapss3n
            check24= not od_s321s0y and not od_s321s1y and not od_s321s2y and not od_s321s3y and not od_s321s0n and not od_s321s1n and not od_s321s2n and not od_s321s3n
            check25= not od_sres0y and not od_sres1y and not od_sres2y and not od_sres3y and not od_sres0n and not od_sres1n and not od_sres2n and not od_sres3n
            check26= not od_mpapss0y and not od_mpapss1y and not od_mpapss2y and not od_mpapss3y and not od_mpapss0n and not od_mpapss1n and not od_mpapss2n and not od_mpapss3n
            check27= not od_m321s0y and not od_m321s1y and not od_m321s2y and not od_m321s3y and not od_m321s0n and not od_m321s1n and not od_m321s2n and not od_m321s3n
            check28= not od_mres0y and not od_mres1y and not od_mres2y and not od_mres3y and not od_mres0n and not od_mres1n and not od_mres2n and not od_mres3n
            check29= not od_scas0y and not od_scas1y and not od_scas2y and not od_scas3y and not od_scas0n and not od_scas1n and not od_scas2n and not od_scas3n
            check30= not od_mcas0y and not od_mcas1y and not od_mcas2y and not od_mcas3y and not od_mcas0n and not od_mcas1n and not od_mcas2n and not od_mcas3n
            if  check23 and check24 and check25 and check26 and check27 and check28 and check29 and check30:
                    print 'not found any order'

            else:
                    if context is None:
                        context={}
                    export_ids=[]
                    context['print_pslip']=True
                    context['batch_report'] = 'True'
                    context['origin_id'] = origin_id
                    if od_spapss0n:

                        odspapss0n=[]
                        listofTuples = sorted(od_spapss0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='spapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss0n, context=context)
                    if od_spapss0y:

                        odspapss0y=[]
                        listofTuples = sorted(od_spapss0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='spapsy'

                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss0y, context=context)

                    if od_spapss1n:

                        odspapss1n=[]
                        listofTuples = sorted(od_spapss1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='spapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss1n, context=context)
                        
                    if od_spapss1y:

                        odspapss1y=[]
                        listofTuples = sorted(od_spapss1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='spapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss1y, context=context)


                    if od_spapss2n:

                        odspapss2n=[]
                        listofTuples = sorted(od_spapss2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='spapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss2n, context=context)
                        
                    if od_spapss2y:

                        odspapss2y=[]
                        listofTuples = sorted(od_spapss2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='spapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss2y, context=context)
                    
                    if od_spapss3n:

                        odspapss3n=[]
                        listofTuples = sorted(od_spapss3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='spapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss3n, context=context)
                        
                    if od_spapss3y:

                        odspapss3y=[]
                        listofTuples = sorted(od_spapss3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odspapss3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='spapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odspapss3y, context=context)
                    
                    if od_s321s0y:

                        ods321s0y=[]
                        listofTuples = sorted(od_s321s0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='s321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s0y, context=context)
                        
                    if od_s321s0n:

                        ods321s0n=[]
                        listofTuples = sorted(od_s321s0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='s321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s0n, context=context)
                    
                    if od_s321s1y:

                        ods321s1y=[]
                        listofTuples = sorted(od_s321s1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='s321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s1y, context=context)
                        
                    if od_s321s1n:

                        ods321s1n=[]
                        listofTuples = sorted(od_s321s1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='s321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s1n, context=context)
                    
                    if od_s321s2y:

                        ods321s2y=[]
                        listofTuples = sorted(od_s321s2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='s321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s2y, context=context)
                        
                    if od_s321s2n:

                        ods321s2n=[]
                        listofTuples = sorted(od_s321s2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='s321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s2n, context=context)
                    
                    if od_s321s3y:

                        ods321s3y=[]
                        listofTuples = sorted(od_s321s3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='s321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s3y, context=context)
                        
                    if od_s321s3n:

                        ods321s3n=[]
                        listofTuples = sorted(od_s321s3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            ods321s3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='s321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, ods321s3n, context=context)
                    
                    if od_mpapss0y:

                        odmpapss0y=[]
                        listofTuples = sorted(od_mpapss0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mpapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss0y, context=context)
                        
                    if od_mpapss0n:

                        odmpapss0n=[]
                        listofTuples = sorted(od_mpapss0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mpapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss0n, context=context)
                    
                    if od_mpapss1y:

                        odmpapss1y=[]
                        listofTuples = sorted(od_mpapss1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mpapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss1y, context=context)
                        
                    if od_mpapss1n:

                        odmpapss1n=[]
                        listofTuples = sorted(od_mpapss1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mpapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss1n, context=context)
                    
                    if od_mpapss2y:

                        odmpapss2y=[]
                        listofTuples = sorted(od_mpapss2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mpapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss2y, context=context)
                        
                    if od_mpapss2n:

                        odmpapss2n=[]
                        listofTuples = sorted(od_mpapss2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mpapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss2n, context=context)

                    if od_mpapss3y:

                        odmpapss3y=[]
                        listofTuples = sorted(od_mpapss3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mpapsy'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss3y, context=context)
                        
                    if od_mpapss3n:

                        odmpapss3n=[]
                        listofTuples = sorted(od_mpapss3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmpapss3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mpapsn'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmpapss3n, context=context)
                    
                    if od_m321s0y:

                        odm321s0y=[]
                        listofTuples = sorted(od_m321s0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='m321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s0y, context=context)
                        
                    if od_m321s0n:

                        odm321s0n=[]
                        listofTuples = sorted(od_m321s0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='m321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s0n, context=context)
                    
                    if od_m321s1y:

                        odm321s1y=[]
                        listofTuples = sorted(od_m321s1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='m321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s1y, context=context)
                        
                    if od_m321s1n:

                        odm321s1n=[]
                        listofTuples = sorted(od_m321s1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='m321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s1n, context=context)
                    
                    if od_m321s2y:

                        odm321s2y=[]
                        listofTuples = sorted(od_m321s2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='m321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s2y, context=context)
                        
                    if od_m321s2n:

                        odm321s2n=[]
                        listofTuples = sorted(od_m321s2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='m321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s2n, context=context)
                    
                    if od_m321s3y:

                        odm321s3y=[]
                        listofTuples = sorted(od_m321s3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='m321y'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s3y, context=context)
                        
                    if od_m321s3n:

                        odm321s3n=[]
                        listofTuples = sorted(od_m321s3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odm321s3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='m321n'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odm321s3n, context=context)


                    if od_mres0y:

                        odmres0y=[]
                        listofTuples = sorted(od_mres0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mrey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres0y, context=context)
                        
                    if od_mres0n:

                        odmres0n=[]
                        listofTuples = sorted(od_mres0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres0n, context=context)

                    if od_mres1y:

                        odmres1y=[]
                        listofTuples = sorted(od_mres1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mrey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres1y, context=context)
                        
                    if od_mres1n:

                        odmres1n=[]
                        listofTuples = sorted(od_mres1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres1n, context=context)

                    if od_mres2y:

                        odmres2y=[]
                        listofTuples = sorted(od_mres2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mrey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres2y, context=context)
                        
                    if od_mres2n:

                        odmres2n=[]
                        listofTuples = sorted(od_mres2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres2n, context=context)

                    if od_mres3y:

                        odmres3y=[]
                        listofTuples = sorted(od_mres3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mrey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres3y, context=context)
                        
                    if od_mres3n:

                        odmres3n=[]
                        listofTuples = sorted(od_mres3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmres3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmres3n, context=context)

                    if od_sres0y:

                        odsres0y=[]
                        listofTuples = sorted(od_sres0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='srey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres0y, context=context)
                        
                    if od_sres0n:

                        odsres0n=[]
                        listofTuples = sorted(od_sres0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='sren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres0n, context=context)

                    if od_sres1y:

                        odsres1y=[]
                        listofTuples = sorted(od_sres1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='srey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres1y, context=context)

                    if od_sres1n:

                        odsres1n=[]
                        listofTuples = sorted(od_sres1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='sren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres1n, context=context)

                    if od_sres2y:

                        odsres2y=[]
                        listofTuples = sorted(od_sres2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='srey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres2y, context=context)
                        
                    if od_sres2n:

                        odsres2n=[]
                        listofTuples = sorted(od_sres2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='sren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres2n, context=context)

                    if od_sres3y:

                        odsres3y=[]
                        listofTuples = sorted(od_sres3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='srey'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres3y, context=context)
                        
                    if od_sres3n:

                        odsres3n=[]
                        listofTuples = sorted(od_sres3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odsres3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='sren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odsres3n, context=context)

                    #listofTuples = sorted(order_id3s.items() , reverse=True, key=lambda x: x[1])
                    
                    if od_mcas0y:

                        odmcas0y=[]
                        listofTuples = sorted(od_mcas0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mcay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas0y, context=context)

                    if od_mcas0n:

                        odmcas0n=[]
                        listofTuples = sorted(od_mcas0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='mcan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas0n, context=context)

                    if od_mcas1y:

                        odmcas1y=[]
                        listofTuples = sorted(od_mcas1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mcay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas1y, context=context)

                    if od_mcas1n:

                        odmcas1n=[]
                        listofTuples = sorted(od_mcas1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='mcan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas1n, context=context)

                    if od_mcas2y:

                        odmcas2y=[]
                        listofTuples = sorted(od_mcas2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mcay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas2y, context=context)

                    if od_mcas2n:

                        odmcas2n=[]
                        listofTuples = sorted(od_mcas2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='mcan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas2n, context=context)

                    if od_mcas3y:

                        odmcas3y=[]
                        listofTuples = sorted(od_mcas3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mcay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas3y, context=context)

                    if od_mcas3n:

                        odmcas3n=[]
                        listofTuples = sorted(od_mcas3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odmcas3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='mren'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odmcas3n, context=context)

                    if od_scas0y:

                        odscas0y=[]
                        listofTuples = sorted(od_scas0y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas0y.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='scay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas0y, context=context)

                    if od_scas0n:

                        odscas0n=[]
                        listofTuples = sorted(od_scas0n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas0n.append(elem[0])

                        context['paper_format']='standard'
                        context['note']='scan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas0n, context=context)

                    if od_scas1y:

                        odscas1y=[]
                        listofTuples = sorted(od_scas1y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas1y.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='scay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas1y, context=context)

                    if od_scas1n:

                        odscas1n=[]
                        listofTuples = sorted(od_scas1n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas1n.append(elem[0])

                        context['paper_format']='s1'
                        context['note']='scan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas1n, context=context)

                    if od_scas2y:

                        odscas2y=[]
                        listofTuples = sorted(od_scas2y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas2y.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='scay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas2y, context=context)

                    if od_scas2n:

                        odscas2n=[]
                        listofTuples = sorted(od_scas2n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas2n.append(elem[0])

                        context['paper_format']='s2'
                        context['note']='scan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas2n, context=context)

                    if od_scas3y:

                        odscas3y=[]
                        listofTuples = sorted(od_scas3y.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas3y.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='scay'
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas3y, context=context)

                    if od_scas3n:

                        odscas3n=[]
                        listofTuples = sorted(od_scas3n.items() ,  key=lambda x: x[1])
                        for elem in listofTuples :
                            odscas3n.append(elem[0])

                        context['paper_format']='s3'
                        context['note']='scan'
                        context['print_pslip']=False
                        report_ids=self.pool.get('stock.picking').action_process(cr, uid, odscas3n, context=context)

                    
        product_picking_ids=product_picking_obj.search(cr, uid, [('state', '=','start')])
        for pp_id in product_picking_ids:
            moves = self.browse(cr, uid, pp_id).line_ids
            for move in moves:
                if move.state=='done' and move.picking_id.state=='picking':
                    self.write(cr,uid,[pp_id],{'state':'pick'})
                    
        view_object=self.pool.get('ir.ui.view')
        view_ids = view_object.search(cr, uid, [('model', '=', 'product.picking'),('type', '=', 'tree')])
        end_time = datetime.datetime.now()
        difference = start_time - end_time
        seconds_in_day = 24 * 60 * 60
        time_diff=divmod(difference.days * seconds_in_day + difference.seconds, 60)
        try:
            f = open("/tmp/logtime", "w+")
            f.write("start time:" +str(start_time)+"\n")
            f.write("end time:" +str(end_time)+"\n")
            f.write("difference:" +str(time_diff)+"\n")
        finally:
            f.close()
        return {
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'product.picking',
            'view_id': view_ids[0],
            'type': 'ir.actions.act_window',
            'context': context,
        }
    def stock_picking_lines(self,cr,uid,ids,context=None):
        stock_picking_obj=self.pool.get_browser('stock.picking')
        
    def get_stock_pickings_ids(self, cr, uid, ids, context=None):
        picking_ids = []
        if ids:

            cr.execute("""  SELECT DISTINCT(sm.picking_id)
                            FROM product_picking pick
                                LEFT JOIN stock_move sm ON (sm.product_picking_id = pick.id)
                            WHERE 1=1
                                AND pick.id IN %s
                                AND sm.picking_id IS NOT NULL
                """, (tuple(ids), ))
            picking_ids = [line[0] for line in cr.fetchall() if line]
        return picking_ids

    def write(self, cr, uid, ids, vals, context=None):

        res = super(product_picking, self).write(cr, uid, ids, vals, context=context)

        state = vals.get('state', False)
        if state in ('pick', 'done'):
            product_picking_ids = self.get_stock_pickings_ids(cr, uid, ids)
            if product_picking_ids:
                self.pool.get('stock.picking').write(cr, uid, product_picking_ids, {'process_by_item': state == 'pick'})

        return res

    def unlink(self, cr, uid, ids, context=None):

        stock_picking_ids = self.get_stock_pickings_ids(cr, uid, ids)

        # Unlock print-by-item ids for usual process and batches
        self.pool.get('stock.picking').write(cr, uid, stock_picking_ids, {'process_by_item': False})

        res = super(product_picking, self).unlink(cr, uid, ids, context=context)
        return res

    def action_print_product_picking(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        product_picking_list_obj = self.pool.get('product.picking.list')
        new_product_picking_list_id = product_picking_list_obj.create(cr, uid, {})
        self.write(cr, uid, context.get('active_ids', []), {
            'product_picking_list_id': new_product_picking_list_id,
            'state': 'pick',
        })

        datas = {
            'id': new_product_picking_list_id,
            'ids': [new_product_picking_list_id],
            'model': 'product.picking.list',
            'report_type': 'pdf',
        }

        act_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'product.picking.report',
            'datas': datas,
            'context': {'active_ids': [new_product_picking_list_id], 'active_id': new_product_picking_list_id}
        }

        return act_print

    def check_done(self, cr, uid, ids, context=None):
        for product_picking in self.browse(cr, uid, ids, context=context):
            if product_picking.state in ('pick',) and not [move for move in product_picking.line_ids if move.picking_id.state in ('assigned',)]:
                self.write(cr, uid, [product_picking.id], {'state': 'done'})

product_picking()


class product_picking_list(osv.osv):
    _name = "product.picking.list"

    _rec_name = "product_picking_ids"

    _columns = {
        'create_date': fields.datetime('Creation date', readonly=True),
        'product_picking_ids': fields.one2many('product.picking', 'product_picking_list_id', 'Products'),
    }

    _order = "create_date desc"

product_picking_list()


class product_picking_print(osv.osv_memory):
    _name = "product.picking.print"

    _rec_name = "product_name"
    
    def _available_printers(self, cr, uid, context=None):
        if context is None:
            context={}
        result=[]
        conn=cups.Connection()
        printers = conn.getPrinters()
        for x in printers:
            result.append((x,x))
        return result
    _columns = {
        'product_name': fields.char('Name', size=256, required=True,),
        'category': fields.selection([('321', '321'), ('paps', 'Paps'), ('resize', 'Resize'), ('ca', 'CA')], 'category', select=True,),
        'printer_std': fields.selection(_available_printers, 'Printer(std)',),
        'printer_s1': fields.selection(_available_printers, 'Printer(s1)',),
        'printer_s2': fields.selection(_available_printers, 'Printer(s2)', ),
        'printer_s3': fields.selection(_available_printers, 'Printer(s3)',),
        'printer_splb': fields.selection(_available_printers, 'Shipping Label Printer',),
        'order_ids': fields.many2many('picking.export', 'picking_export_print_rel', 'print_id', 'order_id', 'Orders'),
        'refresh': fields.boolean('Refresh'),
    }
    _defaults = {
       'category': '321',
    }
    
    def print_direct(self, cr, uid, result, format, printer):
        import pdb; pdb.set_trace()
        fd, file_name = mkstemp()
        try:
            result = base64.decodestring(result)
            os.write(fd, result)
        finally:
            os.close(fd)
        printer_system_name = ''
        if printer:
            if isinstance(printer, (basestring)):
                printer_system_name = printer
            else:
                printer_system_name = printer.system_name
            if format == 'raw':
                # -l is the same as -o raw
                cmd = "lpr -l -P %s %s" % (printer_system_name,file_name)
            else:
                cmd = "lpr -P %s %s" % (printer_system_name,file_name)
            print cmd
            #os.system(cmd)
            time.sleep(3)
            os.remove(file_name)
        return True

    def get_orders_by_prod(self, cr, uid, ids, search_product_name,search_category,pstd,ps1,ps2,ps3,splb, context=None):
        response = {}
        product_name = search_product_name
        category=search_category

        response['value'] = {
            'product_name': '',
            'order_ids': [],
        }

        warning_msg = {
            'title': _('Warning!'),
            'message': False
        }

        if product_name:
            product_name = product_name.strip().upper()

        if not product_name:
            warning_msg['message'] = _('Please input product name')
            response['warning'] = warning_msg
            return response
        if not pstd or not ps1 or not ps2 or not ps3:
            warning_msg['message'] = _('Please select printers')
            response['warning'] = warning_msg
            return response
        size_id = False
        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')

        product_names = product_obj.name_search(cr, uid, product_name, operator="=", context={'mode': 'simple'})
        if  '/' in product_name:
            prod_els = product_name.split('/')
            try:
                size_f = float(prod_els[-1])
                if size_f:
                    size_ids = size_obj.search(cr, uid, [('name', '=', str(size_f))])
                    if size_ids:
                        size_id = size_ids[0]
                product_name = "/".join(prod_els[:-1])
                product_names = product_obj.name_search(cr, uid, product_name, operator="=", context={'mode': 'simple'})
            except:
                pass

        product_ids = [prod[0] for prod in product_names]

        if not product_ids:
            warning_msg['message'] = _('Product not found.')
            response['warning'] = warning_msg
            return response

        elif len(product_ids) > 1:
            warning_msg['message'] = _('Too many products found.')
            response['warning'] = warning_msg
            return response
        export_ids=[]
        product_picking_obj = self.pool.get('product.picking')
        export_obj=self.pool.get('picking.export')

        if size_id:
            product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', 'in', product_ids),('size_id','=',size_id), ('state', '=', 'pick'),('category', '=', category)])
        else:
            product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', 'in', product_ids),('state', '=', 'pick'),('category', '=', category)])
            if product_picking_ids:
                for row in product_picking_obj.browse(cr,uid,[product_picking_ids[0]]):
                    size_id=row.size_id
                if size_id:
                    warning_msg['message'] = _('Product not found(without size).')
                    response['warning'] = warning_msg
                    return response
        if product_picking_ids:
            product_picking_id=product_picking_ids[0]
            export_picking_ids=export_obj.search(cr,uid,[('product_picking_id', '=', product_picking_id)])
            for export_picking in export_obj.browse(cr,uid,export_picking_ids):
                if export_picking.id:
                    export_ids.append(export_picking.id)
                    printer_obj = self.pool.get('printer.setting')
                    export_obj=self.pool.get('picking.export')
                    conn=cups.Connection()
                    # for obj in export_obj.browse(cr, uid,[export_picking.id]):
                    #     if obj.pdf_file and obj.print_pslip:
                    #         if obj.paper_format =='standard':
                    #             self.print_direct(cr, uid,obj.pdf_file,'', pstd)
                    #         if obj.paper_format =='s1':
                    #             self.print_direct(cr, uid,obj.pdf_file,'', ps1)
                    #         if obj.paper_format =='s2':
                    #             self.print_direct(cr, uid,obj.pdf_file,'', ps2)
                    #         if obj.paper_format =='s3':
                    #             self.print_direct(cr, uid,obj.pdf_file,'', ps3)
                            # printer_ids=printer_obj.search(cr,uid,[('paper_format','=',obj.paper_format)])
                            #for item in printer_obj.browse(cr,uid,printer_ids):
                            #    printer_name=item.printer_name
                            #    self.print_direct(cr, uid,obj.pdf_file,'', printer_name)
            
            server_data = self.pool.get('fetchdb.server')
            server_ids_2 = server_data.get_main_servers(cr, uid, context=context)
            product_picking = product_picking_obj.browse(cr, uid, product_picking_id)
            picking_id_lists = product_picking.orders.split(", ")
            
            for po in picking_id_lists:
                stock_picking_obj = self.pool.get('stock.picking')
                picking_ids = stock_picking_obj.search(cr, uid, [('name', '=', po)])
                # import pdb; pdb.set_trace()
                created_label = stock_picking_obj.get_shipping_label(cr, uid, picking_ids, context=context)
                if not created_label:
                    break

                fed_shp_sql = """SELECT ZPL_LABEL FROM delmarifctest.dbo.FED_SHP WHERE BJ_NO = '%s';""" % str(po)
                label = server_data.make_query(
                    cr, uid, 1,
                    fed_shp_sql, params=product_picking, select=True
                )
                # import pdb; pdb.set_trace()
                print(label)
                if label:
                    self.print_direct(cr, uid, label[0][0], 'raw', splb)


        response['value'].update({'order_ids':export_ids})

        return response


    def process_all_orders(self, cr, uid, ids, context=None):
        if not ids:
            raise osv.except_osv(_('Warning!'), _('No orders for process!'))

        order_list = self.read(cr, uid, ids[0], ['order_ids'])
        order_ids = order_list.get('order_ids', [])
        if not order_ids:
            raise osv.except_osv(_('Warning!'), _('No orders for process!'))

        return self.pool.get('stock.picking').action_process(cr, uid, order_ids, context=context)

product_picking_print()

class res_partner(osv.osv):
    _inherit = "res.partner"
    _description = "Inventory Line Delmar"

    _columns = {
        'paper_format': fields.char(size=25, string="Paper format"),
        'o321':fields.boolean('321 customer ?'),
        'print_pslip':fields.boolean('Print packing slip ?'),
    }
    _defaults = {
       'o321': False,
       'print_pslip': True,
    }

res_partner()


class picking_export(osv.osv):
    _inherit = "picking.export"
    _description = "Packing slip export"

    _columns = {
        'po': fields.char('PO#', size=256),
        'paper_format': fields.char(size=25, string="Paper format",select=True),
        'category': fields.char(size=25, string="Category"),
        'product_picking_id': fields.many2one('product.picking', 'Product Picking',select=True),
        'print_pslip':fields.boolean('Print packing slip ?'),
        'zpl_file': fields.binary(string='Zpl file', readonly=True),
        'zpl_filename': fields.char('', size=256),
    }
    _defaults = {
       'print_pslip': True,
    }


picking_export()

class printer_setting(osv.osv):
    _name = "printer.setting"
    _description = "Setting printer"

    def _available_printers(self, cr, uid, context=None):
        if context is None:
            context={}
        result=[]
        conn=cups.Connection()
        printers = conn.getPrinters()
        for x in printers:
            result.append((x,x))
        return result
        #return [x for x in printers if x[0]]
    _columns = {
        'paper_format': fields.char(size=25, string="Paper format"),
        'printer_name': fields.selection(_available_printers, 'Printer', required=True),
    }


printer_setting()



