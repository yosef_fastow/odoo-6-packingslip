# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012-2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import logging
import re
import exrex
import itertools


logger = logging.getLogger('delmar_sale')

def make_expressions(value_from, value_to, value_type=False, min_symbol=False, max_symbol=False, start_string=''):
    """
        Makes list of regexps for a range.
        Example:
            parameters:
                value_from = '123'
                value_to = '789'
            result:
                [
                    '12[3-9]',
                    '1[3-9][0-9]',
                    '[2-6][0-9][0-9]',
                    '7[0-8][0-9]',
                ]
    """
    if not value_from or not value_to:
        return ['']
    if not value_type:
        char_regexp_pattern = '^[A-Z]+$'
        digit_regexp_pattern = '^[0-9]+$'
        prog_char = re.compile(char_regexp_pattern)
        prog_digit = re.compile(digit_regexp_pattern)
        if prog_char.match(value_from) and prog_char.match(value_to):
            value_type = 'char'
            min_symbol = 'A'
            max_symbol = 'Z'
        elif prog_digit.match(value_from) and prog_digit.match(value_to):
            value_type = 'digit'
            min_symbol = '0'
            max_symbol = '9'
        else:
            return ['']
    res = []
    # Swap values if value_from > value_to
    if len(value_from) > len(value_to) or value_from > value_to:
        value_from, value_to = value_to, value_from
    # Example '123'..'45678': split it to '123'..'999' and '0000'..'45678'
    if len(value_from) < len(value_to):
        res += make_expressions(
            value_from,  # 123
            len(value_from) * max_symbol,  # 999
            value_type,
            min_symbol,
            max_symbol,
            start_string,
        )
        res += make_expressions(
            (len(value_from) + 1) * min_symbol,  # '0000'
            value_to,  # '45678'
            value_type,
            min_symbol,
            max_symbol,
            start_string,
        )
    # Example 1234..1421
    else:
        for i in range(len(value_from)):
            if value_from[i] == value_to[i]:
                start_string += value_from[i]  # '1'
            else:
                end_res = []
                if value_from[i+1:] != min_symbol * (len(value_from) - i - 1):
                    res += make_expressions(
                        value_from[i+1:],  # '34'
                        len(value_from[i+1:]) * max_symbol,  # '99'
                        value_type,
                        min_symbol,
                        max_symbol,
                        start_string + value_from[i],  # '1'+'2' = '12'
                    )  # 1234..1299
                    value_from = value_from[:i] + chr(ord(value_from[i]) + 1) + len(value_from[i+1:]) * min_symbol  # '1300'
                if value_to[i+1:] != max_symbol * (len(value_to) - i - 1):
                    end_res = make_expressions(
                        len(value_to[i+1:]) * min_symbol,  # '00'
                        value_to[i+1:],  # '21'
                        value_type,
                        min_symbol,
                        max_symbol,
                        start_string + value_to[i],  # '1'+'4' = '14'
                    )  # 1400..1421
                    value_to = value_to[:i] + chr(ord(value_to[i]) - 1) + len(value_to[i+1:]) * max_symbol  # '1399'
                if value_from < value_to:
                    # 1300..1399
                    res_expr = start_string + (value_from[i] == value_to[i] and value_from[i] or '[{0}-{1}]'.format(value_from[i], value_to[i])) + '[{0}-{1}]'.format(min_symbol, max_symbol) * (len(value_from) - i - 1)
                    res.append(res_expr)
                res += end_res
                break
        if not res:
            res = [start_string]
    return res

class stock_bin(osv.osv):
    _name = "stock.bin"

    _columns = {
        'name': fields.char('Bin name', size=256),
    }

    def create(self, cr, uid, vals, context=None):
        if 'name' in vals:
            vals.update({'name': str(vals.get('name', '')).strip()})
        return super(stock_bin, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if 'name' in vals:
            vals.update({'name': str(vals.get('name', '')).strip()})
        return super(stock_bin, self).write(cr, uid, ids, vals, context=context)

    def get_range_expressions(self, cr, uid, *args):
        return make_expressions(*args)

    def get_max_bin_length(self, cr, uid):
        param_obj = self.pool.get('ir.config_parameter')
        try:
            bin_length = int(param_obj.get_param(cr, uid, 'max_bin_length'))
            if not bin_length:
                raise
        except:
            bin_length = 200

        return bin_length

    def get_bin_id_by_name(self, cr, uid, bin):
        bin_id = False
        if bin:
            bin_ids = self.search(cr, uid, [('name', '=', bin)])
            if bin_ids:
                bin_id = bin_ids[0]

            if not bin_id:
                bin_id = self.create(cr, uid, {'name': bin})

        return bin_id

    def check_bins_existence(self, cr, uid, bin_list, warehouse_name, server_id=None, posted=False):
        if not bin_list:
            return []
        if isinstance(bin_list, (str, unicode)):
            bin_list = [bin_list]
        server_data = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_data.get_sale_servers(cr, uid, single=True)

        posted_sql_part = posted and "1 > 0" or "status <> 'Posted'"
        bin_data_sql = """SELECT distinct bin FROM transactions
            WHERE bin in ('%s')
                AND warehouse = '%s'
                AND %s
        """ % ("','".join(bin_list), warehouse_name, posted_sql_part)
        bin_data = server_data.make_query(cr, uid, server_id, bin_data_sql, select=True, commit=False)

        existing_bins = [bin_data_row[0] for bin_data_row in bin_data]

        return existing_bins

    def get_items_for_bins(self, cr, uid, bin_list, warehouse_name, ignore_empty=True, ignore_phantom=False, server_id=None, mssql_conn=None, mssql_curs=None):
        if not bin_list:
            return []
        if isinstance(bin_list, (str, unicode)):
            bin_list = [bin_list]
        server_data = self.pool.get('fetchdb.server')
        if not (server_id or mssql_conn or mssql_curs):
            server_id = server_data.get_sale_servers(cr, uid, single=True)
        if mssql_conn:
            server_id = None
        having_str = ''
        phantom_subquery = ''
        if ignore_empty:
            having_str = "HAVING max(sum_qty) <> 0"
            if not ignore_phantom:
                phantom_subquery = """,
                    COALESCE(
                        (
                            SELECT TOP 1 phantom FROM transactions
                            WHERE phantom is NOT NULL
                                AND warehouse = '{warehouse}'
                                AND status <> 'Posted'
                                AND bin = groups.bin
                                AND stockid = groups.stockid
                                AND id_delmar = groups.id_delmar
                                AND size = groups.size
                            ORDER BY id desc
                        ), 0
                    ) as phantom_qty
                """.format(warehouse=warehouse_name)
                having_str += " OR max(phantom_qty) > 0"

        bin_data_sql = """SELECT bin, concat(id_delmar, '/', size) as itemid FROM
            (
                SELECT bin, stockid, id_delmar, size, sum_qty {phantom_subquery}
                FROM
                (
                    SELECT bin, stockid, id_delmar, size, sum(adjusted_qty) as sum_qty
                    FROM transactions
                    WHERE bin in ('{bins}')
                        AND warehouse = '{warehouse}'
                        AND status <> 'Posted'
                    GROUP BY bin, stockid, id_delmar, size
                ) groups
            ) groups_by_bin
            GROUP BY bin, id_delmar, size
            {having_str}
        """.format(bins="','".join(bin_list), warehouse=warehouse_name, having_str=having_str, phantom_subquery=phantom_subquery)
        bin_data = server_data.make_query(cr, uid, server_id, bin_data_sql, select=True, commit=False, connection=mssql_conn, cursor=mssql_curs) or []
        bins = {}
        for bin in bin_data:
            if bin[0] not in bins.keys():
                bins.update({
                    bin[0]: [bin[1]]
                })
            else:
                bins[bin[0]].append(bin[1])
        return bins

    def check_bins_for_release(self, cr, uid, bin_list, warehouse_name, id_delmar=None, size=None, server_id=None, mssql_conn=None, mssql_curs=None):
        if isinstance(bin_list, (str, unicode)):
            bin_list = [bin_list]
        if not bin_list:
            return []
        server_data = self.pool.get('fetchdb.server')
        if not (server_id or mssql_conn or mssql_curs):
            server_id = server_data.get_sale_servers(cr, uid, single=True)
        if mssql_conn:
            server_id = False
        itemid_sql_part = "AND id_delmar = '{}' AND size = '{}'".format(id_delmar, size) if id_delmar and size else ''
        bin_data_sql = """SELECT bin FROM
            (
                SELECT bin, stockid, sum_qty,
                    COALESCE(
                        (
                            SELECT TOP 1
                                CASE WHEN bin = groups.bin THEN phantom ELSE 0 end as phantom
                            FROM transactions
                            WHERE phantom is NOT NULL
                                AND status <> 'Posted'
                                AND stockid = groups.stockid
                                {itemid}
                            ORDER BY id desc
                        ), 0
                    ) as phantom_qty
                FROM
                (
                    SELECT coalesce(bin, '') as bin, stockid, sum(adjusted_qty) as sum_qty
                    FROM transactions
                    WHERE coalesce(bin, '') in ('{bins}')
                        AND status <> 'Posted'
                        {itemid}
                    GROUP BY coalesce(bin, ''), stockid
                ) groups
            ) groups_by_bin
            GROUP BY bin
            HAVING max(sum_qty) = 0
            AND max(phantom_qty) = 0
        """.format(bins="','".join(bin_list), itemid=itemid_sql_part)
        bin_data = server_data.make_query(cr, uid, server_id, bin_data_sql, select=True, commit=False, connection=mssql_conn, cursor=mssql_curs) or []

        to_release_bins = [bin_data_row[0] for bin_data_row in bin_data]

        return to_release_bins

    def release_bins(self, cr, uid, bin_list, warehouse_name, id_delmar=None, size=None, server_id=None, mssql_conn=None, mssql_curs=None):
        if isinstance(bin_list, (str, unicode)):
            bin_list = [bin_list]
        if not bin_list:
            return []
        server_data = self.pool.get('fetchdb.server')
        if not (server_id or mssql_conn or mssql_curs):
            server_id = server_data.get_sale_servers(cr, uid, single=True)
        if mssql_conn:
            server_id = None
        to_release_bins = self.check_bins_for_release(cr, uid, bin_list, warehouse_name, id_delmar, size, server_id=server_id, mssql_conn=mssql_conn, mssql_curs=mssql_curs)
        itemid_sql_part = "AND id_delmar = '{}' AND size = '{}'".format(id_delmar, size) if id_delmar and size else ''
        # Release query
        if to_release_bins:
            bin_post_sql = """UPDATE transactions SET status = 'Posted'
                WHERE coalesce(bin, '') in ('%s')
                    AND status <> 'Posted'
                    %s
            """ % ("','".join(to_release_bins), itemid_sql_part)
            server_data.make_query(cr, uid, server_id, bin_post_sql, select=False, commit=True, connection=mssql_conn, cursor=mssql_curs)
        return to_release_bins

    # TODO: change return False to Raise exceptions
    def rename_mssql_bin(self, cr, uid, params=None, server_id=None, context=None):
        if not params:
            return False

        required_fields = ['id_delmar', 'size', 'warehouse', 'stockid']
        for key in required_fields:
            if not params.get(key):
                return False

        allow_empty = ['bin']
        for key in allow_empty:
            if not params.get(key):
                params[key] = ''

        server_obj = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_obj.get_sale_servers(cr, uid, single=True)

        if not server_id:
            return False

        update_sql = """
            UPDATE transactions
            SET bin = %(bin)s
            WHERE 1>0
                AND id_delmar = %(id_delmar)s
                AND size = %(size)s
                AND warehouse = %(warehouse)s
                AND stockid = %(stockid)s
                AND bin <> %(bin)s
                AND status <> 'Posted'
        """
        return server_obj.make_query(cr, uid, server_id, update_sql, params=params, select=False, commit=True)

    def is_bin_available_for_product(self, cr, uid, warehouse_name, bin_name, id_delmar, size_name):
        if not all([warehouse_name, bin_name]):
            return False
        cr.execute("""SELECT s.id
            FROM stock_warehouse w
            INNER JOIN stock_bin_setup s
                ON w.id = s.warehouse_id
            WHERE w.name = %s""", (warehouse_name,))
        if not cr.fetchone():
            return True
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        check_sql = """SELECT TOP 1
                CASE
                WHEN id_delmar = '{id_delmar}' AND size = '{size_name}'
                THEN 1
                ELSE 0
                END
            FROM transactions
            WHERE warehouse = '{warehouse_name}'
                AND coalesce(bin, '') = '{bin_name}'
                AND status <> 'Posted'
            ORDER BY CASE
                WHEN id_delmar = '{id_delmar}' AND size = '{size_name}'
                THEN 1
                ELSE 0
                END DESC
        """.format(warehouse_name=warehouse_name, bin_name=str(bin_name),
            id_delmar=id_delmar, size_name=size_name)
        check_res = server_data.make_query(cr, uid, server_id, check_sql,
            select=True, commit=False)
        return check_res and check_res[0] and check_res[0][0] == 1

    def split_itemids(self, itemids):
        result = []
        if not itemids:
            return result
        if isinstance(itemids, (str, unicode)):
            itemids = [itemids]
        for itemid in itemids:
            try:
                parts = itemid.split('/')
                result.append({
                    'size': parts[-1],
                    'id_delmar': '/'.join(parts[:-1]),
                })
            except:
                pass
        return result

    def get_mssql_bin_for_product(self, cr, uid, row_itemids=None, row_warehouse=None, row_stock=None, ignore_bins=None, min_qty=None):
        res = {}

        if not row_itemids or not row_warehouse:
            return res
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            return res

        and_ignore_bins = ''
        if ignore_bins:
            if isinstance(ignore_bins, (str, unicode)):
                ignore_bins = [ignore_bins]
            and_ignore_bins = "and coalesce(bin, '') not in ('%s')" % ("','".join(ignore_bins))
        if min_qty is None:
            having_qty = ''
        else:
            having_qty = 'HAVING sum(adjusted_qty) >= %s' % (int(min_qty))
        split_itemids = self.split_itemids(row_itemids)
        items_values = ', '.join([u"('{id_delmar}', '{size}')".format(**item) for item in split_itemids])


        sql = """SELECT
                concat(id_delmar, '/', size),
                coalesce(bin, '') as bin
            from transactions
            where id in (
                select max(id)
                from (values {itemids}) as i (id_delmar, size)
                left join transactions t
                    on t.ID_DELMAR = i.id_delmar
                    and t.Size = i.size
                where 1=1
                    and warehouse = ?
                    {stock_condition}
                    and invredid is not null
                    and invredid <> ''
                    and status != 'Posted'
                    {and_ignore_bins}
                group by i.id_delmar, i.size
                {having_qty}
            )
        """.format(
            stock_condition='and stockid = ?' if row_stock else '',
            itemids=items_values,
            and_ignore_bins=and_ignore_bins,
            having_qty=having_qty,
            )
        params = [row_warehouse]
        if row_stock:
            params.append(row_stock)

        ms_res = server_data.make_query(
            cr, uid, server_id,
            sql, params=params,
            select=True, commit=False
        ) or []
        for line in ms_res:
            res[line[0]] = line[1]

        return res

    def create_mssql_bin(self, cr, uid, id_delmar, size, warehouse, stockid, shelf, server_id=None, context=None):

        move_obj = self.pool.get('stock.move')
        server_obj = self.pool.get('fetchdb.server')

        if not server_id:
            server_id = server_obj.get_sale_servers(cr, uid, single=True)

        if not server_id:
            raise osv.except_osv('Warning!', 'Inventory server is not available!')

        trn_params = {
            'transactioncode': 'STK',
            'id_delmar': id_delmar,
            'size': size,
            'qty': 0,
            'warehouse': warehouse,
            'stockid': stockid,
            'bin': shelf,
            'sign': 1,
            'invoiceno': '_NEW_BIN',
            'unit_price': 0
        }
        list_params, context = move_obj.prepare_transaction_params(
            cr, uid,
            trn_params
            )

        if not list_params:
            raise osv.except_osv('Warning!', 'Wrong params: %s' % (trn_params))

        return move_obj.write_into_transactions_table(
            cr, uid,
            server_id,
            list_params,
            context=context
            )

    def get_qty_for_bin_item(self, cr, uid, id_delmar, size, warehouse, bin, server_id=None):
        server_data = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_data.get_sale_servers(cr, uid, single=True)
        result = []
        if id_delmar and size and warehouse and bin:
            sql_check_qty = """SELECT warehouse, stockid, bin, qty,
                    COALESCE(
                        (
                            SELECT TOP 1 phantom FROM transactions
                            WHERE phantom is NOT NULL
                                AND warehouse = '{warehouse}'
                                AND status <> 'Posted'
                                AND bin = groups.bin
                                AND stockid = groups.stockid
                            ORDER BY id desc
                        ), 0
                    ) as phantom_qty
                FROM
                (SELECT
                    warehouse,
                    stockid,
                    coalesce(bin, '') as bin,
                    sum(adjusted_qty) as qty
                FROM transactions
                WHERE 1=1
                    and id_delmar = '{id_delmar}'
                    and size = '{size}'
                    and warehouse = '{warehouse}'
                    and coalesce(bin, '') = '{bin}'
                    and status != 'Posted'
                group by
                    warehouse,
                    stockid,
                    coalesce(bin, '')
                ) groups
            """.format(id_delmar=id_delmar, size=size, warehouse=warehouse, bin=bin)
            qty_res = server_data.make_query(cr, uid, server_id, sql_check_qty, select=True, commit=False)
            for res in qty_res:
                result.append({
                    'warehouse': res[0],
                    'stockid': res[1],
                    'bin': res[2],
                    'qty': res[3],
                    'phantom_qty': res[4],
                    })

        return result

    def get_qty_for_bin_item_stock(self, cr, uid, id_delmar, size, warehouse, bin_name, stockid, server_id=None):
        server_data = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_data.get_sale_servers(cr, uid, single=True)
        actual_qty = 0
        if not (id_delmar and size and warehouse and stockid):
            return actual_qty

        params = [id_delmar, size, warehouse, stockid]
        if bin_name:
            where_bin = "AND bin = ?"
            params.append(bin_name)
        else:
            where_bin = "AND (bin is null or bin = '')"

        sql_check = """SELECT TOP 1
                SUM(adjusted_qty) AS QTY
            FROM TRANSACTIONS
            WHERE id_delmar = ?
                AND size = ?
                AND warehouse = ?
                AND stockid = ?
                %s
                AND status <> 'Posted'
            GROUP BY
                id_delmar,
                size,
                warehouse,
                stockid,
                coalesce(bin, '')
        """ % where_bin
        res = server_data.make_query(
            cr, uid, server_id,
            sql_check, params=params,
            select=True, commit=False
        )
        if res:
            actual_qty = res[0][0]
        return actual_qty

stock_bin()


class stock_release_bin(osv.osv_memory):
    _name = "stock.release.bin"
    _columns = {
        'bin_list': fields.text('Bin Names'),
        'result_info': fields.text('Result info'),
        'bin_to_release_list': fields.text('Ready to Release Bins'),
        'line_ids': fields.many2many('stock.product.report.line', 'resease_stock_product_report_line_rel', 'release_id', 'line_id', 'Not Empty Bins'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
        'location': fields.char('Location name', size=128, required=True),
    }

    def add_bins(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)

        bin_obj = self.pool.get('stock.bin')

        for release in self.browse(cr, uid, ids):
            if not release.bin_list:
                raise osv.except_osv(_('Warning!'), _('Bin List is empty!'))
            bin_list = release.bin_list.replace('\n', ',').replace('\t', ',')
            bin_list = re.sub(r'(.),+?', r'\1,', bin_list)
            bin_list = bin_list.split(',')
            bin_list = [el.upper().strip() for el in bin_list if el.strip()]

            warehouse_name = release.warehouse_id and release.warehouse_id.name or ''

            existing_location_sql = """SELECT distinct stockid FROM transactions
                WHERE warehouse = '%s'
                    AND stockid = '%s'
            """ % (warehouse_name, release.location)
            existing_location = server_data.make_query(cr, uid, server_id, existing_location_sql, select=True, commit=False)
            if not existing_location:
                raise osv.except_osv(_('Warning!'), _('Location %s (warehouse %s) does not exists in the TRANSACTIONS ') % (release.location, warehouse_name))
            else:
                loc_ids = self.pool.get('stock.location').search(cr, uid, [('complete_name', 'ilike', '% / ' + warehouse_name + ' / Stock / ' + release.location.strip())])
                if not loc_ids:
                    raise osv.except_osv(_('Warning!'), _('Location %s (warehouse %s) does not exists in the ERP ') % (release.location, warehouse_name))

            existing_bins_sql = """SELECT distinct bin FROM transactions
                WHERE bin in ('%s')
                    AND warehouse = '%s'
                    AND stockid = '%s'
            """ % ("','".join(bin_list), warehouse_name, release.location)
            existing_bins = server_data.make_query(cr, uid, server_id, existing_bins_sql, select=True, commit=False)
            if not existing_bins:
                self.write(cr, uid, ids, {
                    'bin_list': '',
                    'result_info': 'Bins have not found. Please check Warehouse, Location and Bin Names',
                    'line_ids': [(6, 0, [])],
                    'bin_to_release_list': '',
                }, context=context)
                return False
            existing_bins_list = [row[0].upper() for row in existing_bins]

            not_released_bins_sql = """SELECT distinct bin FROM transactions
                WHERE bin in ('%s')
                    AND status <> 'posted'
            """ % ("','".join(existing_bins_list))
            not_released_bins = server_data.make_query(cr, uid, server_id, not_released_bins_sql, select=True, commit=False)
            not_released_bins_list = [row[0].upper() for row in not_released_bins]

            to_release_bins = bin_obj.check_bins_for_release(cr, uid, existing_bins_list, warehouse_name, server_id)

            not_found_bins_list = list(set(bin_list) - set(existing_bins_list))
            released_bins_list = list(set(existing_bins_list) - set(not_released_bins_list))
            not_empty_bins = list(set(not_released_bins_list) - set(to_release_bins))

            result_info = ''
            if not_found_bins_list:
                result_info += '%d bins not found in %s, %s:\n' % (len(not_found_bins_list), warehouse_name, release.location)
                result_info += '\n'.join(not_found_bins_list) + '\n\n'
            if released_bins_list:
                result_info += '%d bins are released already:' % (len(released_bins_list))
                result_info += '\n'.join(released_bins_list) + '\n\n'
            result_info += '%d bins are ready to release:' % (len(to_release_bins))
            result_info += '\n'.join(to_release_bins) + '\n\n'
            if not_empty_bins:
                result_info += '%d bins still have items inside either positive phantom qty:' % (len(not_empty_bins))
                result_info += '\n'.join(not_empty_bins) + '\n\n'

            line_ids = []
            if not_empty_bins:
                stock_lines = self.get_stock_product_report_lines(cr, uid, not_empty_bins, warehouse_name, context=context)
                line_ids = self.build_stock_report_lines(cr, uid, stock_lines, context=context)

            self.write(cr, uid, ids, {
                'bin_list': '',
                'result_info': result_info,
                'line_ids': [(6, 0, line_ids)],
                'bin_to_release_list': ','.join(to_release_bins),
            }, context=context)

        return True

    def get_stock_product_report_lines(self, cr, uid, bins, warehouse, context=None):
        res = None
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)

        if server_id:
            sql = """SELECT
                    SUM(adjusted_qty) AS QTY,
                    id_delmar, size,
                    warehouse, stockid, bin,
                    MAX(transactdate) AS last_date
                FROM TRANSACTIONS
                WHERE bin in ('%s')
                    AND status <> 'Posted'
                GROUP BY id_delmar, size, warehouse, stockid, bin
                HAVING sum(adjusted_qty) > 0
                ORDER BY bin, id_delmar, size;
            """ % ("','".join(bins))
            res = server_data.make_query(cr, uid, server_id, sql, select=True, commit=False)

        return res

    def build_stock_report_lines(self, cr, uid, data, context=None):
        line_ids = []

        product_obj = self.pool.get('product.product')

        if data:
            line_obj = self.pool.get('stock.product.report.line')
            for qty, delmar_id, size, warehouse, stock, bin, last_date in data:
                product_ids = product_obj.search(cr, uid, [('default_code', '=', delmar_id.strip()), ('type', '=', 'product')], context=context)
                if not product_ids:
                    continue
                line_id = line_obj.create(cr, uid, {
                    'item': delmar_id and delmar_id.strip() or False,
                    'size': size and size.strip() or False,
                    'warehouse': warehouse and warehouse.strip() or False,
                    'stock': stock and stock.strip() or False,
                    'bin': bin and bin.rstrip() or False,
                    'qty': qty,
                    'last_date': last_date,
                    'product_id': product_ids[0],
                })
                if line_id:
                    line_ids.append(line_id)

        return line_ids

    def release_bins(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        bin_obj = self.pool.get('stock.bin')

        for release in self.browse(cr, uid, ids):
            bins = release.bin_to_release_list.split(",")
            warehouse_name = release.warehouse_id and release.warehouse_id.name or ''

            if not bins:
                continue
            released_bins = bin_obj.release_bins(cr, uid, bins, warehouse_name)
            result_info = release.result_info or ''
            if released_bins:
                result_info += '\n\nThe following bins have been released:\n%s' % "\n".join(released_bins)
            else:
                result_info += '\n\nNothing has been released'
            self.write(cr, uid, ids, {
                'bin_list': '',
                'result_info': result_info,
                'bin_to_release_list': '',
            }, context=context)

        return True

stock_release_bin()


class stock_bin_setup(osv.osv):
    _name = 'stock.bin.setup'
    _character_types = [
        ('A-Z', 'A-Z'),
        ('0-9', '0-9'),
    ]

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
        'x': fields.selection(_character_types, 'Corridor (X)'),
        'delimiler_xy': fields.boolean('Delimiter'),
        'y': fields.selection(_character_types, 'Shelves (Y)', required=True),
        'delimiler_yz': fields.boolean('Delimiter'),
        'z': fields.selection(_character_types, 'Height (Z)'),
        'format': fields.char('Format', size=128),
    }

    _sql_constraints = [
        ('warehouse_uniq', 'unique (warehouse_id)', 'Warehouse must be unique!')
    ]

    _rec_name = "format"

    def create(self, cr, uid, vals, context=None):
        x = vals.get('x', False)
        delimiler_xy = vals.get('delimiler_xy', False)
        y = vals.get('y', False)
        delimiler_yz = vals.get('delimiler_yz', False)
        z = vals.get('z', False)

        format = self.auto_format(cr, uid, [], x, delimiler_xy, y, delimiler_yz, z).get('value', {}).get('format')
        vals.update({'format': format})
        return super(stock_bin_setup, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):

        setup = self.browse(cr, uid, ids[0])

        x = vals.get('x', setup.x)
        delimiler_xy = vals.get('delimiler_xy', setup.delimiler_xy)
        y = vals.get('y', setup.y)
        delimiler_yz = vals.get('delimiler_yz', setup.delimiler_yz)
        z = vals.get('z', setup.z)

        format = self.auto_format(cr, uid, ids, x, delimiler_xy, y, delimiler_yz, z).get('value', {}).get('format')
        vals.update({'format': format})
        return super(stock_bin_setup, self).write(cr, uid, ids, vals, context=context)

    # Result values: (bool: status, string: bin name or error message)
    def bin_name_validation(self, cr, uid, warehouse_id, full_name=None, x='', y='', z=''):
        if not warehouse_id:
            return (False, 'Warehouse is not defined!')
        setup_ids = self.search(cr, uid, [('warehouse_id', '=', warehouse_id)])
        if not setup_ids:
            return (True, full_name)
        setup = self.browse(cr, uid, setup_ids[0])

        if not full_name:
            full_name = x or ''
            full_name += setup.x and setup.delimiler_xy and '-' or ''
            full_name += y or ''
            full_name += setup.z and setup.delimiler_yz and '-' or ''
            full_name += z or ''

        full_name = full_name.strip().upper()

        if not full_name:
            return (False, 'Bin Name cannot be Empty!')

        regexp_pattern = '^'

        if setup.x:
            regexp_pattern += '[{0}]+'.format(setup.x)
            if setup.delimiler_xy:
                regexp_pattern += '\-'

        regexp_pattern += '[{0}]+'.format(setup.y or '')

        if setup.z:
            if setup.delimiler_yz:
                regexp_pattern += '\-'
            regexp_pattern += '[{0}]+'.format(setup.z)
        regexp_pattern += '$'

        prog = re.compile(regexp_pattern)
        result = prog.match(full_name)
        if result:
            return (True, full_name)
        else:
            return (False, "{0} doesn't match with the Bin name format!".format(full_name))

    def auto_format(self, cr, uid, ids, x, delimiler_xy, y, delimiler_yz, z, context=None):
        format = ''

        if x == 'A-Z':
            format += 'A'
        elif x == '0-9':
            format += '1'

        if delimiler_xy:
            format += '-'

        if y == 'A-Z':
            format += 'ABCD'
        elif y == '0-9':
            format += '1234'

        if delimiler_yz:
            format += '-'

        if z == 'A-Z':
            format += 'Z'
        elif z == '0-9':
            format += '9'

        return {'value': {'format': format}}

stock_bin_setup()


class stock_bin_create(osv.osv_memory):
    _name = "stock.bin.create"

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
        'setup_id': fields.many2one('stock.bin.setup', 'Bin Setup'),
        'setup_x': fields.char('Setup X', size=256),
        'setup_y': fields.char('Setup Y', size=256),
        'setup_z': fields.char('Setup Z', size=256),
        'location_id': fields.many2one('stock.location', 'Location'),
        'location': fields.char('Location name', size=128, required=True),
        'name': fields.char('Full Bin Name', size=256),
        'x': fields.char('Corridor (X)', size=256),
        'y': fields.char('Shelves (Y)', size=256),
        'z': fields.char('Height (Z)', size=256),
        'product_id': fields.many2one('product.product', 'Product'),
        'sizeable': fields.related('product_id', 'sizeable', string="Sizeable", type="boolean", readonly=True,),
        'size_id': fields.many2one('ring.size', 'Ring Size'),
        'info': fields.text('Info'),
        'success': fields.boolean('Success'),
        'another_stock': fields.boolean('The same bin name for another stock in the same warehouse'),
        'qty_capacity': fields.integer('Quantity Capacity'),
    }

    def create_bin(self, cr, uid, loc_id, loc_name, prod_id, size, qty):
        loc_obj = self.pool.get('stock.location')
        size_obj =self.pool.get('ring.size')
        new_bin = ''
        warehouse_id = loc_obj.get_warehouse(cr, uid, loc_id)
        if isinstance(size, (int, long)):
            size_id = size
        else:
            size_ids = size_obj.search(cr, uid, [('name', '=', size)])
            size_id = size_ids[0] if size_ids else None
        sb_create_id = self.create(cr, uid, {
            'warehouse_id': warehouse_id,
            'location_id': loc_id,
            'location': loc_name,
            'product_id': prod_id,
            'size_id': size_id,
            'qty_capacity': qty or 0,
        })
        if sb_create_id and self.check_location(cr, uid, sb_create_id):
            sug_bin_data = self.read(cr, uid, sb_create_id, ['name'])
            sug_bin = sug_bin_data.get('name')
            if (
                sug_bin
                and sug_bin != 'null'
                and self.check_bin(cr, uid, sb_create_id)
            ):
                new_bin = sug_bin
        return new_bin

    def get_sold_items_qty(self, cr, uid, itemid, months=6):
        result = 0
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        size_obj = self.pool.get('ring.size')
        id_delmar, size = size_obj.split_itemid_to_id_delmar_size(cr, uid,
                                                                  itemid)
        if not server_id:
            return result
        query = """SELECT -sum(adjusted_qty)
            FROM transactions
            WHERE 1 > 0
                AND id_delmar = %(id_delmar)s
                AND size = %(size)s
                AND transactdate > DATEADD(month, -%(months)s, GETDATE())
                AND transactioncode in ('SLE', 'SHP')
        """
        params = {
            'id_delmar': id_delmar,
            'size': size,
            'months': months,
        }
        query_res = server_data.make_query(cr, uid, server_id,
                                           query, params=params,
                                           select=True, commit=False)
        result = query_res and query_res[0][0]
        return max(result, 0)

    def _check_for_another_location(self, cr, uid, wh, stock, id_delmar, size):
        result = {}
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            return result
        check_location_query = """SELECT bin FROM transactions
            WHERE 1>0
                AND warehouse = '%s'
                AND stockid = '%s'
                AND status <> 'Posted'
                AND id_delmar = '%s'
                AND size = '%s'
            GROUP BY bin
        """ % (wh, stock, id_delmar, size)
        bin_data = server_data.make_query(
            cr, uid, server_id, check_location_query,
            select=True, commit=False,
        )
        if bin_data:
            msg = 'Location {0}/Stock/{1} is already using for {2}/{3}!'
            raise osv.except_osv(
                _('Warning!'),
                msg.format(wh, stock, id_delmar, size),
            )

        check_warehouse_query = """SELECT bin, stockid FROM transactions
            WHERE 1>0
                AND warehouse = '%s'
                AND status <> 'Posted'
                AND id_delmar = '%s'
                AND size = '%s'
            GROUP BY bin, stockid
        """ % (wh, id_delmar, size)

        bin_warehouse_data = server_data.make_query(
            cr, uid, server_id, check_warehouse_query,
            select=True, commit=False,
        )
        if bin_warehouse_data:
            result['bin'] = bin_warehouse_data[0][0] or False
            result['location'] = bin_warehouse_data[0][1] or False
        return result

    def _get_bin_setup(self, cr, uid, wh_id):
        setup_obj = self.pool.get('stock.bin.setup')
        setup_ids = setup_obj.search(cr, uid, [('warehouse_id', '=', wh_id)])
        setup_id = setup_ids and setup_ids[0] or False
        return setup_obj.browse(cr, uid, setup_id) if setup_id else False

    def get_bin_data(self, cr, uid, warehouse, location, product, size_str, qty):
        result = {'bin_name': False}
        wh_id = warehouse.id
        wh_name = warehouse.name
        loc_name = location.name
        id_delmar = product.default_code
        another_loc_data = self._check_for_another_location(cr, uid, wh_name,
                                                            loc_name, id_delmar,
                                                            size_str)

        if another_loc_data.get('bin'):
            another_str = "You can use only the same name that is already using"
            another_str += " for another stock ({0}) in the same warehouse\n"
            result.update({
                'bin_name': another_loc_data['bin'],
                'info': another_str.format(another_loc_data['location']),
                'another_stock': True,
            })
            return result

        setup = self._get_bin_setup(cr, uid, wh_id)
        bin_format = setup and setup.format
        if not bin_format:
            no_format_str = "The correct bin format has not been set for {0}\n"
            result['info'] = no_format_str.format(wh_name)
            return result

        result.update({
            'setup_x': setup.x,
            'setup_y': setup.y,
            'setup_z': setup.z,
            'info': "The correct bin format is {0}\n".format(bin_format),
            'setup_id': setup.id,
        })
        itemid = id_delmar + '/' + size_str
        qty_sold = self.get_sold_items_qty(cr, uid, itemid)
        sold_str = "Qty sold for item {0} for the last 6 months: {1}\n"
        result['info'] += sold_str.format(itemid, qty_sold)

        rule_obj = self.pool.get('stock.bin.rule')
        bin_type_ids = rule_obj.get_rule(cr, uid, wh_id, location.id,
                                         product.id, qty, qty_sold)
        range_obj = self.pool.get('stock.bin.range')
        for bin_type_id in bin_type_ids:
            suggested_bin = range_obj.suggest_bin(cr, uid, bin_type_id,
                                                  wh_name, loc_name)
            if not suggested_bin:
                continue
            result['bin_name'] = suggested_bin
            result['info'] += 'Suggested bin: {0}\n'.format(str(suggested_bin))
            break
        return result

    def _get_location(self, cr, uid, create_bin):
        if create_bin.location_id:
            return create_bin.location_id
        loc_name = (create_bin.location or '').strip().upper()
        full_name = 'Physical Locations / {0} / Stock / {1}'.format(
            create_bin.warehouse_id.name,
            loc_name,
        )
        args = [('complete_name', '=ilike', full_name),]
        loc_ids = self.pool.get('stock.location').search(cr, uid, args)
        if not loc_ids:
            raise osv.except_osv(
                _('Warning!'),
                'Location {0} does not exist in ERP!'.format(full_name),
            )
        loc_id = loc_ids[0]
        return self.pool.get('stock.location').browse(cr, uid, loc_id)

    def check_location(self, cr, uid, ids, context=None):
        if not ids:
            return False
        sb_create_id = ids if isinstance(ids, (int, long)) else ids[0]
        create_bin = self.browse(cr, uid, sb_create_id)
        qty_capacity = create_bin.qty_capacity or 0
        warehouse = create_bin.warehouse_id
        product = create_bin.product_id
        size_name = create_bin.size_id and create_bin.size_id.name or 0.0
        size_postfix = re.sub('\.', '', "%05.2f" % float(size_name))
        location = self._get_location(cr, uid, create_bin)
        bin_data = self.get_bin_data(cr, uid, warehouse, location, product,
                                     size_postfix, qty_capacity)
        if not bin_data.get('bin_name') and not create_bin.name:
            return False
        values = {
            'location_id': location.id,
            'info': bin_data['info'],
            'setup_id': bin_data.get('setup_id', False),
            'setup_x': bin_data.get('setup_x', False),
            'setup_y': bin_data.get('setup_y', False),
            'setup_z': bin_data.get('setup_z', False),
            'name': create_bin.name and create_bin.name.strip() or bin_data.get('bin_name', False),
            'another_stock': bin_data.get('another_stock', False),
        }
        self.write(cr, uid, ids, values)
        return True

    def check_bin(self, cr, uid, ids, context=None):
        if not ids:
            return False
        if not self.check_location(cr, uid, ids, context):
            raise osv.except_osv(_('Error!'), 'Please, enter bin name!')

        sb_create_id = ids if isinstance(ids, (int, long)) else ids[0]

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)

        create_bin = self.browse(cr, uid, sb_create_id)
        wh_id = create_bin.warehouse_id.id
        setup_obj = self.pool.get('stock.bin.setup')
        setup_ids = setup_obj.search(cr, uid, [('warehouse_id', '=', wh_id)])
        warehouse = create_bin.warehouse_id.name
        bin_name = create_bin.name
        if setup_ids and not create_bin.another_stock:
            validation_info = setup_obj.bin_name_validation(
                cr, uid, wh_id, create_bin.name,
                create_bin.x, create_bin.y, create_bin.z
            )
            if not validation_info[0]:
                raise osv.except_osv(_('Error!'), validation_info[1])
            bin_name = validation_info[1]
            self._raise_if_bin_in_use(cr, uid, warehouse, bin_name, server_id)

        bin_obj = self.pool.get('stock.bin')
        stockid = create_bin.location.strip().upper()
        size_name = create_bin.size_id and create_bin.size_id.name or 0.0
        size_postfix = re.sub('\.', '', "%05.2f" % float(size_name))
        id_delmar = create_bin.product_id.default_code
        bin_obj.create_mssql_bin(cr, uid, id_delmar, size_postfix, warehouse,
                                 stockid, bin_name or 'null', server_id)
        info = self.read(cr, uid, sb_create_id, ['info']).get('info', '')
        info += 'Success! Bin {0} has been created'.format(bin_name)
        values = {
            'info': info,
            'success': True,
        }
        self.write(cr, uid, ids, values)
        self.pool.get('stock.product.report').get_prod_locations(cr, uid, create_bin.id, create_bin.product_id.id,
                                                                 size_id=False, warehouse_id=False, location_id=False,
                                                                 shelf=False)
        return {'type': 'ir.actions.act_window_close'}

    def _raise_if_bin_in_use(self, cr, uid, warehouse, bin_name, server_id):
        server_data = self.pool.get('fetchdb.server')
        bin_obj = self.pool.get('stock.bin')
        check_bin_sql = """SELECT
                concat(id_delmar, '/', size),
                sum(adjusted_qty)
            FROM transactions
            WHERE 1>0
                AND warehouse = '{0}'
                AND status <> 'Posted'
                AND bin = '{1}'
            GROUP BY id_delmar, size
        """.format(warehouse, bin_name)
        bin_data = server_data.make_query(cr, uid, server_id, check_bin_sql,
                                          select=True, commit=False)
        if not bin_data:
            return
        for item_data in bin_data:
            if item_data[1] > 0:
                error_msg = "Bin {0} is exist and already using for product {1}"
                raise osv.except_osv(
                    _('Error!'),
                    error_msg.format(bin_name, item_data[0])
                )
        released_bins = bin_obj.release_bins(cr, uid, [bin_name], warehouse)
        if not released_bins:
            error_msg = "Bin {0} has stock in another location or phantom QTY"
            raise osv.except_osv(_('Error!'), error_msg.format(bin_name))

stock_bin_create()


class stock_bin_type(osv.osv):
    _name = 'stock.bin.type'

    _columns = {
        'warehouse_id': fields.related('setup_id', 'warehouse_id', type='many2one', relation='stock.warehouse', string='Warehouse', readonly=True,),
        'setup_id': fields.many2one('stock.bin.setup', 'Setup Format'),
        'name': fields.char('Name', size=256, required=True, ),
        'description': fields.char('Bin Type Description', size=1024),
        'location': fields.many2one('stock.location', 'Location'),
        'range_ids': fields.one2many('stock.bin.range', 'type_id', 'Ranges'),
    }

stock_bin_type()


class stock_bin_rule(osv.osv):
    _name = 'stock.bin.rule'

    _priority_list = [
        'warehouse_id',
        ('priority', 'min'),
        'prod_categ_id',
        ('max_qty', 'min'),
        ('min_qty', 'max'),
        ('max_sales', 'min'),
        ('min_sales', 'max'),
        ('value_from', 'max'),
    ]

    _qty_sold_help = 'Considers sold qty in all warehouses for the last 6 months'

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
        'priority': fields.integer('Priority'),
        'type_id': fields.many2one('stock.bin.type', 'Bin Type'),
        'prod_categ_id': fields.many2one('product.category', 'Product Category'),
        'max_qty': fields.integer('Max Qty Capacity'),
        'min_qty': fields.integer('Min Qty Capacity'),
        'max_sales': fields.integer('Max Qty Sold', help=_qty_sold_help),
        'min_sales': fields.integer('Min Qty Sold', help=_qty_sold_help),
        'value_from': fields.float('Value From'),
    }

    def get_rule(self, cr, uid, warehouse_id, loc_id, product_id, qty_capacity, qty_sold):
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        product_categ_id = product.categ_id and product.categ_id.id or False
        price = product.standard_price or product.lst_price or 0.0

        rule_ids = self.search(cr, uid, [
            ('warehouse_id', 'in', [False, warehouse_id]),
            ('prod_categ_id', 'in', [False, product_categ_id]),
            '|',
                '|', ('max_qty', '<=', 0), ('max_qty', '=', False),
                ('max_qty', '>=', qty_capacity),
            '|', ('min_qty', '=', False), ('min_qty', '<=', qty_capacity),
            '|',
                '|', ('max_sales', '<=', 0), ('max_sales', '=', False),
                ('max_sales', '>=', qty_sold),
            '|', ('min_sales', '=', False), ('min_sales', '<=', qty_sold),
            '|', ('value_from', '=', False), ('value_from', '<=', price),
        ])

        rules = self.read(cr, uid, rule_ids)
        if not rules:
            return []

        for priority_elem in self._priority_list:
            if len(rules) == 1:
                break
            if isinstance(priority_elem, str):
                rules = [rule for rule in rules if rule.get(priority_elem)] or rules
            elif isinstance(priority_elem, tuple):
                func = priority_elem[1] == 'min' and min or max
                best_value = func([rule.get(priority_elem[0]) for rule in rules if rule.get(priority_elem[0])] or [0])
                if best_value:
                    rules = [rule for rule in rules if rule.get(priority_elem[0]) == best_value]

        return [rule.get('type_id')[0] for rule in rules if rule.get('type_id')]

stock_bin_rule()


class stock_bin_range(osv.osv):
    _name = 'stock.bin.range'

    _columns = {
        'type_id': fields.many2one('stock.bin.type', 'Bin Type'),
        'x_range_from': fields.char('X Range From', size=256),
        'x_range_to': fields.char('X Range To', size=256),
        'y_range_from': fields.char('Y Range From', size=256),
        'y_range_to': fields.char('Y Range To', size=256),
        'z_range_from': fields.char('Z Range From', size=256),
        'z_range_to': fields.char('Z Range To', size=256),
        'priority': fields.integer('Priority'),
    }

    def format_validation(self, cr, uid, ids):
        setup_obj = self.pool.get('stock.bin.setup')
        for bin_range in self.browse(cr, uid, ids):
            warehouse_id = bin_range.type_id.warehouse_id.id
            first_value_validation = setup_obj.bin_name_validation(
                cr, uid, warehouse_id,
                x=bin_range.x_range_from,
                y=bin_range.y_range_from,
                z=bin_range.z_range_from,
            )
            if not first_value_validation[0]:
                raise osv.except_osv(_('Error!'), _(first_value_validation[1]))
            last_value_validation = setup_obj.bin_name_validation(
                cr, uid, warehouse_id,
                x=bin_range.x_range_to,
                y=bin_range.y_range_to,
                z=bin_range.z_range_to,
            )
            if not last_value_validation[0]:
                raise osv.except_osv(_('Error!'), _(last_value_validation[1]))
            return True

    def generate_expression(self, exprs):
        result = ''
        for expr in exprs:
            if not expr:
                continue
            elif len(expr) == 1:
                result += expr[0]
            else:
                result += '({0})'.format('|'.join(['({0})'.format(x) for x in expr]))
        return result

    def generate_mssql_expressions(self, expr):
        result = map(''.join, itertools.product(*expr))
        return result

    def get_expr_list(self, cr, uid, range_id):
        bin_range = self.browse(cr, uid, range_id)
        setup = bin_range.type_id and bin_range.type_id.setup_id
        delimiter_xy = '-' if setup and setup.delimiler_xy else ''
        delimiter_yz = '-' if setup and setup.delimiler_yz else ''
        x_exprs = make_expressions(bin_range.x_range_from, bin_range.x_range_to)
        y_exprs = make_expressions(bin_range.y_range_from, bin_range.y_range_to)
        z_exprs = make_expressions(bin_range.z_range_from, bin_range.z_range_to)
        return [x_exprs, [delimiter_xy], y_exprs, [delimiter_yz], z_exprs]

    def suggest_bin(self, cr, uid, bin_type_id, wh_name, location):
        ids = self.search(cr, uid, ['|', '|', '&', ('type_id', '=', bin_type_id), ('z_range_from', '!=', None), '&',
                                    ('type_id', '=', bin_type_id), ('y_range_from', '!=', None), '&',
                                    ('type_id', '=', bin_type_id), ('x_range_from', '!=', None)], order='priority')
        result_bin = False
        for bin_range_id in ids:
            expr_list = self.get_expr_list(cr, uid, bin_range_id)
            expr = self.generate_expression(expr_list)
            mssql_exprs = self.generate_mssql_expressions(expr_list)
            result_bin = self.check_availaible_bin(cr, uid, expr, mssql_exprs,
                                                   wh_name, location)
            if result_bin:
                break
        return result_bin

    def check_availaible_bin(self, cr, uid, expr, mssql_exprs, wh_name, location):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        result = None
        all_bins = list(exrex.generate(expr))
        param_obj = self.pool.get('ir.config_parameter')
        wh_obj = self.pool.get('stock.warehouse')
        try:
            autorelease_delay_days = int(param_obj.get_param(cr, uid, 'autorelease_delay_days'))
            if autorelease_delay_days < 1:
                autorelease_delay_days = 1
        except:
            autorelease_delay_days = 1
        forbid_wh_sql = ''
        wh_ids = wh_obj.search(cr, uid, [('allow_autorelease', '!=', True)])
        if wh_ids:
            wh_read_data = wh_obj.read(cr, uid, wh_ids, ['name'])
            wh_names = [wh['name'] for wh in wh_read_data]
            wh_str = "', '".join(wh_names)
            forbid_wh_sql = "OR max(WAREHOUSE) in ('{}')".format(wh_str)
        sql = """SELECT DISTINCT bin from 
                (select invoiceno, bin, adjusted_qty,transactdate,warehouse,first_value(IsNull(Phantom,0)) OVER (PARTITION BY bin ORDER BY id DESC)as phantom_qty
                FROM
                    transactions
                WHERE 1=1
                    AND status <> 'Posted'
                    AND (
                        {additional_conditions}
                        1 = 2
                    ))sel
                GROUP BY bin
                HAVING sum(adjusted_qty) <> 0
                    OR sum(phantom_qty)>0
                    OR DATEDIFF(day, max(transactdate),getdate()) < {autorelease_delay_days}
                    OR (DATEDIFF(day, max(transactdate),getdate()) < 7 and max(INVOICENO) in ('_NEW_BIN'))
                    {forbid_wh_sql}
        """.format(
            # wh_name=wh_name,
            # location=location,
            additional_conditions='\n'.join(["        bin LIKE '{0}' OR".format(x) for x in mssql_exprs]),
            autorelease_delay_days=autorelease_delay_days,
            forbid_wh_sql=forbid_wh_sql,
        )
        logger.info(
            "\ncheck_available_bin: Search all bins in MSSQL, \nFOR REGEXP '{expr}':\n{sql}".format(
                expr=expr,
                sql=sql
            )
        )
        bins_in_ms = server_data.make_query(cr, uid, server_id, sql, select=True, commit=False) or []
        bins_in_ms = [x[0] for x in bins_in_ms if x]
        if (len(bins_in_ms) == len(all_bins)):
            logger.info("Not found free bins\nFOR REGEXP: {0} IN MSSQL".format(expr))
            result = False
        else:
            # bin_obj = self.pool.get('stock.bin')
            free_bins_set = set(all_bins) - set(bins_in_ms)
            free_bins = list(free_bins_set)
            # existing_free_bins = bin_obj.check_bins_existence(cr, uid, free_bins, wh_name, server_id)
            # non_phantomed_bins = []
            # HOT FIX: Get only first non-phantomic bin, it take loo long time to check all array
            # for bin in sorted(existing_free_bins):
            #     if bin_obj.check_bins_for_release(cr, uid, [bin], wh_name, server_id):
            #         non_phantomed_bins = [bin]
            #         break
            # free_bins now is a list of all non-existing and 1 minimal non-phantomic bin.
            # free_bins = list(free_bins_set - (set(existing_free_bins) - set(non_phantomed_bins)))
            if not free_bins:
                logger.info("Not found free bins\nFOR REGEXP: {0} IN MSSQL".format(expr))
                result = False
            else:
                logger.info("Found free bins:\n{0}".format(','.join(free_bins)))
                try:
                    result = min(free_bins)
                    logger.info("Minimal bin in free bins: {0}".format(result))
                except ValueError:
                    result = False
                    logger.info("Unknown error, couldn't get minimal bin from free bins!")
        return result

stock_bin_range()


class stock_release_warehouse(osv.osv_memory):
    _name = "stock.release.warehouse"
    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
        'days': fields.selection([
            ('30', '30'),
            ('60', '60'),
            ('90', '90'),
        ], 'Not used more than (days)', required=True),
        'result_info': fields.text('Result info'),
    }

    def _post_old_zero_bins(self, cr, uid, warehouse_name, days):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            return False
        params = {
            'warehouse_name': warehouse_name,
            'days': int(days),
        }
        release_sql = """WITH zeros AS (
                SELECT
                    id_delmar,
                    "size",
                    coalesce(bin, '') as bin,
                    sum(adjusted_qty) AS qty,
                    max(transactdate) AS transactdate
                FROM transactions
                WHERE warehouse = %(warehouse_name)s
                    AND status <> 'posted'
                GROUP BY id_delmar, "size", coalesce(bin, '')
                HAVING sum(adjusted_qty) <= 0
                    AND max(transactdate) < DATEADD(day, -%(days)s, GETDATE())
            )
            UPDATE transactions
            SET status = 'Posted'
            WHERE id IN
            (
                SELECT t.id
                FROM zeros
                LEFT JOIN transactions t
                    ON t.ID_DELMAR = zeros.id_delmar
                    AND t."Size" = zeros."size"
                    AND coalesce(t.BIN, '') = coalesce(zeros.bin, '')
                WHERE t.STATUS  <> 'posted'
                    AND warehouse = %(warehouse_name)s
            )
        """
        result = server_data.make_query(
            cr, uid, server_id,
            release_sql, params=params,
            select=False, commit=True,
        )
        return bool(result)

    def release_warehouse(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        release = self.browse(cr, uid, ids[0])
        success = False
        wh_name = release.warehouse_id and release.warehouse_id.name
        days = release.days
        if wh_name and days:
            success = self._post_old_zero_bins(cr, uid, wh_name, days)
        if success:
            result_info = 'The releasing has finished!'
        else:
            result_info = 'The releasing FAILED!'
        self.write(cr, uid, ids, {'result_info': result_info})
        return True

stock_release_warehouse()


class stock_bin_reallocate(osv.osv_memory):
    _name = 'stock.bin.reallocate'
    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
        'limit': fields.integer('Limit the Amount of Bins'),
        'line_ids': fields.one2many('stock.bin.reallocate.lines',
                                    'reallocate_id', 'Lines'),
        'done': fields.boolean('Done'),
        'info': fields.text('Result Info'),
    }

    _defaults = {
        'limit': 50,
        'done': False,
    }

    def _get_ms_products_bins(self, cr, uid, bin_range_id, wh_name):
        bin_range_obj = self.pool.get('stock.bin.range')
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)

        expr_list = bin_range_obj.get_expr_list(cr, uid, bin_range_id)
        mssql_exprs = bin_range_obj.generate_mssql_expressions(expr_list)
        bin_like_expr = ["bin LIKE '{0}' OR".format(x) for x in mssql_exprs]
        ms_products_query = """SELECT
                CONCAT(id_delmar, '/', size),
                bin,
                sum(adjusted_qty)
            FROM transactions
            WHERE 1 > 0
                AND status <> 'Posted'
                AND warehouse = '{wh_name}'
                AND (
                    {bins_like}
                    1 = 2
                )
            GROUP BY
                CONCAT(id_delmar, '/', size),
                bin
        """.format(
            bins_like=' '.join(bin_like_expr),
            wh_name=wh_name,
        )
        res = server_data.make_query(cr, uid, server_id, ms_products_query,
                                     select=True, commit=False)
        return {row[0]: {'bin': row[1], 'qty': row[2]} for row in res}

    def _get_ms_sold_qty(self, cr, uid, itemids, max_sales, min_sales):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        having_more = ''
        if max_sales and max_sales > 0:
            having_more = 'OR -SUM(adjusted_qty) > {0}'.format(max_sales)
        having_less = ''
        if min_sales and min_sales > 0:
            having_less = 'OR -SUM(adjusted_qty) < {0}'.format(min_sales)
        bin_obj = self.pool.get('stock.bin')
        split_itemids = bin_obj.split_itemids(row_itemids)
        items_values = ', '.join([u"('{id_delmar}', '{size}')".format(**item) for item in split_itemids])
        ms_sold_query = """SELECT
            concat(id_delmar, '/', size),
            -SUM(t.adjusted_qty)
            FROM (values {itemids}) as i (id_delmar, size)
            LEFT JOIN transactions t
                ON t.ID_DELMAR = i.id_delmar
                AND t.Size = i.size
            WHERE t.transactdate > DATEADD(month, -6, GETDATE())
                AND transactioncode IN ('SLE', 'SHP')
            GROUP BY id_delmar, size
            HAVING 1 = 2
                {having_more}
                {having_less}
        """.format(
            having_more=having_more,
            having_less=having_less,
            itemids=items_values,
        )
        res = server_data.make_query(cr, uid, server_id, ms_sold_query,
                                     select=True, commit=False)
        return {row[0]: row[1] for row in res}

    def _get_ranges(self, cr, uid, warehouse):
        cr.execute("""SELECT r.max_sales, r.min_sales, d.id
            FROM stock_bin_rule r
            LEFT JOIN stock_bin_range d
                ON r.type_id = d.type_id
            WHERE r.warehouse_id = %d
            AND (r.max_sales > 0 OR r.min_sales > 0)
        """ % warehouse.id)
        ranges = cr.dictfetchall()
        if not ranges:
            no_rules_msg = 'There are no bin rules with max/min sales in {0}'
            raise osv.except_osv(
                'Warning!',
                no_rules_msg.format(warehouse.name)
            )
        return ranges

    def _filter_line_data(self, cr, uid, line_data, limit):
        # Clean duplicates
        line_data = [dict(tuples) for tuples in
                     set(tuple(line.items()) for line in line_data)]
        # Sort by qty_diff
        sorted_lines = sorted(line_data,
                              key=lambda el: el['qty_diff'],
                              reverse=True)
        return sorted_lines[:limit]

    def _create_lines(self, cr, uid, lines_data):
        size_obj = self.pool.get('ring.size')
        line_obj = self.pool.get('stock.bin.reallocate.lines')
        for line_vals in lines_data:
            id_delmar, size = size_obj.split_itemid_to_id_delmar_size(
                cr, uid, line_vals['itemid']
            )
            line_vals.update({
                'id_delmar': id_delmar,
                'size': size,
            })
            del line_vals['itemid']
            line_obj.create(cr, uid, line_vals)
        return True

    def show_items(self, cr, uid, ids, context=None):
        ids = [ids] if isinstance(ids, (int, long)) else ids
        reallocate = self.browse(cr, uid, ids[0])
        warehouse = reallocate.warehouse_id
        if not warehouse:
            raise osv.except_osv('Warning!', 'Warehouse Can\'t be empty!')
        ranges = self._get_ranges(cr, uid, warehouse)
        line_data = []
        for bin_range in ranges:
            prod_bin_dict = self._get_ms_products_bins(cr, uid, bin_range['id'],
                                                       warehouse.name)
            if not prod_bin_dict:
                continue
            itemids = prod_bin_dict.keys()
            prod_sold_dict = self._get_ms_sold_qty(cr, uid, itemids,
                                                   bin_range['max_sales'],
                                                   bin_range['min_sales'])
            for itemid, sold_qty in prod_sold_dict.items():
                if bin_range['max_sales'] < sold_qty:
                    qty_diff = sold_qty - bin_range['max_sales']
                else:
                    qty_diff = bin_range['min_sales'] - sold_qty
                current_qty = prod_bin_dict[itemid]['qty'] or 0
                line_data.append({
                    'reallocate_id': reallocate.id,
                    'itemid': itemid,
                    'bin_from': prod_bin_dict[itemid]['bin'],
                    'bin_max_qty': bin_range['max_sales'],
                    'bin_min_qty': bin_range['min_sales'],
                    'sold_qty': sold_qty,
                    'qty_diff': qty_diff,
                    'current_qty': max(0, current_qty),
                })
        if not line_data:
            raise osv.except_osv(
                'Warning!',
                'All items already have appropriate bins in the warehouse!',
            )
        if reallocate.limit and reallocate.limit > 0:
            limit = reallocate.limit
        else:
            limit = self._defaults['limit']
        filtered_line_data = self._filter_line_data(cr, uid, line_data, limit)
        self._create_lines(cr, uid, filtered_line_data)
        return True

    def _post_old_bin(self, cr, uid, line):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        get_ids_query = """SELECT id
            FROM transactions
            WHERE 1 > 0
                AND warehouse = %(warehouse)s
                AND bin = %(bin)s
                AND id_delmar = %(id_delmar)s
                AND size = %(size)s
                AND status <> 'Posted'
        """
        params = {
            'warehouse': line.reallocate_id.warehouse_id.name,
            'bin': line.bin_from,
            'id_delmar': line.id_delmar,
            'size': line.size,
        }
        res = server_data.make_query(cr, uid, server_id, get_ids_query,
                                     params=params, select=True, commit=False)
        post_ids = [row[0] for row in res]
        post_res = self._update_transactions_status(cr, uid, post_ids, 'Posted')
        return post_res and post_ids

    def _get_new_bin(self, cr, uid, line):
        warehouse = line.reallocate_id.warehouse_id
        location = warehouse.lot_release_id
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [
            ('default_code', '=', line.id_delmar.strip()),
            ('type', '=', 'product'),
        ])
        if not product_ids:
            return False
        product = product_obj.browse(cr, uid, product_ids[0])
        create_obj = self.pool.get('stock.bin.create')
        bin_data = create_obj.get_bin_data(cr, uid, warehouse, location,
                                           product, line.size, line.current_qty)
        return bin_data['bin_name']

    def _update_transactions_status(self, cr, uid, ids, status):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        update_query = """UPDATE transactions
            SET status = '{status}'
            WHERE id in ({ids})
        """.format(
            ids=', '.join([str(transact_id) for transact_id in ids]),
            status=status,
        )
        res = server_data.make_query(cr, uid, server_id, update_query,
                                     select=False, commit=True)
        return res

    def _revert_posted_bin(self, cr, uid, line, posted_ids):
        self._update_transactions_status(cr, uid, posted_ids, 'PENDING')
        itemid = line.id_delmar + '/' + line.size
        return "Could not create new bin for {0}\n".format(itemid)

    def _put_stock_to_new_bin(self, cr, uid, line, new_bin, posted_ids):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        get_stock_qty_query = """SELECT stockid, sum(adjusted_qty)
            FROM transactions
            WHERE id in ({ids})
            GROUP BY stockid
            HAVING sum(adjusted_qty) > 0
        """.format(
            ids=', '.join([str(transact_id) for transact_id in posted_ids]),
        )
        res = server_data.make_query(cr, uid, server_id, get_stock_qty_query,
                                     select=True, commit=False)
        move_obj = self.pool.get('stock.move')
        for row in res:
            dict_params = {
                'transactioncode': 'ADJ',
                'id_delmar': line.id_delmar,
                'size': line.size,
                'qty': row[1],
                'warehouse': line.reallocate_id.warehouse_id.name,
                'stockid': row[0],
                'bin': new_bin,
                'sign': 1,
                'invoiceno': '_Reallocate_Stock',
                'unit_price': 0
            }
            params, context = move_obj.prepare_transaction_params(cr, uid, dict_params)
            move_obj.write_into_transactions_table(cr, uid, server_id, params, context=context)
        line.write({
            'bin_to': new_bin,
            'done': True,
        })

    def _generate_report(self, cr, uid, reallocate):
        ids = [reallocate.id]
        datas = {
            'id': ids[0],
            'ids': ids,
            'model': 'stock.bin.reallocate',
            'report_type': 'pdf',
            }
        act_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'stock.bin.reallocate.list',
            'datas': datas,
            'context': {'active_ids': ids, 'active_id': ids[0]}
        }
        return act_print

    def do_reallocate(self, cr, uid, ids, context=None):
        ids = [ids] if isinstance(ids, (int, long)) else ids
        reallocate = self.browse(cr, uid, ids[0])
        info = ''
        has_done_lines = False
        for line in reallocate.line_ids:
            posted_ids = self._post_old_bin(cr, uid, line)
            try:
                new_bin = self._get_new_bin(cr, uid, line)
            except Exception, e:
                info += str(e) + '\n'
                new_bin = False
            if not new_bin:
                info += self._revert_posted_bin(cr, uid, line, posted_ids)
                continue
            self._put_stock_to_new_bin(cr, uid, line, new_bin, posted_ids)
            has_done_lines = True
        reallocate.write({
            'info': info,
            'done': True,
        })
        if not has_done_lines:
            return False
        return self._generate_report(cr, uid, reallocate)

stock_bin_reallocate()


class stock_bin_reallocate_lines(osv.osv_memory):
    _name = 'stock.bin.reallocate.lines'
    _columns = {
        'reallocate_id': fields.many2one('stock.bin.reallocate', 'Reallocate'),
        'id_delmar': fields.char('Product', size=256),
        'size': fields.char('Size', size=4),
        'bin_from': fields.char('Bin From', size=256),
        'bin_to': fields.char('Bin To', size=256),
        'bin_max_qty': fields.integer('Bin Max Sold QTY'),
        'bin_min_qty': fields.integer('Bin Min Sold QTY'),
        'sold_qty': fields.integer('Sold QTY Last 6 Months'),
        'qty_diff': fields.integer('QTY out of Range'),
        'current_qty': fields.integer('Current QTY'),
        'done': fields.boolean('Done'),
    }

    _defaults = {
        'done': False,
    }

    _order = "qty_diff desc"

stock_bin_reallocate_lines()
