from osv import osv, fields


class order_tag_update_history(osv.osv):
    _name = 'order.tag.update.history'

    _columns = {
        'user_responsible': fields.char("User", size=64, readonly=True),
        'update_date': fields.datetime("Updated Date", readonly=True),
        'order_id': fields.many2one("order.history", "Order history"),
        'order_name': fields.char("Order history", size=64, readonly=True),
        'tag_id': fields.many2one('tagging.order', 'Order Tag'),
        'description': fields.text('Activity'),
    }

    _order = "id desc"
    
order_tag_update_history()

class order_tag_history_update(osv.osv):
    _inherit = "tagging.order"
    _name = _inherit

    _columns = {
        'order_tag_history_ids': fields.one2many("order.tag.update.history", "tag_id", "order_id"),
    }


order_tag_history_update()