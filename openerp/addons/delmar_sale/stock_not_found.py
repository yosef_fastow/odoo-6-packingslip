# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2013
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import time
import tools
import netsvc


class stock_not_found(osv.osv):
    _name = 'stock.not.found'

    _columns = {
        'create_uid': fields.many2one('res.users', 'Created by', readonly=True),
        'name': fields.char('Order#', size=256, ),
        'picking_id': fields.many2one('stock.picking', 'Delivery Order', readonly=True, ),
        # 'move_id': fields.many2one('stock.move', 'Stock Move', readonly=True,),
        'btn_confirm': fields.boolean('Scan', ),
        'note': fields.text('Note', readonly=True),
        'status': fields.char('Order#', size=256, ),
        'move': fields.integer('move'),
    }

    _default = {
        'note': 'Type order# for force adjustment 0 qty (works with single-line orders only)',
        'status': 'Dirty',
    }

    def adjust_zero_stock(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        stockmove_obj = self.pool.get('stock.move')
        picking_obj = self.pool.get('stock.picking')
        inventory_obj = self.pool.get('stock.inventory')
        inventory_line_obj = self.pool.get('stock.inventory.line')
        wf_service = netsvc.LocalService("workflow")
        sale_order_obj = self.pool.get('sale.order')
        res = True

        for track in self.browse(cr, uid, ids, context=context):
            order = track.picking_id

            if not order:
                res = False
                continue

            inv_no = "ZEROSTOCK %s" % order.name
            order_id = order.id

            if order.state != 'picking':
                res = False
                continue

            user_name = track.create_uid.name
            note = order.report_history
            move = order.move_lines[0]
            if track.move:
                move = stockmove_obj.browse(cr, uid, int(track.move), context=context)
            # for move in order.move_lines:
            bin = move.bin or move.bin_id and move.bin_id.name or False
            # Logging, un-combine and workflow rollback to shelf
            note += "\n%s %s: Stock not found. Adjust 0 for bin %s" % (
                time.strftime('%m-%d %H:%M:%S', time.gmtime()), user_name, bin)
            self.send_mail(cr, uid, ids)

            picking_obj.write(cr, uid, order_id, {'report_history': note})
            picking_obj.action_separate_stock_pickings(cr, uid, [order_id], context={'active_ids': [order_id]})

            wf_service.trg_validate(uid, 'stock.picking', order_id, 'action_reship', cr)
            wf_service.trg_validate(uid, 'stock.picking', order_id, 'button_back', cr)

            inventory_ids = []

            size_str = move.size_id and move.size_id.name or ''
            size_id = move.size_id and move.size_id.id
            product_id = move.product_id.id
            loc_id = move.location_id.id

            # Force sync stock for product
            sale_order_obj.sync_product_stock(cr, uid, product_id, size_id or False, context=context)

            # Adjust 0
            inventory_id = inventory_obj.create(cr, uid, {
                'name': _('INV: %s%s') % (tools.ustr(move.product_id.name), size_str),
                'reason': 'Stock not found. Adjust 0',
            }, context=context)

            inventory_line_obj.create(cr, uid, {
                'inventory_id': inventory_id,
                'product_qty': 0,
                'location_id': loc_id,
                'product_id': product_id,
                'product_uom': move.product_id.uom_id.id,
                'prod_lot_id': move.prodlot_id.id,
                'size_id': size_id,
                'bin': bin,
                'bin_id': move.bin_id and move.bin_id.id or False,
            }, context=context)
            inventory_ids.append(inventory_id)

            inventory_obj.action_confirm(cr, uid, inventory_ids, context=context)
            inventory_obj.action_done(cr, uid, inventory_ids, context=context)
            inventory_obj.force_adjustment_pending(cr, uid, product_id, size_id, loc_id, bin, 0, invoiceno=inv_no)
            # Check availability again
            wf_service.trg_validate(uid, 'stock.picking', order_id, 'button_reassign', cr)

        return res

    # Cron function
    def cron_adjust_zero_stock(self, cr, uid, context=None):
        print "cron_adjust_zero_stock"
        obj_ids = self.search(cr, uid, [('picking_id', '!=', False), ('status', '=', 'New')])
        for obj_id in obj_ids:
            cr.execute('SELECT create_uid, name from stock_not_found WHERE id=%s', (obj_id,))
            user_id = 0
            for snf in cr.fetchall():
                user_id = snf[0]
                invoice_name = snf[1]

            try:
                if self.adjust_zero_stock(cr, uid, [obj_id], context=context):
                    self.write(cr, uid, [obj_id], {'status': 'Done'})
                else:
                    self.write(cr, uid, [obj_id], {'status': 'Error'})
            except:
                continue

            if user_id > 0:
                user = self.pool.get('res.users').browse(cr, user_id, user_id)
                user_creator = 'openerp_%s' % (user.name or '')

                server_data = self.pool.get('fetchdb.server')
                server_id = server_data.get_sale_servers(cr, uid, single=True)

                transaction_zerostock_sql = """SELECT transactdate, itemid, itemdescription, bin, warehouse, stockid FROM transactions WHERE invoiceno = 'ZEROSTOCK %s'""" % (invoice_name,)
                transaction_zerostock = server_data.make_query(cr, uid, server_id, transaction_zerostock_sql, select=True, commit=False)

                if transaction_zerostock:
                    transaction_sql = """
                        SELECT id
                        FROM transactions
                        WHERE DATEDIFF(minute, transactdate, '%s') BETWEEN -10 and 10
                            AND itemid = '%s'
                            AND bin = '%s'
                            AND warehouse = '%s'
                            AND stockid = '%s'
                            AND invoiceno = 'ADJDEF'
                        ORDER BY id DESC
                    """ % (transaction_zerostock[0][0].strftime("%Y-%m-%d %H:%M:%S"), transaction_zerostock[0][1], transaction_zerostock[0][3], transaction_zerostock[0][4], transaction_zerostock[0][5],)
                    transaction_id = server_data.make_query(cr, uid, server_id, transaction_sql, select=True, commit=False)

                    if transaction_id:
                        transaction_update_sql = """
                            UPDATE transactions
                                SET
                                    _user_creator='%s',
                                    _user_modificator='%s',
                                    transactioncode='CDF'
                            WHERE id=%s""" % (user_creator, user_creator, transaction_id[0][0],)
                        server_data.make_query(cr, uid, server_id, transaction_update_sql, select=False, commit=True)

        return True

    def scan_order(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        picking_obj = self.pool.get('stock.picking')

        for track in self.browse(cr, uid, ids, context=context):
            if context.get('new_picking_id', False):
                order_id = context.get('new_picking_id', False)
                ids = context.get('stock_not_found_id', False)
            else:
                # Get order
                search_str = track.name or ''
                search_str = search_str.replace('\n', '').replace('\t', '').strip()

                order_ids = picking_obj.search(cr, uid, [('name', '=', search_str)])
                if not order_ids:
                    raise osv.except_osv(_('Warning'), 'Order not found.')

                order_id = order_ids[0]

            order = picking_obj.browse(cr, uid, order_id)
            if context.get('new_picking_id', False):
                search_str = order.name

            if order.state != 'picking':
                raise osv.except_osv(_('Warning'), 'The order is not in the state "Waiting Tracking Number".')

            if order.move_lines and len(order.move_lines) > 1:
                context.update({'stock_not_found_id': ids})
                return self.do_split(cr, uid, order.id, context)

            if self.search(cr, uid, [('picking_id', '=', order_id), ('status', '=', 'New')]):
                raise osv.except_osv(_('Warning'), 'The order is in queue already.')

            self.create(cr, uid, {'picking_id': order_id, 'status': 'New', 'name': search_str})

            note = 'Order %s will be processed' % (search_str,)
            self.write(cr, uid, [track.id], {'note': note, 'name': ''})

        return True

    def scan_split_order(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        picking_obj = self.pool.get('stock.picking')

        if context.get('new_picking_id', False):
            order_id = context.get('new_picking_id', False)
            order = picking_obj.browse(cr, uid, order_id)
            search_str = order.name

            # if len(order.move_lines) > 1:
            #     raise osv.except_osv(_('Warning'), 'Please choise only one line for stock not found')

            self.create(cr, uid, {'picking_id': order_id, 'status': 'New', 'name': search_str})

        return {'type': 'ir.actions.act_window_close'}

    def do_split(self, cr, uid, picking_id, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        context.update({
            'picking_id': picking_id
        })
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_not_found_split')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])

        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'context': context,
        })
        return act_win

    def send_mail(self, cr, uid, ids, context=None):

        if not ids:
            return False

        if isinstance(ids, (int, long)):
            ids = [ids]

        email_context = {}

        tmpl_obj = self.pool.get('email.template')
        data_obj = self.pool.get('ir.model.data')

        tmpl_id = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'email_template_stock_not_found')

        if tmpl_id:
            mng_grp = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_stock_analysis')
            email_list = self.pool.get('res.groups').get_user_emails(cr, uid, [mng_grp[1]])[mng_grp[1]] or []

            email_list.append(self.pool.get('ir.config_parameter').get_param(cr, uid, 'support.email.list'))

            email_context.update({
                'email_to': ", ".join(email_list)
            })

            for task_id in ids:
                tmpl_obj.send_mail(cr, uid, tmpl_id[1], task_id, force_send=False, context=email_context)

        return True

    def _get_lines(self, cr, uid, picking_id, location_id=None):
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id)
        result = []
        for move in picking.move_lines:
            if move.state not in ('assigned', 'reserved', 'done'):
                continue
            if not location_id or location_id == move.location_id.id:
                bin_to = move.bin_id.name if move.bin_id else move.bin
            else:
                bin_to = False
            result.append({
                'move_id': move.id,
                'product_id': move.product_id.id,
                'size_id': move.size_id.id or False,
                'product_qty': move.product_qty,
                'price_unit': move.price_unit,
                'location_id': location_id or move.location_id.id,
                'bin_to': bin_to,
            })
        return result


stock_not_found()


class stock_not_found_list(osv.osv_memory):
    _name = "stock.not.found.memory"
    _rec_name = 'product_id'
    _columns = {
        'product_id': fields.many2one('product.product', string="Product", required=True),
        'wizard_id': fields.many2one('stock.return.picking', string="Wizard"),
        'move_id': fields.many2one('stock.move', "Move"),
    }

    _order = "create_date desc"


stock_not_found_list()
