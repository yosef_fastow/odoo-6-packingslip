# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#
# May be uncommented to logs workflows modifications

from workflow import wkf_logs
from osv import fields, osv
import time
from stock_picking import STOCK_PICKING_STATES
from sale import SALE_ORDER_STATES


def _log(cr, ident, act_id, info=''):

    if ident[1] == 'stock.picking' and info == 'complete':
        cr.execute('INSERT INTO stock_picking_workflow_log (create_uid, create_date, act_id, info, res_id) VALUES (%s, %s, %s, %s, %s)', (ident[0], time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), act_id, info, ident[2]))
        expr = "wkf_activity.name"
        for state in STOCK_PICKING_STATES:
            expr = "replace(%s, '%s', '%s')" % (expr, state[0], state[1])

        cr.execute("UPDATE stock_picking SET report_history = concat(report_history,  E'\n', '%s', ' ', res_users.name, ': ', %s) FROM res_users, wkf_activity WHERE stock_picking.id=%s AND res_users.id=%s AND wkf_activity.id=%s" % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), expr, ident[2], ident[0], act_id))

    if ident[1] == 'sale.order' and info == 'complete':
        cr.execute('INSERT INTO sale_order_workflow_log (create_uid, create_date, act_id, info, res_id) VALUES (%s, %s, %s, %s, %s)', (ident[0], time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), act_id, info, ident[2]))
        expr = "wkf_activity.name"
        for state in SALE_ORDER_STATES:
            expr = "replace(%s, '%s', '%s')" % (expr, state[0], state[1])

        cr.execute("UPDATE sale_order SET report_history = concat(report_history,  E'\n', '%s', ' ', res_users.name, ': ', %s) FROM res_users, wkf_activity WHERE sale_order.id=%s AND res_users.id=%s AND wkf_activity.id=%s" % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), expr, ident[2], ident[0], act_id))
    return
    #    msg = """
    #res_type: %r
    #res_id: %d
    #uid: %d
    #act_id: %d
    #info: %s
    #""" % (ident[1], ident[2], ident[0], act_id, info)

    #cr.execute('insert into wkf_logs (res_type, res_id, uid, act_id, time, info) values (%s,%s,%s,%s,current_time,%s)', (ident[1],int(ident[2]),int(ident[0]),int(act_id),info))

wkf_logs.log = _log


class stock_picking_workflow_log(osv.osv):
    _name = "stock.picking.workflow.log"

    def name_get(self, cr, uid, ids, context=None):
        res = []
        if ids:
            cr.execute("""
                SELECT lg.id, concat(ru.name, ':', sp.name, ':', ac.name)
                FROM stock_picking_workflow_log lg
                    left join res_users ru on lg.create_uid = ru.id
                    left join stock_picking sp on sp.id = lg.res_id
                    left join wkf_activity ac on lg.act_id = ac.id
                where 1=1
                    and lg.id in %s
                """, (tuple(ids), )
            )
            res = cr.fetchall() or []
        return res

    _columns = {
        'create_uid': fields.many2one('res.users', 'User'),
        'create_date': fields.datetime('Creation Date', required=True),
        'act_id': fields.many2one('workflow.activity', 'State'),
        'info': fields.char('Info', size=256),
        'res_id': fields.many2one('stock.picking', 'Delivery Order'),
    }

stock_picking_workflow_log()


class sale_order_workflow_log(osv.osv):
    _name = "sale.order.workflow.log"

    def name_get(self, cr, uid, ids, context=None):
        res = []
        if ids:
            cr.execute("""
                SELECT lg.id, concat(ru.name, ':', so.name, ':', ac.name)
                FROM stock_picking_workflow_log lg
                    left join res_users ru on lg.create_uid = ru.id
                    left join sale_order so on so.id = lg.res_id
                    left join wkf_activity ac on lg.act_id = ac.id
                where 1=1
                    and lg.id in %s
                """, (tuple(ids), )
            )
            res = cr.fetchall() or []
        return res

    _columns = {
        'create_uid': fields.many2one('res.users', 'User'),
        'create_date': fields.datetime('Creation Date', required=True),
        'act_id': fields.many2one('workflow.activity', 'State'),
        'info': fields.char('Info', size=256),
        'res_id': fields.many2one('sale.order', 'Sale Order'),
    }

sale_order_workflow_log()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: