# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import logging
import tools
import time
import re
from tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from pf_utils.utils.label_printer import LabelWizPrinter
from openerp.addons.delmar_sale.lib.product.product_factory import ProductFactory

BIN_FOR_RELEASE = 'POST'

logger = logging.getLogger('delmar_sale')


class stock_partial_picking(osv.osv_memory):
    _inherit = "stock.partial.picking"

    _columns = {
        'reason': fields.related('picking_id', 'return_reason', type='text', relation='stock.picking', string='Reason', store=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}

        all_states = context.get('all_states', False)

        res = super(stock_partial_picking, self).default_get(cr, uid, fields, context=context)
        picking_ids = context.get('active_ids', [])
        if not picking_ids or (not context.get('active_model') == 'stock.picking') \
            or len(picking_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        picking_id, = picking_ids
        if 'picking_id' in fields:
            res.update(picking_id=picking_id)
        if 'move_ids' in fields:
            picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
            moves = [self._partial_move_for(cr, uid, m, context=context) for m in picking.move_lines if all_states or m.state not in ('done', 'cancel')]
            res.update(move_ids=moves)
        if 'date' in fields:
            res.update(date=time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        return res

    def _partial_move_for(self, cr, uid, move, context=None):
        if context is None:
            context = {}

        default_type = context.get('default_type', 'base')

        bin_obj = self.pool.get('stock.bin')
        loc_obj = self.pool.get('stock.location')
        all_states = context.get('all_states', False)

        bin_id = None

        partner_id = move.picking_id and move.picking_id.real_partner_id and move.picking_id.real_partner_id.id or False
        return_partner_reason = False

        r_location, r_bin = self.pool.get('stock.picking.return.line').get_source_location_for_return(
            cr, uid,
            move.product_id.id,
            move.size_id.id or False,
            None,
            r_location_id=move.location_id.id or False,
            ignore_exceptions=True,
            context=None)

        if r_bin:
            bin_id = r_bin.id or False

        if not bin_id and move.bin_id:
            bin_id = move.bin_id.id

        if not bin_id and move.bin:
            bin_ids = bin_obj.search(cr, uid, [('name', '=', move.bin)])
            if bin_ids:
                bin_id = bin_ids[0]

        if partner_id:
            return_reason_default = self.pool.get('stock.partner.return').get_default_customer(cr, uid, partner_id)
            if return_reason_default:
                return_partner_reason = return_reason_default.id

        partial_move = {
            'product_id': move.product_id.id,
            'quantity': (all_states or move.state in ('assigned', 'new')) and move.product_qty or 0,
            'product_uom': move.product_uom.id,
            'prodlot_id': move.prodlot_id.id,
            'move_id': move.id,
            'location_dest_id': move.location_dest_id.id,
            'location_id': move.location_id.id,
            'bin_id': bin_id,
            'partner_id': partner_id,
            'return_partner_reason': return_partner_reason,
            'size_id': move.size_id.id or False,
            'cost': move.sale_line_id.price_unit or 0,
        }

        if default_type == 'return':
            return_location_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('name', '=', 'RETURN'), ('complete_name', 'ilike', '%% usreturn %% stock %%')])
            partial_move.update({
                'location_id': False if not return_location_ids else return_location_ids[0],
                'bin_id': False,
            })

        if move.picking_id.type == 'in' and move.product_id.cost_method == 'average':
            partial_move.update(update_cost=True, **self._product_cost_for_average_update(cr, uid, move))
        return partial_move

    def save_rma(self, cr, uid, ids, context=None):

        return {'type': 'ir.actions.act_window_close'}

    def do_only_return(self, cr, uid, ids, context=None):
        logger.warn("Run Stock Partial Picking -> do_only_return")
        assert len(ids) == 1, 'Partial picking return may only be done one at a time'
        stock_picking = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        partial = self.browse(cr, uid, ids[0], context=context)

        customer = partial.picking_id.real_partner_id
        customer_product = ProductFactory.factory(customer.name, cr, uid, self.pool)

        not_returned_product = set()
        for product_line in partial.move_ids:

            can_returning = customer_product.check_that_can_returning(
                product_line.product_id,
                customer,
                product_line.size_id
            )
            product_name = product_line.product_id.default_code
            if not can_returning:
                not_returned_product.add(product_name)

        if not_returned_product:
            raise osv.except_osv(
                _('Warning!'),
                _('Return for this items: {} is not allowed!'.format(' '.join(not_returned_product)))
            )

        partial_data = {
            'delivery_date': partial.date
        }
        user = self.pool.get('res.users').browse(cr, uid, uid)
        note = '%s \n%s %s: Return some products: \n' % (partial.picking_id.report_history, time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name)
        for wizard_line in partial.move_ids:

            move_id = wizard_line.move_id.id
            move = move_obj.browse(cr, uid, move_id)

            #if move.is_set_component:
            #    locations_rule = self.check_locations_set_components(cr, uid, partial.picking_id.id, context=context)
            #    if locations_rule:
            #        raise osv.except_osv(_('Warning!'), _('Direct return of set component not allowed !'))

            #Quantiny must be Positive
            if not wizard_line.quantity or not wizard_line.quantity > 0:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            if wizard_line.quantity > wizard_line.move_id.product_qty:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            # if not wizard_line.rma_number:
            #     raise osv.except_osv(_('Warning!'), _('Please specify all RMA numbers !'))

            if not wizard_line.location_id:
                raise osv.except_osv(_('Warning!'), _('Please specify Location !'))

            return_reason = wizard_line.return_partner_reason and wizard_line.return_partner_reason.value or False
            return_reason_id = wizard_line.return_partner_reason and wizard_line.return_partner_reason.id or False

            if return_reason is False:
                return_reason_default = self.pool.get('stock.partner.return').get_default_customer(cr, uid, wizard_line.partner_id.id)
                if return_reason_default:
                    return_reason = return_reason_default.value
                    return_reason_id = return_reason_default.id

            # dlmr-866 - check generate bin
            if wizard_line.generate_bin and wizard_line.location_id and not wizard_line.bin_id:
                logger.info("Need to generate bin")

            partial_data['move%s' % (move_id)] = {
                'line_id': wizard_line.id,
                'product_id': wizard_line.product_id.id,
                'product_qty': wizard_line.quantity,
                'product_uom': wizard_line.product_uom.id,
                'prodlot_id': wizard_line.prodlot_id.id,
                'location_id': wizard_line.location_id and wizard_line.location_id.id or False,
                'bin_id': wizard_line.bin_id and wizard_line.bin_id.id or False,
                'generate_bin': wizard_line.generate_bin or False,
                'return_reason': return_reason,
                'return_reason_id': return_reason_id,
                'last_return_reason': partial and partial.reason or False,
                'is_set': wizard_line.is_set or False,
            }

        res = stock_picking.do_only_return(cr, uid, [partial.picking_id.id], partial_data, context=context)

        returned_picking_id = res[partial.picking_id.id].get('returned_picking', False)
        if returned_picking_id:
            returned_picking_note = res[partial.picking_id.id].get('return_note', False)
            if returned_picking_note:
                note += returned_picking_note
            if partial.reason:
                note += 'By reason: %s' % (partial.reason)

            stock_picking.write(cr, uid, partial.picking_id.id, {'report_history': note})

            return stock_picking.print_fly_qr_label(cr, uid, [returned_picking_id], context=context)
        else:
            return True


    def check_locations_set_components(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')
        stock_moves = move_obj.search(cr, uid, [('set_parent_order_id', '=', ids)])
        prev_loc_id = ''
        for cline in move_obj.browse(cr, uid, stock_moves):
            if prev_loc_id:
                if prev_loc_id != cline.location_id.id:
                    return True
            prev_loc_id = cline.location_id.id

        return False

    def do_partial(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'Partial picking processing may only be done one at a time'
        stock_picking = self.pool.get('stock.picking')
        stock_move = self.pool.get('stock.move')
        uom_obj = self.pool.get('product.uom')
        partial = self.browse(cr, uid, ids[0], context=context)
        partial_data = {
            'delivery_date': partial.date
        }
        picking_type = partial.picking_id.type

        if not partial.move_ids:
            raise osv.except_osv(_('Warning!'), _('Nothing to process in stock.py:do_partial!'))

        for wizard_line in partial.move_ids:
            line_uom = wizard_line.product_uom
            move_id = wizard_line.move_id.id

            #Quantiny must be Positive
            if not wizard_line.quantity or not wizard_line.quantity > 0:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            if wizard_line.quantity > wizard_line.move_id.product_qty:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            # Compute the quantity for respective wizard_line in the line uom (this just round if necessary)
            qty_in_line_uom = uom_obj._compute_qty(cr, uid, line_uom.id, wizard_line.quantity, line_uom.id)

            if line_uom.factor and line_uom.factor != 0:
                if qty_in_line_uom != wizard_line.quantity:
                    raise osv.except_osv(_('Warning'), _('The uom rounding does not allow you to ship "%s %s", only roundings of "%s %s" is accepted by the uom.') % (wizard_line.quantity, line_uom.name, line_uom.rounding, line_uom.name))
            if move_id:
                #Check rounding Quantity.ex.
                #picking: 1kg, uom kg rounding = 0.01 (rounding to 10g),
                #partial delivery: 253g
                #=> result= refused, as the qty left on picking would be 0.747kg and only 0.75 is accepted by the uom.
                initial_uom = wizard_line.move_id.product_uom
                #Compute the quantity for respective wizard_line in the initial uom
                qty_in_initial_uom = uom_obj._compute_qty(cr, uid, line_uom.id, wizard_line.quantity, initial_uom.id)
                without_rounding_qty = (wizard_line.quantity / line_uom.factor) * initial_uom.factor
                if qty_in_initial_uom != without_rounding_qty:
                    raise osv.except_osv(
                        _('Warning'),
                        _('The rounding of the initial uom does not allow you to ship "%s %s", as it would let a quantity of "%s %s" to ship and only roundings of "%s %s" is accepted by the uom.') % (
                            wizard_line.quantity,
                            line_uom.name,
                            wizard_line.move_id.product_qty - without_rounding_qty,
                            initial_uom.name,
                            initial_uom.rounding,
                            initial_uom.name
                        )
                    )
            else:
                seq_obj_name = 'stock.picking.' + picking_type
                move_id = stock_move.create(cr, uid, {
                    'name': self.pool.get('ir.sequence').get(cr, uid, seq_obj_name),
                    'product_id': wizard_line.product_id.id,
                    'product_qty': wizard_line.quantity,
                    'product_uom': wizard_line.product_uom.id,
                    'prodlot_id': wizard_line.prodlot_id.id,
                    'location_id': wizard_line.location_id.id,
                    'location_dest_id': wizard_line.location_dest_id.id,
                    'picking_id': partial.picking_id.id
                    }, context=context)
                stock_move.action_confirm(cr, uid, [move_id], context)
            partial_data['move%s' % (move_id)] = {
                'line_id': wizard_line.id,
                'product_id': wizard_line.product_id.id,
                'product_qty': wizard_line.quantity,
                'product_uom': wizard_line.product_uom.id,
                'prodlot_id': wizard_line.prodlot_id.id,
                'size_id': wizard_line.size_id.id,
            }
            if (picking_type == 'in') and (wizard_line.product_id.cost_method == 'average'):
                partial_data['move%s' % (wizard_line.move_id.id)].update(product_price=wizard_line.cost,
                                                                  product_currency=wizard_line.currency.id)
        res = stock_picking.do_partial(cr, uid, [partial.picking_id.id], partial_data, context=context)
        return res

    def action_print_qr_return_labels(self, cr, uid, ids, context=None):
        return True

    def action_print_labels(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        act = True
        partial_line_ids = []
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.report.xml')

        for partial in self.browse(cr, uid, ids):
            for line in partial.move_ids:

                if not line.quantity:
                    continue
                # if line.quantity < 0:
                #     raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))
                # if line.quantity > line.move_id.product_qty:
                #     raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

                partial_line_ids.append(line.id)

        if partial_line_ids:
            datas = {
                'ids': partial_line_ids,
                }

            act_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'stock_partial_picking_line')
            act_id = data_obj.read(cr, uid, act_res_id, ['res_id'])
            act = act_obj.read(cr, uid, act_id['res_id'], [], context=context)
            act.update({
                'datas': datas
            })

        return act

stock_partial_picking()


class stock_partial_picking_line(osv.osv):
    _inherit = "stock.partial.picking.line"

    def _item(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            item = False
            if line.product_id:
                prod = line.product_id.default_code or ''
                size = line.move_id and line.move_id.size_id and line.move_id.size_id.name or '0'
                size = str(float(size)) if float(size) % 1 else str(int(float(size)))
                item = "%s/%s" % (prod, size)

            res[line.id] = item

        return res

    def _get_image(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.product_id and line.product_id.product_image or False
        return res

    def _get_is_set(self, cr, uid, ids, name, args, context=None):
        res = {}
        helper = self.pool.get('stock.picking.helper')
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.product_id and helper.check_product_is_set(cr, uid, product_id=line.product_id.id) or False
        return res

    def _get_wh_identifier(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if line.product_id.calculated_standard_price < 250:
                wh = 'MONTREAL'
            elif 250 <= line.product_id.calculated_standard_price < 1000:
                wh = "CHAMPLAIN"
            else:
                wh = "HIGH VALUE"

            res[line['id']] = wh
        return res

    _columns = {
        'item': fields.function(_item, string='Item', type='char', method=True),
        'create_uid': fields.many2one('res.users', 'Created by', readonly=True),
        'location_id': fields.many2one('stock.location', 'Location'),
        'bin_id': fields.many2one('stock.bin', 'Bin'),
        'generate_bin': fields.boolean('Generate Bin(s)', required=False),
        'location': fields.char('New location', size=255),
        'rma_number': fields.related('move_id', 'rma_number', type='char', relation='stock.move', string='RMA', store=True),
        'return_partner_reason': fields.related('move_id', 'return_partner_reason', type='many2one',
                                                relation='stock.partner.return', string="Reason", store=True, ),
        'partner_id': fields.many2one('res.partner', string='Partner'),
        'size_id': fields.many2one('ring.size', string='Size', readonly=True),
        'product_image': fields.function(_get_image, string="Image", type='binary', method=True, store=False, ),
        'is_set': fields.function(_get_is_set, string="Set", type='boolean', method=True, store=False, ),
        'wh_identifier': fields.function(_get_wh_identifier, string="WH Ident", type='char', method=True, store=False, ),
    }
    _defaults = {
        'generate_bin': False,
    }

    def location_id_change(self, cr, uid, ids, product_id, location_id, size_id, context=None):
        logger.warn('Location change initiated.')
        msrl = self.pool.get('stock.picking.mass.return.line')
        helper = self.pool.get('stock.picking.helper')
        if helper.check_product_is_set(cr, uid, product_id=product_id):
            return_value = {
                'bin_id': False,
                'generate_bin': True
            }
            return_domain = {}
        else:
            res = msrl.location_change(cr, uid, [], product_id=product_id, size_id=size_id, return_location=location_id, return_bin=False, return_target='selected', context=None)
            logger.warn("Location change result: %s" % res)
            return_value = {
                'bin_id': res.get('domain')['return_bin'][0][2][0] if len(res.get('domain')['return_bin'][0][2]) == 1 else False,
            }
            return_domain = {
                'bin_id': res.get('domain').get('return_bin')
            }

        return_data = {
            'value': return_value,
            'domain': return_domain
        }
        return return_data

stock_partial_picking_line()


class resolution_code(osv.osv):
    _name = "stock.move.exception.resolution_code"
    _description = 'Stock move exception resolution codes'

    _columns = {
        'name': fields.char('Resolution Code', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The finishing of the finishing_labour must be unique !')
    ]
resolution_code()


# STOCK INVOICE REPORT
class stock_invoice_report_line(osv.osv):
    _name = "stock.invoice.report.line"

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', select=True),
        'picking_id': fields.many2one('stock.picking', 'Order'),
        'product_qty': fields.float('Qty'),
        'cost': fields.float('Cost'),
        'group_id': fields.many2one('stock.invoice.report.group', 'Report', ondelete='cascade', select=True),
    }

stock_invoice_report_line()


class stock_invoice_report_group(osv.osv):
    _name = "stock.invoice.report.group"

    _columns = {
        'country': fields.char('Country', size=256),
        'tariff': fields.char('Tariff', size=256),
        'report_id': fields.many2one('stock.invoice.report', 'Report', ondelete='cascade', select=True),
        'report_line_ids': fields.one2many('stock.invoice.report.line', 'group_id', 'Report lines'),
    }

stock_invoice_report_group()


class stock_invoice_report(osv.osv):
    _name = "stock.invoice.report"

    _columns = {
        'stock_move_ids': fields.many2many('stock.move', 'stock_invoice_report_move_rel', 'move_id', 'report_id'),
        'stock_picking_ids': fields.many2many('stock.picking', 'stock_invoice_report_pcking_rel', 'picking_id', 'report_id'),
        'report_group_ids': fields.one2many('stock.invoice.report.group', 'report_id', 'Report lines'),
    }

stock_invoice_report()


def _get_selected_reason(self, cr, uid, context=None):
    SELECTED_REASON_DEFAULT_SELECTION = [
        ("Approved to send wrong size", "Approved to send wrong size"),
        ("Could not locate", "Could not locate"),
        ("Damaged item", "Damaged item"),
        ("Fixing negative", "Fixing negative"),
        ("QC Reject", "QC Reject"),
        ("For Order (put what order )", "For Order (put what order )"),
        ("Found", "Found"),
        ("MTL Ship To NY", "MTL Ship To NY"),
        ("Per ( put who)", "Per ( put who)"),
        ("Polished", "Polished"),
        ("Repaired", "Repaired"),
        ("Return - Wrong Item", "Return - Wrong Item"),
        ("Return - Wrong Size", "Return - Wrong Size"),
        ("RTV Item per Shaul", "RTV Item per Shaul"),
        ("ent to mtl", "ent to mtl"),
        ("Sizing", "Sizing"),
        ("Took to be repaired", "Took to be repaired"),
        ("Was sized", "Was sized"),
        ("Wrong Item", "Wrong Item"),
        ("OTHER", "OTHER"),
    ]
    _selected_reason = []
    try:
        conf_obj = self.pool.get('ir.config_parameter')
        _selected_reason_obj = conf_obj.get_param(cr, uid, 'delmar.sale.selected.reason')
        _selected_reason_raw = eval(_selected_reason_obj) if _selected_reason_obj else False
        if(not _selected_reason_raw):
            raise Exception('Not found delmar.sale.selected.reason parametr')
        for _reason in _selected_reason_raw:
            temp_obj = []
            temp_obj.append(_reason)
            temp_obj.append(_reason)
            _selected_reason.append(temp_obj)
        _selected_reason.append(["OTHER", "OTHER"])
    except Exception as _err:
        logger.error("Error get selected reason settings: {0}".format(str(_err.message)))
        _selected_reason = SELECTED_REASON_DEFAULT_SELECTION

    return (_selected_reason)


# STOCK PRODUCT REPORT
class stock_product_move(osv.osv):
    _name = "stock.product.move"

    def _get_name(self, cr, uid, ids, fn, args, context=None):
        res = {}
        for move in self.browse(cr, uid, ids):
            res[move.id] = "%s %s/%s" % (move.create_date, move.line_id.item, move.line_id.size)
        return res

    def new_location_id_change(self, cr, uid, ids, location_id, context=None):
        prod_obj = self.pool.get('product.product')
        bin_obj = self.pool.get('stock.bin')
        report_line_obj = self.pool.get('stock.product.report.line')

        product_id = context.get('product_id', False)
        from_id = context.get('active_id', False)
        is_move_reserved = context.get('move_reserved', False)

        size_id = False
        excluding_bins = []
        if from_id:
            loc_from = report_line_obj.browse(cr, uid, from_id)
            size = loc_from.size
            if from_id == location_id:
                excluding_bins.append(loc_from.bin)
            if size:
                size_name = str(float(size) / 100)
                size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                if size_id and size_id[0]:
                    size_id = size_id[0]

        context = context or {}
        result = {
            'new_location_id': location_id,
            'new_bin_id': False,
        }

        bin_ids_arr = prod_obj.get_mssql_product_bins(cr, uid, [product_id], [location_id], [size_id], excluding_bins)
        if not bin_ids_arr:
            bin_ids_arr = []
        else:
            bin_ids_arr = list(bin_ids_arr)

        if is_move_reserved:
            incoming_bin_id = bin_obj.search(cr, uid, [('name', '=', 'INCOMING')])
            if not incoming_bin_id:
                incoming_bin_id = [bin_obj.create(cr, uid, {'name': 'INCOMING'}, context=context)]

            bin_ids_arr.append(incoming_bin_id[0])

        domain = {'new_bin_id': [('id', 'in', bin_ids_arr)]}

        return {'value': result, 'domain': domain}

    _columns = {
        'name': fields.function(_get_name, string="Name", method=True, type="char"),
        'create_date': fields.datetime('Date', required=True, select=True),
        'create_uid': fields.many2one('res.users', 'Creator'),
        'line_id': fields.many2one('stock.product.report.line', 'Line'),
        'qty': fields.integer('Qty'),
        'new_warehouse': fields.char('Warehouse', size=256),
        'new_stock': fields.char('Stock', size=256),
        'new_location_id': fields.many2one('stock.location',
                                           'Destination Location',
                                           required=True,
                                           select=True,
                                           domain = ['&', ('complete_name', 'like', '/ Stock /'), ('usage','=','internal')]),
        'new_bin_id': fields.many2one( 'stock.bin',
                                       'Destination Bin',
                                       required=True,
                                       select=True),
        # 'new_bin': fields.char('Bin', size=256),
        'selected_reason': fields.selection(_get_selected_reason, 'Select reason', required=True,),
        'reason': fields.text('Reason'),
    }

    _defaults = {
        'selected_reason': 'OTHER',
    }

    def do_bin_to_bin_move(self, cr, uid, from_data, to_data, item, size, qty,
        prod_desc, reason, reserved=False, server_id=False, allow_partial=False
    ):
        if not qty or qty < 0:
            raise osv.except_osv(_('Warning !'), 'Qty must be positive!')
        server_obj = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_obj.get_sale_servers(cr, uid, single=True)
        stock_move_obj = self.pool.get('stock.move')
        bin_obj = self.pool.get('stock.bin')

        warehouse = from_data['warehouse']
        new_warehouse = to_data['warehouse']
        if warehouse != new_warehouse and 'CAFER' in (warehouse, new_warehouse):
            raise osv.except_osv(
                _('Warning !'),
                'Moves between warehouses are locked for CAFER!',
            )
        stock = from_data['stock']
        bin_name = from_data['bin_name']
        new_stock = to_data['stock']
        new_bin_name = to_data['bin_name']

        itemid = item and '%s/%s' % (item, size) or 'null'
        reserved_flag_from = stock_move_obj.get_reserved_flag(
            cr, uid, itemid, warehouse, stock, server_id
        )
        reserved_flag_to = reserved_flag_from
        is_another_stock = (warehouse, stock) != (new_warehouse, new_stock)
        if new_warehouse and new_stock and is_another_stock:
            reserved_flag_to = stock_move_obj.get_reserved_flag(
                cr, uid, itemid, new_warehouse, new_stock, server_id
            )

        invredid_from = stock_move_obj.get_invredid_for_mssql_row(
            cr, uid, itemid, warehouse, stock, bin_name
        )

        if reserved and new_bin_name == "INCOMING":
            invredid_to = 1
        else:
            invredid_to = stock_move_obj.get_invredid_for_mssql_row(
                cr, uid, itemid, new_warehouse, new_stock, new_bin_name
            )
        if not (invredid_from and invredid_to):
            raise osv.except_osv(
                _('Warning !'),
                'Can`t find invredid (Maybe you\'re trying to use a bin that not exists?).',
            )

        if reserved:
            trn_code_from = 'SHP'
            trn_code_to = 'RCV'
        else:
            trn_code_from = 'ADJ'
            trn_code_to = 'ADJ'

        actual_qty = bin_obj.get_qty_for_bin_item_stock(
            cr, uid, item, size, warehouse, bin_name, stock, server_id
        )

        if actual_qty < qty:
            if not allow_partial:
                raise osv.except_osv(
                    _('Warning !'),
                    'There is no enough qty on the shelf.',
                )
            elif qty <= 0:
                raise osv.except_osv(_('Warning !'), 'The bin is empty.')
            else:
                qty = actual_qty

        timestasmp = int(time.time())
        invoiceno = 'B2B' + str(timestasmp)

        move_params_from = [
            itemid,  # itemid
            trn_code_from,  # transactioncode
            qty,  # qty
            warehouse or None,  # warehouse
            prod_desc or None,  # itemdescription
            bin_name or '',  # bin
            stock or None,  # stockid
            'TRS',  # transactiontype
            item,  # id_delmar
            '-1',  # sign
            (-1 * qty) or '0',  # adjusted_qty
            size,  # size
            invredid_from,  # invredid
            None,  # customer id
            item,  # ProductCode
            invoiceno,  # invoiceno
            reserved_flag_from or 0,  # reserved
            None,  # trans_tag
            0,  # unit price from sale_order_line
        ]

        move_params_to = [
            itemid,  # itemid
            trn_code_to,  # transactioncode
            qty,  # qty
            new_warehouse or None, # warehouse
            prod_desc or None,  # itemdescription
            new_bin_name or '', # bin
            new_stock or None, # stockid
            'TRS',  # transactiontype
            item,  # id_delmar
            '1',  # sign
            (1 * qty) or '0',  # adjusted_qty
            size,  # size
            invredid_to,  # invredid
            None,  # customer id
            item,  # ProductCode
            invoiceno,  # invoiceno
            reserved_flag_to or 0,  # reserved
            None,  # trans_tag
            0,  # unit price from sale_order_line
        ]
        mssql_conn = server_obj.connect(cr, uid, server_id)
        mssql_curs = mssql_conn.cursor()

        new_context = {'reason': reason or ''}
        sql_res_from = stock_move_obj.write_into_transactions_table(
            cr, uid, server_id,
            params=move_params_from,
            mssql_conn=mssql_conn,
            mssql_curs=mssql_curs,
            context=new_context
        )
        sql_res_to = stock_move_obj.write_into_transactions_table(
            cr, uid, server_id,
            params=move_params_to,
            mssql_conn=mssql_conn,
            mssql_curs=mssql_curs,
            context=new_context
        )

        sql_res = sql_res_from and sql_res_to
        if sql_res:
            mssql_conn.commit()
        else:
            mssql_conn.rollback()
        mssql_conn.close()

        if not sql_res:
            raise osv.except_osv(_('Warning !'), 'Can\'t process move.')

        return move_params_from, move_params_to

    def move_product(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning !'), _('Server not available.'))

        data_obj = self.pool.get('ir.model.data')
        template_obj = self.pool.get('email.template')
        loc_obj = self.pool.get('stock.location')
        prod_obj = self.pool.get('product.product')
        report_line_obj = self.pool.get('stock.product.report.line')
        sale_order_obj = self.pool.get('sale.order')
        res_log_obj = self.pool.get('res.log')

        prod_id = context.get('product_id', False)
        is_move_reserved = context.get('move_reserved', False)
        from_id = context.get('active_id', False)
        if not from_id:
            raise osv.except_osv(_('Warning !'), 'Location from not available')

        prod_name = prod_obj.read(cr, uid, prod_id, ['name']).get('name')

        loc_from = report_line_obj.browse(cr, uid, from_id)
        for move in self.browse(cr, uid, ids):
            if move.selected_reason != 'OTHER':
                reason = move.selected_reason
            else:
                reason = move.reason

            new_warehouse, new_stock = loc_obj.get_warehouse_and_location_names(
                cr, uid, move.new_location_id.id
            )

            item = loc_from.item and loc_from.item.strip() or False
            size = loc_from.size or '0000'

            from_data = {
                'warehouse': loc_from.warehouse,
                'stock': loc_from.stock,
                'bin_name': loc_from.bin,
            }
            to_data = {
                'warehouse': new_warehouse,
                'stock': new_stock,
                'bin_name': move.new_bin_id and move.new_bin_id.name or False,
            }

            move_params_from, move_params_to = self.do_bin_to_bin_move(
                cr, uid,
                from_data, to_data,
                item, size, move.qty, prod_name,
                reason, is_move_reserved, server_id,
            )

            res_log_obj.create(cr, uid, {
                'name': 'Move from bin (%s)' % ('success'),
                'res_model': 'stock.product.move',
                'res_id': move.id,
                'debug_information': 'params_from: ' + str(move_params_from) + '\n\nparams_to: ' + str(move_params_to),
            })

            email_context = context
            template = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'task_email_template_product_move')
            if template:
                cc_group = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_stock_analysis')
                email_cc = self.pool.get('res.groups').get_user_emails(cr, uid, [cc_group[1]])[cc_group[1]] or []
                email_context.update({'email_cc': ", ".join(email_cc)})
                if email_cc:
                    template_obj.send_mail(cr, uid, template[1], move.id, False, email_context)

        # FIXME: unbound local variable size.
        try:
            size_id = False
            if size != '0000':
                size_name = str(float(size) / 100)
                size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                if size_id and size_id[0]:
                    size_id = size_id[0]
            if prod_id:
                sale_order_obj.sync_product_stock(cr, uid, prod_id, size_id, context=context)
        except:
            server_obj.log(cr, uid, server_id, "Can`t sync stock")

        if context.get('target', False) == 'new':
            return_action_name = 'action_open_stock_product_report_new'
        else:
            return_action_name = 'action_open_stock_product_report'

        context.update({
            'flags': {'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False, 'can_be_discarded': True},
        })
        return self.get_act_win(cr, uid, return_action_name, prod_id, context=context)

    def get_act_win(self, cr, uid, act_window, prod_id, context=None, module_name='delmar_sale'):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, module_name, act_window)
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        target_str = ''
        if context.get('target') == 'new':
            target_str = ", 'target': 'new'"
        if prod_id:
            act_win['context'] = "{'default_product_id': %s%s}" % (prod_id, target_str)

        return act_win

stock_product_move()


class stock_product_report_line(osv.osv):
    _name = "stock.product.report.line"

    def _item(self, cr, uid, ids, fn, args, context=None):
        res = {}
        for move in self.browse(cr, uid, ids):
            size = 0
            if move.size:
                size = float(move.size) / 100
            size = str(size) if size % 1 else str(int(size))
            res[move.id] = "%s/%s" % (move.item, size)
        return res

    def _location(self, cr, uid, ids, fn, args, context=None):
        res = {}
        for move in self.browse(cr, uid, ids):
            res[move.id] = "%s/%s/%s" % (move.warehouse, move.stock, move.bin)

        return res

    def _get_link(self, cr, uid, ids, fn, args, context=None):
        res = {}
        if ids:
            base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'base.url.for.summary.link')
            base_url += '/inventory_report/count_manage/stock_summary'
            lines = self.read(cr, uid, ids, ['item', 'size', 'warehouse', 'stock', 'bin'])
            for line in lines:
                res[line['id']] = """%s?delmarID=%s/%s&warehouse=%s&location=%s""" % (
                    base_url,
                    line['item'],
                    line['size'],
                    line['warehouse'],
                    line['stock']
                )
        return res

    def _get_phantom(self, cr, uid, ids, fn, args, context=None):
        res = {}
        if ids:
            server_data = self.pool.get('fetchdb.server')

            server_id = server_data.get_main_servers(cr, uid, single=True)
            if not server_id:
                raise osv.except_osv(_('Warning!'), _('No connection to MSSQL!'))

            rep_lines = self.read(cr, uid, ids, ['item', 'size', 'warehouse', 'stock'])

            values = ', '.join([u"('{item}', '{size}', '{warehouse}', '{stock}', {id})".format(**item) for item in rep_lines])

            sql_cur_phantom = u"""SELECT DISTINCT v.id,
                    COALESCE(
                        (
                            SELECT TOP 1 phantom FROM transactions
                            WHERE phantom is NOT NULL
                                AND status <> 'Posted'
                                AND warehouse = v.warehouse
                                AND stockid = v.stock
                                AND id_delmar = v.item
                                AND size = v.size
                            ORDER BY transactions.id desc
                        ), 0
                    ) as phantom_qty
                FROM (VALUES {values}) v (item, size, warehouse, stock, id)
            """.format(values=values).replace(u'\xa0', u'\\xa0')
            cur_phantoms = server_data.make_query(cr, uid, server_id, sql_cur_phantom, select=True, commit=False)
            res = {cur_phantom[0]: cur_phantom[1] for cur_phantom in cur_phantoms}
        return res

    _columns = {
        'product_id': fields.many2one('product.product', 'Product'),
        'create_date': fields.datetime('Date', required=True, select=True),
        'create_uid': fields.many2one('res.users', 'Creator'),
        'name': fields.char('Name', size=256),
        'item': fields.char('Item', size=256),
        'size': fields.char('Size', size=4),
        'warehouse': fields.char('Warehouse', size=256),
        'stock': fields.char('Stock', size=256),
        'bin': fields.char('Bin', size=256),
        'qty': fields.integer('Qty'),
        'last_date': fields.datetime('Last transaction date'),
        'report_id': fields.many2one('stock.product.report', 'Report'),
        'product_item': fields.function(_item, string='Item', type='char', method=True),
        'location': fields.function(_location, string='location', type='char', method=True),
        'summary_link': fields.function(_get_link, string='Summary', type='char', method=True),
        'phantom': fields.function(_get_phantom, string="Phantom", type="char", method=True),
        'rename_to': fields.char('Rename bin to', size=256, ),
        'is_set': fields.boolean('Is set', ),
        'product_type': fields.char('Product type', size=256, readonly=True, ),
    }

    def print_label_wiz(self, cr, uid, ids, template, context=None):
        def _get_cafer_bin_name(cr, uid, line, size_id):
            """Check if line' size, bin and warehouse refers to CAFER.
            :param line - stock_move line.
            :return True if line belongs to CAFER."""
            bin_ids = []
            # if (line.product_id.sizeable or line.product_id.ring_size_ids) and size_id:
            bin_ids = list(self.pool.get('product.product').get_mssql_product_bins(cr, uid,
                                                                                   ids=[line.product_id.id],
                                                                                   loc_ids=[],
                                                                                   size_ids=[size_id],
                                                                                   context={'warehouse': 'CAFER'}
                                                                                   ))
            bin_names = filter(lambda x: re.compile('^\d{1,2}-[A-Z]-\d{1,2}$').match(x),
                               map(lambda x: x.name, self.pool.get('stock.bin').browse(cr, uid, bin_ids)))
            return bin_names and bin_names[0]

        line = self.browse(cr, uid, ids[0])
        size_obj = self.pool.get('ring.size')
        size_ids = size_obj.search(cr, uid, [('name', '=', line.size)], limit=1)
        size_id = size_ids[0] if size_ids else ''
        label_printer = LabelWizPrinter(self.pool, cr, uid)
        user_name = self.pool.get('res.users').read(cr, uid, uid, ['name'])['name']

        autoprint = 0
        if context and context.get('autoprint'):
            autoprint = context.get('autoprint', 0)
        return label_printer.print_label(
            line.product_id.id, size_id,
            template,
            force={
                'bin_name': line.bin,
                'cafer_bin_id': _get_cafer_bin_name(cr, uid, line, size_id),
                'user_name': user_name,
                'autoprint': autoprint
            }, )

    def print_sr_label_wiz(self, cr, uid, ids, context=None):
        template = 'ShowRoomLabel'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_ca_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_sr_label_wiz_auto(self, cr, uid, ids, context=None):
        template = 'ShowRoomLabel'
        return self.print_label_wiz(cr, uid, ids, template, context={'autoprint':1})
    
    def print_int_label_wiz_auto(self, cr, uid, ids, context=None):
        template = 'QRItemLabelInt'
        return self.print_label_wiz(cr, uid, ids, template, context={'autoprint':1})

    def print_qr_item_label_ca_wiz_auto(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template, context={'autoprint':1})

    def print_qr_item_label_wiz_auto(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template, context={'autoprint':1})

    def release_bin(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'rename_to': BIN_FOR_RELEASE})
        return self.rename_bin(cr, uid, ids, context=context)

    def rename_bin(self, cr, uid, ids, context=None):
        if not ids:
            return False

        assert len(ids) == 1, 'You may rename only 1 bin at a time'

        if context is None:
            context = {}

        reason = None

        just_post = False
        line = self.read(
            cr, uid, ids[0], [
                'product_id',
                'item',
                'size',
                'warehouse',
                'stock',
                'bin',
                'rename_to',
            ]
        )

        if not line['bin']:
            line['bin'] = ''

        itemid = line['item'] + '/' + line['size']
        new_bin = (line['rename_to'] or '').strip()
        new_bin = re.sub("[^\w\s~\-_]", "", new_bin).strip()
        if not new_bin:
            raise osv.except_osv(_('Warning!'), 'New bin name can\'t be empty.')

        if new_bin.upper() == BIN_FOR_RELEASE:
            just_post = True

        server_obj = self.pool.get('fetchdb.server')

        # Connect to MSSQL Transactions
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), _('No connection to MSSQL!'))

        bin_obj = self.pool.get('stock.bin')
        if not just_post:
            # Check if there are other items in the old or a new bin
            # 1/2. Check only stock without phantom, It may be very costly to check all at once
            is_phantom_or_stock = 'stock'
            bins_items = bin_obj.get_items_for_bins(cr, uid, [line['bin'], new_bin], line['warehouse'], ignore_phantom=True, server_id=server_id)
            old_bin_items = [bin_item for bin_item in bins_items.get(line['bin'], []) if bin_item != itemid]
            bin_items = old_bin_items or bins_items.get(new_bin, None)
            # 2/2. Then check phantom only if there is no stock
            if not bin_items:
                is_phantom_or_stock = 'phantom'
                bins_items = bin_obj.get_items_for_bins(cr, uid, [line['bin'], new_bin], line['warehouse'], ignore_phantom=False, server_id=server_id)
                old_bin_items = [bin_item for bin_item in bins_items.get(line['bin'], []) if bin_item != itemid]
                bin_items = old_bin_items or bins_items.get(new_bin, None)
            if bin_items:
                type_error, bin_name = old_bin_items and ('Old', line['bin']) or ('New', new_bin)
                items = ', '.join((bin_items)[:10])
                if len(bin_items) > 10:
                    items += '... and {} more items'.format(len(bin_items) - 10)
                raise osv.except_osv(
                    _('Warning!'),
                    'Renaming cannot be done. {type_error} bin name {bin} in warehouse {warehouse} has {is_phantom_or_stock} for other items: {items}'.format(
                        warehouse=line['warehouse'],
                        bin=bin_name,
                        items=items,
                        type_error=type_error,
                        is_phantom_or_stock=is_phantom_or_stock
                    )
                )
        # Check invredid
        invredid = self.pool.get('stock.move').get_invredid_for_mssql_row(cr, uid, itemid, line['warehouse'], line['stock'], line['bin'])

        # Update qty
        mssql_conn = server_obj.connect(cr, uid, server_id)
        if not mssql_conn:
            raise osv.except_osv(_('Warning!'), _('No connection to MSSQL!'))
        mssql_curs = mssql_conn.cursor()
        if just_post:
            empty_bins = bin_obj.check_bins_for_release(cr, uid, line['bin'], line['warehouse'], line['item'], line['size'], server_id=server_id)
            # Bin has real or phantom qty
            if line['bin'] not in empty_bins:
                raise osv.except_osv(
                    _('Warning!'),
                    'Can\'t release bin "{bin}" in warehouse {warehouse}. This bin has stock or phantom qty.'.format(
                        bin=line['bin'],
                        warehouse=line['warehouse'],
                    )
                )

        try:
            reason = None
            stock_obj = self.pool.get('stock.move')
            phantom_obj = self.pool.get('stock.phantom.management')
            if not just_post:
                bin_obj.release_bins(cr, uid, new_bin, line['warehouse'], server_id=None, mssql_conn=mssql_conn, mssql_curs=mssql_curs)
                invs = bin_obj.get_qty_for_bin_item(cr, uid, line['item'], line['size'], line['warehouse'], line['bin'], server_id=server_id)
                for inv in invs:
                    # B2B
                    reason = 'Rename bin from "%s" to "%s" in %s/%s' % (inv['bin'], new_bin, inv['warehouse'], inv['stockid'])
                    trn_code = 'ADJ'
                    trn_type = 'STK'
                    reserved_flag = stock_obj.get_reserved_flag(cr, uid, itemid, inv['warehouse'], inv['stockid'], server_id)

                    timestasmp = int(time.time())
                    invoiceno = 'RNM' + str(timestasmp)

                    move_params_from = (
                        itemid,  # itemid
                        trn_code,  # transactioncode
                        inv['qty'],  # qty
                        inv['warehouse'],  # warehouse
                        line['product_id'][1],  # itemdescription
                        inv['bin'] or '',  # bin
                        inv['stockid'] or None,  # stockid
                        trn_type,  # transactiontype
                        line['item'],  # id_delmar
                        '-1',  # sign
                        (-1 * int(inv['qty'])) or '0',  # adjusted_qty
                        line['size'],  # size
                        invredid,  # invredid
                        None,  # customer id
                        line['item'],  # ProductCode
                        invoiceno,  # invoiceno
                        reserved_flag,  # reserved
                        None,  # trans_tag
                        0,  # unit price from sale_order_line
                    )

                    move_params_to = (
                        itemid,  # itemid
                        trn_code,  # transactioncode
                        inv['qty'],  # qty
                        inv['warehouse'],  # warehouse
                        line['product_id'][1],  # itemdescription
                        new_bin,  # bin
                        inv['stockid'] or None,  # stockid
                        trn_type,  # transactiontype
                        line['item'],  # id_delmar
                        '1',  # sign
                        inv['qty'],  # adjusted_qty
                        line['size'],  # size
                        invredid,  # invredid
                        None,  # customer id
                        line['item'],  # ProductCode
                        invoiceno,  # invoiceno
                        reserved_flag,  # reserved
                        None,  # trans_tag
                        0,  # unit price from sale_order_line
                    )

                    new_context = {
                        'reason': reason
                    }
                    sql_res_from = stock_obj.write_into_transactions_table(
                        cr, uid, server_id,
                        params=move_params_from,
                        mssql_conn=mssql_conn,
                        mssql_curs=mssql_curs,
                        context=new_context
                    )
                    if inv['phantom_qty']:
                        # set phantom to 0 for release
                        phantom_obj.set_phantom_in_ms(
                            cr,
                            uid,
                            0,
                            line['item'],
                            line['size'],
                            inv['warehouse'],
                            inv['stockid'],
                            mssql_conn=mssql_conn,
                            mssql_curs=mssql_curs,
                        )
                    sql_res_to = stock_obj.write_into_transactions_table(
                        cr, uid, server_id,
                        params=move_params_to,
                        mssql_conn=mssql_conn,
                        mssql_curs=mssql_curs,
                        context=new_context
                    )
                    if inv['phantom_qty']:
                        # restore phantom qty for new bin
                        phantom_obj.set_phantom_in_ms(
                            cr,
                            uid,
                            inv['phantom_qty'],
                            line['item'],
                            line['size'],
                            inv['warehouse'],
                            inv['stockid'],
                            mssql_conn=mssql_conn,
                            mssql_curs=mssql_curs,
                        )

                    if not (sql_res_from and sql_res_to):
                        raise

                    # RENAME
                    insert_table = 'prg_invredid_change'
                    insert_values = {
                        'ID': 0,
                        'WAREHOUSE': inv['warehouse'],
                        'STOCKID': inv['stockid'],
                        'ID_DELMAR': line['item'],
                        'SIZE': line['size'],
                        'BIN': new_bin,
                        'OLD_BIN': inv['bin'],
                        'INVREDID': invredid,
                    }
                    sql_rename_res = server_obj.insert_record(
                        cr, uid, server_id,
                        insert_table, insert_values,
                        select=False, commit=False,
                        connection=mssql_conn, cursor=mssql_curs
                    )
                    if not sql_rename_res:
                        raise

            # Post
            released_bins = bin_obj.release_bins(cr, uid, line['bin'], line['warehouse'], line['item'], line['size'], server_id=None, mssql_conn=mssql_conn, mssql_curs=mssql_curs)
            if not released_bins:
                reason = 'Release bin failed: warehouse {warehouse}, bin name: {bin}'.format(
                    warehouse=line['warehouse'],
                    bin=line['bin']
                )
                raise

        except:
            if reason:
                logger.error("%s" % (reason))
            raise osv.except_osv(
                _('Warning!'),
                "Something went wrong with rename/release of the bin."
            )
        finally:
            try:
                mssql_conn.close()
            except:
                pass

        if context.get('sync_stock', False):
            try:
                size_id = False
                if line['size'] != '0000':
                    size_name = str(float(line['size']) / 100)
                    size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                    if size_id and size_id[0]:
                        size_id = size_id[0]
                self.pool.get('sale.order').sync_product_stock(cr, uid, line['product_id'][0], size_id, context=context)
            except:
                logger.error("Can`t update stock: %s/%s %s/%s" % (line['item'], line['size'], line['warehouse'], line['stock']))

        return self.pool.get('stock.product.move').get_act_win(cr, uid, 'action_open_stock_product_report', line['product_id'][0], context=context)

    def adjust_zero(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        change_qty_obj = self.pool.get('stock.change.product.qty')
        bin_obj = self.pool.get('stock.bin')
        ring_size_obj = self.pool.get('ring.size')
        loc_obj = self.pool.get('stock.location')
        release_obj = self.pool.get('stock.release.bin')

        for line in self.browse(cr, uid, ids):

            size_name = str(float(line.size) / 100)
            size_ids = ring_size_obj.search(cr, uid, [('name', '=', size_name)])
            size_id = size_ids and size_ids[0] or False

            bin = line.bin
            bin_ids = bin_obj.search(cr, uid, [('name', '=', bin)])
            bin_id = bin_ids and bin_ids[0] or False

            loc_ids = loc_obj.search(cr, uid, [('complete_name', 'ilike', '% / ' + line.warehouse.strip() + ' / Stock / ' + line.stock.strip())])
            loc_id = loc_ids and loc_ids[0] or False

            if loc_id:
                change_qty_id = change_qty_obj.create(cr, uid, {
                        'size_id': size_id,
                        'bin': bin,
                        'bin_id': bin_id,
                        'reason': 'Adjust 0 for release bin',
                        'location_id': loc_id,
                        'new_quantity': 0,
                        'prodlot_id': False,
                    })

                new_context = {'product_id': line.product_id.id, 'release_bin': True}
                change_qty_obj.change_product_qty(cr, uid, [change_qty_id], context=new_context)

        release_ids = release_obj.search(cr, uid, [('line_ids', 'in', ids)])
        set_ids = set(ids)
        for release in release_obj.browse(cr, uid, release_ids):
            release_line_ids = set([release_line.id for release_line in release.line_ids])
            new_release_line_ids = list(release_line_ids - set_ids)
            released_line_ids = list(release_line_ids & set_ids)
            not_empty_bin_names = [new_release_line.bin for new_release_line in self.browse(cr, uid, new_release_line_ids)]
            bin_names = [released_line.bin for released_line in self.browse(cr, uid, released_line_ids) if released_line.bin not in not_empty_bin_names]
            release_obj.write(cr, uid, [release.id], {
                    'line_ids': [(6, 0, new_release_line_ids)],
                    'result_info': release.result_info and release.result_info + '\n\nBin %s has been adjusted to 0\n' % "\n".join(bin_names) or '',
                    'bin_to_release_list': bin_names and (release.bin_to_release_list and release.bin_to_release_list + ',' + ','.join(bin_names) or ','.join(bin_names)) or release.bin_to_release_list,
                })
        return True

    def action_update_stock(self, cr, uid, ids, context=None):
        product_id = context and context.get('product_id', False) or False
        if not product_id:
            stock_product_report_record = self.browse(cr, uid, ids)
            if stock_product_report_record and \
                    stock_product_report_record[0] and \
                    stock_product_report_record[0].report_id and \
                    stock_product_report_record[0].report_id.product_id:
                product_id = stock_product_report_record[0].report_id.product_id.id
        if product_id:
            context.update({'active_id': product_id})

        act_win = False

        if ids:
            curr_id = ids[0]
            if curr_id:
                curr_obj = self.browse(cr, uid, curr_id)

                warehouse = (curr_obj.warehouse or '').strip()
                stockid = (curr_obj.stock or '').strip()
                move_bin = curr_obj.bin
                size_code = curr_obj.size
                qty = curr_obj.qty

                new_context = {'product_id': product_id, 'active_id': product_id}
                vals = {'product_id': product_id}

                # set default location
                if warehouse and stockid:
                    loc_ids = self.pool.get('stock.location').search(cr, uid, [('complete_name', 'ilike', '% / ' + warehouse + ' / Stock / ' + stockid)])
                    if loc_ids:
                        vals.update({'location_id': loc_ids[0]})
                    else:
                        raise osv.except_osv(_('Warning!'), 'Location %s / Stock / %s not exist in ERP. Please create this location after that you will may update qty.' % (warehouse, stockid))

                # set default size
                move_size_id = None
                if size_code:
                    size_name = str(float(size_code) / 100)
                    move_size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                    if move_size_ids:
                        move_size_id = move_size_ids[0]
                        if move_size_id:
                            vals.update({'size_id': move_size_id})

                # set default bin
                if move_bin:
                    bin_id = self.pool.get('stock.bin').get_bin_id_by_name(cr, uid, move_bin)
                    vals.update({'bin_id': bin_id})

                # set default qty
                if qty:
                    vals.update({'new_quantity': qty})

                # Sync Item before update qty
                # self.pool.get('sale.order').sync_product_stock(cr, uid, product_id, move_size_id)

                res_id = self.pool.get('stock.change.product.qty').create(cr, uid, vals)
                data_obj = self.pool.get('ir.model.data')
                act_obj = self.pool.get('ir.actions.act_window')

                act_win_res_id = data_obj._get_id(cr, uid, 'stock', 'action_view_change_product_quantity')
                act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
                act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
                act_win.update({
                    'res_id': res_id,
                    'context': new_context,
                })

        return act_win

    def get_erp_qty_for_product_size(self, cr, uid, product_id, size_id=False, context=None):
        result = []

        if not product_id:
            return result

        where_size = ''
        if size_id:
            where_size = ' AND size_id = %s ' % size_id

        sql = """SELECT sm.product_id,
                    sm.size_id,
                    array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                    CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id,
                    sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)),
                    coalesce(sb.name, sm.bin, '') as bin
                FROM stock_move sm
                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                    LEFT JOIN stock_bin sb on sb.id = sm.bin_id
                WHERE sm.state IN ('done', 'reserved', 'assigned')
                    AND sm.product_id IN (%s)
                    AND ( sm.location_dest_id not in (0) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                    %s
                GROUP BY (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END)
                    , sm.product_id
                    , sm.size_id
                    , coalesce(sb.name, sm.bin, '')
                ORDER BY CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END
            """ % (product_id,
                   where_size)

        sql = sql.replace("=None", " IS NULL ")
        cr.execute(sql)
        res = cr.fetchall()

        for product_id, size_id, invredid, location_dest_id, qty, bin in res:
            loc_obj = self.pool.get('stock.location')
            size_obj = self.pool.get('ring.size')

            loc = loc_obj.browse(cr, uid, location_dest_id, context=context)

            if loc and loc.complete_name:
                loc_parts = loc.complete_name.split('/')

            size_name = '0000'
            size = size_obj.browse(cr, uid, size_id)
            if size:
                size_name = ("00%s" % (float(size.name) * 100))[-6:-2]

            result.append({
                'size': size_name,
                'wh_name': '' if len(loc_parts) < 2 else loc_parts[1].strip(),
                'loc_name': '' if len(loc_parts) < 4 else loc_parts[3].strip(),
                'bin': bin,
                'qty': qty,
            })

        return result

    def action_phantom_stock(self, cr, uid, ids, context=None):
        rep_line_obj = self.pool.get('stock.product.report.line')
        size_obj = self.pool.get('ring.size')

        server_data = self.pool.get('fetchdb.server')

        server_id = server_data.get_main_servers(cr, uid, single=True)
        if not server_id:
            print "Can't read PHANTOM inventory: no connection to MSSQL."
            return False

        new_context = {}

        product_id = context and context.get('product_id', False) or False
        if product_id:
            rep_lines = rep_line_obj.browse(cr, uid, ids)
            if rep_lines:
                rep_line = rep_lines[0]
            else:
                return False

            size_id = False

            size_name = rep_line.size
            if size_name and size_name != '0000':
                size_float = float(size_name)/100
                size_ids = size_obj.search(cr, uid, [('name', '=', str(size_float))])
                if size_ids:
                    size_id = size_ids[0]

            sql_cur_phantom = """
                SELECT TOP 1 phantom
                FROM transactions
                WHERE
                    phantom is not null
                    and status = 'Pending'
                    and id_delmar = ?
                    and size = ?
                    and warehouse = ?
                    and stockid = ?
                ORDER BY id desc
            """

            params = (
                rep_line.item,
                rep_line.size,
                rep_line.warehouse,
                rep_line.stock,
            )

            cur_phantom = server_data.make_query(cr, uid, server_id, sql_cur_phantom, params=params, select=True, commit=False)

            phantom_val = 0
            if cur_phantom:
                phantom_val = cur_phantom[0][0] or 0

            new_context.update({
                'default_itemid': "%s/%s" % (rep_line.item, rep_line.size),
                'default_warehouse': rep_line.warehouse,
                'default_stockid': rep_line.stock,
                'default_phantom_value': phantom_val,
                'size_id': size_id,
            })

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_phantom_management')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': False,
            'context': new_context,
        })

        return act_win


    def action_sync_stock(self, cr, uid, ids, context=None):
        rep_line_obj = self.pool.get('stock.product.report.line')
        rep_obj = self.pool.get('stock.product.report')
        sync_line_obj = self.pool.get('stock.sync.product.qty.line')
        size_obj = self.pool.get('ring.size')

        new_context = {}

        product_id = context and context.get('product_id', False) or False

        if not product_id:
            product_id = self.browse(cr, uid, ids)[0].product_id.id

        if product_id:
            rep_lines = rep_line_obj.browse(cr, uid, ids)
            if rep_lines:
                rep_line = rep_lines[0]
            else:
                return False

            size_id = False

            size_name = rep_line.size
            if size_name and size_name != '0000':
                size_float = float(size_name)/100
                size_ids = size_obj.search(cr, uid, [('name', '=', str(size_float))])
                if size_ids:
                    size_id = size_ids[0]

            new_context.update({'size_id': size_id})

            sync_lines = rep_obj.get_qty_prod_locations(cr, uid, ids, product_id, size_id=size_id, context=context)

            erp_lines = self.get_erp_qty_for_product_size(cr, uid, product_id, size_id, context=context)

            new_lines = []

            for qty, delmar_id, size, warehouse, stock, bin, last_date in sync_lines:
                erp_qty = '-'
                # iterating over a copy of erp_lines to be able to remove items in the loop
                for erp_line in list(erp_lines):
                    if erp_line['wh_name'] == warehouse and \
                       erp_line['loc_name'] == stock and \
                       erp_line['bin'] == bin and \
                       erp_line['size'] == size:
                        erp_qty = erp_line['qty']
                        erp_lines.remove(erp_line)

                new_line = sync_line_obj.create(cr, uid, {
                    'size': size,
                    'warehouse': warehouse,
                    'stockid': stock,
                    'bin': bin,
                    'mssql_product_qty': qty or '0',
                    'erp_product_qty': erp_qty,
                })

                new_lines.append(new_line)

            for rest_erp_line in erp_lines:
                new_line = sync_line_obj.create(cr, uid, {
                    'size': rest_erp_line['size'],
                    'warehouse': rest_erp_line['wh_name'],
                    'stockid': rest_erp_line['loc_name'],
                    'bin': rest_erp_line['bin'],
                    'mssql_product_qty': '-',
                    'erp_product_qty': rest_erp_line['qty'] or '0',
                })

                new_lines.append(new_line)

            new_context.update({
                'default_sync_product_line_ids': new_lines,
                'default_itemid': "%s/%s" % (rep_line.item, rep_line.size),
            })

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_sync_product_quantity')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': False,
            'context': new_context,
        })

        return act_win

    def action_open_stock_move(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        new_context = {}

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        move_reserved = context.get('move_reserved', False)

        incoming_bin = False
        if move_reserved:
            bin_data = self.pool.get('stock.bin')
            incoming_bin_id = bin_data.search(cr, uid, [('name', '=', 'INCOMING')])
            if not incoming_bin_id:
                incoming_bin_id = [bin_data.create(cr, uid, {'name': 'INCOMING'}, context=context)]

            incoming_bin = bin_data.browse(cr, uid, incoming_bin_id[0])

        if ids:
            line = self.browse(cr, uid, ids[0])
            new_context.update({
                # 'default_new_warehouse': line.warehouse,
                # 'default_new_stock': line.stock,
                # 'default_new_bin': incoming_bin and incoming_bin.name or False,
                'default_qty': line.qty,
                'default_line_id': line.id,
                'default_reason': context.get('default_reason', False),
                'reserved_move_id': context.get('reserved_move_id', False),
                'move_reserved': move_reserved,
                'active_id': line.id,
                'active_ids': ids,
                'product_id': context.get('product_id', False),
            })

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_product_move')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': None,
            'context': new_context,
        })
        return act_win

stock_product_report_line()


class product_size_conjugate(osv.osv):
    _name = "product.size.conjugate"

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=False, select=True, ondelete='cascade'),
        'size_id': fields.many2one('ring.size', 'Size', select=True),
    }

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for record in self.browse(cr, uid, ids, context=context):
            res.append((record.id, record.product_id.default_code))
        return res

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
        if not name: return []
        parts = name.split('/')
        # Product
        product_id = self.pool.get('product.product').search(cr, uid, [('default_code', '=', parts[0])])
        # Size
        size_id = None
        if len(parts) > 1:
            size_name = self.pool.get('ring.size').convert_to_regular_name(cr, uid, parts[1], context)
            size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
        record_id = self.create(cr, uid, {'product_id': product_id and product_id[0] or None,
                                          'size_id': size_id and size_id[0] or None})
        return [(record_id, name)]

product_size_conjugate()


class stock_product_report(osv.osv):
    _name = "stock.product.report"

    _columns = {
        'refresh': fields.boolean('Refresh'),
        'sync_product': fields.boolean('Sync'),
        'product_size_conj_id': fields.many2one('product.size.conjugate', 'Item/Size', required=False, select=True, ondelete='cascade'),
        'product_id': fields.many2one('product.product', 'Product', required=False, select=True, ondelete='cascade'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', select=True, donain=[('virtual', '=', False)]),
        'location_id': fields.many2one('stock.location', 'Location', select=True),
        'size_id': fields.many2one('ring.size', 'Size', select=True),
        'shelf': fields.char('Bin', size=256,),
        'sizeable': fields.related('product_id', 'sizeable', type='boolean', size=256, string='Sizeable'),
        'line_ids': fields.one2many('stock.product.report.line', 'report_id', 'Report lines'),
        'is_set': fields.boolean('Is set', ),
    }

    def sync_all_product_sizes(self, cr, uid, ids, product_id, context=None):
        size_obj = self.pool.get('ring.size')
        sale_obj = self.pool.get('sale.order')

        if not product_id:
            item_size_record = context and \
                               self.pool.get('product.size.conjugate').browse(cr,
                                                                              uid,
                                                                              context.get('product_size_conj_id', []))
            if item_size_record:
                product_id = item_size_record.product_id.id

        sale_obj.update_set_stock(cr, uid, product_id, context=context)

        res = self.get_qty_prod_locations(cr, uid, ids, product_id, context=context)

        size_arr = []
        if res:
            for qty, delmar_id, size, warehouse, stock, bin, last_date in res:
                size_arr.append(size)

        size_set = set(size_arr)

        if size_set:

            for size_str in size_set:
                prod_size_id = False
                size_float = float(size_str)/100
                if size_float:
                    found_sizes = size_obj.search(cr, uid, [('name', '=', str(size_float))])
                    if found_sizes:
                        prod_size_id = found_sizes[0]

                sale_obj.sync_product_stock(cr, uid, product_id, prod_size_id, context=context)

        result = self.build_stock_report_lines(cr, uid, ids, product_id, res, context=context)

        return {'value': result}

    def get_qty_prod_locations(self, cr, uid, ids, product_id, size_id=None, warehouse_id=None, location_id=None, shelf=None, context=None):
        res = []

        if not product_id:
            return res

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        add_search = {
            'conditions': [],
            'params': []
        }
        if size_id:
            size = size_obj.browse(cr, uid, size_id, context=context)
            size_str = ("00%s" % (float(size.name) * 100))[-6:-2]
            add_search['conditions'].append('AND Size = ?')
            add_search['params'].append(size_str)
        product = product_obj.browse(cr, uid, product_id, context=context)

        if warehouse_id:
            add_search['conditions'].append('AND Warehouse = ?')
            add_search['params'].append(self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id).name)

        if location_id:
            add_search['conditions'].append('AND StockID = ?')
            add_search['params'].append(self.pool.get('stock.location').browse(cr, uid, location_id).name)

        if shelf:
            add_search['conditions'].append('AND bin = ?')
            add_search['params'].append(shelf)

        product_codes = []

        if product.default_code:
            product_codes.append(product.default_code)

        if product.id_delmar:
            product_codes.append(product.id_delmar)

        if len(product_codes) == 0:
            return res

        id_delmar_search = ""
        for code in product_codes:
            id_delmar_search += """ OR id_delmar = '%s' """ % (code)

        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)

        if server_id:
            sql = """SELECT
                    SUM(adjusted_qty) AS QTY,
                    id_delmar,
                    size,
                    warehouse,
                    stockid,
                    coalesce(bin, '') as bin,
                    MAX(transactdate) AS last_date
                FROM TRANSACTIONS
                WHERE   ( 1 != 1 %s)
                    AND status <> 'Posted'
                    %s
                GROUP BY
                    id_delmar,
                    size,
                    warehouse,
                    stockid,
                    coalesce(bin, '')
                ORDER BY size, warehouse, stockid;
            """ % (id_delmar_search, "\n".join(add_search['conditions']),)
            res = server_obj.make_query(cr, uid, server_id, sql, params=add_search['params'], select=True, commit=False)
        return res

    def build_stock_report_lines(self, cr, uid, ids, product_id, data, context=None):
        result = {
            'sizeable': False,
            'line_ids': None,
            'is_set': False,
        }

        if not product_id:
            return result

        product_obj = self.pool.get('product.product')
        product = product_obj.read(cr, uid, product_id, ['sizeable', 'set_component_ids', 'categ_id'], context=context)

        is_set = bool(product['set_component_ids'])
        result.update(sizeable=product['sizeable'], is_set=is_set)

        if data:
            line_ids = []
            line_obj = self.pool.get('stock.product.report.line')
            for qty, delmar_id, size, warehouse, stock, bin, last_date in data:
                line_id = line_obj.create(cr, uid, {
                    'item': delmar_id and delmar_id.strip() or False,
                    'size': size and size.strip() or False,
                    'warehouse': warehouse and warehouse.strip() or False,
                    'stock': stock and stock.strip() or False,
                    'bin': bin and bin.rstrip() or False,
                    'qty': qty,
                    'last_date': last_date,
                    'product_id': product_id,
                    'is_set': is_set,
                    'product_type': product and product['categ_id'] and product['categ_id'][1] or '',
                    'report_id': (isinstance(ids, list) and ids and ids[0]) or None
                    })
                if line_id:
                    line_ids.append(line_id)
            result.update({'line_ids': line_ids})

        return result

    def get_prod_locations(self, cr, uid, ids, product_id, size_id=None, warehouse_id=None, location_id=None, shelf=None, context=None):
        result = {
            'sizeable': False,
            'product_id': product_id,
        }

        is_rec = context and context.get('product_size_conj_id', [])
        item_size_record = is_rec and self.pool.get('product.size.conjugate').browse(cr, uid, is_rec) or None
        if item_size_record:
            product_id = item_size_record.product_id.id
            size_id = item_size_record.size_id.id
            vals = {
                'product_id': product_id,
                'size_id': size_id,
            }
            ids = [self.create(cr, uid, vals, context)]

        if not product_id:
            return result

        if warehouse_id and location_id:
            if warehouse_id != self.pool.get('stock.location').get_warehouse(cr, uid, location_id):
                location_id = None
                result['location_id'] = location_id

        res = self.get_qty_prod_locations(
            cr, uid, ids, product_id,
            size_id=size_id,
            warehouse_id=warehouse_id,
            location_id=location_id,
            shelf=shelf,
            context=context
            )
        result.update(self.build_stock_report_lines(cr, uid, ids, product_id, res, context=context))

        return {'value': result}

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        res = super(stock_product_report, self).default_get(cr, uid, fields_list, context=context)

        if context.get('def_product_id', False):
            res.update({'product_id': context.get('def_product_id')})

        return res

    def create(self, cr, uid, vals, context=None):
        return_id = super(stock_product_report, self).create(cr, uid, vals, context=context)
        return_obj = self.browse(cr, uid, return_id)

        # update null-lines, if the manual addition
        return_obj_vals = {}
        item_size_record = None
        # product id
        if not vals.get('product_id'):
            is_rec = vals and vals.get('product_size_conj_id', [])
            item_size_record = is_rec and self.pool.get('product.size.conjugate').browse(cr, uid, is_rec) or None

            if item_size_record:
                product_id = item_size_record.product_id.id
                return_obj_vals.update({'product_id': product_id})

        # size id
        if not vals.get('size_id'):
            if not item_size_record:
                is_rec = vals and vals.get('product_size_conj_id', [])
                item_size_record = is_rec and self.pool.get('product.size.conjugate').browse(cr, uid, is_rec) or None

            if item_size_record:
                size_id = item_size_record.size_id.id
                return_obj_vals.update({'size_id': size_id})

        return_obj.write(return_obj_vals)
        return return_id

stock_product_report()


class stock_change_product_qty(osv.osv):
    _inherit = "stock.change.product.qty"

    def write(self, cr, uid, ids, vals, context=None):
        bin_obj = self.pool.get('stock.bin')
        bin = vals.get('bin', False)
        bin_id = bin_obj.get_bin_id_by_name(cr, uid, bin)

        if bin_id:
            vals.update({'bin_id': bin_id})

        return super(stock_change_product_qty, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'size_ids': fields.char(size=256, string="Product size ids"),
        'bin': fields.char(size=256, string="Bin"),
        'bin_id': fields.many2one('stock.bin', 'Bin object'),
        'selected_reason': fields.selection(_get_selected_reason, 'Select reason', required=True,),
        'reason': fields.text('Reason'),
        'picking_id': fields.many2one('stock.picking', 'Order to Check availability'),
        # 'bin_ids': fields.char(size=256, string="Product bin ids"),
    }

    _defaults = {
        'selected_reason': "OTHER",
    }

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        product_id = context and context.get('product_id', False) or False
        res = super(stock_change_product_qty, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar, submenu)

        if product_id:
            context.update({'active_id': product_id})
            cr.execute("""SELECT sp.id
                    FROM stock_picking sp
                    -- filter by product
                    LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                    LEFT JOIN product_product pp ON sm.product_id = pp.id

                    WHERE sp.state in ('confirmed', 'waiting_split', 'back')
                    AND pp.id = %s
                """, (product_id, ))
            picking_ids = [x[0] for x in cr.fetchall() or [] if x]
            # context.update({'domain_picking_ids': picking_ids})
            res['fields']['picking_id']['domain'] = str([('id', 'in', picking_ids)])
        return res

    def default_get(self, cr, uid, fields, context=None):
        res = super(stock_change_product_qty, self).default_get(cr, uid, fields, context=context)

        if context.get('default_new_quantity'):
            res.update({'new_quantity': context.get('default_new_quantity')})
        return res

    def product_id_change(self, cr, uid, ids, product_id, location_id, size_id, bin_id, context=None):
        prod_obj = self.pool.get('product.product')
        context = context or {}
        product = prod_obj.read(cr, uid, product_id, ['ring_size_ids'])
        result = {}

        bin_ids = []
        size_ids = [str(x) for x in product['ring_size_ids']]
        if product:
            size_ids_str = ','.join(size_ids)
            result.update({'size_ids': size_ids_str})

            bin_ids = prod_obj.get_product_bins(cr, uid, [product['id']], [location_id], [size_id])

        if not size_ids or (size_id and (str(size_id) not in size_ids)):
            result.update({'size_id': None})

        if not bin_ids or (bin_id and (bin_id not in bin_ids)):
            result.update({'bin_id': None})

        return {'value': result, 'domain': {'bin_id': [('id', 'in', bin_ids)]}}

    def change_product_qty(self, cr, uid, ids, context=None):
        """ Changes the Product Quantity by making a Physical Inventory.
        @param self: The object pointer.
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param ids: List of IDs selected
        @param context: A standard dictionary
        @return:
        """
        if context is None:
            context = {}


        data_obj = self.pool.get('ir.model.data')
        template_obj = self.pool.get('email.template')

        product_id = context and context.get('product_id', False) or False
        if product_id:
            context.update({'active_id': product_id})

        rec_id = context and context.get('active_id', False)
        assert rec_id, _('Active ID is not set in Context')

        inventory_obj = self.pool.get('stock.inventory')
        inventory_line_obj = self.pool.get('stock.inventory.line')
        prod_obj_pool = self.pool.get('product.product')

        res_original = prod_obj_pool.browse(cr, uid, rec_id, context=context)
        for data in self.browse(cr, uid, ids, context=context):
            size_id = data.size_id and data.size_id.id or False
            loc = data.location_id
            loc_id = loc.id
            move_ids = []
            if data.picking_id:
                order = data.picking_id

                # # Check location for assigned order
                # customer = order.real_partner_id
                # primary_loc_ids = [l.id for l in customer.location_ids if l]
                # if loc_id not in primary_loc_ids:
                #     raise osv.except_osv(_('Warning!'), _('Location %s is not available for customer %s (order %s).') % (data.location_id.complete_name, customer.name, order.name))

                # Check ring size for assigned order
                for move in order.move_lines:
                    if move.state in ('confirmed') and move.product_id.id == product_id:
                        if (move.size_id and move.size_id.id or False) == size_id:
                            move_ids.append(move.id)
                if not move_ids:
                    raise osv.except_osv(_('Warning!'), _('Item/Size %s/%s is not correct for order %s.') % (res_original.default_code, data.size_id and data.size_id.name or '0', order.name))
            
            #DELMAR-75 start           
            if data.adjustment < 0:
                data.adjustment = abs(data.adjustment)
                new_quantity = data.new_quantity - data.adjustment

            elif data.adjustment > 0 or data.adjustment == 0:
                new_quantity = data.new_quantity + data.adjustment

            #DELMAR-75 end
            if new_quantity < 0:
                raise osv.except_osv(_('Warning!'), _('Quantity cannot be negative.'))
            size_str = data.size_id and data.size_id.name and tools.ustr('(' + data.size_id.name + ')') or ''

            if rec_id and not context.get('is_force_stock_update', False):
                self.pool.get('sale.order').sync_product_stock(cr, uid, rec_id, data.size_id.id, context=None)

            _reason = data.selected_reason if data.selected_reason != "OTHER" else data.reason
            inventory_id = inventory_obj.create(cr, uid, {
                'name': _('INV: %s%s') % (tools.ustr(res_original.name), size_str),
                'reason': _reason
            }, context=context)

            if not data.bin:
                data.bin = data.bin_id and data.bin_id.name or False

            line_data = {
                'inventory_id': inventory_id,
                'product_qty': new_quantity,
                'location_id': loc_id,
                'product_id': rec_id,
                'product_uom': res_original.uom_id.id,
                'prod_lot_id': data.prodlot_id.id,
                'size_id': data.size_id and data.size_id.id,
                'bin': data.bin,
                'bin_id': data.bin_id and data.bin_id.id,
            }

            inventory_line_obj.create(cr, uid, line_data, context=context)

            inventory_obj.action_confirm(cr, uid, [inventory_id], context=context)
            inventory_obj.action_done(cr, uid, [inventory_id], context=context)

            if not context.get('is_force_stock_update', False):
                email_context = context
                template = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'task_email_template_stock_product_update')
                if template:
                    cc_group = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_stock_analysis')
                    email_cc = self.pool.get('res.groups').get_user_emails(cr, uid, [cc_group[1]])[cc_group[1]] or []
                    email_context.update({'email_cc': ", ".join(email_cc)})
                    if email_cc:
                        size_str = self.pool.get('stock.move').get_size_postfix(cr, uid, data.size_id) if data.size_id else '0000'
                        email_context.update({'size_str': size_str})
                        template_obj.send_mail(cr, uid, template[1], inventory_id, False, email_context)

            if data.picking_id:
                self.pool.get('stock.picking').action_assign(cr, uid, [data.picking_id.id], location_ids=[loc_id], move_ids=move_ids)
        context.update({
            'flags': {'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False, 'can_be_discarded': True},
        })
        if context.get('target', False) == 'new':
            return_action_name = 'action_open_stock_product_report_new'
        else:
            return_action_name = 'action_open_stock_product_report'

        if context.get('release_bin'):
            return True
        return self.pool.get('stock.product.move').get_act_win(cr, uid, return_action_name, rec_id, context=context)

stock_change_product_qty()


class stock_sync_product_qty(osv.osv_memory):
    _name = "stock.sync.product.qty"
    _description = 'Stock sync product quantity (pgsql with mssql)'

    _columns = {
        'sync_product_line_ids': fields.one2many('stock.sync.product.qty.line', 'sync_product_id', 'Stocks for sync product'),
        'itemid': fields.char(size=256, string="DelmarID/Size"),
    }

    def sync_product_qty(self, cr, uid, ids, context=None):
        set_obj = self.pool.get('product.set.components')
        product_id = context.get('product_id', False)
        print 'syncing: ',product_id,context,ids

        id_delmar = context.get('default_itemid', False)
        if not product_id and id_delmar:
            product_obj = self.pool.get('product.product')
            prod_ids = product_obj.search(cr, uid, [
                ('type', '=', 'product'),
                '|', ('default_code', "=", id_delmar.split('/')[0]), ('id_delmar', "=", id_delmar.split('/')[0])
            ])
            if prod_ids:
                product_id = prod_ids[0]
                print 'found: ', product_id

        if product_id:

            if set_obj.is_set(cr, uid, product_id) and set_obj.is_nonsize_set(cr, uid, product_id):
                self.pool.get('stock.product.report').sync_all_product_sizes(cr, uid, [],product_id, context)
                return {'type': 'ir.actions.act_window_close'}

            prod_size_id = context.get('size_id', False)

            self.pool.get('sale.order').sync_product_stock(cr, uid, product_id, prod_size_id, context=context)

        return {'type': 'ir.actions.act_window_close'}

stock_sync_product_qty()


class stock_sync_product_qty_line(osv.osv_memory):
    _name = "stock.sync.product.qty.line"
    _description = 'Line for particular stock of syncing product (pgsql with mssql)'

    _columns = {
        'sync_product_id': fields.many2one('stock.sync.product.qty', 'Product to sync', ondelete='cascade', select=True),
        'warehouse': fields.char(size=256, string="Warehouse"),
        'stockid': fields.char(size=256, string="Stock ID"),
        'bin': fields.char(size=256, string="Bin"),
        'mssql_product_qty': fields.char(size=256, string="Real QTY"),
        'erp_product_qty': fields.char(size=256, string="ERP Qty"),
        'size': fields.char(size=256, string="Size"),
    }

stock_sync_product_qty()


class stock_phantom_management_log(osv.osv_memory):
    _name = "stock.phantom.management.log"
    _description = 'Phantom actions log'

    _columns = {
        'old_phantom_value': fields.char(size=256, string="Phantom Value"),
        'new_phantom_value': fields.char(size=256, string="Phantom Value"),
        'itemid': fields.char(size=256, string="Item ID"),
        'warehouse': fields.char(size=256, string="Warehouse"),
        'stockid': fields.char(size=256, string="Stock ID"),
    }

    def phantom_log(self, cr, uid, phantom=None, server_data=None, server_id=None, context=None):
        status = False
        if not context or not phantom:
            raise osv.except_osv(_('Warning!'), _('Please take real values'))
        if not server_data or not server_id:
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_main_servers(cr, uid, single=True)
            if not server_id:
                print "Can't update PHANTOM inventory: no connection to MSSQL."
                return False
        size_obj = self.pool.get('ring.size')
        itemid = context.get('default_itemid', False)
        id_delmar, size = size_obj.split_itemid_to_id_delmar_size(cr, uid, itemid)
        warehouse = context.get('default_warehouse', False)
        stockid = context.get('default_stockid', False)

        sql = """
            SELECT TOP 1 phantom
            FROM transactions
            WHERE phantom is not null
                and status = 'Pending'
                and id_delmar = '%s'
                and size = '%s'
                and warehouse = '%s'
                and stockid = '%s'
            ORDER BY id desc
        """ % (id_delmar, size, warehouse, stockid)
        res = server_data.make_query(cr, uid, server_id, sql, select=True, commit=False)

        if res:
            try:
                old_value = int(float(res[0][0]))
            except:
                old_value = res[0][0]
        else:
            old_value = '0'

        new_value = phantom

        status = self.create(cr, uid, {
            'old_phantom_value': old_value,
            'new_phantom_value': new_value,
            'itemid': itemid,
            'warehouse': warehouse,
            'stockid': stockid,
        })

        return status


stock_phantom_management_log()


class stock_phantom_management(osv.osv_memory):
    _name = "stock.phantom.management"
    _description = 'Update phantom value'

    _columns = {
        'itemid': fields.char(size=256, string="DelmarID/Size"),
        'warehouse': fields.char(size=256, string="Warehouse"),
        'stockid': fields.char(size=256, string="StockID"),
        'phantom_value': fields.char(size=256, string="Phantom Value"),
    }

    def set_phantom_in_ms(self, cr, uid, phantom, id_delmar, size, warehouse, stockid, server_data=None, server_id=None, mssql_conn=None, mssql_curs=None):
        if not server_data:
            server_data = self.pool.get('fetchdb.server')
        commit = True
        if mssql_curs and mssql_conn:
            server_id = None
            commit = False
        sql_upd_phantom = """
            update transactions
            set phantom = %s
            where id in
            (
                select max(id)
                from transactions
                where 1=1
                and status = 'Pending'
                and id_delmar = '%s'
                and size = '%s'
                and warehouse = '%s'
                and stockid = '%s'
            )
        """ % (phantom,
               id_delmar,
               size,
               warehouse,
               stockid, )

        res_upd_phantom = server_data.make_query(
            cr,
            uid,
            server_id,
            sql_upd_phantom,
            select=False,
            commit=commit,
            connection=mssql_conn,
            cursor=mssql_curs,
        )

        return True

    def update_phantom_value(self, cr, uid, ids, context=None):
        server_data = self.pool.get('fetchdb.server')

        server_id = server_data.get_main_servers(cr, uid, single=True)
        if not server_id:
            print "Can't update PHANTOM inventory: no connection to MSSQL."
            return False

        phantom = self.browse(cr, uid, ids[0]).phantom_value
        itemid = context.get('default_itemid', False)
        warehouse = context.get('default_warehouse', False)
        stockid = context.get('default_stockid', False)
        product_id = context.get('product_id', False)
        size_id = context.get('size_id', False)
        id_delmar_str = itemid[:itemid.rfind('/')]
        size_str = itemid[itemid.rfind('/')+1:]

        self.pool.get('stock.phantom.management.log').phantom_log(cr, uid, phantom, server_data, server_id, context)

        self.set_phantom_in_ms(cr, uid, phantom, id_delmar_str, size_str, warehouse, stockid, server_data, server_id)

        where_size_id = ' size_id is null '
        if size_id:
            where_size_id = ' size_id = %s ' % (size_id)

        sql_reset_phantom = """
            delete from product_multi_customer_fields
            where 1=1
            and field_name_id in (select fn.id
                                   from product_multi_customer_fields_name fn
                                   left join res_partner p on fn.customer_id = p.id
                                   where label = 'Phantom Inventory'
                                 )
            and product_id = %s
            and %s
        """ % (product_id, where_size_id,)

        cr.execute(sql_reset_phantom)

        data = [
            id_delmar_str,  # ID_DELMAR
            size_str,  # SIZE
            warehouse,  # WAREHOUSE
            stockid,  # STOCKID
            phantom,  # PHANTOM
        ]

        self.pool.get('sale.order').fill_phantom_customer_field(cr, uid, data)

        return {'type': 'ir.actions.act_window_close'}

stock_phantom_management()
