# -*- coding: utf-8 -*-
##############################################################################
#
#    SOLVVE
#    Copyright (C) 2020.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import logging
import csv
import cStringIO
import base64
import time
from datetime import datetime
from tools.translate import _
from sale import SIGN_DEFAULT_VALUE

_logger = logging.getLogger(__name__)


class TrackingRef(osv.osv):
    _name = "tracking.ref"

    _columns = {
        'name': fields.char('Tracking Ref', size=256),
        'line_ids': fields.one2many('tracking.ref.line', 'tracking_id', string='Scanned Lines', ),
    }
    _rec_name = 'name'

    def perm_write(self, cr, user, ids, fields, context=None):
        pass


TrackingRef()


class TrackingRefLine(osv.osv):
    _name = "tracking.ref.line"
    _columns = {
        'name': fields.char('Name', size=64, required=False, ),
        'scan_id': fields.many2one('scan.tracking.ref', 'Scan', ),
        'tracking_id': fields.many2one('tracking.ref', 'Tracking Ref', required=True, ),
        'manifest_id': fields.many2one('stock.picking.manifest', 'Manifest', required=True, ondelete='cascade', ),
        'sub_manifest_id': fields.many2one('stock.picking.sub.manifest', 'Sub Manifest', required=True, ondelete='cascade', ),
        'picking_id': fields.many2one('stock.picking', 'Order', required=True, ),
        'create_date': fields.datetime('Date', required=True, select=True, ),
        'create_uid': fields.many2one('res.users', 'Creator', required=False, ),
    }
    _rec_name = 'name'
    _sql_constraints = [
        ('uniq_line', 'unique(picking_id, sub_manifest_id)',
         'Sub-manifest can contain only unique orders')
    ]
    _defaults = {}

    def perm_write(self, cr, user, ids, fields, context=None):
        pass

    # just go to manifest view screen
    def open_manifest(self, cr, uid, ids, context=None):
        line = self.browse(cr, uid, ids and ids[0], context)
        return {
            'view_mode': 'page',        # 'form'
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'manifest_form')[1],
            'view_type': 'page',        # 'form'
            'res_model': 'stock.picking.manifest',
            'res_id': line.manifest_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',        # 'new'
            'domain': '[]',
            'context': {}
        }

    # just go to sub-manifest view screen
    def open_submanifest(self, cr, uid, ids, context=None):
        line = self.browse(cr, uid, ids and ids[0], context)
        return {
            'view_mode': 'page',        # 'form'
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'sub_manifest_form')[1],
            'view_type': 'page',        # 'form'
            'res_model': 'stock.picking.sub.manifest',
            'res_id': line.sub_manifest_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',        # 'new'
            'domain': '[]',
            'context': {}
        }

    # just go to tracking view screen
    def open_tracking(self, cr, uid, ids, context=None):
        line = self.browse(cr, uid, ids and ids[0], context)
        return {
            'view_mode': 'page',        # 'form'
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'manifest_tracking_ref_form')[1],
            'view_type': 'page',        # 'form'
            'res_model': 'tracking.ref',
            'res_id': line.tracking_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',        # 'new'
            'domain': '[]',
            'context': {}
        }

    # on change manifest in form
    def onchange_manifest(self, cr, uid, ids, manifest_id, context=None):
        result = {
            'value': {
                'sub_manifest_id': None
            },
            'domain': {
                'sub_manifest_id': [('manifest_id', '=', manifest_id)]
            }
        }
        return result


TrackingRefLine()


class ScanTrackingRef(osv.osv):
    _name = "scan.tracking.ref"
    _rec_name = 'tracking_ref'

    _columns = {
        'tracking_ref': fields.char('Tracking ref', size=256, required=True, ),
        'manifest_id': fields.many2one('stock.picking.manifest', 'Manifest', required=True, ondelete='cascade', ),
        'sub_manifest_id': fields.many2one('stock.picking.sub.manifest', 'Sub Manifest', required=True, ondelete='cascade', ),
        "line_ids": fields.one2many("tracking.ref.line", "scan_id", string="Scanned lines"),
    }

    def perm_write(self, cr, user, ids, fields, context=None):
        pass

    @staticmethod
    def _extract_tracking_ref(value):
        """Extract true tracking ref from incoming value"""
        length = len(value)
        if length == 34:
            value = value[-12:]
        elif length == 30:
            prefix = value[-22:-20]
            if prefix == '92':
                value = value[-20:]
            else:
                value = value[-22:]

        return value

    def action_scan_tracking(self, cr, uid, ids, context=None):
        # get scan data
        scan = self.browse(cr, uid, ids[0])
        # Extract true tracking ref from incoming tracking ref
        track_ref = self._extract_tracking_ref(scan.tracking_ref.strip())
        _logger.info("Got tracking: %s" % track_ref)

        # check object has empty values
        if not scan.manifest_id or not scan.sub_manifest_id or not track_ref:
            raise osv.except_osv(_('Error!'), _('All fields must be non empty!'))

        # init objects
        track_ref_obj = self.pool.get('tracking.ref')
        track_line_obj = self.pool.get('tracking.ref.line')
        picking_obj = self.pool.get('stock.picking')

        # search or create tracking.ref record
        existed_track_ids = track_ref_obj.search(cr, uid, [('name', '=', track_ref)])
        if existed_track_ids and existed_track_ids[0]:
            # tracking already exists
            track_id = existed_track_ids[0]
            _logger.info("Found existed tracking_ref id: %s" % track_id)
        else:
            # create new tracking
            track_id = track_ref_obj.create(cr, uid, {
                'name': track_ref,
            })
            _logger.info("Created tracking_ref id: %s" % track_id)

        # search orders by tracking_ref
        pick_ids = picking_obj.search(cr, uid, [('tracking_ref', 'ilike', track_ref)])
        _logger.info("Found pickings by tracking: %s" % pick_ids)
        # create scanned line record
        # for each order
        for picking in picking_obj.browse(cr, uid, pick_ids):
            # check picking already in
            existed_line_ids = track_line_obj.search(cr, uid, [('picking_id', '=', picking.id), ('sub_manifest_id', '=', scan.sub_manifest_id.id)])
            if existed_line_ids and len(existed_line_ids) > 0:
                _logger.warn("Combination picking + submanifest already exists")
                continue
            # prepare vals
            line_vals = {
                'tracking_id': track_id,
                'picking_id': picking.id,
                'manifest_id': scan.manifest_id.id,
                'sub_manifest_id': scan.sub_manifest_id.id,
                'scan_id': scan.id,
            }
            _logger.info("Create line: %s" % line_vals)
            # create line
            try:
                track_line_obj.create(cr, uid, line_vals)
            except Exception, e:
                _logger.warn("Record already exists: %s" % e.message)
                # cr.commit()
                pass

        # clear tracking_ref field
        self.write(cr, uid, ids, dict(tracking_ref=''))
        # return TODO: what to return just true or cleared object?
        return True


ScanTrackingRef()


class StockPickingManifest(osv.osv):
    _name = "stock.picking.manifest"

    EXPORT_REF_CA_PREFIX = 'FLXQF'
    EXPORT_REF_CA_PADDING = 7

    def _create_export_ref_ca(self, cr, uid):
        if not self.pool.get('ir.sequence').search(cr, uid, [('name', '=', self._name)]):
            # TODO: reassign this value to actual seq no form max(stock_move.export_ref_ca)
            max_export_ref_ca_no = 1

            seq = {
                'name': self._name,
                'implementation': 'standard',
                'prefix': self.EXPORT_REF_CA_PREFIX,
                'padding': self.EXPORT_REF_CA_PADDING,
                'number_increment': 1,
                'number_next': max_export_ref_ca_no + 1,
            }

            self.pool.get('ir.sequence').create(cr, uid, seq)

        seq_ids = self.pool.get('ir.sequence').search(cr, uid, [('name', '=', self._name)])
        if seq_ids:
            new_val = self.pool.get('ir.sequence').next_by_id(cr, uid, seq_ids[0])
        else:
            raise osv.except_osv(_('Error'), 'Cannot get next sequence number for {} model'.format(self._name))

        return new_val

    def _get_export_ref_ca(self, cr, uid, picking_ids):
        # If need to regenerate export_ref_ca
        result = {'empty_records': [],
                  'bad_format_records': [],
                  'good_format_records': [],
                  'export_ref_ca': None,
                  'status': True,
                  }
        picking_obj = self.pool.get('stock.picking')

        for picking in picking_obj.browse(cr, uid, picking_ids):
            move_lines = picking.move_lines
            for line in move_lines:
                # if export_ref_ca has bad prefix, fire regeneration of it
                export_ref_ca = (line.export_ref_ca or '')
                if not export_ref_ca:
                    result['empty_records'].append(picking.id)
                    if result['bad_format_records'] or result['good_format_records']:
                        result['status'] = False
                elif self.EXPORT_REF_CA_PREFIX not in export_ref_ca:
                    result['bad_format_records'].append(picking.id)
                    result['export_ref_ca'] = export_ref_ca
                    result['status'] = False
                else:
                    result['good_format_records'].append(picking.id)
                    result['export_ref_ca'] = export_ref_ca

        return result

    def _write_export_ref_ca(self, cr, uid, picking_ids, new_export_ref_ca_value):
        picking_obj = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        # picking_ids = picking_obj.search(self.cr, self.uid, [('tracking_ref', '=', tracking_ref)])
        move_ids = []
        for picking in picking_obj.browse(cr, uid, picking_ids):
            move_lines = picking.move_lines
            for line in move_lines:
                move_ids.append(line.id)
        update_values_dict = dict(export_ref_ca=new_export_ref_ca_value)
        move_obj.write(cr, uid, move_ids, update_values_dict)
        return True

    def write_order_history(self, cr, uid, ids, message=None):
        # Save old export_ref_ca value in export_history
        picking_obj = self.pool.get('stock.picking')
        time_now = time.strftime('%m-%d %H:%M:%S', time.gmtime())
        scan_user = self.pool.get('res.users').browse(cr, uid, uid)
        for order in picking_obj.browse(cr, uid, ids):
            if not order:
                continue
            new_history = u'{}\n{} {} {}'.format(order.report_history, time_now, scan_user.name, message)
            update_values_dict = dict(report_history=new_history)
            picking_obj.write(cr, uid, order.id, update_values_dict)
        return True

    """
    def _get_child_trackings(self, cr, uid, ids, field, arg, context=None):
        res = {}
        lines = []
        for manifest in self.browse(cr, uid, ids, context=context):
            for submanifest in manifest.sub_manifest_ids:
                lines.extend([x.id for x in submanifest.line_ids])
            res[manifest.id] = lines
        return res
    """
    def perm_write(self, cr, user, ids, fields, context=None):
        pass

    _columns = {
        'name': fields.char('Manifest name', size=256),
        'fc_invoice': fields.char('FC Invoice', size=256, required=False),
        'fc_order': fields.char('FC Order', size=256, required=False),
        'del_invoice': fields.char('DEL Invoice', size=256, required=False),
        'del_order': fields.char('DEL Order', size=256, required=False),
        'export_ref_us': fields.char('Export REF US', size=256, required=False),
        'bill_of_lading': fields.char('BILL OF LADING', size=100, required=False),
        "override_existing": fields.boolean('Override existing', help='Override existing fields.', store=False, required=False),
        "sub_manifest_ids": fields.one2many("stock.picking.sub.manifest", "manifest_id", string="Sub manifests"),
        # "line_ids": fields.function(_get_child_trackings, string='Scanned lines', type='one2many', obj='tracking.ref.line', method=True, ),
        "line_ids": fields.one2many('tracking.ref.line', 'manifest_id', string='Scanned Lines', ),
        "file": fields.binary(string="CSV Export", readonly=True),
    }

    _defaults = {
        "override_existing": False,
    }

    def generate_export_ref_ca(self, cr, uid, tracking_ref):
        picking_obj = self.pool.get('stock.picking')
        sub_manifest_obj = self.pool.get('stock.picking.sub.manifest')
        tracking_obj = self.pool.get('tracking.ref')
        tracking_line_obj = self.pool.get('tracking.ref.line')

        try:
            tracking_id = tracking_obj.search(cr, uid, [('name', '=', tracking_ref)])[0]
        except:
            raise osv.except_osv('Critical', 'Tracking ref %s was not found' % tracking_ref)

        picking_ids_tracking_ref = picking_obj.search(cr, uid, [('tracking_ref', '=', tracking_ref)])
        tracking_line_ids = tracking_line_obj.search(cr, uid, [('tracking_id', '=', tracking_id)])
        sub_manifest_ids = []
        for scan_line in tracking_line_obj.browse(cr, uid, tracking_line_ids):
            if scan_line.sub_manifest_id.id not in sub_manifest_ids:
                sub_manifest_ids.append(scan_line.sub_manifest_id.id)

        picking_ids_manifest = []
        for sub_manifest in sub_manifest_obj.browse(cr, uid, sub_manifest_ids):
            picking_ids_manifest.extend(map(lambda x: x.picking_id.id, sub_manifest.line_ids))

        result = self._get_export_ref_ca(cr, uid, picking_ids_tracking_ref)
        new_value = self._create_export_ref_ca(cr, uid)
        export_ref_ca = result['export_ref_ca']
        # export_ref_ca wasn't filled yet
        if not export_ref_ca and result['empty_records']:
            self._write_export_ref_ca(cr, uid, result['empty_records'], new_value)
            message = 'has created for order export_ref_ca {}'.format(new_value)
            self.write_order_history(cr, uid, result['empty_records'], message=message)
            export_ref_ca = new_value
        # export_ref_ca was filled wrongly of by hand
        elif not result['status'] and result['bad_format_records']:
            self._write_export_ref_ca(cr, uid, picking_ids_tracking_ref, new_value)
            message = 'has changed export_ref_ca from {} to {}'.format(export_ref_ca, new_value)
            self.write_order_history(cr, uid, result['bad_format_records'] + result['good_format_records'], message=message)
            message = 'has created for order export_ref_ca {}'.format(new_value)
            self.write_order_history(cr, uid, result['empty_records'], message=message)
            export_ref_ca = new_value

        # Update transactions record for stock move lines corresponding to given tracking_ref
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        move_obj = self.pool.get('stock.move')
        # Update only SLE transactions
        transaction_code = SIGN_DEFAULT_VALUE
        for original_order_id in move_obj.search(cr, uid, ['|', ('picking_id', 'in', picking_ids_tracking_ref),
                                                                ('set_parent_order_id', 'in', picking_ids_tracking_ref),
                                                           ]):
            original_order_record = move_obj.browse(cr, uid, original_order_id)
            move_obj.update_transactions_table(cr, uid, server_id, server_obj,
                                               original_order_record, original_order_record.warehouse.name,
                                               context=dict(export_ref_ca=export_ref_ca,
                                                            transaction_code=transaction_code))

    def action_update_transactions(self, cr, uid, ids, context=None):
        """Update transactions table for orders in given tag."""
        return self.pool.get('tagging.order').action_update_transactions(cr, uid, ids, context=dict(context, manifest=True))

    def action_generate_export_ref_ca(self, cr, uid, ids, context=None):
        """Add orders in sub manifest by tracking ref"""
        ids = context.get('active_ids', [])

        for manifest in self.browse(cr, uid, ids):
            for sub_manifest in manifest.sub_manifest_ids:
                for tracking in sub_manifest.tracking_ids:
                    self.generate_export_ref_ca(cr, uid, tracking.name)
        return {'type': 'ir.actions.act_window_close'}


StockPickingManifest()


class StockPickingSubManifest(osv.osv):
    _name = "stock.picking.sub.manifest"

    def _get_child_trackings(self, cr, uid, ids, field, arg, context=None):
        res = {}
        lines = []
        for submanifest in self.browse(cr, uid, ids, context=context):
            for line in submanifest.line_ids:
                lines.append(line.tracking_id.id)
            res[submanifest.id] = list(set(lines))
        return res

    _columns = {
        'manifest_id': fields.many2one('stock.picking.manifest', 'Manifest', ondelete='cascade'),
        'name': fields.char('Name', size=256, required=True, ),
        'description': fields.char('Description', size=256, required=False, ),
        'tracking_ids': fields.function(_get_child_trackings, string='Trackings', type='one2many', obj='tracking.ref', method=True, ),
        'line_ids': fields.one2many('tracking.ref.line', 'sub_manifest_id', string='Scanned Lines', ),
    }
    _defaults = {}

    def perm_write(self, cr, user, ids, fields, context=None):
        pass

    # TODO: REMOVE, unused
    def get_tracking_refs(self, cr, uid, ids, tracking_ref, context=None):
        result = [tracking_ref]
        for sub_manifest in self.browse(cr, uid, [ids]):
            if sub_manifest.tracking_refs and tracking_ref not in sub_manifest.tracking_refs: result.append(sub_manifest.tracking_refs)
        return ','.join(result)

    # TODO: REMOVE, unused
    def remove_tracking_ref(self, cr, uid, scan_line):
        ids = self.pool.get('tracking.ref').search(cr, uid, [('name', '=', scan_line.tracking_ref),
                                                             ('sub_manifest_id', '=', scan_line.sub_manifest_id.id)])
        picking_ids = self.pool.get('stock.picking').search(cr, uid, [('tracking_ref', '=', scan_line.tracking_ref)])
        message = 'has removed order from manifest {}.'.format(scan_line.manifest_id.name)
        # Write history who and when removed order from manifest
        self.pool.get('stock.picking.manifest').write_order_history(cr, uid, picking_ids, message=message)
        # Remove order ids from sub-manifest
        for picking_id in picking_ids:
            self.write(cr, uid, [scan_line.sub_manifest_id.id], {'picking_ids': [(3, picking_id)]})

        return self.pool.get('tracking.ref').unlink(cr, uid, ids)


StockPickingSubManifest()


class StockPickingManifestVerifyReport(osv.osv_memory):

    _name = "stock.picking.manifest.verify.report"

    REASON_ORDER_WAS_CANCELLED = 'Order was cancelled'
    REASON_ORDER_HAS_NO_WAREHOUSE = 'Order has no warehouse'
    REASON_ORDER_HAS_OTHER_SUB_MANIFEST = 'Order has other sub-manifest'
    REASON_ORDER_HAS_OUT_OF_DOOR_TIME = 'Order has out-of-door time'
    REASON_ORDER_NOT_IN_READY = 'Order not in ready'
    REASON_ORDER_MULTI = 'Order multi'
    REASON_ORDER_ABOVE_800 = 'Order above $800'
    REASON_ORDER_HAS_NON_ESIXTING_TRACKING = 'Sub manifest has non-existing tracking ref'
    REASON_ORDER_HAS_EMPTY_EXPORT_REF = 'Order has epmty or non FLXQ export_ref_ca'
    REASON_ORDER_NOT_IN_TAG = lambda self, x: 'order from manifest is not in tag {}'.format(x)
    REASON_ORDER_NOT_IN_MANIFEST = lambda self, x: 'order from tag {} is not in manifest'.format(x)

    COLUMN_MANIFEST = 'manifest'
    COLUMN_SUBMANIFEST = 'submanifest'
    COLUMN_TRACKING_REF = 'tracking ref'
    COLUMN_ORDER_NAME = 'delivery order name'
    COLUMN_BATCH_NAME = 'batch name'
    COLUMN_REASON = 'reason'

    OVER_800_VALUE = 800.00

    REPORT_HEADER = [
        COLUMN_MANIFEST,
        COLUMN_SUBMANIFEST,
        COLUMN_TRACKING_REF,
        COLUMN_ORDER_NAME,
        COLUMN_BATCH_NAME,
        COLUMN_REASON,
    ]

    def perm_write(self, cr, user, ids, fields, context=None):
        pass

    @staticmethod
    def _write_lines(lines):
        if not lines:
            lines = {}
        quote_style = csv.QUOTE_ALL
        output = cStringIO.StringIO()
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
        writer = csv.writer(output, dialect='csv', quoting=quote_style)
        for line in lines:
            writer.writerow([isinstance(line[col], unicode) and line[col].encode('utf-8') or str(line[col]).encode('utf-8') for col in StockPickingManifestVerifyReport.REPORT_HEADER])
        return output

    def _add_orders(self, cr, uid, sub_manifest_obj):
        sp_obj = self.pool.get('stock.picking')
        picking_ids = []
        non_existing_tracking_refs = []
        for tracking in sub_manifest_obj.tracking_ids:
            items = sp_obj.search(cr, uid, [('tracking_ref', '=', tracking.name)])
            if not items:
                non_existing_tracking_refs.append(tracking.name)
            else:
                picking_ids.extend(items)
        sub_manifest_values = {
            'picking_ids': [(6, 0, picking_ids)]
        }
        message = 'order has been added to manifest {}/{}.'.format(sub_manifest_obj.manifest_id.name, sub_manifest_obj.name)
        self.pool.get('stock.picking.manifest').write_order_history(cr, uid, picking_ids, message=message)
        self.pool.get('stock.picking.sub.manifest').write(cr, uid, sub_manifest_obj.id, sub_manifest_values)
        return non_existing_tracking_refs

    _columns = {
        "file": fields.binary(string="CSV Export", readonly=True),
        "filename": fields.char("", size=256),
        'state': fields.boolean("state"),
        "check_multiaddress": fields.boolean('Multi address', help='Check orders with multi address', store=False, ),
        "check_price_over_800": fields.boolean('Price over 800', help='Check orders with total price above 800', store=False, ),
        "check_tracking_ref": fields.boolean('Tracking ref', help='Check orders by non existiong tracking ref', store=False, ),
        "check_export_ref": fields.boolean('Export ref', help='Check orders by empty or invalid export ref ca', store=False, ),
        'order_tag': fields.many2one('tagging.order', 'Order tag', ),
    }

    _defaults = {
        'state': False,
        'file': '',
        "check_multiaddress": False,
        "check_price_over_800": False,
        "check_tracking_ref": False,
        "check_export_ref": False,
    }

    def action_generate_verify_report(self, cr, uid, ids, context=None):
        """Generate csv report for orders in given manifest to verify it by next rules:
        Required
            cancelled
            without_warehouse (for set in Done ,Returned take stock_moves with mark is_set=true per set_parent_order_id= %)
            has_other_submanifest
            out_of_door_time
        for batches
            not_in_ready
        optional
            multi
            over_800
            non existing tracking
            epmty or non FLXQ export_ref_ca
        """

        def _get_order_dict(order):
            """Check if given tracking ref in other sub-manifests"""
            order_moves = self.pool.get('stock.picking.track').get_order_moves(cr, uid, order.id)
            res = {'order_id': order.id,
                   'order_name': order.name,
                   'state': order.state,
                   'wh_id': order.warehouses.id,
                   'locations': order_moves['locations'],
                   'shipping_batch_name': order.shipping_batch_id.name or '',
                   'shipping_batch_id': order.shipping_batch_id.id,
                   'total_price': order.total_price,
                   'tracking_ref': order.tracking_ref,
                   'address_grp': order.address_grp,
                   'full_name': '{}({})'.format(order.name, order.tracking_ref),
                   }
            return res

        def _has_other_sub_manifests(order, sub_manifest):
            """Check if given tracking ref in other sub-manifests"""
            sql = """select count(DISTINCT(sub_manifest_id)), picking_id 
                        from tracking_ref_line 
                        where picking_id={} 
                        group by picking_id 
                    """.format(order.get('order_id'))
            cr.execute(sql)
            found_in = cr.fetchone()[0]
            if found_in > 1:
                _logger.info("Found in other %s submanifests" % found_in)
                return True
            return False

        _logger.info("Run report verification action for ids: %s" % ids)
        if context is None:
            context = {}
        picking_obj = self.pool.get('stock.picking')

        # Browse verification form object self object
        verify_data = self.pool.get('stock.picking.manifest.verify.report').browse(cr, uid, ids[0])

        # Get order tag if it selected on form
        order_tag = verify_data.order_tag
        tag_orders = map(lambda x: (x.do_name, x.tracking_ref), order_tag and order_tag.order_ids or [])

        # create header line
        lines = [{
            self.COLUMN_MANIFEST: self.COLUMN_MANIFEST,
            self.COLUMN_SUBMANIFEST: self.COLUMN_SUBMANIFEST,
            self.COLUMN_TRACKING_REF: self.COLUMN_TRACKING_REF,
            self.COLUMN_ORDER_NAME: self.COLUMN_ORDER_NAME,
            self.COLUMN_BATCH_NAME: self.COLUMN_BATCH_NAME,
            self.COLUMN_REASON: self.COLUMN_REASON,
        }]

        # load manifest list or except if nothing defined
        manifests = self.pool.get('stock.picking.manifest').browse(cr, uid, context.get('active_ids', []))
        if not manifests:
            raise osv.except_osv("Warning", "No manifests defined")

        # iterate over manifests, sub_manifests and lines
        for manifest in manifests:
            order_manifest = []
            # iterate over sub_manifests
            for sub_manifest in manifest.sub_manifest_ids:
                # Check orders that a not in sub_manifest but has same tracking
                non_existing_tracking_refs = self._add_orders(cr, uid, sub_manifest)
                if verify_data.check_tracking_ref and non_existing_tracking_refs:
                    for item in non_existing_tracking_refs:
                        report_row = {
                            self.COLUMN_MANIFEST: manifest.name,
                            self.COLUMN_SUBMANIFEST: sub_manifest.name,
                            self.COLUMN_TRACKING_REF: item,
                            self.COLUMN_ORDER_NAME: '',
                            self.COLUMN_BATCH_NAME: '',
                            self.COLUMN_REASON: self.REASON_ORDER_HAS_NON_ESIXTING_TRACKING,
                        }
                        lines.append(report_row)

                # Iterate over orders (scanned lines) that are in submanifest
                for scan_line in sub_manifest.line_ids:
                    order = _get_order_dict(scan_line.picking_id)
                    order_manifest.append((order['order_name'], order['tracking_ref']))
                    report_row = {
                        self.COLUMN_MANIFEST: manifest.name,
                        self.COLUMN_SUBMANIFEST: sub_manifest.name,
                        self.COLUMN_TRACKING_REF: order['tracking_ref'],
                        self.COLUMN_ORDER_NAME: order['order_name'],
                        self.COLUMN_BATCH_NAME: order['shipping_batch_name'],
                    }

                    # Check order state
                    if order['state'] == 'cancel':
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_WAS_CANCELLED,
                        }
                        lines.append(dict(report_row, **row))

                    # Check order warehouse
                    if not order['wh_id']:
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_HAS_NO_WAREHOUSE,
                        }
                        lines.append(dict(report_row, **row))

                    # Check order rescan
                    if _has_other_sub_manifests(order, sub_manifest):
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_HAS_OTHER_SUB_MANIFEST,
                        }
                        lines.append(dict(report_row, **row))

                    # Check out of doors
                    # Get tracking ref scan date
                    if not self.pool.get('stock.picking.track').get_permission_to_out(
                            cr, uid,
                            order['order_id'],
                            scan_line.create_date,
                            locations=order['locations'].values()
                    ):
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_HAS_OUT_OF_DOOR_TIME,
                        }
                        lines.append(dict(report_row, **row))

                    # If order in batch and its state not in ready
                    if order['shipping_batch_name'] and order['state'] not in ('picking',
                                                                               'outcode_except',
                                                                               'shipped',
                                                                               'done'):
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_NOT_IN_READY,
                        }
                        lines.append(dict(report_row, **row))

                    # Check multi address consolidation
                    if verify_data.check_multiaddress:
                        status, _ = self.pool.get('stock.picking.track').check_multiaddress(
                            cr, uid,
                            order,
                            order['shipping_batch_id'],
                            context=context
                        )
                        if not status:
                            row = {
                                self.COLUMN_REASON: self.REASON_ORDER_MULTI,
                            }
                            lines.append(dict(report_row, **row))

                    # Check if order total price is over $800
                    if verify_data.check_price_over_800 and float(order['total_price']) >= self.OVER_800_VALUE:
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_ABOVE_800,
                        }
                        lines.append(dict(report_row, **row))

                    # Check if order has epmty or non FLXQ export_ref_ca
                    if verify_data.check_export_ref and not all(
                            map(lambda y: y.startswith(StockPickingManifest.EXPORT_REF_CA_PREFIX),
                                map(lambda x: x['export_ref_ca'] or '',
                                    self.pool.get('stock.picking.track').get_order_moves(cr, uid, order['order_id'],
                                                                                         context=context)['moves']))):
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_HAS_EMPTY_EXPORT_REF,
                        }
                        lines.append(dict(report_row, **row))

                    # Check if tag is selected and order in tag
                    if tag_orders and order['order_name'] not in map(lambda x: x[0], tag_orders):
                        row = {
                            self.COLUMN_REASON: self.REASON_ORDER_NOT_IN_TAG(order_tag.name),
                        }
                        lines.append(dict(report_row, **row))

            # Add orders from given tag if they not exists in manifest/submanifest/scan record
            for order in set(tag_orders) - set(order_manifest):
                report_row = {
                    self.COLUMN_MANIFEST: manifest.name,
                    self.COLUMN_SUBMANIFEST: '',
                    self.COLUMN_TRACKING_REF: order[1],
                    self.COLUMN_ORDER_NAME: order[0],
                    self.COLUMN_BATCH_NAME: '',
                    self.COLUMN_REASON: self.REASON_ORDER_NOT_IN_MANIFEST(order_tag.name),
                }
                lines.append(report_row)

        # write lines to csv
        buf = self._write_lines(lines)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        return self.write(cr, uid, ids, {'state': True, 'file': out, 'filename': 'manifest_report.csv'}, context=context)


StockPickingManifestVerifyReport()

