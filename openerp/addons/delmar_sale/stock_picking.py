# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import logging
from datetime import datetime, timedelta
import pytz
from pytz import timezone
import re
import base64
# from pytz import timezone
from dateutil.relativedelta import relativedelta
from pf_utils.utils.xml_utils import _try_parse
from pf_utils.utils.re_utils import str2float
from osv import fields, osv
import netsvc
import requests
from tools.translate import _
from openerp.addons.pf_utils.utils.decorators import to_ascii
from pf_utils.utils.label_printer import LabelWizPrinter
from openerp.addons.delmar_sale.report.simple_report.simple_report import THRESHOLD_PRICE
from openerp.addons.delmar_reports.reports.invoice import invoice
from openerp.addons.delmar_sale.lib.product.product_factory import ProductFactory
from openerp.addons.delmar_sale.sale import SIGN_CANCEL, SIGN_CANCEL_VALUE, SIGN_RETURN_VALUE, SIGN_DEFAULT_VALUE, SIGN_ADJUSTMENT_VALUE

logger = logging.getLogger('delmar_sale')

STOCK_PICKING_STATES = [
    ('draft', 'New'),
    ('auto', 'Waiting Another Operation'),
    ('confirmed', 'Waiting Availability'),
    ('assigned', 'Ready to Process'),
    ('unverified', 'Unverified'),
    ('picking', 'Waiting Tracking Number'),
    ('shipped', 'Ready'),
    ('done', 'Done'),
    ('back', 'Waiting Availability (after Back to Shelf)'),
    # ('rejected', 'Rejected'),
    ('waiting_split', 'Need to Split'),
    ('address_except', 'Address Exception'),
    ('incode_except', 'Unknown Incoming Code'),
    ('outcode_except', 'Unknown Outgoing Code'),
    ('bad_format_tracking', 'Bad Format Tracking'),
    ('returned', 'Returned'),
    ('reserved', 'Reserved'),
    ('cancel', 'Cancelled'),
    ('final_cancel', 'Final cancelled'),
]

SIGNATURE_CODES = [
    (1, 'Y'),
    (2, '2'),
    (3, 'A'),
    (4, 'I'),
    (5, 'D'),
    (6, 'N'),
]

SIGNATURE = [
    ('false', None),
    ('true', 1),
    ('4', 4)
]


def full_revert(context):
    return ('flash_revert' in context) and all(map(lambda (x, y): x == y, context.get('flash_revert', {}).items()))


class stock_picking(osv.osv):
    _inherit = "stock.picking"
    _description = "Delmar Picking List"

    # BATCH SET DONE
    _set_done_options = {}

    _set_done_options_default = {
        'limit': 64,
        'cron_id': 0,
        'bad_ids': [],
        'bad_id': 0,
        'numbercall': -1,
        }

    def print_label_wiz(self, cr, uid, ids, template, context=None):

        if not template:
            raise osv.except_osv('Warning', 'Not defined template')

        order = self.browse(cr, uid, ids[0])
        label_printer = LabelWizPrinter(self.pool, cr, uid)
        if not order.id:
            raise osv.except_osv('Warning', 'Nothing to print.')
        return label_printer.print_packing_label(order.id, template)


    def print_qr_item_label_ca_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_fly_qr_label(self, cr, uid, ids, country='China', context=None):
        if ids:
            datas = {
                'id': ids[0],
                'ids': ids,
                'model': 'stock.picking',
                'report_type': 'pdf'
                }
            act_print = {
                'type': 'ir.actions.report.xml',
                'report_name': 'stock.picking.picking_return_label',
                'datas': datas,
                'context': {'made_id': country, 'active_ids': ids, 'active_id': ids[0]}
            }
            return act_print
        return True

    def print_fly_qr_label_ca(self, cr, uid, ids, context=None):
        return self.print_fly_qr_label(cr, uid, ids, country='Canada')

    def print_fly_qr_label_ch(self, cr, uid, ids, context=None):
        return self.print_fly_qr_label(cr, uid, ids, country='China')

    def _get_picking_move(self, cr, uid, ids, context=None):
        result = {}
        moves = self.pool.get('stock.move').browse(cr, uid, ids, context=context)
        for sm in moves:
            sp_id = sm.picking_id
            if sp_id:
                result[sp_id.id] = True

        return result.keys()

    def _get_picking_address(self, cr, uid, ids, context=None):
        stock_picking_obj = self.pool.get('stock.picking')
        pick_ids = stock_picking_obj.search(cr, uid, [('address_id', 'in', ids)])
        move_ids = self.pool.get('stock.move').search(cr, uid, [('address_id', 'in', ids)])
        mpick_ids = stock_picking_obj._get_picking_move(cr, uid, move_ids, context)

        reset_ids = list(set(pick_ids + mpick_ids))
        return reset_ids

    def _get_sale_orders(self, cr, uid, ids, context=None):
        result = {}
        orders = self.pool.get('sale.order').browse(cr, uid, ids, context=context)
        for so in orders:
            sp_ids = so.picking_ids
            for sp_id in sp_ids:
                result[sp_id.id] = True

        return result.keys()

    def _get_picking(self, cr, uid, ids, context=None):
        return ids

    def _get_combined_picking(self, cr, uid, ids, context=None):
        if not ids:
            return []

        cr.execute("""  SELECT DISTINCT sp2.id
                        FROM
                            stock_picking sp LEFT JOIN stock_picking sp2
                                ON (sp.id = sp2.id or sp.shipping_batch_id = sp2.shipping_batch_id)
                        WHERE
                            1 = 1
                            AND sp.id IN %s
                        ;""", (tuple(ids), ))
        res = [x[0] for x in cr.fetchall() or [] if x]
        return res

    def get_address_grp_picking(self, cr, uid, ids, context=None):
        if not ids:
            return []

        cr.execute("""  SELECT DISTINCT sp2.id
                        FROM stock_picking sp
                            LEFT JOIN stock_picking sp2 ON (sp.id = sp2.id or sp.address_grp = sp2.address_grp)
                            LEFT JOIN res_partner rp ON rp.id = sp2.real_partner_id
                        WHERE sp.id IN %s AND (
                            sp.id = sp2.id OR
                            (rp.virtual IS NOT TRUE AND sp2.state not in ('done', 'cancel', 'final_cancel', 'returned'))
                        );""", (tuple(ids), ))
        res = [x[0] for x in cr.fetchall() or [] if x]
        return res

    def _get_picking_partners(self, cr, uid, ids, context=None):
        result = []
        if ids:
            cr.execute('SELECT DISTINCT (sp.id) FROM stock_picking sp WHERE real_partner_id in %s', (tuple(ids), ))
            orders = cr.fetchall() or []
            result = [x[0] for x in orders if x]
        return result

    def _get_picking_to_reset(self, cr, uid, ids, context=None):
        reset_ids = self.search(cr, uid, [('id', 'in', ids), ('state', 'in', ['cancel', 'move', 'picking', 'shipped'])])
        return reset_ids

    def _get_shelves(self, cr, uid, ids, name, args, context=None):
        res = {}
        for sp_id in ids:
            shelves = []
            res[sp_id] = ''
            moves = self.browse(cr, uid, sp_id).move_lines
            for move in moves:
                shelf = move.bin
                if shelf and shelf not in shelves:
                    shelves.append(shelf)
            if shelves:
                res[sp_id] += shelves[0]
                if shelves[1:]:
                    for shelf in shelves[1:]:
                        res[sp_id] += ' ' + shelf
        return res

    def _get_delmar_ids(self, cr, uid, ids, name, args, context=None):
        res = {}
        for sp_id in ids:
            delmar_ids = []
            moves = self.browse(cr, uid, sp_id).move_lines
            for move in moves:
                d_id = move.product_id
                code = d_id and d_id.default_code and d_id.default_code or False
                size = move.size_id and move.size_id.name or False

                if code and size:
                    code = '%s/%s' % (code, size)

                if code and code not in delmar_ids:
                    delmar_ids.append(code)

            res[sp_id] = ", ".join(delmar_ids)

        return res

    def _get_locations(self, cr, uid, ids, name, args, context=None):
        res = {}
        for sp_id in ids:
            locations = []
            moves = self.browse(cr, uid, sp_id).move_lines
            for move in moves:
                loc = move.location_id
                if loc and loc.complete_name not in locations:
                    locations.append(loc.complete_name)
            res[sp_id] = ", ".join(locations)
        return res

    def _get_warehouses(self, cr, uid, ids, name, args, context=None):
        res = {}
        location_obj = self.pool.get('stock.location')
        for sp in self.browse(cr, uid, ids):
            wh_ids = {}
            for move in sp.move_lines:
                wh_id = False
                if move.location_id:
                    wh_id = location_obj.get_warehouse(cr, uid, move.location_id.id)

                if not wh_id:
                    wh_id = 0

                if not wh_ids.get(wh_id, False):
                    wh_ids[wh_id] = []

                wh_ids[wh_id].append(move)

            res[sp.id] = wh_ids
        return res

    def _get_price(self, cr, uid, ids, name, args, context=None):
        res = {}

        if not ids:
            return res

        for order_id in ids:
            res[order_id] = {
                'total_price': 0.0,
                'total_price_rules': 0.0,
                'total_unit_cost': 0.0,
                'shipping_batch_price': 0.0,
                'order_321_price': 0.0
                }

        if 'total_unit_cost' in name:
            cr.execute("""
                select
                    sm.picking_id as id,
                    sum( sol.price_unit * sm.product_qty ) as order_price
                from
                    stock_move sm left join sale_order_line sol on
                    sm.sale_line_id = sol.id
                where
                           sm.id IN (SELECT r.id
                                    FROM
                                      (SELECT id, set_product_id, product_id
                                       FROM stock_move
                                       WHERE picking_id in %s) r
                                    WHERE r.set_product_id != r.product_id OR r.set_product_id IS NULL)

                group by
                    sm.picking_id
            """, (tuple(ids),))
            for row in cr.dictfetchall():
                res[row['id']]['total_unit_cost'] = float(row.get('order_price', 0) or 0)

        if ('total_price' in name) or ('total_price_rules' in name):
            picking = self.pool.get('stock.picking').browse(cr, uid, ids)
            partner = picking and picking[0].real_partner_id
            price_field = partner and partner.price_field or False
            if price_field:
                # Get total_price from additional fields if it's needs
                # For example Bluesteam with merchandise_amount
                cr.execute("""  SELECT sof.value as order_price, sp.id AS id
                    FROM stock_picking as sp
                    LEFT JOIN sale_order_fields as sof ON sof.order_id = sp.sale_id AND sof.name = %s
                    WHERE sp.id in %s
                """, (price_field, tuple(ids)))
                for row in cr.dictfetchall():
                    if 'total_price' in name:
                        res[row['id']]['total_price'] = float(row.get('order_price', 0.0))
                    if 'total_price_rules' in name:
                        res[row['id']]['total_price_rules'] = float(row.get('order_price', 0.0))
            else:
                if 'total_price' in name:
                    cr.execute("""
                        SELECT
                          sm.picking_id AS id,
                          sum((CASE WHEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT) > 0.0
                            THEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT)
                               WHEN cast(coalesce(sol."customerCost", '0.0') AS FLOAT) = 0.0 AND sol.order_partner_id IN
                                                                                                 (48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936)
                                 THEN sol.price_unit
                               ELSE 0.0 END) * sm.product_qty) AS order_price
                        FROM
                          stock_move sm LEFT JOIN sale_order_line sol ON
                                                                        sm.sale_line_id = sol.id
                        WHERE
                          sm.id IN (SELECT r.id
                                    FROM
                                      (SELECT id, set_product_id, product_id
                                       FROM stock_move
                                       WHERE picking_id in %s) r
                                    WHERE r.set_product_id != r.product_id OR r.set_product_id IS NULL)
                        GROUP BY sm.picking_id
                    """, (tuple(ids),))
                    for row in cr.dictfetchall():
                        res[row['id']]['total_price'] = float(row.get('order_price', 0.0))

                if 'total_price_rules' in name:
                    cr.execute("""
                select
                    sm.picking_id as id,
                    sum(( case when cast( coalesce( sol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( sol."customerCost", '0.0' ) as float ) else sol.price_unit end )* sm.product_qty ) as order_price
                from
                    stock_move sm left join sale_order_line sol on
                    sm.sale_line_id = sol.id
                where
                    sm.id in(
                        select
                            r.id
                        from
                            (
                                select
                                    id,
                                    set_product_id,
                                    product_id
                                from
                                    stock_move
                                where
                                    picking_id in %s
                            ) r
                        where
                            r.set_product_id != r.product_id
                            or r.set_product_id is null
                    )
                group by
                    sm.picking_id
                    """, (tuple(ids),))
                    for row in cr.dictfetchall():
                        res[row['id']]['total_price_rules'] = float(row.get('order_price', 0.0))

        if 'order_321_price' in name:
            cr.execute("""
                select
                    sm.picking_id as id,
                    sum(( case when cast( coalesce( sol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( sol."customerCost", '0.0' ) as float ) when cast( coalesce( sol."customerCost", '0.0' ) as float )= 0.0 and sol.order_partner_id in( 48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936 ) then sol.price_unit else 0.0 end ) * sm.product_qty ) as order_321_price
                from
                    stock_move sm left join sale_order_line sol on
                    sm.sale_line_id = sol.id
                where
                    sm.picking_id in %s
                group by
                    sm.picking_id
                having
                    min(( case when cast( coalesce( sol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( sol."customerCost", '0.0' ) as float ) when cast( coalesce( sol."customerCost", '0.0' ) as float )= 0.0 and sol.order_partner_id in( 48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936 ) then sol.price_unit else 0.0 end ) * sm.product_qty )> 0

                            ;
                """, (tuple(ids), ))
            for row in cr.dictfetchall():
                res[row['id']]['order_321_price'] = float(row.get('order_321_price', 0.0))

        if 'address_grp_price' in name:
            # FIXME: [YT] hardcoded states!!!
            cr.execute("""
                select
                    sp.id as id,
                    sp.address_grp_price,
                    coalesce(
                        sp2.order_price,
                        0.0
                    ) as group_price
                from
                    stock_picking sp left join(
                        select
                            sp.address_grp as address_grp,
                            sum(( case when cast( coalesce( ol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( ol."customerCost", '0.0' ) as float ) when cast( coalesce( ol."customerCost", '0.0' ) as float )= 0.0 and ol.order_partner_id in( 48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936 ) then ol.price_unit else 0.0 end )* sm.product_qty ) as order_price
                        from
                            stock_picking sp left join stock_move sm on
                            sm.picking_id = sp.id left join sale_order_line ol on
                            ol.id = sm.sale_line_id
                        where
                            sp.address_grp in(
                                select
                                    address_grp
                                from
                                    stock_picking
                                where
                                    id in %(ids)s
                            )
                            and sp.state in(
                                'address_except',
                                'assigned',
                                'back',
                                'confirmed',
                                'picking',
                                'shipped',
                                'waiting_split'
                            )
                        group by
                            sp.address_grp
                    ) sp2 on
                    (
                        sp.address_grp = sp2.address_grp
                    )
                where
                    sp.id in %(ids)s
            """, {'ids': tuple(ids)})
            for row in cr.dictfetchall():
                res[row['id']]['address_grp_price'] = float(row.get('group_price', 0.0))

        if 'shipping_batch_price' in name:
            # FIXME: [YT] hardcoded states!!!
            cr.execute("""
                select
                    sp.id as id,
                    sum( coalesce( sp2.order_price, 0.0 )) as group_price
                from
                    stock_picking sp left join(
                        select
                            sp.id as id,
                            sp.shipping_batch_id as shipping_batch_id,
                            sum(( case when cast( coalesce( ol."customerCost", '0.0' ) as float )> 0.0 then cast( coalesce( ol."customerCost", '0.0' ) as float ) when cast( coalesce( ol."customerCost", '0.0' ) as float )= 0.0 and ol.order_partner_id in( 48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936 ) then ol.price_unit else 0.0 end )* sm.product_qty ) as order_price
                        from
                            stock_picking sp left join stock_move sm on
                            sm.picking_id = sp.id left join sale_order_line ol on
                            sm.sale_line_id = ol.id
                        where
                            sp.state in(
                                'address_except',
                                'assigned',
                                'back',
                                'confirmed',
                                'picking',
                                'shipped',
                                'waiting_split'
                            )
                            and coalesce(
                                sp.shipping_batch_id,
                                - sp.id
                            ) in(
                                select
                                    coalesce(
                                        shipping_batch_id,
                                        - id
                                    )
                                from
                                    stock_picking
                                where
                                    id in %(ids)s
                            )
                        group by
                            sp.id,
                            sp.shipping_batch_id
                    ) sp2 on
                    (
                        sp.id = sp2.id
                        or sp.shipping_batch_id = sp2.shipping_batch_id
                    )
                where
                    1 = 1
                    and sp.id in %(ids)s
                group by
                    sp.id
            """, {'ids': tuple(ids)})
            for row in cr.dictfetchall():
                res[row['id']]['shipping_batch_price'] = float(row.get('group_price', 0.0))
        return res

    def _get_address_grp_field(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for order_id in ids:
                res[order_id] = False

            sql = """   SELECT
                            sp.id as id,
                            concat(sw.name, E'/', rp.ref, E'/', ra.name, E'/', rc.name, E'/', ra.zip) as address
                        FROM stock_picking sp
                            LEFT JOIN res_partner rp on sp.real_partner_id = rp.id
                            LEFT JOIN res_partner_address ra on sp.address_id = ra.id
                            LEFT JOIN res_country rc on ra.country_id = rc.id
                            LEFT JOIN stock_warehouse sw on sp.warehouses = sw.id
                        WHERE sp.id in %s
            """
            cr.execute(sql, (tuple(ids), ))
            result_sql = cr.dictfetchall()

            for row in result_sql:
                res[row['id']] = str(row['address']).lower()

        return res

    def _get_address_grp_field_fg(self, cr, uid, ids, name, args, context=None):
            res = {}
            if ids:
                for order_id in ids:
                    res[order_id] = False

                sql = """SELECT
                            sp.id as id,
                            concat(ra.street, E'/', ra.street2, E'/', ra.city, E'/', rs.name, E'/', ra.zip, E'/', rc.name) as address
                         FROM stock_picking sp
                            LEFT JOIN res_partner rp on sp.real_partner_id = rp.id
                            LEFT JOIN res_partner_address ra on sp.address_id = ra.id
                            LEFT JOIN res_country rc on ra.country_id = rc.id
                            LEFT JOIN stock_warehouse sw on sp.warehouses = sw.id
                            LEFT JOIN res_country_state rs on rs.id = ra.state_id
                         WHERE sp.id in %s
                """
                cr.execute(sql, (tuple(ids),))
                result_sql = cr.dictfetchall()

                for row in result_sql:
                    res[row['id']] = str(row['address']).lower()

            return res

    def _get_address_grp_combined(self, cr, uid, ids, name, args, context=None):
            res = {}
            if ids:
                for order_id in ids:
                    res[order_id] = False

                sql = """SELECT
                            sp.id as id,
                            concat(rp.ref, E'/', ra.name, E'/', ra.street, E'/', ra.street2, E'/', ra.city, E'/', rs.name, E'/', ra.zip, E'/', rc.name) as address
                         FROM stock_picking sp
                            LEFT JOIN res_partner rp on sp.real_partner_id = rp.id
                            LEFT JOIN res_partner_address ra on sp.address_id = ra.id
                            LEFT JOIN res_country rc on ra.country_id = rc.id
                            LEFT JOIN stock_warehouse sw on sp.warehouses = sw.id
                            LEFT JOIN res_country_state rs on rs.id = ra.state_id
                         WHERE sp.id in %s
                """
                cr.execute(sql, (tuple(ids),))
                result_sql = cr.dictfetchall()

                for row in result_sql:
                    res[row['id']] = str(row['address']).lower()

            return res

    def _get_printed_date(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for order_id in ids:
                res[order_id] = False

            sql = """SELECT
                    sl.res_id as order_id,
                    min(sl.create_date) as printed_date
                FROM stock_picking_workflow_log sl
                    INNER JOIN wkf_activity wa ON wa.id = sl.act_id AND wa.name = 'picking'
                WHERE sl.res_id in %s
                GROUP BY sl.res_id
            """
            cr.execute(sql, (tuple(ids), ))
            result_sql = cr.dictfetchall()

            for row in result_sql:
                res[row['order_id']] = row.get('printed_date', False) or False

        return res

    def _flat_rate_priority_id(self, cr, uid, ids, name, args, context=None):
        res = {x: None for x in ids}
        if ids:
            priority_obj = self.pool.get('priority.dimension')
            shp_code_obj = self.pool.get('res.shipping.code')
            cr.execute("""
                select
                    sp.id as id,
                    ra.zip as zip,
                    ss.id as service_id,
                    st.id as type_id
                from stock_picking sp
                    left join res_partner rp on sp.real_partner_id = rp.id
                    left join res_partner_address ra on sp.address_id = ra.id
                    left join res_country rc on rc.id = ra.country_id
                    left join res_shipping_service ss on ss.name =  split_part(sp.carrier, ', ', 1)
                    left join res_shipping_type st on st.name =  split_part(sp.carrier, ', ', 2)
                where 1=1
                    and sp.id in %s
                    and rp.ignore_priority_dimension is not True
                    and ra.zip is not null
                    and coalesce(rc.code, 'US') = 'US'
            """, (tuple(ids), ))
            for pick in cr.dictfetchall() or []:
                if pick['zip']:
                    priority = None
                    if pick['service_id'] and pick['type_id']:
                        priority = priority_obj.get_priority(cr, uid, pick['zip'], pick['service_id'], pick['type_id'])
                    else:
                        code = shp_code_obj.get_shipping_service(cr, uid, picking_id=pick['id'], fields=("service_id", "type_id"))
                        if code:
                            priority = priority_obj.get_priority(cr, uid, pick['zip'], code['service_id'], code['type_id'])
                    if priority:
                        res[pick['id']] = (priority['id'], priority['name'])
        return res

    def _get_so_shp_handling(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            res = {x: None for x in ids}

            cr.execute("""
                select
                    sm.picking_id,
                    sum(str_to_float(ol."shipCost")) as shp
                from stock_move sm
                    left join sale_order_line ol on sm.sale_line_id = ol.id
                where sm.picking_id in %s
                group by sm.picking_id
            """, (tuple(ids), ))
            res.update(dict(cr.fetchall() or []))
        return res

    def _get_tags_search(self, cr, uid, obj, name, args, context):
        if not context:
            context = {}
        tag = None
        result = []
        if args and args[0] and args[0][0] and args[0][0] == 'tag_id' and args[0][2]:
            tag = args[0][2]

        if tag:
            sql = """SELECT distinct sp.id
                FROM taggings_order as ts
                LEFT JOIN order_history as oh on ts.order_id = oh.id
                LEFT JOIN stock_move as sm on sm.id = oh.sm_id
                LEFT JOIN stock_picking as sp on sp.id = sm.picking_id
                LEFT JOIN tagging_order tg on ts.tag_id = tg.id
                WHERE {field} = %s
            """.format(field='ts.tag_id' if isinstance(tag, (int, long)) else 'tg.name')
            params = [tag]
            cr.execute(sql, params)
            sql_res = cr.fetchall()
            if sql_res:
                for order_id in sql_res:
                    if order_id:
                        result.append(order_id[0])
        res = [('id', 'in', result)]

        return res

    def _get_set_parent_line(self, cr, uid, move_id):
        move = self.pool.get('stock.move').browse(cr, uid, move_id)
        picking = self.browse(cr, uid, move.picking_id.id)
        if picking.set_parent_move_lines:
            for set_line in picking.set_parent_move_lines:
                if move.set_product_id.id == set_line.set_product_id.id and set_line.set_parent_order_id.id is not None:
                    return set_line

        return False

    def _fake_field_for_search (self, cr, uid, ids, name, args, context=None):
        """Most functional method ;)"""
        return {}

    def _search_by_xml(self, cr, uid, obj, name, args, context):
        ids = tuple()
        for search_item in args:
            if search_item[0] == 'search_by_xml':
                like_str = search_item[2].replace("'", "''")
                sql = """SELECT
                         sp.id FROM stock_picking sp
                         LEFT JOIN sale_order so ON sp.sale_id = so.id
                         LEFT JOIN sale_integration_xml six ON so.sale_integration_xml_id = six.id
                        WHERE sp.state = 'done'
                        AND six.xml LIKE '%{}%';
                    """.format(like_str)
                cr.execute(sql)
                ids = tuple([id[0] for id in cr.fetchall()])
        if ids:
            return [('id', 'in', ids)]
        return [('id', '=', '0')]

    def _search_by_state(self, cr, uid, obj, name, args, context):
        ids = tuple()
        for search_item in args:
            if search_item[0] == 'search_by_state':
                like_str = search_item[2].replace("'", "''")
                sql = """SELECT sp.id FROM stock_picking sp
                            INNER JOIN res_partner_address rpa ON sp.address_id = rpa.id
                            INNER JOIN res_country_state rcs ON rpa.state_id = rcs.id
                            WHERE sp.state = 'done'
                             AND rcs.name ILIKE '{}%';
                    """.format(like_str)
                cr.execute(sql)
                ids = tuple([id[0] for id in cr.fetchall()])
        if ids:
            return [('id', 'in', ids)]
        return [('id', '=', '0')]

    def _orders_with_changed_sizes(self, cr, uid, obj, name, args, context):
        ids = tuple()

        for search_item in args:
            if search_item[0] == 'orders_with_changed_sizes':
                number_value = int(search_item[2])

                if number_value == 1:
                    sql = """
                    SELECT sp.id FROM stock_picking sp
                    INNER JOIN stock_move sm ON sp.id = sm.picking_id
                    INNER JOIN sale_order so ON sp.sale_id = so.id
                    INNER JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                    WHERE (
                    (    sm.product_id <> sol.product_id
                        AND sm.size_id <> sol.size_id   )
                    OR (    sm.product_id = sol.product_id
                            AND sm.size_id <> sol.size_id   )
                            )
                    AND sp.state NOT IN ('picking','shipped','done','bad_format_tracking') 
                    AND sm.is_no_resize is not true
                    AND sp.type = 'out';"""

                elif number_value == 2:
                    sql = """
                    SELECT sp.id
                    FROM stock_picking sp
                    WHERE id NOT IN (
                       SELECT sp.id
                       FROM stock_picking sp
                         INNER JOIN stock_move sm ON sp.id = sm.picking_id
                         INNER JOIN sale_order so ON sp.sale_id = so.id
                         INNER JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                       WHERE (sm.product_id <> sol.product_id
                              AND sm.size_id <> sol.size_id)
                             OR (sm.product_id = sol.product_id
                                 AND sm.size_id <> sol.size_id)
                             AND sp.state NOT IN ('picking','shipped','done', 'bad_format_tracking') 
                             AND sm.is_no_resize is not true
                             AND sp.type = 'out' 
                    )
                    AND sp.state NOT IN ('picking','shipped','done','bad_format_tracking') 
                    AND sp.type = 'out';"""

                cr.execute(sql)
                ids = tuple([id[0] for id in cr.fetchall()])
        if ids:
            return [('id', 'in', ids)]
        return [('id', '=', '0')]

    def _get_under_1(self, cr, uid, ids, name, args, context=None):
        response = {}
        for pick_id in ids:
            sql = """SELECT count(*)
                FROM sale_order_line sol
                LEFT JOIN stock_move sm ON sm.sale_line_id = sol.id
                LEFT JOIN stock_picking sp ON sp.id = sm.picking_id
                WHERE 1=1
                and sp.id={}
                and 
                CASE WHEN
                    (CAST( COALESCE( sol."customerCost", '0.0') as float ) = 0.0
                    and sp.real_partner_id in (48,22,4,23,596,597,598,49,51,131,50,173,224,144,944,38,89,88,100,872,43,870,881,130,208,924,936))
                    then sol.price_unit < 1
                    ELSE
                    CAST(coalesce(sol."customerCost", '0') AS float) < 1
                END 
                """.format(pick_id)
            cr.execute(sql)
            res = cr.fetchone()
            response[pick_id] = res and res[0] or False
        return response

    _columns = {
        'name': fields.char('Reference', size=64, required=False,
                            readonly=True, states={'draft': [('readonly', False)]}, select=True),
        'default_name': fields.char('Default Reference', size=64, required=True,
                                    readonly=True, states={'draft': [('readonly', False)]}, select=True),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True),
        'tracking_ref': fields.char('Tracking Ref', size=32),
        'real_partner_id': fields.many2one('res.partner', 'Partner', select=True),
        'shelves': fields.function(_get_shelves,
                                   string="Shelves",
                                   type='text',
                                   method=True,
                                   store={
                                       'stock.move': (_get_picking_move, ['price_unit', 'product_qty', 'qty', 'bin'], 10),
                                       'stock.picking': (_get_picking, ['move_lines'], 10),
                                       },
                                   ),
        'delmar_ids': fields.function(_get_delmar_ids,
                                      string="Delmar ids",
                                      type='text',
                                      method=True,
                                      ),
        'locations': fields.char('Locations', size=512),
        'warehouses': fields.many2one("stock.warehouse", "Warehouse", select=True),
        'total_unit_cost': fields.function(_get_price,
                                       string="Unit cost", type='float', method=True, multi="total_unit_cost",
                                       store={
                                           'stock.move': (_get_picking_move, ['price_unit', 'product_qty', 'qty'], 8),
                                           'sale.order': (_get_sale_orders, ['price_unit', 'product_qty', 'qty'], 8),
                                           'stock.picking': (_get_picking, ['move_lines', 'shp_date'], 8),
                                           }
        ),
        'total_price': fields.function(_get_price,
                                       string="Price", type='float', method=True, multi="total_price",
                                       store={
                                           'stock.move': (_get_picking_move, ['price_unit', 'product_qty', 'qty'], 8),
                                           'sale.order': (_get_sale_orders, ['price_unit', 'product_qty', 'qty'], 8),
                                           'stock.picking': (_get_picking, ['move_lines', 'shp_date'], 8),
                                           }
        ),
        'total_price_rules': fields.function(_get_price,
                                       string="Price rule", type='float', method=True, multi="total_price_rules",
                                       store={
                                           'stock.move': (_get_picking_move, ['price_unit', 'product_qty', 'qty'], 8),
                                           'sale.order': (_get_sale_orders, ['price_unit', 'product_qty', 'qty'], 8),
                                           'stock.picking': (_get_picking, ['move_lines', 'shp_date'], 8),
                                       }
                                       ),
        'shipping_batch_price': fields.function(_get_price,
                                                string="Group Price", type='float', method=True, multi="shipping_batch_price",
                                                store=False,
                                                ),
        'order_321_price': fields.function(_get_price,
                                        string="321 Price", type='float', method=True, multi="order_321_price",
                                        store=False,
                                        ),
        'address_grp_price': fields.function(_get_price,
                                             string="Address Price", type='float', method=True, multi="address_grp_price",
                                             store={
                                                 'stock.move': (_get_picking_move, ['price_unit', 'product_qty', 'qty'], 10),
                                                 'sale.order': (_get_sale_orders, ['price_unit', 'product_qty', 'qty'], 10),
                                                 'stock.picking': (get_address_grp_picking, ['warehouses', 'address_id', 'move_lines', 'shp_date', 'state'], 8),
                                                 'res.partner': (_get_picking_partners, ['ref'], 10),
                                                 'res.partner.address': (_get_picking_address, ['name', 'zip', 'country_id'], 10),
                                                 }
        ),
        'state': fields.selection(STOCK_PICKING_STATES, 'State', readonly=True, select=True,
                                  help="* Draft: not confirmed yet and will not be scheduled until confirmed\n" \
                                       "* Confirmed: still waiting for the availability of products\n" \
                                       "* Available: products reserved, simply waiting for confirmation.\n" \
                                       "* Waiting: waiting for another move to proceed before it becomes automatically available (e.g. in Make-To-Order flows)\n" \
                                       "* Done: has been processed, can't be modified or cancelled anymore\n" \
            ),
        'move_type': fields.selection([
                                          ('direct', 'Partial Delivery'),
                                          ('one', 'All at once')], 'Delivery Method',
                                      readonly=True, required=True,
                                      help="It specifies goods to be delivered all at once or by direct delivery"
        ),
        'first_class_mail_type_usps': fields.selection([
                                                           ('Letter', 'Letter'),
                                                           ('Flat', 'Flat'),
                                                           ('Parcel', 'Parcel'),
                                                           ('Postcard', 'Postcard')
                                                       ], 'First Class Mail Type', size=50),
        'date_expected': fields.datetime('Expected date', help="Expected date"),
        'shp_date': fields.datetime('Shipped Date', readonly=False, select=True, help="Date on which sales order is shipped."),
        'carrier': fields.char('Carrier', size=128),
        'carrier_code': fields.char('Carrier code', size=128),
        'shp_code': fields.char('Shipping code', size=128),
        'shp_signature': fields.selection(SIGNATURE_CODES, 'Shipping signature' ),
        'shp_account': fields.char('Shipping account', size=256),
        'shp_account_country': fields.char('Shipping account country', size=2),
        'billing_method': fields.char('Shipping account Billing Method', size=100),
        'shp_service': fields.char('Shipping service', size=256),
        'shp_insurance': fields.boolean('Insurance', required=True),
        'ship_from': fields.many2one('res.partner.address', 'Ship From'),
        'invoiced_rate': fields.related('sale_id', 'invoiced_rate',  string='Invoiced', type='float'),
        'picked_rate': fields.related('sale_id', 'picked_rate',  string='Picked', type='float'),
        'wkf_log_ids': fields.one2many('stock.picking.workflow.log', 'res_id', 'Workflow Logs'),
        'report_history': fields.text('History'),
        'tech_report_history': fields.text('Technical History'),
        'invoice_no': fields.char('Invoice No', size=128),
        'type': fields.selection([
            ('out', 'Sending Goods'),
            ('in', 'Getting Goods'),
            ('internal', 'Internal'),
            ], 'Shipping Type', required=True, select=True,
            help="Shipping type specify, goods coming in or going out.", ),
        'date_out': fields.datetime('Date Out'),
        'list_ids': fields.many2many('stock.picking.out.list', "stock_out_list_picking_rel", "picking_id", "list_id", string="PAPS"),
        'scan_date': fields.datetime('Scan Date'),
        'scan_uid': fields.many2one('res.users', 'Scan users',),
        'fixed_shp_code': fields.boolean(
            'Fixed Shipping Code',
            help="""Select this checkbox if you want to specify shipping service ignore current rules.
            Please, pay attention that if this flag is selected you should fill/update manually following fields:
            - Carrier,
            - Shipping code,
            - Shipping service,
            - Shipping Handling,
            - Shipping signature,
            - Insurance,
            - Shipping account.
            """,
            ),
        'process_by_item': fields.boolean('Print by Item'),
        'process_by_machine': fields.boolean('Process by Machine'),
        'address_grp': fields.function(_get_address_grp_field,
                                       string="Address",
                                       type='char',
                                       method=True,
                                       store={
                                           'res.partner': (_get_picking_partners, ['ref'], 8),
                                           'res.partner.address': (_get_picking_address, ['name', 'zip', 'country_id'], 8),
                                           'stock.picking': (_get_picking, ['warehouses', 'address_id'], 8),
                                           },
                                       ),
        'address_grp_fg': fields.function(_get_address_grp_field_fg,
                                          string="Address2",
                                          type='char',
                                          method=True,
                                          store={
                                              'res.partner': (_get_picking_partners, ['ref'], 8),
                                              'res.partner.address': (
                                                  _get_picking_address, ['name', 'zip', 'country_id'], 8),
                                              'stock.picking': (_get_picking, ['warehouses', 'address_id'], 8),
                                          },
                                          ),
        'address_grp_combined': fields.function(_get_address_grp_combined,
                                                string="Address3",
                                                type='char',
                                                method=True,
                                                store={
                                                    'res.partner': (_get_picking_partners, ['ref'], 8),
                                                    'res.partner.address': (
                                                        _get_picking_address, ['name', 'zip', 'country_id'], 8),
                                                    'stock.picking': (_get_picking, ['warehouses', 'address_id'], 8),
                                                },
                                                ),
        'under_1': fields.function(_get_under_1,
                                   string="Retail price below 1",
                                   type='boolean',
                                   method=True,
                                   store=True,
                                   ),
        'shipping_batch_id': fields.many2one('stock.picking.shipping.batch', 'Shipping Batch', ondelete='set null'),
        'combined': fields.boolean('Combined', group_operator="sum", readonly=True,),
        'shp_cost': fields.float('SHP Cost'),
        'shp_weight': fields.float('Shipping Weight'),
        'machine_tracking_ref': fields.char('Machine Tracking Ref', size=256),
        'machine_status': fields.char('Machine Status', size=256),
        'shp_bulk_id': fields.many2one('stock.picking.bulk', 'Ship in Bulk', ondelete="set null", ),
        'shp_bulk_state': fields.related('shp_bulk_id', 'state', string='State', type='char', readonly=True, ),
        'flash': fields.related('sale_id', 'flash', string='Flash', type='boolean', readonly=True, ),
        'printed_date': fields.function(_get_printed_date, strig='Printed Date', type='datetime', method=True, ),
        'printed_bulk_label_date': fields.datetime('Printed Bulk Label Date', ),
        'product_sku_filter': fields.char('Product SKU', size=256, ),
        'flat_rate_priority_id': fields.function(_flat_rate_priority_id, string="Priority", type="many2one", relation="priority.flat.rate", method=True),
        'shp_date_from': fields.function(lambda *a, **k: {}, method=True, type='datetime', string="Shipped date from"),
        'shp_date_to': fields.function(lambda *a, **k: {}, method=True, type='datetime', string="Shipped date to"),
        'tag_id': fields.function(lambda *a, **k: {}, fnct_search=_get_tags_search, method=True, type='many2one', relation="tagging.order", string="Tags"),
        'recalculate': fields.boolean('Show', ),
        'shp_handling': fields.float(
            'Shipping Handling',
            help="Shipping handling calculated based on Shipping Cost in sale order lines or shipping rules if first ome is missing",
            ),
        'shp_handling_from_sale_order': fields.function(
            _get_so_shp_handling,
            string="SO Shipping Handling",
            method=True, type="float", store=False,
            help="Shipping handling calculated based on Shipping Cost in sale order lines",
        ),
        'internal_shipment_id': fields.one2many('internal.shipment', 'picking_id', 'Internal Shipment'),
        'ret_invoice_no': fields.char('Returned Invoice', size=512),
        'ret_date': fields.datetime('Return Date', readonly=False, select=True, help="Date on which deliver order is returned."),
        'warehouse_fed_shp': fields.char('Warehouse that the items shipped from (WH field in the SHP table)', size=32),
        'warehouse_fdx_hed': fields.char('Warehouse that the items came from (WH field in HED table)', size=32),
        'scanner': fields.char('', size=32),
        'is_set': fields.boolean('is Set', readonly=True),
        'set_parent_move_lines': fields.one2many('stock.move', 'set_parent_order_id', 'set parent order line',
                                      states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}),
        'search_by_xml': fields.function(_fake_field_for_search, string="XML", type='char',
                                         method=True, store=False, fnct_search=_search_by_xml, ),
        'search_by_state': fields.function(_fake_field_for_search, string="State", type='char',
                                         method=True, store=False, fnct_search=_search_by_state, ),
        'orders_with_changed_sizes': fields.function(_fake_field_for_search, string="Changed sizes", type='selection', method=True,
                                    selection=[(1,'Orders with changed sizes'), (2,'Orders with not changed sizes')],
                                    store=False, fnct_search=_orders_with_changed_sizes, ),
    }

    _defaults = {
        'date_expected': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
        'name': False,
        'default_name': lambda self, cr, uid, context: '/',
        'move_type': 'one',
        'first_class_mail_type_usps': 'Letter',
        'date_out': False,
        'list_ids': [(6, 0, [])],
        'scan_date': False,
        'scan_uid': False,
        'fixed_shp_code': False,
        'process_by_item': False,
        'process_by_machine': False,
        'shipping_batch_id': False,
        'combined': False,
        'is_set': False
        }

    _order = "date desc"

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=True):
        """Override method in openerp\osv\orm.py to add order by count(real_partner_id)."""
        result = super(stock_picking, self).read_group(cr, uid, domain, fields, groupby, offset=offset, limit=limit, context=context, orderby=orderby)
        # The trick itself
        for group_item in groupby:
            if all(map(lambda x, gi=group_item: '{}_count'.format(gi) in x, result)):
                result.sort(key=lambda x: x['{}_count'.format(group_item)], reverse=True)
        return result

    def write(self, cr, uid, ids, vals, context=None):
        logger.info("WRITE STOCK_PICKING VALS: %s, CONTEXT: %s" % (vals, context))
        pick_ids = ids
        if isinstance(ids, int):
            pick_ids = [ids]
        # DLMR-1190
        # on changing state to unverified - update qty_verified to all lines to 0
        if 'state' in vals and vals.get('state') in ['unverified', 'confirmed', 'draft', 'cancel']:
            move_ids_to_zero = []
            for order in self.browse(cr, uid, pick_ids):
                flash = order.sale_id.flash
                if (not flash) or (flash and vals.get('state') != 'confirmed'):
                    for move in order.move_lines:
                        move_ids_to_zero.append(move.id)
            logger.info("WRITE ZEROES TO MOVE LINES: %s" % move_ids_to_zero)
            self.pool.get('stock.move').write(cr, uid, move_ids_to_zero, {'qty_verified': 0})
            if vals.get('state') in ['cancel']:
                logger.info("WRITE ZEROES TO TRANSACTIONS FOR SM_IDS: %s" % move_ids_to_zero)
                self.pool.get('stock.move').unverify_transaction(cr, uid, move_ids_to_zero, context=None)

        batch_vals = {}
        batch_fields = [
            'tracking_ref',
            'carrier_code',
            'warehouse_fed_shp',
            'warehouse_fdx_hed',
        ]

        if 'shipping_batch_id' in vals:
            vals['combined'] = bool(vals.get('shipping_batch_id', False))

        for batch_field in batch_fields:
            if batch_field in vals:
                batch_vals[batch_field] = vals[batch_field]

        # Write who has changed tracking_ref
        if vals.get('tracking_ref'):
            time_now = time.strftime('%m-%d %H:%M:%S', time.gmtime())
            scan_user = self.pool.get('res.users').browse(cr, uid, uid)
            for order in self.browse(cr, uid, pick_ids):
                message = 'has changed tracking ref from {} to {}'.format(order.tracking_ref, vals['tracking_ref'])
                new_history = u'{}\n{} {} {}'.format(order.report_history, time_now, scan_user.name, message)
                #vals = dict(vals, report_history=new_history)
                #vals['report_history'] = '{}\n{}'.format(vals.get('report_history', ''), new_history)
                vals['report_history'] = vals.get('report_history', '') + '\n' + new_history
                batch_vals = dict(batch_vals, report_history=new_history)

        res = super(stock_picking, self).write(cr, uid, ids, vals, context=context)

        if ids and batch_vals:
            base_ids = ids if isinstance(ids, list) else [ids]
            batch_pick_ids = list(set(self.get_batch_pickings(cr, uid, base_ids, only_pickings=True, in_states=['picking', 'outcode_except', 'shipped'])) - set(base_ids))
            if batch_pick_ids:
                super(stock_picking, self).write(cr, uid, batch_pick_ids, batch_vals, context=context)

        return res

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if args is None:
            args = []
        if context is None:
            context = {}

        def is_default_list(data):
            if (len(data) != 2) or (type(data[0]) == str) or (data[1][0] != 'state'):
                return True
            if type(data[0][2]) == str and 'done' == data[1][2]:
                return False
            return True

        if args and is_default_list(args):
            if context.get('from_view', False):
                if args:
                    # Check domain if it default query (on page load)
                    # The main problem: delivery and outgoing is the same model, but with different domain
                    # If somebody knows how to do better, please touch Alex.F (task 19139)
                    args_count = {}
                    process_by_item_flag = True
                    for element in args:
                        if element[0] == 'process_by_item':
                            process_by_item_flag = element[2]
                        if element[0] not in args_count:
                            args_count[element[0]] = 0
                        args_count[element[0]] = args_count[element[0]] + 1
                    main_check = args_count.get('type', 0) is 1 and args_count.get('state', 0) is 1
                    # If from Outgoing or from Delivery
                    if (main_check and len(args) is 2) or (len(args) is 3 and main_check and process_by_item_flag is False):
                       return []
                else:
                    return []

            args = self.pool.get('res.users').search_args_user_warehouses(cr, uid, args, field='warehouses', context=context)
            args = self.prepare_search_by_sku_domain(cr, uid, args, context=context)
            return super(stock_picking, self).search(cr, uid, args, offset, limit, order, context, count)
        else:
            return []

    def prepare_search_by_sku_domain(self, cr, uid, args=None, context=None):
        new_args = []
        if args is None:
            args = []

        for arg in args:
            if arg[0] == 'product_sku_filter':
                value = arg[2] and arg[2].strip() or ''
                value = value.lstrip('0')
                if not value:
                    continue

                value += '%'
                cr.execute( """ SELECT DISTINCT sm.picking_id
                                FROM sale_order_line ol
                                    left join stock_move sm on sm.sale_line_id = ol.id
                                WHERE 1=1
                                    and ltrim(ol."merchantSKU", '0') ilike %s
                """, (value, ))
                res = cr.fetchall()
                picking_ids = res and res[0] or []
                new_args.append(['id', 'in', picking_ids])
            else:
                new_args.append(arg)

        return new_args

    def generate_invoiceno(self, cr, uid, partner, context=None):
        invoice_no = False
        if partner:
            partner_ref = partner.ref
            seq_partner_ref = partner_ref

            if partner.is_sterling:
                seq_partner_ref = 'STK'

            if partner_ref:
                invoiceno_seq_name = 'stock.picking.invoiceno.' + seq_partner_ref.lower()

                if not self.pool.get('ir.sequence').search(cr, uid, [('name', '=', invoiceno_seq_name)]):
                    pg_get_max_inv_id_sql = """SELECT cast(max(regexp_replace(invoice_no, '.*?(\d+).*','\\1')) as integer)
                        from stock_picking
                        where invoice_no like '%s%%';
                    """ % (partner_ref,)
                    cr.execute(pg_get_max_inv_id_sql)
                    pg_inv_res = cr.fetchone()

                    max_inv_no = pg_inv_res and pg_inv_res[0] or 1

                    seq = {
                        'name': invoiceno_seq_name,
                        'implementation': 'standard',
                        'prefix': "",
                        'padding': 8 if partner_ref == 'HBC' else 9 - len(seq_partner_ref),
                        'number_increment': 1,
                        'number_next': max_inv_no + 1,
                        }

                    self.pool.get('ir.sequence').create(cr, uid, seq)

                seq_ids = self.pool.get('ir.sequence').search(cr, uid, [('name', '=', invoiceno_seq_name)])
                if seq_ids:
                    inv_seq = self.pool.get('ir.sequence').next_by_id(cr, uid, seq_ids[0])

                    invoice_no = inv_seq if partner_ref == 'HBC' else partner_ref + inv_seq
                    if partner_ref == 'FBW':
                        invoice_no = partner_ref + '1' + inv_seq

                    if not inv_seq or not invoice_no:
                        raise osv.except_osv(_('Warning !'), _("""Can not generate invoice No for order '%s' for partner '%s'!""" % (invoiceno_seq_name, partner_ref,)))
                else:
                    raise osv.except_osv(_('Warning !'), _("""Can not create sequence for Partner '%s'!""" % (partner_ref,)))
        else:
            raise osv.except_osv(_('Warning !'), """Partner not found!""" )

        return invoice_no

    def create(self, cr, uid, vals, context=None):
        name = vals.get('name', False)

        if not name:
            sale_id = vals.get('sale_id', False)
            if sale_id:

                sale = self.pool.get('sale.order').browse(cr, uid, sale_id)
                ref = sale.name
                num = len(sale.picking_ids)
                name = "%s-%s" % (ref, num)

                while self.search(cr, uid, [('name', '=', name)]):
                    num += 1
                    name = "%s-%s" % (ref, num)

                vals.update({'name': name, 'note': sale.note})
                expected_date = sale.latest_ship_date_order or False
                if expected_date and len(expected_date) == 10:
                    vals.update({'date_expected': datetime(
                        int(expected_date[:4]),
                        int(expected_date[5:7]),
                        int(expected_date[8:]),
                        18,
                        59,
                        # tzinfo=timezone('US/Eastern'),
                    )})
                # ship_code_ids = []
                # ship_code_obj = self.pool.get('res.shipping.code')
                # if sale.carrier:
                #     ship_code_ids = ship_code_obj.search(cr, uid, [('incoming_code', '=', sale.carrier)], context=context)
                # if not ship_code_ids:
                #     ship_code_ids = ship_code_obj.search(cr, uid, [('default', '=', True)], context=context)
                # if ship_code_ids:
                #     ship_code = ship_code_obj.browse(cr, uid, ship_code_ids[0])
                #     carrier = (ship_code.service_id and ship_code.service_id.name or '') + (ship_code.type_id and (', ' + ship_code.type_id.name) or '') or False
                #     vals.update({'carrier': carrier})
                if sale.partner_order_id:
                    vals.update({'ship_from': sale.partner_order_id.id})

        sale_id = vals.get('sale_id', False)
        if sale_id:
            sale = self.pool.get('sale.order').browse(cr, uid, sale_id)
        else:
            raise osv.except_osv(_('Warning !'), _("""Please specify Sale Order for the Delivery Order '%s'!""" % (name,)))

        invoice_no = vals.get('invoice_no', False)
        if not invoice_no:
            invoice_no = self.generate_invoiceno(cr, uid, sale.partner_id, context=context)
            vals.update({'invoice_no': invoice_no})

        if (sale.partner_id.ref == 'WMT'):
            asn_prefix = sale.asnnumber_prefix or False
            if asn_prefix and invoice_no:
                invoice_number = str(invoice_no).replace('WMT', '')
                if len(str(invoice_number)) < 9:
                    invoice_number = ('0' * (9 - len(invoice_number))) + invoice_number
                asn = asn_prefix + invoice_number

                first_digit = '0'
                upc_odd_sum = 0
                upc_even_sum = 0
                for i in range(len(asn) - 1):
                    j = len(asn) - 1 - i
                    if j % 2:
                        upc_odd_sum += int(asn[j])
                    else:
                        upc_even_sum += int(asn[j])

                last_digit_temp = (upc_odd_sum * 3 + upc_even_sum) % 10
                if last_digit_temp == 0:
                    last_digit = 0
                else:
                    last_digit = (10 - last_digit_temp)
                last_digit = int(last_digit)

                asn = first_digit + asn + str(last_digit)
                vals.update({'asn_number': asn})

        return super(stock_picking, self).create(cr, uid, vals, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        picking_obj = self.browse(cr, uid, id, context=context)
        move_obj = self.pool.get('stock.move')

        if ('name' not in default) or (picking_obj.name == '/'):
            default['origin'] = ''
            default['backorder_id'] = False
        if picking_obj.invoice_state == 'invoiced':
            default['invoice_state'] = '2binvoiced'

        if default.get('new_name', False):
            default['name'] = default['new_name']
        else:
            default['name'] = False

        default['list_ids'] = [(6, 0, [])]

        res = super(stock_picking, self).copy(cr, uid, id, default, context)
        if res:
            picking_obj = self.browse(cr, uid, res, context=context)
            for move in picking_obj.move_lines:
                move_obj.write(cr, uid, [move.id], {'tracking_id': False, 'prodlot_id': False, 'move_history_ids2': [(6, 0, [])], 'move_history_ids': [(6, 0, [])]})
        return res

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        def_id = False
        res = super(stock_picking, self).default_get(cr, uid, fields_list, context=context)


        if active_model == 'order.history' and active_ids and type(active_ids) in (tuple, list) and len(active_ids) == 1:
            oh_obj = self.pool.get(active_model).browse(cr, uid, active_ids)[0]
            def_id = oh_obj.sm_id and oh_obj.sm_id.picking_id and oh_obj.sm_id.picking_id.id or False
            if def_id:
                order_fields = self.read(cr, uid, def_id)
                res.update(order_fields)

        if context.get('def_order_id', False) and not def_id:
            def_id = context.get('def_order_id', False)
            order_fields = self.read(cr, uid, context.get('def_order_id'))
            res.update(order_fields)

        if context.get('def_oh_move_id', False) and not def_id:
            def_obj = self.pool.get('stock.move').browse(cr, uid, context.get('def_oh_move_id'))
            def_id = def_obj.picking_id.id or def_obj.set_parent_order_id.id
            order_fields = self.read(cr, uid, def_id)
            res.update(order_fields)

        return res

    def prepare_reopen(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state': 'confirmed',
            'tracking_ref': False,
            })
        stock_move_data = self.pool.get('stock.move')
        address_data = self.pool.get('res.partner.address')
        for pick in self.browse(cr, uid, ids, context=context):

            if pick.sale_id.flash and pick.sale_id.shp_date:
                raise osv.except_osv('Warning', '%s order is a part of shipped flash order. You can\'t restart it.' % (pick.name))

            stock_move_data.write(
                cr, uid, [x.id for x in pick.move_lines if x.state == 'cancel'],
                {'state': 'draft', 'old_location_id': False}
                )
            if pick.address_id and pick.address_id.cancelled:
                address_data.write(cr, uid, [pick.address_id.id], {'cancelled': False})
        return True

    # Demo prep
    def product_pick_remove(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')

        for pick in self.browse(cr, uid, ids, context=context):
            move_ids = [move.id for move in pick.move_lines]
            move_obj.product_pick_remove(cr, uid, move_ids, context=context)
        return True

    # Demo prep
    def product_pick_process(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')

        for pick in self.browse(cr, uid, ids, context=context):
            move_ids = [move.id for move in pick.move_lines]
            move_obj.product_pick_process(cr, uid, move_ids, context=context)
        return True

    # Demo prep
    def is_product_pick_block(self, cr, uid, ids, context=None):
        pick = self.browse(cr, uid, ids[0], context=context)
        for move in pick.move_lines:
            if move.product_picking_id.state in ('pick',):
                return True
        return False

    def allow_cancel(self, cr, uid, ids, context=None):
        def _get_transaction_record(cr, uid, server_id, server_obj, original_move):
            params = original_move.get_record_info()
            select_sql = """
                           SELECT TOP 1 warehouse, transactioncode --ID, QTY, TRANSACTIONCODE, FC_INVOICE, EXPORT_REF  
                           FROM dbo.TRANSACTIONS
                           WHERE (invoiceno=%(external_customer_order_id)s or invoiceno=%(po_number)s)
                           AND transactioncode in ('SLE', 'SHP')
                           AND itemid=%(itemid)s
                           --AND warehouse=%(warehouse)s
                           AND qty=%(product_qty)s
                           order by id desc;
                       """
            response = server_obj.make_query(cr, uid, server_id, select_sql,
                                             params=params, select=True, commit=False)
            return response and response[0] or None

        if context is None:
            context = {}

        stock_move_data = self.pool.get('stock.move')
        helper = self.pool.get('stock.picking.helper')

        result = True

        # If advanced cancel to warehouse, other than in transactions table, don't change transaction code
        # transaction_code = SIGN_CANCEL_VALUE
        other_warehouses = {}
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        for pick in self.browse(cr, uid, ids, context=context):
            for line in pick.move_lines:
                transaction_details = _get_transaction_record(cr, uid, server_id, server_obj, line)
                other_warehouse = transaction_details and (transaction_details[0] != line.warehouse.name) or False
                other_warehouses[line.id] = other_warehouse
                if transaction_details and other_warehouse:
                    move_context = dict(context,
                                        transaction_code=transaction_details[1],
                                        sign=SIGN_CANCEL,
                                        sm_entries=[line],
                                        )
                    # transaction_code = transaction_details[1]
                    # other_warehouse = True
                    # break

                # if not other_warehouse:
                else:
                    move_context = dict(context,
                                        transaction_code=SIGN_CANCEL_VALUE,
                                        sign=SIGN_CANCEL,
                                        export_ref_ca=None,
                                        export_ref_us=None,
                                        fc_invoice=None,
                                        del_invoice=None,
                                        fc_order=None,
                                        del_order=None,
                                        bill_of_lading=None,
                                        sm_entries=[line],
                                        )

                self.action_update_transactions(cr, uid, ids, move_context)

        for pick in self.browse(cr, uid, ids, context=context):
            move_context.update(flash_revert={x.id: x.id for x in pick.move_lines},
                                # transaction_code=SIGN_CANCEL_VALUE if not other_warehouse else SIGN_ADJUSTMENT_VALUE
                                )
            logger.info("Test allow cancel for {}".format(pick.name))
            if pick.state not in ('done', ):
                self.reset_shipping_info(cr, uid, [pick.id], context=context)

            if pick.move_lines:
                # DLMR-1190
                # roll-back stock count qty's on cancel
                logger.info("RUN ROLL BACK STOCK COUNT ON CANCEL")
                for line in pick.move_lines:
                    other_warehouse = other_warehouses[line.id]
                    move_context.update(
                        transaction_codes={line_id: SIGN_CANCEL_VALUE if not other_warehouses[line_id] else SIGN_ADJUSTMENT_VALUE for line_id in other_warehouses}
                    )
                    # Step 0. Do only for smart scanning moves
                    smart_scanning = line.location_id and line.location_id.warehouse_id and line.location_id.warehouse_id.smart_scanning or False
                    if not smart_scanning:
                        logger.info("NOT SMART SCANNING WH, SKIP LINE")
                        continue
                    # Step 1. Check has active stock count
                    wh_name = line.location_id and line.location_id.warehouse_id and line.location_id.warehouse_id.name or None
                    loc_name = line.location_id and line.location_id.name or None
                    bin_name = line.bin_id and line.bin_id.name or None
                    item_id = line.product_id.default_code + "/" + stock_move_data.get_size_postfix(cr, uid, line.size_id)
                    logger.info("CANCEL FOR ITEM: %s, WH: %s, BIN: %s" % (item_id, wh_name, bin_name))
                    if not wh_name or not bin_name:
                        continue
                    active_count_sql = """
                        SELECT cn.countID 
                        FROM dbo.prg_inventory_count_header cn
                        WHERE cn.complete = 0
                            AND cn.warehouse = %(wh_name)s 
                            AND (cn.bin_from <= %(bin_name)s OR cn.bin_from='')
                            AND (cn.bin_to >= %(bin_name)s OR cn.bin_to='')
                    """
                    active_count_params = {'wh_name': wh_name, 'bin_name': bin_name}
                    count_data = helper.MSSQLInventorySelect(cr, uid, sql=active_count_sql, params=active_count_params)
                    logger.info("GOT STOCK COUNT DATA: %s" % count_data)
                    if not count_data:
                        continue
                    count_ids = [x[0] for x in count_data if x]
                    # Step 2. Search for item and update qty
                    for count_id in count_ids:
                        # 2.1. Search for item details
                        item_details_sql = """
                            SELECT TOP 1 d.id, d.frozenQTY, d.countQTY, d.countQTY2, d.base_line_id
                            FROM dbo.prg_inventory_count_details d
                            WHERE d.countID = %(count_id)s
                            AND d.delmarID = %(item_id)s
                            AND d.bin = %(bin_name)s
                            AND d.stockid = %(loc_name)s
                            AND d.warehouse = %(warehouse_name)s
                        """
                        item_details_params = {'count_id': count_id, 'item_id': item_id, 'bin_name': bin_name, 'loc_name': loc_name, 'warehouse_name': wh_name}
                        detail_data = helper.MSSQLInventorySelect(cr, uid, sql=item_details_sql, params=item_details_params)
                        logger.info("GOT DETAILS DATA: %s" % detail_data)
                        # 2.2. Update item details line
                        if detail_data:
                            detail = detail_data[0]
                            update_detail_sql = """
                                UPDATE dbo.prg_inventory_count_details
                                SET frozenQTY = frozenQTY+%(qty)s{}{}
                                WHERE id = %(line_id)s
                            """
                            update_qty = ''
                            if detail[2] and int(detail[2]) > 0:
                                update_qty = ", countQTY = countQTY+%(qty)s"
                            update_qty_2 = ''
                            if detail[3] and int(detail[3]) > 0:
                                update_qty_2 = ", countQTY2 = countQTY2+%(qty)s"
                            update_detail_sql = update_detail_sql.format(update_qty, update_qty_2)
                            update_detail_params = {'line_id': detail[0], 'qty': line.product_qty}
                            logger.info('UPDATE DETAIL SQL: %s, params: %s' % (update_detail_sql, update_detail_params))
                            update_detail_res = helper.MSSQLInventoryExecute(cr, uid, sql=update_detail_sql, params=update_detail_params)
                            # 2.3. Update summary line
                            if detail[4] and int(detail[4]) > 0:
                                update_summary_sql = update_detail_sql.format('', '')
                                update_summary_params = {'line_id': detail[4], 'qty': line.product_qty}
                                logger.info('UPDATE SUMMARY SQL: %s, params: %s' % (update_summary_sql, update_summary_params))
                                update_summary_res = helper.MSSQLInventoryExecute(cr, uid, sql=update_summary_sql, params=update_summary_params)

                # Un-check assign for stock move
                result &= stock_move_data.uncheck_assign(cr, uid, [x.id for x in pick.move_lines], context=move_context)

        # Demo prep
        context.update({'product_pick_hard_remove': True})
        self.product_pick_remove(cr, uid, ids, context=context)

        return result

    def update_list(self, cr, uid, ids, context=None):
        cr.execute("""SELECT distinct tg.name, tg.description
            FROM tagging_order as tg
            left join taggings_order ts on ts.tag_id=tg.id
            left join order_history oh on oh.id=ts.order_id
            left join stock_picking sp on sp."name"=oh.do_name
            where sp.id in %s""", (tuple(ids), ))

        value = {
            'tag_id': [(0, False, {'name': x[0],
                                   'description': x[1] or ''}) for x in cr.fetchall()],
        }

        return dict(value=value, warning={})

    def action_picking(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'picking'}, context=context)

        stock_pickings = self.read(cr, uid, ids, ['move_lines'])
        for pick in stock_pickings:
            if pick['move_lines']:
                oh_ids = self.pool.get('order.history').search(cr, uid, [('sm_id', 'in', pick['move_lines']), ('first_print_date', '=', False)])
                self.pool.get('order.history').write(cr, uid, oh_ids, {'first_print_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())})

        return True

    def test_picking(self, cr, uid, ids):
        for pick in self.browse(cr, uid, ids):
            if pick.tracking_ref:
                return True

        return False

    def action_incode_except(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        self.write(cr, uid, ids, {
            'state': 'incode_except',
            }, context=context)

        # Demo prep
        context.update({'product_pick_hard_remove': True})
        self.product_pick_remove(cr, uid, ids, context=context)

        return True

    def set_billing_method(self, cr, pick_id):
        # fill shipping account & billing method
        sql_select = """select
                rpa.account,
                rpa.billing_method
            from
                stock_picking as sp
            left join sale_order as so on
                so."name" = sp.origin
            left join res_partner_account as rpa on
                (
                    rpa.partner_id = sp.real_partner_id
                    and lower( rpa.service )= lower( sp.shp_service )
                    and ((so.ship2store is true)  or (so.ship2store is not true and rpa.s2s is not true))
                )
            where
                sp.id = %s""" % pick_id
        cr.execute(sql_select)
        fields = cr.fetchone()
        if fields[0] and fields[1]:
            cr.execute("""update stock_picking set shp_account=%s, billing_method=%s where id=%s""",
                       (fields[0], fields[1], pick_id))

        return True

    def check_incoming_code(self, cr, uid, ids, context=None):
        #def _do_apply_shipping_rule(cr, shp_obj, pick):
        #    """Check if zip code is CA and shipping rule excludes it"""
        #    cr.execute("""select * from exclude_ca_zip_codes where '{}' like name || '%'""".format(pick.address_id.zip))
        #    res = cr.fetchone()
        #    return False if shp_obj.exclude_ca_codes and res else True

        wf_service = netsvc.LocalService("workflow")
        shp_data = self.pool.get('res.shipping.code')
        move_obj = self.pool.get('stock.move')
        for pick in self.browse(cr, uid, ids):

            if pick.shp_code and pick.fixed_shp_code:
                if pick.fixed_shp_code and not pick.shp_account:
                    account = False
                    account_from_order = self._get_shp_account_from_order(cr, uid, pick.id)
                    if not account_from_order and pick.shp_service:
                        account = self.get_shp_account(cr, uid, pick.id, pick.shp_service)
                    account_number = account_from_order or account and account.account
                    if account_number:
                        self.write(cr, uid, pick.id, {
                            'shp_account': account_number,
                            'shp_account_country': account and account.country_id and account.country_id.code,
                            'billing_method': account and account.billing_method
                        })
                continue

            # 'reserved' move_lines become 'available'
            reserved_ids = move_obj.search(cr, uid, [('picking_id', '=', pick.id), ('state', '=', 'reserved')])
            if reserved_ids:
                move_obj.write(cr, uid, reserved_ids, {
                        'state': 'assigned',
                        }, context=context)

            shp_obj = shp_data.get_code(
                cr, uid,
                pick_id=pick.id,
                )

            if shp_obj:
                self.set_shipping_rule(cr, uid, pick.id, shp_obj.id)
                wf_service.trg_write(uid, 'stock.picking', pick.id, cr)

            else:
                self.reset_shipping_rule(cr, uid, pick.id)
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_incode_except', cr)

        return True

    def set_shipping_rule(self, cr, uid, pick_id, rule_id, context=None):
        shp_data = self.pool.get('res.shipping.code')
        shp_obj = shp_data.browse(cr, uid, rule_id, context={'picking_id': pick_id})

        pick = self.browse(cr, uid, pick_id)
        sale = pick.sale_id

        sale_order_fields_obj = self.pool.get('sale.order.fields')
        sale_order_fields_row = sale_order_fields_obj.search(
                cr, uid, [('order_id', '=', sale.id), ('name', '=', 'signature')]
            )
        field = self.browse(cr, uid, sale_order_fields_row)
        if shp_obj:
            shp_service = shp_obj.service_id and shp_obj.service_id.name or False
            shp_type = shp_obj.type_id and shp_obj.type_id.name or False
            carrier = shp_service
            if carrier and shp_type:
                carrier += ', ' + shp_type

            pick = self.browse(cr, uid, pick_id)
            account_from_order = self._get_shp_account_from_order(cr, uid, pick_id)

            account = self.get_shp_account(cr, uid, pick_id, shp_service)
            billing_method = account and account.billing_method
            shp_account = account_from_order or account and account.account

            if not account_from_order:
                account = False
            if ('%' == shp_account):
                shp_account = None
                billing_method = None

            signature = shp_obj.signature
            if pick.origin_ship2store and shp_obj.not_ss_signature:
                signature = False

            if (field):
                sql = """SELECT value
                              FROM sale_order_fields
                              WHERE "order_id" = '%d'
                                  AND "name" = '%s'
                              """ % (sale.id, 'signature')
                cr.execute(sql)
                sql_res = cr.fetchone()
                if (sql_res[0]):
                    signature = dict(SIGNATURE).get(sql_res[0], signature)

            shp_code = shp_obj.code
            if shp_obj.flat_rate_priority_id:
                if shp_obj.flat_rate_priority_id.shp_code:
                    shp_code = shp_obj.flat_rate_priority_id.shp_code
            shp_handling = pick.shp_handling_from_sale_order if pick.sale_id.shp_handling else shp_obj.shipping_handling

            self.write(cr, uid, pick_id, {
                'carrier': carrier,
                'shp_code': shp_code,
                'shp_signature': signature,
                'shp_account': shp_account,
                'shp_account_country': account and account.country_id and account.country_id.code,
                'billing_method': billing_method,
                'shp_service': shp_service,
                'shp_insurance': shp_obj.insurance,
                'shp_handling': shp_handling,
                })

        return True

    def reset_shipping_rule(self, cr, uid, ids, vals=None, context=None):
        default_vals = {
            'carrier': False,
            'shp_code': False,
            'shp_signature': False,
            'shp_account': False,
            'shp_account_country': False,
            'billing_method': False,
            'shp_service': False,
            'shp_insurance': False,
            'shp_handling': False,
            'shp_date': False,
            'carrier_code': False,
            }
        if vals:
            default_vals.update(vals)
        self.write(cr, uid, ids, default_vals)

        return True

    def test_incoming_code(self, cr, uid, ids, context=None):
        ok = True
        move_obj = self.pool.get('stock.move')
        # self.check_incoming_code(cr, uid, ids, context=context)
        for pick in self.browse(cr, uid, ids):
            ok_pick = bool(pick.shp_code and pick.shp_service) or False
            status = False if ok_pick else 'Bad incoming code'
            move_obj.write(cr, uid, [x.id for x in pick.move_lines], {'status': status})
            ok &= ok_pick

        return ok

    def action_outcode_except(self, cr, uid, ids, context=None):

        if not ids:
            return False


        self.write(cr, uid, ids, {
            'state': 'outcode_except',
            }, context=context)

        cr.execute("""
            select sp.id, so.carrier as origin_carrier, sp.carrier_code
            from stock_picking sp
                left join sale_order so on so.id = sp.sale_id
            where sp.id in %(ids)s
                and so.type_api in ('walmart', 'walmartcanada')
                and coalesce(sp.carrier_code, '') = ''
                and coalesce(so.carrier) != ''
        """, {
            'ids': tuple(ids)
        })

        res = cr.dictfetchall()

        for pick in res:
            self.write(cr, uid, pick['id'], {'carrier_code': pick['origin_carrier']})

        return True

    def test_outcode_except(self, cr, uid, ids, context=None):
        ok = True
        move_obj = self.pool.get('stock.move')
        for pick in self.browse(cr, uid, ids):
            ok_pick = bool(pick.carrier_code)
            status = False if ok_pick else 'Bad outgoing code'
            if pick.shp_bulk_id or False:
                if not pick.shp_bulk_id.tracking_ref:
                    ok_pick = False
                    status = 'Need to update batch tracking#'
            move_obj.write(cr, uid, [x.id for x in pick.move_lines], {'status': status})
            ok &= ok_pick

        return ok

    def test_flash_not_shipped(self, cr, uid, ids, context=None):
        ok = True
        for pick in self.browse(cr, uid, ids):
            sale = pick.sale_id
            if sale.flash and sale.shp_date:
                raise osv.except_osv(
                    'Warning',
                    '%s Order already shipped. You can\'t process this delivery.' % (sale.name)
                )

        return ok

    def test_address(self, cr, uid, ids, context=None):
        valid = True
        address_obj = self.pool.get('res.partner.address')
        move_obj = self.pool.get('stock.move')
        for pick in self.browse(cr, uid, ids):
            valid_order = False
            check_address = True
            if pick.sale_id and pick.sale_id.bulk_transfer and pick.sale_id.partner_id.ref != 'JMS' or False:
                # Force any kind of checking addresses for bulk orders
                valid_order = True
                if pick.address_id:
                    address_obj.write(cr, uid, pick.address_id.id, {'is_approved': True})

            if (pick.sale_id.partner_id.ref == 'JMS' and pick.sale_id.bulk_transfer) or (pick.sale_id.partner_id.ref == 'GH1' and pick.address_id.country_id.code != 'US') or (pick.sale_id.partner_id.ref == 'BAL' and pick.address_id.state_id.code in ('AK','HI')):
                if uid == 1:
                    valid_order = False
                else:
                    valid_order = True
                check_address = False

            if not valid_order and pick.address_id and check_address:
                address = pick.address_id
                valid_order = address_obj.check_address(cr, uid, address.id, {'partner_name': pick.sale_id.partner_id.name})

            vals = {
                'exception_date': False,
                'status': False,
                }
            if not valid_order:
                vals.update({
                    'status': 'Address exception',
                    'exception_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                    })

            line_ids = self.read(cr, uid, pick.id, ['move_lines'])['move_lines']
            move_obj.write(cr, uid, line_ids, vals)

            valid &= valid_order

            if not valid:
                break

        self.write_unique_fields_for_orders(cr, uid, ids)

        return valid

    def action_address_except(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'address_except'})

        return True

    def action_shipped(self, cr, uid, ids, context=None):
        timestamp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        for order in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, order.id, {'shp_date': timestamp})
        self.write(cr, uid, ids, {'state': 'shipped'}, context=context)

        return True

    def action_try_validate_tracking(self, cr, uid, ids, context=None):
        if not ids:
            return False
        if isinstance(ids, (int, long)):
            ids = [ids]
        si_obj = self.pool.get('sale.integration')
        for order in self.browse(cr, uid, ids, context=context):
            tracking_is_valid = si_obj.try_validate_tracking(cr, uid, order.id, context=context)
            if(tracking_is_valid):
                self.write(cr, uid, [order.id], {'state': 'shipped'}, context=context)
        return True

    def test_multiline(self, cr, uid, ids):
        for pick in self.browse(cr, uid, ids):
            if len(pick.move_lines) > 1:
                return True

        return False

    def test_assigned(self, cr, uid, ids, context=None):
        # print "Test assigned"

        if context is None:
            context = {}

        move_obj = self.pool.get("stock.move")

        allow_multi_warehouse = context.get('allow_multi_warehouse', False)

        ok = True
        for pick in self.browse(cr, uid, ids):

            flash = pick and pick.sale_id and pick.sale_id.flash or False
            default_params = [('picking_id', '=', pick.id)]
            mt = pick.move_type
            if mt == 'one':
                if move_obj.search(cr, uid, default_params + [('state', 'in', ('confirmed', 'cancel', 'draft'))], limit=1):
                    return False
            elif mt == 'direct':
                ok = not bool(move_obj.search(cr, uid, default_params + ['|', ('state', '!=', 'assigned'), ('product_qty', '=', False)], limit=1))

            else:
                ok = not bool(move_obj.search(cr, uid, default_params + [('state', 'in', ('done', 'assigned', 'reserved'))], limit=1))

            if ok and not (flash or allow_multi_warehouse):
                locations = set()
                order_wh_ids = self._get_warehouses(cr, uid, [pick.id], 'warehouses', None)[pick.id]
                for wh_id in order_wh_ids:
                    locations |= set([x.location_id.name for x in order_wh_ids[wh_id] if x.location_id])

                print "wh locs: %s" % (order_wh_ids)
                single_wh = (len(order_wh_ids) == 1 or set(['FEOSPP']) == locations) and 0 not in order_wh_ids.keys()
                ok &= single_wh

                if not single_wh:
                    return False

        # print "assigned: %s" % (str(ok))
        return ok

    def action_cancel(self, cr, uid, ids, context=None):
        res = super(stock_picking, self).action_cancel(cr, uid, ids, context)

        for order in self.browse(cr, uid, ids, context=context):
            self.reset_shipping_rule(cr, uid, order.id, vals={
                'process_by_item': False,
                'process_by_machine': False,
                })

            self.action_separate_stock_pickings(cr, uid, ids, context={'active_ids': [order.id]})

            if order.address_id.id or False:
                self.pool.get('res.partner.address').write(cr, uid, order.address_id.id, {'cancelled': True})

        return res

    def action_set_only_done(self, cr, uid, ids, context=None):
        if ids:
            wf_service = netsvc.LocalService("workflow")
            for pick_id in ids:
                wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_done', cr)

        return True

    def action_global_done(self, cr, uid, ids, context=None):

        pick_ids = context.get('active_ids', [])

        wf_service = netsvc.LocalService("workflow")
        for pick_id in pick_ids:
            wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_done', cr)

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'openerp_shipping', 'action_shipping_picking_tree')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win['context'] = "{'search_default_done': 1}"

        return act_win

    def action_check_tracking_numbers(self, cr, uid, ids, context=None):

        logger.debug("cr: %s" % (cr))
        if context is None:
            context = {}

        pick_ids = context.get('active_ids', [])
        self.check_tracking_numbers(cr, uid, pick_ids)

        return {'type': 'ir.actions.act_window_close'}

    def action_multi_check_availability(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        pick_ids = context.get('active_ids', [])
        if uid != 1:
            cr.execute('SELECT order_id FROM _rabbitmq_availability_stack')
            rabbit_ids = list(set(pick_ids) - set(r[0] for r in cr.fetchall()))
            cr.execute('SELECT order_id FROM _rabbitmq_availability_queue')
            rabbit_ids = list(set(rabbit_ids) - set(r[0] for r in cr.fetchall()))
            if not rabbit_ids:
                message = _('All %s orders are being processed by automate' % (len(pick_ids)))
                raise osv.except_osv(_('Info'), _(message))
            else:
                import openerp.pooler as pooler
        
                if len(rabbit_ids) < 25:
                    message = _(
                        '%s are being processed by automate.Y orders will be processed now. ok?' % (len(rabbit_ids)))
                    self.multi_check_availability(cr, uid, rabbit_ids)
                else:
                    message = _(
                        '%s are being processed by automate.Y orders will be added to automate\'s procession ,because number of orders Y>=25. ' % (len(rabbit_ids)))

                    pool = pooler.get_pool(cr.dbname)
                    cron_obj = pool.get('ir.cron')

                    vals = {
                        'user_id': uid,
                        'model': 'stock.picking',
                        'interval_type': 'minutes',
                        'function': 'sent_to_rabbit',
                        'numbercall': -1,
                        'priority': 4,
                        'type': 'other',
                        'doall': False,
                        'name': 'multi_check_availability'#.format(datetime.now().strftime('%Y%m%d_%H%M%S'))
                    }

                    cron_id = cron_obj.create(cr, uid, vals)
                    params = {'cron_id': cron_id}

                    cron_obj.write(cr, uid, [cron_id], {'args': "(%s,%s)" % (rabbit_ids, params)})

                    raise osv.except_osv(_('Info'), _(message))
        else:
            self.multi_check_availability(cr, uid, pick_ids)

        return {'type': 'ir.actions.act_window_close'}

    def _check_availability(self, cr, uid, limit=10, tag=False, context=None):

        pick_ids = []
        conf_obj = self.pool.get('ir.config_parameter')
        last_id = conf_obj.get_param(cr, uid, 'check.inventory.id') or 0

        if tag:
            print "Cron Check Availability by tag %s" % (tag)
            sql = """
                SELECT distinct s_m.picking_id
                FROM taggings_order t_h
                    LEFT JOIN tagging_order t_o on (t_h.tag_id = t_o.id)
                    LEFT JOIN order_history o_h on (t_h.order_id = o_h.id)
                    LEFT JOIN stock_move s_m on (o_h.sm_id = s_m.id)

                WHERE t_o.name = '%s'
                    AND o_h.sm_id IS NOT NULL
                    AND o_h.sm_id IS NOT NULL
                    AND s_m.picking_id IS NOT NULL
                    AND s_m.picking_id > %s

                ORDER BY s_m.picking_id
                LIMIT %s ;
            """ % (tag, last_id, limit)
            cr.execute(sql)
            res = cr.fetchall()
            # print res
            if res:
                pick_ids = [x[0] for x in res]

        else:
            print "Cron Check Availability"
            sql = """
                SELECT id FROM stock_picking
                WHERE state in ('confirmed', 'waiting_split')
                    AND id > %s
                ORDER BY id
                LIMIT %s; """ % (last_id, limit)
            cr.execute(sql)
            res = cr.fetchall()
            # print res
            if res:
                pick_ids = [x[0] for x in res]

        print "Check ids: %s" % (pick_ids)
        if pick_ids:
            conf_obj.set_param(cr, uid, 'check.inventory.id', pick_ids[-1])
            self.multi_check_availability(cr, uid, pick_ids)

        else:
            conf_obj.set_param(cr, uid, 'check.inventory.id', '0')

        return True

    def multi_check_availability(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")

        for pick in self.browse(cr, uid, ids):
            if pick.state == 'draft':
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_confirm', cr)
            else:
                if pick.state == 'confirmed':
                    self.action_assign(cr, uid, [pick.id])
                if pick.state == 'waiting_split':
                    wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_recheck', cr)

        return True

    def sent_to_rabbit(self, cr, uid, ids, params,  context=None):

        if not ids:
            return False

        cron_id = params['cron_id']
        cr.execute('SELECT order_id FROM _rabbitmq_availability_stack')
        ids = list(set(ids) - set(r[0] for r in cr.fetchall()))

        select_sql = """
           select id as order_id, name as order_name from stock_picking where id IN %(ids)s;
        """
        cr.execute(select_sql, {'ids': tuple(ids)})
        rows = cr.fetchall()
        values = ["(%s, '%s')" % (x[0], str(x[1])) for x in rows]
        insert_sql = """
            INSERT INTO _rabbitmq_availability_stack(
                order_id,
                order_name
            ) VALUES {values};
        """
        insert_sql =  insert_sql.format(values=",".join(values))
        cr.execute(insert_sql)
        self.pool.get('ir.cron').unlink(cr, uid, [cron_id])
        return True

    def force_allocate(self, cr, uid, picking_id, context=None):

        if not picking_id:
            return False

        pick = self.browse(cr, uid, picking_id)
        sale = pick.sale_id

        sale_obj = self.pool.get('sale.order')
        trs_code = sale_obj.get_sale_transaction_code(cr, uid, sale.id, sign=-1)

        force_id = sale.flash_force_id
        if not force_id:
            return False

        force_id = int(force_id)
        customer_ref = sale.partner_id.ref
        invoice_no = sale.po_number
        user_name = "openerp_" + self.pool.get("res.users").browse(cr, uid, uid).name

        import hashlib
        logger.info("Force allocate %s" % (picking_id))
        cr.execute("""select force_allocate_flash({order_id})""".format(order_id=picking_id))

        logger.info("Remote allocate %s" % (picking_id))

        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv('Warning', 'Remote server is not available.')

        i = 0
        part_count = 1000
        queries = []
        params = []
        values = []

        flash_data_sql = """
            INSERT INTO _prg_flash_data(
                id,
                line_id,
                ext_id
            ) VALUES {values};
        """
        cr.execute("""
            SELECT
                ol.external_customer_line_id as id,
                ol.id as line_id
            FROM stock_picking sp
                LEFT JOIN stock_move sm on sp.id = sm.picking_id
                LEFT JOIN sale_order_line ol on ol.id = sm.sale_line_id
            WHERE sp.id = %(picking_id)s
        """, {
            "picking_id": picking_id
        })

        data = cr.dictfetchall() or []
        value_tmpl = """(?, ?, {force_id})""".format(force_id=force_id)

        while data:
            if i == part_count:
                queries.append((
                    flash_data_sql.format(values=",".join(values)),
                    params
                ))
                i = 0
                values = []
                params = []

            i += 1

            x = data.pop()

            values.append(value_tmpl)

            params += [
                x['id'],
                hashlib.md5(str(x['line_id'])).hexdigest()
            ]

        if params:
            queries.append((
                flash_data_sql.format(values=",".join(values)),
                params
            ))

        if queries:
            connection = server_obj.connect(cr, uid, server_id)
            cursor = connection.cursor()

            clean_sql = """
                DELETE FROM _prg_flash_data
                WHERE ext_id = {force_id}
            """.format(force_id=force_id)
            server_obj.make_query(
                cr, uid, server_id,
                clean_sql,
                select=False, commit=False,
                connection=connection, cursor=cursor
            )

            for i, query in enumerate(queries):
                logger.info("Send #%s flash_data query" % (i))
                update_res = server_obj.make_query(
                    cr, uid, server_id,
                    query[0], params=query[1],
                    select=False, commit=False,
                    connection=connection, cursor=cursor
                )
                if update_res is None:
                    raise osv.except_osv(_('Warning!'), 'Error in remote allocate sale order')

            allocate_sql = """
                INSERT INTO dbo.TRANSACTIONS(
                    sign,
                    transactiontype,
                    transactioncode,
                    sos_tag,
                    timestamp,
                    transactdate,
                    _creation_date,
                    _modif_date,
                    status,
                    _user_creator,
                    _user_modificator,
                    customerid,
                    invoiceno,
                    --
                    itemdescription,
                    productcode,
                    id_delmar,
                    itemid,
                    size,
                    qty,
                    adjusted_qty,
                    warehouse,
                    stockid,
                    bin,
                    invredid,
                    reserved,
                    trans_tag
                )
                SELECT
                    -1 as sign,
                    'TRS' as transactiontype,
                    '{trs_code}' as transactioncode,
                    'openerp_mssql' as sos_tag,
                    getdate() as timestamp,
                    getdate() as transactdate,
                    getdate() as _creation_date,
                    getdate() as _modif_date,
                    'Pending' as status,
                    ?,
                    ?,
                    ?,
                    ?,
                    --
                    fi.itemdescription as itemdescription,
                    fi.id_delmar as productcode,
                    fi.id_delmar as id_delmar,
                    concat(fi.id_delmar, '/', fi.size) as itemid,
                    fi.size as size,
                    fi.qty as qty,
                    -1 * fi.qty as adjusted_qty,
                    fi.warehouse as warehouse,
                    fi.stockid as stockid,
                    fi.bin as bin,
                    fi.invredid as invredid,
                    fi.reserved as reserved,
                    fd.line_id as trans_tag
                FROM _prg_flash_inv fi
                    LEFT JOIN _prg_flash_data fd
                        on fi.id = fd.id and fi.ext_id = fd.ext_id
                WHERE fi.ext_id = ?
                    and fd.ext_id is not null
            """.format(trs_code=trs_code)
            allocate_params = [
                user_name,
                user_name,
                customer_ref,
                invoice_no,
                force_id
            ]
            logger.info("Send allocate query")
            update_res = server_obj.make_query(
                    cr, uid, server_id,
                    allocate_sql, params=allocate_params,
                    select=False, commit=True,
                    connection=connection, cursor=cursor
                )
            if update_res is None:
                raise osv.except_osv(_('Warning!'), 'Error in remote allocate sale order')

        return True

    def export_activity(self, cr, uid, ids, context=None):
        return {}

    def create_invoice_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        report_obj = self.pool.get('stock.invoice.report')
        active_ids = context.get('active_ids', [])
        move_ids = context.get('move_ids', [])

        new_report_id = report_obj.create(cr, uid, {
            'stock_picking_ids': active_ids,
            'stock_move_ids': move_ids,
            })

        is_internal = context.get('internal', False)

        if active_ids or move_ids:
            country = context.get('country', False)
            direction = country and country.strip().lower() or ''
            cr.execute("""SELECT
                    rc.code as country_code
                    , rc.name as country_name
                    , (CASE
                        WHEN '{direction}' = 'ca' THEN pt.prod_tarif_classification_ca
                        ELSE pt.prod_tarif_classification
                    END) as prod_tarif
                    , ARRAY_TO_STRING(ARRAY_ACCUM(CONCAT(m.product_id, ':', m.qty, ':', pt.standard_price, ':', m.price_unit, ':', m.stock_picking)),',') as items
                FROM (
                        SELECT
                            sm.product_id
                            , sum(sm.product_qty) as qty
                            , sol.price_unit
                            , sp.id as stock_picking
                        FROM stock_move sm
                        LEFT JOIN sale_order_line as sol ON sol.id = sm.sale_line_id
                        LEFT JOIN stock_picking AS sp ON sm.picking_id = sp.id
                        WHERE 1=1
                            AND (sm.picking_id IN %(picking_ids)s or sm.id in %(move_ids)s)
                        GROUP by sm.product_id, sol.price_unit, sp.id
                    ) as m
                    LEFT JOIN product_product pp ON (m.product_id = pp.id)
                    LEFT JOIN product_template pt on pp.product_tmpl_id = pt.id
                    LEFT JOIN res_country rc on /*pt.made_in=rc.id or*/ lower(rc.name)=lower(pt.prod_manufacture)
                GROUP BY
                    rc.id,
                    pt.prod_tarif_classification,
                    pt.prod_tarif_classification_ca
                ORDER BY
                    pt.prod_tarif_classification asc
                """.format(direction=direction), {
                    'picking_ids': tuple(active_ids + [0]),
                    'move_ids': tuple(move_ids + [0]),
                })
            res = cr.fetchall()
            if res:
                report_group_obj = self.pool.get('stock.invoice.report.group')
                report_line_obj = self.pool.get('stock.invoice.report.line')
                discount_value = self.pool.get('ir.config_parameter').get_param(cr, uid, 'internal_ship_discount_persent_value') or False
                if discount_value:
                    discount_value = _try_parse(discount_value, 'float')
                default_param_value = -0.15
                if is_internal:
                    param_name = 'internal.shipment.tax.to.' + context.get('country')
                    param_value = self.pool.get('ir.config_parameter').get_param(cr, uid, param_name) or 0.0
                    default_param_value = float(param_value)

                for country_code, country_name, prod_tarif, items in res:
                    if not prod_tarif:
                        prod_tarif = 'No Tariff'

                    group_id = report_group_obj.create(cr, uid, {
                        'country': country_name,
                        'tariff': prod_tarif,
                        'report_id': new_report_id
                    })

                    param_value = default_param_value
                    for item in items.split(','):
                        product_id, qty, cost, price_unit, stock_picking_id = item.split(':')
                        if discount_value and price_unit and context.get('country') == 'us':
                            cost = float(price_unit) - (float(price_unit) * (discount_value / 100))
                        else:
                            cost = float(cost or 0.0) * (1.0 + param_value)
                        report_line_obj.create(cr, uid, {
                            'product_id': product_id,
                            'product_qty': qty,
                            'cost': cost,
                            'group_id': group_id,
                            'picking_id': stock_picking_id,
                            })

        return new_report_id

    def action_print_invoice_info(self, cr, uid, ids, context=None):
        new_report_id = self.create_invoice_report(cr, uid, ids, context=context)
        datas = {
            'id': new_report_id,
            'ids': [new_report_id],
            'model': 'stock.invoice.report',
            'report_type': 'pdf',
            }
        act_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'stock.picking.invoice.info',
            'datas': datas,
            'context': {'active_ids': [new_report_id], 'active_id': new_report_id}
        }
        return act_print

    def get_picking_without_tracking_no(self, cr, uid, context=None):
        picking_ids = self.search(cr, uid, [('state', 'in', ('done', 'picking')), ('tracking_ref', '=', False)], context=context)
        return picking_ids

    def get_picking_with_machine_tracking(self, cr, uid, context=None):
        picking_ids = self.search(cr, uid, [
            ('state', 'in', ('done', 'picking')),
            ('tracking_ref', '=', False),
            ('machine_tracking_ref', '!=', False),
            ('machine_status', '=', 'finish'),
            ], context=context)
        return picking_ids

    def check_picking_orders(self, cr, uid, picking_ids=None, context=None):
        if picking_ids is None:
            picking_ids = self.get_picking_without_tracking_no(cr, uid, context=context)

        self.do_remote_order_process(cr, uid, picking_ids, context=context)

        return True

    def set_machine_tracking_number(self, cr, uid, id, tracking_number, context=None):

        order = self.browse(cr, uid, id)
        if order.machine_tracking_ref == tracking_number:
            return True

        if context is None:
            context = {}

        zpl = context.get('zpl', False)
        shp_carrier = context.get('carrier', False)
        shp_type = context.get('shp_type', False)

        sale_obj = order.sale_id

        note = order.note or ''
        note += '\n%s Get machine tracking number %s with ship code %s' % (
            time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), tracking_number, shp_type)

        history = order.report_history or ''
        user = self.pool.get('res.users').browse(cr, uid, uid)
        history += '\n%s %s: Get machine tracking number %s with \nShipping code: %s\nCarrier code: %s' % (
            time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name or '', tracking_number, shp_type, shp_carrier)

        vals = {
            'machine_tracking_ref': tracking_number
        }

        if zpl is not False and zpl != '' and zpl is not None:
            comment_ids = self.pool.get('sale.order.sterling.comments').search(cr, uid, [('order_id', '=', sale_obj.id),
                                                                                         ('type', '=', 'ZPL')])
            # zpl = base64.b64encode(zpl)
            if (len(comment_ids) > 0):
                self.pool.get('sale.order.sterling.comments').write(cr, uid, comment_ids, {
                    'order_id': sale_obj.id,
                    'type': 'ZPL',
                    'bin_data': zpl
                })
            else:
                self.pool.get('sale.order.sterling.comments').create(cr, uid, {
                    'order_id': sale_obj.id,
                    'type': 'ZPL',
                    'bin_data': zpl
                })

        vals.update({
            'note': note,
            'report_history': history,
            })

        res = self.write(cr, uid, id, vals, context=context)

        return res

    def set_tracking_number(self, cr, uid, picking_id, tracking_number, context=None):
        if context is None:
            context = {}

        shp_carrier = context.get('carrier', False)
        shp_type = context.get('shp_type', False)
        shp_date = context.get('shp_date', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()))
        zpl = context.get('zpl', False)
        shp_cost = context.get('shp_cost', 0) or 0
        warehouse_fed_shp = context.get('warehouse_fed_shp', '')
        scanner = context.get('scanner', '')
        warehouse_fdx_hed = context.get('warehouse_fdx_hed', '')

        if shp_cost:
            try:
                shp_cost = float(shp_cost)
            except:
                shp_cost = 0

        shipping_data = self.pool.get('res.shipping.code')
        order = self.browse(cr, uid, picking_id)
        wh_id = order.warehouses and order.warehouses.id or False
        sale_obj = order.sale_id

        note = order.note or ''
        note += '\n%s Get tracking number %s with ship code %s' % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), tracking_number, shp_type)
        history = order.report_history or ''
        user = self.pool.get('res.users').browse(cr, uid, uid)
        history += '\n%s %s: Get tracking number %s with \nShipping code: %s\nCarrier code: %s' % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name or '', tracking_number, shp_type, shp_carrier)
        weight = self.get_shp_weight(cr, uid, order)

        vals = {
            'tracking_ref': tracking_number,
            'shp_date': shp_date,
            'carrier_code': False,
            'shp_cost': shp_cost,
            'shp_weight': weight,
            'warehouse_fed_shp': warehouse_fed_shp,
            'scanner': scanner,
            'warehouse_fdx_hed': warehouse_fdx_hed
            }
        ship = False
        if sale_obj.type_api in ('walmart', 'walmartcanada') and sale_obj.carrier:
            vals.update({
                'carrier_code': sale_obj.carrier,
                })

        if sale_obj.type_api_id and wh_id:
            shp_codes = []
            if shp_type:
                shp_codes.append(shp_type)

            if shp_carrier:
                shp_codes.append(shp_carrier)

            if shp_codes:
                ship = shipping_data.get_code(cr, uid, order.id, target='outgoing', codes=shp_codes)

            if ship:
                carrier_code = ship.outgoing_code or False
                if not vals.get('carrier_code', False):
                    vals.update({
                        'carrier_code': carrier_code,
                    })
                vals.update({
                    'shp_service': ship.service_id and ship.service_id.name or False,
                })
                ###########
                ## Avi: I would assume the rule set on the shipping code selected originally should stand
                ###########
                # if ship.shipping_handling:
                #     vals.update({
                #         'shp_handling': ship.shipping_handling,
                #     })
            else:
                note += '\n%s Could not find a match shipping type and outgoing code' % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()))

        if zpl is not False and zpl != '' and zpl is not None:
            comment_ids = self.pool.get('sale.order.sterling.comments').search(cr, uid, [('order_id', '=', sale_obj.id), ('type', '=', 'ZPL')])
            # zpl = base64.b64encode(zpl)
            if(len(comment_ids) > 0):
                self.pool.get('sale.order.sterling.comments').write(cr, uid, comment_ids, {
                    'order_id': sale_obj.id,
                    'type': 'ZPL',
                    'bin_data': zpl
                })
            else:
                self.pool.get('sale.order.sterling.comments').create(cr, uid, {
                    'order_id': sale_obj.id,
                    'type': 'ZPL',
                    'bin_data': zpl
                })

        vals.update({
            'note': note,
            'report_history': history,
            })
        res = self.write(cr, uid, picking_id, vals, context=context)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_write(uid, 'stock.picking', picking_id, cr)
        return res

    def check_tracking_numbers(self, cr, uid, picking_ids=None, context=None):
        if picking_ids is None:
            picking_ids = self.get_picking_without_tracking_no(cr, uid, context=context)

        # machine_picking_ids = self.get_picking_with_machine_tracking(cr, uid, context=context)
        # picking_ids += machine_picking_ids

        # The machine
        machine_obj = self.pool.get('machine.settings')
        machine_ids = machine_obj.search(cr, uid, [('wh_ids', '!=', False), ('active', '=', True)])
        machine_server_ids = []
        if len(machine_ids) > 0:
            for machine in machine_obj.browse(cr, uid, machine_ids):
                if machine.shipping_server_id.id not in machine_server_ids:
                    machine_server_ids.append(machine.shipping_server_id.id)

        check_picking = []
        orders_by_server = self.get_shipping_server_id(cr, uid, picking_ids)
        for picking in self.read(cr, uid, picking_ids, ['name', 'process_by_machine']):
            server_id = orders_by_server.get(picking['id'], False)
            if server_id:
                check_picking.append({
                    'server_id': server_id,
                    'picking_id': picking['id'],
                    'picking_ref': picking['name'].upper(),
                    })
            if picking['process_by_machine'] is True and server_id not in machine_server_ids:
                for machine_server_id in machine_server_ids:
                    check_picking.append({
                        'server_id': machine_server_id,
                        'picking_id': picking['id'],
                        'picking_ref': picking['name'].upper(),
                    })


        self.pool.get('res.log').create(cr, uid, {
            'name': 'Updating shipping information ',
            'res_model': 'stock.picking',
            'res_id': 0,
            'debug_information': ";\n ".join(["%s: %s" % (x['picking_ref'], x['picking_id']) for x in check_picking]),
            })

        fetchdb_server_data = self.pool.get('fetchdb.server')
        server_ids = fetchdb_server_data.get_warehouse_servers(cr, uid)

        limit = 30
        try:
            system_limit = self.pool.get('ir.config_parameter').get_param(cr, uid, 'check.tracking.numbers.limit')
            if system_limit:
                limit = int(system_limit)
        except:
            pass

        for server in fetchdb_server_data.browse(cr, uid, server_ids, context=context):
            server_picking = [x['picking_ref'] for x in check_picking]  # if x['server_id'] == server.id]
            #LTRIM(RTRIM(SHP_Cost))
            if len(server_picking) > 0:
                sql = """IF OBJECT_ID('dbo.FED_SHP') IS NOT NULL
                    BEGIN
                        SELECT TOP %s
                            FS.BJ_NO,
                            LTRIM(RTRIM(FS.TRACK_NO)),
                            DATEADD(MINUTE,DATEDIFF(MINUTE,CURRENT_TIMESTAMP,GETUTCDATE()),FS.SHP_DATE),
                            LTRIM(RTRIM(FS.Carrier)),
                            LTRIM(RTRIM(FS.SHP_TYPE)),
                            LTRIM(RTRIM(FS.SHP_Cost)),
                            LTRIM(RTRIM(FS.ZPL_LABEL)),
                            LTRIM( RTRIM (FS.warehouse)),
                            LTRIM( RTRIM (FS.scanner)),
                            LTRIM( RTRIM (FH.WhLocation))
                        FROM dbo.FED_SHP as FS
                        LEFT JOIN FDX_HED FH on FH.BJ_NO = FS.BJ_NO
                        WHERE FS.BJ_NO in ('%s')
                            AND FS.TRACK_NO IS NOT NULL
                            AND LTRIM(RTRIM(FS.TRACK_NO)) != ''
                            AND FS.TRACK_NO NOT LIKE '%%ERROR%%'
                            AND (FS.CANCEL IS NULL OR FS.CANCEL != 1)
                            AND (FS.void IS NULL OR FS.void NOT IN ('Y', 'y', '1'))
                    END
                    ELSE
                    BEGIN
                        SELECT TOP 0 NULL
                    END
                """ % (limit, "', '".join(server_picking),)
                result = fetchdb_server_data.make_query(cr, uid, server.id, sql) or []

                if result:
                    updated_trac_numbers = []

                    for picking in result:

                        pick_id = False

                        for check_pick in check_picking:
                            if check_pick['picking_ref'] == picking[0]:
                                pick_id = check_pick['picking_id']
                                break

                        if pick_id:

                            updated_trac_numbers.append("id = %s ref=%s track=%s" % (pick_id, picking[0], picking[1]))
                            pick_context = {
                                'shp_date': picking[2] if picking[2] and str(picking[2]) > '2000-01-01' else time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                                'carrier': picking[3],
                                'shp_type': picking[4],
                                'shp_cost': picking[5],
                                'zpl': picking[6] and base64.b64encode(picking[6]) or None,
                                'warehouse_fed_shp': picking[7] or None,
                                'scanner': picking[8] or None,
                                'warehouse_fdx_hed': picking[9] or None,
                            }
                            pick_order = self.read(cr, uid, pick_id, ['process_by_machine', 'machine_status'])
                            if pick_order['process_by_machine'] is True and bool(pick_order['machine_status']) and bool(pick_context['zpl']) and pick_order['machine_status'] != 'finish':
                                self.set_machine_tracking_number(cr, uid, pick_id, picking[1], context=pick_context)
                            else:
                                if pick_order['process_by_machine'] is True:
                                    self.reset_machine_tracking(cr, uid, pick_id, picking[1], context=pick_context)
                                self.set_tracking_number(cr, uid, pick_id, picking[1], context=pick_context)

                    self.pool.get('res.log').create(cr, uid, {
                        'name': 'New tracking numbers for delivery orders',
                        'res_model': 'stock.picking',
                        'res_id': 0,
                        'debug_information': ";\n ".join(updated_trac_numbers),
                        })

        return True

    def find_tracking_number(self, cr, uid, tracking_ref, context=None):
        result = []
        result_picking_ids = []
        tracking_ref = tracking_ref.strip() if tracking_ref else ''

        if not tracking_ref:
            return result_picking_ids

        fetchdb_server_data = self.pool.get('fetchdb.server')
        server_ids = fetchdb_server_data.get_warehouse_servers(cr, uid)

        for server in fetchdb_server_data.browse(cr, uid, server_ids, context=context):
            sql = """IF OBJECT_ID('dbo.FED_SHP') IS NOT NULL
                BEGIN
                    SELECT
                        FS.BJ_NO,
                        LTRIM(RTRIM(FS.TRACK_NO)),
                        DATEADD(MINUTE,DATEDIFF(MINUTE,CURRENT_TIMESTAMP,GETUTCDATE()),FS.SHP_DATE),
                        LTRIM(RTRIM(FS.Carrier)),
                        LTRIM(RTRIM(FS.SHP_TYPE)),
                        LTRIM(RTRIM(FS.SHP_Cost)),
                        LTRIM(RTRIM(FS.ZPL_LABEL)),
                        LTRIM( RTRIM (FS.warehouse)),
                        LTRIM( RTRIM (FS.scanner)),
                        LTRIM( RTRIM (FH.WhLocation))
                    FROM dbo.FED_SHP as FS
                    LEFT JOIN FDX_HED FH on FH.BJ_NO = FS.BJ_NO
                    WHERE 1=1
                        AND '%s' like '%%' + LTRIM(RTRIM(FS.TRACK_NO))
                        AND FS.BJ_NO IS NOT NULL
                        AND LTRIM(RTRIM(FS.BJ_NO)) <> ''
                        AND FS.TRACK_NO IS NOT NULL
                        AND LTRIM(RTRIM(FS.TRACK_NO)) <> ''
                        AND (FS.CANCEL IS NULL OR FS.CANCEL != 1)
                        AND (FS.void IS NULL OR FS.void NOT IN ('Y', 'y', '1'))
                    ORDER BY FS.RECORD_ID DESC
                END
                ELSE
                BEGIN
                    SELECT TOP 0 NULL
                END
            """ % (tracking_ref,)
            result = fetchdb_server_data.make_query(cr, uid, server.id, sql) or []
        tracking_rows = set([tuple(x) for x in result if x])
        for tracking_row in tracking_rows:
            picking_ids = self.search(cr, uid, [('name', '=', tracking_row[0])])
            if picking_ids:

                for order in self.browse(cr, uid, picking_ids):

                    if order.state == 'returned':
                        raise osv.except_osv(_('Warning!'), 'Order %s already returned' % (order.name))

                    elif order.state == 'done':
                        batch_str = ''
                        if order.shipping_batch_id and order.shipping_batch_id.id or False:
                            batch_str = " in %s batch" % (order.shipping_batch_id.name)
                        raise osv.except_osv(_('Warning!'), 'Order %s already shipped%s with %s tracking#' % (order.name, batch_str, order.tracking_ref))

                    else:
                        picking_context = {
                            'shp_date': tracking_row[2] if str(tracking_row[2]) > '2000-01-01' else datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                            'carrier': tracking_row[3],
                            'shp_type': tracking_row[4],
                            'shp_cost': tracking_row[5],
                            'zpl': tracking_row[6] and base64.b64encode(tracking_row[6]) or None,
                            'warehouse_fed_shp': tracking_row[7] or None,
                            'scanner': tracking_row[8] or None,
                            'warehouse_fdx_hed': tracking_row[9] or None,
                        }
                        self.set_tracking_number(cr, uid, order.id, tracking_row[1], context=picking_context)
                        result_picking_ids.append(order.id)

        return result_picking_ids

    def get_warehouse(self, cr, uid, order_id, context=None):
        wh_id = False
        order = self.browse(cr, uid, order_id, context=context)
        for move_line in order.move_lines:
            if move_line.location_id:
                wh_id = self.pool.get('stock.location').get_warehouse(cr, uid, move_line.location_id.id)

                if wh_id:
                    return wh_id

        logger.warn("Failed. Can't find warehouse")

        return False

    def get_locations(self, cr, uid, ids, context=None):
        res = {}
        for sp_id in ids:
            locations = []
            moves = self.browse(cr, uid, sp_id).move_lines
            for move in moves:
                loc = move.location_id
                if loc and loc.complete_name not in locations:
                    locations.append(loc.complete_name)
            res[sp_id] = ", ".join(sorted(locations))
        return res

    # HOT FIX for bug # 21241
    def set_warehouses_locations(self, cr, uid, order_id, context=None):
        order = self.browse(cr, uid, order_id)
        locations = self.get_locations(cr, uid, [order_id], context=context)[order_id]
        if locations != order.locations:
            warehouse_id = self.get_order_params(
                cr, uid, order_id, ['wh_to_from_internal_shipment']
            ).get(order_id, {}).get('wh_to_from_internal_shipment', False)
            if not warehouse_id:
                warehouse_id = self.get_warehouse(cr, uid, order_id, context=context)
            self.write(cr, uid, [order_id], {
                'locations': locations,
                'warehouses': warehouse_id,
            })
            return True
        return False

    def get_shipping_info_by_code_and_wh(self, cr, uid, code, wh_id, context=None):
        shipping_data = self.pool.get('res.shipping.code')

        shipping_info = {
            'shp_type': None,
            'carrier': None
        }
        ship_code_ids = []

        if code:
            ship_code_ids = shipping_data.search(cr, uid, [('warehouse_id', '=', wh_id), ('code', '=', code)])

        if len(ship_code_ids) == 0:
            ship_code_ids = shipping_data.search(cr, uid, [('default', '=', True)])

        if len(ship_code_ids) > 0 and isinstance(ship_code_ids[0], (int, long)):
            ship_code = shipping_data.browse(cr, uid, ship_code_ids[0], context=context)
            shipping_info['shp_type'] = ship_code.type_id and str(ship_code.type_id.name)
            shipping_info['carrier'] = ship_code.service_id and str(ship_code.service_id.name)

        return shipping_info

    def get_boxs_weight(self, cr, uid, order, context=None):
        weight = 0.0
        ir_cp_obj = self.pool.get('ir.config_parameter')
        material_box_default_weight = ir_cp_obj.get_param(cr, uid, 'material.box.default.weight')
        material_main_box_weight = ir_cp_obj.get_param(cr, uid, 'material.main.box.weight')
        order_notes_list = []
        for stock_move in order.move_lines:
            if stock_move.box_id:
                if stock_move.box_id.packing_weight:
                    box_weight = stock_move.box_id.packing_weight
                    msg = "Using {box_code} Box with weight {box_weight}".format(box_code=stock_move.box_id.packing_code, box_weight=box_weight)
                    # order.add_message_to_note(msg)
                    order_notes_list.append(msg)
                else:
                    msg = 'Box weight for box: {box} is empty, using default box weight({default_box_weight})'.format(
                        box=(
                            stock_move.box_id and
                            (stock_move.box_id.packing_code or stock_move.box_id.packing_desc or 'None') or
                            'None'
                        ),
                        default_box_weight=material_box_default_weight
                    )
                    logger.warn(msg)
                    # order.add_message_to_note(msg)
                    order_notes_list.append(msg)
                    box_weight = material_box_default_weight
            else:
                msg = 'Not found box for {default_code}, using default box weight({default_box_weight})'.format(
                    default_code=(
                        stock_move and
                        stock_move.product_id and
                        stock_move.product_id.default_code or
                        ''),
                    default_box_weight=material_box_default_weight
                )
                logger.warn(msg)
                # order.add_message_to_note(msg)
                order_notes_list.append(msg)
                box_weight = material_box_default_weight
            try:
                box_weight = float(box_weight)
                weight += box_weight * stock_move.product_qty
            except ValueError:
                msg = "Failed. Cannot convert box_weight: '{}'' to float".format(str(box_weight))
                logger.warn(msg)
                order.add_message_to_note(msg)
                return False

        # DLMR-1054
        order.add_message_to_note(message=order_notes_list)

        try:
            weight += round(float(material_main_box_weight), 2)
        except ValueError:
            msg = "Failed. Cannot convert material_main_box_weight: '{}'' to float".format(
                str(material_main_box_weight))
            logger.warn(msg)
            order.add_message_to_note(msg)
            return False
        return weight

    def get_shp_weight(self, cr, uid, order, context=None):
        box_weight = self.get_boxs_weight(cr, uid, order, context=context)
        if box_weight:
            return box_weight
        weight = 0.0
        if(bool(re.match(r"(?i).*(\bups\b)", order.shp_service or ''))):
            weight = 1.0

        if order.process_by_machine is True:
            machine_weight = self.pool.get('ir.config_parameter').get_param(cr, uid, 'machine.default.weight')
            weight = machine_weight and float(machine_weight) or weight

        return weight

    def get_dimensions(self, cr, uid, order, context=None):
        def safe_convert(v, t=int, default=None):
            try:
                res = t(v)
            except:
                res = default
            return res

        result = False
        # DLMR-1671
        # Hardcode dimensions for StageStores/SmartPostService
        if order.real_partner_id.id == 41 and str(order.shp_code) == '95':
            return {
                'DimL': 6,
                'DimW': 4,
                'DimH': 1,
            }
        # END DLMR-1671
        moves = order.move_lines
        if len(moves) == 1 and safe_convert(moves[0].product_qty) == 1:
            box_id = moves[0].box_id
            if box_id:
                result = {
                    'DimL': box_id.length or None,
                    'DimW': box_id.width or None,
                    'DimH': box_id.height or None,
                }
        if not result:
            conf_obj = self.pool.get('ir.config_parameter')
            length = conf_obj.get_param(cr, uid, 'material.box.default.length')
            width = conf_obj.get_param(cr, uid, 'material.box.default.width')
            height = conf_obj.get_param(cr, uid, 'material.box.default.height')
            result = {
                'DimL': safe_convert(length),
                'DimW': safe_convert(width),
                'DimH': safe_convert(height),
            }
        return result

    def get_column_lengths(self, cr, uid, table_name, server_data, server_id):
        column_lengths = server_data.make_query(
            cr, uid, server_id,
            """SELECT column_name, character_maximum_length
               FROM information_schema.columns
               WHERE table_name = '{table_name}'
            """.format(table_name=table_name),
            params=[], select=True, commit=False)
        return {x[0]: x[1] for x in column_lengths}

    def check_column_lengths(self, columns, column_lengths, insert_obj):
        for column_name in columns:
            column_value = insert_obj.get(column_name, None)
            column_max_length = column_lengths.get(column_name, None)
            if column_value and column_max_length:
                column_length = 0
                if isinstance(column_value, (str, unicode)):
                    column_length = len(str(to_ascii(column_value)))
                else:
                    column_length = len(str(column_value))
                if column_length > column_max_length:
                    raise NotImplementedError(
                        'Length column {}[{}] > {}'.format(
                            column_name, column_length, column_max_length
                        )
                    )

    def fill_fedex_shipping_tables(self, cr, uid, ids, server_id=False, context=None):
        if not ids or not server_id:
            return False

        orders = self.browse(cr, uid, ids)
        if not orders:
            return False

        success = False

        address_data = self.pool.get('res.partner.address')
        server_data = self.pool.get('fetchdb.server')
        integration_data = self.pool.get('sale.integration')

        order_names = [x.name for x in orders]

        sql = """SELECT BJ_NO
            FROM dbo.FDX_HED
            WHERE BJ_NO IN (%s)
            GROUP BY BJ_NO;
        """ % (",".join(['?'] * len(order_names)))
        result = server_data.make_query(
            cr, uid, server_id,
            sql, params=order_names, select=True
        )

        if isinstance(result, (list,)) and len(result) != len(order_names):

            system_limit = self.pool.get('ir.config_parameter').get_param(
                cr, uid, 'delivery.remote.order.process.limit'
            )
            limit = system_limit and int(system_limit) or 100

            insert_sqls = []
            insert_params = []
            checked_names = [x[0].strip() for x in result if x and x[0]]
            new_names = []

            for order in orders:

                if len(insert_sqls) >= limit and limit > 0:
                    break

                if order.name in checked_names:
                    logger.warn("Failed. Record with BJ_NO = %s already exists in dbo.FDX_HED" % (order.name))
                    continue

                customer = order.sale_id.partner_id

                addresses = self.get_addresses(cr, uid, order.id)
                s_adr_id = addresses[order.id]['ship']
                f_adr_id = addresses[order.id]['return']
                t_adr_id = addresses[order.id]['invoice']
                s_adr = address_data.get_address(cr, uid, s_adr_id)
                f_adr = address_data.get_address(cr, uid, f_adr_id)
                t_adr = address_data.get_address(cr, uid, t_adr_id)

                billing_opt = 'TP' if bool(re.match(r"(?i).*(\bups\b)", order.shp_service or '')) else ''

                dimensions = self.get_dimensions(cr, uid, order)
                # TO REFACTOR: need to create and use a common function in
                # fetchbd for many inserts. insert_record won't help now.
                sql = """INSERT INTO dbo.FDX_HED (
                        BUS_CODE,
                        BJ_NO,
                        S_COMPANY,
                        SERVICE_TP,
                        SIGNATURE,
                        ACCOUNT,
                        ACCOUNT_COUNTRY,
                        ACCOUNT_BILLING_METHOD,
                        WEIGHT,
                        ShipDTStamp,
                        billingOPT,

                        cust,

                        NAME,
                        ADDRESS1,
                        ADDRESS2,
                        ADDRESS3,
                        S_CITY,
                        S_STATE,
                        S_COUNTRY,
                        S_ZIP,
                        S_TEL,

                        F_NAME,
                        F_COMPANY,
                        F_Address1,
                        F_Address2,
                        F_Address3,
                        F_CITY,
                        F_STATE,
                        F_Country,
                        F_ZIP,
                        F_Phone,

                        T_NAME,
                        T_COMPANYN,
                        T_Address1,
                        T_Address2,
                        T_Address3,
                        T_City,
                        T_State,
                        T_COUNTRY,
                        T_Zip,
                        T_Phone,
                        Ref1,
                        Ref2,
                        ASIN,
                        insurance,
                        WhLocation,
                        DimL,
                        DimW,
                        DimH,
                        "321"
                    )VALUES(
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        dateadd(day,+?,getdate()),
                        ?,

                        ?,

                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,

                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,

                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                );"""
                insert_obj = {
                    'BUS_CODE': order.name,
                    'BJ_NO': order.name,
                    'S_COMPANY': s_adr['s_company'] or '',
                    'SERVICE_TP': order.shp_code or '',
                    'SIGNATURE': dict(SIGNATURE_CODES).get(order.shp_signature, 'N'),
                    'ACCOUNT': order.shp_account or '',
                    'ACCOUNT_COUNTRY': order.shp_account_country or '',
                    'ACCOUNT_BILLING_METHOD': order.billing_method or '',
                    'WEIGHT': self.get_shp_weight(cr, uid, order),
                    'ShipDTStamp': context.get('shp_date_delta', 0),
                    'billingOPT': billing_opt or '',

                    'cust': customer.ref or 'null',

                    'NAME': s_adr['name'] or '',
                    'ADDRESS1': s_adr['street'] or '',
                    'ADDRESS2': s_adr['street2'] or '',
                    'ADDRESS3': s_adr['street3'] or '',
                    'S_CITY': s_adr['city'] or '',
                    'S_STATE': s_adr['state'] or '',
                    'S_COUNTRY': s_adr['country'] or '',
                    'S_ZIP': s_adr['zip'] or '',
                    'S_TEL': s_adr['tel'] or '',

                    'F_NAME': f_adr['name'] or '',
                    'F_COMPANY': f_adr['company'] or '',
                    'F_Address1': f_adr['street'] or '',
                    'F_Address2': f_adr['street2'] or '',
                    'F_Address3': f_adr['street3'] or '',
                    'F_CITY': f_adr['city'] or 'null',
                    'F_STATE': f_adr['state'] or 'null',
                    'F_Country': f_adr['country'] or 'US',
                    'F_ZIP': f_adr['zip'] or 'null',
                    'F_Phone': f_adr['tel'] or 'null',

                    'T_NAME': t_adr['name'] or '',
                    'T_COMPANYN': t_adr['company'] or '',
                    'T_Address1': t_adr['street'] or '',
                    'T_Address2': t_adr['street2'] or '',
                    'T_Address3': t_adr['street3'] or '',
                    'T_City': t_adr['city'] or 'null',
                    'T_State': t_adr['state'] or 'null',
                    'T_COUNTRY': t_adr['country'] or '',
                    'T_Zip': t_adr['zip'] or 'null',
                    'T_Phone': t_adr['tel'] or 'null',
                    'Ref1': 'null',  # overrided by additional_information below
                    'Ref2': 'null',  # overrided by additional_information below
                    'ASIN': order.asn_number or 'null',
                    'insurance': "Y" if order.shp_insurance else "N",
                    'WhLocation': order.warehouses and order.warehouses.name[:10] or "null",
                    'DimL': dimensions['DimL'] or None,
                    'DimW': dimensions['DimW'] or None,
                    'DimH': dimensions['DimH'] or None,
                    '321': order.total_price if self.is_321(cr, order) else None,
                }
                # Monkey patching for fix S_TEL for FMJ
                if str(customer.ref).strip().lower() == 'fmj':
                    insert_obj['S_TEL'] = '8003426663'

                insert_sqls.append(sql)

                new_names.append(order.name)

                additional_information = \
                    integration_data.getAdditionalShippingInformation(
                        cr, uid,
                        order.id,
                        insert_obj, {
                            'customer_id': customer.id
                        })

                if additional_information:
                    insert_obj.update(additional_information)


                columns = (
                    'BUS_CODE', 'BJ_NO', 'S_COMPANY', 'SERVICE_TP', 'SIGNATURE', 'ACCOUNT', 'ACCOUNT_COUNTRY', 'ACCOUNT_BILLING_METHOD', 'WEIGHT',
                    'ShipDTStamp', 'billingOPT', 'cust', 'NAME', 'ADDRESS1', 'ADDRESS2', 'ADDRESS3', 'S_CITY', 'S_STATE', 'S_COUNTRY', 'S_ZIP',
                    'S_TEL', 'F_NAME', 'F_COMPANY', 'F_Address1', 'F_Address2', 'F_Address3', 'F_CITY', 'F_STATE', "F_Country", 'F_ZIP', "F_Phone",
                    'T_NAME', 'T_COMPANYN', 'T_Address1', 'T_Address2', 'T_Address3', 'T_City', 'T_State', 'T_COUNTRY', 'T_Zip',
                    'T_Phone', 'Ref1', 'Ref2', "ASIN", "insurance", "WhLocation", "P_NAME", "P_COMPANY", "P_Address1",
                    "P_Address2", "P_CITY", "P_STATE", "P_ZIP", "P_Country", "P_Phone", "DimL", "DimW", "DimH", "321")

                column_lengths = self.get_column_lengths(
                    cr, uid, 'FDX_HED', server_data, server_id)
                self.check_column_lengths(columns, column_lengths, insert_obj)

                for i in columns:
                    if i in insert_obj:
                        insert_params.append(insert_obj.get(i))

            if insert_sqls:
                success = bool(server_data.make_query(cr, uid, server_id, "\n\n".join(insert_sqls), params=insert_params, select=False, commit=True))
                if success:
                    logger.warn("Success. New records was inserted into dbo.FDX_HED for following orders: \n%s" % (new_names))
                    self.update_FDX_DET(
                        cr, uid, orders, server_data, server_id)

        else:
            logger.warn("Faild. All records already exist in dbo.FDX_HED for following orders: \n%s" % (order_names))

        return success

    def update_FDX_DET(self, cr, uid, orders, server_data, server_id):
        insert_sqls = []
        insert_params = []
        exists_names = []
        new_names = []
        order_names = [x.name for x in orders]
        sql = """SELECT BUS_CODE
            FROM dbo.FDX_DET
            WHERE BUS_CODE IN (%s)
            GROUP BY BUS_CODE;
        """ % (",".join(['?'] * len(order_names)))
        result = server_data.make_query(
            cr, uid, server_id,
            sql, params=order_names, select=True
        )
        if isinstance(result, (list,)):
            exists_names = [x[0].strip() for x in result if x and x[0]]
        for order in orders:
            if order.name in exists_names:
                logger.warn(
                    """
                    Failed. Record with BUS_CODE = {}
                    already exists in dbo.FDX_HED
                    """.format(order.name))
                continue
            lines = order.move_lines
            for line in lines:
                sql = """INSERT INTO dbo.FDX_DET (
                        BUS_CODE,
                        DESC1,
                        QTY_SHIP,
                        MEA,
                        VALUE,
                        ORIGIN,
                        TARIFF,
                        Style,
                        Bin
                    )VALUES(
                        ?,
                        ?,
                        ?,
                        NULL,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                );"""

                country_code = False
                if (
                    order and order.sale_id and
                    order.sale_id.partner_shipping_id and
                    order.sale_id.partner_shipping_id.country_id and
                    order.sale_id.partner_shipping_id.country_id.code
                ):
                    country_code = \
                        order.sale_id.partner_shipping_id.country_id.code

                product = line and line.product_id or False

                if product:
                    insert_obj = {
                        'BUS_CODE': (order and order.name or ''),
                        'DESC1': product.short_description or '',
                        'QTY_SHIP': line and line.product_qty or 0.0,
                        # 'MEA': 'null',
                        'VALUE': (
                            line and line.sale_line_id and
                            line.sale_line_id.customerCost or 0.0),
                        'ORIGIN': product.prod_manufacture or '',
                        'TARIFF': (
                            product.prod_tarif_classification_ca
                            if country_code == 'CA'
                            else product.prod_tarif_classification
                        ) or '',
                        # 'ID': 'auto',
                        # 'TS': 'auto',
                        'Style': (
                            line and (
                                line.product_id.id_delmar or
                                line.product_id.default_code
                            ) or ''),
                        'Bin': (
                            line and line.bin_id and
                            line.bin_id.name or ''),
                    }

                    # DLMR-1428
                    # Hard-code cheat for Gem&Harmony customer
                    if  country_code != 'US' and order.sale_id.partner_id.id == 870:
                        insert_obj.update({
                            'VALUE': (line and line.sale_line_id and line.sale_line_id.price_unit or 0.0)
                        })
                    # END DLMR-1428

                    columns = [
                        'BUS_CODE', 'DESC1', 'QTY_SHIP', 'VALUE',
                        'ORIGIN', 'TARIFF', 'Style', 'Bin'
                    ]
                    column_lengths = self.get_column_lengths(
                        cr, uid, 'FDX_HED', server_data, server_id)
                    self.check_column_lengths(
                        columns, column_lengths, insert_obj)
                    insert_sqls.append(sql)
                    for i in columns:
                        if i in insert_obj:
                            insert_params.append(insert_obj.get(i))
                    new_names.append(order.name)
                else:
                    logger.warn(
                        """
                        Failed get product for stock_move: {sm_id}
                        for order: {order_name}
                        """.format(sm_id=line.id, order_name=order.name))
        success = bool(
            server_data.make_query(
                cr, uid, server_id, "\n\n".join(insert_sqls),
                params=insert_params, select=False, commit=True
            ))
        if success:
            logger.warn(
                """
                Success. New records was inserted
                into dbo.FDX_DET for following orders:
                {}
                """.format(new_names))

    def get_addresses(self, cr, uid, ids, addresses=None, context=None):
        res = {}
        if addresses is None:
            addresses = [
                'ship',
                'physical',
                'invoice',
                'return',
                'other'
            ]
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]
            for order in self.browse(cr, uid, ids):
                tmp_addr = False
                res[order.id] = {
                    'ship': order.address_id.id,
                    'physical': None,
                    'invoice': None,
                    'return': None,
                }

                cus = order.sale_id.partner_id
                wh_id = order.warehouses.id or False

                for addr in cus.address:
                    if addr.type in addresses:
                        if not res[order.id]['physical'] and addr.type == 'physical' and addr.warehouse_id == wh_id:
                            res[order.id]['physical'] = addr.id
                        elif not res[order.id]['invoice'] and addr.type == 'invoice':
                            res[order.id]['invoice'] = addr.id
                        elif addr.type == 'return':
                            if addr.warehouse_id and addr.warehouse_id.id == wh_id \
                                and (not addr.min_cost or addr.min_cost <= order.total_price) \
                                and (not tmp_addr or addr.min_cost > tmp_addr.min_cost):
                                tmp_addr = addr

                if 'return' in addresses:
                    res[order.id]['return'] = tmp_addr and tmp_addr.id or None
                    if not res[order.id]['return']:
                        for addr in cus.address:
                            if addr.type == 'return' and addr.default_return:
                                res[order.id]['return'] = addr.id
                                break

                if 'physical' in addresses and not res[order.id]['physical']:
                    for addr in cus.address:
                        if addr.type == 'physical' and addr.default_return:
                            res[order.id]['physical'] = addr.id
                            break

                if 'other' not in res[order.id]:
                    for addr in cus.address:
                        if addr.type == 'other':
                            res[order.id]['other'] = addr.id
                            break

        return res

    def fill_to_process_queue(self, cr, uid, ids, server_id=False, context=None):
        logger.warn("Start fill_to_process_queue")
        server_data = self.pool.get('fetchdb.server')

        if not ids or not server_id:
            return False
        orders = self.browse(cr, uid, ids)
        if not orders:
            return False

        if not server_data.check_mssql_schema(cr, uid, server_id, 'dbo.ProcessQueue'):
            return False

        new_names = []
        insert_sqls = []
        for order in orders:

            sql_fcdprocessqueue = "INSERT INTO dbo.ProcessQueue (PQ_IMPKGID, PQ_Priority) VALUES ('%s', 5);" % (order.name)
            sql_fcdprocessqueue = sql_fcdprocessqueue.replace("'null'", "NULL")
            insert_sqls.append(sql_fcdprocessqueue)

            new_names.append(order.name)

        if insert_sqls:
            success = bool(server_data.make_query(cr, uid, server_id, "\n\n".join(insert_sqls), select=False, commit=True))
            if success:
                logger.warn("Success. New records was inserted into dbo.ProcessQueue for following orders: \n%s" % (new_names))

    def fill_shipping_info_table(self, cr, uid, ids, server_id=False, context=None):
        logger.warn("Start fill_shipping_info_table")

        if not ids or not server_id:
            return False

        server_data = self.pool.get('fetchdb.server')
        if not server_data.check_mssql_schema(cr, uid, server_id, 'dbo.Order_Item'):
            return False

        orders = self.browse(cr, uid, ids)
        if not orders:
            return False
        order_list = []
        for order in orders:
            if order.sale_id.partner_id.fill_order_item:
                order_list.append(order)
        if len(order_list) == 0:
            return False
        orders = order_list

        success = False

        order_names = [x.name for x in orders]

        sql = """SELECT
                    BJ_NO
                 FROM
                    dbo.Order_Item
                 WHERE
                    BJ_NO IN ('%s')
                 GROUP BY
                    BJ_NO;""" % ("','".join(order_names))
        result = server_data.make_query(cr, uid, server_id, sql, select=True)

        if isinstance(result, (list,)) and len(result) != len(order_names):

            system_limit = self.pool.get('ir.config_parameter').get_param(cr, uid, 'delivery.remote.order.process.limit')
            limit = system_limit and int(system_limit) or 100

            insert_data = {}  # {'order_name': {'item/size': {'BJ_NO': 'order_name', ...}}}
            checked_names = [x[0].strip() for x in result if x and x[0]]
            new_names = []

            insert_field_names = (
                'BJ_NO',
                'Items',
                'UPC',
                'QTY',
                'Tariff',
                'CountryOfManufacture',
                'SellingPrice',
                'ShortDesc',
            )
            len_insert_fields = len(insert_field_names)
            insert_sql = """INSERT INTO dbo.Order_Item ({field_names}) VALUES {vals};"""
            params_base_sql = '(' + ', '.join(['?'] * len_insert_fields) + ')'

            upc_sql = """SELECT
                        pmcn.name,
                        pmcf.value,
                        pmcf.size_id
                     FROM
                        product_multi_customer_names pmcn
                        INNER JOIN product_multi_customer_fields_name pmcfn ON pmcfn.name_id = pmcn.id
                        INNER JOIN product_multi_customer_fields pmcf ON pmcf.field_name_id = pmcfn.id
                     WHERE
                        1=1
                        AND pmcf.partner_id = %s
                        AND pmcn.name = 'upc'
                        AND pmcf.product_id = %s
                        AND pmcfn.label = 'UPC'
                        AND pmcfn.customer_id = %s"""
            for order in orders:
                if order.name in checked_names:
                    logger.warn("Fail. Record with BJ_NO = %s already exist in dbo.Order_Item" % (order.name))
                    continue

                if order.name not in insert_data:
                    insert_data[order.name] = {}
                order_data = insert_data[order.name]

                cus = order.sale_id.partner_id
                for move in order.move_lines:
                    size = ""
                    sale_line = move.sale_line_id
                    sale_product = sale_line.product_id
                    if sale_line.size_id:
                        size = "/" + str(sale_line.size_id.name)

                    item = sale_product.default_code + size
                    if item in order_data:
                        item_data = order_data[item]
                        new_qty = item_data.get('QTY', 0) + move.product_qty
                        item_data['QTY'] = new_qty
                    else:
                        where_size = " AND pmcf.size_id = %s" % str(sale_line.size_id.id) if size else ""
                        cr.execute(upc_sql + where_size, (cus.id, sale_product.id, cus.id))
                        res = cr.fetchone()
                        upc = res[1] if res else ''

                        selling_price = sale_line.price_unit
                        try:
                            retail_price = float(sale_line.customerCost)
                            if retail_price:
                                selling_price = retail_price
                        except:
                            pass

                        order_data[item] = {
                            'BJ_NO': order.name,
                            'Items': item,
                            'UPC': upc,
                            'QTY': move.product_qty,
                            'Tariff': sale_product.prod_tarif_classification or '',  # PROD_TARIF
                            'CountryOfManufacture': sale_product.made_in and sale_product.made_in.code or '',  # PROD_MANUF
                            'SellingPrice': selling_price or '',  # SELLINGPRICE
                            'ShortDesc': sale_product.short_description or sale_product.name or '',  # DESC
                        }
                new_names.append(order.name)

            insert_params = [insert_data[order][item] for order in insert_data for item in insert_data[order]]

            def chunks(l, n):
                """Yield successive n-sized chunks from l."""
                for i in xrange(0, len(l), n):
                    yield l[i:i+n]

            for insert_chunk in chunks(insert_params, limit):
                params_sql = ',\n'.join([params_base_sql] * len(insert_chunk))
                params_list = []
                for insert_vals in insert_chunk:
                    params_list += [insert_vals.get(name) for name in insert_field_names]
                insert_chunk_sql = insert_sql.format(field_names=', '.join(insert_field_names), vals=params_sql)
                success = bool(server_data.make_query(cr, uid, server_id, insert_chunk_sql, params=params_list, select=False, commit=True))
            if success:
                logger.warn("Success. New records was inserted into dbo.Order_Item for following orders: \n%s" % (new_names))

        else:
            logger.warn("Faild. All records already exist in dbo.Order_Item for following orders: \n%s" % (order_names))

        return success

    def reset_shipping_info(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        server_data = self.pool.get('fetchdb.server')

        logger.info("reset_shipping_info")
        wh_server_ids = server_data.get_warehouse_servers(cr, uid, context=context)
        if not wh_server_ids:
            logger.warn("Failed. Can't reset shipping info. List of warehouse's DB is empty.")

        for order in self.browse(cr, uid, ids):

            for server_id in wh_server_ids:

                self.reset_fedex_shipping_tables(cr, uid, order, server_id)

                if context.get('update_order_item', True):
                    self.reset_shipping_info_table(cr, uid, order, server_id)

        return True

    def update_shipping_info(self, cr, uid, ids, context=None, force=False):

        batch_obj = self.pool.get('stock.picking.shipping.batch')
        shp_data = self.pool.get('res.shipping.code')
        shipping_context = {'update_order_item': False}
        allowed_states = ['picking', 'outcode_except', 'shipped']

        for order_id in ids:

            if not force:
                self.check_tracking_numbers(cr, uid, picking_ids=[order_id], context=None)

            order = self.browse(cr, uid, order_id)

            if force:
                if order.state not in allowed_states:
                    continue
            else:
                if order.state != 'picking':
                    continue

            shp_obj = False
            update_shp_info = False

            if order.fixed_shp_code and order.shp_code and not force:
                update_shp_info = True

            else:

                shp_obj = shp_data.get_code(cr, uid,
                                            pick_id=order.id,
                                            context={'force': force, },
                                            )

                if not shp_obj:
                    if force and order.fixed_shp_code and order.shp_code:
                        update_shp_info = True
                    else:
                        raise osv.except_osv(_('Warning!'), 'Can not find code for %s order . Please check Shipping Code table' % (order.name))

                elif shp_obj:
                    if force:
                        update_shp_info = True
                    else:
                        if order.shipping_batch_id and order.shipping_batch_id.shipping_code != shp_obj.code:
                            batch_obj.write(cr, uid, order.shipping_batch_id.id, {'shipping_code': shp_obj.code, 'tracking_ref': False})

                        current_shp_service = shp_obj.service_id and shp_obj.service_id.name or False
                        current_shp_type = shp_obj.type_id and shp_obj.type_id.name or False
                        current_carrier = current_shp_service

                        current_shp_insurance = shp_obj.insurance

                        if current_carrier and current_shp_type:
                            current_carrier += ', ' + current_shp_type

                        if shp_obj.code != order.shp_code \
                                or shp_obj.signature != order.shp_signature \
                                or current_shp_service != order.shp_service \
                                or current_carrier != order.carrier \
                                or current_shp_insurance != order.shp_insurance:
                            update_shp_info = True

            if shp_obj and shp_obj.shipping_handling != order.shp_handling:
                self.write(cr, uid, order.id, {
                    'shp_handling': shp_obj.shipping_handling,
                })

            if update_shp_info:
                self.reset_shipping_info(cr, uid, [order.id], context=shipping_context)

                if shp_obj:
                    self.write(cr, uid, order.id, {
                        'tracking_ref': False,
                        'machine_tracking_ref': False,
                        'carrier_code': False,
                        'fixed_shp_code': False
                    })
                    self.set_shipping_rule(cr, uid, order.id, shp_obj.id)
                elif order.fixed_shp_code and not order.shp_account:
                    account = False
                    account_from_order = self._get_shp_account_from_order(cr, uid, order.id)
                    if not account_from_order:
                        account = self.get_shp_account(cr, uid, order.id, order.shp_service)
                    account_number = account_from_order or account and account.account
                    if account_number:
                        self.write(cr, uid, [order.id], {
                            'shp_account': account_number,
                            'shp_account_country': account and account.country_id and account.country_id.code,
                            'billing_method': account and account.billing_method,
                        })
                self.do_remote_order_process(cr, uid, [order.id], context=shipping_context)

        return True

    def reset_machine_tracking(self, cr, uid, id, tracking_number, context=None):

        order = self.browse(cr, uid, id)
        if order.machine_tracking_ref == tracking_number:
            return True

        if context is None:
            context = {}

        machine_obj = self.pool.get('machine.settings')
        machine_ids = machine_obj.search(cr, uid, [('wh_ids', '!=', False), ('active', '=', True)])
        machine_server_ids = []
        if len(machine_ids) > 0:
            for machine in machine_obj.browse(cr, uid, machine_ids):
                if machine.shipping_server_id.id not in machine_server_ids:
                    machine_server_ids.append(machine.shipping_server_id.id)

        fetchdb_server_data = self.pool.get('fetchdb.server')

        for server_id in machine_server_ids:
            sql = """SELECT
                        LTRIM(RTRIM(FS.BJ_NO)),
                        LTRIM(RTRIM(FS.TRACK_NO))
                    FROM
                        dbo.FED_SHP as FS
                        LEFT JOIN dbo.FDX_HED as FH on FS.BJ_NO = FH.BJ_NO
                    WHERE
                        FS.BJ_NO  = '%s'
                        AND FS.TRACK_NO  = '%s'""" % (order.name, tracking_number)

            check_field = fetchdb_server_data.make_query(cr, uid, server_id, sql) or []
            if bool(check_field) and len(check_field) > 0:
                return True
            else:
                self.reset_fedex_shipping_tables(cr, uid, order, server_id, context=context)

        return True

    def reset_fedex_shipping_tables(self, cr, uid, order, server_id, context=None):
        server_data = self.pool.get('fetchdb.server')
        order_no = order.name

        sql = """
            IF OBJECT_ID('dbo.FDX_HED') IS NOT NULL
            BEGIN
                DELETE FROM dbo.FDX_HED WHERE BJ_NO = '%s'
            END;
            IF OBJECT_ID('dbo.FDX_DET') IS NOT NULL
            BEGIN
                DELETE FROM dbo.FDX_DET WHERE BUS_CODE = '%s'
            END;
            IF OBJECT_ID('dbo.FED_SHP') IS NOT NULL
            BEGIN
                DELETE FROM dbo.FED_SHP WHERE BJ_NO = '%s'
            END;
        """ % (order_no, order_no, order_no)
        server_data.make_query(cr, uid, server_id, sql, select=False, commit=True)

        return True

    def reset_shipping_info_table(self, cr, uid, order, server_id, context=None):
        if order.sale_id.partner_id.fill_order_item:
            server_data = self.pool.get('fetchdb.server')
            order_no = order.name

            sql = """
            IF OBJECT_ID('dbo.Order_Item') IS NOT NULL
            BEGIN
                DELETE FROM dbo.Order_Item WHERE BJ_NO = '%s'
            END;
            """ % (order_no)
            server_data.make_query(cr, uid, server_id, sql, select=False, commit=True)

        return True

    def do_remote_order_process(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        orders_by_server = {
            'fedex': {},
            'processQueue': {},
            'machine': {}
        }

        server_settings = self.get_shipping_server_id(cr, uid, ids)

        # The machine
        machine_obj = self.pool.get('machine.settings')
        machine_ids = machine_obj.search(cr, uid, [('wh_ids', '!=', False), ('active', '=', True)])
        machine_server_ids = {}
        if len(machine_ids) > 0:
            for machine in machine_obj.browse(cr, uid, machine_ids):
                for wh_id in machine.wh_ids:
                    if not machine_server_ids.get(wh_id.id, False):
                        machine_server_ids[wh_id.id] = machine.shipping_server_id.id

        for order in self.browse(cr, uid, ids):

            if order.shipping_batch_id and order.shipping_batch_id.tracking_ref:
                self.write(cr, uid, order.id, {'tracking_ref': order.shipping_batch_id.tracking_ref, 'carrier_code': order.shipping_batch_id.carrier_code})
                continue

            key = 'fedex'
            server_id = server_settings.get(order.id, False)

            if not order.move_lines:
                logger.warn("Failed. Order has no move_lines")

            processQueue_key = ''
            if order.sale_id.processQueue:
                processQueue_key = 'processQueue'


            #Check if order for machine
            order_wh_id = self.get_warehouse(cr, uid, order.id)
            if order.process_by_machine and machine_server_ids.get(order_wh_id, False):
                machine_server_id = machine_server_ids[order_wh_id]
                if orders_by_server['machine'].get(machine_server_id, False):
                    orders_by_server['machine'][machine_server_id].append(order.id)
                else:
                    orders_by_server['machine'][machine_server_id] = [order.id]

            if key:
                if orders_by_server[key].get(server_id, False):
                    orders_by_server[key][server_id].append(order.id)
                else:
                    orders_by_server[key][server_id] = [order.id]
                if(processQueue_key != ''):
                    if orders_by_server[processQueue_key].get(server_id, False):
                        orders_by_server[processQueue_key][server_id].append(order.id)
                    else:
                        orders_by_server[processQueue_key][server_id] = [order.id]

            else:
                logger.warn("Failed. Can't find warehouse for order %s" % (order.name))

        for key in orders_by_server:
            for server_id in orders_by_server[key]:

                if not server_id:
                    logger.warn("Failed. Can't find db server for following orders: %s" % (tuple(orders_by_server[key][server_id]), ))
                    continue

                if key == 'fedex':
                    self.fill_fedex_shipping_tables(cr, uid, orders_by_server[key][server_id], server_id=server_id, context=context)
                elif key == 'processQueue':
                    self.fill_to_process_queue(cr, uid, orders_by_server[key][server_id], server_id=server_id, context=context)

                if key == 'machine':
                    context.update({'shp_date_delta': 1})
                    self.fill_fedex_shipping_tables(cr, uid, orders_by_server[key][server_id], server_id=server_id,
                                                    context=context)

                if context.get('update_order_item', True):
                    self.fill_shipping_info_table(cr, uid, orders_by_server[key][server_id], server_id=server_id, context=context)

        return True

    def action_process(self, cr, uid, ids, context=None):
        #import pdb; pdb.set_trace()
        # setting basic defaults for the method
        export_obj = self.pool.get('picking.export')
        product_picking_obj=self.pool.get('product.picking')
        logger.info("RUN ACTION_PROCESS for IDS: %s, CONTEXT: %s" % (ids, context))
        if context is None or not isinstance(context, dict):
            context = {}

        process_ids = []
        picking_report_ids = []
        res = {'datas': {'ids': []}}
        wf_service = netsvc.LocalService("workflow")

        if not ids:
            return res

        # get all pos that are ready to process
        picking_ids = self.search(cr, uid, [('id', 'in', ids), ('state', 'in', ['assigned', 'incode_except', 'unverified'])])

        # updates the shiping infromation. If Fixed SHipping code is clicked it just updates account if not we get infromation from res_shipping_code.
        self.check_incoming_code(cr, uid, picking_ids, context=context)

        # figures out which box and which packing material to use and updates stock_move. Also adds this infromation to TRANSACTIONS table
        self.write_box_id(cr, uid, picking_ids, context=context)

        # DLMR-1190
        if picking_ids and (not self.check_verified(cr, uid, picking_ids, context=context)):
            return self.create_picking_repot(cr, uid, ids=picking_ids, report_name=None, context={'unverified': True})

        # unverified packing print signal
        print_unverified_packing = False
        print_unverified_ids = []

        for picking in self.read(cr, uid, picking_ids, ['id', 'carrier', 'shp_code', 'fixed_shp_code', 'shp_account', 'shp_service', 'state'], context=context):
            """
            logger.info("CHECK TEST_VERIFIED, State: %s" % picking['state'])
            if not self.test_verified(cr, uid, [picking['id']], context):
                if not picking['state'] == 'unverified':
                    self.write(cr, uid, picking['id'], {'state': 'unverified'})
                # signal to print packing list
                print_unverified_packing = True
                print_unverified_ids.append(picking['id'])
                continue
            """
            # Demo prep
            #if not context.get('print_packslip_by_items', None) and self.is_product_pick_block(cr, uid, [picking['id']]):
            #    continue

            # if have shipping code but no account add the account
            if picking.get('fixed_shp_code', False) and not picking.get('shp_account', False):
                account = False
                # check if account is in order
                account_from_order = self._get_shp_account_from_order(cr, uid, picking.get('id'))
                if not account_from_order and picking.get('shp_service', False):
                    # get account fro stock_move.acc
                    account = self.get_shp_account(cr, uid, picking.get('id'), picking.get('shp_service'))
                account_number = account_from_order or account and account.account
                if account_number:
                    # save account infroamtion to stock_move
                    self.write(cr, uid, [picking.get('id')], {
                        'shp_account': account_number,
                        'shp_account_country': account and account.country_id and account.country_id.code,
                        'billing_method': account and account.billing_method
                    })

            if picking.get('shp_code', False):
                process_ids.append(picking['id'])
            else:
                # change status to incoming code exception. CANT PROCESS
                wf_service.trg_validate(uid, 'stock.picking', picking['id'], 'button_incode_except', cr)

        logger.info("Process IDS: %s" % process_ids)

        for process_id in process_ids:
            # allocates the item from designated bin officially moving them out. Data will be updated in stock_move
            context = dict(context, active_ids=[process_id], active_model=self._name)
            partial_data = self.pool.get("stock.partial.picking")
            partial_id = partial_data.create(cr, uid, {}, context=context)
            logger.info("Partial ID: %s, Context: %s" % (partial_id, context))
            res = partial_data.do_partial(cr, uid, [partial_id], context=context)
            logger.info("Partial res: %s" % res)
            picking_report_ids += [res[x]['delivered_picking'] for x in res]
            logger.info("Picking report ids: %s" % picking_report_ids)

            # Demo prep
            context.update({'product_pick_hard_remove': False})
            #self.product_pick_remove(cr, uid, ids, context=context)
            #self.product_pick_process(cr, uid, ids, context=context)

        if picking_report_ids:
            context.update({'process_order': False})

            if context.get('print_packslip_by_items', None):
                self.send_data_to_machine(cr, uid, picking_report_ids, context=context)
            if not context.get('batch_report', False):
                res = self.create_picking_repot(cr, uid, picking_report_ids, context=context)

            if context.get('batch_report', False):
                service = netsvc.LocalService("report.stock.picking.list")
                (result, format) = service.create(cr, uid, picking_report_ids, data={}, context=context)
                if result:
                    order_names = [str(pick.name) for pick in self.browse(cr, uid, picking_report_ids)]
                    wh_id = self.get_warehouse(cr, uid, picking_report_ids[0])
                    paper_format=context.get('paper_format')
                    print_pslip=context.get('print_pslip')
                    notes_str = self.pool.get('res.users').browse(cr, uid, uid).name + ' manually printed ' + str(len(picking_report_ids)) + ' orders: ' + ', '.join(order_names) + ' info: ' + str(context.get('note'))
                    report_id=self.pool.get('picking.export').create(cr, uid, {
                        'pdf_filename': 'manually_printed_packslips.pdf',
                        'pdf_file': base64.b64encode(result),
                        'state': 'printed',
                        'status': '',
                        'paper_format':paper_format.lower(),
                        'notes': notes_str,
                        'scheduled_datetime': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                        'printed_user': uid,
                        'warehouse': [wh_id],
                        'print_pslip': print_pslip,
                    })
                    if report_id:
                        origin_id=context.get('origin_id')
                        export_obj.write(cr, uid, [report_id], {'product_picking_id': origin_id})
                        product_picking_obj.write(cr, uid, [origin_id], {'state': 'pick'})

        # DLMR-1190
        #if print_unverified_packing:
        #    logger.info('Trying to auto print packing list')
        #    return self.create_picking_repot(cr, uid, ids=print_unverified_ids, report_name=None, context=context)

        return res

    def generate_packing_slip_test(self,cr, uid, ids, picking_report_ids, po, origin_id, context=None):
        import pdb; pdb.set_trace()
        if picking_report_ids:
            context = {'process_order': False}
            service = netsvc.LocalService("report.stock.picking.list")
            (result, format) = service.create(cr, uid, picking_report_ids, data={}, context=context)
            if result:
                order_names = [str(pick.name) for pick in self.browse(cr, uid, picking_report_ids)]
                wh_id = self.get_warehouse(cr, uid, picking_report_ids[0])
                paper_format='standard'
                print_pslip=False
                notes_str = self.pool.get('res.users').browse(cr, uid, uid).name + ' manually printed ' + str(len(picking_report_ids)) + ' orders: ' + ', '.join(order_names) + ' info: ' + str(context.get('note'))
                report_id=self.pool.get('picking.export').create(cr, uid, {
                    'pdf_filename': 'manually_printed_packslips.pdf',
                    'pdf_file': base64.b64encode(result),
                    'state': 'printed',
                    'status': '',
                    'paper_format':paper_format.lower(),
                    'notes': notes_str,
                    'scheduled_datetime': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                    'printed_user': uid,
                    'warehouse': [wh_id],
                    'print_pslip': print_pslip,
                    'po':po,
                })
                if report_id:
                    export_obj.write(cr, uid, [report_id], {'product_picking_id': origin_id})
                    product_picking_obj.write(cr, uid, [origin_id], {'state': 'pick'})
        return res


    def send_data_to_machine(self, cr, uid, ids, context=None):
        # TODO: Machine still working or not?

        if not ids:
            return False

        service = netsvc.LocalService("report.stock.picking.list")

        for pick in self.browse(cr, uid, ids):
            item = pick.move_lines and pick.move_lines[0].product_id
            default_code = item.default_code
            (result, format) = service.create(cr, uid, [pick.id], data={}, context=context)
            if result:
                packing_slip = base64.b64encode(result)
                data = {
                    'order': pick.name,
                    'default_code': default_code,
                    'packing_slip': packing_slip,
                    }

        return True

    # DLMR-1190
    def test_verified(self, cr, uid, ids, context=None):
        logger.info("RUN TEST_VERIFIED, ids: %s, context: %s" % (ids, context))
        ok = True
        # init objects
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        move_obj = self.pool.get('stock.move')
        # iterate over orders
        pick = self.browse(cr, uid, ids[0])
        # get movelines ids
        sm_ids = [x.id for x in pick.move_lines if x.location_id.warehouse_id.smart_scanning]
        not_sm_ids = [x.id for x in pick.move_lines if not x.location_id.warehouse_id.smart_scanning]
        # if not smart_scanning lines - return verified true
        if not_sm_ids:
            # logger.info("NO STOCK_MOVES DIE TO WAREHOUSE is not SMART SCANNING")
            # sm_ids_to_verify_qty = [x.id for x in pick.move_lines if not x.location_id.warehouse_id.smart_scanning]
            logger.info("UPDATING STOCK_MOVES qty_verified")
            for mv in move_obj.browse(cr, uid, not_sm_ids):
                move_obj.write(cr, uid, mv.id, {'qty_verified': mv.product_qty})
            if not sm_ids:
                return True

        if sm_ids:
            # do check mssql query
            check_sql = """
                SELECT 
                    ERP_SM_ID, 
                    CASE WHEN PICK_VERIFIED=1 THEN 1 ELSE 0 END AS VERIFIED 
                FROM TRANSACTIONS 
                WHERE ERP_SM_ID IN ({}) AND SIGN=-1
            """.format(",".join([str(x) for x in sm_ids]))
            sql_response = server_obj.make_query(cr, uid, server_id, check_sql, params={}, select=True, commit=False)
            # check queried transactions are they verified
            for (sm_id, verified) in sql_response:
                logger.info("TRANSACTION FOR MOVE: %s, VERIFIED: %s" % (sm_id, bool(verified)))
                if not bool(verified):
                    ok = False
                    move_obj.write(cr, uid, sm_id, {
                        'status': 'Unverified',
                        'exception_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                        'qty_verified': 0
                    })
                else:
                    sm = move_obj.browse(cr, uid, sm_id)
                    if sm.status == 'Unverified':
                        move_obj.write(cr, uid, sm.id, {'status': 'Verified', 'qty_verified': sm.product_qty})
        return ok

    def verify_manually(self, cr, uid, ids, context=None):
        logger.info("RUN VERIFY MANUALLY, IDS: %s" % ids)
        if not ids:
            return True
        pick = self.browse(cr, uid, ids[0])
        if pick.state not in ['unverified']:
            return True
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_write(uid, 'stock.picking', ids[0], cr)
        #wf_service.trg_validate(uid, 'stock.picking', ids[0], 'button_done', cr)
        #return True
        return self.action_process(cr, uid, ids, context)

    def check_verified(self, cr, uid, ids, context=None):
        logger.info("RUN CHECK_VERIFIED SIGNAL, ids: %s, context: %s" % (ids, context))
        wf_service = netsvc.LocalService("workflow")
        if not self.test_verified(cr, uid, ids, context):
            logger.info("KICK WORKFLOW to unverified")
            wf_service.trg_validate(uid, 'stock.picking', ids[0], 'button_unverified', cr)
            return False
        #else:
            #wf_service.trg_write(uid, 'stock.picking', ids[0], cr)
            #wf_service.trg_validate(uid, 'stock.picking', ids[0], 'button_done', cr)
        return True

    def action_verify_transactions(self, cr, uid, ids, context=None):
        logger.info("RUN ACTION_VERIFY_TRANSACTIONS")
        if context is None:
            context = {}

        self.write(cr, uid, ids, {
            'state': 'unverified',
            }, context=context)

        #return self.create_picking_repot(cr, uid, ids=ids, report_name=None, context=context)
        return True

    def action_assign_wkf(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        super(stock_picking, self).action_assign_wkf(cr, uid, ids, context=context)

        move_obj = self.pool.get('stock.move')

        # The machine
        machine_obj = self.pool.get('machine.settings')
        machine_wh_ids = []
        machine_ids = machine_obj.search(cr, uid, [('wh_ids', '!=', False), ('active', '=', True)])
        machine_partner = {}
        if len(machine_ids) > 0:
            for machine in machine_obj.browse(cr, uid, machine_ids):
                for wh_id in machine.wh_ids:
                    if wh_id.id not in machine_wh_ids:
                        machine_wh_ids.append(wh_id.id)
                        machine_partner[wh_id.id] = [x.id for x in machine.partner_ids]

        for order in self.browse(cr, uid, ids):

            order_vals = {}
            wh_id = self.get_warehouse(cr, uid, order.id)
            if order.move_lines:

                move_obj.write(cr, uid, [x.id for x in order.move_lines], {'status': False})

                if wh_id in machine_wh_ids:
                    if order.sale_id.partner_id.id in machine_partner[wh_id]:
                        self.write(cr, uid, order.id, {'process_by_machine': True})

                    # Demo prep
                    # if len(order.move_lines) == 1:
                    #     move_obj.product_pick(cr, uid, [order.move_lines[0].id], context=context)
            date_expected = order.sale_id and order.sale_id.latest_ship_date_order
            if date_expected and len(date_expected) == 10:
                date_expected = datetime(
                    int(date_expected[:4]),
                    int(date_expected[5:7]),
                    int(date_expected[8:]),
                    18,
                    59,
                    # tzinfo=timezone('US/Eastern'),
                )
            else:
                to_address = order.sale_id.partner_shipping_id
                date_expected = order.date

                if to_address:
                    sql = """SELECT MAX(date_expected)
                        FROM stock_picking p
                            -- to
                            LEFT JOIN sale_order s ON p.sale_id = s.id
                            LEFT JOIN res_partner_address sa ON s.partner_shipping_id = sa.id
                            LEFT JOIN res_country rs1 ON sa.country_id = rs1.id

                            -- from
                            LEFT JOIN res_partner_address pa ON p.ship_from = pa.id
                            LEFT JOIN res_country rs2 ON pa.country_id = rs2.id

                        WHERE p.id <> %s
                            AND sa.id = %s
                            AND rs1.code = 'US' AND rs2.code = 'CA'
                        """ % (order.id, to_address.id)
                    cr.execute(sql)
                    res = cr.fetchone()

                    if res:
                        max_date = res[0]
                        time_format = '%Y-%m-%d %H:%M:%S'
                        if max_date and date_expected <= max_date:
                            if '.' in max_date:
                                time_format += '.%f'
                            date_expected = (datetime.strptime(max_date, time_format) + relativedelta(days=1)).strftime('%Y-%m-%d')
                        else:
                            if '.' in date_expected:
                                time_format += '.%f'
                            date_expected = datetime.strptime(date_expected, time_format)

            order_vals.update({'date_expected': date_expected})

            locations = self.get_locations(cr, uid, [order.id], context=None)[order.id]
            order_vals.update({
                'warehouses': wh_id,
                'locations': locations,
                })
            ship_addrs = self.pool.get('res.partner.address').search(cr, uid, [('warehouse_id', '=', wh_id), ('partner_id', '=', order.sale_id.partner_id.id), ('type', '=', 'invoice')])
            if ship_addrs:
                order_vals.update({
                    'ship_from': ship_addrs[0],
                    })

            self.write(cr, uid, order.id, order_vals)
        return True

    def force_assign(self, cr, uid, ids, *args):
        return True

    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        """ Makes partial picking and moves done.
        @param partial_datas : Dictionary containing details of partial picking
                          like partner_id, address_id, delivery_date,
                          delivery moves with product_id, product_qty, uom
        @return: Dictionary of values
        """
        res = self.do_split(cr, uid, ids, partial_datas, context=context)
        if context is None:
            context = {}
        wf_service = netsvc.LocalService("workflow")

        if not context.get('split_only', False):
            for delivery in res:
                picking_id = res[delivery]['delivered_picking']
                self.action_move(cr, uid, [picking_id])
                self.set_warehouses_locations(cr, uid, picking_id, context=context)
                wf_service.trg_write(uid, 'stock.picking', picking_id, cr)

            order_ids = [res[x]['delivered_picking'] for x in res]
            self.do_remote_order_process(cr, uid, order_ids, context=context)

        return res

    def do_split_not_assigned(self, cr, uid, picking_ids, context=None):
        if context is None:
            context = {}


        for pick in self.browse(cr, uid, picking_ids, context=context):
            if pick.state in ('cancel', 'final_cancel'):
                continue
            split_move_ids = self.pool.get('stock.move').search(cr, uid, [('picking_id', '=', pick.id), '|', ('state', 'not in', ('assigned', 'done')), ('product_qty', '=', 0)])

            self.do_split_move_ids(cr, uid, pick.id, split_move_ids, '', context=context)

        return True

    def do_split_zero(self, cr, uid, picking_ids, context=None):
        if context is None:
            context = {}

        for pick in self.browse(cr, uid, picking_ids, context=context):
            if pick.state in ('cancel', 'final_cancel'):
                continue
            zero_move_ids = []
            for move in pick.move_lines:
                if move.product_qty == 0:
                    zero_move_ids.append(move.id)

            self.do_split_move_ids(cr, uid, pick.id, zero_move_ids, context=context)

        return True

    def is_allow_split(self, cr, uid, ids, context=None):
        if not ids:
            return {}

        if isinstance(ids, (int, long)):
            ids = [ids]

        cr.execute("""
            SELECT sp.id, LOWER(COALESCE(sof."value", 'y')) in ('1', 'true', 'y')
            FROM stock_picking sp
                LEFT JOIN sale_order_fields sof on sp.sale_id = sof.order_id and sof.name = 'split_shipments'
            WHERE sp.id in %(ids)s
        """, {'ids': tuple(ids)})
        res = cr.fetchall() or []
        return {x[0]: x[1] for x in res}

    def do_done(self, cr, uid, pick_id, context=None):
        if not pick_id:
            return False

        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_confirm', cr)
        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_recheck', cr)
        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_reject', cr)
        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_done', cr)
        self.pool.get('stock.picking').write(cr, uid, pick_id, {
            'manual_done_approved': True
        })

        return True

    def do_split_move_ids(self, cr, uid, pick_id, move_ids, action='to_cancel', context=None):
        def _full_revert(context, pattern='flash_revert'):
            return (pattern in context) and all(map(lambda (x, y): x == y, context.get(pattern, {}).items()))

        logger.info("Run do_split_move_ids with action %s and context %s" % (action, context))

        if not (pick_id and move_ids):
            return None

        if not self.is_allow_split(cr, uid, pick_id).get(pick_id):
            return None

        # read data from source picking
        pick = self.read(cr, uid, pick_id, ['move_lines', 'type', 'state', 'tracking_ref', 'origin_carrier', 'is_set'])

        if pick['state'] in ('cancel', 'final_cancel'):
            return pick_id

        # inner intersection of confirmed lines and all lines
        to_split = list(set(move_ids) & set(pick['move_lines'] or []))

        move_obj = self.pool.get('stock.move')
        sequence_obj = self.pool.get('ir.sequence')
        wf_service = netsvc.LocalService("workflow")

        sm_entries = move_obj.browse(cr, uid, to_split)
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)

        for sm_entry in sm_entries:
            original_order_id = (context.get('flash_confirm', {}) or context.get('flash_revert', {})).get(sm_entry.id)
            original_order_record = move_obj.browse(cr, uid, original_order_id)

            # warehouse = original_order_record.warehouse.name
            # original_qty = original_order_record.product_qty + sm_entry.product_qty if not full_revert(context) else 0
            # If full revert, mark original transaction as CXL
            # Else, mark as SLE
            # transaction_code = SIGN_CANCEL_VALUE if full_revert(context) else SIGN_DEFAULT_VALUE

            # get invoice data
            export_ref_ca = (sm_entry.export_ref_ca or None) if not full_revert(context) else None
            export_ref_us = (sm_entry.export_ref_us or None) if not full_revert(context) else None
            fc_invoice = (sm_entry.fc_invoice or None) if not full_revert(context) else None
            fc_order = (sm_entry.fc_order or None) if not full_revert(context) else None
            del_invoice = (sm_entry.del_invoice or None) if not full_revert(context) else None
            del_order = (sm_entry.del_order or None) if not full_revert(context) else None
            bill_of_lading = (sm_entry.bill_of_lading or None) if not full_revert(context) else None

            #customer ref
            customer_ref = sm_entry.sale_line_id.order_id and sm_entry.sale_line_id.order_id.partner_id.ref or None

            if 'flash_confirm' in context and not _full_revert(context, 'flash_confirm'):
                record_info = sm_entry.get_record_info()
                logger.info("Flash confirm, product move params: %s" % record_info)
                unit_price = sm_entry.sale_line_id and sm_entry.sale_line_id.price_unit or 0
                params = (
                    record_info['itemid'],
                    SIGN_DEFAULT_VALUE,
                    record_info['product_qty'],
                    sm_entry.warehouse.name,
                    sm_entry.sale_line_id.name or sm_entry.product_id.name or sm_entry.product_id.description or None,
                    sm_entry.bin,
                    sm_entry.location_id.name,
                    'TRS',  # TransactionType
                    sm_entry.product_id.default_code,
                    -1,
                    (record_info['product_qty'] * -1) or '0',
                    move_obj.get_size_postfix(cr, uid, sm_entry.size_id),
                    sm_entry.invredid,
                    customer_ref,  # CustomerID
                    sm_entry.product_id.default_code,
                    sm_entry.sale_line_id.order_id.po_number,
                    move_obj.get_reserved_flag(cr, uid, record_info['itemid'], sm_entry.warehouse.name, sm_entry.location_id.name, server_id),
                    sm_entry.sale_line_id.id,
                    unit_price,  # unit price from sale_order_line
                )
                tr_context = {
                    'erp_sm_id': sm_entry.id,
                    'export_ref_ca': export_ref_ca,
                    'export_ref_us': export_ref_us,
                    'fc_invoice': fc_invoice,
                    'fc_order': fc_order,
                    'del_invoice': del_invoice,
                    'del_order': del_order,
                    'bill_of_lading': bill_of_lading,
                }
                move_obj.write_into_transactions_table(cr, uid, server_id, params, context=tr_context)

        if len(to_split) == len(pick['move_lines']):
            new_picking = pick_id
        else:
            logger.info("Need to split lines")
            is_set = pick['is_set'] and move_obj.check_isset_lines(cr, uid, to_split)
            if context.get('flash_confirm', {}) or context.get('flash_revert', {}):
                for sm_entry in sm_entries:
                    original_order_id = (context.get('flash_confirm', {}) or context.get('flash_revert', {})).get(
                        sm_entry.id)
                    original_order_record = move_obj.browse(cr, uid, original_order_id)
                    warehouse = original_order_record.warehouse.name
                    original_qty = original_order_record.product_qty + sm_entry.product_qty if not full_revert(
                        context) else 0
                    transaction_code = SIGN_CANCEL_VALUE if full_revert(context) else SIGN_DEFAULT_VALUE
                    fc_invoice = (sm_entry.fc_invoice or None) if not full_revert(context) else None
                    export_ref_ca = (sm_entry.export_ref_ca or None) if not full_revert(context) else None
                    export_ref_us = (sm_entry.export_ref_us or None) if not full_revert(context) else None
                    move_obj.update_transactions_table(cr, uid, server_id, server_obj, original_order_record, warehouse, context=dict(fc_invoice=fc_invoice, export_ref_ca=export_ref_ca, export_ref_us=export_ref_us, transaction_code=transaction_code, original_qty=original_qty, qty=original_order_record.product_qty))

            new_picking = self.copy(
                cr, uid, pick_id,
                {
                    'name': sequence_obj.get(cr, uid, 'stock.picking.%s' % (pick['type'])),
                    'move_lines': [],
                    'state': 'draft',
                    'tracking_ref': context and context.get('tracking_ref', False) or pick['tracking_ref'],
                    'machine_tracking_ref': False,
                    'shp_date': False,
                    'shelves': False,
                    'locations': False,
                    'warehouses': False,
                    'ship_from': False,
                    'backorder_id': pick_id,
                    'invoice_no': False,
                    'carrier_code': context and context.get('origin_carrier', False) or pick['origin_carrier'],
                    'set_parent_move_lines': [],
                    'is_set': is_set
                })
            if action != 'to_done':
                logger.warn("Reverting move lines: %s" % to_split)
                move_obj.revert_assign(cr, uid, to_split, context=context)

            # update move lines
            move_obj.write(cr, uid, to_split, {
                'picking_id': new_picking,
                'fc_invoice': fc_invoice,
                'export_ref_ca': export_ref_ca,
                'export_ref_us': export_ref_us,
            })

            if is_set and new_picking != pick_id:
                if type(move_ids[0]) == int: #split by line or split by qty
                    move_obj.move_parent_setline(cr, uid, pick_id, to_split, new_picking)
                else:
                    move_obj.clone_parent_setline(cr, uid, pick_id, to_split, new_picking, context)

        if action == 'to_cancel':
            wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_cancel', cr)
        else:
            if action == 'to_done':
                self.do_done(cr, uid, new_picking, context)
            else:
                wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)
                wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_recheck', cr)

        return new_picking

    def do_split(self, cr, uid, ids, partial_datas, context=None):

        if context is None:
            context = {}
        else:
            context = dict(context)
        res = {}
        move_obj = self.pool.get('stock.move')
        uom_obj = self.pool.get('product.uom')
        sequence_obj = self.pool.get('ir.sequence')
        wf_service = netsvc.LocalService("workflow")
        for pick in self.browse(cr, uid, ids, context=context):

            new_picking = None
            split_set = False
            complete, too_many, too_few = [], [], []
            move_product_qty, prodlot_ids, partial_qty, product_uoms = {}, {}, {}, {}

            sets_in_picking, sets_in_split = {}, {}
            sets_in_picking_p = {}
            set_components_pids = {}
            for line in pick.set_parent_move_lines:
                components = self.pool.get('product.product').browse(cr, uid, line.product_id.id).set_component_ids
                set_components_pids[line.product_id.id] = [x.cmp_product_id.id for x in components]
                set_components_pids[line.product_id.id].append(line.product_id.id)

                prodlot_ids[line.id] = False
                product_uoms[line.id] = 1
                partial_qty[line.id] = uom_obj._compute_qty(cr, uid, product_uoms[line.id], line.product_qty, line.product_uom.id)

            set_moves = filter(lambda x: x.is_set, pick.move_lines)
            if set_moves:
                for move in set_moves:
                    if not sets_in_picking.get(move.set_product_id.id):
                        sets_in_picking[move.set_product_id.id] = []
                    sets_in_picking[move.set_product_id.id].append(move.id)
                set_parent_moves = filter(lambda x: x.is_set, pick.set_parent_move_lines)
                if set_parent_moves:
                    import copy
                    sets_in_picking_p = copy.deepcopy(sets_in_picking)
                    for move in set_parent_moves:
                        if not sets_in_picking_p.get(move.set_product_id.id):
                            sets_in_picking_p[move.set_product_id.id] = []
                        sets_in_picking_p[move.set_product_id.id].append(move.id)

            componets_prods_list = []
            for move in pick.move_lines:
                if move.state in ('done') and not context.get('please_split', False):
                    continue
                partial_data = partial_datas.get('move%s' % (move.id), {})
                product_qty = partial_data.get('product_qty', 0.0)
                move_product_qty[move.id] = product_qty
                product_uom = partial_data.get('product_uom', False)
                prodlot_id = partial_data.get('prodlot_id')
                prodlot_ids[move.id] = prodlot_id
                product_uoms[move.id] = product_uom
                partial_qty[move.id] = uom_obj._compute_qty(cr, uid, product_uoms[move.id], product_qty, move.product_uom.id)

                if (move.is_set_component and partial_data) or (
                        move.is_set and move.set_product_id.id == move.product_id.id):
                    if not sets_in_split.get(move.set_product_id.id):
                        sets_in_split[move.set_product_id.id] = []
                    sets_in_split[move.set_product_id.id].append(move.id)

                if move.product_qty == partial_qty[move.id]:
                    complete.append(move)
                elif move.product_qty > partial_qty[move.id]:
                    if move.is_set:
                        for set_prods_list in sets_in_picking:
                            if move.id in sets_in_picking_p.get(set_prods_list):
                                componets_prods_list += sets_in_picking_p.get(set_prods_list)
                    else:
                        too_few.append(move)
                else:
                    too_many.append(move)

            #@todo: fix bug split sets by various sizes
            if len(componets_prods_list):
                for move in pick.move_lines + pick.set_parent_move_lines:
                    if move.is_set and move.id in componets_prods_list and move not in too_few:
                        too_few.append(move)

            for set_product in sets_in_picking:
                if set_product in sets_in_split:
                    if sets_in_picking[set_product] != sets_in_split[set_product]:
                        sets_in_split[set_product] = []

            if split_set:
                raise osv.except_osv(_('Warning!'), "Can't split {order}. Cant's split set items.".format(order=pick.name))

            if too_many:
                logger.error("Can't split {order}. Wrong qty for new part.".format(order=pick.name))
                continue

            if len(too_few) == len(pick.move_lines + pick.set_parent_move_lines):
                continue

            if too_few and not new_picking:
                if not self.is_allow_split(cr, uid, pick.id).get(pick.id):
                    raise osv.except_osv(_('Warning!'), 'You can\'t split this order! Please, check sale order settings.')

                new_picking = self.copy(cr, uid, pick.id, {
                    'name': sequence_obj.get(cr, uid, 'stock.picking.%s' % (pick.type)),
                    'move_lines': [],
                    'state': 'draft',
                    'tracking_ref': False,
                    'machine_tracking_ref': False,
                    'shp_date': False,
                    'shelves': False,
                    'locations': False,
                    'warehouses': False,
                    'ship_from': False,
                    'invoice_no': False,
                    'set_parent_move_lines': [],
                })

            # At first we confirm the new picking (if necessary)
            if new_picking:
                delivered_pack_id = new_picking
                self.write(cr, uid, [pick.id], {'backorder_id': new_picking})

                move_obj.write(cr, uid, [move.id for move in too_few], {
                    'picking_id': new_picking,
                })
                if pick.is_set:
                    for move in too_few:
                        if move.set_parent_order_id.id:
                            move_obj.write(cr, uid, [move.id], {
                                'picking_id': '',
                                'set_parent_order_id': new_picking,
                            })

                self.recalculate_stored_fields(cr, uid, [pick.id, new_picking], context=context)
                wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_recheck', cr)

            else:
                delivered_pack_id = pick.id

            delivered_pack = self.browse(cr, uid, delivered_pack_id, context=context)
            res[pick.id] = {'delivered_picking': delivered_pack.id or False}

        return res

    def do_reprocess(self, cr, uid, picking_id, partial_datas, context=None):
        picking = self.browse(cr, uid, picking_id, context=context)
        if not partial_datas or not picking.move_lines:
            return False
        default_move_id = picking.move_lines[0].id
        # Replacement parts is similar to warranty
        if partial_datas['type'] == 'R':
            partial_datas['type'] = 'W'
        name_pattern = "%s-%s{iter}" % (picking.name, partial_datas['type'])
        new_name = self.find_unused_name(cr, uid, name_pattern, start=0, end=1000)
        user = self.pool.get('res.users').browse(cr, uid, uid)
        history = '%s %s: Did reprocess for %s order' % (
            time.strftime('%m-%d %H:%M:%S', time.gmtime()),
            user.name or '',
            picking.name,
        )

        picking_data = self.pool.get('sale.order')._prepare_order_picking(cr, uid, picking.sale_id)

        is_order_confirmed = (partial_datas['type'] == 'W')
        need_invoice = (partial_datas['type'] not in ['W', 'L'])
        now_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        picking_data.update({
            'name': new_name,
            'note': False,
            'report_history': history,
            'date': now_time,
            'sale_integration_ship_confirmed': is_order_confirmed,
            'sale_integration_invoice_send': not need_invoice,
            'order_params': [
                (0, 0, {'name': 'skip_customer_invoice', 'value': '0' if need_invoice else '1'}),
                (0, 0, {'name': 'skip_ms_sales', 'value': '1'}),
                (0, 0, {'name': 'skip_ms_invoice', 'value': '1'}),
            ],
            'invoice_no': picking.invoice_no,
            'is_set': True if picking.is_set and len(partial_datas['set_parent_order_lines']) else False
        })

        new_picking = self.create(cr, uid, picking_data)

        move_obj = self.pool.get('stock.move')
        wf_service = netsvc.LocalService("workflow")
        picking_warehouse = picking.sale_id.shop_id.warehouse_id
        for value in partial_datas['lines']:
            move_id = value['move_id'] or default_move_id
            move_defaults = {
                'product_id': value['product_id'],
                'size_id': value['size_id'],
                'product_qty': value['product_qty'],
                'product_uos_qty': value['product_qty'],
                'picking_id': new_picking,
                'state': 'draft',
                'move_dest_id': False,
                'bin': False,
                'bin_id': False,
                'status': False,
                'location_id': picking_warehouse.lot_stock_id.id,
                'location_dest_id': picking_warehouse.lot_output_id.id,
                'old_location_id': False,
                'date_expected': now_time,
                'set_parent_order_id': '',
                'bill_of_lading': '',
                'export_ref_ca': '',
                'export_ref_us': '',
                'fc_invoice': '',
                'fc_order': '',
                'del_invoice': '',
                'del_order': '',
            }
            move_obj.copy(cr, uid, move_id, move_defaults)

        if picking.is_set and partial_datas['set_parent_order_lines']:
            move_defaults.update({
                'picking_id': '',
                'set_parent_order_id': new_picking
            })
            for value in partial_datas['set_parent_order_lines']:
                move_obj.copy(cr, uid, value['move_id'] or default_move_id, move_defaults)

        wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)

        return True

    def create_invredid(self, cr, uid, wh, stockid, default_code, size, bin_name):
        invredid = False
        if not bin_name:
            bin_name = None

        if bin_name and len(bin_name) > 50:
            bin_name = bin_name[:50]

        if wh and default_code and stockid and size:
            server_data = self.pool.get('fetchdb.server')

            server_id = server_data.get_sale_servers(cr, uid, single=True)
            if server_id:
                # Check exist
                query = """ SELECT coalesce(id, invredid)
                    FROM dbo.prg_invredid
                    WHERE
                        warehouse=?
                        AND stockid=?
                        AND id_delmar=?
                        AND size=?
                """
                params = [wh, stockid, default_code, size]
                res = server_data.make_query(cr, uid, server_id, query, params=params, select=True, commit=False)
                if res:
                    invredid = res[0][0] or False
                if not invredid:
                    user_obj = self.pool.get('res.users')
                    user = user_obj.browse(cr, uid, uid)

                    insert_values = {
                        'warehouse': wh,
                        'stockid': stockid,
                        'id_delmar': default_code,
                        'size': size,
                        'bin': bin_name,
                        'source': 'ERP',
                        'username': user.name,
                    }
                    res = server_data.insert_record(
                        cr, uid, server_id,
                        'dbo.prg_invredid', insert_values,
                        select=False, commit=True
                    )

                    if res:
                        query = """ SELECT max(invredid)
                            FROM dbo.prg_invredid
                            WHERE
                                warehouse=?
                                AND stockid=?
                                AND id_delmar=?
                                AND size=?
                                {bin_str}
                        """
                        params = [wh, stockid, default_code, size]
                        bin_str = ''
                        if bin_name:
                            bin_str = 'AND bin=?'
                            params.append(bin_name)
                        query = query.format(bin_str=bin_str)
                        res = server_data.make_query(
                            cr, uid, server_id,
                            query, params=params,
                            select=True, commit=False
                        )
                        if res:
                            invredid = res[0][0] or False

                    else:
                        raise osv.except_osv(_('Warning!'), _('MSSQL connect error!'))
            else:
                raise osv.except_osv(_('Warning!'), _('MSSQL connect error!'))

        else:
            raise osv.except_osv(_('Warning!'), _('Not all params are specified!'))

        return invredid

    def fill_return_product_history(self, cr, uid, ids, context=None, partial_datas=None):
        if context is None:
            context = {}
        if partial_datas is None:
            partial_datas = {}
        if ids:
            if isinstance(ids, list):
                ids = ids[0]
            pick = self.pool.get('stock.picking').browse(cr, uid, ids) or False
            if pick:
                moves = pick.move_lines or []
                for move in moves:
                    line = partial_datas.get('move%s' % (move.id), False)
                    if line and move and line.get('product_id', False):
                        self.pool.get('return.product.history').create(cr, uid, {
                            'returned_location_id': line.get('location_id', False),
                            'bin_id': line.get('bin_id', False),
                            'picking_id': pick.id,
                            'product_id': line.get('product_id', False),
                            'size_id': (move.size_id and move.size_id.id) or False,
                            'sale_id': (pick.sale_id and pick.sale_id.id) or False,
                            'qty': (line.get('product_qty', False) or move.product_qty) or False,
                            'reason': line.get('last_return_reason', False),
                        }, context=context)
                        # DLMR-1775
                        # Save set lines
                        if move.is_set and move.product_id.id == move.set_product_id.id:
                            set_move_ids = self.pool.get('stock.move').search(cr, uid, [('set_parent_order_id', '=', move.picking_id.id), ('set_product_id', '=', move.product_id.id)])
                            for set_line in self.pool.get('stock.move').browse(cr, uid, set_move_ids):
                                self.pool.get('return.product.history').create(cr, uid, {
                                    'returned_location_id': set_line.location_id.id or False,
                                    'bin_id': set_line.bin_id.id or False,
                                    'picking_id': pick.id,
                                    'product_id': set_line.product_id.id,
                                    'size_id': (set_line.size_id and set_line.size_id.id) or False,
                                    'sale_id': (pick.sale_id and pick.sale_id.id) or False,
                                    'qty': set_line.product_qty or move.product_qty or False,
                                    'reason': line.get('last_return_reason', False),
                                    'set_component': True,
                                }, context=context)

        return True

    def find_unused_name(self, cr, uid, pattern, start=None, end=None, context=None):
        if not pattern or '{iter}' not in pattern:
            return False

        i = start or 0
        if not end:
            end = 1000

        while i <= end:
            name = pattern.format(iter=i or '')
            if not self.search(cr, uid, [('name', '=', name)]):
                return name
            i += 1
        return False

    def do_only_return(self, cr, uid, ids, partial_datas, context=None):
        logger.warn("Run Stock.Picking -> do_only_return")

        if context is None:
            context = {}
        else:
            context = dict(context)
        context.update({'do_only_return': True})
        res = {}
        new_picking = None
        partial_line_obj = self.pool.get('stock.partial.picking.line')
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        uom_obj = self.pool.get('product.uom')
        # wh_obj = self.pool.get('stock.warehouse')
        server_data = self.pool.get('fetchdb.server')
        helper = self.pool.get('stock.picking.helper')
        _order_history = self.pool.get("order.history")
        sale_server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not sale_server_id:
            raise osv.except_osv(_('Warning!'), _('Sale server is not available!'))

        invoice_server_id = server_data.get_invoice_servers(cr, uid, single=True)
        if not invoice_server_id:
            raise osv.except_osv(_('Warning!'), _('Invoice server is not available!'))

        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), _('Can not do the stock update. Server with TRANSACTIONS table is not available!'))

        forbidden_for_return = {}
        stock_picking_orders = self.browse(cr, uid, ids, context=context)
        for pick in stock_picking_orders:

            customer = pick.real_partner_id
            customer_product = ProductFactory.factory(customer.name, cr, uid, self.pool)

            not_returned_product = set()
            for product_line in pick.move_lines:

                can_returning = customer_product.check_that_can_returning(
                    product_line.product_id,
                    customer,
                    product_line.size_id
                )
                product_name = product_line.product_id.default_code
                if not can_returning:
                    not_returned_product.add(product_name)

            if not_returned_product:
                if forbidden_for_return.get(customer.name):
                    forbidden_for_return[customer.name] = forbidden_for_return[customer.name] | not_returned_product
                else:
                    forbidden_for_return[customer.name] = not_returned_product
        if forbidden_for_return:
            error_message = ''
            for key, value in forbidden_for_return.items():
                products = ' '.join(value)
                'These products ({}) are not allowed to return for this customer - {} \n'.format(products, key)
            raise osv.except_osv(
                _('Warning!'),
                _('Return for this items is not allowed! \n' + error_message)
            )

        return_line_ids = []
        tr_ids = []
        for pick in stock_picking_orders:
            new_picking = None
            complete, too_many, too_few = [], [], []
            move_product_qty, prodlot_ids, partial_qty, product_uoms = {}, {}, {}, {}
            return_note = ''

            for move in pick.move_lines:

                partial_data = partial_datas.get('move%s' % (move.id), {})
                logger.warn('MOVE Partial data: %s' % partial_data)
                if (partial_data):
                    return_line_ids.append(move.id)
                else:
                    continue

                product_qty = partial_data.get('product_qty', 0.0)
                move_product_qty[move.id] = product_qty
                product_uom = partial_data.get('product_uom', False)
                prodlot_id = partial_data.get('prodlot_id')
                prodlot_ids[move.id] = prodlot_id
                product_uoms[move.id] = product_uom
                partial_qty[move.id] = uom_obj._compute_qty(cr, uid, product_uoms[move.id], product_qty, move.product_uom.id)
                move.return_location_id = partial_data.get('location_id')
                move.is_set = partial_data.get('is_set', False)

                move.return_partner_reason = partial_data.get('return_reason_id', False)

                if partial_data.get('location_id', False):
                    loc = loc_obj.browse(cr, uid, partial_data['location_id'])
                    move.return_warehouse = move_obj.get_warehouse_by_move_location(cr, uid, loc.id)
                    move.return_stockid = loc.name
                # else:
                #     wh_id = loc_obj.get_warehouse(cr, uid, move.location_id and move.location_id.id or False)
                #     move.return_warehouse = wh_obj.browse(cr, uid, wh_id).name if wh_id else False
                #     move.return_stockid = 'RETURN'

                if partial_data.get('location_id', False) and partial_data.get('bin_id', False):
                    move.return_bin = bin_obj.browse(cr, uid, partial_data['bin_id']).name
                    move.return_invredid = 1 if move.return_bin.upper() == 'RETURN' else move_obj.get_invredid_for_stock_move(cr, uid, False, move.return_bin, loc.id, move.product_id.id, move.size_id, ignore_bin=True)
                else:
                    move.return_bin = 'RETURN'
                    move.return_invredid = 1

                if move.return_stockid != 'MNTLRETURN' and move.return_bin == 'RETURN' and not partial_data.get('generate_bin'):
                    move.return_stockid = 'RETURN'

                if not move.return_warehouse or not move.return_stockid or not move.return_bin:
                    raise osv.except_osv(_('Warning!'), _('Can not do return product. Please select correct location!'))

                if not move.return_invredid:
                    # Create new invredid in prg_invredid in MSSQL
                    prg_wh = loc.warehouse_id.name or ''
                    prg_stockid = loc.name or ''
                    prg_default_code = move.product_id.default_code or ''
                    prg_size = self.pool.get('stock.move').get_size_postfix(cr, uid, move.size_id)
                    prg_bin = move.return_bin or ''
                    new_invredid = self.create_invredid(cr, uid, prg_wh, prg_stockid, prg_default_code, prg_size, prg_bin)
                    move.return_invredid = str(new_invredid)
                    # raise osv.except_osv(_('Warning!'), _('Can not do return product. Can not find invredid for item %s on bin %s!' % (move.product_id and move.product_id.default_code or "", move.return_bin or "")))

                # dlmr-866
                move.generate_bin = False
                if partial_data.get('generate_bin', False):
                    move.generate_bin = True

                partial_line_obj.write(
                    cr, uid,
                    partial_data.get('line_id', []), {
                        'location': "%s/%s/%s" % (move.return_warehouse, move.return_stockid, move.return_bin)
                    }
                )

                if move.product_qty == partial_qty[move.id]:
                    complete.append(move)
                elif move.product_qty > partial_qty[move.id]:
                    too_few.append(move)
                else:
                    too_many.append(move)

            if too_many:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            order_line_item_no = 0

            # DLMR-133
            # check if it was a W/L order
            wl_order = re.match('^(.+-\d+-(W|L|W\d+|L\d+))$', pick.name)

            # name unique conflicts resolving for multi-lines orders
            new_name = self.find_unused_name(cr, uid, pick.name + "R{iter}", start=0, end=1000)

            if not new_name:
                raise osv.except_osv(_('Warning!'), _("Can't generate unique name for returned order! Please, create new batch"))

            if too_few or complete:
                remote_status = True
                order_line_item_no = 0
                order_cost = 0
                order_qty = 0
                shipping_handling = pick.shp_handling

                # new invoice ID generation
                # check if it isn't WL order
                if not wl_order:
                    ret_invoice = self.pool.get('ir.sequence').next_by_code(cr, uid, 'stock.picking.return.invoice')

                settings_id = self.pool.get('sale.integration').search(cr, uid, [('customer_ids', '=', pick.sale_id.partner_id.id)])
                tbl_prefix = ''
                if (len(settings_id) > 0):
                    settings_obj = self.pool.get('sale.integration').browse(cr, uid, settings_id)

                    if settings_obj[0].type_api == 'overstock':
                        tbl_prefix = 'OS_'

                if len(complete) != len(pick.move_lines):

                    data_for_new_picking = {
                        'new_name': new_name,
                        'move_lines': [],
                        'set_parent_move_lines': [],
                        'state': 'draft',
                        'shp_date': False,
                        'shelves': False,
                        'locations': False,
                        'warehouses': False,
                        'ship_from': False,
                        'carrier_code': False,
                        'shp_code': False,
                        'shp_service': False,
                        'shp_account': False,
                        'shp_account_country': False,
                        'billing_method': False,
                        'shp_signature': False,
                        'shipping_batch_id': False,
                        'combined': False,
                        'is_set': pick.is_set
                    }

                    new_picking = self.copy(cr, uid, pick.id, data_for_new_picking)

                for move in too_few + complete:
                    logger.info('Returning stock move: %s' % move.id)
                    order_line_item_no += 1

                    qty = move_product_qty[move.id]
                    if not qty:
                        continue

                    tr_id = False

                    # TRANSACTIONS FOR SETS
                    if move.is_set:
                        logger.info("Move is a set: %s" % move.id)
                        if context.get('from_mass_return', False):
                            logger.info('Started from mass return, skipping transactions')
                            remote_status = True
                        else:
                            # get components list
                            cmp_list = helper.get_set_product_components(cr, uid, product_id=move.product_id.id, picking_id=pick.id)
                            for comp in cmp_list:
                                delmarid = comp.get('default_code', False)
                                description = comp.get('product_description', False)
                                if comp.get('sizeable'):
                                    size_postfix = move_obj.get_size_postfix(cr, uid, move.size_id)
                                else:
                                    size_postfix = '0000'
                                itemid = delmarid and (delmarid + '/' + size_postfix) or False
                                sign = 1
                                reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, move.return_warehouse, move.return_stockid, server_id)
                                cmp_qty = qty * helper.get_component_set_qty(cr, uid, comp.get('product_id', False), move.product_id.id)
                                # dlmr-866 is need to generate bin
                                if move.generate_bin:
                                    gen_bin_id = helper.generate_bin(cr, uid, product_id=comp.get('product_id', False), size_id=move.size_id.id, location_id=move.return_location_id)
                                    if gen_bin_id:
                                        move.return_bin = bin_obj.browse(cr, uid, gen_bin_id).name
                                # fill transactions for components
                                unit_price = move.sale_line_id and move.sale_line_id.price_unit or 0
                                customer_ref = move.sale_line_id.order_id and move.sale_line_id.order_id.partner_id.ref or None
                                params = (
                                    itemid or 'null',  # itemid
                                    SIGN_RETURN_VALUE,  # TransactionCode
                                    cmp_qty,  # qty
                                    move.return_warehouse or 'null',  # warehouse
                                    description or 'null',  # itemdescription
                                    move.return_bin,  # move.bin or 'null',  # bin
                                    move.return_stockid,  # stockid
                                    'TRS',  # TransactionType
                                    delmarid,  # id_delmar
                                    sign,  # sign
                                    (cmp_qty * sign) or '0',  # adjusted_qty
                                    size_postfix,  # size
                                    move.return_invredid,  # move.invredid or 'null',  # invredid
                                    customer_ref,  # CustomerID
                                    delmarid,  # ProductCode
                                    pick and pick.sale_id and pick.sale_id.po_number or 'no_invoiceno',
                                    # 'ADJDEF',  # InvoiceNo
                                    reserved_flag or 0,  # reserved
                                    move.sale_line_id.id or None,  # trans_tag
                                    unit_price * (-sign),  # unit price from sale_order_line
                                )
                                # dlmr-866 - write notes to order
                                iloc = loc_obj.browse(cr, uid, move.return_location_id)
                                return_note += '%s %s/%s\ninto %s / %s\n' % (
                                    delmarid,
                                    size_postfix,
                                    cmp_qty,
                                    iloc.complete_name,
                                    move.return_bin
                                )
                                tr_context = {
                                    #'erp_sm_id': move.id, # no way to define set line id :(
                                    'export_ref_us': '',
                                    'export_ref_ca': '',  # move.export_ref,
                                    'fc_invoice': '',  # move.fc_invoice,
                                    'fc_order': '',  # move.fc_order,
                                    'del_invoice': '',  # move.del_invoice,
                                    'del_order': '',  # move.del_order
                                    'bill_of_lading': '',
                                }
                                tr_id = move_obj.write_into_transactions_table(cr, uid, server_id, params, context=tr_context)
                                if tr_id:
                                    tr_ids.append(tr_id)
                                    # GET SOURCE SET_COMPONENT LINE
                                    hidden_moves_ids = move_obj.search(cr, uid, [('set_parent_order_id', '=', pick.id), ('product_id', '=', comp.get('product_id', False)), ('set_product_id', '=', move.product_id.id)])
                                    if len(hidden_moves_ids) > 0:
                                        hidden_src_moves = move_obj.browse(cr, uid, hidden_moves_ids)
                                        for src_move in hidden_src_moves:
                                            logger.warn('Found hidden component move for copying: %s' % src_move.id)
                                            oh_id = _order_history.search(cr, uid, [('sm_id', '=', src_move.id)])
                                            if new_picking:
                                                # return full component qty
                                                if src_move.product_qty == cmp_qty:
                                                    move_obj.write(cr, uid, [src_move.id], {
                                                        'set_parent_order_id': new_picking,
                                                        'state': 'draft',
                                                        'return_partner_reason': move.return_partner_reason
                                                    })


                                                    # _order_history.write(cr, uid, oh_id,{'do_name': data_for_new_picking.get('new_name')})
                                                    # # 'parent_sale_order': new_picking,
                                                    cr.execute(
                                                        """update order_history set do_name=%s where id = %s""",
                                                        (new_name, oh_id[0],))
                                                else:
                                                    # CREATE NEW STOCK.MOVE lines
                                                    new_move = move_obj.copy(cr, uid, src_move.id, {
                                                        'product_qty': cmp_qty,
                                                        'product_uos_qty': cmp_qty,
                                                        'picking_id': False,
                                                        'move_dest_id': False,
                                                        'bin': False,
                                                        'bin_id': False,
                                                        'export_ref_us': None,
                                                        'export_ref_ca': None,
                                                        'fc_invoice': None,
                                                        'fc_order': None,
                                                        'del_invoice': None,
                                                        'del_order': None,
                                                        'bill_of_lading': None,
                                                        'status': False,
                                                        'state': 'draft',
                                                        'set_parent_order_id': new_picking
                                                    })
                                                    if new_move:
                                                        move_obj.write(cr, uid, [src_move.id], {
                                                            'product_qty': src_move.product_qty - cmp_qty,
                                                            'product_uos_qty': src_move.product_qty - cmp_qty,
                                                            'return_partner_reason': move.return_partner_reason
                                                        })
                                            else:
                                                # update old component lines
                                                move_obj.write(cr, uid, [src_move.id], {
                                                    'state': 'draft',
                                                    'return_partner_reason': move.return_partner_reason
                                                })
                                                cr.execute(
                                                    """update order_history set do_name=%s where id = %s""",
                                                    (new_name, oh_id[0],))

                                else:
                                    remote_status = False
                                    break


                    # TRANSACTIONS AND SALES FOR COMMON PRODUCT
                    else:
                        logger.warn("Move is a common product: %s" % move.id)
                        delmarid = move.product_id and move.product_id.default_code or False
                        description = move.product_id and move.product_id.description
                        size_postfix = move_obj.get_size_postfix(cr, uid, move.size_id)
                        itemid = delmarid and (delmarid + '/' + size_postfix) or False
                        sign = 1
                        reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, move.return_warehouse, move.return_stockid, server_id)
                        if move.generate_bin:
                            ret_bin_id = helper.generate_bin(cr, uid, product_id=move.product_id.id,
                                                                  size_id=move.size_id.id,
                                                                  location_id=move.return_location_id)
                            if ret_bin_id:
                                move.return_bin = bin_obj.browse(cr, uid, ret_bin_id).name

                        bin_shorter_name = move.return_bin[:50] if len(move.return_bin) > 50 else move.return_bin
                        unit_price = move.sale_line_id and move.sale_line_id.price_unit or 0
                        customer_ref = move.sale_line_id.order_id and move.sale_line_id.order_id.partner_id.ref or None
                        params = (
                            itemid or 'null',  # itemid
                            SIGN_RETURN_VALUE,  # TransactionCode
                            qty,  # qty
                            move.return_warehouse or 'null',  # warehouse
                            description or 'null',  # itemdescription
                            bin_shorter_name,  # move.bin or 'null',  # bin
                            move.return_stockid,  # stockid
                            'TRS',  # TransactionType
                            delmarid,  # id_delmar
                            sign,  # sign
                            (qty * sign) or '0',  # adjusted_qty
                            size_postfix,  # size
                            move.return_invredid,  # move.invredid or 'null',  # invredid
                            customer_ref,  # CustomerID
                            delmarid,  # ProductCode
                            pick and pick.sale_id and pick.sale_id.po_number or 'no_invoiceno',
                            # 'ADJDEF',  # InvoiceNo
                            reserved_flag or 0,  # reserved
                            move.sale_line_id.id or None,  # trans_tag
                            unit_price * (-sign),  # unit price from sale_order_line
                        )

                        # dlmr-866 - write notes to order
                        iloc = loc_obj.browse(cr, uid, move.return_location_id)
                        return_note += '%s %s/%s\ninto %s / %s\n' % (
                            delmarid,
                            size_postfix,
                            qty,
                            iloc.complete_name,
                            bin_shorter_name
                        )

                        remote_status = True
                        tr_context = {
                            'erp_sm_id': move.id,
                            'export_ref_us': '',
                            'export_ref_ca': '',  # move.export_ref,
                            'fc_invoice': '',  # move.fc_invoice,
                            'fc_order': '',  # move.fc_order,
                            'del_invoice': '',  # move.del_invoice,
                            'del_order': '',  # move.del_order
                            'bill_of_lading': '',  # move.bill_of_lading
                        }
                        tr_id = move_obj.write_into_transactions_table(cr, uid, server_id, params, context=tr_context)
                        if tr_id:
                            tr_ids.append(tr_id)
                        else:
                            remote_status = False
                            break

                    # DLMR-133
                    # and not wl_order
                    if sale_server_id and server_id and not wl_order:
                        line = move
                        remote_status &= self.pool.get('stock.picking').fill_OS_Sales_tables(cr, uid, line, ret_invoice, server_id, sale_server_id, order_line_item_no, partial_qty[move.id])
                        if not remote_status:
                            logger.warn("OS SALES TABLES weren't filled!")

                    order_cost += float(move.sale_line_id.price_unit) * float(move.product_qty)
                    total_sale_line_cost = float(move.sale_line_id.price_unit) * float(move.product_qty)
                    order_qty += float(move.product_qty)

                    # DLMR-133
                    # and not wl_order
                    if invoice_server_id and remote_status and not wl_order:
                        remote_status &= self.fill_invoice_detail(cr, uid, move, tbl_prefix, ret_invoice, invoice_server_id, total_sale_line_cost, order_line_item_no, direct=0, return_qty=partial_qty[move.id])
                        if not remote_status:
                            logger.warn("INVOICE DETAILS table wasn't filled!")

                # DLMR-133
                # and not wl_order
                if remote_status and invoice_server_id and remote_status and not wl_order:
                    remote_status &= self.fill_invoice_header(cr, uid, shipping_handling, pick, False, invoice_server_id, tbl_prefix, ret_invoice, order_qty, order_line_item_no, order_cost, direct=0, return_qties=partial_qty)
                    if not remote_status:
                        logger.warn("INVOICE HEADER table wasn't filled!")

                if not remote_status:
                    if len(tr_ids) > 0:
                        move_obj.delete_from_transactions_table(cr, uid, server_id, tr_ids)
                    self.remove_OS_Sales_BU(cr, uid, pick, ret_invoice, sale_server_id, return_flag=True)
                    self.remove_OS_Sales(cr, uid, pick, ret_invoice, sale_server_id)
                    self.remove_invoice_detail(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, ret_invoice)
                    self.remove_invoice_header(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, ret_invoice, direct=0)
                    raise osv.except_osv(_('Warning!'), _('Something went wrong when creating TR or INV or BU.'))

            for move in too_few:
                product_qty = move_product_qty[move.id]
                if product_qty != 0:
                    defaults = {
                        'product_qty': product_qty,
                        'product_uos_qty': product_qty,
                        'picking_id': new_picking,
                        'state': 'draft',
                        'move_dest_id': False,
                        'price_unit': move.price_unit,
                        'product_uom': product_uoms[move.id],
                        'bin': False,
                        'bin_id': False,
                        'export_ref_us': None,
                        'export_ref_ca': None,
                        'fc_invoice': None,
                        'fc_order': None,
                        'del_invoice': None,
                        'del_order': None,
                        'bill_of_lading': None,
                        'status': False
                    }
                    prodlot_id = prodlot_ids[move.id]
                    if prodlot_id:
                        defaults.update(prodlot_id=prodlot_id)
                    new_move_id = move_obj.copy(cr, uid, move.id, defaults)
                    # DLMR-1266 - fix partial data for new move
                    ret_move_partial = 'move%s' % new_move_id
                    partial_datas.update({ret_move_partial: partial_datas.get('move%s' % move.id, {})})
                    # END DLMR-1266

                move_obj.write(cr, uid, [move.id],
                               {
                                   'product_qty': move.product_qty - partial_qty[move.id],
                                   'product_uos_qty': move.product_qty - partial_qty[move.id],
                                   'return_partner_reason': move.return_partner_reason
                                   })

            for move in complete:
                defaults = {
                    'state': 'draft',
                    'product_uom': product_uoms[move.id],
                    'product_qty': move_product_qty[move.id],
                    'return_partner_reason': move.return_partner_reason
                    }
                if prodlot_ids.get(move.id):
                    defaults.update({'prodlot_id': prodlot_ids[move.id]})

                if new_picking:
                    defaults.update({'picking_id': new_picking})

                move_obj.write(cr, uid, [move.id], defaults, context=context)

            if new_picking:
                returned_id = new_picking
                self.write(cr, uid, [pick.id], {'backorder_id': new_picking})
                up_data = {
                    'state': 'returned',
                    'ret_invoice_no': '',
                    'ret_date': datetime.utcnow()
                }
                if wl_order:
                    up_data.update({'ret_invoice_no': pick.invoice_no})
                else:
                    up_data.update({'ret_invoice_no': ret_invoice})
                self.write(cr, uid, new_picking, up_data)

            else:
                returned_id = pick.id
                up_data = {
                    'state': 'returned',
                    'name': new_name,
                    'ret_invoice_no': '',
                    'ret_date': datetime.utcnow(),
                }
                if wl_order:
                    up_data.update({'ret_invoice_no': pick.invoice_no})
                else:
                    up_data.update({'ret_invoice_no': ret_invoice})
                self.write(cr, uid, [pick.id], up_data)

            res[pick.id] = {'returned_picking': returned_id or False, 'return_note': return_note}

            # DLMR-133
            if not wl_order and not (settings_obj[0].type_api in ['commercehub','hsn'] and (
                context.get('from_mass_return', False) or context.get('from_batch_return', False))):

                self.pool.get('sale.integration').integrationApiReturnDeliveryOrder(cr, uid, return_line_ids)

        # Save origin product before return
        # If partial return then write new picking id into return_product_history table
        self.fill_return_product_history(cr, uid, new_picking or ids, context, partial_datas)

        return res


    def do_remote_return(self, cr, uid, ids, context=None):
        """
        Special function for resending return confirmations via XML-RPC.
        """
        return_line_ids = []
        for pick in self.browse(cr, uid, ids, context=context):
            for move in pick.move_lines:
                return_line_ids.append(move.id)
            self.pool.get('sale.integration').integrationApiReturnDeliveryOrder(cr, uid, return_line_ids)
        return True

    def revert_assign(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        for pick in self.browse(cr, uid, ids):
            done_ids = [x.id for x in pick.move_lines if x.state == 'done']
            if done_ids:
                raise osv.except_osv(_('Warning !'), _('Order has done moves. Cannot back these.'))

            move_ids = [x.id for x in pick.move_lines if x.state in ('assigned', 'reserved')] or []
            self.pool.get('stock.move').revert_assign(cr, uid, move_ids, context=context)
            self.pool.get('stock.move').unset_bin(cr, uid, move_ids, context=context)
            warehouse_id = self.get_warehouse(cr, uid, pick.id, context=None)
            locations = self.get_locations(cr, uid, [pick.id], context=None)[pick.id]
            time_format = '%Y-%m-%d %H:%M:%S'
            if '.' in pick.date:
                time_format += '.%f'
            self.write(cr, uid, pick.id, {
                'date_expected': datetime.strptime(pick.date, time_format),
                'warehouses': warehouse_id,
                'locations': locations,
            })
        return True

    def action_back(self, cr, uid, ids, *args):
        context = dict(sign=SIGN_CANCEL, transaction_code=SIGN_CANCEL_VALUE, fc_invoice=None, export_ref_ca=None, export_ref_us=None)
        self.action_update_transactions(cr, uid, ids, context)
        if self.revert_assign(cr, uid, ids, context):
            self.reset_shipping_rule(cr, uid, ids, vals={
                'state': 'back',
                'process_by_item': False,
                'process_by_machine': False
                })

            # Demo prep
            context.update({'product_pick_hard_remove': True})
            self.product_pick_remove(cr, uid, ids, context=context)

            return True
        return False

    def action_waiting_split(self, cr, uid, ids, *args):
        wf_service = netsvc.LocalService("workflow")
        self.write(cr, uid, ids, {'state': 'waiting_split'})
        move_obj = self.pool.get('stock.move')
        for sp in self.browse(cr, uid, ids):
            move_obj.write(cr, uid, [x.id for x in sp.move_lines], {
                'status': 'Need to split',
                'exception_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                })
            wf_service.trg_write(uid, 'stock.picking', sp.id, cr)
        return True

    def get_split_required(self, cr, uid, ids=None, context=None, limit=None):

        if not ids:
            return []

        allow_split = self.is_allow_split(cr, uid, ids)
        ids = [x for x, y in allow_split.iteritems() if y]
        if not ids:
            return []

        sql = """
            SELECT sp.id
            FROM stock_picking sp
                LEFT JOIN sale_order so on sp.sale_id = so.id
                LEFT JOIN stock_move sm on sp.id = sm.picking_id
                LEFT JOIN stock_location sl on sm.location_id = sl.id
                LEFT JOIN res_partner rp on sp.real_partner_id = rp.id
            WHERE 1=1 {where_ids}
                and so.flash is not true
                and rp.allow_auto_split = true
                and sp.state = 'waiting_split'
            GROUP BY
                sp.id
            HAVING 1=1
                and count(sm.id) > 1
                and array_agg(distinct sm.state) <@ array['assigned', 'done', 'reserved']::varchar[]
                and (count(distinct sl.warehouse_id) != 1 and array_agg(distinct sl.name) != array['FEOSPP']::varchar[])
            {limit}
        """.format(
            where_ids="and sp.id in %s" if ids else '',
            limit="limit %s" if limit and limit > 0 else ''
            )
        cr.execute(sql, (tuple(ids), ))
        res = cr.fetchall() or []
        return [x[0] for x in res]

    # DLMR-2069
    # Do split exception lines and cancel
    def do_auto_split_and_cancel(self, cr, uid, ids, context=None):
        logger.info("Run auto-split and cancel unavailable moves")
        if context is None:
            context = {}
        wf_service = netsvc.LocalService("workflow")
        for pick in self.browse(cr, uid, ids, context=context):
            if pick.state in ('cancel', 'final_cancel'):
                continue
            split_move_ids = self.pool.get('stock.move').search(cr, uid, [('picking_id', '=', pick.id), '|', ('state', 'not in', ('assigned', 'done')), ('product_qty', '=', 0)])
            logger.info("Move line IDs to be separated: %s" % split_move_ids)
            cancelled_picking_id = self.do_split_move_ids(cr, uid, pick.id, split_move_ids, 'to_cancel', context=context)
            logger.info("Cancelled order ID: %s" % cancelled_picking_id)
            wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_recheck', cr)
        return True

    def action_auto_split(self, cr, uid, ids, *args):
        auto_split = False
        conf_obj = self.pool.get('ir.config_parameter')
        auto_split_str = conf_obj.get_param(cr, uid, 'delivery.auto.split', default='False')
        if auto_split_str and isinstance(auto_split_str, basestring) and auto_split_str.strip().lower() in ['yes', 'true', '1']:
            auto_split = True

        if auto_split:
            to_split_ids = self.get_split_required(cr, uid, ids=ids)
            if to_split_ids:

                for pick_id in to_split_ids:

                    order_wh_ids = self._get_warehouses(cr, uid, [pick_id], 'warehouses', None)[pick_id]
                    for wh_id in order_wh_ids:
                        partial_data = {}
                        if not wh_id:
                            continue
                        for move in order_wh_ids[wh_id]:
                            partial_data['move%s' % (move.id)] = {
                                'product_id': move.product_id.id,
                                'product_qty': move.product_qty,
                                'product_uom': move.product_uom and move.product_uom.id or False,
                                'prodlot_id': move.prodlot_id.id
                            }
                        if partial_data:
                            partial_data.update({'delivery_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())})
                            print 'Split %s from %s' % (partial_data, pick_id)
                            self.do_split(cr, uid, [pick_id], partial_data)
                            break
        return True

    def action_confirm(self, cr, uid, ids, context=None):
        """ Confirms picking.
        @return: True
        """
        self.write(cr, uid, ids, {'state': 'confirmed'})
        move_obj = self.pool.get('stock.move')

        self.log_picking(cr, uid, ids, context=context)

        revert_flg = move_obj.search(cr, uid, [('picking_id', 'in', ids), ('back_flag', '=', True)])
        if revert_flg:
            move_obj.action_remove_back_flag(cr, uid, revert_flg)

        todo = move_obj.search(cr, uid, [('picking_id', 'in', ids), ('state', '=', 'draft')])
        todo = self.action_explode(cr, uid, todo, context)
        if todo:
            move_obj.action_confirm(cr, uid, todo, context=context)

        return True

    def action_assign(self, cr, uid, ids, *args, **kwargs):
        context = {}
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        only_move_ids = False
        force_location_ids = False
        if kwargs:
            if kwargs.get('move_ids', False):
                only_move_ids = kwargs['move_ids']

            force_location_ids = kwargs.get('location_ids', False)
            if not force_location_ids:
                force_location_ids = []
            elif isinstance(force_location_ids, (int, long)):
                force_location_ids = [force_location_ids]

        # Filter out orders, already taken by rabbit
        cr.execute('SELECT order_id FROM _rabbitmq_availability_queue')
#       ids = list(set(ids) - set(r[0] for r in cr.fetchall()))
#       if not ids and uid != 1 and all(map(lambda x: x.status == 'draft', self.browse(cr, uid, ids))):
        rabbit_ids = list(set(ids) - set(r[0] for r in cr.fetchall()))
        if not rabbit_ids and uid != 1 and all(map(lambda x: x.state == 'draft', self.browse(cr, uid, ids))):

            # No, it is not a rabbit symbol, it's just a typo )
            raise osv.except_osv(_('Warning !'), _('Order is being processed by 🐇 already.'))

        for pick in self.browse(cr, uid, ids):
            sale_order = pick and pick.sale_id or False
            partner = sale_order and sale_order.partner_id or False
            if not partner:
                return False

            # Check Specific location (ignore customer settings)

            if sale_order.force_location_ids:
                force_location_ids = [x.id for x in sale_order.force_location_ids]

            if force_location_ids:
                context.update({'force_location_ids': force_location_ids})

            else:

                # Ship by country (get warehouses)
                country_warehouses = self.get_shipping_warehouses(cr, uid, pick.id).get(pick.id, False)

                customer_location_ids = []
                try:
                    if partner.order_ids:
                        customer_location_ids = [int(x) for x in partner.order_ids.split(",")]

                    if not customer_location_ids and partner.location_ids:
                        customer_location_ids = [x.id for x in partner.location_ids]
                except:
                    return False

                # Ship by country (primary filtering)
                if country_warehouses:
                    filtered_locs = []
                    for lc_id in customer_location_ids:
                        if loc_obj.get_warehouse(cr, uid, lc_id) in country_warehouses:
                            filtered_locs.append(lc_id)
                    customer_location_ids = filtered_locs

                if not customer_location_ids:
                    # raise osv.except_osv(_('Warning !'), _('Customer has no locations, unable to reserve the products.'))
                    return False
                else:
                    customer_location_ids = move_obj.filter_locations_of_frozen_wh(cr, uid, customer_location_ids)
                    context.update({'customer_location_ids': customer_location_ids})

                sec_customer_location_ids = []
                try:
                    if partner.sec_order_ids:
                        sec_customer_location_ids = [int(x) for x in partner.sec_order_ids.split(",")]

                    if not sec_customer_location_ids and partner.sec_location_ids:
                        sec_customer_location_ids = [x.id for x in partner.sec_location_ids]
                except:
                    sec_customer_location_ids = []

                # Ship by country (secondary filtering)
                if country_warehouses:
                    filtered_locs = []
                    for lc_id in sec_customer_location_ids:
                        if loc_obj.get_warehouse(cr, uid, lc_id) in country_warehouses:
                            filtered_locs.append(lc_id)
                    sec_customer_location_ids = filtered_locs

                context.update({'sec_customer_location_ids': sec_customer_location_ids})

            if only_move_ids:
                move_ids = only_move_ids
            else:
                move_ids = move_obj.search(cr, uid, [('picking_id', '=', pick.id), ('state', '=', 'confirmed')])

            res = move_obj.action_assign(cr, uid, move_ids, context=context)

            move_ring_ids = [i for i in move_obj.search(cr, uid, [('picking_id', '=', pick.id)]) if i not in res]
            if pick.state in ('waiting_split', 'confirmed') and partner.auto_size_tolerance and partner.size_tolerance.id and len(move_ring_ids):
                move_obj.check_availability(cr, uid, move_ring_ids, partner.id, context=context)

            """TODO
            In stock_move.py we have hardcode for enabled_secondary_reserve = False in check_assign function.
            So we don't use secondary_location anyway"""

            self.set_warehouses_locations(cr, uid, pick.id)

        return True

    def reject_manual(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'rejected'})
        return True

    def approve_manual(self, cr, uid, ids, context=None):
        address_obj = self.pool.get('res.partner.address')

        for order in self.browse(cr, uid, ids, context=context):
            if order.address_id:
                address_obj.approve_manual(cr, uid, [order.address_id.id], context=context)
        return True

    def test_address_by_form(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            is_valid = self.test_address(cr, uid, [id], context=context)
            logger.warn("Stock_picking address check: %s" % is_valid)

            if not is_valid:
                raise osv.except_osv(_('Warning !'), _('Bad picking address.'))
            else:
                wf_service.trg_write(uid, 'stock.picking', id, cr)

        return True

    def action_done(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        sale_order_data = self.pool.get('sale.order')
        for order in self.browse(cr, uid, ids, context=context):
            if order.state == 'shipped':
                super(stock_picking, self).action_done(cr, uid, [order.id])
                if order.shipping_batch_id:
                    self.pool.get('stock.picking.shipping.batch').action_done(cr, uid, [order.shipping_batch_id.id])
                if order.shp_bulk_id or False:
                    wf_service.trg_write(uid, 'stock.picking.bulk', order.shp_bulk_id.id, cr)
                sale_order_data.check_order_status(cr, uid, order.sale_id.id, context=context)
        return True

    def action_send_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        pick_id = ids[0]
        pick_order = self.browse(cr, uid, pick_id, context=context)
        customer_id = pick_order.sale_id.partner_id.id

        if len(self.pool.get('stock.picking.final.cancel.codes').search(cr, uid, [('customer', '=', customer_id)])) > 0:
            self.write(cr, uid, pick_id, {'state': 'final_cancel'})
            data_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_picking_final')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
            context['picking_id'] = pick_id
            context['customer_id'] = customer_id
            act_win['context'] = context
            return act_win
        else:
            self.pool.get('sale.integration').integrationApiSendCancel(cr, uid, ids, context=None)
            return False

    def action_advanced_cancel(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        act_win_res_id = data_obj._get_id(
            cr, uid, 'delmar_sale','action_stock_picking_advanced_cancel')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [],
                               context=context)
        context['picking_id'] = ids[0]
        act_win['context'] = context
        return act_win

    def button_final_cancel(self, cr, uid, ids, context=None):
        result = True
        if context is None:
            context = {}

        pick_id = ids[0]
        pick_order = self.browse(cr, uid, pick_id, context=context)
        customer_id = pick_order.sale_id.partner_id.id

        self.reset_shipping_info(cr, uid, ids, context=context)

        if len(self.pool.get('stock.picking.final.cancel.codes').search(cr, uid, [('customer', '=', customer_id)])) > 0:
            data_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_picking_final')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
            context['picking_id'] = pick_id
            context['customer_id'] = customer_id
            act_win['context'] = context
            result = act_win

        else:
            wf_service = netsvc.LocalService("workflow")
            for order in self.browse(cr, uid, ids, context=context):
                if order.state == 'cancel':
                    remote_status = self.pool.get('sale.integration').integrationApiSendCancel(cr,
                                                                                               uid,
                                                                                               order.id,
                                                                                               context=context)
                    if remote_status:
                        wf_service.trg_validate(uid, 'stock.picking', order.id, 'button_final_cancel', cr)

        sale_order_obj = self.pool.get('sale.order')
        sale_orders = sale_order_obj.search(cr, uid, [('picking_ids', 'in', ids), ('flash', '=', True)])
        try:
            logger.info('Cancel flash orders: {}'.format(sale_orders))
            sale_order_obj.action_cancel(cr, uid, sale_orders)
        except osv.except_osv as err:
            logger.warning('Cannot cancel flash order! {}'.format(err))

        return result

    def action_final_cancel(self, cr, uid, ids, context=None):

        pick_order = self.browse(cr, uid, ids, context=context)
        partner = pick_order[0].real_partner_id or pick_order[0].partner_id
        if partner and partner.order_support_user_id and partner.order_support_user_id.user_email:
            email_to = partner.order_support_user_id.user_email or False
            if email_to:
                mail_message = {
                    'to_send': True,
                    'subtype': 'plain',
                    'from': 'erp.delmar@gmail.com',
                    'to': [email_to],
                    'subject': '[OPENERP] Final cancellation',
                    'body': """Hi {},
                       Order {} has been canceled
                    """.format(partner.order_support_user_id.name,pick_order[0].name),
                }

                mid = self.pool.get('mail.message').schedule_with_attach(
                    cr, uid,
                    email_from=mail_message.get('from'),
                    email_to=mail_message.get('to'),
                    subject=mail_message.get('subject'),
                    body=mail_message.get('body'),
                    subtype=mail_message.get('subtype'), context=None)

        self.write(cr, uid, ids, {'state': 'final_cancel'})

    def action_reprocess(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context = dict(context, active_ids=ids, active_model=self._name)

        partial_id = self.pool.get("stock.picking.reprocess").create(cr, uid, {}, context=context)

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_picking_reproces')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        context.update(flags={'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False})
        act_win.update({
            'res_id': partial_id,
            'context': context,
            })
        return act_win

    def action_only_return(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        logger.info('Start order return form for partial picking')
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        context = dict(context, active_ids=ids, active_model=self._name, all_states=True, default_type='return')

        # DLMR-1070
        # Inititated for Jet
        grant_return = self.pool.get('sale.integration').integrationApiGrantReturnOrder(cr, uid, picking_id=ids[0])
        if not grant_return:
            raise osv.except_osv("Warning!", "This order cannot be returned!")
        # END DLMR-1070

        self.pool.get('sale.integration').integrationApiGetRMA(cr, uid, picking_id=ids[0])
        partial_id = self.pool.get("stock.partial.picking").create(cr, uid, {}, context=context)

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_only_return_picking')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': partial_id,
            'context': context,
            })
        return act_win

    def action_update_transactions(self, cr, uid, ids, context=None):
        """Update transactions table."""
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)

        # Update fc_invoice and export_refs in transactions table
        sm_obj = self.pool.get('stock.move')
        sm_entries = context.get('sm_entries')
        if not sm_entries:
            sm_ids = sm_obj.search(cr, uid, [('picking_id', 'in', ids)], context=context)
            sm_entries = sm_obj.browse(cr, uid, sm_ids, context=context)

        for sm_entry in sm_entries:
            logger.info("Starting update move transaction")
            warehouse = sm_entry.warehouse.name
            sm_obj.update_transactions_table(cr, uid, server_id, server_obj, sm_entry, warehouse, context)
            # DLMR-963
            logger.info("Starting update box transaction")
            if sm_entry.box_id:
                sm_box_ids = sm_obj.search(cr, uid, [('box_parent_move_id', '=', sm_entry.id)])
                if sm_box_ids and len(sm_box_ids) > 0:
                    logger.info("Box move found, updating transaction")
                    box_entry = sm_obj.browse(cr, uid, sm_box_ids[0], context=context)
                    if not sm_obj.update_transactions_table(cr, uid, server_id, server_obj, box_entry, box_entry.warehouse.name, context):
                        logger.warn("Box transaction was not updated to CXL")
            else:
                logger.warn("No box_id found in moveline %s" % sm_entry.id)
            # END DLMR-963
        return

    def action_combine_stock_pickings(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        batch_obj = self.pool.get('stock.picking.shipping.batch')
        pick_ids = context.get('active_ids', [])

        combine_orders = {}
        return_action = {'type': 'ir.actions.act_window_close'}
        allowed_states = [
            'assigned',
            'picking',
            'shipped',
            'incode_except',
            'outcode_except',
            ]

        is_auto_process = context.get('auto_process', True)

        # Don't include s2s
        if context.get('exclude_s2s'):
            sql = """select
                    sp.id
                from
                    stock_picking as sp
                left join sale_order as so on
                    so.id = sp.sale_id
                where 1=1
                    and so.ship2store is not true  
                    and sp.id in %(ids)s"""
            cr.execute(sql, {'ids': tuple(pick_ids)})
            pick_ids = [x[0] for x in cr.fetchall() if x]

        for order in self.read(cr, uid, pick_ids, ['name', 'address_grp', 'state', 'origin_carrier', 'shipping_batch_id']):
            address_grp = order.get('address_grp', False)
            order_state = order.get('state', False)

            if not address_grp:
                continue
            # DLMR-2178
            address_grp = address_grp.lower()
            # END DLMR-2178
            if is_auto_process:
                address_parts = address_grp.split('/')
                address_parts[0] = 'FORCE'
                address_grp = "/".join(address_parts)

            if not combine_orders.get(address_grp, False):
                combine_orders[address_grp] = {
                    'combine': [],
                    'code': {},
                    }

            if order_state in allowed_states:
                combine_orders[address_grp]['combine'].append(order['id'])
                if not order.get('shipping_batch_id', False):
                    combine_orders[address_grp]['code'][order.get('origin_carrier', False)] = True

        if len(combine_orders) > 1:
            raise osv.except_osv(_('Warning !'), _("""You are trying to combine orders with different addresses!"""))
        if not combine_orders:
            raise osv.except_osv(_('Warning !'), _("""Nothing to combine!"""))

        address_grp, attributes = combine_orders.iteritems().next()
        combine_ids = attributes.get('combine', [])

        if not combine_ids:
            raise osv.except_osv(_('Warning !'), _("""You must choose at least one order in "Ready to Process", "Waiting Tracking Number" or "Ready" state!"""))

        else:
            incoming_codes = attributes.get('code', {}).keys()
            batch_in_code = False
            batch_id = batch_obj.get_active_batch_id(cr, uid, address_grp)
            if batch_id:
                batch_in_code = batch_obj.read(cr, uid, batch_id, ['incoming_code']).get('incoming_code', False)
                if batch_in_code not in incoming_codes:
                    incoming_codes.append(batch_in_code)

            if len(incoming_codes) > 1:
                wizard_id = self.pool.get('stock.batch.carrier.wizard').create(cr, uid, {'address_grp': address_grp, 'shipping_code': batch_in_code}, context={'picking_ids': combine_ids})
                act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_batch_carrier_wizard')
                act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
                act_win = act_obj.read(cr, uid, act_win_id['res_id'], [])
                act_win['res_id'] = wizard_id
                return_action = act_win

            else:
                process_res = batch_obj.combine_orders(cr, uid, address_grp, combine_ids, incoming_code=incoming_codes[0], auto_process=is_auto_process)
                if process_res:
                    return_action = process_res

        return return_action

    def action_separate_stock_pickings(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        batch_obj = self.pool.get('stock.picking.shipping.batch')

        pick_ids = context.get('active_ids', [])
        if pick_ids:
            batch_ids = []
            batch_pick_ids = []
            batch_with_pickis = self.get_batch_pickings(cr, uid, pick_ids)

            for batch_id, picking_ids in batch_with_pickis:
                batch_ids.append(batch_id)
                batch_pick_ids += picking_ids

            self.write(cr, uid, pick_ids, {'shipping_batch_id': False})
            self.update_shipping_info(cr, uid, batch_pick_ids, force=True)

            for batch_id in batch_ids:
                batch_obj.write(cr, uid, batch_id, {'shipping_code': batch_obj.get_shp_code(cr, uid, batch_id)})

        return {'type': 'ir.actions.act_window_close'}

    def get_batch_pickings(self, cr, uid, ids, only_pickings=False, in_states=None, context=None):
        if not in_states: in_states = []
        result = []

        if ids:
            params = (tuple(ids),)
            state_cond = ""
            if in_states:
                state_cond = " AND sp.state in %s"
                params += (tuple(in_states), )

            if only_pickings:
                cr.execute("""  SELECT DISTINCT sp.id
                                FROM stock_picking sp
                                WHERE sp.shipping_batch_id in (
                                    SELECT shipping_batch_id
                                    FROM stock_picking
                                    WHERE id in %s
                                ) """
                           + state_cond, params)
                result = [x[0] for x in cr.fetchall() if x]

            else:
                cr.execute("""  SELECT sp.shipping_batch_id as batch_id,
                                    array_to_string(array_accum(DISTINCT sp.id), ',') as picking_ids
                                FROM stock_picking sp
                                WHERE sp.shipping_batch_id in (
                                    SELECT shipping_batch_id
                                    FROM stock_picking
                                    WHERE id in %s
                                ) """
                           + state_cond + """
                                GROUP BY sp.shipping_batch_id
                                """, params)
                for batch_id, pick_ids in cr.fetchall():
                    result.append((batch_id, [int(x) for x in pick_ids.split(',')]))

        return result

    def allow_reship(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        user_obj = self.pool.get('res.users')
        move_obj = self.pool.get('stock.move')
        user = user_obj.browse(cr, uid, uid)

        logger.info("RUN ALLOW RE-SHIP")

        for order in self.browse(cr, uid, ids):
            note = order.report_history or ''
            note += "\n%s %s: Re-ship order" % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name)
            self.reset_shipping_info(cr, uid, [order.id], context=context)

            if order.move_lines:
                move_ids = [x.id for x in order.move_lines]
                # DLMR-1190
                # UNVERIFY Move lines
                if not move_obj.unverify_transaction(cr, uid, ids=move_ids):
                    logger.warn("Transactions for %s were not unverified!" % move_ids)
                # update move status
                move_obj.write(cr, uid, move_ids, {'state': 'assigned', 'qty_verified': 0})

            self.reset_shipping_rule(cr, uid, order.id, vals={
                'report_history': note,
                'status': False,
                'tracking_ref': False,
                'machine_tracking_ref': False,
                })

        return True

    def action_return_wkf(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'returned'})
        return True

    def create_picking_repot(self, cr, uid, ids, report_name=None, context=None):

        if report_name is None:
            report_name = 'stock.picking.list'

        if context is None:
            context = {}

        res = {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': {
                'model': 'stock.picking',
                'report_type': 'pdf',
                'ids': ids,
                },
            'context': context,
            }

        return res

    def log_done(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        data_obj = self.pool.get('ir.model.data')
        for pick in self.browse(cr, uid, ids, context=context):

            message = "Delivery order '%s' is shipped." % (pick.name)

            res = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'view_picking_track_form')
            context.update({'view_id': res and res[1] or False})

            self.log(cr, uid, pick.id, message, context=context)
        return True

    def get_invoice_no_for_pick(self, cr, uid, pick, invoice_server_id):
        server_data = self.pool.get('fetchdb.server')

        invoice_no = False

        table_prefix = ''
        if pick.sale_id.partner_id.ref == 'OS':
            table_prefix = 'OS_'

        sale_order_name = pick.origin

        sel_invno_sql = """ SELECT InvoiceNo
                  FROM gmotoc.%sInvoice_Header
                  WHERE PO_ID = ? """ % (table_prefix,)

        invno_res = server_data.make_query(cr, uid, invoice_server_id, sel_invno_sql, params=(sale_order_name,), select=True, commit=False)
        if invno_res and invno_res[0] and invno_res[0][0]:
            invoice_no = invno_res[0][0]
        else:
            logger.warn("Not found invoice record in %sInvoice_Header table for the following order: \n%s" % (table_prefix, sale_order_name, ))

        return invoice_no

    def action_reserved_wkf(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'reserved'})
        return True

    def test_reserved(self, cr, uid, ids, context=None):
        result = False

        if context is None:
            context = {}

        if self.pool.get("stock.move").search(cr, uid, [('picking_id', 'in', ids), ('state', '=', 'reserved')], limit=1):
            result = True

        return result

    def cron_set_done_cycle(self, cr, uid, customer_id=None, scanned_only=False, context=None):

        logger.info('cron_set_done_cycle')

        if not customer_id:
            customer_id = -1

        if not self._set_done_options.get(customer_id, False):
            self._set_done_options[customer_id] = {}

        if not self._set_done_options[customer_id].get(scanned_only, False):
            self._set_done_options[customer_id][scanned_only] = self._set_done_options_default

        limit = self._set_done_options[customer_id][scanned_only].get('limit', 64)
        cron_id = self._set_done_options[customer_id][scanned_only].get('cron_id', 0)
        bad_ids = self._set_done_options[customer_id][scanned_only].get('bad_ids', [])
        bad_id = self._set_done_options[customer_id][scanned_only].get('bad_id', 0)
        numbercall = self._set_done_options[customer_id][scanned_only].get('numbercall', -1)
        cron_obj = self.pool.get('ir.cron')

        if not cron_id:
            print "Cron ID (batch set done) not found."
            cron_ids = cron_obj.search(cr, uid, [('function', '=', 'cron_set_done_cycle'), ('args', '=', '([%s,%s,])' % (customer_id, str(scanned_only)))])
            if cron_ids:
                cron_id = cron_ids[0]
                self._set_done_options[customer_id][scanned_only].update({'cron_id': cron_id})
            else:
                return True

        search_args = [('state', '=', 'shipped')]
        if customer_id > 0:
            search_args += [('real_partner_id', '=', customer_id)]
        if scanned_only:
            search_args += ['|', ('scan_uid', '!=', False), ('scan_date', '!=', False)]

        wf_service = netsvc.LocalService("workflow")
        todo = self.search(cr, uid, search_args + [('id', 'not in', bad_ids)])
        if not todo:
            cron_obj.unlink(cr, uid, [cron_id], context)
            return True
        if limit:
            self._set_done_options[customer_id][scanned_only].update({'limit': limit/2})
        else:
            if numbercall == -1:
                self._set_done_options[customer_id][scanned_only].update({'numbercall': 8, 'limit': limit/2, 'bad_id': todo[0]})
            elif numbercall == 1:
                self._set_done_options[customer_id][scanned_only].update({'numbercall': -1, 'limit': limit/2, 'bad_ids': bad_ids + [bad_id], 'bad_id': 0})
            else:
                if bad_id in todo:
                    todo = [bad_id]
                else:
                    self._set_done_options[customer_id][scanned_only].update({'limit': 2})

        limit = limit or 1
        print "Trying to set done %d orders." % (limit,)
        db = self.pool.db
        for pick_id in todo[:limit]:
            step_cr = db.cursor()
            try:
                wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_done', step_cr)
                step_cr.commit()
            finally:
                step_cr.close()

        final_cr = db.cursor()
        not_done_ids = self.search(final_cr, uid, search_args + [('id', 'in', todo[:limit])])
        print "Orders was set done."
        bad_ids += not_done_ids
        todo = self.search(final_cr, uid, search_args + [('id', 'not in', bad_ids)])
        final_cr.close()

        if not todo:
            cron_obj.unlink(cr, uid, [cron_id], context)
            return True
        else:
            self._set_done_options[customer_id][scanned_only].update({'numbercall': -1, 'limit': limit*2, 'bad_ids': bad_ids})

        return True

    def cron_set_done_daily(self, cr, uid, customer_id=None, scanned_only=False, context=None):

        logger.info('cron_set_done_daily')

        cron_obj = self.pool.get('ir.cron')
        customer_name = ''
        postfix = ''

        if scanned_only:
            postfix = '(scanned only)'

        if not customer_id:
            customer_id = -1
        elif customer_id > 0:
            customer = self.pool.get('res.partner').read(cr, uid, customer_id, ['ref'])
            if customer:
                customer_name = '_' + customer.get('ref', customer_id)
            else:
                return True

        args_str = '([%s,%s,])' % (customer_id, str(scanned_only))

        if not self._set_done_options.get(customer_id, False):
            self._set_done_options[customer_id] = {}

        customer_options = self._set_done_options_default

        active_crons = cron_obj.search(cr, uid, [
            ('function', '=', 'cron_set_done_cycle'),
            ('args', '=', args_str),
            ('active', '=', True),
            ('numbercall', '!=', 0)]
        )

        if not active_crons:
            vals = {
                'name': datetime.now().strftime('%y-%m-%d') + customer_name + '_set_done' + postfix,
                'user_id': uid,
                'model': self._name,
                'interval_type': 'minutes',
                'function': 'cron_set_done_cycle',
                'args': args_str,
                'numbercall': -1,
                'priority': 4,
                'type': 'set_done', # defined in pf_utils/ir/ir_cron.py
            }

            cron_id = cron_obj.create(cr, uid, vals)
            customer_options.update({'cron_id': int(cron_id)})

        self._set_done_options[customer_id][scanned_only] = customer_options

        return True

    def check_for_reprocess(self, cr, uid, pick, invoice_server_id, tbl_prefix, context=None):
        server_data = self.pool.get('fetchdb.server')
        result = False

        if pick and pick.name and (pick.name[-1:] == 'R'):
            so_name = pick.sale_id and pick.sale_id.name
            if so_name:
                sql = """
                    select count(*)
                    from gmotoc.%sInvoice_Header
                    where PO_ID = '%s'
                    """ % (tbl_prefix, so_name)
                sql_result = server_data.make_query(cr, uid, invoice_server_id, sql, select=True, commit=False)
                result = sql_result and sql_result[0] and sql_result[0][0] and (sql_result[0][0] > 0)
        return result

    def do_remote_done(self, cr, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        server_data = self.pool.get('fetchdb.server')
        sale_server_id = server_data.get_sale_servers(cr, uid, single=True)
        invoice_server_id = server_data.get_invoice_servers(cr, uid, single=True)
        server_id = server_data.get_main_servers(cr, uid, single=True)

        remote_invoice_status = False

        if len(ids) == 1:
            pick = self.browse(cr, uid, ids[0])
            if pick.sale_id and pick.sale_id.partner_id and pick.sale_id.partner_id.virtual:
                return True

            order_params = self.get_order_params(cr, uid, pick.id, ['skip_ms_invoice', 'skip_ms_sales'])[pick.id]
            if order_params.get('skip_ms_invoice', False) == '1' and order_params.get('skip_ms_sales', False) == '1':
                self.pool.get('sale.integration')._switch_set_lines(cr, uid, pick, 'to_done')
                return True

            if pick.state == 'shipped':

                settings_id = self.pool.get('sale.integration').search(cr, uid, [('customer_ids', '=', pick.sale_id.partner_id.id)])
                tbl_prefix = ''
                if (len(settings_id) > 0):
                    settings_obj = self.pool.get('sale.integration').browse(cr, uid, settings_id)

                    if settings_obj[0].type_api == 'overstock':
                        tbl_prefix = 'OS_'
                    #if settings_obj[0].type_api in ['sterling'] or pick.sale_id.type_api in ['import_orders']:
                    #    self.pool.get('sale.integration')._switch_set_lines(cr, uid, pick, 'to_done')
                    #    pick = self.browse(cr, uid, ids[0])

                # shp_obj_ids = self.pool.get('res.shipping.code').search(cr, uid, [('incoming_code', '=', pick.sale_id.carrier), ('sale_integration_type', '=', settings_obj[0].name)])
                shp_obj_ids = self.pool.get('res.shipping.code').search(cr, uid, [('outgoing_code', '=', pick.carrier_code)])
                shp_obj = self.pool.get('res.shipping.code').browse(cr, uid, shp_obj_ids)

                if self.check_for_reprocess(cr, uid, pick, invoice_server_id, tbl_prefix, context=context):
                    self.pool.get('sale.order').write(cr, uid, pick.sale_id.id, {'delmar_invoiced': 't'}, context=context)
                    return True

                order_line_item_no = 0
                order_cost = 0
                order_qty = 0
                shipping_handling = pick.shp_handling

                invoice_no = pick.invoice_no
                remote_invoice_status = True

                self.remove_invoice_detail(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, invoice_no)
                self.remove_invoice_header(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, invoice_no, direct=1)

                self.remove_OS_Sales_BU(cr, uid, pick, invoice_no, sale_server_id)
                self.remove_OS_Sales(cr, uid, pick, invoice_no, sale_server_id)

                set_lines = {}
                for line in pick.move_lines:

                    order_line_item_no += 1

                    order_cost += float(line.sale_line_id.price_unit) * float(line.product_qty)
                    total_sale_line_cost = float(line.sale_line_id.price_unit) * float(line.product_qty)

                    order_qty += float(line.product_qty)

                    if invoice_server_id:
                        remote_invoice_status &= self.fill_invoice_detail(cr, uid, line, tbl_prefix, invoice_no, invoice_server_id, total_sale_line_cost, order_line_item_no)

                    if sale_server_id and server_id and remote_invoice_status:
                        remote_invoice_status &= self.fill_OS_Sales_tables(cr, uid, line, invoice_no, server_id, sale_server_id, order_line_item_no)

                if invoice_server_id and remote_invoice_status:
                    remote_invoice_status &= self.fill_invoice_header(cr, uid, shipping_handling, pick, shp_obj, invoice_server_id, tbl_prefix, invoice_no, order_qty, order_line_item_no, order_cost)

                if not remote_invoice_status and order_params.get('skip_ms_invoice', False) == '0' and order_params.get('skip_ms_sales', False) == '0':
                    # remove all inserted rows from OS_Sales, Invoice_Detail and Invoice_Header
                    self.remove_OS_Sales_BU(cr, uid, pick, invoice_no, sale_server_id)
                    self.remove_OS_Sales(cr, uid, pick, invoice_no, sale_server_id)

                    self.remove_invoice_detail(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, invoice_no)
                    self.remove_invoice_header(cr, uid, invoice_server_id, tbl_prefix, pick.sale_id.name, pick.tracking_ref, invoice_no, direct=1)
                else:
                    # do invoced order
                    self.pool.get('sale.order').write(cr, uid, pick.sale_id.id, {'delmar_invoiced': 't'}, context=context)

                return remote_invoice_status
        else:
            return False

    def fill_invoice_detail(self, cr, uid, line, tbl_prefix, invoice_no, invoice_server_id, total_sale_line_cost, line_no, direct=1, return_qty=False):
        pick = line.picking_id
        order_params = self.get_order_params(
            cr, uid, pick.id, ['skip_ms_invoice']
        )[pick.id]
        if order_params.get('skip_ms_invoice', False) == '1':
            return True

        server_data = self.pool.get('fetchdb.server')
        product_data = self.pool.get('product.product')

        s_line = line.sale_line_id
        acc_price_price = 0.0
        # calculate price from acc_price if only:
        # sale and customer with Acc_Price invoice cost
        if direct and pick.real_partner_id.invoice_cost_from_acc_price:
            acc_price_price = self.get_acc_price_price(cr, uid, line.id)

        # take price unit from acc_price first, if not then from sale order line
        line_price_unit = s_line.price_unit and float(s_line.price_unit)
        price_unit = acc_price_price or line_price_unit or 0.0

        # DLMR-996
        if s_line.cf_price_unit and float(s_line.cf_price_unit) > 0:
            cf_price_unit = float(s_line.cf_price_unit)*((100 - pick.real_partner_id.discount)/100)
        else:
            cf_price_unit = self.get_acc_price_price(cr, uid, line.id) or (line_price_unit*((100 - pick.real_partner_id.discount)/100)) or 0.0

        if line.is_set_component:
            qty = line.sale_line_id.product_uom_qty if direct else return_qty
            price_unit = line_price_unit * qty
        else:
            qty = line.product_qty if direct else return_qty

        # Adjust price_unit by discount
        price_unit *= ((100 - pick.real_partner_id.discount)/100)

        total_sale_line_cost = price_unit * float(qty)

        # revert numbers for returns
        if not direct:
            price_unit *= -1.0
            total_sale_line_cost *= -1.0
            cf_price_unit *= -1.0

        if pick.real_partner_id.external_customer_id == 'pii':
            ProductID = s_line.merchantSKU or None
        else:
            ProductID = line.sale_line_id.product_id.default_code or None

        so_partner = s_line.order_id.partner_id or s_line.set_parent_order_id.partner_id

        upc = product_data.get_val_by_label(cr, uid, line.sale_line_id.product_id.id, so_partner.id, 'UPC', line.sale_line_id.size_id and line.sale_line_id.size_id.id or False) or None
        Size = line.sale_line_id.size_id and line.sale_line_id.size_id.name or None
        id_delmar = line.sale_line_id.product_id.default_code or None
        customer_id_delmar = product_data.get_val_by_label(cr, uid, line.sale_line_id.product_id.id, so_partner.id, 'Customer ID Delmar', line.sale_line_id.size_id and line.sale_line_id.size_id.id or False) or None

        table_name = 'gmotoc.%sInvoice_Detail' % (tbl_prefix)
        insert_values = {
            'PO_ID': pick.sale_id.name or None,
            'MerchantID': so_partner.ref or None,
            'ID_Delmar': id_delmar or None,
            'ProductID': ProductID,
            'Quantity': qty and int(qty) or None,
            'UnitCost': price_unit or 0,
            'ExtCost': total_sale_line_cost,
            'UPC': upc,
            'UnitPrice': cf_price_unit or 0,
            'InvoiceNo': invoice_no,
            'Size': Size,
            'VendorSKU': s_line.merchantSKU or None,
            'OrderDate_In': pick.date_done,
            'TrackingNo': pick.tracking_ref or None,
            'OrderLineItem_': s_line.merchantLineNumber or line_no,
            'prc_ovride': 0,
            'customer_id_delmar': customer_id_delmar
        }

        result = server_data.insert_record(
            cr, uid, invoice_server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        return bool(result)

    # Get price from acc_price table
    def get_acc_price_price(self, cr, uid, move_id):
        res = 0.0
        move = self.pool.get('stock.move').browse(cr, uid, move_id)
        if move:
            if move.is_set_component:
                default_code = move.sale_line_id.product_id.default_code
            else:
                default_code = move.product_id.default_code

            partner = move.picking_id.real_partner_id
            acc_price_code = partner and (partner.acc_price_code or partner.ref)
            if acc_price_code:
                sql = """SELECT "PRICE"
                    FROM acc_price
                    WHERE "ITEM_" = '%s'
                        AND "CODE" = '%s'
                    """ % (default_code, acc_price_code)
                cr.execute(sql)
                sql_res = cr.fetchone()
                res = sql_res and sql_res[0] and float(sql_res[0]) or 0.0
        return res

    def remove_invoice_detail(self, cr, uid, invoice_server_id, tbl_prefix, po_number, tracking_ref, invoice_no):
        server_data = self.pool.get('fetchdb.server')
        remove_failed_details_sql = """
            DELETE FROM gmotoc.%sInvoice_Detail
            WHERE 1=1
                AND PO_ID = '%s'
                AND TrackingNo = '%s'
                AND InvoiceNo = '%s'
            ;""" % (tbl_prefix, po_number, tracking_ref, invoice_no)
        result = server_data.make_query(cr, uid, invoice_server_id, remove_failed_details_sql, select=False, commit=True)
        return result

    # TOREFACTOR: remove code duplication with:
    # sale_order.fba_fill_OS_Sales_tables and sale_order.fbc_fill_OS_Sales(_BU)
    # TOFIX: do all inserts in one transaction
    def fill_OS_Sales_tables(self, cr, uid, line, invoice_no, server_id, sale_server_id, line_no, return_qty=False):
        pick = line.picking_id
        order_params = self.get_order_params(
            cr, uid, pick.id, ['skip_ms_sales']
        )[pick.id]
        if order_params.get('skip_ms_sales') == '1':
            return True

        result = False
        server_data = self.pool.get('fetchdb.server')
        s_line = line.sale_line_id
        order = s_line.order_id or s_line.set_parent_order_id
        order_name = order.po_number
        partner_ref = order.partner_id and order.partner_id.ref or None
        delmar_id = line.sale_line_id.product_id and line.sale_line_id.product_id.default_code or None
        prod_size = line.sale_line_id.size_id and line.sale_line_id.size_id.name
        tr_prod_size = line.size_id and line.size_id.name
        tr_delmar_id = line.product_id and line.product_id.default_code or None
        four_digit_size = self.pool.get('ring.size').convert_to_4digit_name(cr, uid, tr_prod_size)
        price_unit = s_line.price_unit and float(s_line.price_unit) or 0
        if line.is_set_component and not return_qty:
            price_unit = s_line.price_unit * line.sale_line_id.product_uom_qty

        def concat_delmar_id_size(delmar_id, size):
            return (delmar_id + "/" + size) if delmar_id and size else delmar_id

        itemid = concat_delmar_id_size(delmar_id, prod_size)
        itemid_long_size = concat_delmar_id_size(tr_delmar_id, four_digit_size)

        trn_id_sql = """SELECT ID from dbo.Transactions
            where invoiceno = '%s'
                and ITEMID = '%s'
        """ % (order_name, itemid_long_size)
        trn_id_res = server_data.make_query(
            cr, uid, server_id,
            trn_id_sql, select=True, commit=False
        )

        four_digit_size = self.pool.get('ring.size').convert_to_4digit_name(cr, uid, prod_size)
        itemid_long_size = concat_delmar_id_size(delmar_id, four_digit_size)

        if not trn_id_res and not line.is_set_component and not line.is_set:
            return False
        transaction_id = trn_id_res[0][0] if trn_id_res else None

        shp_date = pick.shp_date
        if shp_date and isinstance(shp_date, (str, unicode)):
            try:
                if '.' in shp_date:
                    shp_date = shp_date[:shp_date.find('.')]
                shp_date = datetime.strptime(shp_date, '%Y-%m-%d %H:%M:%S')
            except:
                shp_date = None
        if not shp_date:
            shp_date = datetime.utcnow()
        shp_date = shp_date.replace(microsecond=0)

        year = shp_date.year or None
        month = shp_date.month or None
        quarter = month and (month + 2) / 3 or None

        return_qty_to_insert = (int(return_qty) or None) if return_qty else 0
        if line.is_set_component:
            prod_qty = line.sale_line_id.product_uom_qty
        else:
            prod_qty = line.product_qty
        qty_for_cost = return_qty or prod_qty

        if return_qty:
            utc_time_now = time.strftime('%Y%m%d%H:%M:%S', time.gmtime())
            track_no = 'RET' + utc_time_now + 'U'
            qty = 0
            order_date = datetime.now(timezone('Canada/Central'))
        else:
            track_no = pick.tracking_ref or None
            qty = prod_qty and int(prod_qty) or None
            canada_delta_hour = int(str(abs(int(datetime.now(pytz.timezone('Canada/Central')).strftime('%z'))))[:1])
            order_date = datetime.strptime(order.create_date, '%Y-%m-%d %H:%M:%S')-timedelta(hours=canada_delta_hour)
            pytz.utc.localize(order_date)

        # Sales report
        order_year = order_date.year
        order_month = order_date.month
        order_quarter = month and (month + 2) / 3 or None
        order_week = order_date.isocalendar()[1]  # ISO week number

        # DLMR-1901, salesDivision for Zales
        sub_partner_ref = None
        if partner_ref == 'ZLS':
            zls_map = {
                '0006': 'GRN',
                '0008': 'ZLSO',
                '0010': 'PRP'
            }
            zls_division_ids = self.pool.get('sale.order.fields').search(cr, uid, [
                ('order_id', '=', order.id),
                ('name', '=', 'salesDivision'),
                ('value', 'in', zls_map.keys())
            ])
            if zls_division_ids:
                zls_division = self.pool.get('sale.order.fields').read(cr, uid, zls_division_ids[0], ['value']).get('value', None)
                if zls_division and zls_division in zls_map:
                    sub_partner_ref = zls_map.get(zls_division)
        # END DLMR-1901

        table_name = 'OS_Sales_BU'
        insert_values = {
            'ID_Delmar': delmar_id,
            'Size': four_digit_size,
            'Date': shp_date,
            'Qty': qty,
            'Cost': price_unit * float(qty_for_cost) or 0,
            'order_ID': order_name or None,
            'track_no': track_no,
            'Line_ID': s_line.external_customer_line_id or line_no,
            'Customer_ID': sub_partner_ref or partner_ref,  # DLMR-1901 added sub_partner_ref for ZLS
            'customer_SKU': itemid,
            'customer_CODE': delmar_id,
            'customer_ITEM': itemid,
            'Year': year,
            'Month': month,
            'Quarter': quarter,
            'ItemID': itemid_long_size,
            'trnid': transaction_id,
            'ReturnQuantity': return_qty_to_insert,
            'order_date': order_date,
            'order_year': order_year,
            'order_month': order_month,
            'order_quarter': order_quarter,
            'order_week': order_week,
        }

        result = server_data.insert_record(
            cr, uid, sale_server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        if not result:
            return False

        table_name = 'OS_Sales'
        insert_values.update({
            'status': 0,
            'InvoiceNo': invoice_no,
            'Year': None,
            'Month': None,
        })
        del insert_values['Quarter']  # delete

        result = server_data.insert_record(
            cr, uid, sale_server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        return bool(result)

    def remove_OS_Sales(self, cr, uid, order, invoice_no, sale_server_id):

        server_data = self.pool.get('fetchdb.server')
        remove_os_sales_sql = """
            DELETE FROM dbo.OS_Sales
            WHERE 1=1
                AND Order_ID = '%s'
                AND InvoiceNo = '%s'
            ;""" % (order.sale_id.po_number, invoice_no)
        result = server_data.make_query(cr, uid, sale_server_id, remove_os_sales_sql, select=False, commit=True)

        return result

    def remove_OS_Sales_BU(self, cr, uid, order, invoice_no, sale_server_id, return_flag=False):

        if return_flag:
            return_operand = '>'
        else:
            return_operand = '='
        server_data = self.pool.get('fetchdb.server')
        get_os_sales_sql = """
            SELECT ID_Delmar, order_ID, track_no, "Size", Qty, Date, trnid, ReturnQuantity FROM dbo.OS_Sales_BU osb
            WHERE Order_ID = '{order_id}'
                AND ReturnQuantity {operand} 0
                AND EXISTS ( 
                SELECT 1 FROM dbo.OS_Sales oss
                WHERE InvoiceNo = '{invoice_no}' 
                    AND osb.ID_Delmar=oss.ID_Delmar AND oss."Size"=osb."Size" AND osb."Date"=oss."Date"
                    AND osb.order_ID=oss.order_ID AND osb.track_no=oss.track_no AND osb.trnid=oss.trnid 
                    AND oss.ReturnQuantity=osb.ReturnQuantity );
        """.format(order_id=order.sale_id.po_number, operand=return_operand, invoice_no=order.invoice_no)
        select_result = server_data.make_query(cr, uid, sale_server_id, get_os_sales_sql, select=True, commit=True)

        for item in select_result:
            remove_os_sales_sql = """
                DELETE FROM dbo.OS_Sales_BU
                WHERE ID_Delmar  = '{delmar_id}'
                    AND Order_ID = '{order_id}'
                    AND track_no = '{track_no}'
                    AND "Size"   = '{size}'
                    AND Qty      = '{qty}'
                    AND Date     = '{date}'
                    AND trnid    = '{trnid}'
                    AND ReturnQuantity = '{ret_quan}';
                """.format(delmar_id=item[0], order_id=item[1], track_no=item[2],
                           size=item[3], qty=item[4], date=item[5], trnid=item[6], ret_quan=item[7])
            server_data.make_query(cr, uid, sale_server_id, remove_os_sales_sql, select=False, commit=True)

    def fill_invoice_header(self, cr, uid, shipping_handling, pick, shp_obj, invoice_server_id, tbl_prefix, invoice_no, order_qty, lines_count, order_cost, direct=1, return_qties=False):

        order_params = self.get_order_params(
            cr, uid, pick.id, ['skip_ms_invoice']
        )[pick.id]
        if order_params.get('skip_ms_invoice', False) == '1':
            return True
        sale_order = pick.sale_id
        server_data = self.pool.get('fetchdb.server')
        shipping_address = sale_order.partner_shipping_id
        invoice_address = sale_order.partner_invoice_id
        shp_service = shp_obj and shp_obj[0].service_id
        carrier_name = shp_service and shp_service.name or ''
        shp_type = shp_obj and shp_obj[0].type_id
        shp_type_name = shp_type and shp_type.name or ''

        taxes_obj = self.pool.get("delmar.sale.taxes")
        invoice_obj = invoice(cr, uid, 'test', context={})
        discount = invoice_obj._get_discount(pick)
        freight = invoice_obj._get_freight(pick)
        sale_taxes = taxes_obj.compute_stock_picking_taxes(cr, uid, pick.id, discount, freight)

        gst_value = sale_taxes['gst'] or 0.0
        gst_value += sale_taxes['hst'] or 0.0
        pst_value = sale_taxes['pst'] or 0.0
        amount_tax = sale_taxes.get('amount_tax', False) or 0
        if sale_taxes['qst']:
            pst_value += sale_taxes['qst']

        flg = 1
        so_partner = sale_order.partner_id

        manual_shipping_handling = self.check_manual_shipping_handling(
            cr, uid,
            context={
                'ext_cust_id': pick.real_partner_id.external_customer_id,
                'carrier': pick.carrier,
            })
        if(manual_shipping_handling):
            shipping_handling = manual_shipping_handling

        if not direct or so_partner.invoice_without_ship_cost:
            shipping_handling = 0.0

        total_qty = 0
        shipped_order_cost = 0.0
        # recalculate order_cost for Returns and customers
        # with Acc_Price invoice cost
        if pick.real_partner_id.invoice_cost_from_acc_price or not direct:
            order_cost = 0.0
            for line in pick.move_lines:
                sale_line = line.sale_line_id
                price_unit = sale_line and sale_line.price_unit or 0.0
                if line.is_set_component:
                    line_qty = line.sale_line_id.product_uom_qty
                    price_unit = sale_line.price_unit * line_qty
                else:
                    line_qty = line.product_qty
                if not direct:
                    # calculate qty for partial returns
                    if return_qties:
                        qty = return_qties.get(line.id, 0)
                    else:
                        qty = line_qty
                    total_qty += qty
                    order_cost += price_unit * float(qty)
                    shipped_order_cost += price_unit * line_qty
                else:
                    order_cost += line_qty * (
                        self.get_acc_price_price(cr, uid, line.id) or price_unit
                    )

        order_cost -= discount
        if not direct:
            flg = 0
            order_cost *= -1.0
            shipping_handling *= -1.0
            proportion = -1.0
            if shipped_order_cost:
                proportion = order_cost / shipped_order_cost
            gst_value *= proportion
            pst_value *= proportion
            amount_tax *= proportion

        total_ship_cost = order_cost + shipping_handling + amount_tax

        if so_partner.ref == 'ICE':
            total_ship_cost = float(order_cost) + float(sale_order.tax)
            total_ship_cost += float(sale_order.shipping)

        state = shipping_address and shipping_address.state_id
        state_code = state and state.code or None
        insert_values = {
            'PO_ID': sale_order.name,
            'InvoiceNo': invoice_no,
            'MerchantID': so_partner.ref or None,
            'TrackingNo': pick.tracking_ref or None,
            'Address1': shipping_address and shipping_address.street or None,
            'Address2': shipping_address and shipping_address.street2 or None,
            'City': shipping_address and shipping_address.city or None,
            'State': state_code,
            'Zip': shipping_address and shipping_address.zip or None,
            'B_Address1': invoice_address and invoice_address.street or None,
            'B_Address2': invoice_address and invoice_address.street2 or None,
            'B_City': invoice_address and invoice_address.city or None,
            'B_State': state_code,
            'B_Name': invoice_address and invoice_address.name or None,
            'FullName': shipping_address and shipping_address.name or None,
            'B_Zip': invoice_address and invoice_address.zip or None,
            'OrderCost': order_cost,
            'OrderQty': int(total_qty or order_qty),
            'Items': int(lines_count),
            'Shipping_Handling': shipping_handling,
            'Total_Ship_Cost': total_ship_cost,
            'ServiceTypeName': (carrier_name + " " + shp_type_name) or None,
            'CarrierName': carrier_name or None,
            'CarrierID': None,
            'ShipDate': pick.shp_date or None,
            'ShippingWhse': 'USCHP',
            'InvoiceDate': pick.shp_date or {'subquery': 'getdate()'},
            'InvoiceDate2': pick.shp_date or {'subquery': 'getdate()'},
            'OrderDate': {'subquery': 'getdate()'},
            'imported': 0,
        'InvoiceDate1': pick.shp_date or {'subquery': 'getdate()'},
            'Flg': flg,
            'GST': gst_value,
            'PST': pst_value,
            'date_entered': {'subquery': 'getdate()'},
            'Discount': discount,
            'tax': amount_tax,
        }

        keys = insert_values.keys()
        si_obj = self.pool.get('sale.integration')
        additional_information = si_obj.getAdditionalInvoiceInformation(
            cr, uid, sale_order.id, insert_values, {
                'customer_id': so_partner.id,
            }
        )

        if additional_information:
            insert_values.update({
                # Secure filter to not add new keys in insert dict
                k: v for k, v in additional_information.items() if k in keys
            })

        table_name = 'gmotoc.%sInvoice_Header' % (tbl_prefix)

        result = server_data.insert_record(
            cr, uid, invoice_server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        return bool(result)

    def remove_invoice_header(self, cr, uid, invoice_server_id, tbl_prefix, po_number, tracking_ref, invoice_no, direct=1):
        server_data = self.pool.get('fetchdb.server')
        remove_failed_header_sql = """
            DELETE FROM gmotoc.%sInvoice_Header
            WHERE 1=1
                AND PO_ID = '%s'
                AND TrackingNo = '%s'
                AND flg = %s
                AND InvoiceNo = '%s'
            ;""" % (tbl_prefix, po_number, tracking_ref, direct, invoice_no)
        result = server_data.make_query(cr, uid, invoice_server_id, remove_failed_header_sql, select=False, commit=True)

        return result

    def get_shipping_server_id(self, cr, uid, ids, context=None):

        res = {}

        if ids:

            if isinstance(ids, (int, long, )):
                ids = [ids]

            cr.execute("""  SELECT
                                DISTINCT ON (sp.id)
                                    sp.id AS order_id
                                    , cs.server_id AS server_id
                                    /*
                                    -- debug-info
                                        , rp.name AS partner
                                        , rp.ship_by_country AS ship_by_country
                                        , sw.name AS warehouse
                                        , rc.name AS country
                                        , fs.name AS server
                                    -- end debug-info
                                    */
                            FROM stock_picking sp
                                LEFT JOIN country_shipping cs ON (
                                    sp.warehouses = cs.warehouse_id
                                )
                                lEFT JOIN res_partner_address ra ON ra.id = sp.address_id
                                LEFT JOIN res_partner rp ON rp.id = sp.real_partner_id
                                /*
                                -- debug info
                                    LEFT JOIN stock_warehouse sw ON sw.id = cs.warehouse_id
                                    LEFT JOIN res_country rc ON ra.country_id = rc.id
                                    LEFT JOIN fetchdb_server fs ON fs.id = cs.server_id
                                -- end debug-info
                                */
                            WHERE 1=1
                                AND sp.id in %s
                                AND sp.warehouses IS NOT NULL
                                AND (( 1=1
                                        AND rp.ship_by_country = True
                                        AND cs.partner_id IS NOT NULL AND sp.real_partner_id = cs.partner_id
                                        AND cs.country_id IS NOT NULL AND ra.country_id = cs.country_id
                                    ) OR ( 1=1
                                        -- AND rp.ship_by_country = False
                                        AND cs.partner_id IS NULL
                                        AND cs.country_id IS NULL
                                ))
                            ORDER BY
                                sp.id,
                                COALESCE(cs.country_id, 0) DESC,
                                COALESCE(cs.warehouse_id, 0) DESC
                            ;
            """, (tuple(ids), ))
            result = cr.fetchall()
            for row in result:
                res[row[0]] = row[1]

        return res

    def get_shipping_warehouses(self, cr, uid, ids, context=None):
        res = {}

        if ids:

            if isinstance(ids, (int, long, )):
                ids = [ids]

            cr.execute("""  SELECT
                                  sp.id AS order_id
                                , ARRAY_TO_STRING(ARRAY_ACCUM(cs.warehouse_id), ',') AS warehouse_ids
                            FROM stock_picking sp
                                lEFT JOIN res_partner_address ra ON ra.id = sp.address_id
                                LEFT JOIN res_partner rp ON rp.id = sp.real_partner_id
                                LEFT JOIN country_shipping cs ON ( 1=1
                                    AND cs.country_id = ra.country_id
                                    AND cs.partner_id = rp.id
                                    AND rp.ship_by_country = true
                                )
                            WHERE 1=1
                                AND sp.id IN %s
                            GROUP BY sp.id
                            ;
            """, (tuple(ids), ))
            result = cr.fetchall()
            for row in result:
                res[row[0]] = [int(x) for x in (row[1] or '').split(',') if x]

        return res

    def _get_shp_account_from_order(self, cr, uid, order_id):
        if not order_id:
            return False
        sale_order = self.browse(cr, uid, order_id).sale_id
        for item in sale_order.additional_fields:
            if item.name == 'account_number':
                return item.value or False

    def get_shp_account(self, cr, uid, order_id, shp_service, context=None):

        account = False

        # FIXME: bad hardcode
        available_services = [
            'fedex',
            'ups',
            'usps',
            'ca post',
            'canpar',
            'loomis',
            'generic',
            'generic1',
            'generic2',
            'generic3',
            'generic4'
        ]

        shp_service = (shp_service or '').strip().lower()

        if order_id and shp_service:
            # FIXME: bad hardcode with replcing. It makes from 'canada post' to 'ca post'
            shp_service = shp_service.replace('nada', '')
            for service in available_services:
                regex = re.compile(r"(?i).*(\b%s\b)" % (service), re.IGNORECASE)
                if bool(regex.match(shp_service)):
                    cr.execute("""  SELECT
                                        acc.id
                                        /* -- debug
                                        , acc.s2s
                                        , so.ship2store
                                        , sp.id
                                        , sp.name AS "order"
                                        , rp.ship_by_country AS ship_by_country
                                        , rco.name AS order_country
                                        , rca.name AS account_country
                                        -- end debug */
                                    FROM stock_picking sp
                                        LEFT JOIN sale_order so ON so.id = sp.sale_id
                                        LEFT JOIN res_partner rp ON rp.id = sp.real_partner_id
                                        LEFT JOIN res_partner_address addr ON addr.id = sp.address_id
                                        INNER JOIN res_partner_account acc ON (1=1
                                            AND acc.partner_id = sp.real_partner_id
                                            AND acc.service = %s
                                            AND COALESCE(acc.country_id, 0) IN (addr.country_id, 0)
                                            and ((so.ship2store is true)  or (so.ship2store is not true and acc.s2s is not true))
                                        )
                                        /* --debug
                                        left join res_country rca on acc.country_id = rca.id
                                        left join res_country rco on addr.country_id = rco.id
                                        -- end debug*/
                                    WHERE 1=1
                                        AND sp.id = %s
                                    ORDER BY acc.country_id,(case when acc.s2s is not true then false else true end) desc
                                    ;
                    """, (service, order_id))
                    res = cr.fetchone()
                    if res:
                        account_obj = self.pool.get('res.partner.account')
                        account = account_obj.browse(cr, uid, res[0])

                    break
        return account

    def print_bulk_label(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        context.update({'target': 'mark'})

        res = self.mark_bulk_order(cr, uid, ids, context=context)
        if res is True:

            self.write(cr, uid, ids[0], {
                'printed_bulk_label_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            })

            data_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.report.xml')

            category = data_obj.get_object(cr, uid, 'delmar_sale', 'cm_report_categ_ol')
            act_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'sterling_ss_report')
            act_id = data_obj.read(cr, uid, act_res_id, ['res_id'])
            res = act_obj.read(cr, uid, act_id['res_id'], [], context=context)
            res.update({
                'res_id': ids[0],
                'context': {
                    'categ_id': category.id,
                }
            })

        return res

    def action_add_to_balk(self, cr, uid, ids, context=None):
        return self.mark_bulk_order(cr, uid, ids, target='mark', context=context)

    def action_remove_from_bulk(self, cr, uid, ids, context=None):
        return self.mark_bulk_order(cr, uid, ids, target='unmark', context=context)

    def mark_bulk_order(self, cr, uid, ids, target=False, show_warnings=True, context=None):
        if context is None:
            context = {}

        warnings = []
        to_add = []
        to_remove = []

        shp_bulk_id = context.get('shp_bulk_id', False)
        if shp_bulk_id:
            for order in self.read(cr, uid, ids, ['shp_bulk_id', 'name']):

                current_bulk_id = order and order.get('shp_bulk_id', False) or False
                current_bulk_id = current_bulk_id and current_bulk_id[0] or False

                if current_bulk_id:
                    if current_bulk_id != shp_bulk_id:
                        warnings.append(order['name'])
                    elif current_bulk_id == shp_bulk_id:
                        if target == 'unmark':
                            to_remove.append(order['id'])
                elif target == 'mark':
                    to_add.append(order['id'])

            if to_remove:
                self.write(cr, uid, to_remove, {
                    'shp_bulk_id': False,
                    'printed_bulk_label_date': False,
                    })

            if to_add:
                self.write(cr, uid, to_add, {
                    'shp_bulk_id': shp_bulk_id,
                    })

            if to_remove or to_add:
                self.pool.get('stock.picking.bulk').generate_packing_xls(cr, uid, [shp_bulk_id])

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_write(uid, 'stock.picking.bulk', shp_bulk_id, cr)

        if warnings:
            warning_str = 'Following order(s) already used in other bulk: %s.' % (', '.join(warnings))
            if show_warnings:
                return self.pool.get('ir.notification').warning(
                    cr, uid,
                    'Warning',
                    warning_str,
                )
            else:
                return warning_str

        return True

    def check_manual_shipping_handling(self, cr, uid, context=None):
        if(context is None):
            context = {}
        ext_cust_id = context.get('ext_cust_id', False)
        carrier = context.get('carrier', False)
        manual_shipping_handling = None
        if(ext_cust_id and carrier):
            list_manual_shp_handling = []
            try:
                shp_hand_for_cust_obj = self.pool.get('ir.config_parameter').get_param(cr, uid, 'shipping_handling_for_customers')
                shp_hand_for_cust = eval(shp_hand_for_cust_obj) if shp_hand_for_cust_obj else {}
                for _ext_cust_id in shp_hand_for_cust:
                    if(ext_cust_id == _ext_cust_id):
                        list_manual_shp_handling = shp_hand_for_cust[_ext_cust_id]
                if(list_manual_shp_handling):
                    carrier_strip_lower = str(carrier).lower().replace(' ', '')
                    for sub_carrier_str in list_manual_shp_handling:
                        if(str(sub_carrier_str).lower().replace(' ', '') in carrier_strip_lower):
                            manual_shipping_handling = list_manual_shp_handling[sub_carrier_str]
            except Exception as ex:
                _msg = "Failed find manual shipping handling: \n\n{0}".format(ex.message)
                logger.warn(_msg)
                manual_shipping_handling = False
            finally:
                if(manual_shipping_handling):
                    if(isinstance(manual_shipping_handling, int)):
                        manual_shipping_handling = float(manual_shipping_handling)
                    if(not isinstance(manual_shipping_handling, float)):
                        logger.warn('Bad value manual shipping handling: {0}'.format(manual_shipping_handling))
                        manual_shipping_handling = False
        return manual_shipping_handling

    def action_simple_report(self, cr, uid, ids, context=None):
        def __handle_id(report_id):
            """Query report for order with given id.
            @:param report_id - report id as a str.
            @:return query result as a list"""
            cr.execute(
                "SELECT 1 FROM stock_picking sp WHERE sp.id in %s and sp.is_set and sp.state not in ('done', 'cancel');",
                (tuple([report_id]),))
            res = cr.dictfetchone()
            if res:
                cr.execute("""
                    select
                        sp.name as order_no,
                        concat('''',coalesce(sp.tracking_ref, sp.name)) as package,
                        to_char(so.create_date,'DD/MM/YY') as "date",
                        concat(pp.default_code, case when rs.id is null then '' else concat('/', rs.name) end) as style,
                        coalesce(pp.custom_description, pp.short_description, ol.name, pp.name_template) as description,
                        sm.product_qty as qty,
                        CASE WHEN
                            CAST( COALESCE( ol."customerCost", '0.0') as float ) > 0.0 THEN ol."customerCost"
                            -- start DLMR-321
                            WHEN
                            (CAST( COALESCE( ol."customerCost", '0.0') as float ) = 0.0
                            and sp.real_partner_id in (48,22,4,23,596,597,598,49,51,131,50,173,224,144,944,38,89,88,100,872,43,870,881,130,208,924,936))
                            then concat('(', ol.price_unit, ')')
                            ELSE '0.0'
                            -- end DLMR-321
                        END as price,
                        prod_manufacture as made_in,
                        pt.prod_tarif_classification as tariff,
                        ra.name as cs_name,
                        ra.street as cs_street,
                        ra.street2 as cs_street2,
                        ra.city as cs_city,
                        cs.name as cs_state,
                        ra.zip as cs_zip,
                        rc.name as cs_country,
                        ra.phone as cs_phone,
                        sp.carrier as shp_carrier,
                        (sp.shp_date - interval '5 hours') as shp_date
                    from stock_picking sp
                        left join sale_order so on so.id = sp.sale_id
                        left join stock_move sm on sp.id = set_parent_order_id
                        left join product_product pp on pp.id = sm.product_id
                        left join product_template pt on pt.id = pp.product_tmpl_id
                        left join ring_size rs on rs.id = sm.size_id
                        left join sale_order_line ol on ol.id = sm.sale_line_id
                        left join res_partner_address ra on ra.id = sp.address_id
                        left join res_country rc on rc.id = ra.country_id
                        left join res_country_state cs on cs.id = ra.state_id
                    where sp.id in %s
                    order by
                        so.name,
                        sp.name,
                        style
                    ;
                """, (tuple([report_id]),)
                           )
            else:
                cr.execute("""
                                    select
                                        sp.name as order_no,
                                        concat('''',coalesce(sp.tracking_ref, sp.name)) as package,
                                        to_char(so.create_date,'DD/MM/YY') as "date",
                                        concat(pp.default_code, case when rs.id is null then '' else concat('/', rs.name) end) as style,
                                        coalesce(pp.custom_description, pp.short_description, ol.name, pp.name_template) as description,
                                        sm.product_qty as qty,
                                        CASE WHEN
                                            CAST( COALESCE( ol."customerCost", '0.0') as float ) > 0.0 THEN ol."customerCost"
                                            -- start DLMR-321
                                            WHEN
                                            (CAST( COALESCE( ol."customerCost", '0.0') as float ) = 0.0
                                            and sp.real_partner_id in (48,22,4,23,596,597,598,49,51,131,50,173,224,144,944,38,89,88,100,872,43,870,881,130,208,924,936))
                                            then concat('(', ol.price_unit, ')')
                                            ELSE '0.0'
                                            -- end DLMR-321
                                        END as price,
                                        prod_manufacture as made_in,
                                        pt.prod_tarif_classification as tariff,
                                        ra.name as cs_name,
                                        ra.street as cs_street,
                                        ra.street2 as cs_street2,
                                        ra.city as cs_city,
                                        cs.name as cs_state,
                                        ra.zip as cs_zip,
                                        rc.name as cs_country,
                                        ra.phone as cs_phone,
                                        sp.carrier as shp_carrier,
                                        (sp.shp_date - interval '5 hours') as shp_date
                                    from stock_picking sp
                                        left join sale_order so on so.id = sp.sale_id
                                        left join stock_move sm on sp.id = sm.picking_id
                                        left join product_product pp on pp.id = sm.product_id
                                        left join product_template pt on pt.id = pp.product_tmpl_id
                                        left join ring_size rs on rs.id = sm.size_id
                                        left join sale_order_line ol on ol.id = sm.sale_line_id
                                        left join res_partner_address ra on ra.id = sp.address_id
                                        left join res_country rc on rc.id = ra.country_id
                                        left join res_country_state cs on cs.id = ra.state_id
                                    where sp.id in %s
                                    order by
                                        so.name,
                                        sp.name,
                                        style
                                    ;
                                """, (tuple([report_id]),)
                           )
            __handle_id.info.extend(cr.dictfetchall() or [])

            cr.execute("select sp.total_price as total_price from stock_picking sp where sp.id in %s;", (tuple([report_id]),))
            result = cr.dictfetchone()
            __handle_id.count_orders += 1 if result['total_price'] >= THRESHOLD_PRICE else 0

        res = []

        __handle_id.info = []
        __handle_id.count_orders = 0
        map(__handle_id, ids)

        import string
        header = (
            'Style#',
            'Description',
            'QTY',
            'Date Of Order',
            'Order No',
            'Origin',
            'Tariff',
            'Customer name and address',
            'Price',
            'Packages',
            'Carrier',
            'Ship Date'
        )
        header_length = len(header)

        shipper_info = [
            'Shipper:',
            '3385175 Canada Inc',
            '4058 Jean Talon O',
            'Montreal, QC',
            'H4P 1V5, Canada',
            '(514) 875-4800 Ext 111',
            ''
        ]
        for i in shipper_info:
            res.append([i] + ['']*(header_length-1))

        res.append(header)
        if __handle_id.info:
            cs_fields = [
                'cs_name',
                'cs_street',
                'cs_street2',
                'cs_city',
                'cs_state',
                'cs_zip',
                'cs_country',
                'cs_phone',
            ]
            for line in __handle_id.info:
                made_in = (line.get('made_in', False) or '').strip().lower()
                if made_in == 'hong kong':
                    made_in = 'china'
                made_in = string.capitalize(made_in)
                res.append((
                    line['style'],
                    line['description'],
                    line['qty'],
                    line['date'],
                    line['order_no'],
                    made_in,
                    line['tariff'],
                    " ".join(line[x].strip() for x in cs_fields if (line[x] or '').strip()),
                    line['price'],
                    line['package'],
                    line['shp_carrier'],
                    line['shp_date']
                ))

        cr.execute("""
            select count(distinct(coalesce(sp.tracking_ref, sp.name))) as count
            from stock_picking sp
            where sp.id in %s
        """, (tuple(ids), )
        )
        count_res = cr.fetchone()
        count = count_res and count_res[0] or 0
        summary_count = [
            'Package count:',
            count,
        ]
        summary_weight = [
            'Package weight:',
            "%s LB." % count,
        ]
        res.append([''] * (header_length - len(summary_count)) + summary_count)
        res.append([''] * (header_length - len(summary_weight)) + summary_weight)
        return res, __handle_id.count_orders

    def action_bad_format_tracking(self, cr, uid, _id, context=None):
        self.write(cr, uid, _id, {'state': 'bad_format_tracking'}, context=context)
        return True

    def add_message_to_note(self, cr, uid, id_, message=None, context=None):
        id_ = id_ and id_[0] if isinstance(id_, (list, tuple)) else id_ or None
        if(context is None):
            context = {}
        if(message and id_):
            date_now = "{:%Y-%m-%d %H:%M:%S}".format(datetime.now())
            note = self.browse(cr, uid, id_).note or ''
            if isinstance(message, (list, tuple)):
                for msg in message:
                    note += '\n{0} {1}'.format(date_now, msg)
            else:
                note += '\n{0} {1}'.format(date_now, message)
            self.write(cr, uid, id_, {'note': note}, context=context)
        return True

    def write_box_id(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sl_obj = self.pool.get('stock.location')
        spm_obj = self.pool.get('stock.packing.material')
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]
            ids_str = ','.join([unicode(id_) for id_ in ids])
            sql = '''SELECT
                        sp.id AS sp_id,
                        sm.location_id AS location_id,
                        sol.product_id AS product_id,
                        sp.real_partner_id AS partner_id,
                        pt.categ_id AS categ_id,
                        CASE
                            WHEN str_to_float(sol."customerCost") != 0
                            THEN str_to_float(sol."customerCost")
                            ELSE sol.price_unit
                        END AS "value",
                        sm.id AS sm_id
                    FROM
                        stock_picking AS sp
                    LEFT JOIN
                        stock_move AS sm
                            ON sm.picking_id = sp.id
                    LEFT JOIN
                        sale_order_line AS sol
                            ON sm.sale_line_id = sol.id
                    LEFT JOIN
                        product_product AS pp
                            ON sol.product_id = pp.id
                    LEFT JOIN
                        product_template AS pt
                            ON pp.product_tmpl_id = pt.id
                    WHERE 1=1
                        AND sp.id IN ({ids})
                    ORDER BY sp_id
            '''.format(ids=ids_str)
            cr.execute(sql)
            rows = cr.dictfetchall()
            update_sql = ''
            product_obj = self.pool.get('product.product')
            for row in rows:
                row['warehouse_id'] = sl_obj.get_warehouse(cr, uid, row['location_id'])
                brand = product_obj.get_val_by_label(cr, uid, row['product_id'], False, 'Brand', False) or False
                box_id = spm_obj.get_packing_material(
                    cr, uid, row['warehouse_id'],
                    row['product_id'], row['categ_id'],
                    row['partner_id'], row['value'], brand, submaterials=['box'], location_id=row['location_id']
                )[0]['box_id']
                if box_id and row['sm_id']:
                    # DLMR-963
                    # create hidden stock_move for box product
                    logger.info("Write moveline for box")
                    box_move_id, old_box_move_id = spm_obj.write_box_moveline(cr, uid, box_id=box_id, move_id=row['sm_id'], context=None)
                    box_fix_id = box_move_id or old_box_move_id
                    box_moveline = self.pool.get('stock.move').browse(cr, uid, box_fix_id)
                    if box_move_id or (old_box_move_id and box_moveline.state in ['cancel', 'assigned']):
                        logger.info("Write transaction of new moveline for box")
                        if not spm_obj.write_box_transaction(cr, uid, box_move_id=box_fix_id, sign=-1, transactcode=SIGN_DEFAULT_VALUE, context=None):
                            logger.warn('Failed to write transaction for BOX')
                        # update box_moveline state
                        self.pool.get('stock.move').write(cr, uid, box_fix_id, {
                            'state': 'done',
                        })
                    else:
                        logger.warn('Move line for BOX was not created')
                    # END DLMR-963

                    # add sql to update product's stock_move
                    update_sql += '''UPDATE
                                        stock_move
                                    SET box_id = {box_id}
                                    WHERE id = {sm_id};\n
                    '''.format(box_id=box_id, sm_id=row['sm_id'])

            if update_sql:
                try:
                    # set box_id to product stock_move
                    cr.execute(update_sql)
                except Exception, e:
                    logger.error(e)
        return True

    def write_unique_fields_for_orders(self, cr, uid, ids):
        if isinstance(ids, (int, long)):
            ids = [ids]
        si_obj = self.pool.get('sale.integration')
        for picking in self.browse(cr, uid, ids):
            si_obj.write_unique_fields_for_orders(cr, uid, picking)
        return True

    def get_ms_customer_price(self, cr, picking, customer_ref):
        server_obj = self.pool.get('fetchdb.server')
        adm_uid = 1
        server_id = server_obj.get_price_servers(cr, adm_uid, single=True)
        price = 0.0
        if not server_id:
            raise osv.except_osv('Warning', 'Remote server is not available.')
        query = """SELECT top 1 Price FROM CustomerPrice
            WHERE DelmarID = ?
                AND Customer = ?
            ORDER BY Date DESC
        """
        for move in picking.move_lines:
            params = [move.product_id.default_code, customer_ref]
            query_res = server_obj.make_query(
                cr, adm_uid, server_id,
                query, params=params, select=True
            )
            move_price = None
            if query_res:
                try:
                    move_price = str2float(query_res[0][0])
                except:
                    pass
            if not move_price:
                line = move.sale_line_id
                move_price = str2float(line.customerCost, 0.0) or line.price_unit or 0.0
            price += move_price * move.product_qty
        return price

    def is_321(self, cr, picking, price=None):
        result = False
        _321_price = None
        _321_price_str = self.pool.get('ir.config_parameter').get_param(cr, 1, '321.price')
        try:
            _321_price = _try_parse(to_ascii(_321_price_str), 'float')
            if not _321_price:
                raise Exception("321 Price can't be empty!")
        except Exception, e:
            _321_price = 800
            logger.error(
                "321 price (\"321.price\") system parameter wrong specified. Used hardcoded %s value.\nError: %s" % (_321_price_str, e)
            )
        if price is None:
            customer_ref = picking.real_partner_id.ref
            try:
                price = picking.order_321_price
            except Exception, e:
                logger.error(
                    "Can't get data from Customers Price table: %s" % (e,)
                )
            if not price:
                price = self.get_ms_customer_price(cr, picking, customer_ref)

        if (price is None) or (price and price < _321_price and not picking.combined):
            cr.execute("""SELECT id
                    FROM stock_picking
                    WHERE address_grp = %s
                        AND id <> %s
                        AND create_date > now() at time zone 'utc' - interval '1 day'
                        AND state in (
                            'picking',
                            'outcode_except',
                            'bad_format_tracking',
                            'shipped',
                            'done'
                        )
                    LIMIT 1
                """, (picking.address_grp, picking.id))
            res = cr.fetchone()
            if not (res and res[0]):
                result = True

        return result

stock_picking()


class return_product_history(osv.osv):
    _name = "return.product.history"
    _description = "Return Product History"

    _columns = {
        'returned_location_id': fields.many2one('stock.location', 'Returned Location'),
        'bin_id': fields.many2one('stock.bin', 'Returned bin'),
        'picking_id': fields.many2one('stock.picking', 'Picking'),
        'product_id': fields.many2one('product.product', 'Product'),
        'size_id': fields.many2one('ring.size', 'Size'),
        'sale_id': fields.many2one('sale.order', 'Sale'),
        'qty': fields.integer('Quantity'),
        'reason': fields.text('Reason'),
        'set_component': fields.boolean('Set component'),
    }
    _defaults = {
        'set_component': False
    }

return_product_history()
