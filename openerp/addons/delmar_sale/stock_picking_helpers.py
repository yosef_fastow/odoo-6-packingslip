# -*- coding: utf-8 -*-
##############################################################################
#
#    ISDDesign
#    Copyright (C) 2017
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
import re
import logging
_logger = logging.getLogger(__name__)


class StockPickingHelper(osv.osv_memory):
    _name = 'stock.picking.helper'

    def MSSQLInventorySelect(self, cr, uid, sql=None, params=None):
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        data = server_obj.make_query(cr, uid, server_id, sql=sql, params=params, select=True, commit=False)
        return data

    def MSSQLInventoryExecute(self, cr, uid, sql=None, params=None):
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        result = server_obj.make_query(cr, uid, server_id, sql=sql, params=params, select=False, commit=True)
        return result

    def check_product_is_set(self, cr, uid, product_id=None):
        # check product is a SET
        if not product_id:
            return False
        set_query = """select nullif(count(*), 0) as is_set from product_set_components 
            WHERE parent_product_id=%(product_id)s"""
        cr.execute(set_query, {'product_id': product_id})
        is_set = cr.fetchone()[0]
        _logger.warn('Check product is set: %s' % is_set)
        if is_set is None:
            return False
        else:
            return True

    def get_component_set_qty(self, cr, uid, cmp_id=None, product_id=None):
        cmp_ids = self.pool.get('product.set.components').search(cr, uid, [('parent_product_id', '=', product_id), ('cmp_product_id', '=', cmp_id)], limit=1)
        if len(cmp_ids) > 0 and cmp_ids[0]:
            component = self.pool.get('product.set.components').browse(cr, uid, cmp_ids[0])
            if component and component.qty:
                return component.qty
        return 0

    def get_set_product_components(self, cr, uid, product_id=None, picking_id=None, context=None):
        components = []
        stock_obj = self.pool.get('stock.move')
        # try to get components from picking's stock_moves
        if picking_id:
            moves = stock_obj.search(cr, uid, [('set_parent_order_id', '=', picking_id), ('set_product_id', '=', product_id)])
            if len(moves) > 0:
                for move in stock_obj.browse(cr, uid, moves):
                    cmp = {
                        'product_id': move.product_id.id,
                        'default_code': move.product_id.default_code,
                        'product_description': move.product_id.short_description,
                        'qty': move.product_qty,
                        'sizeable': move.product_id.sizeable or move.product_id.ring_size_ids or False,
                        'size_id': move.size_id and move.size_id.id or False
                    }
                    components.append(cmp)
        # if no components were found in stock_moves, try to get them from product_set_components
        if len(components) == 0:
            cmp_ids = self.pool.get('product.set.components').search(cr, uid, [('parent_product_id', '=', product_id)])
            if len(cmp_ids) > 0:
                cmps = self.pool.get('product.set.components').browse(cr, uid, cmp_ids)
                for cmp_record in cmps:
                    cmp = {
                        'product_id': cmp_record.cmp_product_id.id,
                        'default_code': cmp_record.cmp_product_id.default_code,
                        'product_description': cmp_record.cmp_product_id.short_description,
                        'qty': cmp_record.qty,
                        'sizeable': cmp_record.cmp_product_id.sizeable or cmp_record.cmp_product_id.ring_size_ids or False,
                        'size_id': None
                    }
                    components.append(cmp)
        # return list of dicts
        return components

    def get_set_component_source_location(self, cr, uid, picking_id=None, component_id=None):
        if not component_id:
            return False
        if not picking_id:
            return False

        stock_obj = self.pool.get('stock.move')
        size_obj = self.pool.get('ring.size')

        res_obj = {
            'product_id': component_id,
            'product_name': None,
            'size_id': None,
            'size_name': None,
            'warehouse_id': None,
            'warehouse_name': None,
            'location_id': None,
            'location_name': None,
            'location_complete_name': None,
            'bin_id': None,
            'bin_name': None
        }

        moves_params = [('set_parent_order_id', '=', picking_id), ('product_id', '=', component_id), ('is_set_component', '=', True)]
        moves = stock_obj.search(cr, uid, moves_params)
        if moves and len(moves) > 0:
            move = stock_obj.browse(cr, uid, moves[0])
            res_obj.update({
                'product_name': move.product_id.default_code,
                'size_id': move.size_id.id,
                'size_name': size_obj.get_4digit_name(cr, uid, move.size_id.id, context=None),
                'warehouse_id': move.location_id.warehouse_id.id,
                'warehouse_name': move.location_id.warehouse_id.name,
                'location_id': move.location_id.id,
                'location_name': move.location_id.name,
                'location_complete_name': move.location_id.complete_name,
                'bin_id': move.bin_id.id,
                'bin_name': move.bin_id.name
            })
            return res_obj

        return None

    # DLMR-1921
    # Switch CAFER to CAFERNDP
    def get_caferndp_location(self, cr, uid, cafer_location_id=None):
        if not cafer_location_id:
            raise Exception("CAFER source location isn't defined")
        loc_obj = self.pool.get('stock.location')
        wh_obj = self.pool.get('stock.warehouse')

        # get CAFERNDP warehouse object
        wh_ids = wh_obj.search(cr, uid, [('name', '=', 'CAFERNDP')])
        if not wh_ids:
            raise Exception("CAFERNDP warehouse isn't defined in database")
        ndp = wh_obj.browse(cr, uid, wh_ids[0])

        # get CAFER warehouse and souce location objects
        cafer_location = loc_obj.browse(cr, uid, cafer_location_id)
        cafer_location_path_ids = loc_obj.search(cr, uid, [
            ('parent_left', '<=', cafer_location.parent_left),
            ('parent_right', '>=', cafer_location.parent_right),
            ('parent_left', '!=', 0)
        ], order='parent_right desc')
        if (not cafer_location_path_ids) or len(cafer_location_path_ids) < 1:
            raise Exception("Invalid location")

        # Hardcoded root 'Physical Locations' ID
        parent_location_id = 1
        # Iterate over nested tree path
        for cafer_item in loc_obj.browse(cr, uid, cafer_location_path_ids):
            _logger.info("Checking %s" % cafer_item.name)
            vals_to_check = {
                'location_id': parent_location_id
            }
            if cafer_item.name == 'CAFER':
                vals_to_check.update({'name': 'CAFERNDP'})
            else:
                vals_to_check.update({
                    'name': cafer_item.name,
                    'warehouse_id': ndp.id
                })
            # check line for NDP exists and create if not
            parent_location_id = self.get_caferndp_location_line(cr, uid, vals=vals_to_check, parent_id=parent_location_id)
            if not parent_location_id:
                raise Exception("Cannot get or create CAFERNDP location line")

        return parent_location_id

    # DLMR-1921
    # get or create CAFERNDP location line
    def get_caferndp_location_line(self, cr, uid, vals=None, parent_id=None):
        criteria = []
        for param, val in vals.iteritems():
            criteria.append((param, '=', val))
        loc_obj = self.pool.get('stock.location')
        _logger.info('Search NDP line criteria: %s' % criteria)
        ids = loc_obj.search(cr, uid, criteria)
        _logger.info("Found location IDS: %s" % ids)
        # create new record if not exists
        if (not ids) or len(ids) == 0:
            ndp_line_vals = {
                'location_id': parent_id,
                'name': vals.get('name', ''),
                'company_id': 1,
                'warehouse_id': vals.get('warehouse_id', None)
            }
            # create and return id
            return loc_obj.create(cr, uid, ndp_line_vals)
        # pass existed line id
        return ids[0]

    def get_product_source_location_mssql(self, cr, uid, warehouse_id=None, warehouse_name=None, location_id=None, location_name=None, product_id=None, product_name=None, size_id=None, size_name=None):
        location_sql = ''
        priority_sql = ''
        params = []
        warehouse = None
        location = None
        res_obj = {
            'product_id': product_id,
            'product_name': product_name,
            'size_id': size_id,
            'size_name': size_name,
            'warehouse_id': warehouse_id,
            'warehouse_name': warehouse_name,
            'location_id': location_id,
            'location_name': location_name,
            'location_complete_name': None,
            'bin_id': None,
            'bin_name': None
        }

        # do warehouse
        wh_obj = self.pool.get('stock.warehouse')
        if warehouse_id:
            warehouse = wh_obj.browse(cr, uid, warehouse_id)
            warehouse_name = warehouse.name
            res_obj.update({'warehouse_name': warehouse_name})
        elif warehouse_name:
            warehouse_name = warehouse_name.strip().upper()
            warehouse_ids = wh_obj.search(cr, uid, [('name', '=ilike', warehouse_name)])
            if warehouse_ids:
                warehouse = wh_obj.browse(cr, uid, warehouse_ids[0])
                res_obj.update({'warehouse_id': warehouse.id})
        else:
            return False

        # do location
        loc_obj = self.pool.get('stock.location')
        if location_id:
            location = loc_obj.browse(cr, uid, location_id)
            res_obj.update({'location_name': location.name})
            res_obj.update({'location_complete_name': location.complete_name})
        elif location_name:
            location_ids = wh_obj.search(cr, uid, [('name', '=ilike', location_name), ('warehouse_id', '=', warehouse.id)])
            if location_ids:
                location = wh_obj.browse(cr, uid, location_ids[0])
                res_obj.update({'location_id': location.id})
                res_obj.update({'location_complete_name': location.complete_name})

        # do size
        size_obj = self.pool.get('ring.size')
        if size_id:
            size_name = size_obj.get_4digit_name(cr, uid, size_id, context=None)
        elif size_name:
            reg_4_dig = re.compile("\d{4}")
            if reg_4_dig.match(size_name):
                size_name = str(float(size_name)/100)
            reg_dig_dot = re.compile("\d{1,2}\.\d{1}")
            reg_dig = re.compile("\d{1,2}")
            size_ids = None
            if reg_dig_dot.match(size_name):
                size_ids = size_obj.search(cr, uid, [('name', '=', size_name)])
            elif reg_dig.match(size_name):
                size_ids = size_obj.search(cr, uid, [('name', '=', '{}.0'.format(size_name))])
            if size_ids and size_ids[0]:
                size_id = size_ids[0]
                size_name = size_obj.get_4digit_name(cr, uid, size_id, context=None)
            else:
                return False
        else:
            size_name = '0000'
        res_obj.update({'size_id': size_id})
        res_obj.update({'size_name': size_name})

        # do product code
        product_obj = self.pool.get('product.product')
        if product_id:
            product = product_obj.read(cr, uid, product_id, ['default_code'], context=None)
            product_name = product['default_code'].upper()
            res_obj.update({'product_name': product_name})
        elif product_name:
            product_name = product_name.strip().upper()
            product_ids = product_obj.search(cr, uid, [('default_code', '=', product_name)])
            product_id = product_ids and product_ids[0] or None
            res_obj.update({'product_id': product_id})
        else:
            return False

        # MAIN PARAMS
        params.append(product_name)
        params.append(size_name)
        params.append(warehouse_name)

        # ADDITIONAL QUERIES
        if location:
            location_sql = "AND stockid = ?"
            location_name = location.name
            params.append(location_name)

        priority_location = warehouse and warehouse.lot_release_id and warehouse.lot_release_id.name or None
        if priority_location:
            priority_sql = "ORDER BY CASE WHEN stockid = ? THEN 0 ELSE 1 END, CASE WHEN TRANSACTIONCODE in ('SLE','SHP') THEN 0 ELSE 1 END"
            params.append(priority_location)

        # MAIN SQL
        sql = """
            select top 1
                itemid,
                warehouse,
                stockid,
                bin
            from transactions
            where id in (
                select max(id)
                from transactions
                where 1=1
                    and bin is NOT NULL and TRANSACTIONCODE in ('SLE','RET','SHP')
                    and status <> 'Posted'
                    and id_delmar = ?
                    and size = ?
                    and warehouse = ?
                    %s
                group by id_delmar, size, warehouse, stockid, bin
            )
            %s
        """ % (location_sql, priority_sql)

        # query the transactions table
        trn_data = self.MSSQLInventorySelect(cr, uid, sql=sql, params=params)
        if trn_data:
            # update location if was not set
            if not res_obj.get('location_id'):
                location_ids = loc_obj.search(cr, uid, [('name', '=ilike', trn_data[0][2].strip()), ('warehouse_id', '=', warehouse.id)])
                if location_ids:
                    location = loc_obj.browse(cr, uid, location_ids[0])
                    res_obj.update({'location_id': location.id})
                    res_obj.update({'location_name': location.name})
                    res_obj.update({'location_complete_name': location.complete_name})
                else:
                    return False
            # update bin
            bin_obj = self.pool.get('stock.bin')
            bin_ids = bin_obj.search(cr, uid, [('name', '=', trn_data[0][3].strip())], limit=1)
            if bin_ids:
                bin = bin_obj.browse(cr, uid, bin_ids[0])
                res_obj.update({'bin_id': bin.id})
                res_obj.update({'bin_name': bin.name})
            else:
                res_obj.update({'bin_id': bin_obj.create(cr, uid, {'name': trn_data[0][3].strip()})})
                res_obj.update({'bin_name': trn_data[0][3].strip()})

        return res_obj

    def generate_bin(self, cr, uid, product_id=False, size_id=False, location_id=False, context=None):
        if not product_id or not location_id:
            return False
        _logger.info('Bin generation requested')
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        bin_cr_obj = self.pool.get('stock.bin.create')
        location = loc_obj.browse(cr, uid, location_id)
        warehouse = location.warehouse_id

        _logger.info("Trying to get existed bin for product %s in location %s with size %s" % (product_id, location_id, size_id))
        bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [product_id], [location_id], [size_id])
        if bin_ids:
            _logger.info("Found existed bin for product: %s" % bin_ids[0])
            return bin_ids[0]

        bin_cr_vals = {
            'warehouse_id': warehouse.id,
            'location_id': location.id,
            'location': location.name,
            'product_id': product_id,
            'size_id': size_id or False,
            'qty_capacity': 0
        }
        _logger.warn("Creating bin with vals: %s" % bin_cr_vals)
        bin_cr_id = bin_cr_obj.create(cr, uid, bin_cr_vals)
        bin_n = bin_cr_obj.browse(cr, uid, bin_cr_id)
        _logger.warn("Created bin id, name: %s, %s" % (bin_cr_id, bin_n.name))
        # check location
        if bin_cr_id and bin_cr_obj.check_location(cr, uid, bin_cr_id):
            _logger.warn('CR_BIN_ID AND LOCATION CHECKED')
            # check bin
            if bin_cr_obj.check_bin(cr, uid, bin_cr_id):
                _logger.warn('BIN CHECKED')
                bin_cr = bin_cr_obj.browse(cr, uid, bin_cr_id)
                _logger.warn("New bin: %s" % bin_cr.name)
                return bin_obj.get_bin_id_by_name(cr, uid, bin_cr.name)
            else:
                return False
        else:
            return False


StockPickingHelper()
