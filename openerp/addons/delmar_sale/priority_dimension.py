# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv


class priority_dimension(osv.osv):
    _inherit = "priority.dimension"

    _columns = {
        'shp_service_id': fields.many2one('res.shipping.service', 'Service', required=True),
        'shp_type_id': fields.many2one('res.shipping.type', 'Shipping type', required=True),
    }

    def get_priority(self, cr, uid, zip_code, shp_service_id, shp_type_id, weight=0.5, context=None):
        res = {}
        zip_code = self.pool.get('zip.zone').convert_zip(zip_code)
        if zip_code and shp_service_id and shp_type_id:

            cr.execute("""
                select
                        rf.id,
                        rf.name as name,
                        rf.shp_code as shp_code
                from priority_dimension pd
                    left join priority_dimension_line dl on pd.id = dl.pd_id
                    left join zip_zone zz on dl.zone_id = zz.id
                    left join zip_zone_line zl on zl.zz_id = zz.id
                    left join priority_flat_rate rf on
                        pd.id = rf.pd_id
                where 1=1
                    and pd.active = true
                    and zz.active = true
                    and rf.print_on_packing = true
                    and dl.weight = {weight}
                    and
                        case when zl.to_zip is null
                        then zl.from_zip = {zip_code}
                        else zl.from_zip <= {zip_code} and zl.to_zip >= {zip_code}
                        end
                    and pd.shp_service_id = {shp_service_id}
                    and pd.shp_type_id = {shp_type_id}
                    and dl.price >= rf.price
                order by rf.price desc
                limit 1
                """.format(zip_code=zip_code, shp_service_id=shp_service_id, shp_type_id=shp_type_id, weight=weight)
            )
            res = cr.dictfetchone()

        return res

priority_dimension()


class priority_flat_rate(osv.osv):
    _inherit = "priority.flat.rate"
    _columns = {
        'shp_code': fields.char('Shipping Code', size=128, ),
    }
priority_flat_rate()
