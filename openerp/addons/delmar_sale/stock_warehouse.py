from osv import fields, osv


class stock_warehouse(osv.osv):
    _inherit = "stock.warehouse"

    _columns = {
        'virtual': fields.boolean('Virtual'),
        'allow_autorelease': fields.boolean('Allow Auto-Release'),
        'allow_return': fields.boolean('Allow returns'),
        'frozen': fields.boolean('Frozen'),
        'sample': fields.boolean('Sample'),
        'show_on_catalog': fields.boolean('Show on Catalog'),
        'receivable': fields.boolean('Receivable', help="Allow select warehouse on receiving screen", ),
        'lot_release_id': fields.many2one('stock.location', 'Location Release', required=True,
                                          domain=[('usage', '=', 'internal')]),
        'onhold': fields.boolean('Onhold'),
        'allow_update_count': fields.boolean('Update count on/off', required=False,
                                             help="Allow update count on receiving screen", ),
        'smart_scanning': fields.boolean('Smart scanning', ),
    }

    _defaults = {
        'virtual': False,
        'allow_return': False,
        'frozen': False,
        'sample': False,
        'show_on_catalog': False,
        'allow_autorelease': True,
        'onhold': False,
        'allow_update_count': False,
        'smart_scanning': False,
    }

stock_warehouse()
