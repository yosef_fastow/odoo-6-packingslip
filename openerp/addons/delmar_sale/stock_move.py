# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
from tools.translate import _
from datetime import datetime
import netsvc
from stock_picking import STOCK_PICKING_STATES
from stock_picking import full_revert
from sale import SIGN_CANCEL, SIGN_CANCEL_VALUE
import time
import socket
import logging
import hashlib

STOCK_MOVE_STATES = [
    ('draft', 'New'),
    ('waiting', 'Waiting Another Move'),
    ('confirmed', 'Waiting Availability'),
    ('reserved', 'Reserved'),  # reserved in secondary location (new state for delmar_sate)
    ('assigned', 'Available'),
    ('done', 'Done'),
    ('cancel', 'Cancelled'),
]


logger = logging.getLogger(__name__)


class stock_move(osv.osv):
    _inherit = "stock.move"

    def _get_warehouse(self, cr, uid, ids, name, args, context=None):
        res = {}
        for sm in self.browse(cr, uid, ids):
            wh_id = False
            if sm.location_id:
                wh_id = self.pool.get('stock.location').get_warehouse(cr, uid, sm.location_id.id) or False
            res[sm.id] = wh_id
        return res

    def _is_item_changed(self, cr, uid, ids, name, args, context=None):
        res = {}
        sets_lines = []
        for sm in self.browse(cr, uid, ids):
            is_changed = False
            sale_line = sm.sale_line_id
            if sale_line:
                move_product_id = sm.product_id and sm.product_id.id or False
                sale_line_product_id = sale_line.product_id and sale_line.product_id.id or False
                move_size_id = sm.size_id and sm.size_id.id or False
                sale_line_size_id = sale_line.size_id and sale_line.size_id.id or False
                if move_product_id != sale_line_product_id or move_size_id != sale_line_size_id:
                    is_changed = True
            res[sm.id] = is_changed
        return res

    def _get_move_item(self, cr, uid, ids, name, args, context=None):
        res = {}

        sql = """   SELECT
                        sm.id as id,
                        concat(sp.name, E'/', pp.default_code, E'/', CASE WHEN rs.name IS NULL THEN '0' ELSE rs.name END) AS product_desc
                    FROM stock_move sm
                        LEFT JOIN product_product pp on sm.product_id = pp.id
                        LEFT JOIN ring_size rs on sm.size_id = rs.id
                        LEFT JOIN stock_picking sp on sm.picking_id = sp.id
                    WHERE
                        sm.id in (%s);
        """ % (",".join([str(x) for x in ids]))
        cr.execute(sql)
        result = cr.dictfetchall()

        for sm in result:
            res[sm['id']] = sm.get('product_desc', False)
        return res

    def _get_total_available(self, cr, uid, ids, name, args, context=None):
        res = {}

        for sm_id in ids:
            res[sm_id] = 0

        system_locations = ('Inventory loss', 'Customers', 'Output', 'EXCP')
        system_locations_str = str(system_locations)

        location_ids = self.pool.get('stock.location').get_internal_loc_ids(cr, uid, except_virtual=True, except_by_name=['RETURN'], to_string=True)
        if not location_ids:
            logger.warn('Internal locations are not available')
            return res

        sql = """   SELECT a.line_id AS line_id, SUM(a.qty) AS qty
                    FROM (
                        SELECT sm.id AS line_id,
                            sm.product_id,
                            sm.size_id,
                            CASE WHEN sl.name IN %s THEN sm2.location_id ELSE sm2.location_dest_id END AS location_dest_id,
                            SUM(sm2.product_qty * (CASE WHEN sl.name IN %s THEN -1 ELSE 1 END)) as qty
                        FROM stock_move sm
                            LEFT JOIN stock_move sm2 ON (
                                sm.product_id = sm2.product_id
                                AND COALESCE(sm.size_id, -1) = COALESCE(sm2.size_id, -1)
                            )
                            LEFT JOIN stock_location sl ON sl.id = sm2.location_dest_id
                        WHERE 1=1
                            AND sm2.state IN ('done', 'reserved', 'assigned')
                            AND ( sm2.location_dest_id in (%s) OR sl.name IN %s )
                            AND sm2.bin IS NOT NULL
                            AND sm2.bin <> ''
                            AND sm.id in (%s)
                        GROUP BY sm.id,
                            sm.product_id,
                            sm.size_id,
                            (CASE WHEN sl.name IN %s THEN sm2.location_id ELSE sm2.location_dest_id END)
                    ) a
                    WHERE a.location_dest_id in (%s)
                    GROUP BY a.line_id
                ;
        """ % (system_locations_str, system_locations_str, location_ids, system_locations_str, ','.join([str(x) for x in ids]), system_locations_str, location_ids)
        cr.execute(sql)
        qty_res = cr.dictfetchall()

        for sm_qty in qty_res:
            res[sm_qty['line_id']] = sm_qty.get('qty', 0)

        return res

    def _get_available_locs(self, cr, uid, ids, name, args, context=None):
        res = {}

        for sm_id in ids:
            res[sm_id] = False

        system_locations = ('Inventory loss', 'Customers', 'Output', 'EXCP')
        system_locations_str = str(system_locations)

        location_ids = self.pool.get('stock.location').get_internal_loc_ids(cr, uid, except_virtual=True, except_by_name=['RETURN'], to_string=True)

        if not location_ids:
            logger.warn('Internal locations are not available')
            return res

        sql = """   SELECT
                        a.line_id AS line_id,
                        a.location_dest_id AS loc_id,
                        regexp_replace(sl.complete_name, '.*/ (.*?) / Stock / (.*?)', '\\1 / \\2') AS loc_name,
                        a.bin AS bin,
                        a.qty AS qty
                    FROM (
                        SELECT sm.id AS line_id,
                            sm.product_id,
                            sm.size_id,
                            sm2.bin,
                            CASE WHEN sl.name IN %s THEN sm2.location_id ELSE sm2.location_dest_id END AS location_dest_id,
                            SUM(sm2.product_qty * (CASE WHEN sl.name IN %s THEN -1 ELSE 1 END)) as qty
                        FROM stock_move sm
                            LEFT JOIN stock_move sm2 ON (
                                sm.product_id = sm2.product_id
                                AND COALESCE(sm.size_id, -1) = COALESCE(sm2.size_id, -1)
                            )
                            LEFT JOIN stock_location sl ON sl.id = sm2.location_dest_id
                        WHERE 1=1
                            AND sm2.state IN ('done', 'reserved', 'assigned')
                            AND ( sm2.location_dest_id in (%s) OR sl.name IN %s )
                            AND sm2.bin IS NOT NULL
                            AND sm2.bin <> ''
                            AND sm.id in (%s)
                        GROUP BY sm.id,
                            sm.product_id,
                            sm.size_id,
                            sm2.bin,
                            (CASE WHEN sl.name IN %s THEN sm2.location_id ELSE sm2.location_dest_id END)
                    ) a
                        LEFT JOIN stock_location sl ON sl.id = a.location_dest_id
                    WHERE 1=1
                        AND a.qty != 0
                        AND a.location_dest_id IN (%s)
                    ;
        """ % (system_locations_str, system_locations_str, location_ids, system_locations_str, ','.join([str(x) for x in ids]), system_locations_str, location_ids)
        cr.execute(sql)
        inventory = cr.dictfetchall()

        for inv in inventory:

            # stock_inventory[inv['line_id']][inv['loc_id']]
            res[inv['line_id']] = "%s%s / %s (%s);\r\n" % (res[inv['line_id']] or '', inv['loc_name'], inv['bin'], int(inv.get('qty', 0)))

        return res

    def _get_set_product_ids(self, cr, uid, id):
        sets_lines = []
        sql = """
        select cmp_product_id from product_set_components where parent_product_id=%s
        """ % (id)
        cr.execute(sql)
        rsets_lines = cr.dictfetchall()
        for sets_line in rsets_lines:
            sets_lines.append(sets_line.get('cmp_product_id'))

        return sets_lines

    def create(self, cr, uid, vals, context=None):
        if vals and vals.get('sale_line_id', False) and vals.get('product_qty', 0) <= 0:
            return False
        new_move = super(stock_move, self).create(cr, uid, vals, context=context)
        if self.browse(cr, uid, new_move).picking_id:
            oh_obj = self.pool.get('order.history')
            so_id = self.browse(cr, uid, new_move).picking_id.sale_id.id
            to_unlink_ohs_ids = so_id and oh_obj.search(cr, uid, [('so_id', '=', so_id)]) or False
            oh_vals = {'type': 'stock_move', 'sm_id': new_move}
            if to_unlink_ohs_ids:
                tags = [tag_id.id for tag_id in oh_obj.browse(cr, uid, to_unlink_ohs_ids[0]).tagging_ids]
                if tags:
                    oh_vals.update({'tagging_ids': [(6, 0, tags)]})
            oh_obj.create(cr, uid, oh_vals)
            if to_unlink_ohs_ids:
                oh_obj.unlink(cr, uid, to_unlink_ohs_ids)
        return new_move

    def write(self, cr, uid, ids, vals, context=None):
        logger.info("WRITE STOCK_MOVE IDS: %s, VALS: %s" % (ids, vals))
        if not context:
            context = {}
        bin_obj = self.pool.get('stock.bin')
        bin = vals.get('bin', False)
        bin_id = bin_obj.get_bin_id_by_name(cr, uid, bin)

        if bin_id:
            vals.update({'bin_id': bin_id})

        if isinstance(ids, (int, long)):
            ids = [ids]
        # if uid != 1 and not context.get('do_only_return', False):
        #     frozen_fields = set(['product_qty', 'product_uom', 'product_uos_qty', 'product_uos', 'location_id', 'location_dest_id', 'product_id'])
        #     for move in self.browse(cr, uid, ids, context=context):
        #         if move.state == 'done':
        #             if frozen_fields.intersection(vals):
        #                 raise osv.except_osv(_('Operation forbidden'),
        #                                      _('Quantities, UoMs, Products and Locations cannot be modified on stock moves that have already been processed (except by the Administrator)'))
        base_class = [parent for parent in self.__class__.__mro__ if 'openerp.addons.stock.stock.stock_move' in str(parent)]
        class_name = base_class and base_class[0] or stock_move
        return super(class_name, self).write(cr, uid, ids, vals, context=context)

    def _get_ordered_product_id(self, cr, uid, ids, name, args, context=None):
        res = {}

        sql = """   SELECT
                        sm.id AS id,
                        concat('[', pp.default_code, '] ', pp.name_template) as ordered_product
                    FROM stock_move sm
                    LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                    LEFT JOIN product_product pp ON sol.product_id = pp.id
                    WHERE
                        sm.id IN (%s);
                """ % (",".join([str(x) for x in ids]))
        cr.execute(sql)
        result = cr.dictfetchall()

        for sm in result:
            res[sm['id']] = sm.get('ordered_product', False)

        return res

    def _get_ordered_size_id(self, cr, uid, ids, name, args, context=None):
        res = {}

        sql = """   SELECT
                        sm.id AS id,
                        rs.name AS ordered_size
                    FROM stock_move sm
                    LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                    LEFT JOIN ring_size rs ON sol.size_id = rs.id
                    WHERE
                        sm.id IN (%s);
                """ % (",".join([str(x) for x in ids]))
        cr.execute(sql)
        result = cr.dictfetchall()

        for sm in result:
            res[sm['id']] = sm.get('ordered_size', False)

        return res

    def _auto_init(self, cursor, context=None):
        res = super(stock_move, self)._auto_init(cursor, context=context)
        cursor.execute('SELECT indexname \
                FROM pg_indexes \
                WHERE indexname = \'stock_move_set_parent_order_id_index\'')
        stock_move_set_parent_order_id_idx = cursor.fetchone()
        if not stock_move_set_parent_order_id_idx:
            cursor.execute('CREATE INDEX stock_move_set_parent_order_id_index \
                    ON stock_move (set_parent_order_id)')

        cursor.execute('SELECT indexname \
                FROM pg_indexes \
                WHERE indexname = \'stock_move_set_product_id_index\'')
        stock_move_set_product_id_index = cursor.fetchone()
        if not stock_move_set_product_id_index:
            cursor.execute('CREATE INDEX stock_move_set_product_id_index \
                    ON stock_move (set_product_id)')

        cursor.execute('SELECT indexname \
                        FROM pg_indexes \
                        WHERE indexname = \'stock_move_address_id_index\'')
        stock_move_address_id_index = cursor.fetchone()
        if not stock_move_address_id_index:
            cursor.execute('CREATE INDEX stock_move_address_id_index ON stock_move (address_id);')

        return res

    _columns = {
        'bin': fields.char('Shelf', size=256),
        'bin_id': fields.many2one('stock.bin', 'Bin object'),
        'invredid': fields.char('InvredId', size=64),
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'size_ids': fields.char('size ids', size=256),
        'item_line_id': fields.char('Order Item Line ID', size=256),
        'old_location_id': fields.many2one('stock.location', 'Old source Location'),
        'status': fields.char('Status', size=256),
        'resolution_code': fields.many2one('stock.move.exception.resolution_code', 'Resolution Code'),
        'resolution_date': fields.date('Resolution Date'),
        'exception_date': fields.datetime('Exception Date'),
        'free_notes': fields.text('Free Notes'),
        'product_image': fields.related('product_id', 'product_image',  string='Image', type='binary'),
        'real_partner_id': fields.related('picking_id', 'real_partner_id', type='many2one', relation="res.partner", string="Customer", select=True),
        'warehouse': fields.function(_get_warehouse, string="Warehouse", type='many2one', relation='stock.warehouse'),
        'is_item_changed': fields.function(_is_item_changed, string="Changed item", type='boolean'),
        'picked_rate': fields.related('picking_id', 'sale_id', 'picked_rate',  string='Picked', type='float'),
        'invoiced_rate': fields.related('picking_id', 'sale_id', 'invoiced_rate',  string='Invoiced', type='float'),
        'carrier': fields.related('picking_id', 'carrier',  string='Carrier', type='char'),
        'tracking_ref': fields.related('picking_id', 'tracking_ref', type='char', string="Tracking Ref"),
        'shp_date': fields.related('picking_id', 'shp_date', type='datetime', string='Shipped Date'),
        'picking_note': fields.related('picking_id', 'note', type='text', string='Notes'),
        'order_state': fields.related('picking_id', 'state', type='selection', string="Order state", select=True, readonly=True, selection=STOCK_PICKING_STATES),
        'state': fields.selection(STOCK_MOVE_STATES, 'State', readonly=True, select=True,
              help='When the stock move is created it is in the \'Draft\' state.\n After that, it is set to \'Not Available\' state if the scheduler did not find the products.\n When products are reserved it is set to \'Available\'.\n When products are reserved in Secondary Locations it is set to \'Reserved\'.\n When the picking is done the state is \'Done\'.\
              \nThe state is \'Waiting\' if the move is waiting for another one.'),
        'product_item': fields.related('product_id', 'default_code', string="Item", type="char", store=False,),
        'product_desc': fields.function(_get_move_item, string="Order/Item/Size", type="char",),
        'total_available': fields.function(_get_total_available, string="Available", type='integer',),
        'product_locs': fields.function(_get_available_locs, string="Available Locations", type='text',),
        # Demo prep
        'product_picking_id': fields.many2one('product.picking', 'Product Picking',select=True),
        'user_notes': fields.related('sale_line_id', 'user_notes', relation='sale.order.line', string='User Notes', type='char', store=False),
        'price_unit': fields.related('sale_line_id', 'price_unit', relation='sale.order.line', string='Unit Price', type='float', store=False),
        'is_need_mount': fields.boolean('is_need_mount'),
        'back_flag': fields.boolean('Reverted item'),
        'is_need_diamond': fields.boolean('is_need_diamond'),
        'is_no_resize': fields.boolean('No resize'),
        'is_update_order': fields.boolean('Update sale order and show on packing slip'),
        'internal_shipment_id': fields.many2many(
            'internal.shipment', 'internal_shipment_move', 'move_id', 'shipment_id',
            string='Internal Shipment', readonly=True,),
        'label_note': fields.text('Label Notes'),
        'box_id': fields.many2one('stock.packing.material.box.code', 'Box code', ),
        'keep_transactions': fields.boolean('Do Not Clear Transactions Table'),
        'is_set_component': fields.boolean('True for set components'),
        'set_product_id': fields.many2one('product.product', 'Original Set ID'),
        'is_set': fields.boolean('is set', readonly=False),
        'set_parent_order_id': fields.many2one('stock.picking', 'set Parent Order Reference'),
        'ordered_product': fields.function(_get_ordered_product_id, string="ordered Product", type="char"),
        'ordered_size': fields.function(_get_ordered_size_id, string="ordered Size", type="char"),
        # used for DLMR-963 boxes moves
        'box_parent_move_id': fields.many2one('stock.move', 'Box parent move', required=False, ),
        # added for DLMR-1146
        'fc_invoice': fields.char('FC Invoice', size=256, ),
        'fc_order': fields.char('FC Order', size=256, ),
        'del_invoice': fields.char('DEL Invoice', size=256, ),
        'del_order': fields.char('DEL Order', size=256, ),
        'bill_of_lading': fields.char('BILL OF LADING', size=100, ),
        'qty_verified': fields.float('Verified QTY', required=False, states={'done': [('readonly', True)]}),
    }

    _defaults = {
        'status': '',
        'qty_verified': 0,
    }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        def is_default_values(data, value):
            if type(value) == list:
                return all(x in value for x in data)
            else:
                return data == value

        def is_default_list(data):
            if (len(data) != 4) or (type(data[0]) == str):
                return False
            if not all(x[0] in ['picking_id', 'state', 'picking_id.type', 'picking_id.state'] for x in data):
                return False
            if (is_default_values(data[0][2], False) and
                    is_default_values(data[1][2], ['waiting', 'confirmed']) and
                    is_default_values(data[2][2], 'out') and
                    is_default_values(data[3][2], ['confirmed', 'waiting_split'])
            ):
                return True
            return False
 
        if context is None:
            context = {}

        if not order and context.get('order_by', False):
            order = context['order_by']

        if order:
            print order
            order = order.replace('product_item', 'product_id')
        
        if args and (not is_default_list(args)):
            return super(stock_move, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []


    # Demo prep
    def product_pick(self, cr, uid, ids, context=None):
        product_picking_obj = self.pool.get('product.picking')
        for move in self.browse(cr, uid, ids):
            if not move.product_picking_id:
                wh_id = move.warehouse.id
                product_picking_ids = product_picking_obj.search(cr, uid, [('product_id', '=', move.product_id.id), ('size_id', '=', move.size_id.id), ('state', '=', 'start'), ('wh_id', '=', wh_id)])
                if product_picking_ids:
                    product_picking = product_picking_obj.browse(cr, uid, product_picking_ids[0])
                    self.write(cr, uid, [move.id], {'product_picking_id': product_picking.id})
                    product_picking_obj.write(cr, uid, [product_picking.id], {
                        'product_qty': product_picking.product_qty + move.product_qty,
                    })
                else:
                    new_product_picking_id = product_picking_obj.create(cr, uid, {
                        'product_id': move.product_id.id,
                        'state': 'start',
                        'product_qty': move.product_qty,
                        'wh_id': wh_id,
                        'bin_id': move.bin_id.id,
                        'size_id': move.size_id.id,
                    })
                    self.write(cr, uid, [move.id], {'product_picking_id': new_product_picking_id})
        return True

    # Demo prep
    def product_pick_remove(self, cr, uid, ids, context=None):
        hard_remove = context.get('product_pick_hard_remove', False)
        product_picking_obj = self.pool.get('product.picking')
        for move in self.browse(cr, uid, ids):
            if move.product_picking_id and (hard_remove or move.product_picking_id.state in ('start',)):
                product_picking = product_picking_obj.browse(cr, uid, move.product_picking_id.id)
                self.write(cr, uid, [move.id], {'product_picking_id': False})
                if not product_picking.line_ids:
                    product_picking_obj.unlink(cr, uid, [product_picking.id])
                else:
                    new_qty = (product_picking.product_qty or 0.0) - (move.product_qty or 0.0)
                    product_picking_obj.write(cr, uid, [move.product_picking_id.id], {'product_qty': new_qty})
                    product_picking_obj.check_done(cr, uid, [move.product_picking_id.id])
        return True

    # Demo prep
    def product_pick_process(self, cr, uid, ids, context=None):
        product_picking_obj = self.pool.get('product.picking')
        for move in self.browse(cr, uid, ids):
            if move.product_picking_id and move.product_picking_id.state in ('pick',):
                product_picking_obj.check_done(cr, uid, [move.product_picking_id.id])
        return True

    def action_partial_split(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if context.get('active_model') != self._name:
            context.update(active_ids=ids, active_model=self._name)
        partial_id = self.pool.get("stock.partial.move").create(
            cr, uid, {}, context=context)
        return {
            'name': _("Products to Process"),
            'view_mode': 'form',
            'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'stock_partial_picking_form')[1],
            'view_type': 'form',
            'res_model': 'stock.partial.move',
            'res_id': partial_id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': context
        }

    def unset_bin(self, cr, uid, ids, context=None):
        for base_move in self.browse(cr, uid, ids):
            if base_move.bin or base_move.bin_id:
                self.write(cr, uid, [base_move.id], {
                    'bin': False,
                    'bin_id': False,
                    })

    def action_assign(self, cr, uid, ids, context=None):
        """ Changes state to confirmed or waiting.
        @return: List of values
        """
        if not context:
            context = {}
        todo = self.search(cr, uid, [('id', 'in', ids), ('state', 'in', ('confirmed', 'waiting')), ('back_flag', '=', False)])
        res = self.check_assign(cr, uid, todo, context=context)
        return res

    def force_assign(self, cr, uid, ids, context=None):
        return True

    def action_reserve(self, cr, uid, ids, context=None):
        """ Changes state to confirmed or waiting.
        @return: List of values
        """
        if not context:
            context = {}
        todo = self.search(cr, uid, [('id', 'in', ids), ('state', 'in', ('confirmed', 'waiting'))])
        res = self.check_assign(cr, uid, todo, context=context, use_secondary_locs=True)
        return res

    def revert_assign(self, cr, uid, ids, context=None):
        return self.uncheck_assign(cr, uid, ids, context=context)

    def uncheck_assign(self, cr, uid, ids, context=None):
        self.clear_caches()
        if context is None:
            context = {}
        moves = [
            move
            for move
            in self.browse(cr, uid, ids, context=context)
            if move.state in ('assigned', 'reserved', 'done')
        ]
        picking_obj = self.pool.get('stock.picking')
        for move in moves:
            invredid = self.get_invredid_for_stock_move(cr, uid, move, move.bin,
                                                        move.location_id.id)
            if not invredid:
                if move and move.picking_id:
                    prod_name = move.product_id.default_code or ''
                    time_now = time.strftime('%Y-%m-%d %H:%M:%S',
                                             time.gmtime())
                    new_history = move.picking_id.report_history + '\n'
                    new_history += time_now + ' Product ' + prod_name
                    new_history += ' cannot be put back to shelf'
                    new_history += ' because it has no invredid.\n'
                    picking_obj.write(cr, uid, [move.picking_id.id], {
                        'report_history': new_history
                    })
                return False
            self.write(cr, uid, move.id, {'invredid': invredid})

        # remote
        need_back_ids = [move.id for move in moves]
        self.do_remote_product_reserve(cr, uid, need_back_ids, sign=1,
                                       context=context)
        # local
        for move in moves:
            old_location = move.old_location_id or move.location_id

            vals = {
                'state': 'confirmed',
                'status': 'Back to shelf',
                'location_id': old_location.id,
                'date_expected': move.create_date,
                'exception_date': datetime.now(),
                'bin': False,
                'bin_id': False,
            }

            if context.get('free_notes'):
                vals.update({'free_notes': context['free_notes']})

            # DLMR-963 - Revert box transaction
            spm_obj = self.pool.get('stock.packing.material')
            box_move_id = self.search(cr, uid, [('box_parent_move_id', '=', move.id)], limit=1)
            if box_move_id and box_move_id[0]:
                # write +1 transaction
                logger.info("Box revert +1 transaction")
                if not spm_obj.write_box_transaction(cr, uid, box_move_id=box_move_id[0], sign=1, transactcode=SIGN_CANCEL_VALUE, context=None):
                    logger.warn("Cannot write %s transaction for BOX" % SIGN_CANCEL_VALUE)
                else:
                    logger.info("Box revert transaction created")
                # write data to box moveline
                logger.info("Set box line as canceled")
                self.write(cr, uid, [box_move_id[0]], {
                    'state': 'cancel',
                    'status': '',
                    'date_expected': move.create_date,
                    'exception_date': datetime.now(),
                    'bin': False,
                    'bin_id': False,
                })
                # remove box moveline
                #logger.info("Deleting box moveline")
                #self.unlink(cr, uid, [box_move_id[0]])
                #logger.info("Box moveline deleted")
            # END DLMR-963

            # update move line
            logger.info("Updating original moveline")
            self.write(cr, uid, [move.id], vals)
            logger.info("Original moveline updated")
        return True

    def filter_locations_of_frozen_wh(self, cr, uid, loc_ids):
        warehouse_obj = self.pool.get('stock.warehouse')
        location_obj = self.pool.get('stock.location')

        location_ids_arr = []

        frozen_wh_ids = warehouse_obj.search(cr, uid, [('frozen', '=', 'True')])
        for x in loc_ids:
            loc_wh_id = location_obj.get_warehouse(cr, uid, x)
            if loc_wh_id not in frozen_wh_ids:
                location_ids_arr.append(x)

        return location_ids_arr

    def divide(self, cr, uid, move_id, required_qty, context=None):

        if not (move_id and required_qty):
            return None

        move = self.browse(cr, uid, move_id)
        current_qty = move.product_qty
        if not current_qty:
            return None

        if current_qty < required_qty:
            raise osv.except_osv(
                'Warning!',
                '''[{id_delmar}{size}] Not available qty {qty}. It should by less or equal to {max_qty}!'''.format(
                    id_delmar=move.product_id.default_code,
                    size='' if not move.size_id.id else '/' + move.size_id.name,
                    qty=required_qty,
                    max_qty=current_qty
                )
            )

        if required_qty == current_qty:
            return move_id

        diff_qty = current_qty - required_qty
        new_move_id = self.copy(
            cr, uid, move_id, {
            'product_qty': required_qty,
            'qty_verified': required_qty,
            'product_uos_qty': required_qty,
            'state': move.state
            })
        self.write(cr, uid, move_id, {'product_qty': diff_qty, 'qty_verified': diff_qty, 'product_uos_qty': diff_qty})

        return new_move_id

    def check_availability(self, cr, uid, ids, customer_id, context=None):
        self.clear_caches()
        pick_obj = self.pool.get('stock.picking')
        customer_obj = self.pool.get('res.partner')
        wf_service = netsvc.LocalService("workflow")
        customer = customer_obj.browse(cr, uid, customer_id)
        ir_config = self.pool.get('ir.config_parameter')
        reserve_from = []

        move_ring_ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('size_id', '!=', False),
        ])
        if customer.auto_size_tolerance and customer.size_tolerance.id and len(move_ring_ids):
            system_locations_str = str(('Inventory loss', 'Customers', 'Output', 'EXCP'))
            loc_ids_str = ','.join([str(x) for x in context.get('customer_location_ids') if x])
            moves = self.browse(cr, uid, move_ring_ids)
            if moves:
                for move in moves:
                    if move.state != 'confirmed':
                        continue
                    float_size = float(move.sale_line_id.size_id.name)
                    size_from = str(float_size - float(customer.size_tolerance.name))
                    size_to = (float_size + float(customer.size_tolerance.name))
                    having_size_str = "AND CASE WHEN rs.name ~ E'^\\\\d+' THEN rs.name::real else 0.0 END BETWEEN %s AND %s" % (
                        size_from, size_to)
                    having_str = """ or (sm.product_id = %s %s and sum(sm.product_qty * (CASE WHEN sl.name in %s THEN -1 ELSE 1 END)) >= %s) 
                    order by diff_tol ,size desc""" % (
                        move.product_id.id, having_size_str, system_locations_str, move.product_qty)
                    and_price_str = ""
                    if customer.min_price:
                        and_price_str += " AND pt.standard_price >= " + str(customer.min_price)
                    if customer.max_price:
                        and_price_str += " AND pt.standard_price <= " + str(customer.max_price)
                    sql = """
                                select res.*, pos.num_location from (
                                        SELECT sm.product_id as product_id,
                                            sm.size_id as size_id,
                                            rs.name as size,
                                            (%s-rs.name::numeric::float) *(CASE WHEN %s-rs.name::numeric::float <0 THEN -1 ELSE 1 END) as diff_tol,
                                            array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                                            CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END as location_id,
                                            coalesce(sb.name, sm.bin, '') as bin,
                                            sum(sm.product_qty * (CASE WHEN sl.name IN %s THEN -1 ELSE 1 END)) as qty
                                        FROM stock_move sm
                                            LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                                            LEFT JOIN ring_size rs on rs.id = sm.size_id
                                            LEFT JOIN stock_bin sb on sb.id = sm.bin_id
                                            LEFT JOIN ring_size_tolerance t on t.id = sm.size_id
                                            LEFT JOIN product_product pp on sm.product_id = pp.id
                                            LEFT JOIN product_template pt on pt.id=pp.product_tmpl_id 
                                        WHERE sm.state IN ('done', 'reserved', 'assigned')
                                            AND sm.product_id = %s
                                            AND ( sm.location_dest_id in (%s) OR sl.name IN %s )
                                            AND coalesce(sb.name, sm.bin, '') != ''
                                            %s
                                            
                                        GROUP BY (CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END)
                                            , coalesce(sb.name, sm.bin, '')
                                            , sm.product_id
                                            , sm.size_id
                                            , rs.name
                                        HAVING 1!=1 %s
                                                        ) res
                                left join (
                                select
                                    location_id,
                                    row_number() over() as num_location
                                from
                                    (
                                        select
                                            id as partner_id,
                                            unnest(
                                                concat( '{', order_ids, '}' )::int []
                                            ) as location_id
                                        from
                                            res_partner
                                        where
                                            order_ids is not null
                                            and id = %s
                                    ) sel
                                )pos on res.location_id = pos.location_id
                                order by diff_tol ,size desc,num_location      
                            """ % (float_size, float_size,
                                   system_locations_str, system_locations_str, move.product_id.id, loc_ids_str,
                                   system_locations_str, and_price_str,
                                   system_locations_str, having_str, customer_id)
                    sql = sql.replace("=None", " IS NULL ")
                    cr.execute(sql)
                    reserve = cr.dictfetchone()

                    if not reserve:
                        continue

                    invredid = self.get_invredid_for_stock_move(cr, uid, move, reserve.get('bin'),
                                                                reserve.get('location_id'))
                    reserve.update({
                        'location_dest_id': reserve.get('location_id'),
                        'invredid': invredid,
                    })
                    reserve_from.append(reserve)
                    # DLMR-1144 - always grant resize for set component
                    if not move.is_set and (customer.no_resize or move.product_id.standard_price < float(ir_config.get_param(cr, uid, 'no_resize_gta_value', 0))):
                        self.write(cr, uid, move.id, {'is_no_resize': True})
                    self.write(cr, uid, move.id, {'size_id': reserve.get('size_id')})

            if not reserve_from:
                return False

            if reserve_from:
                done = self.set_move_location(cr, uid, moves, reserve_from, move.picking_id.id)
                if done:
                    self.write(cr, uid, done, {'state': 'assigned'})
                    self.do_remote_product_reserve(cr, uid, move_ring_ids, sign=-1, context=context)

            if move.picking_id.state not in ['confirmed','waiting_split']:
                wf_service.trg_write(uid, 'stock.picking', move.picking_id.id, cr)

        return True

    def check_assign(self, cr, uid, ids, context=None, use_secondary_locs=False):
        """ Checks the product type and accordingly writes the state.
            All products must be available by one warehouse.
        @return: ids of reserved moves
        """

        self.clear_caches()

        enabled_secondary_reserve = False

        # print "\n\n check_assign"
        if context is None:
            context = {}

        pick_id = None
        picking_vals = {}
        old_history = ''

        done = []
        unsync_items = []
        location_obj = self.pool.get('stock.location')
        picking_obj = self.pool.get('stock.picking')

        customer_location_ids = context.get('force_location_ids', False)
        if not customer_location_ids:
            if use_secondary_locs:
                if not enabled_secondary_reserve:
                    print "Reserve from secondary locations is disabled!"
                    return done
                customer_location_ids = context.get('sec_customer_location_ids', False)
                if not customer_location_ids:
                    print "WARNING! No secondary locations!"
                    return done
            else:
                customer_location_ids = context.get('customer_location_ids', False)
                if not customer_location_ids:
                    print "ERROR! No primary locations!"
                    return done

        customer_location_ids = self.filter_locations_of_frozen_wh(cr, uid, customer_location_ids)

        moves = {}

        # Skip assigned and 0 qty moves
        all_move_ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('product_qty', '>', 0),
            ('state', 'not in', ['assigned', 'reserved', 'done']),
        ])

        all_moves = self.browse(cr, uid, all_move_ids, context=context)
        all_moves_count = len(all_moves)
        moves_count = 0
        sale_lines_ids = []

        if all_move_ids:
            cr.execute("""
                select sale_line_id
                from stock_move
                where id in %s
                    and sale_line_id is not null
                """, (tuple(all_move_ids), )
            )
            for line in cr.fetchall():
                sale_lines_ids.append(line[0])

            unsync_items = self.pool.get('sale.order').check_sync_product(cr, uid, sale_lines_ids) or []
            # print "all moves: %s" % (all_moves)
        for move in all_moves:
            size_id = move.size_id and move.size_id.id or False
            if move.bin_id and move.location_id and move.location_id.name not in ('Output', 'Stock', 'EXCP', 'Virtual Locations', 'Physical Locations', 'Inventory loss'):
                done.append(move.id)
                if not pick_id:
                    pick_id = move.picking_id.id
            else:
                moves_count += 1
                # There are orders with several identical positions.
                # In this case need allocate total qty of items
                key = "%s.%s" % (
                    move.product_id.id,
                    size_id
                )
                if not moves.get(key, False):
                    move.ids = {move.id: move.product_qty}
                    moves[key] = move

                    item = {'product_id': move.product_id.id, 'size_id': size_id}
                    if item in unsync_items:
                        self.pool.get('sale.order').sync_product_stock(cr, uid, move.product_id.id, size_id, context=context)

                    if move.product_id.set_component_ids:
                        # sync ERP inventory for components of SET
                        for set_cmp in move.product_id.set_component_ids:
                            if set_cmp.cmp_product_id:
                                self.pool.get('sale.order').sync_product_stock(cr, uid, set_cmp.cmp_product_id.id, size_id, context=context)
                else:
                    moves[key].ids.update({move.id: move.product_qty})

        moves = moves.values()
        # print "moves: %s" % (moves)

        get_all_locs = (len(moves) == 1 and len(moves[0].ids) == 1)
        search_by = location_obj.get_child_locs_by_wh(cr, uid, customer_location_ids, context=context, get_all_locs=get_all_locs) or {}
        # print "search_by: %s" % (search_by)

        user = self.pool.get('res.users').browse(cr, uid, uid)
        user_name = user and user.name
        new_history = time.strftime('%m-%d %H:%M:%S', time.gmtime()) + ' ' + (user_name or '') + ': Looking for products: \n'

        if moves and search_by:
            # A line from DB that looks like:
            # {'product_id': '', 'size_id': '', 'location_dest_id': '', 'bin': '', 'invredid': '', 'qty': ''}
            reserve_from = []
            # A list of previous ^^^^
            results = []
            location_order_ids = []

            product_ids = []
            having_str = ''

            # add history secondary locations string
            # generate sql-substring for product quantity
            for move in moves:
                if not pick_id:
                    pick_id = move.picking_id.id
                new_history = new_history + '    ' + move.product_id.default_code
                product_ids.append(str(move.product_id.id))
                having_str += """ or (sm.product_id =%s and sm.size_id =%s and sum(sm.product_qty * (CASE WHEN sl.name in ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) >= %s) """ % (
                    move.product_id.id,
                    move.size_id and move.size_id.id or 'None',
                    min(move.ids.values()) if hasattr(move, 'ids') else move.product_qty
                )
            sec_locs = ' secondary' if use_secondary_locs else ''
            new_history = new_history + ' in' + sec_locs + ' locations:\n'

            # get order of location ids from text field order_ids
            if pick_id:
                pick_obj = self.pool.get('stock.picking').browse(cr, uid, pick_id)
                if pick_obj:
                    old_history = pick_obj.tech_report_history or ''
                    partner = pick_obj.sale_id.partner_id
                    if partner:
                        location_order_ids = partner.order_ids and partner.order_ids.split(',') or []
            logger.info("Ordered location IDS: %s" % location_order_ids)

            for warehouse in search_by:
                wh_loc_ids = warehouse.get('lock_ids', [])
                if not wh_loc_ids:
                    continue
                wh_name = 'Unknown warehouse'
                if get_all_locs:
                    wh_name = 'All WHs'
                elif warehouse['id']:
                    wh_rec = self.pool.get('stock.warehouse').read(cr, uid, warehouse['id'], ['name'])
                    wh_name = wh_rec.get('name')
                new_history = str(new_history) + "\n        " + str(wh_name) + " \n"

                # Adding customer location to order history
                for cur_ordered_id in location_order_ids:
                    cur_id = int(cur_ordered_id)
                    if cur_id in wh_loc_ids:
                        loc_object = self.pool.get('stock.location').browse(cr, uid, cur_id)
                        if loc_object:
                            new_history += "        " + loc_object.complete_name + ", \n"

                # What this fucking sql do???
                # As I got this query takes bins from the already assigned moves for defined prod/size/qty>
                sql = """SELECT a.product_id, a.size_id, a.location_dest_id, a.bin, a.invredid, a.qty
                        FROM (
                            SELECT sm.product_id,
                                sm.size_id,
                                array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                                CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id,
                                coalesce(sb.name, sm.bin, '') as bin,
                                -- sm.invredid,
                                sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as qty
                            FROM stock_move sm
                                LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                                LEFT JOIN stock_bin sb on sm.bin_id = sb.id
                            WHERE sm.state IN ('done', 'reserved', 'assigned')
                                AND sm.product_id IN (%s)
                                AND ( sm.location_dest_id in (%s) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                                AND coalesce(sb.name, sm.bin, '') <> ''
                            GROUP BY (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END)
                                , coalesce(sb.name, sm.bin, '')
                                -- , sm.invredid
                                , sm.product_id
                                , sm.size_id
                            HAVING 1 != 1 %s
                            ORDER BY CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END
                        ) a
                """ % (
                       ", ".join(product_ids),
                       ", ".join([str(x) for x in wh_loc_ids]),
                       having_str
                    )

                sql = sql.replace("=None", " IS NULL ")
                cr.execute(sql)
                result = cr.dictfetchall()

                # if query result
                if result:
                    # DLMR-1089
                    # Sort results to customers location priority
                    logger.info("RESULT Before sorting: %s" % result)
                    sorted_result = []
                    result_to_delete = []
                    for priority_location_id in location_order_ids:
                        for i, res_line in enumerate(result):
                            if res_line['location_dest_id'] == int(priority_location_id):
                                sorted_result.append(res_line)
                                result_to_delete.append(i)

                    for i in sorted(result_to_delete, reverse=True):
                        del(result[i])

                    result = sorted_result + result
                    logger.info("RESULT After sorting: %s" % result)
                    # END DLMR-1089

                    unique_items = set([(x['product_id'], x['size_id']) for x in result])
                    if moves_count == all_moves_count:
                        # if delivery order have all stock moves in 'WAITING AVAILABILITY' state
                        if not get_all_locs:
                            # if delivery order have SOME stock moves: try get them from ONE warehouse
                            if len(unique_items) == len(moves):
                                if self.check_availability_in_stockid(cr, uid, moves, result):
                                    reserve_from = result
                                    break
                        else:
                            # if delivery order have only ONE stock move: get location corresponding to its priority
                            wh_locks = search_by[0]
                            if wh_locks:
                                for customer_location in wh_locks['lock_ids']:
                                    for res in result:
                                        if res['location_dest_id'] == customer_location:
                                            if self.check_availability_in_stockid(cr, uid, moves, [res]):
                                                reserve_from.append(res)
                                if reserve_from:
                                    break

                    else:
                        # if delivery order have at least one move in 'AVAILABLE' (state=assigned)
                        if len(unique_items) == moves_count:
                            if self.check_availability_in_stockid(cr, uid, moves, result):
                                reserve_from = result
                                break

                    results.append(result)

            if pick_id:

                if not reserve_from and results:
                    reserve_from = sorted(results, key=lambda x: len(x), reverse=True)
                    reserve_from = reduce(lambda x, y: x + y, reserve_from)

                if reserve_from:
                    # print "reserve from several warehouse"
                    done = self.set_move_location(cr, uid, moves, reserve_from, pick_id)

        # print "done: %s" % (done)

        if done:
            if use_secondary_locs:
                self.write(cr, uid, done, {'state': 'reserved'})
            else:
                self.write(cr, uid, done, {'state': 'assigned'})

            # self.pool.get('stock.move').set_bin(cr, uid, done, context=context)
            self.do_remote_product_reserve(cr, uid, done, sign=-1, context=context)

        if pick_id:
            wh_id = picking_obj.get_warehouse(cr, uid, pick_id)
            picking_vals.update({'warehouses': wh_id})

            locations = picking_obj.get_locations(cr, uid, [pick_id], context=None)[pick_id]
            picking_vals.update({'locations': locations})

            wh_name = self.pool.get('stock.warehouse').read(cr, uid, wh_id, ['name'])['name']
            new_history += '%s %s: Set warehouse %s\n' % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name or '', wh_name)
            picking_vals.update({'tech_report_history': old_history + "\n" + new_history})

        if pick_id and picking_vals:
            picking_obj.write(cr, uid, pick_id, picking_vals)


        return done

    def check_availability_in_stockid(self, cr, uid, moves, reserve_from):
        result = bool(moves)

        location_id = -1

        for move in moves:
            for cur_r in reserve_from:
                if cur_r['product_id'] == move.product_id.id:
                    location_id = cur_r['location_dest_id']
                    break

            having_str = """ or (sm.product_id =%s and sm.size_id =%s and sum(sm.product_qty * (CASE WHEN sl.name in ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) >= %s) """ % (
                move.product_id.id,
                move.size_id and move.size_id.id or 'None',
                min(move.ids.values()) if hasattr(move, 'ids') else move.product_qty
            )

            sql = """SELECT sm.product_id,
                    sm.size_id,
                    array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                    CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id,
                    sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END))
                FROM stock_move sm
                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                WHERE sm.state IN ('done', 'reserved', 'assigned')
                    AND sm.product_id IN (%s)
                    AND ( sm.location_dest_id in (%s) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                    AND sm.bin is not null
                    AND sm.bin <> ''
                GROUP BY (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END)
                    , sm.product_id
                    , sm.size_id
                HAVING 1 != 1 %s
                ORDER BY CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END
            """ % (move.product_id.id,
                   location_id,
                   having_str)

            sql = sql.replace("=None", " IS NULL ")
            cr.execute(sql)
            res = cr.fetchall()
            result &= bool(res)

        return result

    def set_move_location(self, cr, uid, moves, locs, pick_id, context=None):
        changed_ids = []
        all_ids = []

        picking_obj = self.pool.get('stock.picking')
        loc_obj = self.pool.get('stock.location')

        pick_obj = picking_obj.browse(cr, uid, pick_id)
        old_history = pick_obj.tech_report_history or ''
        new_history = '     Available locations: \n'

        except_loc_ids = loc_obj.search(cr, uid, [('name', '=', 'Stock')]) or []

        for move in moves:
            ring_size_tolerance_obj = self.pool.get('ring.size.tolerance')
            reserve_locs = [
                x for x in locs
                if (
                        x['product_id'] == move.product_id.id and
                        (
                                (not move.size_id or x['size_id'] == move.size_id.id) or
                                (x.get('diff_tol', False) and ring_size_tolerance_obj.check_sizes_tolerance(cr, uid, [
                                    move.size_id.id, x['size_id']], x.get('diff_tol', False)))
                        ) and
                        x['location_dest_id'] not in except_loc_ids
                )
            ]

            move_datas = move.ids if hasattr(move, 'ids') else {move.id: move.product_qty}
            all_ids += move_datas.keys()
            if reserve_locs:
                move_ids = [v for v in sorted(move_datas.items(), key=lambda (k, v): (v, k), reverse=True)]
                for move_id, move_qty in move_ids:
                    for reserve_loc in reserve_locs:
                        approve_move = False
                        found_loc_obj = loc_obj.browse(cr, uid, reserve_loc['location_dest_id'])
                        if found_loc_obj:

                            if move_qty <= reserve_loc['qty']:
                                new_history = new_history + "        " + found_loc_obj.complete_name + ' : ' + \
                                              reserve_loc['bin'] + ", \n"

                                approve_move = True
                                set_components = self.pool.get('product.product').browse(cr, uid, reserve_loc[
                                    'product_id']).set_component_ids
                                if set_components:
                                    for set_cmp in set_components:
                                        if not set_cmp.cmp_product_id or not set_cmp.qty:
                                            continue
                                        cmp_size_id = (set_cmp.cmp_product_id.ring_size_ids or set_cmp.cmp_product_id.sizeable) and reserve_loc['size_id'] or False
                                        cmp_qty = self.get_product_qty_in_locations(cr, uid, set_cmp.cmp_product_id.id, cmp_size_id, [reserve_loc['location_dest_id']]) or 0
                                        cmp_qty = cmp_qty // set_cmp.qty
                                        if (cmp_qty < move_qty):
                                            approve_move = False
                                            new_history += " Not found {set_cmp} (x{cmp_qty}) for SET {set_item}\n".format(
                                                set_cmp=set_cmp.cmp_product_id.default_code,
                                                set_item=set_cmp.parent_product_id.default_code,
                                                cmp_qty=set_cmp.qty
                                            )
                        if approve_move:
                            reserve_loc['qty'] -= move_qty
                            invredid = self.get_invredid_for_stock_move(cr, uid, move, reserve_loc['bin'],
                                                                        reserve_loc['location_dest_id'])
                            self.write(cr, uid, move_id, {
                                'location_id': reserve_loc['location_dest_id'],
                                'bin': reserve_loc['bin'] or False,
                                'invredid': invredid,
                                'old_location_id': move.location_id and move.location_id.id or False,
                                'back_flag': False,
                                'status': False,
                            })
                            if invredid:
                                changed_ids.append(move_id)
                            else:
                                if move and move.picking_id:
                                    prod_name = move.product_id and move.product_id.default_code or ''
                                    new_history = move.picking_id.report_history + '\n' + time.strftime(
                                        '%Y-%m-%d %H:%M:%S',
                                        time.gmtime()) + ' Product ' + prod_name + ' is available but has no invredid. \n'

                                    picking_obj.write(cr, uid, [move.picking_id.id], {'report_history': new_history})

                            break

            not_allocated = list(set(all_ids) - set(changed_ids))
            if not_allocated:
                self.write(cr, uid, not_allocated, {
                    'status': 'Waiting availability',
                    'exception_date': datetime.now(),
                    'bin': False,
                    'invredid': False,
                })

        self.pool.get('stock.picking').write(cr, uid, [pick_id], {'tech_report_history': old_history + "\n" + new_history})

        return changed_ids

    def get_invredid_for_stock_move(self, cr, uid, move, move_bin, location_id, product_id=None, size_id=None, ignore_bin=True, return_all=False):
        """
            ignore_bin - check invredid by itemid/warehouse/stock (all bins should have the same value)
            return_all - return all current invredids, not create new
        """
        invredid = False

        location = self.pool.get('stock.location').browse(cr, uid, location_id)
        product = product_id and self.pool.get('product.product').browse(cr, uid, product_id) or (move and move.product_id)
        size_id = size_id or (move and move.size_id) or False

        delmarid = (product and product.default_code) or ''
        size_postfix = (size_id and self.get_size_postfix(cr, uid, size_id)) or '0000'
        itemid = delmarid and (delmarid + '/' + size_postfix) or False

        warehouse = self.get_warehouse_by_move_location(cr, uid, location_id)
        stockid = location and location.name or False

        invredid = self.get_invredid_for_mssql_row(cr, uid, itemid, warehouse, stockid, move_bin, ignore_bin=ignore_bin, return_all=return_all)

        return invredid

    def get_invredid_for_mssql_row(self, cr, uid, row_itemid, row_warehouse, row_stock, row_bin, ignore_bin=True, return_all=False):
        invredids = []

        if 'EXCP' in [row_warehouse, row_stock]:
            raise osv.except_osv(_('Warning!'), _('Denied transactions for warehouse/stockid  EXCP!'))

        if not invredids and row_warehouse in ('USRETURN', 'CARETURN', 'FBA'):
            invredids = [1]

        if not invredids and row_stock in ('ONHOLD', 'MNTLRET'):
            invredids = [1]

        if not invredids and row_stock == 'FEOSPP' and row_warehouse == 'CAFER':
            invredids = [1]

        if not invredids:
            wh_ids = self.pool.get('stock.warehouse').search(cr, uid, [('name', '=', row_warehouse)])
            if (wh_ids):
                wh_obj = self.pool.get('stock.warehouse').browse(cr, uid, wh_ids[0])
                if wh_obj.virtual:
                    invredids = [1]

        if invredids:
            return invredids if return_all else invredids[0]

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        size_obj = self.pool.get('ring.size')
        id_delmar, size = size_obj.split_itemid_to_id_delmar_size(cr, uid, row_itemid)
        if server_id and id_delmar and size and row_warehouse and row_stock:

            if row_bin:

                sql = """
                    select %s
                        cast(invredid as character varying)
                    from transactions
                    where coalesce(bin, '') = ?
                        and id_delmar = ?
                        and size = ?
                        and warehouse = ?
                        and stockid = ?
                        AND CASE WHEN isNumeric(INVREDID) > 0 THEN CAST(INVREDID AS INT) ELSE 0 END > 0
                    group by cast(invredid as character varying)
                    order by max(id) desc
                """ % (
                    'top 1' if not return_all else '',
                )
                params = [
                    row_bin,
                    id_delmar,
                    size,
                    row_warehouse,
                    row_stock
                ]
                res = server_data.make_query(cr, uid, server_id, sql, params=params, select=True, commit=False)
                if res:
                    for line in res:
                        invredids.append(line[0])

            elif ignore_bin:
                sql = """
                    select %s
                        cast(invredid as character varying)
                    from transactions
                    where id_delmar = ?
                        and size = ?
                        and warehouse = ?
                        and stockid = ?
                        AND CASE WHEN isNumeric(INVREDID) > 0 THEN CAST(INVREDID AS INT) ELSE 0 END > 0
                    group by cast(invredid as character varying)
                    order by max(id) desc
                """ % (
                    'top 1' if not return_all else '',
                )
                params = [
                    id_delmar,
                    size,
                    row_warehouse,
                    row_stock
                ]
                res = server_data.make_query(cr, uid, server_id, sql, params=params, select=True, commit=False)
                if res:
                    for line in res:
                        invredids.append(line[0])

            if not invredids and row_bin and row_bin.strip().upper() == 'RETURN':
                invredids = [1]

            if not invredids:
                delmar_id = row_itemid[0:row_itemid.rindex('/')] or False
                size_postfix = row_itemid[row_itemid.rindex('/')+1:len(row_itemid)] or False
                if delmar_id and size_postfix:
                    invredids.append(self.pool.get('stock.picking').create_invredid(cr, uid, row_warehouse, row_stock, delmar_id, size_postfix, row_bin))

        if return_all:
            return invredids
        else:
            return invredids and invredids[0] or False

    def get_reserved_flag(self, cr, uid, itemid, warehouse, stockid, server_id):
        server_data = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_data.get_sale_servers(cr, uid, single=True)

        reserved_flag = 1
        if itemid and warehouse and stockid and server_id:

            reserved_sql = """SELECT top 1 reserved
                FROM StockID
                WHERE 1=1
                AND warehouse = ?
                AND (
                    stockid = ?
                    OR stockid = '%'
                )
            """

            params = [
                warehouse,
                stockid,
            ]

            res = server_data.make_query(cr, uid, server_id, reserved_sql, params=params, select=True, commit=False)
            if res:
                reserved_flag = res[0][0]

        return reserved_flag

    def do_remote_product_reserve(self, cr, uid, move_ids, sign=1, context=None):

        if context is None:
            context = {}
        server_data = self.pool.get('fetchdb.server')
        ring_size_obj = self.pool.get('ring.size')
        sale_order_obj = self.pool.get('sale.order')

        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            return False
        moves = self.browse(cr, uid, move_ids, context=context)
        bin_obj = self.pool.get('stock.bin')
        params_dict = {}
        for move in moves:

            trans_tag = move.sale_line_id.id or None
            invoice_no = move.sale_line_id.order_id.po_number or ''

            delmarid = move.product_id and move.product_id.default_code or False
            size_postfix = self.get_size_postfix(cr, uid, move.size_id)
            itemid = delmarid and (delmarid + '/' + size_postfix) or False

            warehouse = self.get_warehouse_by_move_location(
                cr, uid, move.location_id and move.location_id.id)
            if warehouse == 'EXCP':
                continue
            stockid = move.location_id and move.location_id.name or False
            reserved_flag = self.get_reserved_flag(cr, uid, itemid, warehouse,
                                                   stockid, server_id)

            move_qty = move.product_qty and int(move.product_qty) or 0
            description = move.sale_line_id.name or move.product_id.name
            customer_ref = move.sale_line_id.order_id.partner_id.ref if (move.sale_line_id.order_id.id and move.sale_line_id.order_id.partner_id.id) else ''


            if move.is_set and not move.sale_line_id.order_id.id:
                customer_ref = move.sale_line_id.set_parent_order_id.partner_id.ref if (
                        move.sale_line_id.set_parent_order_id.id and move.sale_line_id.set_parent_order_id.partner_id.id) else ''
                invoice_no = move.sale_line_id.set_parent_order_id.po_number or ''

            bin_name = move.bin_id and move.bin_id.name or move.bin
            if sign == 1 and not (bin_name and
                    bin_obj.is_bin_available_for_product(cr, uid, warehouse,
                        bin_name, delmarid, size_postfix)
                ):
                new_bin_name = bin_obj.get_mssql_bin_for_product(
                    cr, uid, itemid, warehouse).get(itemid)
                if not new_bin_name:
                    err_msg = 'The bin {bin_name} is no longer available for item {itemid}.'
                    err_msg += 'There is no another bin for the item in warehouse {warehouse}.'
                    err_msg += 'Please create a bin for the item before.'
                    raise osv.except_osv(
                        _('Error!'),
                        err_msg.format(
                            bin_name=bin_name,
                            itemid=itemid,
                            warehouse=warehouse,))
                bin_name = new_bin_name

            # SLE - sale/back to shelf,
            # ADJ - adjustment (magic stock movement or B2B moves),
            # STK - stock (usially qty = 0 when item is first appeared in stock),
            # REC - recieving to stock,
            # SHP - delivery to CANADA
            # CXL - cancel or back to shelf order
            # RET - return order

            # Set transactioncode to CXL if last record updated to CLX too.
            # It can be done only if last record and cancelled one are both in the same warehouse.
            original_move_id = context.get('flash_revert', {}) or context.get('flash_confirm', {})
            original_move = self.browse(cr, uid, original_move_id.get(move.id))
            transaction_code = sale_order_obj.get_sale_transaction_code(cr, uid, move.picking_id.sale_id.id or None, sign)
            unit_price = move.sale_line_id and move.sale_line_id.price_unit or 0
            params = [
                itemid or 'null',  # itemid
                context.get('transaction_codes', {}).get(move.id) or context.get('transaction_code') or transaction_code,  # TransactionCode
                move_qty,  # qty
                warehouse or 'null',  # warehouse
                description or 'null',  # itemdescription
                bin_name or 'null',  # bin
                stockid or 'null',  # stockid
                'TRS',  # TransactionType
                delmarid,  # id_delmar
                sign,  # sign
                (move_qty * sign) or '0',  # adjusted_qty
                size_postfix,  # size
                move.invredid or 'null',  # invredid
                customer_ref or None,  # CustomerID
                delmarid,  # ProductCode
                invoice_no or 'null',  # InvoiceNo
                reserved_flag or 0,  # reserved
                trans_tag,  # trans_tag
                unit_price * -sign,  # unit price from sale_order_line
            ]

            if original_move and not full_revert(context):  # Revert on flash screen
                # params - positive transaction
                # splitted - negative transaction
                S_tr = params[:]
                S_tr[9] = -params[9]  # sign
                S_tr[10] = -params[10]  # adjusted_qty
                # Add new transaction to the transaction' queue
                params_dict.update({original_move: S_tr})

            params_dict.update({move: params})

        for move in params_dict:
            params = params_dict.get(move)
            # DLMR-1190
            transaction_verified = True
            if move.location_id and move.location_id.warehouse_id and move.location_id.warehouse_id.smart_scanning:
                transaction_verified = False

            tr_context = {
                'erp_sm_id': move.id,
                'export_ref_ca': move.export_ref_ca,
                'export_ref_us': move.export_ref_us,
                'fc_invoice': move.fc_invoice,
                'fc_order': move.fc_order,
                'del_invoice': move.del_invoice,
                'del_order': move.del_order,
                'bill_of_lading': move.bill_of_lading,
                'transaction_verified': transaction_verified
            }
            self.write_into_transactions_table(cr, uid, server_id, tuple(params), context=tr_context if params[1]!='ADJ' else {})
            move_qty = params[2]
            move_warehouse = params[3]
            move_stockid = params[6]
            product = move.product_id
            set_cmps = product.set_component_ids
            params[1] = 'ADJ'
            del tr_context['erp_sm_id']
            for set_cmp in set_cmps:
                cmp_params = params[:]
                set_cmp_qty = move_qty * set_cmp.qty
                component = set_cmp.cmp_product_id
                is_sizeable = component.sizeable or component.ring_size_ids
                set_cmp_size_id = is_sizeable and move.size_id and move.size_id.id or False
                default_code = component.default_code
                cmp_params[14] = default_code
                four_digit_size = ring_size_obj.get_4digit_name(cr, uid, set_cmp_size_id)
                cmp_params[11] = four_digit_size
                cmp_params[8] = default_code
                itemid = default_code and (default_code + '/' + four_digit_size) or False
                cmp_params[0] = itemid
                cmp_params[2] = set_cmp_qty
                cmp_params[10] = set_cmp_qty * sign

                bin_result = bin_obj.get_mssql_bin_for_product(
                    cr, uid,
                    itemid,
                    move_warehouse,
                    move_stockid,
                    min_qty=set_cmp_qty,
                )
                if itemid not in bin_result:
                    bin_result[itemid] = None
                bin_name = bin_result[itemid]

                invredid = self.get_invredid_for_mssql_row(
                    cr, uid,
                    itemid,
                    move_warehouse,
                    move_stockid,
                    bin_name,
                    True
                )

                cmp_params[5] = bin_name
                cmp_params[12] = invredid

                self.write_into_transactions_table(cr, uid, server_id, tuple(cmp_params), context=tr_context)

                # sync ERP inventory for components of SET
                sale_order_obj.sync_product_stock(cr, uid, component.id, set_cmp_size_id, context=context)

            # if selling product is component of SET
            if not product.set_parent_ids:
                continue

            # current qty for sold component
            #sale_order_obj.sync_product_stock(cr, uid, product.id, move.size_id.id, context=context)
            cmp_qty_line = move.product_qty or 0.0
            cmp_qty_res = self.get_product_qty_in_locations(
                cr,
                uid,
                product.id,
                move.size_id and move.size_id.id,
                move.location_id and move.location_id.id
            ) or 0.0
            cmp_qty = cmp_qty_line + cmp_qty_res

            for set_item in product.set_parent_ids:

                # calculate parent item qty(only by sold cmp)
                set_qty = float(cmp_qty // set_item.qty)
                parent = set_item.parent_product_id
                default_code = parent.default_code
                four_digit_size = params[11]
                itemid = default_code and (default_code + '/' + four_digit_size) or False
                # get parent item bin
                bin_result = bin_obj.get_mssql_bin_for_product(
                    cr, uid,
                    itemid,
                    move_warehouse,
                    move_stockid,
                )
                if itemid not in bin_result:
                    continue
                bin_name = bin_result[itemid]

                # get parent item current qty
                sale_order_obj.sync_product_stock(cr, uid, parent.id, move.size_id.id, context=context)
                cur_set_qty = self.get_product_qty_in_locations(
                    cr,
                    uid,
                    parent.id,
                    move.size_id and move.size_id.id,
                    move.location_id and move.location_id.id,
                    bin_name) or 0.0

                diff_qty = cur_set_qty - set_qty
                if diff_qty <= 0:
                    continue
                params[2] = diff_qty
                params[10] = diff_qty * sign
                params[4] = parent.name
                params[14] = default_code
                params[8] = default_code
                params[5] = bin_name
                params[0] = itemid
                params[12] = self.get_invredid_for_mssql_row(
                    cr, uid,
                    itemid,
                    move_warehouse, move_stockid,
                    bin_name, True
                )

                self.write_into_transactions_table(cr, uid, server_id, tuple(params), context=tr_context)

        # And last one: set fc_invoice and export_refs to None
        # for cancel, advanced cancel, and back to shelf actions
        if context.get('sign') == SIGN_CANCEL:
            self.write(cr, uid, move_ids, dict(fc_invoice=None,
                                               fc_order=None,
                                               del_invoice=None,
                                               del_order=None,
                                               export_ref_ca=None,
                                               export_ref_us=None,
                                               bill_of_lading=None,
                                               )
                       )

        return True

    def update_transactions_table(self, cr, uid, server_id, server_obj, record=None, warehouse=None, context={}):
        if not record:
            return False

        qty = record.product_qty
        if context.get('original_qty'):
            qty = context.get('original_qty')

        # Step 0. Has transaction id in context
        transaction_id = None
        if 'record_id' in context:
            transaction_id = [context.get('record_id')] or None

        params = record.get_record_info()
        transaction_code = context.get('transaction_code', False)

        # Step 1. Check is transaction with erp_sm_id exists:
        if not transaction_id:
            oh_obj = self.pool.get('order.history')
            oh_obj.search(cr, uid, [('sm_id', '=', record and record.id or None)])
            oh_ids = oh_obj.search(cr, uid, [('sm_id', '=', record.id)])
            oh = oh_obj.browse(cr, uid, oh_ids)
            do_name = oh and oh[0].do_name or ''
            oh_state = oh and oh[0].state or 'undefined'
            #if (context.get('order_state') == 'returned') or \
            #        (('-0R' in do_name) and (not context.get('order_state'))):
            #    tr_codes = "'RET'"
            #    adj_qty = qty
            #else:
            adj_qty = -qty
            tr_codes = "'SLE', 'SHP'"
            sm_sql = """
                SELECT ID FROM dbo.TRANSACTIONS 
                WHERE ERP_SM_ID={0}
                AND TRANSACTIONCODE IN ({1})
                AND itemid='{2}'
                AND customerid='{3}'
                order by id desc
                """.format(record.id, tr_codes, params['itemid'],params['customerid'])
            transaction_id = server_obj.make_query(cr, uid, server_id, sm_sql, params={}, select=True, commit=False)

        # Step 2. If still not found transaction id
        # And delivery order' state is returned
        # try to find transaction by
        # transactioncode RET
        # itemid
        # positive qty
        #if not transaction_id and (context.get('order_state') == 'returned') or \
        #        (('-0R' in do_name) and (not context.get('order_state'))):
        #    params['product_qty'] = qty
        #    select_sql = """
        #                SELECT ID FROM dbo.TRANSACTIONS
        #                WHERE (invoiceno='{external_customer_order_id}' or invoiceno='{po_number}')
        #                AND transactioncode in ('RET')
        #                AND qty={product_qty}
        #                -- AND itemid='{itemid}'
        #                order by id desc
        #               """.format(**params)
        #    transaction_id = server_obj.make_query(cr, uid, server_id, select_sql, params={}, select=True, commit=False)

        # Step 3. If still not found transaction id
        # And delivery order' state is one of cancel, final_cancel
        # try to find transaction by
        # transactioncode SLE, SHP
        # itemid
        # negative qty
        if (not transaction_id and oh_state in ('cancel', 'final_cancel','returned','undefined')):
            params = record.get_record_info()
            #params['product_qty'] = qty
            select_sql = """
                        SELECT ID FROM dbo.TRANSACTIONS
                        WHERE (invoiceno=%(external_customer_order_id)s or invoiceno=%(po_number)s)
                        AND transactioncode in ('SLE','SHP')
                        AND itemid=%(itemid)s
                        AND customerid=%(customerid)s   
                        order by id desc
                       """
            transaction_id = server_obj.make_query(cr, uid, server_id, select_sql, params=params, select=True, commit=False)

        # Step 4. If still not found transaction id
        # try to find transaction by warehouse
        if not transaction_id:
            params = record.get_record_info()
            #params['product_qty'] = qty
            params['warehouse'] = warehouse
            select_sql = """
                        SELECT ID FROM dbo.TRANSACTIONS
                        WHERE (invoiceno=%(external_customer_order_id)s or invoiceno=%(po_number)s)
                        AND transactioncode in ('SLE','SHP')
                        AND itemid=%(itemid)s
                        AND warehouse=%(warehouse)s
                        AND customerid=%(customerid)s
                        order by id desc
                       """
            transaction_id = server_obj.make_query(cr, uid, server_id, select_sql, params=params, select=True, commit=False)

        if not transaction_id:
            return False
        elif transaction_code:
            transaction_id = transaction_id[0][0]
        else:
            transaction_id = ', '.join(map(str, [x[0] for x in transaction_id]))

        # Step 3. Prepare data for update transaction
        transaction_code = context.get('transaction_code', SIGN_CANCEL_VALUE)
        if 'qty' in context:
            update_qty = context.get('qty', 0)
        else:
            update_qty = qty

        # update params and basic update query
        update_params = dict(record_id=transaction_id, qty=update_qty)
        update_sql = """UPDATE dbo.TRANSACTIONS
                        SET QTY=%(qty)s {} {},
                            ADJUSTED_QTY = CASE
                                               WHEN TRANSACTIONCODE='RET' THEN %(qty)s
                                               ELSE -%(qty)s
                                           END
                        WHERE ID in ({})"""

        # transactioncode update or not
        trncode_sql = ''
        if transaction_code:
            trncode_sql = ", TRANSACTIONCODE= case when TRANSACTIONCODE='RET' then 'RET' else %(transaction_code)s end"
            update_params.update({'transaction_code': transaction_code})

        # Insert export_ref/fc/del params to update query if exists in context
        fcdel_list = ['bill_of_lading',
                      'export_ref_ca',
                      'export_ref_us',
                      'fc_invoice',
                      'fc_order',
                      'del_invoice',
                      'del_order',
                      'erp_sm_id',
                      ]
        fcdel_sql = ''
        if any(x in context for x in fcdel_list):
            fcdel_queries = []
            for param_name in fcdel_list:
                if param_name in context:
                    fcdel_queries.append("{}=%({})s".format(param_name.upper(), param_name))
                    update_params.update({
                        param_name: context.get(param_name)
                    })
            if fcdel_queries:
                fcdel_sql = ", " + ", ".join(fcdel_queries)

        # do UPDATE query
        update_res = server_obj.make_query(
            cr, uid, server_id,
            update_sql.format(trncode_sql, fcdel_sql, transaction_id), params=update_params,
            select=False, commit=True
        )
        if not (update_res and update_res.rowcount):
            raise osv.except_osv(_('Warning!'), 'Error in remote update transaction.')

        return True

    def get_product_qty_in_locations(self, cr, uid, product_id, size_id, location_ids, bin=None):
        result = False

        if not location_ids or not product_id:
            return False

        if type(location_ids) != list:
            location_ids = [location_ids]

        where_size_id = " sm.size_id IS NULL "
        if size_id:
            where_size_id = " sm.size_id = %s " % (size_id,)

        where_bin = " (coalesce(sb.name, sm.bin, '') <> '') "
        if bin:
            where_bin = " (sm.bin = '%s' or sb.name = '%s') " % (bin, bin,)

        sql = """SELECT sm.product_id,
                sm.size_id,
                coalesce(sb.name, sm.bin, '') as bin,
                array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as qty
            FROM stock_move sm
                LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                LEFT JOIN stock_bin sb ON sb.id = sm.bin_id
            WHERE sm.state IN ('done', 'reserved', 'assigned')
                AND sm.product_id = %s
                AND %s
                AND ( sm.location_dest_id in (%s) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                AND %s
            GROUP BY
                (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END)
                , sm.product_id
                , sm.size_id
                , coalesce(sb.name, sm.bin, '')
            HAVING
                (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END) in (%s)
            ORDER by sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) desc
        """ % (product_id,
               where_size_id,
               ",".join([str(x) for x in location_ids]),
               where_bin,
               ",".join([str(x) for x in location_ids]),)

        sql = sql.replace("=None", " IS NULL ")
        cr.execute(sql)
        # print sql
        result = cr.dictfetchone()

        return result and result['qty'] or 0.0

    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        """ Makes partial pickings and moves done.
        @param partial_datas: Dictionary containing details of partial picking
                          like partner_id, address_id, delivery_date, delivery
                          moves with product_id, product_qty, uom
        """
        res = {}
        picking_obj = self.pool.get('stock.picking')
        product_obj = self.pool.get('product.product')
        currency_obj = self.pool.get('res.currency')
        uom_obj = self.pool.get('product.uom')
        wf_service = netsvc.LocalService("workflow")

        if context is None:
            context = {}

        complete, too_many, too_few = [], [], []
        move_product_qty = {}
        prodlot_ids = {}
        for move in self.browse(cr, uid, ids, context=context):
            if move.state in ('done', 'cancel'):
                continue
            partial_data = partial_datas.get('move%s' % (move.id), False)
            assert partial_data, _('Missing partial picking data for move #%s') % (move.id)
            product_qty = partial_data.get('product_qty', 0.0)
            move_product_qty[move.id] = product_qty
            product_uom = partial_data.get('product_uom', False)
            product_price = partial_data.get('product_price', 0.0)
            product_currency = partial_data.get('product_currency', False)
            prodlot_ids[move.id] = partial_data.get('prodlot_id')
            if move.product_qty == product_qty:
                complete.append(move)
            elif move.product_qty > product_qty:
                too_few.append(move)
            else:
                too_many.append(move)

            # Average price computation
            if (move.picking_id.type == 'in') and (move.product_id.cost_method == 'average'):
                product = product_obj.browse(cr, uid, move.product_id.id)
                move_currency_id = move.company_id.currency_id.id
                context['currency_id'] = move_currency_id
                qty = uom_obj._compute_qty(cr, uid, product_uom, product_qty, product.uom_id.id)
                if qty > 0:
                    new_price = currency_obj.compute(cr, uid, product_currency,
                            move_currency_id, product_price)
                    new_price = uom_obj._compute_price(cr, uid, product_uom, new_price,
                            product.uom_id.id)
                    if product.qty_available <= 0:
                        new_std_price = new_price
                    else:
                        # Get the standard price
                        amount_unit = product.price_get('standard_price', context=context)[product.id]
                        new_std_price = ((amount_unit * product.qty_available)\
                            + (new_price * qty)) / (product.qty_available + qty)

                    product_obj.write(cr, uid, [product.id], {'standard_price': new_std_price})

                    # Record the values that were chosen in the wizard, so they can be
                    # used for inventory valuation if real-time valuation is enabled.
                    self.write(cr, uid, [move.id],
                                {'price_unit': product_price,
                                 'price_currency_id': product_currency,
                                })

        for move in too_few:
            product_qty = move_product_qty[move.id]
            if product_qty != 0:
                defaults = {
                            'product_qty': product_qty,
                            'product_uos_qty': product_qty,
                            'picking_id': move.picking_id.id,
                            'state': 'assigned',
                            'move_dest_id': False,
                            'price_unit': move.price_unit,
                            }
                prodlot_id = prodlot_ids[move.id]
                if prodlot_id:
                    defaults.update(prodlot_id=prodlot_id)
                new_move = self.copy(cr, uid, move.id, defaults)
                complete.append(self.browse(cr, uid, new_move))
            self.write(cr, uid, [move.id],
                    {
                        'product_qty': move.product_qty - product_qty,
                        'product_uos_qty': move.product_qty - product_qty,
                    })

        for move in too_many:
            self.write(cr, uid, [move.id],
                    {
                        'product_qty': move.product_qty,
                        'product_uos_qty': move.product_qty,
                    })
            complete.append(move)

        for move in complete:
            if prodlot_ids.get(move.id):
                self.write(cr, uid, [move.id], {'prodlot_id': prodlot_ids.get(move.id)})
            self.action_done(cr, uid, [move.id], context=context)
            picking_id = move.picking_id and move.picking_id.id or None
            if picking_id:
                # TOCHECK : Done picking if all moves are done
                cr.execute("""
                    SELECT move.id FROM stock_picking pick
                    RIGHT JOIN stock_move move ON move.picking_id = pick.id AND move.state = %s
                    WHERE pick.id = %s""",
                            ('done', move.picking_id.id))
                res = cr.fetchall()
                if len(res) == len(move.picking_id.move_lines):
                    picking_obj.action_move(cr, uid, [picking_id])
                    picking_obj.set_warehouses_locations(cr, uid, picking_id, context=context)
                    wf_service.trg_write(uid, 'stock.picking', picking_id, cr)

        return [move.id for move in complete]

    def onchange_product_id(self, cr, uid, ids, prod_id=False, loc_id=False,
                            loc_dest_id=False, address_id=False):

        res = super(stock_move, self).onchange_product_id(cr, uid, ids, prod_id, loc_id, loc_dest_id, address_id)

        size_ids_arr = []
        if prod_id:
            size_ids = self.pool.get('product.product').browse(cr, uid, prod_id).ring_size_ids
            for size_obj in size_ids:
                size_ids_arr.append(str(size_obj.id))
            size_ids_str = ','.join(size_ids_arr)
            res['value'].update({'size_ids': size_ids_str})

        if not size_ids_arr:
            res['value'].update({'size_id': None})

        return res

    def action_cancel(self, cr, uid, ids, context=None):
        super(stock_move, self).action_cancel(cr, uid, ids, context)
        self.write(cr, uid, ids, {'status': ''})
        return True

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        res = super(stock_move, self).default_get(cr, uid, fields_list, context=context)
        if context.get('def_sm_id', False):
            order_fields = self.read(cr, uid, context.get('def_sm_id'))
            res.update(order_fields)

        return res

    def delete_from_transactions_table(self, cr, uid, server_id, ids, mssql_conn=None, mssql_curs=None, context=None):
        res = True
        server_data = self.pool.get('fetchdb.server')
        close_conn = not bool(mssql_conn)
        commit_sql = not bool(mssql_conn)

        if ids:

            if not mssql_conn:
                mssql_conn = server_data.connect(cr, uid, server_id)
            if not mssql_curs and mssql_conn:
                mssql_curs = mssql_conn.cursor()

            if not mssql_conn:
                return False

            if not isinstance(ids, (list, tuple)):
                ids = [ids]

            sql = """Delete from transactions where id in (%s) """ % (",".join([str(x) for x in ids]))
            res = server_data.make_query(cr, uid, server_id, sql, select=False, commit=commit_sql, connection=mssql_conn, cursor=mssql_curs)

        if close_conn and mssql_conn:
            mssql_conn.close()

        return res

    # TODO: Use this wraper in other places too
    def prepare_transaction_params(self, cr, uid, params, server_id=None, context=None):
        if not context:
            context = {}

        if not params:
            raise osv.except_osv('Error', 'Params are required')

        required_fields = [
            'id_delmar',
            'size',
            'qty',
            'warehouse',
            'stockid',
            'sign',
            'unit_price',
        ]

        for key in required_fields:
            if params.get(key, None) is None or params[key] == '':
                raise osv.except_osv('Error', 'Missed required parameter: %s' % (key))

        allow_empty = [
            'bin',
            'itemdescription',
            'customerid',
            'trans_tag',
            'invoiceno'
        ]

        for key in allow_empty:
            if not params.get(key):
                params[key] = ''

        if not params.get('itemid'):
            params['itemid'] = "%s/%s" % (params['id_delmar'], params['size'])

        if not params.get('transactioncode'):
            params['transactioncode'] = 'ADJ'

        if not params.get('transactiontype'):
            params['transactiontype'] = 'TRS'

        if not params.get('adjusted_qty'):
            params['adjusted_qty'] = params['qty'] * params['sign']

        if not params.get('product_code'):
            params['productcode'] = params['id_delmar']

        if not params.get('invredid'):
            params['invredid'] = self.get_invredid_for_mssql_row(
                cr, uid,
                params['itemid'],
                params['warehouse'],
                params['stockid'],
                params['bin'],
                ignore_bin=True,
                return_all=False
                )

        if params.get('reserved', None) is None:
            params['reserved'] = self.get_reserved_flag(
                cr, uid,
                params['itemid'],
                params['warehouse'],
                params['stockid'],
                server_id
                )

        # add additional params to context
        possible_params = ['bill_of_lading', 'export_ref_ca', 'export_ref_us', 'fc_invoice', 'fc_order', 'del_invoice', 'del_order', 'erp_sm_id']
        for ppar in possible_params:
            if ppar in params:
                context.update({
                    ppar: params.get(ppar)
                })

        return [
            params['itemid'],             # itemid
            params['transactioncode'],    # transactioncode
            params['qty'],                # qty
            params['warehouse'],          # warehouse
            params['itemdescription'],    # itemdescription
            params['bin'],                # bin
            params['stockid'],            # stockid
            params['transactiontype'],    # transactiontype
            params['id_delmar'],          # id_delmar
            params['sign'],               # sign
            params['adjusted_qty'],       # adjusted_qty
            params['size'],               # size
            params['invredid'],           # invredid
            params['customerid'],         # customerid
            params['productcode'],        # productcode
            params['invoiceno'],          # invoiceno
            params['reserved'],           # reserved
            params['trans_tag'],          # trans_tag
            params.get('unit_price'),     # unit_price
        ], context

    # DLMR-1190
    # `ids` are stock_move [ids]
    def unverify_transaction(self, cr, uid, ids, context=None):
        if not ids:
            return False
        ids_to_unverify = []
        for move in self.browse(cr, uid, ids):
            if move.location_id.warehouse_id.smart_scanning:
                ids_to_unverify.append(move.id)
        return self.verify_transaction(cr, uid, ids_to_unverify, verify=False, context=context)

    # DLMR-1190
    # `ids` are stock_move [ids]
    # `verify` is bool
    def verify_transaction(self, cr, uid, ids, verify=True, context=None):
        if (not ids) or (not isinstance(verify, bool)):
            return False
        logger.info("VERIFY TRANSACTIONS FOR MOVES %s: %s" % (ids, verify))
        # init mssql db object
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        # prepare sql
        update_params = dict(pick_verified=int(verify))
        sql = """UPDATE dbo.TRANSACTIONS 
            SET PICK_VERIFIED=%(pick_verified)s 
            WHERE ERP_SM_ID IN ({})""".format(",".join([str(x) for x in ids]))
        # do UPDATE query
        update_res = server_obj.make_query(
            cr, uid, server_id,
            sql, params=update_params,
            select=False, commit=True
        )
        if not (update_res and update_res.rowcount):
            #raise osv.except_osv(_('Warning!'), 'Error in remote update transaction.')
            return False
        # return okay by default
        return True

    def write_into_transactions_table(self, cr, uid, server_id, params, mssql_conn=None, mssql_curs=None, context=None):
        """
        params = [
            0: itemid,
            1: transactioncode,
            2: qty,
            3: warehouse,
            4: itemdescription,
            5: bin,
            6: stockid,
            7: transactiontype,
            8: id_delmar,
            9: sign,
            10: adjusted_qty,
            11: size,
            12: invredid,
            13: customerid,
            14: productcode,
            15: invoiceno,
            16: reserved,
            17: trans_tag,
            18: unit_price,
            # optional params:
            19: fc_invoice, # DEPRECATED! Use context instead
            20: export_ref_ca, # DEPRECATED! Use context instead
        ]
        """
        context = context or {}
        result = False
        server_data = self.pool.get('fetchdb.server')

        close_conn = not bool(mssql_conn)
        commit_sql = not bool(mssql_conn)

        host = socket.gethostname()
        sos_tag = 'openerp_%s' % (host or 'mqsql')

        user = self.pool.get('res.users').browse(cr, uid, uid)
        user_creator = 'openerp_%s' % (user.name or '')

        # SQL INSERT TEMPLATE
        sql = """
            SET NOCOUNT ON;
            INSERT INTO dbo.TRANSACTIONS(itemid, transactioncode, qty, transactdate, warehouse, itemdescription,
                bin, status, stockid, transactiontype, id_delmar, _user_creator, _user_modificator, _creation_date,
                _modif_date, sign, sos_tag, timestamp, adjusted_qty, size, invredid, customerid, productcode,
                invoiceno, reserved, trans_tag, unit_price{}
            ) VALUES (
                ?, ?, ?, getdate(), ?, ?, ?, 'Pending', ?, ?, ?, '%s', '%s', getdate(), getdate(), ?, '%s',
                getdate(), ?, ?, ?, ?, ?, ?, ?, ?, ?{}
            );
            SELECT SCOPE_IDENTITY();
            """ % (user_creator, user_creator, sos_tag)

        # Extended params for transaction query
        additional_params = {}
        # Workaround for deprecated fc_invoice and export_refs included in params
        if len(params) == 21:
            additional_params.update({
                'fc_invoice': params[19],
                'export_ref_ca': params[20]
            })
        # get additional params from context
        possible_params = ['bill_of_lading', 'export_ref_ca', 'export_ref_us', 'fc_invoice', 'fc_order', 'del_invoice', 'del_order', 'erp_sm_id']
        for ppar in possible_params:
            if context and ppar in context:
                additional_params.update({
                    ppar: context.get(ppar)
                })

        # DLMR-1190
        # SET TRANSACTION VERIFIED
        # BASED ON WAREHOUSE
        if params[3] and params[3] is not None and params[3] != 'null':
            wh_obj = self.pool.get('stock.warehouse')
            wh_ids = wh_obj.search(cr, uid, [('name', '=', params[3])])
            if wh_ids and wh_ids[0]:
                wh = wh_obj.browse(cr, uid, wh_ids[0])
                if not wh.smart_scanning:
                    additional_params.update({
                        'PICK_VERIFIED': 1
                    })
        # force define verified transaction
        if 'transaction_verified' in context and isinstance(context.get('transaction_verified'), bool):
            additional_params.update({
                'PICK_VERIFIED': int(context.get('transaction_verified', True))
            })
        # END DLMR-1190

        # update sql - add params
        if len(additional_params) > 0:
            sql = sql.format(", " + ",".join(additional_params.keys()), ", " + ",".join(['?' for x in additional_params.values()]))
            if isinstance(params, tuple):
                params += tuple(additional_params.values())
            else:
                params += additional_params.values()
        else:
            sql = sql.format('', '')

        # check mssql connection
        if not mssql_conn:
            mssql_conn = server_data.connect(cr, uid, server_id)
        if not mssql_curs and mssql_conn:
            mssql_curs = mssql_conn.cursor()
        if not mssql_conn:
            return False

        # query to get transaction id
        sql_get_id = """SELECT max(ID) from Transactions;"""

        # lock stocks CAMTLSR and XYZ
        if params[3] == 'CAMTLSR' and params[6] == 'XYZ':
            raise osv.except_osv(_('Warning!'), _('Can\'t update: Lock ERP Transactions to XYZ location for CAMTLSR'))

        if isinstance(params, tuple):
            params = list(params)

        # WHAT THE FUCK IS THIS ???
        for i in xrange(0, len(params), len(params)):
            params[i+17] = params[i+17] and hashlib.md5(str(params[i+17])).hexdigest() or None
            if params[4]:
                params[4] = params[4][:200]
            params[i+5] = params[i+5] or ''

        # Run queries
        res = server_data.make_query(cr, uid, server_id, sql, params=params, select=True, commit=commit_sql, connection=mssql_conn, cursor=mssql_curs)
        result = res and res[0] and res[0][0] or False
        if res and context.get('reason', False):
            res_id = result and int(result) or False
            if not res_id:
                sql_res_get_id = server_data.make_query(cr, uid, server_id, sql_get_id, select=True, commit=False, connection=mssql_conn, cursor=mssql_curs)
                res_id = sql_res_get_id and sql_res_get_id[0] and sql_res_get_id[0][0] and int(sql_res_get_id[0][0]) or False

            if res_id:
                sql_reason = """INSERT INTO dbo.prg_transactions_reasons(
                        transactionid, reason, createdate
                    ) VALUES (
                        %(res_id)s, %(reason)s, getdate()
                    )
                """
                params = {
                    'res_id': res_id,
                    'reason': context.get('reason', ''),
                }
                server_data.make_query(
                    cr, uid, server_id,
                    sql_reason, params=params,
                    select=False, commit=commit_sql,
                    connection=mssql_conn, cursor=mssql_curs,
                )
            else:
                logger.warn('Can\'t receive transactions id')

        if close_conn:
            mssql_conn.close()

        return result

    def get_warehouse_by_move_location(self, cr, uid, loc_id):
        warehouse = False
        if loc_id:
            warehouse_id = self.pool.get('stock.location').get_warehouse(cr, uid, loc_id)
            if warehouse_id:
                warehouse_obj = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id)
                warehouse = warehouse_obj.name or False

        return warehouse

    def get_itemid(self, cr, uid, move_id):
        move = self.browse(cr, uid, move_id)
        size_obj = self.pool.get('ring.size')
        size_id = move.size_id and move.size_id.id
        size = size_obj.get_4digit_name(cr, uid, size_id)
        derault_code = move.product_id.default_code or ''
        return derault_code + '/' + size

    def get_size_postfix(self, cr, uid, size):
        if not size:
            return '0000'
        size_name = size if type(size) in [unicode, str] else size.name
        return self.pool.get('ring.size').convert_to_4digit_name(cr, uid, size_name)

    def action_replace_reserved(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        reserved_move = self.browse(cr, uid, ids)[0]

        # prod_id = self.browse(cr, uid, ids)[0].product_id.id
        wh_id = self.pool.get('stock.location').get_warehouse(cr, uid, reserved_move.location_id.id)
        warehouse = wh_id and self.pool.get('stock.warehouse').browse(cr, uid, wh_id).name

        from_b2b_id = self.pool.get("stock.product.report.line").create(
            cr, uid,
            {   'item': reserved_move.product_id.default_code,
                'size': self.get_size_postfix(cr, uid, reserved_move.size_id),
                'bin': reserved_move.bin_id and reserved_move.bin_id.name or reserved_move.bin,
                'warehouse': warehouse,
                'stock': reserved_move.location_id.name,
                'qty': reserved_move.product_qty,
            },
            context=context)
        # return {
        #     'name': _("Products to Process"),
        #     'view_mode': 'form',
        #     'view_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_sale', 'stock_partial_picking_form')[1],
        #     'view_type': 'form',
        #     'res_model': 'stock.product.move',
        #     'res_id': b2b_id,
        #     'type': 'ir.actions.act_window',
        #     'nodestroy': True,
        #     'target': 'new',
        #     'domain': '[]',
        #     'context': context
        # }

        # stock_product_move_obj = self.pool.get('stock.product.move')

        # return stock_product_move_obj.get_act_win(cr, uid, 'action_open_stock_product_report', prod_id, context=context)

        context.update({
            'move_reserved': True,
            'default_reason': 'Move reserved products between warehouses',
            'reserved_move_id': reserved_move.id,
            'product_id': reserved_move.product_id.id,
        })

        return self.pool.get('stock.product.report.line').action_open_stock_move(cr, uid, [from_b2b_id], context=context)

    def action_openep_partial_reserve(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        partial_obj = self.pool.get("stock.partial.reserve")

        context = dict(context, active_ids=ids, active_model=self._name)
        partial_id = partial_obj.create(cr, uid, {}, context=context)

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_partial_move_reserve')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': partial_id,
            'context': context,
        })

        return act_win

    def action_back_for_one_product(self, cr, uid, ids, context=None):
        context = dict(sign=SIGN_CANCEL, transaction_code=SIGN_CANCEL_VALUE)
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        # When single back to shelf then update original transaction to CXL too.
        # But, before, save warehouse names because after calling revert_assign they will change their names
        warehouses = {sm.id: sm.warehouse.name for sm in self.browse(cr, uid, ids)}
        if self.revert_assign(cr, uid, ids, context):

            self.write(cr, uid, ids, {'back_flag': True})

            picking_ids = []
            for sm in self.browse(cr, uid, ids):
                # Then, update original transaction to CXL too.
                self.update_transactions_table(cr, uid, server_id, server_obj, sm, warehouses[sm.id], context)
                picking_ids.append(sm.picking_id.id)

            wf_service = netsvc.LocalService("workflow")
            picking_ids = list(set(picking_ids))
            for picking_id in picking_ids:
                wf_service.trg_validate(uid, 'stock.picking', picking_id, 'revert_item_signal', cr)

        return False

    def action_remove_back_flag(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'back_flag': False})
        return True

    def check_mssql_multibin(self, cr, uid, itemids=None, warehouse=None, autorelease=False, context=None):
        """
            Function returns items what use different bins in one warehouse.
            If autorelease is True, then system will try to release bins with qty = 0.
            Res has following format:
            [
                (warehouse, item, [(stockid, bin, qty),...]),
                (warehouse, item, [(stockid, bin, qty),...]),
            ]
        """
        res = None
        line_delimiter = ';'
        bin_delimiter = '|'
        if itemids and warehouse:
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_sale_servers(cr, uid, single=True)
            if server_id:
                params = [warehouse] + itemids[:]

                sql = """
                    SELECT
                        warehouse,
                        concat(id_delmar, '/', size) as itemid,
                        stock_bins = cast(replace(replace((
                            SELECT DISTINCT REPLACE(rtrim(CAST(stockid AS varchar)), ' ', '_') + '{bin_delimiter}' + replace(rtrim(coalesce(bin, '')), ' ', '_') + '{bin_delimiter}' + CAST(sum(adjusted_qty) AS varchar) as 'data()'
                            FROM TRANSACTIONS
                            WHERE id_delmar = t.id_delmar and size = t.size
                                AND warehouse = t.WAREHOUSE
                                AND status != 'Posted'
                            GROUP BY id_delmar, size, stockid, replace(rtrim(coalesce(bin, '')), ' ', '_')
                            for xml path('')), ' ', '{line_delimiter}'),
                            '_', ' ')
                            as varchar(max))
                    FROM transactions t
                    WHERE t.status != 'Posted'
                        AND t.warehouse = ?
                        AND concat(id_delmar, '/', size) in ({itemids})
                        -- and replace(rtrim(coalesce(t.bin, '')), ' ', '_') != ''
                    GROUP BY warehouse, id_delmar, size
                    HAVING COUNT(distinct replace(rtrim(coalesce(bin, '')), ' ', '_')) > 1
                """.format(
                        line_delimiter=line_delimiter,
                        bin_delimiter=bin_delimiter,
                        itemids=",".join(['?']*len(itemids)),
                    )
                sql_res = server_data.make_query(cr, uid, server_id, sql, params=params, select=True, commit=False)

                if sql_res:
                    res = []
                    bin_obj = self.pool.get('stock.bin')
                    for wh, itemid, bins in sql_res:
                        to_report = []
                        if autorelease:
                            unique_bins = set([])
                            positive_bins = set([])
                            for line in bins.split(line_delimiter):
                                stockid_, bin_, qty_ = line.split(bin_delimiter)
                                unique_bins.add(bin_)

                                if int(qty_):
                                    to_report.append(tuple([stockid_, bin_, qty_]))
                                    positive_bins.add(bin_)

                            to_release = list(unique_bins - positive_bins)
                            bin_obj.release_bins(cr, uid, to_release, wh, server_id)
                            if len(positive_bins) < 2:
                                to_report = []

                        else:
                            to_report = [tuple(x.split(bin_delimiter)) for x in bins.split(line_delimiter)]

                        if to_report:
                            res.append(tuple([wh, itemid, to_report]))
        return res

    def check_mssql_multiitem(self, cr, uid, itemids=None, warehouse=None, context=None):
        """
            Function returns bins what used for selected items and where there are some other items with same bin.
            Function ignore bins like IGNORE_BINS
            Res has following format:
            [
                (warehouse, bin, [(stockid, itemid, qty),...]),
                (warehouse, bin, [(stockid, itemid, qty),...]),
            ]
        """
        IGNORE_BINS = ['', 'NULL', 'NONE']
        res = None
        line_delimiter = ';'
        bin_delimiter = '|'
        if itemids:
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_sale_servers(cr, uid, single=True)

            if server_id:

                params = [warehouse, warehouse] + itemids[:]
                sql = """
                    SELECT
                        t.warehouse,
                        replace(rtrim(coalesce(t.bin, '')), ' ', '_') as bin,
                        bin_items = cast(replace(replace((
                            select distinct replace(rtrim(stockid), ' ', '_') + '{bin_delimiter}' + replace(rtrim(id_delmar), ' ', '_') + '/' + coalesce(size, '0000') + '{bin_delimiter}' + CAST(sum(coalesce(adjusted_qty, 0)) AS varchar) as 'data()'
                            from TRANSACTIONS
                            where replace(rtrim(coalesce(bin, '')), ' ', '_') = replace(rtrim(coalesce(t.bin, '')), ' ', '_')
                            and warehouse = t.WAREHOUSE
                            AND status != 'Posted'
                            group by replace(rtrim(id_delmar), ' ', '_'), coalesce(size, '0000'), replace(rtrim(stockid), ' ', '_'), replace(rtrim(coalesce(bin, '')), ' ', '_')
                            for xml path('')), ' ', '{line_delimiter}'),
                            '_', ' ')
                        as varchar(max))
                    FROM transactions t
                        where t.status != 'Posted'
                        and t.warehouse = ?
                        and replace(rtrim(coalesce(t.bin, '')), ' ', '_') in (
                            select distinct replace(rtrim(coalesce(bin, '')), ' ', '_')
                            from transactions
                            where status != 'Posted'
                                and warehouse = ?
                                and concat(id_delmar, '/', size) in ({itemids})
                                {ignore_bins}
                        )
                    group by warehouse, replace(rtrim(coalesce(bin, '')), ' ', '_')
                    having count(distinct concat(id_delmar, '/', size)) > 1
                """.format(
                        line_delimiter=line_delimiter,
                        bin_delimiter=bin_delimiter,
                        itemids=",".join(['?']*len(itemids)),
                        ignore_bins="""and replace(rtrim(coalesce(bin, '')), ' ', '_') not in ('{bins}')""".format(
                                                                                                                bins="','".join(IGNORE_BINS)
                                                                                                            ) if IGNORE_BINS else '',
                    )
                sql_res = server_data.make_query(cr, uid, server_id, sql, params=params, select=True, commit=False)

                if sql_res:
                    res = []
                    for wh_, bin_, items_ in sql_res:
                        to_report = [tuple(x.split(bin_delimiter)) for x in items_.split(line_delimiter)]

                        if to_report:
                            res.append(tuple([wh_, bin_, to_report]))

        return res

    def get_record_info(self, cr, uid, ids, context=None):
        """Supplementary method.
        Getting sale_order record info for ambiguous mapping into transactions table."""
        logger.info('Trying to get sale record_info for moves: %s' % ids)
        sm = self.browse(cr, uid, ids and ids[0])
        product_qty = sm.product_qty
        default_code = sm.product_id.default_code
        ring_size = self.pool.get('ring.size').convert_to_4digit_name(cr, uid, sm.size_id.name)
        # Take sale order from stock_move if corresponding delivery order in 'done' state
        # and is_set flag selected.
        # Else take sale order from stock_picking table.

        so_id = None
        sm_src = sm.box_parent_move_id or sm
        if (sm_src.picking_id.state == 'done' and sm_src.picking_id.is_set) or (sm_src.set_parent_order_id.state == 'done' and sm_src.set_parent_order_id.is_set):
            so_id = sm_src.sale_line_id.set_parent_order_id or sm_src.sale_line_id.order_id
        else:
            so_id = sm_src.picking_id.sale_id or sm_src.set_parent_order_id.sale_id

        #so_id = (sm.sale_line_id.set_parent_order_id or sm.sale_line_id.order_id) \
        #    if (sm.picking_id.state == 'done' and sm.picking_id.is_set) \
        #    else sm.picking_id.sale_id if sm.picking_id else sm.box_parent_move_id.picking_id.sale_id

        po_number = so_id and so_id.po_number or None
        external_customer_order_id = so_id and so_id.external_customer_order_id or None
        customerid = so_id and so_id.partner_id.ref or None

        return dict(po_number=po_number,
                    external_customer_order_id=external_customer_order_id,
                    product_qty=product_qty,customerid=customerid,
                    itemid='{}/{}'.format(default_code, ring_size),
                    )

stock_move()
