# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
from tools.translate import _
from tools import drop_view_if_exists
from datetime import date, timedelta, datetime
import time


class stock_inventory(osv.osv):
    _inherit = "stock.inventory"
    _description = "Inventory Delmar"

    def write(self, cr, uid, ids, vals, context=None):
        bin_obj = self.pool.get('stock.bin')
        bin = vals.get('bin', False)
        bin_id = bin_obj.get_bin_id_by_name(cr, uid, bin)

        if bin_id:
            vals.update({'bin_id': bin_id})

        return super(stock_inventory, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'name': fields.char('Inventory Reference', size=255, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'create_uid': fields.many2one('res.users', 'Creator'),
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'bin_id': fields.many2one('stock.bin', 'Bin object'),
        'reason': fields.text('Reason'),
        'state': fields.selection( (('draft', 'Draft'), ('done', 'Done'), ('confirm','Confirmed'),('cancel','Cancelled')), 'State', readonly=True, select=False),
        'company_id': fields.many2one('res.company', 'Company', required=True, select=False, readonly=True, states={'draft':[('readonly',False)]}),
    }

    def action_confirm(self, cr, uid, ids, context=None):
        """ Confirm the inventory and writes its finished date
        @return: True
        """
        if context is None:
            context = {}
        # to perform the correct inventory corrections we need analyze stock location by
        # location, never recursively, so we use a special context
        product_context = dict(context, compute_child=False)
        states = context.get('states', ['done', 'assigned', 'reserved'])
        location_obj = self.pool.get('stock.location')
        new_moves_count = 0

        for inv in self.browse(cr, uid, ids, context=context):
            move_ids = []
            for line in inv.inventory_line_id:
                pid = line.product_id.id
                product_context.update(uom=line.product_uom.id,
                                       date=inv.date,
                                       prodlot_id=line.prod_lot_id.id,
                                       bin=line.bin,
                                       ring_size=line.size_id.id)
                amount = location_obj._product_get(cr, uid, line.location_id.id, [pid], product_context, states)[pid]

                change = line.product_qty - amount
                lot_id = line.prod_lot_id.id
                size_id = line.size_id and line.size_id.id
                # if change:
                if change == 0:
                    sql = """SELECT id
                    FROM stock_move
                    WHERE product_id = '%s' AND location_dest_id = '%s'""" % (line.product_id.id, line.location_id.id)
                    cr.execute(sql)
                    array_id = cr.fetchone()
                    if (array_id):
                        continue
                new_moves_count += 1
                location_id = line.product_id.product_tmpl_id.property_stock_inventory.id

                invredid = self.pool.get('stock.move').get_invredid_for_stock_move(cr, uid, None, line.bin, line.location_id.id, line.product_id.id, line.size_id)

                value = {
                    'name': 'INV:' + str(line.inventory_id.id) + ':' + line.inventory_id.name,
                    'product_id': line.product_id.id,
                    'product_uom': line.product_uom.id,
                    'prodlot_id': lot_id,
                    'date': inv.date,
                    'size_id': size_id,
                    'bin': line.bin,
                    'bin_id': line.bin_id and line.bin_id.id or False,
                    'invredid': invredid,
                }

                if change >= 0:
                    value.update({
                        'product_qty': change,
                        'location_id': location_id,
                        'location_dest_id': line.location_id.id,
                    })
                else:
                    value.update({
                        'product_qty': -change,
                        'location_id': line.location_id.id,
                        'location_dest_id': location_id,
                    })

                if not invredid and not context.get('is_force_stock_update', False):
                    raise osv.except_osv(_('Warning!'), _('Can not do the stock update. Can not find invredid for item %s on bin %s!' % (line.product_id and line.product_id.default_code or "", line.bin or "",)))

                move_ids.append(self._inventory_line_hook(cr, uid, line, value))

            message = _("Inventory '%s' is done. (%s new moves)") % (inv.name, new_moves_count)
            if not context.get('is_force_stock_update', False) or new_moves_count > 0:
                self.log(cr, uid, inv.id, message)
            self.write(cr, uid, [inv.id], {'state': 'confirm', 'move_ids': [(6, 0, move_ids)]})
            self.pool.get('stock.move').action_confirm(cr, uid, move_ids, context=context)

            if not context.get('is_force_stock_update', False):
                context.update({'reason': inv.reason})

                self.write_stock_move_into_transactions_table(cr, uid, move_ids, context=context)
            else:
                self.write(cr, uid, inv.id, {'reason': 'Force sync of OpenERP stock'})

        return True

    def write_stock_move_into_transactions_table(self, cr, uid, move_ids, context=None):
        if context is None:
            context = {}

        server_data = self.pool.get('fetchdb.server')
        stock_move_data = self.pool.get('stock.move')

        server_id = server_data.get_sale_servers(cr, uid, single=True)

        if server_id:
            for move in stock_move_data.browse(cr, uid, move_ids, context=context):
                if move.product_qty:

                    delmarid = move.product_id and move.product_id.default_code or False
                    size_postfix = stock_move_data.get_size_postfix(cr, uid, move.size_id)
                    itemid = delmarid and (delmarid + '/' + size_postfix) or False

                    if not itemid:
                        raise osv.except_osv(_('Warning!'), _('Can not find ITEM for stock move.'))

                    src_loc = move.location_id
                    dest_loc = move.location_dest_id
                    sign = 1 if src_loc and src_loc.name == 'Inventory loss' else -1
                    stock_loc = src_loc if sign < 0 else dest_loc
                    stockid = stock_loc and stock_loc.name or False
                    move_bin = move.bin_id and move.bin_id.name or ''

                    warehouse = stock_move_data.get_warehouse_by_move_location(cr, uid, stock_loc and stock_loc.id)
                    reserved_flag = stock_move_data.get_reserved_flag(cr, uid, itemid, warehouse, stockid, server_id)
                    invredid = stock_move_data.get_invredid_for_mssql_row(cr, uid, itemid, warehouse, stockid, move_bin)

                    qty = move.product_qty and int(move.product_qty) or 0
                    trans_tag = move.sale_line_id.id or None
                    description = move.sale_line_id and move.sale_line_id.name or move.product_id and move.product_id.name

                    transactionCode = 'ADJ'
                    reason = context.get('reason', False)
                    if reason and reason == 'Could not locate':
                        transactionCode = 'CDF'
                        if sign > 0:
                            raise osv.except_osv(_('Warning!'), _('It\'s forbidden to add qty with "could not locate" reason'))

                    if not warehouse:
                        raise osv.except_osv(_('Warning!'), _('Can not find WAREHOUSE for stock move.'))

                    if not stockid:
                        raise osv.except_osv(_('Warning!'), _('Can not find STOCKID for stock move.'))

                    unit_price = move.sale_line_id and move.sale_line_id.price_unit or 0
                    customer_ref = move.sale_line_id.order_id and move.sale_line_id.order_id.partner_id.ref or None
                    params = (
                        itemid or 'null',  # itemid
                        transactionCode,  # TransactionCode
                        qty,  # qty
                        warehouse or 'null',  # warehouse
                        description or 'null',  # itemdescription
                        move_bin,  # move.bin or 'null',  # bin
                        stockid or 'null',  # stockid
                        'TRS',  # TransactionType
                        delmarid,  # id_delmar
                        sign,  # sign
                        (qty * sign) or '0',  # adjusted_qty
                        size_postfix,  # size
                        invredid,  # invredid
                        customer_ref,  # CustomerID
                        delmarid,  # ProductCode
                        'ADJDEF',  # InvoiceNo
                        reserved_flag or 0,  # reserved
                        trans_tag,  # trans_tag
                        unit_price * (-sign),  # unit_price
                    )
                    # add stock_move id to transaction
                    context.update({'erp_sm_id': move.id})
                    stock_move_data.write_into_transactions_table(cr, uid, server_id, params, context=context)

        return True

    # This function set status = Posted for all Pending transactions (per product, size, wh, stock, bin) and create STK with force set qty
    def force_adjustment_pending(self, cr, uid, product_id, size_id, loc_id, bin_name, qty, invoiceno=None):
        move_obj = self.pool.get('stock.move')
        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        server_obj = self.pool.get('fetchdb.server')
        location_obj = self.pool.get('stock.location')
        warehouse_obj = self.pool.get('stock.warehouse')

        server_id = server_obj.get_sale_servers(cr, uid, single=True)

        if server_id:
            size_postfix = '0000'
            size = False
            if size_id:
                size = size_obj.browse(cr, uid, size_id)
                size_postfix = move_obj.get_size_postfix(cr, uid, size)

            product = product_obj.browse(cr, uid, product_id)
            id_delmar = product.default_code
            itemid = id_delmar and (id_delmar + '/' + size_postfix) or False

            location = location_obj.browse(cr, uid, loc_id)
            loc_name = location.name
            wh_id = location_obj.get_warehouse(cr, uid, loc_id)
            wh_name = warehouse_obj.browse(cr, uid, wh_id).name

            reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, wh_name, loc_name, server_id)
            invredid = self.pool.get('stock.move').get_invredid_for_mssql_row(cr, uid, itemid, wh_name, loc_name, bin_name, ignore_bin=True)

            # Pending -> Posted
            posted_sql = """UPDATE TRANSACTIONS
                        SET STATUS = 'Posted'
                        WHERE 1>0
                        AND id_delmar = ?
                        AND size = ?
                        AND warehouse = ?
                        AND stockid = ?
                        AND bin = ?
                        """
            posted_params = [
                id_delmar,
                size_postfix,
                wh_name,
                loc_name,
                bin_name
            ]
            server_obj.make_query(cr, uid, server_id, posted_sql, params=posted_params, select=False, commit=True)

            # Creating STK record with new qty
            params = (
                itemid or 'null',  # itemid
                'STK',  # TransactionCode
                qty,  # qty
                wh_name or 'null',  # warehouse
                product.name or 'null',  # itemdescription
                bin_name or '',  # move.bin or '',  # bin
                loc_name or 'null',  # stockid
                'STK',  # TransactionType
                id_delmar,  # id_delmar
                1,  # sign
                qty,  # adjusted_qty
                size_postfix,  # size
                invredid or 'null',  # invredid
                None,  # CustomerID
                id_delmar,  # ProductCode
                invoiceno or 'null',  # InvoiceNo
                reserved_flag or 0,  # reserved
                None,  # trans_tag
                0,  # unit_price
            )
            move_obj.write_into_transactions_table(cr, uid, server_id, params)
            return True
        return False

    def _check_feospp_transactions(self, cr, uid, context=None):
        data_obj = self.pool.get('ir.model.data')
        mail_obj = self.pool.get('mail.message')
        server_obj = self.pool.get('fetchdb.server')

        cc_group = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_feospp_trans_control')
        emails = self.pool.get('res.groups').get_user_emails(cr, uid, [cc_group[1]])[cc_group[1]] or []

        if not emails:
            return True

        report_date = (datetime.utcnow() - timedelta(1)).strftime('%m-%d-%Y')
        fail_msg = "ERROR. Can't create report by %s. MS SQL is not available." % (report_date)

        vals = {
            'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            'subject': 'FEOSPP daily inventory e-mail batch report',
            'body_text': fail_msg,
            'body_html': fail_msg,
            'email_from': 'erp@delmar.com',
            'email_to': ", ".join(emails),
            'email_cc': False,
            'email_bcc': False,
            'reply_to': False,
            'auto_delete': False,
            'model': 'stock.inventory',
            'res_id': False,
            'mail_server_id': False,
            # 'attachments': False,
            'attachment_ids': False,
            'message_id': False,
            'state': 'outgoing',
            'subtype': 'html',
        }

        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if server_id:

            sql = """
                SELECT
                    t.TIMESTAMP,
                    t._USER_CREATOR,
                    t.ITEMID,
                    t.ADJUSTED_QTY,
                    t.TRANSACTIONCODE,
                    t.WAREHOUSE,
                    t.STOCKID,
                    t.BIN,
                    t.INVOICENO
                FROM dbo.transactions t
                WHERE 1=1
                AND t._USER_CREATOR not like 'openerp%%'
                AND t.TRANSACTDATE = '%s'
                AND t.STOCKID = 'FEOSPP'
            ;""" % (report_date)
            result = server_obj.make_query(cr, uid, server_id, sql, select=True, commit=False)

            if result:
                table_rows = []
                csv_rows = []

                columns = ['Time', 'User', 'Item', 'QTY', 'Trans Code', 'Warehouse', 'Stock', 'Bin', 'InvoiceNo']
                table_header = """<thead><tr>%s</tr></thead>""" % ("".join(["<td>%s</td>" % (x) for x in columns]))
                csv_rows.append(",".join([str(x) for x in columns]))

                for res in result:
                    table_rows.append("".join(["<td>%s</td>" % (x) for x in res]))
                    csv_rows.append(",".join([str(x) for x in res]))

                table_rows.append("<td></td>" * len(columns))
                table_body = """<tbody>%s</tbody>""" % ("\n".join(["<tr>%s</tr>" % (x) for x in table_rows]))
                table = "<table>%s\n%s</table>" % (table_header, table_body)

                csv = "\n".join(x for x in csv_rows)

                body_html = "%s transactions for FEOSPP by %s. \n%s" % (len(result), report_date, table if result else '')
                body_plain = "%s transactions for FEOSPP by %s. \n%s" % (len(result), report_date, csv if result else '')

                vals.update({
                    'body_text': body_plain,
                    'body_html': body_html,
                })

        mail_obj.create(cr, uid, vals, context=context)
        return True

stock_inventory()


class report_stock_inventory(osv.osv):
    _inherit = 'report.stock.inventory'
    _description = "Stock Statistics Delmar"

    _columns = {
        'size_id': fields.many2one('ring.size', 'Size'),
        'bin_id': fields.many2one('stock.bin', 'Shelf'),
    }

    def init(self, cr):
        cr.execute("""
            DROP AGGREGATE IF EXISTS array_accum(anyelement);

            CREATE AGGREGATE array_accum (
            sfunc = array_append,
            basetype = anyelement,
            stype = anyarray,
            initcond = '{}'
            );
        """)
        drop_view_if_exists(cr, 'report_stock_inventory')
        cr.execute("""
CREATE OR REPLACE view report_stock_inventory AS (
    (SELECT
        min(m.id) as id,
        m.date as date,
        CASE WHEN m.date IS NULL THEN NULL ELSE to_char(m.date, 'YYYY') END as year,
        CASE WHEN m.date IS NULL THEN NULL ELSE to_char(m.date, 'MM') END as month,
        m.address_id as partner_id, m.location_id as location_id,
        m.product_id as product_id, pt.categ_id as product_categ_id, l.usage as location_type,
        m.company_id,
        m.state as state, m.prodlot_id as prodlot_id,

        coalesce(sum(-pt.standard_price * m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as value,
        coalesce(sum(-m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
        m.size_id as size_id,
        m.bin_id as bin_id
    FROM
        stock_move m
            LEFT JOIN stock_picking p ON (m.picking_id=p.id)
            LEFT JOIN product_product pp ON (m.product_id=pp.id)
                LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
            LEFT JOIN product_uom u ON (m.product_uom=u.id)
            LEFT JOIN stock_location l ON (m.location_id=l.id)
    GROUP BY
        m.id, m.product_id, m.product_uom, pt.categ_id, m.address_id, m.location_id,  m.location_dest_id,
        m.prodlot_id, m.date, year, month, m.state, l.usage, m.company_id, pt.uom_id,
        m.size_id, m.bin_id
) UNION ALL (
    SELECT
        -m.id as id,
        m.date as date,
        CASE WHEN m.date IS NULL THEN NULL ELSE to_char(m.date, 'YYYY') END as year,
        CASE WHEN m.date IS NULL THEN NULL ELSE to_char(m.date, 'MM') END as month,
        m.address_id as partner_id, m.location_dest_id as location_id,
        m.product_id as product_id, pt.categ_id as product_categ_id, l.usage as location_type,
        m.company_id,
        m.state as state, m.prodlot_id as prodlot_id,
        coalesce(sum(pt.standard_price * m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as value,
        coalesce(sum(m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
        m.size_id as size_id,
        m.bin_id as bin_id
    FROM
        stock_move m
            LEFT JOIN stock_picking p ON (m.picking_id=p.id)
            LEFT JOIN product_product pp ON (m.product_id=pp.id)
                LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
            LEFT JOIN product_uom u ON (m.product_uom=u.id)
            LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
    GROUP BY
        m.id, m.product_id, m.product_uom, pt.categ_id, m.address_id, m.location_id, m.location_dest_id,
        m.prodlot_id, m.date, year, month, m.state, l.usage, m.company_id, pt.uom_id,
        m.size_id, m.bin_id
    )
);
        """)
report_stock_inventory()


class stock_inventory_line(osv.osv):
    _inherit = "stock.inventory.line"
    _description = "Inventory Line Delmar"

    _columns = {
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'bin': fields.char(size=256, string="Bin"),
        'bin_id': fields.many2one('stock.bin', 'Bin object'),
        'company_id': fields.related('inventory_id','company_id',type='many2one',relation='res.company',string='Company',store=True, select=False, readonly=True),
    }

    def write(self, cr, uid, ids, vals, context=None):
        bin_obj = self.pool.get('stock.bin')
        bin = vals.get('bin', False)
        bin_id = bin_obj.get_bin_id_by_name(cr, uid, bin)

        if bin_id:
            vals.update({'bin_id': bin_id})

        return super(stock_inventory_line, self).write(cr, uid, ids, vals, context=context)

stock_inventory_line()
