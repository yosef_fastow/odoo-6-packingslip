# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import logging
from tools import float_compare


class stock_location(osv.osv):
    _inherit = 'stock.location'

    def _product_reserve(self, cr, uid, ids, product_id, product_qty, context=None, lock=False, size_id=None):
        result = []
        amount = 0.0
        if context is None:
            context = {}
        uom_obj = self.pool.get('product.uom')
        uom_rounding = self.pool.get('product.product').browse(cr, uid, product_id, context=context).uom_id.rounding
        if context.get('uom'):
            uom_rounding = uom_obj.browse(cr, uid, context.get('uom'), context=context).rounding
        for lock_id in ids:
            for id in self.search(cr, uid, [('location_id', 'child_of', lock_id)]):
                if lock:
                    try:
                        # Must lock with a separate select query because FOR UPDATE can't be used with
                        # aggregation/group by's (when individual rows aren't identifiable).
                        # We use a SAVEPOINT to be able to rollback this part of the transaction without
                        # failing the whole transaction in case the LOCK cannot be acquired.

                        sql = """SELECT id FROM stock_move
                                      WHERE product_id=%s AND
                                            size_id=%s AND
                                              (
                                                (location_dest_id=%s AND
                                                 location_id<>%s AND
                                                 state='done')
                                                OR
                                                (location_id=%s AND
                                                 location_dest_id<>%s AND
                                                 state in ('done', 'assigned', 'reserved'))
                                              )
                                      FOR UPDATE of stock_move NOWAIT""" % (product_id, size_id, id, id, id, id)

                        cr.execute("SAVEPOINT stock_location_product_reserve")
                        sql = sql.replace("=None", " IS NULL ")
                        cr.execute(sql, None, log_exceptions=False)
                    except Exception:
                        # Here it's likely that the FOR UPDATE NOWAIT failed to get the LOCK,
                        # so we ROLLBACK to the SAVEPOINT to restore the transaction to its earlier
                        # state, we return False as if the products were not available, and log it:
                        cr.execute("ROLLBACK TO stock_location_product_reserve")
                        logger = logging.getLogger('stock.location')
                        logger.warn("Failed attempt to reserve %s x product %s, likely due to another transaction already in progress. Next attempt is likely to work. Detailed error available at DEBUG level.", product_qty, product_id)
                        logger.debug("Trace of the failed product reservation attempt: ", exc_info=True)
                        return False

                # XXX TODO: rewrite this with one single query, possibly even the quantity conversion
                sql = """SELECT product_uom, sum(product_qty) AS product_qty
                              FROM stock_move
                              WHERE location_dest_id=%s AND
                                    location_id<>%s AND
                                    product_id=%s AND
                                    size_id=%s AND
                                    state='done'
                              GROUP BY product_uom
                           """ % (id, id, product_id, size_id)
                sql = sql.replace("=None", " IS NULL ")
                cr.execute(sql)
                results = cr.dictfetchall()
                sql = """SELECT product_uom,-sum(product_qty) AS product_qty
                              FROM stock_move
                              WHERE location_id=%s AND
                                    location_dest_id<>%s AND
                                    product_id=%s AND
                                    size_id=%s AND
                                    state in ('done', 'assigned', 'reserved')
                              GROUP BY product_uom
                           """ % (id, id, product_id, size_id)
                sql = sql.replace("=None", " IS NULL ")
                cr.execute(sql)
                results += cr.dictfetchall()
                total = 0.0
                results2 = 0.0
                for r in results:
                    amount = uom_obj._compute_qty(cr, uid, r['product_uom'], r['product_qty'], context.get('uom', False))
                    results2 += amount
                    total += amount
                if total <= 0.0:
                    continue

                amount = results2
                compare_qty = float_compare(amount, 0, precision_rounding=uom_rounding)
                if compare_qty == 1:
                    if amount > min(total, product_qty):
                        amount = min(product_qty, total)
                    result.append((amount, id))
                    product_qty -= amount
                    total -= amount
                    if product_qty <= 0.0:
                        return result
                    if total <= 0.0:
                        continue
        return False

    _sql_constraints = [
        ('name_uniq', 'unique (location_id, name)',
            'The location name and parent location should be unique pair!')
    ]

    def get_child_locs_by_wh(self, cr, uid, ids, context=None, get_all_locs=False):
        res = []

        for base_loc in self.browse(cr, uid, ids, context=context):
            wh_id = 0 if get_all_locs else self.get_warehouse(cr, uid, base_loc.id)

            cur_wh = [x for x in res if x.get('id', False) == wh_id]

            if not cur_wh:
                cur_wh = {'id': wh_id, 'lock_ids': []}
                res.append(cur_wh)
            else:
                cur_wh = cur_wh[0]

            cur_wh['lock_ids'].append(base_loc.id)

            for child_loc_id in self.search(cr, uid, [('location_id', 'child_of', base_loc.id)], context=context):
                cur_wh['lock_ids'].append(child_loc_id)

        return res

    def get_internal_loc_ids(self, cr, uid, except_virtual=False, to_string=False, except_by_name=None, context=None):

        if not except_by_name:
            except_by_name = []

        where_name = ""
        if except_by_name:
            where_name = "AND name NOT IN ('%s')" % ("','".join(except_by_name))

        sql = """   SELECT id as id
                    FROM stock_location
                    WHERE active = True AND usage = 'internal'
                        AND chained_picking_type is null
                        AND chained_location_type = 'none'
                        %s
        """ % (where_name)

        if except_virtual:
            sql += """  EXCEPT
                        SELECT lot_stock_id as id
                            FROM stock_warehouse
                            WHERE virtual = true
            """

        if to_string:
            sql = """   SELECT array_to_string(array_accum(a.id), ',') as locations_ids
                        FROM (%s) a
            """ % (sql)
            cr.execute(sql)
            locations_res = cr.fetchone()
            return locations_res and locations_res[0] or False

        cr.execute(sql)
        return cr.dictfetchall()

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):

        if args is None:
            args = []

        if context is None:
            context = {}
        args = args[:]

        if not (name == '' and operator == 'ilike'):
            args += [(self._rec_name, operator, name)]

        res = []
        ids = []
        top_ids = []
        top_search = context.get('top_search', '').split(',')
        for search in top_search:
            search = search.strip()
            if search:
                top_ids += self.search(cr, uid, args + [('complete_name', 'ilike', search)])

        if top_ids:
            uniq_top_ids = []

            for top in top_ids:
                if top not in uniq_top_ids:
                    uniq_top_ids.append(top)
                    res += self.name_get(cr, uid, [top])
            top_ids = uniq_top_ids

        if not limit or (len(top_ids) < limit):
            limit = limit - len(top_ids) if limit else None
            domain = args + [('id', 'not in', top_ids)] if top_ids else args
            ids = self.search(cr, uid, domain, limit=limit, context=context)
            res += self.name_get(cr, uid, ids)

        return res

    def get_warehouse_and_location_names(self, cr, uid, loc_id):
        location_name = self.read(cr, uid, loc_id, ['name']).get('name')

        warehouse_name = None
        warehouse_id = self.get_warehouse(cr, uid, loc_id)
        if warehouse_id:
            warehouse_obj = self.pool.get('stock.warehouse')
            warehouse_data = warehouse_obj.read(cr, uid, warehouse_id, ['name'])
            warehouse_name = warehouse_data.get('name')

        return warehouse_name, location_name

stock_location()
