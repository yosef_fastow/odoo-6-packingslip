# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import netsvc


class stock_picking_advanced_cancel(osv.osv_memory):
    _name = "stock.picking.advanced.cancel"

    _columns = {
        'picking_id': fields.many2one(
            'stock.picking',
            'Delivery Order',
            required=True,
            readonly=True),
        'location_id': fields.many2one(
            'stock.location',
            'Return Location'),
        'autoselect_bin': fields.boolean('Choose Bins Automatically'),
        'bin_to': fields.char('Or Use Bin', size=50),
        'line_ids': fields.one2many(
            'stock.picking.advanced.cancel.line',
            'wizard_id',
            'Lines'),
    }

    _defaults = {
        'autoselect_bin': True,
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_picking_advanced_cancel, self).default_get(
            cr, uid, fields, context=context)
        lines = self._get_lines(cr, uid, context['picking_id'])
        if not lines:
            raise osv.except_osv(
                _('Warning!'),
                'Nothing to return. Please use "Cancel" button instead')
        res.update({
            'picking_id': context['picking_id'],
            'line_ids': lines,
        })
        return res

    def onchange_location_id(self, cr, uid, ids, picking_id, location_id):
        result = {'value': {
            'line_ids': self._get_lines(cr, uid, picking_id, location_id)
        }}
        return result

    def _get_lines(self, cr, uid, picking_id, location_id=None):
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id)
        result = []
        for move in picking.move_lines:
            if move.state not in ('assigned', 'reserved', 'done'):
                continue
            if not location_id or location_id == move.location_id.id:
                bin_to = move.bin_id.name if move.bin_id else move.bin
            else:
                bin_to = False
            result.append({
                'move_id': move.id,
                'product_id': move.product_id.id,
                'size_id': move.size_id.id or False,
                'product_qty': move.product_qty,
                'price_unit': move.price_unit,
                'location_id': location_id or move.location_id.id,
                'bin_to': bin_to,
            })
        return result

    def _get_bin_name(self, cr, uid, line):
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        bin_create_obj = self.pool.get('stock.bin.create')
        wizard = line.wizard_id
        id_delmar = line.product_id.default_code
        size_name = self.pool.get('ring.size').get_4digit_name(
            cr, uid, line.size_id.id)
        itemid = id_delmar + '/' + size_name
        location = line.location_id
        warehouse_name, stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, location.id)
        bin_name = line.bin_to or wizard.bin_to
        bin_available = False
        if bin_name:
            bin_available = bin_obj.is_bin_available_for_product(
                cr, uid, warehouse_name, bin_name, id_delmar, size_name)
        if not bin_available and wizard.autoselect_bin:
            bin_name = bin_obj.get_mssql_bin_for_product(
                cr, uid, itemid, warehouse_name, stockid).get(itemid)
            if not bin_name:
                bin_name = bin_create_obj.create_bin(
                    cr, uid, location.id, stockid, line.product_id.id,
                    size_name, line.product_qty)
        if not bin_name:
            error_msg = 'Bin not found in {0}/{1} for {2}, qty {3}'.format(
                warehouse_name, stockid, itemid, int(line.product_qty))
            raise osv.except_osv(_('Error'), error_msg)
        return bin_name

    def do_cancel(self, cr, uid, ids, context=None):
        bin_obj = self.pool.get('stock.bin')
        move_obj = self.pool.get('stock.move')
        sale_order_obj = self.pool.get('sale.order')
        wizard_id = ids[0]
        wizard = self.browse(cr, uid, wizard_id, context=context)
        sale_line_ids = []
        for line in wizard.line_ids:
            bin_name = self._get_bin_name(cr, uid, line)
            write_vals = {}
            move = line.move_id
            if bin_name != move.bin_id.name:
                bin_id = bin_obj.get_bin_id_by_name(cr, uid, bin_name)
                write_vals['bin_id'] = bin_id
            if line.location_id != move.location_id:
                write_vals['location_id'] = line.location_id.id
            if write_vals:
                write_vals['keep_transactions'] = True
                sale_line_ids.append(move.sale_line_id.id)
                move_obj.write(cr, uid, [move.id], write_vals)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(
            uid, 'stock.picking', wizard.picking_id.id, 'button_cancel', cr)
        to_sync = sale_order_obj.check_sync_product(cr, uid, sale_line_ids)
        for to_sync_item in to_sync:
            if to_sync_item:
                sale_order_obj.sync_product_stock(
                    cr, uid, to_sync_item['product_id'],
                    to_sync_item['size_id'], context=context)
        return {'type': 'ir.actions.act_window_close'}

stock_picking_advanced_cancel()


class stock_picking_advanced_cancel_line(osv.osv_memory):
    _name = "stock.picking.advanced.cancel.line"

    _columns = {
        'move_id': fields.many2one(
            'stock.move',
            'Stock Move',
            required=True,
            readonly=True),
        'product_id': fields.related(
            'move_id',
            'product_id',
            type='many2one',
            string="Product",
            readonly=True,
            relation='product.product'),
        'size_id': fields.related(
            'move_id',
            'size_id',
            type='many2one',
            string="Ring Size",
            readonly=True,
            relation='ring.size'),
        'product_qty': fields.related(
            'move_id',
            'product_qty',
            string="Quantity",
            readonly=True),
        'price_unit': fields.related(
            'move_id',
            'price_unit',
            string="Unit Price",
            readonly=True),
        'location_id': fields.many2one(
            'stock.location',
            'Return Location',
            required=True),
        'bin_to': fields.char('Bin', size=50),
        'wizard_id': fields.many2one(
            'stock.picking.advanced.cancel',
            'Cancel Wizard'),
    }

stock_picking_advanced_cancel_line()
