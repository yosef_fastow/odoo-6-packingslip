# -*- coding: utf-8 -*-
# #############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import logging
from tools.translate import _


logger = logging.getLogger('delmar_sale')

REPROCESS_TYPE_SELECTION = [
    ('W', 'Warranty'),
    ('L', 'Lost Package'),
    ('R', 'Replacement Parts'),
]

class stock_picking_reprocess_line(osv.osv_memory):
    _name = "stock.picking.reprocess.line"
    _rec_name = 'product_id'
    _columns = {
        'product_id': fields.many2one('product.product', string="Product", required=True, ondelete='CASCADE'),
        'size_id': fields.many2one('ring.size', string="Size", ondelete='CASCADE'),
        'product_qty': fields.integer("Quantity", required=True),
        'product_uom': fields.many2one('product.uom', 'Unit of Measure'),
        'move_id': fields.many2one('stock.move', "Move", ondelete='CASCADE'),
        'wizard_id': fields.many2one('stock.picking.reprocess', string="Wizard", ondelete='CASCADE'),
        'type': fields.related('wizard_id', 'type', type='selection', store=False,
                               selection=REPROCESS_TYPE_SELECTION, string='Type'),
    }

stock_picking_reprocess_line()


class stock_picking_reprocess(osv.osv_memory):
    _name = "stock.picking.reprocess"
    _rec_name = 'picking_id'

    _columns = {
        'picking_id': fields.many2one('stock.picking', 'Picking', required=True, ondelete='CASCADE'),
        'move_ids': fields.one2many('stock.picking.reprocess.line', 'wizard_id', 'Product Moves'),
        'type': fields.selection(REPROCESS_TYPE_SELECTION, 'Type', required=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}

        res = super(stock_picking_reprocess, self).default_get(cr, uid, fields, context=context)
        picking_ids = context.get('active_ids', [])
        if not picking_ids or (not context.get('active_model') == 'stock.picking') \
                or len(picking_ids) != 1:
            # Picking Reprocess may only be done for one picking at a time
            return res
        picking_id, = picking_ids
        if 'picking_id' in fields:
            res.update(picking_id=picking_id)
        if 'type' in fields:
            reprocess_type = context.get('type')
            if reprocess_type not in [el[0] for el in REPROCESS_TYPE_SELECTION]:
                reprocess_type = 'W'
            res.update(type=reprocess_type)
        if 'move_ids' in fields:
            moves = self._partial_moves_for(cr, uid, picking_id, context=context)
            res.update(move_ids=moves)
        return res

    def _partial_moves_for(self, cr, uid, picking_id, context=None):
        if not picking_id:
            return {}
        if context is None:
            context = {}
        if context.get('type') == 'R':
            # Get components if exist
            query = """SELECT
                    sm.id AS move_id,
                    COALESCE(ppl.product_id, sm.product_id) AS product_id,
                    CASE WHEN ppl.product_id IS NULL OR pc.sizeable IS TRUE
                        THEN sm.size_id
                        ELSE NULL
                        END AS size_id,
                    sm.product_qty * COALESCE(ppl.quantity, 1) AS product_qty,
                    sm.product_uom AS product_uom
                FROM stock_move sm
                LEFT JOIN product_pack_line ppl
                    ON ppl.parent_product_id = sm.product_id
                LEFT JOIN product_product pp
                    ON ppl.product_id = pp.id
                LEFT JOIN product_template pt
                    ON pp.product_tmpl_id = pt.id
                LEFT JOIN product_category pc
                    ON pt.categ_id = pc.id
                WHERE sm.picking_id = %s
            """
        else:
            query = """SELECT
                    sm.id as move_id,
                    sm.product_id as product_id,
                    sm.size_id as size_id,
                    sm.product_qty as product_qty,
                    sm.product_uom as product_uom
                FROM stock_move sm
                WHERE sm.picking_id = %s
            """
        cr.execute(query % (picking_id))
        return cr.dictfetchall() or {}

    def process(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'Reprocess may only be done one at a time'
        partial = self.browse(cr, uid, ids[0], context=context)
        partial_data = {
            'type': partial.type,
            'lines': [],
            'set_parent_order_lines': []
        }
        set_product_ids = []
        sets_lines = []
        for wizard_line in partial.move_ids:

            # Quantiny must be Positive
            if not wizard_line.product_qty or not wizard_line.product_qty > 0:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            if wizard_line.move_id.sale_line_id is None:
                line_incorect = True
                if partial.picking_id.is_set:

                    for move in partial.picking_id.set_parent_move_lines:
                        if move.product_id == wizard_line.product_id:
                            if move.product_qty >= wizard_line.product_qty:
                                if move.size_id == wizard_line.size_id or not move.size_id.id or not wizard_line.size_id:
                                    line_incorect = False
                for move in partial.picking_id.move_lines:
                    if move.product_id == wizard_line.product_id:
                        if move.product_qty >= wizard_line.product_qty:
                            if move.size_id == wizard_line.size_id or not move.size_id.id or not wizard_line.size_id:
                                line_incorect = False

                if line_incorect:
                    raise osv.except_osv(_('Warning!'), _('Rejected. Sale order don\'t have this product!'))

            if not wizard_line.move_id.is_set:
                partial_data['lines'].append({
                    'move_id': wizard_line.move_id.id,
                    'product_id': wizard_line.product_id.id,
                    'product_qty': wizard_line.product_qty,
                    'size_id': wizard_line.size_id.id or False,
                })
            else:
                sets_lines.append({
                    'move_id': wizard_line.move_id.id,
                    'product_id': wizard_line.product_id.id,
                    'product_qty': wizard_line.product_qty,
                    'size_id': wizard_line.size_id.id or False,
                })

            if wizard_line.move_id.is_set and wizard_line.move_id.set_product_id.id:
                set_product_ids.append(wizard_line.move_id.set_product_id.id)

        if partial.picking_id.id and partial.picking_id.is_set and len(sets_lines):
            partial_data['set_parent_order_lines'] = sets_lines
            for line in partial.picking_id.set_parent_move_lines:
                if line.set_product_id.id in set_product_ids:
                    components = self.pool.get('product.product').browse(cr, uid, line.set_product_id.id).set_component_ids
                    component_qum = [x.qty  for x in components if x.cmp_product_id.id ==line.product_id.id]
                    line_qty = [x.product_qty for x in partial['move_ids'] if x.product_id.id == line.set_product_id.id]
                    component_qty = (component_qum[0] or 1) * (line_qty[0] or 1)
                    partial_data['lines'].append({
                        'move_id': line.id,
                        'product_id': line.product_id.id,
                        'product_qty': component_qty,
                        'size_id': line.size_id.id or False,
                    })

        if not partial_data:
            raise osv.except_osv(_('Warning!'), _('Nothing to reprocess!'))

        picking_obj = self.pool.get('stock.picking')
        res = picking_obj.do_reprocess(cr, uid, partial.picking_id.id, partial_data, context=context)
        print res

        return {'type': 'ir.actions.act_window_close'}

stock_picking_reprocess()
