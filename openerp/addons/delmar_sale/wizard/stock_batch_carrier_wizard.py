# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _


class stock_batch_carrier_wizard(osv.osv_memory):
    _name = "stock.batch.carrier.wizard"
    _rec_name = 'address_grp'

    _columns = {
        # 'batch_id': fields.many2one('stock.picking.shipping.batch', 'Shipping Batch', required=True, readonly=True, ),
        'address_grp': fields.char('Address Grp', size=256, ),
        'picking_ids': fields.many2many('stock.picking', 'stock_picking_batch_carrier_rel', 'wizard_id', 'picking_id', string="Orders", ),
        'shipping_code_id': fields.many2one('res.shipping.code', 'Shipping Code', ),
        'sale_integration_id': fields.many2one('sale.integration', 'Type API', ),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', ),
        'shipping_code': fields.char('Shipping Code for Group', size=256, ),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_batch_carrier_wizard, self).default_get(cr, uid, fields, context=context)

        picking_ids = context.get('picking_ids', [])
        res['picking_ids'] = [(6,0,[x for x in picking_ids])]

        if picking_ids:
            picking = self.pool.get('stock.picking').browse(cr, uid, picking_ids[0])
            res['warehouse_id'] = picking.warehouses and picking.warehouses.id or False
            res['sale_integration_id'] = picking.sale_id and picking.sale_id.type_api_id and picking.sale_id.type_api_id.id or False

        return res

    def apply_shipping_code(self, cr, uid, ids, context=None):
        return_action = {'type': 'ir.actions.act_window_close'}
        for wizard in self.browse(cr, uid, ids):
            address_grp = wizard.address_grp
            incoming_code = wizard.shipping_code_id and wizard.shipping_code_id.incoming_code or False
            picking_ids = [x.id for x in wizard.picking_ids if x]

            if not address_grp:
                raise osv.except_osv(_('Warning !'), """Missing Address!""")
            elif not incoming_code:
                raise osv.except_osv(_('Warning !'), """Missing Incoming Code!""")
            elif not picking_ids:
                raise osv.except_osv(_('Warning !'), """Delivery Orders not found!""")
            else:
                res = self.pool.get('stock.picking.shipping.batch').combine_orders(cr, uid,
                    address_grp, picking_ids, incoming_code=incoming_code, )
                if res:
                    return_action = res

        return return_action

stock_batch_carrier_wizard()
