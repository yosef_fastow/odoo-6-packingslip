# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from datetime import datetime
from tools import DEFAULT_SERVER_DATE_FORMAT
from tools.translate import _
import logging


logger = logging.getLogger(__name__)

class stock_return_onfly_wizard(osv.osv_memory):
    _name = "stock.return.onfly.wizard"

    _columns = {
        'name': fields.char('External Customer Order Id', size=128, required=True,
            readonly=True, states={'draft': [('readonly', False)]},
        ),
        'po_number': fields.char('PurchaseOrderNumber', size=128, required=False,
            readonly=True, states={'draft': [('readonly', False)]},),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True,
            readonly=True, states={'draft': [('readonly', False)]},),
        'type_api_id': fields.many2one('sale.integration', 'Service provider', required=False,
            readonly=True, states={'draft': [('readonly', False)]},),
        'shp_date': fields.datetime('Shipping Date',
            readonly=True, states={'draft': [('readonly', False)]},),
        'state': fields.selection([
            ('draft', 'New'),
            ('done', 'Done')], 'State'),
        'product_id': fields.many2one('product.product', 'Product', required=True, select=True,
            domain=[('type', '!=', 'service')],
            readonly=True, states={'draft': [('readonly', False)]},),
        'description': fields.char('Description', size=256, required=True, select=True,
            readonly=True, states={'draft': [('readonly', False)]},),
        'product_qty': fields.integer('Quantity', required=True,
            readonly=True, states={'draft': [('readonly', False)]},),
        'size_id': fields.many2one('ring.size', 'Ring Size Sale',
            readonly=True, states={'draft': [('readonly', False)]},),
        'size_id_delivery': fields.many2one('ring.size', 'Ring Size Delivery',
            readonly=True, states={'draft': [('readonly', False)]},),
        'size_ids': fields.char(size=256, string="Product size ids",),
        'location_id': fields.many2one('stock.location', 'Location',
            domain="[('usage', '=', 'internal'), ('complete_name', 'ilike', '%% / stock / %%')]",
            readonly=True, states={'draft': [('readonly', False)]},),
        'location_name': fields.related('location_id', 'name', string="Location Name",
            type="char", relation='stock.location', store=False,),
        'bin_id': fields.many2one('stock.bin', 'Bin',
            readonly=True, states={'draft': [('readonly', False)]},),
        'bin_ids': fields.char('bin ids', size=256),
        'external_customer_line_id': fields.char('External Customer Line Id', size=128, required=True,
            readonly=True, states={'draft': [('readonly', False)]},),
        'merchant_line_number': fields.char('Merchant Line Number', size=128,
            readonly=True, states={'draft': [('readonly', False)]},),
        'ship_cost': fields.float('Shipping Cost',
            readonly=True, states={'draft': [('readonly', False)]},),
        'price_unit': fields.float('Unit Price', required=True,
            readonly=True, states={'draft': [('readonly', False)]},),
        'rma_number': fields.char('RMA', size=128, required=False,
            readonly=True, states={'draft': [('readonly', False)]},),
        'tracking_ref': fields.char('Tracking Ref', size=256, ),
        'return_batch_id': fields.many2one('stock.picking.return.batch', 'Return Batch', ),
        'return_batch_line_id': fields.many2one('stock.picking.return.line', 'Return Batch Line', ),
        'mass_return_line_id': fields.many2one('stock.picking.mass.return.line', 'Mass Return Line', ),
        'is_set': fields.boolean('Is Set'),
    }

    _defaults = {
        'state': 'draft',
        'product_qty': 1,
    }

    def default_get(self, cr, uid, fields, context=None):

        loc_obj = self.pool.get('stock.location')
        res = super(stock_return_onfly_wizard, self).default_get(cr, uid, fields, context=context)

        return_loc_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('name', '=', 'RETURN'), ('complete_name', 'ilike', '%% usreturn %% stock %%')])
        res.update({
            'location_id': False if not return_loc_ids else return_loc_ids[0],
            'bin_id': False,
        })

        return res

    def _getProductPrice(self, cr, uid, partner_id, product_id, size_id):
        if product_id:
            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product_id, partner_id,
                                                                             'Customer Price', size_id)
            if product_cost:
                try:
                    product_cost = float(product_cost)
                except Exception, e:
                    product_cost = 0.0
                    logger.error('Wrong Customer Price value: partner_id = {partner_id}, product_id = {product_id}, size_id = {size_id}.\nError: {error}'.format(
                        partner_id=partner_id,
                        product_id=product_id,
                        size_id=size_id,
                        error=e
                    ))
            else:
                product_cost = 0.0
            return product_cost


    def partner_id_change(self, cr, uid, ids, partner_id, product_id, size_id, context=None):

        domain = {}
        value = {
            'partner_id': partner_id,
            'type_api_id': False,
            'price_unit': self._getProductPrice(cr, uid, partner_id, product_id, size_id)
        }

        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
            if partner.type_api_ids:
                type_api = partner.type_api_ids[0]
                value['type_api_id'] = type_api.id

        return {'value': value, 'domain': domain}

    def product_id_change(self, cr, uid, ids, partner_id, product_id, location_id, size_id, bin_id, context=None,):
        context = context or {}
        prod_obj = self.pool.get('product.product')
        bin_id_list = []
        product = None

        result = {
            'product_id': product_id,
            'location_id': location_id,
            'location_name': False,
            'description': False,
            'size_id': False,
            'size_ids': False,
            'bin_id': False,
            'bin_ids': False,
            'price_unit': self._getProductPrice(cr, uid, partner_id, product_id, size_id)
        }

        if product_id:

            if context.get('sync_stock', False):
                self.pool.get('sale.order').sync_product_stock(cr, uid, product_id, size_id, context=context)

            result['description'] = self.pool.get('product.product').name_get(cr, uid, [product_id], context=context)[0][1]
            size_ids = prod_obj.read(cr, uid, product_id, ['ring_size_ids'])['ring_size_ids']
            product = self.pool.get('product.product').browse(cr, uid, product_id)

            if product.categ_id and product.categ_id.name.lower() == 'ring':
                size_ids.append(-1)

            if size_ids:
                result['size_ids'] = ",".join([str(x) for x in size_ids if x])
                result['size_id'] = size_id

                # if size_id in size_ids:
                #     result['size_id'] = size_id

        if location_id:
            location = self.pool.get('stock.location').read(cr, uid, location_id, ['name', 'warehouse_id'])
            result['location_name'] = location['name']

            if product_id and product and product.default_code:
                server_obj = self.pool.get('fetchdb.server')
                server_id = server_obj.get_sale_servers(cr, uid, single=True)
                if server_id:
                    if size_id:
                        size_obj = self.pool.get('ring.size').browse(cr, uid, size_id)
                        size = self.pool.get('stock.move').get_size_postfix(cr, uid, size_obj.name or False)
                    else:
                        size = '0000'
                    query = '''SELECT
                        --itemid,
                        bin
                        FROM dbo.transactions
                        WHERE itemid = '%s/%s'
                        AND warehouse = '%s'
                        AND bin is not NULL
                        AND status <> 'Posted'
                        GROUP BY
                        --itemid,
                        bin''' % (product.default_code, size, location['warehouse_id'][1])
                    params = []
                    sql_res = server_obj.make_query(cr, uid, server_id, query, params=params, select=True, commit=False) or []
                    bin_name_list = [x[0] for x in sql_res if x ]
                    bin_id_list = self.pool.get('stock.bin').search(cr, uid, [('name', 'in', bin_name_list)])
                else:
                    raise osv.except_osv(_('Warning!'), 'MSSQL server error!')

            if not location['name'] or location['name'].strip().lower() == 'return':
                bin_ids_arr = self.pool.get('stock.bin').search(cr, uid, [('name', '=', 'RETURN')])
                if bin_ids_arr:
                    result['bin_ids'] = ",".join([str(x) for x in bin_ids_arr if x])
                    if bin_id in bin_ids_arr:
                        result['bin_id'] = bin_id
                    else:
                        result['bin_id'] = bin_ids_arr[0]

            else:
                bin_ids_arr = prod_obj.get_product_bins(cr, uid, [product_id], [location_id], [size_id], force_bins=False, excluding_bins=['RETURN'])
                for one_bin_id in bin_ids_arr:
                    bin_id_list.append(one_bin_id)
                bin_id_list.append(bin_id)
                if bin_id_list:
                    result['bin_ids'] = ",".join([str(x) for x in bin_id_list if x])

                    if bin_id in bin_id_list:
                        result['bin_id'] = bin_id

        return {'value': result}

    def _prepare_sale_order(self, cr, uid, order, context=None):

        shop_obj = self.pool.get('sale.shop')
        partner_obj = self.pool.get('res.partner')
        helper = self.pool.get('stock.picking.helper')

        shop = False
        shop_ids = shop_obj.search(cr, uid, [])
        if shop_ids:
            shop = shop_obj.browse(cr, uid, shop_ids[0])

        if not shop:
            raise osv.except_osv(_('Warning'), _('Shop not found'))

        vals = {
            'state': 'returned_onfly',
            'partner_id': order.partner_id and order.partner_id.id,
            'shop_id': shop.id,
            'pricelist_id': shop.pricelist_id and shop.pricelist_id.id or False,
            'external_customer_order_id': order.name,
            'po_number': order.po_number,
            'type_api_id': order.type_api_id and order.type_api_id.id or False,
            'tracking_ref': order.tracking_ref or False,
            'is_set': helper.check_product_is_set(cr, uid, order.product_id.id) or False
        }

        addr = partner_obj.address_get(cr, uid, [order.partner_id.id], ['delivery', 'invoice', 'contact'])
        if addr:
            vals['partner_invoice_id'] = addr['invoice']
            vals['partner_order_id'] = addr['contact']
            vals['partner_shipping_id'] = addr['delivery']

        return vals

    def _prepare_sale_order_line(self, cr, uid, so_id, line, context=None):

        if not line.product_id or not line.product_qty:
            raise osv.except_osv(_('Warning'), _('Wrong sale order line format'))

        helper = self.pool.get('stock.picking.helper')

        vals = {
            'order_id': so_id,
            'state': 'done',
            'name': line.description,
            'product_id': line.product_id and line.product_id.id or False,
            'product_uom_qty': line.product_qty,
            'product_uom': 1,
            'price_unit': line.price_unit,
            'shipCost': line.ship_cost,
            'external_customer_line_id': line.external_customer_line_id,
            'merchantLineNumber': line.merchant_line_number,
            'size_id': line.size_id and line.size_id.id or False,
            'is_set': helper.check_product_is_set(cr, uid, line.product_id.id) or False
        }

        return vals

    def _prepare_picking_order(self, cr, uid, order, context=None):

        vals = self.pool.get('sale.order')._prepare_order_picking(cr, uid, order, context=context)
        vals['state'] = 'done'
        vals['tracking_ref'] = order['tracking_ref']
        vals['is_set'] = order.is_set or False

        return vals

    def _prepare_move(self, cr, uid, sp_id, sale_order, sale_line, widget_line, date_expected, context=None):

        vals = self.pool.get('sale.order')._prepare_order_line_move(cr, uid, sale_order, sale_line, sp_id, date_expected, context=None)
        vals['state'] = 'draft'
        vals['rma_number'] = widget_line.rma_number or False
        vals['is_set'] = sale_line.is_set or False

        if sale_line.is_set:
            vals['set_product_id'] = sale_line.set_product_id.id
            if sale_line.order_id and sale_line.order_id.id:
                vals['picking_id'] = ''
                vals['set_parent_order_id'] = sp_id
            elif sale_line.set_parent_order_id and sale_line.set_parent_order_id.id:
                vals['picking_id'] = sp_id
                vals['set_parent_order_id'] = ''

        return vals

    def _prepare_partial_data(self, cr, uid, stock_move, wizard_line, context=None):

        sm_obj = self.pool.get('stock.move')

        if not stock_move.product_id or not stock_move.product_qty:
            raise osv.except_osv(_('Warning'), _('Wrong stock move line format'))

        if not wizard_line.location_id:
            raise osv.except_osv(_('Warning'), _('There is no return location'))

        vals = {
            'product_id': stock_move.product_id.id,
            'product_qty': stock_move.product_qty,
            'product_uom': stock_move.product_uom and stock_move.product_uom.id or False,
            'prodlot_id': stock_move.prodlot_id and stock_move.prodlot_id.id or False,
            'location_id': wizard_line.location_id.id,
            'bin_id': wizard_line.bin_id and wizard_line.bin_id.id or False,

        }

        size_postfix = sm_obj.get_size_postfix(cr, uid, stock_move.size_id)
        vals['note'] = '%s %s/%s\ninto %s / %s\n' % (
            stock_move.product_id.default_code,
            size_postfix,
            stock_move.product_qty,
            wizard_line.location_id.complete_name,
            wizard_line.bin_id and wizard_line.bin_id.name or 'RETURN'
        )

        return vals

    def _print_label(self, cr, uid, partial_line_ids=None, context=None):
        act = True
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.report.xml')

        if partial_line_ids:
            datas = {
                'ids': partial_line_ids,
            }

            act_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'stock_partial_picking_line')
            act_id = data_obj.read(cr, uid, act_res_id, ['res_id'])
            act = act_obj.read(cr, uid, act_id['res_id'], [], context=context)
            act.update({
                'datas': datas
            })

        return act

    def create_and_return(self, cr, uid, ids, context=None, from_mass_return=False):
        if context is None:
            context = {}

        if len(ids) != 1:
            raise osv.except_osv(_('Warning!'), 'May only be done for one return at a time!')

        so_obj = self.pool.get('sale.order')
        so_line_obj = self.pool.get('sale.order.line')
        sp_obj = self.pool.get('stock.picking')
        sm_obj = self.pool.get('stock.move')

        partial_obj = self.pool.get('stock.partial.picking')
        partial_line_obj = self.pool.get('stock.partial.picking.line')

        helper = self.pool.get('stock.picking.helper')

        partial_line_ids = []

        vals_to_return = self.browse(cr, uid, ids[0], context=context)
        partial_datas = {}
        partner = vals_to_return.partner_id
        notes = ''

        if not vals_to_return.product_qty or vals_to_return.product_qty <= 0:
            raise osv.except_osv(_('Warning'), _('Must be a positive number'))

        if not partner:
            raise osv.except_osv(_('Warning'), _('Partner is not specified'))

        if not vals_to_return.po_number:
            vals_to_return.po_number = vals_to_return.name

        if not vals_to_return.price_unit or vals_to_return.price_unit <= 0:
            raise osv.except_osv(_('Warning!'), 'Price must be a positive value!')

        so_vals = self._prepare_sale_order(cr, uid, vals_to_return, context=None)
        so_id = so_obj.create(cr, uid, so_vals)
        if not so_id:
            raise osv.except_osv(_('Warning'), _('Failed to create a sale order'))

        so_line_vals = self._prepare_sale_order_line(cr, uid, so_id, vals_to_return, context=None)
        so_line_id = so_line_obj.create(cr, uid, so_line_vals)
        if not so_line_id:
            raise osv.except_osv(_('Warning'), _('Failed to create a sale_order_line'))

        component_lines = so_line_obj.split_set_line_to_components(cr, uid, [so_line_id])
        logger.info('ONFLY CREATE LINES: %s' % component_lines)

        so = so_obj.browse(cr, uid, so_id)
        so_obj.log(cr, uid, so.id, 'Sale order %s is created' % (so.name))

        sp_vals = self._prepare_picking_order(cr, uid, so, context=None)
        sp_id = sp_obj.create(cr, uid, sp_vals)
        if not sp_id:
            raise osv.except_osv(_('Warning'), _('Failed to create a picking order'))

        date_planned = datetime.strptime(so.date_order, DEFAULT_SERVER_DATE_FORMAT)
        for so_line in so.order_line + so.set_parent_order_line:
            sm_vals = self._prepare_move(cr, uid, sp_id, so, so_line, vals_to_return, date_planned)
            sm_id = sm_obj.create(cr, uid, sm_vals)
            if not sm_id:
                raise osv.except_osv(_('Warning'), _('Failed to create a stock move'))

            sm = sm_obj.browse(cr, uid, sm_id)
            partial_data = self._prepare_partial_data(cr, uid, sm, vals_to_return)

            line_data_value = partial_obj._partial_move_for(cr, uid, sm, context=context)
            partial_line_id = partial_line_obj.create(cr, uid, line_data_value)
            if partial_line_id:
                partial_line_ids.append(partial_line_id)
                partial_data['line_id'] = partial_line_id

            notes += partial_data.get('note', '')
            partial_datas['move%s' % (sm_id)] = partial_data

            # update sale order line to close sale order
            if so_line.is_set:
                so_line_obj.write(cr, uid, so_line.id, {
                    'order_id': so_id if so_line.set_parent_order_id else '',
                    'set_parent_order_id': so_id if so_line.order_id else ''
                })

        sp_obj.write(cr, uid, sp_id, {'report_history': notes})

        sp = sp_obj.browse(cr, uid, sp_id)
        sp_obj.log(cr, uid, sp.id, 'Delivery order %s is created' % (sp.name))

        # Uncomment remote process before commit
        if from_mass_return:
            logger.info('Set context for from_mass_return')
            context.update({'from_mass_return': True})
        return_res = sp_obj.do_only_return(cr, uid, [sp_id], partial_datas, context=context)
        print "Return res: %s" % (return_res)
        if not return_res:
            raise osv.except_osv(_('Warning'), _('Failed to return item'))

        if vals_to_return.return_batch_id:

            line_vals = {
                'batch_id': vals_to_return.return_batch_id.id or False,
                'product_id': vals_to_return.product_id.id or False,
                'size_id': vals_to_return.size_id.id or False,
                'qty': vals_to_return.product_qty,
                'cost': vals_to_return.price_unit,
                'partner_id': partner.id or False,
                'picking_id': sp.id or False,
                'state': 'done',
                }
            if from_mass_return:
                if vals_to_return.mass_return_line_id:
                    self.pool.get('stock.picking.mass.return.line').write(cr, uid, vals_to_return.mass_return_line_id.id, line_vals)
                else:
                    self.pool.get('stock.picking.mass.return.line').create(cr, uid, line_vals)
            else:
                if vals_to_return.return_batch_line_id.id or False:
                    self.pool.get('stock.picking.return.line').write(cr, uid, vals_to_return.return_batch_line_id.id, line_vals)
                else:
                    self.pool.get('stock.picking.return.line').create(cr, uid, line_vals)

        sp_obj.log(cr, uid, sp.id, 'Delivery order %s is returned' % (sp.name))
        self.write(cr, uid, vals_to_return.id, {'state': 'done'})

        if from_mass_return:
            return return_res.get(sp_id).get('returned_picking')
        else:
            # return self._print_label(cr, uid, partial_line_ids, context=context)
            return sp_obj.print_fly_qr_label(cr, uid, [sp_id])
        # return {'type': 'ir.actions.act_window_close'}
        # return partial_line_ids

    # def create_and_continue(self, cr, uid, ids, context=None):

    #     res = self.create_and_return(cr, uid, ids, context=context)
    #     if res:
    #         data_obj = self.pool.get('ir.model.data')
    #         act_obj = self.pool.get('ir.actions.act_window')
    #         act_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_return_onfly_wizard')
    #         act_id = data_obj.read(cr, uid, act_res_id, ['res_id'])
    #         res = act_obj.read(cr, uid, act_id['res_id'], [], context=None)

    #     return res

stock_return_onfly_wizard()
