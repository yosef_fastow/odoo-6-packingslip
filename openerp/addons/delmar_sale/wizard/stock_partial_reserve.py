# -*- coding: utf-8 -*-

import time
from osv import fields, osv
from tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
import decimal_precision as dp
import netsvc
import re


class stock_partial_reserve_line(osv.osv):

    _name = "stock.partial.reserve.line"
    _rec_name = "product_id"
    _columns = {
        'product_id': fields.many2one('product.product', string="Product", required=True, ondelete='CASCADE'),
        'quantity': fields.float("Quantity", digits_compute=dp.get_precision('Product UoM'), required=True),
        'location_id': fields.many2one('stock.location', 'Location', required=True, ondelete='CASCADE', domain=[('usage', '<>', 'view')]),
        'bin': fields.char('Bin', size=256),
        'size_id': fields.many2one('ring.size', 'Size', ondelete='CASCADE'),
        'wizard_id': fields.many2one('stock.partial.reserve', string="Wizard", ondelete='CASCADE'),
    }

    def action_partial_move_confirm(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        if isinstance(ids, (int, long)):
            ids = [ids]
        elif len(ids) != 1:
            raise osv.except_osv('Warning!', 'Partial Picking Reserve may only be done for one move at a time!')

        is_no_resize = context.get('is_no_resize', False)
        update_sale_order_line = context.get('update_sale_order_line', False)
        move_obj = self.pool.get('stock.move')
        act_obj = self.pool.get('ir.actions.act_window')
        wf_service = netsvc.LocalService("workflow")
        result_action = {'type': 'ir.actions.act_window_close'}

        done = []
        line_id, = ids
        line = self.browse(cr, uid, line_id)
        move = line.wizard_id.move_id

        if move:

            if move.is_set_component and update_sale_order_line:
                raise osv.except_osv('Warning!','You can\'t relocate sets!')

            if not move.is_set_component and update_sale_order_line and type(move.set_product_id) is not osv.orm.browse_null:
                raise osv.except_osv('Warning!','Update of set\'s components on sale order is forbidden. if you need it - please update all set\'s components to the same size without option "update sale order..." and than ask IT to replace size in sale order for all set\'s components ')

            if not ((move.state == 'confirmed') or (move.picking_id.state == 'cancel' and move.state == 'cancel') or False):
                raise osv.except_osv('Warning!', 'The line has been changed by someone or automatically and it is not Waiting Availability!')

            if move.is_set_component:
                is_no_resize = False

            move_obj.write(cr, uid, move.id, {
                'product_id': line.product_id.id,
                'size_id': line.size_id and line.size_id.id or False,
                'is_no_resize': is_no_resize,
                'is_update_order': update_sale_order_line,
            })

            if move.is_set:
                set_line = self.pool.get('stock.picking')._get_set_parent_line(cr, uid, move.id)
                if set_line:
                    move_obj.write(cr, uid, set_line.id, {
                        'size_id': line.size_id and line.size_id.id or False,
                        'is_no_resize': is_no_resize,
                        'is_update_order': update_sale_order_line,
                    })

            # If order in cancell state then just update size
            if not (move.picking_id.state == 'cancel' and move.state == 'cancel'):

                move = move_obj.browse(cr, uid, move.id)
                pick_id = move.picking_id.id
                location_dest_id = line.location_id and line.location_id.id or False
                invredid = move_obj.get_invredid_for_stock_move(cr, uid, move, line.bin, location_dest_id)

                reserve_from = {
                    'location_dest_id': location_dest_id,
                    'bin': line.bin,
                    'invredid': invredid,
                    'product_id': line.product_id.id,
                    'size_id': line.size_id and line.size_id.id or False,
                    'qty': line.quantity
                }

                done = move_obj.set_move_location(cr, uid, [move], [reserve_from], pick_id)
                if done:
                    move_obj.write(cr, uid, done, {'state': 'assigned'})
                    move_obj.do_remote_product_reserve(cr, uid, [move.id], sign=-1, context=context)

                    if move.picking_id.state == 'back':
                        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_reassign', cr)
                    elif move.picking_id.state == 'waiting_split':
                        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'button_recheck', cr)
                    elif move.picking_id.state == 'confirmed':
                        wf_service.trg_validate(uid, 'stock.picking', pick_id, 'action_assign', cr)
                    else:
                        wf_service.trg_write(uid, 'stock.picking', pick_id, cr)

                    if update_sale_order_line and move.sale_line_id and line.size_id:
                        vals = {}
                        vals.update({'size_id': line.size_id.id})
                        if move.sale_line_id.lineRef4:
                            vals.update({'lineRef4': line.size_id.name})
                        self.pool.get('sale.order.line').write(cr, uid, move.sale_line_id.id, vals)


                if not done:
                    raise osv.except_osv('Warning!', 'Partial Picking Reserve can\'t be done!')

        if context.get('action_id', False):
            action = act_obj.read(cr, uid, context['action_id'])
            if action:
                result_action = action

        return result_action

stock_partial_reserve_line()


class stock_partial_reserve(osv.osv):
    _name = "stock.partial.reserve"
    _description = "Partial Picking Reserve Wizard"
    _rec_name = 'move_id'

    def _get_size_tolerance_range(self, cr, uid, context=None):
        return [(float(x)/10, str(float(x)/10)) for x in range(0, 165, 5)]

    _columns = {
        'date': fields.datetime('Date', required=True),
        'move_id': fields.many2one('stock.move', "Move", ondelete='CASCADE'),
        'product_id': fields.related('move_id', 'sale_line_id', 'product_id', string='Item', type='many2one', relation='product.product', readonly=True),
        'size_id': fields.related('move_id', 'sale_line_id', 'size_id', string='Size', type='many2one', relation='ring.size', readonly=True),
        'reason': fields.text('Reason', required=False),
        'line_ids': fields.one2many('stock.partial.reserve.line', 'wizard_id', 'Variants'),
        'size_tolerance': fields.selection(_get_size_tolerance_range, 'Size Tolerance',),
        'style_tolerance': fields.boolean('Style Tolerance',),
        'research_btn': fields.boolean('Search',),
        'new_product_id': fields.many2one('product.product', "New Product", domain="[('type', '=', 'product')]"),
        'update_sale_order_line': fields.boolean('Update sale order and show on packing slip'),
        'is_no_resize': fields.boolean('No resize'),
        'is_no_resize_check': fields.related('product_id', 'is_no_resize_check', string="", type='boolean', relation='product.product'),
        'is_set_component': fields.boolean('is_set_component'),
    }

    _defaults = {
        'size_tolerance': 0.0,
        'style_tolerance': False,
        'research_btn': False,
    }

    def on_change_new_product_id(self, cr, uid, ids, product_id, context=None):
        if context is None:
            context = {}
        is_no_resize_check = False

        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id)
            is_no_resize_check = product.is_no_resize_check

        return {'value': {
            'is_no_resize_check': is_no_resize_check,
            'is_no_resize': is_no_resize_check,
        }}

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_partial_reserve, self).default_get(cr, uid, fields, context=context)

        move_ids = context.get('active_ids', [])
        if not move_ids or (not context.get('active_model') == 'stock.move') \
            or len(move_ids) != 1:
            # Partial Picking Reserve may only be done for one move at a time
            return res
        move_id, = move_ids
        move = self.pool.get('stock.move').browse(cr, uid, move_id, context=context)
        if 'move_id' in fields:
            res.update(move_id=move_id)
        if 'line_ids' in fields:
            lines = self._partial_move_for(cr, uid, move, size_tolerance=0.0, style_tolerance=False, context=context)
            res.update(line_ids=lines)
        if 'is_no_resize' in fields:
            res.update(is_no_resize=move and move.product_id and move.product_id.is_no_resize_check or False)
        if 'date' in fields:
            res.update(date=time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))

        res.update(is_set_component=move and move.is_set_component or False)

        return res

    def _partial_move_for(self, cr, uid, move, size_tolerance=0.0, style_tolerance=False, new_product_id=False, context=None):

        if context is None:
            context = {}

        loc_ids = []
        loc_ids_str = ""
        partial_moves = []
        product_ids = []
        having_size_str = ""

        if not move:
            return partial_moves

        sale_obj = self.pool.get('sale.order')

        system_locations = ('Inventory loss', 'Customers', 'Output', 'EXCP')
        system_locations_str = str(system_locations)
        size_id = move.sale_line_id.size_id and move.sale_line_id.size_id.id or False

        sale_obj.sync_product_stock(cr, uid, move.product_id.id, size_id, context=context)
        if move.product_id.set_component_ids:
            # sync ERP inventory for components of SET
            for set_cmp in move.product_id.set_component_ids:
                sale_obj.sync_product_stock(cr, uid, set_cmp.cmp_product_id.id, size_id, context=context)

        product_ids = [move.product_id.id]
        if new_product_id:
            product_ids.append(new_product_id)
        if style_tolerance:
            regex = re.compile("^([^\-]*)")
            styles = regex.findall(move.product_id.default_code or '')
            if styles:
                product_ids = self.pool.get('product.product').search(cr, uid, [('default_code', 'ilike', styles[0])])
        product_ids_str = ','.join([str(x) for x in product_ids if x])

        if not size_id:
            having_size_str = "and sm.size_id =None"
        elif not size_tolerance:
            having_size_str = "and sm.size_id =%s" % (size_id)
        else:
            try:
                float_size = float(move.sale_line_id.size_id.name)
                size_from = str(float_size - size_tolerance)
                size_to = (float_size + size_tolerance)
                having_size_str = "AND CASE WHEN rs.name ~ E'^\\\\d+' THEN rs.name::real else 0.0 END BETWEEN %s AND %s" % (size_from, size_to)
            except:
                pass

        having_str = """ or (sm.product_id in (%s) %s and sum(sm.product_qty * (CASE WHEN sl.name in %s THEN -1 ELSE 1 END)) >= %s) """ % (product_ids_str, having_size_str, system_locations_str, move.product_qty)

        sql_get_locs = """  SELECT
                                a.location_dest_id as location_dest_id,
                                sl.name as name
                            FROM (
                                SELECT CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id
                                FROM stock_move sm
                                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                                    LEFT JOIN ring_size rs on rs.id = sm.size_id
                                    LEFT JOIN stock_bin sb on sm.bin_id = sb.id
                                WHERE sm.state IN ('done', 'reserved', 'assigned')
                                    AND sm.product_id IN (%s)
                                    AND coalesce(sb.name, sm.bin, '') != ''
                                GROUP BY (CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END)
                                    , sm.product_id
                                    , sm.size_id
                                    , rs.name
                                HAVING 1!=1 %s
                            ) a
                                LEFT JOIN stock_location sl ON sl.id = a.location_dest_id
                            WHERE sl.name != 'RETURN'
        """ % (system_locations_str, product_ids_str, system_locations_str, having_str)
        sql_get_locs = sql_get_locs.replace("=None", " IS NULL ")
        cr.execute(sql_get_locs)
        result_get_locs = cr.dictfetchall()

        for locs_res in result_get_locs:
            loc_ids.append(str(locs_res['location_dest_id']))

        if not loc_ids:
            return partial_moves

        loc_ids_str = ', '.join(loc_ids)

        sql = """SELECT
                    a.product_id as product_id,
                    a.size_id as size_id,
                    a.qty as qty,
                    a.location_id as location_id,
                    a.bin as bin
                FROM (
                    SELECT sm.product_id as product_id,
                        sm.size_id as size_id,
                        array_to_string(array_accum(DISTINCT sm.invredid), ';') as invredid,
                        CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END as location_id,
                        coalesce(sb.name, sm.bin, '') as bin,
                        sum(sm.product_qty * (CASE WHEN sl.name IN %s THEN -1 ELSE 1 END)) as qty
                    FROM stock_move sm
                        LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                        LEFT JOIN ring_size rs on rs.id = sm.size_id
                        LEFT JOIN stock_bin sb on sb.id = sm.bin_id
                    WHERE sm.state IN ('done', 'reserved', 'assigned')
                        AND sm.product_id in (%s)
                        AND ( sm.location_dest_id in (%s) OR sl.name IN %s )
                        AND coalesce(sb.name, sm.bin, '') != ''
                    GROUP BY (CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END)
                        , coalesce(sb.name, sm.bin, '')
                        , sm.product_id
                        , sm.size_id
                        , rs.name
                    HAVING 1!=1 %s
                    ORDER BY CASE WHEN sl.name IN %s THEN sm.location_id ELSE sm.location_dest_id END
                ) a
                WHERE a.location_id in (%s)
        """ % (system_locations_str, system_locations_str, product_ids_str, loc_ids_str, system_locations_str, system_locations_str, having_str, system_locations_str, loc_ids_str)

        sql = sql.replace("=None", " IS NULL ")
        cr.execute(sql)
        result = cr.dictfetchall()
        for res in result:

            partial_move = {
                'product_id': res['product_id'],
                'bin': res.get('bin', False),
                'quantity': res.get('qty', 0),
                'location_id': res['location_id'],
            }

            if res.get('size_id', False):
                partial_move['size_id'] = res['size_id']

            partial_moves.append(partial_move)

        return partial_moves

    def research(self, cr, uid, ids, move_id, size_tolerance, style_tolerance, new_product_id, context=None):

        line_ids = []
        line_obj = self.pool.get('stock.partial.reserve.line')

        values = {
            'size_tolerance': size_tolerance,
            'style_tolerance': style_tolerance,
            'line_ids': [],
            'move_id': move_id,
            'research_btn': False,
        }

        if move_id:
            move = self.pool.get('stock.move').browse(cr, uid, move_id, context=context)
            lines = self._partial_move_for(cr, uid, move, size_tolerance=size_tolerance, style_tolerance=style_tolerance, new_product_id=new_product_id, context=context)

            for line in lines:
                line['wizard_id'] = ids[0]
                line_ids.append(line_obj.create(cr, uid, line))
            values.update({'line_ids': line_ids})
            is_set_component = move.is_set_component
            values.update({'is_set_component': is_set_component})
        return {'value': values}

stock_partial_reserve()
