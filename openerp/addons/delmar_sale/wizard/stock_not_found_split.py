from osv import fields, osv
import netsvc
from tools.translate import _
import tools
import logging
logger = logging.getLogger('stock.not.found.split')

class stock_not_found_split(osv.osv_memory):
    _name = "stock.not.found.split"

    _columns = {
        'picking_id': fields.many2one(
            'stock.picking',
            'Delivery Order',
            required=True,
            readonly=True),
        'line_ids': fields.one2many(
            'stock.not.found.split.line',
            'wizard_id',
            'Lines'),
    }


    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_not_found_split, self).default_get(
            cr, uid, fields, context=context)
        lines = self._get_lines(cr, uid, context['picking_id'])
        if not lines:
            raise osv.except_osv(
                _('Warning!'),
                'Nothing to return. Please use "Cancel" button instead')
        res.update({
            'picking_id': context['picking_id'],
            'line_ids': lines,
        })
        return res

    def _get_lines(self, cr, uid, picking_id, location_id=None):
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id)
        result = []
        for move in picking.move_lines:
            if move.state not in ('assigned', 'reserved', 'done'):
                continue
            if not location_id or location_id == move.location_id.id:
                bin_to = move.bin_id.name if move.bin_id else move.bin
            else:
                bin_to = False
            result.append({
                'move_id': move.id,
                'product_id': move.product_id.id,
                'size_id': move.size_id.id or False,
                'product_qty': move.product_qty,
                'price_unit': move.price_unit,
                'location_id': location_id or move.location_id.id,
                'bin_to': bin_to,
            })
        return result

    def do_action(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'Partial picking processing may only be done one at a time'
        stf = self.pool.get('stock.not.found')
        partial = self.browse(cr, uid, ids[0], context=context)
        partial_data = {}

        picking_type = partial.picking_id.type

        if not partial.line_ids:
            raise osv.except_osv(_('Warning!'), _('Nothing to process in stock_not_found_split:do_action!'))

        for wizard_line in partial.line_ids:
            move_id = wizard_line.move_id.id


            if wizard_line.product_qty > wizard_line.move_id.product_qty:
                raise osv.except_osv(_('Warning!'), _('Please provide Proper Quantity !'))

            partial_data['move%s' % (move_id)] = {
                'line_id': wizard_line.id,
                'product_id': wizard_line.product_id.id,
                'product_qty': wizard_line.product_qty,
            }

            if (picking_type == 'in') and (wizard_line.product_id.cost_method == 'average'):
                partial_data['move%s' % (wizard_line.move_id.id)].update(product_price=wizard_line.cost,
                                                                                 product_currency=wizard_line.currency.id)
                if len(partial.line_ids) > 1:
                    break

        note = 'Order %s will be processed' % (partial.picking_id.name,)
        stf.create(cr, uid, {'picking_id': partial.picking_id.id, 'status': 'New', 'name': partial.picking_id.name,'note': note, 'move': move_id})

        return {'type': 'ir.actions.act_window_close'}

    def do_action_move(self, cr, uid, ids, context=None):

        assert len(ids) == 1, 'Partial picking processing may only be done one at a time'

        sm_object = self.pool.get('stock.move')
        inventory_obj = self.pool.get('stock.inventory')
        inventory_line_obj = self.pool.get('stock.inventory.line')
        partial = self.browse(cr, uid, ids[0], context=context)
        if not partial.line_ids:
            raise osv.except_osv(_('Warning!'), _('Nothing to process in stock_not_found_split:do_action_move!'))
        
        moves = []
        for wizard_line in partial.line_ids:
            moves.append(wizard_line.move_id.id)

        order = partial.picking_id
        lines = sm_object.browse(cr, uid, moves, context=context)
        inventory_ids = []

        for move in lines:
            bin = move.bin or move.bin_id and move.bin_id.name or False
            size_str = move.size_id and move.size_id.name or ''
            size_id = move.size_id and move.size_id.id
            product_id = move.product_id.id
            loc_id = move.location_id.id

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'stock.picking', order.id, 'action_reship', cr)
            # wf_service.trg_validate(uid, 'stock.picking', order.id, 'button_back', cr)

            sm_object.action_back_for_one_product(cr, uid, moves)

            inventory_id = inventory_obj.create(cr, uid, {
                'name': _('INV: %s%s') % (tools.ustr(move.product_id.name), size_str),
                'reason': 'Stock not found. Adjust 0',
            }, context=context)

            inventory_line_obj.create(cr, uid, {
                'inventory_id': inventory_id,
                'product_qty': 0,
                'location_id': loc_id,
                'product_id': product_id,
                'product_uom': move.product_id.uom_id.id,
                'prod_lot_id': move.prodlot_id.id,
                'size_id': size_id,
                'bin': bin,
                'bin_id': move.bin_id and move.bin_id.id or False,
            }, context=context)
            inventory_ids.append(inventory_id)

            inventory_obj.action_confirm(cr, uid, inventory_ids, context=context)
            inventory_obj.action_done(cr, uid, inventory_ids, context=context)

            inventory_obj.force_adjustment_pending(cr, uid, product_id, size_id, loc_id, bin, 0, invoiceno="ZEROSTOCK %s" % order.name)

        wf_service.trg_validate(uid, 'stock.picking', order.id, 'button_reassign', cr)

        self.update_transactioncode(cr, uid, order.name)

        return {'type': 'ir.actions.act_window_close'}

    def update_transactioncode(self, cr, uid, itemname):
        try:
            logger.info('update_transactioncode: %s'.format(itemname))

            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_sale_servers(cr, uid, single=True)
            transaction_zerostock_sql = """SELECT transactdate, itemid, itemdescription, bin, warehouse, stockid FROM transactions WHERE invoiceno = 'ZEROSTOCK %s'""" % (
                itemname,)
            transaction_zerostock = server_data.make_query(cr, uid, server_id, transaction_zerostock_sql, select=True,
                                                           commit=False)
            if transaction_zerostock:
                transaction_sql = """
                    SELECT id
                    FROM transactions
                    WHERE DATEDIFF(minute, transactdate, '%s') BETWEEN -10 and 10
                        AND itemid = '%s'
                        AND bin = '%s'
                        AND warehouse = '%s'
                        AND stockid = '%s'
                        AND invoiceno = 'ADJDEF'
                    ORDER BY id DESC
                """ % (transaction_zerostock[0][0].strftime("%Y-%m-%d %H:%M:%S"), transaction_zerostock[0][1],
                       transaction_zerostock[0][3], transaction_zerostock[0][4], transaction_zerostock[0][5],)
                transaction_id = server_data.make_query(cr, uid, server_id, transaction_sql, select=True, commit=False)

            if transaction_id:
                transaction_update_sql = """
                                        UPDATE transactions
                                            SET
                                                transactioncode='CDF'
                                        WHERE id=%s""" % (transaction_id[0][0],)
                server_data.make_query(cr, uid, server_id, transaction_update_sql, select=False, commit=True)

        except:
            logger.warn('update_transactioncode: %s Failed'.format(itemname))

stock_not_found_split()


class stock_not_found_split_line(osv.osv_memory):
    _name = "stock.not.found.split.line"
    _columns = {
        'move_id': fields.many2one(
            'stock.move',
            'Stock Move',
            required=True,
            readonly=True),
        'product_id': fields.related(
            'move_id',
            'product_id',
            type='many2one',
            string="Product",
            readonly=True,
            relation='product.product'),
        'size_id': fields.related(
            'move_id',
            'size_id',
            type='many2one',
            string="Ring Size",
            readonly=True,
            relation='ring.size'),
        'product_qty': fields.related(
            'move_id',
            'product_qty',
            string="Quantity",
            readonly=True),
        'price_unit': fields.related(
            'move_id',
            'price_unit',
            string="Unit Price",
            readonly=True),
        'location_id': fields.many2one(
            'stock.location',
            'Return Location',
            required=True),
        'bin_to': fields.char('Bin', size=50),
        'wizard_id': fields.many2one(
            'stock.not.found.split',
            'Cancel Wizard'),
    }

stock_not_found_split_line()
