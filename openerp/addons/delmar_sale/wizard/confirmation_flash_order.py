# -*- coding: utf-8 -*-

from osv import fields, osv
import netsvc


class confirmation_flash_order(osv.osv_memory):
    _name = 'confirmation.flash.order'

    _rec_name = "sale_id"

    _columns = {
        'sale_id': fields.many2one('sale.order', 'Order', required=True, ),
        'state': fields.selection([
            ('draft', 'Selection of type'),
            ('processing', 'Selection of lines'),
        ], "State"),
        'target': fields.selection([
            ('to_source',       'Return items to source location'),
        ], "Type", required=True, ),
        'line_ids': fields.one2many('confirmation.flash.order.line', 'wizard_id', 'Lines', ondelete='cascade',),
        'removed_line_ids': fields.one2many('confirmation.flash.order.line', 'origin_wizard_id', 'Removed Lines', ondelete='cascade',),
        'location_id': fields.many2one('stock.location', 'Location', domain="[('usage', '=', 'internal'), ('complete_name', 'ilike', ' / Stock / ')]"),
        'action_back': fields.boolean('Back'),
        'action_forward': fields.boolean('Forward'),
        'action_refresh': fields.boolean('Refresh'),
        'action_approve_all': fields.boolean('Confirm all'),
        'action_reject_all': fields.boolean('Leave all'),
    }

    _defaults = {
        'state': 'draft',
        'target': 'to_source',
    }

    def action_back(self, cr, uid, ids, context=None):
        line_ids = []
        vals = {'state': 'draft', 'line_ids': [(6, 0, line_ids)], 'removed_line_ids': [(6, 0, line_ids)]}
        self.write(cr, uid, ids, vals)

        vals.update({
            'line_ids': line_ids,
            'removed_line_ids': line_ids,
        })

        return {'value': vals}

    def action_forward(self, cr, uid, ids, target='to_source', location_id=None, sale_id=None, context=None):

        line_ids = []
        value = {
            'state': 'processing',
            'line_ids': line_ids
        }

        self.write(cr, uid, ids, value)

        warning = {}

        move_obj = self.pool.get('stock.move')
        line_obj = self.pool.get('confirmation.flash.order.line')
        if ids and sale_id:
            line_ids = []
            revert_id = ids[0]
            if target == 'to_direct' and not location_id:
                warning = {
                    'title': 'Configuration Error !',
                    'message': 'Please, specify target location'
                }

                value.update({'state': 'draft'})

            else:
                # Select reserved items
                move_ids = move_obj.search(
                    cr, uid, [
                        ('sale_line_id.order_id', '=', sale_id),
                        ('state', 'in', ('done', 'assigned')),
                        ('product_qty', '>', 0),
                        ('picking_id.state', 'not in', ('done', 'returned', 'cancel', 'final_cancel')),
                        ('picking_id.state', '!=', 'shipped'), ('picking_id.manual_done_approved', '!=', 'True')
                    ]) or []
                if not move_ids:
                    move_ids = move_obj.search(
                        cr, uid, [
                            ('sale_line_id.order_id', '=', sale_id),
                            ('picking_id.state', '=', 'shipped'),
                            ('picking_id.manual_done_approved', '!=', 'True'),
                            ('product_qty', '>', 0),
                        ]) or []

                if move_ids:
                    if target == 'to_source':
                        # Make as it is
                        for move in move_obj.browse(cr, uid, move_ids):
                            line_ids.append(line_obj.create(cr, uid, {
                                'wizard_id': revert_id,
                                'move_id': move.id,
                                'location_id': move.location_id.id or False,
                                'bin': move.bin_id and move.bin_id.name or None,
                                }))

                    else:
                        # Check bins for some specific location
                        to_check = {}
                        location = self.pool.get('stock.location').read(cr, uid, location_id, ['name', 'warehouse_id'])
                        cr.execute("""
                            SELECT
                                concat(pp.default_code, '/', CASE WHEN rs.name is null THEN '0000' ELSE substring('00' || cast(rs.name as float)*100, length(rs.name)-1, 4) END) as item,
                                a.set_product_id,
                                a.product_id,
                                a.size_id,
                                a.qty,
                                a.move_ids
                            FROM (
                                SELECT
                                    sum(sm.product_qty) as qty,
                                    sm.set_product_id,
                                    sm.product_id,
                                    sm.size_id,
                                    array_to_string(array_accum(sm.id),', ') as move_ids
                                FROM stock_move sm
                                    left join stock_picking sp on sm.picking_id = sp.id
                                WHERE sm.id in %s
                                GROUP BY sm.product_id, sm.size_id
                            ) a
                                left join product_product pp on pp.id = a.product_id
                                left join ring_size rs on rs.id = a.size_id
                        """, (tuple(move_ids), ))
                        pg_res = cr.fetchall() or []
                        for r_itemid, r_product_id, r_size_id, r_qty, r_move_ids in pg_res:
                            to_check[r_itemid] = {
                                'qty': r_qty,
                                'move_ids': r_move_ids,
                                'product_id': r_product_id,
                                'size_id': r_size_id
                            }

                        exists_bins = self.pool.get('stock.bin').get_mssql_bin_for_product(
                            cr, uid,
                            row_itemids=to_check.keys(),
                            row_warehouse=location['warehouse_id'] and location['warehouse_id'][1] or None,
                            ignore_bins=['', 'NULL']
                            )

                        to_generate = list(set(to_check.keys()) - set(exists_bins.keys()))
                        if to_generate:
                            sb_create_obj = self.pool.get('stock.bin.create')
                            for item in to_generate:
                                new_bin = None
                                try:
                                    sb_create_id = sb_create_obj.create(cr, uid, {
                                        'warehouse_id': location['warehouse_id'] and location['warehouse_id'][0] or None,
                                        'location_id': location_id,
                                        'location': location['name'] or None,
                                        'product_id': to_check[item]['product_id'],
                                        'size_id': to_check[item]['size_id'] or None,
                                        'qty_capacity': to_check[item]['qty'] or 0
                                    })
                                    if sb_create_id and sb_create_obj.check_location(cr, uid, sb_create_id):
                                        sug_bin = sb_create_obj.read(cr, uid, sb_create_id, ['name'])['name']
                                        if sug_bin and sug_bin != 'null' and sb_create_obj.check_bin(cr, uid, sb_create_id):
                                            new_bin = sug_bin
                                except:
                                    pass
                                exists_bins.update({item: new_bin})

                        for key, val in exists_bins.iteritems():
                            if key in to_check:
                                bin_move_ids = to_check[key].get('move_ids', '').split(',')
                                for move_id in bin_move_ids:
                                    line_ids.append(line_obj.create(cr, uid, {
                                        'wizard_id': revert_id,
                                        'move_id': int(move_id),
                                        'location_id': location_id or False,
                                        'bin': val,
                                        }))

            value.update({'line_ids': [(6, 0, line_ids)]})
        else:
            warning = {
                'title': 'Configuration Error !',
                'message': 'Nothing to return or source order not found'
                }
        self.write(cr, uid, ids, value)
        value.update(line_ids=line_ids)
        return {'value': value, 'warning': warning}

    def action_confirmation(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'confirmation may only be done one at a time'
        confirmation = self.browse(cr, uid, ids[0])
        if confirmation.sale_id:
            picking_obj = self.pool.get('stock.picking')
            sale_obj = self.pool.get('sale.order')
            move_obj = self.pool.get('stock.move')
            order = sale_obj.read(cr, uid, confirmation.sale_id.id, ['tracking_ref','origin_carrier', 'is_set'])
            if order and order.get('tracking_ref', False):
                context.update({'tracking_ref': order.get('tracking_ref', False)})

            if order and order.get('origin_carrier', False):
                context.update({'origin_carrier': order.get('origin_carrier', False)})
            else:
                context.update({'origin_carrier': confirmation.sale_id.picking_ids[0].origin_carrier})

            wf_service = netsvc.LocalService("workflow")
            if confirmation.target == 'all_to_source':
                for r in confirmation.sale_id.picking_ids:
                    if r not in ('draft', 'cancel', 'final_cancel'):
                        wf_service.trg_validate(uid, 'stock.picking', r.id, 'button_done', cr)
                sale_obj.set_done(cr, uid, [confirmation.sale_id.id])

            elif confirmation.line_ids:
                to_return = {}
                confirm_cont = {}

                if order.get('is_set'):
                    sets = {}
                    set_product_ids = []
                    for line in confirmation.line_ids:
                        if line.move_id.is_set:
                            if line.move_id.set_product_id.default_code not in sets:
                                sets[line.move_id.set_product_id.default_code] = []
                            if line.move_id.set_product_id.id not in set_product_ids:
                                set_product_ids.append(line.move_id.set_product_id.id)
                            sets[line.move_id.set_product_id.default_code].append(line)
                    if sets and confirmation.removed_line_ids:
                        for setid, components in sets.items():
                            psc_lines = self.pool.get('product.set.components').get_formula(cr, uid, components)
                            get_n = psc_lines.get(components[0].product_id.id, False)
                            context.update({'count_split_sets': components[0].qty / get_n or False})
                            if not move_obj.check_set(cr, uid, components, context):
                                raise osv.except_osv('Warning!', 'Incorect try split sets  %s !' % setid)

                for line in confirmation.line_ids:
                    if line.move_id.picking_id.id not in to_return:
                        to_return[line.move_id.picking_id.id] = []
                        confirm_cont[line.move_id.picking_id.id] = {}

                    confirmation_move_id = move_obj.divide(cr, uid, line.move_id.id, line.qty)
                    if confirmation_move_id:

                        confirm_cont[line.move_id.picking_id.id][confirmation_move_id] = line.move_id.id
                        to_return[line.move_id.picking_id.id].append(confirmation_move_id)
                        move_obj.write(cr, uid, confirmation_move_id, {
                            'location_id': line.location_id.id,
                            'bin': line.bin,
                            })

                for pick_id, move_ids in to_return.iteritems():
                    if move_ids:
                        confirm_dict = confirm_cont.get(pick_id, {})
                        if len(confirmation.removed_line_ids) == 0:
                            if context and context.get('tracking_ref', False) and context.get('origin_carrier', False):
                                self.pool.get('stock.picking').write(cr, uid, pick_id, {
                                    'tracking_ref': context.get('tracking_ref', False),
                                    'carrier_code': context.get('origin_carrier', False)
                                })
                            picking_obj.do_done(cr, uid, pick_id, context)
                        else:
                            picking_obj.do_split_move_ids(cr, uid, pick_id, move_ids, 'to_done',
                                                          context=dict(context or {},
                                                                       flash_confirm=confirm_dict))
                    else:
                        raise osv.except_osv('Warning!', 'Nothing to confirmation')

        return {'type': 'ir.actions.act_window_close'}

    def action_refresh(self, cr, uid, ids, line_ids,removed_line_ids, context=None):

        line_obj = self.pool.get("confirmation.flash.order.line")
        confirmation = self.browse(cr, uid, ids[0])

        self._split(cr, uid, [[confirmation.line_ids, 'wizard_id'], [confirmation.removed_line_ids, 'origin_wizard_id']][::1], [self._get_update_lines(line_ids), self._get_update_lines(removed_line_ids)][::1])
        self._split(cr, uid, [[confirmation.line_ids, 'wizard_id'], [confirmation.removed_line_ids, 'origin_wizard_id']][::-1], [self._get_update_lines(line_ids), self._get_update_lines(removed_line_ids)][::-1])

        self._combine(cr, uid, [confirmation.line_ids, confirmation.removed_line_ids])

        value = {
            'line_ids': line_obj.search(cr, uid, [('wizard_id', 'in', ids)]),
            'removed_line_ids': line_obj.search(cr, uid, [('origin_wizard_id', 'in', ids)]),
        }

        return {'value': value}

    def _split(self, cr, uid, lines, updated_lines):
        line_obj = self.pool.get("confirmation.flash.order.line")
        sours = lines[0]
        dest = lines[1]
        for line in sours[0]:
            if sours[0] and updated_lines[0]:
                for update_line in updated_lines[0]:
                    if line.id in update_line:
                        r_qty = line.qty - update_line[line.id]['qty']
                        u_qty = update_line[line.id]['qty']
                        uline = self._get_line_by_id(sours[0], line.id)
                        rline = self._get_line_by_bin(dest[0], line.bin)
                        if rline:
                            line_obj.write(cr, uid, [rline.id], {'qty': r_qty + rline.qty, })
                        else:
                            new_line = line_obj.create(cr, uid, {'move_id': int(line.move_id.id),
                                                                 'location_id': line.location_id.id or False,
                                                                 'bin': line.bin,
                                                                 dest[1]: line.wizard_id.id or line.origin_wizard_id.id,
                                                                 'qty': r_qty})
                            line_obj.write(cr, uid, [new_line], {'qty': r_qty, })
                        if rline or new_line:
                            line_obj.write(cr, uid, [line.id],
                                           {
                                               'qty': u_qty,
                                           })
                            if u_qty == 0:
                                line_obj.write(cr, uid, [line.id],
                                               {
                                                   'wizard_id': False,
                                                   'origin_wizard_id': False,
                                               })

    def _combine(self, cr, uid, all_lines):
        line_obj = self.pool.get("confirmation.flash.order.line")

        if all_lines:
            for lines in all_lines:
                source = lines
                dist = lines
                for sline in source:
                    for dline in dist:
                        if sline.bin == dline.bin \
                                and sline.id != dline.id \
                                and sline.move_id.is_set == dline.move_id.is_set:
                            line_obj.write(cr, uid, [sline.id],
                                           {
                                               'qty': sline.qty + dline.qty
                                           })
                            line_obj.write(cr, uid, [dline.id],
                                           {
                                               'wizard_id': False,
                                               'origin_wizard_id': False,
                                               'qty': 0
                                           })
                            if dline in source:
                                source.remove(dline)
                            if dline in dist:
                                dist.remove(dline)


    def _get_update_lines(self, lines=None):
        res = []
        if lines is not None:
            for line in lines:
                if line[1] and line[2] and line[2] != False:
                    res.append({line[1]: line[2]})
        return res

    def _get_line_by_id(self,lines=None, id=None):
        res = False
        if lines and id:
            for line in lines:
                if line.id == id:
                    res = line
        return res

    def _get_line_by_bin(self,lines=None, bin=None):
        res = False
        if lines and id:
            for line in lines:
                if line.bin == bin:
                    res = line
        return res

    def action_reject_all(self, cr, uid, ids, context=None):
        return self.action_move_all(cr, uid, ids, target=-1, context=context)

    def action_approve_all(self, cr, uid, ids, context=None):
        return self.action_move_all(cr, uid, ids, target=1, context=context)

    def action_move_all(self, cr, uid, ids, target=1, context=None):
        dest, source = [('wizard_id', 'line_ids'), ('origin_wizard_id', 'removed_line_ids')][::target]
        line_obj = self.pool.get("confirmation.flash.order.line")
        line_ids = line_obj.search(cr, uid, ['|', ('wizard_id', 'in', ids), ('origin_wizard_id', 'in', ids)])
        line_obj.write(cr, uid, line_ids, {dest[0]: ids[0], source[0]: False})
        return {'value': {dest[1]: line_ids, source[1]: []}}

confirmation_flash_order()


class confirmation_flash_order_line(osv.osv_memory):
    _name = 'confirmation.flash.order.line'

    _rec_name = "move_id"

    def _get_default_code(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, False)

        if ids:
            cr.execute("""  select cl.id,pp.default_code from confirmation_flash_order_line cl
                left join stock_move sm on sm.id = cl.move_id
                left join product_product pp on pp.id = sm.set_product_id
                where cl.id in  %s
                            ;
                    """, (tuple(ids), ))
            for row_id, name in cr.fetchall():
                result[row_id] = name
        return result

    _columns = {
        'wizard_id': fields.many2one('confirmation.flash.order', 'Wizard', required=False,),
        'origin_wizard_id': fields.many2one('confirmation.flash.order', 'Wizard', required=False,),
        'move_id': fields.many2one('stock.move', 'Move', required=True, ),
        'location_id': fields.many2one('stock.location', 'Location', required=True, ),
        'set_product_id': fields.related('move_id', 'set_product_id', string="Set Product", type="many2one",
                                     relation="product.product", ),
        'set': fields.function(_get_default_code, string='Set', type='char', size=128),
        'product_id': fields.related('move_id', 'product_id', string="Product", type="many2one", relation="product.product", ),
        'size_id': fields.related('move_id', 'size_id', string="Size", type="many2one", relation="ring.size", ),
        'product_qty': fields.related('move_id', 'product_qty', string="Available Qty", type="integer", ),
        'qty': fields.integer(string="confirmation Qty", ),
        'bin': fields.char('Bin', size=256, ),
    }

    def create(self, cr, uid, vals, context=None):
        if 'move_id' in vals:
            vals['qty'] = self.pool.get('stock.move').read(cr, uid, vals['move_id'], ['product_qty'])['product_qty']

        return super(confirmation_flash_order_line, self).create(cr, uid, vals, context=None)

    def approve(self, cr, uid, ids, context=None):
        return self.move(cr, uid, ids, target=1, context=context)

    def reject(self, cr, uid, ids, context=None):
        return self.move(cr, uid, ids, target=-1, context=context)

    def check_set_components(self, cr, uid, ids, context=None, column ='wizard_id'):
        line_obj = self.pool.get("confirmation.flash.order.line")
        line_ids = []
        size = False
        for line in self.browse(cr, uid, ids, context=context):
            if line.move_id.set_product_id.id:
                wizard_id = context.get('wizard_id')
                if not line_ids:
                    line_ids = line_obj.search(cr, uid, [(column, 'in', [wizard_id])])
                    size = line.move_id.size_id.id
                for wizard_line in self.browse(cr, uid, line_ids, context=context):
                    if wizard_line.move_id.set_product_id.id and\
                            wizard_line.move_id.set_product_id.id == line.move_id.set_product_id.id:
                        if not size and wizard_line.move_id.size_id.id:
                            size = wizard_line.move_id.size_id.id
                        if(wizard_line.move_id.size_id.id == size or not wizard_line.move_id.size_id.id):
                            ids.append(wizard_line.id)

        return sorted(set(ids))

    def move(self, cr, uid, ids, target=1, context=None):
        dest, source = ['wizard_id', 'origin_wizard_id'][::target]
        if context is None:
            context = {}
        ids = self.check_set_components(cr, uid, ids, context, source)
        wizard_id = context.get('wizard_id')
        if wizard_id:
            data = {dest: wizard_id, source: False}
            self.write(cr, uid, ids, data)

        return True

confirmation_flash_order_line()
