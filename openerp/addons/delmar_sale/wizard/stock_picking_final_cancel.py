# -*- coding: utf-8 -*-

from osv import fields, osv
import netsvc


class stock_picking_final_cancel_codes(osv.osv):
    _name = "stock.picking.final.cancel.codes"

    _columns = {
        'name': fields.char('Description code',
                            size=256,
                            required=True
                            ),
        'code': fields.char('Cancel code',
                            size=64,
                            required=True
                            ),
        'customer': fields.many2one('res.partner', 'Customer')
    }

    def action_confirm_cancel_code(self, cr, uid, ids, context=None):
        line_id, = ids
        cancel_code = self.browse(cr, uid, line_id)
        context['cancel_code'] = cancel_code.code
        wf_service = netsvc.LocalService("workflow")
        remote_status = self.pool.get('sale.integration').integrationApiSendCancel(cr, uid, context['picking_id'], context=context)
        if remote_status:
            wf_service.trg_validate(uid, 'stock.picking', context['picking_id'], 'button_final_cancel', cr)
        return {'type': 'ir.actions.act_window_close'}

stock_picking_final_cancel_codes()


class stock_picking_final_wizard(osv.osv_memory):
    _name = "stock.picking.final.wizard"

    _columns = {
        'picking_id': fields.many2one('stock.picking', 'Customer'),
        'cancel_codes': fields.many2many('stock.picking.final.cancel.codes', 'stock_picking_final_wizard_rel', 'wizard_id', 'code_id', string="Cancel Codes", readonly=True)
    }

    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}

        res = super(stock_picking_final_wizard, self).default_get(cr, uid, fields, context=context)

        if context.get('customer_id', False):
            ids = self.pool.get('stock.picking.final.cancel.codes').search(cr, uid, [('customer', '=', context['customer_id'])])

            res.update({
                'picking_id': context.get('picking_id'),
                'cancel_codes': [(6, 0, [x for x in ids])]
            })

        return res

stock_picking_final_wizard()
