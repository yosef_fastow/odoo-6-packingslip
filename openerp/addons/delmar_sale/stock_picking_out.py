# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import logging
import time
from datetime import datetime


logger = logging.getLogger('stock_picking_out')


class stock_picking_track(osv.osv):
    _name = "stock.picking.track"

    _columns = {
        'name': fields.char('Tracking numbers', size=256),
        'ignore_address_dups': fields.boolean('Ignore Address Duplicates', ),
        'ignore_price': fields.boolean('Add order above 200$', ),
    }

    _defaults = {
        'name': False,
        'ignore_address_dups': False,
        # 'ignore_price': False, # switched off by request Erel
    }

    def confirm_track(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        picking_obj = self.pool.get('stock.picking')
        batch_obj = self.pool.get('stock.picking.shipping.batch')

        for track in self.browse(cr, uid, ids, context=context):
            logger.info("Search Tracking Number: %s", track.name)
            dt_start = datetime.now()

            success = False
            order = {}
            shipping_batch = {}
            scan_date = datetime.utcnow()
            scan_user = self.pool.get('res.users').read(cr, uid, uid, ['name'])
            log_date = time.strftime('%m-%d %H:%M:%S', time.gmtime())

            try:

                # Get order
                search_str = track.name or ''
                search_str = search_str.replace('\n', '').replace('\t', '').strip()

                order = self.get_order(cr, uid, search_str)

                if order.get('shipping_batch_id', False):
                    shipping_orders = batch_obj.get_orders(cr, uid, order['shipping_batch_id'])
                    shipping_order_ids = []

                    for shipping_order in shipping_orders:
                        shipping_order['tracking_ref'] = order['tracking_ref']
                        if order['order_id'] != shipping_order['order_id']:
                            shipping_order_ids.append(shipping_order['order_id'])

                    shipping_batch.update({
                        'id': order['shipping_batch_id'],
                        'name': order.get('shipping_batch_name', False),
                        'orders': shipping_orders,
                        'order_ids': shipping_order_ids,
                    })

                if not shipping_batch:  #single order

                    # Check state
                    if order.get('state', False) == 'cancel':
                        raise osv.except_osv(_('Warning'), _('Order %s was cancelled' % (order['full_name'])))

                    # Check order warehouse
                    if not order.get('wh_id', False):
                        raise osv.except_osv(_('Warning'), _('Order %s has not warehouse' % (order['full_name'])))

                    # Check rescan
                    if order.get('date_out', False):
                        raise osv.except_osv(_('Warning'), _('Order %s already scanned' % (order['full_name'])))

                    # Check multi address consolidation
                    if not track.ignore_address_dups:
                        status, msg = self.check_multiaddress(cr, uid, order, context=context)
                        if not status:
                            raise osv.except_osv(_('Warning'), warning)

                    # Check price
                    if not track.ignore_price and float(order.get('total_price', 0.0)) >= 200.00:
                        raise osv.except_osv(_('Warning'), _('Order %s above 200$' % (order['full_name'])))


                else:  # batch of orders

                    total_price = 0.0
                    canceled_orders = []
                    scanned_orders = []
                    not_ready_orders = []

                    for batch_order in shipping_batch.get('orders', []):

                        total_price += float(batch_order.get('total_price', 0.0))

                        if batch_order.get('state', False) == 'cancel':
                            canceled_orders.append(batch_order['order_name'])

                        if batch_order.get('state', False) not in ('picking', 'outcode_except', 'shipped', 'done'):
                            not_ready_orders.append(batch_order['order_name'])

                        if batch_order.get('date_out', False):
                            scanned_orders.append(batch_order['order_name'])

                    if canceled_orders:
                        raise osv.except_osv(_('Warning'), _(
                            '%s order(s) were cancelled: %s' % (len(canceled_orders), ", ".join(canceled_orders))))

                    if not_ready_orders:
                        raise osv.except_osv(_('Warning'), _(
                            '%s order(s) not ready to ship: %s' % (len(not_ready_orders), ", ".join(not_ready_orders))))

                    if scanned_orders:
                        raise osv.except_osv(_('Warning'), _(
                            '%s order(s) were already scanned: %s' % (len(scanned_orders), ", ".join(scanned_orders))))

                    if not track.ignore_price and total_price >= 200.00:
                        raise osv.except_osv(_('Warning'), _('Orders above 200$'))

                    # Check multi address consolidation
                    if not track.ignore_address_dups:
                        status, msg = self.check_multiaddress(cr, uid, order, batch_id=shipping_batch['id'], context=context)
                        if not status:
                            raise osv.except_osv(_('Warning'), msg)

                # Get order moves
                moves = self.get_order_moves(cr, uid, order['order_id'])
                order['moves'] = moves.get('moves', [])
                order['locations'] = moves.get('locations', {})
                if not order['moves']:
                    raise osv.except_osv(_('Warning'), 'Order %s has not stock moves' % (order['full_name']))

                for shipping_order in shipping_batch.get('orders', []):
                    if shipping_order['order_id'] == order['order_id']:
                        continue

                    shipping_order['moves'] = self.get_order_moves(cr, uid, shipping_order['order_id']).get('moves', [])
                    if not shipping_order['moves']:
                        raise osv.except_osv(_('Warning'),
                                             'Order %s has not stock moves' % (shipping_order['full_name']))

                # Get list id
                order['paps'] = self.get_order_paps(cr, uid, order['locations'].keys())
                if not order['paps']:
                    raise osv.except_osv(_('Warning'),
                                         _('Could not find PAPS list for order %s' % (order['full_name'])))

                # Prepare lines to insert
                inserts = self.prepare_inserts(cr, uid, order, search_str)
                insert_sqls = inserts.get('insert_sqls', [])
                insert_params = inserts.get('insert_params', [])

                for shipping_order in shipping_batch.get('orders', []):
                    shipping_order['paps'] = order['paps']
                    if shipping_order['order_id'] == order['order_id']:
                        continue
                    sec_inserts = self.prepare_inserts(cr, uid, shipping_order, search_str)
                    insert_sqls += sec_inserts.get('insert_sqls', [])
                    insert_params += sec_inserts.get('insert_params', [])

                if not insert_sqls or not insert_params:
                    raise osv.except_osv(_('Warning'), 'Nothing to insert')

                # Get permission to out
                valid = self.get_permission_to_out(cr, uid, order['order_id'], locations=order['locations'].values())
                if not valid:
                    raise osv.except_osv(_('Warning'), _(
                        'Order %s cannot be scanned now. Please check "Daily Out of Door" settings' % (
                            order['full_name'])))


                # Update orders info
                picking_obj.write(cr, uid, order['order_id'], {
                    'date_out': scan_date,
                    'list_ids': [(4, x, False) for x in order['paps'].keys()],
                })

                if shipping_batch.get('order_ids', False):
                    picking_obj.write(cr, uid, shipping_batch['order_ids'], {
                        'date_out': scan_date,
                        'list_ids': [(4, x, False) for x in order['paps'].keys()],
                    })

                for paps in order['paps'].values():
                    if shipping_batch:
                        batch_obj.log(cr, uid, shipping_batch['id'],
                                      "PAPS %s: Batch %s scanned" % (paps, shipping_batch['name']))
                    else:
                        picking_obj.log(cr, uid, order['order_id'],
                                        "PAPS %s: Order %s scanned" % (paps, order['full_name']))

                if shipping_batch:
                    batch_obj.action_scan(cr, uid, [shipping_batch['id']], order['tracking_ref'])

                # Send to MSSQL
                server_obj = self.pool.get('fetchdb.server')

                server_id = server_obj.get_sale_servers(cr, uid, single=True)
                if not server_id:
                    raise osv.except_osv(_('Warning!'), 'Can not do scan order. Server is not available!')

                logger.debug("Prepare Time %s ", datetime.now() - dt_start)
                success = bool(server_obj.make_query(cr, uid, server_id, "\n\n".join(insert_sqls), params=insert_params,
                                                     select=False, commit=True))

                if success:
                    self.write(cr, uid, track.id, self._defaults)
                else:
                    raise osv.except_osv(_('Warning'), 'MSSQL return some errors')

            finally:
                if shipping_batch and shipping_batch.get('orders', False):
                    log_str = "\n%s %s: Scan in %s group by %s (%s)" % (
                        log_date, scan_user.get('name', ''), shipping_batch.get('name', ''), search_str or '',
                        "Success" if success else "Failure")
                    for batch_order in shipping_batch['orders']:
                        picking_obj.write(cr, uid, batch_order['order_id'], {
                            'scan_uid': uid,
                            'scan_date': scan_date,
                            'report_history': batch_order.get('report_history', '') + log_str,
                        })
                elif order and order.get('order_id', False):
                    log_str = "\n%s %s: Scan by %s (%s)" % (
                        log_date, scan_user.get('name', ''), search_str or '', "Success" if success else "Failure")
                    picking_obj.write(cr, uid, order['order_id'], {
                        'scan_uid': uid,
                        'scan_date': scan_date,
                        'report_history': order.get('report_history', '') + log_str,
                    })

            logger.debug("Total time %s ", datetime.now() - dt_start)

        return True

    def get_order(self, cr, uid, search_str, context=None):
        order = False
        if not search_str:
            raise osv.except_osv(_('Warning'), 'Search string is empty')

        else:
            get_order_sql = """ SELECT
                                    sp.id as order_id,
                                    sp.name as order_name,
                                    sp.tracking_ref as tracking_ref,
                                    sp.carrier_code as carrier_code,
                                    sp.date_out as date_out,
                                    sp.warehouses as wh_id,
                                    sp.address_grp as address_grp,
                                    sp.state as state,
                                    sp.shipping_batch_id as shipping_batch_id,
                                    sb.name as shipping_batch_name,
                                    sp.total_price as total_price,
                                    sp.report_history as report_history
                                FROM stock_picking sp
                                    LEFT JOIN stock_picking_shipping_batch sb on sp.shipping_batch_id = sb.id
                                WHERE 1=1
                                    AND sp.tracking_ref IS NOT NULL
                                    AND trim(sp.tracking_ref) != ''
                                    AND %s ilike trim(quote_literal(concat('%%', trim(sp.tracking_ref))), '''')
                                ;
            """
            cr.execute(get_order_sql, [search_str])
            orders_res = cr.dictfetchall()
            if not orders_res:
                if self.pool.get('stock.picking').find_tracking_number(cr, uid, search_str, context=context):
                    cr.execute(get_order_sql, [search_str])
                    orders_res = cr.dictfetchall()
            if not orders_res:
                raise osv.except_osv(_('Warning'), _('Order with tracking number %s not found' % (search_str)))
            else:
                order_batches = {}
                for order_res in orders_res:
                    full_name = "%s(%s)" % (order_res['order_name'], order_res['tracking_ref'])
                    batch_id = order_res.get('shipping_batch_id', False)
                    batch_name = order_res.get('shipping_batch_name', False) or 'Not Combined'
                    if not order_batches.get('batch_id', False):
                        order_batches[batch_id] = {
                            'name': batch_name,
                            'order_names': [],
                            'orders': [],
                        }
                    order_res['full_name'] = full_name
                    order_batches[batch_id]['orders'].append(order_res)
                    order_batches[batch_id]['order_names'].append(full_name)

                batch_ids = order_batches.keys()
                if len(orders_res) > 1 and (False in batch_ids or len(batch_ids) > 1):
                    orders_str = [
                        "%s batch: %s" % (order_batches[x]['name'], ", ".join(order_batches[x]['order_names'])) for x in
                        order_batches]
                    raise osv.except_osv(_('Warning'), _(
                        'Found several orders by %s tracking number. \n %s.' % (search_str, ";\n ".join(orders_str))))
                order = order_batches[batch_ids[0]]['orders'][0]

        return order

    def get_order_moves(self, cr, uid, picking_id, context=None):
        moves = []
        locations = {}

        sql = """   SELECT
                        pp.default_code as default_code,
                        rs.name as size,
                        sm.product_qty as qty,
                        sm.export_ref_ca as export_ref_ca,
                        sl.name as location,
                        sl.id as location_id
                    FROM stock_move sm
                        LEFT JOIN product_product pp ON (sm.product_id = pp.id)
                        LEFT JOIN ring_size rs ON (sm.size_id = rs.id)
                        LEFT JOIN stock_location sl ON (sm.location_id = sl.id)
                    WHERE sm.picking_id = %s;
        """ % (picking_id)
        cr.execute(sql)
        order_moves_res = cr.dictfetchall() or []

        for move in order_moves_res:
            if not move.get('default_code', False) or not move.get('qty', 0):
                continue

            size_postfix = self.pool.get('stock.move').get_size_postfix(cr, uid, move['size'])
            move['itemid'] = "%s/%s" % (move['default_code'], size_postfix)
            moves.append(move)

            if move.get('location_id', False):
                locations[move['location_id']] = move['location']

        return {
            'moves': moves,
            'locations': locations
        }

    def check_multiaddress(self, cr, uid, order, batch_id=False, context=None):
        where_statements = []
        if batch_id:
            where_statements.append('AND (sp.shipping_batch_id IS NULL OR sp.shipping_batch_id != %s)' % (batch_id))

        check_address_sql = """ SELECT
                                    concat(sp.name, E'(', coalesce(sp.tracking_ref, 'No Tracking #'), E')') as dup,
                                    ru.name as scan_user,
                                    CASE WHEN
                                        sp.scan_date IS NOT NULL
                                    THEN
                                        (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::TIMESTAMP WITHOUT TIME ZONE - sp.scan_date
                                    ELSE
                                        NULL
                                    END as time_ago
                                FROM stock_picking sp
                                    LEFT JOIN res_users ru ON ru.id = sp.scan_uid
                                WHERE 1=1
                                    AND sp.id != %s
                                    AND sp.state NOT IN ('done', 'returned', 'cancel')
                                    AND sp.address_grp = '%s'
                                    %s
                                ;
        """ % (order['order_id'], order.get('address_grp', '').replace("'", "''"), " ".join(where_statements))

        cr.execute(check_address_sql)
        address_res = cr.dictfetchall()
        if address_res:
            dup_ordes = []
            for dup in address_res:
                row = ""
                if dup.get('dup', False):
                    row += dup['dup']

                    if dup.get('scan_user', False):
                        row += " scanned by " + dup['scan_user']
                    if dup.get('time_ago', False):

                        if dup['time_ago'].days:
                            row += dup['time_ago'].days + ' day(s) ago'
                        elif dup['time_ago'].seconds:
                            sec = dup['time_ago'].seconds
                            mins = sec // 60
                            hours = mins // 60
                            if hours:
                                row += " %s hour(s) ago" % (hours)
                            elif mins:
                                row += " %s min(s) ago" % (mins)
                            else:
                                row += " %s second(s) ago" % (sec)

                if row:
                    dup_ordes.append(row)

            warning = _("Order %s has same address with %s other order(s): %s" % (
                order['full_name'], len(address_res), ", ".join([x for x in dup_ordes])))
            return False, warning

        return True, ''

    def get_permission_to_out(self, cr, uid, picking_id, scan_date=None, locations=None, context=None):
        if not locations:
            locations = []
        valid = False
        sql = """   SELECT sp.id,
                        from_time::time AS from_time,
                        -- (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time AS current,
                        coalesce('%s', (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time) AS current,
                        to_time::time AS to_time,
                        CASE WHEN from_time::time < to_time::time
                            THEN
                                from_time::time <= (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time AND (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time <= to_time::time
                            ELSE
                                (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time <= to_time::time OR from_time::time <= (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')::time
                        END AS valid
                    FROM stock_picking sp
                        LEFT JOIN stock_picking_out_settings st ON (
                            (st.warehouse_id IS NOT NULL AND st.warehouse_id = sp.warehouses)
                            or
                            (st.location IS NOT NULL AND st.location != '' AND upper(st.location) IN ('%s'))
                        )
                    WHERE sp.id = %s AND st.active = True;
        """ % (scan_date, "','".join(set(locations)), picking_id)
        cr.execute(sql)
        valid_res = cr.dictfetchall() or []
        for res in valid_res:
            if res.get('valid', False):
                valid = True
                break

        return valid

    def get_order_paps(self, cr, uid, loc_ids, context=None):
        res = {}

        if loc_ids:

            get_list_id_sql = """   SELECT
                                        sl.id as id,
                                        sl.name as name
                                    FROM stock_picking_out_list sl
                                        LEFT JOIN stock_out_list_location_rel rel on sl.id = rel.list_id
                                    WHERE 1=1
                                        AND sl.actual = True
                                        AND rel.location_id in (%s)
                                    ;
            """ % (", ".join([str(x) for x in loc_ids if x]))
            cr.execute(get_list_id_sql)
            list_id_res = cr.dictfetchall() or []

            for list_res in list_id_res:
                res[list_res['id']] = list_res['name']

        return res


    def prepare_inserts(self, cr, uid, order, tracking_ref, context=None):

        insert_sqls = []
        insert_params = []

        for move in order.get('moves', []):
            if not move.get('default_code', False) or not move.get('qty', 0):
                continue

            for paps_id in order.get('paps', {}).keys():
                insert_sqls.append("""
                    IF (
                        NOT EXISTS(
                            SELECT *
                            FROM outofthedoorlist
                            WHERE 1=1
                                AND "OrderID" = ?
                                AND "PAPSID" = ?
                                AND "OrderNumber" = ?
                                AND "ItemID" = ?
                        )
                    )
                    BEGIN
                        INSERT INTO "outofthedoorlist" (
                            "OrderID",
                            "OrderNumber",
                            "ItemID",
                            "QTY",
                            "ScanDateTime",
                            "ScanNumber",
                            "PAPSID"
                        )
                        VALUES(
                            ?,?,?,?,getdate(),?,?
                        )
                    END
                    ELSE
                    BEGIN
                        UPDATE outofthedoorlist
                        SET     "QTY" = ?,
                                "ScanDateTime" = getdate(),
                                "ScanNumber" = ?
                        WHERE 1=1
                            AND "OrderID" = ?
                            AND "PAPSID" = ?
                            AND "OrderNumber" = ?
                            AND "ItemID" = ?
                    END;
                """)
                insert_params += [
                    int(order['order_id']),
                    int(paps_id),
                    order['tracking_ref'],
                    move['itemid'],

                    int(order['order_id']),
                    order['tracking_ref'],
                    move['itemid'],
                    int(move['qty']),
                    tracking_ref,
                    int(paps_id),

                    int(move['qty']),
                    tracking_ref,
                    int(order['order_id']),
                    int(paps_id),
                    order['tracking_ref'],
                    move['itemid']
                ]

        return {
            'insert_sqls': insert_sqls,
            'insert_params': insert_params,
        }

stock_picking_track()


class stock_picking_out_settings(osv.osv):
    _name = "stock.picking.out.settings"

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', select=True, required=False,
                                        domain=[('virtual', '=', False)]),
        'location': fields.char('Location', size=256, required=False, ),
        'from_time': fields.datetime('From Time', required=True, ),
        'to_time': fields.datetime('To Time', required=True, ),
        'active': fields.boolean('Active'),
    }

    _defaults = {
        'active': True,
    }

stock_picking_out_settings()


class stock_picking_out_list(osv.osv):
    _name = "stock.picking.out.list"

    _columns = {
        'name': fields.char('Name', size=256, required=True, ),
        'location_ids': fields.many2many("stock.location", "stock_out_list_location_rel", "list_id", "location_id",
                                         string="Locations", required=True,
                                         domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')]),
        'picking_ids': fields.many2many('stock.picking', "stock_out_list_picking_rel", "list_id", "picking_id",
                                        string="PAPS"),
        'description': fields.char('Description', size=256, ),
        'actual': fields.boolean('Active', readonly=True, ),
    }

    _defaults = {
        'actual': False,
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default['picking_ids'] = [(6, 0, [])]
        default['actual'] = False
        return super(stock_picking_out_list, self).copy(cr, uid, id, default=default, context=context)

    def activate(self, cr, uid, ids, context=None):
        for out_list in self.read(cr, uid, ids, ['location_ids'], context=context):
            loc_ids = out_list.get('location_ids', [])
            if loc_ids:
                list_ids = self.search(cr, uid, [('location_ids', 'in', loc_ids), ('actual', '=', True)],
                                       context=context)
                if list_ids:
                    self.write(cr, uid, list_ids, {'actual': False}, context=context)

            self.write(cr, uid, out_list['id'], {'actual': True}, context=context)

        return True

    def deactivate(self, cr, uid, ids, context=None):
        if ids:
            self.write(cr, uid, ids, {'actual': False}, context=context)


stock_picking_out_list()
