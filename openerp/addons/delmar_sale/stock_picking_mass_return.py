# -*- coding: utf-8 -*-
##############################################################################
#
#    ISDDesign
#    Copyright (C) 2017
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import base64
import xlrd
import csv
import time
from tools.translate import _
import re
import json
from addons.pf_utils.utils.label_printer import LabelWizPrinter
from addons.import_fields import import_fields_wizard
from datetime import datetime

import logging

_logger = logging.getLogger(__name__)

MASS_STATES = [
    ('draft', 'Empty'),
    ('verification', 'Verification'),
    ('confirmation', 'Confirmation'),
    ('done', 'Done'),
]

class StockPickingMassReturn(osv.osv):
    _inherit = 'stock.picking.return.batch'
    _name = 'stock.picking.mass.return'

    def _get_state(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            state = 'undefined'
            if not line.prepared_line_ids and not line.verified_line_ids and not line.return_line_ids:
                state = 'draft'
            elif line.prepared_line_ids:
                state = 'verification'
            elif line.verified_line_ids and not line.prepared_line_ids:
                state = 'confirmation'
            elif line.return_line_ids and not line.prepared_line_ids and not line.verified_line_ids:
                state = 'done'
            res[line['id']] = state
        return res

    def _batch_search(self, cr, uid, obj, name, args, context):
        ids = set()
        _logger.warn("SEARCH BATCH obj: %s" % obj)
        _logger.warn("SEARCH BATCH name: %s" % name)
        _logger.warn("SEARCH BATCH args: %s" % args)
        _logger.warn("SEARCH BATCH context: %s" % context)
        state_cond = list()
        for cond in args:
            if isinstance(cond[2], (list, tuple)):
                state_cond = state_cond + cond[2]

        _logger.warn("SEARCH BATCH cond: %s" % state_cond)
        # Query the status od batch
        sql_q = """
          select id from (
            select id,
              CASE
              when prepared>0
                then 'verification'
              else
                CASE
                when prepared=0 and verified>0
                  then 'confirmation'
                else
                  CASE
                  when prepared=0 and verified=0 and returned>0
                    then 'done'
                  else
                    CASE
                    when prepared=0 and verified=0 and returned=0
                      then 'draft'
                    else
                      'undefined'
                    end
                  end
                end
              end as state
            from (
              select bt.id,
              (select count(*) from stock_picking_return_prepare_line where batch_id=bt.id and state in ('draft','partial')) as prepared,
              (select count(*) from stock_picking_return_verified_line where batch_id=bt.id and state in ('verified')) as verified,
              (select count(*) from stock_picking_mass_return_line where batch_id=bt.id and state in ('done')) as returned
              from stock_picking_mass_return bt
            ) tmp ) tmps where state in %(state)s
        """
        cr.execute(sql_q, {'state': tuple(state_cond)})
        res_ids = set(id[0] for id in cr.fetchall())
        ids = ids and (ids & res_ids) or res_ids

        if ids:
            return [('id', 'in', tuple(ids))]
        return [('id', '=', '0')]

    def _get_qty_prepared(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            query = """select sum(t.qty) as prepared from stock_picking_return_prepare_line t
                WHERE t.batch_id=%(batch)s and t.state in ('draft', 'partial')"""
            cr.execute(query, {'batch': line.id})
            total = cr.fetchone()[0] or 0
            res[line['id']] = total
        return res

    def _get_qty_verified(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            query = """select sum(t.qty_verified) as prepared from stock_picking_return_verified_line t
                WHERE t.batch_id=%(batch)s and t.state in ('verified')"""
            cr.execute(query, {'batch': line.id})
            total = cr.fetchone()[0] or 0
            res[line['id']] = total
        return res

    def _get_qty_returned(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            query = """select sum(t.qty) as prepared from stock_picking_mass_return_line t
                WHERE t.batch_id=%(batch)s and t.state in ('done')"""
            cr.execute(query, {'batch': line.id})
            total = cr.fetchone()[0] or 0
            res[line['id']] = total
        return res

    _columns = {
        'prepared_line_ids': fields.one2many('stock.picking.return.prepare.line', 'batch_id', 'Return-prepared lines', domain=[('state', 'in', ('draft', 'partial'))], ),
        'verified_line_ids': fields.one2many('stock.picking.return.verified.line', 'batch_id', 'Verified return lines', domain=[('state', '=', 'verified')], ),
        'return_line_ids': fields.one2many('stock.picking.mass.return.line', 'batch_id', 'Return Lines', ),
        'state': fields.function(_get_state, string="State", type='char', method=True, store=False, fnct_search=_batch_search, ),
        'mark_prepared': fields.boolean('Mark all', store=False, ),
        'mark_verified': fields.boolean('Mark all', store=False, ),
        'qty_prepared': fields.function(_get_qty_prepared, string='Total to verify QTY', type="integer", store=False, ),
        'qty_verified': fields.function(_get_qty_verified, string='Total verified QTY', type="integer", store=False, ),
        'qty_returned': fields.function(_get_qty_returned, string='Total returned QTY', type="integer", store=False, ),
    }
    _defaults = {
        'state': 'draft',
    }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):

        if context is None:
            context = {}

        try:
            batch_ids = []
            if args:
                f_domain = []
                f_domain = (arg for arg in args if ((isinstance(arg, list) or isinstance(arg, tuple)) and arg[0] == 'product_id')).next()
                f_index = args.index(f_domain)

                if f_domain and f_domain[2]:
                    product_ids = []
                    product_search = f_domain[2]

                    if isinstance(product_search, (int, long)):
                        product_ids.append(product_search)
                    else:
                        products = self.pool.get('product.product').name_search(cr, uid, product_search, context={'mode': 'simple'}, limit=None)
                        if products:
                            product_ids = [x[0] for x in products]

                    if product_ids:
                        cr.execute("""
                            SELECT DISTINCT rl.batch_id
                            FROM stock_picking_return_line rl
                            WHERE rl.product_id in %s;
                        """, (tuple(product_ids), ))

                        res = cr.fetchall()
                        batch_ids = [x[0] for x in res if x]
                args[f_index] = ['id', 'in', batch_ids]
        except:
            pass

        # print args
        return super(StockPickingMassReturn, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

    """def write(self, cr, uid, ids, values, context=None):
        _logger.warn("Batch write values: %s" % values)
        _logger.warn("Batch write context: %s" % context)
        batch = self.browse(cr, uid, ids[0])
        for pline in batch.prepared_line_ids:
            _logger.warn("Batch prepare line: %s" % pline)

        try:
            res = super(StockPickingMassReturn, self).write(cr, uid, ids, values, context=context)
            _logger.warn("Batch write result: %s" % res)
            return res
        except Exception, e:
            _logger.warn("Batch write exception got: %s" % e)
            _logger.info("Batch write exception passed")
            raise osv.except_osv('warning', _(e.message))
            pass
            return True
"""

    def action_print_batch_returns(self, cr, uid, ids, context=None):
        line_obj = self.pool.get("stock.picking.mass.return.line")
        line_ids = line_obj.search(cr, uid, [('batch_id', 'in', ids)])
        return line_obj.action_print_batch_return_line(cr, uid, line_ids)

    # Bulk remove prepared lines
    def action_bulk_remove(self, cr, uid, ids, context=None):
        _logger.warn('BULK REMOVE, ids: %s' % ids)
        _logger.warn('BULK REMOVE, context: %s' % context)
        if context.get('selected_ids') and len(context.get('selected_ids')) > 0:
            prepare_obj = self.pool.get("stock.picking.return.prepare.line")
            prepare_obj.unlink(cr, uid, context.get('selected_ids'), context=context)
            self.write(cr, uid, ids, dict(mark_prepared=False), context)
        else:
            _logger.warn('No lines were selected for remove')
        return True

    # Bulk prepare lines verification
    def action_bulk_verify(self, cr, uid, ids, context=None):
        _logger.warn("Bulk verify context: %s" % context)
        _logger.warn("Selected ids: %s" % context.get('selected_ids'))

        res = False
        if not context:
            context = {}
        if not context['selected_ids']:
            # raise osv.except_osv(_('Warning!'), _("Please mark lines for bulk verification!"))
            return True
        if not context['verify']:
            raise osv.except_osv(_('Warning!'), _("Invalid command"))
            return True

        if context['verify'] == 'custom':
            act_win = False

            # data, window, pepare.line objects
            data_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            line_obj = self.pool.get('stock.picking.return.prepare.line')

            line = self.pool.get("stock.picking.return.prepare.line").browse(cr, uid, context.get('selected_ids')[0], context=context)
            new_context = context
            new_context.update({
                'default_batch_id': line.batch_id.id,
                'default_warehouse_id': line.batch_id.warehouse_id.id,
                'default_lines': context.get('selected_ids'),
                'default_return_target': 'return',
            })

            _logger.warn("New selected context: %s" % new_context)

            # create action window
            act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_mass_return_verify_lines')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
            act_win.update({
                'res_id': None,
                'context': new_context,
            })
            self.write(cr, uid, ids, dict(mark_verified=False), context)
            return act_win

    # Bulk verified lines confirmation
    def action_bulk_confirm(self, cr, uid, ids, context=None):
        _logger.warn('BULK CONFIRM, ids: %s' % ids)
        _logger.warn('BULK CONFIRM, context: %s' % context)
        if context.get('selected_ids') and len(context.get('selected_ids')) > 0:
            verified_obj = self.pool.get('stock.picking.return.verified.line')
            confirmed_lines = verified_obj.confirm_line(cr, uid, context.get('selected_ids'), context=context)
            if context.get('selected_ids', False) and len(context.get('selected_ids', 0)) > 1:
                return self.pool.get('stock.picking.mass.return.line').print_qr_label(cr, uid, confirmed_lines)
            else:
                return confirmed_lines
        else:
            #raise osv.except_osv('Warning!', 'No lines were selected for confirmation!')
            _logger.warn('No lines were selected for confirmation')

        return True

    # Bulk reject verified lines (unverify)
    def action_bulk_reject(self, cr, uid, ids, context=None):
        _logger.warn('BULK REJECT, ids: %s' % ids)
        _logger.warn('BULK REJECT, context: %s' % context)
        if context.get('selected_ids') and len(context.get('selected_ids')) > 0:
            verified_obj = self.pool.get('stock.picking.return.verified.line')
            verified_obj.unlink(cr, uid, context.get('selected_ids'), context=context)
        else:
            _logger.warn('No lines were selected for reject')

        return True


StockPickingMassReturn()


class StockPickingReturnPrepareLine(osv.osv):
    _name = "stock.picking.return.prepare.line"

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for line in self.browse(cr, uid, ids, context=context):
            res.append((line.id, "%s / %s" % (
                line.product_id and line.product_id.default_code or 'N/A',
                line.size_id and line.size_id.name or '0',
            )))
        return res

    def _get_image(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.product_id and line.product_id.product_image or False
        return res

    def _count_verified(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        verified_object = self.pool.get('stock.picking.return.verified.line')
        for line in self.browse(cr, uid, ids, context=context):
            total, verified, unverified = verified_object.count_total_verified_unverified(cr, uid, line.id, context=context)
            res[line['id']] = verified or 0
        return res

    def _check_verification_granted(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        #_logger.warn("Check granted context: %s" % context)
        for line in self.browse(cr, uid, ids, context=context):
            if line.qty <= 0 or not line.product_id or line.cost == 0 or not line.cost or (line.sizeable and not line.size_id):
                res[line['id']] = False
            else:
                res[line['id']] = True
        return res

    def _check_bulk_verification_granted(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        #_logger.warn("Check granted context: %s" % context)
        for line in self.browse(cr, uid, ids, context=context):
            if line.is_set:
                res[line['id']] = True
            else:
                res[line['id']] = True
        return res

    def _check_has_attachments(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            ir_attachment = self.pool.get('ir.attachment')
            at_ids = ir_attachment.search(cr, uid, ['&', ('res_model', '=', self._name), ('res_id', '=', line['id'])])
            if at_ids and len(at_ids) > 0:
                res[line['id']] = True
            else:
                res[line['id']] = False
        return res

    def _get_attachments_list(self, cr, uid, ids, name, args, context=None):
        res = {}
        attachment = self.pool.get('ir.attachment')
        _logger.info("Going to get attachments list")
        for line_id in ids:
            _logger.info("Getting attachments for line_id: %s" % line_id)
            attach_records = attachment.search(cr, uid, ['&', ('res_id', '=', line_id), ('res_model', '=', 'stock.picking.return.prepare.line')])
            res[line_id] = attach_records
        return res

    _columns = {
        'name': fields.char('Complete name', size=256, readonly=True, ),
        'batch_id': fields.many2one('stock.picking.mass.return', 'Mass Return Batch', ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=False, readonly=True, states={'draft': [('readonly', False)]}, ),
        'sku_imported': fields.char('SKU Imported', size=256, readonly=True, states={'draft': [('readonly', False)]}, ),
        'product_image': fields.function(_get_image, string="Image", type='binary', method=True, store=False, ),
        'product_image_bin': fields.binary('Image temp', store=False, ),
        'product_standart_price': fields.related('product_id', 'calculated_standard_price', type='float', string='Standart cost', store=False, ),
        'size_id': fields.many2one('ring.size', 'Sale Size', readonly=True, states={'draft': [('readonly', False)]}, ),
        'size_id_delivery': fields.many2one('ring.size', 'Delivery Size', readonly=True, states={'draft': [('readonly', False)]}, ),
        'size_imported': fields.char('Size Imp.', size=256, readonly=True, ),
        'sizeable': fields.boolean('Sizeable', ),
        'qty': fields.integer('Qty to Verify', readonly=False, states={'draft': [('readonly', False)]}, ),
        'qty_imported': fields.integer('Qty Imp.', readonly=True, ),
        'qty_verified': fields.function(_count_verified, string="Qty verified", type="integer", store=False, ),
        'cost': fields.float('Cost', readonly=True, states={'draft': [('readonly', False)]}, ),
        'cost_imported': fields.float('Cost Imp.', readonly=True, states={'draft': [('readonly', False)]}, ),
        'picking_id': fields.many2one('stock.picking', 'Order',
            domain="[('state', '=', 'done'),('partner_id','=',partner_id)]", required=False,
            readonly=True, states={'draft': [('readonly', False)]},
            help="Please, select delivery order to process return for it."),
        'picking_imported': fields.char('Order Imp.', size=256, readonly=True, states={'draft': [('readonly', False)]}, ),
        'partner_id': fields.many2one('res.partner', "Partner", readonly=True, states={'draft': [('readonly', False)]}, ),
        'verified_lines': fields.one2many('stock.picking.return.verified.line', 'line_id', 'Verified lines', ),
        'comments': fields.char('Comments', size=256, readonly=False, ),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('partial', 'Verified partially'),
            ('verified', 'Verified'),
            ('confirmed', 'Confirmed'),
            ], 'State', ),
        'is_set': fields.boolean('Set', ),
        'grant_verify': fields.function(_check_verification_granted, string="Verification granted", type="boolean", store=False, ),
        'grant_bulk_verify': fields.function(_check_bulk_verification_granted, string="Bulk verification granted", type="boolean", store=False, ),
        'use': fields.boolean('Use', ),
        'damaged': fields.boolean('Dmg item', ),
        'not_delmar_product': fields.boolean('Not Delmar', ),
        'has_attachments': fields.function(_check_has_attachments, string="Is line has attachments", type="boolean", store=False, ),
        'attachments': fields.function(_get_attachments_list, string="Attachments", type="one2many", obj="ir.attachment",  method=True, store=False, ),
        'prepare_file_line_key': fields.char('Import file line key', size=32, readonly=True, ),
    }

    _defaults = {
        'sizeable': True,
        'state': 'draft',
        'damaged': False,
        'is_set': False,
        'product_image_bin': None
    }

    _sql_constraints = [
        ('xls_line_uniq', 'unique(prepare_file_line_key,batch_id)', 'The key must be unique!'),
    ]

    _order = "create_date DESC"

    # check changes on saving lines
    def write(self, cr, uid, ids, vals, context=None):
        _logger.warn("Saving prepare line with values: %s" % vals)
        line_ret = self.browse(cr, uid, ids[0])

        # check qty
        r = self._count_verified(cr, uid, ids)[ids[0]]
        if vals.get('qty') and (line_ret.qty_imported and vals['qty'] > line_ret.qty_imported - r) and not context.get('unlink_verified', False):
            _logger.warn('Quantity invalid: %s' % vals['qty'])
            vals.update({'qty': line_ret.qty_imported - r})
            _logger.warn('Quantity changed to: %s' % vals['qty'])

        # of order was changed in line editor
        if 'picking_id' in vals:
            _logger.warn("Looking for order for line %s" % line_ret)
            self.pool.get('stock.picking').browse(cr, uid, vals['picking_id'])
        # if product was changed - check order has this product/size or remove order
        if 'product_id' in vals and line_ret.picking_id and 'picking_id' not in vals:
            vals.update({
                'picking_id': None,
            })

        # if cost was changed - remove picking_id
        if 'cost' in vals and line_ret.picking_id and 'picking_id' not in vals:
            vals.update({
                'picking_id': None,
            })
        # if size_id was changed - remove picking_id
        if 'size_id' in vals and line_ret.picking_id and 'picking_id' not in vals:
            vals.update({
                'picking_id': None,
            })

        # check product is a SET
        helper = self.pool.get('stock.picking.helper')
        if 'product_id' in vals:
            vals.update({'is_set': helper.check_product_is_set(cr, uid, vals['product_id'])})
        elif line_ret.product_id and line_ret.product_id.id:
            vals.update({'is_set': helper.check_product_is_set(cr, uid, line_ret.product_id.id)})

        vals.update({'product_image_bin': None})
        _logger.warn("Save prepare line vals: %s" % vals)
        super(StockPickingReturnPrepareLine, self).write(cr, uid, ids, vals, context=context)
        return ids

    # on delete prepare line, check if partial verified line exists
    def unlink(self, cr, uid, ids, context=None):
        _logger.warn("Deleting prepare lines: %s" % ids)
        for pline in self.browse(cr, uid, ids, context=context):
            res = self._count_verified(cr, uid, [pline.id], name=None, args=None, context=context)
            if res[pline.id] > 0:
                _logger.warn('Deny deleting')
                continue
            else:
                _logger.warn('Grant deleting')
                super(StockPickingReturnPrepareLine, self).unlink(cr, uid, pline.id, context=context)
        return True

    # create one unverified line in batch return
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}

        _logger.warn("Create line vals: %s" % vals)

        return_id = super(StockPickingReturnPrepareLine, self).create(cr, uid, vals, context=context)
        return_obj = self.browse(cr, uid, return_id)

        # update null-lines, if the manual addition
        return_obj_vals = {}
        # product code
        if not 'sku_imported' in vals.keys() and 'product_id' in vals.keys():
            return_obj_vals.update({'sku_imported': return_obj.product_id.default_code})
        # size
        if not 'size_imported' in vals.keys() and 'size_id' in vals.keys():
            return_obj_vals.update({'size_imported': ''})

        # qty
        if not 'qty_imported' in vals.keys() and 'qty' in vals.keys():
            return_obj_vals.update({'qty_imported': vals['qty']})

        # complete name
        if return_obj.product_id and return_obj.size_id:
            return_obj_vals.update({'name': "%s / %s" % (return_obj.product_id.default_code, return_obj.size_id.name)})

        # check product is a SET
        if 'product_id' in vals:
            helper = self.pool.get('stock.picking.helper')
            return_obj_vals.update({'is_set': helper.check_product_is_set(cr, uid, vals['product_id'])})

        _logger.warn('Update line vals: %s' % return_obj_vals)

        return_obj.write(return_obj_vals)
        return return_id

    def verify_line_form(self, cr, uid, ids, context=None):
        act_win = False

        # data, window, pepare.line objects
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        line_obj = self.pool.get('stock.picking.return.prepare.line')

        if ids:
            # get prepare line object
            line = line_obj.browse(cr, uid, ids[0])
            verified_object = self.pool.get('stock.picking.return.verified.line')
            total, verified, unverified = verified_object.count_total_verified_unverified(cr, uid, line.id, context=context)

            # create new context for verification form
            loc_ids = self.pool.get('stock.location').search(cr, uid, [
                    ('warehouse_id', '=', line.batch_id.warehouse_id.id),
                    ('name', '=', 'RETURN'),
                ], limit=1, context=context)

            new_context = {
                'line_id': line.id,
                'default_line_id': line.id,
                'default_qty_verified': line.qty,
                'default_batch_id': line.batch_id.id,
                'default_product_id': line.product_id.id,
                'default_is_set': line.is_set,
                'default_warehouse_id': line.batch_id.warehouse_id.id,
                'default_return_location': False,
                'default_return_target': 'return',
            }

            # check GTA for target_attention
            ta_tpl = '<h1 style="text-align:center;color:{0};">{1}</h1>'
            if line.product_standart_price < 250:
                new_context.update({'default_target_attention': ta_tpl.format('red', 'MONTREAL')})
            elif 250 <= line.product_standart_price < 1000:
                new_context.update({'default_target_attention': ta_tpl.format('black', 'CHAMPLAIN')})
            else:
                new_context.update({'default_target_attention': ta_tpl.format('black', 'HIGH VALUE')})


            # create action window
            act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_mass_return_verify_line')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
            act_win.update({
                'res_id': None,
                'context': new_context,
            })
            return act_win
        else:
            raise osv.except_osv(_('Warning!'), _('No line selected!'))


    def picking_change(self, cr, uid, ids, picking_id=None, product_id=None, size_id=None, qty=None, qty_imported=None, context={}):
        val = {
            'picking_id': picking_id,
            'product_id': product_id,
            'size_id': context.get('size_id'),
            'size_id_delivery': context.get('size_id_delivery'),
            'qty': qty,
        }
        _logger.warn("Picking change incoming vals: %s" % val)
        domain = {}

        if picking_id and product_id:
            pick_obj = self.pool.get('stock.picking')
            pick = pick_obj.browse(cr, uid, picking_id)
            product_ids = list()
            picking_has_product = False

            for line in pick.move_lines:
                product_ids.append(line.product_id.id)
                if product_id == line.product_id.id and (context.get('size_id_delivery') == line.size_id.id or context.get('size_id') == line.sale_line_id.size_id.id) and line.product_qty >= qty:
                    picking_has_product = True
                    val['size_id'] = line.sale_line_id.size_id.id if line.sale_line_id and line.sale_line_id.size_id else False
                    val['size_id_delivery'] = line.size_id.id if line.size_id else False
                    val['sizeable'] = True if line.size_id or (line.sale_line_id and line.sale_line_id.size_id) else False
                    val['qty'] = line.product_qty
                    val['cost'] = float(line.sale_line_id.price_unit) if line.sale_line_id.price_unit else float(0)
                    break

            if picking_has_product:
                domain['product_id'] = [('product_id', 'in', tuple(product_ids))]
                val['picking_id'] = picking_id
            else:
                val['picking_id'] = None
        else:
            val['picking_id'] = None

        _logger.warn('Product DOMAIN: %s' % domain)
        _logger.warn('Product VAL: %s' % val)
        return {'value': val, 'domain': domain}

    def cost_change(self, cr, uid, ids, picking_id=None, product_id=None, size_id=None, cost=None):
        val = {
            'picking_id': picking_id,
            'cost': cost
        }
        if picking_id and product_id:
            pick_obj = self.pool.get('stock.picking')
            pick = pick_obj.browse(cr, uid, picking_id)
            for line in pick.move_lines:
                if product_id == line.product_id.id and (not size_id or size_id == line.size_id.id):
                    if not float(cost) == float(line.sale_line_id.price_unit):
                        val['picking_id'] = None
                    break

        return {'value': val}

    def product_change(self, cr, uid, ids, product_id=None, size_id=None, sizeable=False, picking_id=None, qty=None, partner_id=None, context={}):
        _logger.warn('Product change fired!: %s' % product_id)
        search_by = context.get('search_by', '')
        if (search_by == 'sale' and not context.get('size_id')) or (search_by == 'delivery' and not context.get('size_id_delivery')):
            return
        if search_by:
            size_id = context.get('size_id') if search_by == 'sale' else context.get('size_id_delivery')
        else:
            size_id = context.get('size_id') or context.get('size_id_delivery')
        val = {
            'product_id': product_id,
            'sizeable': sizeable,
            'size_id': context.get('size_id'),
            'size_id_delivery': context.get('size_id_delivery'),
            'picking_id': picking_id,
        }
        domain = {
            'picking_id': [('state', '=', 'done')],
        }
        if not product_id or product_id is None:
            return {'value': val, 'domain': domain}

        # check product is sizeable
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        tmpl_id = product.product_tmpl_id.id if product else None
        if tmpl_id:
            query = """select size_id from product_ring_size_default_rel pr WHERE pr.product_id=%(tmpl_id)s"""
            params = {
                'tmpl_id': tmpl_id,
            }
            cr.execute(query, params)
            sql_res = cr.dictfetchall() or False
            if sql_res and len(sql_res)>0:
                val['sizeable'] = True
            else:
                val['sizeable'] = False
                val['size_id'] = None
                val['size_id_delivery'] = None

        # check product size in picking
        if size_id is None and picking_id is not None:
            picking = self.pool.get('stock.picking').browse(cr, uid, picking_id)
            for line in picking.move_lines:
                if line.product_id.id == product_id:
                    val['size_id'] = line.sale_line_id.size_id.id if line.sale_line_id and line.sale_line_id.size_id else context.get('size_id')
                    val['size_id_delivery'] = line.size_id.id if line.size_id else context.get('size_id_delivery')
                    break

        # DLMR-1701
        # find product image
        val.update({'product_image': product.product_image, 'product_image_bin': product.product_image})
        # END DLMR-1701

        # check product is a SET
        helper = self.pool.get('stock.picking.helper')
        val.update({'is_set': helper.check_product_is_set(cr, uid, product_id)})

        # create new picking domain for product_id/size/qty
        if (search_by and search_by == 'delivery') or (not search_by and context.get('size_id_delivery')):
            query = """select sm.picking_id, sp.name from stock_move sm
                LEFT JOIN stock_picking sp on sp.id=sm.picking_id
                where sm.product_id=%(prod_id)s
                and sp.real_partner_id=%(partner_id)s
                and sm.product_qty>=%(qty)s
                and sm.size_id=%(size)s
                and sp.state='done'"""
        else:
            query = """select sm.picking_id, sp.name 
                from sale_order_line sol 
                inner join stock_move sm on sm.sale_line_id=sol.id
                inner join sale_order so on so.id=sol.order_id 
                inner join stock_picking sp on sp.id=sm.picking_id
                where sm.product_id=%(prod_id)s
                and sp.real_partner_id=%(partner_id)s
                and sm.product_qty>=%(qty)s
                and sol.size_id=%(size)s
                and sp.state='done'"""
        params = {
            'partner_id': partner_id,
            'prod_id': product_id,
            'qty': qty,
            'size': size_id or None
        }
        cr.execute(query, params)
        res = cr.fetchall() or []
        pickings = {x[0]: x[1] for x in res}
        picking_ids = pickings.keys()
        domain['picking_id'].append(('id', 'in', picking_ids))
        if len(picking_ids) == 0:
            val.update({'picking_id': False})
        # _logger.warn('Product return domain: %s' % domain)
        # _logger.warn('Product return values: %s' % val)
        val.update({'size_id': context.get('size_id'),
                    'size_id_delivery': context.get('size_id_delivery')})
        return {'value': val, 'domain': domain}

    def show_attachments_list(self, cr, uid, ids, context=None):
        _logger.info("Open list of attachments for line: %s" % ids)
        # data, window, pepare.line objects
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        new_context = {
            'id': ids[0],
            'default_id': ids[0],
            'active_id': ids[0],
            'form_view_ref': 'delmar_sale.view_massline_attachment_list',
        }
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_massline_attachment_list')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
        act_win.update({
            'res_id': ids[0],
            'context': new_context,
        })
        return act_win

    def show_attachments_form(self, cr, uid, ids, context=None):
        # data, window, pepare.line objects
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        new_context = {
            'prepare_line_id': ids[0],
            'form_view_ref': 'delmar_sale.view_massline_attachment_upload_form',
        }
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_massline_attachment_upload_form')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
        act_win.update({
            'context': new_context,
        })
        return act_win

StockPickingReturnPrepareLine()


class MassReturnVerifiedComponentLine(osv.osv):
    _name = "mass.return.verified.component.line"

    # DLMR-1754
    # Bins available for component bin
    def _get_bin_domain(self, cr, uid, ids, name, args, context=None):
        _logger.info("BIN DOMAIN context: %s" % context)
        res = {}
        for ln in self.browse(cr, uid, ids):
            bin_ids = []
            if ln.product_id and ln.return_location:
                bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [ln.product_id.id], [ln.return_location.id], [ln.size_id.id])
            # bin_ids.append(ln.return_bin.id)
            domain = "[('id', 'in', {})]".format(str(tuple(bin_ids)))
            _logger.info("BIN DOMAIN: %s" % domain)
            res.update({ln.id: eval(domain)})
        return res

    _columns = {
        'verified_line_id': fields.many2one('stock.picking.return.verified.line', 'Verified line', required=True, ondelete='cascade'),
        'product_id': fields.many2one('product.product', 'Component', required=True, ),
        'parent_product_id': fields.many2one('product.product', 'Parent Set', required=False, ),
        'size_id': fields.many2one('ring.size', 'Size', required=False, store=True, ),
        'qty_in_set': fields.integer('Qty In Set', required=True, readonly=False, ),
        'qty_to_return': fields.integer('Qty To Return', required=True, readonly=False, ),
        'return_location': fields.many2one('stock.location', 'Ret. Location', required=False, ),
        'return_bin_domain': fields.function(_get_bin_domain, string="Bin domain", type='char', method=True, store=False, ),
        'return_bin': fields.many2one('stock.bin', 'Ret. Bin', ),
    }
    _defaults = {
        'qty_in_set': 1,
        'qty_to_return': 1,
    }

    def on_change_location(self, cr, uid, ids, location_id=False):
        ln = self.browse(cr, uid, ids[0])
        bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [ln.product_id.id], [location_id], [ln.size_id.id])
        domain = {
            'return_bin': [('id', 'in', bin_ids)]
        }
        return {'domain': domain, 'value': {'return_bin': False}}


MassReturnVerifiedComponentLine()


class StockPickingReturnVerifiedLine(osv.osv):
    _name = "stock.picking.return.verified.line"

    def _get_size(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.line_id \
                              and (line.line_id.size_id.name if name == 'size' else line.line_id.size_id_delivery.name)\
                              or False
        return res

    def _get_order(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.line_id and line.line_id.picking_id.name or False
        return res

    def _get_customer_sku(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            prod_obj = self.pool.get('product.product')
            sku = prod_obj.get_product_customer_sku(cr, uid, line.batch_id.partner_id.id, line.product_id.id, line.size_id.id, context=context)
            res[line['id']] = sku or None
        return res

    def _count_unverified(self, cr, uid, ids, name, args, context=None):
        res = {}
        _logger.warn('Got IDS: %s' % ids)
        for line in self.browse(cr, uid, ids, context=context):
            _logger.warn("Line data: %s" % line)
            total, verified, unverified = self.count_total_verified_unverified(cr, uid, line.line_id.id, context=context)
            _logger.warn("Confirmation counts for line %s: %s %s %s" % (line['id'], total, verified, unverified))
            res[line['id']] = unverified or 0
        return res

    def _get_complete_src_loc_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        for batch_line in self.browse(cr, uid, ids, context=context):
            if batch_line:

                r_location = batch_line.return_location
                r_bin = batch_line.return_bin

                if r_location and r_bin:
                    complete_source_location = '%s    bin: %s' % (r_location and r_location.complete_name and re.sub('^.*?/(.*)', '\\1', r_location.complete_name), r_bin and r_bin.name)
                else:
                    complete_source_location = 'Suitable stock ID is not found for chosen Warehouse!'

                res[batch_line['id']] = complete_source_location

        return res

    def _check_confirm_granted(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        #_logger.warn("Check granted context: %s" % context)
        for line in self.browse(cr, uid, ids, context=context):
            if line.qty_verified <= 0 or not line.product_id or not line.return_location or not line.return_bin or line.cost == 0 or not line.cost or (line.sizeable and not line.size_id):
                res[line['id']] = False
            else:
                # check components lines
                if line.set_components_line_ids:
                    cmp_set = True
                    for cmp_line in line.set_components_line_ids:
                        if not cmp_line.product_id or not cmp_line.return_location or not cmp_line.return_bin:
                            cmp_set = False
                    res[line['id']] = cmp_set
                else:
                    res[line['id']] = True
        return res

    def _check_bulk_confirm_granted(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        #_logger.warn("Check granted context: %s" % context)
        for line in self.browse(cr, uid, ids, context=context):
            if line.is_set:
                res[line['id']] = False
            else:
                res[line['id']] = True
        return res

    def _damaged(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.line_id.damaged
        return res

    def _not_delmar_product(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.line_id.not_delmar_product
        return res

    _columns = {
        'line_id': fields.many2one('stock.picking.return.prepare.line', 'Prepared line', required=True, ),
        'batch_id': fields.many2one('stock.picking.mass.return', 'Mass Return Batch', required=True, ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=True, ),
        'is_set': fields.related(
            'line_id', 'is_set',
            type='boolean', string='Set', store=False,
        ),
        'set_components_line_ids': fields.one2many('mass.return.verified.component.line', 'verified_line_id', 'Phantom set verified lines', domain=[], ),
        'customer_sku': fields.function(_get_customer_sku, string='SKU', type='char', method=True, store=False, ),
        'size': fields.function(_get_size, string="Sale Size", type='char', method=True, store=False, ),
        'size_delivery': fields.function(_get_size, string="Delivery Size", type='char', method=True, store=False, ),
        'sizeable': fields.related(
            'line_id', 'sizeable',
            type='boolean', string='Sizeable', store=False,
        ),
        'warehouse_id': fields.related(
            'batch_id', 'warehouse_id',
            type='many2one', string='Warehouse', store=True,
            relation='stock.warehouse',
        ),
        'order': fields.function(_get_order, string="Order", type='char', method=True, store=False, ),
        'picking_id': fields.related(
            'line_id', 'picking_id',
            type='many2one', string='Picking', store=True,
            relation='stock.picking',
        ),
        'size_id': fields.related(
            'line_id', 'size_id',
            type='many2one', string='SizeID', store=True,
            relation='ring.size',
        ),
        'invoice_no': fields.related(
            'picking_id', 'invoice_no',
            type='char', string='Invoice#', store=False,
        ),
        'cost': fields.related(
            'line_id', 'cost',
            type='float', string='Cost', store=False,
        ),

        'qty_verified': fields.integer('Qty Verified', required=True, readonly=False, ),
        'qty_unverified': fields.function(_count_unverified, string="Qty Unverified", type="integer", store=False, ),
        'return_target': fields.selection([
            ('return', 'Return Location'),
            ('source', 'Return to Source Location'),
            ('selected', 'Return to Selected'),
            ('mntl', 'Return to MNTL'),
        ], 'Return Target', required=True, ),
        'return_location': fields.many2one('stock.location', 'Ret. Location', required=False, ),
        'return_bin': fields.many2one('stock.bin', 'Ret. Bin', ),
        'src_location_id': fields.many2one('stock.location', 'Location',
            domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],  # , ('warehouse_id', '=', 'warehouse_id')
        ),
        'src_bin_id': fields.many2one('stock.bin', 'Bin', ),
        'generate_bin': fields.boolean('Generate Bin(s)', required=False),
        'custom_bin': fields.char('or type a custom bin name', size=32, required=False, store=False),
        'complete_source_location': fields.function(_get_complete_src_loc_name, string="Source Location", type='char', method=True, store=False, ),
        'components_src_locations': fields.char("Components locations", size=256, store=False, required=False ),
        'components_complete_location': fields.char('Set source location', size=128, required=False, store=False),
        'state': fields.selection([
            ('verified', 'Verified'),
            ('confirmed', 'Confirmed'),
        ], 'State', ),
        'target_attention': fields.dummy(string="Dummy field 01", store=False, ),
        'use': fields.boolean('Use'),
        'grant_confirm': fields.function(_check_confirm_granted, string="Confirmation granted", type="boolean", store=False, ),
        'grant_bulk_confirm': fields.function(_check_bulk_confirm_granted, string="Bulk confirmation granted", type="boolean", store=False, ),
        'damaged': fields.function(_damaged, string="Dmg item", type="boolean", store=False, ),
        'not_delmar_product': fields.function(_not_delmar_product, string="Not Delmar", type="boolean", store=False, ),
    }
    _defaults = {
        'state': 'verified',
        'qty_verified': 0,
    }

    def create(self, cr, uid, vals, context=None):
        if 'generate_bin' not in vals:
            vals['generate_bin'] = False
        if 'return_bin' not in vals:
            vals['return_bin'] = False
        if 'return_location' not in vals:
            vals['return_location'] = False

        _logger.warn("Start create vals: %s" % vals)
        _logger.warn("Start create context: %s" % context)

        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        line_obj = self.pool.get('stock.picking.return.prepare.line')
        helper = self.pool.get('stock.picking.helper')
        cmp_obj = self.pool.get('product.set.components')

        line = line_obj.browse(cr, uid, context.get('line_id'))
        line_state = {}

        # check verified counts
        total, verified, unverified = self.count_total_verified_unverified(cr, uid, context.get('line_id'), context=context)

        # Variable qty - is it needed or not ?
        if vals['qty_verified'] > unverified:
            raise osv.except_osv(_('Error!'), _('Summary verification quantity can\'t be greater than full line quantity!'))

        # Raise error, if zero-qty
        if vals['qty_verified'] == 0:
            raise osv.except_osv(_('Error!'), _('Can\'t verify zero quantity'))

        # -------------------------
        # PRODUCT IS SET operations
        # -------------------------
        if line.is_set:
            # empty lists for components and parent bin value
            components_data = []
            parent_bin_data = []
            # check source location was selected
            # and vals already had components data
            if vals.get('return_target') == 'source' and vals.get('components_src_locations', False):
                src_components = json.loads(str(vals.get('components_src_locations')))
                for cmp_line in src_components:
                    cmp_qty = helper.get_component_set_qty(cr, uid, cmp_id=cmp_line.get('product_id'), product_id=line.product_id.id)
                    cmp_line_vals = {
                        'verified_line_id': None,
                        'product_id': cmp_line.get('product_id'),
                        'size_id': cmp_line.get('size_id', False) or line.size_id.id,
                        'parent_product_id': line.product_id.id,
                        'qty_in_set': cmp_qty,
                        'qty_to_return': cmp_qty * vals.get('qty_verified'),
                        'return_location': cmp_line.get('location_id', False),
                        'return_bin': cmp_line.get('bin_id', False)
                    }
                    # append line into list
                    components_data.append(cmp_line_vals)
                    # for parent bin data
                    bin_data = bin_obj.read(cr, uid, cmp_line_vals.get('return_bin', False), ['name'])
                    parent_bin_data.append({
                        'qty': cmp_qty,
                        'default_code': cmp_line.get('product_name'),
                        'bin': bin_data.get('name')
                    })

            # when no source data was set
            else:
                components = helper.get_set_product_components(cr, uid, line.product_id.id)
                if len(components) == 0:
                    _logger.warn('No components found for set %s' % line.product_id.default_code)
                    return False
                # list of components to add for verified line
                for cmp in components:
                    # inital return data for component
                    cmp_line_vals = {
                        'verified_line_id': None,
                        'product_id': cmp.get('product_id'),
                        'size_id': cmp.get('size_id', False) or line.size_id.id if cmp.get('sizeable') else False,
                        'parent_product_id': line.product_id.id,
                        'qty_in_set': cmp.get('qty'),
                        'qty_to_return': cmp.get('qty') * vals.get('qty_verified'),
                        'return_location': vals.get('return_location', False),
                        'return_bin': vals.get('return_bin', False),
                    }
                    # vals for return locations check
                    cmp_return_vals = {
                        'return_target': vals.get('return_target', False),
                        'return_location': vals.get('return_location', False),
                        'return_bin': vals.get('return_bin', False),
                        'src_location_id': vals.get('src_location_id', False),
                        'src_bin_id': vals.get('src_bin_id', False),
                        'generate_bin': vals.get('generate_bin', False),
                        'custom_bin': vals.get('custom_bin', False),
                        'warehouse_id': vals.get('warehouse_id', False),
                        'product_id': cmp.get('product_id'),
                        'size_id': cmp_line_vals['size_id'],
                    }
                    # recalculate return data for component
                    cmp_return_vals = self.calculate_return_location_bin(cr, uid, line=line, vals=cmp_return_vals, context=context)
                    _logger.info("COMPONENT VALS FOR RETURN: %s" % cmp_return_vals)
                    # new component line data
                    cmp_line_vals.update({
                        'return_location': cmp_return_vals.get('return_location', False),
                        'return_bin': cmp_return_vals.get('return_bin', False),
                    })
                    # append line into list
                    components_data.append(cmp_line_vals)
                    # for parent bin data
                    if cmp_line_vals.get('return_bin', False):
                        bin_data = bin_obj.read(cr, uid, cmp_line_vals.get('return_bin', False), ['name'])
                        parent_bin_data.append({
                            'qty': cmp.get('qty'),
                            'default_code': cmp.get('default_code'),
                            'bin': bin_data.get('name')
                        })

            # Find or create virtual bin for parent product
            if len(parent_bin_data) > 0 and vals['return_target'] in ['source', 'selected']:
                vals.update({'return_location': components_data[0].get('return_location')})
                if not vals.get('custom_bin', False):
                    parent_bin_names = []
                    for bin_line in parent_bin_data:
                        parent_bin_names.append(str(bin_line.get('qty')) + 'x' + bin_line.get('default_code') + '-' + bin_line.get('bin'))
                    parent_bin = '~'.join(parent_bin_names)
                    bin_id = bin_obj.get_bin_id_by_name(cr, uid, parent_bin)
                    if bin_id:
                        vals['return_bin'] = bin_id
                    else:
                        vals['return_bin'] = bin_obj.create(cr, uid, {'name': parent_bin})
                else:
                    vals.update({'return_bin': components_data[0].get('return_bin')})
            else:
                vals.update({'return_location': components_data[0].get('return_location')})
                vals.update({'return_bin': components_data[0].get('return_bin')})
                _logger.warn('No parent location/bin was set for set %s' % line.product_id.default_code)

        # -------------------------
        # COMMON PRODUCT operations
        # -------------------------
        else:
            # if the product is common (not set) - calculate correct return location/bin
            vals = self.calculate_return_location_bin(cr, uid, line=line, vals=vals, context=context)

        # ------------------
        # DEFAULT operations
        # ------------------
        # calculate line state based on verified qty
        if verified + int(vals['qty_verified']) == total:
            line_state.update({'state': 'verified'})
        else:
            line_state.update({'state': 'partial'})

        # update Prepared line state and qty
        line.write({
            'state': line_state['state'],
            'qty': unverified - vals.get('qty_verified')
        })

        # create new verified line object
        _logger.warn("Create verified line vals: %s" % vals)
        new_vline_id = super(StockPickingReturnVerifiedLine, self).create(cr, uid, vals, context=context)

        # re-check if is_set and create set components lines
        if line.is_set and new_vline_id:
            if 'components_data' in locals() and len(components_data) > 0:
                for cmp_line_vals in components_data:
                    cmp_line_vals.update({'verified_line_id': new_vline_id})
                    # if there was a problem with creating set component line - unlink parent line and return
                    if not self.pool.get('mass.return.verified.component.line').create(cr, uid, cmp_line_vals, context=None):
                        self.unlink(cr, uid, [new_vline_id])
                        return False
            # if no set components were found for product - unlink verified line
            else:
                self.unlink(cr, uid, [new_vline_id])
                _logger.warn("Return location was not correctly set, deleting line, rolling back!")
                return False

        # return new object id
        return new_vline_id

    def calculate_return_location_bin(self, cr, uid, line=None, vals=None, context=None):
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        ret_wh = line.batch_id.warehouse_id
        return_wh = 'usreturn'
        if (ret_wh.name in ('CAFER', 'CAMTL')):
            return_wh = 'careturn'

        # prepare default location/bin on create
        if not vals['return_location']:
            return_location_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('complete_name', 'ilike', '%% %s %% stock %%/ return' % (return_wh))], limit=1)
            if return_location_ids:
                r_location = loc_obj.browse(cr, uid, return_location_ids[0])
                vals['return_location'] = r_location.id or None
        if not vals['return_bin'] and not vals['generate_bin'] and not vals['custom_bin']:
            return_bin_ids = bin_obj.search(cr, uid, [('name', '=', 'RETURN')], limit=1)
            if return_bin_ids:
                r_bin = bin_obj.browse(cr, uid, return_bin_ids[0])
                vals['return_bin'] = r_bin.id or None

        # check is source location selected, and BIN was found for return
        if vals['return_target'] == 'source':
            _logger.warn("Source ret vals:  %s" % vals)
            _logger.warn("Source ret context:  %s" % context)
            if vals['src_location_id'] and vals['src_bin_id']:
                vals['return_location'] = vals['src_location_id']
                vals['return_bin'] = vals['src_bin_id']
            else:
                # if bulk operation and no source location/bin configured previously
                #ret_vals = self.target_change(cr, uid, ids, line.batch_id.id, ret_wh.id, line.line_id.id, vals['product_id'], vals['qty_verified'], vals['return_target'], context=None)
                ret_vals = self.pool.get('stock.picking.mass.return.line').param_change(cr, uid, [], product_id=vals['product_id'], size_id=vals['size_id'], partner_id=line.batch_id.partner_id.id, picking_id=line.picking_id.id, qty=line.qty_verified, batch_id=line.batch_id.id, src_location_id=False, external_customer_order_id=False, return_target=vals['return_target'], search_picking_domain=False, context=context).get('value', False)
                _logger.warn('Alternate source location search: %s' % ret_vals)
                if ret_vals and ret_vals.get('src_location_id', False) and ret_vals.get('src_bin_id', False):
                    vals['return_location'] = ret_vals['src_location_id']
                    vals['return_bin'] = ret_vals['src_bin_id']
                else:
                    line.write({'comments': "%s %s;" % (line.comments or None, 'Source location was not found!')})
                    vals['return_location'] = False
                    vals['return_bin'] = False
                    # raise osv.except_osv('ERROR!', 'Can\'t find source location for item %s' % line.product_id.default_code)

        # return to selected custom bin
        elif vals['return_target'] == 'selected':
            _logger.warn("Selected line vals: %s" % vals)
            if vals['return_location'] and vals['custom_bin'] and vals['custom_bin'] is not None:
                if len(vals.get('custom_bin', '').strip()) == 0:
                    raise osv.except_osv('Critical', 'Invalid bin name! Bin name can\'t be empty')
                _logger.warn('Return line to custom bin: %s' % vals['custom_bin'])
                # set location
                # vals['return_location'] = vals['src_location_id']
                # check bin is already exists
                bin_id = bin_obj.get_bin_id_by_name(cr, uid, vals['custom_bin'])
                if bin_id:
                    vals['return_bin'] = bin_id
                else:
                    # create dummy bin ?????
                    new_bin_vals = {
                        'name': vals['custom_bin']
                    }
                    vals['return_bin'] = bin_obj.create(cr, uid, new_bin_vals)
                    if vals['return_bin']:
                        vals.pop('custom_bin', None)
            # If no BIN was found for return to location and need to generate bin
            elif vals['return_location'] and vals['generate_bin'] and not vals['return_bin'] and not vals['custom_bin']:
                # first try to find existing bins
                if line.is_set:
                    bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [vals['product_id']], [vals['return_location']], [vals.get('size_id', False)])
                else:
                    bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [line.product_id.id],
                                                                                      [vals['return_location']],
                                                                                      [line.size_id and line.size_id.id])
                if bin_ids:
                    _logger.info("Found bin_id in MSSQL: %s" % bin_ids[0])
                    vals['return_bin'] = bin_ids[0]
                else:
                    _logger.warn("Need to generate bin")
                    bin_cr_obj = self.pool.get('stock.bin.create')
                    loc = loc_obj.browse(cr, uid, vals['return_location'])
                    loc_nm = loc_obj.read(cr, uid, vals['return_location'], ['warehouse_id'])
                    _logger.warn("READ LOCATION OBJ: %s" % loc_nm)
                    whs_name = loc_nm.get('warehouse_id', False) and loc_nm.get('warehouse_id', False)[0]

                    bin_cr_vals = {
                        'warehouse_id': whs_name or ret_wh.id,
                        'location_id': vals['return_location'],
                        'location': loc.name or None,
                        'product_id': vals.get('product_id') if 'product_id' in vals else line.product_id.id,
                        'size_id': vals.get('size_id') if 'size_id' in vals else line.size_id.id,
                        'qty_capacity': 0
                    }
                    _logger.warn("Try to create bin with vals: %s" % bin_cr_vals)
                    bin_cr_id = bin_cr_obj.create(cr, uid, bin_cr_vals)
                    bin_n = bin_cr_obj.browse(cr, uid, bin_cr_id)
                    _logger.warn("Created bin id, name: %s, %s" % (bin_cr_id, bin_n.name))
                    # check location
                    if bin_cr_id and bin_cr_obj.check_location(cr, uid, bin_cr_id):
                        _logger.warn('CR_BIN_ID AND LOCATION CHECKED')
                        # check bin
                        if bin_cr_obj.check_bin(cr, uid, bin_cr_id):
                            _logger.warn('BIN CHECKED')
                            bin_cr = bin_cr_obj.browse(cr, uid, bin_cr_id)
                            _logger.warn("New bin: %s" % bin_cr.name)
                            bin_obj = self.pool.get('stock.bin')
                            vals['return_bin'] = bin_obj.get_bin_id_by_name(cr, uid, bin_cr.name)
                        else:
                            raise osv.except_osv(
                                _('Warning!'),
                                'Can\'t resolve new BIN for selected warehouse/location',
                            )
                    else:
                        raise osv.except_osv(
                            _('Warning!'),
                            'Invalid location',
                        )
            elif vals['return_location'] and vals['return_bin']:
                _logger.warn("Return to selected location/bin: %s" % vals)
            else:
                raise osv.except_osv(_('Warning!'), 'No location/bin specified for return')

        # check is mntl location selected
        elif vals['return_target'] == 'mntl':
            return_location_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('complete_name', 'ilike', '%% %s %% stock %% mntlreturn' % (return_wh))], limit=0)
            if return_location_ids:
                r_location = loc_obj.browse(cr, uid, return_location_ids[0])
                vals['return_location'] = r_location.id
                # create 'date' bin for MNTL return
                date_bin_name = time.strftime('%m-%d-%y', time.gmtime()),
                _logger.warn('MNTL return requested, need to create date-like-bin %s' % date_bin_name)
                # check bin is already exists
                bin_id = bin_obj.get_bin_id_by_name(cr, uid, date_bin_name[0])
                if bin_id:
                    vals['return_bin'] = bin_id
                else:
                    # create dummy bin ?????
                    new_bin_vals = {
                        'name': date_bin_name[0]
                    }
                    vals['return_bin'] = bin_obj.create(cr, uid, new_bin_vals)

        return vals

    def count_total_verified_unverified(self, cr, uid, line_id, context=None):
        line_obj = self.pool.get('stock.picking.return.prepare.line')
        line = line_obj.browse(cr, uid, line_id, context=context)
        # total for verification
        total = int(line.qty_imported)
        # count verified
        count_sql = """select sum(rv.qty_verified) from stock_picking_return_verified_line rv where rv.line_id=%s""" % line.id
        cr.execute(count_sql)
        count_res = cr.fetchone()
        # set verified/unverified
        if count_res and count_res[0]:
            verified = int(count_res[0])
            unverified = total - verified
        else:
            verified = 0
            unverified = total
        # return list
        return total, verified, unverified

    def verify_line(self, cr, uid, ids, context=None):
        _logger.warn("VerifyLine IDS: %s" % ids)
        _logger.warn("VerifyLine Context: %s" % context)
        res = {'type': 'ir.actions.act_window_close'}
        return res

    def verify_lines(self, cr, uid, ids, context=None):
        _logger.warn("Verify LINES IDS: %s" % ids)
        _logger.warn("Verify LINES CONTEXT: %s" % context)
        raise osv.except_osv(_('Warning!'), _("Not ready yet!"))

    def reject_line(self, cr, uid, ids, context=None):
        self.unlink(cr, uid, ids, context)
        return {}

    def open_set_components(self, cr, uid, ids, context=None):
        _logger.warn("OPEN COMPONENTS IDS: %s" % ids)
        _logger.warn("OPEN COMPONENTS context: %s" % context)
        act_win = False
        # data, window, pepare.line objects
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        # new context
        new_context = {
            'line_id': ids[0],
        }
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_mass_return_verify_line_components')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
        act_win.update({
            'res_id': ids[0],
            'context': new_context,
        })
        return act_win

    def unlink(self, cr, uid, ids, context=None):
        _logger.warn("Unverifying lines: %s" % ids)
        _logger.warn("Unverifying context: %s" % context)
        context.update({'unlink_verified': True})
        for vline in self.browse(cr, uid, ids, context=context):
            pline = vline.line_id
            total, verified, unverified = self.count_total_verified_unverified(cr, uid, pline.id, context=context)
            line_state = {}
            line_state.update({'qty': pline.qty + vline.qty_verified})
            if verified - int(vline.qty_verified) > 0:
                _logger.warn('Change state to PARTIAL')
                line_state.update({'state': 'partial'})
            else:
                _logger.warn('Change state to DRAFT')
                line_state.update({'state': 'draft'})

            pline.write(line_state)
        return super(StockPickingReturnVerifiedLine, self).unlink(cr, uid, ids, context=context)

    def confirm_line(self, cr, uid, ids, context=None):
        _logger.warn("Confirmation IDS: %s" % ids)
        _logger.warn("Confirmation context: %s" % context)

        verified_lines = self.browse(cr, uid, ids, context=context)
        confirmed_lines = []

        for vline in verified_lines:
            if vline.grant_confirm:
                _logger.warn("Confirmation line: %s" % vline)
                return_vals = {
                    'batch_id': vline.batch_id.id,
                    'product_id': vline.product_id.id,
                    'size_id': vline.line_id.size_id.id,
                    'size_id_delivery': vline.line_id.size_id_delivery.id,
                    'sizeable': vline.line_id.sizeable,
                    'qty': vline.qty_verified,
                    'cost': vline.line_id.cost,
                    'picking_id': vline.line_id.picking_id.id,
                    'partner_id': vline.line_id.partner_id.id,
                    'return_target': 'source' if vline.return_target == 'selected' else vline.return_target,
                    'target_location_id': vline.return_location.id,
                    'target_bin_id': vline.return_bin.id,
                    'verified_line_id': vline.id,
                    'is_set': vline.is_set,
                }
                _logger.warn("New return line vals: %s" % return_vals)

                ret_obj = self.pool.get('stock.picking.mass.return.line')
                return_id = ret_obj.create(cr, uid, return_vals, context=context)
                # change source line state
                if return_id:
                    vline.write({'state': 'confirmed'})
                    confirmed_line = ret_obj.read(cr, uid, return_id, ['id'])
                    _logger.info("Got confirmed line: %s" % confirmed_line)
                    confirmed_lines.append(int(confirmed_line.get('id')))

        if len(confirmed_lines) > 0:
            if len(ids) > 1 and context.get('selected_ids', False):
                _logger.info('Confirmed (returned) lines to print: %s' % confirmed_lines)
                return confirmed_lines
            else:
                _logger.info('Confirmed line to autoprint: %s' % confirmed_lines)
                return self.pool.get('stock.picking.mass.return.line').print_qr_label(cr, uid, confirmed_lines)
        return True

    def location_change(self, cr, uid, ids, product_id=False, size_id=False, return_location=False, return_bin=False, return_target=False, context=None):
        return self.pool.get('stock.picking.mass.return.line').location_change(cr, uid, ids, product_id=product_id, size_id=size_id, return_location=return_location, return_bin=return_bin, return_target=return_target, context=context)

    def param_change_on_edit(self, cr, uid, ids, batch_id, warehouse_id, line_id, product_id, qty_verified, return_target, context=None):
        verified = self.browse(cr, uid, ids[0])
        _logger.warn("Param change context: %s" % context)
        return self.pool.get('stock.picking.mass.return.line').param_change(cr, uid, ids, product_id=product_id, size_id=verified.line_id.size_id.id, partner_id=verified.line_id.partner_id.id, picking_id=verified.line_id.picking_id.id, qty=qty_verified, cost=verified.line_id.cost, batch_id=batch_id, src_location_id=verified.src_location_id.id, external_customer_order_id=False, return_target=return_target, context=context)

    def target_change(self, cr, uid, ids, batch_id, warehouse_id, line_id, product_id, qty_verified, return_target, context=None):
        if return_target in ['selected', 'source']:
            prepare = self.pool.get('stock.picking.return.prepare.line').browse(cr, uid, line_id)
            return self.pool.get('stock.picking.mass.return.line').param_change(cr, uid, ids, product_id=product_id, size_id=prepare.size_id.id, partner_id=prepare.partner_id.id, picking_id=prepare.picking_id.id, qty=qty_verified, cost=prepare.cost, batch_id=batch_id, src_location_id=False, external_customer_order_id=False, return_target=return_target, search_picking_domain=False, context=context)
        else:
            return {}

    def show_attachments_list(self, cr, uid, ids, context=None):
        _logger.info("Open list of attachments for Verified line: %s" % ids)
        prepare_obj = self.pool.get('stock.picking.return.prepare.line')
        verified_line = self.browse(cr, uid, ids[0])
        if verified_line and verified_line.line_id:
            return prepare_obj.show_attachments_list(cr, uid, [verified_line.line_id.id])
        else:
            return True

    # DLMR-1754
    # Save changed bins
    def update_components(self, cr, uid, ids, context=None):
        ln = self.browse(cr, uid, ids[0])
        if ln.is_set:
            bin_obj = self.pool.get('stock.bin')
            parent_bin_names = []
            for cmp in ln.set_components_line_ids:
                parent_bin_names.append("{}x{}-{}".format(cmp.qty_in_set, cmp.product_id.default_code, cmp.return_bin.name))
            parent_bin = '~'.join(parent_bin_names)
            _logger.info("New parent bin: %s" % parent_bin)
            bin_id = bin_obj.get_bin_id_by_name(cr, uid, parent_bin)
            if not bin_id:
                bin_id = bin_obj.create(cr, uid, {'name': parent_bin})
            if bin_id:
                self.write(cr, uid, ln.id, {'return_bin': bin_id})
        return {'type': 'ir.actions.act_window_close'}


StockPickingReturnVerifiedLine()


class StockPickingMassReturnLine(osv.osv):
    _name = "stock.picking.mass.return.line"

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for line in self.browse(cr, uid, ids, context=context):
            res.append((line.id, "%s/%s: %s" % (
                line.batch_id and line.batch_id.name or 'N/A',
                line.product_id and line.product_id.default_code or 'N/A',
                line.size_id and line.size_id.name or '0',
            )))
        return res

    def _get_image(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.product_id and line.product_id.product_image or False
        return res

    def _get_complete_src_loc_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        for batch_line in self.browse(cr, uid, ids, context=context):
            if batch_line:

                r_location = batch_line.target_location_id
                r_bin = batch_line.target_bin_id

                if r_location and r_bin:
                    complete_source_location = '%s    bin: %s' % (r_location and r_location.complete_name and re.sub('^.*?/(.*)', '\\1', r_location.complete_name), r_bin and r_bin.name)
                else:
                    complete_source_location = 'Suitable stock ID is not found for chosen Warehouse!'

                res[batch_line['id']] = complete_source_location

        return res

    def _damaged(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if not line.verified_line_id:
                res[line['id']] = False
                continue
            cr.execute("""
                SELECT damaged
                FROM stock_picking_return_prepare_line
                WHERE id = %s
                    """, (line.verified_line_id.line_id.id,))

            data = cr.fetchone()
            result = False
            if data:
                result = data[0]
            res[line['id']] = result
        return res

    def _not_delmar_product(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if not line.verified_line_id:
                res[line['id']] = False
                continue
            cr.execute("""
                SELECT not_delmar_product
                FROM stock_picking_return_prepare_line
                WHERE id = %s
                    """, (line.verified_line_id.line_id.id,))

            data = cr.fetchone()
            result = False
            if data:
                result = data[0]
            res[line['id']] = result
        return res

    _columns = {
        'create_date': fields.datetime('Date', readonly=True, select=1),
        'batch_id': fields.many2one('stock.picking.mass.return', 'Mass Return', ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'verified_line_id': fields.many2one('stock.picking.return.verified.line', 'Verified line'),
        'product_image': fields.function(_get_image, string="Image", type='binary', method=True, store=False, ),
        'size_id': fields.many2one('ring.size', 'Sale Size',
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'size_id_delivery': fields.many2one('ring.size', 'Delivery Size',
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'sizeable': fields.boolean('Sizeable', ),
        'customer_sku': fields.char('Customer SKU', size=256,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'external_line_id': fields.char('Line ID', size=256,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'qty': fields.integer('Qty', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'cost': fields.float('Cost',
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'picking_id': fields.many2one('stock.picking', 'Order',
            domain="[('state', '=', 'done')]", required=False,
            readonly=True, states={'draft': [('readonly', False)]},
            help="Please, select delivery order to process return for it."),
        'external_number': fields.char('Customer SKU', size=256,),
        'external_customer_order_id': fields.char('External Order ID', size=256, ),
        'partner_id': fields.many2one('res.partner', "Partner",
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'return_target': fields.selection([
            ('return', 'Return Location'),
            ('source', 'Source Location'),
            ('selected', 'Selected Location'),
            ('mntl', 'Return to MNTL'),
            ], 'Return Location', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'location_id': fields.many2one('stock.location', 'Location', ),
        'target_location_id': fields.many2one('stock.location', 'Location', ),
        'target_bin_id': fields.many2one('stock.bin', 'Bin', ),
        'fixed_return_target': fields.boolean('Fixed Target', ),
        'state': fields.selection([
            ('draft', 'New'),
            ('done', 'Done'),
            ], 'State', ),
        # 'complete_source_location': fields.char('Source Location', size=256,
        #     readonly=False,),
        'src_location_id': fields.many2one('stock.location', 'Location',
            domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],
        ),
        'src_bin_id': fields.many2one('stock.bin', 'Bin', ),
        'complete_source_location': fields.function(_get_complete_src_loc_name,
            string="Source Location", type='char', method=True, store=False,
        ),
        'invoice_no': fields.related(
            'picking_id', 'invoice_no',
            type='char', string='Invoice#', store=False,
        ),
        'ret_invoice_no': fields.related(
            'picking_id', 'ret_invoice_no',
            type='char', string='Retern Invoice#', store=False,
        ),
        'is_set': fields.boolean('Is set', ),
        'damaged': fields.function(_damaged, string="Dmg item", type="boolean", method=True, store=False, ),
        'not_delmar_product': fields.function(_not_delmar_product, string="Not Delmar", type="boolean", method=True, store=False, ),
    }

    _defaults = {
        'fixed_return_target': False,
        'return_target': 'return',
        'state': 'draft',
        'sizeable': False,
    }

    _order = "create_date DESC"

    def product_id_change(self, cr, uid, ids, product_id, size_id=False, partner_id=False, picking_id=False, qty=False, cost=False, batch_id=False, src_location_id=False, external_customer_order_id=None, return_target=None, context=None):
        res = self.param_change(
            cr, uid, ids, product_id, size_id=size_id,
            partner_id=partner_id,
            picking_id=picking_id,
            qty=qty, cost=cost,
            batch_id=batch_id,
            src_location_id=src_location_id,
            external_customer_order_id=external_customer_order_id,
            return_target=return_target,
            context=context
            )
        updated_vals = {
            'fixed_return_target': False,
            'location_id': False,
            'is_set': False,
        }
        if product_id:
            product = self.pool.get('product.product').read(cr, uid, product_id, ['default_code', 'set_component_ids'])
            updated_vals.update(is_set=bool(product['set_component_ids']))

            server_obj = self.pool.get('fetchdb.server')
            server_id = server_obj.get_sale_servers(cr, uid, single=True)

            if server_id:
                sql_check_stock = """   SELECT DISTINCT t.WAREHOUSE, t.STOCKID
                                        FROM dbo.transactions t
                                        WHERE 1=1
                                            AND t.ID_DELMAR = ?
                                            AND status != 'Posted'
                                        GROUP BY t.ID_DELMAR, t.WAREHOUSE, t.STOCKID
                                        ;
                """
                params = [product['default_code']]
                sql_res = server_obj.make_query(cr, uid, server_id, sql_check_stock, params=params, select=True, commit=False)
                if sql_res:

                    locations = ["Physical Locations / %s / Stock / %s" % (x[0], x[1]) for x in sql_res if x]
                    cr.execute("""  SELECT rs.location_dest_id
                                    --  , rs.id, rs.cost, rs.priority, sl.complete_name as check_location, sld.complete_name as dest_location  -- debug sql
                                    FROM stock_picking_return_settings rs
                                        LEFT JOIN stock_location sl ON rs.location_id = sl.id
                                    --  LEFT JOIN stock_location sld on rs.location_dest_id = sld.id  -- debug sql
                                    WHERE 1=1
                                        AND sl.usage = 'internal'
                                        AND sl.complete_name in %s
                                        AND COALESCE(rs.cost, 0) <= %s
                                    ORDER BY
                                        rs.priority DESC,
                                        COALESCE(rs.cost, 0) DESC,
                                        COALESCE(rs.location_dest_id, 0) DESC
                                    ;
                        """, (tuple(locations), cost or 0, )
                    )
                    res_check_settings = cr.fetchone()
                    if res_check_settings:
                        updated_vals['return_target'] = 'mntl'
                        updated_vals['fixed_return_target'] = True
                        updated_vals['location_id'] = res_check_settings[0]

        if res.get('value', None):
            res['value'].update(updated_vals)

        return res

    def param_change(self, cr, uid, ids,
                     product_id=False,
                     size_id=False,
                     partner_id=False,
                     picking_id=False,
                     qty=False,
                     cost=False,
                     batch_id=False,
                     src_location_id=False,
                     external_customer_order_id=None,
                     return_target=None,
                     search_picking_domain=True,
                     context=None):

        batch = self.pool.get('stock.picking.mass.return').browse(cr, uid, batch_id)
        ret_wh = batch.warehouse_id
        r_location_id = None

        val = {
            'product_id': product_id,
            'size_id': size_id,
            'picking_id': False,
            'sizeable': False,
            'product_image': False,
            'return_location': False,
            'return_bin': False,
            'src_location_id': False,
            'src_bin_id': False,
        }
        if ret_wh.name in ('USCHP', 'USRETURN'):
            loc_domain = [
                ('usage', '=', 'internal'),
                '|',
                    ('complete_name', 'ilike', 'USRETURN / Stock /'),
                    ('complete_name', 'ilike', 'USCHP / Stock /'),
            ]
        else:
            loc_domain = [
                ('usage', '=', 'internal'),
                ('complete_name', 'ilike', '%s / Stock /' % (ret_wh.name)),
            ]
        domain = {
            'picking_id': [('real_partner_id', '=', partner_id), ('state', '=', 'done')],
            #'src_location_id': loc_domain,
        }

        if product_id and partner_id and search_picking_domain:
            product = self.pool.get('product.product').read(cr, uid, product_id, ['ring_size_ids', 'product_image'])
            size_ids = product['ring_size_ids']
            if not size_ids or size_id not in size_ids:
                val['size_id'] = False
            val['sizeable'] = bool(size_ids)
            #val['product_image'] = product['product_image']

            where_sql = ""
            if size_id:
                where_sql += " AND sol.size_id = %(size_id)s \n"
            else:
                where_sql += " AND sol.size_id IS NULL \n"

            if cost:
                where_sql += " AND sol.price_unit = %(cost)s \n"

            if qty:
                where_sql += " AND sol.product_uom_qty >= %(qty)s \n"

            if external_customer_order_id:
                where_sql += " AND (so.external_customer_order_id ilike %(external_customer_order_id)s and so.cust_order_number ilike %(external_customer_order_id)s) \n"

            sql = """   SELECT
                            sp.id, array_agg(sm.location_id) as location_ids
                        FROM
                            stock_picking sp
                            LEFT JOIN stock_move sm ON sp.id = sm.picking_id
                            LEFT JOIN sale_order_line sol ON sol.id = sm.sale_line_id
                            LEFT JOIN sale_order so ON sol.order_id = so.id
                        WHERE 1=1
                            AND sp.state = 'done'
                            AND sol.product_id = %(product_id)s
                            AND sp.real_partner_id = %(partner_id)s
                            {where}
                        GROUP BY sp.id;
            """.format(where=where_sql)
            params = {
                'partner_id': partner_id,
                'product_id': product_id,
                'size_id': size_id,
                'qty': qty,
                'cost': cost,
                'external_customer_order_id': '%{order_id}%'.format(order_id=external_customer_order_id or ''),
            }
            cr.execute(sql, params)
            res = cr.fetchall() or []

            if product_id and picking_id:
                params = {
                    'product_id': product_id,
                    'picking_id': picking_id
                }
                sql = """SELECT sol.price_unit
                FROM stock_move sm
                LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                WHERE 1=1
                    and sm.picking_id = %(picking_id)s
                    and sm.product_id = %(product_id)s"""
                cr.execute(sql, params)
                product_cost = cr.fetchone() or []
                if product_cost:
                    val['cost'] = product_cost[0]

            pickings = {x[0]: x[1] for x in res}
            picking_ids = pickings.keys()

            if len(pickings) == 1 and external_customer_order_id:
                picking_id = picking_ids[0]

            if picking_id and picking_id in picking_ids:
                val['picking_id'] = picking_id
                if return_target == 'source':
                    r_location_id = pickings[picking_id][0]
            domain['picking_id'].append(('id', 'in', picking_ids))

        if return_target == 'source':
            helper = self.pool.get('stock.picking.helper')
            complete_source_location = 'Suitable stock ID was not found for chosen Warehouse!'

            # IF PRODUCT IS SET
            if helper.check_product_is_set(cr, uid, product_id):
                components = helper.get_set_product_components(cr, uid, product_id=product_id, picking_id=picking_id)
                cmp_list = []
                csl_list = []
                for component in components:
                    # try to get source location from stock_move DEPRECATED
                    # ret_data = helper.get_set_component_source_location(cr, uid, component_id=component.get('product_id'), picking_id=picking_id)
                    # _logger.warn("SOURCE DATA from stock_move: %s" % ret_data)
                    # if not ret_data:

                    # get component data from transactions
                    ret_data = helper.get_product_source_location_mssql(cr, uid, product_id=component.get('product_id'), size_id=component.get('size_id', False) or size_id, warehouse_id=ret_wh.id)
                    if ret_data and ret_data.get('location_id', False) and ret_data.get('bin_id', False):
                        _logger.warn("SOURCE DATA from transactions: %s" % ret_data)
                        # DLMR-1921
                        # Update warehouse/location to CAFERNDP/location if warehouse is CAFER
                        # For basic item
                        src_wh_name = ret_data.get('warehouse_name')
                        if ret_wh.name == 'CAFER':
                            try:
                                _logger.info("Switch CAFER->CAFERNDP initiated for set item")
                                caferndp_location = helper.get_caferndp_location(cr, uid, cafer_location_id=ret_data.get('location_id', None))
                                ret_data.update({
                                    'src_location_id': caferndp_location,
                                    'location_id': caferndp_location,
                                    'location_complete_name': ret_data.get('location_complete_name').replace(' CAFER ', ' CAFERNDP '),
                                })
                                src_wh_name = 'CAFERNDP'
                            except Exception, e:
                                raise osv.except_osv('Warning', e.message)
                        # END OF DLMR-1921
                        csl_list.append("{}*{}/{}/{}:{}".format(
                            component.get('qty'),
                            ret_data.get('product_name'),
                            src_wh_name,
                            ret_data.get('location_name'),
                            ret_data.get('bin_name')
                        ))
                        cmp_list.append(ret_data)
                # add components to return vals
                if len(cmp_list) == len(components) and len(csl_list) > 0:
                    _logger.info("Components data to verification: %s" % cmp_list)
                    val.update({'components_src_locations': json.dumps(cmp_list)})
                    complete_source_location = ', '.join(csl_list)
                    _logger.info("Set complete location: %s" % complete_source_location)

            # IF BASIC ITEM (NOT SET)
            else:
                transactions_data = helper.get_product_source_location_mssql(cr, uid,
                                                                             product_id=product_id,
                                                                             size_id=size_id,
                                                                             warehouse_id=ret_wh.id)
                if transactions_data and transactions_data.get('location_id', False) and transactions_data.get('bin_id', False):
                    _logger.info('SOURCE DATA from transactions: %s' % transactions_data)
                    val['src_location_id'] = transactions_data.get('location_id')
                    val['src_bin_id'] = transactions_data.get('bin_id')
                    src_wh_name = transactions_data.get('warehouse_name')
                    # DLMR-1921
                    # Update warehouse/location to CAFERNDP/location if warehouse is CAFER
                    # For basic item
                    if ret_wh.name == 'CAFER':
                        try:
                            _logger.info("Switch CAFER->CAFERNDP initiated for basic item")
                            val.update({'src_location_id': helper.get_caferndp_location(cr, uid, cafer_location_id=val.get('src_location_id', None))})
                            src_wh_name = 'CAFERNDP'
                        except Exception, e:
                            raise osv.except_osv('Warning', e.message)
                    # END OF DLMR-1921
                    complete_source_location = '{}/{}:{}'.format(
                        src_wh_name,
                        transactions_data.get('location_name'),
                        transactions_data.get('bin_name')
                    )
                    _logger.info("Set complete location: %s" % complete_source_location)
            # set visible source location to return values dict
            val.update({'complete_source_location': complete_source_location})
        # return changes
        _logger.info("Returning vals: %s" % val)
        return {'value': val, 'domain': domain}

    def location_change(self, cr, uid, ids, product_id=False, size_id=False, return_location=False, return_bin=False, return_target=False, context=None):
        if return_target == 'source':
            return {}

        val = {
            'product_id': product_id,
            'size_id': size_id,
            'return_location': return_location or False,
            'return_bin': return_bin or False,
        }
        domain = {}

        bin_ids = []
        if product_id and return_location:
            bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [product_id], [return_location], [size_id])

        domain['return_bin'] = [('id', 'in', bin_ids)]
        _logger.warn('Loc.change found bins: %s' % domain)

        if not bin_ids or return_bin not in bin_ids:
            val['return_bin'] = False

        return {'value': val, 'domain': domain}

    def customer_sku_change(self, cr, uid, ids, customer_sku, partner_id=False, picking_id=False, qty=False, cost=False, batch_id=False, context=None):

        res = {
            'value': {},
        }

        product_id = False
        size_id = False
        if customer_sku:
            search_res = self.pool.get('product.product').search_product_by_cf(
                cr, uid,
                partner_id=partner_id,
                search_value=customer_sku,
                search_field=['customer_sku', 'upc', 'customer_id_delmar'],
                limit=1
            )
            if search_res and search_res[0]:
                product_id = search_res[0].get('product_id', False)
                size_id = search_res[0].get('size_id', False)
        res['value'].update({
            'size_id': size_id,
            'product_id': product_id,
            'customer_sku': customer_sku,
        })

        return res

    def line_id_change(self, cr, uid, ids, line_id, partner_id=False, batch_id=False, context=None):
        res = {
            'value': {
                'size_id': None,
                'product_id': None,
                'cost': 0.0,
                'qty': 0.0,
                'external_number': None,
                'picking_id': None,
            },
            'warning': {},
        }

        line_id = (line_id or '').strip()
        if line_id and len(line_id) > 4:

            cr.execute("""
                select
                    ol.product_id as product_id,
                    ol.size_id as size_id,
                    ol.price_unit as cost,
                    sm.product_qty as qty,
                    ol."merchantSKU" as external_number,
                    sp.id as picking_id
                from sale_order_line ol
                    left join stock_move sm on ol.id = sm.sale_line_id
                    left join stock_picking sp on sp.id = sm.picking_id
                    left join sale_order so on ol.order_id = so.id
                where 1=1
                    and sp.state = 'done'
                    and so.partner_id = %s
                    and (ol."optionSku" = %s or ol.external_customer_line_id = %s)
                limit 2
                """, (partner_id, line_id, line_id)
            )
            sql_res = cr.dictfetchall() or False

            if sql_res:
                if len(sql_res) == 1:
                    sql_res = sql_res[0]
                    res['value'].update({
                        'size_id': sql_res['size_id'] or None,
                        'product_id': sql_res['product_id'] or None,
                        'cost': sql_res['cost'] or 0.0,
                        'qty': sql_res['qty'] or 0.0,
                        'external_number': sql_res['external_number'] or None,
                        'picking_id': sql_res['picking_id'] or None,
                    })
                else:
                    res['warning'] = {
                        'title': _('Warning!'),
                        'message': 'Found several orders with this number. Please, select required fields manually.'
                    }

        return res

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        return_id = super(StockPickingMassReturnLine, self).create(cr, uid, vals, context=context)

        return_obj = self.browse(cr, uid, return_id)

        if not return_obj.qty or return_obj.qty <= 0:
            raise osv.except_osv(_('Warning!'), 'Qty must be a positive value!')

        move_obj = self.pool.get('stock.move')
        r_location = return_obj.target_location_id
        r_bin = return_obj.target_bin_id

        partner = return_obj.partner_id
        price_unit = return_obj.cost
        batch = return_obj.batch_id

        return_obj_vals = {}

        picking_id = return_obj.picking_id.id
        if picking_id:
            # Return existing order
            cr.execute("""  SELECT
                                array_agg(sm.id) as move_ids,
                                sol.id as line_id,
                                sol.product_uom_qty as line_qty
                            FROM stock_move sm
                                LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                            WHERE 1=1
                                AND sm.picking_id = %s
                                AND sm.product_id = %s
                                AND (COALESCE(sm.size_id, 0) = %s OR COALESCE(sol.size_id, 0) = %s)
                            GROUP BY sol.id
                            ;
            """, (picking_id, return_obj.product_id.id, return_obj.size_id_delivery.id or 0, return_obj.size_id.id or 0))
            move_sql_res = cr.dictfetchall()

            ordered_qty = 0
            ol_ids = []
            move_ids = []
            for x in move_sql_res:
                move_ids += x['move_ids']
                ol_ids.append(x['line_id'])
                ordered_qty += x['line_qty']

            if move_ids:

                # Check: how much items returned before
                cr.execute("""
                    SELECT
                        STRING_AGG(CONCAT(name, ' (', qty, ')'), ', ') as return_details,
                        SUM(qty)::int as total_returned
                    FROM (
                        select sum(sm.product_qty) as qty, sp."name", sp.id
                        from stock_move sm
                            left join stock_picking sp on sp.id = sm.picking_id
                        where sm.sale_line_id IN %(line_ids)s
                            and sp.state = 'returned'
                        group by sp.id
                    ) a
                    HAVING %(ordered_qty)s - SUM(qty) < %(return_qty)s
                """, {
                    'line_ids': tuple(ol_ids),
                    'ordered_qty': ordered_qty,
                    'return_qty': return_obj.qty,
                })
                sql_res = cr.dictfetchone()
                if sql_res:
                    raise osv.except_osv(
                        'Warning!',
                        'Not enough items. Have been ordered {ordered_qty} item(s). Already returned {total_returned} item(s): {details}.'.format(
                            ordered_qty=int(ordered_qty), total_returned=sql_res['total_returned'], details=sql_res['return_details']
                        ))
            else:
                raise osv.except_osv(_('Warning!'), 'Stock move not found!')

            # Prepare data
            move_id = move_ids[0]
            move = move_obj.browse(cr, uid, move_id)
            user = self.pool.get('res.users').browse(cr, uid, uid)
            partial_data = {
                'delivery_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            }

            if not r_location:
                raise osv.except_osv(_('Warning!'), 'Can\'t find location for return!')

            partial_data['move%s' % (move.id)] = {
                'line_id': False,
                'product_id': return_obj.product_id.id or False,
                'product_qty': return_obj.qty or 0.0,
                'product_uom': move.product_uom.id or False,
                'prodlot_id': move.prodlot_id.id or False,
                'location_id': r_location and r_location.id or False,
                'bin_id': r_bin and r_bin.id or False,
            }
            size_postfix = move_obj.get_size_postfix(cr, uid, move.size_id)
            note = '%s \n%s %s: Return some products: \n' % (
                move.picking_id.report_history,
                time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                user.name
            )
            note += '%s %s/%s\ninto %s / %s\nBy reason: %s\n' % (
                move.product_id.default_code,
                size_postfix,
                return_obj.qty,
                r_location and r_location.complete_name or False,
                r_bin and r_bin.name or 'RETURN',
                'Return in batch %s' % (batch and batch.chargeback or False)
            )
            price_unit = move.sale_line_id and move.sale_line_id.price_unit or False
            picking_obj = self.pool.get('stock.picking')
            picking_obj.write(cr, uid, move.picking_id.id, {'report_history': note})

            context.update({'from_mass_return': True})

            res_return = picking_obj.do_only_return(cr, uid, [move.picking_id.id], partial_data, context=context)

            if not res_return:
                raise osv.except_osv(_('Warning!'), _('MASSRET: Something went wrong on do_only_return'))

            # Change picking id if there has been done a partial return creating a new picking
            new_picking_id = res_return.get(picking_id, {}).get('returned_picking')
            if new_picking_id and new_picking_id != picking_id:
                return_obj_vals.update({'picking_id': new_picking_id})
            else:
                return_obj_vals.update({'picking_id': picking_id})

        else:

            if not price_unit or price_unit <= 0:
                raise osv.except_osv(_('Warning!'), 'Price must be a positive value!')

            onfly_obj = self.pool.get('stock.return.onfly.wizard')
            order_number = len(batch.return_line_ids or [])
            order_name = '%s-%s' % (batch.chargeback, order_number)
            while self.pool.get('sale.order').search(cr, uid, [('po_number', '=', order_name), ('partner_id', '=', partner.id or False)], limit=1):
                order_number += 1
                order_name = '%s-%s' % (batch.chargeback, order_number)

            _logger.warn('No source order, user name: %s' % order_name)

            onfly_vals = {
                'tracking_ref': batch.tracking_ref,
                'partner_id': partner.id or False,
                'type_api_id': partner.type_api_ids and partner.type_api_ids[0].id or False,
                'name': order_name or False,
                'external_customer_line_id': order_name or False,
                'product_id': return_obj.product_id.id or False,
                'description': return_obj.product_id.name,
                'size_id': return_obj.size_id.id or False,
                'size_id_delivery': return_obj.size_id_delivery.id or False,
                'product_qty': return_obj.qty,
                'price_unit': price_unit,
                'location_id': r_location and r_location.id or False,
                'bin_id': r_bin and r_bin.id or False,
                'return_batch_id': batch.id or False,
                'mass_return_line_id': return_obj.id or False,
                'is_set': return_obj.is_set or False
            }

            _logger.warn('Onfly vars: %s' % onfly_vals)

            onfly_id = onfly_obj.create(cr, uid, onfly_vals)
            res_return = onfly_obj.create_and_return(cr, uid, [onfly_id], from_mass_return=True)
            if not res_return:
                raise osv.except_osv(_('Warning!'), _('MASSRET: Something went wrong on ONFLY create_and_return'))

            if res_return:
                _logger.warn("ONFLY RETURNED PICKING_ID: %s" % res_return)
                return_obj_vals.update({'picking_id': res_return})


        return_obj_vals.update({
            'batch_id': batch.id or False,
            'cost': price_unit or False,
            'state': 'done',
        })
        get_sku_sql = """   SELECT
                                    distinct cf."value"
                            FROM
                                            product_multi_customer_names cn
                                INNER JOIN  product_multi_customer_fields_name fn on (
                                    fn.name_id = cn.id AND fn.customer_id = %s
                                )
                                INNER JOIN  product_multi_customer_fields cf on (
                                    cf.field_name_id = fn.id
                                    AND cf.product_id = %s
                                    AND COALESCE(cf.size_id, 0) = %s
                                )
                            WHERE 1=1
                                AND cn.name = 'customer_sku'
                                AND (cf."value" is not null and trim(cf."value") != '')
                            ;
        """ % (partner.id, return_obj.product_id.id, return_obj.size_id.id or 0)
        cr.execute(get_sku_sql)
        sku_res = cr.fetchone()
        if sku_res and sku_res[0]:
            return_obj_vals.update({
                'external_number': sku_res[0],
            })

        return_obj.write(return_obj_vals)
        ret_pick = self.pool.get('stock.picking').browse(cr, uid, return_obj_vals.get('picking_id'))

        # customer REF
        customer_ref = return_obj.batch_id.partner_id.ref or None

        # Do return for set components if line product is a set
        if return_obj.is_set:
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_sale_servers(cr, uid, single=True)
            for cmp_line in return_obj.verified_line_id.set_components_line_ids:
                delmarid = cmp_line.product_id and cmp_line.product_id.default_code or False
                description = cmp_line.product_id and cmp_line.product_id.description
                size_postfix = move_obj.get_size_postfix(cr, uid, cmp_line.size_id)
                itemid = delmarid and (delmarid + '/' + size_postfix) or False
                sign = 1
                reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, cmp_line.return_location.warehouse_id.name, cmp_line.return_location.name, server_id)
                unit_price = cmp_line.verified_line_id and cmp_line.verified_line_id.cost or 0
                params = (
                    itemid or 'null',  # itemid
                    'RET',  # TransactionCode
                    cmp_line.qty_to_return,  # qty
                    cmp_line.return_location.warehouse_id.name or 'null',  # warehouse
                    description or 'null',  # itemdescription
                    cmp_line.return_bin.name,  # move.bin or 'null',  # bin
                    cmp_line.return_location.name,  # stockid
                    'TRS',  # TransactionType
                    delmarid,  # id_delmar
                    sign,  # sign
                    (cmp_line.qty_to_return * sign) or '0',  # adjusted_qty
                    size_postfix,  # size
                    1,  # move.invredid or 'null',  # invredid
                    customer_ref,  # CustomerID
                    delmarid,  # ProductCode
                    ret_pick and ret_pick.sale_id and ret_pick.sale_id.po_number or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
                    reserved_flag or 0,  # reserved
                    None,  # trans_tag
                    unit_price * (-sign),  # unit price from sale_order_line
                )

                tr_id = move_obj.write_into_transactions_table(cr, uid, server_id, params)
                if not tr_id:
                    return False

        return return_id

    def print_qr_label_ch(self, cr, uid, ids, context):
        context.update({'type': 'China'})
        return self.print_qr_label(cr, uid, ids, context=context)

    def print_qr_label_ca(self, cr, uid, ids, context):
        context.update({'type': 'Canada'})
        return self.print_qr_label(cr, uid, ids, context=context)

    def print_qr_label(self, cr, uid, ids, context=None):
        if context is None:
            context = {'type': 'China'}
        _logger.warn('Print line label: %s' % context)

        if ids:
            _logger.warn('Print line IDS: %s' % ids)
            ret = self.browse(cr, uid, ids[0]).batch_id
            if ret:
                datas = {
                    'id': ret.id,
                    'ids': [ret.id],
                    'model': 'stock.picking.mass.return',
                    'report_type': 'pdf'
                    }
                act_print = {
                    'type': 'ir.actions.report.xml',
                    'report_name': 'stock.picking.mass_return_label',
                    'datas': datas,
                    'context': {'return_line_ids': ids, 'partial_print': True, 'type': context.get('type'), 'active_ids': [ret.id], 'active_id': ret.id}
                }
                return act_print
            else:
                _logger.warn("Ret batch id not found for printing")
        return True

    def show_attachments_list(self, cr, uid, ids, context=None):
        _logger.info("Open list of attachments for Returned line: %s" % ids)
        if ids and len(ids) > 0:
            prepare_obj = self.pool.get('stock.picking.return.prepare.line')
            return_line = self.browse(cr, uid, ids[0])
            if return_line and return_line.verified_line_id and return_line.verified_line_id.line_id:
                prepare_line = return_line.verified_line_id.line_id
                return prepare_obj.show_attachments_list(cr, uid, [prepare_line.id])
            else:
                return True
        else:
            return True


StockPickingMassReturnLine()


class ImportMassReturnPrepare(osv.osv_memory):
    """
    Class for importing lines.
    Objectives:
    - upload file
    - map file columns to acceptable columns
    - create lines in db with unverified state
    """
    # pool name of class
    _name = 'import.mass.return.prepare'

    # personal name_get method
    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    # virtual model params
    _columns = {
        'batch_id': fields.many2one('stock.picking.mass.return', 'Batch #', readonly=True),
        'importing_file': fields.binary(string="Importing file"),
    }

    _defaults = {}

    def open_columns_mapping_form(self, cr, uid, ids, context=None):
        """
        Opens form for fields mapping wizard
        The top-level method of all import process
        """

        # ERP model and windows objects
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        # virtual object for columns in imported file
        column_obj = self.pool.get('import.mass.return.file.column')
        # virtual object for mapping lines management
        mapping_obj = self.pool.get('import.mass.return.mapping.line')
        # virtual object containing acceptable columns for import
        accepted_obj = self.pool.get('import.mass.return.accepted.column')

        # Init prepare id (хрень какая-то, уберу)
        # prepare_data_id = False

        # Takes data from upload form (batch id(name))
        if ids:
            prepare_data_id = ids[0]
            #_logger.warn('prepare_data_id: %s' % prepare_data_id)
        else:
            raise osv.except_osv('Cannot find instance for the first step', '')
            return {'type': 'ir.actions.act_window_close'}

        # get form data (uploaded file)
        prepare_data = self.browse(cr, uid, prepare_data_id, context=context)
        if not prepare_data.importing_file:
            raise osv.except_osv('Please select the import file', '')
        import_file = base64.decodestring(prepare_data.importing_file)

        # Extract data from file
        # I used the existed method (it seems to be static) from import fields wizard
        import_obj = self.pool.get('import.fields.prepare')
        data = import_obj.exctact_data_from_file(cr, uid, import_file, context=context)

        # Init file columns dictionary
        col_dict = {}

        # iterate the columns in file
        column_number = 0
        for col in data['columns']:
            if col is not None:
                col_name = str(col).strip()
                _logger.warn("Got column name: %s" % col_name)
            else:
                _logger.warn("Got empty column name, updating to default")
                col_name = "Unnamed col #" + str(column_number)

            # create file column object
            file_column_id = column_obj.create(cr, uid, {
                'prepare_id': prepare_data_id,
                'name': col_name,
                'number': column_number,
            })
            _logger.warn('File column ID: %s' % file_column_id)
            # add object to columns dict
            col_dict.update({file_column_id: col_name.lower()})
            column_number += 1

            # try to get column contains product code to set by default
            if col_name.lower().replace(' ', '').replace('_', '') in ['delmarid', 'iddelmar', 'customersku', 'sku', 'upc']:
                default_delmar_id_column = file_column_id
            # try to get column contains product size to set by default
            if col_name.lower().replace(' ', '').replace('_', '') in ['size']:
                default_size_column = file_column_id

        # Init mapping lines list
        mapping_line_ids = []
        # create list of default accepting column names for mapping
        accepted_columns_ids = accepted_obj.search(cr, uid, [], context=context)
        accepted_columns = accepted_obj.browse(cr, uid, accepted_columns_ids, context=context)
        for a_col in accepted_columns:
            # try to find file column relative to accepted column
            try:
                lower_accepted_name = a_col.name.lower()
                file_column_name_id = col_dict.keys()[col_dict.values().index(lower_accepted_name)]
            except:
                file_column_name_id = False

            # create mapping line
            mapping_line_id = mapping_obj.create(cr, uid, {
                'prepare_id': prepare_data_id,
                'accepted_column_id': a_col.id,
                'file_column_id': file_column_name_id,
            })

            # add line to lines list
            mapping_line_ids.append([4, mapping_line_id, False])

        _logger.warn(mapping_line_ids)
        # create mapping object for user selection
        mapping_id = self.pool.get('import.mass.return.mapping').create(cr, uid, {
                'prepare_id': prepare_data_id,
                'line_ids': mapping_line_ids,
        })
        _logger.warn(mapping_id)

        # find form for the next step
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_view_import_mass_return_mapping')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)

        act_win.update({
            'res_id': mapping_id,
        })

        return act_win

ImportMassReturnPrepare()


# Mapping virtual object (top-level) for handling mapping setup
class ImportMassReturnMapping(osv.osv_memory):
    _name = "import.mass.return.mapping"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'prepare_id': fields.many2one('import.mass.return.prepare', 'Prepare Step of Wizard', ondelete='cascade'),
        'line_ids': fields.one2many('import.mass.return.mapping.line', 'mapping_id', "Mapping", help="File's columns Mapping"),
    }

    def import_file(self, cr, uid, ids, context=None):
        """
        Do all processes to import file to database
        to stock.picking.return.prepare.line object
        """
        if not ids:
            raise osv.except_osv('Can not process file import, please check file and try again', '')

        # init needed objects:
        # prepare object
        prepare_obj = self.pool.get('import.mass.return.prepare')
        # external object with xls/csv parser
        import_obj = self.pool.get('import.fields.prepare')
        # real object - lines of batch return
        preline_obj = self.pool.get('stock.picking.return.prepare.line')
        # picking object
        picking_obj = self.pool.get('stock.picking')
        # sale_order_line object
        sol_obj = self.pool.get('sale.order.line')

        # gets accepted_columns<->file_columns mapping
        mapping_data = self.browse(cr, uid, ids[0], context=context)
        # gets prepare data (batch_id, import_file)
        prepare_data = prepare_obj.browse(cr, uid, mapping_data.prepare_id.id, context=context)
        # parse data from file
        import_file = base64.decodestring(prepare_data.importing_file)
        file_data = import_obj.exctact_data_from_file(cr, uid, import_file, context=context)

        # create mapping dict
        mapping = {}
        for mapLine in mapping_data.line_ids:
            mapping[mapLine.accepted_column_id.name] = mapLine.file_column_id.number

        # list of already defined orders
        exclude_orders = list()
        file_line_num = 0   # DLMR-1518
        # iterate lines
        for fileLine in file_data['rows']:
            # DLMR-1518
            file_line_key = "{}_{}".format(prepare_data.id, file_line_num)
            # check key already exists in batch
            existed = preline_obj.search(cr, uid, ['&', ('batch_id', '=', prepare_data.batch_id.id), ('prepare_file_line_key', '=', file_line_key)])
            if existed:
                continue
            file_line_num += 1
            # END DLMR-1518
            # Line params, set default values
            line_params = {
                'name': "%s / %s" % (fileLine[mapping['Delmar ID']].strip(), float(fileLine[mapping['Size']]) if len(fileLine[mapping['Size']]) > 0 else 0),
                'batch_id': prepare_data.batch_id.id,
                'product_id': None,
                'sku_imported': fileLine[mapping['Delmar ID']].strip(),
                'product_image': None,
                'not_delmar_product': False,
                'size_id': None,
                'size_id_delivery': None,
                'size_imported': str(float(fileLine[mapping['Size']])) if len(fileLine[mapping['Size']]) > 0 else 0,
                'sizeable': True if len(fileLine[mapping['Size']]) > 0 else False,
                'qty': int(fileLine[mapping['Quantity']]) if fileLine[mapping['Quantity']] else 0,
                'qty_imported': int(fileLine[mapping['Quantity']]) if fileLine[mapping['Quantity']] else 0,
                'cost': float(fileLine[mapping['Cost']]) if fileLine[mapping['Cost']] else 0,
                'cost_imported': float(fileLine[mapping['Cost']]) if fileLine[mapping['Cost']] else 0,
                'picking_id': None,
                'picking_imported': str(fileLine[mapping['Order']]).strip() if fileLine[mapping['Order']] else None,
                'customer_order_id': None,
                'partner_id': prepare_data.batch_id.partner_id.id,
                'comments': None,
                'is_set': False,
                'prepare_file_line_key': file_line_key,     # DLMR-1518
            }
            # re-check size and sizeable
            if float(line_params['size_imported']) > 0:
                line_params.update({'sizeable': True})
            else:
                line_params.update({'sizeable': False, 'size_imported': 0})

            # trying to find product by incoming SKU/Customer SKU
            # 1. In product.product by default code
            product_obj = self.pool.get('product.product')
            product = None
            product_ids = product_obj.search(cr, uid, [('default_code', '=', line_params['sku_imported']), ('type', '=', 'product')], context=context)
            line_params.update({'product_id': product_ids[0] if product_ids else None})
            # 2. In multi-customer fields by Customer ID Delmar if not found by default_code
            if line_params['product_id'] is None:
                pn_obj = self.pool.get('product.multi.customer.fields.name')
                pn_ids = pn_obj.search(cr, uid, [('label', 'like', '%Customer ID Delmar%'), ('customer_id', '=', prepare_data.batch_id.partner_id.id)], context=context)
                if pn_ids:
                    pf_obj = self.pool.get('product.multi.customer.fields')
                    pf_ids = pf_obj.search(cr, uid, [('field_name_id', '=', pn_ids[0]), ('value', '=', line_params['sku_imported'])], context=context)
                    if pf_ids:
                        pf = pf_obj.browse(cr, uid, pf_ids[0], context=context)
                        line_params.update({'product_id': pf.product_id.id if pf.product_id else None})
                        line_params.update({'size_id': pf.size_id.id if pf.size_id else None})
                        line_params.update({'sizeable': True if pf.size_id else False})
            # 3. In multi-customer fields by Customer SKU
            if line_params['product_id'] is None:
                pn_obj = self.pool.get('product.multi.customer.fields.name')
                pn_ids = pn_obj.search(cr, uid, [('label', 'like', '%Customer SKU%'), ('customer_id', '=', prepare_data.batch_id.partner_id.id)], context=context)
                if pn_ids:
                    pf_obj = self.pool.get('product.multi.customer.fields')
                    pf_ids = pf_obj.search(cr, uid, [('field_name_id', '=', pn_ids[0]), ('value', '=', line_params['sku_imported'])], context=context)
                    if pf_ids:
                        pf = pf_obj.browse(cr, uid, pf_ids[0], context=context)
                        line_params.update({'product_id': pf.product_id.id if pf.product_id else None})
                        line_params.update({'size_id': pf.size_id.id if pf.size_id else None})
                        line_params.update({'sizeable': True if pf.size_id else False})
            # 4. In multi-customer fields by UPC
            if line_params['product_id'] is None:
                pn_obj = self.pool.get('product.multi.customer.fields.name')
                pn_ids = pn_obj.search(cr, uid, [('label', '=', 'UPC'), ('customer_id', '=', prepare_data.batch_id.partner_id.id)], context=context)
                if pn_ids:
                    pf_obj = self.pool.get('product.multi.customer.fields')
                    pf_ids = pf_obj.search(cr, uid, [('field_name_id', '=', pn_ids[0]), ('value', 'like', "%"+line_params['sku_imported']+"%")], context=context)
                    if pf_ids:
                        pf = pf_obj.browse(cr, uid, pf_ids[0], context=context)
                        line_params.update({'product_id': pf.product_id.id if pf.product_id else None})

            # check product is sizeable
            if line_params['product_id'] is not None:
                product = product_obj.browse(cr, uid, line_params['product_id'], context=context)
                if product:
                    sizeable_query = """ SELECT count(*) as cnt FROM product_ring_size_default_rel WHERE product_id=%s;"""
                    cr.execute(sizeable_query, (product.product_tmpl_id.id, ))
                    sizeable_check = cr.fetchone()[0] or 0
                    # set sizeable parameter in line pool
                    line_params.update({'sizeable': True if sizeable_check > 0 else False})

            # check product is set
            if line_params['product_id'] is not None and product:
                # check product is a SET
                helper = self.pool.get('stock.picking.helper')
                line_params.update({'is_set': helper.check_product_is_set(cr, uid, product.id)})

            # trying to get size id by name
            size_ids = None
            if line_params['sizeable'] and line_params['size_id'] is None:
                size_obj = self.pool.get('ring.size')
                size_ids = size_obj.search(cr, uid, [('name', '=', str(line_params['size_imported']))], context=context)
                line_params.update({'size_id': size_ids[0] if size_ids else None})
                _logger.warn("Size search result: %s" % size_ids)

            # set complete name
            if line_params['product_id'] is not None and product and line_params['size_id'] is not None:
                line_params.update({'name': "%s / %s" % (product.default_code, line_params['size_imported'])})

            # trying to get order (stock_picking) by incoming picking_imported
            # 1. By Delivery order name
            if line_params['picking_id'] is None and line_params['picking_imported'] is not None:
                picking_ids = picking_obj.search(cr, uid, [('name', '=', line_params['picking_imported']), ('state', '=', 'done')], context=context)
                _logger.warn("Found picking id by delivery order name: %s" % picking_ids)
                line_params.update({'picking_id': picking_ids[0] if picking_ids else None})
            # 2. By Sale order name or external_order_id
            if line_params['picking_id'] is None and line_params['picking_imported'] is not None:
                sale_obj = self.pool.get('sale.order')
                sale_ids = sale_obj.search(cr, uid, ['|', '|', ('name', '=', line_params['picking_imported']), ('external_customer_order_id', '=', line_params['picking_imported']), ('po_number', '=', line_params['picking_imported'])], context=context)
                _logger.warn("Found sale_order id by name or external order id or po %s: %s" % (line_params['picking_imported'], sale_ids))
                if sale_ids and sale_ids[0]:
                    picking_ids = picking_obj.search(cr, uid, [('sale_id', '=', sale_ids[0]), ('state', '=', 'done')], context=context)
                    _logger.warn("Found picking id by sale_order.id: %s" % picking_ids)
                    line_params.update({'picking_id': picking_ids[0] if picking_ids else None})

            # # 3. Crazy order finder related to DLMR-686
            # if line_params['picking_id'] is None and product and (line_params['size_id'] is not None or not line_params['sizeable']) and line_params['qty'] > 0 and line_params['cost'] >= 0:
            #     size_sql = 'and sl.size_id = %(size_id)s' if line_params['size_id'] else 'and sl.size_id is NULL'
            #     if len(exclude_orders) > 1:
            #         exclude_picking_sql = 'and sp.id NOT IN {pick_list}'.format(pick_list=str(tuple(exclude_orders)))
            #     elif len(exclude_orders) == 1:
            #         exclude_picking_sql = 'and sp.id <> {pick_id}'.format(pick_id=exclude_orders[0])
            #     else:
            #         exclude_picking_sql = ''
            #
            #     # query
            #     sq = """SELECT
            #             sp.id, sp.name, sp.shp_date, sl.product_id, sl.product_uom_qty as sale_qty, sm.product_qty as pick_qty, sl.size_id as sale_size, sm.size_id as pick_size, sl.price_unit
            #         from stock_picking sp
            #           LEFT JOIN stock_move sm on sm.picking_id=sp.id
            #           LEFT JOIN sale_order_line sl on sl.id=sm.sale_line_id
            #         where
            #           1=1
            #           and sp.shp_date < (current_date - '1 month'::interval)
            #           and sp.state='done'
            #           and (sp.real_partner_id = %(partner_id)s or sp.partner_id = %(partner_id)s)
            #           and sl.product_id = %(product_id)s
            #           and sl.product_uom_qty >= %(qty)s
		     #  and coalesce(sl.size_id,0) = coalesce(sm.size_id,0)
            #           {size_sql}
            #           {exclude_picking_sql}
            #           and sl.price_unit = %(cost)s
            #         order by sp.shp_date desc
            #         limit 1""".format(size_sql=size_sql, exclude_picking_sql=exclude_picking_sql)
            #     # parameters for query
            #     sq_params = {
            #         'partner_id': line_params['partner_id'],
            #         'product_id': product.id,
            #         'qty': line_params['qty'],
            #         'size_id': line_params['size_id'],
            #         'cost': line_params['cost']
            #     }
            #     _logger.warn("SQL for found import order: %s with params: %s" % (sq, sq_params))
            #     # run query
            #     cr.execute(sq, sq_params)
            #     sql_res = cr.dictfetchone()
            #
            #     pick_id = sql_res.get('id', False) if sql_res else False
            #     if pick_id:
            #         line_params.update({'picking_id': pick_id})
            #
            #     _logger.warn("Found order by crazy algorithm: %s" % sql_res)

            # when picking was found in imported file
            # check size, cost and quantity in picking
            if line_params['picking_id'] is not None:
                exclude_orders.append(line_params.get('picking_id'))
                picking = picking_obj.browse(cr, uid, line_params['picking_id'], context=context)
                # set cost
                line_params.update({'cost': line_params['cost_imported'] or 0})

                # find and set qty and size
                move = False
                sol = None
                for move_line in picking.move_lines:
                    sol = sol_obj.browse(cr, uid, move_line.sale_line_id.id)
                    _logger.info("Iterated move_line: prod={}:{}, size(sale:delivery:imported)={}:{}:{}, qty={}:{}"
                                 .format(move_line.product_id.id,
                                         line_params['product_id'],
                                         sol.size_id.id,
                                         move_line.size_id and move_line.size_id.id,
                                         line_params['size_imported'],
                                         move_line.product_qty,
                                         line_params['qty_imported']))
                    if move_line.product_id.id == line_params['product_id'] \
                            and (
                                    (sol.size_id and sol.size_id.name == str(float(line_params['size_imported'])))
                                    or (not move_line.size_id)
                                )\
                            and int(move_line.product_qty) >= int(line_params['qty_imported']):
                        move = move_line
                        break

                # If move_line was found in order's list, fill data from order
                _logger.warn("VARIABLE TYPE: %s " % type(move))
                if move and isinstance(move, osv.orm.browse_record):
                    _logger.warn("Found correct move_line: %s" % move)
                    # qty commented die to DLMR-772
                    # line_params.update({'qty': move.product_qty})
                    # size
                    line_params.update({'size_id': sol and sol.size_id.id or None})
                    line_params.update({'size_id_delivery': move.size_id and move.size_id.id or None})
                    # cost
                    line_params.update({'cost': move.sale_line_id.price_unit or 0})
                else:
                    # clear Order id
                    line_params.update({'picking_id': None})
                    # search size id
                    size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', line_params['size_imported'])], context=context)
                    line_params.update({'size_id': size_ids[0] if size_ids else None})
                    line_params.update({'size_id_delivery': size_ids[0] if size_ids else None})

            # check QTY imported vs in order
            if line_params['qty_imported'] > line_params['qty']:
                line_params.update({'picking_id': None})

            # Finally, create the line
            unverified_line_id = preline_obj.create(cr, uid, line_params)
            _logger.warn("LineID: %s" % unverified_line_id)

        # close window
        res = {'type': 'ir.actions.act_window_close'}
        return res

ImportMassReturnMapping()


# Mapping virtual object for handling mapping lines
class ImportMassReturnMappingLine(osv.osv_memory):
    _name = "import.mass.return.mapping.line"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'prepare_id': fields.many2one('import.mass.return.prepare', 'Prepare Step of Import', ondelete='cascade'),
        'mapping_id': fields.many2one('import.mass.return.mapping', 'Columns Mapping Wizard', ondelete='cascade'),
        'accepted_column_id': fields.many2one('import.mass.return.accepted.column', 'Available Columns', required=True, ondelete='cascade'),
        'file_column_id': fields.many2one('import.mass.return.file.column', 'Column Name In File', required=True, ondelete='cascade', help="Name of column in imported file"),
    }

ImportMassReturnMappingLine()


class ImportMassReturnFileColumn(osv.osv_memory):
    _name = "import.mass.return.file.column"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        reads = self.read(cr, uid, ids, ['name'], context=context)
        res = []
        for record in reads:
            name = record['name']
            res.append((record['id'], name))
        return res

    _columns = {
        'prepare_id': fields.many2one('import.mass.return.prepare', 'Prepare Step of Import', ondelete='cascade'),
        'name': fields.char('Column name', size=256),
        'number': fields.integer('Column number'),
    }

ImportMassReturnFileColumn()


# Real class handling acceptable column names
class ImportMassReturnAcceptedColumn(osv.osv):
    _name = "import.mass.return.accepted.column"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        reads = self.read(cr, uid, ids, ['name'], context=context)
        res = []
        for record in reads:
            name = record['name']
            res.append((record['id'], name))
        return res

    _columns = {
        'name': fields.char('Name', size=128),
        'number': fields.integer('Column Number'),
    }
    _order = "number ASC"

ImportMassReturnAcceptedColumn()


# Virtual object for handling bulk verify lines
class MassReturnBulkVerify(osv.osv_memory):
    _name = "mass.return.bulk.verify"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'batch_id': fields.many2one('stock.picking.mass.return', 'Mass Return Batch', ),
        'warehouse_id': fields.related(
            'batch_id', 'warehouse_id',
            type='many2one', string='Warehouse', store=True,
            relation='stock.warehouse',
        ),
        #'lines': fields.one2many('stock.picking.return.prepare.line', 'id', 'Lines to verify', ),
        #'lines': fields.dummy(string='Lines', relation='stock.picking.return.prepare.line', type='one2many', ),
        'return_target': fields.selection([
            ('return', 'Return Location'),
            ('source', 'Source Location'),
            ('selected', 'Selected Location'),
            ('mntl', 'Return to MNTL'),
        ], 'Return Target', required=True, ),
        'src_location_id': fields.many2one('stock.location', 'Location', domain=[('complete_name', 'ilike', '/ Stock /')], ),
        # ^^ deleted condition ('usage', '=', 'internal'), ('warehouse_id', '=', 'warehouse_id')
        'generate_bin': fields.boolean('Generate bin if not exists', required=False),
        'return_bin': fields.many2one('stock.bin.create', 'Select Bin', ),
        'custom_bin': fields.char('or type a custom bin name', size=32, required=False)
    }

    _defaults = {
        'return_target': 'return'
    }

    def verify_lines(self, cr, uid, ids, context=None):
        _logger.warn("Lines to verify context: %s for ID: %s" % (context, ids))

        if not context.get('default_lines'):
            osv.except_osv('Warning!', 'No lines selected for return!')

        obj = self.browse(cr, uid, ids[0], context)
        ver_obj = self.pool.get('stock.picking.return.verified.line')
        _logger.warn("Bulk object src location: %s" % obj.src_location_id.complete_name)

        for id in context.get('default_lines'):
            line = self.pool.get('stock.picking.return.prepare.line').browse(cr, uid, id)
            line_vals = {
                'line_id': id,
                'batch_id': line.batch_id.id,
                'product_id': line.product_id.id,
                'size_id': line.size_id.id,
                'sizeable': line.sizeable,
                'qty_verified': line.qty,
                'return_target': obj.return_target,
                'return_location': obj.src_location_id.id or False,
                'return_bin': obj.return_bin and obj.return_bin.id or False,
                'src_location_id': obj.src_location_id.id or None,
                'src_bin_id': obj.return_bin and obj.return_bin.id or False,
                'generate_bin': obj.generate_bin,
                'custom_bin': obj.custom_bin and obj.custom_bin.strip() or None,
            }

            context.update({'line_id': id})
            _logger.warn("Bulk verify object: %s" % line_vals)

            ver_id = ver_obj.create(cr, uid, line_vals, context=context)

            if ver_id:
                total, verified, unverified = ver_obj.count_total_verified_unverified(cr, uid, id, context=context)
                _logger.warn("Total: %s, Verified: %s, Unverified: %s" % (total, verified, unverified))
                line.write({'qty': unverified})
                # change state if needed
                if verified < total:
                    line.write({'state': 'partial'})
                else:
                    line.write({'state': 'verified'})

        # close window
        res = {'type': 'ir.actions.act_window_close'}
        return res

MassReturnBulkVerify()


class StockPickingMassReturnWarehouseLabel(osv.osv_memory):
    _name = 'stock.picking.mass.return.warehouse.label'
    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', )
    }

    def print_warehouse_labels(self, cr, uid, ids, context=None):
        _logger.warn('Print WH context: %s' % context)
        dt = self.browse(cr, uid, ids[0], context=context)
        _logger.warn('Print WH id %s' % dt.warehouse_id)

        wh_id = dt.warehouse_id.id if dt.warehouse_id else None
        if not wh_id:
            raise osv.except_osv('Warning', 'Warehouse was not selected!')

        ret_id = context.get('id', None) or context.get('active_ids')[0] or None
        if not ret_id:
            raise osv.except_osv('Warning', 'Mass return batch was not selected!')

        ret = self.pool.get('stock.picking.mass.return').browse(cr, uid, ret_id, context=None)
        if not ret or not ret.return_line_ids:
            raise osv.except_osv('Warning', 'Something went wrong - cannot browse batch!')

        ret_line_ids = []
        for line in ret.return_line_ids:
            if line.target_location_id.warehouse_id.id == wh_id:
                ret_line_ids.append(line.id)
        _logger.warn('Return line IDS to print: %s' % ret_line_ids)

        ret_lines = self.pool.get('stock.picking.mass.return.line').browse(cr, uid, ret_line_ids, context=None);
        _logger.warn('Return lines to print: %s' % ret_lines)

        if ret_line_ids:
            datas = {
                'id': ret_id,
                'ids': [ret_id],
                'model': 'stock.picking.mass.return',
                'report_type': 'pdf'
                }
            act_print = {
                'type': 'ir.actions.report.xml',
                'report_name': 'stock.picking.mass_return_label',
                'datas': datas,
                'context': {'return_line_ids': ret_line_ids, 'partial_print': True, 'active_ids': [ret_id], 'active_id': ret_id}
            }
            return act_print

        return True

StockPickingMassReturnWarehouseLabel()

class ReturnItemLabel(osv.osv_memory):
    _name = 'return.item.label'

    def _get_product_qr(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            move_obj = self.pool.get('stock.move')
            delmarid = line.product_id and line.product_id.default_code or False
            size_postfix = move_obj.get_size_postfix(cr, uid, line.size_id)
            serial = line.serial and '/{}'.format(line.serial) or ''
            res[line['id']] = delmarid and ("{}/{}{}".format(delmarid, size_postfix, serial)) or False
        return res

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=True, ),
        'product_image': fields.char('Product image', size=128, required=False, ),
        'product_qr': fields.function(_get_product_qr, string='QR-code string', type="char", store=False, ),
        'size_id': fields.many2one('ring.size', 'Size', required=False, ),
        'bin_id': fields.many2one('stock.bin', 'Bin', required=False, ),
        'bin_name': fields.char('Bin name', size=128, required=False, ),
        'cafer_bin_id': fields.many2one('stock.bin', 'CAFER Bin', required=False, ),
        'made_in': fields.char('Made in', size=32, required=False, ),
        'serial': fields.char('Serial', size=32, required=False, ),
    }

    def create(self, cr, uid, vals, context=None):
        id = super(ReturnItemLabel, self).create(cr, uid, vals, context=context)
        self.define_cafer_bin(cr, uid, [id])
        return id

    def define_cafer_bin(self, cr, uid, ids, context=None):
        for label in self.browse(cr, uid, ids):
            if not label.product_id:
                continue
            size_ids = []
            if label.product_id.sizeable or label.product_id.ring_size_ids:
                if label.size_id:
                    size_ids.append(label.size_id.id)
                else:
                    continue
            bin_ids = list(self.pool.get('product.product').\
                get_mssql_product_bins(cr, uid,
                                       ids=[label.product_id.id],
                                       loc_ids=[],
                                       size_ids=size_ids,
                                       context={'warehouse': 'CAFER'}
                                       ))
            if bin_ids:
                _logger.info("BIN IDS FOUND: %s" % bin_ids)
                bin_found = False
                bin_tpl = re.compile('^\d{1,2}-[A-Z]-\d{1,2}$')
                for bin in self.pool.get('stock.bin').browse(cr, uid, bin_ids):
                    if bin and bin_tpl.match(bin.name):
                        self.write(cr, uid, label.id, {'cafer_bin_id': bin.id})
                        bin_found = True
                        break
                return bin_found
        return False


ReturnItemLabel()


class ImportOrderLines(osv.osv_memory):
    _name = 'mass.return.import.order.lines'

    _columns = {
        'batch_id': fields.many2one('stock.picking.mass.return', 'Mass Return Batch', ),
        'picking_id': fields.many2one('stock.picking', 'Order',
            domain="[('state', '=', 'done')]", required=True,
            readonly=False, help="Please, select delivery order to process return for it."),
    }

    def create_lines(self, cr, uid, ids, context=None):
        pick_obj = self.pool.get('stock.picking')
        move_obj = self.pool.get('stock.move')
        line_obj = self.pool.get('stock.picking.return.prepare.line')
        form = self.browse(cr, uid, ids[0])
        orders = pick_obj.browse(cr, uid, form.picking_id.id, context=context)
        for move in orders.move_lines:
            line_create_vals = {
                'batch_id': form.batch_id.id,
                'product_id': move.product_id.id,
                'sku_imported': move.product_id.default_code,
                'sizeable': move.product_id.sizeable or move.product_id.ring_size_ids or False,
                'size_id': move.size_id and move.size_id.id or False,
                'size_imported': move.size_id and move.size_id.name or False,
                'qty': move.product_qty,
                'qty_imported': move.product_qty,
                'cost': move.sale_line_id.price_unit,
                'cost_imported': move.sale_line_id.price_unit,
                'picking_id': form.picking_id.id,
                'picking_imported': form.picking_id.name,
                'partner_id': form.picking_id.real_partner_id.id,
                'state': 'draft'
            }
            pline_id = line_obj.create(cr, uid, line_create_vals, context=None)

        # close window
        res = {'type': 'ir.actions.act_window_close'}
        return res

ImportOrderLines()


class MassReturnLineAttachment(osv.osv_memory):
    # pool name of class
    _name = 'mass.return.line.attachment'

    # personal name_get method
    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    # virtual model params
    _columns = {
        'importing_file': fields.binary(string="File to attach to line", required=True, ),
        'importing_filename': fields.char('Filename', size=32, required=True, ),
    }

    _defaults = {}

    def process_attachment(self, cr, uid, ids, context=None):

        ir_attach = self.pool.get('ir.attachment')

        for data in self.browse(cr, uid, ids):
            file_data = None
            try:
                file_data = data.importing_file
            except TypeError:
                pass
            assert file_data, 'Incorrect/Missing attachment file content'
            imported_filename = data.importing_filename or "{:massretfile-%Y-%m-%d-%H%M%S}".format(datetime.now())
            attachment_data = {
                'name': imported_filename,
                'datas_fname': imported_filename,
                'datas': file_data,
                'res_model': 'stock.picking.return.prepare.line',
                'res_id': context.get('prepare_line_id'),
            }
            _logger.info("Attachment data: %s" % attachment_data)
            attachment_id = ir_attach.create(cr, uid, attachment_data)
            _logger.info("Created attachment id: %s" % attachment_id)

        #return {'type': 'ir.actions.act_window_close'}
        return self.pool.get('stock.picking.return.prepare.line').show_attachments_list(cr, uid, [context.get('prepare_line_id')], context=None)


MassReturnLineAttachment()


# DLMR-1266
# Serial number generator for return labels
class StockPickingReturnLabelSerial(osv.osv):
    _name = 'stock.picking.return.label.serial'
    _rec_name = 'serial'

    def _generate_serial(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            # Workaround for old orders without records in history
            if line.return_line:
                res[line['id']] = "{}-{}".format(line.return_line.id, line.id)
            else:
                res[line['id']] = '{}-{}'.format(line.id, line.id)
        return res

    _columns = {
        'return_line': fields.many2one('return.product.history', "Product return history line", required=True, ),
        'product': fields.many2one('product.product', 'Optional product', required=True, ),
        'num': fields.integer('Number',  required=True, ),
        'serial': fields.function(_generate_serial, string='Serial', type="char", store=True, ),
    }
    _sql_constraints = [
        ('serial_unique', 'unique(return_line, product, num)', 'The serial must be unique!'),
    ]

    def define_for_label(self, cr, uid, picking_id=None, product_id=None, size_id=None, qty=1, set_component=False, num=1):
        if (not picking_id) or (not product_id) or num <= 0:
            return None
        # search for history line
        search_domain = [('picking_id', '=', picking_id), ('product_id', '=', product_id), ('qty', '=', qty), ('set_component', '=', set_component)]
        if size_id:
            search_domain.append(('size_id', '=', size_id))
        _logger.info("Serial history search domain: %s" % search_domain)
        hist_ids = self.pool.get('return.product.history').search(cr, uid, search_domain)
        if hist_ids:
            line_id = hist_ids[0]
            # try to find existed before creating
            existed_ids = self.search(cr, uid, [('return_line', '=', line_id), ('product', '=', product_id), ('num', '=', num)])
            if existed_ids and existed_ids[0]:
                serial_id = existed_ids[0]
            else:
                # create new serial record if wasn't defined before
                serial_id = self.create(cr, uid, {'return_line': line_id, 'product': product_id, 'num': num})
            serial = self.browse(cr, uid, serial_id)
            return serial.serial

        return 'NO_SERIAL'


StockPickingReturnLabelSerial()
