# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009 Sharoon Thomas
#    Copyright (C) 2010-Today OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import osv


class email_template(osv.osv):
    _inherit = 'email.template'

    def send_mail(self, cr, uid, template_id, res_id, force_send=False, context=None):

        if context is None:
            context = {}

        context.update({
            'web_root_url': self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')
            })

        return super(email_template, self).send_mail(cr, uid, template_id, res_id, force_send=force_send, context=context)

email_template()
