# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2013
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv

class country_shipping(osv.osv):
    _name = 'country.shipping'

    _rec_name = "server_id"

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customer', required=False, domain="[('customer', '=', True)]"),
        'country_id': fields.many2one('res.country', 'Ship to Country', required=False, ),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True, ),
        'server_id': fields.many2one('fetchdb.server', 'DB Connection', required=True,
            domain="[('target', '=', 'warehouse'), ('state', '=', 'done')]", ),
    }

    _order = " warehouse_id, COALESCE(country_id, 0), COALESCE(partner_id)"

    _sql_constraints = [
        ('uniq', 'unique (partner_id, country_id, warehouse_id)',
            'The setting must be unique !')
    ]

country_shipping()
