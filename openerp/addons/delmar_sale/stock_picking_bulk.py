# -*- coding: utf-8 -*-

from osv import fields, osv
import logging
import re
from tools.translate import _
import xlsxwriter
import base64
import cStringIO
import time


_logger = logging.getLogger(__name__)


class stock_picking_bulk(osv.osv):
    _name = "stock.picking.bulk"

    def _get_search_ids(self, cr, uid, ids, name, args, context=None):
        res = self.get_search_ids(cr, uid, ids, context=context)
        return res

    _columns = {
        'name': fields.char(
            'Batch#', size=128,
            required=True,
            readonly=True, states={'draft': [('readonly', False)]},
            ),
        'partner_id': fields.many2one(
            'res.partner', 'Customer',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)]},
            domain="[('customer', '=', True)]",
            ),
        'warehouse_id': fields.many2one(
            'stock.warehouse', 'Warehouse',
            required=False,
            ),
        'shp_date': fields.datetime(
            'Ship Date',
            select=True,
            readonly=False,
            ),
        'state': fields.selection([
            ('draft', 'New'),
            ('active', 'Active'),
            ('picking', 'Waiting Tracking Number'),
            ('ready', 'Ready'),
            ('done',  'Done'),
            ], 'State', readonly=True, select=True, ),
        'search_params': fields.char('Search', size=256, ),
        'search_btn': fields.boolean('Search', ),
        'delivery_address_id': fields.many2one(
            'res.partner.address', "Deliver to",
            required=True,
            ),
        'shipper_address_id': fields.many2one(
            'res.partner.address', "Shipped From",
            required=True,
            ),
        'search_ids': fields.function(
            _get_search_ids,
            string='Orders',
            type='many2many',
            relation="stock.picking",
            method=True,
            ),
        'picking_ids': fields.one2many(
            'stock.picking',
            'shp_bulk_id',
            'Orders',
            ondelete='set null'
            ),
        'tracking_ref': fields.char('Track Number', size=128, ),
        'packing_xlsx': fields.binary(string="GlobalPackingList", readonly=True, ),
        'packing_name': fields.char('Packing Name', size=256, ),
        'add_all_btn': fields.boolean('Add all', help="Add all orders to the batch", ),
        'remove_all_btn': fields.boolean('Remove all', help="Remove all orders from the batch", ),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The bulk name must be unique !')
    ]

    _defaults = {
        'state': 'draft',
        'packing_xlsx': False,
        'warehouse_id': False,
    }

    _reset = [
        'search_params',
        'search_btn',
        'add_all_btn',
        'remove_all_btn',
    ]

    def create(self, cr, uid, vals, context=None):
        if vals and self._reset:
            for r in self._reset:
                vals[r] = False
        new_id = super(stock_picking_bulk, self).create(cr, uid, vals, context=context)

        if new_id:
            self.generate_packing_xls(cr, uid, new_id)

        return new_id

    def write(self, cr, uid, ids, vals, context=None):
        if vals and self._reset:
            for r in self._reset:
                vals[r] = False
        return super(stock_picking_bulk, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        active_ids = self.search(cr, uid, [('id', 'in', ids), ('picking_ids', '!=', False)], context=context)
        if active_ids:
            raise osv.except_osv(_('ERROR!'), _('You can only delete draft batches or active batches without orders'))

        else:
            super(stock_picking_bulk, self).unlink(cr, uid, ids, context=context)

        return True

    def change_partner_id(self, cr, uid, ids, partner_id=None, context=None):
        value = {
            'partner_id': partner_id,
            'delivery_address_id': False,
            'shipper_address_id': False,
        }

        delivery_domain = [('type', '=', 'delivery'), ('partner_id', '=', partner_id or 0)]
        shipper_domain = [('type', '=', 'bulk'), ('partner_id', '=', partner_id or 0)]

        if partner_id:
            address_obj = self.pool.get('res.partner.address')

            delivery_ids = address_obj.search(cr, uid, delivery_domain, limit=1)
            if delivery_ids:
                value.update({'delivery_address_id': delivery_ids[0]})

            shipper_ids = address_obj.search(cr, uid, shipper_domain, limit=1)
            if shipper_ids:
                value.update({'shipper_address_id': shipper_ids[0]})

        return {'value': value}

    def get_search_ids(self, cr, uid, ids, search=None, wh_id=None, context=None):
        res = {}
        for bulk_id in ids:
            res[bulk_id] = [(6, 0, [])]

        if ids:
            order_where = ''
            if search:
                search_params = re.findall(r"[\w-]+", search)
                if search_params:
                    order_where = 'AND ( %s )' % (" OR ".join(["""sp.name like '%s%%%%'""" % (x.upper()) for x in search_params if x]))

            wh_where = ''
            if wh_id:
                wh_where = 'AND sp.warehouses = %s' % (wh_id)

            cr.execute("""  SELECT
                                pb.id as bulk_id,
                                array_to_string(array_accum(sp.id), ',') as order_ids
                            FROM stock_picking sp
                                inner join sale_order so on sp.sale_id = so.id and so.bulk_transfer = True
                                left join stock_picking_bulk pb on (
                                    1=1
                                    and pb.partner_id = sp.real_partner_id
                                    and sp.type = 'out'
                                    and (
                                        (
                                            sp.shp_bulk_id is null
                                            and sp.state in ('picking', 'outcode_except')
                                            and pb.state in ('draft', 'active')
                                        ) or (
                                            pb.id = sp.shp_bulk_id
                                        )
                                    )
                                    %s
                                )
                            WHERE 1=1
                                and pb.id in %%s %s
                            GROUP BY
                                pb.id
            """ % (wh_where, order_where), (tuple(ids),))
            result = cr.dictfetchall() or []
            if result:
                for r in result:
                    res[r['bulk_id']] = r.get('order_ids', False) and [(6, 0, [int(x) for x in r['order_ids'].split(',')])]

        return res

    def on_search(self, cr, uid, ids, search=None, wh_id=None, context=None):
        value = {
            'search_ids': False,
        }

        if ids:
            if len(ids) > 1:
                raise osv.except_osv(_('Warning !'), 'Search may only be done for one bulk')
            else:
                search_result = self.get_search_ids(cr, uid, ids, search=search, wh_id=wh_id, context=context)
                value.update({
                    'search_ids': search_result and search_result[ids[0]]
                    })

        return {'value': value}

    # Actions
    def action_active(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'active'}, context=context)

    def action_picking(self, cr, uid, ids, context=None):
        self.generate_packing_xls(cr, uid, ids, context=context)
        return self.write(cr, uid, ids, {'state': 'picking'}, context=context)

    def generate_packing_xls(self, cr, uid, ids, context=None):

        if isinstance(ids, (int, long)):
            ids = [ids]

        for bulk in self.browse(cr, uid, ids, context=context):
            buf = cStringIO.StringIO()
            workbook = xlsxwriter.Workbook(buf)
            worksheet = workbook.add_worksheet()

            deliver_to = bulk.delivery_address_id
            return_to = bulk.shipper_address_id

            money_style = '[$$-409]* #,##0.00;-[$$-409]* #,##0.00'

            header_format = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 36,
                'font_name': 'Times New Roman',
            })

            default_options = {
                'align': 'left',
                'valign': 'vcenter',
                'font_size': 11,
                'font_name': 'Times New Roman',
            }

            default_format = workbook.add_format(default_options)

            bold_format = workbook.add_format(
                dict(default_options, **{
                    'bold': 1,
                    'top': 1,
                    'left': 1,
                })
            )

            packing_format = workbook.add_format(
                dict(default_options, **{
                    'bold': 1,
                    'top': 1,
                    'left': 1,
                    'font_size': 12,
                })
            )

            top_format = workbook.add_format(
                dict(default_options, **{
                    'top': 1,
                })
            )

            right_format = workbook.add_format(
                dict(default_options, **{
                    'right': 1,
                })
            )

            left_format = workbook.add_format(
                dict(default_options, **{
                    'left': 1,
                })
            )

            topright_format = workbook.add_format(
                dict(default_options, **{
                    'top': 1,
                    'right': 1,
                })
            )

            border_format = workbook.add_format(
                dict(default_options, **{
                    'border': 1,
                })
            )

            table_options = {
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 11,
                'font_name': 'Calibri',
            }

            table_format = workbook.add_format(table_options)
            table_price_format = workbook.add_format(
                dict(table_options, **{
                    'num_format': money_style,
                })
            )
            summary_format = workbook.add_format(
                dict(table_options, **{
                    'border': 0,
                    'num_format': money_style,
                })
            )

            table_header_format = workbook.add_format(
                dict(default_options, **{
                    'border': 1,
                    'bold': 1,
                    'align': 'center',
                    'bg_color': '#D9D9D9',
                })
            )

            worksheet.set_column('A:A', 12.85546875)
            worksheet.set_column('B:B', 24.7109375)
            worksheet.set_column('C:C', 11.140625)
            worksheet.set_column('D:D', 14.5703125)
            worksheet.set_column('E:E', 10.5703125)

            # header
            worksheet.merge_range('A1:E1', 'Delmar MFG', header_format)

            # Shipper
            worksheet.write('A2:B4', '', border_format)
            worksheet.write('A2', 'Shipper:', bold_format)
            worksheet.write_blank('A3', '', left_format)
            worksheet.write_blank('A4', '', left_format)

            if return_to:
                worksheet.write('B2', return_to.name or '', top_format)
                worksheet.write('B3', return_to.street or '', default_format)
                worksheet.write(
                    'B4',
                    '%s, %s %s' % (
                        return_to.city or '',
                        return_to.state_id and return_to.state_id.code or '',
                        return_to.zip or '',
                    ),
                    default_format
                )

            # Ship Date
            worksheet.write('C2', 'Ship Date:', bold_format)
            worksheet.write_blank('C3', '', left_format)
            worksheet.write_blank('C4', '', left_format)
            worksheet.write('D2', time.strftime('%m/%d/%Y', time.gmtime()), default_format)
            worksheet.write_blank('E2', '', topright_format)
            worksheet.write_blank('E3', '', right_format)
            worksheet.write_blank('E4', '', right_format)

            # Delivery
            worksheet.write('A5', 'Delivery to:', bold_format)
            worksheet.write_blank('A6', '', left_format)
            worksheet.write_blank('A7', '', left_format)

            if deliver_to:
                worksheet.write('B5', deliver_to.name or '', top_format)
                worksheet.write('B6', deliver_to.street or '', default_format)
                worksheet.write(
                    'B7',
                    '%s, %s %s' % (
                        deliver_to.city or '',
                        deliver_to.state_id and deliver_to.state_id.code or '',
                        deliver_to.zip or '',
                    ),
                    default_format
                )
            else:
                worksheet.write_blank('B5', '', top_format)

            # Tracking#
            worksheet.write('C5', 'Tracking#', bold_format)
            worksheet.write('D5', bulk.tracking_ref or '', top_format)
            worksheet.write_blank('C6', '', left_format)
            worksheet.write_blank('C7', '', left_format)
            worksheet.write_blank('E5', '', topright_format)
            worksheet.write_blank('E6', '', right_format)
            worksheet.write_blank('E7', '', right_format)

            # Packing List
            worksheet.write('A8', 'Packing List', packing_format)
            worksheet.write_blank('B8', '', top_format)
            worksheet.write_blank('C8', '', top_format)
            worksheet.write_blank('D8', '', top_format)
            worksheet.write_blank('E8', '', topright_format)

            # Table
            worksheet.write('A9', 'STORE', table_header_format)
            worksheet.write('B9', 'PO#', table_header_format)
            worksheet.write('C9', 'QTY', table_header_format)
            worksheet.write('D9', 'SKU', table_header_format)
            worksheet.write('E9', '$$$$$', table_header_format)

            cr.execute("""  SELECT
                                so.store_number as "store",
                                so.po_number as po,
                                sol."merchantSKU" as sku,
                                sum(sm.product_qty) as qty,
                                sum(sol.price_unit) as price,
                                sum(COALESCE(st.tax_value, 0)) as taxes,
                                rp.company_id as company_id
                            FROM stock_picking_bulk bulk
                                INNER JOIN stock_picking sp ON sp.shp_bulk_id = bulk.id
                                LEFT JOIN stock_move sm ON (sm.set_parent_order_id = sp.id and sp.state<>'done' and sm.is_set is true) or  (
                                sm.picking_id = sp.id and (( sp.state='done' and sm.is_set is true ) or sm.is_set is not true))
                                LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                                LEFT JOIN sale_order so ON 
                                (sol.set_parent_order_id = so.id and sp.state<>'done' and sol.is_set is true) or  (
                                sol.order_id= so.id and (( sp.state='done' and sol.is_set is true ) or sol.is_set is not true))
                                
                                LEFT JOIN res_partner rp ON rp.id = so.partner_id
                                LEFT JOIN res_partner_address ra on so.partner_shipping_id = ra.id
                                LEFT JOIN delmar_sale_taxes st on (
                                    st.active is True
                                    and st.state_id = ra.state_id
                                    and st.country_id = ra.country_id
                                )
                            WHERE bulk.id = %s
                            GROUP BY
                                "store",
                                po,
                                sku,
                                rp.company_id
                            ORDER BY
                                po,
                                sku
            """, (bulk.id,))
            lines = cr.dictfetchall()
            first_line = 9
            current_line = first_line
            company_obj = self.pool.get('res.company')
            for line in lines:
                worksheet.write(current_line, 0, line['store'] or '', table_format)
                worksheet.write(current_line, 1, line['po'] or '', table_format)
                worksheet.write(current_line, 2, line['qty'] or '', table_format)
                worksheet.write(current_line, 3, line['sku'] or '', table_format)
                line_price = float(line.get('price', False) or 0.0) * int(line.get('qty', False) or 0)
                company_id = line.get('company_id') or False
                company = company_obj.browse(cr, uid, company_id)
                is_canadian = company.country_id.code == 'CA'
                line_tax = float(line.get('taxes') or 0) if is_canadian else 0.0
                if line_tax:
                    line_price += line_price * line_tax
                worksheet.write_number(current_line, 4, line_price, table_price_format)
                current_line += 1

            if lines:
                worksheet.write_formula(current_line, 4, '{=SUM(E%s:E%s)}' % (first_line+1, current_line), summary_format)
            else:
                worksheet.write_number(current_line, 4, 0.0, table_price_format)

            workbook.close()
            out = base64.encodestring(buf.getvalue())
            buf.close()

            bulk.write({
                'packing_xlsx': out,
                'packing_name': '%s.xlsx' % (bulk.name)
            })

        return True

    def action_done(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'done'}, context=context)

    def action_ready(self, cr, uid, ids, context=None):

        self.generate_packing_xls(cr, uid, ids, context=context)
        for bulk in self.read(cr, uid, ids, ['picking_ids', 'tracking_ref', 'shp_date'], context=context):
            shp_date = bulk.get('shp_date', False) or time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            vals = {
                'state': 'ready',
                'shp_date': shp_date,
            }
            if bulk.get('picking_ids', False) and bulk.get('tracking_ref', False):
                self.pool.get('stock.picking').write(
                    cr, uid, bulk['picking_ids'],
                    {
                        'tracking_ref': bulk['tracking_ref'],
                        'shp_date': shp_date,
                        'carrier_code': 'BULK',
                    })

            self.write(cr, uid, bulk['id'], vals)

        return True

    # Conditions
    def check_tracking(self, cr, uid, ids, context=None):
        res = True
        for bulk in self.read(cr, uid, ids, ['tracking_ref']):
            tracking_ref = bulk['tracking_ref'] and bulk['tracking_ref'].strip() or False
            res &= bool(tracking_ref)
        return res

    def is_active(self, cr, uid, ids, context=None):
        active = bool(ids)
        for bulk in self.browse(cr, uid, ids, context=context):
            active &= bool(bulk.picking_ids)
        return active

    def is_done(self, cr, uid, ids, context=None):
        res = True
        allowed_states = (
            'done',
            'returned',
            'final_cancel'
        )
        picking_obj = self.pool.get('stock.picking')
        for bulk_id in ids:
            not_done_ids = picking_obj.search(
                cr, uid, [
                    ('type', '=', 'out'),
                    ('shp_bulk_id', '=', bulk_id),
                    ('state', 'not in', allowed_states)
                ])
            res &= not bool(not_done_ids)
        return res

    def mark_orders(self, cr, uid, ids, search_ids=None, search=None, target='mark', context=None):
        if ids and len(ids) == 1:
            new_context = {'shp_bulk_id': ids[0]}
            order_ids = search_ids and search_ids[0][2] or []
            self.pool.get('stock.picking').mark_bulk_order(cr, uid, order_ids, target=target, show_warnings=False, context=new_context)
        else:
            raise osv.except_osv(_('Warning !'), 'Mark may only be done for one bulk')

        return self.on_search(cr, uid, ids, search=search, context=context)

stock_picking_bulk()
