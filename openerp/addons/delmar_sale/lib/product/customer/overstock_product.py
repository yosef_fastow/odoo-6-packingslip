from openerp.addons.delmar_sale.lib.product.product import Product


class OverstockProduct(Product):

    def check_that_can_returning(self, product, customer, size):
        no_returns = self.pool.get('product.product').get_val_by_label(
            self.cr, self.uid, product.id, customer.id, 'No returns', None
        )
        if int(no_returns) == 1:
            return False
        return True
