from openerp.addons.delmar_sale.lib.product.customer.overstock_product import OverstockProduct
from openerp.addons.delmar_sale.lib.product.product import Product


class ProductFactory:

    def factory(type, cr, uid, pool):

        if type == 'Overstock.com':
            return OverstockProduct(cr, uid, pool)
        return Product(cr, uid, pool)

    factory = staticmethod(factory)
