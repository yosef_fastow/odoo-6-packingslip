# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import logging
import csv
import base64
import time
from StringIO import StringIO
from sale import SIGN_DEFAULT_VALUE, SIGN_ADJUSTMENT_VALUE
from datetime import datetime


logger = logging.getLogger('delmar_sale')

# Do not rearrange this! The correct order is: Box, Pouch, Card, Cloth
SUBMATERIAL_SELECTION = [
    ('box', 'Box'),
    ('pouch', 'Pouch'),
    ('card', 'Card'),
    ('cloth', 'Cloth'),
]


class stock_packing_material(osv.osv):
    _name = "stock.packing.material"
    _constraint_msg = 'Brand, customer, warehouse, product, product category and the cost range must be unique!'
    _m2o_search_fields = (
        'warehouse_id',
        'customer_id',
        'product_category_id',
        'product_id',
    )

    _priority_list = [
        'location_id',
        'brand',
        'customer_id',
        'warehouse_id',
        ['product_id', 'product_category_id'],  # filter by product_category_id only if all product_ids are False
        'value_from',
        'value_to',
    ]

    _columns = {
        'box_id': fields.many2one('stock.packing.material.box.code', 'Box code', ondelete='CASCADE'),
        'packing_desc_related': fields.related('box_id', 'packing_desc', type='char', string='Packing description', size=256),
        'customer_id': fields.many2one('res.partner', 'Customer', ondelete='CASCADE', domain="[('customer', '=', True)]"),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', ondelete='CASCADE', ),
        'location_id': fields.many2one('stock.location', 'Location', ondelete='CASCADE', domain="[('warehouse_id', '=', warehouse_id)]", ),
        'product_id': fields.many2one('product.product', 'Product', ondelete='CASCADE', domain="[('type', '!=', 'consu')]"),
        'product_category_id': fields.many2one('product.category', 'Product Category', ondelete='CASCADE', ),
        'value_from': fields.float('Value From'),
        'value_to': fields.float('Value To'),
        'packing_note_related': fields.related('box_id', 'packing_note', type='text', string='Packing Note'),
        'packing_weight': fields.related('box_id', 'packing_weight', type='char', string='Average Weight'),
        'brand': fields.char('Brand', size=1024),
        'submaterial': fields.related('box_id', 'submaterial', type='selection', selection=SUBMATERIAL_SELECTION,
                                      readonly=True, string='Sub Material Type')
    }

    _sql_constraints = [
        (
            'cust_wh_prod_uniq',
            'unique(brand,customer_id,warehouse_id,product_id,product_category_id,value_from,value_to)',
            _constraint_msg,
        ),
    ]

    # DLMR-1920
    def warehouse_change(self, cr, uid, ids, warehouse_id=None, context=None):
        val = {
            'location_id': None,
        }
        domain = {}
        return {'value': val, 'domain': domain}
    # END DLMR-1920

    # Forbid Duplication
    def copy(self, cr, uid, id, default=None, context=None):
        raise osv.except_osv('Error!', self._constraint_msg)

    def write(self, cr, uid, ids, vals, context=None):
        to_write_materials = self.read(cr, uid, ids, context=context)
        for material in to_write_materials:
            vals_to_check = self.prepare_vals_from_read_data(material, vals)
            self.raise_if_duplicates(cr, uid, vals_to_check)
        res = super(stock_packing_material, self).write(cr, uid, ids, vals, context=context)
        return res

    def create(self, cr, uid, vals, context=None):
        self.raise_if_duplicates(cr, uid, vals)
        res = super(stock_packing_material, self).create(cr, uid, vals, context=context)
        return res

    def prepare_vals_from_read_data(self, material, vals):
        return_vals = material.copy()
        return_vals.update(vals)
        for m2o_field in self._m2o_search_fields:
            if return_vals.get(m2o_field) and isinstance(return_vals[m2o_field], (list, tuple)):
                return_vals[m2o_field] = return_vals[m2o_field][0]
        return return_vals

    def raise_if_duplicates(self, cr, uid, vals):
        duplicate_ids = self.get_duplicate_ids(cr, uid, vals)
        if not duplicate_ids:
            return
        error_msg = None
        duplicates = self.read(cr, uid, duplicate_ids, ['box_id'])
        duplicate_codes = set([dup['box_id'][1] for dup in duplicates if dup.get('box_id') and dup.get('id') != vals.get('id')])
        len_codes = len(duplicate_codes)
        codes = ', '.join(duplicate_codes)
        if len_codes > 1:
            error_msg = '{len_codes} codes ({codes}) already exist with the same columns. Please remove all wrong ones before.'
        elif len_codes == 1:
            error_msg = "Code {codes} already exists with the same columns. Please remove it before if it's wrong."
        if error_msg:
            raise osv.except_osv(
                'Error!',
                error_msg.format(len_codes=len_codes, codes=codes),
            )

    def get_duplicate_ids(self, cr, uid, vals):
        search_args = []
        m2o_fields = self._m2o_search_fields
        price_fields = ('value_from', 'value_to')
        text_fields = ('brand', 'submaterial')
        for m2o_field in m2o_fields:
            self.add_m2o_search_arg(search_args, vals, m2o_field)
        for price_field in price_fields:
            self.add_price_search_arg(search_args, vals, price_field)
        for text_field in text_fields:
            self.add_text_search_arg(search_args, vals, text_field)
        duplicate_ids = self.search(cr, uid, search_args)
        return duplicate_ids

    def add_m2o_search_arg(self, search_args, vals, field):
        field_param = (field, '=', vals.get(field, False))
        search_args.append(field_param)

    def add_price_search_arg(self, search_args, vals, field):
        field_value = vals.get(field) or 0
        if field_value <= 0:
            field_params = [
                '|',
                (field, '<=', 0),
                (field, '=', False),
            ]
        else:
            field_params = [(field, '=', field_value),]
        search_args.extend(field_params)

    def add_text_search_arg(self, search_args, vals, field):
        if vals.get(field):
            text_param = (field, '=ilike', vals[field])
        else:
            text_param = (field, 'in', [False, ''])
        search_args.append(text_param)

    def _get_filtered_materials(
            self, cr, uid,
            warehouse_id, product_id, product_category_id, customer_id, value, brand, submaterial, location_id=False):
        material_ids = self.search(cr, uid, [
            ('submaterial', '=', submaterial),
            '|', ('brand', '=ilike', brand), ('brand', 'in', [False, '']),
            ('warehouse_id', 'in', [False, warehouse_id]),
            ('location_id', 'in', [False, location_id]),
            ('customer_id', 'in', [False, customer_id]),
            ('product_category_id', 'in', [False, product_category_id]),
            ('product_id', 'in', [False, product_id]),
            '|', ('value_from', '=', False), ('value_from', '<=', value),
            '|',
                '|', ('value_to', '<=', 0), ('value_to', '=', False),
                ('value_to', '>=', value),
        ])

        materials = self.read(cr, uid, material_ids)
        logger.info("GOT MATERIALS DATA: %s" % materials)
        if not materials:
            return False

        for priority_elem in self._priority_list:
            if len(materials) == 1:
                break
            if isinstance(priority_elem, str):
                materials = [material for material in materials if material.get(priority_elem)] or materials
            elif isinstance(priority_elem, list):
                for priority_elem_elem in priority_elem:
                    filtered_materials = [material for material in materials if material.get(priority_elem_elem)]
                    if filtered_materials:
                        materials = filtered_materials
                        break
        logger.info("FILTERED MATERIALS: %s" % materials)
        return materials

    def get_packing_material(
            self, cr, uid,
            warehouse_id, product_id, product_category_id, customer_id, value, brand, submaterials=None, location_id=False):
        if not submaterials:
            submaterials = [sub[0] for sub in SUBMATERIAL_SELECTION]
        result = []
        for submaterial in submaterials:
            submaterial_result = {
                'box_code': False,
                'box_id': False,
                'submaterial': submaterial,
            }
            filtered_materials = self._get_filtered_materials(
                cr, uid, warehouse_id,
                product_id, product_category_id,
                customer_id, value, brand, submaterial, location_id
            )
            material = filtered_materials and filtered_materials[0].get('box_id')
            if material:
                submaterial_result.update({
                    'box_id': material[0],
                    'box_code': material[1],
                })
            result.append(submaterial_result)
        return result


    # DLMR-963
    def write_box_moveline(self, cr, uid, box_id=None, move_id=None, context=None):
        if not box_id or not move_id:
            logger.warn("Cannot create stock move for box packing!")
            return False, False

        box_obj = self.pool.get('stock.packing.material.box.code')
        sm_obj = self.pool.get('stock.move')

        box = box_obj.browse(cr, uid, box_id)
        if not box.product_id:
            logger.warn("Box product_id was not setup for box code!")
            return False, False

        # source moveline
        product_move = sm_obj.browse(cr, uid, move_id)

        # check order is flash and return falses
        picking = product_move.picking_id or product_move.set_parent_order_id or None
        if (not picking) or picking.flash:
            logger.info("Picking is flash - not write box movelines")
            return False, False

        # check box_move_line was created earlier (was back-to-shelf for example)
        old_box_move_id = sm_obj.search(cr, uid, [('box_parent_move_id', '=', product_move.id)], limit=1)
        if old_box_move_id and old_box_move_id[0]:
            """
            sm_obj.write(cr, uid, old_box_move_id[0], {
                'location_id': product_move.location_id.id,
                'state': product_move.state,
                'status': product_move.status
            })
            """
            return False, old_box_move_id[0]

        # copy stock move for box
        box_move_id = sm_obj.copy(cr, uid, product_move.id, {
            'box_parent_move_id': product_move.id,
            'sale_line_id': None,
            'picking_id': None,
            'set_parent_order_id': None,
            'product_id': box.product_id.id,
            'product_desc': box.product_id.short_description,
            'bin': '',
            'bin_id': None,
            'is_set': False,
            'is_set_component': False,
            'price_unit': 0,
            'size_id': None,
            'size_ids': None,
        })
        return box_move_id, False

    # DLMR-963
    def write_box_transaction(self, cr, uid, box_move_id=None, sign=1, transactcode=SIGN_DEFAULT_VALUE, context=None):
        if not box_move_id:
            return False

        move_obj = self.pool.get('stock.move')
        bin_obj = self.pool.get('stock.bin')
        move = move_obj.browse(cr, uid, box_move_id)

        delmarid = move.product_id and move.product_id.default_code or False
        description = move.box_parent_move_id and move.box_parent_move_id.product_id.default_code
        size_postfix = move_obj.get_size_postfix(cr, uid, move.size_id)
        itemid = delmarid and (delmarid + '/' + size_postfix) or False
        warehouse = move_obj.get_warehouse_by_move_location(cr, uid, move.location_id and move.location_id.id)
        stockid = move.location_id and move.location_id.name or False
        bin_name = bin_obj.get_mssql_bin_for_product(cr, uid, itemid, warehouse, stockid).get(itemid) or False
        qty = move.product_qty
        customer_ref = move.box_parent_move_id.sale_line_id and move.box_parent_move_id.sale_line_id.order_id and move.box_parent_move_id.sale_line_id.order_id.partner_id.ref or None

        logger.info("Writing box transaction: %s %s * %s" % (sign, transactcode, qty))

        # transaction sql params
        unit_price = move.sale_line_id and move.sale_line_id.price_unit or 0
        params = (
            itemid or 'null',  # itemid
            transactcode,  # TransactionCode
            qty,  # qty
            move.box_parent_move_id.location_id.warehouse_id.name or 'null',  # warehouse
            description or 'null',  # itemdescription
            bin_name or False,  # move.bin or 'null',  # bin
            stockid or 'null',  # stockid
            'TRS',  # TransactionType
            delmarid,  # id_delmar
            sign,  # sign
            (qty * sign) or '0',  # adjusted_qty
            size_postfix,  # size
            '',  # move.invredid or 'null',  # invredid
            customer_ref,  # CustomerID
            delmarid,  # ProductCode
            move.box_parent_move_id.sale_line_id and move.box_parent_move_id.sale_line_id.order_id and move.box_parent_move_id.sale_line_id.order_id.po_number or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
            0,  # reserved flag
            move.box_parent_move_id.sale_line_id.id or None,  # trans_tag
            unit_price * (-sign),   # price_unit
        )
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), _('Can not write box transaction. Server with TRANSACTIONS table is not available!'))
        tr_context = {
            'erp_sm_id': move.id,
            'export_ref_ca': move.export_ref_ca,
            'export_ref_us': move.export_ref_us,
            'fc_invoice': move.fc_invoice,
            'fc_order': move.fc_order,
            'del_invoice': move.del_invoice,
            'del_order': move.del_order
        }
        tr_id = move_obj.write_into_transactions_table(cr, uid, server_id, params, context=tr_context)
        if tr_id:
            return True

        return False

stock_packing_material()


class stock_packing_material_box_code(osv.osv):
    _name = "stock.packing.material.box.code"

    def _get_available_qty(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        cache_obj = self.pool.get('stock.packing.material.box.cache')
        for line in self.browse(cr, uid, ids, context=context):
            cache_ids = cache_obj.search(cr, uid, [('box', '=', line.id)])
            caches = cache_obj.browse(cr, uid, cache_ids)
            qty = 0
            for cache in caches:
                qty += cache.qty
            res[line['id']] = qty
        return res

    def _get_product_def_domain(self, cr, uid, ids, name=None, args=None, context=None):
        res_ids = []
        sql = """SELECT pp.id
                    FROM product_product pp
                    LEFT JOIN product_template pt ON pt.id=pp.product_tmpl_id
                    LEFT JOIN product_category pc ON pc.id=pt.categ_id
                WHERE
                    pc.id in (19,61,64) AND pp.type='product' AND pc.parent_id=2 AND pp.active IS TRUE"""
        # logger.info("Get Product Domain SQL: %s" % sql)
        cr.execute(sql)
        res_ids = list(id[0] for id in cr.fetchall())
        # logger.info("RES IDS: %s" % res_ids)
        domain = "[('id', 'in', {})]".format(str(tuple(res_ids)))
        # logger.info("PRODUCT DOMAIN: %s" % domain)
        return eval(domain)

    def _get_product_domain(self, cr, uid, ids, name=None, args=None, context=None):
        res = {}
        domain = self._get_product_def_domain(cr, uid, ids)
        for line in self.browse(cr, uid, ids, context=context):
            res.update({line.id: domain})
        return res

    def _min_qty_search(self, cr, uid, obj, name, args, context):
        ids = set()
        sql = """
            WITH qty_counts AS (
              SELECT box, sum(qty) as avail from stock_packing_material_box_cache GROUP BY box
            )
            SELECT sc.id FROM stock_packing_material_box_code sc
            LEFT JOIN qty_counts qc ON qc.box=sc.id
            WHERE  qc.avail <= sc.min_qty or coalesce(qc.avail, 0) < 1        
        """
        cr.execute(sql)
        res_ids = set(id[0] for id in cr.fetchall())
        ids = ids and (ids & res_ids) or res_ids

        if ids:
            return [('id', 'in', tuple(ids))]
        return [('id', '=', '0')]

    def _get_link(self, cr, uid, ids, fn, args, context=None):
        res = {}
        if ids:
            base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'base.url.for.summary.link')
            base_url += '/inventory_report/count_manage/stock_summary'
            lines = self.read(cr, uid, ids, ['item', 'size', 'warehouse', 'stock', 'bin'])
            for line in lines:
                res[line['id']] = """%s?delmarID=%s/%s&warehouse=%s&location=%s""" % (
                    base_url,
                    line['item'],
                    line['size'],
                    line['warehouse'],
                    line['stock']
                )
        return res


    _columns = {
        'product_domain': fields.function(_get_product_domain, string='PD', type='char', method=True, required=True, store=False, ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=True, readonly=False, ),
        'packing_code': fields.char('Packing Code', size=256, required=True),
        'packing_desc': fields.char('Packing Description', size=256),
        'packing_note': fields.text('Packing Note'),
        'packing_weight': fields.char('Average Weight', size=256, ),
        'length': fields.integer('Length', ),
        'width': fields.integer('Width', ),
        'height': fields.integer('Height', ),
        'submaterial': fields.selection(SUBMATERIAL_SELECTION, 'Sub Material Type', required=True, select=True),
        'min_qty': fields.integer('Threshold', ),
        'available_qty': fields.function(_get_available_qty, string="QTY on stock", type='integer', fnct_search=_min_qty_search, method=True, store=False, ),
        'caches': fields.one2many('stock.packing.material.box.cache', 'box', 'Warehouse counts', )
    }

    _defaults = {
        'product_domain': _get_product_def_domain,
        'submaterial': 'box',
        'min_qty': 200
    }

    _rec_name = 'packing_code'

    _sql_constraints = [
        ('packing_code_uniq', 'unique(packing_code)', 'Packing Code must be unique!'),
    ]

    def do_box_stock_refresh(self, cr, uid, ids, context=None):
        logger.info('Stock refresh IDs: %s' % ids)
        for box_id in ids:
            self.pool.get('stock.packing.material.box.cache').update_box_all_cache(cr, uid, box_id=box_id)

        return {'type': 'ir.actions.act_reload_list_view'}

    def open_charge_stock(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        if context is None:
            context = {}

        new_context = context
        new_context.update({
            'default_box': ids[0] if ids and ids[0] else None,
            'default_warehouse': 15,
            'default_qty': 1,
            'flags': {
                'action_buttons': False,
                'deletable': False,
                'pager': False,
                'views_switcher': False
            },
        })

        # create action window
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_charge_stock')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
        act_win.update({
            'res_id': None,
            'context': new_context,
        })
        return act_win

    def open_stock_caches(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        if context is None:
            context = {}

        new_context = context
        new_context.update({
            'id': ids[0] if ids and ids[0] else None,
            'default_id': ids[0] if ids and ids[0] else None,
            'flags': {
                'action_buttons': False,
                'deletable': False,
                'pager': False,
                'views_switcher': False
            },
        })

        logger.warn('Form context: %s' % new_context)

        # create action window
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_open_stock_warehouse')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=new_context)
        act_win.update({
            'res_id': ids[0],
            'context': new_context,
        })
        return act_win

stock_packing_material_box_code()


class StockPackingMaterialBoxTransaction(osv.osv_memory):
    _name = "stock.packing.material.box.transaction"
    _columns = {
        'box': fields.many2one('stock.packing.material.box.code', 'Box', required=True, ),
        'warehouse': fields.many2one('stock.warehouse', 'Warehouse', required=True, ),
        'qty': fields.integer('QTY to add', required=True, ),
    }

    def write_transaction(self, cr, uid, ids, context=None):
        logger.warn("Need to write transactions for ids: %s" % ids)
        charge = self.browse(cr, uid, ids[0])

        move_obj = self.pool.get('stock.move')

        delmarid = charge.box.product_id and charge.box.product_id.default_code or False
        description = charge.box.product_id and charge.box.product_id.default_code
        size_postfix = move_obj.get_size_postfix(cr, uid, None)
        itemid = delmarid and (delmarid + '/' + size_postfix) or False
        qty = charge.qty
        # transaction sql params
        params = (
            itemid or 'null',  # itemid
            'ADJ',  # TransactionCode
            qty,  # qty
            charge.warehouse.name.upper() or 'null',  # warehouse
            description or 'null',  # itemdescription
            '',  # move.bin or 'null',  # bin
            '',  # stockid
            'TRS',  # TransactionType
            delmarid,  # id_delmar
            1,  # sign
            (qty * 1) or '0',  # adjusted_qty
            size_postfix,  # size
            '',  # move.invredid or 'null',  # invredid
            None,  # CustomerID
            delmarid,  # ProductCode
            'ADJDEF',  # 'ADJDEF',  # InvoiceNo
            0,  # reserved flag
            None,  # trans_tag
            0,  # unit_price
        )
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), _('Can not write box ADJ transaction. Server with TRANSACTIONS table is not available!'))
        tr_id = move_obj.write_into_transactions_table(cr, uid, server_id, params)
        if tr_id:
            self.pool.get('stock.packing.material.box.cache').update_box_all_cache(cr, uid, box_id=charge.box.id, warehouse_id=charge.warehouse.id, data=None, context=None)
            return {'type': 'ir.actions.act_close_wizard_and_reload_view'}
        else:
            raise osv.except_osv('Error!', 'Can not ADJUST box quantity!')


StockPackingMaterialBoxTransaction()


class StockPackingMaterialBoxCache(osv.osv):
    _name = "stock.packing.material.box.cache"

    def _get_link(self, cr, uid, ids, fn, args, context=None):
        res = {}
        if ids:
            base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'base.url.for.summary.link')
            base_url += '/inventory_report/count_manage/stock_summary'
            lines = self.browse(cr, uid, ids)
            for line in lines:
                res[line.id] = """%s?delmarID=%s/%s&warehouse=%s&location=%s""" % (
                    base_url,
                    line.box.product_id.default_code.upper(),
                    '0000',
                    line.warehouse.name.upper(),
                    ''
                )
        return res

    _columns = {
        'box': fields.many2one('stock.packing.material.box.code', 'Box', required=True, ),
        'warehouse': fields.many2one('stock.warehouse', 'Warehouse', required=False, ),
        'qty': fields.integer('QTY on stock', ),
        'transactdate': fields.datetime('Last updated', ),
        'summary_link': fields.function(_get_link, string='Summary', type='char', method=True),
    }
    _sql_constraints = [
        ('box_wh_uniq', 'unique (box, warehouse)', 'Box with warehouse must be unique!')
    ]

    def box_all_select_sql(func):
        def wrapped(*args, **kvargs):
            self = args[0]
            cr = args[1]
            uid = args[2]
            helper = self.pool.get('stock.picking.helper')
            data = func(*args, **kvargs)
            if not data:
                return False
            delmar_id = data.get('delmar_id', None)
            warehouse_name = data.get('warehouse_name', None)
            if delmar_id is not None and warehouse_name is not None:
                sql = """
                    SELECT 
                        WAREHOUSE, 
                        sum(ADJUSTED_QTY) 
                    FROM TRANSACTIONS 
                    WHERE ID_DELMAR='{}'
                    AND WAREHOUSE='{}' 
                    AND STATUS='Pending' 
                    GROUP BY WAREHOUSE
                """.format(delmar_id, warehouse_name)
            elif delmar_id is not None:
                sql = """
                    SELECT 
                        WAREHOUSE, 
                        sum(ADJUSTED_QTY) 
                    FROM TRANSACTIONS 
                    WHERE ID_DELMAR='{}' 
                    AND STATUS='Pending' 
                    GROUP BY WAREHOUSE
                """.format(delmar_id)
            else:
                return False

            counts = helper.MSSQLInventorySelect(cr, uid, sql=sql, params=None)
            kvargs.update({'data': counts})
            return func(*args, **kvargs)
        return wrapped

    @box_all_select_sql
    def update_box_all_cache(self, cr, uid, box_id=None, warehouse_id=None, data=None, context=None):
        if box_id is None:
            return False

        box = self.pool.get('stock.packing.material.box.code').browse(cr, uid, box_id)
        wh_obj = self.pool.get('stock.warehouse')
        # prepare params for mssql query inside decorator @box_all_select_sql
        if data is None:
            vals = {'delmar_id': box.product_id.default_code.upper()}
            if warehouse_id is not None:
                wh = wh_obj.browse(cr, uid, warehouse_id)
                if wh:
                    vals.update({'warehouse_name': wh.name.upper()})
            return vals
        # process cache update
        else:
            logger.warn("RECEIVED DATA: %s" % data)
            for line in data:
                wh_ids = wh_obj.search(cr, uid, [('name', '=', line[0])])
                if not wh_ids:
                    continue
                # check cache line exists
                cached_ids = self.search(cr, uid, [('box', '=', box.id), ('warehouse', '=', wh_ids[0])])
                if cached_ids and cached_ids[0]:
                    # update current qty
                    cache = self.browse(cr, uid, cached_ids[0])
                    if cache.qty <> line[1]:
                        self.write(cr, uid, cache.id, {
                            'qty': line[1],
                            'transactdate': datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
                        })
                else:
                    # create new cache record
                    vals = {
                        'box': box.id,
                        'warehouse': wh_ids[0],
                        'qty': line[1],
                        'transactdate': datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
                    }
                    new_id = self.create(cr, uid, vals)

            return True


StockPackingMaterialBoxCache()


class stock_packing_material_import(osv.osv_memory):

    _name = "stock.packing.material.import"
    _columns = {
        "csv_import": fields.binary(string="Input file", required=True),
        "notes": fields.text('Notes', ),
    }

    _csv_separator = ','
    _required_cols = [
        'product', 'category',
        'warehouse', 'customer',
        'box_code',
        'value_from', 'value_to',
    ]

    __tmp_table_name = None

    def import_confirm(self, cr, uid, ids, context=None):

        cur = self.browse(cr, uid, ids[0])
        if cur.csv_import:
            csv_data = base64.decodestring(cur.csv_import)
            if csv_data:
                self.write(cr, uid, ids, {'notes': self.__process_import_settings(cr, uid, csv_data)})
        else:
            raise osv.except_osv('File is not selected', '')

        return True

    def __process_import_settings(self, cr, uid, csv_data):
        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        head_row = [row for row in csv.reader([import_rows[0]], delimiter=str(self._csv_separator))][0]

        salt = int(round(time.time() * 1000))
        self.__tmp_table_name = '__tmp_stock_packing_material_import_%s' % salt

        idx = self.__validate_header(cr, uid, head_row)
        if not idx:
            return False

        self.__create_tmp_table(cr)
        self.__import_data(cr, import_rows, idx)
        self.__normalize_date(cr)
        errors = self.__validate_data(cr)
        result = self.__process_data(cr, uid)
        self.__drop_tmp_table(cr)

        if errors:
            result += "\n\nErrors: " + errors

        return result

    def __validate_header(self, cr, uid, head_row):

        head_row = [col.strip().lower().replace(' ', '_') for col in head_row if col]
        idx = {}

        for col in head_row:
            idx[col] = head_row.index(col)

        missing_cols = [x for x in self._required_cols if x not in head_row]
        if missing_cols:
            error_msg = 'Wrong csv format. Missing the following columns: %s.\n\nImport was aborted.' % (', '.join(missing_cols))
            raise osv.except_osv('Warning', error_msg)

        return idx

    def __create_tmp_table(self, cr):

        sql = """
            CREATE table {table_name} (
                id serial,
                product varchar,
                product_ids integer[],
                category varchar,
                category_ids integer[],
                warehouse varchar,
                warehouse_id integer,
                customer varchar,
                customer_ids integer[],
                box_code varchar,
                box_id integer,
                value_from varchar,
                value_to varchar
            )
        """.format(table_name=self.__tmp_table_name)
        cr.execute(sql)

    def __drop_tmp_table(self, cr):
        sql = """DROP TABLE {table_name}""".format(table_name=self.__tmp_table_name)
        cr.execute(sql)

    def __import_data(self, cr, import_rows, idx):
        try:
            tmp_file = StringIO()
            tmp_file.write("\n".join(import_rows[1:]))
            tmp_file.seek(0)
            cr.copy_from(
                tmp_file,
                self.__tmp_table_name,
                sep=self._csv_separator,
                columns=tuple(sorted(idx, key=idx.get))
            )
        except Exception, e:
            raise osv.except_osv('Warning', """Can't update temporary table. \nImport was aborted. \n\n %s""" % e)

    def __process_data(self, cr, uid):

        cr.execute("""
            with new_setup as (
                select box_id, warehouse_id,
                    case when product_ids is not null then unnest(product_ids) else null end as product_id,
                    case when customer_ids is not null then unnest(customer_ids) else null end as customer_id,
                    case when category_ids is not null then unnest(category_ids) else null end as category_id,
                    value_from, value_to
                from {table_name}
            ), updated_ids as (
                UPDATE stock_packing_material dd
                SET value_from = vv.value_from,
                    value_to = vv.value_to,
                    write_uid = {uid},
                    write_date = (now() at time zone 'UTC')
                from (
                    select
                        pm.id as id,
                        nullif(str_to_float(tt.value_from), 0) as value_from,
                        nullif(str_to_float(tt.value_to), 0) as value_to
                    from new_setup tt
                        left join stock_packing_material pm on
                                tt.box_id = pm.box_id
                                and coalesce(tt.product_id, 0) = coalesce(pm.product_id, 0)
                                and coalesce(tt.category_id, 0) = coalesce(pm.product_category_id, 0)
                                and coalesce(tt.warehouse_id, 0) = coalesce(pm.warehouse_id, 0)
                                and coalesce(tt.customer_id, 0) = coalesce(pm.customer_id, 0)
                        where pm.id is not null
                ) vv
                where dd.id = vv.id
                RETURNING dd.id
            ), insert_ids as (
                insert into stock_packing_material (
                    create_uid, create_date,
                    product_id, product_category_id,
                    warehouse_id, customer_id,
                    box_id,
                    value_from, value_to
                )
                select
                    {uid} as create_uid, (now() at time zone 'UTC') as create_date,
                    tt.product_id, tt.category_id,
                    tt.warehouse_id, tt.customer_id,
                    tt.box_id,
                    nullif(str_to_float(tt.value_from), 0), nullif(str_to_float(tt.value_to), 0)
                from new_setup tt
                    left join stock_packing_material pm on
                        tt.box_id = pm.box_id
                        and coalesce(tt.product_id, 0) = coalesce(pm.product_id, 0)
                        and coalesce(tt.category_id, 0) = coalesce(pm.product_category_id, 0)
                        and coalesce(tt.warehouse_id, 0) = coalesce(pm.warehouse_id, 0)
                        and coalesce(tt.customer_id, 0) = coalesce(pm.customer_id, 0)
                where pm.id is null
                RETURNING id
            )
            SELECT
                (SELECT COUNT(*) FROM insert_ids) inserted,
                (SELECT COUNT(*) FROM updated_ids) updated
        """.format(table_name=self.__tmp_table_name, uid=uid))
        res = cr.dictfetchone()

        return "Inserted new: {inserted}\nUpdated: {updated}".format(**res)

    def __normalize_date(self, cr):
        cr.execute("""
            update {table_name}
            set warehouse = nullif(upper(warehouse), ''),
                product = nullif(upper(product), ''),
                customer = nullif(lower(customer), ''),
                category = nullif(lower(category), ''),
                box_code = nullif(box_code, '')
        """.format(table_name=self.__tmp_table_name))

    def __validate_data(self, cr):
        errors = []
        absent_codes = self.__validate_code(cr)
        if absent_codes:
            errors.append('Please, create following codes: %s' % ", ".join(absent_codes))

        absent_warehouse = self.__validate_warehouse(cr)
        if absent_warehouse:
            errors.append('Warehouse(s) not exist: %s' % ", ".join(absent_warehouse))

        wrong_products = self.__validate_product(cr)
        if wrong_products:
            errors.append("Can't find following products: %s" % ", ".join(wrong_products))

        wrong_customers = self.__validate_customer(cr)
        if wrong_customers:
            errors.append("Can't find following customers: %s" % ", ".join(wrong_customers))

        wrong_categories = self.__validate_category(cr)
        if wrong_categories:
            errors.append("Can't find following categories: %s" % ", ".join(wrong_categories))

        return "\n\n".join(errors)

    def __validate_code(self, cr):
        cr.execute("""
            update {table_name} dd
            set box_id = vv.id
            from (
                select ic.box_code, bc.id
                from (
                    select distinct box_code
                    from {table_name}
                    where box_code is not null
                ) ic
                    left join stock_packing_material_box_code bc on bc.packing_code = ic.box_code
                where bc.packing_code is not null
            ) vv
            where dd.box_code = vv.box_code
        """.format(table_name=self.__tmp_table_name))

        cr.execute("""
            select distinct box_code
            from {table_name}
            where box_id is null
        """.format(table_name=self.__tmp_table_name))
        res = cr.fetchall()
        codes = [x[0] for x in res]

        if codes:
            cr.execute("""
                DELETE from {table_name} WHERE box_id is null
            """.format(table_name=self.__tmp_table_name))

        return codes

    def __validate_warehouse(self, cr):
        cr.execute("""
            update {table_name} dd
            set warehouse_id = vv.id
            from (
                select ic.warehouse, wh.id
                from (
                    select distinct warehouse
                    from {table_name}
                    where warehouse is not null
                ) ic
                    left join stock_warehouse wh on wh.name = ic.warehouse
                where wh.id is not null
            ) vv
            where vv.warehouse = dd.warehouse
        """.format(table_name=self.__tmp_table_name))

        cr.execute("""
            select distinct warehouse
            from {table_name}
            where warehouse is not null and warehouse_id is null
        """.format(table_name=self.__tmp_table_name))
        res = cr.fetchall()
        warehouses = [x[0] for x in res]

        if warehouses:
            cr.execute("""
                DELETE from {table_name} where warehouse is not null and warehouse_id is null
            """.format(table_name=self.__tmp_table_name))

        return warehouses

    def __validate_product(self, cr):
        cr.execute("""
            update {table_name} dd
            set product_ids = vv.ids
            from (
                select ic.id, array_agg(pp.id) as ids
                from {table_name} ic
                    left join product_product pp on pp.default_code = ic.product
                where ic.product is not null and pp.id is not null
                group by ic.id
            ) vv
            where dd.id = vv.id
        """.format(table_name=self.__tmp_table_name))

        cr.execute("""
            select distinct product
            from {table_name}
            where product is not null and product_ids is null
        """.format(table_name=self.__tmp_table_name))
        res = cr.fetchall()
        products = [x[0] for x in res]

        if products:
            cr.execute("""
                DELETE from {table_name} where product is not null and product_ids is null
            """.format(table_name=self.__tmp_table_name))

        return products

    def __validate_customer(self, cr):
        cr.execute("""
            update {table_name} dd
            set customer_ids = vv.ids
            from (
                select ic.customer, array_agg(distinct rp.id) as ids
                from {table_name} ic
                    left join res_partner rp on lower(rp.ref) = ic.customer or lower(rp.name) = ic.customer
                where ic.customer is not null and rp.id is not null
                group by ic.customer
            ) vv
            where vv.customer = dd.customer
        """.format(table_name=self.__tmp_table_name))

        cr.execute("""
            select distinct customer
            from {table_name}
            where customer is not null and customer_ids is null
        """.format(table_name=self.__tmp_table_name))
        res = cr.fetchall()
        customers = [x[0] for x in res]

        if customers:
            cr.execute("""
                DELETE from {table_name} where customer is not null and customer_ids is null
            """.format(table_name=self.__tmp_table_name))

        return customers

    def __validate_category(self, cr):
        cr.execute("""
            update {table_name} dd
            set category_ids = vv.ids
            from (
                select ic.category, array_agg(distinct pc.id) as ids
                from {table_name} ic
                    left join product_category pc on lower(pc.name) = ic.category
                where ic.category is not null and pc.id is not null
                group by ic.category
            ) vv
            where vv.category = dd.category
        """.format(table_name=self.__tmp_table_name))

        cr.execute("""
            select distinct category
            from {table_name}
            where category is not null and category_ids is null
        """.format(table_name=self.__tmp_table_name))
        res = cr.fetchall()
        categories = [x[0] for x in res]

        if categories:
            cr.execute("""
                DELETE from {table_name} where category is not null and category_ids is null
            """.format(table_name=self.__tmp_table_name))

        return categories
