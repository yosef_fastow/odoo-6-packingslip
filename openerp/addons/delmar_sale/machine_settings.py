# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv

class machine_settings_gate(osv.osv):
    _name = "machine.settings.gate"

    _columns = {
        "code": fields.char("Shipping code", size=256, required=True),
        'gate': fields.selection([
                                   ('01', '1'),
                                   ('02', '2'),
                                   ('03', '3'),
                                   ('04', '4'),
                                   ('05', '5'),
                                   ('06', '6'),
                                   ('07', '7'),
                                   ('08', '8'),
                                   ('09', '9'),
                                   ('10', '10'),
                                   ('11', '11'),
                                   ('12', '12'),
                                   ('13', '13'),
                                   ('14', '14'),
                                   ('15', '15'),
                                   ('16', '16'),
                                   ('17', '17'),
                                   ('18', '18'),
                                   ('19', '19'),
                                   ('20', '20')
                                   ], 'Gate', size=10),
    }

machine_settings_gate()

class machine_settings(osv.osv):
    _name = "machine.settings"

    _columns = {
        "name": fields.char("Machine name", size=256, required=True),
        'shipping_server_id': fields.many2one('fetchdb.server', 'Shipping Server settings'),
        'wh_ids': fields.many2many('stock.warehouse', 'machine_warehouse_rel', 'machine_id', 'warehouse_id', 'Warehouse'),
        'partner_ids': fields.many2many('res.partner', 'machine_partner_rel', 'machine_id', 'partner_id', 'Customers'),
        'machine_gate': fields.many2many('machine.settings.gate', 'machine_gate_rel', 'machine_id', 'gate_id', 'Gate'),
        'active': fields.boolean('Active')
    }
#fields.many2one('stock.warehouse', 'Warehouse'),
machine_settings()


