# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import time
from tools.translate import _
import re
from pf_utils.utils.label_printer import LabelWizPrinter
from datetime import datetime


class stock_picking_return_batch(osv.osv):
    _name = "stock.picking.return.batch"
    _rec_name = 'chargeback'
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customer', required=True, ),
        'chargeback': fields.char('Charge Back / Batch#', size=45, required=True, ),
        'create_uid': fields.many2one('res.users', 'User', ),
        'received_date': fields.datetime('Received Date', required=True, ),
        'line_ids': fields.one2many('stock.picking.return.line', 'batch_id', 'Return Lines', ),
        'product_id': fields.many2one('product.product', 'Product', domain="[('type', '=', 'product')]"),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', ),
        'tracking_ref': fields.char('Tracking Number', size=256, required=False, ),
    }

    _defaults = {
        'warehouse_id': lambda obj, cr, uid, context: (obj.pool.get('stock.warehouse').search(cr, uid, [('name', '=', 'USCHP')], context=context) + [None])[0],
        'received_date': datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    }

    _sql_constraints = [
        ('name_uniq', 'unique (chargeback, partner_id)',
            'The chargeback should be unique per customer !')
    ]

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):

        if context is None:
            context = {}

        try:
            batch_ids = []
            if args:
                f_domain = []
                f_domain = (arg for arg in args if ((isinstance(arg, list) or isinstance(arg, tuple)) and arg[0] == 'product_id')).next()
                f_index = args.index(f_domain)

                if f_domain and f_domain[2]:
                    product_ids = []
                    product_search = f_domain[2]

                    if isinstance(product_search, (int, long)):
                        product_ids.append(product_search)
                    else:
                        products = self.pool.get('product.product').name_search(cr, uid, product_search, context={'mode': 'simple'}, limit=None)
                        if products:
                            product_ids = [x[0] for x in products]

                    if product_ids:
                        cr.execute("""
                            SELECT DISTINCT rl.batch_id
                            FROM stock_picking_return_line rl
                            WHERE rl.product_id in %s;
                        """, (tuple(product_ids), ))

                        res = cr.fetchall()
                        batch_ids = [x[0] for x in res if x]
                args[f_index] = ['id', 'in', batch_ids]
        except:
            pass

        # print args
        return super(stock_picking_return_batch, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def action_print_batch_returns(self, cr, uid, ids, context=None):
        line_obj = self.pool.get("stock.picking.return.line")
        line_ids = line_obj.search(cr, uid, [('batch_id', 'in', ids)])
        return line_obj.action_print_batch_return_line(cr, uid, line_ids)

stock_picking_return_batch()


class stock_picking_return_line(osv.osv):
    _name = "stock.picking.return.line"

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for line in self.browse(cr, uid, ids, context=context):
            res.append((line.id, "%s/%s: %s" % (
                line.batch_id and line.batch_id.name or 'N/A',
                line.product_id and line.product_id.default_code or 'N/A',
                line.size_id and line.size_id.name or '0',
            )))
        return res

    def _get_image(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line['id']] = line.product_id and line.product_id.product_image or False
        return res

    def _get_complete_src_loc_name(self, cr, uid, ids, name, args, context=None):
        res = {}
        for batch_line in self.browse(cr, uid, ids, context=context):
            if batch_line:

                r_location = batch_line.target_location_id
                r_bin = batch_line.target_bin_id

                if r_location and r_bin:
                    complete_source_location = '%s    bin: %s' % (r_location and r_location.complete_name and re.sub('^.*?/(.*)', '\\1', r_location.complete_name), r_bin and r_bin.name)
                else:
                    complete_source_location = 'Suitable stock ID is not found for chosen Warehouse!'

                res[batch_line['id']] = complete_source_location

        return res

    _columns = {
        'create_date': fields.datetime('Date', readonly=True, select=1),
        'batch_id': fields.many2one('stock.picking.return.batch', 'Return Batch', ),
        'mass_id': fields.many2one('stock.picking.mass.return', 'Mass Return', ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'product_image': fields.function(_get_image, string="Image", type='binary', method=True, store=False, ),
        'size_id': fields.many2one('ring.size', 'Size',
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'sizeable': fields.boolean('Sizeable', ),
        'customer_sku': fields.char('Customer SKU', size=256,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'external_line_id': fields.char('Line ID', size=256,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'qty': fields.integer('Qty', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'cost': fields.float('Cost',
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'picking_id': fields.many2one('stock.picking', 'Order',
            domain="[('state', '=', 'done')]", required=False,
            readonly=True, states={'draft': [('readonly', False)]},
            help="Please, select delivery order to process return for it."),
        'external_number': fields.char('Customer SKU', size=256,),
        'external_customer_order_id': fields.char('External Order ID', size=256, ),
        'partner_id': fields.many2one('res.partner', "Partner",
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'return_target': fields.selection([
            ('return', 'Return Location'),
            ('source', 'Return to Source Location'),
            ('mntl', 'Return to MNTL'),
            ], 'Return Location', required=True,
            readonly=True, states={'draft': [('readonly', False)]}, ),
        'location_id': fields.many2one('stock.location', 'Location', ),
        'target_location_id': fields.many2one('stock.location', 'Location', ),
        'target_bin_id': fields.many2one('stock.bin', 'Bin', ),
        'fixed_return_target': fields.boolean('Fixed Target', ),
        'state': fields.selection([
            ('draft', 'New'),
            ('done', 'Done'),
            ], 'State', ),
        # 'complete_source_location': fields.char('Source Location', size=256,
        #     readonly=False,),
        'src_location_id': fields.many2one('stock.location', 'Location',
            domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],
        ),
        'src_bin_id': fields.many2one('stock.bin', 'Bin', ),
        'complete_source_location': fields.function(_get_complete_src_loc_name,
            string="Source Location", type='char', method=True, store=False,
        ),
        'invoice_no': fields.related(
            'picking_id', 'invoice_no',
            type='char', string='Invoice#', store=False,
        ),
        'ret_invoice_no': fields.related(
            'picking_id', 'ret_invoice_no',
            type='char', string='Retern Invoice#', store=False,
        ),
        'is_set': fields.boolean('Is set', ),
    }

    _defaults = {
        'fixed_return_target': False,
        'return_target': 'return',
        'state': 'draft',
        'sizeable': False,
    }

    _order = "create_date DESC"

    def print_label_wiz(self, cr, uid, ids, template, context=None):
        line = self.browse(cr, uid, ids[0])

        if (line.size_id and line.size_id.id):
            size_id = line.size_id.id
        else:
            size_id = ''

        label_printer = LabelWizPrinter(self.pool, cr, uid)
        user_name = self.pool.get('res.users').read(cr, uid, uid, ['name'])['name']
        bin_name = line.target_bin_id and line.target_bin_id.name or ''
        return label_printer.print_label(
            line.product_id.id, size_id,
            template,
            force={
                'bin_name': bin_name,
                'user_name': user_name,
            })


    def print_qr_item_label_ca_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabelCa'
        return self.print_label_wiz(cr, uid, ids, template)

    def print_qr_item_label_wiz(self, cr, uid, ids, context=None):
        template = 'QRItemLabel'
        return self.print_label_wiz(cr, uid, ids, template)



    def product_id_change(self, cr, uid, ids, product_id, size_id=False, partner_id=False, picking_id=False, qty=False, cost=False, batch_id=False, src_location_id=False, external_customer_order_id=None, return_target=None, context=None):
        # import pdb; pdb.set_trace()
        res = self.param_change(
            cr, uid, ids, product_id, size_id=size_id,
            partner_id=partner_id,
            picking_id=picking_id,
            qty=qty, cost=cost,
            batch_id=batch_id,
            src_location_id=src_location_id,
            external_customer_order_id=external_customer_order_id,
            return_target=return_target,
            context=context
            )
        updated_vals = {
            'fixed_return_target': False,
            'location_id': False,
            'is_set': False,
        }
        if product_id:
            product = self.pool.get('product.product').read(cr, uid, product_id, ['default_code', 'set_component_ids'])
            updated_vals.update(is_set=bool(product['set_component_ids']))

            server_obj = self.pool.get('fetchdb.server')
            server_id = server_obj.get_sale_servers(cr, uid, single=True)

            if server_id:
                sql_check_stock = """   SELECT DISTINCT t.WAREHOUSE, t.STOCKID
                                        FROM dbo.transactions t
                                        WHERE 1=1
                                            AND t.ID_DELMAR = ?
                                            AND status != 'Posted'
                                        GROUP BY t.ID_DELMAR, t.WAREHOUSE, t.STOCKID
                                        ;
                """
                params = [product['default_code']]
                sql_res = server_obj.make_query(cr, uid, server_id, sql_check_stock, params=params, select=True, commit=False)
                if sql_res:

                    locations = ["Physical Locations / %s / Stock / %s" % (x[0], x[1]) for x in sql_res if x]
                    cr.execute("""  SELECT rs.location_dest_id
                                    --  , rs.id, rs.cost, rs.priority, sl.complete_name as check_location, sld.complete_name as dest_location  -- debug sql
                                    FROM stock_picking_return_settings rs
                                        LEFT JOIN stock_location sl ON rs.location_id = sl.id
                                    --  LEFT JOIN stock_location sld on rs.location_dest_id = sld.id  -- debug sql
                                    WHERE 1=1
                                        AND sl.usage = 'internal'
                                        AND sl.complete_name in %s
                                        AND COALESCE(rs.cost, 0) <= %s
                                    ORDER BY
                                        rs.priority DESC,
                                        COALESCE(rs.cost, 0) DESC,
                                        COALESCE(rs.location_dest_id, 0) DESC
                                    ;
                        """, (tuple(locations), cost or 0, )
                    )
                    res_check_settings = cr.fetchone()
                    if res_check_settings:
                        updated_vals['return_target'] = 'mntl'
                        updated_vals['fixed_return_target'] = True
                        updated_vals['location_id'] = res_check_settings[0]

        if res.get('value', None):
            res['value'].update(updated_vals)

        return res

    def param_change(self, cr, uid, ids, product_id=False, size_id=False, partner_id=False, picking_id=False, qty=False, cost=False, batch_id=False, src_location_id=False, external_customer_order_id=None, return_target=None, context=None):
        # import pdb; pdb.set_trace()
        batch = self.pool.get('stock.picking.return.batch').browse(cr, uid, batch_id)
        ret_wh = batch.warehouse_id
        r_location_id = None

        val = {
            'product_id': product_id,
            'size_id': size_id,
            'picking_id': False,
            'sizeable': False,
            'product_image': False,
            'src_location_id': src_location_id,
            'src_bin_id': False,
        }
        if ret_wh.name in ('USCHP', 'USRETURN'):
            loc_domain = [
                ('usage', '=', 'internal'),
                '|',
                    ('complete_name', 'ilike', 'USRETURN / Stock /'),
                    ('complete_name', 'ilike', 'USCHP / Stock /'),
            ]
        else:
            loc_domain = [
                ('usage', '=', 'internal'),
                ('complete_name', 'ilike', '%s / Stock /' % (ret_wh.name)),
            ]
        domain = {
            'picking_id': [('real_partner_id', '=', partner_id), ('state', '=', 'done')],
            'src_location_id': loc_domain,
        }

        if product_id and partner_id:
            product = self.pool.get('product.product').read(cr, uid, product_id, ['ring_size_ids', 'product_image'])
            size_ids = product['ring_size_ids']
            if not size_ids or size_id not in size_ids:
                val['size_id'] = False
            val['sizeable'] = bool(size_ids)
            val['product_image'] = product['product_image']

            where_sql = ""
            if size_id:
                where_sql += " AND sol.size_id = %(size_id)s \n"
            else:
                where_sql += " AND sol.size_id IS NULL \n"

            if cost:
                where_sql += " AND sol.price_unit = %(cost)s \n"

            if qty:
                where_sql += " AND sol.product_uom_qty >= %(qty)s \n"

            if external_customer_order_id:
                where_sql += " AND (so.external_customer_order_id ilike %(external_customer_order_id)s and so.cust_order_number ilike %(external_customer_order_id)s) \n"

            sql = """   SELECT
                            sp.id, array_agg(sm.location_id) as location_ids
                        FROM
                            stock_picking sp
                            LEFT JOIN stock_move sm ON sp.id = sm.picking_id
                            LEFT JOIN sale_order_line sol ON sol.id = sm.sale_line_id
                            LEFT JOIN sale_order so ON sol.order_id = so.id
                        WHERE 1=1
                            AND sp.state = 'done'
                            AND sol.product_id = %(product_id)s
                            AND sp.real_partner_id = %(partner_id)s
                            {where}
                        GROUP BY sp.id;
            """.format(where=where_sql)
            params = {
                'partner_id': partner_id,
                'product_id': product_id,
                'size_id': size_id,
                'qty': qty,
                'cost': cost,
                'external_customer_order_id': '%{order_id}%'.format(order_id=external_customer_order_id or ''),
            }
            cr.execute(sql, params)
            res = cr.fetchall() or []

            if product_id and picking_id:
                params = {
                    'product_id': product_id,
                    'picking_id': picking_id
                }
                sql = """SELECT
                sol.price_unit
            FROM
                stock_move sm
              LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
            where 1=1
              and sm.picking_id = %(picking_id)s
              and sm.product_id = %(product_id)s"""
                cr.execute(sql, params)
                product_cost = cr.fetchone() or []
                if product_cost:
                    val['cost'] = product_cost[0]

            pickings = {x[0]: x[1] for x in res}
            picking_ids = pickings.keys()

            if len(pickings) == 1 and external_customer_order_id:
                picking_id = picking_ids[0]

            if picking_id and picking_id in picking_ids:
                val['picking_id'] = picking_id
                if return_target == 'source':
                    r_location_id = pickings[picking_id][0]
            domain['picking_id'].append(('id', 'in', picking_ids))

        r_location, r_bin = self.get_source_location_for_return(cr, uid, product_id, size_id, ret_wh.name, r_location_id=r_location_id, ignore_exceptions=True, context=context)

        if r_location and r_bin:
            complete_source_location = '%s    bin: %s' % (r_location and r_location.complete_name and re.sub('^.*?/(.*)', '\\1', r_location.complete_name), r_bin and r_bin.name)
        else:
            complete_source_location = 'Suitable stock ID is not found for chosen Warehouse!'

        val['complete_source_location'] = complete_source_location

        domain.update(self.location_change(cr, uid, ids, product_id, size_id, src_location_id)['domain'])

        return {'value': val, 'domain': domain}

    def location_change(self, cr, uid, ids, product_id=False, size_id=False, src_location_id=False, src_bin_id=False, context=None):
        val = {
            'product_id': product_id,
            'size_id': size_id,
            'src_location_id': src_location_id,
            'src_bin_id': src_bin_id,
        }
        domain = {}

        bin_ids = []
        if product_id and src_location_id:
            bin_ids = self.pool.get('product.product').get_mssql_product_bins(cr, uid, [product_id], [src_location_id], [size_id])

        domain['src_bin_id'] = [('id', 'in', bin_ids)]

        if not bin_ids or src_bin_id not in bin_ids:
            val['src_bin_id'] = False

        return {'value': val, 'domain': domain}

    def customer_sku_change(self, cr, uid, ids, customer_sku, partner_id=False, picking_id=False, qty=False, cost=False, batch_id=False, context=None):

        res = {
            'value': {},
        }

        product_id = False
        size_id = False
        if customer_sku:
            search_res = self.pool.get('product.product').search_product_by_cf(
                cr, uid,
                partner_id=partner_id,
                search_value=customer_sku,
                search_field=['customer_sku', 'upc', 'customer_id_delmar'],
                limit=1
            )
            if search_res and search_res[0]:
                product_id = search_res[0].get('product_id', False)
                size_id = search_res[0].get('size_id', False)
        res['value'].update({
            'size_id': size_id,
            'product_id': product_id,
            'customer_sku': customer_sku,
        })

        return res

    def line_id_change(self, cr, uid, ids, line_id, partner_id=False, batch_id=False, context=None):
        res = {
            'value': {
                'size_id': None,
                'product_id': None,
                'cost': 0.0,
                'qty': 0.0,
                'external_number': None,
                'picking_id': None,
            },
            'warning': {},
        }

        line_id = (line_id or '').strip()
        if line_id and len(line_id) > 4:

            cr.execute("""
                select
                    ol.product_id as product_id,
                    ol.size_id as size_id,
                    ol.price_unit as cost,
                    sm.product_qty as qty,
                    ol."merchantSKU" as external_number,
                    sp.id as picking_id
                from sale_order_line ol
                    left join stock_move sm on ol.id = sm.sale_line_id
                    left join stock_picking sp on sp.id = sm.picking_id
                    left join sale_order so on ol.order_id = so.id
                where 1=1
                    and sp.state = 'done'
                    and so.partner_id = %s
                    and (ol."optionSku" = %s or ol.external_customer_line_id = %s)
                limit 2
                """, (partner_id, line_id, line_id)
            )
            sql_res = cr.dictfetchall() or False

            if sql_res:
                if len(sql_res) == 1:
                    sql_res = sql_res[0]
                    res['value'].update({
                        'size_id': sql_res['size_id'] or None,
                        'product_id': sql_res['product_id'] or None,
                        'cost': sql_res['cost'] or 0.0,
                        'qty': sql_res['qty'] or 0.0,
                        'external_number': sql_res['external_number'] or None,
                        'picking_id': sql_res['picking_id'] or None,
                    })
                else:
                    res['warning'] = {
                        'title': _('Warning!'),
                        'message': 'Found several orders with this number. Please, select required fields manually.'
                    }

        return res

    def get_source_location_for_return(self, cr, uid, product_id, size_id, ret_wh_name, r_location_id=None, ignore_exceptions=False, context=None):
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')
        size_obj = self.pool.get('ring.size')
        server_obj = self.pool.get('fetchdb.server')
        product_obj = self.pool.get('product.product')
        warehouse_obj = self.pool.get('stock.warehouse')

        priority_location = None

        r_location = None
        r_bin = None
        if product_id:
            ret_stock = None
            product = product_obj.read(cr, uid, product_id, ['default_code'], context=context)
            def_code = product['default_code']
            size_name = size_obj.get_4digit_name(cr, uid, size_id, context=context)

            server_id = server_obj.get_sale_servers(cr, uid, single=True)

            if ret_wh_name:
                warehouse_ids = warehouse_obj.search(cr, uid, [('name', '=ilike', ret_wh_name)])
                if warehouse_ids:
                    wh = warehouse_obj.browse(cr, uid, warehouse_ids[0])
                    priority_location = wh.lot_release_id and wh.lot_release_id.name or None

            additional_params = ""
            priority = ""
            if r_location_id:
                r_location = loc_obj.browse(cr, uid, r_location_id)
                if r_location:
                    additional_params = "AND stockid = ?"
                    ret_wh_name = r_location.warehouse_id.name
                    ret_stock = r_location.name

            if priority_location:
                priority = "ORDER BY CASE WHEN stockid = ? THEN 0 ELSE 1 END"

            if server_id:
                search_wh_sql = """
                    select top 1
                        concat(id_delmar, '/', size),
                        warehouse,
                        stockid,
                        bin
                    from transactions
                    where id in (
                        select max(id)
                        from transactions
                        where 1=1
                            and status <> 'Posted'
                            and id_delmar = ?
                            and size = ?
                            and warehouse = ?
                            %s
                        group by id_delmar, size, warehouse, stockid, bin
                    )
                    %s
                """ % (additional_params, priority)

                params = [def_code, size_name, ret_wh_name]
                if ret_stock:
                    params.append(ret_stock)
                if priority_location:
                    params.append(priority_location)

                search_wh_res = server_obj.make_query(cr, uid, server_id, search_wh_sql, params=params, select=True, commit=False)
                if search_wh_res:

                    mssql_bin = search_wh_res[0][3] and search_wh_res[0][3].strip() or ''
                    if r_location is None:
                        mssql_wh = search_wh_res[0][1] and search_wh_res[0][1].strip() or ''
                        mssql_stockid = search_wh_res[0][2] and search_wh_res[0][2].strip() or ''

                        return_location_ids = loc_obj.search(cr, uid, [('complete_name', 'ilike', '%% %s / stock / %s' % (mssql_wh, mssql_stockid))], limit=0)
                        if return_location_ids:
                            r_location = loc_obj.browse(cr, uid, return_location_ids[0])

                    return_bin_ids = bin_obj.search(cr, uid, [('name', '=', mssql_bin)], limit=1)
                    if return_bin_ids:
                        r_bin_id = return_bin_ids[0]
                    else:
                        r_bin_id = bin_obj.create(cr, uid, {'name': mssql_bin})

                    r_bin = bin_obj.browse(cr, uid, r_bin_id)

                elif not ignore_exceptions:
                    raise osv.except_osv(_('Warning!'), 'Suitable stock ID is not found for chosen Warehouse!')

        return r_location, r_bin

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        return_id = super(stock_picking_return_line, self).create(cr, uid, vals, context=context)

        return_obj = self.browse(cr, uid, return_id)

        if not return_obj.qty or return_obj.qty <= 0:
            raise osv.except_osv(_('Warning!'), 'Qty must be a positive value!')

        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        bin_obj = self.pool.get('stock.bin')

        partner = return_obj.partner_id
        price_unit = return_obj.cost
        batch_id = context.get('target_batch_id', False)
        if batch_id:
            batch = self.pool.get('stock.picking.return.batch').browse(cr, uid, batch_id)
        else:
            batch = return_obj.batch_id

        return_obj_vals = {}

        ret_wh = batch.warehouse_id
        return_wh = 'usreturn'
        if (ret_wh.name in ('CAFER', 'CAMTL')):
            return_wh = 'careturn'

        # Default return Loc/bin
        r_bin = False
        r_location = False
        # FIXME: this is very bad way to search return bin
        return_location_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('complete_name', 'ilike', '%% %s %% stock %%/ return' % (return_wh))], limit=1)
        if return_location_ids:
            r_location = loc_obj.browse(cr, uid, return_location_ids[0])
        return_bin_ids = bin_obj.search(cr, uid, [('name', '=', 'RETURN')], limit=1)
        if return_bin_ids:
            r_bin = bin_obj.browse(cr, uid, return_bin_ids[0])

        # MNTL return Loc/bin (override the default)
        if return_obj.return_target == 'mntl':
            if return_obj.location_id.id or False:
                r_location = return_obj.location_id
            else:
                return_location_ids = loc_obj.search(cr, uid, [('usage', '=', 'internal'), ('complete_name', 'ilike', '%% %s %% stock %% mntlreturn' % (return_wh))], limit=0)
                if return_location_ids:
                    r_location = loc_obj.browse(cr, uid, return_location_ids[0])

        # Set Loc/bin for return into Source (override the default)
        if return_obj.return_target == 'source' and batch and batch.warehouse_id:
            if return_obj.src_location_id and return_obj.src_bin_id:
                r_location, r_bin = return_obj.src_location_id, return_obj.src_bin_id
            elif return_obj.src_location_id or return_obj.src_bin_id:
                raise osv.except_osv(_('Warning!'), 'If you want to return item not into the source location, please, specify both Location and Bin fields!')
            else:
                r_location, r_bin = self.get_source_location_for_return(
                    cr, uid,
                    return_obj.product_id.id, return_obj.size_id.id or None,
                    ret_wh.name,
                    ignore_exceptions=False,
                    context=context)

        picking_id = return_obj.picking_id.id
        if picking_id:
            # Return existing order
            cr.execute("""  SELECT
                                array_agg(sm.id) as move_ids,
                                sol.id as line_id,
                                sol.product_uom_qty as line_qty
                            FROM stock_move sm
                                LEFT JOIN sale_order_line sol ON sm.sale_line_id = sol.id
                            WHERE 1=1
                                AND sm.picking_id = %s
                                AND sol.product_id = %s
                                AND COALESCE(sol.size_id, 0) = %s
                            GROUP BY sol.id
                            ;
            """, (picking_id, return_obj.product_id.id, return_obj.size_id.id or 0))
            move_sql_res = cr.dictfetchall()

            ordered_qty = 0
            ol_ids = []
            move_ids = []
            for x in move_sql_res:
                move_ids += x['move_ids']
                ol_ids.append(x['line_id'])
                ordered_qty += x['line_qty']

            if move_ids:

                # Check: how much items returned before
                cr.execute("""
                    SELECT
                        STRING_AGG(CONCAT(name, ' (', qty, ')'), ', ') as return_details,
                        SUM(qty)::int as total_returned
                    FROM (
                        select sum(sm.product_qty) as qty, sp."name", sp.id
                        from stock_move sm
                            left join stock_picking sp on sp.id = sm.picking_id
                        where sm.sale_line_id IN %(line_ids)s
                            and sp.state = 'returned'
                        group by sp.id
                    ) a
                    HAVING %(ordered_qty)s - SUM(qty) < %(return_qty)s
                """, {
                    'line_ids': tuple(ol_ids),
                    'ordered_qty': ordered_qty,
                    'return_qty': return_obj.qty,
                })
                sql_res = cr.dictfetchone()
                if sql_res:
                    raise osv.except_osv(
                        'Warning!',
                        'Not enough items. Have been ordered {ordered_qty} item(s). Already returned {total_returned} item(s): {details}.'.format(
                            ordered_qty=int(ordered_qty), total_returned=sql_res['total_returned'], details=sql_res['return_details']
                        ))
            else:
                raise osv.except_osv(_('Warning!'), 'Stock move not found!')

            # Prepare data
            move_id = move_ids[0]
            move = move_obj.browse(cr, uid, move_id)
            user = self.pool.get('res.users').browse(cr, uid, uid)
            if return_obj.return_target == 'source' and not (return_obj.src_location_id and return_obj.src_bin_id):
                r_location, r_bin = self.get_source_location_for_return(
                    cr, uid,
                    return_obj.product_id.id, return_obj.size_id.id,
                    ret_wh.name,
                    r_location_id=move.location_id.id or None,
                    ignore_exceptions=False,
                    context=context)

            partial_data = {
                'delivery_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            }

            if not r_location:
                raise osv.except_osv(_('Warning!'), 'Can\'t find location for return!')

            partial_data['move%s' % (move.id)] = {
                'line_id': False,
                'product_id': return_obj.product_id.id or False,
                'product_qty': return_obj.qty or 0.0,
                'product_uom': move.product_uom.id or False,
                'prodlot_id': move.prodlot_id.id or False,
                'location_id': r_location and r_location.id or False,
                'bin_id': r_bin and r_bin.id or False,
            }
            size_postfix = move_obj.get_size_postfix(cr, uid, move.size_id)
            note = '%s \n%s %s: Return some products: \n' % (
                move.picking_id.report_history,
                time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                user.name
            )
            note += '%s %s/%s\ninto %s / %s\nBy reason: %s\n' % (
                move.product_id.default_code,
                size_postfix,
                return_obj.qty,
                r_location and r_location.complete_name or False,
                r_bin and r_bin.name or 'RETURN',
                'Return in batch %s' % (batch and batch.chargeback or False)
            )
            price_unit = move.sale_line_id and move.sale_line_id.price_unit or False
            picking_obj = self.pool.get('stock.picking')
            picking_obj.write(cr, uid, move.picking_id.id, {'report_history': note})

            context.update({'from_batch_return': True})
            res_return = picking_obj.do_only_return(cr, uid, [move.picking_id.id], partial_data, context=context)

            if not res_return:
                raise osv.except_osv(_('Warning!'), _('Something went wrong. Please try again'))
            # Change picking id if there has been done a partial return creating a new picking
            new_picking_id = res_return.get(picking_id, {}).get('returned_picking')
            if new_picking_id and new_picking_id != picking_id:
                return_obj_vals.update({'picking_id': new_picking_id})

        else:

            if not price_unit or price_unit <= 0:
                raise osv.except_osv(_('Warning!'), 'Price must be a positive value!')

            onfly_obj = self.pool.get('stock.return.onfly.wizard')
            order_number = len(batch.line_ids or [])
            order_name = '%s-%s' % (batch.chargeback, order_number)
            while self.pool.get('sale.order').search(cr, uid, [('po_number', '=', order_name), ('partner_id', '=', partner.id or False)], limit=1):
                order_number += 1
                order_name = '%s-%s' % (batch.chargeback, order_number)

            onfly_vals = {
                'tracking_ref': batch.tracking_ref,
                'partner_id': partner.id or False,
                'type_api_id': partner.type_api_ids and partner.type_api_ids[0].id or False,
                'name': order_name or False,
                'external_customer_line_id': order_name or False,
                'product_id': return_obj.product_id.id or False,
                'description': return_obj.product_id.name,
                'size_id': return_obj.size_id.id or False,
                'product_qty': return_obj.qty,
                'price_unit': price_unit,
                'location_id': r_location and r_location.id or False,
                'bin_id': r_bin and r_bin.id or False,
                'return_batch_id': batch.id or False,
                'return_batch_line_id': return_obj.id or False,
            }

            onfly_id = onfly_obj.create(cr, uid, onfly_vals)
            res_return = onfly_obj.create_and_return(cr, uid, [onfly_id])
            if not res_return:
                raise osv.except_osv(_('Warning!'), _('Something went wrong. Please try again'))

        return_obj_vals.update({
            'batch_id': batch.id or False,
            'cost': price_unit or False,
            'state': 'done',
            })
        get_sku_sql = """   SELECT
                                    distinct cf."value"
                            FROM
                                            product_multi_customer_names cn
                                INNER JOIN  product_multi_customer_fields_name fn on (
                                    fn.name_id = cn.id AND fn.customer_id = %s
                                )
                                INNER JOIN  product_multi_customer_fields cf on (
                                    cf.field_name_id = fn.id
                                    AND cf.product_id = %s
                                    AND COALESCE(cf.size_id, 0) = %s
                                )
                            WHERE 1=1
                                AND cn.name = 'customer_sku'
                                AND (cf."value" is not null and trim(cf."value") != '')
                            ;
        """ % (partner.id, return_obj.product_id.id, return_obj.size_id.id or 0)
        cr.execute(get_sku_sql)
        sku_res = cr.fetchone()
        if sku_res and sku_res[0]:
            return_obj_vals.update({
                'external_number': sku_res[0],
                })

        if r_location:
            return_obj_vals.update({
                'target_location_id': r_location and r_location.id or False,
                'target_bin_id': r_bin and r_bin.id or False,
                })

        return_obj.write(return_obj_vals)
        return return_id

    def create_return(self, cr, uid, ids, context=None):
        res = {'type': 'ir.actions.act_window_close'}

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        if len(ids) != 1:
            raise osv.except_osv(_('Warning!'), 'May only be done for one return at a time!')

        return_id = ids[0]
        return_obj = self.browse(cr, uid, return_id)

        if return_obj.return_target == 'mntl':
            if return_obj.location_id:
                res = self.pool.get('ir.notification').info(cr, uid, 'Return', """Please return %s to MNTL!\nItem in %s""" % (return_obj.product_id.default_code, return_obj.location_id.complete_name))
            return res

        # Open popup
        if context.get('save_and_print', False):
            res = self.action_print_batch_return_line(cr, uid, ids)
        elif context.get('save_and_new', False):
            act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_stock_picking_return_line')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
            res = act_win

        return res

    def action_print_batch_return_line(self, cr, uid, ids, context=None):
        picking_ids = [x['picking_id'][0] for x in self.read(cr, uid, ids, ['picking_id']) if x['picking_id']]

        if picking_ids:
            datas = {
                'id': picking_ids[0],
                'ids': picking_ids,
                'model': 'stock.picking',
                'report_type': 'pdf',
                }
            act_print = {
                'type': 'ir.actions.report.xml',
                'report_name': 'stock.picking.picking_label',
                'datas': datas,
                'context': {'active_ids': picking_ids, 'active_id': picking_ids[0]}
            }
            return act_print

stock_picking_return_line()


class stock_picking_return_settings(osv.osv):
    _name = "stock.picking.return.settings"
    _rec_name = "location_id"
    _columns = {
        'location_id': fields.many2one(
            'stock.location', 'Location',
            select=True, domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],
            required=True,
            ),
        'cost': fields.float('Cost', ),
        'location_dest_id': fields.many2one(
            'stock.location', 'Dest. Location',
            select=True, domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],
            ),
        'priority': fields.integer('Priority', ),
    }

    _defaults = {
        'priority': 0,
    }

    _order = "location_id, priority, cost desc"

stock_picking_return_settings()
