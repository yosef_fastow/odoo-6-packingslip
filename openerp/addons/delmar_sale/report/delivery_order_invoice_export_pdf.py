# -*- coding: utf-8 -*-
##############################################################################

import time
from report import report_sxw


class delivery_order_invoice_export_pdf(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(delivery_order_invoice_export_pdf, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
             'time': time,
             'get_group_amount': self._get_group_amount,
             'get_total_amount': self._get_total_amount,
             'country': context.get('country'),
             'from_addr': context.get('from_addr'),
             'to_addr': context.get('to_addr'),
         })

    def _get_group_amount(self, report_group):
        total = 0.0
        total_qty = 0.0
        for line in report_group.report_line_ids:
            qty = line.product_qty or 0.0
            total_qty += qty
            cost = line.cost or 0.0
            total += qty * cost
        return {'amount': total, 'qty': total_qty}


    def _get_total_amount(self, report):
        total = 0.0
        total_qty = 0.0
        for group in report.report_group_ids:
            group_amount = self._get_group_amount(group)
            total += group_amount['amount']
            total_qty += group_amount['qty']
        return {'amount': total, 'qty': total_qty}
       
report_sxw.report_sxw(
    'report.delivery.order.invoice.export.pdf',
    'stock.invoice.report',
    'addons/delmar_sale/report/delivery_order_invoice_export_pdf.rml',
    parser=delivery_order_invoice_export_pdf,
    # header=True
)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
