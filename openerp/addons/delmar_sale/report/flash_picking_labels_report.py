# -*- coding: utf-8 -*-
##############################################################################
import re
from report import report_sxw
from osv import osv
import logging
import requests
import base64

_logger = logging.getLogger(__name__)

class flash_picking_labels_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(flash_picking_labels_report, self).__init__(cr, uid, name, context=context)
        _logger.info('INIT FLASH PICKING context: %s' % context)
        self.localcontext.update({
            'get_user_uid': self._get_user_uid,
            'get_product_image': self._get_product_image,
        })

    def repeatIn(self, lst, name, nodes_parent=False):
        ret_lst = []
        ret_ids = []
        label_obj = self.pool.get('flash.sale.order.picking.label')
        sale_line_obj = self.pool.get('sale.order.line')
        sale_obj = self.pool.get('sale.order')
        product_obj = self.pool.get('product.product')

        if name == 'order':
            ret_lst.extend([{name: sale_obj.browse(self.cr, self.uid, self.localcontext.get('active_id'))}])

        elif name == 'line':
            _logger.info("Iterating for lines")
            order = sale_obj.browse(self.cr, self.uid, self.localcontext.get('active_id'))
            line_ids = self.localcontext.get('selected_ids')
            cmp_lines_ids = sale_line_obj.search(self.cr, self.uid, [('id', 'in', line_ids), ('is_set', '=', True)])
            set_products = []
            for src_line in sale_line_obj.browse(self.cr, self.uid, cmp_lines_ids):
                set_products.append(src_line.set_product_id.id)

            set_search_domain = [('product_id', 'in', set_products), ('is_set', '=', True)]
            if order.state == 'done':
                set_search_domain.append(('order_id', '=', order.id))
            else:
                set_search_domain.append(('set_parent_order_id', '=', order.id))
                set_search_domain.append(('order_id', '=', False))

            set_lines = sale_line_obj.search(self.cr, self.uid, set_search_domain)
            #_logger.info("set lines found: %s" % set_lines)

            common_lines = sale_line_obj.search(self.cr, self.uid, [('id', 'in', line_ids), ('order_id', '=', order.id), ('is_set', '=', False)])
            #_logger.info("common lines found: %s" % common_lines)

            # preparing set lines
            for line in sale_line_obj.browse(self.cr, self.uid, set_lines):
                cmp_search_domain = [('external_customer_line_id', '=', line.external_customer_line_id), ('set_product_id', '=', line.product_id.id), ('is_set', '=', True)]
                if order.state == 'done':
                    cmp_search_domain.append(('set_parent_order_id', '=', order.id))
                else:
                    cmp_search_domain.append(('order_id', '=', order.id))
                cmp_line_ids = sale_line_obj.search(self.cr, self.uid, cmp_search_domain)
                warehouse_parts = []
                location_parts = []
                bin_parts = []
                for cmp_sale_line in sale_line_obj.browse(self.cr, self.uid, cmp_line_ids):
                    warehouse_parts.append(cmp_sale_line.location_and_bin and cmp_sale_line.location_and_bin[0] or '')
                    location_parts.append(cmp_sale_line.location_and_bin and len(cmp_sale_line.location_and_bin) >= 3 and cmp_sale_line.location_and_bin[2] or '')
                    bin_parts.append("{}x{}:{}".format(int(cmp_sale_line.product_uom_qty), cmp_sale_line.product_id.default_code, cmp_sale_line.location_and_bin and len(cmp_sale_line.location_and_bin) >= 4 and cmp_sale_line.location_and_bin[3] or ''))
                    #_logger.warn("LOCATION AND BIN FOR SET: %s" % cmp_sale_line.location_and_bin)

                report_vals = {
                    'product_id': line.product_id.id,
                    'product_image': self._get_product_image(line),
                    #'sku': product_obj.get_val_by_label(self.cr, self.uid, line.product_id.id, order.partner_id.id, 'Customer SKU', line.size_id.id),
                    'sku': line.merchantSKU or None,
                    'size_id': line.size_id and line.size_id.id or None,
                    'warehouse': " ".join(warehouse_parts),
                    'location': " ".join(location_parts),
                    'bin': " ".join(bin_parts),
                    'qty': line.product_uom_qty,
                }

                report_line_id = label_obj.create(self.cr, self.uid, report_vals, context=None)
                #ret_lst.extend([{name: label_obj.browse(self.cr, self.uid, report_line_id)}])
                ret_ids.append(report_line_id)

            # preparing common lines
            for line in sale_line_obj.browse(self.cr, self.uid, common_lines):
                report_vals = {
                    'product_id': line.product_id.id,
                    'product_image': self._get_product_image(line),
                    #'sku': product_obj.get_val_by_label(self.cr, self.uid, line.product_id.id, line.order_id.partner_id.id, 'Customer SKU', line.size_id.id),
                    'sku': line.merchantSKU or None,
                    'size_id': line.size_id and line.size_id.id or None,
                    'warehouse': line.location_and_bin and line.location_and_bin[0] or '',
                    'location': line.location_and_bin and len(line.location_and_bin) >= 3 and line.location_and_bin[2] or '',
                    'bin': line.location_and_bin and len(line.location_and_bin) >= 4 and line.location_and_bin[3] or '',
                    'qty': line.product_uom_qty,
                }
                #_logger.warn("LOCATION AND BIN FOR COMMON: %s" % line.location_and_bin)
                report_line_id = label_obj.create(self.cr, self.uid, report_vals, context=None)
                #ret_lst.extend([{name: label_obj.browse(self.cr, self.uid, report_line_id)}])
                ret_ids.append(report_line_id)

            # re-sort records by bin
            resorted_ids = label_obj.search(self.cr, self.uid, [('id', 'in', ret_ids)], order='bin ASC')
            for plabel in label_obj.browse(self.cr, self.uid, resorted_ids):
                ret_lst.extend([{name: plabel}])
        #_logger.warn('PDF ret_lst: %s' % ret_lst)
        return ret_lst

    def _get_user_uid(self, context=None):
        if context is None:
            context = {}

        user_obj = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)

        return user_obj.name

    def _get_product_image(self, line):
        """Returns image of product in base64 format.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return image in base64 format."""
        result = None
        if line:
            item = line.product_id.default_code.lower()
            sizes = dict(small='85', medium='185')
            conf_obj = self.pool.get('ir.config_parameter')
            #host = 'https://catalog.delmarintl.ca'
            host = conf_obj.get_param(self.cr, self.uid, 'product.catalog.host')
            image_url = '{}/themes/progforce/catalog/{}/{}.jpg'.format(host, sizes['medium'], item)
            response = requests.get(image_url)
            result = base64.encodestring(response.content)
        return result

report_sxw.report_sxw(
    'report.sale.order.line.flash_picking_labels_report',
    'sale.order',
    'addons/delmar_sale/report/flash_picking_labels_report.rml',
    parser=flash_picking_labels_report,
    header=False
)
