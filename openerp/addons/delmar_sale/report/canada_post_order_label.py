
# -*- coding: utf-8 -*-
##############################################################################
import os
import re
import requests
import base64
import tools

from report import report_sxw


class canada_post_order_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(canada_post_order_labels, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_company_name': self._get_company_name,
            'get_order_name': self._get_order_name,
            'get_street_name': self._get_street_name,
            'get_company_addr': self._get_company_addr,
            'get_customer_name': self._get_customer_name,
            'get_customer_street': self._get_customer_street,
            'get_customer_addr': self._get_customer_addr,
            'get_logo': canada_post_order_labels._get_logo,
        })
        self.company_info = None
        self.partner_info = None

    def _get_company_info(self, picking):
        """Returns company info for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return company info, i.e. company, name, street, and address."""
        sql = """   SELECT
                        company,
                        sp.name AS name,
                        rpa.street AS street,
                        CONCAT( rpa.city, ' ', rcs.code, ' ', rpa.zip ) AS address
                    FROM
                        stock_picking sp
                    INNER JOIN res_partner rp ON
                        sp.real_partner_id = rp.id
                    INNER JOIN res_partner_address rpa ON
                        rp.id = rpa.partner_id
                    LEFT JOIN res_country_state rcs ON
                        rcs.id = rpa.state_id
                    WHERE
                        rpa.type = 'return'
                        AND sp.name = '%s';
        """ % picking.name
        self.cr.execute(sql)
        self.company_info = self.cr.dictfetchone()

    def _get_partner_info(self, picking):
        """Returns customer info for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return customer info, i.e. name, street, and address."""
        sql = """   SELECT
                        rpa.name,
                        CONCAT(rpa.street,' ',rpa.street2, ' ', rpa.street3) as street,
                        CONCAT( rpa.city, ' ', rcs.code, ' ', rpa.zip ) AS address
                    FROM
                        stock_picking sp
                    INNER JOIN res_partner_address rpa ON
                        sp.address_id = rpa.id
                    LEFT JOIN res_country_state rcs ON
                        rcs.id = rpa.state_id
                    WHERE
                        sp.name = '%s';
        """ % picking.name
        self.cr.execute(sql)
        self.partner_info = self.cr.dictfetchone()

    def _get_customer_name(self, picking):
        """Returns customer name for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return customer address."""
        if not self.partner_info:
            self._get_partner_info(picking)
        return self.partner_info and '{}'.format(self.partner_info.get('name', '').encode('utf8')) or None

    def _get_customer_street(self, picking):
        """Returns customer street for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return customer street."""
        if not self.partner_info:
            self._get_partner_info(picking)
        return self.partner_info and '{}'.format(self.partner_info.get('street', '').encode('utf8')) or None

    def _get_customer_addr(self, picking):
        """Returns customer address for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return customer address."""
        if not self.partner_info:
            self._get_partner_info(picking)
        return self.partner_info and '{}'.format(self.partner_info.get('address', '').encode('utf8')) or None

    def _get_company_name(self, picking):
        """Returns company name for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return company name."""
        if not self.company_info:
            self._get_company_info(picking)
        return self.company_info and '{company}'.format(**self.company_info) or None

    def _get_order_name(self, picking):
        """Returns order name for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return order name."""
        if not self.company_info:
            self._get_company_info(picking)
        return self.company_info and '{name}'.format(**self.company_info) or None

    def _get_street_name(self, picking):
        """Returns company street for selected order.
        @:param picking - line in osv orm model, usually stock.move instance.
        @:return company street."""
        if not self.company_info:
            self._get_company_info(picking)
        return self.company_info and '{street}'.format(**self.company_info) or None

    def _get_company_addr(self, picking):
        """Returns company address for selected order line.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return company address."""
        if not self.company_info:
            self._get_company_info(picking)
        return self.company_info and '{address}'.format(**self.company_info) or None

    @staticmethod
    def _get_logo():
        """Returns company logo.
        @:return company logo as base64 string."""
        return open(os.path.join(tools.config['root_path'], 'addons', 'base', 'res', 'canada_post_logo.png'),
                    'rb').read().encode('base64')


report_sxw.report_sxw(
    'report.canada.post.order.labels',
    'stock.picking',
    'addons/delmar_sale/report/canada_post_order_label.rml',
    parser=canada_post_order_labels,
    header=False
)
