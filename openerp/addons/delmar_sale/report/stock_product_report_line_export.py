# -*- coding: utf-8 -*-
##############################################################################

import csv
import time
import base64
import datetime
from osv import osv, fields
from cStringIO import StringIO
import logging


logger = logging.getLogger('delmar_sale')


class stock_product_report_line_export(osv.osv):
    _name = 'stock.product.report.line.export'

    _columns = {
        'create_date': fields.datetime('Date', required=True, select=True),
        'csv_file': fields.binary(string='CSV Export', readonly=True),
        'csv_filename': fields.char('', size=256),
    }

    # Cron function
    def cron_truncate_table(self, cr, uid, context=None):
        logger.info('Archive stock_product_report_line table.')
        cr.execute("""SELECT *
            FROM stock_product_report_line sprl
            LEFT JOIN stock_product_move sm ON sm.line_id=sprl.id
            LEFT JOIN resease_stock_product_report_line_rel sr ON sr.line_id=sprl.id
            WHERE sm.line_id IS NOT NULL
              OR sr.line_id IS NOT NULL;
        """)
        res = cr.dictfetchall()

        if not res:
            return False

        buf = StringIO()
        writer = csv.writer(buf, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, lineterminator='\n')
        writer.writerow(res[0].keys())
        writer.writerows(map(lambda x: x.values(), res))
        out = base64.encodestring(buf.getvalue())
        create_date = datetime.datetime.now()
        self.write(cr, uid, self.create(cr, uid, {}, context), {'create_date': create_date,
                                                                'csv_file': out,
                                                                'csv_filename': 'stock_product_report_line_{}.csv'.\
                                                                format(datetime.datetime.strftime(create_date,
                                                                                                  '%Y-%m-%d_%H-%M-%S')),
                                                                }, context=context)

        logger.info('Truncate stock_product_report_line table.')
        cr.execute('TRUNCATE stock_product_report_line CASCADE;')
        return True


stock_product_report_line_export()
