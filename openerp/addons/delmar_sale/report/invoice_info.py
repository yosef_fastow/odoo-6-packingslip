# -*- coding: utf-8 -*-
##############################################################################

import time
from report import report_sxw


class invoice_info(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(invoice_info, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_group_amount': self._get_group_amount,
            'get_total_amount': self._get_total_amount,
        })
        self.rml_header = '''
<header>
<pageTemplate>
    <frame id="first" x1="1.3cm" y1="2.5cm" height="22cm" width="19.0cm"/>
    <pageGraphics>
        <fill color="black"/>
        <stroke color="black"/>
        <setFont name="Arial Bold" size="10"/>
        <drawCentredString x="11.1cm" y="28.3cm">INVOICE</drawCentredString>
        <drawString x="14.0cm" y="28.3cm"> DATE: [[ formatLang(time.strftime("%Y-%m-%d"), date=True) ]] </drawString>
        <drawString x="14.0cm" y="27.9cm"> INV: FCDC </drawString>

        <drawString x="2.0cm" y="27.3cm"> FROM:FIRST CANADIAN DIAMOND CUTTING WORKS INC </drawString>
        <drawString x="2.0cm" y="26.9cm"> 4058, Jean-Talon W. ,Suite 100 </drawString>
        <drawString x="2.0cm" y="26.5cm"> Montreal, Quebec, H4P 1V5 </drawString>

        <drawString x="14.0cm" y="27.3cm"> TO:DELMAR MANUFACTURING LLC. </drawString>
        <drawString x="14.0cm" y="26.9cm"> 1083, Main Street </drawString>
        <drawString x="14.0cm" y="26.5cm"> Champlain, NY </drawString>
        <drawString x="14.0cm" y="26.1cm"> 11219.U.S.A. </drawString>
        <drawString x="14.0cm" y="25.7cm"> TAX ID # 75-3264433 </drawString>

    </pageGraphics>
</pageTemplate>
</header>
'''

    def _get_group_amount(self, report_group):
        total = 0.0
        for line in report_group.report_line_ids:
            qty = line.product_qty or 0.0
            cost = line.cost or 0.0
            total += qty * cost
        return {'amount': total}

    def _get_total_amount(self, report):
        total = 0.0
        for group in report.report_group_ids:
            total += self._get_group_amount(group)['amount']
        return {'amount': total}

report_sxw.report_sxw(
    'report.stock.picking.invoice.info',
    'stock.invoice.report',
    'addons/delmar_sale/report/invoice_info.rml',
    parser=invoice_info,
    # header=True
    )

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
