# -*- coding: utf-8 -*-
from osv import fields, osv
import xlwt
import cStringIO
import base64
from datetime import datetime


class delivery_order_pick_invoice_export(osv.osv):
    _name = "delivery.order.pick.invoice.export"
    _columns = {
        "xls_file": fields.binary(string="CSV Export", readonly=True),
        "xls_filename": fields.char("", size=256),
        'state': fields.boolean("state")
    }

    _defaults = {
        'state': False,
        'xls_file': ''
    }

    def _write_line(self, sheet, row, datas=None):
        if not datas:
            datas = []
        for data in datas:
            for i in xrange(0, 19):
                sheet.write(row, i, data.get('s' + str(i), '') or '')
            row += 1
        return row

    def action_invoice_report_export(self, cr, uid, ids, context=None):

        active_ids = context.get('active_ids', [])

        pick_invoice_report = xlwt.Workbook()
        sheet = pick_invoice_report.add_sheet("pick_invoice_report")

        row = 0
        row = self._write_line(sheet, row, [{
            's0': 'Order Name',
            's1': 'Ship From',
            's2': 'Ship To',
            's3': 'Order No',
            's4': 'Carrier',
            's5': 'Date of Order',
            's6': 'PRODUCT NO.',
            's7': 'PRODUCT NO.2',
            's8': 'Description',
            's9': 'Country of origin',
            's10': 'QTY',
            's11': 'Price Unit',
            's12': 'Price',
            's13': 'Amount',
            's14': 'Weight',
            's15': 'Tracking Number',
            's16': 'Customer Order Number',
            's17': 'Tariff',
            's18': 'Ship Date',
        }])

        for picking in self.pool.get('stock.picking').browse(cr, uid, active_ids):
            ship_from = ""
            if picking.ship_from:
                ship_from += picking.ship_from.name or ''
                ship_from += " " + (picking.ship_from.street or '')
                ship_from += picking.ship_from.street2 and " " + picking.ship_from.street2 or ''
                ship_from += picking.ship_from.city and " " + picking.ship_from.city or ''
                ship_from += picking.ship_from.state_id and " " + picking.ship_from.state_id.name or ''
                ship_from += picking.ship_from.zip and " " + picking.ship_from.zip or ''
                ship_from += picking.ship_from.country_id and " " + picking.ship_from.country_id.name or ''
            ship_to = ""
            if picking.address_id:
                ship_to += picking.address_id.name or ''
                ship_to += " " + (picking.address_id.street or '')
                ship_to += picking.address_id.street2 and " " + picking.address_id.street2 or ''
                ship_to += picking.address_id.city and " " + picking.address_id.city or ''
                ship_to += picking.address_id.state_id and " " + picking.address_id.state_id.name or ''
                ship_to += picking.address_id.zip and " " + picking.address_id.zip or ''
                ship_to += picking.address_id.country_id and " " + picking.address_id.country_id.name or ''

            time_format = "%Y-%m-%d %H:%M:%S"
            if '.' in picking.date:
                time_format += '.%f'

            pick_row = {
                's0': picking.name.replace('/', '') or '',
                's1': ship_from,
                's2': ship_to,
                's3': picking.origin_external_customer_order_id or '',
                's4': picking.carrier or 'USPS First Class',
                's5': datetime.strptime(picking.date, time_format).strftime("%Y/%m/%d"),
                's14': '1 pound',
                's15': picking.tracking_ref or '',
                's16': picking.sale_id.cust_order_number or '',
                's18': picking.shp_date or '',
            }

            if picking.is_set and picking.state not in ('done',):
                sm_line_ids = self.pool.get('stock.move').search(cr, uid, [('set_parent_order_id', '=', picking.id)])
                sm_lines = self.pool.get('stock.move').browse(cr, uid, sm_line_ids)
                picking.move_lines = filter(lambda x: not x.is_set, picking.move_lines) + sm_lines

            for move_lines in picking.move_lines:

                pick_row.update({
                    's6': move_lines.sale_line_id.optionSku or move_lines.sale_line_id.item_line_id or move_lines.sale_line_id.external_customer_line_id or '',
                    's7': move_lines.product_id.default_code + (move_lines.sale_line_id.size_id and ('/' + move_lines.sale_line_id.size_id.name) or ''),
                    's8': move_lines.sale_line_id.name,
                    's9': move_lines.product_id.made_in and move_lines.product_id.made_in.name or '',
                    's10': str(move_lines.product_qty).replace('.000', '') + ' ' + str(move_lines.product_uom.name),
                    's11': move_lines.sale_line_id.price_unit,
                    's12': move_lines.sale_line_id.customerCost,
                    's13': float(move_lines.sale_line_id.customerCost) * float(move_lines.product_qty),
                    's17':move_lines.product_id.product_tmpl_id.prod_tarif_classification or '',
                })
                row = self._write_line(sheet, row, [pick_row])

        buf = cStringIO.StringIO()
        pick_invoice_report.save(buf)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        return self.write(cr, uid, ids, {'state': True, 'xls_file': out, 'xls_filename': 'pick_invoice_report.xls'}, context=context)

delivery_order_pick_invoice_export()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
