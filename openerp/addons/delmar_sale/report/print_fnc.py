import openerp.report.print_fnc as print_fnc
import time

print_fnc.functions['label_time'] = lambda x: time.strftime('%m-%d-%y %H:%m', time.localtime()).decode('latin1')
