# -*- coding: utf-8 -*-
##############################################################################

import time
from report import report_sxw


class stock_onhold_task(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(stock_onhold_task, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_stock': self._get_stock,
            'lines_sort_by_bin': self._lines_sort_by_bin,
        })

    def _get_stock(self, line, context=None):

        move_obj = self.pool.get('stock.move')
        task_line_obj = self.pool.get('stock.onhold.task.line')

        warehouse = line.task_id.warehouse_id and line.task_id.warehouse_id.name or False
        stockid = line.task_id.location_id and line.task_id.location_id.name or False

        delmarid = line.product_id and line.product_id.default_code or False
        size_postfix = move_obj.get_size_postfix(self.cr, self.uid, line.size_id)
        itemid = delmarid and (delmarid + '/' + size_postfix) or False

        res = task_line_obj.get_stock(self.cr, self.uid, itemid=itemid, warehouse=warehouse, stockid=stockid, context=context)

        return res

    def _lines_sort_by_bin(self, line_ids):
        return sorted(line_ids, key=lambda line: line.loc_ids and line.loc_ids.bin)

report_sxw.report_sxw(
    'report.stock.onhold',
    'stock.onhold.task',
    'addons/delmar_sale/report/stock_onhold.rml',
    parser=stock_onhold_task,
    # header=True
)

report_sxw.report_sxw(
    'report.stock.onhold.task.item.list',
    'stock.onhold.task',
    'addons/delmar_sale/report/stock_onhold_task_item_list.rml',
    parser=stock_onhold_task,
    # header=True
)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
