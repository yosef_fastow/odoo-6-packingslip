# -*- coding: utf-8 -*-
from osv import fields, osv


class picking_combine_orders(osv.osv):
    _name = "picking.combine.orders"
    _columns = {
        "exclude_s2s": fields.boolean('Exclude Ship to Store', help='Exclude Ship to Store orders when combine them.', store=False,),
    }

    _defaults = {
        'exclude_s2s': True,
    }

    def action_combine_orders(self, cr, uid, ids, context=None):
        return self.pool.get('stock.picking').action_combine_stock_pickings(cr, uid, ids, context)

picking_combine_orders()
