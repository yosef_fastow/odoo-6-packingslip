# -*- coding: utf-8 -*-
from osv import fields, osv
import base64
import csv
import cStringIO
from tools.translate import _

THRESHOLD_PRICE = 800


class delivery_order_pick_simple_report(osv.osv_memory):
    _name = "delivery.order.pick.simple.report"
    _columns = {
        "file": fields.binary(string="CSV Export", readonly=True),
        "filename": fields.char("", size=256),
        'state': fields.boolean("state")
    }

    _defaults = {
        'state': False,
        'file': ''
    }

    def _write_lines(self, lines, context=None):
        if not lines:
            lines = []
        quote_style = csv.QUOTE_ALL
        output = cStringIO.StringIO()
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
        writer = csv.writer(output, dialect='csv', quoting=quote_style)
        for line in lines:
            writer.writerow([isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in line])
        return output

    def action_generate_report(self, cr, uid, ids, context=None):
        picking_obj = self.pool.get('stock.picking')
        active_ids = context.get('active_ids', [])
        filtered_ids = picking_obj.search(cr, uid, [('tracking_ref', '!=', False), ('id', 'in', active_ids)])
        # filtered_ids = picking_obj.search(cr, uid, [('address_id.country_id.code', '=', 'US'), ('id', 'in', active_ids)])
        if not filtered_ids:
            raise osv.except_osv(_('Error'), 'Need select at least one order what has a tracking#.')

        # DLMR-418 start
        lines, orders = picking_obj.action_simple_report(cr, uid, filtered_ids, context=None)

        # If #orders > 0 with prices above $800, show warning and disable download report
        if orders:
            raise osv.except_osv('Warning!', 'Orders with prices above $800: {}'.format(orders))
        # DLMR-418 end

        buf = self._write_lines(lines)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        return self.write(cr, uid, ids, {'state': True, 'file': out, 'filename': '321 report.csv'}, context=context)

delivery_order_pick_simple_report()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
