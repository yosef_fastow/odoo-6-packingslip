# -*- coding: utf-8 -*-
##############################################################################
import re
from report import report_sxw
from osv import osv
import logging
import requests
import base64

_logger = logging.getLogger(__name__)

class mass_return_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(mass_return_labels, self).__init__(cr, uid, name, context=context)

        if len(self.localcontext.get('return_line_ids', 0)) == 0:
            raise osv.except_osv('Warning!', 'No returned lines found for labels printing!')

        if context.get('type', False):
            self.localcontext.update({'label_type': context.get('type')})

        self.localcontext.update({
            'get_made_in': self._get_made_in,
            'get_user_uid': self._get_user_uid,
            'get_product_image': self._get_product_image,
        })

    def repeatIn(self, lst, name, nodes_parent=False):
        ret_lst = []
        if name == 'label':
            _logger.info('for LABEL: %s' % lst)
            label_obj = self.pool.get('return.item.label')
            serial_obj = self.pool.get('stock.picking.return.label.serial')
            for mass in lst:
                for line in mass.return_line_ids:
                    if self.localcontext.get('partial_print', False) and line.id not in self.localcontext.get('return_line_ids'):
                        continue
                    if line.is_set and line.verified_line_id.set_components_line_ids and len(line.verified_line_id.set_components_line_ids) > 0:
                        for cmp_line in line.verified_line_id.set_components_line_ids:
                            for num in range(1, int(cmp_line.qty_to_return) + 1):
                                # DLMR-1266
                                # Define serial for label
                                size_id = cmp_line.size_id and cmp_line.size_id.id or None
                                parent_size_id = line.size_id and line.size_id.id or None
                                label_serial = serial_obj.define_for_label(self.cr, self.uid, picking_id=line.picking_id.id, product_id=cmp_line.product_id.id, size_id=size_id, qty=line.qty, set_component=True, num=num)
                                label_vals = {
                                    'product_id': cmp_line.product_id.id,
                                    'product_image': '',
                                    'size_id': size_id,
                                    'bin_id': cmp_line.return_bin.id,
                                    'serial': label_serial or '',
                                }
                                label_id = label_obj.create(self.cr, self.uid, label_vals, context=None)
                                label = label_obj.browse(self.cr, self.uid, label_id)
                                # ret_lst.extend([{name: label}] * int(cmp_line.qty_to_return))
                                ret_lst.append({name: label})
                    else:
                        for num in range(1, int(line.qty) + 1):
                            # DLMR-1266
                            # Define serial for label
                            size_id = line.size_id and line.size_id.id or None
                            label_serial = serial_obj.define_for_label(self.cr, self.uid, picking_id=line.picking_id.id, product_id=line.product_id.id, size_id=size_id, qty=line.qty, num=num)
                            label_vals = {
                                'product_id': line.product_id.id,
                                'product_image': '',
                                'size_id': size_id,
                                'bin_id': line.target_bin_id.id,
                                'serial': label_serial or '',
                            }
                            label_id = label_obj.create(self.cr, self.uid, label_vals, context=None)
                            label = label_obj.browse(self.cr, self.uid, label_id)
                            # ret_lst.extend([{name: label}] * int(line.qty))
                            ret_lst.append({name: label})
        else:
            _logger.warn('Not for PICKING: %s' % lst)
            for id in lst:
                ret_lst.append({name: id})
        _logger.warn('PDF ret_lst: %s' % ret_lst)
        return ret_lst

    def _get_made_in(self, context=None):
        made_in = 'China'
        if self.localcontext.get('label_type', False):
            _logger.warn('GET MADE IN context: %s' % self.localcontext.get('label_type'))
            made_in = self.localcontext.get('label_type')
        return made_in

    def _get_user_uid(self, context=None):
        if context is None:
            context = {}

        user_obj = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)

        return user_obj.name

    def _get_product_image(self, line):
        """Returns image of product in base64 format.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return image in base64 format."""
        result = None
        if line:
            item = line.product_id.default_code.lower()
            sizes = dict(small='85', medium='185')
            conf_obj = self.pool.get('ir.config_parameter')
            #host = 'https://catalog.delmarintl.ca'
            host = conf_obj.get_param(self.cr, self.uid, 'product.catalog.host')
            image_url = '{}/themes/progforce/catalog/{}/{}.jpg'.format(host, sizes['medium'], item)
            _logger.info("GOT IMAGE URL: %s" % image_url)
            response = requests.get(image_url)
            result = base64.encodestring(response.content)
        return result


report_sxw.report_sxw(
    'report.stock.picking.mass_return_label',
    'stock.picking.mass.return',
    'addons/delmar_sale/report/stock_mass_return_label.rml',
    parser=mass_return_labels,
    header=False
)
