# -*- coding: utf-8 -*-
##############################################################################

from report import report_sxw
from pf_utils.utils.helper import _get_one
from openerp.addons.delmar_pricing.stock_product_pricelist import pricelist_fn


class stock_move_sr_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(stock_move_sr_labels, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_customer_field': self._get_customer_field,
            'get_price': self._get_price,
        })

    def recalculate_price_hook(self, cr, uid, partner_id, product_id, size_id):
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')
        product_ids = []
        if size_id:
            product_ids = [[product_id, [size_id]]]
        else:
            product_ids = [product_id]
        pricelist_callbacks_obj.calculate_pricelists(
            cr, uid,
            context={
                'profile_it': False,
                'store_to_customer_fields': True,
                'overwrite': True,
                'data': {partner_id: product_ids},
            }
        )

    def _get_customer_field(self, line, field=None):
        if not field:
            field = 'Customer SKU'
        result = False
        cr = self.cr
        uid = self.uid
        if line and line.product_id:
            partner_id = self.pool.get('res.partner').search(cr, uid, [('name', '=', 'Delmar Showroom')])
            partner_id = _get_one(partner_id)
            size = False
            if line.size != '0000':
                try:
                    size_name = '{:2,.1f}'.format(float(line.size)/100)
                except Exception:
                    size_name = line.size
                size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                if size_id:
                    size = _get_one(size_id)
            if field == pricelist_fn:
                self.recalculate_price_hook(cr, uid, partner_id, line.product_id.id, size)
            result = self.pool.get('product.product').get_val_by_label(
                cr,
                uid,
                line.product_id.id,
                partner_id,
                field,
                size
            )
        return result

    def _get_price(self, line, field=None):
        if not field:
            field = pricelist_fn
        result = False

        if line and line.product_id:
            value = self._get_customer_field(line, field)
            if value:
                result = value
                if result:
                    try:
                        result = float(result)
                        result = '{:4,.2f}'.format(result).replace('.00', '')
                    except Exception:
                        pass

        return result

report_sxw.report_sxw(
    'report.stock.product.report.line.product_sr_labels',
    'stock.product.report.line',
    'addons/delmar_sale/report/stock_product_sr_label.rml',
    parser=stock_move_sr_labels,
    header=False
)
