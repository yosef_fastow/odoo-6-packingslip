# -*- coding: utf-8 -*-
from osv import fields, osv
import base64
import csv
import cStringIO
from tools.translate import _
import re
import logging
logger = logging.getLogger(__name__)


class CustomerReport(osv.osv_memory):
    _name = "delivery.order.customer.report"
    _columns = {
        "tag_name": fields.text("Tag name", required=False, help="If tag name is defined tag will be exported, not selected orders"),
        "file": fields.binary(string="CSV Export", readonly=True),
        "filename": fields.char("", size=256),
        'state': fields.boolean("state"),
        'limited': fields.boolean("limited"),
    }

    _defaults = {
        'limited': False,
        'state': False,
        'file': ''
    }

    def _write_lines(self, lines, context=None):
        if not lines:
            lines = []
        quote_style = csv.QUOTE_ALL
        output = cStringIO.StringIO()
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
        writer = csv.writer(output, dialect='csv', quoting=quote_style)
        for line in lines:
            writer.writerow([isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in line])
        return output

    def action_generate_report(self, cr, uid, ids, context=None):
        def _get_manifest_order_ids(manifest_ids):
            """Supplementary function to extract picking ids from manifest."""
            manifest_obj = self.pool.get('stock.picking.manifest')
            # manifest_obj.browse(cr, uid, active_ids)
            order_ids = set()
            for manifest in manifest_obj.browse(cr, uid, manifest_ids):
                logger.info("Checking manifest ID: %s" % manifest.id)
                for line in manifest.line_ids:
                    sub_manifests[line.picking_id.id] = sub_manifests.get(line.picking_id.id, []) + [line.sub_manifest_id.name]
                    order_ids.add(line.picking_id.id)
            return list(order_ids)

        active_ids = context.get('active_ids', [])
        logger.info("Total picking IDs to process: %s" % len(active_ids))

        report = self.browse(cr, uid, ids[0])
        if report.tag_name:
            active_ids = []
            tag_obj = self.pool.get('tagging.order')
            tag_ids = tag_obj.search(cr, uid, [('name', '=', report.tag_name)])
            if tag_ids and len(tag_ids) > 0:
                sql = "SELECT distinct sp.id FROM taggings_order t LEFT JOIN order_history oh ON oh.id=t.order_id LEFT JOIN sale_order so ON so.id=oh.parent_sale_order LEFT JOIN stock_picking sp ON sp.sale_id=so.id WHERE t.tag_id={} AND sp.total_price<800 and sp.state in ('done', 'shipped')".format(tag_ids[0])
                cr.execute(sql)
                for order in cr.fetchall():
                    logger.info("Adding order_id into active_ids: %s" % order)
                    active_ids.append(order[0])

        if not active_ids:
            raise osv.except_osv(_('Error'), 'No orders were selected')
        picking_obj = self.pool.get('stock.picking')
        product_obj = self.pool.get('product.product')

        # DLMR-1631
        # Check some orders total_price is more than 800$
        cr.execute("select count(*) as overpriced from stock_picking sp where sp.total_price >= 800 and sp.id in %s", (tuple(active_ids),))
        result = cr.dictfetchone()
        if result['overpriced'] > 0:
            raise osv.except_osv('Warning!', 'Orders with price over $800: {}'.format(result['overpriced']))
        # END DLMR-1631

        sub_manifests = {}
        if context.get('manifest'):
            active_ids = _get_manifest_order_ids(active_ids)
        else:
            sub_manifests = {pick: [''] for pick in active_ids}

        logger.info("Manifest picking IDs to process: %s" % len(active_ids))

        # init lines list
        lines = list()
        # create header line
        columns = {
            'Customer': {'mandatory': 0, 'position': 0},
            'Sale Order': {'mandatory': 0, 'position': 1},
            'Tracking': {'mandatory': 0, 'position': 2},
            'Shipper': {'mandatory': 1, 'position': 3},
            'ShipperAddress': {'mandatory': 1, 'position': 4},
            'ShipperCity': {'mandatory': 1, 'position': 5},
            'ShipperState': {'mandatory': 1, 'position': 6},
            'ShipperZipCode': {'mandatory': 1, 'position': 7},
            'Consignee': {'mandatory': 1, 'position': 8},
            'ConsigneeAddress': {'mandatory': 1, 'position': 9},
            'ConsigneeCity': {'mandatory': 1, 'position': 10},
            'ConsigneeState': {'mandatory': 1, 'position': 11},
            'ConsigneeZipCode': {'mandatory': 1, 'position': 12},
            'ShipmentType': {'mandatory': 1, 'position': 13},
            'SCN': {'mandatory': 1, 'position': 14},
            'Description': {'mandatory': 1, 'position': 15},
            'Quantity': {'mandatory': 1, 'position': 16},
            'UMQuantity': {'mandatory': 1, 'position': 17},
            'Weight': {'mandatory': 1, 'position': 18},
            'UMWeight': {'mandatory': 1, 'position': 19},
            'Retail Price': {'mandatory': 1, 'position': 20},
            'IWT': {'mandatory': 0, 'position': 21},
            'Country of origin': {'mandatory': 0, 'position': 22},
            'Sub manifest': {'mandatory': 0, 'position': 23},
            'Tariff': {'mandatory': 0, 'position': 24},
        }
        # Select only mandatory columns for limited report if need
        columns = [item for item in columns.items() if item[1]['mandatory']] if context.get('limited') \
                  else columns.items()
        header = [item[0] for item in sorted(columns, key=lambda x: x[1]['position'])]
        lines.append(header)

        # iterate over pickings

        for active_id in active_ids:
            try:
                picking = picking_obj.browse(cr, uid, active_id)
                logger.info("Iterating over picking id: %s, name: %s" % (picking.id, picking.name))
            except Exception:
                logger.critical("BAD PICKING ID %s" % active_id)
                continue
            # address = picking.sale_id and picking.sale_id.partner_order_id and picking.sale_id.partner_order_id or None
            address = picking.sale_id and picking.sale_id.partner_shipping_id or None
            partner = picking.sale_id and picking.sale_id.partner_id and picking.sale_id.partner_id or None
            sale = picking.sale_id or None

            address_zip = address.zip
            if address.country_id.code == 'US':
                if address_zip:
                    zip_preg = re.compile('^\d{9}$')
                    if zip_preg.match(address_zip):
                        address_zip = address_zip[:5] + '-' + address_zip[5:]

            if not address:
                continue
            for move in picking.move_lines:
                cost = move.sale_line_id.customerCost or None
                if (not cost) and partner.id in (48, 22, 4, 23, 596, 597, 598, 49, 51, 131, 50, 173, 224, 144, 944, 38, 89, 88, 100, 872, 43, 870, 881, 130, 208, 924, 936):
                    cost = move.sale_line_id.price_unit or 0.0
                streets = [address.street]
                if address.street2:
                    streets.append(address.street2)
                if address.street3:
                    streets.append(address.street3)

                itw_diamond_res = product_obj.get_total_weight(cr, uid, move.product_id.id,
                                                               {'component_type': 'diamond'})
                itw_gemstone_res = product_obj.get_total_weight(cr, uid, move.product_id.id,
                                                                {'component_type': 'gemstone'})
                itw_metal_res = product_obj.get_total_weight(cr, uid, move.product_id.id,
                                                             {'component_type': 'metal'})
                itw_res = (float(itw_diamond_res) + float(itw_gemstone_res)) / 5 + float(itw_metal_res)
                line = {
                    'Customer': partner.ref,
                    'Sale Order': sale.name,
                    'Tracking': ''.join(['\'', picking.tracking_ref or '']),
                    'Shipper': '3385175 Canada Inc.',
                    'ShipperAddress': '4058 Jean Talon O',
                    'ShipperCity': 'Montreal',
                    'ShipperState': 'QC',
                    'ShipperZipCode': 'H4P 1V5',
                    'Consignee': address.name,
                    'ConsigneeAddress': ', '.join(streets),
                    'ConsigneeCity': address.city,
                    'ConsigneeState': address.state_id.code,
                    'ConsigneeZipCode': address_zip,
                    'ShipmentType': '',
                    'SCN': '\'{}'.format((move.export_ref_ca or '').replace('FLXQ', '')),
                    'Description': move.product_id.custom_description or move.product_id.short_description or
                                   move.product_id.name or '',
                    'Quantity': move.product_qty,
                    'UMQuantity': 'PCS',
                    'Weight': '1',
                    'UMWeight': 'LBS',
                    'Retail Price': cost or 0.0,
                    'IWT': itw_res,
                    'Country of origin': move.product_id.made_in.name or move.product_id.prod_manufacture or '',
                    'Sub manifest': ', '.join(sub_manifests[picking.id]),
                    'Tariff': move.product_id.product_tmpl_id.prod_tarif_classification or '',
                }
                lines.append([line[item] for item in header])
        # write lines to csv
        buf = self._write_lines(lines)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        return self.write(cr, uid, ids, {'state': True, 'file': out, 'filename': 'picking_customer_report.csv'}, context=context)


CustomerReport()
