# -*- coding: utf-8 -*-
##############################################################################

import time
import pooler
from report import report_sxw
from json import JSONDecoder as JDecode


class sterling_dc_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(sterling_dc_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_order_comment': self._get_order_comment,
            'get_order_line_comment': self._get_order_line_comment,
            'get_gmtime': self._get_gmtime,
            'get_custom_field': self._get_custom_field,
            'convert_to_int': self._convert_to_int,
        })

    def _get_order_comment(self, sale, comment_type):
        cr = self.cr
        uid = self.uid
        order_id = sale.id or 0
        comment_obj = pooler.get_pool(cr.dbname).get('sale.order.sterling.comments')
        comments_ids = comment_obj.search(cr, uid, [('order_id', '=', order_id), ('type', '=', comment_type)])
        comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
        return comment

    def _get_order_line_comment(self, sale_line, comment_type):
        cr = self.cr
        uid = self.uid
        comment = ''
        if sale_line:
            comment_obj = pooler.get_pool(cr.dbname).get('sale.order.line.sterling.comments')
            comments_ids = comment_obj.search(cr, uid, [('line_id', '=', sale_line.id), ('type', '=', comment_type)])
            comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
        return comment

    def _get_gmtime(self, format):
        return time.strftime(format, time.gmtime())

    def set_context(self, objects, data, ids, report_type=None):
        super(sterling_dc_report, self).set_context(objects, data, ids, report_type=report_type)
        self.rml_header = """
<header>
</header>
"""

    def _get_custom_field(self, sale_line, label, partner=True, size=True):
        value = False
        try:
            size_id = size and sale_line.size_id and sale_line.size_id.id or False
            partner_id = partner and sale_line.order_id.partner_id.id or False
            value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, sale_line.product_id.id, partner_id, label, size_id)
        except:
            value = False
        return value

    def _convert_to_int(self, value):
        try:
            res = int(value)
        except:
            res = 1
        return res


class sterling_ss_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(sterling_ss_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_order_comment': self._get_order_comment,
            'get_order_line_comment': self._get_order_line_comment,
            'get_qtytotal': self._get_qtytotal,
            'get_gmtime': self._get_gmtime,
            'get_customerCost': self._get_customerCost,
            'get_subtotalCost': self._get_subtotalCost,
            'get_totalCost': self._get_totalCost,
            'return_address': self._return_address,
            'get_address': self._get_address,
            'get_additional_order_field': self._get_additional_order_field,
            'get_additional_order_line_field': self._get_additional_order_line_field,
            'get_json': self._get_json,
            'get_json_barcode': self._get_json_barcode,
            'get_custom_field': self._get_custom_field,
            'get_delivery_order_ssid': self._get_delivery_order_ssid,
            'get_total_custom_price': self._get_total_custom_price,
            'get_total_customerCost_on_qty': self._get_total_customerCost_on_qty,
            'get_subtotalRetail': self._get_subtotalRetail,
            'get_totalCostWithTax': self._get_totalCostWithTax,
            'get_total_priceUnit_on_qty': self._get_total_priceUnit_on_qty
        })

    def _get_delivery_order_ssid(self, picking):
        cr = self.cr
        uid = self.uid
        sql = """
            select uffo.id
            from stock_picking sp
            inner join unique_fields_for_orders uffo on uffo.picking_id=sp.id
            inner join sale_order_fields sof on sof.order_id=sp.sale_id
            where sp.id=%s and sof."name"='serial_reference_number'
        """ % picking.id
        cr.execute(sql)
        ids = cr.fetchone()
        if not ids:
            return ''
        else:
            field_id = ids[0]
            uffo_obj = pooler.get_pool(cr.dbname).get('unique.fields.for.orders')
            sscc_id = uffo_obj.generate_sscc_id(cr, uid, field_id, extension_digit='4').get(field_id, '')
            return sscc_id

    def _get_total_custom_price(self, price_unit, qty, line_sum=False, return_format='$%.2f'):

        try:
            price_unit = float(price_unit)
        except (ValueError, TypeError):
            price_unit = 0.0

        total_price = price_unit * qty

        if line_sum:
            self.__total_sub_price += total_price

        if not return_format:
            return total_price
        else:
            return return_format % total_price

    def _get_total_customerCost_on_qty(self, picking):
        result = 0.0
        for move_line in picking.move_lines:
            result += (float(move_line.sale_line_id.customerCost or 0) * move_line.product_qty)

        return '$%.2f' % (result)

    def _get_total_priceUnit_on_qty(self, picking):
        result = 0.0
        for move_line in picking.move_lines:
            result += (float(move_line.sale_line_id.price_unit or 0) * move_line.product_qty)

        return '$%.2f' % (result)

    def _get_order_comment(self, picking, comment_type):
        cr = self.cr
        uid = self.uid
        order_id = picking.sale_id and picking.sale_id.id or 0
        comment_obj = pooler.get_pool(cr.dbname).get('sale.order.sterling.comments')
        comments_ids = comment_obj.search(cr, uid, [('order_id', '=', order_id), ('type', '=', comment_type)])
        comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
        return comment

    def _get_order_line_comment(self, move_line, comment_type):
        cr = self.cr
        uid = self.uid
        comment = ''
        if move_line and move_line.sale_line_id:
            comment_obj = pooler.get_pool(cr.dbname).get('sale.order.line.sterling.comments')
            comments_ids = comment_obj.search(cr, uid, [('line_id', '=', move_line.sale_line_id.id), ('type', '=', comment_type)])
            comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
        return comment

    def _get_qtytotal(self, move_lines):
        total = 0.0
        uom = move_lines[0].product_uom.name
        for move in move_lines:
            total += move.product_qty or 0.0
        return {'quantity': total, 'uom': uom}

    def _get_customerCost(self, move_line):
        return round(float(move_line.sale_line_id.customerCost or '0.00'), 2)

    def _get_priceUnit(self, move_line):
        return round(float(move_line.sale_line_id.price_unit or '0.00'), 2)

    def _get_totalCostWithTax(self, picking):
        result = 0.0
        for move_line in picking.move_lines:
            result += (float(move_line.sale_line_id.customerCost or 0) * move_line.product_qty)
        return round(float(picking.sale_id.tax) + result,2)

    def _get_subtotalCost(self, move_line):
        return round(float(self._get_customerCost(move_line) * move_line.product_qty), 2)

    def _get_subtotalRetail(self, move_line):
        return round(float(self._get_priceUnit(move_line) * move_line.product_qty), 2)

    def _get_totalCost(self, move_line, fret=0):
        return round(float(self._get_subtotalCost(move_line) + float(fret)), 2)

    def _get_gmtime(self, format):
        return time.strftime(format, time.gmtime())

    def set_context(self, objects, data, ids, report_type=None):
        super(sterling_ss_report, self).set_context(objects, data, ids, report_type=report_type)
        self.rml_header = """
<header>
</header>
"""
    def _return_address(self, picking):
        addr = None
        addr_id = self.pool.get('stock.picking').get_addresses(self.cr, self.uid, picking.id, addresses=['return'])[picking.id]['return']
        if addr_id:
            addr = self.pool.get('res.partner.address').browse(self.cr, self.uid, addr_id)

        return addr

    def _get_address(self, picking, address_type):
        addr = None
        addr_id = self.pool.get('stock.picking').get_addresses(
            self.cr, self.uid, picking.id, addresses=[address_type]
        )[picking.id][address_type]
        if addr_id:
            addr = self.pool.get('res.partner.address').browse(self.cr, self.uid, addr_id)
        return addr

    def _get_additional_order_field(self, picking, field_name):
        res = ''
        sale_order = picking.sale_id
        if sale_order and sale_order.additional_fields:
            for item in sale_order.additional_fields:
                if item.name == field_name:
                    res = item.value
                    break
        return res

    def _get_additional_order_line_field(self, move_line, field_name):
        res = ''
        sale_line = move_line.sale_line_id
        if sale_line and sale_line.additional_fields:
            for item in sale_line.additional_fields:
                if item.name == field_name:
                    res = item.value
                    break
        return res

    def _get_json(self, json_line):
        decoder = JDecode()
        return decoder.decode(json_line)['sscc_id']

    def _get_json_barcode(self, json_line):
        decoder = JDecode()
        barcode = decoder.decode(json_line)['sscc_id']
        return '(' + barcode[0:2] + ') ' + barcode[2:3] + ' ' + barcode[3:10] + ' ' + barcode[10:19] + ' ' + barcode[19:20]

    def _get_custom_field(self, move_lines, label, partner=True, size=True):
        value = False
        try:
            sale_line = move_lines.sale_line_id
            size_id = size and sale_line.size_id and sale_line.size_id.id or False
            partner_id = partner and move_lines.picking_id.real_partner_id.id or False
            product_id = sale_line.product_id.id
            value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, product_id, partner_id, label, size_id)
            # Get base line value if sized one not found
            if size_id and not value:
                value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, product_id, partner_id, label, False)
        except:
            value = False
        return value


report_sxw.report_sxw(
    'report.extra.material.dc',
    'sale.order.line',
    'addons/delmar_sale/report/sterling_dc_report.rml',
    parser=sterling_dc_report,
    # header=True
)

report_sxw.report_sxw(
    'report.extra.material.ss',
    'stock.picking',
    'addons/delmar_sale/report/sterling_ss_report.rml',
    parser=sterling_ss_report,
    # header=True
)

report_sxw.report_sxw(
    'report.sale.order.individual.label',
    'sale.order.line',
    'addons/delmar_sale/report/sterling_dc_report.rml',
    parser=sterling_dc_report,
    # header=True
)
