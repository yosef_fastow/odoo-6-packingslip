# -*- coding: utf-8 -*-
##############################################################################
import re
import requests
import base64

from report import report_sxw
import logging

_logger = logging.getLogger(__name__)

class stock_picking_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(stock_picking_labels, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_actual_bin': self._get_actual_bin,
            'get_user_uid': self._get_user_uid,
            'get_product_image': self._get_product_image,
            'get_qr_string': self._get_qr_string,
        })

    def _get_user_uid(self, context=None):
        if context is None:
            context = {}

        user_obj = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)

        return user_obj.name

    def _get_qr_string(self, line):
        """Returns string for QR label generator as a template from product' default code, '/', and product size.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return QR string e.g. FC0L2F-K3KJ-WB/0500"""
        move_obj = self.pool.get('stock.move')
        delmarid = line.product_id and line.product_id.default_code or False
        size_postfix = move_obj.get_size_postfix(self.cr, self.uid, line.size_id)
        return delmarid and (delmarid + '/' + size_postfix) or False

    def _get_product_image(self, line):
        """Returns image of product in base64 format.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return image in base64 format."""
        result = None
        if line:
            item = line.product_id.default_code.lower()
            sizes = dict(small='85', medium='185')
            conf_obj = self.pool.get('ir.config_parameter')
            host = conf_obj.get_param(self.cr, self.uid, 'product.catalog.host')
            image_url = '{}/themes/progforce/catalog/{}/{}.jpg'.format(host, sizes['medium'], item)
            response = requests.get(image_url)
            result = base64.encodestring(response.content)

        return result

    def _get_actual_bin(self, line, context=None):
        if not context:
            context = {}
        result = False
        if line:

            loc = line.location_id
            external_customer_order_id = line.sale_line_id.order_id.external_customer_order_id

            rl_obj = self.pool.get("stock.picking.return.line")
            rl_ids = rl_obj.search(self.cr, self.uid, [('picking_id', '=', line.picking_id.id)])
            if rl_ids:
                rl = rl_obj.browse(self.cr, self.uid, rl_ids[0])
                bin_name = rl.target_bin_id and rl.target_bin_id.name or ''
                if bin_name:
                    return bin_name
                elif rl.target_location_id:
                    loc = rl.target_location_id

            size = '0000'
            stock = loc.name
            wh = loc.warehouse_id and loc.warehouse_id.name
            default_code = line.product_id and line.product_id.default_code
            if line.size_id:
                size = re.sub('\.', '', "%05.2f" % float(line.size_id.name))

            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_main_servers(self.cr, self.uid, single=True)
            if server_id:
                sql = """SELECT TOP 1 cast(coalesce(bin, '') as varchar) as bin FROM transactions
                    WHERE id_delmar='%s' AND size='%s' AND ((warehouse='%s' AND stockid = '%s') OR (INVOICENO = '%s'))
                    ORDER BY timestamp DESC
                """ % (default_code, size, wh, stock, external_customer_order_id)
                result = server_data.make_query(self.cr, self.uid, server_id, sql, select=True, commit=False)
                if result and result[0] and result[0][0]:
                    result = result[0][0]

        return result

report_sxw.report_sxw(
    'report.stock.picking.picking_label',
    'stock.picking',
    'addons/delmar_sale/report/stock_returns_label.rml',
    parser=stock_picking_labels,
    header=False
)
