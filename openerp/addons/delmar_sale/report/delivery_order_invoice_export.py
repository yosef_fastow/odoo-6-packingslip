# -*- coding: utf-8 -*-
from osv import fields, osv
import xlwt
import cStringIO
import base64
import logging
from datetime import datetime
from tools.translate import _
from openerp.addons.delmar_sale.sale import SIGN_DEFAULT_VALUE


_logger = logging.getLogger(__name__)


class delivery_order_invoice_export(osv.osv):
    _name = "delivery.order.invoice.export"
    _columns = {
        "xls_file": fields.binary(string="CSV Export", readonly=True),
        "xls_filename": fields.char("", size=256),
        'state': fields.boolean("State"),
        'country': fields.selection([
            ('us', 'USA'),
            ('ca', 'Canada'),
        ], 'Country',),
        'export_ref_ca': fields.char('Export REF CA', size=256, ),
        'export_ref_us': fields.char('Export REF US', size=256, ),
        'fc_invoice': fields.char('FC Invoice', size=256, ),
        'fc_order': fields.char('FC Order', size=256,),
        'del_invoice': fields.char('DEL Invoice', size=256,),
        'del_order': fields.char('DEL Order', size=256,),
        'bill_of_lading': fields.char('BILL OF LADING', size=100, ),
        "override_existing": fields.boolean('Override existing', help='Override existing fields.', store=False, ),
    }

    _defaults = {
        'state': False,
        'xls_file': '',
        "override_existing": False,
    }

    def _write_line(self, sheet, row, datas=None, style=False):
        if not datas:
            datas = []
        for data in datas:
            for i in xrange(0, 5):
                cell_style = False
                if style:
                    if isinstance(style, dict):
                        cell_style = style.get(i, False)
                    else:
                        cell_style = style
                if cell_style:
                    sheet.write(row, i, data.get('s' + str(i), ''), cell_style)
                else:
                    sheet.write(row, i, data.get('s' + str(i), ''))
            row += 1
        return row

    def _get_group_amount(self, report_group):
        total = 0.0
        total_qty = 0.0
        for line in report_group.report_line_ids:
            qty = line.product_qty or 0.0
            total_qty += qty
            cost = line.cost or 0.0
            total += qty * cost
        return {'amount': total, 'qty': total_qty}

    def _get_total_amount(self, report):
        total = 0.0
        total_qty = 0.0
        for group in report.report_group_ids:
            group_amount = self._get_group_amount(group)
            total += group_amount['amount']
            total_qty += group_amount['qty']
        return {'amount': total, 'qty': total_qty}

    def generate_invoice_report_export(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]

        report_data = self.browse(cr, uid, ids[0])
        if context.get('internal'):
            active_ids = context.get('active_ids', [])
            picking_ids = self.pool.get('stock.picking').search(cr, uid, [('internal_shipment_id', 'in', active_ids), ('internal_shipment_id.move_ids', '=', False)])
            move_ids = self.pool.get('stock.move').search(cr, uid, [('internal_shipment_id', 'in', active_ids)])
            context.update({
                'active_ids': picking_ids,
                'move_ids': move_ids,
                'country': report_data.country,
            })
        new_report_id = self.pool.get('stock.picking').create_invoice_report(cr, uid, ids, context=context)
        
        return new_report_id

    def action_invoice_report_export_pdf(self, cr, uid, ids, context=None):

        report_id = self.generate_invoice_report_export(cr, uid, ids, context=context)
        report = self.pool.get("stock.invoice.report").browse(cr, uid, report_id)
        from_addr = ''
        to_addr = ''
        if context.get('internal'):
            from_wh = False
            to_wh = False
            warehouse_obj = self.pool.get('stock.warehouse')
            
            ca_wh = warehouse_obj.search(cr, uid, [('name', '=', 'CAMTL')])
            ca_wh = ca_wh and warehouse_obj.browse(cr, uid, ca_wh[0]) or False
            us_wh = warehouse_obj.search(cr, uid, [('name', '=', 'USCHP')])
            us_wh = us_wh and warehouse_obj.browse(cr, uid, us_wh[0]) or False
            if context.get('country') == 'us':
                from_wh, to_wh = ca_wh, us_wh
            elif context.get('country') == 'ca':
                from_wh, to_wh = us_wh, ca_wh
            if from_wh and to_wh:
                from_addr = from_wh.partner_address_id
                from_addr = {
                    'name': from_addr.name,
                    'street': from_addr.street or '',
                    'address': (from_addr.city and from_addr.city + ', ' or '') + (from_addr.state_id and from_addr.state_id.code + ', ' or '') + (from_addr.zip or ''),
                    'country': from_addr.country_id and from_addr.country_id.name,
                }
                to_addr = to_wh.partner_address_id
                to_addr = {
                    'name': to_addr.name,
                    'street': to_addr.street or '',
                    'address': (to_addr.city and to_addr.city + ', ' or '') + (to_addr.state_id and to_addr.state_id.code + ', ' or '') + (to_addr.zip or ''),
                    'country': to_addr.country_id and to_addr.country_id.name,
                }

        datas = {
            'id': report_id,
            'ids': [report_id],
            'model': 'stock.invoice.report',
            'report_type': 'pdf'
            }
        act_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'delivery.order.invoice.export.pdf',
            'datas': datas,
            'context': {'active_ids': [report_id], 'active_id': report_id,  'country': context.get('country'), 'from_addr': from_addr, 'to_addr': to_addr,}
        }
        return act_print

    def action_update_transactions(self, cr, uid, ids, context=None):
        """Update entries in transactions table, related to selected ids in internal_shipment table."""
        # Check out if all required fields are filled
        param_list = ['bill_of_lading', 'export_ref_ca', 'export_ref_us', 'fc_invoice', 'fc_order', 'del_invoice', 'del_order']
        if not filter(lambda x: x in context, param_list):
            raise osv.except_osv(_('Error!'), _('Please fill in any field!'))

        active_ids = context.get('active_ids')
        if not all([active_ids]):
            raise osv.except_osv(_('Error!'), _('active ids mustn\'t be empty!'))

        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)

        move_obj = self.pool.get('stock.move')

        # Take only records from internal_shipment table with action receive.
        # internal_shipment_records = self.pool.get('internal.shipment').browse(cr, uid, active_ids, [('action', 'in', ('receive',))])
        # Temporary relax constrain to allow retrieve records in any action state.
        internal_shipment_records = self.pool.get('internal.shipment').browse(cr, uid, active_ids)

        # For each order in internal_shipment_records
        # Filter out only records presented in internal.shipment.move table (move_ids)
        # or select all order' records (picking_id from stock.move table)
        # And aggregate retrieved ids into one list
        # stock_move_records = sum(map(lambda x, move_obj=move_obj, cr=cr, uid=uid: x.move_ids or
        #        move_obj.browse(cr, uid, move_obj.search(cr, uid, ['|', ('set_parent_order_id', '=', x.picking_id.id), ('picking_id', '=', x.picking_id.id)]))
        #        , internal_shipment_records), [])
        sm_ids = []
        no_sm_ids_pickings = []
        for ship_rec in internal_shipment_records:
            if ship_rec.move_ids:
                sm_ids.extend(x.id for x in ship_rec.move_ids)
            else:
                if ship_rec.picking_id and ship_rec.picking_id.id:
                    no_sm_ids_pickings.append(ship_rec.picking_id.id)
        _logger.info("Got sm_ids from internal shipment moves: %s" % len(sm_ids))
        if no_sm_ids_pickings:
            sm_ids.extend(move_obj.search(cr, uid, ['|', ('picking_id', 'in', no_sm_ids_pickings), '&', ('set_parent_order_id', 'in', no_sm_ids_pickings), ('is_set', '=', True)]))
        _logger.info("Got sm_ids total: %s" % len(sm_ids))
        stock_move_records = move_obj.browse(cr, uid, list(set(sm_ids)))
        _logger.info("Got %s unique stock_moves" % len(stock_move_records))
        # Filter list of orders by fc_invoice field in stock_move table
        # Leave only orders with empty corresponding field
        # stock_move_records = filter(lambda x: not x.fc_invoice, stock_move_records)

        if not stock_move_records:
            raise osv.except_osv(_('Error!'), _('Nothing to update!'))

        # going through the objects
        sm_updated = 0
        for record in stock_move_records:   
            # Get stock.move entries corresponding to order_history records
            warehouse = record.warehouse.name
            mssql_context = {'transaction_code': SIGN_DEFAULT_VALUE, 'erp_sm_id': record.id}
            sm_context = {}
            for param in param_list:
                if (not record[param] or context.get('override_existing', False)) and context.get(param):
                    mssql_context.update({param: context.get(param)})
                    sm_context.update({param: context.get(param)})
            
            if len(sm_context) == 0:
                continue

            move_obj.update_transactions_table(cr, uid, server_id, server_obj, record, warehouse, mssql_context)
            move_obj.write(cr, uid, record.id, sm_context)
            sm_updated += 1

        self.log(cr, uid, 1, _("{} stock move lines were updated.".format(sm_updated)))
        return {}

    def action_invoice_report_export(self, cr, uid, ids, context=None):

        report_id = self.generate_invoice_report_export(cr, uid, ids, context=context)
        report = self.pool.get("stock.invoice.report").browse(cr, uid, report_id)
        invoice_report = xlwt.Workbook()
        sheet = invoice_report.add_sheet("invoice_report")
        row = 0

        # Styles and fontes for formatting
        font_bu = xlwt.easyxf('font: underline on, bold on;')
        font_b = xlwt.easyxf('font: bold on;')
        font_cost = xlwt.easyxf(num_format_str='_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)')
        font_b_cost = xlwt.easyxf(strg_to_parse='font: bold on;', num_format_str='_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)')
        font_bu_cost = xlwt.easyxf(strg_to_parse='font: underline on, bold on;', num_format_str='_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)')

        if context.get('internal'):
            from_wh = False
            to_wh = False
            warehouse_obj = self.pool.get('stock.warehouse')
            ca_wh = warehouse_obj.search(cr, uid, [('name', '=', 'CAMTL')])
            ca_wh = ca_wh and warehouse_obj.browse(cr, uid, ca_wh[0]) or False
            us_wh = warehouse_obj.search(cr, uid, [('name', '=', 'USCHP')])
            us_wh = us_wh and warehouse_obj.browse(cr, uid, us_wh[0]) or False
            if context.get('country') == 'us':
                from_wh, to_wh = ca_wh, us_wh
            elif context.get('country') == 'ca':
                from_wh, to_wh = us_wh, ca_wh
            if from_wh and to_wh:
                now = datetime.utcnow()
                from_addr = from_wh.partner_address_id
                for value in (
                    [from_addr.name,None,None],
                    [from_addr.street, 'INVOICE #', now.strftime('DELM%d%m%Y')],
                    [(from_addr.city and from_addr.city + ', ' or '') + (from_addr.state_id and from_addr.state_id.code + ', ' or '') + (from_addr.zip or ''), 'DATE', now.strftime('%m/%d/%Y')],
                    [from_addr.country_id and from_addr.country_id.name, None, None],
                ):
                    row = self._write_line(sheet, row, [{
                        's0': value[0] or '',
                        's3': value[1] or '',
                        's4': value[2] or '',
                    }])
                row += 1
                row = self._write_line(sheet, row, [{
                        's0': 'Bill To:',
                        's3': 'Ship To:',
                }])
                to_addr = to_wh.partner_address_id
                for value in (
                    to_addr.name,
                    to_addr.street,
                    (to_addr.city and to_addr.city + ', ' or '') + (to_addr.state_id and to_addr.state_id.code + ', ' or '') + (to_addr.zip or ''),
                    to_addr.country_id and to_addr.country_id.name,
                ):
                    row = self._write_line(sheet, row, [{
                        's0': value or '',
                        's3': value or '',
                    }])
                row += 1

        row = self._write_line(sheet, row, [{
                        's2': 'CUSTOMER TAX ID #75-3264433'
                    }], context.get('internal') and font_bu or False)

        row += 1

        row = self._write_line(sheet, row, [{
            's0': 'STYLE#',
            's1': 'QTY',
            's2': 'DESCRIPTION',
            's3': 'COST',
            's4': 'AMOUNT'
        }], context.get('internal') and font_bu or False)
        for group in report.report_group_ids:
            row = self._write_line(sheet, row, [
                {'s0': 'COUNTRY OF ORIGIN: %s' %  (group.country and group.country.upper() or '-')},
                {'s0': 'HS TARIFF CLASSIFICATION # %s' % group.tariff},
            ], context.get('internal') and font_b or False)
            for line in group.report_line_ids:
                if context.get('internal'):
                    desc = line.product_id.custom_description or line.product_id.short_description or ''
                    cost = line.cost
                    amount = cost * line.product_qty
                    style = {
                        3: font_cost,
                        4: font_cost,
                    }
                else:
                    desc = line.product_id.custom_description or line.product_id.short_description or line.product_id.name or ''
                    cost = round(line.cost, 2)
                    amount = round(line.cost * line.product_qty, 2)
                    style = False
                row = self._write_line(sheet, row, [{
                    's0': line.product_id.default_code,
                    's1': int(line.product_qty),
                    's2': desc,
                    's3': cost,
                    's4': amount,
                }], style)
            group_data = self._get_group_amount(group)
            group_amount = group_data['amount']
            if context.get('internal'):
                style = {
                    0: font_b,
                    1: font_b,
                    2: font_b,
                    4: font_b_cost,
                }
            else:
                group_amount = round(group_amount, 2)
                style = False
            row_data = {
                    's0': group.tariff,
                    's2': 'SUBTOTAL %s' % (group.country and group.country.upper() or '-'),
                    's4': group_amount,
            }
            if context.get('internal'):
                row_data.update({'s1':int(group_data['qty'])})
            row = self._write_line(sheet, row, [row_data], style)
            row += 1
        row += 1
        total_data = self._get_total_amount(report)
        total_amount = total_data['amount']
        if context.get('internal'):
            style = {
                1: font_bu,
                2: font_bu,
                4: font_bu_cost,
            }
        else:
            total_amount = round(total_amount, 2)
            style = False
        row_data = {
            's2': 'TOTAL AMOUNT OF INVOICE',
            's4': total_amount,
        }
        if context.get('internal'):
            row_data.update({'s1':int(total_data['qty'])})
        row = self._write_line(sheet, row, [row_data], style)

        row += 4

        row = self._write_line(sheet, row, [{
                        's0': 'GST/TPS# R883554891'
                    }], style)
        row = self._write_line(sheet, row, [{
                        's0': 'QST/TVQ# 1021255528'
                    }], style)
        row = self._write_line(sheet, row, [{
                        's0': 'DUNN\'S # 243549128'
                    }], style)

        buf = cStringIO.StringIO()
        invoice_report.save(buf)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        
        return self.write(cr, uid, ids, {'state': True, 'xls_file': out, 'xls_filename': 'invoice_report.xls'}, context=context)

    def action_accpac_export(self, cr, uid, ids, context=None):
        report_id = self.generate_invoice_report_export(cr, uid, ids, context=context)
        report = self.pool.get("stock.invoice.report").browse(cr, uid, report_id)
        product_obj = self.pool.get('product.product')
        invoice_report = xlwt.Workbook()
        sheet = invoice_report.add_sheet("Accpac_export")
        # Styles and fontes for formatting
        font_b = xlwt.easyxf('font: bold on;')

        row = 0
        columns = ['Delmar Id', 'QTY', 'Description', 'Cost', 'Country', 'Tariff', 'IWT', 'Order No (with customer code)']
        map(lambda x, s=sheet, r=row, f=font_b: s.write(r, x[0], x[1], f), enumerate(columns))
        row += 1

        for group in report.report_group_ids:
            country = group.country
            tariff = group.tariff
            for line in group.report_line_ids:
                po_number = ''
                if line.picking_id:
                    po_number = line.picking_id.name
                delmar_id = line.product_id.default_code
                qty = line.product_qty
                description = line.product_id.custom_description or \
                              line.product_id.short_description or \
                              line.product_id.name or ''
                cost = round(line.cost, 2)

                itw_diamond_res = product_obj.get_total_weight(cr, uid, line.product_id.id, {'component_type':'diamond'})
                itw_gemstone_res = product_obj.get_total_weight(cr, uid, line.product_id.id, {'component_type':'gemstone'})
                itw_metal_res = product_obj.get_total_weight(cr, uid, line.product_id.id,
                                                                {'component_type': 'metal'})
                itw_res = float(itw_diamond_res) + float(itw_gemstone_res)/5 + float(itw_metal_res)

                map(lambda x, s=sheet, r=row: s.write(r, x[0], x[1]), enumerate([delmar_id,
                                                                                 qty,
                                                                                 description,
                                                                                 cost,
                                                                                 country,
                                                                                 tariff,
                                                                                 itw_res,
                                                                                 po_number]))
                row += 1

        buf = cStringIO.StringIO()
        invoice_report.save(buf)
        out = base64.encodestring(buf.getvalue())
        buf.close()

        return self.write(cr, uid, ids, {'state': True, 'xls_file': out, 'xls_filename': 'accpac_export.xls'}, context=context)

delivery_order_invoice_export()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
