# -*- coding: utf-8 -*-
##############################################################################

import time
from report import report_sxw


class product_picking_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(product_picking_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
        })


report_sxw.report_sxw(
    'report.product.picking.report',
    'product.picking.report',
    'addons/delmar_sale/report/product_picking_report.rml',
    parser=product_picking_report,
    # header=True
    )

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
