# -*- coding: utf-8 -*-
##############################################################################
import re
from report import report_sxw
from osv import osv
import logging
import requests
import base64

_logger = logging.getLogger(__name__)

class picking_return_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(picking_return_labels, self).__init__(cr, uid, name, context=context)
        _logger.warn('INIT PICKING RETURN LABEL: %s' % name)
        self.localcontext.update({
            'made_in': 'China',
            'get_actual_bin': self._get_actual_bin,
            'get_user_uid': self._get_user_uid,
            'get_product_image': self._get_product_image,
            'get_qr_string': self._get_qr_string,
        })
        if context.get('made_in', False):
            self.localcontext.update({'made_in': context.get('made_in')})

    def repeatIn(self, lst, name, nodes_parent=False):
        def _get_cafer_bin_id(move_line):
            """Check if line' size, bin and warehouse refers to CAFER.
            :param line - stock_move line.
            :return True if line belongs to CAFER."""
            bin_ids = []
            if (move_line.product_id.sizeable or move_line.product_id.ring_size_ids) and move_line.size_id:
                bin_ids = list(self.pool.get('product.product').get_mssql_product_bins(self.cr, self.uid,
                                                                                       ids=[move_line.product_id.id],
                                                                                       loc_ids=[],
                                                                                       size_ids=[move_line.size_id.id],
                                                                                       context={'warehouse': 'CAFER'}
                                                                                       ))
            bin_ids = filter(lambda x: re.compile('^\d{1,2}-[A-Z]-\d{1,2}$').match(x.name),
                               self.pool.get('stock.bin').browse(self.cr, self.uid, bin_ids))
            return bin_ids and bin_ids[0].id or None

        ret_lst = []
        label_obj = self.pool.get('return.item.label')
        serial_obj = self.pool.get('stock.picking.return.label.serial')
        move_obj = self.pool.get('stock.move')
        _logger.warn('for PICKING: %s' % lst)
        for pick in lst:
            set_moves = move_obj.search(self.cr, self.uid, [('set_parent_order_id', '=', pick.id), ('picking_id', '=', False), ('is_set', '=', True)])
            _logger.warn("Found SET MOVES: %s" % set_moves)
            common_moves = move_obj.search(self.cr, self.uid, [('picking_id', '=', pick.id), ('is_set', '=', False)])
            _logger.warn("Found COMMON MOVES: %s" % common_moves)

            for line in move_obj.browse(self.cr, self.uid, set_moves + common_moves):
                # DLMR-1775
                # add serial number generation and querying
                for num in range(1, int(line.product_qty) + 1):
                    # for set component
                    size_id = line.size_id and line.size_id.id or False
                    if (not line.picking_id) and line.set_parent_order_id and line.set_parent_order_id.id == pick.id and (line.is_set is True):
                        label_serial = serial_obj.define_for_label(self.cr, self.uid, picking_id=pick.id,
                                                                   product_id=line.product_id.id, size_id=size_id,
                                                                   qty=line.product_qty, set_component=True, num=num)
                    # for common product
                    else:
                        label_serial = serial_obj.define_for_label(self.cr, self.uid, picking_id=pick.id,
                                                                   product_id=line.product_id.id, size_id=size_id,
                                                                   qty=line.product_qty, num=num)
                    label_vals = {
                        'product_id': line.product_id.id,
                        'product_image': '',
                        'size_id': size_id,
                        'bin_name': line.bin_id and line.bin_id.name if pick.state == 'done' else self._get_actual_bin(line, context=None),
                        'cafer_bin_id': _get_cafer_bin_id(line),
                        'made_in': self.localcontext.get('made_id'),
                        'serial': label_serial or '',
                    }
                    label_id = label_obj.create(self.cr, self.uid, label_vals, context=None)
                    label = label_obj.browse(self.cr, self.uid, label_id)
                    # ret_lst.extend([{name: label}] * int(line.product_qty))
                    ret_lst.append({name: label})

        _logger.warn('PDF ret_lst: %s' % ret_lst)
        return ret_lst

    def _get_user_uid(self, context=None):
        if context is None:
            context = {}
        user_obj = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)
        return user_obj.name

    def _get_qr_string(self, line):
        """Returns string for QR label generator as a template from product' default code, '/', and product size.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return QR string e.g. FC0L2F-K3KJ-WB/0500"""
        move_obj = self.pool.get('stock.move')
        delmarid = line.product_id and line.product_id.default_code or False
        size_postfix = move_obj.get_size_postfix(self.cr, self.uid, line.size_id)
        return delmarid and (delmarid + '/' + size_postfix) or False

    def _get_product_image(self, line):
        """Returns image of product in base64 format.
        @:param line - return line in osv orm model, usually stock.move instance.
        @:return image in base64 format."""
        result = None
        if line:
            item = line.product_id.default_code.lower()
            sizes = dict(small='85', medium='185')
            conf_obj = self.pool.get('ir.config_parameter')
            #host = 'https://catalog.delmarintl.ca'
            host = conf_obj.get_param(self.cr, self.uid, 'product.catalog.host')
            image_url = '{}/themes/progforce/catalog/{}/{}.jpg'.format(host, sizes['medium'], item)
            response = requests.get(image_url)
            result = base64.encodestring(response.content)
        return result

    def _get_actual_bin(self, line, context=None):
        if not context:
            context = {}
        result = False
        if line:
            if line.set_parent_order_id and line.set_parent_order_id.id:
                external_customer_order_id = line.set_parent_order_id.sale_id.po_number
            else:
                external_customer_order_id = line.sale_line_id.order_id.po_number
            size = '0000'
            default_code = line.product_id and line.product_id.default_code
            if line.size_id:
                size = re.sub('\.', '', "%05.2f" % float(line.size_id.name))

            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_main_servers(self.cr, self.uid, single=True)
            if server_id:
                sql = """SELECT TOP 1 cast(coalesce(bin, '') as varchar) as bin FROM transactions
                    WHERE id_delmar='%s' AND size='%s'
                    AND INVOICENO = '%s'
                    AND TRANSACTIONCODE='RET'
                    ORDER BY timestamp DESC
                """ % (default_code, size, external_customer_order_id)
                _logger.info('LABELS BIN QUERY: %s' % sql)
                result = server_data.make_query(self.cr, self.uid, server_id, sql, select=True, commit=False)
                if result and result[0] and result[0][0]:
                    result = result[0][0]

        return result


report_sxw.report_sxw(
    'report.stock.picking.picking_return_label',
    'stock.picking',
    'addons/delmar_sale/report/picking_return_label.rml',
    parser=picking_return_labels,
    header=False
)
