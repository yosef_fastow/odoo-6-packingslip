# -*- coding: utf-8 -*-
##############################################################################

import time
from report import report_sxw


class stock_move_labels(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(stock_move_labels, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'change_size_format': self._change_size_format,
            'get_user_uid': self._get_user_uid,
            'get_time': self._get_time,
        })

    def _get_user_uid(self, context=None):
        if context is None:
            context = {}

        user_obj = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)

        return user_obj.name

    def _get_time(self, format):
        return time.strftime(format, time.gmtime())

    def _change_size_format(self, size):
        change_size = ''
        if size:
            try:
                if (size[0] != '0'):
                    if (size[2] != '0'):
                        change_size = '%s.%s'%(size[:2], size[2])
                    else:
                        change_size = size[:2]      
                else:
                    if (size[2] == '0'):
                        change_size = size[1]
                    else:
                        change_size = '%s.%s'%(size[1], size[2])
            except Exception, e:
                print e
        
        return change_size

report_sxw.report_sxw(
    'report.stock.product.report.line.product_labels',
    'stock.product.report.line',
    'addons/delmar_sale/report/stock_product_label.rml',
    parser=stock_move_labels,
    # header=True
)
