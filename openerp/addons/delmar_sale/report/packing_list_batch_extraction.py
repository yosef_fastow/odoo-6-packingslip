from osv import osv, fields
from tools.translate import _
from openerp.report.report_sxw import report_sxw
from openerp.addons.stock.report.picking import picking
import base64
from datetime import datetime, timedelta
import time
import re
import random
import cups
import string
import os
from tempfile import mkstemp
from delmar_reports.report_sxw import create_source_pdf, create, BATCH_SIZE


DEFAULT_BATCH_PACKSLIP_PARAMS = {
    'loc_ids': None,
    'wh_id': 0,
    'sort': False,
    'real_partner_id': 0,
    'partner_firm_id': 0,
    'ship_service_id': 0,
    's2s': 'all',
    'combine': 1,
    'flat_rate': 0,
    'cron_id': 0,
    'max_batch_size': None,
    'limited_print': None,
    'value_from': 0,
    'value_to': 0,
    'multi_qty': False,
    'changed_size': False,
    'separate_ship_method': False,
}


class packing_export(osv.osv):
    _name = 'picking.export'

    report_sxw_batch = report_sxw(
        'report.stock.picking.list.batch',
        'stock.picking',
        'addons/stock/report/picking.rml',
        parser=picking
    )

    def _get_warehouse_names(self, cr, uid, ids, field_name, arg, context):
        return {packing_export_id: ', '.join((warehouse.name for warehouse in self.browse(cr, uid, packing_export_id, context=context).warehouse)) for packing_export_id in ids}

    _columns = {
        'create_date': fields.datetime('Creation date', readonly=True),
        'create_uid': fields.many2one('res.users', 'User'),
        'pdf_file': fields.binary(string='Pdf file', readonly=True),
        'pdf_filename': fields.char('', size=256),
        'picking_ids': fields.many2many('stock.picking', 'export_stock_picking_rel', 'export_id', 'picking_id', 'Delivery orders'),
        'state': fields.selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('done', 'Done'), ('printed', 'Printed')], 'State', select=True, required=True, readonly=True),
        'status': fields.char('Export state', size=256),

        'warehouse': fields.many2many('stock.warehouse', 'packing_stock_warehouse_rel', 'packing_id', 'warehouse_id', 'Warehouses'),
        'warehouse_names': fields.function(
            _get_warehouse_names,
            type='char',
            size=128,
            obj="stock.warehouse",
            method=True,
            string='Warehouses'),

        'location': fields.char('Location', size=256),
        'sort': fields.selection([('asc', 'Shelf (A-Z)'), ('desc', 'Shelf (Z-A)')], 'Sorting', select=True),
        'real_partner_id': fields.many2one('res.partner', 'Partner', select=True, domain="[('customer', '=', True)]"),
        'partner_firm_id': fields.many2one('res.partner.title', 'Partner Firm', select=True),
        'ship_service_id': fields.many2one('res.shipping.service', 'Shipping Service'),
        'info': fields.char('Export Info', size=256),
        'notes': fields.text('Notes'),
        'scheduled_datetime': fields.datetime('Scheduled Date and Time', required=True),
        'cron_id': fields.many2one('ir.cron', 'Cron ID'),
        'printed_user': fields.many2one('res.users', 'Printed By'),
        'multi_print': fields.boolean('Multi Print'),
        'interval_uom': fields.selection([('minutes', 'Minutes'), ('hours', 'Hours'), ('days', 'Days')], 'Interval UoM', select=True, required=True),
        'interval_value': fields.integer('Interval value'),
        'interval_count': fields.integer('Count of prints'),
        's2s': fields.selection([('all', 'All'), ('s2s', 'Only Ship to Store'), ('regular', 'Only Regular Orders')], 'Ship to Store Options', select=True, required=True),
        'white_paper': fields.boolean('White Paper'),
        'flat_rate': fields.boolean('Flat rate'),
        'combine': fields.boolean('Combine'),
        'batch_size': fields.integer('Batch size'),
        'value_from': fields.float('$ Value From'),
        'value_to': fields.float('$ Value To'),
        'multi_qty': fields.boolean('Items with multi qty'),
        'changed_size': fields.boolean('Items with changed sizes'),
        'separate_ship_method': fields.boolean('Separate by Ship Method'),

    }

    _defaults = {
        'pdf_file': '',
        'state': 'draft',
        'status': 'Start',
        'multi_print': False,
        'interval_uom': 'hours',
        'interval_value': 1,
        'interval_count': 1,
        'pdf_filename': 'batch_packslips,pdf',
        's2s': 'all',
        'combine': True,
        'scheduled_datetime': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
        'sort': 'asc',
        'flat_rate': False,
        'value_from': 0,
        'value_to': 0,
        'multi_qty': False,
        'changed_size': False,
        'separate_ship_method': False,
        # 'batch_size': 50
    }

    _order = "scheduled_datetime desc"
    def print_direct(self, cr, uid,ids, result, format, printer):
        fd, file_name = mkstemp()
        try:
            os.write(fd, base64.decodestring(result))
        finally:
            os.close(fd)
        printer_system_name = ''
        if printer:
            if isinstance(printer, (basestring)):
                printer_system_name = printer
            else:
                printer_system_name = printer.system_name
            if format == 'raw':
                # -l is the same as -o raw
                cmd = "lpr -l -P %s %s" % (printer_system_name,file_name)
            else:
                cmd = "lpr -P %s %s" % (printer_system_name,file_name)
            os.system(cmd)
            os.remove(file_name)
        """
        for item in self.browse(cr,uid,ids):
            product_picking_id=item.product_picking_id.id
        product_picking_obj=self.pool.get('product.picking')
        for row in product_picking_obj.browse(cr,uid,[product_picking_id]):
            product_name=row.product_id.default_code
            category=row.category
            if row.size_id:
                size=row.size_id.name
                name=product_name+'/'+size
            else:
                name=product_name
        self.pool.get('product.picking.print').get_orders_by_prod(cr, uid, ids,name,category, context=None)
        """
        return True
    def print_packing_slips(self, cr, uid, ids, context=None):
        printer_obj = self.pool.get('printer.setting')
        conn=cups.Connection()
        for obj in self.browse(cr, uid, ids):
            if obj.pdf_file:
                printer_ids=printer_obj.search(cr,uid,[('paper_format','=',obj.paper_format)])
                for item in printer_obj.browse(cr,uid,printer_ids):
                    printer_name=item.printer_name
                    self.print_direct(cr, uid,ids,obj.pdf_file,'', printer_name)
        return True
    def create(self, cr, uid, vals, context=None):
        scheduled_datetime = vals.get('scheduled_datetime', False)
        if scheduled_datetime and isinstance(scheduled_datetime, str):
            scheduled_datetime = datetime.strptime(scheduled_datetime, '%Y-%m-%d %H:%M:%S')
        if scheduled_datetime < datetime.utcnow():
            scheduled_datetime = datetime.utcnow() + timedelta(minutes=1)
            new_scheduled_datetime = scheduled_datetime.strftime('%Y-%m-%d %H:%M:%S')
            vals.update({'scheduled_datetime': new_scheduled_datetime})
        new_picking_export = super(packing_export, self).create(cr, uid, vals, context=context)
        return new_picking_export

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args = self.pool.get('res.users').search_args_user_warehouses(cr, uid, args=args, field='warehouse', context=context)
        return super(packing_export, self).search(cr, uid, args, offset, limit, order, context, count)

    def unlink(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids):
            if obj.cron_id:
                try:
                    self.pool.get('ir.cron').unlink(cr, uid, [obj.cron_id.id], context)
                except:
                    raise osv.except_osv(_('Error'), _('Cannot remove a running process.'))
        return super(packing_export, self).unlink(cr, uid, ids, context=context)

    def search_orders_by_loc_wh(self, cr, uid, params):
        picking_obj = self.pool.get('stock.picking')
        search_vals = [('state', '=', 'assigned'), ('process_by_item', '=', False)]
        if params['partner_firm_id']:
            partner_ids = self.pool.get('res.partner').search(cr, uid, [('title', '=', params['partner_firm_id'])])
            search_vals.append(('real_partner_id', 'in', partner_ids))
        elif params['real_partner_id']:
            if params['real_partner_id'] > 0:
                search_vals.append(('real_partner_id', '=', params['real_partner_id']))
            else:
                partner_ids = self.pool.get('res.partner').search(cr, uid, [('white_paper', '=', True)])
                search_vals.append(('real_partner_id', 'in', partner_ids))
        search_ids = picking_obj.search(cr, uid, search_vals)

        rejected_ids = set()
        for picking in picking_obj.browse(cr, uid, search_ids):

            # exclude FLASHes
            if picking.sale_id.flash:
                rejected_ids.add(picking.id)
                continue

            if params['loc_ids']:
                has_move_in_loc_ids = False
                for move in picking.move_lines:
                    if move.location_id.id in params['loc_ids']:
                        has_move_in_loc_ids = True
                        break
                if not has_move_in_loc_ids:
                    rejected_ids.add(picking.id)
                    continue

            if params['wh_id'] and picking_obj.get_warehouse(cr, uid, picking.id) not in params['wh_id']:
                rejected_ids.add(picking.id)
                continue

            if params['s2s'] == 's2s' and not picking.sale_id.ship2store or params['s2s'] == 'regular' and picking.sale_id.ship2store:
                rejected_ids.add(picking.id)
                continue

            if params['value_from'] and picking.total_price < params['value_from']:
                rejected_ids.add(picking.id)
                continue

            if params['value_to'] and picking.total_price > params['value_to']:
                rejected_ids.add(picking.id)
                continue

            if params['multi_qty'] and len(picking.move_lines) == 1 and picking.move_lines[0].product_qty == 1:
                rejected_ids.add(picking.id)
                continue

            if params['changed_size']:
                has_move_with_changed_size = False
                for line in picking.move_lines:
                    if not line.is_no_resize and line.size_id and line.sale_line_id and line.size_id.id != (line.sale_line_id.size_id and line.sale_line_id.size_id.id or 0):
                        has_move_with_changed_size = True
                        break
                if not has_move_with_changed_size:
                    rejected_ids.add(picking.id)
                    continue

        search_ids = list(set(search_ids) - rejected_ids)
        # Force incoming exceptions and exclude them from the batch
        picking_obj.check_incoming_code(cr, uid, search_ids)
        search_ids = picking_obj.search(cr, uid, [('id', 'in', search_ids), ('state', '=', 'assigned')])

        if params['limited_print'] and len(search_ids) > params['batch_size']:
            search_ids = sorted(search_ids, key={
                picking.id: picking.sale_id.create_date for picking in picking_obj.browse(cr, uid, search_ids)
            }.get)
            search_ids = search_ids[:params['batch_size']]
        if params['sort'] == 'desc':
            search_ids = sorted(search_ids, key={
                picking.id: max([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['zzz']) for picking in picking_obj.browse(cr, uid, search_ids)
            }.get)
        else:
            search_ids = sorted(search_ids, key={
                picking.id: min([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['']) for picking in picking_obj.browse(cr, uid, search_ids)
            }.get)
        return search_ids

    # Cron function
    def daily_packslip_scheduling(self, cr, uid, context=None, store_period=30):
        scheduler_obj = self.pool.get('packing.scheduler')
        loc_obj = self.pool.get('stock.location')
        now_time = datetime.now()
        current_weekday = now_time.weekday()
        weekday_field_name = 'weekday' + str(current_weekday)

        for scheduler_id in scheduler_obj.search(cr, uid, [('type', '=', 'daily')]):
            scheduler = scheduler_obj.browse(cr, uid, scheduler_id)
            location = scheduler.location or False
            loc_ids = location and loc_obj.search(cr, uid, [('name', '=', location)]) or []
            wh_ids = [wh.id for wh in scheduler.warehouse]
            real_partner_id = scheduler.real_partner_id and scheduler.real_partner_id.id or False
            partner_firm_id = scheduler.partner_firm_id and scheduler.partner_firm_id.id or False
            ship_service_id = scheduler.ship_service_id and scheduler.ship_service_id.id or False
            s2s = scheduler.s2s or 'all'
            white_paper = scheduler.white_paper or False
            combine = scheduler.combine
            flat_rate = scheduler.flat_rate
            value_to = scheduler.value_to or 0
            value_from = scheduler.value_from or 0
            multi_qty = scheduler.multi_qty
            changed_size = scheduler.changed_size
            separate_ship_method = scheduler.separate_ship_method
            scheduled_datetime = scheduler.scheduled_datetime or False
            try:
                scheduled_datetime = datetime.strptime(scheduled_datetime, '%Y-%m-%d %H:%M:%S')
            except:
                continue
            scheduled_datetime = datetime(now_time.year, now_time.month, now_time.day, scheduled_datetime.hour, scheduled_datetime.minute, scheduled_datetime.second)
            if not any(map(lambda wh_id, loc_ids=loc_ids, scheduled_datetime=scheduled_datetime: (
                       loc_ids or wh_id) and scheduled_datetime, wh_ids)):
                continue
            sort = scheduler.sort or ''
            multi_print = scheduler.multi_print or False
            interval_count = scheduler.interval_count or 1
            interval_value = scheduler.interval_value or 1
            interval_uom = scheduler.interval_uom or 'hours'
            if not scheduler_obj.read(cr, uid, scheduler.id, [weekday_field_name])[weekday_field_name]:
                continue
            if current_weekday > 4:
                weekend_printing = scheduler.weekend_printing or 'first'
                if weekend_printing == 'not':
                    continue
                elif weekend_printing == 'first':
                    multi_print = False
            planned_ids = self.search(cr, uid, [
                ('real_partner_id', '=', real_partner_id),
                ('partner_firm_id', '=', partner_firm_id),
                ('white_paper', '=', white_paper),
                ('ship_service_id', '=', ship_service_id),
                ('s2s', '=', s2s),
                ('scheduled_datetime', '=', scheduled_datetime.strftime('%Y-%m-%d %H:%M:%S')),
                ('warehouse', 'in', wh_ids),
                ('location', '=', location),
            ])
            if not planned_ids:
                filename = scheduled_datetime.strftime('%Y%m%d_%H%M_') + ('_'.join((warehouse.name for warehouse in self.pool.get('stock.warehouse').browse(cr, uid, wh_ids, context=context))) + '_' or '') + (location and location + '_' or '') + 'daily'
                self_id = self.create(cr, uid, {
                    'warehouse':  [(6, 0, wh_ids)],
                    'location': location,
                    'sort': sort,
                    'real_partner_id': real_partner_id,
                    'partner_firm_id': partner_firm_id,
                    'ship_service_id': ship_service_id,
                    's2s': s2s,
                    'white_paper': white_paper,
                    'combine': combine,
                    'flat_rate': flat_rate,
                    'value_to': value_to,
                    'value_from': value_from,
                    'multi_qty': multi_qty,
                    'changed_size': changed_size,
                    'separate_ship_method': separate_ship_method,
                    'scheduled_datetime': scheduled_datetime,
                    'multi_print': multi_print,
                    'interval_count': interval_count,
                    'interval_value': interval_value,
                    'interval_uom': interval_uom,
                    'pdf_filename': filename + '.pdf',
                })
                self.export_confirm(cr, uid, [self_id])

        # Remove pdf files from old records
        to_remove_ids = self.search(cr, uid, [('scheduled_datetime', '<=', (datetime.now() - timedelta(days=store_period)).strftime('%Y-%m-%d')), ('pdf_file', 'not in', ['', False])])
        self.write(cr, uid, to_remove_ids, {
            'pdf_file': False,
            })
        return True

    # Cron function
    def hourly_packslip_scheduling(self, cr, uid, ids, params):
        scheduler_obj = self.pool.get('packing.scheduler')
        stock_picking_obj = self.pool.get('stock.picking')
        data = {'report_type': 'pdf'}
        cron_id = params['cron_id']
        # Get all scheduler ids related to our task
        active_scheduler_ids = scheduler_obj.search(cr, uid, [('type', '=', 'hourly')])
        # Get all schedulers corresponding to theirs ids
        active_schedulers = scheduler_obj.browse(cr, uid, active_scheduler_ids)
        for scheduler in active_schedulers:
            # sort by bin in ascending order
            ids = sorted(ids,
                         key={picking.id: min([move.bin_id.name for move in picking.move_lines if move.bin_id] or [''])
                              for
                              picking in stock_picking_obj.browse(cr, uid, ids)}.get)
            start, step = 0, BATCH_SIZE
            range_top = (divmod(len(ids), step)[0] + (1 if len(ids) % step > 0 else 0)) * step
            for i in range(0, range_top, step):
                batch_ids = ids[i: i + step]
                result = create(self.report_sxw_batch, cr, uid, batch_ids, data, context=dict(run_by_scheduler=True,
                                                                                              active_ids=batch_ids,
                                                                                              active_model='stock.picking',
                                                                                              uid=uid,
                                                                                              section_id=False,
                                                                                              active_id=batch_ids[0],
                                                                                              bin_raw=True,
                                                                                              project_id=False,
                                                                                              department_id=False,
                                                                                              ))
                if result[0]:
                    order_names = [str(pick.name) for pick in stock_picking_obj.browse(cr, uid, batch_ids)]
                    wh_id = stock_picking_obj.get_warehouse(cr, uid, batch_ids[0])
                    notes_str = self.pool.get('res.users').browse(cr, uid, uid).name + ' manually printed ' + str(
                        len(batch_ids)) + ' orders: ' + ', '.join(order_names)
                    self.pool.get('picking.export').create(cr, uid, {
                        'pdf_filename': 'manually_printed_packslips.pdf',
                        'pdf_file': base64.b64encode(result[0]),
                        'state': 'done',
                        'sort': 'asc',
                        'status': 'Manually printed',
                        'notes': notes_str,
                        'scheduled_datetime': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                        'printed_user': uid,
                        'warehouse': [wh_id],
                    })
                # Remove hourly scheduler fromm queue
                scheduler_obj.unlink(cr, uid, scheduler.id)

        # Remove pdf files from old records
        to_remove_ids = self.search(cr, uid, [('scheduled_datetime', '<=', (datetime.now() - timedelta(30)).strftime('%Y-%m-%d')), ('pdf_file', 'not in', ['', False])])
        self.write(cr, uid, to_remove_ids, {
            'pdf_file': False,
            })
        # Remove cron job from queue
        self.pool.get('ir.cron').unlink(cr, uid, [cron_id])

        return True

    # Cron function
    def export_csv_background(self, cr, uid, ids, params=None, context=None):
        if isinstance(params, dict):
            params = params.copy()
            for param_name in DEFAULT_BATCH_PACKSLIP_PARAMS:
                if param_name not in params:
                    params[param_name] = DEFAULT_BATCH_PACKSLIP_PARAMS[param_name]
        else:
            params = DEFAULT_BATCH_PACKSLIP_PARAMS.copy()

        notes = ''
        loc_ids = params['loc_ids'] or []
        sort = params['sort']
        real_partner_id = params['real_partner_id']
        partner_firm_id = params['partner_firm_id']
        ship_service_id = params['ship_service_id']
        separate_ship_method = params['separate_ship_method'] and not ship_service_id
        s2s = params['s2s']
        combine = params['combine']
        flat_rate = params['flat_rate']
        cron_id = params['cron_id']
        max_batch_size = params['max_batch_size']
        limited_print = params['limited_print']
        if not max_batch_size or max_batch_size < 0:
            max_batch_size = int(self.pool.get('ir.config_parameter').get_param(cr, uid, 'max_batch_size')) or 50
        max_count_batches = int(self.pool.get('ir.config_parameter').get_param(cr, uid, 'max_count_of_batches')) or 0
        if limited_print:
            notes += 'Limited print for %d orders\n' % max_batch_size
        print_ids = []
        cur = self.browse(cr, uid, ids[0])
        was_first_print = False
        picking_obj = self.pool.get('stock.picking')
        ship_code_obj = self.pool.get('res.shipping.code')
        if cur and self.search(cr, uid, [('id', 'in', ids), ('status', 'not in', ['Done', 'Printed'])]):
            all_search_ids = self.search_orders_by_loc_wh(cr, uid, params)
            if not all_search_ids:
                self.write(cr, uid, ids, {'status': 'Printed', 'notes': 'Orders not found', 'state': 'printed'})
            else:
                # FIND TO COMBINE
                to_combine_sets = {}
                if combine:
                    cr.execute("""SELECT address_grp, array_accum(id) as array_ids
                        FROM stock_picking
                        WHERE id in %s
                        GROUP BY address_grp
                        HAVING count(id) > 1
                    """, (tuple(all_search_ids),))
                    to_combine_res = cr.dictfetchall()
                    to_combine_sets = {comb['address_grp']: set(comb['array_ids']) for comb in to_combine_res}

                # COMBINE ACTIONS AND GROUP IN BATCHES
                batches = []  # list of sets
                # one batch is a dict like {'combined': bool, 'changed': bool, 'priority': string, 'ids': list}
                combined_ids = set()
                not_combined_ids = set(all_search_ids)

                for addr in to_combine_sets:
                    filled = False
                    to_combine_ids = list(to_combine_sets[addr])
                    try:
                        picking_obj.action_combine_stock_pickings(cr, uid, [], context={'active_ids': to_combine_ids, 'auto_process': False})
                    finally:
                        combine_cancelled_ids = set()
                        # Check combine success
                        for pick in picking_obj.browse(cr, uid, to_combine_ids):
                            if not pick.combined or (ship_service_id and ship_code_obj.get_shipping_service(cr, uid, pick.id) != ship_service_id):
                                combine_cancelled_ids.add(pick.id)
                        if combine_cancelled_ids:
                            if combine_cancelled_ids == to_combine_sets[addr]:
                                del to_combine_sets[addr]
                                break
                            else:
                                to_combine_sets[addr] -= combine_cancelled_ids
                        for batch in batches:
                            if len(batch | to_combine_sets[addr]) <= max_batch_size:
                                batch |= to_combine_sets[addr]
                                filled = True
                                break
                        if not filled:
                            batches.append(to_combine_sets[addr].copy())
                        combined_ids |= to_combine_sets[addr]
                        not_combined_ids -= to_combine_sets[addr]
                comb_batches_len = len(batches)
                batches = [{'combined': True, 'changed': False, 'priority': None, 'ids': x, 'ship_service_id': False} for x in batches]

                if ship_service_id:
                    not_combined_ids = set([not_combined_id for not_combined_id in not_combined_ids if ship_code_obj.get_shipping_service(cr, uid, not_combined_id) == ship_service_id])
                all_search_ids = list(combined_ids | not_combined_ids)

                if not all_search_ids:
                    self.write(cr, uid, ids, {'status': 'Printed', 'notes': 'Orders not found', 'state': 'printed'})

                # FIND ORDERS WITH CHANGED ITEMS/SIZES
                changed_ids = set([])
                for pick in picking_obj.browse(cr, uid, all_search_ids):
                    for line in pick.move_lines:
                        if line.is_item_changed:
                            changed_ids.add(pick.id)
                            continue
                changed_and_combined_ids = changed_ids - not_combined_ids
                changed_and_not_combined_ids = changed_ids & not_combined_ids

                # FIND ORDERS WITH PRIORITY SETTINGS
                priority_rate_groups = {}
                for pick in picking_obj.browse(cr, uid, list(not_combined_ids)):
                    priority = pick.flat_rate_priority_id
                    if priority:
                        if not priority_rate_groups.get(priority.id, False):
                            priority_rate_groups[priority.id] = {'name': priority.name, 'changed': set(), 'regular': set()}
                        priority_rate_groups[priority.id]['changed' if pick.id in changed_and_not_combined_ids else 'regular'].add(pick.id)

                def add_new_batches(batches, sort, max_batch_size, new_ids, batch_settings=None):
                    if batches is None:
                        batches = []
                    if new_ids:
                        if batch_settings is None:
                            batch_settings = {}
                        new_batches = []
                        if sort == 'desc':
                            not_filled_ids = sorted(list(new_ids), key={picking.id: max([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['zzz']) for picking in picking_obj.browse(cr, uid, list(new_ids))}.get)
                        else:
                            not_filled_ids = sorted(list(new_ids), key={picking.id: min([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['']) for picking in picking_obj.browse(cr, uid, list(new_ids))}.get)
                        nn = 0
                        count_batches = len(new_ids)/max_batch_size
                        while nn <= count_batches:
                            batch_list = not_filled_ids[nn*max_batch_size:(nn+1)*max_batch_size]
                            nn += 1
                            if batch_list:
                                batch = batch_settings.copy()
                                batch.update({'ids': batch_list})
                                new_batches.append(batch)
                        batches.extend(new_batches)

                    return batches

                # GROUP IN BATCHES (PRIORITY AND CHANGED AND NOT COMBINED)
                for priority_id in priority_rate_groups:
                    priority = priority_rate_groups[priority_id]
                    not_combined_ids -= priority['regular']
                    batches = add_new_batches(batches, sort, max_batch_size, priority['regular'], batch_settings={'changed': False, 'priority': priority['name']})
                    not_combined_ids -= priority['changed']
                    batches = add_new_batches(batches, sort, max_batch_size, priority['changed'], batch_settings={'changed': True, 'priority': priority['name']})

                # GROUP IN BATCHES (CHANGED AND NOT COMBINED)
                changed_ids &= not_combined_ids
                batches = add_new_batches(batches, sort, max_batch_size, changed_ids, batch_settings={'combined': False, 'changed': True})

                # GROUP IN BATCHES (NOT COMBINED)
                not_combined_ids -= changed_ids
                batches = add_new_batches(batches, sort, max_batch_size, not_combined_ids, batch_settings={'combined': False, 'changed': False})

                # Oh, Lord, it's a grave sin to write a shit code like below. I regret it and will do refactoring when will have time. RAmen! (YI 2015-12-03)
                # SEPARATE BY SHIP METHOD
                if separate_ship_method:
                    regroup_batches = []
                    for batch in batches:
                        groups_ship_method = {}
                        for pick_id in batch['ids']:
                            cur_ship_id = ship_code_obj.get_shipping_service(cr, uid, pick_id)
                            if not groups_ship_method.get(cur_ship_id):
                                groups_ship_method.update({cur_ship_id: []})
                            groups_ship_method[cur_ship_id].append(pick_id)
                        for group_method in groups_ship_method:
                            regroup = None
                            for regroup_batch in regroup_batches:
                                if (regroup_batch.get('combined', False) == batch.get('combined', False)
                                    and regroup_batch.get('changed', False) == batch.get('changed', False)
                                    and regroup_batch.get('priority', False) == batch.get('priority', False)
                                    and regroup_batch.get('ship_service_id') == group_method
                                ):
                                    regroup = regroup_batch
                                    break
                            if not regroup:
                                regroup = {
                                    'combined': batch.get('combined', False),
                                    'changed': batch.get('changed', False),
                                    'priority': batch.get('priority', False),
                                    'ship_service_id': group_method,
                                    'ids': [],
                                }
                                regroup_batches.append(regroup)
                            regroup['ids'].extend(groups_ship_method[group_method])
                    # break again by max_batch_size
                    new_regroup_batches = []
                    comb_counter = 0
                    for regroup_batch in regroup_batches:
                        if len(regroup_batch['ids']) > max_batch_size:
                            if regroup_batch['combined']:
                                to_fill_combines = set(regroup_batch['ids'])
                                new_combined_batches = []
                                for addr in to_combine_sets:
                                    combine_mini_set = to_fill_combines & to_combine_sets[addr]
                                    if combine_mini_set:
                                        filled = False
                                        for new_combined_batch in new_combined_batches:
                                            if len(new_combined_batch | combine_mini_set) <= max_batch_size:
                                                new_combined_batch |= combine_mini_set
                                                filled = True
                                                break
                                        if not filled:
                                            new_combined_batches.append(combine_mini_set)
                                for new_combined_batch in new_combined_batches:
                                    new_batch = regroup_batch.copy()
                                    new_batch.update({'ids': list(new_combined_batch)})
                                    new_regroup_batches.append(new_batch)
                                    comb_counter += 1
                            else:
                                nn = 0
                                count_batches = len(regroup_batch['ids']) / max_batch_size
                                while nn <= count_batches:
                                    batch_list = regroup_batch['ids'][nn * max_batch_size:(nn + 1) * max_batch_size]
                                    nn += 1
                                    new_batch = regroup_batch.copy()
                                    new_batch.update({'ids': batch_list})
                                    new_regroup_batches.append(new_batch)
                        else:
                            new_regroup_batches.append(regroup_batch)
                            if regroup_batch['combined']:
                                comb_counter += 1
                    batches = new_regroup_batches
                    comb_batches_len = comb_counter

                # MAIN CYCLE - PRINT BATCHES
                batch_nn = 0

                if max_count_batches and max_count_batches > 0:
                    batches = batches[:max_count_batches]
                for batch_obj in batches:
                    batch = batch_obj.get('ids', set([]))
                    print_ids = picking_obj.search(cr, uid, [('id', 'in', list(batch)), ('state', '=', 'assigned'), ('process_by_item', '=', False)])
                    # Sort: changed before
                    if print_ids:
                        if comb_batches_len > batch_nn:
                            # Skip all the combined batches. They should not be in a flat_rate batch.
                            if flat_rate:
                                batch_nn += 1
                                continue
                            batch_changed_ids = []
                            if changed_and_combined_ids:
                                batch_changed_ids = list(set(print_ids) & changed_and_combined_ids)
                                print_ids = batch_changed_ids + list(set(print_ids) - changed_and_combined_ids)
                            combined_lists = []  # list of lists
                            not_filled_prints_ids = print_ids[:]
                            while len(not_filled_prints_ids) != 0:
                                curr_id = not_filled_prints_ids.pop()
                                for addr in to_combine_sets:
                                    if curr_id in to_combine_sets[addr]:
                                        comb_list = [curr_id]
                                        for comb_id in to_combine_sets[addr]:
                                            if comb_id in not_filled_prints_ids:
                                                comb_list.append(comb_id)
                                                not_filled_prints_ids.remove(comb_id)
                                        combined_lists.append(comb_list)
                                        break
                            combined_strs = []
                            for comb_list in combined_lists:
                                comb_order_names = [str(pick.name) for pick in picking_obj.browse(cr, uid, comb_list)]
                                combined_strs.append('[' + ', '.join(comb_order_names) + ']')
                            orders_str = str(len(batch)) + ' combined orders' + (batch_changed_ids and ' (some of items were CHANGED)' or '') + ': ' + ', '.join(combined_strs)
                            ordered_print_ids = [curr_id for comb_list in combined_lists for curr_id in comb_list]
                        else:
                            # Flat rate is for priority only
                            if flat_rate and not batch_obj.get('priority', False):
                                batch_nn += 1
                                continue
                            # NOT Flat rate is for not priority only
                            if not flat_rate and batch_obj.get('priority', False):
                                batch_nn += 1
                                continue

                            if sort == 'desc':
                                ordered_print_ids = sorted(list(batch), key={picking.id: max([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['zzz']) for picking in picking_obj.browse(cr, uid, list(batch))}.get)
                            else:
                                ordered_print_ids = sorted(list(batch), key={picking.id: min([move.bin_id.name for move in picking.move_lines if move.bin_id] or ['']) for picking in picking_obj.browse(cr, uid, list(batch))}.get)

                        try:
                            pdf_file = self.report_sxw_batch.create(cr, uid, ordered_print_ids, {'model': 'stock.picking', 'report_type': 'pdf'}, {
                                'report_id': 'report_picking_list',
                                'report_rml': 'stock/report/picking.rml',
                                'report_name': 'stock.picking.list',
                                'report_model': 'stock.picking',
                            })[0]
                            if comb_batches_len <= batch_nn:
                                info = ' %s orders%s: ' % (
                                    batch_obj['priority'] + ' ' if batch_obj.get('priority', False) else '',
                                    'with CHANGED item(s)' if batch_obj.get('changed', False) else '',
                                )
                                not_comb_order_names = [str(pick.name) for pick in picking_obj.browse(cr, uid, ordered_print_ids)]
                                orders_str = str(len(not_comb_order_names)) + info + ', '.join(not_comb_order_names)

                            # notes = notes + orders_str
                            problematic_orders = [str(pick.name) for pick in picking_obj.browse(cr, uid, ordered_print_ids) if pick.state in ('confirmed', 'assigned', 'waiting_split', 'address_except', 'incode_except')]
                            if problematic_orders:
                                orders_str += '\nWARNING! ' + str(len(problematic_orders)) + ' orders have not been processed and printed: ' + ', '.join(problematic_orders)
                            if not was_first_print:
                                vals = {
                                    'status': 'Done',
                                    'flat_rate': bool(batch_obj.get('priority', False)),
                                    'pdf_file': base64.encodestring(pdf_file),
                                    'notes': orders_str,
                                    'state': 'done'
                                }
                                if separate_ship_method:
                                    vals.update({'ship_service_id': batch_obj.get('ship_service_id')})
                                self.write(cr, uid, ids, vals)
                                was_first_print = True
                            else:
                                vals = {
                                    'flat_rate': bool(batch_obj.get('priority', False)),
                                    'status': 'Done',
                                    'pdf_file': base64.encodestring(pdf_file),
                                    'notes': orders_str,
                                    'state': 'done',
                                    'pdf_filename': cur.pdf_filename[:-4] + '_' + str(batch_nn) + '.pdf'
                                }
                                if separate_ship_method:
                                    vals.update({'ship_service_id': batch_obj.get('ship_service_id')})
                                new_id = self.copy(cr, uid, ids[0], default=vals, context=None)
                            batch_nn += 1
                        except Exception, e:
                            self.write(cr, uid, ids, {'status': 'Error! ' + str(e)})
                            if cron_id:
                                self.pool.get('ir.cron').unlink(cr, uid, [cron_id], context)
                            return True
                    elif not was_first_print:
                        self.write(cr, uid, ids, {
                            'status': 'Printed',
                            'notes': 'Orders not found',
                            'state': 'printed',
                        })
        report_data = self.read(cr, uid, ids[0], ['pdf_file'])
        if not (was_first_print or report_data and report_data.get('pdf_file')):
            try:
                self.write(cr, uid, ids, {
                    'status': 'Printed',
                    'notes': 'Orders not found.',
                    'state': 'printed',
                })
            except:
                pass
        if cron_id:
            self.pool.get('ir.cron').unlink(cr, uid, [cron_id], context)
        return True

    def export_done(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {'state': 'done'})
        return True

    def export_printed(self, cr, uid, ids, context=None):
        username = self.pool.get('res.users').browse(cr, uid, uid).name
        for cur_id in ids:
            cur = self.browse(cr, uid, cur_id)
            self.write(cr, uid, [cur_id], {'state': 'printed', 'notes': cur.notes and (username + ' printed: ' + cur.notes) or '', 'status': 'Printed', 'printed_user': uid})
        return True

    def add_tag_from_list(self, cr, uid, ids, context=None):
        select_ids = list(context['active_ids'])
        picking_exports = self.pool.get('picking.export').browse(cr, uid, select_ids)
        ids_list = []
        for picking_export in picking_exports:
            order_list = picking_export.notes
            ids_list += self.get_orders_list_from_string(order_list)

        ctx = context.copy()
        ctx.update({'default_order_list': '\n'.join(ids_list)})
        res = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tagging.order',
            'target': 'new',
            'res_id': False,
            'type': 'ir.actions.act_window',
            'flags': {'action_buttons': True},
            'context': ctx,
        }

        return res

    def tag_add_select(self, cr, uid, ids, context=None):
        picking_export = self.pool.get('picking.export').browse(cr, uid, ids, context=context)

        res = {}
        if picking_export and picking_export[0].notes:
            order_list = picking_export[0].notes

            ids_list = self.get_orders_list_from_string(order_list)

            ctx = context.copy()
            ctx.update({'default_order_list': ids_list})
            res = {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'order.tag.selector',
                'target': 'new',
                'res_id': False,
                'type': 'ir.actions.act_window',
                'flags': {'action_buttons': False},
                'context': ctx,
            }
        return res

    # TODO: deprecated method, need to be deleted possibly
    def tag_add(self, cr, uid, ids, context=None):

        picking_export = self.pool.get('picking.export').browse(cr, uid, ids, context=context)

        res = {}
        if picking_export and picking_export[0].notes:
            order_list = picking_export[0].notes

            ids_list = self.get_orders_list_from_string(order_list)

            ctx = context.copy()
            ctx.update({'default_order_list': '\n'.join(ids_list)})
            res = {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'tagging.order',
                'target': 'new',
                'res_id': False,
                'type': 'ir.actions.act_window',
                'flags': {'action_buttons': True},
                'context': ctx,
            }
        return res

    def get_orders_list_from_string(self, string):
        regex = re.compile(r"(?<=:\s)(?P<orders_list>\[?[A-Z\a-z\d\-\._]{4,}[,?\s\]]?,?\s*){1,}")
        list_of_orders = regex.finditer(string)
        orders_str = ', '.join([value.group(0) for value in list_of_orders])
        orders_str = re.sub('\[|\]', '', orders_str)
        orders = re.split('\s*,?\s+', orders_str)
        new_orders = []
        for order in orders:
            letter = re.search(r"[A-Z]", order)
            number = re.search(r"\d", order)
            dash   = re.search(r"-", order)

            if not (letter and number and dash):
                continue
            new_orders.append(order)
        return new_orders

    def export_confirm(self, cr, uid, ids, *args):
        cur = self.browse(cr, uid, ids[0])
        location_obj = self.pool.get('stock.location')
        if not cur.location and not cur.warehouse:
            raise osv.except_osv(_('Error'), _('Not selected warehouse or location. Need at least one of them'))
        scheduled_datetime = False
        loc_ids = []
        wh_ids = [wh.id for wh in cur.warehouse]
        sort = cur.sort or ''
        real_partner_id = cur.real_partner_id and cur.real_partner_id.id or 0
        partner_firm_id = cur.partner_firm_id and cur.partner_firm_id.id or 0
        ship_service_id = cur.ship_service_id and cur.ship_service_id.id or 0
        flat_rate = cur.flat_rate or False
        value_to = cur.value_to or 0
        value_from = cur.value_from or 0
        multi_qty = cur.multi_qty or False
        changed_size = cur.changed_size or False
        separate_ship_method = cur.separate_ship_method or False
        s2s = cur.s2s or 'all'
        notes = ''
        batch_size = 0
        limited_print = 0
        if not (real_partner_id or partner_firm_id):
            white_paper = cur.white_paper or False
            if white_paper:
                real_partner_id = -1
        else:
            white_paper = cur.real_partner_id.white_paper
        if cur.location:
            loc_ids = location_obj.search(cr, uid, [('name', '=', cur.location)])
            if not loc_ids:
                raise osv.except_osv(_('Error'), _('Locations not found'))
        if cur.scheduled_datetime:
            try:
                scheduled_datetime = datetime.strptime(cur.scheduled_datetime, '%Y-%m-%d %H:%M:%S')
            except:
                raise osv.except_osv(_('Error'), _('Wrong datetime format'))
            if scheduled_datetime <= datetime.now():
                raise osv.except_osv(_('Error'), _('Datetime in the past'))
        if value_to < 0 or value_from < 0:
            raise osv.except_osv(_('Error'), _('"$ Values" cannot be negative!'))
        if value_to and value_from and value_to < value_from:
            raise osv.except_osv(_('Error'), _('"$ Value To" is less than "$ Value From"!'))
        if cur.batch_size:
            try:
                batch_size = int(cur.batch_size)
                limited_print = 1
                notes = 'Limited print for %d orders\n' % batch_size
            except:
                raise osv.except_osv('Error', 'Batch size should be integer')
            if cur.batch_size < 1 or cur.batch_size > 50:
                raise osv.except_osv(_('Error'), _('Batch size should be greater than 0 and less or equal than 50'))
        combine = int(bool(cur.combine))
        cron_obj = self.pool.get('ir.cron')
        params = {
            'loc_ids': loc_ids,
            'wh_id': wh_ids,
            'sort': sort,
            'real_partner_id': real_partner_id,
            'partner_firm_id': partner_firm_id,
            'ship_service_id': ship_service_id,
            's2s': s2s,
            'combine': combine,
            'flat_rate': flat_rate,
            'value_to': value_to,
            'value_from': value_from,
            'multi_qty': multi_qty,
            'changed_size': changed_size,
            'separate_ship_method': separate_ship_method,
            'batch_size': batch_size,
            'limited_print': limited_print,
            'cron_id': 0,
        }
        vals = {
            'user_id': uid,
            'model': self._name,
            'interval_type': 'minutes',
            'function': 'export_csv_background',
            'args': "(%s,%s)" % (ids, params),
            'numbercall': -1,
            'priority': 4,
            'type': 'printing_packings', # defined in pf_utils/ir/ir_cron.py
            'doall': False,
        }
        if scheduled_datetime:
            name_datetime = scheduled_datetime
            vals.update({'nextcall': scheduled_datetime})
        else:
            name_datetime = datetime.now()
        filename = name_datetime.strftime('%Y%m%d_%H%M%S%f') +  str(random.randrange(1, 10000)) + ('_'.join((str(uid) for uid in wh_ids)) + '_' or '') + (cur.location and cur.location + '_' or '') + cur.create_uid.name
        vals.update({'name': filename + '_packings'})

        cron_id = cron_obj.create(cr, uid, vals)
        params['cron_id'] = cron_id
        cron_obj.write(cr, uid, [cron_id], {'args': "(%s,%s)" % (ids, params)})

        self.write(cr, uid, ids, {'state': 'confirm', 'info': 'Request has been submitted', 'status': 'In Process', 'pdf_filename': filename + '.pdf', 'cron_id': cron_id, 'white_paper': white_paper, 'notes': notes})

        if cur.multi_print and cur.interval_count and cur.interval_count > 1 and cur.interval_value:

            delta = cur.interval_uom == 'hours' and timedelta(hours=cur.interval_value) or cur.interval_uom == 'days' and timedelta(days=cur.interval_value) or cur.interval_uom == 'minutes' and timedelta(minutes=cur.interval_value) or False
            if not delta:
                return True

            for i in range(1, cur.interval_count)[:40]:
                new_datetime = name_datetime + i * delta
                filename = new_datetime.strftime('%Y%m%d_%H%M_') + ('_'.join((str(uid) for uid in wh_ids)) + '_' or '') + (cur.location and cur.location + '_' or '') + cur.create_uid.name
                new_id = self.copy(cr, uid, ids[0], default={'scheduled_datetime': new_datetime, 'pdf_filename': filename + '.pdf'}, context=None)
                params['cron_id'] = 0
                vals.update({
                    'name': filename + '_packings',
                    'nextcall': new_datetime,
                    'args': "(%s,%s)" % ([new_id], params),
                })
                new_cron_id = cron_obj.create(cr, uid, vals)
                params['cron_id'] = new_cron_id
                cron_obj.write(cr, uid, [new_cron_id], {'args': "(%s,%s)" % ([new_id], params)})
                self.write(cr, uid, [new_id], {'cron_id': new_cron_id})
        return True

packing_export()


class PackingTagSelector(osv.osv_memory):
    _name = 'order.tag.selector'
    _columns = {
        'order_tag': fields.many2one('tagging.order', 'Order tag', ),
    }
    _defaults = {}

    def use_selected(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context)
        if not obj.order_tag.id:
            return False
        tag = self.pool.get('tagging.order').browse(cr, uid, obj.order_tag.id)
        tag.write({'order_list': '\n'.join(context.get('default_order_list'))})
        ctx = context.copy()
        ctx.update({'active_id': obj.id})
        ctx.update({'active_ids': [obj.id]})
        res = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tagging.order',
            'target': 'new',
            'res_id': obj.order_tag.id,
            'type': 'ir.actions.act_window',
            'flags': {'action_buttons': True},
            'context': ctx,
        }
        return res

    def create_new(self, cr, uid, ids, context=None):
        ctx = context.copy()
        ctx.update({'default_order_list': '\n'.join(context.get('default_order_list'))})
        res = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tagging.order',
            'target': 'new',
            'res_id': False,
            'type': 'ir.actions.act_window',
            'flags': {'action_buttons': True},
            'context': ctx,
        }
        return res


class packing_scheduler(osv.osv):
    _name = 'packing.scheduler'

    def _get_name(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, False)

        if ids:
            cr.execute("""  SELECT
                                ps.id as id,
                                CONCAT(sw.name, E'/', coalesce(rpt.name, rp.name, 'White Paper'), E'/', ss.name, E'/', ps.id) as name
                            FROM packing_scheduler ps
                                LEFT JOIN stock_warehouse sw ON ps.warehouse = sw.id
                                LEFT JOIN res_partner rp ON ps.real_partner_id = rp.id
                                LEFT JOIN res_partner_title rpt ON ps.partner_firm_id = rpt.id
                                LEFT JOIN res_shipping_service ss on ps.ship_service_id = ss.id
                            WHERE ps.id in %s
                            ;
                    """, (tuple(ids), ))
            for row_id, name in cr.fetchall():
                result[row_id] = name

        return result

    def _get_warehouse_names(self, cr, uid, ids, field_name, arg, context):
        return {packing_export_id: ', '.join((warehouse.name for warehouse in self.browse(cr, uid, packing_export_id, context=context).warehouse)) for packing_export_id in ids}

    _columns = {
        'warehouse': fields.many2many('stock.warehouse', 'scheduler_stock_warehouse_rel', 'scheduler_id', 'warehouse_id', 'Warehouses'),
        'warehouse_names': fields.function(
            _get_warehouse_names,
            type='string',
            obj="stock.warehouse",
            method=True,
            string='Warehouses'),

        'location': fields.char('Location', size=256),
        'sort': fields.selection([('asc', 'Shelf (A-Z)'), ('desc', 'Shelf (Z-A)')], 'Sorting', select=True),
        'real_partner_id': fields.many2one('res.partner', 'Partner', select=True, domain="[('customer', '=', True)]"),
        'partner_firm_id': fields.many2one('res.partner.title', 'Partner Firm', select=True),
        'ship_service_id': fields.many2one('res.shipping.service', 'Shipping Service', select=True),
        'scheduled_datetime': fields.datetime('Scheduled Date and Time'),
        'multi_print': fields.boolean('Multi Print'),
        'interval_uom': fields.selection([('minutes', 'Minutes'), ('hours', 'Hours'), ('days', 'Days')], 'Interval UoM', select=True, required=True),
        'interval_value': fields.integer('Interval value'),
        'interval_count': fields.integer('Count of prints'),
        'weekend_printing': fields.selection([
                ('all', 'All Prints'),
                ('not', 'Do not Print'),
                ('first', 'First Print Only'),
                # ('last', 'Last Print Only'),
                # ('first_last', 'First and Last Prints Only')
            ], 'Weekend printing', select=True),
        'type': fields.selection([('hourly', 'Hourly'), ('daily', 'Daily')], 'Scheduler Type', select=True, required=True),
        'min_limit': fields.integer('Minimal count for printing'),
        'break_by_carrier_partner_ids': fields.many2many('res.partner', 'scheduler_partner_rel', 'scheduler_id', 'partner_id', 'Breaking by carriers customers'),
        'break_by_s2s_ids': fields.many2many('res.partner', 'scheduler_partner_s2s_rel', 'scheduler_id', 'partner_id', 'Breaking by order type (Ship to store or Regular) customers'),
        's2s': fields.selection([('all', 'All'), ('s2s', 'Only Ship to Store'), ('regular', 'Only Regular Orders')], 'Ship to Store Options', select=True, required=True),
        'white_paper': fields.boolean('Print Packslips on White Paper'),
        'combine': fields.boolean('Combine'),
        'flat_rate': fields.boolean('Flat Rate Box'),
        'value_from': fields.float('$ Value From'),
        'value_to': fields.float('$ Value To'),
        'multi_qty': fields.boolean('Items with multi qty'),
        'changed_size': fields.boolean('Items with changed sizes'),
        'separate_ship_method': fields.boolean('Separate by Ship Method'),
        'weekday0': fields.boolean('Mo'),
        'weekday1': fields.boolean('Tu'),
        'weekday2': fields.boolean('We'),
        'weekday3': fields.boolean('Th'),
        'weekday4': fields.boolean('Fr'),
        'weekday5': fields.boolean('Sa'),
        'weekday6': fields.boolean('Su'),
        'name': fields.function(_get_name, string='Name', type='char', size=128),
        'order_ids': fields.text('', 'Order IDs (separate by comma)'),
    }

    _defaults = {
        'multi_print': False,
        'interval_uom': 'hours',
        'interval_value': 1,
        'interval_count': 1,
        'min_limit': 20,
        'weekend_printing': 'first',
        's2s': 'all',
        'combine': False,
        'flat_rate': False,
        'value_to': 0,
        'value_from': 0,
        'multi_qty': False,
        'changed_size': False,
        'separate_ship_method': False,
        'weekday0': True,
        'weekday1': True,
        'weekday2': True,
        'weekday3': True,
        'weekday4': True,
        'weekday5': False,
        'weekday6': False,
    }

    def add_multi_orders_task(self, cr, uid, ids):
        sql_params = {
            'create_uid': uid,
            'create_date': datetime.now(),
            'write_date': datetime.now(),
            'write_uid': uid,
            'sort': 'asc',
            'type': 'hourly',
            'interval_uom': 'hours',
            'interval_value': 0,
            'interval_count': 1,
            'weekend_printing': 'not',
            's2s': 'all',
            'min_limit': 1,
            'multi_print': True,
            'weekday0': True,
            'weekday1': True,
            'weekday2': True,
            'weekday3': True,
            'weekday4': True,
            'weekday5': True,
            'weekday6': True,
            'order_ids': ','.join([str(x) for x in ids]),
        }

        cr.execute("""
                INSERT INTO
                    packing_scheduler (create_uid, create_date, write_date, write_uid, sort, type, interval_uom, interval_value, interval_count, weekend_printing, s2s, min_limit, multi_print, weekday0, weekday1, weekday2, weekday3, weekday4, weekday5, weekday6, order_ids)
                VALUES (%(create_uid)s, %(create_date)s, %(write_date)s, %(write_uid)s, %(sort)s, %(type)s, %(interval_uom)s, %(interval_value)s, %(interval_count)s, %(weekend_printing)s, %(s2s)s, %(min_limit)s, %(multi_print)s, %(weekday0)s, %(weekday1)s, %(weekday2)s, %(weekday3)s, %(weekday4)s, %(weekday5)s, %(weekday6)s, %(order_ids)s)
            """, sql_params)

        return True

packing_scheduler()
