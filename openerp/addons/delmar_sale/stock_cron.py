from osv import fields, osv
from tools.translate import _
import logging

logger = logging.getLogger(__name__)

class StockCron(osv.osv_memory):
    _name = "stock.cron"

    # ------------------
    # QUERIES' TEMPLATES
    # ------------------
    # DLMR-1325: Get sales transactions that has no return assigned
    # !!!!!! REMOVE TOP 1 FOR PRODUCTION
    sql_sle_no_ret_tr = """
        SELECT 
            sle.ID, sle.WAREHOUSE, sle.STOCKID, sle.BIN, sle.ITEMID, sle.QTY, sle.TRANSACTDATE
        FROM dbo.TRANSACTIONS sle
            LEFT JOIN dbo.TRANSACTIONS ret ON 
                ret.WAREHOUSE = 'USRETURN'
                AND ret.STOCKID = 'RETURN' 
                AND ret.BIN=sle.BIN 
                AND ret.TRANSACTIONCODE IN ('RET')
                AND ret.ITEMID=sle.ITEMID 
                AND ret.QTY=sle.QTY 
                AND ret.PARENT_SALE_TR=sle.ID
        WHERE
            sle.WAREHOUSE = 'USRETURN' 
            AND sle.STOCKID = 'RETURN' 
            AND sle.BIN IN ('HKA', 'HKB')
            AND sle.TRANSACTIONCODE IN ('SLE', 'SHP' ,'ADJ') 
            AND sle.ADJUSTED_QTY<0
            AND ret.ID is NULL
        ORDER BY sle.ID DESC
    """
    # DLMR-1325: Return transaction for sale transaction
    sql_ret_for_sle_tr = """
        SELECT TOP 1
            ret.ID, ret.WAREHOUSE, ret.STOCKID, ret.BIN, ret.TRANSACTDATE, ret.TRANSACTIONCODE
        FROM dbo.TRANSACTIONS ret
        WHERE
            ret.PARENT_SALE_TR IS NULL AND ret.WAREHOUSE = 'USRETURN' AND ret.STOCKID = 'RETURN' 
            AND ret.TRANSACTIONCODE IN ('RET') 
            AND ret.BIN = %(sle_bin)s
            AND ret.QTY = %(sle_qty)s
            AND ret.ITEMID = %(sle_itemid)s
            AND ret.TRANSACTDATE <= cast(%(sle_trdate)s as DATETIME)
        ORDER BY TRANSACTDATE DESC, ID DESC
    """
    # DLMR-1325: Update Return transaction to ADJ and set sale transaction id to parent_sale_id
    sql_upd_ret_to_adj = """
        UPDATE dbo.TRANSACTIONS
        SET TRANSACTIONCODE = 'ADJ', PARENT_SALE_TR = %(sle_id)s
        WHERE ID = %(ret_id)s 
    """

    # DLMR-1738: This sql was created by Serhii Dekun. He said keep it simple stupid )
    sql_inspired_by_serhii_dekun = """
        update dbo.TRANSACTIONS set TRANSACTIONCODE='ADJ', PARENT_SALE_TR = 1
              from
            (select sel_ret.balance,sel_ret.qty,sel_ret.itemid,sel_ret.id,sel_main.mark_qty,sel_ret.PARENT_SALE_TR
            from
            (select sel.itemid ,
        (case when sales>=rets then rets else sales end ) as mark_qty from
        (select
        itemid,
        sum(case when sle.TRANSACTIONCODE in ('ADJ','SLE','SHP') and sle.ADJUSTED_QTY<0 then sle.qty else 0 end) sales,
        sum(case when sle.TRANSACTIONCODE in ('RET','ADJ') and sle.ADJUSTED_QTY>0 then sle.qty else 0 end) rets
        from dbo.TRANSACTIONS sle where
        sle.WAREHOUSE = 'USRETURN'
            AND sle.STOCKID = 'RETURN'
            AND sle.BIN IN ('HKA', 'HKB')
            AND cast(sle.TRANSACTDATE as date)>='2018-08-28'
            AND (sle.TRANSACTIONCODE IN ('RET') or (sle.TRANSACTIONCODE='ADJ' and sle.ADJUSTED_QTY>0 and sle.PARENT_SALE_TR in (0,1))
            or (sle.TRANSACTIONCODE='ADJ' and sle.ADJUSTED_QTY<0) or sle.TRANSACTIONCODE in ('SLE','SHP'))
            group by itemid)sel where sel.sales>0 and sel.rets>0)sel_main
            inner join
            (select
            *,
         sum(qty) OVER (partition by ITEMID ORDER BY id asc) as balance
        from dbo.TRANSACTIONS sle where
        sle.WAREHOUSE = 'USRETURN'
            AND sle.STOCKID = 'RETURN'
            AND sle.BIN IN ('HKA', 'HKB')
            AND cast(sle.TRANSACTDATE as date)>='2018-08-28'
            AND (sle.TRANSACTIONCODE ='RET' or  (sle.TRANSACTIONCODE='ADJ' and sle.ADJUSTED_QTY>0 and sle.PARENT_SALE_TR in (0,1)))
            )  sel_ret
            on sel_main.itemid=sel_ret.itemid
            where sel_ret.balance<=sel_main.mark_qty)ret where ret.id=dbo.TRANSACTIONS.id and ret.PARENT_SALE_TR<>0"""

    # DLMR-1325
    # Update RET transactions to ADJ
    def update_ret_to_adj_transactions(self, cr, uid, context=None):
        if not context:
            context = {}
        helper = self.pool.get('stock.picking.helper')
        logger.info("Query to update: {}".format(self.sql_inspired_by_serhii_dekun))
        upd_res = helper.MSSQLInventoryExecute(cr, uid, sql=self.sql_inspired_by_serhii_dekun, params={})
        logger.info("Query result: %s" % upd_res)

        # Rest of the procedure can be omitted
        # # Step 1. Find sales transactions
        # sales_tr = helper.MSSQLInventorySelect(cr, uid, sql=self.sql_sle_no_ret_tr)
        # # Step 2. Find returns over iterated sales transactions
        # if sales_tr and len(sales_tr) > 0:
        #     logger.info("Found %s sales transactions" % len(sales_tr))
        #     for sle in sales_tr:
        #         query_params = {
        #             'sle_bin': sle[3],
        #             'sle_qty': sle[5],
        #             'sle_itemid': sle[4],
        #             'sle_trdate': sle[6]
        #         }
        #         #logger.info("SQL search RET pair transaction: %s, params: %s" % (self.sql_ret_for_sle_tr, query_params))
        #         ret_tr = helper.MSSQLInventorySelect(cr, uid, sql=self.sql_ret_for_sle_tr, params=query_params)
        #         if ret_tr and len(ret_tr) > 0:
        #             ret = ret_tr[0]
        #             logger.info("Found pair. SLE: %s, RET: %s" % (sle[0], ret[0]))
        #             upd_params = {
        #                 'sle_id': sle[0],
        #                 'ret_id': ret[0]
        #             }
        #             logger.info("Query to update: %s %s" % (self.sql_upd_ret_to_adj, upd_params))
        #             upd_res = helper.MSSQLInventoryExecute(cr, uid, sql=self.sql_upd_ret_to_adj, params=upd_params)
        #             #logger.info("Query result: %s" % upd_res)
        #         else:
        #             continue
        # else:
        #     logger.info("No sales transactions were found for pairing")
        return True
