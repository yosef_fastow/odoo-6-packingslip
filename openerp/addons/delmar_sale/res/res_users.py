# -*- coding: utf-8 -*-
##############################################################################

from osv import fields, osv
import logging


_logger = logging.getLogger(__name__)


class groups(osv.osv):
    _inherit = 'res.groups'

    def get_user_emails(self, cr, uid, ids, context=None):
        res = {}

        for group in self.browse(cr, uid, ids):
            res[group.id] = [x.user_email for x in group.users if x.user_email]

        return res


class users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'partner_ids': fields.many2many('res.partner', 'res_partner_users_rel', 'user_id', 'partner_id', 'Customers'),
        'warehouse_ids': fields.many2many('stock.warehouse', 'user_warehouse_rel', 'user_id', 'warehouse_id', 'Warehouses'),
    }

    def search_args_user_warehouses(self, cr, uid, args=None, field=None, context=None):
        if args is None:
            args = []

        if not field:
            field = 'warehouse_id'

        try:
            user = self.pool.get('res.users').read(cr, uid, uid, ['warehouse_ids'], context=context)
            wh_ids = user['warehouse_ids']
            if wh_ids:
                excp_wh_id = self.pool.get('stock.warehouse').search(cr, uid, [('name', '=', 'EXCP')], context=context)
                if excp_wh_id:
                    wh_ids += excp_wh_id
                args.append((field, 'in', wh_ids))
        except Exception, e:
            _logger.error(e)

        return args

users()
