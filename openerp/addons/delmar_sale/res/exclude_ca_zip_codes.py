from osv import osv, fields


class exclude_ca_zip_codes(osv.osv):
    _name = "exclude.ca.zip.codes"

    _columns = {
        'name': fields.char('CA zip code', size=64, required=True),
    }


exclude_ca_zip_codes()
