from osv import osv, fields
import time
import re
from openerp.addons.delmar_sale.stock_picking import SIGNATURE_CODES
import logging

_logger = logging.getLogger(__name__)


class ShippingCodeExcp(Exception):
    pass


class res_shipping_service(osv.osv):
    _name = "res.shipping.service"

    _columns = {
        'name': fields.char('Service', size=64, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The service name must be unique !')
    ]


res_shipping_service()


class res_shipping_type(osv.osv):
    _name = "res.shipping.type"

    _columns = {
        'name': fields.char('Shipping type', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The shipping type must be unique !')
    ]


res_shipping_type()


class res_shipping_code(osv.osv):
    _name = 'res.shipping.code'

    def _flat_rate_priority_id(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        res = {x: None for x in ids}
        picking_id = context.get('picking_id', False)
        if picking_id:
            pick = self.pool.get('stock.picking').browse(cr, uid, picking_id)
            zip_code = pick.address_id and pick.address_id.zip
            if zip_code:
                priority_obj = self.pool.get('priority.dimension')
                for rule in self.browse(cr, uid, ids):
                    priority = priority_obj.get_priority(cr, uid, zip_code, rule.service_id.id or None, rule.type_id.id or None)
                    if priority:
                        res[rule.id] = (priority['id'], priority['name'])

        return res

    def _get_name(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, False)

        if ids:
            cr.execute("""  SELECT
                                sc.id as id,
                                CONCAT(si.name, E'/', sw.name, E'/', ss.name, E'/', st.name, E'/', sc.incoming_code) as name
                            FROM res_shipping_code sc
                                LEFT JOIN sale_integration si ON sc.so_provider = si.id
                                LEFT JOIN stock_warehouse sw ON sc.warehouse_id = sw.id
                                LEFT JOIN res_shipping_service SS on sc.service_id = ss.id
                                LEFT JOIN res_shipping_type st ON sc.type_id = st.id
                            WHERE sc.id in %s
                            ;
                    """, (tuple(ids), ))
            for row_id, name in cr.fetchall():
                result[row_id] = name

        return result

    def _get_signature(self, cr, uid, ids, fn, args, context=None):
        if context is None:
            context = {}
        # DELMAR-150
        # Add checking for N-signature in shipping code rules
        default_signature = None
        params = self.pool.get('ir.config_parameter')
        if 'picking_id' in context and context.get('picking_id'):
            picking = self.pool.get('stock.picking').read(cr, uid, context.get('picking_id'), ['total_price'])
            if picking.get('total_price') > float(params.get_param(cr, uid, 'shipping_rule.default_signature_price')):
                _logger.info("Price is more than %s, signature required" % params.get_param(cr, uid, 'shipping_rule.default_signature_price'))
                default_signature = int(params.get_param(cr, uid, 'shipping_rule.default_signature_level'))
        # END DELMAR-150
        res = {x: None for x in ids}
        ship_codes = self.browse(cr, uid, ids)
        for ship_code in ship_codes:
            # DELMAR-150
            if ship_code.signature_level and type(ship_code.signature_level) is int:
                res[ship_code.id] = ship_code.signature_level
            else:
                res[ship_code.id] = default_signature
            # END DELMAR-150
        _logger.info("Signature checking result is: %s" % res)
        return res

    _columns = {
        'name': fields.function(_get_name, string='Name', type='char', size=128),
        'incoming_code': fields.char('Incoming code', size=256, required=False),
        'outgoing_code': fields.char('Outgoing code', size=256, required=False),
        'service_id': fields.many2one('res.shipping.service', 'Service', required=True),
        'type_id': fields.many2one('res.shipping.type', 'Shipping type'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=False),
        'code': fields.char('Code', size=64, required=True),
        'signature': fields.function(_get_signature, string='Signature', type='integer', ),
        'signature_level': fields.selection(SIGNATURE_CODES, 'Signature Level', ),
        'shipping_handling': fields.float('Shipping Handling', required=False, ),
        'insurance': fields.boolean('Insurance', required=True),
        'not_ss_signature': fields.boolean('Not sign Ship to Store'),
        'so_provider': fields.many2one('sale.integration', 'Service provider', required=True),
        'default': fields.boolean('Default'),
        'value_from': fields.float('Value From'),
        'value_to': fields.float('Value To'),
        'received_from': fields.datetime('Received from',),
        'received_to': fields.datetime('Received to',),
        'shipped_from': fields.datetime('Shipped from',),
        'shipped_to': fields.datetime('Shipped to',),
        'country_id': fields.many2one('res.country', 'Ship to Country', ),
        'product_id': fields.many2one('product.product', 'Product', ),
        'tag_id': fields.many2one('tagging.tags', 'Tag', ),
        'is_po_box': fields.boolean('PO BOX'),
        'order_type': fields.selection([
            ('all', 'All orders'),
            ('s2s', 'Ship to Store'),
            ('not_s2s', 'Regular'),
            ], 'Order Type', required=True, ),
        'priority': fields.integer('Priority', help="1 is the top priority. By default, all rules have priority 10.", ),
        'flat_rate_priority_id': fields.function(_flat_rate_priority_id, string="Priority", type="many2one", relation="priority.flat.rate", method=True),
        'exclude_ca_codes': fields.boolean('Exclude CA codes', help='Exclude list of CA codes'),
    }

    _defaults = {
        'order_type': 'all',
        'priority': 10,
        'insurance': False,
        'is_po_box': False,
        'value_from': 0.0,
        'value_to': 0.0,
        'shipping_handling': 0.0,
        'exclude_ca_codes': False,
    }

    _order = 'priority asc'

    def create(self, cr, uid, vals, context=None):
        res = super(res_shipping_code, self).create(cr, uid, vals, context=context)
        if vals.get('default', False):
            self.set_default(cr, uid, res, context=context)

        return res

    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_shipping_code, self).write(cr, uid, ids, vals, context=context)
        if vals.get('default', False) or 'warehouse_id' in vals or 'so_provider' in vals or 'country_id' in vals:
            self.set_default(cr, uid, ids[-1], context=context)

        return res

    def set_default(self, cr, uid, id, context=None):
        rule = self.browse(cr, uid, id, context=context)

        is_default = bool(rule.default)
        if is_default:
            wh_id = rule.warehouse_id.id or False
            so_provider = rule.so_provider.id or False
            country_id = rule.country_id.id or False

            if so_provider and rule.incoming_code:
                all_ids = self.search(cr, uid, [('warehouse_id', '=', wh_id), ('so_provider', '=', so_provider), ('country_id', '=', country_id)], context=context)
                super(res_shipping_code, self).write(cr, uid, all_ids, {'default': False})
                super(res_shipping_code, self).write(cr, uid, id, {'default': True})
            else:
                super(res_shipping_code, self).write(cr, uid, id, {'default': False})

        return True

    ###########################################################
    # Get Shipping Rule:
    # target:
    #   incoming - get rule for mssql code by incoming_code
    #       required:
    #           shipped_date: datetime string in '%Y-%m-%d %H:%M:%S' format
    #   outgoing - get rule for outgoing code by mssql response
    #       required:
    #           codes - list of codes to find rule
    def get_code(
            self, cr, uid,
            pick_id,
            target='incoming',
            shipped_date=None,
            codes=None,
            context=None):

        def _do_apply_exclude_ca_codes(cr, pick):
            """Check if zip code is CA and shipping rule excludes it"""
            cr.execute("""select * from exclude_ca_zip_codes where '{}' like name || '%'""".format(pick.address_id.zip))
            res = cr.fetchone()
            return True if res else False

        if context is None:
            context = {}

        if codes is None:
            codes = []

        if shipped_date is None:
            shipped_date = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())

        res = False
        domain = []
        order = []

        if target not in ('incoming', 'outgoing'):
            return False

        pick = self.pool.get('stock.picking').browse(cr, uid, pick_id)
        wh_id = pick.warehouses and pick.warehouses.id or False
        country_id = pick.address_id and pick.address_id.country_id and pick.address_id.country_id.id or False
        street = pick.address_id and pick.address_id.street or False
        street2 = pick.address_id and pick.address_id.street2 or False
        so_provider = pick.sale_id and pick.sale_id.type_api_id.id or False
        s2s = pick.origin_ship2store or False
        user = self.pool.get('res.users').browse(cr, uid, uid)
        code = False
        value = False
        received_date = False
        history = ''

        is_po_box = False
        regexp = re.compile(r"(?i).*(\b(([po]*box)|([af][\.\s]*p[\.\s]*o))[\d]*\b)", re.IGNORECASE)
        for address in [street, street2]:
            if address:
                is_po_box = bool(regexp.match(address))
                if is_po_box:
                    break

        if pick.shipping_batch_id:
            code = pick.shipping_batch_id.incoming_code or False
            value = pick.shipping_batch_id.total_price or  False
            received_date = pick.shipping_batch_id.received_date or False

        else:
            code = pick.origin_carrier or False
            value = pick.total_price_rules or False
            received_date = pick.create_date or False

        if so_provider and wh_id:

            # SO_Provider
            domain += ["""
                AND "rc"."so_provider" = %s
            """ % (so_provider)]

            # Warehouse_id
            domain += ["""
                AND (
                    "rc"."warehouse_id" = %s
                    OR "rc"."warehouse_id" IS NULL
                )
            """ % (wh_id)]
            order += [
                'CASE WHEN "rc"."warehouse_id" IS NULL THEN 0 ELSE 1 END DESC',
            ]

            # country_id
            where_country = """ AND coalesce("rc"."country_id", 0) in (0, %s) """ % (country_id or 0)
            domain += [where_country]
            order += [
                'CASE WHEN "rc"."country_id" IS NULL THEN 0 ELSE 1 END DESC',
            ]

            if target == 'incoming':

                get_prod_ids_sql = """  SELECT DISTINCT sm.product_id
                                        FROM "stock_move" sm
                                        WHERE sm.picking_id = %s;
                """ % (pick.id)
                cr.execute(get_prod_ids_sql)
                prod_ids = cr.fetchall() or []
                prod_ids_str = ",".join([str(x[0]) for x in prod_ids])

                if code:

                    # Tag ID
                    where_tag_id = """OR "rc"."tag_id" in (
                            SELECT tp.tag_id
                            FROM  taggings_product tp
                            WHERE tp.product_id in (%s)
                        )
                    """ % (prod_ids_str) if prod_ids else ''
                    domain += [
                        """AND ("rc"."tag_id" IS NULL %s
                        )
                        """ % (where_tag_id)
                    ]

                    # Product ID
                    where_product_id = 'OR "rc"."product_id" IN (%s)' % (prod_ids_str) if prod_ids else ''
                    domain += [
                        """AND (
                            "rc"."product_id" IS NULL %s
                        )
                        """ % (where_product_id)
                    ]

                    # Order Type
                    domain += [
                        """AND "rc"."order_type" in ('all', '%s')""" % ('s2s' if s2s else 'not_s2s'),
                    ]

                    # Incoming Code
                    domain += ["""
                        AND('%s' ilike "rc"."incoming_code")
                    """ % (code.replace("'", "''"))
                    ]

                    # PO BOX
                    if is_po_box:
                        domain += ["""
                            AND "rc".is_po_box is True
                        """]
                    else:
                        domain += ["""
                            AND "rc".is_po_box is not True
                        """]


                    # Exclude CA zip code
                    if _do_apply_exclude_ca_codes(cr, pick):
                        domain += ["""
                            AND "rc".exclude_ca_codes is True
                        """]
                    else:
                        domain += ["""
                            AND "rc".exclude_ca_codes is False
                        """]

                    # Order Price
                    where_value_from = """OR("rc"."value_from" <= %s)""" % str((value)) if value else ""
                    where_value_to = """OR("rc"."value_to" > %s)""" % (str(value)) if value else ""
                    order += [
                        'CASE WHEN "rc"."value_from" IS NULL THEN 0 ELSE "rc"."value_from" END DESC',
                        'CASE WHEN "rc"."value_to" IS NULL THEN 0 ELSE "rc"."value_to" END DESC',
                    ]
                    domain += ["""
                        AND(
                            (
                                1!=1
                                %s
                                OR(
                                    ("rc"."value_from" IN('0.0')) OR "rc"."value_from" IS NULL
                                )
                            )
                            AND(
                                1!=1
                                %s
                                OR(
                                    ("rc"."value_to" IN('0.0')) OR "rc"."value_to" IS NULL
                                )
                            )
                        )
                    """ % (where_value_from, where_value_to)]

                    # Received_date
                    if received_date:
                        order += [
                            'CASE WHEN "rc"."received_from" IS NULL THEN 0 ELSE 1 END DESC',
                            '"rc"."received_from" DESC',
                            'CASE WHEN "rc"."received_to" IS NULL THEN 0 ELSE 1 END DESC',
                            '"rc"."received_to" DESC',
                        ]
                        domain += ["""
                            AND(
                                (
                                    (
                                        "rc"."received_from" <= '%s'
                                    )
                                    OR(
                                        "rc"."received_from" IS NULL
                                    )
                                )
                                AND(
                                    (
                                        "rc"."received_to" > '%s'
                                    )
                                    OR(
                                        "rc"."received_to" IS NULL
                                    )
                                )
                            )
                        """ % (received_date, received_date)]

                    # Shipped Date
                    if shipped_date:
                        order += [
                            'CASE WHEN "rc"."shipped_from" IS NULL THEN 0 ELSE 1 END DESC',
                            '"rc"."shipped_from" DESC',
                            'CASE WHEN "rc"."shipped_to" IS NULL THEN 0 ELSE 1 END DESC',
                            '"rc"."shipped_to" DESC',
                        ]
                        domain += ["""
                            AND(
                                (
                                    (
                                        "rc"."shipped_from" <= '%s'
                                    )
                                    OR(
                                        "rc"."shipped_from" IS NULL
                                    )
                                )
                                AND(
                                    (
                                        "rc"."shipped_to" > '%s'
                                    )
                                    OR(
                                        "rc"."shipped_to" IS NULL
                                    )
                                )
                            )
                    """ % (shipped_date, shipped_date)]
                else:
                    # Default Rule
                    domain += ["""AND "rc"."default" = True"""]

                order = [
                    """CASE WHEN "rc"."order_type" = 'all' OR "rc"."order_type" IS NULL THEN 0 ELSE 1 END DESC""",
                    'CASE WHEN "rc"."product_id" IS NULL THEN 0 ELSE 1 END DESC',
                    'CASE WHEN "rc"."tag_id" IS NULL THEN 0 ELSE 1 END DESC',
                ] + order

            elif target == 'outgoing':
                if not codes:
                    return False

                domain += [
                    """AND ('%s' ilike "rc"."code")""" % ("""' ilike "rc"."code" OR '""".join([x.replace("'", "''") for x in codes])),
                    """AND ("rc"."outgoing_code" IS NOT NULL AND "rc"."outgoing_code" != '')""",
                ]

            try:
                res = self.get_by(cr, uid, domain=domain, order=order, query=True, context={'picking_id': pick_id})
            except ShippingCodeExcp, e:
                history = '\n%s %s: %s' % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name or '', e)

        if history:
            pick.write({'report_history': (pick.report_history or '') + history})
        if res:
            flat_rate_priority_code = res.flat_rate_priority_id and res.flat_rate_priority_id.shp_code or None
            if flat_rate_priority_code:
                res.code = flat_rate_priority_code
            if pick.sale_id.shp_handling:
                res.shipping_handling = pick.shp_handling_from_sale_order

        return res

    ###########################################################
    # Execute query for shipping rules:
    # target: ('incoming', 'outgoing')
    # query: execute sql or orm method
    def get_by(self, cr, uid, domain=None, order=None, target='incoming', query=False, context=None):

        if not order:
            order = []
        if not domain:
            domain = []

        if context is None:
            context = {}

        result_ids = []

        if query:
            select = []
            checked_product_ids = []
            checked_tag_ids = []
            product_specific_rules = []
            order = [
                'CASE WHEN "rc"."priority" IS NULL THEN 10 ELSE "rc"."priority" END ASC',
            ] + order

            if target == 'incoming':
                select += [
                    ', "rc"."product_id" as product_id',
                    ', "rc"."tag_id" as tag_id',
                    ', "rc"."service_id" as service_id',
                    ', "rc"."type_id" as type_id',
                    ', "rc"."signature_level" as signature',
                    ', "rc"."code" as code',
                ]

            sql = """   SELECT
                            "rc"."id" as id
                            , "ss"."name" as service_name
                            , "st"."name" as type_name
                            %s
                        FROM
                            "res_shipping_code" rc
                                LEFT JOIN "res_shipping_service" ss on rc."service_id" = ss."id"
                                LEFT JOIN "res_shipping_type" st on rc."type_id" = st."id"
                        WHERE 1=1
                            %s
                        ORDER BY
                            %s
            """ % ("\n".join(select), " ".join(domain), ",\n".join(order))
            cr.execute(sql)
            res = cr.dictfetchall() or []
            for code in res:
                result_ids.append(code['id'])
                if target == 'incoming':
                    to_check = False
                    if code.get('product_id', False) and code['product_id'] not in checked_product_ids:
                        checked_product_ids.append(code['product_id'])
                        to_check = True
                    if code.get('tag_id', False) and code['tag_id'] not in checked_tag_ids:
                        checked_tag_ids.append(code['tag_id'])
                        to_check = True
                    if to_check:
                        rule_str = "Service: %s, Type: %s, Signature required: %s, MSSQl code: %s" % (
                            code.get('service_name', False),
                            code.get('type_name', False),
                            str(code.get('signature', False)),
                            code.get('code', False),
                        )
                        if rule_str not in product_specific_rules:
                            product_specific_rules.append(rule_str)

                    else:
                        if product_specific_rules and len(product_specific_rules) > 1:
                            raise ShippingCodeExcp("There are several product specific rules. Please check correct rule:\n%s." % ("\n".join(product_specific_rules)))
                        break
                else:
                    break

        else:
            result_ids = self.search(cr, uid, domain, order=order)

        if result_ids:
            return self.browse(cr, uid, result_ids[0], context=context)

        return False

    def get_shipping_service(self, cr, uid, picking_id=False, fields=None):
        code = {}
        res = {}

        if picking_id:

            if fields is None:
                fields = 'service_id'

            shipping_data = self.pool.get('res.shipping.code')
            ship_code = shipping_data.get_code(
                cr, uid,
                pick_id=picking_id,
                shipped_date=time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            )

            if ship_code:
                code = {
                    'service_id': ship_code.service_id.id or False,
                    'type_id': ship_code.type_id.id or False,
                    'code': ship_code.code or False,
                }

        if isinstance(fields, (str, unicode)):
            res = code.get(fields, False)
        elif isinstance(fields, (tuple, list)):
            res = {f: code.get(f, False) for f in fields}
        return res


res_shipping_code()
