from osv import osv, fields
from tools.translate import _
import netsvc
from cStringIO import StringIO
import csv
import base64
import time
import re
import logging

_logger = logging.getLogger(__name__)


class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        'ref': fields.char('Reference', size=64, select=1, required=True),
        "location_ids": fields.many2many('stock.location',  'customer_location_relation', 'customer_id', 'location_id', string='Customer\'s locations'),
        "order_ids": fields.text("order ids"),
        "sec_location_ids": fields.many2many('stock.location',  'customer_sec_location_relation', 'customer_id', 'location_id', string='Secondary Customer\'s locations'),
        "sec_order_ids": fields.text("secondary order ids"),
        'user_ids': fields.many2many('res.users', 'res_partner_users_rel', 'partner_id', 'user_id', 'Users'),
        "fill_order_item": fields.boolean("Fill order item"),
        'virtual': fields.boolean('Virtual customer'),
        'is_sterling': fields.boolean('Sterling customer'),
        'invoice_without_ship_cost': fields.boolean('Invoice without shipping cost'),
        'acc_price_code': fields.char('Acc Price Code', size=256),
        'white_paper': fields.boolean('White Paper Packslips'),
        'ship_by_country': fields.boolean('Ship by Country'),
        'allow_auto_split': fields.boolean('Allow auto split'),
        'flash_carrier_code': fields.char('Flash Carrier Code', size=256),
        'invoice_cost_from_acc_price': fields.boolean('Fill Invoice Costs From Acc_Price'),
        'price_field': fields.char(
            'Price field', size=256,
            help="Additional field name with price for customers like Bluestem which should show price from order instead of total_price"
        ),
        "currency_id":  fields.many2one("res.currency", "Currency", required=False, help="Partner's Currency."),
        'tag_id':       fields.many2one('tagging.tags', 'Tag'),
        "ignore_priority_dimension": fields.boolean("Ignore flat rate priority"),
        "prevent_combine": fields.boolean("Prevent Combine"),
        'fcdc_code': fields.char('FCDC code', size=256),
        'fcdcw_name': fields.char('FCDCW name', size=100),
        'partner_email': fields.char('Partner E-Mail', size=256),
        'brand_ids': fields.one2many('partner.brand.relation', 'partner_id', 'Brands'),
        'max_inv': fields.integer('Send inventory (%)'),
    }

    _defaults = {
        'is_sterling': False,
        'white_paper': False,
        'ship_by_country': False,
        'allow_auto_split': False,
        'invoice_cost_from_acc_price': False,
        'max_inv': 100
    }

    def onchange_location_ids(self, cr, uid, ids, location_ids, context=None):
        order_str = ""
        try:
            if location_ids:
                fields_ids_list = location_ids[0][2]
                order_str = ",".join([str(x) for x in fields_ids_list if x])
        except:
            order_str = ""
        return {'value': {'order_ids': order_str}}

    def onchange_sec_location_ids(self, cr, uid, ids, location_ids, context=None):
        order_str = ""
        try:
            if location_ids:
                fields_ids_list = location_ids[0][2]
                order_str = ",".join([str(x) for x in fields_ids_list if x])
        except:
            order_str = ""
        return {'value': {'sec_order_ids': order_str}}

res_partner()


class res_partner_address(osv.osv):
    _inherit = 'res.partner.address'

    def _get_picking_names(self, cr, uid, ids, field_name, arg, context=None):

        res = {x: '' for x in ids}
        if ids:

            cr.execute(
                """
                SELECT
                    sp.address_id,
                    string_agg(sp.name, ', ')
                FROM stock_picking sp
                WHERE sp.address_id in %(address_ids)s
                GROUP BY sp.address_id
                """,
                {
                    'address_ids': tuple(ids),
                }
            )

            for addr in cr.fetchall():
                res[addr[0]] = addr[1]

        return res

    def _check_format(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if ids:
            conf_obj = self.pool.get('ir.config_parameter')
            valid_length = conf_obj.get_param(cr, uid, 'valid_adress_length')
            if valid_length:
                try:
                    valid_length = float(valid_length)
                except:
                    pass
            if not valid_length:
                valid_length = 35

            cr.execute(
                """
                SELECT
                    id as id,
                    (street is null or length(street) <= %(valid_length)s) and
                    (street2 is null or length(street2) <= %(valid_length)s) and
                    (s_company is null or length(s_company) <= %(valid_length)s) as is_valid
                FROM res_partner_address
                WHERE id in %(ids)s
                """,
                {
                    'valid_length': valid_length,
                    'ids': tuple(ids),
                }
            )

            for addr in cr.fetchall():
                res[addr[0]] = addr[1]

        return res

    _columns = {
        'create_date': fields.datetime('Date', required=True, select=True),
        "since_date": fields.date('Since date', readonly=True, store=False),
        "to_date": fields.date('To date', readonly=True, store=False),
        'is_valid': fields.boolean('Address is valid'),
        'is_approved': fields.boolean('Address is approved'),
        'stock_picking_ids': fields.one2many('stock.picking', 'address_id', 'Delivery orders'),
        'outgoind_partner_id': fields.many2one('res.partner', 'Partner Name', select=True),
        'external_customer_order_id': fields.char('Customer Order ids', size=512),
        "csv_file": fields.binary(string="CSV Export", readonly=True),
        "csv_filename": fields.char("", size=256),
        'state': fields.boolean("state"),
        'note': fields.text('Notes'),
        'btn_contact': fields.boolean("Contact customer"),
        'cancelled': fields.boolean("Cancelled"),
        'min_cost': fields.float("Minimal cost"),
        'default_return': fields.boolean("Default For All Warehouses"),
        'company': fields.char('Company', size=256, ),
        'is_valid_format': fields.function(_check_format, string="Is Valid Format", method=True, type="boolean"),
        'stock_picking_names': fields.function(_get_picking_names, string="Orders", method=True, type="text"),
    }

    _defaults = {
        'is_valid': False,
        'is_approved': False,
        'state': False,
        'csv_file': '',
        'note': '',
        'btn_contact': False,
        'cancelled': False,
        'default_return': False,
    }

    def __init__(self, pool, cr):
        """Add a new state value"""
        selection = super(res_partner_address, self)._columns['type'].selection or []
        selection += [
            ('physical', 'Physical'),
            ('return', 'Return'),
            ('invoice_for_receivings', 'Invoice for receivings'),
            ('other', 'Other'),
        ]
        super(res_partner_address, self)._columns['type'].selection = list(set(selection))
        super(res_partner_address, self).__init__(pool, cr)

    def name_get(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        if not len(ids):
            return []
        res = []
        for r in self.read(cr, user, ids, ['name', 'zip', 'state_id', 'country_id', 'city', 'partner_id', 'street', 'street2', 'phone']):
            if context.get('contact_display', 'contact') == 'partner' and r['partner_id']:
                res.append((r['id'], r['partner_id'][1]))
            else:
                # make a comma-separated list with the following non-empty elements
                elems = [r['name'], r['street'], r['street2'], r['city'], r['state_id'] and r['state_id'][1], r['zip'], r['country_id'] and r['country_id'][1], r['phone']]
                addr = ', '.join(filter(bool, elems))
                if (context.get('contact_display', 'contact') == 'partner_address') and r['partner_id']:
                    res.append((r['id'], "%s: %s" % (r['partner_id'][1], addr or '/')))
                else:
                    res.append((r['id'], addr or '/'))
        return res

    def action_invalid_adress_export(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        pick_ids = context.get('active_ids', [])
        if not pick_ids:
            raise osv.except_osv(_('Error!'), ("Please select at least one address to export"))

        rows = []
        rows.append(['partner_name', 'erp_order_names', 'customer_order_ids', 'contact name', 'street', 'street2', 'zip', 'city', 'country', 'state', 'phone', 'date'])
        for partner_id in pick_ids:
            row = []
            partner_obj = self.pool.get("res.partner.address").browse(cr, uid, partner_id)
            # Partner Name
            row.append(str(partner_obj.outgoind_partner_id.name))
            # ERP order names
            row.append(str(partner_obj.stock_picking_names))
            # Customer Order ids
            row.append(str(partner_obj.external_customer_order_id))
            # Contact Name
            row.append(str(partner_obj.name))
            # Street
            row.append(str(partner_obj.street))
            # Street2
            row.append(str(partner_obj.street2))
            # Zip
            row.append(str(partner_obj.zip))
            # city
            row.append(str(partner_obj.city))
            # country
            row.append(str(partner_obj.country_id.name))
            # state
            row.append(str(partner_obj.state_id.name))
            # phone
            row.append(str(partner_obj.phone))
            # date
            row.append(str(partner_obj.create_date))
            rows.append(row)
        buf = StringIO()
        writer = csv.writer(buf, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, lineterminator='\n')
        for row in rows:
            writer.writerow(row)
        out = base64.encodestring(buf.getvalue())

        return self.write(cr, uid, ids, {'state': True, 'csv_file': out, 'csv_filename': 'invalid_adress_export.csv'}, context=context)

    def search_args_since_date(self, cr, user, args, context=None):
        if not context: context = {}
        try:
            since_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'since_date')).next()
            since_date_index = args.index(since_date_arg)
            args[since_date_index][0] = 'create_date'
            args[since_date_index][1] = '>'
            return args
        except StopIteration:
            return args

    def search_args_to_date(self, cr, user, args, context=None):
        if not context: context = {}
        try:
            to_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'to_date')).next()
            to_date_index = args.index(to_date_arg)
            args[to_date_index] = ['create_date', '<=', args[to_date_index][2] + ' 24:00:00']
            return args
        except StopIteration:
            return args

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if args is None:
            args = []
        if context is None:
            context = {}
        act_window_ids = self.pool.get('ir.actions.act_window').search(cr, user, [('name', '=', 'Invalid Addresses')])
        if act_window_ids:
            domain_str = self.pool.get('ir.actions.act_window').browse(cr, user, act_window_ids[0])
            if domain_str:
                domain = eval(domain_str.domain)
                # If args without manually added filters then disable auto query
                if domain and [list(d) for d in domain] == [list(a) for a in args]:
                    return []

        args = self.search_args_since_date(cr, user, args, context=context)
        args = self.search_args_to_date(cr, user, args, context=context)
        return super(res_partner_address, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if not context:
            context = {}
        if context.get('contact_display', 'contact') == 'partner ' or context.get('contact_display', 'contact') == 'partner_address ':
            ids = self.search(cr, user, [('partner_id', operator, name)], limit=limit, context=context)
        else:
            if not name:
                ids = self.search(cr, user, args, limit=limit, context=context)
            else:
                ids = self.search(cr, user, [('zip', '=', name)] + args, limit=limit, context=context)
            if not ids:
                ids = self.search(cr, user, [('city', operator, name)] + args, limit=limit, context=context)
            if name:
                ids += self.search(cr, user, [('name', operator, name)] + args, limit=limit, context=context)
                ids += self.search(cr, user, [('partner_id', operator, name)] + args, limit=limit, context=context)
                ids += self.search(cr, user, [('street2', operator, name)] + args, limit=limit, context=context)
                ids += self.search(cr, user, [('street', operator, name)] + args, limit=limit, context=context)
        return self.name_get(cr, user, ids, context=context)

    def create(self, cr, uid, vals, context=None):
        # DLMR-2177
        # Fix phone number
        if 'phone' in vals and vals.get('phone', None):
            _logger.info("Source phone format: {}".format(vals.get('phone')))
            vals.update({'phone': self.format_phone(vals.get('phone'))})
            _logger.info("Resulting phone format: {}".format(vals.get('phone')))
        # END DLMR-2177
        res = super(res_partner_address, self).create(cr, uid, vals, context=context)
        if res:
            self.check_address(cr, uid, res, context=context)

        return res

    def write(self, cr, uid, ids, vals, context=None):
        # DLMR-2177
        # Fix phone number
        if 'phone' in vals and vals.get('phone', None):
            _logger.info("Source phone format: {}".format(vals.get('phone')))
            vals.update({'phone': self.format_phone(vals.get('phone'))})
            _logger.info("Resulting phone format: {}".format(vals.get('phone')))
        # END DLMR-2177
        res = super(res_partner_address, self).write(cr, uid, ids, vals, context=context)

        if isinstance(ids, (int, long)):
            ids = [ids]

        if not vals.get('is_valid', False) and set(['name', 'street', 'street2', 'city', 'state_id', 'country_id', 'zip']) & set(vals.keys()):
            for address_id in ids:
                self.check_address(cr, uid, address_id, context=context)

        return res

    def check_address(self, cr, uid, address_id, context=None):
        is_valid = False
        if context is None:
            context = {}

        _logger.info("RUN CHECK ADDRESS")
        address = address_id and self.get_address(cr, uid, address_id, context=context) or False

        _logger.info("GOT ADDRESS OBJECT: %s" % address)

        if context.get('partner_name', False):
            sale_integration = self.pool.get("sale.integration")
            (settings, api) = sale_integration.getApi(cr, uid, context.get('partner_name', False))
            if api and api.apierp_is_available('check_address'):
                is_valid = api.apierp.check_address(cr, uid, address)
                if not is_valid:
                    return is_valid


        if not address:
            return {'status': False} if context.get('get_response', False) else False

        if not address.get('is_valid_format', False):
            return {'status': False, 'msg': 'The address length too long.'} if context.get('get_response', False) else False

        if address.get('is_valid', False):
            return {'status': True} if context.get('get_response', False) else True

        elif address.get('is_approved', False) or address.get('is_valid', False):
            is_valid = True
            res = {'status': True} if context.get('get_response', False) else True

        elif self.is_approved_early(cr, uid, address_id).get(address_id, False):
            is_valid = True
            res = {'status': True} if context.get('get_response', False) else True

        else:
            _logger.info("RUN ADDRESS.SERVICES.CHECK_ADDRESS")
            res = self.pool.get('address.services').check_address(cr, uid, address, context=context)
            # self.log(cr, uid, address_id, 'Check Address (%s)' % ('valid' if is_valid else 'invalid'))
            if context.get('get_response', False):
                is_valid = res['status']
            else:
                is_valid = res

        _logger.info("DONE CHECKS")

        super(res_partner_address, self).write(cr, uid, address_id, {'is_valid': is_valid})

        _logger.info("ADDRESS SAVED")

        if is_valid:
            self.trigger_related_picking(cr, uid, [address_id], context=context)
        _logger.info("FINISHED. PICKING TRIGGERED")

        return res

    @staticmethod
    def format_phone(phone=''):
        clearing_chars = ["(", ")", ".", "-", " "]
        ext_chars = [',', 'e', 'x']
        for ext_point in ext_chars:
            pos = phone.rfind(ext_point)
            if pos and pos > 0:
                phone = str(phone[:pos]).strip(" -.,")
        for rchr in clearing_chars:
            phone = phone.replace(rchr, '')
        check_digits = re.compile("^\d+$")
        if not check_digits.match(phone):
            phone = re.sub('[^0-9]', '', phone)
        converted_phone = phone
        if len(phone) >= 10:
            num_last = phone[-4:]
            num_first = phone[-7:][:3]
            num_code = phone[-10:][:3]
            num_prefix = ''
            if len(phone) > 10:
                num_prefix = phone[:-10]
            converted_phone = "{}-{}-{}-{}".format(num_prefix, num_code, num_first, num_last).strip("- ")
        return converted_phone

    def get_address(self, cr, uid, address_id=None, context=None):
        self.clear_caches()
        address = self.browse(cr, uid, address_id)

        res = {
            'id': address.id,
            'name': address and address.name and address.name.replace("'", "''") or False,
            'street': address and address.street and address.street.replace("'", "''") or False,
            'street2': address and address.street2 and address.street2.replace("'", "''") or False,
            'street3': address and address.street3 and address.street3.replace("'", "''") or False,
            'city': address and address.city and address.city.replace("'", "''") or False,
            'state': address and address.state_id and address.state_id.code and address.state_id.code.replace("'", "''") or False,
            'state_full': address and address.state_id and address.state_id.name and address.state_id.name.replace("'", "''") or False,
            'country': address and address.country_id and address.country_id.code or False,
            'company': address and address.company and address.company.replace("'", "''") or False,
            'zip': address and address.zip and address.zip.replace("'", "''") or False,
            'tel': address and address.phone and address.phone.replace("'", "''") or False,
            'is_approved': address.is_approved,
            'is_valid': address.is_valid,
            'is_valid_format': address.is_valid_format,
            's_company': address and address.s_company or False,
            'ship': address and address.type == 'delivery'
        }

        return res

    def is_approved_early(self, cr, uid, ids, context=None):
        res = {}

        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]

            res = dict.fromkeys(ids, False)

            cr.execute("""
                SELECT
                    distinct ra_check.id as id
                FROM res_partner_address ra_check
                    left join res_partner_address ra_approved on
                        ra_approved.is_approved = True
                        and ra_check.id != ra_approved.id
                        and ra_check.country_id = ra_approved.country_id
                        and ra_check.state_id = ra_approved.state_id
                        and ra_check.zip = ra_approved.zip
                        and (
                            lower(coalesce(ra_check.street, '')) = lower(coalesce(ra_approved.street, ''))
                            and
                            lower(coalesce(ra_check.street2, '')) = lower(coalesce(ra_approved.street2, ''))
                        )
                WHERE ra_check.id in %s and ra_approved.id is not null
                """, (tuple(ids), )
            )
            for addr in cr.fetchall() or []:
                res[addr[0]] = True

        return res

    def approve_manual(self, cr, uid, ids, context=None):
        allow_approve = []
        for addr in self.read(cr, uid, ids, ['is_valid_format']):
            if addr.get('is_valid_format', False):
                allow_approve.append(addr['id'])
            else:
                raise osv.except_osv(_('Error !'), _('Address is too long.'))
        if allow_approve:
            self.write(cr, uid, allow_approve, {'is_approved': True})
            self.trigger_related_picking(cr, uid, allow_approve, context=context)
        return True

    def cancel_orders(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")

        for address in self.browse(cr, uid, ids):
            for order in address.stock_picking_ids:
                if order.state == 'address_except':
                    wf_service.trg_validate(uid, 'stock.picking', order.id, 'button_cancel', cr)
        return True

    def trigger_related_picking(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        data_obj = self.pool.get('ir.model.data')

        res = data_obj.get_object_reference(cr, uid, 'base', 'user_root')
        root_uid = uid or res[1]
        for address in self.browse(cr, uid, ids):
            for order in address.stock_picking_ids:
                if order.state == 'address_except':
                    wf_service.trg_write(root_uid, 'stock.picking', order.id, cr)
        return True

    def test_address_by_form(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context.update({'get_response': True})
        for id in ids:

            res = self.check_address(cr, uid, id, context=context)
            if not res['status']:
                raise osv.except_osv(_('Warning !'), res.get('msg', False) or _('Bad partner address.'))

        return True

    def onchange_btn_contact(self, cr, uid, ids, note, context=None):
        if not ids:
            return {}
        address_id = type(ids) is list and ids[0] or ids
        note = note or ''
        new_note = note + '# %s Tried to contact person.' % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),)
        self.log(cr, uid, address_id, new_note)
        self.write(cr, uid, address_id, {'note': new_note})
        return {'value': {'note': new_note}}

res_partner_address()


class partner_brand_relation(osv.osv):
    _name = 'partner.brand.relation'
    _columns = {
        'brand_id': fields.many2one('brand', "Brand", required=True, ),
        'partner_id': fields.many2one('res.partner', "Customer", required=True, ),
        'deduction': fields.integer('Deduction %'),
        'royalty': fields.integer('Royalty %')
    }

partner_brand_relation()
