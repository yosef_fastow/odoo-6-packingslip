# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import logging
from datetime import datetime


_logger = logging.getLogger(__name__)


class stock_picking_shipping_batch(osv.osv):
    _name = "stock.picking.shipping.batch"

    def _get_butch_shp_info(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, {
            'tracking_ref': False,
            'carrier_code': False,
            })

        if ids:
            cr.execute("""  SELECT
                                sp.shipping_batch_id as batch_id,
                                MIN(sp.tracking_ref) as tracking_ref,
                                MIN(sp.carrier_code) as carrier_code
                            FROM stock_picking sp
                            WHERE sp.shipping_batch_id in %s
                            GROUP BY sp.shipping_batch_id
                            ;
                    """, (tuple(ids), ))
            res = cr.fetchall()

            for batch_id, tracking_ref, carrier_code in res:
                if tracking_ref:
                    result[batch_id]['tracking_ref'] = tracking_ref
                if carrier_code:
                    result[batch_id]['carrier_code'] = carrier_code

        return result

    def _get_total_price(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, 0.0)
        if ids:
            cr.execute("""
              SELECT
                sb.id,
                sum(
                  case
                    when sp.total_price > 0 then sp.total_price
                    else cast(coalesce(sol."customerCost", '0.0') AS FLOAT) * sm.product_qty
                  end
                ) as group_price
              FROM stock_picking_shipping_batch sb
              LEFT JOIN stock_picking sp ON sp.shipping_batch_id=sb.id
              LEFT JOIN stock_move sm ON sm.picking_id=sp.id
              LEFT JOIN sale_order_line sol ON sol.id=sm.sale_line_id
              WHERE sb.id in %s
              GROUP BY sb.id
            """, (tuple(ids), ))
            for row_id, value in cr.fetchall():
                result[row_id] = float(value)
        return result

    def _get_received_date(self, cr, uid, ids, fn, args, context=None):
        result = dict.fromkeys(ids, False)

        if ids:
            cr.execute("""  SELECT
                                sb.id,
                                MIN(sp.create_date) as received_date
                            FROM stock_picking_shipping_batch sb
                                LEFT JOIN stock_picking sp ON sp.shipping_batch_id = sb.id
                            WHERE sb.id in %s
                            GROUP BY sb.id
                            ;
                    """, (tuple(ids), ))
            for row_id, value in cr.fetchall():
                result[row_id] = value

        return result

    _columns = {
        'name': fields.char('Name', size=256, ),
        'address_grp': fields.char('Address', size=256, ),
        'picking_ids': fields.one2many('stock.picking', 'shipping_batch_id', 'Orders', readonly=True, ),
        'scan_date': fields.datetime('Scan Date', readonly=True, ),
        'tracking_ref': fields.function(_get_butch_shp_info, string="Tracking Ref", type='char', methos=True, multi='tracking_ref'),
        'carrier_code': fields.function(_get_butch_shp_info, string="Carrier Code", type='char', methos=True, multi='carrier_code'),
        'incoming_code': fields.char('Incoming Code', size=256, ),
        'shipping_code': fields.char('Shipping Code', size=66, ),
        'total_price': fields.function(_get_total_price, string="Total Price", type='float', method=True, store=False, ),
        'received_date': fields.function(_get_received_date, string="Received Date", type='date', method=True, store=False, ),
        'state': fields.selection([
            ('active', 'Active'),
            ('scanned', 'Closed by Tracking#'),
            ('done', 'Shipped'),
            ], 'State', readonly=True, required=True, ),
    }

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Name must be unique!'),
    ]

    _defaults = {
        'name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').next_by_code(cr, uid, 'stock.picking.shipping.batch'),
        'incoming_code': False,
        'state': 'active',
    }

    def combine_orders(self, cr, uid, address_grp, order_ids=None, incoming_code=None, auto_process=True, context=None):
        if not order_ids: order_ids = []
        return_action = False

        if not address_grp or not order_ids:
            return return_action

        picking_obj = self.pool.get('stock.picking')

        batch_id = self.get_active_batch_id(cr, uid, address_grp)
        if not batch_id:
            batch_id = self.create(cr, uid, {
                'address_grp': address_grp,
                'incoming_code': incoming_code or False
            })
        if not batch_id:
            raise osv.except_osv(_('Warning !'), """Can't find or create new group!""")

        picking_obj.write(cr, uid, order_ids, {'shipping_batch_id': batch_id})

        batch = self.read(cr, uid, batch_id, ['shipping_code', 'picking_ids', 'name'])
        shp_code = self.pool.get('res.shipping.code').get_code(cr, uid, pick_id=order_ids[0],)
        if shp_code and shp_code.code or False:
            if batch.get('shp_code', False) != shp_code.code:
                _logger.info("Reset shipping info for %s group" % (batch['name']))
                self.write(cr, uid, batch_id, {'shipping_code': shp_code.code})
                if batch.get('picking_ids', False):
                    picking_obj.update_shipping_info(cr, uid, batch['picking_ids'], force=True)
        else:
            raise osv.except_osv(
                _('Warning !'),
                """Can't find corresponding shipping code!\nGroup: {grp}\nCode: {code}""".format(
                    grp=address_grp,
                    code=incoming_code
                )
            )

        tracking_refs = picking_obj.read(cr, uid, batch['picking_ids'], ['tracking_ref'])
        for order in tracking_refs:
            if order.get('tracking_ref', False):
                picking_obj.update_shipping_info(cr, uid, batch['picking_ids'], force=True)
                break

        if auto_process:
            process_ids = picking_obj.search(cr, uid, [('id', 'in', order_ids), ('state', 'in', ('assigned', 'incode_except'))])

            if process_ids:
                process_res = picking_obj.action_process(cr, uid, process_ids)

                if process_res:
                    return_action = process_res

        return return_action


    def get_orders(self, cr, uid, batch_id, context=None):

        batch_orders = None
        if batch_id:
            sql = """   SELECT
                            sp.id as order_id,
                            sp.name as order_name,
                            sp.date_out as date_out,
                            sp.state as state,
                            sp.total_price as total_price,
                            sp.report_history as report_history
                        FROM stock_picking sp
                        WHERE 1=1
                            AND sp.shipping_batch_id = %s
                        ;
            """ % (batch_id)
            cr.execute(sql)
            batch_orders = cr.dictfetchall()

        return batch_orders

    def get_shp_code(self, cr, uid, batch_id, context=None):
        res = None
        batch = self.read(cr, uid, batch_id, ['shipping_code', 'picking_ids', 'name'])

        if not batch.get('picking_ids', False):
            res = False
        else:
            shp_code = self.pool.get('res.shipping.code').get_code(cr, uid, pick_id=batch['picking_ids'][0], )

            if shp_code:
                res = shp_code.code

        return res

    def get_active_batch_id(self, cr, uid, address_grp, context=None):
        batch_id = False
        batch_ids = self.search(cr, uid, [('address_grp', '=', address_grp), ('state', '=', 'active')])
        if batch_ids:
            batch_id = batch_ids[0]

        return batch_id

    def action_scan(self, cr, uid, ids, tracking_ref, scan_date=False, context=None):
        if not scan_date:
            scan_date = datetime.utcnow()
        vals = {
            'state': 'scanned',
            'tracking_ref': tracking_ref,
            'scan_date': scan_date,
            }
        return self.write(cr, uid, ids, vals, context=context)

    def action_done(self, cr, uid, ids, context=None):
        vals = {'state': 'done'}
        return self.write(cr, uid, ids, vals, context=context)

stock_picking_shipping_batch()
