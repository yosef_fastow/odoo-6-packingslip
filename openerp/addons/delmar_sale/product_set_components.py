from osv import osv, fields
import logging

logger = logging.getLogger('product.set.components')

class product_set_components(osv.osv):
    _inherit = 'product.set.components'

    def get_formula(self, cr, uid, move_lines):
        components = {}
        psc_lines = {}

        for line in move_lines:
            if line.move_id.set_product_id.id not in components:
                components[line.move_id.set_product_id.id] = []

            components[line.move_id.set_product_id.id].append({'product_id':line.move_id.product_id.id,'s_qty':line.qty})

        if components:
            psc_ids = self.search(cr, uid, [('parent_product_id', 'in', components.keys())])

            psc_lines = {}
            cr.execute("""SELECT cmp_product_id,parent_product_id, qty  FROM product_set_components WHERE id in %s""", (tuple(psc_ids), ))
            for data in cr.dictfetchall():
                psc_lines[data.get('cmp_product_id')] = data.get('qty')

        return psc_lines

    def product_is_set_component(self, cr, uid, product=None):
        if product:
            cmp_ids = self.search(cr, uid, [('cmp_product_id', '=', product.id)])
            if len(cmp_ids) > 0:
                return True
        return False


    def is_set(self, cr, uid, product_id):
        if self.search(cr, uid, [('parent_product_id', '=', product_id)]):
            return True
        return False

    def is_nonsize_set(self, cr, uid, product_id):
        if not product_id:
            return False
        sql = """
        select  case when count(a.sizeable) = 2 then true else false end nonsize from (
        SELECT sizeable FROM "product_set_components" WHERE (product_set_components."parent_product_id" = %s) group by sizeable) a
        """ % (product_id)
        cr.execute(sql)
        res = cr.dictfetchone()

        return res.get('nonsize', False)

product_set_components()

class stock_picking(osv.osv):
    _inherit = 'stock.picking'
stock_picking()

class stock_move(osv.osv):
    _inherit = 'stock.move'

    def get_origin_set_lines(self, cr, uid, ids):
        return self.search(cr, uid, [('set_parent_order_id', 'in', ids), ('is_set', '=', True)])

    def check_set(self, cr, uid, move_lines, context):
        if not isinstance(move_lines, list):
            raise osv.except_osv('Warning!', 'Invalid format')

        if not bool(context.get('count_split_sets', 1)):
            return False

        psc_lines = self.pool.get('product.set.components').get_formula(cr, uid, move_lines)

        res = True

        for line in move_lines:
            if line.qty != psc_lines[line.product_id.id]:
                res = False

        if not res:
            res = True
            for line in move_lines:
                if (psc_lines[line.product_id.id] * context.get('count_split_sets', 1)) != line.qty:
                    return False

        return res

    def move_parent_setline(self, cr, uid, pick_id, to_split, new_picking):
        sets_ids = self.get_parent_setline(cr, uid, pick_id, to_split)
        if sets_ids:
            self.write(cr, uid, sets_ids, {
                'set_parent_order_id': new_picking,
                'picking_id': '',
            })

    def clone_parent_setline(self, cr, uid, pick_id, to_split, new_picking, context):

        sets_ids = self.get_parent_setline(cr, uid, pick_id, to_split)
        if sets_ids:
             for set_id in sets_ids:
                 move = self.browse(cr, uid, set_id)
                 self.copy(
                     cr, uid, set_id,
                     {
                         'set_parent_order_id': new_picking,
                         'picking_id': '',
                         'product_uos_qty': context.get('count_split_sets', 1),
                         'product_qty': context.get('count_split_sets', 1),
                     })

                 self.write(cr, uid, move.id, {
                     'product_uos_qty': move.product_uos_qty - context.get('count_split_sets', 1),
                     'product_qty': move.product_qty -context.get('count_split_sets', 1),
                 })

    def get_parent_setline(self, cr, uid, pick_id, to_split):
        cr.execute("""
            SELECT
                id
            FROM
                stock_move
            WHERE 
                set_parent_order_id = %(pick_id)s
                and set_product_id in (
                    SELECT
                        distinct set_product_id
                    FROM
                        stock_move
                    WHERE
                        id in %(move_lines)s
                        and is_set = True
                    )
                and size_id in (SELECT
                        distinct size_id
                    FROM
                        stock_move
                    WHERE
                         id in %(move_lines)s)
        """, {'pick_id': pick_id, 'move_lines': tuple(to_split)})

        res = cr.fetchall() or []
        sets_ids = [x[0] for x in res]

        return sets_ids


    def check_isset_lines(self, cr, uid, move_ids):
        result = False
        data_stock_moves = self.read(cr, uid, move_ids, ['is_set'])
        for line in data_stock_moves:
            if line.get('is_set'):
                result = True
                break

        return result

stock_move()
