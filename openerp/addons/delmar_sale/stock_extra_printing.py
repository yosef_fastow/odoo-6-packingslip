# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _


class stock_extra_printing(osv.osv_memory):
    _name = "stock.extra.printing"
    _columns = {
        'name': fields.char('Order#', size=256,),
        'picking_id': fields.many2one('stock.picking', 'Delivery Order', readonly=True,),
        'sale_id': fields.many2one('sale.order', 'Sale Order', readonly=True,),
        'order_type': fields.related('picking_id', 'sale_id', 'orderType', string='Order Type', type='char', readonly=True,),
        'sale_order_line_ids': fields.related(
            'picking_id', 'sale_id', 'order_line',
            type='one2many', obj='sale.order.line',
            string='Sale Order Lines', readonly=True,),
        'sterling_comment_ids': fields.many2many(
            'sale.order.sterling.comments', 'extra_printing_comment_rel', 'printing_id', 'picking_id',
            string='Order Reports', readonly=True,),
        'sterling_line_comment_ids': fields.many2many(
            'sale.order.line.sterling.comments', 'extra_printing_comment_line_rel', 'printing_id', 'line_id',
            string='Order Line Reports', readonly=True,),
        'btn_confirm': fields.boolean('Search',),
    }

    def onchange_order_name(self, cr, uid, ids, name, context=None):

        response = {
            'value': {
                'name': name,
                'picking_id': False,
                'sale_id': False,
                'order_type': False,
                'sterling_comment_ids': [],
                'sterling_line_comment_ids': [(6, 0, [])],
            }
        }

        warning_msg = {
            'title': _('Warning!'),
            'message': False
        }

        type_where = ''
        allowed_dc_types = ['DB', 'GR', 'ESP', 'JRP']
        allowed_ss_types = ['LA', 'TN', 'INV', 'OL']

        if name:
            name = name.strip().upper()
            sql = """   SELECT
                            sp.id as picking_id,
                            so.id as sale_id,
                            so."orderType" as order_type,
                            array_agg(ol.id) as line_ids,
                            so."processQueue" as process_queue
                        FROM stock_picking sp
                            LEFT JOIN sale_order so ON so.id = sp.sale_id
                            LEFT JOIN sale_order_line ol ON so.id = ol.order_id
                        WHERE sp.name = '%s'
                        GROUP BY sp.id, so.id, so."orderType", so."processQueue";
            """ % (name)
            cr.execute(sql)
            packing = cr.dictfetchone()

            if packing:
                response['value']['picking_id'] = packing.get('picking_id', False)
                response['value']['sale_id'] = packing.get('sale_id', False)
                order_type = packing.get('order_type', False)
                process_queue = packing.get('process_queue', False)
                response['value']['order_type'] = order_type
                response['value']['sale_order_line_ids'] = packing['line_ids'] or []

                sterling_comment_ids = []

                if order_type:
                    if order_type in ('DC', 'SS') and response['value']['picking_id']:
                        if allowed_dc_types:
                            type_where = "AND lc.type in ('%s')" % ("', '".join(allowed_dc_types))

                        sql = """   SELECT
                                        sm.picking_id as picking_id,
                                        array_agg(lc.id) as comment_ids
                                    FROM stock_move sm
                                        LEFT JOIN sale_order_line sl on sm.sale_line_id = sl.id
                                        LEFT JOIN sale_order_line_sterling_comments lc on sl.id = lc.line_id
                                    WHERE 1=1
                                        AND sm.picking_id = %s
                                        %s
                                    GROUP BY sm.picking_id;
                        """ % (response['value']['picking_id'], type_where)
                        cr.execute(sql)
                        comments = cr.dictfetchone()
                        if comments:
                            response['value']['sterling_line_comment_ids'] = [(6, 0, comments['comment_ids'] or [])]

                    if order_type == 'SS' and response['value']['sale_id']:
                        if allowed_ss_types:
                            type_where = " AND sc.type in ('%s') " % ("', '".join(allowed_ss_types))
                        sql = """   SELECT
                                        so.id as sale_id,
                                        array_agg(sc.id) as comment_ids
                                    FROM sale_order so
                                        LEFT JOIN sale_order_sterling_comments sc ON so.id = sc.order_id
                                    WHERE 1=1
                                        AND so.id = %s
                                        %s
                                    GROUP BY so.id;
                        """ % (response['value']['sale_id'], type_where)
                        cr.execute(sql)
                        comments = cr.dictfetchone()
                        if comments:
                            sterling_comment_ids = comments['comment_ids'] or []

                if process_queue:
                    sql = """   SELECT
                                        so.id as sale_id,
                                        array_agg(sc.id) as comment_ids
                                    FROM sale_order so
                                        LEFT JOIN sale_order_sterling_comments sc ON so.id = sc.order_id
                                    WHERE 1=1
                                        AND so.id = %s
                                        AND sc.type = 'ZPL'
                                    GROUP BY so.id;
                        """ % (response['value']['sale_id'])
                    cr.execute(sql)
                    comments = cr.dictfetchone()
                    if comments:
                        sterling_comment_ids += comments['comment_ids'] or []

                response['value']['sterling_comment_ids'] = [(6, 0, sterling_comment_ids)]

        if not response['value']['picking_id']:
            warning_msg['message'] = _('Delivery order not found.')
            response['warning'] = warning_msg

        elif not response['value']['sale_id']:
            warning_msg['message'] = _('There are no sale orders associated with delivery %s.' % (name))
            response['warning'] = warning_msg

        return response

    def get_report(self, cr, uid, report_id, report_type=False, picking_id=False, line_id=False, context=None):
        if context is None:
            context = {}

        data_obj = self.pool.get('ir.model.data')
        log_obj = self.pool.get('stock.extra.printing.log')

        if not report_id:
            raise osv.except_osv(_('Error!'), _('Missing report id !'))
        report = data_obj.get_object(cr, uid, 'delmar_sale', report_id)
        if not report:
            raise osv.except_osv(_('Error!'), _('Report template not found !'))

        if not report_type:
            raise osv.except_osv(_('Error!'), _('Missing report type !'))

        category = data_obj.get_object(cr, uid, 'delmar_sale', 'cm_report_categ_%s' % (report_type.lower()))
        if not report_type:
            raise osv.except_osv(_('Error!'), _('Missing report type !'))
        if not category:
            raise osv.except_osv(_('Error!'), _('Report category not found !'))

        log_obj.create(cr, uid, {
            'line_id': line_id,
            'picking_id': picking_id,
            'report_id': report.id,
            'categ_id': category.id,
        })

        active_id = False
        if report.model == 'stock.picking':
            if report_type == 'INV' and not self.pool.get('stock.picking').browse(cr, uid, picking_id).tracking_ref:
                raise osv.except_osv(_('Error!'), _('Order without tracking number !'))
            active_id = picking_id
        elif report.model == 'sale.order.line':
            active_id = line_id
        res = {
            'type': 'ir.actions.report.xml',
            'report_name': report.report_name,
            'datas': {
                'model': report.model,
                'report_type': 'pdf',
                'id': active_id,
                'ids': [active_id],
            },
            'context': {
                'categ_id': category.id,
            }
        }

        return res


stock_extra_printing()


class stock_extra_printing_log(osv.osv):
    _name = "stock.extra.printing.log"
    _columns = {
        'create_uid': fields.many2one('res.users', 'User', readonly=True, select=True,),
        'create_date': fields.datetime('Creation Date', readonly=True, select=True,),
        'picking_id': fields.many2one('stock.picking', 'Order', readonly=True,),
        'line_id': fields.many2one('sale.order.line', 'Line', readonly=True,),
        'report_id': fields.many2one('ir.actions.report.xml', 'Report', readonly=True,),
        'categ_id': fields.many2one('customer.report.category', 'Category', readonly=True,),
    }

    _order = "create_date desc"


class sale_order_sterling_comments(osv.osv):
    _inherit = 'sale.order.sterling.comments'

    def _check_tracking(self, cr, uid, ids, name, args, context=None):
        res = {}
        tracking_ref = False
        if context.get('picking_id'):
            tracking_ref = not bool(self.pool.get('stock.picking').browse(cr, uid, context['picking_id']).tracking_ref)
        for id in ids:
            res[id] = tracking_ref
        return res

    _columns = {
        'tracking_exist': fields.function(_check_tracking,
                                          string="Tracking Exist",
                                          type='boolean',
                                          method=True
                                          )
    }

    def action_print_report(self, cr, uid, ids, context=None):
        res = True
        if context is None:
            context = {}

        for comment in self.browse(cr, uid, ids):
            res = self.pool.get('stock.extra.printing').get_report(cr, uid,
                'sterling_ss_report',
                report_type=comment.type,
                picking_id=context.get('picking_id', False),
                line_id=False,
                context=context)

        return res

stock_extra_printing()


class sale_order_line_sterling_comments(osv.osv):
    _inherit = 'sale.order.line.sterling.comments'

    _columns = {
        'delmar_id': fields.related('line_id', 'product_id',
            type='many2one', relation="product.product",
            string="Delmar ID", store=False,),
        'product_image': fields.related('delmar_id', 'product_image',
            type='binary', string='Image', store=False,),
    }

    def action_print_report(self, cr, uid, ids, context=None):
        res = True
        if context is None:
            context = {}

        for comment in self.browse(cr, uid, ids):
            res = self.pool.get('stock.extra.printing').get_report(cr, uid,
                'sterling_dc_report',
                report_type=comment.type,
                picking_id=context.get('picking_id', False),
                line_id=comment.line_id and comment.line_id.id or False,
                context=context)
        return res

sale_order_line_sterling_comments()
