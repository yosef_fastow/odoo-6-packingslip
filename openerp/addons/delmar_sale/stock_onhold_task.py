# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
import time
import netsvc
import logging
from tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
import csv
import base64
from datetime import datetime

from tools.translate import _


logger = logging.getLogger('delmar_sale')


class stock_onhold_task(osv.osv):
    _name = 'stock.onhold.task'

    _item_report_heads = ['Item', 'Size', 'Desired Qty', 'Real Qty', 'Stock', 'Bin']

    def create(self, cr, uid, vals, context=None):
        if ('name' not in vals) or (not vals.get('name', False)):
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.onhold.task')
        new_id = super(stock_onhold_task, self).create(cr, uid, vals, context)
        return new_id

    _columns = {
        'name': fields.char(
            'Name', size=256, readonly=True,
            states={'draft': [('readonly', False)]}
            ),
        'create_uid': fields.many2one('res.users', 'Creator', readonly=True),
        'partner_id': fields.many2one(
            'res.partner', 'Partner',
            select=True, readonly=True,
            states={'draft': [('readonly', False)]}
            ),
        'date_from': fields.datetime('From date', readonly=True),
        'date_to': fields.datetime(
            'To date', required=True, readonly=False,
            states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}
            ),
        'date_done': fields.datetime('End date', required=False, readonly=True),
        'comment': fields.text('Comments'),
        'warehouse_id': fields.many2one(
            'stock.warehouse', 'Warehouse', required=True, readonly=True, select=True,
            states={'draft': [('readonly', False)]},
            domain=[('virtual', '=', False)]
            ),
        'location_id': fields.many2one(
            'stock.location', 'Location', readonly=True, select=True,
            domain=[('usage', '=', 'internal'), ('name', '!=', 'ONHOLD'), ('complete_name', 'ilike', '/ Stock /')],
            states={'draft': [('readonly', False)]}
            ),
        'location_dest_id': fields.many2one(
            'stock.location', 'Dest Location', readonly=True, select=True,
            domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')],
            states={'draft': [('readonly', False)]}
            ),
        'manager_id': fields.many2one(
            'res.users', 'Manager', required=True, select=True, readonly=True,
            states={'draft': [('readonly', False)]}
            ),
        'state': fields.selection([
            ('draft', 'New'),
            ('approved', 'Approved'),
            ('confirmed', 'Confirmed'),
            ('done', 'Released'),
            ('cancel', 'Cancelled'),
            ], 'State', readonly=True, select=True,),
        'csv_file': fields.binary(string="Load list from CSV"),
        'line_ids': fields.one2many('stock.onhold.task.line', 'task_id', string='List of Items'),
        'po_id': fields.many2one(
            "purchase.order", "Purchase order", required=False, select=True, readonly=True,
            states={'draft': [('readonly', False)]}
            ),
    }

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Name must be unique!'),
    ]

    _defaults = {
        'name': False,
        'state': 'draft',
    }

    _order = 'date_to, name'

    # Base methods

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        line_obj = self.pool.get('stock.onhold.task.line')
        loc_obj = self.pool.get('stock.onhold.task.line.location')
        default = default.copy()
        task = self.browse(cr, uid, id, context=context)
        if ('name' not in default) or (not task.name):
            seq_obj_name = 'stock.picking.onhold.task'
            default['name'] = self.pool.get('ir.sequence').get(cr, uid, seq_obj_name)
        default.update({
            'date_from': False,
            'date_to': False,
            'date_done': False,
        })
        res = super(stock_onhold_task, self).copy(cr, uid, id, default=default, context=context)
        if res:
            task = self.browse(cr, uid, res, context=context)
            for line in task.line_ids:
                for loc in line.loc_ids:
                    loc_obj.unlink(cr, uid, [loc.id])

                line_obj.write(cr, uid, [line.id], {
                    'state': 'draft',
                    'approved': False,
                    'real_qty': False,
                    'revert_qty': False,
                })

        return res

    def unlink(self, cr, uid, ids, context=None):

        active_ids = self.search(cr, uid, [('id', 'in', ids), ('state', '=', 'confirmed')], context=context)
        if active_ids:
            raise osv.except_osv(_('ERROR!'), 'Some task(s) still in the stage of ONHOLD. Please finish it(s) before removing')

        else:
            super(stock_onhold_task, self).unlink(cr, uid, ids, context=context)

        return True

    def fill_lines_from_po(self, cr, uid, ids, context=None):
        if not ids:
            return False

        if isinstance(ids, (int, long)):
            ids = [ids]

        ids = self.search(cr, uid, [('id', 'in', ids), ('line_ids', '=', False)])
        if not ids:
            return False

        cr.execute("""
            INSERT INTO stock_onhold_task_line (
                create_uid,
                create_date,
                task_id,
                product_id,
                desired_qty,
                size_id,
                state
            )
            SELECT
                st.create_uid,
                st.create_date,
                st.id as task_id,
                pl.product_id as product_id,
                pl.product_qty as qty,
                pl.size_id as size_id,
                'draft' as state
            FROM stock_onhold_task st
                LEFT JOIN purchase_order po on st.po_id = po.id
                LEFT JOIN purchase_order_line pl on po.id = pl.order_id
            WHERE st.id in %(ids)s
        """, {
            'ids': tuple(ids),
        })

        self.create_bins(cr, uid, ids, context=context)

        return True

    def create_bins(self, cr, uid, ids, context=None):
        if not ids:
            return False

        if isinstance(ids, (int, long)):
            ids = [ids]
        cr.execute("""
            select
                ot.id,
                sw.id as warehouse_id,
                sw.name as warehouse,
                sl.id as location_id,
                sl.name as location
            from stock_onhold_task ot
                left join stock_warehouse sw on sw.id = ot.warehouse_id
                left join stock_location sl on sl.id = ot.location_dest_id
            where ot.id in %(ids)s
                and sw.id is not null
                and sl.id is not null
        """, {
            'ids': tuple(ids),
        })
        sql_res = cr.dictfetchall() or []
        if not sql_res:
            return False

        size_obj = self.pool.get("ring.size")
        for task in sql_res:
            cr.execute("""
                select distinct
                    pp.id as product_id,
                    rs.id as size_id,
                    pp.default_code as id_delmar,
                    rs.name as size
                from stock_onhold_task_line tl
                    left join product_product pp on pp.id = tl.product_id
                    left join ring_size rs on rs.id = tl.size_id
                where tl.task_id = %(task_id)s
            """, {
                'task_id': task['id']
            })
            task_items_res = cr.dictfetchall() or []
            task_items = {
                '%s/%s' % (x['id_delmar'], size_obj.convert_to_4digit_name(cr, uid, x['size'])): {
                    'product_id': x['product_id'],
                    'size_id': x['size_id']
                } for x in task_items_res
            }

            to_check = set(task_items.keys())
            exists_bins = self.pool.get('stock.bin').get_mssql_bin_for_product(
                cr, uid,
                row_itemids=to_check,
                row_warehouse=task['warehouse'],
                row_stock=task['location'],
                ignore_bins=['', 'NULL']
                )

            to_generate = to_check - set(exists_bins.keys())
            if to_generate:
                sb_create_obj = self.pool.get('stock.bin.create')
                for itemid in to_generate:
                    try:
                        sb_create_id = sb_create_obj.create(cr, uid, {
                            'warehouse_id': task['warehouse_id'],
                            'location_id': task['location_id'],
                            'location': task['location'],
                            'product_id': task_items[itemid]['product_id'],
                            'size_id': task_items[itemid]['size_id'] or None,
                            'qty_capacity': 0
                        })
                        if sb_create_id and sb_create_obj.check_location(cr, uid, sb_create_id):
                            sug_bin = sb_create_obj.read(cr, uid, sb_create_id, ['name'])['name']
                            if sug_bin and sug_bin != 'null':
                                sb_create_obj.check_bin(cr, uid, sb_create_id)
                            else:
                                raise osv.except_osv('Warning!', 'There is no available names!')
                    except Exception, e:
                        logger.error("Can't create new bin for %s/%s/%s:\n%s" % (task['warehouse'], task['location'], itemid, e))
                        pass
        return True

    # Wkf Actions
    def action_draft(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        line_obj = self.pool.get('stock.onhold.task.line')

        for task in self.read(cr, uid, ids):

            line_obj.action_draft(cr, uid, task.get('line_ids', []), context=context)

            self.write(cr, uid, task['id'], {
                'state': 'draft',
            }, context=context)

        return True

    def action_approve(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        line_obj = self.pool.get('stock.onhold.task.line')

        for task in self.read(cr, uid, ids, context=context):

            line_obj.action_approve(cr, uid, task.get('line_ids', []), context=context)

            self.write(cr, uid, task['id'], {
                'state': 'approved',
            }, context=context)

        self.notify_approved(cr, uid, ids, context=context)

        return True

    def action_confirm(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        server_obj = self.pool.get('fetchdb.server')
        line_obj = self.pool.get('stock.onhold.task.line')

        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), 'Can not put poduct in ONHOLD stock. Server with TRANSACTIONS table is not available!')

        for task in self.browse(cr, uid, ids, context=context):

            try:
                # TODO: split condition for PO and Regular orders
                res = {}
                to_onhold = []

                if task.po_id:
                    to_onhold = [x.id for x in task.line_ids if x.desired_qty > 0]
                    if to_onhold:
                        res = line_obj.force_put_onhold(cr, uid, task=task, lines=to_onhold, context=context)

                else:
                    for line in task.line_ids:
                        if line.state == 'approved' and line.onhand and line.approved:
                            to_onhold.append(line)

                    if to_onhold:
                        res = line_obj.put_onhold(cr, uid, task=task, lines=to_onhold, server_id=server_id, context=context)

                if not (res and res.get('line_ids', False)):
                    raise osv.except_osv(_('ERROR!'), 'Can\'t to do ONHOLD reserve')

                self.write(cr, uid, task.id, {
                    'state': 'confirmed',
                    'date_from': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                }, context=context)

            except Exception, e:
                sql = """   DELETE from dbo.TRANSACTIONS
                            WHERE 1=1
                                AND INVOICENO = ?
                                AND _USER_CREATOR like 'openerp%%';
                """
                server_obj.make_query(cr, uid, server_id, sql, params=["ADJ_TO_ONHOLD_%s" % (task.name)], select=False, commit=True)
                raise osv.except_osv(_('ERROR!'), 'Can\'t to do ONHOLD reserve.\n\n{details}'.format(details=e.value if isinstance(e, osv.except_osv) else ''))

        self.notify_confirmed(cr, uid, ids, context=context)

        return True

    def action_done(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        server_obj = self.pool.get('fetchdb.server')
        line_obj = self.pool.get('stock.onhold.task.line')

        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), 'Can not put poduct in ONHOLD stock. Server with TRANSACTIONS table is not available!')

        for task in self.browse(cr, uid, ids, context=context):

            res = {}
            try:
                to_revert = []
                for line in task.line_ids:
                    if line.state == 'onhold':
                        to_revert.append(line)

                if to_revert:
                    res = line_obj.revert_onhold(cr, uid, task=task, lines=to_revert, server_id=server_id, context=context)

                if not (res and res.get('line_ids', False)):
                    raise osv.except_osv(_('ERROR!'), 'Nothing was returned')

                self.write(cr, uid, task['id'], {
                    'state': 'done',
                    'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                }, context=context)

            except Exception, e:
                sql = """   DELETE from dbo.TRANSACTIONS
                            WHERE 1=1
                                AND INVOICENO = ?
                                AND _USER_CREATOR like 'openerp%%';
                """
                server_obj.make_query(cr, uid, server_id, sql, params=["ADJ_FROM_ONHOLD_%s" % (task.name)], select=False, commit=True)
                raise osv.except_osv(_('ERROR!'), 'Can\'t to do ONHOLD revert.\n\n{details}'.format(details=e.value if isinstance(e, osv.except_osv) else ''))

            if res and res.get('conflicts', False):
                task_ctx = context.copy() or {}
                task_ctx['conflicts'] = res['conflicts']
                self.notify_done(cr, uid, [task.id], context=task_ctx)

        return True

    def action_cancel(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        line_obj = self.pool.get('stock.onhold.task.line')

        for task in self.read(cr, uid, ids, context=context):

            line_obj.action_cancel(cr, uid, task.get('line_ids', []), context=context)

            self.write(cr, uid, task['id'], {
                'state': 'cancel',
            }, context=context)

        return True

    # Conditions
    def allow_reject(self, cr, uid, ids, context=None):

        return True

    def allow_approve(self, cr, uid, ids, context=None):

        for task in self.browse(cr, uid, ids, context=context):

            if task.po_id:
                return False

            if task.date_to:
                self.validate_date(task.date_to, period='futire')

            if not task.manager_id:
                raise osv.except_osv(_('Error'), 'The manager is not selected')

            if not task.line_ids:
                raise osv.except_osv(_('Error'), 'Nothing to approve')

            for line in task.line_ids:
                if not line.desired_qty or line.desired_qty < 0:
                    raise osv.except_osv(_('Error'), 'Desired qty must be positive')

                categ = line.product_id and line.product_id.categ_id or None
                if categ and categ.sizeable and not line.size_id:
                    raise osv.except_osv(_('Error'), 'The size can not be empty for {category}'.format(category=categ.name))

        return True

    def allow_confirm(self, cr, uid, ids, context=None):

        for task in self.browse(cr, uid, ids, context=context):
            if task.date_to:
                self.validate_date(task.date_to, period='future')
            if not [x for x in task.line_ids if x.approved]:
                return False

        return True

    def allow_po_confirm(self, cr, uid, ids, context=None):

        for task in self.browse(cr, uid, ids, context=context):
            if not task.po_id:
                return False
            if task.date_to:
                self.validate_date(task.date_to, period='future')
            if not [x for x in task.line_ids if x.desired_qty > 0]:
                return False

        return True

    def allow_cancel(self, cr, uid, ids, context=None):

        return True

    # Notifications
    def notify_approved(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        context.update({'act_id': self.get_action_id(cr, uid)})
        self.send_mail(cr, uid, ids, 'email_template_stock_onhold_user_approve', context=context)
        self.send_mail(cr, uid, ids, 'email_template_stock_onhold_manager_approve', context=context)

        return True

    def notify_confirmed(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        filtered_ids = self.filter_tasks_for_notify_confirmed(cr, uid, ids, context=context)
        if filtered_ids:

            data_obj = self.pool.get('ir.model.data')
            cc_group = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_stock_analysis')
            email_cc = self.pool.get('res.groups').get_user_emails(cr, uid, [cc_group[1]])[cc_group[1]] or []
            context.update({'email_cc': ", ".join(email_cc)})

            context.update({'act_id': self.get_action_id(cr, uid)})
            self.send_mail(cr, uid, filtered_ids, 'email_template_stock_onhold_user_confirmed', context=context)
            self.send_mail(cr, uid, filtered_ids, 'email_template_stock_onhold_manager_confirmed', context=context)

        return True

    def filter_tasks_for_notify_confirmed(self, cr, uid, ids, context=None):
        filtered_ids = []

        if not ids:
            return filtered_ids

        if isinstance(ids, (int, long)):
            ids = [ids]

        cr.execute("""
            select distinct  ol.task_id as id
            from stock_onhold_task_line ol
            where ol.task_id in %(task_ids)s
                and ol.approved
                and coalesce(ol.real_qty, 0) > 0
        """, {
            'task_ids': tuple(ids)
        })
        q_res = cr.fetchall() or []
        filtered_ids = [x[0] for x in q_res]

        return filtered_ids

    def notify_done(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        data_obj = self.pool.get('ir.model.data')
        cc_group = data_obj.get_object_reference(cr, uid, 'delmar_sale', 'group_stock_analysis')
        email_cc = self.pool.get('res.groups').get_user_emails(cr, uid, [cc_group[1]])[cc_group[1]] or []
        context.update({'email_cc': ", ".join(email_cc)})

        context.update({'act_id': self.get_action_id(cr, uid)})
        self.send_mail(cr, uid, ids, 'email_template_stock_onhold_user_done', context=context)
        self.send_mail(cr, uid, ids, 'email_template_stock_onhold_manager_done', context=context)

        return True

    # Scheduled Methods
    def _scheduled_action_done(self, cr, uid, context=None):

        logger.debug("Check Scheduled ONHOLD tasks")

        wf_service = netsvc.LocalService("workflow")
        task_ids = self.search(cr, uid, [
            ('state', '=', 'confirmed'),
            ('date_to', '!=', False),
            ('date_to', '<', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        ])

        if task_ids:
            logger.debug("Scheduled action done for following tasks: %s" % (task_ids))

        for task_id in task_ids:
            wf_service.trg_validate(uid, self._name, task_id, 'button_done', cr)

        return True

    # Methods
    def validate_date(self, date_str, period='future', context=None):
        date_tst = False
        try:
            date_tst = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        except:
            raise osv.except_osv(_('Error'), 'Wrong datetime format')

        if date_tst <= datetime.now():
            raise osv.except_osv(_('Error'), 'Datetime in the past')

    def get_action_id(self, cr, uid, context=None):

        model_obj = self.pool.get('ir.model.data')
        action = model_obj.get_object_reference(cr, uid, 'delmar_sale', 'action_stock_onhold')
        if action:
            return action[1]

        return False

    def load_items(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        size_obj = self.pool.get('ring.size')
        prod_obj = self.pool.get('product.product')
        line_obj = self.pool.get('stock.onhold.task.line')

        for task in self.browse(cr, uid, ids, context=context):

            try:
                csv_file = base64.decodestring(task.csv_file)
                file_text = csv_file.replace('\r\n', '\n').replace('\r', '\n').split('\n')
                rows = list(csv.reader(file_text)) or []
                missing_rows = 0
                for row in rows:
                    if not row:
                        continue
                    try:
                        delmar_id, size, qty = row
                    except:
                        continue

                    logger.debug("%s %s %s" % (delmar_id, size, qty))

                    if not delmar_id or not qty:
                        missing_rows += 1
                        continue

                    finding_pc_ids = self.pool.get('product.category').search(cr, uid, [('name', '=', 'Finding')])
                    finding_pc_ids = finding_pc_ids or None

                    prod_domain = [('default_code', '=', delmar_id)]
                    if finding_pc_ids:
                        prod_domain.append(('categ_id', 'not in', finding_pc_ids))

                    prod_id = False
                    # prod_ids = prod_obj.name_search(cr, uid, name=delmar_id, operator='=', limit=1)
                    prod_ids = prod_obj.search(cr, uid, prod_domain, limit=1)
                    if not prod_ids:
                        missing_rows += 1
                        continue
                    # prod_id = prod_ids[0][0]
                    prod_id = prod_ids[0]
                    prod = prod_obj.browse(cr, uid, prod_id)

                    size_id = False
                    if prod.sizeable and size:
                        size_ids = size_obj.search(cr, uid, [('name', '=', size)], limit=1)
                        if size_ids:
                            size_id = size_ids[0]

                    old_line_ids = line_obj.search(cr, uid, [('task_id', '=', task.id), ('product_id', '=', prod_id), ('size_id', '=', size_id)])
                    if old_line_ids:
                        line_obj.write(cr, uid, old_line_ids, {'desired_qty': qty})
                    else:
                        line_obj.create(cr, uid, {
                            'task_id': task.id,
                            'product_id': prod_id,
                            'desired_qty': qty,
                            'size_id': size_id,
                        })

            except:
                raise osv.except_osv(_('Warning!'), 'Can not load CSV file!')

        return True

    def send_mail(self, cr, uid, ids, tmpl, context=None):

        if not ids or not tmpl:
            return True

        if context is None:
            context = {}

        tmpl_obj = self.pool.get('email.template')
        data_obj = self.pool.get('ir.model.data')

        tmpl_res_id = data_obj._get_id(cr, uid, 'delmar_sale', tmpl)
        tmpl_id = data_obj.read(cr, uid, tmpl_res_id, ['res_id']).get('res_id', False)

        if tmpl_id:

            web_root_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')

            context.update({
                'web_root_url': web_root_url,
            })

            for task_id in ids:
                tmpl_obj.send_mail(cr, uid, tmpl_id, task_id, force_send=False, context=context)

        return True

    # On change
    def change_warehouse_id(self, cr, uid, ids, wh_id=None, context=None):

        domain = {}
        result = {
            'warehouse_id': wh_id,
            'location_id': False,
            'location_dest_id': False,
        }

        wh = self.pool.get('stock.warehouse').browse(cr, uid, wh_id, context=context)
        if wh and wh.lot_stock_id:
            domain = {
                'location_id': [('id', 'child_of', wh.lot_stock_id.id), ('usage', '=', 'internal'), ('name', '!=', 'ONHOLD'), ('complete_name', 'ilike', '/ Stock /')],
                'location_dest_id': [('id', 'child_of', wh.lot_stock_id.id), ('usage', '=', 'internal'), ('complete_name', 'ilike', '/ Stock /')]
            }

        return {'value': result, 'domain': domain}

    # Reports
    def action_print_report(self, cr, uid, ids, module='delmar_sale', report_id=False, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.report.xml')

        new_context = {
            'active_ids': ids,
            'active_id': [ids],
        }

        act_report_res_id = data_obj._get_id(cr, uid, module, report_id)
        act_report_id = data_obj.read(cr, uid, act_report_res_id, ['res_id'])
        act_report = act_obj.read(cr, uid, act_report_id['res_id'], [], context=context)
        act_report.update({
            'context': new_context,
        })

        return act_report

    def action_print_onhold_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        active_ids = context.get('active_ids', [])
        if not active_ids:
            raise osv.except_osv(_('Warning!'), 'You must choose at least one record.')
        confirmed_ids = self.search(cr, uid, [('state', 'in', ('confirmed', 'done')), ('id', 'in', active_ids)])

        if not confirmed_ids:
            raise osv.except_osv(_('Warning!'), 'You must choose at least one record in "Confirmed" or "Released" state.')
        act_report = self.action_print_report(cr, uid, confirmed_ids, module='delmar_sale', report_id='report_stock_onhold', context=context)

        return act_report

    def action_approve_all_lines(self, cr, uid, ids, context=None):
        vals = {'approved': True}
        return self.update_all_lines(cr, uid, ids, vals, state='approved', context=context)

    def action_reject_all_lines(self, cr, uid, ids, context=None):
        vals = {'approved': False}
        return self.update_all_lines(cr, uid, ids, vals, state='approved', context=context)

    def update_all_lines(self, cr, uid, ids, vals, state=None, context=None):
        if ids and vals:
            for task in self.read(cr, uid, ids, ['state', 'line_ids']):
                if state is not None:
                    if task['state'] != state:
                        continue
                if task['line_ids']:
                    self.pool.get('stock.onhold.task.line').write(cr, uid, task['line_ids'], vals)
        return True

    def cron_send_sales(self, cr, uid, context=None):
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(
                _('Warning!'),
                (
                    'Can build onhold sales report.',
                    'Server with TRANSACTIONS table is not available!'
                )
            )

        active_sot_ids = self.search(cr, uid, [('state', '=', 'confirmed')])
        for sot in self.browse(cr, uid, active_sot_ids):
            sales_transactions = []
            sql = '''
                WITH limits AS (
                    SELECT
                        ITEMID,
                        WAREHOUSE,
                        STOCKID,
                        BIN,
                        ID AS FROM_ID
                    FROM TRANSACTIONS
                    WHERE 1 = 1
                          AND INVOICENO = %(invoiceno)s
                          AND SIGN > 0
                )
                SELECT
                    t.INVOICENO,
                    t.TRANSACTIONCODE,
                    t.ITEMID,
                    t.WAREHOUSE,
                    t.STOCKID,
                    t.ADJUSTED_QTY
                FROM TRANSACTIONS t
                    INNER JOIN limits l
                        ON t.id > l.FROM_ID
                        AND t.ITEMID = l.ITEMID
                    AND t.WAREHOUSE = l.WAREHOUSE
                    AND t.STOCKID = l.STOCKID
                    AND t.BIN = l.BIN
                WHERE 1 = 1
                    AND t.INVOICENO IS NOT NULL
                    AND t.TRANSACTDATE >= dateadd(day,datediff(day,1,GETDATE()),0)
                    AND t.TRANSACTDATE < dateadd(day,datediff(day,0,GETDATE()),0)
                ORDER BY t.ITEMID, t.ID
            '''
            params = {
                'invoiceno': 'ADJ_TO_ONHOLD_{}'.format(sot.name),
            }
            answer = server_obj.make_query(
                cr, uid, server_id,
                sql,
                params=params,
                select=True,
                commit=False,
            ) or []
            for row in answer:
                sales_transactions.append({
                    'INVOICENO': row[0],
                    'TRANSACTIONCODE': row[1],
                    'ITEMID': row[2],
                    'WAREHOUSE': row[3],
                    'STOCKID': row[4],
                    'ADJUSTED_QTY': row[5]
                })
            if sales_transactions:
                self.send_mail(
                    cr, uid, [sot.id],
                    'email_template_stock_onhold_sales',
                    context={'sales_transactions': sales_transactions}
                )

stock_onhold_task()


class stock_onhold_task_line(osv.osv):
    _name = 'stock.onhold.task.line'

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for line in self.browse(cr, uid, ids, context=context):
            res.append((line.id, "%s/%s: %s" % (
                line.product_id and line.product_id.default_code or 'Unknown Product',
                line.size_id and line.size_id.name or '0',
                line.desired_qty or '0',
            )))
        return res

    def _get_onhand(self, cr, uid, ids, name, args, context=None):

        onhold = self.get_onhand(cr, uid, ids, context=context)
        return onhold

    _columns = {
        'name': fields.char('Name', size=250, required=False, select=True),
        'task_id': fields.many2one('stock.onhold.task', 'Task'),
        'product_id': fields.many2one(
            'product.product', 'Item',
            required=True, select=True, readonly=True,
            states={'draft': [('readonly', False)]},
            ),
        'categ_name': fields.related(
            'product_id', 'categ_name', string='Category',
            type='char', relation='product.product',
            readonly=True, store=False,
            ),
        'sizeable': fields.related(
            'product_id', 'sizeable', string='Sizeable',
            type='boolean', relation='product.product',
            readonly=True, store=False,
            ),
        'size_id': fields.many2one(
            'ring.size', 'Size', select=True, readonly=True,
            states={'draft': [('readonly', False)]},
            ),
        'desired_qty': fields.integer(
            'Qty', required=True, select=True, readonly=True,
            states={'draft': [('readonly', False)]},
            ),
        'real_qty': fields.integer(
            'Reserved qty', required=False, select=True, readonly=True,
            ),
        'revert_qty': fields.integer(
            'Reverted qty', required=False, select=True, readonly=True,
            ),
        'onhand': fields.function(
            _get_onhand, string='On hand', type='integer', method=True, store=False
            ),
        'approved': fields.boolean(
            'Approved', readonly=True,
            states={'approved': [('readonly', False)]},
            ),
        'state': fields.selection([
            ('draft', 'New'),
            ('approved', 'Approved'),
            ('onhold', 'Onhold'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),
        ], 'State', readonly=True, select=True,),
        'loc_ids': fields.one2many('stock.onhold.task.line.location', 'line_id', string='List of Locations'),
    }

    _defaults = {
        'state': 'draft',
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        default.update({
            'state': 'draft',
            'approved': False,
            'real_qty': False,
            'revert_qty': False,
        })
        res = super(stock_onhold_task_line, self).copy(cr, uid, id, default=default, context=context)

        if res:
            loc_obj = self.pool.get('stock.onhold.task.line.location')
            for line in self.read(cr, uid, res, fields=['loc_ids'], context=context):
                if line.get('loc_ids', False):
                    loc_obj.unlink(cr, uid, line['loc_ids'])

        return res

    def action_draft(self, cr, uid, ids, context=None):

        self.write(cr, uid, ids, {
            'state': 'draft',
            'approved': False
        }, context=context)

        return True

    def action_approve(self, cr, uid, ids, context=None):

        self.write(cr, uid, ids, {
            'state': 'approved',
        }, context=context)

        return True

    def action_cancel(self, cr, uid, ids, context=None):

        to_cancel = []
        for line in self.browse(cr, uid, ids, context=context):
            if line.state not in ('onhold', 'done'):
                to_cancel.append(line.id)

        self.write(cr, uid, to_cancel, {'state': 'cancel'}, context=context)
        return True

    # Methods
    def change_product_id(self, cr, uid, ids, product_id, context=None):

        v = {
            'product_id': product_id,
            'size_id': False,
            'categ_name': False,
            'sizeable': False,
        }

        prod = self.pool.get('product.product').browse(cr, uid, product_id)
        if prod:
            v.update({
                'categ_name': prod.categ_name,
                'sizeable': prod.sizeable,
            })

        return {'value': v}

    def get_stock(self, cr, uid, itemid=False, warehouse=False, stockid=False, ignore_stockid=False, server_id=False, context=None):
        res = []

        if not itemid or not warehouse:
            return res

        if context is None:
            context = {}

        ignore_stockids = ['ONHOLD']
        if ignore_stockid:
            ignore_stockids.append(ignore_stockid)

        server_obj = self.pool.get('fetchdb.server')
        if not server_id:
            server_id = server_obj.get_sale_servers(cr, uid, single=True)

        if not server_id:
            raise osv.except_osv(_('Warning!'), 'Can not onhand qty. Server with TRANSACTIONS table is not available!')

        stockid_where = "" if not stockid else " AND stockid = '%s' " % (stockid)

        mode = context.get('mode', 'positive')

        having_str = 'HAVING SUM(adjusted_qty) > 0'
        if mode == 'positive':
            having_str = ''
        size_obj = self.pool.get('ring.size')
        id_delmar, size = size_obj.split_itemid_to_id_delmar_size(cr, uid, itemid)

        sql = """SELECT
                    replace(concat(id_delmar, '/', size), ' ', '') as itemid,
                    SUM(adjusted_qty) AS qty,
                    warehouse as warehouse,
                    stockid as stockid,
                    bin as bin
                FROM dbo.TRANSACTIONS
                WHERE id_delmar = '%s'
                    AND size = '%s'
                    AND warehouse = '%s'
                    AND stockid not in ('%s')
                    AND status <> 'Posted'
                    AND bin != '' AND bin is not null
                    %s
                GROUP BY id_delmar, size, warehouse, stockid, bin
                %s
                ORDER BY qty desc;
        """ % (id_delmar, size, warehouse, "','".join(ignore_stockids), stockid_where, having_str)
        res_sql = server_obj.make_query(cr, uid, server_id, sql, select=True, commit=False) or []
        for row in res_sql:
            if row:
                res.append({
                    'itemid': row[0],
                    'qty': int(row[1]),
                    'warehouse': row[2],
                    'stockid': row[3],
                    'bin': row[4]
                })

        return res

    def check_multi_bins(self, cr, uid, itemids, warehouse=None, autorelease=False, onhold=False, variants=None, mode=None, context=None):
        """
        Raise exception if there is some conflicts in current stock.
        variants:
               all - check all variants of conflicts;
               multi_bins - check if one item has stock on several different bins in one warehouse;
               multi_items - check if bins what used for current list of items also used for other items.
        mode:
               raise - generate exception;
               conflicts_only - return only conflicts text;
               other - just return message.
        """

        if not variants:
            variants = 'all'

        if not mode:
            mode = 'raise'

        conflicts = []
        results = ""

        if variants in ['all', 'multi_bins']:
            multi_bins = self.pool.get('stock.move').check_mssql_multibin(cr, uid, itemids=itemids, warehouse=warehouse, autorelease=True, context=None)
            if multi_bins:
                messages = []
                for wh_, item_, variants in multi_bins:
                    variants_ = []
                    for stock_, bin_, qty_ in variants:
                        variants_.append('{stock_} / {bin_} with qty = {qty_}'.format(stock_=stock_, bin_=bin_, qty_=qty_))
                    messages.append("{warehouse} {item} has following variants: \n{variants}.".format(warehouse=wh_, item=item_, variants="\n".join(variants_)))
                if messages and not onhold:
                    conflicts.append("Following items use diferent bins:\n\n{messages}".format(messages="\n\n".join(messages)))

        if variants in ['all', 'multi_items']:
            multi_bins = self.pool.get('stock.move').check_mssql_multiitem(cr, uid, itemids=itemids, warehouse=warehouse, context=None)
            if multi_bins:
                messages = []
                for wh_, bin_, variants in multi_bins:
                    variants_ = []
                    for stock_, item_, qty_ in variants:
                        variants_.append('{item_}  {stock_} with qty = {qty_}'.format(stock_=stock_, item_=item_, qty_=qty_))
                    messages.append("{warehouse} {bin} has following variants: \n{variants}.".format(warehouse=wh_, bin=bin_, variants="\n".join(variants_)))
                if messages and not onhold:
                    conflicts.append("Following bins have several items:\n\n{messages}".format(messages="\n\n".join(messages)))

        if conflicts:
            results = ''
            if mode == 'conflicts_only':
                results = "\n\n".join(conflicts)
            else:
                results = 'There are several conflicts what should be resolved!\n\n{messages}\n\nPlease, fix them on Stock Move screen.'.format(messages="\n\n".join(conflicts))

            if mode == 'raise':
                raise osv.except_osv(
                    _('Warning!'),
                    results
                )
        return results

    def put_onhold(self, cr, uid, task=None, lines=None, server_id=None, context=None):

        if not lines:
            lines = []
        response = {'line_ids': []}
        line_objs = []
        if not task or not lines or not server_id:
            logger.debug("Have nothing to put on ONHOLD")
            return response

        if not task.warehouse_id:
            logger.debug("Warehouse is not specified")
            return response

        move_obj = self.pool.get('stock.move')
        line_location_obj = self.pool.get('stock.onhold.task.line.location')

        invoice_no = "ADJ_TO_ONHOLD_%s" % (task.name)
        warehouse = task.warehouse_id and task.warehouse_id.name or False
        onhold = task.warehouse_id and task.warehouse_id.onhold or False
        partner_ref = task.partner_id and task.partner_id.ref or False
        stockid = task.location_id and task.location_id.name or False
        onhold_stock = task.location_dest_id and task.location_dest_id.name or 'ONHOLD'
        itemids = set([])

        for line in lines:
            if line.state == 'approved' and line.approved and line.desired_qty:

                delmarid = line.product_id and line.product_id.default_code or False
                description = line.product_id and line.product_id.description
                size_postfix = move_obj.get_size_postfix(cr, uid, line.size_id)
                itemid = delmarid and (delmarid + '/' + size_postfix) or False

                if not itemid:
                    continue

                line_obj = {
                    'id': line.id,
                    'desired_qty': line.desired_qty,
                    'delmarid': delmarid,
                    'description': description,
                    'size_postfix': size_postfix,
                    'itemid': itemid,
                }
                line_objs.append(line_obj)
                itemids.add(itemid)

        self.check_multi_bins(cr, uid, itemids=list(itemids), warehouse=warehouse, autorelease=True, onhold=onhold, variants='all', mode='conflicts_only', context=None)

        for line in line_objs:

            real_qty = 0
            desired_qty = line['desired_qty']

            delmarid = line['delmarid']
            description = line['description']
            size_postfix = line['size_postfix']
            itemid = line['itemid']

            logger.debug("Item: %s" % (itemid))
            logger.debug("Desired qty: %s" % (desired_qty))

            onhold_bin = None
            onhold_invredid = move_obj.get_invredid_for_mssql_row(cr, uid, itemid, warehouse, onhold_stock, onhold_bin, ignore_bin=True)
            onhold_reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, warehouse, onhold_stock, server_id)

            res = self.get_stock(
                cr, uid,
                itemid=itemid,
                warehouse=warehouse,
                stockid=stockid,
                ignore_stockid=onhold_stock,
                server_id=server_id,
                context={'mode': 'positive'}
                )
            logger.debug("Found in %s" % (res))

            for row in res:

                logger.debug("Row: %s" % (row))
                if not desired_qty or desired_qty < 0:
                    break

                bin_qty = row.get('qty', 0)
                source_stock = row.get('stockid', False)
                source_bin = row.get('bin', False)
                onhold_bin = source_bin

                if not bin_qty:
                    continue

                cur_qty = bin_qty
                if bin_qty > desired_qty:
                    cur_qty = desired_qty

                logger.debug("Cur bin qty: %s" % (cur_qty))

                source_invredid = move_obj.get_invredid_for_mssql_row(cr, uid, itemid, warehouse, source_stock, source_bin, ignore_bin=True)
                source_reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, warehouse, source_stock, server_id)

                sign = -1
                params = (
                    itemid or 'null',  # itemid
                    'ADJ',  # TransactionCode
                    cur_qty,  # qty
                    warehouse or 'null',  # warehouse
                    description or 'null',  # itemdescription
                    source_bin,  # bin
                    source_stock,  # stockid
                    'TRS',  # TransactionType
                    delmarid,  # id_delmar
                    sign,  # sign
                    (cur_qty * sign) or '0',  # adjusted_qty
                    size_postfix,  # size
                    source_invredid,  # move.invredid or 'null',  # invredid
                    partner_ref,  # CustomerID
                    delmarid,  # ProductCode
                    invoice_no or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
                    source_reserved_flag or 0,  # reserved
                    None,  # trans_tag
                    0,  # unit price
                )
                success_get = bool(move_obj.write_into_transactions_table(cr, uid, server_id, params))
                if not success_get:
                    logger.debug("Can't recived from %s %s " % (source_stock, source_bin))
                    raise Exception()

                sign = 1
                params = (
                    itemid or 'null',  # itemid
                    'ADJ',  # TransactionCode
                    cur_qty,  # qty
                    warehouse,  # warehouse
                    description or 'null',  # itemdescription
                    onhold_bin,  # bin
                    onhold_stock,  # stockid
                    'TRS',  # TransactionType
                    delmarid,  # id_delmar
                    sign,  # sign
                    (cur_qty * sign) or '0',  # adjusted_qty
                    size_postfix,  # size
                    onhold_invredid,  # invredid
                    partner_ref,  # CustomerID
                    delmarid,  # ProductCode
                    invoice_no or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
                    onhold_reserved_flag or 0,  # reserved
                    None,  # trans_tag
                    0,  # unit price
                )
                success_put = bool(move_obj.write_into_transactions_table(cr, uid, server_id, params))
                if not success_put:
                    logger.debug("Can't put on %s %s " % (onhold_stock, onhold_bin))
                    raise Exception()

                line_location_obj.create(cr, uid, {
                    'line_id': line['id'],
                    'stock': source_stock,
                    'qty': cur_qty,
                    'bin': source_bin,
                })

                real_qty += cur_qty
                desired_qty -= cur_qty

            self.write(cr, uid, line['id'], {'state': 'onhold', 'real_qty': real_qty})
            response['line_ids'].append(line['id'])

        return response

    def force_put_onhold(self, cr, uid, task=None, lines=None, context=None):
        response = {'line_ids': []}
        if not lines:
            return response

        cr.execute("""
            UPDATE stock_onhold_task_line
            SET
                write_uid = %(uid)s,
                write_date = (now() at time zone 'UTC'),
                state = 'onhold',
                real_qty = desired_qty,
                approved = true
            WHERE id in %(ids)s
        """, {
            'uid': uid,
            'ids': tuple(lines),
        })

        if task.location_dest_id and task.location_dest_id.id:
            cr.execute("""
                INSERT INTO stock_onhold_task_line_location (
                    create_uid,
                    create_date,
                    stock,
                    qty,
                    line_id
                )
                SELECT
                    %(uid)s as create_uid,
                    (now() at time zone 'UTC') as create_date,
                    %(stock)s as stock,
                    desired_qty as qty,
                    id as line_id
                FROM stock_onhold_task_line
                WHERE id in %(ids)s
            """, {
                'uid': uid,
                'stock': task.location_id.name,
                'ids': tuple(lines),
            })

        response['line_ids'] = lines
        return response

    def revert_onhold(self, cr, uid, task=None, lines=None, server_id=None, context=None):
        if not lines:
            lines = []
        logger.debug("## Revert DONE")
        logger.debug("Params: \ntask: %s\nlines: %s" % (task.name, lines))

        response = {'line_ids': [], 'conflicts': None}
        reverted_itemids = set()
        if not task or not lines or not server_id:
            logger.debug("Have nothing to revert from ONHOLD")
            return response

        if not task.warehouse_id:
            logger.debug("Warehouse is not specified")
            return response

        move_obj = self.pool.get('stock.move')
        server_obj = self.pool.get('fetchdb.server')
        line_location_obj = self.pool.get('stock.onhold.task.line.location')

        active_onhold = {}
        available_onhold = {}
        sql = """   SELECT sl1.id as line_id, case when sum(sl2.real_qty) is Null then 0 else sum(sl2.real_qty) end AS qty
                    FROM stock_onhold_task st1
                        LEFT JOIN stock_onhold_task_line sl1 ON (st1.id = sl1.task_id)
                        LEFT JOIN stock_onhold_task_line sl2 ON (
                            sl1.product_id = sl2.product_id
                            and COALESCE(sl1.size_id, -1) = COALESCE(sl2.size_id, -1)
                        )
                        LEFT JOIN stock_onhold_task st2 ON (sl2.task_id = st2.id)
                            and COALESCE(st1.location_dest_id, -1) = COALESCE(st2.location_dest_id, -1)
                    WHERE 1=1
                        AND st1.id = %s
                        AND sl1.state = 'onhold'
                        AND sl2.state = 'onhold'
                        AND st2.state = 'confirmed'
                        AND st1.warehouse_id = st2.warehouse_id
                        AND sl1.task_id != sl2.task_id
                    GROUP BY sl1.id;
        """ % (task.id)
        cr.execute(sql)
        res = cr.dictfetchall()
        for row in res:
            active_onhold[row.get('line_id', False)] = int(row.get('qty', 0))

        items = {}
        invoice_no = "ADJ_FROM_ONHOLD_%s" % (task.name)
        warehouse = task.warehouse_id.name
        onhold = task.warehouse_id.onhold
        onhold_stock = task.location_dest_id and task.location_dest_id.name or 'ONHOLD'
        partner_ref = task.partner_id and task.partner_id.ref or False

        for line in lines:
            if line.state == 'onhold' and line.real_qty:

                delmarid = line.product_id and line.product_id.default_code or False
                size_postfix = move_obj.get_size_postfix(cr, uid, line.size_id)
                itemid = delmarid and (delmarid + '/' + size_postfix) or False
                description = line.product_id and line.product_id.description

                if not itemid:
                    continue

                items[itemid] = {
                    'itemid': itemid,
                    'line_id': line.id,
                    'description': description,
                    'delmarid': delmarid,
                    'size_postfix': size_postfix,
                }

        if items.keys():
            sql = """   SELECT itemid, sum(adjusted_qty) as qty
                        FROM dbo.transactions
                        WHERE 1=1
                            AND itemid in ('%s')
                            AND warehouse = '%s'
                            AND stockid = '%s'
                            AND status <> 'Posted'
                        GROUP BY itemid;
            """ % ("','".join(items.keys()), warehouse, onhold_stock)
            res = server_obj.make_query(cr, uid, server_id, sql, select=True, commit=False)
            if res:
                for row in res:
                    available_onhold[items.get(row[0])['line_id']] = int(row[1] or 0)

        else:
            logger.debug("Have no items to revert")

        for line in lines:

            cur_item = None
            for item, data in items.items():
                if data.get('line_id', False) == line.id:
                    cur_item = data
                    break

            if not cur_item:
                continue

            cur_itemid = cur_item.get('itemid', False)
            delmarid = cur_item.get('delmarid', False)
            size_postfix = cur_item.get('size_postfix', False)

            real_qty = line.real_qty
            reverted_qty = 0
            if not real_qty:
                logger.debug("Nothing has been reserved")
                response['line_ids'].append(line.id)
                continue

            active_qty = active_onhold.get(line.id, 0)
            available_qty = available_onhold.get(line.id, 0)

            if available_qty <= 0:
                logger.debug("Not available items")
                # done.append(line.id)
                # continue

            qty_to_revert = available_qty - active_qty
            if qty_to_revert <= 0:
                logger.debug("Item is essential for active tasks")
                # done.append(line.id)
                # continue

            for loc in line.loc_ids:
                # if qty_to_revert <= 0:
                #     break

                if not loc.qty or loc.qty <= 0:
                    continue

                desired_qty = qty_to_revert if qty_to_revert < loc.qty else loc.qty
                real_qty = 0

                sql = """   SELECT TOP 1
                                itemid,
                                coalesce(bin, '') as bin,
                                sum(adjusted_qty) as qty
                            FROM dbo.transactions
                            WHERE 1=1
                                AND itemid = %(itemid)s
                                AND warehouse = %(warehouse)s
                                AND stockid = %(stockid)s
                            {real_bin}  AND bin = %(bin)s
                                AND status <> 'Posted'
                            GROUP BY itemid, coalesce(bin, '')
                            ORDER BY qty DESC;
                """.format(real_bin='' if loc.bin else '--')
                params = {
                    'itemid': cur_itemid,
                    'warehouse': warehouse,
                    'stockid': onhold_stock,
                }
                if loc.bin:
                    params['bin'] = loc.bin

                res = server_obj.make_query(cr, uid, server_id, sql, params=params, select=True, commit=False)
                if res:

                    onhold_invredid = move_obj.get_invredid_for_mssql_row(cr, uid, cur_itemid, warehouse, onhold_stock, loc.bin, ignore_bin=True)
                    onhold_reserved_flag = move_obj.get_reserved_flag(cr, uid, cur_itemid, warehouse, onhold_stock, server_id)

                    for row in res:
                        loc_bin = row[1] or ''
                        if not loc.bin and loc_bin:
                            loc.write({'bin': loc_bin})

                        qty_on_bin = int(row[2] or 0)

                        if not qty_on_bin or qty_on_bin < 0:
                            qty_on_bin = 0
                            # continue

                        if desired_qty < qty_on_bin:
                            qty_on_bin = desired_qty

                        if qty_on_bin and qty_on_bin > 0:
                            # From ONHOLD
                            sign = -1
                            params = (
                                cur_itemid or 'null',  # itemid
                                'ADJ',  # TransactionCode
                                qty_on_bin,  # qty
                                warehouse or 'null',  # warehouse
                                description or 'null',  # itemdescription
                                loc_bin,  # bin
                                onhold_stock,  # stockid
                                'TRS',  # TransactionType
                                delmarid,  # id_delmar
                                sign,  # sign
                                (qty_on_bin * sign) or '0',  # adjusted_qty
                                size_postfix,  # size
                                onhold_invredid,  # move.invredid or 'null',  # invredid
                                partner_ref,  # CustomerID
                                delmarid,  # ProductCode
                                invoice_no or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
                                onhold_reserved_flag or 0,  # reserved
                                None,  # trans_tag
                                0,  # unit price
                            )
                            success_get = bool(move_obj.write_into_transactions_table(cr, uid, server_id, params))
                            if not success_get:
                                logger.debug("Can't revert from %s %s " % (onhold_stock, loc.bin))
                                raise Exception()

                            # To Source
                            sign = 1
                            source_stock = loc.stock
                            source_invredid = move_obj.get_invredid_for_mssql_row(cr, uid, cur_itemid, warehouse, source_stock, loc.bin, ignore_bin=True)
                            source_reserved_flag = move_obj.get_reserved_flag(cr, uid, cur_itemid, warehouse, source_stock, server_id)

                            params = (
                                cur_itemid or 'null',  # itemid
                                'ADJ',  # TransactionCode
                                qty_on_bin,  # qty
                                warehouse,  # warehouse
                                description or 'null',  # itemdescription
                                loc_bin,  # bin
                                source_stock,  # stockid
                                'TRS',  # TransactionType
                                delmarid,  # id_delmar
                                sign,  # sign
                                (qty_on_bin * sign) or '0',  # adjusted_qty
                                size_postfix,  # size
                                source_invredid,  # invredid
                                partner_ref,  # CustomerID
                                delmarid,  # ProductCode
                                invoice_no or 'no_invoiceno',  # 'ADJDEF',  # InvoiceNo
                                source_reserved_flag or 0,  # reserved
                                None,  # trans_tag
                                0,  # unit price
                            )
                            success_put = bool(move_obj.write_into_transactions_table(cr, uid, server_id, params))
                            if not success_put:
                                logger.debug("Can't put on %s %s " % (source_stock, loc.bin))
                                raise Exception()

                            reverted_itemids.add(cur_itemid)

                        desired_qty = desired_qty - qty_on_bin
                        real_qty = real_qty + qty_on_bin

                reverted_qty += real_qty
                line_location_obj.write(cr, uid, loc.id, {'revert_qty': real_qty})

            self.write(cr, uid, line.id, {
                'revert_qty': reverted_qty,
                'state': 'done',
            })

            response['line_ids'].append(line.id)

        # Check bin conflicts
        if reverted_itemids:
            conflicts_msg = self.check_multi_bins(
                cr, uid,
                itemids=list(reverted_itemids),
                warehouse=warehouse,
                autorelease=True,
                onhold=onhold,
                variants='all',
                mode='conflicts_only',
                context=None
                )
            if conflicts_msg:
                response['conflicts'] = conflicts_msg

        return response

    def get_onhand(self, cr, uid, ids, task=None, context=None):

        result = {}
        todo = {}

        if not ids:
            return result

        server_obj = self.pool.get('fetchdb.server')
        move_obj = self.pool.get('stock.move')

        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(_('Warning!'), 'Can not onhand qty. Server with TRANSACTIONS table is not available!')

        lines = self.browse(cr, uid, ids)
        for line in lines:
            cur_task = task or line.task_id
            if not todo.get(cur_task.id, False):
                todo[cur_task.id] = {
                    'task': cur_task,
                    'items': {},
                }

            result[line.id] = 0
            delmarid = line.product_id and line.product_id.default_code or False
            size_postfix = move_obj.get_size_postfix(cr, uid, line.size_id)
            itemid = delmarid and (delmarid + '/' + size_postfix) or False

            if itemid:
                todo[cur_task.id]['items'][itemid] = line.id

        for task_id in todo:
            task = todo[task_id]['task']

            if not task.warehouse_id:
                continue

            warehouse = task.warehouse_id and task.warehouse_id.name or False
            stockid = task.location_id and task.location_id.name or False
            stockid_where = "" if not stockid else " AND stockid = '%s' " % (stockid)

            onhold_stockids = ['ONHOLD']
            if task.location_dest_id:
                onhold_stockids.append(task.location_dest_id.name)

            # logger.debug(todo[task_id])
            items = todo[task_id]['items'].keys()
            if items:
                items_str = "','".join(items)

                sql = """SELECT a.itemid, SUM(a.qty) as qty
                        FROM (
                            SELECT
                                SUM(adjusted_qty) AS qty,
                                itemid as itemid,
                                warehouse,
                                stockid
                            FROM dbo.TRANSACTIONS
                            WHERE   itemid in ('%s')
                                AND warehouse = '%s'
                                AND stockid not in ('%s')
                                AND status <> 'Posted'
                                %s
                            GROUP BY itemid, warehouse, stockid
                            HAVING SUM(adjusted_qty) > 0
                        ) as a
                        GROUP BY a.itemid;
                """ % (items_str, warehouse, "','".join(onhold_stockids), stockid_where)
                res = server_obj.make_query(cr, uid, server_id, sql, select=True, commit=False)
                if res:
                    for row in res:
                        line_id = todo[task_id]['items'].get(row[0], False)
                        if line_id and row[1]:
                            result[line_id] = int(row[1])

        return result

stock_onhold_task_line()


class stock_onhold_task_line_location(osv.osv):

    _name = 'stock.onhold.task.line.location'

    _columns = {
        'stock': fields.char('Stoc', size=256, required=True,),
        'bin': fields.char('Bin From', size=256, required=False,),
        'qty': fields.integer('Qty', required=True,),
        'revert_qty': fields.integer('Revert Qty', required=False,),
        'line_id': fields.many2one('stock.onhold.task.line', string="Task line", required=True,),
    }

stock_onhold_task_line_location()
