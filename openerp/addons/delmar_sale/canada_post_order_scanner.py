# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _


class canada_post_order_scanner(osv.osv_memory):
    _name = "canada.post.order.scanner"
    _columns = {
        't_name': fields.char('Order#', size=256,),
    }

    def onchange_order_name(self, cr, uid, ids, context=None):

        picking_obj = self.pool.get('stock.picking')

        if not context.get('order_name', False):
            raise osv.except_osv(_('Error!'), _('Enter order name!'))

        name = context.get('order_name').strip().upper()
        sql = """   SELECT
                        sp.id as picking_id,
                        so.id as sale_id,
                        so."orderType" as order_type,
                        array_agg(ol.id) as line_ids,
                        so."processQueue" as process_queue
                    FROM stock_picking sp
                        LEFT JOIN sale_order so ON so.id = sp.sale_id
                        LEFT JOIN sale_order_line ol ON so.id = ol.order_id
                    WHERE upper(sp.name) = '%s'
                    GROUP BY sp.id, so.id, so."orderType", so."processQueue";
        """ % name
        cr.execute(sql)
        packing = cr.dictfetchone()

        if not packing:
            raise osv.except_osv(_('Error!'), _('Order {} not found!'.format(name)))

        pick_id = packing.get('picking_id', False)

        order = picking_obj.browse(cr, uid, pick_id, context=context)
        if order.state == 'picking':
            import netsvc
            from datetime import datetime
            now = datetime.now()
            #date = now.strftime('%Y%m%d')
            date = now.strftime('%Y%m%d_%H%M%S')
            shp_date = now.strftime('%Y-%m-%d %H:%M:%S')
            user = self.pool.get('res.users').browse(cr, uid, uid).login
            tracking_ref = 'Canada_Post_{}'.format(date)
            picking_obj.write(cr, uid, [pick_id], {'tracking_ref': tracking_ref,
                                                   'shp_date': shp_date,
                                                   #'carrier_code': order.carrier_code or order.sale_id.carrier,
                                                   'carrier_code': 'LM',
                                                   }
                              )
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'stock.picking', pick_id, 'ship_confirm', cr)

        elif order.state not in ('shipped', 'done', 'outcode_except', 'bad_format_tracking', 'picking'):
            raise osv.except_osv(_('Error!'), _('Order can\'t be processes'))

        datas = {
            'id': pick_id,
            'ids': [pick_id],
            'model': 'stock.picking',
            'report_type': 'pdf',
            }
        act_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'canada.post.order.labels',
            'datas': datas,
            'context': {'active_ids': [pick_id], 'active_id': pick_id}
        }
        return act_print

canada_post_order_scanner()
