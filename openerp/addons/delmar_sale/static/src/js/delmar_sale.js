openerp.delmar_sale = function (openerp)
{
    $(document).ready(function() {
    	//DLMR-181 start
        var checkExist = setInterval(function() {
                 var arr = ['Search: Multi Address'] //If required this feature for more screen, (as per discussion with Avi) then just add screen text in array.
                 if ($('.oe_searchable_view').length) {
                        $(".oe-listview > .oe-listview-content > tbody > tr").live('hover', function(e){
                            if(arr.includes($('.oe_view_title_text')[0].textContent) && $(this).parent().hasClass("ui-widget-content") == false){
                                  if (e.type == 'mouseenter') {
                                        $(this).css("background-color", "#A8A8A8");
                                        $(this).find('td').css({'border-left':'#A8A8A8'});
                                        $(this).find('td').css({'border-bottom':'#A8A8A8'});
                                    }
                                  if (e.type == 'mouseleave') {
                                        $(this).css("background-color", "");
                                        $(this).find('td').css({'border-left':''});
                                        $(this).find('td').css({'border-bottom':''});
                                  }
                              }
                    });
                  clearInterval(checkExist);
               }
            }, 100);
            //DLMR-181 end
        // Initialise the table
        $(".oe-listview-content").live('mouseenter', function(){
            if ($("span.oe_view_title_text").text() == "Customers" && $(this).find("button:contains('Add')").length != 0 && $(this).find("th:contains('Location Name')")){
                $(this).children('tbody').tableDnD();
                $(this).children('tbody').bind('mouseleave', function(){
                    ids_str = "";
                    $.each($(this).find('tr[data-id]'), function(i, v){
                        ids_str += ',' + v.getAttribute('data-id');
                    });
                    ids_str = ids_str.substr(1);


                    label = $('#' + $('li.ui-state-active a').attr('href').replace('#','')).find("label:contains('order ids')")
                    $(label).parent().next().children().val(ids_str);
                    $(label).parent().next().children().change();
                });
            }
        });
    });

    openerp.web.form.widgets.add('track_number', 'openerp.delmar_sale.char_with_confirm');
    openerp.delmar_sale.char_with_confirm = openerp.web.form.FieldChar.extend({

        init: function(view, node) {
            this._super(view, node);
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);

            this.$input = this.$element.find("input");
            this.$input.keyup(function(e) {
                if(e.which === 13) {
                    self.on_enter_pressed();
                }
                isSelecting = false;
            });
        },
        on_enter_pressed: function() {
            var self = this;
            this.validate();
            if(this.is_valid()) {
                $("button span:contains('Done')").parent().click();
                $("button span:contains('Scan')").parent().click();
                $(".field_track_number").focus();
            } else {
                this.update_dom(true);
            }
        }
    });

    openerp.web.ActionManager = openerp.web.ActionManager.extend({
        ir_actions_act_close_wizard_and_reload_view: function (action, options) {
            if (!this.dialog) {
                options.on_close();
            }
            var controller = this.inner_viewmanager.views[this.inner_viewmanager.active_view].controller;
            controller.reload_content();
            this.dialog_stop();
            return $.when();
        },
        ir_actions_act_reload_list_view: function (action, options) {
            var controller = this.inner_viewmanager.views[this.inner_viewmanager.active_view].controller;
            controller.reload_content();
            return $.when();
        },
    });


}
