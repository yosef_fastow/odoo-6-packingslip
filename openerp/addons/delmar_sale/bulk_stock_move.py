from osv import fields, osv
from pf_utils.utils.label_printer import LabelWizPrinter
import logging

logger = logging.getLogger('delmar_sale')


class bulk_stock_move(osv.osv_memory):
    _name = "bulk.stock.move"

    _columns = {
        'location_from_id': fields.many2one(
            'stock.location',
            'Source Location',
            required=True,
        ),
        'location_to_id': fields.many2one(
            'stock.location',
            'Target Location',
            required=True,
        ),
        'src_bin': fields.char('Source Bin', size=50),
        'auto_src_bin': fields.boolean('Choose automatically', required=False),
        'bin_to': fields.char('Target Bin', size=50),
        'autocreate_bin': fields.boolean('Choose automatically', required=False),
        'all_stock': fields.boolean('Add All Stock Items'),
        'tag_id': fields.many2one('tagging.tags', 'Tag'),
        'add_item': fields.char('Add Item', size=50),
        'line_ids': fields.one2many('bulk.stock.move.line', 'bulk_id', 'Lines'),
        'size_id': fields.many2one('ring.size', 'Size'),
    }

    _defaults = {
        'auto_src_bin': False,
        'autocreate_bin': False
    }

    def create(self, cr, uid, vals, context=None):
        new_line_ids = []
        if 'line_ids' in vals:
            for line in vals['line_ids']:
                # (0, 0, {vals}) - insert new record
                # (1, ID, {vals}) - update ID without link
                # (2, ID) - remove ID
                # (3, ID) - un-link ID
                # (4, ID) - link ID
                # (5) - un-link all linked ids
                # (6, 0, [IDs]) - replace all linked ids with IDs
                if line[0] == 1:
                    # Fix a bug with link loss
                    new_line_ids.append([4, line[1], False])
                new_line_ids.append(line)
            vals['line_ids'] = new_line_ids
        return super(bulk_stock_move, self).create(cr, uid, vals,
                                                   context=context)

    def add_stock(
            self, cr, uid, ids, loc_from_id, loc_to_id, all_stock, tag_id,
            src_bin, auto_src_bin, autocreate_bin, bin_to, context=None):
        if not tag_id and not all_stock:
            return {'value': {'line_ids': []}}
        products = None
        if tag_id:
            tag = self.pool.get('tagging.tags').browse(cr, uid, tag_id)
            products = [product.default_code for product in tag.product_ids]
        line_ids = self._create_lines(cr, uid, loc_from_id, loc_to_id, products, src_bin, auto_src_bin,
                                      autocreate_bin, bin_to)
        return {'value': {'line_ids': line_ids}}

    def _create_lines(self, cr, uid, loc_from_id, loc_to_id, ids_delmar, src_bin, auto_src_bin, autocreate_bin, bin_to):
        loc_obj = self.pool.get('stock.location')
        line_obj = self.pool.get('bulk.stock.move.line')
        warehouse, stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, loc_from_id
        )

        if not loc_from_id:
            raise osv.except_osv('Error!', '"Location From" not selected.')
        if not warehouse:
            raise osv.except_osv('Error!', '"Location From" has no warehouse.')
        new_warehouse, new_stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, loc_to_id
        )

        if not loc_to_id:
            raise osv.except_osv('Error!', '"Location To" not selected.')
        if not new_warehouse:
            raise osv.except_osv('Error!', '"Location To" has no warehouse.')
        vals_list = self.get_ms_qty(cr, uid, warehouse, stockid, ids_delmar, bin=src_bin)
        vals_to = {}
        if autocreate_bin:
            vals_to = self.get_ms_qty(cr, uid, new_warehouse, new_stockid, ids_delmar, bin=False, zero_stock=True)
            vals_to = {(v['product'], v['size']): v['bin'] for v in vals_to}

        line_ids = []
        for vals in vals_list:
            prod_and_size = (vals['product'], vals['size'])
            vals['bin_to'] = vals_to.get(prod_and_size, bin_to or '')
            line_id = line_obj.create(cr, uid, vals)
            line_ids.append(line_id)
        return line_ids

    def _add_component(self, cr, uid, bulk, component):
        id_delmar = component['default_code']
        size = component['size']
        qty = component['qty']
        loc_obj = self.pool.get('stock.location')
        warehouse, stock = loc_obj.get_warehouse_and_location_names(
            cr, uid, bulk.location_from_id.id
        )
        # source bin where to take item
        src_bin = False
        if bulk.src_bin and not bulk.auto_src_bin:
            src_bin = bulk.src_bin

        qty_rows = self.get_ms_qty(cr, uid, warehouse=warehouse, stock=stock, ids_delmar=[id_delmar], size=size, bin=src_bin)
        if not qty_rows:
            error_msg = '{id_delmar}/{size} not found in {wh}/{stock}'.format(
                id_delmar=id_delmar,
                size=size,
                wh=warehouse,
                stock=stock,
            )
            raise osv.except_osv('Error!', error_msg)
        new_warehouse, new_stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, bulk.location_to_id.id
        )

        # target bin for item
        bin_to = bulk.bin_to or ''
        if bulk.autocreate_bin:
            loc_to_data = self.get_ms_qty(cr, uid, new_warehouse, new_stockid,
                                          [id_delmar], size, zero_stock=True)
            if loc_to_data:
                bin_to = loc_to_data[0]['bin']
        for qty_row in qty_rows:
            row_has_line = False
            if not qty_row['bin']:
                qty_row['bin'] = ''
            ms_tuple = (id_delmar, size, qty_row['bin'])
            for line in bulk.line_ids:
                if (line.product, line.size, line.bin) == ms_tuple:
                    if qty_row['qty'] > line.qty:
                        new_qty = min(line.qty + qty, qty_row['qty'])
                        self.write(cr, uid, [bulk.id], {
                            # (1, ID, {vals}) - update ID without link
                            'line_ids': [(1, line.id, {'qty': new_qty})],
                            'add_item': False,
                        })
                        return True
                    else:
                        row_has_line = True
                        break
            if not row_has_line:
                vals = qty_row.copy()
                vals['qty'] = min(qty, vals['qty'])
                vals['bin_to'] = bin_to
                self.write(cr, uid, [bulk.id], {
                    # (0, 0, {vals}) - insert new record
                    'line_ids': [(0, 0, vals)],
                    'add_item': False,
                })
                return True
        return False

    def add_item(self, cr, uid, ids, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        bulk = self.browse(cr, uid, ids[0])
        if not bulk.add_item:
            raise osv.except_osv('Error!', "Nothing to Scan!")
        size_obj = self.pool.get('ring.size')
        product_obj = self.pool.get('product.product')
        id_delmar, size = size_obj.split_itemid_to_id_delmar_size(cr, uid,
                                                                  bulk.add_item)
        if bulk.size_id.id and size == '0000':
            size = size_obj.convert_to_4digit_name(cr, uid,bulk.size_id.name)

        product_ids = product_obj.search(cr, uid, [
            ('default_code', '=', id_delmar),
            ('type', '!=', 'consu')
        ])
        if not product_ids:
            raise osv.except_osv(
                'Error!',
                "Not found product {0}!".format(id_delmar),
            )
        prod_id = product_ids[0]
        components_data = product_obj.split_sets_to_components(cr, uid, prod_id,
                                                               size)
        res = False
        for component in components_data:
            res |= self._add_component(cr, uid, bulk, component)
        if not res:
            error_msg = 'All possible pieces {0}/{1} are already added'
            raise osv.except_osv('Error!', error_msg.format(id_delmar, size))

        return {'value': {'add_item': bulk.add_item}}

    def get_ms_qty(
            self, cr, uid, warehouse, stock, ids_delmar, size=None,
            zero_stock=False, bin=False):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv('Error!', 'Could not connect to MS Database.')
        if ids_delmar:
            ids_delmar_str = "', '".join(ids_delmar)
            and_id_delmar = "AND id_delmar in ('{}')".format(ids_delmar_str)
        else:
            and_id_delmar = ''
        and_size = '' if size is None else "AND size = '{}'".format(size)
        and_bin = ''
        if bin:
            and_bin = "AND bin = '{}'".format(bin)
        having = '' if zero_stock else 'HAVING sum(adjusted_qty) > 0'
        params = {
            'warehouse': warehouse,
            'stockid': stock,
        }
        get_stock_sql = """SELECT
                id_delmar,
                size,
                coalesce(bin, ''),
                sum(adjusted_qty)
            FROM transactions
            WHERE warehouse = %(warehouse)s
                AND stockid = %(stockid)s
                AND status <> 'posted'
                {and_id_delmar}
                {and_size}
                {and_bin}
            GROUP BY id_delmar, size, coalesce(bin, '')
            {having}
            ORDER BY id_delmar, size, coalesce(bin, '')
        """.format(
            and_id_delmar=and_id_delmar,
            and_size=and_size,
            and_bin=and_bin,
            having=having,
        )
        query_result = server_data.make_query(
            cr, uid, server_id,
            get_stock_sql, params=params,
            select=True, commit=False,
        )
        result = [{
            'product': row[0],
            'size': row[1],
            'bin': row[2],
            'qty': row[3],
        } for row in query_result]
        return result

    def _get_bin_to(self, cr, uid, line, prod_ids, warehouse, loc_id, stockid):
        if line.bin_to:
            return line.bin_to
        bulk = line.bulk_id
        if not bulk.autocreate_bin:
            return bulk.bin_to or ''
        itemid = line.product + '/' + line.size
        bin_obj = self.pool.get('stock.bin')
        existing_bins = bin_obj.get_mssql_bin_for_product(cr, uid, itemid,
                                                          warehouse, stockid)
        if itemid in existing_bins:
            return existing_bins[itemid] or ''
        elif prod_ids:
            sb_create_obj = self.pool.get('stock.bin.create')
            return sb_create_obj.create_bin(cr, uid, loc_id, stockid,
                                            prod_ids[0], line.size, line.qty)
        return ''

    def move_stock(self, cr, uid, ids, context=None):

        b2b_obj = self.pool.get('stock.product.move')
        loc_obj = self.pool.get('stock.location')
        prod_obj = self.pool.get('product.product')
        line_obj = self.pool.get('bulk.stock.move.line')

        if isinstance(ids, (int, long)):
            ids = [ids]
        bulk = self.browse(cr, uid, ids[0])
        warehouse, stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, bulk.location_from_id.id
        )
        loc_to_id = bulk.location_to_id.id
        new_warehouse, new_stockid = loc_obj.get_warehouse_and_location_names(
            cr, uid, loc_to_id
        )
        if not new_warehouse:
            raise osv.except_osv('Error!', '"Location To" has no warehouse.')
        from_data = {
            'warehouse': warehouse,
            'stock': stockid,
        }
        to_data = {
            'warehouse': new_warehouse,
            'stock': new_stockid,
        }
        for line in bulk.line_ids:
            product = line.product
            from_data['bin_name'] = line.bin or ''
            prod_args = [
                ('default_code', '=', product),
                ('type', '!=', 'consu'),
            ]
            prod_ids = prod_obj.search(cr, uid, prod_args)
            desc = None
            if prod_ids:
                desc = prod_obj.read(cr, uid, prod_ids[0], ['name']).get('name')
            to_data['bin_name'] = self._get_bin_to(cr, uid, line, prod_ids,
                                                   new_warehouse, loc_to_id,
                                                   new_stockid)
            try:
                move_params_from, move_params_to = b2b_obj.do_bin_to_bin_move(
                    cr, uid, from_data, to_data, product, line.size, line.qty,
                    desc, reason='Bulk Stock Move', allow_partial=True
                )
                moved_qty = move_params_from[2]
            except Exception, e:
                moved_qty = 0
                status = 'Failed! ' + str(e)
            else:
                status = line_obj.get_status(line.qty, moved_qty)
            line_args = {
                'status': status,
                'moved_qty': moved_qty,
                'bin_to': to_data['bin_name'],
            }
            line_obj.write(cr, uid, line.id, line_args)
        return True

bulk_stock_move()


class bulk_stock_move_line(osv.osv_memory):
    _name = "bulk.stock.move.line"

    _columns = {
        'bulk_id': fields.many2one('bulk.stock.move', 'Parent Bulk'),
        'product': fields.char('Product', size=500),
        'size': fields.char('Size', size=4),
        'qty': fields.integer('Qty'),
        'moved_qty': fields.integer('Moved Qty'),
        'status': fields.char('Status', size=100),
        'bin': fields.char('Bin From', size=50),
        'bin_to': fields.char('Bin To', size=50),
    }

    _defaults = {
        'moved_qty': 0,
        'status': 'Draft',
    }

    def get_status(self, qty, moved_qty):
        if qty <= 0:
            status = 'Not Valid Qty'
        elif moved_qty == 0:
            status = 'Failed'
        elif moved_qty == qty:
            status = 'Moved'
        else:
            status = 'Partially Moved'
        return status

    def print_sr_label_wiz(self, cr, uid, ids, context=None):
        line = self.browse(cr, uid, ids[0])
        prod_obj = self.pool.get('product.product')
        prod_args = [
            ('default_code', '=', line.product),
            ('type', '!=', 'consu'),
        ]
        prod_ids = prod_obj.search(cr, uid, prod_args)
        if not prod_ids:
            raise osv.except_osv(
                'Error!',
                "Not found product {0}!".format(line.product),
            )
        product_id = prod_ids[0]
        size_obj = self.pool.get('ring.size')
        size_ids = size_obj.search(cr, uid, [('name', '=', line.size)], limit=1)
        size_id = size_ids[0] if size_ids else ''
        template = 'ShowRoomLabel'
        label_printer = LabelWizPrinter(self.pool, cr, uid)
        return label_printer.print_label(product_id, size_id, template)

bulk_stock_move_line()
