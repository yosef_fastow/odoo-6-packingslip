# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
import logging
import re
from tools import DEFAULT_SERVER_DATE_FORMAT, float_compare
import time
import pytz
from pytz import timezone
import decimal_precision as dp
import netsvc
from datetime import datetime, timedelta
import base64
from tools.translate import _
import hashlib


logger = logging.getLogger('sale')

SALE_ORDER_STATES = [
    ('draft', 'Quotation'),
    ('waiting_date', 'Waiting Schedule'),
    ('manual', 'To Invoice'),
    ('progress', 'In Progress'),
    ('address_except', 'Invalid address'),
    ('shipping_except', 'Shipping Exception'),
    ('invoice_except', 'Invoice Exception'),
    ('done', 'Done'),
    ('cancel', 'Cancelled'),
    ('rejected', 'Rejected'),
    ('returned_onfly', 'Returned For Non Existing Order'),
    ]

# Transactioncode field in ms transactions table
SIGN_CANCEL = 2
SIGN_DEFAULT = -1
SIGN_CANCEL_VALUE = 'CXL'
SIGN_DEFAULT_VALUE = 'SLE'
SIGN_RETURN_VALUE = 'RET'
SIGN_ADJUSTMENT_VALUE = 'ADJ'


class sale_order(osv.osv):
    _inherit = "sale.order"
    _description = "Delmar Sales Order"

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({
            'name': False,
            'state': 'draft',
            'shipped': False,
            'invoice_ids': [],
            'picking_ids': [],
            'date_confirm': False,
            'default_name': self.pool.get('ir.sequence').get(cr, uid, 'sale.order'),
        })
        return super(sale_order, self).copy(cr, uid, id, default, context=context)

    def search_by_delmar_id(self, cr, user, args, context=None):
        if not context:
            context = {}
        try:
            delmar_id_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'delmar_id_search')).next()
            delmar_id_index = args.index(delmar_id_arg)
            delmar_id = delmar_id_arg[2]
            if delmar_id:
                cr.execute("""  SELECT DISTINCT so.id
                                FROM product_product pp
                                    INNER JOIN sale_order_line sol on sol.product_id = pp.id
                                    INNER JOIN sale_order so on sol.order_id = so.id
                                WHERE pp.default_code = %s
                                    AND so.flash = True
                    """, (delmar_id,))
                search_res = cr.fetchall()
                search_ids = [res[0] for res in search_res]
                args[delmar_id_index] = ['id', 'in', search_ids]
            else:
                del args[delmar_id_index]
            return args
        except StopIteration:
            return args

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}

        if ('flash_view' in context) and (len(args) == 2):
            return []

        args = self.search_by_delmar_id(cr, user, args, context=context)
        return super(sale_order, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def _invoiced(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id] = sale.delmar_invoiced

        return res

    def _invoiced_search(self, cursor, user, obj, name, args, context=None):
        return super(sale_order, self)._invoiced_search(cursor, user, obj, name, args, context=context)

    def _get_search_lines_ids(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for order in self.browse(cr, uid, ids, context=context):
                line_ids = self.get_search_lines_ids(
                    cr, uid, order.id,
                    warehouse_search=order.warehouse_search and order.warehouse_search.id or False,
                    delmar_id_search=order.delmar_id_search or False,
                    line_state_search=order.line_state_search or False,
                    line_used_search=order.line_used_search or False,
                    context=context,
                    )
                res.update(line_ids)
        return res

    def _get_flash_not_ready_count(self, cr, uid, ids, name, args, context=None):
        res = self.get_flash_not_ready_count(cr, uid, ids, context=context)
        return res

    def _get_flash_not_ready_items(self, cr, uid, ids, name, args, context=None):
        res = self.get_flash_not_ready_items(cr, uid, ids, context=context)
        return res

    def _save_method(self, cr, uid, ids, field_name, field_value, arg, context):
        pass

    def _amount_flash(self, cr, uid, ids, field_name, arg, context=None):
        def __calc_left(order):
            order_states = ('assigned', 'picking', 'shipped', 'done', 'waiting_split', 'address_except', 'incode_except', 'outcode_except', 'bad_format_tracking',)
            order_line_states = ('assigned', 'done',)
            sql_query = """SELECT sum(sm.product_qty * sol.price_unit) left_sum
                           FROM sale_order_line sol
                           INNER JOIN stock_move sm ON sol.id = sm.sale_line_id
                           INNER JOIN stock_picking sp ON sp.id = sm.picking_id
                           WHERE sol.order_id =
                                ( SELECT id
                                 FROM sale_order
                                 WHERE "name" = '%s' )
                              AND sm.state IN %s
                              AND sp.state IN %s
                              AND sp.manual_done_approved = FALSE;
            """ % (order.name, order_line_states, order_states,)
            cr.execute(sql_query)
            result = cr.fetchone()
            return result and result[0] or 0.0

        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_requested': 0.0,
                'amount_sent': 0.0,
                'amount_left': 0.0,
            }
            val_requested = 0.0
            val_sent = 0.0
            val_left = __calc_left(order)
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val_requested += line.price_unit * line.product_order_qty
                val_sent += line.price_unit * line.product_uom_qty
            res[order.id]['amount_requested'] = cur_obj.round(cr, uid, cur, val_requested)
            res[order.id]['amount_sent'] = cur_obj.round(cr, uid, cur, val_sent)
            res[order.id]['amount_left'] = cur_obj.round(cr, uid, cur, val_left)
        return res

    def _get_website(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for sale in self.browse(cr, uid, ids, context=context):
            if sale.partner_id and sale.partner_id.name == 'Amazon Retail':
                res[sale.id] = 'https://vendorcentral.amazon.com/st/vendor/members/po-mgmt/order?orderId=' + sale.external_customer_order_id
        return res

    def _get_so_shp_handling(self, cr, uid, ids, name, args, context=None):
        res = {}

        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]

            res = {x: 0 for x in ids}

            cr.execute("""
                select
                    ol.order_id as id,
                    sum(CASE WHEN ol."shipCost" ~ '^\s*[\d\.]+\s*$' THEN ol."shipCost"::float ELSE 0.0 END) as shp
                from sale_order_line ol
                where order_id in %s
                group by ol.order_id
            """, (tuple(ids), )
            )
            res.update(dict(cr.fetchall() or []))
        return res


    _columns = {
        'name': fields.char('Order Reference', size=64, required=False,
            readonly=True, states={'draft': [('readonly', False)]}, select=True),
        'default_name': fields.char('Derfault Order Reference', size=64, required=True,
            readonly=True, states={'draft': [('readonly', False)]}, select=True),
        'state': fields.selection(SALE_ORDER_STATES, 'Order State', readonly=True,
            help="Gives the state of the quotation or sales order. \nThe exception state is automatically set when a cancel operation occurs in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception). \nThe 'Waiting Schedule' state is set when the invoice is confirmed but waiting for the scheduler to run on the order date."
            ),
        'tracking_ref': fields.char('Tracking Ref', size=256),
        'picking_policy': fields.selection([
            ('direct', 'Deliver each product when available'),
            ('one', 'Deliver all products at once')],
            'Picking Policy',
            required=True, readonly=True,
            help="""If you don't have enough stock available to deliver all at once, do you accept partial shipments or not?"""
        ),
        'ship2store': fields.boolean('Ship To Store'),
        'delmar_invoiced': fields.boolean('Delmar Invoiced Status'),
        'address_approved': fields.boolean('Address rechecked'),
        'partner_shipping_id': fields.many2one('res.partner.address', 'Shipping Address', readonly=True, required=True, states={'draft': [('readonly', False)], 'address_except': [('readonly', False)]}, help="Shipping address for current sales order."),
        'invoiced': fields.function(_invoiced, string='Paid',
            fnct_search=_invoiced_search, type='boolean', help="It indicates that an invoice has been paid."),
        'wkf_log_ids': fields.one2many('sale.order.workflow.log', 'res_id', 'Workflow Logs'),
        'report_history': fields.text('History'),
        'order_line': fields.one2many('sale.order.line', 'order_id', 'Order Lines', readonly=False),
        'set_parent_order_line': fields.one2many('sale.order.line', 'set_parent_order_id', 'set parent Order Lines', readonly=False),
        'check_sync_product_last_time': fields.float('Last Product Synchronize'),
        'shp_date': fields.datetime('Shipping Date', readonly=True),  # Flash
        'warehouse_search': fields.many2one('stock.warehouse', 'Warehouse'),  # Flash
        'delmar_id_search': fields.char('Delmar ID', size=256,),  # Flash
        'line_state_search': fields.selection([
            ('all', 'All'),
            ('allocated', 'Allocated'),
            ('waiting', 'Waiting availability'),
            ('waiting_positive', 'Waiting availability with positive qty'),
            ], 'Line State'),  # Flash
        'line_used_search': fields.selection([
            ('all', 'All'),
            ('used', 'Picked'),
            ('not_used', 'Not picked'),
            ], 'Picked/Not picked'),  # Flash
        'search_btn': fields.boolean('Search'),  # Flash
        'search_lines_ids': fields.function(
            _get_search_lines_ids,
            string='Search',
            type='many2many',
            fnct_inv=_save_method,
            relation="sale.order.line",
            method=True,
            ),  # Flash
        'mark_use': fields.boolean('Mark All'),  # Flash
        'flash_processed': fields.boolean('Flash Processed'),  # Flash
        'flash_delivery_processed': fields.boolean('Flash Delivery Processed'),  # Flash
        'flash_not_ready_count': fields.function(
            _get_flash_not_ready_count,
            type='integer',
            string='Count of not ready to process lines',
            method=True,
            ),  # Flash
        'flash_not_ready_items': fields.function(
            _get_flash_not_ready_items,
            type='text',
            string='Problematic items',
            method=True,
            ),  # Flash
        'holder_field': fields.many2one('sale.order.line', 'Save field'),  # Flash
        'amount_requested': fields.function(_amount_flash, digits_compute=dp.get_precision('Sale Price'), string='$ Requested', store=False, multi='sums', help="The requested amount."),
        'amount_sent': fields.function(_amount_flash, digits_compute=dp.get_precision('Sale Price'), string='$ Sent', store=False, multi='sums', help="The sent amount."),
        'amount_left': fields.function(_amount_flash, digits_compute=dp.get_precision('Sale Price'), string='$ Left', store=False, multi='sums', help="The left amount."),
        'website_flash': fields.function(_get_website, string='Website', type='char', size=512, store=False),
        'shp_handling': fields.function(
            _get_so_shp_handling,
            string="Shipping Handling",
            method=True, type="float", store=False,
            help="Shipping handling calculated based on Shipping Cost in sale order lines",
        ),
        'is_set': fields.boolean('is Set', readonly=True),
    }

    _defaults = {
        'name': False,
        'default_name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'sale.order'),
        'picking_policy': 'one',
        'ship2store': False,
        'delmar_invoiced': False,
        'invalid_addr': True,
        'address_approved': False,
        'flash_processed': False,
        'is_set': False
    }

    _reset = [
        'mark_use',
    ]

    _order = "date_order desc"

    def create(self, cr, uid, vals, context=None):
        name = vals.get('name', False)
        if not name:
            partner_ref = ''
            original_order = ''
            partner_id = vals.get('partner_id', False)
            if partner_id:
                partner_ref = self.pool.get('res.partner').browse(cr, uid, partner_id).ref
            ext_id = vals.get('po_number', False)
            if vals.get('priorityEDI', '0') == '1':
                ext_id += 'SO'

            if vals.get('original_order_number', False):
                original_order = 'NEW' + vals['original_order_number'] + '_'

            name = "%s%s%s" % (partner_ref, original_order, ext_id)

            vals.update({'name': name})

        new_order = super(sale_order, self).create(cr, uid, vals, context=context)
        self.pool.get('order.history').create(cr, uid, {'type': 'sale_order', 'so_id': new_order, 'order_name': vals.get('name', False)})
        return new_order

    def write(self, cr, uid, ids, vals, context=None):

        if vals and self._reset:
            for r in self._reset:
                vals[r] = False
        saved_order = super(sale_order, self).write(cr, uid, ids, vals, context=context)
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]
            for order in self.browse(cr, uid, ids) or []:
                reconfirm_flag = False
                if order.flash:
                    if vals.get('order_line', False):
                        if 1 in [x[0] for x in vals['order_line']]:
                            reconfirm_flag = True
                if reconfirm_flag:
                    self.action_reconfirm_order_write(cr, uid, [order.id], context)
            if context and context.get('flash_view', False) and vals.get('tracking_ref', False):
                cr.execute(
                """update order_history set tracking_ref = %(t_r)s where parent_sale_order in %(ids)s """,
                {'t_r': vals['tracking_ref'], 'ids': tuple(ids), })

            if ids and vals.get('state', False):
                cr.execute("""
                    update order_history
                        set state = %(state)s
                    where so_id in %(ids)s
                        and type = 'sale_order'
                """, {
                    'state': 'so_' + vals['state'],
                    'ids': tuple(ids),
                })
        return saved_order

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        def_id = False
        res = super(sale_order, self).default_get(cr, uid, fields_list, context=context)
        if active_model == 'order.history' and active_ids and type(active_ids) in (tuple, list) and len(active_ids) == 1:
            if self.pool.get(active_model).search(cr, uid, [('id', 'in', active_ids)]):
                oh_obj = self.pool.get(active_model).browse(cr, uid, active_ids)[0]
                def_id = oh_obj.parent_sale_order and oh_obj.parent_sale_order.id or False
                context.update(default_parent_sale_order=def_id)
            elif context.get('default_parent_sale_order'):
                def_id = context.get('default_parent_sale_order')
            if def_id:
                order_fields = self.read(cr, uid, def_id)
                res.update(order_fields)

        if context.get('def_so_id', False) and not def_id:
            order_fields = self.read(cr, uid, context.get('def_so_id'))
            res.update(order_fields)

        return res

    def action_check_inventory(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        orders = self.read(cr, uid, ids, ['order_line'])
        conf_obj = self.pool.get('ir.config_parameter')
        try:
            limit_lines = conf_obj.get_param(cr, uid, 'limit_sync_order_lines')
            limit_lines = int(limit_lines)
            if limit_lines <= 0:
                limit_lines = None
        except:
            limit_lines = 10

        for order in orders:
            sale_lines_ids = order['order_line']
            if context.get('line_ids', False) and len(context['line_ids']) > 0:
                sale_lines_ids = context['line_ids']

            if limit_lines and len(sale_lines_ids) <= limit_lines:
                unsync_items = self.check_sync_product(cr, uid, sale_lines_ids) or []
                for unsync_item in unsync_items:
                    if unsync_item:
                        # split_unsync_item = unsync_item.split(':')
                        self.sync_product_stock(cr, uid, unsync_item['product_id'], unsync_item['size_id'], context=context)

        return True

    def action_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')
        proc_obj = self.pool.get('procurement.order')
        for sale in self.browse(cr, uid, ids, context=context):
            for pick in sale.picking_ids:
                if pick.state not in ('cancel', 'final_cancel'):
                    raise osv.except_osv(
                        _('Could not cancel sales order !'),
                        _('You must first cancel all picking attached to this sales order.'))
                if pick.state in ('cancel', 'final_cancel'):
                    for mov in pick.move_lines:
                        proc_ids = proc_obj.search(cr, uid, [('move_id', '=', mov.id)])
                        if proc_ids:
                            for proc in proc_ids:
                                wf_service.trg_validate(uid, 'procurement.order', proc, 'button_check', cr)
            for r in self.read(cr, uid, ids, ['picking_ids']):
                for pick in r['picking_ids']:
                    wf_service.trg_validate(uid, 'stock.picking', pick, 'button_cancel', cr)
            for inv in sale.invoice_ids:
                if inv.state not in ('cancel', 'final_cancel'):
                    raise osv.except_osv(
                        _('Could not cancel this sales order !'),
                        _('You must first cancel all invoices attached to this sales order.'))
            for r in self.read(cr, uid, ids, ['invoice_ids']):
                for inv in r['invoice_ids']:
                    wf_service.trg_validate(uid, 'account.invoice', inv, 'invoice_cancel', cr)
            sale_order_line_obj.write(
                cr, uid, [l.id for l in sale.order_line],
                {'state': 'cancel'}
            )
            message = _("The sales order '%s' has been cancelled.") % (sale.name,)
            self.log(cr, uid, sale.id, message)
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True

    def check_sync_product(self, cr, uid, ids):
        res = []
        if not ids:
            return res

        so_lines = self.pool.get('sale.order.line').browse(cr, uid, ids)
        if not so_lines:
            return res

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)
        if not server_id:
            return res

        itemid_array = []
        all_items = []
        for so_line in so_lines:
            if so_line:
                product_id = so_line and so_line.product_id and so_line.product_id.id or False
                if product_id:
                    # self.clear_transactions_for_reentered_order(cr, uid, so_line)
                    # self.sync_product_stock(cr, uid, product_id, size_id, context=context)
                    size_str = '0000'
                    size_id = so_line.size_id and so_line.size_id.id or False
                    if size_id:
                        size_str = re.sub('\.', '', "%05.2f" % float(so_line.size_id.name))
                    itemid_array.append("%s/%s" % (so_line.product_id.default_code, size_str))
                    item = {
                        'product_id': product_id,
                        'id_delmar': so_line.product_id.default_code,
                        'size_id': size_id or 0,
                        'size': size_str,
                    }
                    if item not in all_items:
                        all_items.append(item)
        if not all_items:
            return res
        mssql_inventory_sql = """SELECT t.id_delmar, t.size, warehouse, stockid, coalesce(bin, ''), cast(sum(adjusted_qty) as varchar) as qty
            from (values %s) as i (id_delmar, size)
            left join transactions t
                on t.ID_DELMAR = i.id_delmar
                and t.Size = i.size
            where 1=1
              and status <> 'Posted'
            group by t.id_delmar, t.size, warehouse, stockid, coalesce(bin, '')
        """ % (', '.join([u"('{id_delmar}', '{size}')".format(**item) for item in all_items]),)

        mssql_inventory = server_data.make_query(cr, uid, server_id, mssql_inventory_sql, select=True, commit=False)
        if not mssql_inventory: # all items were released
            items_values = ', '.join([u'({product_id}, {size_id})'.format(**item) for item in all_items])
            pg_inventory_sql = u"""SELECT DISTINCT v.product_id, v.size_id
                FROM (VALUES {items_values}) v (product_id, size_id)
                    LEFT JOIN stock_move sm
                        ON sm.product_id = v.product_id
                        AND coalesce(sm.size_id, 0) = v.size_id
                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                    LEFT JOIN stock_location sld ON sld.id = sm.location_id
                    LEFT JOIN stock_bin sb on sm.bin_id = sb.id
                WHERE sm.state IN ('done', 'assigned', 'reserved')
                    AND ( sm.location_dest_id not in (0) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                GROUP BY
                    CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP')
                        THEN sld.complete_name -- sm.location_id
                        ELSE sl.complete_name -- sm.location_dest_id
                        END,
                    v.product_id,
                    v.size_id,
                    coalesce(sb.name, sm.bin, '')
                HAVING
                    cast(sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as integer) <> 0
            """.format(items_values=items_values).replace(u'\xa0', u'\\xa0')
        else:
            ms_values = ', '.join(['(' + ', '.join([u"'{}'"]*len(ms_line)).format(*[isinstance(s, (str, unicode)) and s.replace("'", "''") or s for s in ms_line]) + ')' for ms_line in mssql_inventory])
            pg_inventory_sql = u"""SELECT DISTINCT
                    coalesce(erp_stock.product_id, ms_stock.product_id),
                    coalesce(erp_stock.size_id, ms_stock.size_id)
                FROM
                (
                    SELECT
                        p.id as product_id,
                        rs.id as size_id,
                        p.default_code || '/' ||
                            CASE WHEN rs.name is null THEN '0000' ELSE right('00' || cast(rs.name as float)*100, 4) END || ':' ||
                            regexp_replace(CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP')
                                THEN sld.complete_name -- sm.location_id
                                ELSE sl.complete_name -- sm.location_dest_id
                                END,'.*?/\s+([^/]+)\s+/.*?/\s+([^/]+).*','\\1/\\2') as item_data,
                        cast(sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as integer) as qty,
                        coalesce(sb.name, sm.bin, '') as bin
                    FROM stock_move sm
                        LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                        LEFT JOIN stock_location sld ON sld.id = sm.location_id
                        left join product_product p on p.id = sm.product_id
                        left join ring_size rs on rs.id = sm.size_id
                        LEFT JOIN stock_bin sb on sm.bin_id = sb.id
                    WHERE sm.state IN ('done', 'assigned', 'reserved')
                        AND p.default_code || '/' || CASE WHEN rs.name is null THEN '0000' ELSE right('00' || cast(rs.name as float)*100, 4) END
                           IN ('{itemids}')
                        and p.type = 'product'
                        AND ( sm.location_dest_id not in (0) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                    GROUP BY
                        CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP')
                            THEN sld.complete_name -- sm.location_id
                            ELSE sl.complete_name -- sm.location_dest_id
                            END,
                        p.id,
                        rs.id,
                        coalesce(sb.name, sm.bin, '')
                ) erp_stock
                FULL OUTER JOIN
                (
                    SELECT
                        pp.id as product_id,
                        rs2.id as size_id,
                        ms_data.id_delmar || '/' || ms_data.size || ':' || ms_data.warehouse || '/' || ms_data.stockid as item_data,
                        cast(ms_data.qty as integer) as qty,
                        ms_data.bin
                    FROM (VALUES {ms_values}) ms_data (id_delmar, size, warehouse, stockid, bin, qty)
                    LEFT JOIN product_product pp
                        ON pp.default_code = ms_data.id_delmar AND pp.type = 'product'
                    LEFT JOIN ring_size rs2
                        ON ms_data.size = CASE WHEN rs2.name is null THEN '0000' ELSE substring('00' || cast(rs2.name as float)*100, length(rs2.name)-1, 4) END
                ) ms_stock
                    ON erp_stock.item_data = ms_stock.item_data
                    AND erp_stock.bin = ms_stock.bin
                WHERE 1>0
                    AND coalesce(erp_stock.qty, 0) <> coalesce(ms_stock.qty, 0)
            """.format(itemids="','".join(itemid_array), ms_values=ms_values).replace(u'\xa0', u'\\xa0')
        cr.execute(pg_inventory_sql)
        sql_res = cr.fetchall()
        res = [{'product_id': item[0], 'size_id': item[1] or False} for item in sql_res if item]
        print res
        return res

    def _create_pickings_and_procurements(self, cr, uid, order, order_lines, picking_id=False, context=None):
        """Create the required procurements to supply sale order lines, also connecting
        the procurements to appropriate stock moves in order to bring the goods to the
        sale order's requested location.

        If ``picking_id`` is provided, the stock moves will be added to it, otherwise
        a standard outgoing picking will be created to wrap the stock moves, as returned
        by :meth:`~._prepare_order_picking`.

        Modules that wish to customize the procurements or partition the stock moves over
        multiple stock pickings may override this method and call ``super()`` with
        different subsets of ``order_lines`` and/or preset ``picking_id`` values.

        :param browse_record order: sale order to which the order lines belong
        :param list(browse_record) order_lines: sale order line records to procure
        :param int picking_id: optional ID of a stock picking to which the created stock moves
                               will be added. A new picking will be created if ommitted.
        :return: True
        """
        move_obj = self.pool.get('stock.move')
        picking_obj = self.pool.get('stock.picking')
        procurement_obj = self.pool.get('procurement.order')
        sets_obj = self.pool.get('product.set.components')

        for line in order_lines:
            if line.state == 'done':
                continue

            proc_ids = []
            move_ids = []

            date_planned = self._get_date_planned(cr, uid, order, line, order.date_order, context=context)

            if line.product_id:
                if line.product_id.product_tmpl_id.type in ('product', 'consu'):
                    if not picking_id:
                        picking_id = picking_obj.create(cr, uid, self._prepare_order_picking(cr, uid, order, context=context))

                    if order.partner_id.auto_split_sets:
                        if line.is_set:
                            note = "\nSet order. Ordered as {set} component.\n".format(set=line.product_id.default_code)
                            move_id = move_obj.create(cr, uid, self._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned, context=context))
                            move = move_obj.browse(cr, uid, move_id)
                            move.write({
                                'product_id': line.product_id.id,
                                'product_qty': line.product_uom_qty,
                                'is_set_component': True,
                                'note': move.note + note,
                                'is_set': True,
                                'origin': order.name,
                                'order_partner_id': order.partner_id,
                                'set_product_id': line.set_product_id.id
                            })

                            if line.set_parent_order_id:
                                move_obj.write(cr, uid, [move_id],{'picking_id': '', 'set_parent_order_id': picking_id})
                            else:
                                move_obj.write(cr, uid, [move_id], {'picking_id': picking_id,'set_parent_order_id': ''})
                            picking_obj.write(cr, uid, picking_id, {
                                'is_set': True,
                            })
                        else:
                            move_id = move_obj.create(cr, uid,
                                                      self._prepare_order_line_move(cr, uid, order, line, picking_id,
                                                                                    date_planned, context=context))
                            move_ids.append(move_id)
                    else:
                        move_id = move_obj.create(cr, uid, self._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned, context=context))
                        move_ids.append(move_id)
                else:
                    # a service has no stock move
                    move_id = False
                    move_ids.append(move_id)

                for move_id in move_ids:
                    proc_id = procurement_obj.create(cr, uid, self._prepare_order_line_procurement(cr, uid, order, line, move_id, date_planned, context=context))
                    proc_ids.append(proc_id)
                    line.write({'procurement_id': proc_id})
                    self.ship_recreate(cr, uid, order, line, move_id, proc_id)

        wf_service = netsvc.LocalService("workflow")
        # Don't do auto-confirm. It's RabbitMQ's work
        # if picking_id:
        #     wf_service.trg_validate(uid, 'stock.picking', picking_id, 'button_confirm', cr)

        for proc_id in proc_ids:
            wf_service.trg_validate(uid, 'procurement.order', proc_id, 'button_confirm', cr)

        val = {}
        if order.state == 'shipping_except':
            val['state'] = 'progress'
            val['shipped'] = False

            if (order.order_policy == 'manual'):
                for line in order.order_line:
                    if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                        val['state'] = 'manual'
                        break
        order.write(val)
        return True

    def _create_pickings_for_flash(self, cr, uid, order, context=None):
        picking_obj = self.pool.get('stock.picking')
        date_planned = order.create_date
        picking_id = picking_obj.create(cr, uid, self._prepare_order_picking(cr, uid, order, context=context))

        move_defaults = self._prepare_order_line_move_from_order(cr, uid, order, picking_id, date_planned, context=context)
        move_defaults['create_uid'] = uid

        default_fields = move_defaults.keys()
        default_fields_value = ['%({key})s as {key}'.format(key=key) for key in default_fields]

        sql = """
            insert into stock_move (
                {default_fields},
                create_date,
                sale_line_id,
                note,
                name,
                product_id,
                size_id,
                product_qty,
                product_uom,
                product_uos_qty,
                product_uos,
                product_packaging,
                address_id,
                external_customer_line_id,
                set_product_id,
                is_set
               )
            select
                {default_fields_value},
                (now() at time zone 'UTC') as create_date,
                ol.id as sale_line_id,
                concat(so.note, ol.notes) as note,
                ol.name as name,
                ol.product_id as product_id,
                ol.size_id as size_id,
                ol.product_uom_qty as product_qty,
                ol.product_uom as product_uom,
                ol.product_uom_qty as product_uos_qty,
                ol.product_uom as product_uos,
                ol.product_packaging as product_packaging,
                so.partner_shipping_id as address_id,
                ol.external_customer_line_id,
                ol.set_product_id,
                ol.is_set
            from sale_order so
                left join sale_order_line ol on so.id = ol.order_id
            where 1=1
                and ol.id is not null
                and coalesce(ol.product_uom_qty, 0) > 0
                and so.id = {order_id}
        """.format(
            order_id=order.id,
            default_fields=",\n".join(default_fields),
            default_fields_value=",\n".join(default_fields_value),
            picking_id = picking_id
        )
        cr.execute(sql, move_defaults)

        if order.is_set:
            picking_obj.write(cr, uid, picking_id, {'is_set': True})
            set_line_ids = self.pool.get('sale.order.line').search(cr, uid, [('set_parent_order_id', '=', order.id)])
            if set_line_ids:
                for line in self.pool.get('sale.order.line').browse(cr, uid, set_line_ids):
                    components_obj = {}
                    components_obj['state'] = move_defaults.get('state')
                    components_obj['location_dest_id'] = move_defaults.get('location_dest_id')
                    components_obj['date_expected'] = move_defaults.get('date_expected')
                    components_obj['tracking_id'] = move_defaults.get('tracking_id')
                    components_obj['date'] = move_defaults.get('date')
                    components_obj['location_id'] = move_defaults.get('location_id')
                    components_obj['company_id'] = move_defaults.get('company_id')
                    components_obj['sale_line_id'] = line.id
                    components_obj['note'] = line.notes
                    components_obj['name'] = line.name
                    components_obj['product_id'] = line.product_id.id
                    components_obj['size_id'] = line.size_id.id or ''
                    components_obj['product_qty'] = line.product_uos_qty
                    components_obj['product_uom'] = line.product_uom.id or ''
                    components_obj['product_uos_qty'] = line.product_uos_qty
                    components_obj['product_uos'] = line.product_uom.id or ''
                    components_obj['product_packaging'] = line.product_packaging.id or ''
                    components_obj['address_id'] = order.partner_shipping_id.id or ''
                    components_obj['external_customer_line_id'] = line.external_customer_line_id
                    components_obj['picking_id'] = ''
                    components_obj['set_parent_order_id'] = picking_id
                    components_obj['set_product_id'] = line.set_product_id.id
                    components_obj['is_set'] = 'True'

                    component_line_id = self.pool.get("stock.move").create(cr, uid, components_obj)

        if order.flash_force_id:
            self.pool.get("stock.picking").force_allocate(cr, uid, picking_id, context=context)
        self.pool.get("order.history").force_create_sm_history(cr, uid, order.id, context=context)

        order.write({'state': 'manual'})
        return True

    def action_ship_create(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids, context=context):
            if order.picking_ids:
                continue
            lines = []
            if order.flash:
                if order.partner_id.auto_split_sets and order.is_set == False:
                    lines = self._generate_set_components(cr, uid, order, order.order_line)
                    order = self.pool.get("sale.order").browse(cr, uid, order.id)

                self._create_pickings_for_flash(cr, uid, order, context=context)
            else:
                for line in order.order_line:
                    if line.product_uom_qty > 0:
                        lines.append(line)

                if order.is_set and order.set_parent_order_line != []:
                    for set_lines in order.set_parent_order_line:
                        if set_lines not in order.order_line:
                            lines.append(set_lines)

                if order.partner_id.auto_split_sets and order.is_set == False:
                    lines = self._generate_set_components(cr, uid, order, lines)
                    order = self.pool.get("sale.order").browse(cr, uid, order.id)

                self._create_pickings_and_procurements(cr, uid, order, lines, None, context=context)

        return True

    def _generate_set_components(self, cr, uid, order, lines, context=None):

        if order.partner_id.auto_split_sets and order.is_set == False:
            sets_obj = self.pool.get('product.set.components')
            for line in lines:
                set_ids = sets_obj.search(cr, uid, [('parent_product_id', '=', line.product_id.id,)])
                if set_ids:
                    self.pool.get("sale.order").write(cr, uid, order.id, {"is_set": True})

                    def _check_number(number):
                        if isinstance(number, int) or isinstance(number, float):
                            return number
                        if len(number) == 0 or number is None:
                            return 0

                        return number

                    set_price_unit = line.price_unit / len(set_ids) if _check_number(line.price_unit) else 0
                    set_customerCost = (float(line.customerCost) / len(set_ids)) if _check_number(
                        line.customerCost) else 0
                    set_credit_amount = (float(line.credit_amount) / len(set_ids)) if _check_number(
                        line.credit_amount) else 0
                    set_balance_due = (float(line.balance_due) / len(set_ids)) if _check_number(line.balance_due) else 0
                    set_full_retail = (float(line.full_retail) / len(set_ids)) if _check_number(line.full_retail) else 0
                    set_tax = (float(line.tax) / len(set_ids)) if _check_number(line.tax) else 0
                    set_linePrice = (float(line.linePrice) / len(set_ids)) if _check_number(line.linePrice) else 0
                    set_shipCost = (float(line.shipCost) / len(set_ids)) if _check_number(line.shipCost) else 0
                    set_ice_sub_total = (float(line.ice_sub_total) / len(set_ids)) if _check_number(
                        line.ice_sub_total) else 0
                    set_ice_row_total = (float(line.ice_row_total) / len(set_ids)) if _check_number(
                        line.ice_row_total) else 0
                    set_ice_tax_amount = (float(line.ice_tax_amount) / len(set_ids)) if _check_number(
                        line.ice_tax_amount) else 0
                    set_ice_discount = (float(line.ice_discount) / len(set_ids)) if _check_number(
                        line.ice_discount) else 0

                    component_objs = sets_obj.browse(cr, uid, set_ids)
                    self.pool.get("sale.order.line").write(cr, uid, line.id,
                                                           {"order_id": "", "set_parent_order_id": order.id,
                                                            "is_set": True,
                                                            "name": component_objs[0].parent_product_id.name,
                                                            "set_product_id": line.product_id.id})
                    for component in component_objs:
                        components_obj = {}
                        components_obj["size_id"] = self._check_prod_size(cr, uid, component.cmp_product_id.id,
                                                                          line.size_id.id)
                        components_obj["size"] = '' if components_obj["size_id"] == '' else line.size
                        components_obj["is_set"] = True
                        components_obj["is_set_component"] = True
                        components_obj["name"] = component.cmp_product_id.name
                        components_obj['external_customer_line_id'] = line.id
                        components_obj["product_uom"] = 1
                        components_obj["order_id"] = int(order.id)
                        components_obj["set_parent_order_id"] = ''
                        components_obj["product_id"] = component.cmp_product_id.id
                        components_obj["set_product_id"] = component.parent_product_id.id
                        components_obj["state"] = line.state
                        components_obj["merchantSKU"] = line.merchantSKU
                        components_obj["external_customer_line_id"] = line.external_customer_line_id
                        components_obj["type_api"] = line.type_api
                        components_obj["shipCost"] = line.shipCost
                        components_obj["tax"] = line.tax
                        components_obj["linePrice"] = line.linePrice
                        components_obj["product_last_confirm_qty"] = line.product_last_confirm_qty
                        components_obj["price_unit"] = set_price_unit / component.qty
                        components_obj["customerCost"] = set_customerCost / component.qty
                        components_obj['product_uos_qty'] = component.qty
                        components_obj['product_uom_qty'] = component.qty * int(line.product_uom_qty)
                        components_obj["credit_amount"] = set_credit_amount / component.qty
                        components_obj["balance_due"] = set_balance_due / component.qty
                        components_obj["full_retail"] = set_full_retail / component.qty
                        components_obj["tax"] = (set_tax / component.qty) if line.type_api == 'walmart' else set_tax
                        components_obj["linePrice"] = set_linePrice
                        components_obj["shipCost"] = (
                                set_shipCost / component.qty) if line.type_api == 'walmart' else set_shipCost
                        components_obj["ice_sub_total"] = set_ice_sub_total
                        components_obj["ice_row_total"] = set_ice_row_total
                        components_obj["ice_tax_amount"] = set_ice_tax_amount
                        components_obj["ice_discount"] = set_ice_discount

                        component_line_id = self.pool.get("sale.order.line").create(cr, uid, components_obj)
                        self._update_set_components_line_fields(cr, uid, component_line_id, line.id,
                                                                component.qty * len(set_ids), len(set_ids))

                    order = self.pool.get("sale.order").browse(cr, uid, order.id)
                    lines = order.order_line
                    for set_lines in order.set_parent_order_line:
                        if set_lines not in order.order_line:
                            lines.append(set_lines)

        return lines

    def _update_set_components_line_fields(self, cr, uid, cmp_line_id, set_line_id, cmp_qty, set_qty):
        sql = """
    update sale_order_line_fields solf2 set value=sel.new_value
          from
          (select solf.name,round((case when solf.name='retail_cost' then solf.value::float/(%(cmp_qty)s) else value::float/(%(set_qty)s) end)::numeric,2)::text as new_value
          from sale_order_line_fields solf
          inner join sale_order_line sol on sol.id=solf.line_id
          inner join sale_order so on so.id=sol.order_id or so.id=sol.set_parent_order_id
          where solf.line_id= %(set_line_id)s and value  ~ '^\s*[\d\.]+\s*$'
          and (
          (solf.name in ('sales_tax','goods_and_services_tax')and so.partner_id=583) or
          (solf.name in ('line_total','postage_handling','item_discount')and so.partner_id=197) or
          (solf.name in ('retail_cost')and so.partner_id=193) or
          (solf.name in ('lineHandling','lineMerchandise','creditAmount','lineShipping','lineTotal')and so.partner_id=192) or
          (solf.name in ('line_shipping','line_credits','line_subtotal')and so.partner_id=168)
          )
          )sel
          where solf2.line_id=%(cmp_line_id)s and solf2.name=sel.name
           """
        cr.execute(sql, {'cmp_qty': cmp_qty,'set_qty': set_qty,'set_line_id': set_line_id,'cmp_line_id': cmp_line_id})

    def _insert_sterling_comments_for_components(self, cr, uid, cmp_line_id, set_line_id):
        try:
            sql = """
        INSERT INTO sale_order_line_sterling_comments
        (id, create_uid, create_date, write_date, write_uid, line_id, "type", "text")
        select nextval('sale_order_line_sterling_comments_id_seq'::regclass), sol.create_uid, sol,create_date, sol.write_date, sol.write_uid, %(cmp_line_id)s, sol."type", sol."text"
        from sale_order_line_sterling_comments sol
        where sol.line_id=%(set_line_id)s
         """
            cr.execute(sql, {
                'set_line_id': set_line_id,
                'cmp_line_id': cmp_line_id,
            })
        except:
            print '-'


    def _check_prod_size(self, cr, uid, component_id, size_id):
        component_size = ''
        if size_id:
            sql = """select rel.* FROM product_product as pp
                LEFT JOIN product_template as pt on pt.id = pp.product_tmpl_id
                LEFT JOIN product_ring_size_default_rel as rel on rel.product_id = pt.id
                where pp.id = %(product_id)s
                and size_id = %(size_id)s
                """
            cr.execute(sql, {
                'product_id': component_id,
                'size_id': size_id
            })
            products_data = cr.fetchall()
            if products_data:
                component_size = size_id

        return component_size

    def clear_transactions_for_reentered_order(self, cr, uid, so_line):
        sale_order = so_line.order_id
        invoiceno = sale_order.po_number
        if not (invoiceno and sale_order.picking_ids and
                len(sale_order.picking_ids) == 1):
            return True
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)
        move_obj = self.pool.get('stock.move')
        if not server_id:
            return False
        size_obj = self.pool.get('ring.size')

        for move in so_line.move_ids:
            if move.state in ('done', 'assigned') or move.keep_transactions:
                continue
            trans_tag = hashlib.md5(str(so_line.id)).hexdigest()

            product_code = move.product_id.default_code or False
            size_code = size_obj.get_4digit_name(cr, uid, move.size_id.id)

            # Check the Posted transactions for an order/an item/a size
            params = (invoiceno, product_code, size_code, trans_tag)

            sum_sql = """SELECT sum(adjusted_qty), warehouse, stockid, bin
                        FROM TRANSACTIONS
                        WHERE invoiceno = ?
                        AND id_delmar = ?
                        AND size = ?
                        AND status like 'Posted'
                        AND (trans_tag is null or trans_tag = ?)
                        AND _user_creator like 'openerp%'
                        GROUP BY warehouse, stockid, bin """
            res = server_data.make_query(cr, uid, server_id, sum_sql,
                select=True, params=params, commit=False)

            # Delete pending transactions for an order/an item/a size
            delete_sql = """DELETE FROM TRANSACTIONS
                        WHERE invoiceno = ?
                        AND id_delmar = ?
                        AND size = ?
                        AND status like 'PENDING'
                        AND (trans_tag is null or trans_tag = ?)
                        AND _user_creator like 'openerp%' """
            server_data.make_query(cr, uid, server_id, delete_sql,
                select=False, params=params, commit=True)

            # ADJUST transaction for balance
            description = so_line.name or move.product_id.name
            itemid = product_code and (product_code + '/' + size_code) or False
            customer_ref = so_line.order_id.partner_id.ref or None
            for res_line in res:
                if not res_line[0]:
                    continue
                reserved_flag = move_obj.get_reserved_flag(cr, uid,
                    itemid, res_line[1], res_line[2], server_id)
                unit_price = move.sale_line_id.price_unit or 0
                params = (
                    itemid or 'null',  # itemid
                    'ADJ',  # TransactionCode
                    abs(res_line[0]),  # qty
                    res_line[1] or 'null',  # warehouse
                    description or 'null',  # itemdescription
                    res_line[3] or '',  # bin
                    res_line[2] or 'null',  # stockid
                    'TRS',  # TransactionType
                    product_code,  # id_delmar
                    res_line[0] > 0 and -1 or 1,  # sign
                    -res_line[0],  # adjusted_qty
                    size_code,  # size
                    move.invredid or 'null',  # invredid
                    customer_ref or None,  # CustomerID
                    product_code,  # ProductCode
                    invoiceno or 'null',  # InvoiceNo
                    reserved_flag,  # reserved
                    so_line.id,  # trans_tag
                    unit_price * (res_line[0] > 0 and 1 or -1),  # unit price
                )
                tr_context = {
                    'erp_sm_id': move.id,
                    'export_ref_ca': move.export_ref_ca,
                    'export_ref_us': move.export_ref_us,
                    'fc_invoice': move.fc_invoice,
                    'fc_order': move.fc_order,
                    'del_invoice': move.del_invoice,
                    'del_order': move.del_order,
                    'bill_of_lading': move.bill_of_lading,
                }
                move_obj.write_into_transactions_table(cr, uid, server_id, params, context=tr_context)

        return True

    def add_new_sizes_for_product(self, cr, uid, prod_id, id_delmar, server_id):
        server_data = self.pool.get('fetchdb.server')

        sel_sql = """select cast(size as float) / 100
                    from transactions
                    where id_delmar = '%s'
                    and isnumeric(size) = 1
                    and status != 'Posted'
                    group by size""" % (id_delmar,)

        res = server_data.make_query(cr, uid, server_id, sel_sql, select=True, commit=False)

        mssql_size_list = None
        if res:
            mssql_size_arr = [str(x[0]) for x in res if x[0]]
            mssql_size_list = ",".join(mssql_size_arr)

        if mssql_size_list:

            ins_sql = """insert into product_ring_size_default_rel
                        (product_id, size_id)
                        select
                        (select product_tmpl_id from product_product where id = %s) as product_id,
                        s.id as size_id
                        from (select regexp_split_to_table('%s',',') as mssql_size) m
                            left join
                            (select rs.name as openerp_size
                            from product_ring_size_default_rel r
                            left join ring_size rs on rs.id = r.size_id
                            left join product_product p on p.product_tmpl_id = r.product_id
                            where p.id = %s
                            ) t on regexp_replace(m.mssql_size, '.0', '') = regexp_replace(t.openerp_size, '.0', '')
                        left join ring_size s on regexp_replace(m.mssql_size, '.0', '') = regexp_replace(s.name, '.0', '')
                        where t.openerp_size is null
                        and s.id is not null""" % (prod_id, mssql_size_list, prod_id)

            cr.execute(ins_sql)

        return True

    def fill_phantom_inventory(self, cr, uid):
        server_data = self.pool.get('fetchdb.server')

        server_id = server_data.get_main_servers(cr, uid, single=True)
        if not server_id:
            print "Can't update PHANTOM inventory: no connection to MSSQL."
            return False

        sql_reset_phantom = """
            --update product_multi_customer_fields
            --set value = 0
            delete from product_multi_customer_fields
            where 1=1
            and field_name_id in (select fn.id
                                   from product_multi_customer_fields_name fn
                                   left join res_partner p on fn.customer_id = p.id
                                   where label = 'Phantom Inventory'
                                 )
            --and value <> '0'
        """
        cr.execute(sql_reset_phantom)

        sql_prod_list = """
            select id_delmar, size, warehouse, stockid, phantom
            from transactions
            where 1=1
                and id in (
                    select max(id)
                    from transactions
                    where 1=1
                        and phantom is not null
                        and status = 'Pending'
                    group by id_delmar, size, warehouse, stockid
                )
                and phantom > 0
        """

        prod_list = server_data.make_query(cr, uid, server_id, sql_prod_list, select=True, commit=False)

        for prod in prod_list:

            self.fill_phantom_customer_field(cr, uid, prod)

            # print "Phantom QTY " + ('%.0f' % (prod[PHANTOM],)) + " is inserted for " + prod[ID_DELMAR] + "/" + prod[SIZE] + " (" + cust['name'] + ")"

        return True

    def fill_phantom_customer_field(self, cr, uid, prod):
        location_data = self.pool.get('stock.location')
        field_label = 'Phantom Inventory'

        ID_DELMAR = 0
        SIZE = 1
        WAREHOUSE = 2
        STOCKID = 3
        PHANTOM = 4

        prod_size = str(float(re.sub('[^\d]', '', prod[SIZE])) / 100)
        complete_name = prod[WAREHOUSE] + ' / Stock / ' + prod[STOCKID]
        location_ids = location_data.search(cr, uid, [('complete_name', 'like', complete_name)])
        if not location_ids:
            return False

        # pg_stock_ids.append(location_id)

        sql_check_prod = """
            select id
            from product_product p
            where 1=1
            and p.default_code = '%s'
            and p.type = 'product'
        """ % (prod[ID_DELMAR],)

        cr.execute(sql_check_prod)
        prod_check = cr.fetchone()
        prod_id = prod_check and prod_check[0] or False

        if not prod_id:
            return False

        sql_customer = """
            select
                rp.id as rp_id,
                rp.name as rp_name,
                fn.id as fn_id
            from  res_partner rp
                inner join product_multi_customer_fields_name fn on (
                    rp.id = fn.customer_id
                    and fn.label = '%s'
                )
            where ','||rp.order_ids||',' like '%%,%s,%%'
            or ','||rp.sec_order_ids||',' like '%%,%s,%%'
        """ % (field_label, location_ids[0], location_ids[0],)

        cr.execute(sql_customer)
        cust_list = cr.dictfetchall()
        for cust in cust_list:
            # create new customer field value

            where_size_id = " size_id is null "
            if prod[SIZE] != '0000':
                where_size_id = " size_id = (select id from ring_size where name = '%s' limit 1) " % (prod_size)

            sql_check_phantom = """
                select *
                from product_multi_customer_fields
                where 1=1
                and product_id = %s
                and %s
                and field_name_id = %s
                and partner_id = %s
            """ % (prod_id,
                   where_size_id,
                   cust['fn_id'],
                   cust['rp_id'],)

            cr.execute(sql_check_phantom)
            check_phantom_res = cr.fetchall()

            if not check_phantom_res:

                sql_insert_field = """
                    insert into product_multi_customer_fields (
                            id,
                            create_uid,
                            create_date,
                            field_name_id,
                            product_id,
                            size_id,
                            partner_id,
                            value
                        )
                    select
                        nextval('product_multi_customer_fields_id_seq') as id,
                        %s, -- create_uid
                        (now() at time zone 'UTC'), -- create_date
                        %s, -- field_name_id
                        %s, -- product_id
                        (select id from ring_size where name = '%s' limit 1), -- size_id
                        %s, -- partner_id
                        '%s'  -- value
                """ % (uid,
                       cust['fn_id'],
                       prod_id,
                       prod_size,
                       cust['rp_id'],
                       ('%.0f' % (float(prod[PHANTOM]),)),)

                cr.execute(sql_insert_field)

    def cron_sync_product_stock(self, cr, uid, sql_limit=100, context=None):
        config_data = self.pool.get('ir.config_parameter')
        server_data = self.pool.get('fetchdb.server')

        last_inventory_update_date = None
        last_inventory_update_id = None

        param_id = config_data.search(cr, uid, [('key', '=', 'last_inventory_update_date')])
        if param_id:
            param_obj = config_data.browse(cr, uid, param_id[0])
            if param_obj:
                last_inventory_update_date = param_obj.value
            else:
                return False

        id_param_id = config_data.search(cr, uid, [('key', '=', 'last_inventory_update_id')])
        if id_param_id:
            id_param_obj = config_data.browse(cr, uid, id_param_id[0])
            if id_param_obj:
                last_inventory_update_id = id_param_obj.value
            else:
                return False

        print "%s - last update date %s" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), last_inventory_update_date,)
        print "%s - last updated ID %s" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), last_inventory_update_id,)

        server_id = server_data.get_main_servers(cr, uid, single=True)
        if server_id:

            sql_max_ts = """
                SELECT max(ts), max(id) FROM (
                    SELECT TOP %s ID_Delmar, timestamp as ts, id
                    FROM TRANSACTIONS
                    WHERE id > %s
                      AND status <> 'Posted'
                    ORDER BY id
                ) a""" % (sql_limit, last_inventory_update_id,)

            res_max_ts = server_data.make_query(cr, uid, server_id, sql_max_ts, select=True, commit=False)

            print "%s - new last update date %s" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), res_max_ts and res_max_ts[0] and res_max_ts[0][0],)
            print "%s - new last updated id %s" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), res_max_ts and res_max_ts[0] and res_max_ts[0][1],)

            sql_count = """
                    SELECT ID_Delmar
                    FROM TRANSACTIONS
                    WHERE id > %s
                      AND status <> 'Posted'
                    GROUP BY ID_Delmar""" % (last_inventory_update_id,)

            res_count = server_data.make_query(cr, uid, server_id, sql_count, select=True, commit=False)

            print "%s - %s products need to update stock QTY" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), len(res_count),)

            sql = """
                SELECT ID_Delmar
                FROM (
                    SELECT TOP %s ID_Delmar
                    FROM TRANSACTIONS
                    WHERE id > %s
                      AND status <> 'Posted'
                    ORDER BY ID
                ) a
                GROUP BY ID_Delmar""" % (sql_limit, last_inventory_update_id,)

            res = server_data.make_query(cr, uid, server_id, sql, select=True, commit=False)

            print "%s - %s products taken for current transaction of update QTY" % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), len(res or []),)

            if res:
                for prod in res:
                    if prod:
                        self.sync_product(cr, uid, prod[0], context=context)

            if param_id:
                if res_max_ts and res_max_ts[0] and res_max_ts[0][0]:
                    curr_inventory_update_date = res_max_ts[0][0].replace(microsecond=0)
                    config_data.write(cr, uid, param_id, {'value': curr_inventory_update_date})

            if id_param_id:
                if res_max_ts and res_max_ts[0] and res_max_ts[0][1]:
                    curr_inventory_update_id = res_max_ts[0][1]
                    config_data.write(cr, uid, id_param_id, {'value': curr_inventory_update_id})
        return True

    def sync_product(self, cr, uid, id_delmar, context=None):
        """
        Sync one product with MSSQL inventory.

        :param dbcursor cr:
        :param int uid:
        :param str id_delmar:
        :param dict context:
        :return:
        """
        product_data = self.pool.get('product.product')
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)

        id_delmar = id_delmar.strip()
        if not id_delmar:
            return False

        prod_ids = product_data.search(cr, uid, [
            ('type', '=', 'product'),
            '|', ('default_code', "=", id_delmar), ('id_delmar', "=", id_delmar)
        ])

        if not prod_ids:
            return False

        prod_id = prod_ids[0]
        self.add_new_sizes_for_product(cr, uid, prod_id, id_delmar, server_id)

        prod_obj = product_data.browse(cr, uid, prod_id)

        size_ids = prod_obj.ring_size_ids
        if size_ids:
            for prod_size_id in size_ids:
                self.sync_product_stock(cr, uid, prod_id, prod_size_id.id, context=context)
        else:
            self.sync_product_stock(cr, uid, prod_id, None, context=context)

        print "%s - %s product is successfully updated" % \
              (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), prod_obj.default_code,)

        return True

    def update_set_stock(self, cr, uid, prod_id, context=None):
        server_obj = self.pool.get('fetchdb.server')
        product_obj = self.pool.get('product.product')
        move_obj = self.pool.get('stock.move')
        bin_obj = self.pool.get('stock.bin')
        set_obj = self.pool.get('product.set.components')
        max_bin_length = bin_obj.get_max_bin_length(cr, uid)
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(
                'Warning!',
                'Can not do the stock update. Check settings for the server with TRANSACTIONS table!'
            )

        product = product_obj.browse(cr, uid, prod_id).default_code

        if not product:
            return False

        cmp_delimiter = ";"
        attr_delimiter = '||'

        set_sql = """
            SELECT
                ps.id as set_id,
                ps.id_delmar as parent,
                STUFF(
                  (
                    SELECT '{cmp_delimiter}' + pc.id_delmar + '{attr_delimiter}' + CAST(pc.qty AS VARCHAR(MAX))
                    FROM prg_sets_components pc
                    WHERE pc.set_id = ps.id
                    FOR XML PATH ('')
                  ), 1, {cmp_delimiter_len}, ''
                ) as cmps
            FROM prg_sets ps
            WHERE (ps.id_delmar = %(id_delmar)s
                OR ps.id in (
                    select set_id
                    from prg_sets_components cc
                    where id_delmar = %(id_delmar)s
                ))and ps.ARCHIVED=0
        """.format(
                cmp_delimiter=cmp_delimiter,
                attr_delimiter=attr_delimiter,
                cmp_delimiter_len=len(cmp_delimiter)
            )

        set_items = server_obj.make_query(cr, uid, server_id, set_sql, params={'id_delmar': product}, select=True, commit=False) or []
        for set_id, parent_item, cmps_str in set_items:
            if not cmps_str:
                continue
            parent_item = parent_item.strip().upper()
            cmp_items = {}
            for cmp_str in cmps_str.split(cmp_delimiter):
                cmp_str = cmp_str.strip()
                if not cmp_str:
                    continue
                set_cmp, qty = cmp_str.split(attr_delimiter)
                try:
                    set_cmp = set_cmp.strip()
                    qty = int(qty.strip())
                    if set_cmp and qty > 0:
                        cmp_items[set_cmp] = qty
                except:
                    logger.error('Wrong set settings for %s: %s' % (parent_item, cmp_str))

            if not cmp_items:
                continue

            parent_item_info = {}
            cmp_item_info = {}

            sets_query = """
                select
                    t.size,
                    t.warehouse,
                    t.stockid,
                    sum(t.adjusted_qty) as qty,
                    max(t.invredid) as invredid,
                    max(t.bin) as bin
                from transactions t
                where t.id_delmar = ?
                and t.status <> 'Posted'
                group by t.id_delmar, t.size, t.warehouse, t.stockid
            """
            item_fields = ['size', 'warehouse', 'stockid', 'qty', 'invredid', 'bin']
            item_info = server_obj.make_query(cr, uid, server_id, sets_query, params=[parent_item], select=True, commit=False) or []
            for item in item_info:
                info = dict(zip(item_fields, item))
                key = "{size}/{warehouse}/{stockid}".format(
                    size=info['size'].strip(),
                    warehouse=info['warehouse'].strip(),
                    stockid=info['stockid'].strip()
                ).upper()
                parent_item_info[key] = info

            cmps_query = """
                WITH a as (
                    SELECT
                        psc.set_id,
                        psc.id,
                        size,
                        warehouse,
                        STOCKID,
                        floor(sum(t.adjusted_qty) / psc.qty) as qty,
                        max(t.bin) as bin,
                        t.id_delmar,
                        psc.qty as comp_qty,
                        CASE WHEN EXISTS (
                            SELECT id
                            FROM transactions
                            WHERE id_delmar = t.ID_DELMAR
                                AND STATUS <> 'Posted'
                                AND size <> '0000'
                        )
                        THEN 1 ELSE 0 END sized
                    FROM prg_sets_components psc
                        LEFT join transactions t on psc.id_delmar = t.id_delmar
                    WHERE psc.set_id = {set_id}
                        AND t.STATUS <> 'Posted'
                    GROUP by t.size, t.warehouse, t.stockid, psc.id, psc.qty, psc.set_id, t.id_delmar
                    )
                SELECT
                    b.size,
                    b.warehouse,
                    b.stockid,
                    min(b.qty)as qty,
                    bin = CAST(STUFF((
                                SELECT '~' + cast(a.comp_qty as varchar) + 'x' + a.id_delmar + '-' + max(a.bin)
                            FROM a
                            WHERE 1=1
                              and a.set_id = b.set_id
                                and (a.sized = 1 and a.size = b.size
                                  or a.sized = 0 and a.size = '0000')
                                and a.warehouse = b.warehouse
                                and a.stockid = b.stockid
                          group by a.set_id, a.id_delmar, a.comp_qty, a.warehouse, a.stockid
                          order by a.id_delmar
                            FOR XML PATH('')),1,1,'') as TEXT)
                FROM (
                    SELECT distinct ns.set_id, ns.id, s.size, ns.warehouse, ns.stockid, ns.qty, ns.bin
                    FROM a as ns
                    LEFT JOIN a as s
                        ON s.warehouse = ns.warehouse
                            AND s.stockid = ns.stockid
                            AND ns.sized = 0
                    WHERE s.sized = 1 AND s.size <> '0000'
                    UNION
                    SELECT set_id, id, size, warehouse, stockid, qty, bin
                    FROM a
                ) b
                GROUP BY
                      b.set_id,
                      b.size,
                      b.warehouse,
                      b.stockid
                HAVING COUNT(*) = {set_cmp_count}
            """.format(
                set_id=set_id,
                set_cmp_count=len(cmp_items)
            )
            item_fields = ['size', 'warehouse', 'stockid', 'qty', 'bin']
            item_info = server_obj.make_query(cr, uid, server_id, cmps_query, select=True, commit=False) or []

            if set_obj.is_set(cr, uid, prod_id) and set_obj.is_nonsize_set(cr, uid, prod_id):
                from math import ceil
                def getNonSizeComponent(arr, recept):
                    nonSizeItem = None
                    non_size_arr = []
                    for c_item in arr:
                        if c_item['size'] == '0000':
                            non_size_arr.append(c_item)

                    if len(recept) != len(non_size_arr):
                        nonSizeItem = non_size_arr[0]
                        nonSizeItem['qty'] = 0
                        return nonSizeItem

                    if len(non_size_arr):
                        if len(non_size_arr) == 1:
                            nonSizeItem = non_size_arr[0]
                        else:
                            for non_size_el in non_size_arr:
                                if not nonSizeItem or nonSizeItem and (
                                        int(nonSizeItem.get('qty')) / int(nonSizeItem.get('cmp_qty'))) > (
                                        int(non_size_el.get('qty')) / int(non_size_el.get('cmp_qty'))):
                                    nonSizeItem = non_size_el

                    return nonSizeItem

                def caclSumQtySizesComponent(arr):
                    sum_size_comp = 0
                    size_dict = {}
                    for c_item in arr:
                        size = c_item['size']
                        if '0000' != size:
                            if size_dict.get(size):
                                if (c_item['qty'] / c_item['cmp_qty'] < size_dict.get(size)['qty'] /
                                    size_dict.get(size)['cmp_qty']) and (
                                        c_item['comp_id'] != size_dict.get(size)['comp_id']):
                                    size_dict.update({size: c_item})
                            else:
                                size_dict.update({size: c_item})
                    if not size_dict:
                        return False
                    for key in size_dict:
                        sum_size_comp = sum_size_comp + int(size_dict.get(key)['qty'])

                    return sum_size_comp / size_dict.get(key)['cmp_qty']

                def check_complex_set(arr):
                    component = {}
                    no_size_comp = []
                    for key in arr:
                        for item in arr.get(key):
                            if item.get('size') != '0000' and item.get('comp_id') not in component:
                                component.update({item.get('comp_id'): [x['size'] for x in arr.get(key) if
                                                                        x['comp_id'] == item.get('comp_id')]})
                            if item.get('size') == '0000':
                                no_size_comp.append(key)

                    for key in arr:
                        for item in arr.get(key):
                            if key not in no_size_comp:
                                arr[key] = []

                    it_complex_set = True if len(component.values()) > 1 else False

                    if it_complex_set:
                        need_drop = []
                        if len(component.values()) > 1:
                            set1 = set(component.values()[0])
                            set2 = set(component.values()[1])
                            if len(component.values()[0]) > len(component.values()[1]):
                                need_drop = list(set1.difference(set2))
                            else:
                                need_drop = list(set2.difference(set1))

                        if len(need_drop) > 0:
                            for key in arr:
                                for item in arr.get(key):
                                    if item.get('size') in need_drop:
                                        arr.get(key).remove(item)

                    return it_complex_set

                def getMinQtysizeItem(size_str, arr):
                    item = None
                    for c_item in arr:
                        if c_item['size'] != '0000' and c_item['size'] == size_str:
                            if not item:
                                item = c_item
                            if ((c_item['qty'] / c_item['cmp_qty']) < item['qty'] / item['cmp_qty']):
                                item = c_item

                    return item

                cmps_query = """
                    SELECT
                        psc.set_id,
                        psc.id,
                        size,
                        warehouse,
                        STOCKID,
                        floor(sum(t.adjusted_qty) / psc.qty) as qty,
                        max(t.bin) as bin,
                        t.id_delmar,
                        psc.qty as comp_qty,
                        CASE WHEN EXISTS (
                            SELECT id
                            FROM transactions
                            WHERE id_delmar = t.ID_DELMAR
                                AND STATUS <> 'Posted'
                                AND size <> '0000'
                        )
                        THEN 1 ELSE 0 END sized
                    FROM prg_sets_components psc
                        LEFT join transactions t on psc.id_delmar = t.id_delmar
                    WHERE psc.set_id = {set_id}
                        AND t.STATUS <> 'Posted'
                    GROUP by t.size, t.warehouse, t.stockid, psc.id, psc.qty, psc.set_id, t.id_delmar
                """.format(set_id=set_id)
                component_info = server_obj.make_query(cr, uid, server_id, cmps_query, select=True, commit=False) or []
                arr_component_inv = {}

                # composit component inventory
                for cmp in component_info:
                    info = dict(zip(
                        ['set_id', 'comp_id', 'size', 'warehouse', 'stockid', 'qty', 'bin', 'sku', 'cmp_qty', 'sized'],
                        cmp))
                    key = "{warehouse}/{stockid}".format(
                        size=cmp[2].strip(),
                        warehouse=cmp[3].strip(),
                        stockid=cmp[4].strip()
                    ).upper()

                    if not arr_component_inv.get(key, False):
                        arr_component_inv[key] = [info]

                    else:
                        arr_component_inv[key].append(info)

                its_complex_set = check_complex_set(arr_component_inv)

                set_sql = """
                    select c.id_delmar,c.sizeable from prg_sets_components c
                    where c.set_id = %(set_id)s
                """.format(set_id=set_id)

                component_items_arr = server_obj.make_query(cr, uid, server_id, set_sql, params={'set_id': set_id},
                                                  select=True, commit=False) or []

                for item in arr_component_inv:
                    if arr_component_inv.get(item):
                        non_size_comp = getNonSizeComponent(arr_component_inv.get(item), [r[0] for r in component_items_arr if not r[1]])
                        sum_size_comp = caclSumQtySizesComponent(arr_component_inv.get(item))
                        if not sum_size_comp or sum_size_comp == 0 or not non_size_comp:
                            continue
                        writed_item = ['0000']
                        for c_item in arr_component_inv.get(item):
                            if c_item['size'] in writed_item:
                                continue
                            write_qty = 0
                            min_qtysize_item = getMinQtysizeItem(c_item['size'], arr_component_inv.get(item))
                            writed_item.append(min_qtysize_item['size'])
                            trn_params = {
                                'id_delmar': parent_item,
                                'size': min_qtysize_item['size'],
                                'warehouse': min_qtysize_item['warehouse'],
                                'stockid': min_qtysize_item['stockid'],
                                'invredid': min_qtysize_item.get('invredid', ''),
                                'unit_price': 0
                            }
                            numerator = int(min_qtysize_item['qty'] / min_qtysize_item['cmp_qty'])
                            nosizedetorminator = int(non_size_comp['qty'] / non_size_comp.get('cmp_qty'))
                            if nosizedetorminator > sum_size_comp:
                                size_qty = numerator
                            else:
                                denominator = sum_size_comp
                                percente_size_qty = float(float(numerator * 100) / denominator) * 0.01
                                size_qty = ceil(percente_size_qty * nosizedetorminator)
                                if int(size_qty) == 0 and nosizedetorminator > 0 and min_qtysize_item['qty'] > 0:
                                    denominator = nosizedetorminator
                                    percente_size_qty = float(float(numerator * 100) / denominator) * 0.01
                                    size_qty = ceil(percente_size_qty * nosizedetorminator)

                                if 0 < size_qty < 1 and nosizedetorminator > 0:
                                    size_qty = 1

                            write_qty = size_qty

                            set_inv = parent_item_info.get(
                                min_qtysize_item['size'] + '/' + min_qtysize_item['warehouse'] + '/' +
                                min_qtysize_item['stockid'], False)

                            if set_inv and bool(set_inv.get('qty', 0)):
                                if set_inv.get('bin', False):
                                    trn_params.update({'bin': set_inv.get('bin', False) })
                                diff_size_qty = int(size_qty - set_inv.get('qty', 0))
                                write_qty = diff_size_qty
                            else:
                                setbin = None
                                if item_info:
                                    for setitem in item_info:
                                        if setitem[0] == c_item['size'] and setitem[1] == c_item['warehouse'] and setitem[2] == c_item['stockid']:
                                            setbin = setitem[4]
                                if setbin:
                                    trn_params.update({'bin': setbin})

                            if abs(write_qty) > 0:
                                trn_params.update({'qty': abs(write_qty), 'sign': 1 if (write_qty > 0) else -1})
                                list_params = move_obj.prepare_transaction_params(
                                    cr, uid,
                                    trn_params
                                )
                                if list_params:
                                    move_obj.write_into_transactions_table(
                                        cr, uid,
                                        server_id,
                                        list_params[0]
                                    )
                return True


            for item in item_info:
                info = dict(zip(item_fields, item))
                key = "{size}/{warehouse}/{stockid}".format(
                    size=info['size'].strip(),
                    warehouse=info['warehouse'].strip(),
                    stockid=info['stockid'].strip()
                ).upper()
                cmp_item_info[key] = info

            # Update inventory for existing items
            for key, stock in parent_item_info.iteritems():
                last_qty = stock['qty']
                cur_qty = 0
                bin_name = stock['bin']

                if cmp_item_info.get(key):
                    cur_qty = cmp_item_info[key]['qty']
                    bin_name = cmp_item_info[key]['bin'][:max_bin_length]

                trn_qty = cur_qty - last_qty
                trn_params = {
                    'id_delmar': parent_item,
                    'size': stock['size'],
                    'qty': abs(trn_qty),
                    'warehouse': stock['warehouse'],
                    'stockid': stock['stockid'],
                    'bin': bin_name,
                    'sign': 1 if (trn_qty > 0) else -1,
                    'invredid': stock['invredid'],
                    'unit_price': 0
                }

                if bin_name != stock['bin']:
                    bin_obj.rename_mssql_bin(cr, uid, trn_params, server_id=server_id)
                if trn_qty:

                    list_params, context = move_obj.prepare_transaction_params(
                        cr, uid,
                        trn_params,
                        server_id=server_id
                        )
                    if list_params:
                        move_obj.write_into_transactions_table(
                            cr, uid,
                            server_id,
                            list_params,
                            context=context
                            )
            # Update inventory for new items:
            # Transaction for base item not exist yet, but all components are present
            for key, stock in cmp_item_info.items():
                if not parent_item_info.get(key):
                    trn_params = {
                        'id_delmar': parent_item,
                        'size': stock['size'],
                        'qty': stock['qty'],
                        'warehouse': stock['warehouse'],
                        'stockid': stock['stockid'],
                        'bin': stock['bin'],
                        'sign': 1,
                        'unit_price': 0
                    }
                    list_params, context = move_obj.prepare_transaction_params(
                        cr, uid,
                        trn_params
                        )
                    if list_params:
                        move_obj.write_into_transactions_table(
                            cr, uid,
                            server_id,
                            list_params,
                            context=context
                            )

        return True

    def sync_product_stock(self, cr, uid, prod_id, prod_size_id, context=None):
        t = time.time()
        server_data = self.pool.get('fetchdb.server')
        product_data = self.pool.get('product.product')
        location_data = self.pool.get('stock.location')
        ring_size_data = self.pool.get('ring.size')
        move_data = self.pool.get('stock.move')

        print "Profiling sync_product_stock: preparation: %f" % (time.time()-t)
        t = time.time()

        server_id = server_data.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv(
                'Warning!',
                'Can not do the stock update. Check settings for the server with TRANSACTIONS table!'
            )

        product = product_data.browse(cr, uid, prod_id)
        id_delmar = product and product.default_code or False

        if not id_delmar:
            return False

        if prod_size_id:
            ring_size_obj = ring_size_data.browse(cr, uid, prod_size_id)
            size_str = move_data.get_size_postfix(cr, uid, ring_size_obj)
            where_size = " size = '%s' " % (size_str)
        else:
            where_size = " size = '0000' "

        pg_where_size = prod_size_id and " = %s" % prod_size_id or " is null "

        stock_sql = """
            SELECT warehouse, stockid, coalesce(bin, ''), sum(adjusted_qty) as sum_qty,
            stock_bins = cast(replace(replace((
                select distinct replace(rtrim(coalesce(bin, '')), ' ', '_') as 'data()'
                from TRANSACTIONS
                where id_delmar = t.id_delmar
                and size = t.size
                and warehouse = t.WAREHOUSE
                and stockid = t.STOCKID
                AND status <> 'Posted'
                for xml path('')), ' ', ','),
                '_', ' ')
                as varchar(255))
            FROM transactions t
            WHERE id_delmar = '%s'
            AND %s
            AND status <> 'Posted'
            GROUP BY
                id_delmar,
                size,
                warehouse,
                stockid,
                coalesce(bin, '')
        """ % (id_delmar, where_size)

        print "Profiling sync_product_stock: before MSSQL: %f" % (time.time()-t)
        t = time.time()

        stock_res = server_data.make_query(cr, uid, server_id, stock_sql, select=True, commit=False)

        WAREHOUSE  = 0
        STOCKID    = 1
        BIN        = 2
        SUM_QTY    = 3
        STOCK_BINS = 4

        print "Profiling sync_product_stock: MSSQL select: %f" % (time.time()-t)
        t = time.time()

        location_id = False
        location_id_list = []
        loc_ids = location_data.search(cr, uid, [
            '|',
            ('complete_name', 'ilike', '%Inventory loss%'),
            ('complete_name', 'ilike', '%EXCP%'),
        ])

        if loc_ids:
            location_id_list = loc_ids

        if stock_res is not None:
            looked_locs = set()
            for res in stock_res:

                loc_ids = location_data.search(cr, uid, [('complete_name', 'ilike', '% / ' + res[WAREHOUSE].strip() + ' / Stock / ' + res[STOCKID].strip())])
                if loc_ids:
                    location_id = loc_ids[0]
                    location_id_list.append(location_id)
                else:
                    continue
                if location_id not in looked_locs:
                    pg_where_bins = " and (coalesce(sb.name, sm.bin, '') not in ('" + "','".join(map(lambda x: x.strip().replace("'", "''"), res[STOCK_BINS].split(','))) + "'))"

                    empty_bin_sql = """SELECT distinct coalesce(sb.name, sm.bin, '') bin
                        from stock_move sm
                            left join stock_bin sb on sb.id = sm.bin_id
                        where (sm.location_id = {location_id}
                            or sm.location_dest_id = {location_id})
                        {pg_where_bins}
                        and sm.product_id = {prod_id}
                        and sm.size_id {pg_where_size}
                    """.format(location_id=location_id, pg_where_bins=pg_where_bins, prod_id=prod_id, pg_where_size=pg_where_size)
                    cr.execute(empty_bin_sql)

                    empty_bin_res = cr.dictfetchall()
                    if empty_bin_res:
                        for empty_bin in empty_bin_res:
                            self.set_bin_qty(cr, uid, prod_id, prod_size_id, 0, location_id, empty_bin['bin'], context=context)
                looked_locs.add(location_id)
                bin_name = res[BIN] and res[BIN].strip() or None
                self.set_bin_qty(cr, uid, prod_id, prod_size_id, res[SUM_QTY], location_id, bin_name, context=context)

            where_loc = ",".join([str(x) for x in location_id_list])

            if location_id_list:
                empty_loc_sql = """
                    select
                        case
                            when sm.location_dest_id not in (%s)
                            then sm.location_dest_id
                            else sm.location_id
                        end as loc_id,
                        coalesce(sb.name, sm.bin, '') as bin
                    from stock_move sm
                        left join stock_bin sb on sb.id = sm.bin_id
                    where 1=1
                    and (sm.location_dest_id not in (%s) or sm.location_id not in (%s))
                    and sm.product_id = %s
                    and sm.size_id %s
                    group by loc_id, coalesce(sb.name, sm.bin, '')
                """ % (
                    where_loc,
                    where_loc,
                    where_loc,
                    prod_id,
                    pg_where_size,
                )
                cr.execute(empty_loc_sql)

                empty_loc_res = cr.dictfetchall()
                if empty_loc_res:
                    for empty_loc in empty_loc_res:
                        self.set_bin_qty(cr, uid, prod_id, prod_size_id, 0, empty_loc['loc_id'], empty_loc['bin'], context=context)

        else:
            raise osv.except_osv(
                'Warning!',
                'Can not do the stock update. Server with TRANSACTIONS table is not available!'
            )

        print "Profiling sync_product_stock: end: %f" % (time.time() - t)
        return True

    def set_bin_qty(self, cr, uid, prod_id, prod_size_id, prod_qty, loc_id, bin, context=None):
        stock_change_product_qty_obj = self.pool.get('stock.change.product.qty')

        new_context = {
            'active_id': prod_id,
            'states': ['done', 'assigned', 'reserved'],
            'is_force_stock_update': True
        }

        bin_data = {
            'bin': bin,
            'location_id': loc_id,
            'new_quantity': prod_qty if prod_qty >= 0 else 0,
            'prodlot_id': None,
            'product_id': prod_id,
            'size_id': prod_size_id,
        }

        if bin_data:
            stock_change_product_qty_id = stock_change_product_qty_obj.create(cr, uid, bin_data, context=context)
            stock_change_product_qty_obj.change_product_qty(cr, uid, [stock_change_product_qty_id], context=new_context)

        return True

    def sync_product_stock_simple(self, cr, uid, prod_id, prod_size_id, prod_qty, location_id, context=None):
        self.set_bin_qty(cr, uid, prod_id, prod_size_id, prod_qty, location_id, '', context=context)

        return True

    def _prepare_order_picking(self, cr, uid, order, context=None):
        pick_default_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking.out')
        return {
            'name': False,
            'default_name': pick_default_name,
            'origin': order.name,
            'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            'type': 'out',
            'state': 'draft',
            'move_type': order.picking_policy,
            'sale_id': order.id,
            'address_id': order.partner_shipping_id.id,
            'note': order.note,
            'invoice_state': (order.order_policy == 'picking' and '2binvoiced') or 'none',
            'company_id': order.company_id.id,
            'real_partner_id': order.partner_id.id,
        }

    def _prepare_order_line_move(self, cr, uid, order, line, picking_id, date_planned, context=None):
        result = super(sale_order, self)._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned, context)
        result.update({
            'size_id': line.size_id.id,
            'note': str(order.note or '') + str(line.notes or ''),
        })
        return result

    def _check_shipping_tracking(self, cr, uid, context=None):
        self.log(cr, uid, None, 'Check tracking numbers')
        stock_picking_data = self.pool.get('stock.picking')
        stock_picking_data.check_picking_orders(cr, uid, context=context)
        stock_picking_data.check_tracking_numbers(cr, uid, context=context)
        self.check_order_status(cr, uid, context=context)
        return True

    def check_shipping_tracking(self, cr, uid, ids=None, context=None):
        return self._check_shipping_tracking(cr, uid, context=context)

    def set_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'done'}, context=context)

        return True

    def update_tracking(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids, context=context):
            tracking_numbers = []
            for pick in order.picking_ids:
                if pick.state == 'done' and pick.tracking_ref:
                    tracking_numbers.append(pick.tracking_ref)
            self.write(cr, uid, order.id, {'tracking_ref': ", ".join(tracking_numbers)}, context=context)
        return True

    def update_shp_date(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids, context=context):
            if not order.shp_date:
                for pick in order.picking_ids:
                    if pick.shp_date:
                        self.write(cr, uid, order.id, {'shp_date': pick.shp_date}, context=context)
                        break
        return True

    def action_reject(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')

        for sale in self.browse(cr, uid, ids, context=context):
            sale_order_line_obj.write(
                cr, uid,
                [l.id for l in sale.order_line], {'state': 'rejected'}
                )
            message = _("The sales order '%s' has been rejected.") % (sale.name,)
            self.log(cr, uid, sale.id, message)
        self.write(cr, uid, ids, {'state': 'rejected'})
        super(sale_order, self).action_reject(cr, uid, ids, context=context)
        return True

    def fba_do_remote_done(self, cr, uid, ids, context=None):
        order = self.browse(cr, uid, ids[0], context=context)
        server_data = self.pool.get('fetchdb.server')
        sale_server_id = server_data.get_sale_servers(cr, uid, single=True)
        server_id = server_data.get_main_servers(cr, uid, single=True)

        # for pick in order.picking_ids:
        remote_status = True
        if sale_server_id and server_id:

            order_line_item_no = 0

            order_line_ids = self.pool.get('sale.order.line').search(cr, uid, [('order_id', '=', order.id)])
            order_line = self.pool.get('sale.order.line').browse(cr, uid, order_line_ids, context=context)

            self.fba_remove_OS_Sales(cr, uid, order, sale_server_id)
            self.fba_remove_OS_Sales_BU(cr, uid, order, sale_server_id)

            invoice_server_id = server_data.get_invoice_servers(cr, uid, single=True)
            invoice_no = self.generate_invoiceno(cr, uid, order.partner_id, context=context)

            order_qty = 0
            order_cost = 0
            for line in order_line:
                if not remote_status:
                    break

                order_qty += float(line.product_uom_qty)
                order_cost += float(line.price_unit) * float(line.product_uom_qty)
                total_sale_line_cost = float(line.price_unit) * float(line.product_uom_qty)
                order_line_item_no += 1

                remote_status &= self.fba_fill_OS_Sales_tables(cr, uid, line, sale_server_id, order_line_item_no)
                invoice_detail_status = self.fill_invoice_detail(cr, uid, line, order, invoice_no, invoice_server_id,
                                                                 total_sale_line_cost, order_line_item_no)

            if not invoice_server_id:
                raise osv.except_osv(_('Warning!'), _('Invoice server is not available!'))

            invoice_header_status = self.fill_invoice_header(cr, uid, order, invoice_no, invoice_server_id, order_qty,
                                                             order_line_item_no, order_cost, order_line)

            if not remote_status:
                self.fba_remove_OS_Sales(cr, uid, order, sale_server_id)
                self.fba_remove_OS_Sales_BU(cr, uid, order, sale_server_id)

            remove_os_sales_sql = """
                UPDATE sale_order SET fba_invoice='%s' WHERE "name"='%s'
            """ % (invoice_no, order.name)
            cr.execute(remove_os_sales_sql)
        else:
            remote_status = False

        return remote_status

    def generate_invoiceno(self, cr, uid, partner, context=None):
        invoice_no = False
        if partner:
            partner_ref = partner.ref
            seq_partner_ref = partner_ref

            if partner.is_sterling:
                seq_partner_ref = 'STK'

            if partner_ref:
                invoiceno_seq_name = 'stock.picking.invoiceno.' + seq_partner_ref.lower()

                if not self.pool.get('ir.sequence').search(cr, uid, [('name', '=', invoiceno_seq_name)]):
                    pg_get_max_inv_id_sql = """SELECT cast(max(regexp_replace(invoice_no, '.*?(\d+).*','\\1')) as integer)
                        from stock_picking
                        where invoice_no like '%s%%';
                    """ % (partner_ref,)
                    cr.execute(pg_get_max_inv_id_sql)
                    pg_inv_res = cr.fetchone()

                    max_inv_no = pg_inv_res and pg_inv_res[0] or 1

                    seq = {
                        'name': invoiceno_seq_name,
                        'implementation': 'standard',
                        'prefix': "",
                        'padding': 8 if partner_ref == 'HBC' else 9 - len(seq_partner_ref),
                        'number_increment': 1,
                        'number_next': max_inv_no + 1,
                        }

                    self.pool.get('ir.sequence').create(cr, uid, seq)

                seq_ids = self.pool.get('ir.sequence').search(cr, uid, [('name', '=', invoiceno_seq_name)])
                if seq_ids:
                    inv_seq = self.pool.get('ir.sequence').next_by_id(cr, uid, seq_ids[0])

                    invoice_no = inv_seq if partner_ref == 'HBC' else partner_ref + inv_seq
                    if partner_ref == 'FBW':
                        invoice_no = partner_ref + '1' + inv_seq

                    if not inv_seq or not invoice_no:
                        raise osv.except_osv(_('Warning !'), _("""Can not generate invoice No for order '%s' for partner '%s'!""" % (invoiceno_seq_name, partner_ref,)))
                else:
                    raise osv.except_osv(_('Warning !'), _("""Can not create sequence for Partner '%s'!""" % (partner_ref,)))
        else:
            raise osv.except_osv(_('Warning !'), """Partner not found!""" )

        return invoice_no

    def fill_invoice_header(self, cr, uid, order, invoice_no, invoice_server_id, order_qty, lines_count, order_cost, order_lines):
        server_data = self.pool.get('fetchdb.server')
        shipping_address = order.partner_shipping_id
        invoice_address = order.partner_invoice_id
        shp_service = False
        carrier_name = shp_service and shp_service.name or ''
        shp_type = False
        shp_type_name = shp_type and shp_type.name or ''

        taxes_obj = self.pool.get("delmar.sale.taxes")

        discount = order.discount_total
        sale_taxes = taxes_obj.compute_sale_order_taxes(cr, uid, order, order_lines)

        gst_value = sale_taxes['gst'] or 0.0
        gst_value += sale_taxes['hst'] or 0.0
        pst_value = sale_taxes['pst'] or 0.0
        amount_tax = sale_taxes.get('amount_tax', False) or 0

        if sale_taxes['qst']:
            pst_value += sale_taxes['qst']

        flg = 1
        so_partner = order.partner_id

        total_qty = 0
        order_cost -= discount
        total_ship_cost = order_cost + amount_tax

        if so_partner.ref == 'ICE':
            total_ship_cost = float(order_cost) + float(order.tax)
            total_ship_cost += float(order.shipping)

        state = shipping_address and shipping_address.state_id
        state_code = state and state.code or None
        insert_values = {
            'PO_ID': order.name,
            'InvoiceNo': invoice_no,
            'MerchantID': so_partner.ref or None,
            'TrackingNo': order.tracking_ref or None,
            'Address1': shipping_address and str(shipping_address.street) or None,
            'Address2': shipping_address and shipping_address.street2 or None,
            'City': shipping_address and str(shipping_address.city) or None,
            'State': state_code,
            'Zip': shipping_address and shipping_address.zip or None,
            'B_Address1': invoice_address and invoice_address.street or None,
            'B_Address2': invoice_address and invoice_address.street2 or None,
            'B_City': invoice_address and invoice_address.city or None,
            'B_State': state_code,
            'B_Name': invoice_address and invoice_address.name or None,
            'FullName': shipping_address and shipping_address.name or None,
            'B_Zip': invoice_address and invoice_address.zip or None,
            'OrderCost': order_cost,
            'OrderQty': int(total_qty or order_qty),
            'Items': int(lines_count),
            'Shipping_Handling': 0,
            'Total_Ship_Cost': total_ship_cost,
            'ServiceTypeName': (carrier_name + " " + shp_type_name) or None,
            'CarrierName': carrier_name or None,
            'CarrierID': None,
            'ShipDate': order.shp_date or None,
            'ShippingWhse': 'USCHP',
            'InvoiceDate': {'subquery': 'getdate()'},
            'InvoiceDate2': {'subquery': 'getdate()'},
            'OrderDate': {'subquery': 'getdate()'},
            'imported': 0,
            'InvoiceDate1': {'subquery': 'getdate()'},
            'Flg': flg,
            'GST': gst_value,
            'PST': pst_value,
            'date_entered': {'subquery': 'getdate()'},
            'Discount': discount,
            'tax': amount_tax,
        }

        keys = insert_values.keys()
        si_obj = self.pool.get('sale.integration')
        additional_information = si_obj.getAdditionalInvoiceInformation(
            cr, uid, order.id, insert_values, {
                'customer_id': so_partner.id,
            }
        )

        if additional_information:
            insert_values.update({
                # Secure filter to not add new keys in insert dict
                k: v for k, v in additional_information.items() if k in keys
            })

        table_name = 'gmotoc.Invoice_Header'

        result = server_data.insert_record(
            cr, uid, invoice_server_id,
            table_name, insert_values,
            select=False, commit=True
        )

        return bool(result)

    def fill_invoice_detail(self, cr, uid, line, order, invoice_no, invoice_server_id, total_sale_line_cost, line_no, direct=1, return_qty=False):
        server_data = self.pool.get('fetchdb.server')
        product_data = self.pool.get('product.product')

        acc_price_price = 0.0
        # calculate price from acc_price if only:
        # sale and customer with Acc_Price invoice cost
        if direct and order.partner_id.invoice_cost_from_acc_price:
            acc_price_price = self.get_acc_price_price(cr, uid, line.id)

        # take price unit from acc_price first, if not then from sale order line
        line_price_unit = line.price_unit and float(line.price_unit)
        price_unit = acc_price_price or line_price_unit or 0.0

        if line.is_set:
            qty = line.product_uom_qty if direct else return_qty
            price_unit = line_price_unit * qty
        else:
            qty = line.product_uom_qty if direct else return_qty

        # Adjust price_unit by discount
        price_unit *= ((100 - order.partner_id.discount)/100)

        # revert numbers for returns
        if not direct:
            price_unit *= -1.0
            total_sale_line_cost *= -1.0

        if order.partner_id.external_customer_id == 'pii':
            ProductID = line.merchantSKU or None
        else:
            ProductID = line.product_id.default_code or None

        so_partner = line.order_id.partner_id or line.set_parent_order_id.partner_id

        upc = product_data.get_val_by_label(cr, uid, line.product_id.id, so_partner.id, 'UPC', line.size_id and line.size_id.id or False) or None
        Size = line.size_id and line.size_id.name or None
        id_delmar = line.product_id.default_code or None

        table_name = 'gmotoc.Invoice_Detail'
        insert_values = {
            'PO_ID': order.name or None,
            'MerchantID': so_partner.ref or None,
            'ID_Delmar': id_delmar or None,
            'ProductID': ProductID,
            'Quantity': qty and int(qty) or None,
            'UnitCost': price_unit or 0,
            'ExtCost': total_sale_line_cost,
            'UPC': upc,
            'UnitPrice': price_unit or 0,
            'InvoiceNo': invoice_no,
            'Size': Size,
            'VendorSKU': line.merchantSKU or None,
            'OrderDate_In': order.date_order,
            # 'TrackingNo': pick.tracking_ref or None,
            'TrackingNo': None,
            'OrderLineItem_': line.merchantLineNumber or line_no,
            'prc_ovride': 0,
        }

        result = server_data.insert_record(
            cr, uid, invoice_server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        return bool(result)

    def fba_remove_OS_Sales(self, cr, uid, order, sale_server_id):
        server_data = self.pool.get('fetchdb.server')
        remove_os_sales_sql = """
            DELETE FROM dbo.OS_Sales
            WHERE 1=1
                AND Order_ID = '%s'
            ;""" % (order.external_customer_order_id)
        result = server_data.make_query(cr, uid, sale_server_id, remove_os_sales_sql, select=False, commit=True)

        return result

    def fba_remove_OS_Sales_BU(self, cr, uid, order, sale_server_id, return_flag=False):
        if return_flag:
            return_operand = '>'
        else:
            return_operand = '='
        server_data = self.pool.get('fetchdb.server')
        remove_os_sales_sql = """
            DELETE FROM dbo.OS_Sales_BU
            WHERE 1=1
                AND Order_ID = '%s'
                AND ReturnQuantity %s 0
            ;""" % (order.external_customer_order_id, return_operand)
        result = server_data.make_query(cr, uid, sale_server_id, remove_os_sales_sql, select=False, commit=True)

        return result

    # TO_REFACTOR: remove code duplication with:
    # stock_picking.fill_OS_Sales_tables and sale_order.fbc_fill_OS_Sales(_BU)
    def fba_fill_OS_Sales_tables(self, cr, uid, s_line, server_id, line_no, return_flag=False):
        server_data = self.pool.get('fetchdb.server')

        order = s_line.order_id
        partner_ref = order.partner_id and order.partner_id.ref or None

        # возможно default_code = vendorSku
        delmar_id = s_line.product_id and s_line.product_id.default_code or None
        prod_size = s_line.size_id and s_line.size_id.name
        four_digit_size = (
            prod_size and float(prod_size) and
            re.sub('\.', '', "%05.2f" % float(prod_size)) or '0000'
        )
        price_unit = s_line.price_unit and float(s_line.price_unit) or 0

        def concat_delmar_id_size(delmar_id, size):
            return (delmar_id + "/" + size) if delmar_id and size else delmar_id

        itemid = concat_delmar_id_size(delmar_id, prod_size)
        itemid_long_size = concat_delmar_id_size(delmar_id, four_digit_size)

        shp_date = datetime.strptime(order.create_date, '%Y-%m-%d %H:%M:%S')
        year = shp_date.year or None
        month = shp_date.month or None
        quarter = month and (month + 2) / 3 or None

        abs_qty = s_line.product_uom_qty and int(s_line.product_uom_qty) or None
        signed_qty = abs_qty
        if signed_qty and return_flag:
            signed_qty *= -1

        if return_flag:
            utc_time_now = time.strftime('%Y%m%d%H:%M:%S', time.gmtime())
            track_no = 'RET' + utc_time_now + 'U'
            order_date = datetime.now(timezone('Canada/Central'))
        else:
            track_no = order.tracking_ref or None
            canada_delta_hour = int(str(abs(int(datetime.now(pytz.timezone('Canada/Central')).strftime('%z'))))[:1])
            order_date = datetime.strptime(order.create_date, '%Y-%m-%d %H:%M:%S')-timedelta(hours=canada_delta_hour)

        # Sales report
        order_year = order_date.year
        order_month = order_date.month
        order_quarter = month and (month + 2) / 3 or None
        order_week = order_date.isocalendar()[1]  # ISO week number
        return_qty = abs_qty if return_flag else 0

        table_name = 'OS_Sales_BU'
        insert_values = {
            'ID_Delmar': delmar_id,
            'Size': four_digit_size,
            'Date': shp_date,
            'Qty': signed_qty,
            'Cost': price_unit * abs_qty or 0,
            'order_ID': order.external_customer_order_id or None,
            'track_no': track_no,
            'Line_ID': s_line.external_customer_line_id or line_no,
            'customer_ID': partner_ref,
            'customer_SKU': itemid,
            'customer_CODE': delmar_id,
            'customer_ITEM': itemid,
            'Year': year,
            'Month': month,
            'Quarter': quarter,
            'ItemID': itemid_long_size,
            'ReturnQuantity': return_qty,
            'order_date': order_date,
            'order_year': order_year,
            'order_month': order_month,
            'order_quarter': order_quarter,
            'order_week': order_week,
        }
        result = server_data.insert_record(
            cr, uid, server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        if not result:
            return False

        table_name = 'OS_Sales'
        insert_values.update({
            'status': 0,
            'trnid': None,
        })
        result = server_data.insert_record(
            cr, uid, server_id,
            table_name, insert_values,
            select=False, commit=True
        )
        return bool(result)

    def check_order_status(self, cr, uid, sale_id=None, context=None):
        sql = """
            select id
            from sale_order
            where state in ('manual', 'progress', 'shipping_except')
                and id in (
                    select sp.sale_id
                    from (
                        select sp.sale_id, count(*) as cnt
                        from stock_picking sp
                        {} group by sale_id
                    ) sp
                    left join (
                        select sale_id, count(*) as cnt
                        from stock_picking where state in ('done', 'final_cancel', 'returned')
                        {} group by sale_id
                    ) a on a.sale_id = sp.sale_id
                    where sp.cnt = a.cnt
                )
         """
        if sale_id:
            sql = sql.format('where sale_id={}'.format(sale_id), 'and sale_id={}'.format(sale_id))
        else:
            sql = sql.format('', '')
        cr.execute(sql)
        rows = cr.fetchall()
        for row in rows:
            self.log(cr, uid, row[0], 'Close sale order %s' % (row[0]))
            # self.do_remote_done(cr, uid, [row[0]])
            self.update_tracking(cr, uid, [row[0]])
            self.update_shp_date(cr, uid, [row[0]], context=context)
            self.set_done(cr, uid, [row[0]], context=context)

        return True

    def action_print_labels_report(self, cr, uid, ids, context=None):
        line_ids = []

        for order in self.read(cr, uid, ids, ['order_line']):
            line_ids += order['order_line'] or []

        res = self.pool.get('sale.order.line').action_print_labels_report(
            cr, uid, line_ids,
            context=context)

        return res

    def action_print_marked_lines(self, cr, uid, ids, context=None):
        res = False
        if not context:
            context = {}

        report_type = context.get('report_type', 'label')
        if report_type not in ('label', 'picking'):
            raise osv.except_osv(_('Warning !'), 'Can\'t print report. Selected wrong report type.')

        if ids and len(ids) == 1:
            order = self.read(cr, uid, ids[0], ['order_line'])
            selected_ids = set(context.get('selected_ids', False) or [])
            order_line_ids = set(order and order.get('order_line', False) or [])

            line_ids = list(order_line_ids & selected_ids)
            if not line_ids:
                raise osv.except_osv(_('Warning !'), 'Please select at least one line to print')
            else:
                if report_type == 'label':
                    res = self.pool.get('sale.order.line').action_print_labels_report(
                        cr, uid, line_ids,
                        context=context)
                elif report_type == 'picking':
                    res = self.pool.get('sale.order.line').action_print_picking_lines(
                        cr, uid, ids,
                        context=context)
        else:
            raise osv.except_osv(_('Warning !'), 'Print may only be done for one order')

        return res

    def cron_process_flash_order(self, cr, uid, context=None):
        logger.info("Cron Process Flash Order")
        sale_obj = self.pool.get('sale.order')
        picking_obj = self.pool.get('stock.picking')

        bad_states = [
            'confirmed',
            'waiting_split',
            'assigned',
            'back',
            'waiting_split',
            'address_except',
            'incode_except',
        ]

        if context is None:
            context = {}
        sale_ids = sale_obj.search(cr, uid, [('flash_processed', '=', True), ('flash_delivery_processed', '=', False)])
        if sale_ids:
            for order in self.browse(cr, uid, sale_ids, context=context):
                carrier_code = order.partner_id and order.partner_id.flash_carrier_code

                if order.picking_ids:
                    picking_ids = [x.id for x in order.picking_ids]

                    picking_obj.write(cr, uid, picking_ids, {
                        'tracking_ref': order.tracking_ref,
                        'carrier': carrier_code,
                        'carrier_code': carrier_code,
                        'fixed_shp_code': True
                    })

                    picking_obj.do_split_zero(cr, uid, picking_ids)
                    picking_obj.check_incoming_code(cr, uid, picking_ids)

                    self._cron_process_flash_delivery(cr, uid, order.id, context=context)

                    not_processed_ids = picking_obj.search(
                        cr, uid, [
                            ('id', 'in', picking_ids),
                            ('state', 'in', bad_states)
                        ])
                    if not not_processed_ids:
                        sale_obj.write(cr, uid, order.id, {'flash_delivery_processed': True})

    def _cron_process_flash_delivery(self, cr, uid, sale_order_id, context=None):
        order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        wf_service = netsvc.LocalService("workflow")
        for pick in order.picking_ids:
            if pick.state in ('cancel', 'final_cancel'):
                continue
            if pick.state == 'confirmed':
                self.action_assign(cr, uid, [pick.id])
            if pick.state == 'waiting_split':
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_recheck', cr)

            self.pool.get('stock.picking').action_process(cr, uid, [pick.id], context=context)

    def ship_flash_order(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sale_integration_obj = self.pool.get('sale.integration')

        timestamp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        for order in self.browse(cr, uid, ids, context=context):
            problematic_items = order.flash_not_ready_items
            if problematic_items:
                raise osv.except_osv(
                    'Warning!',
                    'Order not ready to be shipped. Please, check following items: %s' % (problematic_items)
                )
            if not order.tracking_ref:
                raise osv.except_osv(_('Warning !'), 'Empty tracking number')
            carrier_code = order.partner_id and order.partner_id.flash_carrier_code
            self.write(cr, uid, order.id, {'shp_date': timestamp})

            sale_integration_obj.integrationApiConfirmShipmentFlash(cr, uid, order.id, order.tracking_ref, carrier_code)
        return True

    def flash_print_packing_slip(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        picking_report_ids = []
        picking_obj = self.pool.get("stock.picking")

        for sale_order in self.browse(cr, uid, ids, context=context):
            for picking in sale_order.picking_ids:
                if picking.state not in ('cancel', 'final_cancel'):
                    picking_report_ids.append(picking.id)

        if not picking_report_ids:
            raise osv.except_osv(
                _('Warning !'),
                """All delivery orders associated with this sale order in Cancel or Final Cancel states.
                Please start them again, after that you can print packing slip."""
            )

        context.update({'process_order': False})
        report_name = "flash.stock.picking.list"
        res = picking_obj.create_picking_repot(cr, uid, picking_report_ids, report_name=report_name, context=context)

        if not context.get('batch_report', False):
            service = netsvc.LocalService('report.' + report_name)
            (result, format) = service.create(cr, uid, picking_report_ids, data={}, context=context)
            if result:
                order_names = [str(pick.name) for pick in picking_obj.browse(cr, uid, picking_report_ids)]
                wh_id = picking_obj.get_warehouse(cr, uid, picking_report_ids[0])
                notes_str = self.pool.get('res.users').browse(cr, uid, uid).name + ' manually printed ' + str(len(picking_report_ids)) + ' orders: ' + ', '.join(order_names)
                self.pool.get('picking.export').create(cr, uid, {
                    'pdf_filename': 'flash_printed_packslips.pdf',
                    'pdf_file': base64.b64encode(result),
                    'state': 'printed',
                    'status': 'With Flash screen printed',
                    'notes': notes_str,
                    'scheduled_datetime': datetime.utcnow(),
                    'printed_user': uid,
                    'warehouse': [wh_id],
                })

        return res

    def on_search(self, cr, uid, ids, warehouse_search=None, delmar_id_search=None, line_state_search=None, line_used_search=None, context=None):
        if ids:
            if len(ids) > 1:
                raise osv.except_osv(_('Warning !'), 'Search may only be done for one bulk')
            order_id = ids[0]
            search_result = self.get_search_lines_ids(cr, uid, ids, warehouse_search=warehouse_search, delmar_id_search=delmar_id_search, line_state_search=line_state_search, line_used_search=line_used_search, context=None)
            not_ready_count = self.get_flash_not_ready_count(cr, uid, ids, context=None)

            return {
                'value': {
                    'order_line': search_result and search_result[order_id],
                    'flash_not_ready_count': not_ready_count and not_ready_count[order_id],
                }
            }

    def get_search_lines_ids(self, cr, uid, ids, warehouse_search=None, delmar_id_search=None, line_state_search=None, line_used_search=None, context=None):
        res = {}

        if not ids:
            return res

        if isinstance(ids, (int, long)):
            ids = [ids]

        for order_id in ids:
            res[order_id] = []

        where = []
        if line_used_search:
            if line_used_search == 'used':
                where.append('AND sol.used IS True')
            elif line_used_search == 'not_used':
                where.append('AND (sol.used IS NOT True)')

        if line_state_search:
            if line_state_search in ('waiting', 'waiting_positive'):
                where.append("AND sm.state in ('draft', 'waiting', 'confirmed')")
            elif line_state_search == 'allocated':
                where.append("AND sm.state in ('assigned', 'done')")

        if warehouse_search:
            try:
                warehouse_id = int(warehouse_search)
                where.append("AND loc.warehouse_id = %d " % (warehouse_id,))
            except ValueError:
                pass

        if delmar_id_search:
            search_items = re.findall(r"[\w-]+", delmar_id_search)
            if search_items:
                where.append(
                    "AND (%s)" % (
                        " OR ".join(["p.default_code ilike \'%%%%%s%%%%\'" % (str(x)) for x in search_items])
                    )
                )

        for sale_id in ids:
            cr.execute("""  SELECT distinct sol.id as line_id
                            FROM sale_order_line sol
                                INNER JOIN sale_order so
                                    ON sol.order_id = so.id
                                LEFT JOIN stock_move sm
                                    ON sm.sale_line_id = sol.id
                                LEFT JOIN stock_location loc
                                    ON sm.location_id = loc.id
                                LEFT JOIN product_product p
                                    ON sol.product_id = p.id
                            WHERE 1>0
                                AND so.id = %%s %s
                            """ % (" ".join(where), ),
                            (sale_id,)
            )

            result = cr.fetchall() or []
            if line_state_search == 'waiting_positive':
                sale_line = self.pool.get('sale.order.line')
                sale_line_ids = self.pool.get('sale.order').read(cr, uid, sale_id, ['order_line'])['order_line']
                sale_lines_qty = sale_line.get_real_qty(cr, uid, sale_line_ids, context)
                res[sale_id] = [r[0] for r in result if r and sale_lines_qty[r[0]] > 0]
            else:
                res[sale_id] = [r[0] for r in result if r]

        return res

    def clear_flash_filters(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'warehouse_search': False,
            'delmar_id_search': False,
            'line_state_search': False,
            'line_used_search': False,
            })

    def get_flash_not_ready_count(self, cr, uid, ids, context=None):
        res = {}
        if ids:
            for order_id in ids:
                res[order_id] = 0

            cr.execute("""
                SELECT
                    so.id as order_id,
                    COUNT(distinct ol.id) as not_ready_count
                FROM sale_order so
                    LEFT JOIN sale_order_line ol on ol.order_id = so.id
                    LEFT JOIN stock_move sm on ol.id = sm.sale_line_id
                WHERE 1=1
                    AND so.id in %s
                    AND so.flash IS True
                    AND ((
                        ol.used IS NOT TRUE and COALESCE(ol.product_uom_qty, 0) > 0
                        ) OR (
                        ol.used and sm.state not in ('done', 'assigned') and sm.product_qty > 0
                    ))
                GROUP BY
                    so.id
                ;
            """, (tuple(ids), ))
            result = cr.dictfetchall() or []
            for r in result:
                res[r['order_id']] = r.get('not_ready_count', 0)
            for order_id in ids:
                if self.flash_all_lines_are_zeros(cr, uid, order_id):
                    res[order_id] = -1

        return res

    def get_flash_not_ready_items(self, cr, uid, ids, context=None):
        res = {}
        if ids:
            for order_id in ids:
                res[order_id] = False

            cr.execute("""
                SELECT
                    so.id as order_id,
                    string_agg(concat(
                        case when ol.used is not True then 'Item not used'
                             when sm.state not in ('done', 'assigned') then 'Item not allocated'
                        end,
                        ': ',
                        pp.default_code, '/', coalesce(rs.name, '0')
                    ), ', ') as items
                FROM sale_order so
                    LEFT JOIN sale_order_line ol on ol.order_id = so.id
                    LEFT JOIN stock_move sm on ol.id = sm.sale_line_id
                    LEFT JOIN product_product pp on sm.product_id = pp.id
                    LEFT JOIN ring_size rs on rs.id = sm.size_id
                WHERE 1=1
                    AND so.id in %s
                    AND so.flash IS True
                    AND ((
                        ol.used IS NOT TRUE and COALESCE(ol.product_uom_qty, 0) > 0
                        ) OR (
                        ol.used = true and sm.state not in ('done', 'assigned') and sm.product_qty > 0
                    ))
                    AND pp.id is not null
                GROUP BY
                    so.id
                ;
            """, (tuple(ids), ))
            result = cr.dictfetchall() or []
            for r in result:
                res[r['order_id']] = r['items']

        return res

    def flash_all_lines_are_zeros(self, cr, uid, order_id, context=None):
        res = True
        lines = self.browse(cr, uid, order_id, context=context).order_line
        for line in lines:
            if line.product_uom_qty > 0:
                res = False
                break
        return res

    def action_confirmation(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        assert len(ids) == 1, 'Revert may only be done one at a time'

        cur_id = ids[0]

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        context = dict(context, active_ids=ids, active_model=self._name)

        context.update({
            'flags': {
                'action_buttons': False,
                'deletable': False,
                'pager': False,
                'views_switcher': False,
                'can_be_discarded': True
            }, 'target': 'new',
        })

        wizard_id = self.pool.get("confirmation.flash.order").create(cr, uid, {'sale_id': cur_id}, context=context)

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_confirmation_flash_order')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': wizard_id,
            'context': context,
            })
        return act_win

    def action_revert_flash(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        assert len(ids) == 1, 'Revert may only be done one at a time'

        cur_id = ids[0]

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        context = dict(context, active_ids=ids, active_model=self._name)

        context.update({
            'flags': {
                'action_buttons': False,
                'deletable': False,
                'pager': False,
                'views_switcher': False,
                'can_be_discarded': True
            }, 'target': 'new',
        })

        wizard_id = self.pool.get("revert.flash.order").create(cr, uid, {'sale_id': cur_id}, context=context)

        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_revert_flash_order')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': wizard_id,
            'context': context,
            })
        return act_win

    def get_sale_transaction_code(self, cr, uid, order_id, sign=-1, context=None):
        if not order_id:
            return SIGN_DEFAULT_VALUE

        if sign == SIGN_CANCEL:  # Cancel action code
            return SIGN_CANCEL_VALUE

        if sign != -1:
            return SIGN_DEFAULT_VALUE

        cr.execute("""
            select case when rc.code = 'CA' then 'SHP' else 'SLE' end as type
            from sale_order so
                left join res_partner_address ra on so.partner_shipping_id = ra.id
                left join res_country rc on rc.id = ra.country_id
            where so.id = %(order_id)s
        """, {
            'order_id': order_id,
        })

        res = cr.fetchone()
        return res and res[0] or SIGN_DEFAULT_VALUE

sale_order()


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"
    _description = 'Delmar Sales Order Line'

    def write(self, cr, user, ids, vals, context=None):
        if 'price_unit' in vals:
            line = self.browse(cr, user, ids[0])
            if not line.cf_price_unit > 0:
                vals.update({'cf_price_unit': vals.get('price_unit')})
        return super(sale_order_line, self).write(cr, user, ids, vals, context)

    def _get_size_ids(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        if ids:
            for rec in self.browse(cr, uid, ids, context=context):
                size_ids = rec.product_id and rec.product_id.ring_size_ids or False
                if size_ids:
                    size_ids = ",".join(str(x.id) for x in size_ids)
                result[rec.id] = size_ids
        return result

    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
        uom=False, qty_uos=0, uos=False, name='', partner_id=False,
        lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):

        context = context or {}
        lang = lang or context.get('lang', False)
        if not partner_id:
            raise osv.except_osv(_('No Customer Defined !'), _('You have to select a customer in the sales form !\nPlease set one customer before choosing a product.'))
        warning = {}
        product_uom_obj = self.pool.get('product.uom')
        partner_obj = self.pool.get('res.partner')
        product_obj = self.pool.get('product.product')
        context = {'lang': lang, 'partner_id': partner_id}
        if partner_id:
            lang = partner_obj.browse(cr, uid, partner_id).lang
        context_partner = {'lang': lang, 'partner_id': partner_id}

        if not product:
            return {
                'value': {
                    'th_weight': 0,
                    'product_packaging': False,
                    'product_uos_qty': qty,
                },
                'domain': {
                    'product_uom': [],
                    'product_uos': [],
                }
            }
        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        res = self.product_packaging_change(cr, uid, ids, pricelist, product, qty, uom, partner_id, packaging, context=context)
        result = res.get('value', {})
        warning_msgs = res.get('warning') and res['warning']['message'] or ''
        product_obj = product_obj.browse(cr, uid, product, context=context)

        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(cr, uid, uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(cr, uid, uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False
        if product_obj.description_sale:
            result['notes'] = product_obj.description_sale
        fpos = fiscal_position and self.pool.get('account.fiscal.position').browse(cr, uid, fiscal_position) or False
        if update_tax:  # The quantity only have changed
            result['delay'] = (product_obj.sale_delay or 0.0)
            result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, product_obj.taxes_id)
            result.update({'type': product_obj.procure_method})

        if not flag and not name:
            result['name'] = self.pool.get('product.product').name_get(cr, uid, [product_obj.id], context=context_partner)[0][1]
        domain = {}
        if (not uom) and (not uos):
            result['product_uom'] = product_obj.uom_id.id
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
                uos_category_id = product_obj.uos_id.category_id.id
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
                uos_category_id = False
            result['th_weight'] = qty * product_obj.weight
            domain = {
                'product_uom': [('category_id', '=', product_obj.uom_id.category_id.id)],
                'product_uos': [('category_id', '=', uos_category_id)]
            }

        elif uos and not uom:  # only happens if uom is False
            result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
            result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
            result['th_weight'] = result['product_uom_qty'] * product_obj.weight
        elif uom:  # whether uos is set or not
            default_uom = product_obj.uom_id and product_obj.uom_id.id
            q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
            result['th_weight'] = q * product_obj.weight        # Round the quantity up

        if not uom2:
            uom2 = product_obj.uom_id
        compare_qty = float_compare(product_obj.virtual_available * uom2.factor, qty * product_obj.uom_id.factor, precision_rounding=product_obj.uom_id.rounding)
        if (product_obj.type=='product') and int(compare_qty) == -1 \
          and (product_obj.procure_method=='make_to_stock'):
            warn_msg = _('You plan to sell %.2f %s but you only have %.2f %s available !\nThe real stock is %.2f %s. (without reservations)') % \
                    (qty, uom2 and uom2.name or product_obj.uom_id.name,
                     max(0,product_obj.virtual_available), product_obj.uom_id.name,
                     max(0,product_obj.qty_available), product_obj.uom_id.name)
            warning_msgs += _("Not enough stock !: ") + warn_msg + "\n\n"
        # get unit price

        if not pricelist:
            warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
                    'Please set one before choosing a product.')
            warning_msgs += _("No Pricelist !: ") + warn_msg +"\n\n"
        else:
            price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                    product, qty or 1.0, partner_id, {
                        'uom': uom or result.get('product_uom'),
                        'date': date_order,
                        })[pricelist]
            if price:
                result.update({'price_unit': price})
        if warning_msgs:
            warning = {
                'title': _('Configuration Error !'),
                'message': warning_msgs
            }
        res = {'value': result, 'domain': domain, 'warning': warning}

        size_ids_arr = []
        if product:
            size_ids = self.pool.get('product.product').browse(cr, uid, product).ring_size_ids
            for size_obj in size_ids:
                size_ids_arr.append(str(size_obj.id))
            size_ids_str = ','.join(size_ids_arr)
            res['value'].update({'size_ids': size_ids_str})

        if not size_ids_arr:
            res['value'].update({'size_id': None})

        res['value'].update({'delay': 0.0})

        return res

    def _get_available_qty(self, cr, uid, ids, name, arg, context=None):
        res = {}

        if not ids:
            return res

        list_ids = []
        for sale_line_id in ids:
            res[sale_line_id] = 0
            list_ids.append(str(sale_line_id))

        sale_obj = self.pool.get('sale.order')
        str_ids = ",".join(list_ids)

        sql_get_items = """ SELECT DISTINCT product_id AS product_id, size_id AS size_id
                            FROM sale_order_line
                            WHERE id in (%s);
                        """ % (str_ids)
        # print "Get items sql:\n %s" % (sql_get_items)
        cr.execute(sql_get_items)
        items = cr.dictfetchall() or []
        non_sync = sale_obj.check_sync_product(cr, uid, ids)
        for item in items:
            if item.get('product_id', False) and item in non_sync:
                sale_obj.sync_product_stock(cr, uid, item['product_id'], item.get('size_id', False))

        sql_get_customer_settings = """ SELECT rp.id AS partner_id,
                                        array_to_string(array_accum( DISTINCT sol.id), ',') AS sol_ids,
                                        array_to_string(array_accum( DISTINCT clr.location_id), ',') AS location_ids
                                    FROM sale_order_line sol
                                        left JOIN sale_order so ON (sol.order_id = so.id)
                                        left JOIN res_partner rp ON (so.partner_id = rp.id)
                                        left JOIN customer_location_relation clr ON (clr.customer_id = rp.id)
                                    WHERE 1=1
                                        AND sol.id IN (%s)
                                    GROUP BY rp.id;
        """ % (str_ids)
        # print "Get customer sewttings sql:\n %s" % (sql_get_customer_settings)
        cr.execute(sql_get_customer_settings)
        customer_settings = cr.dictfetchall() or []
        for settings in customer_settings:

            if not settings.get('sol_ids', False) or not settings.get('location_ids', False):
                continue

            sql_get_count = """ SELECT line_id AS line_id, SUM(qty) AS qty
                                FROM (
                                    SELECT ol.id AS line_id,
                                        sm.product_id,
                                        sm.size_id,
                                        CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END AS location_dest_id,
                                        SUM(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as qty
                                    FROM sale_order_line ol
                                        LEFT JOIN stock_move sm ON (
                                            ol.product_id = sm.product_id
                                            AND COALESCE(ol.size_id, -1) = COALESCE(sm.size_id, -1)
                                        )
                                        LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                                    WHERE 1=1
                                        AND sm.state IN ('done', 'reserved', 'assigned')
                                        AND ( sm.location_dest_id IN (%s) OR sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                                        AND sm.bin IS NOT NULL
                                        AND sm.bin <> ''
                                        AND ol.id IN (%s)
                                    GROUP BY ol.id,
                                        sm.product_id,
                                        sm.size_id,
                                        (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END)
                                ) a
                                WHERE location_dest_id IN (%s)
                                GROUP BY line_id;
            """ % (settings['location_ids'], settings['sol_ids'], settings['location_ids'])
            # print "Get count sql:\n %s" % (sql_get_count)
            cr.execute(sql_get_count)
            res_qty = cr.dictfetchall() or []
            for row in res_qty:
                if row.get('line_id', False) and row.get('qty', False):
                    res[row['line_id']] = row['qty']

        return res

    def _get_type_api_order(self, cr, uid, ids, name, arg, context=None):
        res = {}
        sale_lines = self.browse(cr, uid, ids)
        for line in sale_lines:
            if (line.order_id and line.order_id.type_api):
                res[line.id] = line.order_id.type_api
        return res

    def _location_and_bin(self, cr, uid, ids, name, args, context=None):
        res = {}

        cr.execute("""
            select id, string_agg(locations, ', ') as locations
            from (
                select ol.id, concat(
                        replace(sl.complete_name, 'Physical Locations / ', ''), ' / ',
                        coalesce(sb.name, sm.bin, '')
                    )as locations
                from sale_order_line ol
                    left join stock_move sm on sm.sale_line_id = ol.id
                    left join stock_location sl on sl.id = sm.location_id
                    left join stock_bin sb on sb.id = sm.bin_id
                where ol.id in %(line_ids)s
                group by ol.id, sl.id, coalesce(sb.name, sm.bin, '')
            ) dd
            group by dd.id
        """, {
            'line_ids': tuple(ids),
        })
        for line_id, location in cr.fetchall() or []:
            res[line_id] = location.split('/') or []
            # Wrap bypass
            max_line_length = 13
            a = list()
            for x in res[line_id]:
                x = x.strip()
                for i in range(max_line_length, len(x), max_line_length):
                    x = '{}\n{}'.format(x[:i], x[i:])
                a.append(x)
            res[line_id] = a
        return res

    def _move_states(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for line_id in ids:
                res[line_id] = False

            cr.execute(
                """ SELECT
                        ol.id as line_id,
                        array_to_string(array_accum(DISTINCT sm.state), ', ') as states
                    FROM sale_order_line ol
                        left join stock_move sm on sm.sale_line_id = ol.id
                    WHERE ol.id in %s
                    GROUP BY ol.id
                """, (tuple(ids), ))
            sql_res = cr.dictfetchall() or []
            for r in sql_res:
                res[r['line_id']] = r.get('states', False)
        return res

    def _get_mssql_qty(self, cr, uid, ids, name, args, context=None):
        return self.get_real_qty(cr, uid, ids, context=context)

    def _picking_note(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids):
            if line.move_ids:
                move = line.move_ids[0]
                res[line.id] = move.picking_note or ''
            else:
                res[line.id] = ''
        return res

    def _b2b_need(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids):
            if 'confirmed' in line.move_states and line.mssql_qty >= line.product_uom_qty and line.product_uom_qty > 0:
                res[line.id] = True
            else:
                res[line.id] = False
        return res

    _columns = {
        'item_line_id': fields.char('Order Item Line ID', size=256),
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'size_ids': fields.function(_get_size_ids, type='char', method=True, ),
        'state': fields.selection([
            ('cancel', 'Cancelled'),
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('exception', 'Exception'),
            ('rejected', 'Rejected'),
            ('done', 'Done')
            ], 'State', required=True, readonly=True,
                help='* The \'Draft\' state is set when the related sales order in draft state. \
                    \n* The \'Confirmed\' state is set when the related sales order is confirmed. \
                    \n* The \'Exception\' state is set when the related sales order is set as exception. \
                    \n* The \'Done\' state is set when the sales order line has been picked. \
                    \n* The \'Cancelled\' state is set when a user cancel the sales order related. \
                    \n* The \'Rejected\' state is set when the related sale order has bad shipping address.'),
        'available_qty': fields.function(_get_available_qty, type="integer", string="Available Qty", store=False,),
        'mssql_qty': fields.function(_get_mssql_qty, type="integer", string="Available Qty", store=False,),
        'product_image': fields.related('product_id', 'product_image',
            type='binary', string='Image', store=False,),
        'product_uom_qty': fields.float('Quantity (UoM)', digits_compute= dp.get_precision('Product UoS'), required=True, readonly=False),
        'product_order_qty': fields.integer('Requested Qty'),
        'product_po_order_qty': fields.integer('Quantity in PO order'),
        'type_api_order': fields.function(_get_type_api_order, type="char", size=256, store=False),
        'location_and_bin': fields.function(_location_and_bin, string="Location/Bin", type='char', size=512, store=False),  # Flash
        'move_states': fields.function(_move_states, string="Move states", type="char", store=False, ),
        'picking_note': fields.function(_picking_note, string="Notes", type="text", store=False, ),
        'used': fields.boolean('Picked'),  # Flash
        'use': fields.boolean('Use'),  # Flash
        'shp_date': fields.related('order_id', 'shp_date', type='datetime', string='Ship Date', store=False),  # Flash
        'b2b_need': fields.function(_b2b_need, string="Bin-to-bin needed", type="boolean", store=False, ),
    }

    def get_real_qty(self, cr, uid, ids, context=None):
        res = {}

        if not ids:
            return res

        so_lines = self.pool.get('sale.order.line').browse(cr, uid, ids)
        if not so_lines:
            return res

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)
        if not server_id:
            return res

        cache_array = {}
        for so_line in so_lines:
            res[so_line.id] = 0
            if so_line:
                if so_line.product_id.id:
                    size_str = '0000'
                    size_id = False
                    if so_line.size_id and so_line.size_id.id:
                        size_id = so_line.size_id.id
                        size_str = re.sub('\.', '', "%05.2f" % float(so_line.size_id.name))
                    index = "%s/%s" % (so_line.product_id.default_code, size_str)
                    if cache_array.get(index, False):
                        cache_array[index]['line_id'].append(so_line.id)
                    else:
                        cache_array[index] = {
                            'search_pair': "('%s', '%s')" % (so_line.product_id.default_code, size_str),
                            'size_id': size_id,
                            'product_id': so_line.product_id.id,
                            'line_id': [so_line.id]
                        }

        if so_lines[0].is_set and so_lines[0].order_id.partner_id is None:
            complete_names = ["('{warehouse}', '{stock}')".format(warehouse=x.warehouse_id.name, stock=x.name) for x in
                              so_lines[0].set_parent_order_id.partner_id.location_ids]
        else:
            complete_names = ["('{warehouse}', '{stock}')".format(warehouse=x.warehouse_id.name, stock=x.name) for x in so_lines[0].order_id.partner_id.location_ids]

        mssql_inventory_sql = """
            select concat(items.item, '/', items.size), sum(tr.adjusted_qty)
            from (values %s) as items(item, size )
                full outer join (values %s) as stock(warehouse, stockid) on 1=1
                left join transactions tr
                    on tr.id_delmar = items.item
                    and tr.size = items.size
                    and tr.status <> 'Posted'
                    and tr.warehouse = stock.warehouse
                    and tr.stockid = stock.stockid
            where 1=1
            group by items.item, items.size

        """ % (
            ",".join([x['search_pair'] for i, x in cache_array.iteritems()]),
            ",".join(complete_names)
        )
        mssql_inventory = server_data.make_query(cr, uid, server_id, mssql_inventory_sql, select=True, commit=False)
        if mssql_inventory:
            for item_qty in mssql_inventory:
                if item_qty:
                    if cache_array.get(item_qty[0], False):
                        for line_id in cache_array[item_qty[0]]['line_id']:
                            res[line_id] = int(item_qty[1] or 0)
        return res

    def action_print_labels_report(self, cr, uid, ids, context=None):
        res = False
        data_obj = self.pool.get('ir.model.data')
        category = data_obj.get_object(cr, uid, 'delmar_sale', 'cm_report_categ_ol')
        if not category:
            raise osv.except_osv(_('Warning !'), 'Can\'t find report category')

        if ids:
            res = {
                'type': 'ir.actions.report.xml',
                'report_name': 'sale.order.individual.label',
                'datas': {
                    'model': 'sale.order.line',
                    'report_type': 'pdf',
                    'ids': ids,
                    'id': ids[0],
                },
                'context': {
                    'active_ids': ids,
                    'categ_id': category.id,
                }
            }

        return res

    def action_print_picking_lines(self, cr, uid, ids, context=None):
        res = True

        if ids:
            self.action_mark_flash_lines(cr, uid, context.get('selected_ids'), context=context)

            res = {
                'type': 'ir.actions.report.xml',
                'report_name': 'sale.order.line.flash_picking_labels_report',
                'datas': {
                    'model': 'sale.order',
                    'report_type': 'pdf',
                    'ids': ids,
                    'id': ids[0],
                },
                'context': {
                    'active_ids': ids,
                }
            }

        return res

    def action_mark_flash_lines(self, cr, uid, ids, context=None):
        if ids:
            update_line_ids = []
            allowed_states = ['assigned', 'done']
            lines = self.read(cr, uid, ids, ['product_uom_qty', 'move_states'])
            for line in lines:
                if line.get('product_uom_qty', False) and line.get('move_states', False) in allowed_states:
                    update_line_ids.append(line['id'])

            if update_line_ids:
                self.write(cr, uid, update_line_ids, {'used': True})

        return True

    def action_unmark_flash_lines(self, cr, uid, ids, context=None):
        if ids:
            self.write(cr, uid, ids, {'used': False})
        return True

    def open_flash_screen_view(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        # import pdb; pdb.set_trace()

        # new_id = self.pool.get('import.fields.columns.mapping').create(
        #    cr,
        #    uid,
        #    {
        #        'columns_mapping_line_ids': mapping_line_ids,
        #        'delmar_id_column': default_delmar_id_column,
        #        'size_column': default_size_column,
        #        'prepare_step_id': prepare_data_id,
        #    }
        # )

        # find form for the next step
        act_win_res_id = data_obj._get_id(cr, uid, 'delmar_sale', 'action_flash_form_mode')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)

        act_win.update({
            'res_id': context.get('order_id', False),
            # 'view_type': 'page',
            # 'view_mode': 'page',
            # 'view_id': 'view_flash_form'
        })

        # return act_win

    def action_open_stock_move_flash(self, cr, uid, ids, context=None):
        context = context or {}
        context['target'] = 'new'

        for line in self.browse(cr, uid, ids):
            product_id = line.product_id and line.product_id.id or False
            if product_id:
                return self.pool.get('stock.product.move').get_act_win(cr, uid, 'action_open_stock_product_report_new', product_id, context=context)

    def action_flash_partial_reserve(self, cr, uid, ids, context=None):
        local_context = context and dict(context) or {}
        if local_context.get('form_view_ref'):
            del local_context['form_view_ref']
        move_ids = []
        for line in self.browse(cr, uid, ids):
            move_ids += [move.id for move in line.move_ids]

        res = self.pool.get('stock.move').action_openep_partial_reserve(cr, uid, move_ids, local_context)
        return res

    def remove_decimal_point(self, cr, uid, ids):
        if ids:
            change_qty = {}
            try:
                so_lines = self.pool.get('sale.order.line').browse(cr, uid, ids)
                for so_line in so_lines:
                    change_qty = {
                        'change_qty' : int(so_line.product_uom_qty)
                    }
                return change_qty
            except Exception, e:
                print e

    def get_custom_field(self, cr, uid, ids, args):
        sku = False
        if ids:
            so_lines = self.pool.get('sale.order.line').browse(cr, uid, ids)
            try:
                for so_line in so_lines:
                    product_id = so_line and so_line.product_id and so_line.product_id.id or False
                    size_id = so_line and so_line.size_id and so_line.size_id.id or False
                    partner_id = so_line and so_line.order_id and so_line.order_id.partner_id and so_line.order_id.partner_id.id or False
                    sku = {
                        'warehouse': so_line.location_and_bin and so_line.location_and_bin[0] or '',
                        'location': so_line.location_and_bin and so_line.location_and_bin[2] or '',
                        'bin': so_line.location_and_bin and so_line.location_and_bin[3] or '',
                        'customer_sku': self.pool.get('product.product').get_val_by_label(
                             cr, uid,
                             product_id,
                             partner_id,
                             args,
                             size_id
                        )
                    }
            except:
                sku = False
        return sku

    def get_user_name(self, cr, uid, ids):
        name = False
        if ids:
            user_id = self.pool.get('res.users').search(cr, uid,[('id', '=', uid)])
            if user_id:
                result = self.pool.get('res.users').read(cr, uid, user_id, ['name'])
                for line in result:
                    name = {
                        'user_name': line.get('name', False)
                    }
                    break
        return name

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        ids = []
        if context is not None and context.get('flash_view') == 1:
            for item in args:
                if item[0] == 'order_id':
                    ids = item[2]
                    break
        if len(ids) == 1:
            lines = self.pool.get('sale.order')._get_search_lines_ids(cr, uid, ids, '', args, context=context)
            lines = lines[ids[0]]
        else:
            lines = super(sale_order_line, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context,
                                                count=count)
        return lines

    def open_relocate_wizard(self, cr, uid, ids, context=None):
        # action_openep_partial_reserve from stock_move
        local_context = context and dict(context) or {}
        if local_context.get('form_view_ref'):
            del local_context['form_view_ref']
        if isinstance(ids, (int, long)):
            ids = [ids]
        line = self.browse(cr, uid, ids[0])
        stock_move_ids = [sm.id for sm in line.move_ids]
        if stock_move_ids:
            res = self.pool.get('stock.move').action_openep_partial_reserve(
                cr, uid, stock_move_ids, local_context)
            return res

    def split_set_line_to_components(self, cr, uid, ids, settings=None, context=None):
        if not ids:
            return False

        logger.info('START SPLITTING SALE LINE TO COMPONENTS')

        sale_order_model = self.pool.get('sale.order')
        helper = self.pool.get('stock.picking.helper')
        component_line_ids = []

        for parent_line in self.browse(cr, uid, ids, context=context):
            # skip if not is_set
            if not parent_line.is_set:
                logger.warn('CANNOT SPLIT TO COMPONENTS, LINE IS NOT SET')
                continue

            # get components for set
            components = helper.get_set_product_components(cr, uid, product_id=parent_line.product_id.id)

            # calculate values for components sale line
            set_price_unit = parent_line.price_unit and (float(parent_line.price_unit) / len(components)) or 0
            set_cf_price_unit = parent_line.cf_price_unit and (float(parent_line.cf_price_unit) / len(components)) or 0
            set_customerCost = parent_line.customerCost and (float(parent_line.customerCost) / len(components)) or 0
            set_credit_amount = parent_line.credit_amount and (float(parent_line.credit_amount) / len(components)) or 0
            set_balance_due = parent_line.balance_due and(float(parent_line.balance_due) / len(components)) or 0
            set_full_retail = parent_line.full_retail and (float(parent_line.full_retail) / len(components)) or 0
            set_tax = parent_line.tax and (float(parent_line.tax) / len(components)) or 0
            set_linePrice = parent_line.linePrice and (float(parent_line.linePrice) / len(components)) or 0
            set_shipCost = parent_line.shipCost and (float(parent_line.shipCost) / len(components)) or 0
            set_ice_sub_total = parent_line.ice_sub_total and (float(parent_line.ice_sub_total) / len(components)) or 0
            set_ice_row_total = parent_line.ice_row_total and (float(parent_line.ice_row_total) / len(components)) or 0
            set_ice_tax_amount = parent_line.ice_tax_amount and (float(parent_line.ice_tax_amount) / len(components)) or 0
            set_ice_discount = parent_line.ice_discount and (float(parent_line.ice_discount) / len(components)) or 0

            # hide parent set sale order line
            order_id = parent_line.order_id.id
            logger.info("HIDING PARENT LINE %s FROM ORDER: %s" % (parent_line.id, order_id))
            self.write(cr, uid, parent_line.id, {"order_id": "", "set_parent_order_id": order_id, 'set_product_id': parent_line.product_id.id})

            for component in components:
                logger.info("START CREATE COMPONENT %s" % component.get('default_code'))
                # line params for copying
                component_line_params = {
                    'order_id': order_id,
                    'set_parent_order_id': '',
                    'product_id': component.get('product_id'),
                    'price_unit': set_price_unit / component.get('qty'),
                    'cf_price_unit': set_cf_price_unit / component.get('qty'),
                    'customerCost': set_customerCost / component.get('qty'),
                    'product_uos_qty': component.get('qty'),
                    'product_uom_qty': component.get('qty') * parent_line.product_uom_qty,
                    'set_product_id': parent_line.product_id.id,
                    'credit_amount': set_credit_amount / component.get('qty'),
                    'balance_due': set_balance_due / component.get('qty'),
                    'full_retail': set_full_retail / component.get('qty'),
                    'tax': set_tax,
                    'linePrice': set_linePrice,
                    'shipCost': set_shipCost,
                    'ice_sub_total': set_ice_sub_total,
                    'ice_row_total': set_ice_row_total,
                    'ice_tax_amount': set_ice_tax_amount,
                    'ice_discount': set_ice_discount,
                    'size_id': '',
                    'size': ''
                }
                # check sizeability
                if parent_line.size_id and parent_line.size_id.id and component.get('sizeable'):
                    component_line_params.update({
                        'size_id': parent_line.size_id.id,
                        'size': parent_line.size
                    })
                # api settings hooks
                if settings is not None:
                    if settings.type_api == 'walmart':
                        component_line_params.update({
                            'tax': (set_tax / component.get('qty')),
                            'shipCost': (set_shipCost / component.get('qty'))
                        })
                # create sale order line for component
                logger.info("CREATING NEW SET LINE WITH PARAMS: %s" % component_line_params)
                component_line_id = self.copy(cr, uid, parent_line.id, component_line_params)
                logger.info("SET LINE CREATED WITH ID: %s" % component_line_id)
                component_line_ids.append(component_line_id)

                # update sale order line fields for component lines
                sale_order_model._update_set_components_line_fields(cr, uid, component_line_id, parent_line.id, component.get('qty') * len(components), len(components))

        return component_line_ids


sale_order_line()


class FlashPickingLabel(osv.osv_memory):
    _name = 'flash.sale.order.picking.label'

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=True, ),
        'product_image': fields.char('Product image', size=128, required=False, ),
        'sku': fields.char('Customer SKU', size=128, required=False, ),
        'size_id': fields.many2one('ring.size', 'Size', required=False, ),
        'warehouse': fields.char('Warehouse', size=128, required=True, ),
        'location': fields.char('Location', size=128, required=True, ),
        'bin': fields.char('Bin', size=128, required=False, ),
        'qty': fields.integer('Qty', required=False, )
    }


FlashPickingLabel()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
