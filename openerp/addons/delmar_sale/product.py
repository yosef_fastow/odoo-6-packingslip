from osv import osv, fields
import logging
import base64
from tools.translate import _
import csv
import cStringIO

_logger = logging.getLogger(__name__)


class product_product(osv.osv):
    _inherit = 'product.product'

    def _is_no_resize_check(self, cr, uid, ids, name, args, context=None):
        if not context:
            context = {}
        result = {x: False for x in ids}
        if ids:
            products = self.pool.get('product.product').browse(cr, uid, ids)
            no_resize_gta_value = self.pool.get('ir.config_parameter').get_param(cr, uid, 'no_resize_gta_value') or 0
            no_resize_products_list = self.pool.get('ir.config_parameter').get_param(cr, uid, 'no_resize_products_list') or []
            try:
                no_resize_gta_value = float(no_resize_gta_value)
            except ValueError, e:
                _logger.error('Method: _is_no_resize_check. Error: The system parameter "no_resize_gta_value" is not integer or float!')
                return result
            if (no_resize_gta_value > 0) or no_resize_products_list:
                for product in products:
                    # default check by no_resize_gta_value
                    no_resize_flag = False
                    cost_price_value = product.standard_price or 0.0
                    if ((cost_price_value < no_resize_gta_value) and no_resize_gta_value > 0) or (no_resize_products_list and (product.default_code in no_resize_products_list.split(','))):
                        no_resize_flag = True
                    # append result
                    result.update({product.id: no_resize_flag})
        return result

    _columns = {
        'is_no_resize_check': fields.function(_is_no_resize_check, string="Do not resize item", type='boolean'),
    }

    def get_product_available(self, cr, uid, ids, context=None):
        """ Finds whether product is available or not in particular warehouse.
        @return: Dictionary of values
        """

        if context is None:
            context = {}

        location_obj = self.pool.get('stock.location')
        warehouse_obj = self.pool.get('stock.warehouse')
        shop_obj = self.pool.get('sale.shop')

        states = context.get('states', [])
        what = context.get('what', ())
        bin = None
        if 'bin' in context:
            bin = context['bin'] or ''
        if not ids:
            ids = self.search(cr, uid, [])
        res = {}.fromkeys(ids, 0.0)
        if not ids:
            return res

        if context.get('shop', False):
            warehouse_id = shop_obj.read(cr, uid, int(context['shop']), ['warehouse_id'])['warehouse_id'][0]
            if warehouse_id:
                context['warehouse'] = warehouse_id

        if context.get('warehouse', False):
            lot_id = warehouse_obj.read(cr, uid, int(context['warehouse']), ['lot_stock_id'])['lot_stock_id'][0]
            if lot_id:
                context['location'] = lot_id

        if context.get('location', False):
            if type(context['location']) == int:
                location_ids = [context['location']]
            elif type(context['location']) in (str, unicode):
                location_ids = location_obj.search(cr, uid, [('name', 'ilike', context['location'])], context=context)
            else:
                location_ids = context['location']
        else:
            location_ids = []
            width = warehouse_obj.search(cr, uid, [], context=context)
            for w in warehouse_obj.browse(cr, uid, width, context=context):
                if not w.virtual:
                    location_ids.append(w.lot_stock_id.id)

        # build the list of ids of children of the location given by id
        if context.get('compute_child', True):
            child_location_ids = location_obj.search(cr, uid, [('location_id', 'child_of', location_ids)])
            location_ids = child_location_ids or location_ids

        # ring size
        ring_size_id = context.get('ring_size', False)

        # this will be a dictionary of the UoM resources we need for conversion purposes, by UoM id
        uoms_o = {}
        # this will be a dictionary of the product UoM by product id
        product2uom = {}
        for product in self.browse(cr, uid, ids, context=context):
            product2uom[product.id] = product.uom_id.id
            uoms_o[product.uom_id.id] = product.uom_id

        results = []
        results2 = []

        from_date = context.get('from_date', False)
        to_date = context.get('to_date', False)
        date_str = False
        date_values = False
        where = [tuple(location_ids), tuple(location_ids), tuple(ids), tuple(states)]
        if from_date and to_date:
            date_str = "date>=%s and date<=%s"
            where.append(tuple([from_date]))
            where.append(tuple([to_date]))
        elif from_date:
            date_str = "date>=%s"
            date_values = [from_date]
        elif to_date:
            date_str = "date<=%s"
            date_values = [to_date]

        prodlot_id = context.get('prodlot_id', False)

    # TODO: perhaps merge in one query.
        if date_values:
            where.append(tuple(date_values))

        if 'in' in what:
            # all moves from a location out of the set to a location in the set

            sql = 'select sum(sm.product_qty), sm.product_id, sm.product_uom '\
                'from stock_move sm '\
                'left join stock_bin sb on sm.bin_id = sb.id '\
                'where sm.location_id NOT IN %s '\
                'and sm.location_dest_id IN %s '\
                'and sm.product_id IN %s '\
                '' + (prodlot_id and ('and sm.prodlot_id = ' + str(prodlot_id)) or '') + ' '\
                'and sm.state IN %s ' + (date_str and 'and ' + date_str + ' ' or '') + ' '\
                '' + (ring_size_id and ('and sm.size_id = ' + str(ring_size_id)) or '') + ' '\
                '' + (bin is not None and ("and coalesce(sb.name, sm.bin, '') = '" + bin.replace("'", "''") + "'") or '') + ' '\
                'group by sm.product_id, sm.product_uom'

            cr.execute(sql, tuple(where))
            results = cr.fetchall()
        if 'out' in what:
            # all moves from a location in the set to a location out of the set

            sql = 'select sum(sm.product_qty), sm.product_id, sm.product_uom '\
                'from stock_move sm '\
                'left join stock_bin sb on sm.bin_id = sb.id '\
                'where sm.location_id IN %s '\
                'and sm.location_dest_id NOT IN %s '\
                'and sm.product_id  IN %s '\
                '' + (prodlot_id and ('and sm.prodlot_id = ' + str(prodlot_id)) or '') + ' '\
                'and sm.state in %s ' + (date_str and 'and ' + date_str + ' ' or '') + ' '\
                '' + (ring_size_id and ('and sm.size_id = ' + str(ring_size_id)) or '') + ' '\
                '' + (bin is not None and ("and coalesce(sb.name, sm.bin, '') = '" + bin.replace("'", "''") + "'") or '') + ' '\
                'group by sm.product_id, sm.product_uom'

            cr.execute(sql, tuple(where))
            results2 = cr.fetchall()

        # Get the missing UoM resources
        uom_obj = self.pool.get('product.uom')
        uoms = map(lambda x: x[2], results) + map(lambda x: x[2], results2)
        if context.get('uom', False):
            uoms += [context['uom']]
        uoms = filter(lambda x: x not in uoms_o.keys(), uoms)
        if uoms:
            uoms = uom_obj.browse(cr, uid, list(set(uoms)), context=context)
            for o in uoms:
                uoms_o[o.id] = o

        #TOCHECK: before change uom of product, stock move line are in old uom.
        context.update({'raise-exception': False})
        # Count the incoming quantities
        for amount, prod_id, prod_uom in results:
            amount = uom_obj._compute_qty_obj(cr, uid, uoms_o[prod_uom], amount,
                     uoms_o[context.get('uom', False) or product2uom[prod_id]], context=context)
            res[prod_id] += amount
        # Count the outgoing quantities
        for amount, prod_id, prod_uom in results2:
            amount = uom_obj._compute_qty_obj(cr, uid, uoms_o[prod_uom], amount,
                    uoms_o[context.get('uom', False) or product2uom[prod_id]], context=context)
            res[prod_id] -= amount
        return res

    def get_customer_product_available(self, cr, uid, ids, customer_id=False, ring_size_id=False, context=None):

        if context is None:
            context = {}

        new_context = {}

        new_context.update({'compute_child': context.get('compute_child', True)})

        # field_name is one from (qty_available, virtual_available, incoming_qty, outgoing_qty)
        field_name = 'qty_available'
        if context.get('type', False):
            field_name = context['type']

        if customer_id:
            customer = self.pool.get('res.partner').read(cr, uid, customer_id)
            if customer['order_ids']:
                new_context.update({'location': [int(x) for x in customer['order_ids'].split(",")]})

        if ring_size_id:
            new_context.update({'ring_size': ring_size_id})

        res = self._product_available(cr, uid, ids, field_names=[field_name], context=new_context)

        return res

    def get_product_customer_sku(self, cr, uid, partner_id=False, product_id=False, size_id=False, context=None):
        if not partner_id or not product_id:
            return False

        get_sku_sql = """   SELECT
                                distinct cf."value"
                                FROM
                                    product_multi_customer_names cn
                                    INNER JOIN  product_multi_customer_fields_name fn on (
                                        fn.name_id = cn.id AND fn.customer_id = %s
                                    )
                                    INNER JOIN  product_multi_customer_fields cf on (
                                        cf.field_name_id = fn.id
                                        AND cf.product_id = %s
                                        AND COALESCE(cf.size_id, 0) = %s
                                    )
                                WHERE 1=1
                                    AND cn.name = 'customer_sku'
                                    AND (cf."value" is not null and trim(cf."value") != '')
                                ;
                """ % (partner_id, product_id, size_id or 0)
        cr.execute(get_sku_sql)
        sku_res = cr.fetchone()
        if sku_res and sku_res[0]:
            return sku_res[0]
        else:
            return False

product_product()

#DELMAR-140
class Product_CSV_Import_Info(osv.osv_memory):
    _name = 'product.csv.import.info'
Product_CSV_Import_Info()

class Product_CSV_Import(osv.osv_memory):
    _name = "product.csv.import"

    _columns = {
    'csv_file': fields.binary(string="Input file"),
    }

    def create_products(self, cr, uid, ids, context=None):
	    try:
	        csv_pool = self.browse(cr, uid, ids[0])
	        obj = base64.decodestring(csv_pool.csv_file)
	        file = list(csv.reader(cStringIO.StringIO(obj), delimiter=',', quotechar='"'))
	    except:
	        raise osv.except_osv('Error', 'Please select a CSV file to import')
	    if len(file[0]) != 7:
	        raise osv.except_osv('Error', 'Can\'t import this file')
	    for indx in range(0, len(file)):
	        prod_prod_vals = {}
	        prod_tmpl_vals = {}
	        if indx == 0:
	            continue
	        if len(file[indx]) > 7:
	            if file[indx][7] != '':
	                raise osv.except_osv('Error', 'CSV has some extra values than expected columns.')
	        else:
	            current_list = file[indx]
	            product_exists = self.pool.get('product.product').search(cr, uid,[('default_code', '=', current_list[0])])
	            if product_exists:
	                raise osv.except_osv('Error', 'Product with SKU {} is already exists'.format(current_list[0]))
	            prod_type_in_csv = current_list[2].split('/')
	            if prod_type_in_csv:
	                try:
	                    parent_categ_name = prod_type_in_csv[0]
	                    child_categ_name = prod_type_in_csv[1]
	                except:
	                    child_categ_name = False
	                if parent_categ_name and child_categ_name:
	                    parent_categ_name = prod_type_in_csv[0][:-1]
	                    child_categ_name = prod_type_in_csv[1][1:]
	                    prod_type_id = self.pool.get('product.category').search(cr, uid,[('name', '=', child_categ_name), ('parent_id.name', '=',parent_categ_name)])
	                if parent_categ_name and not child_categ_name:
	                    prod_type_id = self.pool.get('product.category').search(cr, uid,[('name', '=', parent_categ_name)])
	                if prod_type_id:
	                    prod_type_id = prod_type_id[0]
	                else:
	                    raise osv.except_osv('Error', 'Product type not found for SKU {}'.format(current_list[0]))
	            else:
	                raise osv.except_osv('Error', 'Product type is not provided for SKU {}'.format(current_list[0]))
	            if current_list[3]:
	                supplier_id = self.pool.get('res.partner').search(cr, uid, [('ref', '=', current_list[3])])
	                if supplier_id:
	                    supplier_id = supplier_id[0]
	                else:
	                    raise osv.except_osv('Error', 'Supplier with ref {} not found'.format(current_list[3]))
	            else:
	                supplier_id = current_list[3]
	            prod_prod_vals['default_code'] = current_list[0]
	            prod_tmpl_vals['name'] = current_list[1]
	            prod_tmpl_vals['categ_id'] = prod_type_id
	            prod_prod_vals['def_supplier_id'] = supplier_id
	            prod_prod_vals['short_description'] = current_list[4]
	            prod_tmpl_vals['prod_manufacture'] = current_list[5]
	            prod_tmpl_vals['prod_tarif_classification_ca'] = current_list[6]
	            tmpl_id = self.pool.get('product.template').create(cr, uid, prod_tmpl_vals, context=None)
	            prod_prod_vals['product_tmpl_id'] = tmpl_id
	            pp_id = self.pool.get('product.product').create(cr, uid, prod_prod_vals, context=None)
	            self.pool.get('product.product').write(cr, uid, pp_id,{'short_description': prod_prod_vals['short_description'],'default_code': prod_prod_vals['default_code']})
	    return {
	        'name': _('Products imported successfully'),
	        'view_type': 'form',
	        'view_mode': 'form',
	        'res_model': 'product.csv.import.info',
	        'type': 'ir.actions.act_window',
	        'target': 'new',
	        'context': context,
	    }