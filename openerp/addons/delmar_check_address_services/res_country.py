from osv import fields, osv


class Country(osv.osv):
    _inherit = "res.country"
    _description = "Country (Delmar)"
    _columns = {
        'address_check_rule': fields.char('Address Check Rule', size=64,
            help='Regular expression for checking of Address format (from API response).', required=True, translate=True),
    }

Country()