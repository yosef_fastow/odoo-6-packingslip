{
    "name": "address_services",
    "version": "0.1",
    "author": "",
    "website": "http://www.progforce.com/",
    "depends": [
    ],
    "description": """
        pip install geopy
    """,
    "category": "Tools",
    "init_xml": [

    ],
    "demo_xml": [],
    "update_xml": [
        'views/views_address_services.xml',
        'views/views_res_country.xml'
    ],
    "active": False,
    "installable": True,
    'auto_install': False,
}
