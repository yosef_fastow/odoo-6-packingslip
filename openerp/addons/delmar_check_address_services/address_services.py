# -*- coding: utf-8 -*-
import re

from osv import fields, osv
from api import usps, geopy_check
import logging

_logger = logging.getLogger(__name__)


def _get_type_api(self, cr, uid, context=None):
    return (
        ('usps', 'USPS'),
        ('geopy_check', 'Google')
    )


def _get_test_method(self, cr, uid, context=None):
    return (('check_address', 'Check Address'),)


class address_services(osv.osv):
    _name = "address.services"
    _columns = {
        'name': fields.char('Name', size=128),
        'url': fields.char('Url', size=128),
        'web_id': fields.char('Web Tools ID', size=128),
        'type_api': fields.selection(_get_type_api, 'Api services'),
        'active_service': fields.boolean('Active'),
        'country_ids': fields.many2many('res.country', 'address_services_country', 'address_services_id', 'res_country_id', 'Country'),
        'test_method': fields.selection(_get_test_method, 'Test Method'),
        'test_street': fields.char('Street', size=128),
        'test_street2': fields.char('Street2', size=128),
        'test_city': fields.char('City', size=128),
        'test_state': fields.char('State', size=128),
        'test_zip': fields.char('Zip', size=128),
        'test_country': fields.many2one('res.country', 'Country'),
        'clear_list': fields.many2many('address.services.clear.list', 'address_services_clear_list_link', 'services_id', 'list_id', 'Clear List'),
        'priority': fields.integer('Priority')
    }

    def test_service(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        result = False

        context.update({'get_response': True})

        address_serv = self.browse(cr, uid, ids[0])
        test_method = address_serv.test_method

        method = getattr(self, test_method, None)

        address_obj = {
            'street': address_serv.test_street,
            'street2': address_serv.test_street2,
            'city': address_serv.test_city,
            'state': address_serv.test_state,
            'zip': address_serv.test_zip,
            'country': address_serv.test_country.code
        }
        if(method):
            result = method(cr, uid, address_obj, context=context)

        title = 'Address verification !'
        message = 'API not found !'

        if result:
            if result.get('status', False):
                message = 'Address is valid'
            else:
                message = result.get('msg', False) or 'Address is not valid !'

        raise osv.except_osv(title, message)

    def getApi(self, cr, uid, address_obj=None):
        if not address_obj: address_obj = {}
        api_list = []
        if(address_obj.get('country', False)):
            countries = self.pool.get('res.country').name_search(cr, uid, address_obj['country'])
            if countries:
                country_id = countries[0][0]
                if(country_id):
                    settings_id = self.search(cr, uid, [('country_ids', '=', country_id), ('active_service', '=', True)], order='priority')
                    if settings_id:
                        for item in settings_id:
                            api = None
                            settings = self.browse(cr, uid, item)
                            if(settings.type_api == "usps"):
                                api = usps.USPSApiClient(settings.url, settings.web_id)
                            elif(settings.type_api == "geopy_check"):
                                api = geopy_check.GeoPyApiClient()

                            if api:
                                api_list.append({'settings': settings, 'api': api})

        return api_list

    def check_address(self, cr, uid, address_obj=None, context=None):
        _logger.info("RUN ADDRESS.SERVICES.check_address")
        # # DELMAR-186
        # if (address_obj):
        #     encode_address_fields = {}
        #     for key, value in address_obj.iteritems():
        #         if (isinstance(value, (dict))):
        #             key_fields = {}
        #             for k, val in value.iteritems():
        #                 if (isinstance(val, (dict))):
        #                     inner_key_fields = {}
        #                     for i_k, i_val in val.iteritems():
        #                         if (i_val == None or i_val == False or i_val == True or isinstance(i_val,
        #                                                                                            (long, int)) == True):
        #                             inner_key_fields.update({
        #                                 i_k: i_val
        #                             })
        #                         else:
        #                             inner_key_fields.update({
        #                                 i_k: i_val.encode('utf-8')
        #                             })
        #                     key_fields.update({k: inner_key_fields})
        #                 else:
        #                     if (val == None or val == False or val == True or isinstance(val, (long, int)) == True):
        #                         key_fields.update({
        #                             k: val
        #                         })
        #                     else:
        #                         key_fields.update({
        #                             k: val.encode('utf-8')
        #                         })
        #             encode_address_fields.update({key: key_fields})
        #         else:
        #             if (value == None or value == False or value == True or isinstance(value, (long, int)) == True):
        #                 encode_address_fields.update({
        #                     key: value
        #                 })
        #             else:
        #                 encode_address_fields.update({
        #                     key: value.encode('utf-8')
        #                 })
        #     address_obj = encode_address_fields
        #     # END DELMAR-186
        if address_obj['street'] and re.findall(r'PEO\d+|ZJC\d{5}|ZO\d{5}|GOR\d+|PAG\d+', address_obj['street']):
            address_obj['street'] = address_obj['street2']
            address_obj['street2'] = ''

        if not address_obj:
            address_obj = {}
        if context is None:
            context = {}

        api_list = self.getApi(cr, uid, address_obj)
        result = {
            'status': False
        }
        _logger.info('GOT api_list')
        address_list = []
        address_list.append(address_obj.copy())
        if address_obj['street2']:
            tmp = address_obj.copy()
            tmp['street2'] = ''
            address_list.append(tmp)

            tmp = address_obj.copy()
            tmp['street'] = address_obj['street2']
            tmp['street2'] = ''
            address_list.append(tmp)

        log = []
        if api_list:
            for api_item in api_list:
                _logger.info("API item: %s" % api_item)
                for address in address_list:
                    _logger.info("Run check addess")
                    result = api_item['api'].check_address(address, [])
                    _logger.info("Address checked, result: %s" % result)
                    if result.get('status', False):
                        break

                if not result.get('status', False):
                    _logger.info("Run second check")
                    clear_list = []
                    for item in api_item['settings'].clear_list:
                        clear_list.append(item.name)
                    _logger.info("ADDRESS LIST %s" % address_list)
                    for address in address_list:
                        for item in clear_list:
                            if address['street'] and address['street'].find(item) != -1: # DELMAR-186
                                _logger.info("Run second check addess")
                                result = api_item['api'].check_address(address.copy(), clear_list)
                                _logger.info("Address second checked, result: %s" % result)
                                break
                        if(result.get('status', False)):
                            break

                # check fraud and send notification
                _logger.info("RUN FRAUD CHECK")
                if 'fraud' in result and result.get('fraud', False):
                    # find order(s)
                    order_ids = self.pool.get('stock.picking').search(cr, uid, [('address_id', '=', address_obj['id'])])
                    _logger.info("ORDER IDSSS %s" % order_ids)
                    for order in self.pool.get('stock.picking').browse(cr, uid, order_ids):
                        email_to = ['avi@delmarintl.ca', 'delmar@isddesign.com']
                        partner = order.real_partner_id or order.partner_id or None
                        if partner:
                            if partner.user_id and partner.user_id.user_email:
                                email_to.append(partner.user_id.user_email)
                            if partner.order_support_user_id and partner.order_support_user_id.user_email:
                                email_to.append(partner.order_support_user_id.user_email)
                        # prepare message
                        message_body = "Address id#{} for order(s) {} was defined as FRAUD!\n".format(address_obj['id'], order.name)
                        message = {
                            'email_from': 'erp.delmar@gmail.com',
                            'email_to': email_to,
                            'subject': "Fraud address suspect {}".format(address_obj['id']),
                            'body': message_body,
                            'attachments': None,
                        }
                        self.pool.get('mail.message').schedule_with_attach(
                            cr, uid,
                            message.get('email_from'), message.get('email_to'), message.get('subject'), message.get('body'),
                            subtype='plain', attachments=message.get('attachments'), context=None
                        )

                if result.get('status', False):
                    result['name'] = api_item['settings'].name
                    break

                log += api_item['api'].getLog()

        if log:
            self.log(cr, uid, log)

        if not address_obj.get('state', False):
            _logger.info('RUN STATE SEARCH')
            found_address = result.get('address', {})
            if found_address:
                found_state = (found_address.get('state', False) or '').strip()
                if found_state:
                    state_ids_name = self.pool.get("res.country.state").name_search(cr, uid, name=found_state, args=[('country_id.code', '=', address_obj['country'])])
                    if state_ids_name:
                        self.pool.get("res.partner.address").write(cr, uid, address_obj['id'], {'state_id': state_ids_name[0][0], 'is_valid': True})
            _logger.info('DONE STATE SEARCH')

        if not context.get('get_response', False):
            result = result['status']

        return result

    def log(self, cr, uid, logs=None):
        ## INFO:
        ## Logs was depricated because to many write operations

        #     if not logs:
        #         logs = []
        #
        #     for log_line in logs:
        #         try:
        #             name = unicode(log_line['title'], errors='ignore')
        #         except:
        #             name = unicode(log_line['title'])
        #         try:
        #             tmp = unicode(log_line['msg'], errors='ignore')
        #         except:
        #             tmp = unicode(log_line['msg'])
        #
        #         self.pool.get('res.log').create(
        #             cr, uid, {
        #                  'name': name,
        #                  'res_model': log_line.get('model', self._name),
        #                  'res_id': log_line.get('id', 0),
        #                  'debug_information': tmp,
        #                  'log_type': 'sale_integration_log'
        #              })

        return True



class address_services_clear_list(osv.osv):
    _name = "address.services.clear.list"
    _columns = {
        'name': fields.char('Name', size=128)
    }
