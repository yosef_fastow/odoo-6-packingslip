# -*- coding: utf-8 -*-
from apiclient import ApiClient
from lxml import etree


class USPSApi(ApiClient):
    web_id = ""
    api_url = "http://production.shippingapis.com/ShippingAPI.dll"
    request_type = "GET"

    def __init__(self, settings):
        self.api_url = settings["api_url"]
        self.web_id = settings["web_id"]
        self._log = []

    def prepare_data(self, data):

        self._log.append({
            'title': "Send %s USPS" % self.name,
            'msg': data,
            'type': 'send'
        })
        data['XML'] = ' '.join(data['XML'].split())
        return data

    def get_request_headers(self):
        return {}

    def check_response(self, response):
        print response
        msg = "Success"
        if(response.find('<Error>') != -1):
            msg = "ERROR"
        self._log.append({
            'title': "Recive %s USPS %s" % (self.name, msg),
            'msg': response,
            'type': 'recive'
        })
        return True

    def getLog(self):
        tmp = self._log[:]
        self._log = []
        return tmp


class USPSApiClient():

    settings = {
        "api_url": "",
        "web_id": ""
    }

    def __init__(self, url="", web_id=""):
        self.settings['api_url'] = url
        self.settings['web_id'] = web_id
        self._log = []

    def check_address(self, address_obj, clear_list=None):
        if not clear_list: clear_list = []
        address_api = USPSApiAddressValidate(self.settings, address_obj, clear_list)
        res = address_api.send()
        self._log = address_api.getLog()
        return res

    def getLog(self):
        return self._log


class USPSApiAddressValidate(USPSApi):
    """Check address use USPS"""
    name = "AddressValidate"
    address_obj = ""
    clear_list = []

    def __init__(self, settings, address_obj, clear_list=None):
        super(USPSApiAddressValidate, self).__init__(settings)
        self.address_obj = address_obj
        self._log = []
        self.clear_list = clear_list or []

    def get_request_data(self):
        if(self.clear_list and self.address_obj.get('street', False)):
                self.address_obj['street'] = self.address_obj['street'].lower()
                for clear in self.clear_list:
                    self.address_obj['street'] = self.address_obj['street'].replace(clear.lower(), '')

        data = """
        <AddressValidateRequest USERID="%s">
            <Address ID="0">
            <FirmName>%s</FirmName>
            <Address1>%s</Address1>
            <Address2>%s</Address2>
            <City>%s</City>
            <State>%s</State>
            <Zip5>%s</Zip5>
            <Zip4></Zip4>
            </Address>
        </AddressValidateRequest>""" % (
            self.web_id,
            self.address_obj.get('s_company', ''),
            self.address_obj.get('street2', False) or '',
            self.address_obj.get('street', False) or '',
            self.address_obj.get('city', False) or '',
            self.address_obj.get('state', False) or '',
            self.address_obj.get('zip', False) or '')

        return {'API': 'Verify', "XML": data}

    def parse_response(self, response):
        # <Error>
        #   <Number></Number>
        #   <Source></Source>
        #   <Description></Description>
        #   <HelpFile></HelpFile>
        #   <HelpContext></HelpContext>
        # </Error>
        print response
        root = etree.XML(response)
        address = root.find('Address')
        res = {'status': True, 'msg': response}
        if(response.find('<Error>') != -1):
            res = {'status': False, 'msg': address.findtext('Error/Description')}
        else:
            res['address'] = {}
            res['address']['street'] = address.findtext('Address2') or ''
            res['address']['city'] = address.findtext('City') or ''
            res['address']['state'] = address.findtext('State') or ''
            res['address']['zip'] = address.findtext('Zip5') or ''

        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
