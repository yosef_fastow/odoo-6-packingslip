# -*- coding: utf-8 -*-
#from geopy import geocoders
#from pygeocoder import Geocoder, GeocoderError
from geocoder_ext import GeocoderExt, GeocoderError
from time import sleep
import re
import logging

_logger = logging.getLogger(__name__)


class GeoPyApiClient():

    def __init__(self):
        self._log = []

    def check_address(self, address_obj, clear_list=None):
        if not clear_list:
            clear_list = []
        returnObj = {
            'status': False,
            'place': False,
            'lat': False,
            'lng': False,
            'msg': False,
            'fraud': False
        }
        msg = "Success"

        if not address_obj.get('ship', False):
            returnObj['status'] = True
            returnObj['address'] = address_obj
            returnObj['msg'] = msg

            return returnObj

        address = ""
        try:
            if(len(clear_list) > 0 and address_obj.get('street', False)):
                address_obj['street'] = address_obj['street'].lower()
                for clear in clear_list:
                    address_obj['street'] = address_obj['street'].replace(clear.lower(), '')

            for i in ('street', 's_company', 'city', 'state_full', 'zip', 'country'):
                if(address_obj.get(i, False)):
                    address += address_obj.get(i, "") + " "

            api_key = 'AIzaSyApnA0uR55xtM576iCZe8YItjHHAsgC5zM'
            geo = GeocoderExt(api_key=api_key)
            #geo = Geocoder()
            geo_received = False
            geo_iter = 0
            place = None
            details = None
            while not geo_received:
                geo_iter += 1
                if geo_iter > 10:
                    raise Exception("Try limit Exception")
                try:
                    place = geo.geocode(address)
                    details = geo.geodetails(address)
                    geo_received = True
                except GeocoderError, e:
                    _logger.warn(e.status)
                    sleep(5)
                    pass

            _logger.info("Check address: %s, Result: %s", (address, place.valid_address))

            # check details
            # is fraud in review text
            fraud_found = False
            # real pattern
            fraud_pattern = re.compile('(.+)fraud(.+)', re.IGNORECASE)
            # fake patter for dev
            #fraud_pattern = re.compile('(.+)google(.+)', re.IGNORECASE)
            if details and details.reviews:
                for review in details.reviews:
                    if fraud_pattern.match(review.get('text')):
                        fraud_found = True
            if fraud_found:
                _logger.info("Fraud address: %s" % fraud_found)
                returnObj['fraud'] = True

            # is valid address
            if place and place.valid_address and not fraud_found:
                returnObj['status'] = True
            else:
                returnObj['status'] = False
                msg = 'False'
            returnObj['place'] = place
            returnObj['address'] = {
                'country': place.country__short_name,
                'state': place.state__short_name,
                'zip': place.postal_code,
                'street': place.route,
            }
        except Exception, e:
            msg = 'False'
            if isinstance(e, (str, unicode, )):
                msg = e
            elif hasattr(e, 'message'):
                msg = e.message

            returnObj['status'] = False
            returnObj['msg'] = msg

        try:
            place = unicode(returnObj['place'], errors='ignore')
        except:
            place = unicode(returnObj['place'])
        self._log.append({
            'title': "Check address %s , found %s, result %s" % (address, place, msg), # DELMAR-186
            'msg': str(returnObj),
            'type': 'recive'
        })
        return returnObj

    def getLog(self):
        tmp = self._log[:]
        self._log = []
        return tmp
