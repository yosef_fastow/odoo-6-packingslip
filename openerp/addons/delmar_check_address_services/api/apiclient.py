# -*- coding: utf-8 -*-
from urllib2 import Request, urlopen, URLError
from urllib import urlencode


class ApiClient(object):

    request_type = "POST"

    def send(self):
        res = {}
        response = False

        if getattr(self, 'response', False):
            response = self.response

        else:
            """
                Send request to server
            """
            data = self.get_request_data()
            data = self.encode_data(data)
            data = self.prepare_data(data)

            headers = self.get_request_headers()
            headers = self.prepare_headers(headers, data)

            print data
            try:
                if(self.request_type == 'POST'):
                    conn = Request(url=self.api_url, data=data, headers=headers)
                else:
                    conn = Request(url="%s?%s" % (self.api_url, urlencode(data)), headers=headers)
                f = urlopen(conn)
                response = f.read()
                self.check_response(response)

            except URLError, e:
                if hasattr(e, 'reason'):
                    print 'Could not reach the server, reason: %s' % e.reason
                elif hasattr(e, 'code'):
                    print 'Could not fulfill the request, code: %d' % e.code

        if response:
            try:
                res = self.parse_response(response)
            except Exception, e:
                pass

        return res

    def check_response(self, response):
        """
            Check response on error
        """
        pass

    def prepare_data(self, data):
        """
            Prepare data before send
        """
        return data

    def encode_data(self, data):
        """
            Encode data before send
        """
        if data and data.get('XML', False):
            data['XML'] = data['XML'].encode('utf-8')
        return data

    def prepare_headers(self, headers, data):
        """
            Prepare headers befor send
        """
        return headers

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
