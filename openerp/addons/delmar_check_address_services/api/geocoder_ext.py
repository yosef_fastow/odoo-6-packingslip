#!/usr/bin/env python

"""
Small python wrapper to pygeocoder
"""
import requests
from pygeocoder import omnimethod, Geocoder, GeocoderResult, GeocoderError

class GeocoderExt(Geocoder):

    GEOCODER_DETAILS_QUERY_URL = 'https://maps.googleapis.com/maps/api/place/details/json?'

    def __init__(self, api_key = None, client_id = None, private_key = None):
        super(GeocoderExt, self).__init__(api_key, client_id, private_key)

    @omnimethod
    def geodetails(
        self,
        address,
        sensor='false',
        bounds='',
        region='',
        language='',
        components=''):
        params = {
            'address':  address,
            'sensor':   sensor,
            'bounds':   bounds,
            'region':   region,
            'language': language,
            'components': components,
        }

        if self is not None:
            data = self.get_data(params=params)
        else:
            data = Geocoder.get_data(params=params)
        
        if len(data) > 0 and 'place_id' in data[0]:
            place_id = data[0].get('place_id', None)
            params.update({'place_id': place_id})
            # params.update({'place_id': 'ChIJN1t_tDeuEmsRUsoyG83frY4'})
        else:
            return None
    
        if self is not None:
            details_data = self.get_details_data(params=params)
        else:
            details_data = Geocoder.get_details_data(params=params)

        return GeocoderResultExt(details_data)


    @omnimethod
    def get_details_data(self, params={}):
        request = requests.Request(
            'GET',
            url=GeocoderExt.GEOCODER_DETAILS_QUERY_URL,
            params=params,
            headers={
                'User-Agent': Geocoder.USER_AGENT
            })

        if self and self.client_id and self.private_key:
            request = self.add_signature(request)
        elif self and self.api_key:
            request.params['key'] = self.api_key

        session = requests.Session()

        if self and self.proxy:
            session.proxies = {'https': self.proxy}

        response = session.send(request.prepare())
        session.close()

        if response.status_code == 403:
            raise GeocoderError("Forbidden, 403", response.url)
        response_json = response.json()

        if response_json['status'] != GeocoderError.G_GEO_OK:
            raise GeocoderError(response_json['status'], response.url)
        return [response_json['result']]



class GeocoderResultExt(GeocoderResult):
    def __init__(self, data):
        super(GeocoderResultExt, self).__init__(data)

    @property
    def reviews(self):
        return self.current_data['reviews'] if 'reviews' in self.current_data else None

