# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2014
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import re
import logging

logger = logging.getLogger('zip_zone')


class zip_zone(osv.osv):
    _name = "zip.zone"

    _columns = {
        'name': fields.char('Name Zones Batch', size=256, required=True, ),
        'active': fields.boolean('Active'),
        'rules': fields.one2many('zip.zone.line', 'zz_id', 'Rules', ),
        'zip': fields.char('Zip Code', size=128),
    }

    _defaults = {
        'active': True,
    }

    def search(self, cr, uid, domain, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}
        try:
            zip_domain = (arg for arg in domain if ((isinstance(arg, list) or isinstance(arg, tuple)) and arg[0] == 'zip')).next()
            first_term_index = domain.index(zip_domain)
            zip_code = self.convert_zip(zip_domain[2])
            if zip_code:
                cr.execute("""
                    select zz_id
                    from zip_zone_line zl
                    where 1=1
                        and
                            case when zl.to_zip is null
                            then zl.from_zip = {zip_code}
                            else zl.from_zip <= {zip_code} and zl.to_zip >= {zip_code}
                            end
                """.format(zip_code=zip_code))
                zone_ids = [x[0] for x in cr.fetchall() or []]
                new_domain = domain[:first_term_index] + [['id', 'in', zone_ids]] + domain[first_term_index+1:]
                domain = new_domain
        finally:
            return super(zip_zone, self).search(cr, uid, domain, offset=offset, limit=limit, order=order, context=context, count=count)

    def convert_zip(self, zip_code):
        zip_code = (str(zip_code) or '').strip()
        if zip_code:
            zip_code = re.split('[-\s]+', zip_code)[0]
            while len(zip_code) > 5:
                zip_code = zip_code[:-4]

            zip_code = ('000' + zip_code)[-5:-2]

        try:
            int(zip_code)
        except:
            zip_code = False

        return zip_code

zip_zone()


class zip_zone_line(osv.osv):
    _name = "zip.zone.line"

    _columns = {
        'id': fields.integer('ID', readonly=True, ),
        'zz_id': fields.many2one('zip.zone', 'Parent', required=True, ondelete='cascade', ),
        'from_zip': fields.integer('From ZIP', required=True, ),
        'to_zip': fields.integer('To ZIP', required=False, ),
    }

zip_zone_line()
