# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2014
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv, orm
import logging
from mako.template import Template

logger = logging.getLogger('priority_dimension')


def _collect_sorted_values(input_dict, key):
    values = []
    for _key in input_dict:
        for row in input_dict[_key]:
            value = row.get(key, None)
            if(isinstance(value, orm.browse_record)):
                value = value.name
            elif(isinstance(value, orm.browse_null)):
                value = None
            if(value and value not in values):
                values.append(value)
    if(values):
        values = sorted(values)
    return values


class priority_dimension(osv.osv):
    _name = "priority.dimension"

    def _get_html_table(self, cr, uid, ids, *args, **kwargs):
        tmpl = \
            """
            <table class="html_table">
                <thead>
                    <tr>
                        % for column in lines[0]:
                        <th id="${get_key(column)}">${get_value(column)}</th>
                        % endfor
                    </tr>
                </thead>
                <tbody>
                    % for line in lines[1:]:
                    <tr>
                        % for column in line:
                        <td id="${get_key(column)}">${get_value(column)}</td>
                        % endfor
                    <tr>
                    % endfor
                </tbody>
            </table>
            <%!
                def get_key(one_dict):
                    result = None
                    if(isinstance(one_dict, dict)):
                        for key in one_dict:
                            result = key
                            break
                    else:
                        result = one_dict
                    return result

                def get_value(one_dict):
                    result = None
                    if(isinstance(one_dict, dict)):
                        for key in one_dict:
                            result = one_dict[key]
                            break
                    else:
                        result = one_dict
                    return result
            %>
            """
        tpl_table = Template(tmpl)
        if isinstance(ids, (int, long)):
            ids = [ids]
        result = {}
        for _id in ids:
            result[_id] = ''
        for _id in ids:
            prices_for_ = {}
            for row in self.browse(cr, uid, _id).rules:
                dict_row = row._get_dict_from_row()
                _weight = dict_row["weight"]
                if(not prices_for_.get(_weight, None)):
                    prices_for_[_weight] = []
                prices_for_[_weight].append(dict_row)
            for weight in prices_for_:
                prices_for_[weight] = sorted(prices_for_[weight], key=lambda k: k['zone_id'])
            sorted_top_header = _collect_sorted_values(prices_for_, 'zone_id')
            lines = []
            header = []
            header.append('Weight (not over lbs.)')
            for item in sorted_top_header:
                header.append(item)
            lines.append(header)
            for weight in sorted(prices_for_):
                line = []
                line.append(weight)
                for _row in prices_for_[weight]:
                    line.append({_row["id"]: _row["price"]})
                lines.append(line)
            result[_id] = tpl_table.render(lines=lines)
        return result

    _columns = {
        'name': fields.char('Method of Delivery', size=256, required=True, ),
        'active': fields.boolean('Active', required=True),
        'rules': fields.one2many('priority.dimension.line', 'pd_id', 'Rules', ),
        'rate_ids': fields.one2many('priority.flat.rate', 'pd_id', 'Rates', ),
        'in_tabular_form': fields.function(_get_html_table, method=True, string='IN TABULAR FORM', ),
    }

    _defaults = {
        'active': True,
    }

priority_dimension()


class priority_dimension_line(osv.osv):
    _name = "priority.dimension.line"

    _columns = {
        'id': fields.integer('ID', readonly=True, ),
        'pd_id': fields.many2one('priority.dimension', 'Parent', required=True, ondelete='cascade', ),
        'weight': fields.float('Weight', required=True, ),
        'zone_id': fields.many2one('zip.zone', 'Zone', required=True, ),
        'price': fields.float('Price', required=True, ),
    }

    def _get_dict_from_row(self, cr, uid, ids, context=None):
        _id = ids[0]
        result_dict = {}
        for key in self._columns.keys():
            if(key == 'zone_id'):
                result_dict.update({key: self.browse(cr, uid, _id)[key].name})
            else:
                result_dict.update({key: self.browse(cr, uid, _id)[key]})
        return result_dict

priority_dimension_line()


class priority_flat_rate(osv.osv):
    _name = 'priority.flat.rate'
    _columns = {
        'name': fields.char('Name', size=256, ),
        'price': fields.float('Price', required=True, ),
        'print_on_packing': fields.boolean('Print on packing', ),
        'pd_id': fields.many2one('priority.dimension', 'Parent', required=True, ondelete='cascade', ),
    }
priority_flat_rate()
