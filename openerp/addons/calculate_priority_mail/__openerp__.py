# -*- coding: utf-8 -*-
{
    "name": "Calculate Priority Mail",
    "version": "0.1",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "description": """ Module for Calculate Priority Mail and box option
    """,
    "category": "Sales Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'view/zip_zone_view.xml',
        'view/zip_zone_line_view.xml',
        'view/priority_dimension_view.xml',
        'view/priority_dimension_line_view.xml',
    ],
    'depends': [
        "base",
        "web",
    ],
    "css": [
        "static/src/css/*.css",
    ],
    'js': [],
    "application": True,
    "active": False,
    "installable": True,
}
