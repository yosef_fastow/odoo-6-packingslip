{
    "name": "fetchdb",
    "version": "0.1",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "base_tools",
        "stock",
    ],
    "description": """
Integration module with different database servers
=============================================

Module requires some packages to be installed.
    $ sudo apt-get install iodbc libiodbc2 tdsodbc freetds-bin freetds-dev libmyodbc unixODBC

To connect MYSQL Server:
    $ sudo apt-get install python-mysqldb

To connect to the MSSQL Server:
    $ sudo apt-get install unixodbc-dev python-dev python-pip
    $ sudo pip install pyodbc


For system DSNs you'll need these files.
/etc/odbcinst.ini           - Driver's settings
/etc/odbc.ini               - Connection's settings
/etc/freetds/freetds.conf   - Phisical servers

For more info check http://www.unixodbc.org/odbcinst.html

=============================================
/etc/odbcinst.ini
=============================================

[FreeTDS]
Description = TDS driver (Sybase/MS SQL)
Driver = /usr/lib/odbc/libtdsodbc.so
Setup = /usr/lib/odbc/libtdsS.so
CPTimeout =
CPReuse =


[MySQL]
Descrition = MySQL driver
Driver = /usr/lib/odbc/libmyodbc.so
Driver64 = /usr/lib
Setup = /usr/lib/odbc/libodbcmyS.so
Setup64 = /usr/lib
UsageCount = 1

...

=============================================
/etc/odbc.ini
=============================================

[ODBC Data Sources]
inventory_server = Microsoft SQL Server

[inventory_test_server]
Driver = FreeTDS
Description = A wonderful description goes here
Trace = No
Servername = inventory_server
Database = DelmarInventoryTest
TDS_Version = 8.0


[Default]
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
HS_LANGUAGE=american_america.we8mswin1252
HS_NLS_NCHAR=UCS2

...

=============================================
/etc/freetds/freetds.conf
=============================================
[global]
        # TDS protocol version
;   tds version = 4.2

    # Whether to write a TDSDUMP file for diagnostic purposes
    # (setting this to /tmp is insecure on a multi-user system)
;   dump file = /tmp/freetds.log
;   debug flags = 0xffff

    # Command and connection timeouts
    timeout = 100
    connect timeout = 100

    text size = 64512
    tds version = 8.0
    client charset = UTF-8
    mssql.charset = "UTF-8"


# Our servers
[inventory_server]
    host = 127.0.0.1
    port = 21433
    tds version = 8.0
    client charset = UTF-8


    """,
    "category": "Tools",
    "init_xml": [
        'data/fetchdb_data.xml',
        'view/fetchdb_view.xml',
    ],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True,
    'auto_install': False,
}
