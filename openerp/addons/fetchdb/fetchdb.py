# -*- coding: utf-8 -*-
from osv import osv, fields
from openerp.addons.pf_utils.utils.decorators import try_or_log_exception
import pyodbc
import logging
import subprocess
import time
from tools.translate import _


logger = logging.getLogger('fetchdb')


class Connection(object):

    connection = None

    def __getattr__(self, name):
        return getattr(self.connection, name)

    def connect(self, *args, **kwargs):
        self.connection = pyodbc.connect(*args, **kwargs)
        return self

    @try_or_log_exception(logger, pyodbc.ProgrammingError)
    def rollback(self, *args, **kwargs):
        return self.connection.rollback(*args, **kwargs)

    @try_or_log_exception(logger, pyodbc.ProgrammingError)
    def close(self, *args, **kwargs):
        return self.connection.close(*args, **kwargs)


class fetchdb_server(osv.osv):
    _name = "fetchdb.server"
    _description = "Database Server"

    def _get_dsn_list(self, cr, uid, context=None):
        p = subprocess.Popen(["odbcinst", "-q", "-s"], stdout=subprocess.PIPE)

        res = []
        ln = p.stdout.readline()
        while ln:
            ln = ln.replace('[', '').replace(']', '').replace('\n', '')
            if ln != 'Default':
                res.append((ln, ln))
            ln = p.stdout.readline()

        return res

    _columns = {
        'name': fields.char('Name', size=256, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'state': fields.selection([
            ('draft', 'Not Confirmed'),
            ('done', 'Confirmed'),
        ], 'State', select=True, required=True, readonly=True, help="Status database connection"),
        'type': fields.selection([
            ('MySQL', 'MY SQL'),
            ('FreeTDS', 'MS SQL'),
        ], 'Server Type', select=True, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'host': fields.char('Host', size=256, required=True, readonly=True, help="Hostname or IP of the db server", states={'draft': [('readonly', False)]}),
        'port': fields.integer('Port', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'schema': fields.char('Schema', size=256, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'user': fields.char('Username', size=256, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'password': fields.char('Password', size=1024, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'target': fields.selection([
            ('main', 'Main'),
            ('sale', 'Sale'),
            ('invoice', 'Invoice'),
            ('warehouse', 'Warehouse'),
            ('other', 'Other'),
            ('po', 'Purchase Order'),
            ('price', 'Customer Price'),
        ], 'Type', select=True, required=True, readonly=True, states={'draft': [('readonly', False)]},
        help="Kind of server"),
        'environment': fields.selection([
            ('production', 'Production'),
            ('development', 'Development'),
        ], 'Environment', select=True, required=True, readonly=True, states={'draft': [('readonly', False)]},
        help=""),
        'system': fields.boolean('System field', help="System fields are undeletable"),
        'use_odbc_dsn': fields.boolean("Use ODBC's DSN", readonly=True, states={'draft': [('readonly', False)]}, help="Use ODBC's DSN defined in /etc/odbc.ini"),
        'odbc_dsn': fields.selection(_get_dsn_list, 'ODBC DSN', select=True, readonly=True, states={'draft': [('readonly', False)]},
        help="ODBC DSN"),
    }
    _defaults = {
        'state': "draft",
        'type': "mysql",
        'port': 3306,
        'system': False,
        'target': 'main',
        'environment': 'development',
    }

    def connect(self, cr, uid, server_id, context=None):
        try:
            if isinstance(server_id, (list, tuple)):
                server_id = server_id[0]
            server = self.browse(cr, uid, server_id, context)
            # if server.use_odbc_dsn:
                # connStr = (
                #     "DSN=" + str(server.odbc_dsn) + ";" +
                #     "UID=" + server.user + ";" +
                #     "PWD=" + server.password + ';'
                # )

            if server_id == 1:
                connStr = (
                    "DRIVER={" + 'FreeTDS' + "};" +
                    "SERVER=" + '192.168.109.2' + ";" +
                    "PORT=" + '1433' + ";" +
                    "UID=" + 'yosef1'+ ";" +
                    "PWD=" + 'Delmar149'+ ";" +
                    "DATABASE=" + 'delmarifctest'
                )

            else:
                connStr = (
                    "DRIVER={" + server.type + "};" +
                    "SERVER=" + '192.168.100.7' + ";" +
                    "PORT=" + str(server.port) + ";" +
                    "UID=" + server.user + ";" +
                    "PWD=" + server.password + ";" +
                    "DATABASE=" + server.schema
                )
            start = time.time()
            connection = Connection().connect(connStr)
            end = time.time()
            logger.warn("FETCHDB_TIME: Connected in {}".format(end-start))
        except Exception, e:
            connection = False
            logger.error("Failed to connect to %s server %s.\n ERROR: %s\n", server.type, server.name, e)

        return connection

    def set_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True

    def set_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'done'})
        return True

    def confirm_connection(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for server in self.browse(cr, uid, ids, context=context):
            try:
                connection = server.connect()
                if connection:
                    server.set_done()
                else:
                    raise Exception
            except Exception, e:
                server.set_draft()
                raise osv.except_osv(("Connection test failed!"), ("Failed to connect to %s server %s.\n ERROR: %s") % (server.type, server.name, e))
                return False
            finally:
                try:
                    if connection:
                        connection.close()
                except Exception:
                    pass
        return True

    def encode_param(self, s):
        return s.encode("utf-8", "ignore") if isinstance(s, unicode) else s

    def prepare_dict(self, sql, params):
        if not params:
            return sql, params

        import re
        new_params = []
        regex = re.compile(ur'(%\((\w+)\)s)', re.MULTILINE)
        keys = regex.findall(sql)
        single = False
        if isinstance(params, (dict)):
            single = True
            params = [params]

        if keys:
            to_replace = []
            for source, key, in keys:
                if key in params[0]:
                    to_replace.append(key)
            if to_replace:
                for repl in list(set(to_replace)):
                    sql = sql.replace("%({repl})s".format(repl=repl), '?')
                for param in params:
                    p = [self.encode_param(param[repl]) for repl in to_replace]
                    new_params.append(p)

        if new_params and single:
            new_params = new_params[0]

        return sql, new_params

    def prepare_query(self, sql, params=None):
        encoded_params = []
        if params:
            if isinstance(params, (list, tuple)):
                if isinstance(params[0], dict):
                    return self.prepare_dict(sql, params)

                for param in params:
                    if isinstance(param, (list, tuple)):
                        for x in param:
                            x = self.encode_param(x)
                    else:
                        param = self.encode_param(param)

                    encoded_params.append(param)

            elif isinstance(params, (dict)):
                return self.prepare_dict(sql, params)

        return sql, encoded_params

    def make_query(self, cr, uid, server_id, sql="", params=None, select=True, commit=False, connection=False, cursor=False, multi=False, return_dict=False):
        # import pdb; pdb.set_trace()
        result = None
        sql, encoded_params = self.prepare_query(sql, params)

        do_close = not(connection)

        try:
            if not connection:
                connection = self.connect(cr, uid, server_id)
            if connection:
                if not cursor:
                    cursor = connection.cursor()

                start = time.time()

                if multi:
                    result = cursor.executemany(sql, encoded_params)
                else:
                    result = cursor.execute(sql, encoded_params)

                if result and select:
                    result = result.fetchall()
                    if return_dict:
                        columns = [column[0] for column in cursor.description]
                        data = [dict(zip(columns, row)) for row in result]
                        result = data

                end = time.time()
                logger.warn("FETCHDB_TIME: Query made in {}".format(end-start))

                if result and commit:
                    start = time.time()
                    connection.commit()
                    end = time.time()
                    logger.warn("FETCHDB_TIME: Commited in {}".format(end-start))

        except Exception, e:
            do_close = True
            logger.error("Failed. Can't execute sql query:\n%s\nParams: %s\nError: %s", sql, encoded_params, e)
            # result = None
            result = _('\nFailed. Cant execute sql query: ') + str(e)
        finally:
            if connection and do_close:
                connection.close()

        return result

    def insert_record(self, cr, uid, server_id, table=False, values=False, output='', select=False, commit=True, connection=False, cursor=False):
        """ Inserts a record in a table.
            @param  table:  str type, Table Name
                    values: dict type, have keys as columns, values are values
                            If need subquery to be as an inserting value then value should have format: {'subquery': '<subquery SQL>'}
                            example: values={"ID": {'subquery': '(SELECT max(ID) + 1 FROM my_table)'}, "Name": 'my_name'}
        """
        columns = []
        params = []
        params_placeholders = []
        for key in values:
            columns.append(key.replace('"', '""'))
            if isinstance(values[key], dict) and 'subquery' in values[key]:
                params_placeholders.append(values[key]['subquery'])
            else:
                params_placeholders.append('?')
                params.append(values[key])
        params_str = ', '.join(params_placeholders)
        columns_str = ', '.join(['"{col}"'.format(col=col) for col in columns])
        sql = """INSERT INTO {table} ({columns}) {output}
            VALUES ({vals})""".format(table=table, columns=columns_str, vals=params_str, output=output)
        return self.make_query(cr, uid, server_id, sql, params=params, select=select, commit=commit, connection=connection, cursor=cursor)

    def search_default_args(self, cr, uid, context=None):
        def_env = 'production'
        cur_env = self.pool.get('ir.config_parameter').get_param(cr, uid, 'system.environment')
        env = cur_env if cur_env in ['development', 'production'] else def_env

        search_args = [
            ('state', '=', 'done'),
            ('environment', '=', env)
        ]

        return search_args

    def get_servers(func):

        def wrapper(self, cr, uid, *args, **kwargs):
            context = kwargs.get('context', {})
            single = kwargs.get('single', False)
            search_args = self.search_default_args(cr, uid, context=context)
            search_args += func(self, cr, uid)

            result = self.search(cr, uid, search_args, limit=None if single else None, context=context)
            if single:
                result = result and result[0] or None
            return result
        return wrapper

    @get_servers
    def get_warehouse_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'warehouse')]

    @get_servers
    def get_main_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'main')]

    @get_servers
    def get_sale_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'sale')]

    @get_servers
    def get_invoice_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'invoice')]

    @get_servers
    def get_po_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'po')]

    @get_servers
    def get_price_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'price')]

    @get_servers
    def get_mirror_servers(self, cr, uid, single=False, context=None):
        return [('target', '=', 'update_qty')]

    def check_mssql_schema(self, cr, uid, server_id, table_name, context=None):
        if server_id and (table_name or '').strip():

            server = self.browse(cr, uid, server_id)
            if server and server.type == 'FreeTDS':

                check_schema_sql = """  IF OBJECT_ID('%s') IS NOT NULL
                                            BEGIN
                                                SELECT 1
                                            END
                                        ELSE
                                            BEGIN
                                                SELECT TOP 0 NULL
                                            END;
                """ % (table_name)
                if self.make_query(cr, uid, server.id, check_schema_sql):
                    return True

        return False

fetchdb_server()
