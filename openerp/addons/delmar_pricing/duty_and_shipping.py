# -*- coding: utf-8 -*-
from osv import fields, osv
from openerp.addons.pf_utils.utils.xml_utils import _try_parse


class duty_and_shipping(osv.osv):
    _name = "duty.and.shipping"

    _columns = {
        'name': fields.char('Name', size=256, required=True, ),
        'map_type': fields.selection(
            [
                ('silver', 'Silver'),
                ('gold', 'Gold'),
            ], 'Metal Type',
            required=True,
        ),
        'duty': fields.float('Duty', digits=(16, 4), required=True, ),
        'shipping': fields.float('Shipping', digits=(16, 4), required=True, ),
        'origin_country': fields.char('Country of Origin', size=64, required=False, ),
    }

    def on_change_float(self, cr, uid, ids, name, value, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = {name: None}
        res.update({name: _try_parse(value, 'float', None)})
        return {'value': res}

duty_and_shipping()
