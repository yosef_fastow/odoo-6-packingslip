# -*- coding: utf-8 -*-
from osv import fields, osv


class brands_profit(osv.osv):
    _name = 'brands.profit'

    _columns = {
        'name': fields.char('Name', size=256, required=True, ),
        'profit': fields.float('Value', required=True, ),
        'flat_fee': fields.float('Flat Fee', required=True, ),
    }

brands_profit()
