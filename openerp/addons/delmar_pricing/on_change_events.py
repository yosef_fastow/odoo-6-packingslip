# -*- coding: utf-8 -*-
from osv import osv
from openerp.addons.pf_utils.utils.re_utils import date_range_string
from tools.translate import _
from stock_product_pricelist import check_formula, FormulaException
from random import randint


events_description = {
    0: 'new',
    1: 'update',
    2: 'remove',
    3: 'cut_and_link_to_new',
    4: 'none',
    5: 'unlink_all',
    6: 'replace_all'
}


class stock_product_pricelist_version(osv.osv):
    _name = "stock.product.pricelist.version"
    _inherit = "stock.product.pricelist.version"

    def check_required_columns(self, cr, uid, ids, vals, context=None):
        actual_data_row = {}
        changed_columns_ids = [key for key, value in vals.iteritems()]
        required_columns_ids = ['name', 'date_start', 'base_price', 'rounding_method']
        needed_columns_ids = list(set(required_columns_ids) - set(changed_columns_ids))
        actual_data_row.update(vals)
        if (needed_columns_ids):
            actual_data_row.update(self.read(cr, uid, ids, needed_columns_ids))
        for key in required_columns_ids:
            if (not actual_data_row[key]):
                raise osv.except_osv(
                    _('Warning!'),
                    _('{0} is required!'.format(key.replace('_', ' ')))
                )
        return True

    def check_for_intersection(self, cr, uid, data, another_datas):
        date_ranges = {
            x['name']: date_range_string(x['date_start'], x['date_end'])
            for x in another_datas if x['date_end']
        }
        another_names = [y['name'] for y in another_datas]
        if (data['name'] in another_names):
            raise osv.except_osv(
                _('Warning!'),
                _('Name must be unique!')
            )
        if (not data['date_end']):
            unclosed = [z['name'] for z in another_datas if not z['date_end']]
            if (unclosed):
                raise osv.except_osv(
                    _('Warning!'),
                    _('''
                        Only one version can
                        be unclosed,
                        now unclosed:
                        {0}
                    '''.format(','.join(unclosed)))
                )
            for name_version, date_range in date_ranges.iteritems():
                if (data['date_start'] in date_range):
                    raise osv.except_osv(
                        _('Warning!'),
                        _('''
                            Intersection with:
                            {0}
                        '''.format(name_version))
                    )
        else:
            if (data['date_start'] > data['date_end']):
                raise osv.except_osv(
                    _('Warning!'),
                    _('''
                        start date:
                        {0}
                        can not be more
                        than end date:
                        {1}
                    '''.format(data['date_start'], data['date_end']))
                )
            dr = date_range_string(data['date_start'], data['date_end'])
            for name_version, date_range in date_ranges.iteritems():
                if (list(set(dr) & set(date_range))):
                    raise osv.except_osv(
                        _('Warning!'),
                        _('''
                            Intersection with:
                            {0}
                        '''.format(name_version))
                    )
        return True

    def write(self, cr, uid, ids, vals, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        self.check_required_columns(cr, uid, ids, vals, context=context)
        actual_data_row = self.read(cr, uid, ids)
        actual_data_row.update(vals)
        another_ids = self.search(
            cr, uid,
            [
                ('pricelist_id', '=', actual_data_row['pricelist_id'][0]),
                ('id', '!=', actual_data_row['id'])
            ]
        )
        another_versions = self.read(cr, uid, another_ids)
        if (another_versions):
            self.check_for_intersection(cr, uid, actual_data_row, another_versions)
        sub_formulas = self.read(cr, uid, ids, ['sub_formula_1', 'sub_formula_2', 'sub_formula_3'])
        sub_formulas_in_vals = {key: value for key, value in vals.iteritems() if 'sub_formula_' in key}
        sub_formulas.update(sub_formulas_in_vals)
        formula = '(base_price + ({0})) / (profit_param - {1}) + ({2})'.format(
            sub_formulas.get('sub_formula_1', False) or 0,
            sub_formulas.get('sub_formula_2', False) or 0,
            sub_formulas.get('sub_formula_3', False) or 0
        )
        formula = self.fill_global_system_fields(cr, uid, formula)
        fields_values = self.get_actual_fields(cr, uid, ids, vals.get('fields', None))
        for name_field, value_field in fields_values.iteritems():
            formula = formula.replace(name_field, unicode(value_field[0]))
        # a second passage for fill all customer fields in condition
        for name_field, value_field in fields_values.iteritems():
            formula = formula.replace(name_field, unicode(value_field[0]))
        try:
            check_formula(formula, skip_base_price=True)
        except FormulaException as f_ex:
            raise osv.except_osv(
                _('Warning!'),
                _("{0}\nbut used in formula\nplease change formula.".format(f_ex.message))
            )
        super(stock_product_pricelist_version, self).write(cr, uid, ids, vals, context=context)
        return True

    def get_actual_fields(self, cr, uid, ids, changed_fields=None, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        sppif_obj = self.pool.get('stock.product.pricelist.item.field')
        changed_fields = changed_fields or []
        context = context or {}
        version_id = ids
        row = self.browse(cr, uid, version_id)
        fields = {}
        for field_ in row.fields:
            field_name_id = field_.field_name_id and field_.field_name_id.id or False
            fields.update({field_.id: [field_.name, field_.value_calculated_with_condition, field_.type, field_name_id]})
        for field in changed_fields:
            event = events_description[field[0]]
            field_id = field[1] or randint(-100, -1)
            value = field[2].get('value', None) if field[2] else None
            name = field[2].get('name', None) if field[2] else None
            field_type = field[2].get('type', None) if field[2] else None
            field_name_id = field[2].get('field_name_id', None) if field[2] else None
            condition_operator = field[2].get('condition_operator', None) if field[2] else None
            condtition_value = field[2].get('condtition_value', None) if field[2] else None
            result_if_true = field[2].get('result_if_true', None) if field[2] else None
            result_if_false = field[2].get('result_if_false', None) if field[2] else None
            if (event == 'new'):
                sub_context = {
                    'data': {
                        'id': field_id,
                        'value': value,
                        'condition_operator': condition_operator,
                        'condtition_value': condtition_value,
                        'result_if_true': result_if_true,
                        'result_if_false': result_if_false,
                        'field_name_id': field_name_id,
                    }
                }
                value = sppif_obj._get_value_calculated_with_condition(
                    cr, uid, [], None, None, context=sub_context
                ).get(field_id, value)
                fields.update({field_id: [name, value, field_type, field_name_id]})
            elif (event == 'update'):
                name = name or sppif_obj.read(cr, uid, field_id, ['name'])['name']
                value = value or sppif_obj.read(cr, uid, field_id, ['value'])['value']
                field_type = field_type or sppif_obj.read(cr, uid, field_id, ['type'])['type']
                field_name_id = field_name_id or sppif_obj.read(cr, uid, field_id, ['field_name_id'])['field_name_id']
                ftr = [
                    'value',
                    'condition_operator',
                    'condtition_value',
                    'result_if_true',
                    'result_if_false'
                ]
                sub_dict_field_values = sppif_obj.read(cr, uid, field_id, ftr)
                for elem in ftr:
                    exec("if ({0} is not None): sub_dict_field_values.update({{'{0}': {0}}})".format(elem))
                sub_context = {'data': sub_dict_field_values}
                sub_context['data'].update({'field_name_id': field_name_id})
                value = sppif_obj._get_value_calculated_with_condition(
                    cr, uid, [], None, None, context=sub_context
                ).get(field_id, value)
                fields.update({
                    field_id: [
                        name,
                        value,
                        field_type,
                        field_name_id,
                    ]
                })
            elif (event == 'none'):
                pass
            else:
                del fields[field_id]
        return {data_field[0]: data_field[1:] for id_field, data_field in fields.iteritems()}

stock_product_pricelist_version()
