# -*- coding: utf-8 -*-
from __future__ import division
from osv import osv
from openerp.addons.pf_utils.utils.decorators import return_json
from json import loads as json_loads
from json import dumps as json_dumps
import logging
from stock_product_pricelist import pricelist_fn
from datetime import datetime
from formula_objects import Formula
from collections import namedtuple

logger = logging.getLogger('delmar.pricing')


customer_price_label = 'Customer Price'
actual_profit_label = 'Actual Profit'
brand_profit_label = 'Brand'
OVERRIDE_PRICE_LABEL = 'Override Price'


def check_price(input_price):
    price = input_price
    if isinstance(price, (str, unicode)):
        price = price.strip().replace('$', '').replace(',', '.')
        try:
            price = float(price)
        except ValueError:
            price = False
    elif isinstance(price, (int, float)):
        price = float(price)
    else:
        price = False
    return price


class ExceptionWithErrMap(Exception):

    err_map = {
        'one_more_rule': 'You have one more rule active for today',
        'not_found_pv': 'Not found active pricelist versions',
    }

    def __init__(self, code):
        super(ExceptionWithErrMap, self).__init__()
        self.code = code

    @property
    def message(self):
        return self.err_map.get(self.code, 'Unknown Error')


class PricelistException(ExceptionWithErrMap):

    def __init__(self, code):
        self.err_map.update({
            'unsupported_firm_id_calculation': 'For now not supported fill values for all partners in firm',
            'unknown_partner': 'Not found partner_id or firm_id',
            'no_product_ids': 'Not Found product_ids',
        })
        super(PricelistException, self).__init__(code)


class ProfitException(ExceptionWithErrMap):

    def __init__(self, code):
        self.err_map.update({
            'no_calculated_price': 'Not found calculated price, please check formula.',
            'no_customer_price': 'Not found Customer Price, please check Customer Fields.',
            'bad_sppv_res': 'Bad type of sppv_result.',
            'bad_type_data': 'Bad type of input data.',
            'not_found_data': 'Not found input data',
            'wrong_calculate_profit': 'Wrong Current Profit Calculation',
        })
        super(ProfitException, self).__init__(code)


class DataDecoder(object):
    def __init__(self):
        super(DataDecoder, self).__init__()

    def decode(self, data):
        new_data = {}
        if isinstance(data, (str, unicode)):
            try:
                data = json_loads(data)
            except Exception:
                pass
        if isinstance(data, dict):
            for partner_id, product_ids in data.iteritems():
                try:
                    new_data[int(partner_id)] = [int(x) for x in product_ids]
                except ValueError:
                    pass
                except Exception:
                    pass
        return new_data if new_data else data


def get_field_name_id(cr, partner_id, field_name):
    field_name_id_sql = """SELECT
                                pmcfn.id AS id
                            FROM
                                product_multi_customer_fields_name AS pmcfn
                            LEFT JOIN
                                product_multi_customer_names AS pmcn
                                    ON pmcfn.name_id = pmcn.id
                            WHERE 1=1
                                AND customer_id = {0}
                                AND pmcn.label = '{1}'
                                AND pmcfn.label NOT LIKE '%NO CUSTOMER%'
                                AND pmcfn.export_label NOT LIKE '%NO CUSTOMER%'
    """.format(partner_id, field_name)
    cr.execute(field_name_id_sql)
    field_name_ids = cr.fetchone()
    field_name_id = field_name_ids and field_name_ids[0] or False
    return field_name_id


def get_cf_from_db(
    cr, partner_id, field_name_id, product_ids=None, product_ids_str=None
):
    result = {}
    if (
        product_ids is not None and
        isinstance(product_ids, (tuple, list))
    ):
        product_ids_str = ','.join([unicode(x) for x in product_ids])
    if partner_id and product_ids_str:
        sql = """SELECT
                    pmcf.product_id as product_id,
                    pmcf.id as pmcf_id
                FROM
                    product_multi_customer_fields AS pmcf
                WHERE 1=1
                    AND pmcf.partner_id = {partner_id}
                    and pmcf.field_name_id = {field_name_id}
                    AND pmcf.product_id IN ({product_ids_str})
                    AND pmcf.size_id IS NULL
        """.format(
            partner_id=partner_id,
            field_name_id=field_name_id,
            product_ids_str=product_ids_str
        )
        cr.execute(sql)
        result = {x['product_id']: x['pmcf_id'] for x in cr.dictfetchall()}
    return result


class pricelist_callbacks(osv.osv_memory):
    _name = "pricelist.callbacks"

    data_decoder = DataDecoder()

    def get_customer_prices(self, cr, uid, partner_id, product_ids, label=customer_price_label):
        res = {}
        if partner_id and product_ids:
            cr.execute('''SELECT
                                pmcf.product_id AS product_id,
                                MAX(pmcf."value") AS customer_price
                            FROM
                                product_multi_customer_fields AS pmcf
                            LEFT JOIN
                                product_multi_customer_fields_name AS pmcfn
                                    ON pmcf.field_name_id = pmcfn.id
                            LEFT JOIN
                                product_multi_customer_fields_name_type AS pmcfnt
                                    ON pmcfn.type_id = pmcfnt.id
                            WHERE 1=1
                                AND pmcf.partner_id = %s  -- partner_id
                                AND pmcf.product_id IN %s  -- product_ids
                                AND pmcf.size_id IS NULL
                                AND pmcfnt.name = %s  -- [customer|override]_price_label
                            GROUP BY
                                pmcf.partner_id,
                                pmcf.product_id
            ''', (partner_id, tuple(product_ids), label, ), )
            res = {row['product_id']: row['customer_price'] for row in cr.dictfetchall()}
        return res

    def get_brands_profit(self, cr, uid, context=None):
        result = {}

        if context is None:
            context = {}

        product_ids = context.get('product_ids', [])
        if product_ids:
            if not isinstance(product_ids, (list, tuple)):
                product_ids = [product_ids]
            product_ids_str = ','.join([unicode(x) for x in product_ids])
            sql = """SELECT
                    pmcf.product_id AS product_id,
                    pmcf."value" AS brand_name,
                    bp.profit AS brand_profit,
                    bp.flat_fee AS flat_fee
                FROM
                    product_multi_customer_fields_name_type AS pmcfnt
                LEFT JOIN
                    product_multi_customer_fields_name AS pmcfn
                        ON pmcfn.type_id = pmcfnt.id
                        AND pmcfn.type_id IS NOT NULL
                LEFT JOIN
                    product_multi_customer_fields AS pmcf
                        ON pmcf.field_name_id = pmcfn.id
                LEFT JOIN brands_profit AS bp ON trim(lower(pmcf."value")) = trim(lower(bp.name))
                WHERE 1=1
                    AND pmcf.partner_id is NULL
                    AND pmcf.product_id IN ({product_ids_str})
                    AND pmcf.size_id IS NULL
                    AND pmcfnt.name = '{brand_profit_label}'
            """.format(
                product_ids_str=product_ids_str,
                brand_profit_label=brand_profit_label
            )
            cr.execute(sql)
            result = {
                res['product_id']: {
                    'profit': res['brand_profit'],
                    'name': res['brand_name'],
                    'flat_fee': res['flat_fee'],
                } for res in cr.dictfetchall()
            }

        return result

    def get_sppv_ids_data_pool(self, cr, uid, all_partner_ids, all_product_ids):
        sppv_obj = self.pool.get('stock.product.pricelist.version')
        # GETTING SPPV_IDS_POOL AND SPPV_IDS_DATA_POOL
        sppv_ids_pool = sppv_obj.sppv_ids_with_partner_id(cr, uid, partner_ids=all_partner_ids)
        sppv_ids_data_pool = sppv_obj.read(
            cr, uid, [item for sublist in sppv_ids_pool.values() for item in sublist],
            ['full_formula', 'gsp_ids', 'fields', 'rounding_method', 'base_price',
                'date_start', 'date_end', 'name', 'overwrite', 'ignore_liquidation', 'currency_discount_rate']
        )
        sppv_ids_data_pool = {x['id']: x for x in sppv_ids_data_pool}
        for id_, value in sppv_ids_data_pool.items():
            value.update({'fields': sppv_obj.get_actual_fields(cr, uid, id_)})
        return sppv_ids_pool, sppv_ids_data_pool

    def get_data_pool_for_all_calculations(self, cr, uid, all_partner_ids, all_product_ids):
        prod_obj = self.pool.get('product.product')
        sppv_ids_pool, sppv_ids_data_pool = self.get_sppv_ids_data_pool(cr, uid, all_partner_ids, all_product_ids)
        # GETTING LIQUIDATION
        prod_liquidations = prod_obj.read(cr, uid, all_product_ids, ['id', 'liquidation'])
        prod_liquidations = {el.get('id'): el.get('liquidation', False) for el in prod_liquidations if el.get('id')}
        conf_obj = self.pool.get('ir.config_parameter')
        # GETTING GSP_FIELDS(Global System Parameters)
        gsp_ids = conf_obj.search(cr, uid, [('key', 'ilike', "pricelist_parametr")])
        gsp_fields = {}
        if (gsp_ids):
            for gsp_id in gsp_ids:
                gsp_row = conf_obj.browse(cr, uid, gsp_id)
                key = gsp_row.key.strip().lower().replace('pricelist_parametr.', '')
                value = gsp_row.value
                gsp_fields.update({key: value})
        # FILL Brand Flat Fee to GSP
        brands_profit_obj = self.pool.get('brands.profit')
        flat_fee_ids = brands_profit_obj.search(cr, uid, [('name', 'like', 'brand_flat_fee')])
        if flat_fee_ids:
            gsp_fields.update({'brand_flat_fee': brands_profit_obj.browse(cr, uid, flat_fee_ids)[0].profit})
        else:
            gsp_fields.update({'brand_flat_fee': 0})
        # GETTING BRANDS_PROFITS
        brands_profits = self.get_brands_profit(
            cr, uid, context={'product_ids': all_product_ids})
        return (
            sppv_ids_pool,
            sppv_ids_data_pool,
            prod_liquidations,
            gsp_fields,
            brands_profits
        )

    def get_base_price_pool(self, cr, uid, product_ids, base_price_name):
        prod_obj = self.pool.get('product.product')
        prod_data = prod_obj.read(cr, uid, product_ids, ['id', base_price_name])
        base_prices_pool = {el.get('id'): el.get(base_price_name, None) for el in prod_data if el.get('id')}
        return base_prices_pool

    def get_customer_fields_pool(self, cr, uid, product_ids, fields):
        customer_fields = {}
        field_name_ids = [str(value[2]) for name_field, value in fields.iteritems() if value[1] == 'field']
        field_ids_names = {value[2]: name_field for name_field, value in fields.iteritems() if value[1] == 'field'}
        if product_ids and field_name_ids:
            cr.execute('''SELECT
                            field_name_id,
                            product_id,
                            max("value") AS val
                        FROM
                            product_multi_customer_fields
                        WHERE 1=1
                            AND field_name_id IN %s  -- field_name_ids
                            AND product_id IN %s  -- product_ids
                        GROUP BY
                            field_name_id,
                            product_id
            ''', (tuple(field_name_ids), tuple(product_ids), ))
            customer_fields_values = cr.dictfetchall()
            for el in customer_fields_values:
                availaible_cfs = customer_fields.get(el['product_id'], {})
                availaible_cfs.update({field_ids_names.get(el['field_name_id'], 'unknown'): el['val']})
                customer_fields.update({el['product_id']: availaible_cfs})
        return customer_fields

    def fill_sizes(self, cr, uid, product_ids):
        result = []
        for product_id in product_ids:
            if isinstance(product_id, (tuple, list)):
                result.append(product_id)
            elif (isinstance(product_id, (int, long))):
                sql = """SELECT
                            rs.id
                        FROM
                            product_product AS pp
                        LEFT JOIN
                            product_ring_size_default_rel AS prs
                                ON pp.product_tmpl_id = prs.product_id
                        LEFT JOIN
                            ring_size AS rs
                                ON prs.size_id = rs.id
                        WHERE 1=1
                            AND pp.id = {product_id}""".format(product_id=product_id)
                cr.execute(sql)
                size_ids = [x[0] for x in cr.fetchall() if x]
                result.append([product_id, size_ids])
            else:
                pass
        return result

    def calculate_pricelists(self, cr, uid, context=None):
        benchmark_report = ''
        result = {
            'errors': [],
            'profile_report': '',
        }
        if (
            context is not None and
            isinstance(context, dict)
        ):
            profile_it = context.get('profile_it', True)
            data = context.get('data', {})
            store_to_customer_fields = context.get('store_to_customer_fields', False)
            return_result = context.get('return_result', False)
            fill_sbs = context.get('fill_result_step_by_step', False)
            fill_profits = context.get('fill_profits', False)
            field_name_id = context.get('field_name_id', False)
            return_json = context.get('return_json', False)
            try:
                # CHECK INPUT DATA
                if not data:
                    raise PricelistException('not_found_data')
                data = self.data_decoder.decode(data)
                if not isinstance(data, dict):
                    raise PricelistException('bad_type_data')
                # GETTING NEEDED DATA
                start = datetime.now()
                # GETTING ALL_PARTNER_IDS AND ALL PRODUCT_IDS
                all_partner_ids = data.keys()
                all_product_ids = []
                for data_by_partner in data.values():
                    for data_by_product in data_by_partner:
                        if isinstance(data_by_product, (int, long, str, unicode)):
                            pp_id = data_by_product
                        elif isinstance(data_by_product, (list, tuple, set)) and len(data_by_product) == 2:
                            pp_id = data_by_product[0]
                        else:
                            continue
                        all_product_ids.append(pp_id)
                # GETTING POOLS
                (
                    sppv_ids_pool,
                    sppv_ids_data_pool,
                    prod_liquidations,
                    gsp_fields,
                    brands_profits
                ) = self.get_data_pool_for_all_calculations(cr, uid, all_partner_ids, all_product_ids)
                end = datetime.now()
                benchmark_report += 'Getting data pools: {}\n'.format(end-start)
                profits_pool = {}
                if fill_profits:
                    profits_pool = json_loads(self.get_actual_profit(
                        cr, uid,
                        {
                            'profile_it': False,
                            'store_to_customer_fields': False,
                            'return_result': True,
                            'data': context.get('data', {}),
                        }
                    ))
                # PROCESSING CALCULATIONS
                start_calculation = datetime.now()
                for partner_id, product_bunches in data.items():
                    product_ids = [(product_id[0] if isinstance(product_id, (tuple, list)) else product_id) for product_id in product_bunches]
                    partner_result = {
                        'errors': [],
                    }
                    sppv_id = sppv_ids_pool.get(partner_id, [])
                    try:
                        if len(sppv_id) > 1:
                            raise PricelistException('one_more_rule')
                        elif len(sppv_id) == 0:
                            raise PricelistException('not_found_pv')
                        else:
                            sppv_id = sppv_id[0]
                            base_prices_pool = self.get_base_price_pool(
                                cr, uid, product_ids, sppv_ids_data_pool[sppv_id]['base_price'])
                            customer_fields_pool = self.get_customer_fields_pool(
                                cr, uid, product_ids, sppv_ids_data_pool[sppv_id]['fields'])
                            partner_profits_pool = profits_pool.get(unicode(partner_id), {})
                            if not context.get('erp_export', False):
                                product_bunches = self.fill_sizes(cr, uid, product_bunches)
                            for product_id_ in product_bunches:
                                size_ids = []
                                if isinstance(product_id_, (tuple, list)):
                                    size_ids = [x for x in product_id_[1] if x]
                                    product_id = product_id_[0]
                                else:
                                    size_ids = []
                                    product_id = product_id_
                                product_result = {
                                    'status': None,
                                    'value': None,
                                    'profits': {},
                                    'message': '',
                                    'errors': [],
                                    'sizes': {},
                                }
                                prod_liquidation = not sppv_ids_data_pool[sppv_id]['ignore_liquidation'] and prod_liquidations.get(product_id, False)
                                # DLMR-2073
                                late_multiplier = 1
                                # check product origin country
                                product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
                                # Check multiplier for CAD & China
                                if float(sppv_ids_data_pool[sppv_id]['currency_discount_rate']) != 1 and product_obj and str(product_obj.prod_manufacture).lower() == 'china':
                                    late_multiplier = late_multiplier * float(sppv_ids_data_pool[sppv_id]['currency_discount_rate'])
                                # END DLMR-2073
                                try:
                                    formula_obj = Formula(
                                        sppv_ids_data_pool[sppv_id]['full_formula'],
                                        gsp_fields,
                                        base_prices_pool[product_id],
                                        sppv_ids_data_pool[sppv_id]['fields'],
                                        customer_fields_pool.get(product_id, {}),
                                        prod_liquidation,
                                        brands_profits.get(product_id, {}),
                                        sppv_ids_data_pool[sppv_id]['rounding_method'],
                                        # DLMR-2073
                                        late_multipliers=late_multiplier,
                                        # END DLMR-2073
                                    )
                                    new_price = formula_obj.calculate_new_customer_price()
                                    if new_price > 0:
                                        new_price = "{0:.2f}".format(new_price).replace('.00', '')
                                        product_result.update({
                                            'status': True,
                                            'value': new_price,
                                            'message': 'Calculated corrected'
                                        })
                                        if size_ids:
                                            for size_id in size_ids:
                                                product_result['sizes'].update({size_id: new_price})
                                    elif new_price is None:
                                        product_result.update({
                                            'message': 'Calculated failed, please check formula'
                                        })
                                    elif new_price < 0:
                                        product_result.update({
                                            'message': 'Calculated incorrect, result value is negative'
                                        })
                                    else:
                                        product_result.update({
                                            'message': 'Calculated incorrect, Unknown error'
                                        })
                                    if fill_sbs:
                                        product_result.update({
                                            'result_step_by_step': formula_obj.json_steps,
                                        })
                                    if fill_profits:
                                        profits = partner_profits_pool.get(unicode(product_id), {})
                                        product_result.update({'profits': profits})
                                except PricelistException as p_ex:
                                    product_result['errors'].append(p_ex.message)
                                except Exception as ex:
                                    product_result['errors'].append(str(ex))
                                finally:
                                    partner_result[product_id] = product_result
                            if store_to_customer_fields:
                                start = datetime.now()
                                end = None
                                self.store_to_db(
                                    cr,
                                    uid,
                                    partner_id,
                                    partner_result,
                                    sppv_ids_data_pool[sppv_id],
                                    context.get('overwrite', None),
                                    context.get('erp_export', False),
                                    field_name_id
                                )
                                end = datetime.now()
                                benchmark_report += 'Store Pricelist\'s to DB: {}\n'.format(end-start)
                    except PricelistException as p_ex:
                        partner_result['errors'].append(p_ex.message)
                    except Exception as ex:
                        partner_result['errors'].append(str(ex))
                    finally:
                        result[partner_id] = partner_result
                end_calculation = datetime.now()
                benchmark_report += 'Calculate Pricelist\'s: {}\n'.format(end_calculation-start_calculation)
                # END CALCULATION
            except PricelistException as p_ex:
                result['errors'].append(p_ex.message)
            except Exception as ex:
                result['errors'].append(str(ex))

        if benchmark_report:
            logger.info(benchmark_report)
        if profile_it:
            result.update({
                'profile_report': benchmark_report,
            })
        if return_json:
            result = json_dumps(result)
        if return_result:
            return result
        else:
            return None

    @return_json
    def get_actual_profit(self, cr, uid, context=None):
        benchmark_report = ''
        result = {
            'errors': [],
            'profile_report': '',
        }
        if (
            context is not None and
            isinstance(context, dict)
        ):
            profile_it = context.get('profile_it', True)
            data = context.get('data', {})
            store_to_customer_fields = context.get('store_to_customer_fields', False)
            return_result = context.get('return_result', False)
            new_price = context.get('new_price', False)
            override = bool(context.get('override', False))
            price_field_label = OVERRIDE_PRICE_LABEL if override else customer_price_label
            try:
                # CHECK INPUT DATA
                if not data:
                    raise ProfitException('not_found_data')
                data = self.data_decoder.decode(data)
                if not isinstance(data, dict):
                    raise ProfitException('bad_type_data')
                # GETTING NEEDED DATA
                start = datetime.now()
                # GETTING ALL_PARTNER_IDS AND ALL PRODUCT_IDS
                all_partner_ids = data.keys()
                all_product_ids = list(set([el for l in data.values() for el in l]))
                # GETTING POOLS
                (
                    sppv_ids_pool,
                    sppv_ids_data_pool,
                    prod_liquidations,
                    gsp_fields,
                    brands_profits
                ) = self.get_data_pool_for_all_calculations(cr, uid, all_partner_ids, all_product_ids)
                end = datetime.now()
                benchmark_report += 'Getting data pools: {}\n'.format(end-start)
                start = datetime.now()
                # GETTING ALL_PARTNER_IDS AND ALL PRODUCT_IDS
                all_partner_ids = data.keys()
                all_product_ids = list(set([el for l in data.values() for el in l]))
                # GETTING POOLS
                (
                    sppv_ids_pool,
                    sppv_ids_data_pool,
                    prod_liquidations,
                    gsp_fields,
                    brands_profits
                ) = self.get_data_pool_for_all_calculations(cr, uid, all_partner_ids, all_product_ids)
                end = datetime.now()
                benchmark_report += 'Getting data pools: {}\n'.format(end-start)
                # PROCESSING CALCULATIONS
                start = end
                end = None
                for partner_id, product_ids in data.items():
                    partner_result = {
                        'errors': [],
                    }
                    sppv_id = sppv_ids_pool.get(partner_id, [])
                    try:
                        if len(sppv_id) > 1:
                            raise ProfitException('one_more_rule')
                        elif len(sppv_id) == 0:
                            raise ProfitException('not_found_pv')
                        else:
                            sppv_id = sppv_id[0]
                            customer_prices = {} if new_price else self.get_customer_prices(cr, uid, partner_id, product_ids, price_field_label)
                            base_prices_pool = self.get_base_price_pool(
                                cr, uid, product_ids, sppv_ids_data_pool[sppv_id]['base_price'])
                            for product_id in product_ids:
                                product_result = {
                                    'profit': None,
                                    'original_profit': None,
                                    'errors': [],
                                }
                                customer_price = new_price or customer_prices.get(product_id, False)
                                customer_price = check_price(customer_price)
                                prod_liquidation = not sppv_ids_data_pool[sppv_id]['ignore_liquidation'] and prod_liquidations.get(product_id, False)
                                # DLMR-2073
                                late_multiplier = 1
                                # check product origin country
                                product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
                                # Check multiplier for CAD & China
                                if float(sppv_ids_data_pool[sppv_id]['currency_discount_rate']) != 1 and product_obj and str(product_obj.prod_manufacture).lower() == 'china':
                                    late_multiplier = late_multiplier * float(sppv_ids_data_pool[sppv_id]['currency_discount_rate'])
                                # END DLMR-2073
                                try:
                                    formula_obj_for_original_profit = Formula(
                                        sppv_ids_data_pool[sppv_id]['full_formula'],
                                        gsp_fields,
                                        base_prices_pool[product_id],
                                        sppv_ids_data_pool[sppv_id]['fields'],
                                        {},
                                        prod_liquidation,
                                        brands_profits.get(product_id, {}),
                                        sppv_ids_data_pool[sppv_id]['rounding_method'],
                                        late_multipliers=late_multiplier
                                    )
                                    formula_obj_for_original_profit.getting_original_profit()
                                    original_profit = formula_obj_for_original_profit.original_profit
                                    product_result.update({'original_profit': original_profit})
                                    if not customer_price:
                                        raise ProfitException('no_customer_price')
                                    customer_fields = {product_id: {'customer_price': customer_price}}
                                    formula_obj = Formula(
                                        sppv_ids_data_pool[sppv_id]['full_formula'],
                                        gsp_fields,
                                        base_prices_pool[product_id],
                                        sppv_ids_data_pool[sppv_id]['fields'],
                                        customer_fields.get(product_id, {}),
                                        prod_liquidation,
                                        brands_profits.get(product_id, {}),
                                        sppv_ids_data_pool[sppv_id]['rounding_method'],
                                        late_multipliers=late_multiplier
                                    )
                                    current_profit = formula_obj.calculate_current_profit()
                                    if not current_profit:
                                        raise ProfitException('wrong_calculate_profit')
                                    product_result.update({'profit': "{:0.2f}".format(current_profit)})
                                except ProfitException as p_ex:
                                    product_result['errors'].append(p_ex.message)
                                except Exception as ex:
                                    product_result['errors'].append(str(ex))
                                finally:
                                    partner_result[product_id] = product_result
                    except ProfitException as p_ex:
                        partner_result['errors'].append(p_ex.message)
                    except Exception as ex:
                        partner_result['errors'].append(str(ex))
                    finally:
                        result[partner_id] = partner_result
                end = datetime.now()
                benchmark_report += 'Calculate Profit\'s: {}\n'.format(end-start)
                # END CALCULATION
            except ProfitException as p_ex:
                result['errors'].append(p_ex.message)
            except Exception as ex:
                result['errors'].append(str(ex))
            finally:
                if store_to_customer_fields:
                    start = end
                    end = None
                    self.store_to_customer_fields(cr, uid, result)
                    end = datetime.now()
                    benchmark_report += 'Store Profit\'s to DB: {}\n'.format(end-start)

        if benchmark_report:
            logger.info(benchmark_report)
        if return_result:
            if profile_it:
                result.update({
                    'profile_report': benchmark_report,
                })
            return result
        else:
            return True

    def create_actual_profit_cf(self, cr, uid, partner_id, context=None):
        pmcfn_obj = self.pool.get('product.multi.customer.fields.name')
        pmcn_obj = self.pool.get('product.multi.customer.names')
        ap_field_name_ids = pmcn_obj.search(cr, uid, [('label', '=', actual_profit_label)])
        if ap_field_name_ids:
            ap_field_name_id = ap_field_name_ids[0]
        else:
            ap_field_name_id = pmcn_obj.create(
                cr, uid,
                {
                    'name': 'actual_profit',
                    'label': actual_profit_label
                },
                context=None
            )
        pmcfn_ids = pmcfn_obj.search(
            cr, uid,
            [
                ('name_id', '=', ap_field_name_id),
                ('customer_id', '=', partner_id),
            ]
        )
        if (pmcfn_ids):
            pass
        else:
            pmcfn_obj.create(
                cr, uid,
                {
                    'name_id': ap_field_name_id,
                    'customer_id': partner_id,
                    'label': actual_profit_label,
                    'export_label': actual_profit_label,
                },
                context=None
            )

    def store_to_customer_fields(self, cr, uid, data):
        full_sql = ''
        for partner_id, profit_array in data.iteritems():
            if partner_id == 'errors':
                continue
            field_name_id = get_field_name_id(cr, partner_id, actual_profit_label)
            if not field_name_id:
                self.create_actual_profit_cf(cr, uid, partner_id)
                field_name_id = get_field_name_id(cr, partner_id, actual_profit_label)
            if field_name_id:
                product_ids_str = ','.join([unicode(key) for key, value in profit_array.iteritems() if key != 'errors'])
                availaible_in_db = get_cf_from_db(cr, partner_id, field_name_id, product_ids_str=product_ids_str)
                for product_id, profit_data in profit_array.iteritems():
                    if product_id == 'errors':
                        continue
                    profit = profit_data['profit']
                    if not profit:
                        continue
                    now_in_db = availaible_in_db.get(product_id, False)
                    if now_in_db:
                        sub_update_sql = """UPDATE
                                                product_multi_customer_fields
                                            SET value = '{value}',
                                                write_uid = {uid},
                                                write_date = (now() at time zone 'UTC')
                                            WHERE id = {field_id};\n
                                        """.format(
                            value=profit,
                            uid=uid,
                            field_id=now_in_db
                        )
                        full_sql += sub_update_sql
                    else:
                        sub_insert_sql = """INSERT INTO
                                                product_multi_customer_fields
                                                    (create_uid, create_date, field_name_id, partner_id, product_id, value)
                                            VALUES
                                                ({uid}, (now() at time zone 'UTC'), {field_name_id}, {partner_id}, {product_id}, '{value}');\n
                        """.format(
                            uid=uid,
                            field_name_id=field_name_id,
                            partner_id=partner_id,
                            product_id=product_id,
                            value=profit
                        )
                        full_sql += sub_insert_sql
        if full_sql:
            cr.execute(full_sql)
        return True

    def get_non_sized_sql(self, cr, uid, partner_id, calculated_costs, field_name_id, overwrite):
        result_sql = ''
        product_ids_str = ','.join([str(key) for key, value in calculated_costs.iteritems() if not isinstance(value, list)])
        availaible_in_db = {}
        if (product_ids_str):
            sql = """SELECT
                        pmcf.product_id as product_id,
                        pmcf.id as pmcf_id
                    FROM
                        product_multi_customer_fields AS pmcf
                    WHERE 1=1
                        AND pmcf.partner_id = {partner_id}
                        and pmcf.field_name_id = {field_name_id}
                        AND pmcf.product_id IN ({product_ids_str})
                        AND pmcf.size_id IS NULL
            """.format(
                partner_id=partner_id,
                field_name_id=field_name_id,
                product_ids_str=product_ids_str
            )
            cr.execute(sql)
            availaible_in_db = {x['product_id']: x['pmcf_id'] for x in cr.dictfetchall()}
        update_sql = ''
        for product_id, pmcf_id in availaible_in_db.iteritems():
            if (product_id in calculated_costs):
                if (overwrite and calculated_costs[product_id]['status']):
                    sub_update_sql = """UPDATE
                                    product_multi_customer_fields
                                SET value = '{value}',
                                    write_uid={uid},
                                    write_date=(now() at time zone 'UTC')
                                WHERE id = {field_id};\n""".format(
                        uid=uid,
                        value=calculated_costs[product_id]['value'],
                        field_id=pmcf_id
                    )
                    update_sql += sub_update_sql
                del calculated_costs[product_id]
        if (update_sql):
            result_sql += update_sql
        if (field_name_id):
            insert_sql = ''
            for product_id, value in calculated_costs.iteritems():
                if (value['status']):
                    sub_insert_sql = """INSERT INTO
                                            product_multi_customer_fields
                                                (create_uid, create_date, field_name_id, partner_id, product_id, value)
                                        VALUES
                                            ({uid}, (now() at time zone 'UTC'), {field_name_id}, {partner_id}, {product_id}, '{value}');\n
                    """.format(
                        uid=uid,
                        field_name_id=field_name_id,
                        partner_id=partner_id,
                        product_id=product_id,
                        value=value['value']
                    )
                    insert_sql += sub_insert_sql
            if (insert_sql):
                result_sql += insert_sql
        return result_sql

    def get_sized_sql(self, cr, uid, partner_id, calculated_costs, field_name_id, overwrite):
        result_sql = ''
        update_sql = ''
        to_delete = []
        for product_id, size_ids_with_cost in calculated_costs.iteritems():
            availaible_in_db = {}
            size_ids_str = ','.join([str(key) for key, value in size_ids_with_cost.iteritems()])
            if (size_ids_str):
                sql = """SELECT
                            pmcf.product_id as product_id,
                            pmcf.size_id as size_id,
                            pmcf.id as pmcf_id
                        FROM
                            product_multi_customer_fields AS pmcf
                        WHERE 1=1
                            AND pmcf.partner_id = {partner_id}
                            and pmcf.field_name_id = {field_name_id}
                            AND pmcf.product_id = {product_id}
                            AND pmcf.size_id IN ({size_ids_str})
                """.format(
                    partner_id=partner_id,
                    field_name_id=field_name_id,
                    product_id=product_id,
                    size_ids_str=size_ids_str
                )
                cr.execute(sql)
                Elem = namedtuple("Elem", ["product_id", "size_id", "pmcf_id"])
                availaible_in_db = [
                    Elem(product_id=x['product_id'], size_id=x['size_id'], pmcf_id=x['pmcf_id'])
                    for x in cr.dictfetchall()
                ]
                for elem in availaible_in_db:
                    size_id = elem.size_id
                    if (size_id in size_ids_with_cost):
                        if (overwrite):
                            sub_update_sql = """UPDATE
                                                    product_multi_customer_fields
                                                SET
                                                    value = '{value}',
                                                    write_uid = {uid},
                                                    write_date = (now() at time zone 'UTC')
                                                WHERE
                                                    id = {field_id};\n
                            """.format(
                                uid=uid,
                                value=size_ids_with_cost[size_id],
                                field_id=elem.pmcf_id
                            )
                            update_sql += sub_update_sql
                        del size_ids_with_cost[size_id]
                if (not calculated_costs[product_id]):
                    to_delete.append(product_id)
        if (update_sql):
            result_sql += update_sql

        for key in to_delete:
            del calculated_costs[key]

        if (field_name_id):
            insert_sql = ''
            for product_id, size_ids_with_cost in calculated_costs.iteritems():
                for size_id, value in size_ids_with_cost.iteritems():
                    sub_insert_sql = """INSERT INTO
                                            product_multi_customer_fields
                                                (create_uid, create_date, field_name_id, partner_id, product_id, size_id, value)
                                        VALUES
                                            ({uid}, (now() at time zone 'UTC'), {field_name_id}, {partner_id}, {product_id}, {size_id}, '{value}');\n
                    """.format(
                        uid=uid,
                        field_name_id=field_name_id,
                        partner_id=partner_id,
                        product_id=product_id,
                        size_id=size_id,
                        value=value
                    )
                    insert_sql += sub_insert_sql
            if (insert_sql):
                result_sql += insert_sql

        return result_sql

    def store_to_db(self, cr, uid, partner_id, calculated_costs, sppv_data, overwrite, erp_export, field_name_id):
        non_sized = {key: value for key, value in calculated_costs.iteritems() if isinstance(key, (int, long))}
        sized = {key: value['sizes'] for key, value in calculated_costs.iteritems() if isinstance(key, (int, long)) and value['sizes']}

        if not erp_export and not field_name_id:
            field_name_id = get_field_name_id(cr, partner_id, pricelist_fn)

        if field_name_id:
            overwrite = overwrite if overwrite is not None else sppv_data['overwrite']
            sql1 = self.get_non_sized_sql(cr, uid, partner_id, non_sized, field_name_id, overwrite)
            sql2 = self.get_sized_sql(cr, uid, partner_id, sized, field_name_id, overwrite)

            sql = sql1 + sql2
            if (sql):
                cr.execute(sql)

        return True

pricelist_callbacks()
