# -*- coding: utf-8 -*-
from json import dumps as json_dumps
from openerp.addons.pf_utils.utils.safe_eval import SAFE_DICT
from openerp.addons.pf_utils.utils.re_utils import str2float
from math import ceil as math_ceil
import traceback
from sympy import solve as sympy_solve
from sympy import symbols as sympy_symbols
import re


class FormulaCalculationStep(object):

    def __init__(self, formula, fields, description):
        super(FormulaCalculationStep, self).__init__()
        self.formula = formula
        self.fields = fields
        self.description = description
        self.errors = []


class Formula(object):

    def __init__(
        self,
        raw_formula,
        gsp_fields,
        base_price,
        fields_definition,
        customer_fields,
        liquidation,
        brand_profit,
        rounding_method,
        late_multipliers=1    # DLMR-2073
    ):
        super(Formula, self).__init__()
        self.raw_formula = raw_formula
        self.current_formula = raw_formula
        self.gsp_fields = gsp_fields
        self.base_price = base_price
        self.fields_definition = fields_definition.copy() if fields_definition else {}
        self.current_fields = {key: value[0] for key, value in fields_definition.items()}
        self.field_ids_names = {value[2]: name_field for name_field, value in fields_definition.items() if value[1] == 'field'}
        self.customer_fields = customer_fields.copy() if customer_fields else {}
        self.liquidation = liquidation
        self.raw_brand_profit = brand_profit
        self.brand_profit = brand_profit.get('profit', 0) or 0
        self.brand_name = brand_profit.get('name') or ''
        self.brand_flat_fee = brand_profit.get('flat_fee', 0) or 0
        self.rounding_method = rounding_method
        self._steps = []
        self.result = None
        self.current_profit = None
        self.raw_original_profit = self.current_fields['Profit']
        self.original_profit = None
        self.late_multipliers = late_multipliers

    @property
    def steps(self):
        return tuple([
            {
                'formula': x.formula,
                'fields': x.fields,
                'description': x.description,
                'errors': x.errors
            }
            for x in self._steps
        ])

    @steps.setter
    def steps(self, value):
        raise NotImplementedError('You can\'t setup property `step`')

    @property
    def json_steps(self):
        return json_dumps(self.steps)

    @json_steps.setter
    def json_steps(self, value):
        raise NotImplementedError('You can\'t setup property `json_steps`')

    def append_step(self, step):
        if not isinstance(step, FormulaCalculationStep):
            raise NotImplementedError('You can append only FormulaCalculationStep object to steps')
        self._steps.append(step)

    def snapshot_step(self, description=''):
        step = FormulaCalculationStep(self.current_formula, self.current_fields, description=description)
        self.append_step(step)
        # if __debug__:
        #     print('===== ' + description + ' =====')
        #     print('\n' + self.current_formula + '\n')
        #     print('\n' + str(self.current_fields) + '\n')
        #     print('\n' + str(self.result) + '\n')

    def initialization(self):
        self.snapshot_step('Initialization')

    def replace_gsp_fields_in_formula(self):
        for key, value in self.gsp_fields.items():
            self.current_formula = self.current_formula.replace(key, "({0})".format(value))
        self.snapshot_step('Replacing Global System Parameters in formula')

    def replace_static_fields_in_formula(self):
        for name_field, value in self.fields_definition.items():
            if value[1] != 'field':
                self.current_formula = self.current_formula.replace(name_field, unicode(value[0]))
        self.snapshot_step('Replacing static fields')

    def getting_base_price(self):
        self.current_fields.update({'base_price': self.base_price})
        self.snapshot_step('Getting base_price')

    def replace_base_price_in_formula(self):
        self.current_formula = self.current_formula.replace('base_price', unicode(self.base_price))
        self.snapshot_step('Replacing base_price')

    # DLMR-2073
    def replace_late_multipliers_in_formula(self):
        self.current_formula = self.current_formula.replace('late_multipliers', unicode(float(self.late_multipliers)))
        self.snapshot_step('Replacing late_multipliers # DLMR-2073')
    # END DLMR-2073

    def getting_customer_fields(self):
        for name_field, value in self.customer_fields.items():
            self.current_fields.update({name_field: value})
        self.snapshot_step('Getting Customer Fields')

    def replace_customer_fields_in_formula(self):
        for name_field, value in self.customer_fields.items():
            try:
                field_value_ = float(value)
            except Exception:
                field_value_ = "'{0}'".format(value)
            self.current_formula = re.sub(r'\b{name}\b'.format(name=name_field), unicode(field_value_), self.current_formula)
        self.snapshot_step('Replacing Customer Fields')

    def getting_original_profit(self):
        profit_param = self.gsp_fields.get('profit_param', None)
        profit_param_value = 0
        if profit_param and self.raw_original_profit:
            try:

                profit_param_value = eval(
                    profit_param.replace(
                        'liquidation', unicode(int(self.liquidation))
                    ).replace(
                        'Profit', unicode(float(self.raw_original_profit))
                    ).replace(
                        'base_price', unicode(float(self.base_price))
                    ).replace(
                        'brand_name', unicode(repr(self.brand_name))
                    )
                )
            except Exception:
                pass
        self.original_profit = profit_param_value
        self.current_fields.update({'original_profit': self.original_profit})
        self.snapshot_step('Getting Original Profit')

    def replace_original_profit_in_formula(self):
        self.current_formula = self.current_formula.replace('profit_param', unicode(float(self.original_profit)))
        self.snapshot_step('Replacing Original Profit')

    def getting_brand_profit(self):
        self.current_fields.update({'brand_profit': self.brand_profit})
        self.snapshot_step('Getting brand_profit')

    def replace_brand_profit_in_formula(self):
        brand_param = self.gsp_fields.get('brand_param', None)
        brand_param_value = 0
        if brand_param and self.brand_profit:
            try:
                brand_param_value = eval(
                    brand_param.replace(
                        'brand_profit', unicode(float(self.brand_profit))
                    )
                )
            except Exception:
                pass
        self.current_formula = self.current_formula.replace('brand_param', unicode(brand_param_value))
        self.snapshot_step('Replacing brand_profit')

    def getting_brand_flat_fee(self):
        self.current_fields.update({'brand_flat_fee': self.brand_flat_fee})
        self.snapshot_step('Getting brand_flat_fee')

    def replace_brand_flat_fee_in_formula(self):
        brand_flat_fee = self.brand_flat_fee
        self.current_formula = self.current_formula.replace(
            'brand_flat_fee', unicode(float(brand_flat_fee))
        )
        self.snapshot_step('Replacing brand_flat_fee')

    def replace_unknow_fields(self):
        for name_field, value in self.fields_definition.items():
            if value[1] == 'field':
                self.current_formula = self.current_formula.replace(name_field, unicode(0))
        for name_bad in ["'None'", "'False'", 'None', 'False']:
            self.current_formula = self.current_formula.replace(name_bad, unicode(0))
        self.snapshot_step('Replacing Unknown Fields')

    def calculation(self):
        try:
            self.result = float(eval(self.current_formula.replace('\n', ' '), SAFE_DICT))
        except Exception:
            self.result = None
        self.snapshot_step('Calculation')

    def rounding(self):
        if self.result is not None:
            if self.rounding_method in ['none', None, False]:
                pass
            elif self.rounding_method == 'top_dollar':
                self.result = math_ceil(self.result)
            elif self.rounding_method == 'top_five':
                self.result = math_ceil(self.result/5.0)*5
        self.snapshot_step('Rounding')

    def cycle(self, *functions):
        for function in functions:
            try:
                function()
            except Exception:
                if self.steps:
                    self._steps[-1].errors.append(traceback.format_exc())
                else:
                    raise
                break

    def calculate_new_customer_price(self):
        self._steps = []
        self.cycle(
            self.initialization,
            self.replace_static_fields_in_formula,
            self.getting_base_price,
            self.replace_base_price_in_formula,
            self.getting_customer_fields,
            self.replace_customer_fields_in_formula,
            self.getting_original_profit,
            self.replace_original_profit_in_formula,
            self.getting_brand_profit,
            self.replace_brand_profit_in_formula,
            self.getting_brand_flat_fee,
            self.replace_brand_flat_fee_in_formula,
            self.replace_unknow_fields,
            self.replace_late_multipliers_in_formula,
            self.calculation,
            self.rounding,
        )
        return self.result

    def calculate_current_profit(self):
        self._steps = []
        self.current_formula = self.current_formula.replace('profit_param', 'Profit')
        customer_price = self.customer_fields.get('customer_price', False)
        if 'Profit' in self.fields_definition:
            del self.fields_definition['Profit']
        if customer_price:
            self.cycle(
                self.initialization,
                self.replace_static_fields_in_formula,
                self.getting_base_price,
                self.replace_base_price_in_formula,
                self.getting_customer_fields,
                self.replace_customer_fields_in_formula,
                self.getting_brand_profit,
                self.replace_brand_profit_in_formula,
                self.getting_brand_flat_fee,
                self.replace_brand_flat_fee_in_formula,
                self.replace_unknow_fields,
                self.replace_late_multipliers_in_formula,
            )
            self.getting_original_profit()
            formula_with_values = self.current_formula
            formula_with_values += ' - ' + unicode(customer_price)
            Profit = sympy_symbols('Profit')
            res = sympy_solve(eval(formula_with_values), Profit)
            self.current_profit = float(res and res[0] or 0)
        return self.current_profit
