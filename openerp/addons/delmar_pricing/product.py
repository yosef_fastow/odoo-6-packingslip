# -*- coding: utf-8 -*-
import logging
from osv import fields, osv
from json import loads as json_loads
from netsvc import ExportService

logger = logging.getLogger(__name__)


SIZE_TOLERANCE_ACTION_INCREMENT = 'inc'
SIZE_TOLERANCE_ACTION_LOCK = 'mark'
SIZE_TOLERANCE_ACTION = 'Size Tolerance'
SIZE_TOLERANCE_INCREMENT_VALUE = 0.5
SIZE_TOLERANCE_DECREMENT_VALUE = -0.5
SIZE_TOLERANCE_LOCKED = '1'
SIZE_TOLERANCE_UNLOCKED = '0'


def fill_values(input_string, dict_values, need_fill):
    result = u'None'
    for name_field in need_fill:
        input_string = input_string.replace(name_field, unicode(dict_values.get(name_field, u'0')))
    try:
        result = eval(input_string)
    except Exception:
        pass
    return unicode(result)


class product_gta_history(osv.osv):
    _name = "product.gta.history"
    _rec_name = 'product_tmpl_id'
    _log_access = False

    _columns = {
        'actor_uid': fields.many2one('res.users', "User", ),
        'action_date': fields.datetime("Date", readonly=True, select=1),
        'product_tmpl_id': fields.many2one("product.template", "Product", ),
        'old_value': fields.float("Old value"),
        'new_value': fields.float("New value"),
    }

    _order = "id DESC"

product_gta_history()


class product_template(osv.osv):
    _inherit = "product.template"

    _columns = {
        'is_auto_standard_price': fields.boolean("Auto", help="Is price calculated automatically?"),
        'gta_history_ids': fields.one2many("product.gta.history", 'product_tmpl_id', "GTA History", ),
    }

product_template()


class product_product(osv.osv):
    _inherit = "product.product"

    def _get_calculated_standard_price(self, cr, uid, ids, *args, **kwargs):
        res = {}

        if not ids:
            return res

        if not isinstance(ids, list):
            ids = [ids]

        for row in self.browse(cr, uid, ids):
            if row.set_component_ids:
                price = 0
                for set_cmp in row.set_component_ids:
                    prod_id = set_cmp.cmp_product_id.id
                    price += set_cmp.qty * self._get_calculated_standard_price(cr, uid, [prod_id])[prod_id]
                res[row.id] = price
            else:
                res[row.id] = sum([x.total_cost for x in row.pack_line_ids])
                res[row.id] += row.miscellaneous or 0
                res[row.id] *= row.markup or 1.0

            if row.is_auto_standard_price:
                row.product_tmpl_id.write({'standard_price': res[row.id]})
        return res

    def _get_normalized_metal_stamp(self, cr, uid, ids, *args, **kwargs):
        res = {}
        if (not ids):
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for product in self.browse(cr, uid, ids):
            metal_stamp = (
                product.prod_metal or product.prod_metal_type
                or (product.metal_stamp_id and product.metal_stamp_id.name)
                or ''
            )
            if 'silver' in str(metal_stamp).lower():
                res[product.id] = 'silver'
            else:
                res[product.id] = 'gold'
        return res

    def _get_pack_product_ids(self, cr, uid, ids, context=None):
        result = {}
        for ppl in self.pool.get('product.pack.line').browse(cr, uid, ids, context=context):
            if ppl and ppl.parent_product_id and ppl.parent_product_id.id:
                result[ppl.parent_product_id.id] = True

        return self.pool.get('product.product')._get_product_ids(cr, uid, result.keys(), context=context)

    def _get_parent_set_product_ids(self, cr, uid, ids, context=None):
        result = {}
        for ppl in self.pool.get('product.set.components').browse(cr, uid, ids, context=context):
            if ppl.parent_product_id and ppl.parent_product_id.id:
                result[ppl.parent_product_id.id] = True

        return self.pool.get("product.product")._get_product_ids(cr, uid, result.keys(), context=context)

    def _get_product_ids(self, cr, uid, ids, context=None):
        result = {}
        for pp in self.browse(cr, uid, ids, context=context):
            for set_item in pp.set_parent_ids:
                if set_item.parent_product_id:
                    result[set_item.parent_product_id.id] = True
        return ids + result.keys()

    def _get_calculated_standard_price_formula(self, cr, uid, ids, *args, **kwargs):
        res = {}
        for product in self.browse(cr, uid, ids):
            if product.set_component_ids:
                res[product.id] = 'Sum(Set\'s item price * qty)'
            else:
                res[product.id] = '(Components Sum + Miscellaneous) * Markup'
        return res

    def _get_calculated_standard_price_details(self, cr, uid, ids, *args, **kwargs):
        res = {}
        for product in self.browse(cr, uid, ids):
            formula = ''
            if product.set_component_ids:
                formula = " + ".join(['{price:.2f} * {qty:.2f}'.format(
                    price=self._get_calculated_standard_price(cr, uid, [set_cmp.cmp_product_id.id])[set_cmp.cmp_product_id.id],
                    qty=set_cmp.qty
                ) for set_cmp in product.set_component_ids])
            else:
                formula = '({components_sum:.2f} + {miscellaneous:.2f}) * {markup:.2f}'.format(
                    components_sum=sum([pack_line.total_cost
                                        for pack_line in product.pack_line_ids]),
                    miscellaneous=product.miscellaneous,
                    markup=product.markup or 1.0,
                )
            res[product.id] = '{formula} = {calculated_standard_price:.2f}'.format(
                formula=formula,
                calculated_standard_price=product.calculated_standard_price,
            )

        return res

    _columns = {
        'default_code': fields.char('SKU No.', size=64, select=True),
        'show_cost_for_customers': fields.boolean('Show', ),
        'line_ids': fields.one2many('product.cost.for.customer', 'report_id', 'Items', ),
        'show_changelog': fields.boolean('Show Changelog'),
        'log_ids': fields.one2many('product.audit.logs', 'product_id', 'Changelog'),
        'btn_inc_size_tolerance': fields.boolean('Increment Size Tolerance', ),
        'btn_dec_size_tolerance': fields.boolean('Decrement Size Tolerance', ),
        'btn_lock_size_tolerance': fields.boolean('Lock Size Tolerance', ),
        'btn_unlock_size_tolerance': fields.boolean('Unlock Size Tolerance', ),
        'st_line_ids': fields.one2many('product.product.line', 'customer_id_delmar', 'Items', ),
        'show_product_list': fields.boolean('Show', ),
        'calculated_standard_price': fields.function(
            _get_calculated_standard_price,
            string='Calculated Cost',
            readonly=True,
            store={
                'product.set.components': (
                    _get_parent_set_product_ids,
                    ['parent_product_id', 'cmp_product_id', 'qty'],
                    9
                ),
                'product.product': (
                    _get_product_ids,
                    ['pack_line_ids', 'miscellaneous', 'markup', 'exclude_ship_and_duty'],
                    40
                ),
                'product.pack.line': (
                    _get_pack_product_ids,
                    ['total_cost'],
                    11
                ),
            },
        ),
        'normalized_metal_stamp': fields.function(
            _get_normalized_metal_stamp,
            string='Normalized Metal Stamp',
            readonly=True,
        ),
        'markup': fields.float('Markup'),
        'miscellaneous': fields.float('Miscellaneous', digits=(16, 4)),
        'calculated_standard_price_formula': fields.function(
            _get_calculated_standard_price_formula,
            type='char',
            string='Calculated Cost Formula'
        ),
        'calculated_standard_price_details': fields.function(
            _get_calculated_standard_price_details,
            type='char',
            string='Calculated Cost Details',
        ),
        'exclude_ship_and_duty': fields.boolean("Ignore S&D", help="Exclude Shipping & Duty from price calculation", ),
        'labour_cost_mode': fields.selection(
            [
                ('max', 'Max'),
                ('last', 'Last'),
            ],
            'Labour Cost',
            help="What kind of cost use for labours"
        ),
    }

    _defaults = {
        'miscellaneous': 0,
        'markup': 1.0,
        'labour_cost_mode': 'max'
    }

    def init(self, cr):

        cr.execute("""
            CREATE OR REPLACE FUNCTION update_gta_history() RETURNS TRIGGER AS $update_gta_history$
                BEGIN

                    IF  (
                        TG_OP = 'UPDATE'
                    ) THEN

                        IF (
                            coalesce(NEW.standard_price, 0) != coalesce(OLD.standard_price, 0)
                        ) THEN
                            INSERT INTO product_gta_history (
                                product_tmpl_id,
                                actor_uid,
                                action_date,
                                old_value,
                                new_value
                            ) values (
                                NEW.id,
                                NEW.write_uid,
                                (now() at time zone 'UTC'),
                                OLD.standard_price,
                                NEW.standard_price
                            );
                        END IF;

                    ELSE
                        INSERT INTO product_gta_history (
                            product_tmpl_id,
                            actor_uid,
                            action_date,
                            new_value
                        ) values (
                            NEW.id,
                            NEW.create_uid,
                            (now() at time zone 'UTC'),
                            NEW.standard_price
                        );

                    END IF;

                    RETURN NEW;
                END;
            $update_gta_history$ LANGUAGE plpgsql;
        """)

        cr.execute("""
            DROP TRIGGER IF EXISTS check_gta_update ON product_template;
            CREATE TRIGGER check_gta_update
                AFTER INSERT OR UPDATE ON product_template
                FOR EACH ROW
                EXECUTE PROCEDURE update_gta_history();
        """)


        cr.execute("""
            --
            -- Create Audit schema and table
            --
            CREATE schema IF NOT EXISTS audit;
            REVOKE CREATE ON schema audit FROM public;

            CREATE TABLE IF NOT EXISTS audit.logged_actions (
                event_id bigserial PRIMARY KEY,
                schema_name text NOT NULL,
                table_name text NOT NULL,
                user_name text,
                client_addr inet,
                transaction_id BIGINT,
                record_id  BIGINT,
                action_tstamp timestamp WITH time zone NOT NULL DEFAULT current_timestamp,
                action TEXT NOT NULL CHECK (action IN ('I','D','U')),
                original_data json,
                new_data json,
                query text
            ) WITH (fillfactor=100);

            REVOKE ALL ON audit.logged_actions FROM public;

            GRANT SELECT ON audit.logged_actions TO public;

            --
            -- Create indices. The syntax with [IF NOT EXISTS] is valid only for PostgreSQL v. 9.5 and newer
            --
            --CREATE INDEX IF NOT EXISTS logged_actions_table_record_idx
            --ON audit.logged_actions(table_name, record_id);

            --CREATE INDEX IF NOT EXISTS logged_actions_transaction_id_idx
            --ON audit.logged_actions(transaction_id);

            --CREATE INDEX IF NOT EXISTS logged_actions_action_idx
            --ON audit.logged_actions(action);

            --CREATE INDEX IF NOT EXISTS logged_actions_action_tstamp_idx
            --ON audit.logged_actions(action_tstamp);

            --
            -- json build function:
            --
            CREATE OR REPLACE FUNCTION audit.build_json(json_keys ANYARRAY, json_values ANYARRAY) RETURNS JSON AS
            $$
            DECLARE
              json_string TEXT := '{';
              delimeter TEXT := '';
              json_result JSON;
            BEGIN
              FOR i IN array_lower(json_keys, 1)..array_upper(json_keys, 1) LOOP
                json_string := json_string || delimeter || json_keys[i] || ':' || json_values[i];
                delimeter := ',';
              END LOOP;
              json_string := json_string || '}';
              EXECUTE format('SELECT %L::json', json_string) INTO json_result;
              RETURN json_result;
            END
            $$
            LANGUAGE plpgsql;

            --
            -- Audit trigger function:
            --
            CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
            DECLARE
                v_old_data json;
                v_new_data json;
            BEGIN
                IF (TG_OP = 'UPDATE') THEN
                    IF (OLD.* is distinct from NEW.*) THEN
                        v_old_data := (SELECT audit.build_json(array_agg(to_json(old_js.key)), array_agg(old_js.value))
                                        FROM json_each(row_to_json(OLD)) old_js
                                        LEFT OUTER JOIN json_each(row_to_json(NEW)) new_js ON old_js.key = new_js.key
                                        WHERE new_js.value::text <> old_js.value::text OR new_js.key IS NULL
                                    );
                        v_new_data := (SELECT audit.build_json(array_agg(to_json(new_js.key)), array_agg(new_js.value))
                                        FROM json_each(row_to_json(OLD)) old_js
                                        RIGHT OUTER JOIN json_each(row_to_json(NEW)) new_js ON old_js.key = new_js.key
                                        WHERE new_js.value::text <> old_js.value::text OR old_js.key IS NULL
                                    );
                        INSERT INTO audit.logged_actions (event_id,schema_name,table_name,user_name,action,original_data,new_data,query,client_addr,transaction_id,record_id)
                        VALUES (NEXTVAL('audit.logged_actions_event_id_seq'),TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data,v_new_data, current_query(), inet_client_addr(), txid_current(), OLD.id);
                    ELSE
                        RAISE INFO '[AUDIT.IF_MODIFIED_FUNC] - Nothing Updated';
                    END IF;
                    RETURN NEW;
                ELSIF (TG_OP = 'DELETE') THEN
                    v_old_data := row_to_json(OLD.*);
                    INSERT INTO audit.logged_actions (event_id,schema_name,table_name,user_name,action,original_data,query,client_addr,transaction_id,record_id)
                    VALUES (NEXTVAL('audit.logged_actions_event_id_seq'),TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data, current_query(), inet_client_addr(), txid_current(),OLD.id);
                    RETURN OLD;
                ELSIF (TG_OP = 'INSERT') THEN
                    v_new_data := row_to_json(NEW.*);
                    INSERT INTO audit.logged_actions (event_id,schema_name,table_name,user_name,action,new_data,query,client_addr,transaction_id,record_id)
                    VALUES (NEXTVAL('audit.logged_actions_event_id_seq'),TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_new_data, current_query(), inet_client_addr(), txid_current(),NEW.id);
                    RETURN NEW;
                ELSE
                    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - Other action occurred: %, at %',TG_OP,now();
                    RETURN NULL;
                END IF;
            EXCEPTION
                WHEN data_exception THEN
                    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                    RETURN NULL;
                WHEN unique_violation THEN
                    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                    RETURN NULL;
                WHEN OTHERS THEN
                    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
                    RETURN NULL;
            END;
            $body$
            LANGUAGE plpgsql
            SECURITY DEFINER
            SET search_path = pg_catalog, audit;

            --
            -- Add triggers to tables:
            --
            DROP TRIGGER IF EXISTS product_product_audit ON product_product;
            CREATE TRIGGER product_product_audit
            AFTER UPDATE OR INSERT OR DELETE ON product_product
            FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();

            DROP TRIGGER IF EXISTS product_template_audit ON product_template;
            CREATE TRIGGER product_template_audit
            AFTER UPDATE OR INSERT OR DELETE ON product_template
            FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();

            DROP TRIGGER IF EXISTS product_pack_line_audit ON product_pack_line;
            CREATE TRIGGER product_pack_line_audit
            AFTER UPDATE OR INSERT OR DELETE ON product_pack_line
            FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
         """)

    def _check_auto_standart_price(self, cr, uid):
        cr.execute("SELECT 1 FROM res_groups_users_rel rel LEFT JOIN res_groups gr ON gr.id = rel.gid WHERE 1=1 AND gr.name = 'Auto standard price' AND rel.uid =%s", (uid,))

        return False if cr.fetchone() else True

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if 'is_auto_standard_price' in vals and self._check_auto_standart_price(cr, uid):
            vals['is_auto_standard_price'] = not vals['is_auto_standard_price']

        new_product_id = super(product_product, self).create(cr, uid, vals, context)

        if new_product_id and uid in [1] and 'pack_line_ids' in vals:
            prod = self.browse(cr, uid, new_product_id)
            prod.write({
                'standard_price': prod.calculated_standard_price,
                'is_auto_standard_price': True,
            }, context=context)

        return new_product_id


    def write(self, cr, uid, ids, vals, context=None):
        if 'is_auto_standard_price' in vals and self._check_auto_standart_price(cr, uid):
            vals['is_auto_standard_price'] = not vals['is_auto_standard_price']
        if 'standard_price' in vals and uid and self._check_auto_standart_price(cr, uid):
            vals['is_auto_standard_price'] = False

        return super(product_product, self).write(cr, uid, ids, vals, context=context)

    def _add_change(self, transactions, change, model, model_id):
        ignore_fields = [
            'write_uid', 'write_date', 'create_uid', 'create_date', 'id'
        ]
        if not change['fields']:
            return None
        if change['transact_id'] not in transactions:
            action_date = (change['action_date'].replace(tzinfo=None) -
                           change['action_date'].tzinfo._offset)
            transaction = {
                'user_id': change['uid'],
                'method': change['action'],
                'action_date': str(action_date),
                'changed_fields': '',
                'field_ids': [],
            }
        else:
            transaction = transactions[change['transact_id']]
        fields_vals = []
        fields_names = []
        for field in change['fields']:
            if field in ignore_fields:
                continue
            old_value, new_value = change['fields'][field]
            field_inst = model._columns[field]
            field_string = field_inst.string
            if field_string == 'unknown':
                field_string = str(field)
            fields_names.append(field_string)
            fields_vals.append({
                'object_id': model_id,
                'field_name': field_string,
                'old_value': old_value,
                'new_value': new_value,
            })
        if not fields_vals:
            return None
        transaction['field_ids'].extend(fields_vals)
        transactions[change['transact_id']] = transaction
        if change['action'] == 'write':
            if transaction['changed_fields']:
                transaction['changed_fields'] += '; '
            transaction['changed_fields'] += ', '.join(fields_names)
        return True

    def _add_transactions(self, cr, uid, model, product_id, transactions):
        cr.execute("SELECT id FROM ir_model WHERE model=%s", (model._name,))
        model_id = cr.fetchone()[0]
        if self == model: # product_product
            record_id = product_id
        else: # product_template
            product_data = self.read(cr, uid, product_id, {'product_tmpl_id'})
            record_id = product_data.get('product_tmpl_id')[0]
        record_changes = model.get_history_changes(cr, uid, record_id)
        for change in record_changes:
            self._add_change(transactions, change, model, model_id)

    def show_changelog(self, cr, uid, ids, context=None):
        transactions = {}
        product_id = ids and ids[0] if isinstance(ids, (list, tuple)) else ids
        self._add_transactions(cr, uid, self, product_id, transactions)
        template_obj = self.pool.get('product.template')
        self._add_transactions(cr, uid, template_obj, product_id, transactions)
        log_ids = []
        for transact_id in transactions:
            transactions[transact_id].update({'transaction_id': transact_id})
            log_ids.append(transactions[transact_id])
        return {'value': {'log_ids': log_ids}}

    def update_list(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')

        value = {
            'line_ids': [],
        }
        warning = {}

        cr.execute('SELECT id, name FROM res_partner WHERE customer IS TRUE')
        partner_names = {i['id']: i['name'] for i in cr.dictfetchall()}
        data = {partner_id: [ids] for partner_id in partner_names.keys()}
        pricelists_values = pricelist_callbacks_obj.calculate_pricelists(
            cr, uid,
            {
                'profile_it': True,
                'store_to_customer_fields': False,
                'return_result': True,
                'fill_result_step_by_step': True,
                'fill_profits': True,
                'data': data
            }
        )
        # errors = []
        if 'errors' in pricelists_values:
            # errors = pricelists_values.get('errors', [])
            del pricelists_values['errors']

        # profile_report = ''
        if 'profile_report' in pricelists_values:
            # profile_report = pricelists_values.get('profile_report', [])
            del pricelists_values['profile_report']

        brands_profits_pool = pricelist_callbacks_obj.get_brands_profit(
            cr, uid, context={'product_ids': [ids]})
        liquidation = self.read(cr, uid, ids, ['liquidation']).get('liquidation', False)
        sppv_ids_pool, sppv_ids_data_pool = pricelist_callbacks_obj.get_sppv_ids_data_pool(
            cr, uid, partner_names.keys(), [ids])
        sppv_ids_data_pool = {x: sppv_ids_data_pool[y[0]] for x, y in sppv_ids_pool.items() if y}

        for partner_id, partner_data in pricelists_values.items():
            product_data = partner_data.get(ids, {})
            partner_errors = partner_data.get('errors', [])
            sub_value = {
                'customer_id': partner_id,
                'customer_name': partner_names[partner_id],
                'cost': 'not calculated',
                'date_start': False,
                'date_end': False,
                'rule_name': False,
                'answer': False,
                'sppv_id': False,
                'original_profit': False,
                'current_profit': False,
                'brand_profit': brands_profits_pool.get(ids, {}).get('profit', ''),
                'brand_name': brands_profits_pool.get(ids, {}).get('name', ''),
                'brand_flat_fee': brands_profits_pool.get(ids, {}).get('flat_fee', ''),
                'liquidation': liquidation,
            }
            sppv_data = sppv_ids_data_pool.get(partner_id, {})
            if sppv_data:
                sub_value.update({
                    'date_start': sppv_data['date_start'],
                    'date_end': sppv_data['date_end'],
                    'rule_name': sppv_data['name'],
                    'sppv_id': sppv_data['id'],
                })
            if product_data:
                pricelist_value = product_data.get('value', False)
                product_errors = product_data.get('errors', [])
                sub_value.update({
                    'customer_id': partner_id,
                    'customer_name': partner_names[partner_id],
                    'cost': pricelist_value or '',
                    'answer': product_data['message'] or '|'.join([x for x in product_errors]),
                    'brand_profit': brands_profits_pool.get(ids, {}).get('profit', ''),
                    'brand_name': brands_profits_pool.get(ids, {}).get('name', ''),
                    'brand_flat_fee': brands_profits_pool.get(ids, {}).get('flat_fee', ''),
                    'liquidation': liquidation,
                })
                profits = product_data.get('profits', {})
                if profits:
                    sub_value.update({
                        'original_profit': profits.get('original_profit', False),
                        'current_profit': profits.get('profit', False) or '|'.join([x for x in profits.get('errors', [])]),
                    })
                if 'result_step_by_step' in product_data:
                    res_sbs = json_loads(product_data['result_step_by_step'])
                    if res_sbs:
                        pcf_ids = []
                        start = res_sbs[0]
                        end = res_sbs[-1]
                        pcfs = {}
                        for field_name, field_value in start['fields'].items():
                            pcfs[field_name] = {
                                'name': field_name,
                                'raw_value': field_value,
                                'value': False,
                            }
                        for field_name, field_value in end['fields'].items():
                            if field_name in pcfs:
                                pcfs[field_name].update({
                                    'value': field_value,
                                })
                            else:
                                pcfs[field_name] = {
                                    'name': field_name,
                                    'raw_value': False,
                                    'value': field_value,
                                }
                        for pcf, pcf_data in pcfs.items():
                            pcf_ids.append((0, False, pcf_data))
                        sub_value.update({
                            'formula': start['formula']
                            .replace('profit_param', 'Profit')
                            .replace('brand_param', 'Brand')
                            .replace('brand_flat_fee', 'Brand Flat Fee'),
                            'formula_with_values': end['formula'],
                            'pcf_ids': pcf_ids,
                        })
            else:
                if partner_errors:
                    sub_value.update({
                        'answer': '|'.join([x for x in partner_errors]),
                    })
            value['line_ids'].append((0, False, sub_value))

        return {'value': value, 'warning': warning}

    def show_products(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        pmcn_obj = self.pool.get('product.multi.customer.names')
        pmcf_obj = self.pool.get('product.multi.customer.fields')
        pmcfn_obj = self.pool.get('product.multi.customer.fields.name')

        name_ids = pmcn_obj.search(cr, uid, [('name', 'in', ('locked_size_tolerance',))])
        field_name_ids = pmcfn_obj.search(cr, uid, [('name_id', 'in', name_ids)])
        pmcf_ids = pmcf_obj.search(cr, uid, [('product_id', '=', ids), ('field_name_id', 'in', field_name_ids)])
        pmcf_items = pmcf_obj.browse(cr, uid, pmcf_ids)
        partners = dict((x.partner_id.id, x.value) for x in pmcf_items)

        st_ids = pmcn_obj.search(cr, uid, [('name', 'in', ('size_tolerance',))])
        fn_ids = pmcfn_obj.search(cr, uid, [('name_id', 'in', st_ids)])
        pmcf_ids = pmcf_obj.search(cr, uid, [('product_id', '=', ids), ('field_name_id', 'in', fn_ids), ('partner_id', '!=', None)])
        items = pmcf_obj.browse(cr, uid, pmcf_ids)

        value = {
            'st_line_ids': [],
        }
        for item in items:
            sub_value = {
                'customer_id_delmar': item.partner_id.id,
                'size_id': item.size_id.id,
                'size_tolerance': float(item.value) if item.size_id.id else '',
                'locked_size_tolerance': True if (item.partner_id.id in partners.keys()) and (partners[item.partner_id.id] == '1') else False,
            }
            value['st_line_ids'].append((0, False, sub_value))

        return {'value': value, 'warning': {}}

    def mass_change_parametres(self, cr, uid, ids, st_line_ids, action_name, action, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        ppl_obj = self.pool.get('product.product.line')

        if action_name == SIZE_TOLERANCE_ACTION:  # Change size tolerance
            # Set value to next size tolerance e.g. 2.5 or 1.0
            # by increment or decrement on SIZE_TOLERANCE_INCREMENT_VALUE value
            value = str(float(st_line_ids[0][2]['size_tolerance']) + (SIZE_TOLERANCE_INCREMENT_VALUE
                           if action == SIZE_TOLERANCE_ACTION_INCREMENT
                           else SIZE_TOLERANCE_DECREMENT_VALUE
                           if float(st_line_ids[0][2]['size_tolerance'])+SIZE_TOLERANCE_DECREMENT_VALUE >= 0 else 0))
        else:  # or lock or unlock size
            value = SIZE_TOLERANCE_LOCKED if action == SIZE_TOLERANCE_ACTION_LOCK else SIZE_TOLERANCE_UNLOCKED

        # Filter by partner id
        partner_ids = set(x[2]['customer_id_delmar'] for x in st_line_ids)
        for partner_id in partner_ids:
            lines = filter(lambda y, i=partner_id: y[2]['customer_id_delmar'] == i, st_line_ids)
            ppl_obj.change_cf(cr, uid, ids, ids, lines, action_name, value)

        return self.show_products(cr, uid, ids, context)

    def cron_update_labour_from_invoice(self, cr, uid, ids=None, days=1, context=None):
        # Repair PO can be loaded from file (import_purchase_order_prepare)
        # or created on flash from stock screen flash_from_stock.
        # Select it all
        cr.execute("""(select po.name from import_purchase_order_prepare ipop inner join 
            purchase_order po on po.name = concat('PO',ipop.po_name)
            where date(po.create_date)>=date(current_date - interval '3 years')
            )
            union
            (
            select po.name from flash_from_stock ffs inner join purchase_order po on po.id=ffs.po_id
            where date(po.create_date)>=date(current_date - interval '3 years'))""")
        # And create subquery for sql below 'IN (<po_names>)' clause
        repair_po_condition = "AND sd.po NOT IN ('{}')".format("', '".join(map(lambda x: x['name'], cr.dictfetchall())))

        # DLMR-146 start
        # Old query
        # sql = """
        # WITH new_labour_invoices AS (
        #     SELECT DISTINCT sd.STYLE
        #     FROM
        #         shipping s
        #         LEFT JOIN
        #         shipping_details sd
        #             ON s.invoice = sd.invoice
        #     WHERE 1=1
        #         AND s.date_ > DATEADD(day, ?, GETDATE())
        #         AND sd.type = 'Lab'
        # )
        # SELECT
        #     sd.STYLE id_delmar,
        #     MAX(sd.price_pre) labour_cost
        # FROM
        #     shipping s
        #     LEFT JOIN
        #     shipping_details sd
        #         ON s.invoice = sd.invoice
        # WHERE 1=1
        #     AND s.date_ > DATEADD(year, -3, GETDATE())
        #     AND sd.type = 'Lab'
        #     AND sd.STYLE IN (SELECT * FROM new_labour_invoices)
        # GROUP BY
        #     sd.STYLE
        # """
        #
        # New one
        sql = """
        WITH new_labour_invoices AS
          ( SELECT DISTINCT sd.STYLE
           FROM shipping s
           LEFT JOIN shipping_details sd ON s.invoice = sd.invoice
           LEFT JOIN singfield_shipping  ss ON ss.invoice = s.invoice
           WHERE 1=1
             AND (s.date_ >= cast(DATEADD(DAY, ?, GETDATE()) AS Date) or ss.date_ >= cast(DATEADD(DAY, ?, GETDATE()) AS Date))
             AND sd.type = 'Lab' 
             -- start temporary solution before DLRM-1340
AND (sd.po not like 'PORO%' and sd.po not like '%RTV%' and sd.po not  in ('PO887550','PO887543','PO887544','PO887548','PO887551','PO887555','PO04062016','PO887546','PO887549','PO887553','PO887554','PO887556','PO887559','PO887560','PO887562','PO887567','PO887568','POICARETURNS07252016','POICEAUS042016','PO-RETURN102016'))
             -- end temporary solution before DLRM-1340
            {repair_po_condition}
          )
        SELECT id_delmar,
               max_labour_cost,
               last_labour_cost
        FROM
          ( SELECT ROW_NUMBER() OVER (PARTITION BY lb.style
                                      ORDER BY lb.id DESC) AS id,
                   lb.STYLE id_delmar,
                   MAX(lb.labour_cost) OVER (PARTITION BY lb.style) max_labour_cost,
                                       LAST_VALUE(lb.labour_cost) OVER (PARTITION BY lb.style
                                                                        ORDER BY lb.id ASC) AS last_labour_cost
           FROM
             ( SELECT max(sd.id) AS id,
                      sd.STYLE,
                      sum(sd.price_pre) AS labour_cost,
                      sd.invoice,
                      sd.po
              FROM shipping_details sd
              LEFT JOIN shipping s ON sd.PO = s.PO
              AND sd.style = s.style
              AND sd.invoice = s.invoice
              WHERE 1=1
                AND sd.type = 'Lab'
                AND sd.price_pre > 0
                -- start temporary solution before DLRM-1340
AND (sd.po not like 'PORO%' and sd.po not like '%RTV%' and sd.po not  in ('PO887550','PO887543','PO887544','PO887548','PO887551','PO887555','PO04062016','PO887546','PO887549','PO887553','PO887554','PO887556','PO887559','PO887560','PO887562','PO887567','PO887568','POICARETURNS07252016','POICEAUS042016','PO-RETURN102016'))
                -- end temporary solution before DLRM-1340
                {repair_po_condition}
                AND sd.style IN
                  (SELECT *
                   FROM new_labour_invoices) 
                AND s.date_ > DATEADD(YEAR, -3, GETDATE())
              GROUP BY sd.style,
                       sd.po,
                       sd.invoice ) lb )a
        WHERE id = 1
        """.format(repair_po_condition=repair_po_condition)
        # DLMR-146 end

        db_server = self.pool.get('fetchdb.server')
        server_id = db_server.get_sale_servers(cr, uid, single=True)

        products_to_update_labour = db_server.make_query(
            cr, uid, server_id, sql,[-days,-days], select=True)

        update_len = len(products_to_update_labour or [])

        for idx, (id_delmar, max_labour_cost, last_labour_cost) in enumerate(products_to_update_labour or [], 1):
            # DLMR-146 start
            product_ids = self.search(cr, uid, [('default_code', '=', id_delmar)])
            product = self.browse(cr, uid, product_ids, context)[0]
            if product.labour_cost_mode == u'max':
                labour_cost = max_labour_cost
            else:
                labour_cost = last_labour_cost
            logger.info('Update labour prices for {} on {}'.format(id_delmar, labour_cost))
            # DLMR-146 end

            for attempt in range(5):
                try:
                    step_cr = self.pool.db.cursor()
                    logger.info('[{idx} / {len}] Update labour prices for {id_delmar}'.format(
                        idx=idx,
                        len=update_len,
                        id_delmar=id_delmar,
                    ))
                    self.update_one_labour(step_cr, uid, id_delmar, labour_cost)
                    break
                except:
                    logger.info('Failed update labour prices for {id_delmar}'.format(
                        id_delmar=id_delmar,
                    ))
                finally:
                    step_cr.commit()
                    step_cr.close()

    def update_one_labour(self, cr, uid, id_delmar, labour_cost):
        product_ids = self.search(cr, uid, [('default_code', '=', id_delmar)])

        if not product_ids:
            return

        pack_line_model = self.pool.get('product.pack.line')
        domain = [('parent_product_id', 'in', product_ids)]
        pack_line_ids = pack_line_model.search(cr, uid, domain)

        labour_pack_lines = filter(lambda x: x.product_id.categ_id.name == 'Labor',
                                   pack_line_model.browse(cr, uid, pack_line_ids))
        labour_pack_line_ids = [pack_line.id for pack_line in labour_pack_lines]

        for labour_pack_line_id in labour_pack_line_ids:
            self.pool.get('product.product').write(cr, uid, product_ids, {
                'pack_line_ids': [(1, labour_pack_line_id, {
                    'quantity': 1, 'cost': labour_cost, 'cost_uom': labour_cost
                })]
            })

    def recalculate_calculated_standard_price(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not ids:
            return False
        if not isinstance(ids, list):
            ids = [ids]
        ppl = self.pool.get('product.pack.line')
        for product_id in ids:
            obj = self.browse(cr, uid, product_id)
            obj.write({'pack_line_ids': [ppl.browse(cr, uid, obj.read(['pack_line_ids'])[0]['pack_line_ids'])]})
        return True

product_product()


class product_product_line(osv.osv_memory):
    _name = "product.product.line"

    _rec_name = "product_id"

    _columns = {
        'customer_id_delmar': fields.many2one('res.partner', 'Customer', ),
        'size_id': fields.many2one('ring.size', 'Size', readonly=True, ),
        'size_tolerance': fields.float('Size Tolerance', ),
        'locked_size_tolerance': fields.boolean('Locked size tolerance', ),
    }

    def change_cf(self, cr, uid, ids, product_id, lines, cf_name, value, context=None):
        def _get_size_ids(lines):
            """No comments."""
            return map(lambda x: (x[2]['size_id']), lines)

        cm_f_obj = self.pool.get('product.multi.customer.fields')
        cm_fm_obj = self.pool.get('product.multi.customer.fields.name')
        psswd = self.pool.get('res.users').read(cr, uid, uid, ['password']).get('password')
        customer_id = lines[0][2]['customer_id_delmar']

        cm_fields_name_ids = cm_fm_obj.search(cr, uid, [('label', '=', cf_name), ('customer_id', '=', customer_id)],
                                              context=context)
        cm_fields_name = cm_fm_obj.read(cr, uid, cm_fields_name_ids[0], ['expand_ring_sizes', 'name_id'])
        size_ids = _get_size_ids(lines) if (cf_name == SIZE_TOLERANCE_ACTION) \
            else (_get_size_ids(lines) if cm_fields_name.get('expand_ring_sizes', False) else [False])
        for size_id in size_ids:
            cm_field_domain = [
                ('product_id', '=', product_id),
                ('partner_id', '=', customer_id),
                ('size_id', '=', size_id),
                ('field_name_id', '=', cm_fields_name['id']),
            ]
            cm_field_ids = cm_f_obj.search(cr, uid, cm_field_domain)
            if cm_field_ids:
                params = (self.pool.db.dbname, uid, psswd, cm_f_obj._name, 'write',
                          (cm_field_ids, {'value': value}, {'uid': uid, 'tz': 'America/Montreal'}), {})
            else:
                cm_f_vals = {
                    'product_id': product_id,
                    'size_id': (line[2]['size_id']) if cf_name == SIZE_TOLERANCE_ACTION else size_id,
                    'field_name_id': cm_fields_name['id'],
                    'value': value,
                }
                params = (self.pool.db.dbname, uid, psswd, cm_f_obj._name, 'create',
                          (cm_f_vals, {'uid': uid, 'tz': 'America/Montreal'}),{})
            ExportService.getService('object').dispatch('execute_kw', params)


product_product_line()


class product_cost_for_customer(osv.osv_memory):
    _name = "product.cost.for.customer"

    _columns = {
        'report_id': fields.many2one('product.template', 'Report', ),
        'customer_id': fields.many2one('res.partner', 'Customer ID', ),
        'customer_name': fields.char('Customer', size=256, ),
        'cost': fields.char('Cost', size=256, ),
        'answer': fields.char('Answer', size=256, ),
        'date_start': fields.date('Start Date', ),
        'date_end': fields.date('End Date', ),
        'rule_name': fields.char('Rule Name', size=64, ),
        'pcf_ids': fields.one2many('pricelist.calculated.fields', 'pcfc_id', 'Pricelist Calculated Fields', ),
        'sppv_id': fields.many2one('stock.product.pricelist.version', 'Stock Product Pricelist Version', ),
        'formula': fields.char('Formula', size=512, ),
        'formula_with_values': fields.char('Formula with Values', size=512, ),
        'original_profit': fields.char('Original Profit', size=2048, ),
        'current_profit': fields.char('Current Profit', size=2048, ),
        'brand_profit': fields.float(string='Brand Profit', ),
        'brand_name': fields.char(string='Brand Name', size=256, ),
        'brand_flat_fee': fields.float(string='Brand Flat Fee', ),
        'liquidation': fields.boolean(string='Liquidation', ),
    }

product_cost_for_customer()


class pricelist_calculated_fields(osv.osv_memory):
    _name = "pricelist.calculated.fields"

    _columns = {
        'pcfc_id': fields.many2one('product.cost.for.customer', 'Product Cost For Customer', ),
        'name': fields.char('Name', size=256, ),
        'raw_value': fields.char('Raw Value', size=512, ),
        'value': fields.char('Value', size=256, ),
    }

pricelist_calculated_fields()


class pricelist_value_step_by_step(osv.osv_memory):
    _name = 'pricelist.value.step.by.step'

    _columns = {
        'step_ids': fields.one2many('pricelist.value.step', 'parent_id', 'Pricelist Calculated Steps', ),
    }

pricelist_value_step_by_step()


class pricelist_value_step(osv.osv_memory):
    _name = 'pricelist.value.step'

    _columns = {
        'parent_id': fields.many2one('pricelist.value.step.by.step', 'Parent', ),
        'formula': fields.text('JSON Formula', ),
        'fields': fields.text('JSON Fields', ),
    }

pricelist_value_step()


class product_audit_logs(osv.osv_memory):
    _name = "product.audit.logs"

    _columns = {
        'product_id': fields.many2one('product.template', 'Product'),
        'transaction_id': fields.integer('transaction_id'),
        'field_ids': fields.one2many('product.audit.logs.fields', 'log_id',
                                     'Changed Fields'),
        'user_id': fields.many2one('res.users', 'User'),
        'action_date': fields.datetime("Date"),
        'method': fields.selection([
            ('create', 'Create'),
            ('write', 'Write'),
            ('delete', 'Delete'),
        ], 'Method', required=True),
        'changed_fields': fields.text('Changed Fields'),
    }

    _order = "transaction_id DESC"

product_audit_logs()


class product_audit_logs_fields(osv.osv_memory):
    _name = "product.audit.logs.fields"

    _columns = {
        'log_id': fields.many2one('product.audit.logs', 'Log Record'),
        'object_id': fields.many2one('ir.model', 'Object'),
        'field_name': fields.char('Field Name', size=512),
        'old_value': fields.text('Old Value'),
        'new_value': fields.text('New Value'),
    }

    _order = "object_id, field_name"

product_audit_logs_fields()
