{
    "name": "Delmar Pricing",
    "version": "1.0.2",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "delmar_products",
        "product",
        "multi_customer",
        "openerp_nan_product_pack",
    ],
    "description": """
    1. Inheritance 'product.pricelist.item' for new calculation formula
    """,
    "category": "Project Management",
    "init_xml": [
        'data/system_params.xml',
        'data/ir_cron.xml',
    ],
    "demo_xml": [],
    "update_xml": [
        'security/ir.model.access.csv',
        'view/pricelist_view.xml',
        'view/pricelist_version_view.xml',
        'view/product_view.xml',
        'view/duty_and_shipping_view.xml',
        'view/pricelist_log_view.xml',
        'view/po_calculation_parameters_view.xml',
        'view/brands_profit_view.xml',
        'view/pricelist_menu_view.xml',
        'view/spp_ram_view.xml',
        'view/product_pack_line_view.xml',
    ],
    'js': [],
    "application": True,
    "active": False,
    "installable": True,
}
