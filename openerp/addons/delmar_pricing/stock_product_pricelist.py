# -*- coding: utf-8 -*-
from osv import fields, osv
from stock_product_pricelist_log import LOG_TYPE_SELECTION
from tools.translate import _
from openerp.addons.pf_utils.utils.re_utils import f_d, date_range_string
from openerp.addons.pf_utils.utils.safe_eval import SAFE_LIST, BUILTIN_CONSTANTS_LIST
from datetime import datetime
import logging
from json import dumps as json_dumps

logger = logging.getLogger('delmar.pricing')

pricelist_fn = 'Customer Price Formula'


class FormulaException(Exception):
    def __init__(self, message):
        super(FormulaException, self).__init__()
        self.message = message


def check_formula(formula, skip_base_price=False):
    if (skip_base_price):
        formula = formula.replace('base_price', '1')
    import re
    matches = re.findall(r"[a-zA-Z_]+[0-9]*", formula)
    matches_strings = re.findall(r"'.+'", formula)
    matches_strings_correct = [a.replace("'", '') for a in matches_strings]
    correct_words = ['liquidation', 'brand_profit','brand_name']
    for correct_string in matches_strings_correct:
        correct_words.extend(correct_string.split(' '))
    matches = ["'{0}'".format(x) for x in matches if x not in SAFE_LIST + BUILTIN_CONSTANTS_LIST + ['from_field'] + matches_strings_correct + correct_words]

    if (matches):
        raise FormulaException("Field(s): {0} \nnot defined!".format(",".join(matches)))
    if (formula.strip()[-1:] in ['+', '-', '*', '/', '(', '^']):
        raise FormulaException(
            'formula is not completed\n'
            'correctly, excected fields\n'
            'or number or operand!'
        )
    return True


def _pricelist_type_get(self, cr, uid, context=None):
    pricelist_type_obj = self.pool.get('product.pricelist.type')
    pricelist_type_ids = pricelist_type_obj.search(
        cr, uid, [], order='name')
    pricelist_types = pricelist_type_obj.read(
        cr, uid, pricelist_type_ids, ['key', 'name'], context=context)
    res = []
    for pricelist in pricelist_types:
        res.append((pricelist['key'], pricelist['name']))
    return res


class stock_product_pricelist(osv.osv):
    _name = "stock.product.pricelist"

    _columns = {
        'name': fields.char('Name', size=256, required=True, ),
        'active': fields.boolean('Active', help="If the active field is set to False, it will allow you to hide the pricelist without removing it."),
        'type': fields.selection(_pricelist_type_get, 'Pricelist Type', required=True),
        'version_id': fields.one2many('stock.product.pricelist.version', 'pricelist_id', 'Pricelist Versions'),
        'partner_id': fields.many2one('res.partner', 'Partner', ),
        'firm_id': fields.many2one('res.partner.title', 'Partner Firm', ),
    }

    def edit_versions(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        spp_ram_obj = self.pool.get('spp.ram')

        new_context = {
            'flags': {
                'action_buttons': False,
                'deletable': False,
                'pager': False,
                'views_switcher': False,
                'can_be_discarded': True
            }
        }

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        if (ids):
            line = self.browse(cr, uid, ids)
            spp_ram_id = spp_ram_obj.create(
                cr, uid,
                {
                    'original_id': line.id,
                    'name': line.name,
                    'active': line.active,
                    'type': line.type,
                    'partner_id': line and line.partner_id and line.partner_id.id or False,
                    'firm_id': line and line.firm_id and line.firm_id.id or False,
                }
            )

        act_win_res_id = data_obj._get_id(
            cr, uid, 'delmar_pricing', 'action_spp_ram')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(
            cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': spp_ram_id,
            'context': new_context,
        })
        return act_win

    def check_active(self, cr, uid, ids, partner_id, firm_id, active, context=None):
        if (active):
            if (partner_id):
                another_active_ids = self.search(
                    cr, uid,
                    [
                        ('active', '=', True),
                        ('id', '!=', ids),
                        ('partner_id', '=', partner_id)
                    ]
                )
                if (another_active_ids):
                    raise osv.except_osv(
                        _('Warning!'),
                        _('''
                            Only one pricelist
                            can be active
                            for customer: {0},
                            now active:
                            {1}
                        '''.format(
                        self.pool.get('res.partner').browse(cr, uid, partner_id).name,
                        ','.join([x.name for x in self.browse(cr, uid, another_active_ids)])))
                    )
            if (firm_id):
                another_active_ids = self.search(
                    cr, uid,
                    [
                        ('active', '=', True),
                        ('id', '!=', ids),
                        ('firm_id', '=', firm_id)
                    ]
                )
                if (another_active_ids):
                    raise osv.except_osv(
                        _('Warning!'),
                        _('''
                            Only one pricelist
                            can be active
                            for firm: {0},
                            now active:
                            {1}
                        '''.format(
                        self.pool.get('res.partner.title').browse(cr, uid, firm_id).name,
                        ','.join([x.name for x in self.browse(cr, uid, another_active_ids)])))
                    )
        return True

    def check_pricelist_customer_field(self, cr, uid, partner_id, firm_id):
        result = None
        partner_obj = self.pool.get('res.partner')
        if (firm_id):
            if (not partner_id):
                partner_ids = partner_obj.search(cr, uid, [('title', '=', firm_id)])
                if (partner_ids):
                    results = []
                    for partner_id_ in partner_ids:
                        sub_res = self.check_pricelist_customer_field(cr, uid, partner_id_, False)
                        results.append(sub_res)
                    return results
                else:
                    raise osv.except_osv(
                        _('Error!'),
                        _('''
                            For firm: {firm_name}
                            Could not find partners
                        '''.format(
                        firm_name=self.pool.get('res.partner.title').browse(cr, uid, firm_id).name)
                        )
                    )
        try:
            pmcfn_obj = self.pool.get('product.multi.customer.fields.name')
            pmcn_obj = self.pool.get('product.multi.customer.names')
            partner = partner_obj.browse(cr, uid, partner_id)
            pfn_name = pricelist_fn.strip().lower().replace(' ', '_')
            pricelist_field_name_ids = pmcn_obj.search(cr, uid, [('name', '=', pfn_name)])
            if (pricelist_field_name_ids):
                pricelist_field_name_id = pricelist_field_name_ids[0]
                logger.info(
                    'product.multi.customer.names with name {0} now availaible'.format(
                        pfn_name
                    )
                )
            else:
                pricelist_field_name_id = pmcn_obj.create(
                    cr, uid,
                    {
                        'name': 'customer_price_formula',
                        'label': pricelist_fn
                    },
                    context=None
                )
                logger.info(
                    'product.multi.customer.names with name {0} now not availaible, created new: id - {1}'.format(
                        pfn_name,
                        pricelist_field_name_id
                    )
                )
            pricelist_field_name = pmcn_obj.browse(cr, uid, pricelist_field_name_id)
            pmcfn_ids = pmcfn_obj.search(
                cr, uid,
                [
                    ('name_id', '=', pricelist_field_name.id),
                    ('customer_id', '=', partner.id),
                    ('composite', '=', '@pricelist_value'),
                ]
            )
            pmcfn_id = None
            if (pmcfn_ids):
                pmcfn_id = pmcfn_ids[0]
                logger.info('Pricelist field for customer: {0} found'.format(partner.name))
            else:
                pmcfn_id = pmcfn_obj.create(
                    cr, uid,
                    {
                        'name_id': pricelist_field_name.id,
                        'customer_id': partner.id,
                        'composite': '@pricelist_value',
                        'label': pricelist_fn,
                        'export_label': pricelist_fn,
                    },
                    context=None
                )
                logger.info('Pricelist field for customer: {0} not found, created new: id - {1}'.format(
                    partner.name,
                    pmcfn_id
                ))
            if (pmcfn_id):
                pmcfn_obj.write(
                    cr, uid, pmcfn_id,
                    {
                        'dynamic_field': True,
                        'expand_by_customer': False,
                        'expand_ring_sizes': True,
                    },
                    context=None
                )
                result = True
        except Exception:
            result = False
        if (not result):
            raise osv.except_osv(
                _('Unknown Error!'),
                _('''
                    Sorry, auto create pricelist
                    customer field failed!
                    Please first go to
                    Tools -> Customer Fields
                    and create field with name
                    "{0}"
                    for partner {1}
                    with composite value
                    @pricelist_value
                    and true dynamic field
                    and true expand by ring sizes
                '''.format(
                pricelist_fn,
                partner and partner.name or ''))
            )
        return result

    def create(self, cr, uid, vals=None, context=None):
        vals = vals or {}
        context = context or None
        partner_id = vals and ('partner_id' in vals) and vals['partner_id'] or False
        firm_id = vals and ('firm_id' in vals) and vals['firm_id'] or False
        if ('active' in vals):
            self.check_active(cr, uid, -1, partner_id, firm_id, vals['active'])
        self.check_pricelist_customer_field(cr, uid, partner_id, firm_id)
        return super(stock_product_pricelist, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        row = self.browse(cr, uid, ids)
        partner_id = row and row.partner_id and row.partner_id.id or False
        firm_id = row and row.firm_id and row.firm_id.id or False
        if ('active' in vals):
            if ((not partner_id) and ('partner_id' in vals)):
                partner_id = vals['partner_id']
            if ((not firm_id) and ('firm_id' in vals)):
                firm_id = vals['firm_id']
            self.check_active(cr, uid, ids, partner_id, firm_id, vals['active'])
        self.check_pricelist_customer_field(cr, uid, partner_id, firm_id)
        return super(stock_product_pricelist, self).write(cr, uid, ids, vals, context=context)

stock_product_pricelist()


class spp_ram(osv.osv_memory):
    _name = 'spp.ram'

    def _get_versions(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for row in self.browse(cr, uid, ids, context=context):
            res[row.id] = [x.id for x in self.pool.get('stock.product.pricelist').browse(cr, uid, row.original_id).version_id if x]
        return res

    _columns = {
        'original_id': fields.integer('Original ID', ),
        'name': fields.char('Name', size=256),
        'active': fields.boolean('Active', ),
        'type': fields.selection(_pricelist_type_get, 'Pricelist Type', required=True),
        'partner_id': fields.many2one('res.partner', 'Partner', ),
        'firm_id': fields.many2one('res.partner.title', 'Partner Firm', ),
        'version_ids': fields.function(
            _get_versions,
            type='one2many',
            obj="stock.product.pricelist.version",
            method=True,
            string='Pricelist Versions'
        ),
    }

    def add_new_version(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or None

        original_id = self.browse(cr, uid, ids).original_id

        res_id = self.pool.get('stock.product.pricelist.version').create(
            cr, uid,
            {
                'pricelist_id': original_id,
            }
        )

        mod_obj = self.pool.get('ir.model.data')
        model_data_ids = mod_obj.search(
            cr, uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'stock_product_pricelist_version_form_view')
            ], context=None)
        resource_id = mod_obj.read(cr, uid, model_data_ids, fields=['res_id'], context=None)[0]['res_id']

        return {
            'name': _('Pricelist Version'),
            'res_id': res_id,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.product.pricelist.version',
            'view_id': False,
            'target': 'inline',
            'views': [(resource_id, 'form')],
            'context': "{'flags': {'action_buttons': True}}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
        }

    def back(self, cr, uid, ids, context=None):
        context = context or {}
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        act_win_res_id = data_obj._get_id(
            cr, uid, 'delmar_pricing', 'stock_product_pricelist_action')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(
            cr, uid, act_win_id['res_id'], [], context=context)
        act_win.update({
            'res_id': None,
        })
        return act_win

spp_ram()


class stock_product_pricelist_version(osv.osv):
    _name = "stock.product.pricelist.version"

    search_ids_sql = """SELECT
                            sppv.id
                        FROM
                            stock_product_pricelist_version AS sppv
                        LEFT JOIN
                            stock_product_pricelist AS spp
                                ON sppv.pricelist_id = spp.id
                        WHERE 1=1
                            AND spp.active = TRUE
                            AND (
                                    sppv.date_end::date >= '{date_now}'::date
                                    OR sppv.date_end is NULL
                                )
                            AND sppv.date_start::date <= '{date_now}'::date
    """
    search_and_partner_id_sql = '''            AND spp.partner_id = {partner_id}'''
    search_and_firm_id_sql = '''            AND spp.firm_id = {firm_id}'''

    def _get_active(self, cr, uid, ids, name, arg, context=None):
        fmt = "%Y-%m-%d"
        date_now = f_d(fmt)
        res = {}
        if (not ids):
            return res
        for row in self.browse(cr, uid, ids):
            date_start = row.date_start and datetime.strptime(row.date_start, fmt)
            date_end = row.date_end and datetime.strptime(row.date_end, fmt)
            if (
                (
                    (not date_end) and
                    date_start and
                    (date_start <= datetime.strptime(date_now, fmt))
                ) or
                (
                    date_start and
                    date_end and
                    date_now in date_range_string(date_start, date_end)
                )
            ):
                res[row.id] = True
            else:
                res[row.id] = False
        return res

    def _get_gsp_params(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if (not ids):
            return res
        sppigsp_obj = self.pool.get('stock.product.pricelist.item.gsp')
        conf_obj = self.pool.get('ir.config_parameter')
        ir_gsp_ids = conf_obj.search(cr, uid, [('key', 'ilike', "pricelist_parametr")])
        gsp_ids = []
        for gsp_row in conf_obj.browse(cr, uid, ir_gsp_ids):
            key = gsp_row.key.strip().lower().replace('pricelist_parametr.', '')
            value = gsp_row.value
            gsp_id = sppigsp_obj.create(cr, uid, {'key': key, 'value': value})
            gsp_ids.append(gsp_id)
        for id_ in ids:
            res[id_] = gsp_ids
        return res

    def _get_full_formula(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if (not ids):
            return res
        for row in self.browse(cr, uid, ids):
            res[row.id] = '( ( (base_price + ({sub_formula_1})) / (profit_param - brand_param - {sub_formula_2}) + (brand_flat_fee) + ({sub_formula_3}) ) * {currency_rate}) * (late_multipliers)'.format(
                sub_formula_1=row.sub_formula_1 or 0,
                sub_formula_2=row.sub_formula_2 or 0,
                sub_formula_3=row.sub_formula_3 or 0,
                currency_rate=row.currency_rate or 1.0,
            )
        return res

    def _get_currecy_rate(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        for row in self.browse(cr, uid, ids):
            res[row.id] = row.currency_id and row.currency_id.rate or 1.0
        return res

    # DLMR-2073
    # discount for CAD
    def _get_currency_discount_rate(self, cr, uid, ids, name, arg, context = None):
        res = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        for row in self.browse(cr, uid, ids):
            res[row.id] = row.currency_id and 'CAD' in row.currency_id.name and 0.9 or 1.0
        return res
    # END DLMR-2073

    _columns = {
        'pricelist_id': fields.many2one(
            'stock.product.pricelist', 'Price List',
            required=True, select=True, ondelete='cascade',
        ),
        'name': fields.char('Name', size=64, translate=True, unique=True, ),
        'active': fields.function(_get_active, type="boolean", string="Active", readonly=True, ),
        'date_start': fields.date('Start Date', help="Starting date for this pricelist version to be valid.", ),
        'date_end': fields.date('End Date', help="Ending date for this pricelist version to be valid."),
        'partner_id': fields.related(
            'pricelist_id', 'partner_id',
            type='many2one', relation='res.partner',
            string='Partner', readonly=True, ),
        'currency_id': fields.related(
            'pricelist_id', 'partner_id', 'currency_id',
            type='many2one', relation='res.currency',
            string='Currency', readonly=True, ),
        'currency_rate': fields.function(_get_currecy_rate, type="float", string="Currency Rate", readonly=True, ),
        # DLMR-2073
        'currency_discount_rate': fields.function(_get_currency_discount_rate, type="float", string="CDR", readonly=True, ),
        # END DLMR-2073
        'firm_id': fields.related(
            'pricelist_id', 'firm_id',
            type='many2one', relation='res.partner.title',
            string='Partner Firm', readonly=True, ),
        'fields': fields.one2many('stock.product.pricelist.item.field', 'pricelist_item_id', 'Fields', ),
        'gsp_ids': fields.function(
            _get_gsp_params,
            type='one2many',
            relation='stock.product.pricelist.item.gsp',
            string='Global System Parameters',
        ),
        'base_price': fields.selection(
            [
                ('lst_price', 'Sale Price'),
                ('standard_price', 'Cost Price'),
                ('calculated_standard_price', 'Calculated Cost Price')
            ], 'Base Price', ),
        # 'formula': fields.text('Formula', ),
        'full_formula': fields.function(
            _get_full_formula,
            type='char',
            string='Full Formula',
        ),
        'sub_formula_1': fields.char('Sub Formula 1', size=512, ),
        'sub_formula_2': fields.char('Sub Formula 2', size=512, ),
        'sub_formula_3': fields.char('Sub Formula 3', size=512, ),
        'overwrite': fields.boolean('Overwrite', ),
        'rounding_method': fields.selection(
            [
                ('none', 'None'),
                ('top_dollar', 'TOP $'),
                ('top_five', 'TOP 5/0'),
            ], 'Rounding Method', ),
        'ignore_liquidation': fields.boolean('Ignore liquidation'),
    }

    _defaults = {
        'rounding_method': 'none'
    }

    def edit(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        mod_obj = self.pool.get('ir.model.data')
        model_data_ids = mod_obj.search(
            cr, uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'stock_product_pricelist_version_form_view')
            ], context=None)
        resource_id = mod_obj.read(cr, uid, model_data_ids, fields=['res_id'], context=None)[0]['res_id']

        return {
            'name': _('Pricelist Version'),
            'res_id': ids,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.product.pricelist.version',
            'view_id': False,
            'target': 'inline',
            'views': [(resource_id, 'form')],
            'context': "{'flags': {'action_buttons': True}}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
        }

    def unlink(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        pricelist_id = self.browse(cr, uid, ids).pricelist_id.id
        super(stock_product_pricelist_version, self).unlink(cr, uid, ids, context=context)
        return self.pool.get('stock.product.pricelist').edit_versions(cr, uid, [pricelist_id])

    def back(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        row = self.browse(cr, uid, ids)
        try:
            pricelist_id = row.pricelist_id.id
        except AttributeError:
            pricelist_id = None
        if (
            pricelist_id and
            not row.name or
            not row.date_start or
            not row.base_price
        ):
            super(stock_product_pricelist_version, self).unlink(cr, uid, ids, context=context)
        if (pricelist_id):
            return self.pool.get('stock.product.pricelist').edit_versions(cr, uid, [pricelist_id])
        else:
            data_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')

            act_win_res_id = data_obj._get_id(
                cr, uid, 'delmar_pricing', 'stock_product_pricelist_action')
            act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
            act_win = act_obj.read(
                cr, uid, act_win_id['res_id'], [], context=context)
            act_win.update({
                'res_id': None,
            })
            return act_win

    def test_formula(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        row = self.browse(cr, uid, ids)
        sql = "SELECT id FROM product_product WHERE active = TRUE AND type = 'product' LIMIT 1"
        cr.execute(sql)
        product_id = cr.fetchone()[0]
        context = {
            'sppv_data': {
                'partner_id': row and row.pricelist_id
                and row.pricelist_id.partner_id and
                row.pricelist_id.partner_id.id or False,
                'firm_id': row and row.pricelist_id
                and row.pricelist_id.firm_id and
                row.pricelist_id.firm_id.id or False,
                'product_ids': [product_id],
            },
            'log_level': 'warning',
            'is_gui_call': True,
            'overwrite': False,
        }
        row.fill_prices_for_products(context=context)
        raise osv.except_osv(
            _('Info!'),
            _('Formula correct!')
        )

    def fill_global_system_fields(self, cr, uid, formula):
        conf_obj = self.pool.get('ir.config_parameter')
        gsp_ids = conf_obj.search(cr, uid, [('key', 'ilike', "pricelist_parametr")])
        if (gsp_ids):
            for gsp_id in gsp_ids:
                gsp_row = conf_obj.browse(cr, uid, gsp_id)
                key = gsp_row.key.strip().lower().replace('pricelist_parametr.', '')
                value = gsp_row.value
                if (key and value):
                    formula = formula.replace(key, "({0})".format(value))
        return formula

    def sppv_ids_with_partner_id(self, cr, uid, partner_ids=None):
        if partner_ids is not None:
            if not isinstance(partner_ids, (list, tuple, set)):
                partner_ids = [partner_ids]
        sql = '''SELECT
                    rs.id AS partner_id,
                    sppv.id AS sppv_id
                FROM
                    stock_product_pricelist_version AS sppv
                LEFT JOIN
                    stock_product_pricelist AS spp
                        ON sppv.pricelist_id = spp.id
                LEFT JOIN
                    res_partner_title AS rpt
                        ON spp.firm_id = rpt.id
                LEFT JOIN
                    res_partner AS rs
                        ON spp.partner_id = rs.id OR rs.title = rpt.id
                WHERE 1=1
                    AND spp.active = TRUE
                    AND (
                            sppv.date_end::date >= NOW()::date
                            OR sppv.date_end is NULL
                        )
                    AND sppv.date_start::date <= NOW()::date
        '''
        if partner_ids:
            sql += 'AND rs.id IN ({})'.format(','.join([str(x) for x in partner_ids]))
        cr.execute(sql)
        res = {}
        for data in cr.dictfetchall():
            partner_id, sppv_id = data['partner_id'], data['sppv_id']
            if partner_id in res:
                res[partner_id].append(sppv_id)
            else:
                res[partner_id] = [sppv_id]
        return res

    def fill_prices_for_products(self, cr, uid, ids, context=None):
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')
        result = {
            'partner_id': False,
            'firm_id': False,
            'answer': '',
            'customer_costs': {},
            'overwrite': None,
            'date_start': False,
            'date_end': False,
            'rule_name': False,
            'sppv_id': False,
            'profile_report': '',
        }
        if context is None:
            context = {}
        data = context.get('sppv_data', {})
        profile_it = context.get('profile_it', True)
        is_gui_call = context.get('is_gui_call', None)
        partner_id = data.get('partner_id', None)
        product_ids = data.get('product_ids', None)
        overwrite = data.get('overwrite', None)
        erp_export = data.get('erp_export', False)
        field_name_id = data.get('field_name_id', False)
        pricelists_values = pricelist_callbacks_obj.calculate_pricelists(
            cr, uid,
            {
                'profile_it': profile_it,
                'store_to_customer_fields': not is_gui_call,
                'overwrite': overwrite,
                'return_result': True,
                'fill_result_step_by_step': False,
                'fill_profits': False,
                'erp_export': erp_export,
                'field_name_id': field_name_id,
                'data': {partner_id: product_ids},
            }
        )
        customer_costs = pricelists_values.get(int(partner_id))
        result.update({
            'customer_costs': customer_costs,
            'overwrite': overwrite,
            'answer': 'calculated',
        })
        if not erp_export:
            result = json_dumps(result)
        logger.info(pricelists_values['profile_report'])
        return result

    def to_tog(self, cr, uid, ids, context=None):
        ids = ids and ids[0] if isinstance(ids, list) else ids or None
        context = context or {}
        self.log(cr, uid, ids, context)

    def log(self, cr, uid, ids, context=None):
        availaible_types_message = [x[0] for x in LOG_TYPE_SELECTION if x]
        log_obj = self.pool.get('stock.product.pricelist.log')
        logs = context.get('logs', [])
        log_level = availaible_types_message.index(context.get('log_level', 'debug'))
        is_gui_call = context.get('is_gui_call', False)
        if (logs):
            if (ids):
                row = self.browse(cr, uid, ids)
                for log in logs:
                    type_message = log.get('type', False)
                    message = log.get('message', False)
                    sppv_id = row.id
                    if (
                        type_message and
                        message and
                        sppv_id and
                        type_message in availaible_types_message and
                        availaible_types_message.index(type_message) >= log_level
                    ):
                        if (is_gui_call):
                            if (type_message != 'debug'):
                                raise osv.except_osv(
                                    _('Error!'),
                                    _(message)
                                )
                        else:
                            log_obj.create(
                                cr, uid,
                                {
                                    'type': type_message,
                                    'message': message,
                                    'sppv_id': sppv_id,
                                }
                            )
        return True

stock_product_pricelist_version()


class stock_product_pricelist_item_field(osv.osv):
    _name = "stock.product.pricelist.item.field"

    min_formula = '({result_if_true} if {value} {condition_operator} {condtition_value} else {result_if_false})'
    min_formula_str = 'IF {value} {condition_operator} {condtition_value} THEN {result_if_true}; ELSE: {result_if_false}'

    def _get_types(self, cr, uid, context=None):
        res = []
        if (context is None):
            context = {}
        main_types = [
            ('int', 'Integer'),
            ('float', 'Float'),
        ]
        res = main_types[:]
        if (('partner_id' in context) and context['partner_id']):
            res.extend([('field', 'Customer Field')])
        return res

    def _get_value_str(self, cr, uid, ids, field, arg, context=None):
        min_formula_str = self.min_formula_str
        res = {}
        if (not ids):
            return res
        for id_ in ids:
            ftr = ['name', 'value', 'condition_operator', 'condtition_value', 'result_if_true', 'result_if_false']
            readed = super(stock_product_pricelist_item_field, self).read(cr, uid, id_, ftr, context, load='_classic_read')
            _value = readed['value']
            nothing_values = [value for key, value in readed.iteritems() if value is False]
            if (not nothing_values):
                if (_value == 'from_field'):
                    _value = readed['name']
                res[id_] = min_formula_str.format(
                    value=_value,
                    condition_operator=readed['condition_operator'],
                    condtition_value=readed['condtition_value'],
                    result_if_true=readed['result_if_true'],
                    result_if_false=readed['result_if_false'],
                )
            else:
                res[id_] = unicode(_value)
        return res

    def _get_value_calculated_with_condition(self, cr, uid, ids, field, arg, context=None):
        min_formula = self.min_formula
        if (context is None):
            context = {}
        data = context.get('data', {})
        res = {}
        if (not ids):
            if (data):
                id_ = data.get('id', False)
                _value = data.get('value', False)
                condition_operator = data.get('condition_operator', False)
                condtition_value = data.get('condtition_value', False)
                result_if_true = data.get('result_if_true', False)
                result_if_false = data.get('result_if_false', False)
                if (
                    (not condition_operator) or
                    (not condtition_value) or
                    (not result_if_true) or
                    (not result_if_false)
                ):
                    pass
                else:
                    _value = min_formula.format(
                        value=_value,
                        condition_operator=condition_operator,
                        condtition_value=condtition_value,
                        result_if_true=result_if_true,
                        result_if_false=result_if_false,
                    )
                    try:
                        _value = eval(_value)
                    except Exception:
                        pass
                res[id_] = _value
            return res
        for row in self.browse(cr, uid, ids):
            _value = row.value
            if (
                (not row.condition_operator) or
                (not row.condtition_value) or
                (not row.result_if_true) or
                (not row.result_if_false)
            ):
                pass
            else:
                _value = min_formula.format(
                    value=_value,
                    condition_operator=row.condition_operator,
                    condtition_value=row.condtition_value,
                    result_if_true=row.result_if_true,
                    result_if_false=row.result_if_false,
                )
                try:
                    _value = eval(_value)
                except Exception:
                    pass
            res[row.id] = _value
        return res

    _columns = {
        'pricelist_item_id': fields.many2one('stock.product.pricelist.version', 'Price List Item', required=True, select=True, ondelete='cascade', ),
        'name': fields.char('Name', size=64, required=True, translate=True),
        'type': fields.selection(_get_types, 'Type', required=True, ),
        'field_name_id': fields.many2one('product.multi.customer.fields.name', 'Field Name',),
        'partner_id': fields.related(
            'pricelist_item_id', 'pricelist_id', 'partner_id',
            type='many2one', relation='res.partner',
            string='Partner', readonly=True, store=True),
        'value': fields.char('Value', size=64, required=True, ),
        'create_date': fields.datetime('Creation date', readonly=True, ),
        'create_uid': fields.many2one('res.users', 'User', readonly=True, ),
        'description': fields.text('Description', ),
        'condition_operator': fields.selection(
            [
                ('>', '>'),
                ('>=', '>='),
                ('<', '<'),
                ('<=', '<='),
                ('==', '=='),
            ], 'Condition operator', ),
        'condtition_value': fields.char('Condition Value', size=256, ),
        'result_if_true': fields.char('Result if True', size=256, ),
        'result_if_false': fields.char('Result if False', size=256, ),
        'value_calculated_with_condition': fields.function(
            _get_value_calculated_with_condition,
            type="char",
            string="Value Calculated With Condition",
            readonly=True,
        ),
        'value_str': fields.function(
            _get_value_str,
            type="char",
            string="Value",
            readonly=True,
        ),
    }

    _defaults = {
        'type': 'float',
    }

    def create(self, cr, uid, vals, context=None):
        field_name_id = vals.get('field_name_id')
        if vals.get('type') == 'field' and field_name_id:
            label = self.pool.get('product.multi.customer.fields.name').read(cr, uid, field_name_id, ['label']).get('label', None)
            label = label.strip().lower().replace(' ', '_')
            vals.update({'name': label})
            vals.update({'value': 'from_field'})
            if (('pricelist_item_id' in vals) and (vals['pricelist_item_id'])):
                pricelist_item = self.pool.get('stock.product.pricelist.version').browse(cr, uid, vals['pricelist_item_id'])
                partner_id = pricelist_item and pricelist_item.partner_id and pricelist_item.partner_id.id or None
                context.update({'partner_id': partner_id})
        return super(stock_product_pricelist_item_field, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        field_name_id = vals.get('field_name_id')
        if vals.get('type') == 'field' and field_name_id:
            label = self.pool.get('product.multi.customer.fields.name').read(cr, uid, field_name_id, ['label']).get('label', None)
            label = label.strip().lower().replace(' ', '_')
            vals.update({'name': label})
            vals.update({'value': 'from_field'})
            row = self.browse(cr, uid, ids)
            if (row):
                row = row and row[0]
                partner_id = row and row.partner_id and row.partner_id.id
                context.update({'partner_id': partner_id})
        return super(stock_product_pricelist_item_field, self).write(cr, uid, ids, vals, context=context)

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(stock_product_pricelist_item_field, self).default_get(cr, uid, fields, context=context)
        res.update({'partner_id': context.get('partner_id', False)})
        return res

    def onchange_field_name(self, cr, uid, ids, field_name_id, context=None):
        if field_name_id:
            label = self.pool.get('product.multi.customer.fields.name').read(cr, uid, field_name_id, ['label']).get('label', '')
        else:
            label = ''
        label = label.strip().lower().replace(' ', '_')
        return {'value': {'value': 'from_field', 'name': label}}

stock_product_pricelist_item_field()


class stock_product_pricelist_item_gsp(osv.osv_memory):
    _name = "stock.product.pricelist.item.gsp"

    _columns = {
        'key': fields.char('Key', size=256, readonly=True, ),
        'value': fields.char('Value', size=256, readonly=True, ),
    }

stock_product_pricelist_item_gsp()
