# -*- coding: utf-8 -*-
from osv import fields, osv


class product_pack_line(osv.osv):
    _inherit = 'product.pack.line'

    def _get_duty_and_shipping(self, cr, uid, product_id):
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        # DLMR-2073 - add country to check duty rule
        # empty by default
        product_origin_country = None
        if str(product.product_tmpl_id.prod_manufacture).lower() in ['china']:
            # set to China to get correct duty
            product_origin_country = 'China'
        # END DLMR-2073
        duty_and_shipping = self.pool.get('duty.and.shipping')
        res_ids = duty_and_shipping.search(
            cr, uid, [('map_type', '=', product.normalized_metal_stamp), ('origin_country', '=', product_origin_country)])
        return res_ids and duty_and_shipping.browse(cr, uid, res_ids[0]) or None

    def _get_total_weight(self, cr, uid, ids, *args, **kwargs):
        res = {}
        for pack in self.browse(cr, uid, ids):
            res[pack.id] = pack.weight * pack.quantity
        return res

    def _get_total_cost(self, cr, uid, ids, *args, **kwargs):

        res = {}
        if not ids:
            return res

        # FCDC Labor Factor
        labor_factor = float(self.pool.get('ir.config_parameter').get_param(
            cr, uid, 'product.labour.coefficient', 1)
        )

        for pack in self.browse(cr, uid, ids):
            ship_and_duty = 1
            if not pack.parent_product_id.exclude_ship_and_duty:
                ship_and_duty = (pack.default_duty or 1) * (pack.default_shipping or 1)

            res[pack.id] = pack.cost * pack.quantity * ship_and_duty
            if pack.product_id.categ_id.name == 'Labor':
                res[pack.id] *= labor_factor

        return res

    def _get_related_pack_lines(self, cr, uid, ids, context):
        return self.pool.get('product.pack.line').search(
            cr, uid, [('product_id', 'in', ids)])

    def _get_child_pack_lines(self, cr, uid, ids, context):
        return self.pool.get('product.pack.line').search(
            cr, uid, [('parent_product_id', 'in', ids)])

    _columns = {
        'default_duty': fields.float('Duty', digits=(16, 4), ),
        'default_shipping': fields.float('Shipping', digits=(16, 4), ),
        'weight': fields.float('Weight', digits=(16, 4), ),
        'total_weight': fields.function(
            _get_total_weight,
            type='float',
            string='Total Weight',
            readonly=True,
        ),
        'cost_uom': fields.float('Cost CT/PC', digits=(16, 2), ),
        'total_cost': fields.function(
            _get_total_cost,
            type='float',
            string='Total Cost (Cost * Quantity * (1 if exclude S&D else Duty * Shipping)',
            readonly=True,
            store={
                'product.pack.line': (
                    lambda self, cr, uid, ids, c={}: ids,
                    ['cost', 'quantity', 'default_duty', 'default_shipping'],
                    10
                ),
                'product.product': (_get_related_pack_lines, ['categ_id'], 20),
                'product.product': (_get_child_pack_lines, ['exclude_ship_and_duty'], 30),
            },
        ),
    }

    def create(self, cr, uid, vals, context=None):
        context.update({'parent_product_id': vals['parent_product_id']})
        return super(product_pack_line, self).create(cr, uid, vals, context=context)

    def onchange_cdsq(self, cr, uid, ids, cost, default_duty, default_shipping, quantity, weight, context=None):
        res = {
            'value': {
                'total_cost': cost * quantity * default_duty * default_shipping,
                'total_weight': quantity * weight
            }
        }
        return res

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(product_pack_line, self).default_get(cr, uid, fields, context=context)
        parent_product_id = context.get('parent_product_id', False)
        if parent_product_id:
            d_and_s_row = self._get_duty_and_shipping(cr, uid, parent_product_id)
            default_duty = d_and_s_row and d_and_s_row.duty or 1.0
            default_shipping = d_and_s_row and d_and_s_row.shipping or 1.0
            res.update({
                'default_duty': default_duty,
                'default_shipping': default_shipping
            })
        else:
            res.update({
                'default_duty': 1.0,
                'default_shipping': 1.0
            })
        return res


product_pack_line()
