# -*- coding: utf-8 -*-
import pooler
from openerp import SUPERUSER_ID
from pf_utils.res.constants import OUNCE

## DLMR-125
import logging
logger = logging.getLogger()

class FormulaException(Exception):
    pass


class FactoryCostFormula(object):

    def __init__(
        self
    ):
        super(FactoryCostFormula, self).__init__()

        self.pool = None

        self.server_obj = None
        self.server_id = None
        self.hash = {}
        self.calculation_parameters = {}
        self.cmp_types = [
            'metal',
            'diamond',
            'finding',
            'gemstone',
            'pearl',
            'lab',
            'packaging',
            'loss',
        ]
        self.cmp_metals = [
            'platinum',
            'silver',
            'gold',
        ]
        self.remap_types = {
            'lab': 'labor',
        }

    def init_params(self, cr):

        self.pool = pooler.get_pool(cr.dbname)
        self.calculation_parameters = self.pool.get('po.calculation.parameters')\
            .get_dict(cr, SUPERUSER_ID)

        self.server_obj = self.pool.get('fetchdb.server')
        self.server_id = self.server_obj.get_main_servers(cr, SUPERUSER_ID, single=True)
        if not self.server_id:
            raise Exception("MS SQL server not found")

    def getinvc(self, cr, product_id, invoice_count=1):
        return self.get(cr, product_id, invoice_count=invoice_count, no_margins=True)

    def get(self, cr, product_id, invoice_count=1, no_margins=False, po=None, invoice=None):
        """
        :except FormulaException

        :rtype: float
        """

        if not self.calculation_parameters:
            self.init_params(cr)

        try:
            invoice_count = int(invoice_count)
        except:
            invoice_count = 1

        if not product_id:
            raise FormulaException("Product ID - required parameter for computation of Factory cost")

        if not no_margins:
            cost = self.__get_hash_cost(product_id, invoice_count=invoice_count)
            if cost is not None:
                return cost

        cost = self.__compute(cr, product_id, invoice_count=invoice_count, no_margins=no_margins, po=po, invoice=invoice)
        if not no_margins:
            self.__set_hash_cost(cost, product_id, invoice_count=invoice_count)

        return cost

    def __compute(self, cr, product_id, invoice_count=1, no_margins=False, po=None, invoice=None):
        """
        :rtype: float
        """
        product = self.__get_product(cr, product_id)

        if product['cmps']:
            # debug
            # # logger.info('Product %s has some components, go recursive to computation them')
            cost = 0.0
            for cmp in product['cmps']:
                cost += cmp['qty'] * self.get(cr, cmp['id'], invoice_count=invoice_count, no_margins=no_margins)
            return cost

        # get latest invoices for product
        invoices = self.__get_invoices(cr, product['default_code'], invoice_count=invoice_count, po=po, invoice=invoice)
        if not invoices:
            return 0.0

        found_invoice_count = len(invoices)

        # check basic chain is in product if it's a Pendant
        # adds chain to invoice if it isn't exists, or sets price_pre if it is zero
        if product['categ_id'] == 22:
            # logger.info('Product %s is Pendant' % product['default_code'])
            # check is product must have basic chain
            basic_chain = self.__get_basic_chain_cost(cr, product['id'])
            if basic_chain is not None:
                # logger.info('Product %s must have basic chain' % product['default_code'])
                # iterate through invoices
                for i, invoice in enumerate(invoices):
                    # save this line for another checks
                    # chain_in_invoice = True in ['basic chain' in cmp['description'].lower() for cmp in invoice['details']]
                    chain_in_invoice = False
                    # iterate throught invoice details to find basic chain
                    for j, cmp in enumerate(invoice['details']):
                        # found line with basic chain
                        if 'basic chain' in cmp['description'].lower():
                            # logger.info('Invoice line for %s is basic chain line' % product['default_code'])
                            chain_in_invoice = True
                            if cmp['price_pre'] > 0:
                                continue
                            elif cmp['price'] > 0:
                                invoices[i]['details'][j]['price_pre'] = cmp['price']
                                continue
                            else:
                                invoices[i]['details'][j]['price'] = self.__setup_basic_chain_cost(product)
                                invoices[i]['details'][j]['price_pre'] = invoices[i]['details'][j]['price']
                                # logger.info('Set basic chain price for %s to %f' % (product['default_code'], invoices[i]['details'][j]['price']))
                                continue
                    # if basic chain line was not found in invoice details
                    # create virtual line with basic chain data
                    if not chain_in_invoice:
                        # logger.info('Basic chain for %s was not found in invoice' % product['default_code'])
                        chain = self.__create_basic_chain_defaults(product)
                        # logger.info('New chain price for %s is %f' % (product['default_code'], chain['price_pre']))
                        invoices[i]['details'].append(chain)

        # compute invoices costs and return final value
        # if no_margins is True:
            # logger.info('Calculating without margins for %s' % product['default_code'])
        # else:
            # logger.info('Calculating with margins for %s' % product['default_code'])

        costs = [self.__compute_invoice_cost(product, invoice, no_margins=no_margins) for invoice in invoices if invoice]

        result_cost = sum(costs) / found_invoice_count
        # logger.info('Cost for %s is %f' % (product['default_code'], result_cost))
        return result_cost

    def __get_hash_cost(self, product_id, invoice_count=1):
        """

        :rtype: float
        """
        return self.hash.get(invoice_count, {}).get(product_id, None)

    def __set_hash_cost(self, cost, product_id, invoice_count=1, ):
        if not self.hash.get(invoice_count):
            self.hash[invoice_count] = {}

        self.hash[invoice_count][product_id] = cost


    # BASIC CHAIN IN PRODUCT
    # Checks is product must have basic chain
    def __get_basic_chain_cost(self, cr, product_id):
        sql = """
        SELECT pp.default_code,pt.prod_sub_type,ppt.prod_metal,
          case
            when ppt.prod_metal='10k' then 12.0
            when ppt.prod_metal='14k' then 15.0
            when ppt.prod_metal='Silver' then 1.8
            else null
          end as add_price
        from product_product p
          LEFT JOIN product_pack_line ppl on ppl.parent_product_id=p.id
          LEFT JOIN product_product pp on pp.id=ppl.product_id
          LEFT JOIN product_template ppt on ppt.id=p.product_tmpl_id
          LEFT JOIN product_template pt on pt.id=pp.product_tmpl_id
          LEFT JOIN product_category pc on pc.id=ppt.categ_id
        where pc.id=22
            and ppt.prod_metal is not null
            and pp.default_code in ('HRP6R17KTY','HRP6R17KTW','HRP6R17KTP','HSING013KW17','HSING013K17','HSING013KP17','HCB30S18')
            and p.id='%s'
        """ % product_id

        cr.execute(sql)
        chain = cr.dictfetchone()

        if chain is None:
            return None

        if chain.get('add_price'):
            return chain
        else:
            return None

    def __get_product(self, cr, product_id):
        """
        :param: product_id int
        :rtype: dict
        """

        product_obj = self.pool.get('product.product')

        cr.execute("""
            SELECT pp.id,
                pp.default_code,
                pt.name,
                pt.categ_id,
                lower(pt.prod_metal) as prod_metal,
                pt.prod_metal_type,
                pt.prod_metal_stamp,
                case
                    when count(cc) = 0 then '[]'
                    else json_agg((select x from (select cc.cmp_product_id as id, cc.qty as qty)x))
                end as cmps
            FROM product_product pp
                left join product_template pt on pp.product_tmpl_id = pt.id
                left join product_set_components cc on pp.id = cc.parent_product_id
            where 1=1
                and pp.id = %(product_id)s
            group by pp.id, pt.id
        """, {'product_id': product_id})
        product = cr.dictfetchone()

        gold_stamp = None
        fields = [
            'prod_metal_stamp',
            'prod_metal',
            'prod_metal_type',
            'name',
        ]

        for field in fields:
            if product.get(field):
                gold_stamp = product_obj.get_gold_stamp(product[field])
                if gold_stamp:
                    break

        return {
            'id': product['id'],
            'categ_id': product['categ_id'],
            'cmps': eval(product['cmps']) if product['cmps'] else None,
            'default_code': product['default_code'],
            'gold_stamp': gold_stamp,
            'prod_metal': 'gold' if gold_stamp else product['prod_metal'],
        }

    def __get_invoices(self, cr, product_code, invoice_count=1, po=None, invoice=None):
        """
        :param: product_code string
        :param: invoice_count int
        :rtype: list of dict
        """

        # For catalog purposes we need to recalculate real cost only for last po and invoice.
        # So, narrowed search by two additional conditions: last po and invoice
        where_clause = ''
        params = {
            'style': product_code
        }
        if po and invoice:
            where_clause = ' AND po = %(po)s AND invoice = %(invoice)s'
            params.update({'po': po,
                           'invoice': invoice})

        sql = """
            SELECT TOP {limit}
                invoice,
                goldmarket,
                silvermarket,
                replace(po, 'PO', '') as po,
                qty,
                description,
                unit,
                amt,
                style
            FROM shipping
            WHERE style = %(style)s {where_clause}
            ORDER BY id DESC
        """.format(limit=invoice_count, where_clause=where_clause)

        res = self.server_obj.make_query(
            cr, SUPERUSER_ID, self.server_id,
            sql=sql, params=params,
            select=True, commit=False, return_dict=True
        )

        for invoice in res:
            invoice['details'] = self.__get_invoice_details(cr, invoice)

        return res

    def __get_invoice_details(self, cr, invoice):
        """
        :param: invoice dict
        :rtype: dict
        """

        sql = """
            SELECT
                qty,
                lower(description) as description,
                lower([type]) as [type],
                unit,
                wt,
                pc,
                price,
                price_pre,
                amt
            FROM shipping_details
            WHERE style = %(style)s
                and invoice = %(invoice)s
                and replace(po, 'PO', '') = %(po)s
            ORDER BY id DESC
        """

        params = {k: invoice[k] for k in ('style', 'invoice', 'po')}

        res = self.server_obj.make_query(
            cr, SUPERUSER_ID, self.server_id,
            sql=sql, params=params,
            select=True, commit=False, return_dict=True
        )

        return res

    def __compute_invoice_cost(self, product, invoice, no_margins=False):

        cost = 0.0
        product_obj = self.pool.get('product.product')

        # check basic chain is in product
        # returns chain tuple or None
        # basic_chain = self.__get_basic_chain_cost(cr, product['id'])

        for cmp in invoice['details']:

            if cmp['type'] not in self.cmp_types:
                continue

            if not no_margins and cmp['type'] == 'loss':
                continue

            gc = 0.0
            margin_key = cmp['type']

            if cmp['type'] == 'metal' and not no_margins:
                material = None
                stamp = None

                weight = cmp['unit'] or 0.0

                if 'silver' in cmp['description']:
                    material = 'silver'
                elif 'platinus' in cmp['description']:
                    material = 'platinum'

                if material is None:
                    tmp_stamp = product_obj.get_gold_stamp(cmp['description'])
                    if tmp_stamp:
                        stamp = tmp_stamp
                        material = 'gold'

                if material is None:
                    stamp = product['gold_stamp']
                    material = product['prod_metal']

                gm = self.calculation_parameters.get('global_%s' % material, 0.0)
                if not gm:
                    raise FormulaException("Not found Global Market value for %s" % material)

                margin_key = material

                if material == 'gold':
                    k = self.calculation_parameters.get('global_k_gold', 0.0)
                    cgm = self.calculation_parameters.get('gold_cgm_%s' % stamp, 0.0)
                    if not cgm:
                        raise FormulaException("Unknown cgm for %s gold stamp" % stamp)

                    gc = weight * cgm * k * gm

                else:
                    k = self.calculation_parameters.get('global_k', 0.0)
                    gc = gm / OUNCE * weight * k

            else:
                gc = cmp['price_pre']

            margin_key = self.remap_types.get(margin_key, margin_key)
            margin = self.calculation_parameters.get('margin_%s' % margin_key, 0.0)

            if not no_margins:
                # logger.info('Price for marginned line %s is %f' % (cmp['description'], gc * (1 + margin)))
                cost += gc * (1 + margin)
            else:
                # logger.info('Price for line %s is %f' % (cmp['description'], gc))
                cost += gc

        if not no_margins:
            cost = cost * (1 + self.calculation_parameters.get('margin_shipping', 0.0))

        return cost

    def __compute_invoice_cost_no_margins(self, product, invoice):
        """
        :param: product dict
        :param: invoice dict
        :rtype: float
        """
        # set starting cost to zero
        cost = 0.0

        # iterate through invoice details
        for cmp in invoice['details']:
            # skip unknown detail
            if cmp['type'] not in self.cmp_types:
                continue

            # logger.info('Price for line %s is %f' % (cmp['description'], cmp['price_pre']))
            # take a price per unit by default
            gc = cmp['price_pre']
            # sum
            cost += gc
        return cost

    def __create_basic_chain_defaults(self, product):
        chain = {
            'qty': 1,
            'description': '1 Basic chains',
            'type': 'finding',
            'unit': 1,
            'wt': 1,
            'pc': None,
            'price': 0.0,
            'price_pre': 0.0,
            'amt': 0
        }

        chain['price'] = self.__setup_basic_chain_cost(product)
        chain['price_pre'] = chain['price']
        chain['amt'] = chain['price_pre'] * chain['qty']

        return chain

    def __setup_basic_chain_cost(self, product):
        prod_material = product['gold_stamp']
        chain_basic_price = 0.0
        if prod_material == '10':
            chain_basic_price = 12.0
        elif prod_material == '14':
            chain_basic_price = 15.0
        elif prod_material == 'Silver':
            chain_basic_price = 1.8
        else:
            chain_basic_price = 0.0

        # logger.info('Set price for %s chain to %f' % (prod_material, chain_basic_price))
        return chain_basic_price
