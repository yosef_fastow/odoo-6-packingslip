# -*- coding: utf-8 -*-
from osv import fields, osv


class po_calculation_parameters(osv.osv):
    _name = 'po.calculation.parameters'

    _columns = {
        'create_uid': fields.many2one('res.users', 'Created by', readonly=True),
        'write_uid': fields.many2one('res.users', "Modify By", select=True),
        'name': fields.char('Name', size=256, required=True, ),
        'value': fields.float('Value', digits=(16, 4), required=True, ),
        'description': fields.char('Description', size=256, required=True, ),
    }

    def get_dict(self, cr, uid, root=None, context=None):

        params = {}
        where_condition = ""

        if root:
            where_condition = "and name like concat(%(root)s, '%')"
            params['root'] = root

        cr.execute("""
            SELECT lower(name) as name, value
            FROM po_calculation_parameters
            WHERE 1=1
                %s
            """ % where_condition,
            params
        )

        return {x['name']: x['value'] for x in cr.dictfetchall()}

po_calculation_parameters()
