# -*- coding: utf-8 -*-
from osv import fields, osv


LOG_TYPE_SELECTION = [
    ('debug', 'DEBUG'),
    ('warning', 'WARNING'),
    ('error', 'ERROR'),
    ('exception', 'EXCEPTION'),
]


class stock_product_pricelist_log(osv.osv):
    _name = "stock.product.pricelist.log"

    _columns = {
        'create_date': fields.datetime('Creation date', readonly=True, ),
        'create_uid': fields.many2one('res.users', 'User', readonly=True, ),
        'sppv_id': fields.many2one('stock.product.pricelist.version', 'Rule', readonly=True, ),
        'type': fields.selection(LOG_TYPE_SELECTION, 'Type', readonly=True, ),
        'message': fields.text('Message', readonly=True, ),
    }

    _defaults = {
        'type': 'debug'
    }

stock_product_pricelist_log()
