import re
import math
import urllib
from urllib2 import Request, urlopen, URLError, quote
import xml.etree.ElementTree as etree
import base64

ups_service_type = {
    '01': 'Next Day Air',
    'ups_1DA': 'Next Day Air',
    '02': 'Second Day Air',
    'ups_2DA': 'Second Day Air',
    '03': 'Ground',
    'ups_GND': 'Ground',
    '07': 'Worldwide Express',
    '08': 'Worldwide Expedited',
    '11': 'Standard',
    '12': 'Three-Day Select',
    'ups_3DS': 'Three-Day Select',
    '13': 'Next Day Air Saver',
    'ups_1DP': 'Next Day Air Saver',
    '14': 'Next Day Air Early AM',
    'ups_1DM': 'Next Day Air Early AM',
    '54': 'Worldwide Express Plus',
    '59': 'Second Day Air AM',
    '65': 'Saver',
}

canadapost_namespaces = {
    'rate': 'http://www.canadapost.ca/ws/ship/rate',
    'messages' : 'http://www.canadapost.ca/ws/messages',
    'shipment' : 'http://www.canadapost.ca/ws/shipment'
}

class Error(object):
    def __init__(self, message):
        self.message = message

    def __repr__(self):
        print '%s' % (self.message)
        raise

class Shipping(object):
    def __init__(self, weight, shipper,receipient):
        self.weight = weight
        self.shipper = shipper
        self.receipient = receipient

class UPSShipping(Shipping):
    def __init__(self, weight, shipper,receipient):
        super(UPSShipping, self).__init__(weight,shipper,receipient)

    def send(self,):
        datas = self._get_data()
        data = datas[0]
        api_url = datas[1]
        try:
            request = Request(api_url, data)
            response_text = urlopen(request).read()
            print "ups response_text: ",response_text
            response = self.__parse_response(response_text)
        except URLError, e:
            if hasattr(e, 'reason'):
                print 'Could not reach the server, reason: %s' % e.reason
            elif hasattr(e, 'code'):
                print 'Could not fulfill the request, code: %d' % e.code
            raise
        return response

    def __parse_response(self, response_text):
        root = etree.fromstring(response_text)
        status_code = root.findtext('Response/ResponseStatusCode')
        if status_code != '1':
            raise Exception('UPS: %s' % (root.findtext('Response/Error/ErrorDescription')))
        else:
            response = self._parse_response_body(root)
        return response

class UPSRateRequest(UPSShipping):

    def __init__(self, ups_info, pickup_type_ups, service_type_ups, packaging_type_ups, weight, shipper,receipient, cust_default, sys_default):
        self.type = 'UPS'
        self.ups_info = ups_info
        self.pickup_type_ups = pickup_type_ups
        self.service_type_ups = service_type_ups
        self.packaging_type_ups = packaging_type_ups
        self.sr_no = 1 if cust_default and cust_default.split('/')[0] == self.type and cust_default.split('/')[1] == self.service_type_ups else 9
        self.sr_no = 2 if self.sr_no == 9 and sys_default and sys_default.split('/')[0] == self.type and sys_default.split('/')[1] == self.service_type_ups else self.sr_no
        super(UPSRateRequest, self).__init__(weight,shipper,receipient)

    def _get_data(self):
        data = []
        data.append("""<?xml version=\"1.0\"?>
        <AccessRequest xml:lang=\"en-US\">
            <AccessLicenseNumber>%s</AccessLicenseNumber>
            <UserId>%s</UserId>
            <Password>%s</Password>
        </AccessRequest>
        <?xml version=\"1.0\"?>
        <RatingServiceSelectionRequest xml:lang=\"en-US\">
            <Request>
                <TransactionReference>
                    <CustomerContext>Rating and Service</CustomerContext>
                    <XpciVersion>1.0001</XpciVersion>
                </TransactionReference>
                <RequestAction>Rate</RequestAction>
                <RequestOption>Rate</RequestOption>
            </Request>
        <PickupType>
            <Code>%s</Code>
        </PickupType>
        <Shipment>
            <Shipper>
                <Address>
                    <PostalCode>%s</PostalCode>
                    <CountryCode>%s</CountryCode>
                </Address>
            <ShipperNumber>%s</ShipperNumber>
            </Shipper>
            <ShipTo>
                <Address>
                    <PostalCode>%s</PostalCode>
                    <CountryCode>%s</CountryCode>
                <ResidentialAddressIndicator/>
                </Address>
            </ShipTo>
            <ShipFrom>
                <Address>
                    <PostalCode>%s</PostalCode>
                    <CountryCode>%s</CountryCode>
                </Address>
            </ShipFrom>
            <Service>
                <Code>%s</Code>
            </Service>
            <Package>
                <PackagingType>
                    <Code>%s</Code>
                </PackagingType>
                <PackageWeight>
                    <UnitOfMeasurement>
                        <Code>LBS</Code>
                    </UnitOfMeasurement>
                    <Weight>%s</Weight>
                </PackageWeight>
            </Package>
        </Shipment>
        </RatingServiceSelectionRequest>""" % (self.ups_info.access_license_no,self.ups_info.user_id,self.ups_info.password,self.pickup_type_ups,self.shipper.zip,self.shipper.country_code,self.ups_info.shipper_no,self.receipient.zip,self.receipient.country_code,self.shipper.zip,self.shipper.country_code,self.service_type_ups,self.packaging_type_ups,self.weight))
        data.append('https://wwwcie.ups.com/ups.app/xml/Rate' if self.ups_info.test else 'https://onlinetools.ups.com/ups.app/xml/Rate')
        return data

    def _parse_response_body(self, root):
        return UPSRateResponse(root, self.weight, self.sr_no)


class UPSRateResponse(object):
    def __init__(self, root, weight, sr_no):
        self.root = root
        self.rate = root.findtext('RatedShipment/TotalCharges/MonetaryValue')
        self.service_type = ups_service_type[root.findtext('RatedShipment/Service/Code')]
        self.weight = weight
        self.sr_no = sr_no


    def __repr__(self):
        return (self.service_type, self.rate, self.weight, self.sr_no)


class UPSShipmentConfirmRequest(UPSShipping):
    def __init__(self, ups_info, pickup_type_ups, service_type_ups, packaging_type_ups, weight, shipper,receipient):
        self.type = 'UPS'
        self.ups_info = ups_info
        self.pickup_type_ups = pickup_type_ups
        self.service_type_ups = service_type_ups
        self.packaging_type_ups = packaging_type_ups
        super(UPSShipmentConfirmRequest, self).__init__(weight,shipper,receipient)

    def _get_data(self):
        data = []
        data.append("""
<?xml version="1.0" ?>
<AccessRequest xml:lang='en-US'>
    <AccessLicenseNumber>%s</AccessLicenseNumber>
    <UserId>%s</UserId>
    <Password>%s</Password>
</AccessRequest>
<?xml version="1.0" ?>
<ShipmentConfirmRequest>
    <Request>
         <TransactionReference>
              <CustomerContext>guidlikesubstance</CustomerContext>
              <XpciVersion>1.0001</XpciVersion>
         </TransactionReference>
         <RequestAction>ShipConfirm</RequestAction>
         <RequestOption>nonvalidate</RequestOption>
    </Request>
    <Shipment>
         <Shipper>
              <Name>%s</Name>
              <AttentionName>%s</AttentionName>
              <PhoneNumber>%s</PhoneNumber>
              <ShipperNumber>%s</ShipperNumber>
              <Address>
                   <AddressLine1>%s</AddressLine1>
                   <City>%s</City>
                   <StateProvinceCode>%s</StateProvinceCode>
                   <CountryCode>%s</CountryCode>
                   <PostalCode>%s</PostalCode>
              </Address>
         </Shipper>
         <ShipTo>
              <CompanyName>%s</CompanyName>
              <AttentionName>%s</AttentionName>
              <PhoneNumber>%s</PhoneNumber>
              <Address>
                   <AddressLine1>%s</AddressLine1>
                   <City>%s</City>
                   <StateProvinceCode>%s</StateProvinceCode>
                   <CountryCode>%s</CountryCode>
                   <PostalCode>%s</PostalCode>
                   <ResidentialAddress />
              </Address>
         </ShipTo>
         <PaymentInformation>
                <Prepaid>
                <BillShipper>
                     <CreditCard>
                         <Type>06</Type>
                         <Number>4111111111111111</Number>
                         <ExpirationDate>121999</ExpirationDate>
                     </CreditCard>
                </BillShipper>
            </Prepaid>
         </PaymentInformation>
         <Service>
              <Code>%s</Code>
              <Description>%s</Description>
         </Service>
        <Package>
            <PackagingType>
                <Code>%s</Code>
            </PackagingType>
            <PackageWeight>
                <Weight>%s</Weight>
            </PackageWeight>
        </Package>
    </Shipment>
    <LabelSpecification>
        <LabelPrintMethod>
            <Code>GIF</Code>
        </LabelPrintMethod>
        <LabelImageFormat>
            <Code>GIF</Code>
        </LabelImageFormat>
    </LabelSpecification>
</ShipmentConfirmRequest>""" % (self.ups_info.access_license_no,self.ups_info.user_id,self.ups_info.password, self.shipper.company_name, self.shipper.name, self.shipper.phone, self.ups_info.shipper_no, self.shipper.address1, self.shipper.city, self.shipper.state_code, self.shipper.country_code, self.shipper.zip, self.receipient.company_name, self.receipient.name, self.receipient.phone, self.receipient.address1, self.receipient.city, self.receipient.state_code, self.receipient.country_code, self.receipient.zip, self.service_type_ups, ups_service_type[self.service_type_ups], self.packaging_type_ups, self.weight))
        data.append('https://wwwcie.ups.com/ups.app/xml/ShipConfirm' if self.ups_info.test else 'https://onlinetools.ups.com/ups.app/xml/ShipConfirm')
        return data

    def _parse_response_body(self, root):
        return UPSShipmentConfirmResponse(root)


class UPSShipmentConfirmResponse(object):
    def __init__(self, root):
        self.root = root
        self.shipment_digest = root.findtext('ShipmentDigest')

    def __repr__(self):
        return (self.shipment_digest)

class UPSShipmentAcceptRequest(UPSShipping):
    def __init__(self, ups_info, shipment_digest):
        self.ups_info = ups_info
        self.shipment_digest = shipment_digest

    def _get_data(self):
        data = []
        data.append("""
<?xml version="1.0" ?>
<AccessRequest xml:lang='en-US'>
    <AccessLicenseNumber>%s</AccessLicenseNumber>
    <UserId>%s</UserId>
    <Password>%s</Password>
</AccessRequest>
<?xml version="1.0" ?>
<ShipmentAcceptRequest>
    <Request>
        <RequestAction>ShipAccept</RequestAction>
    </Request>
    <ShipmentDigest>%s</ShipmentDigest>
</ShipmentAcceptRequest>""" % (self.ups_info.access_license_no,self.ups_info.user_id,self.ups_info.password,self.shipment_digest))
        data.append('https://wwwcie.ups.com/ups.app/xml/ShipAccept' if self.ups_info.test else 'https://onlinetools.ups.com/ups.app/xml/ShipAccept')
        return data

    def _parse_response_body(self, root):
        return UPSShipmentAcceptResponse(root)

class UPSShipmentAcceptResponse(object):
    def __init__(self, root):
        self.root = root
        self.tracking_number = root.findtext('ShipmentResults/PackageResults/TrackingNumber')
        self.image_format = root.findtext('ShipmentResults/PackageResults/LabelImage/LabelImageFormat/Code')
        self.graphic_image = root.findtext('ShipmentResults/PackageResults/LabelImage/GraphicImage')
        self.html_image = root.findtext('ShipmentResults/PackageResults/LabelImage/HTMLImage')

    def __repr__(self):
        return (self.tracking_number, self.image_format, self.graphic_image)

class USPSShipping(Shipping):
    def __init__(self, weight, shipper,receipient):
        super(USPSShipping, self).__init__(weight,shipper,receipient)

    def send(self,):
        datas = self._get_data()
        data = datas[0]
        api_url = datas[1]
        values = {}
        values['XML'] = data
        api_url = api_url + urllib.urlencode(values)
	print "upss api_url: ",api_url
        try:
            request = urlopen(api_url)
            response_text = request.read()
            print "upss response_text: ",response_text
            response = self.__parse_response(response_text)
        except URLError, e:
            if hasattr(e, 'reason'):
                print 'Could not reach the server, reason: %s' % e.reason
            elif hasattr(e, 'code'):
                print 'Could not fulfill the request, code: %d' % e.code
            raise
        return response

    def __parse_response(self, response_text):
        root = etree.fromstring(response_text)
#        packages = root.getiterator("Package")
        error_tag = root.find('Package/Error')
        if error_tag:
            raise Exception('USPS %s' % (root.findtext('Package/Error/Description')))
        else:
            response = self._parse_response_body(root)
        return response

class USPSRateRequest(USPSShipping):
    def __init__(self, usps_info, service_type_usps, first_class_mail_type_usps, container_usps, size_usps, width_usps, length_usps, height_usps, girth_usps, weight, shipper, receipient, cust_default=False, sys_default=False):
        self.type = 'USPS'
        self.usps_info = usps_info
        self.service_type_usps = service_type_usps
        self.first_class_mail_type_usps = first_class_mail_type_usps
        self.container_usps = container_usps
        self.size_usps = size_usps
        self.width_usps = width_usps
        self.length_usps = length_usps
        self.height_usps = height_usps
        self.girth_usps = girth_usps
        self.sr_no = 1 if cust_default and cust_default.split('/')[0] == self.type and cust_default.split('/')[1] == self.service_type_usps else 9
        self.sr_no = 2 if self.sr_no == 9 and sys_default and sys_default.split('/')[0] == self.type and sys_default.split('/')[1] == self.service_type_usps else self.sr_no
        super(USPSRateRequest, self).__init__(weight,shipper,receipient)

    def _get_data(self):
        data = []

        service_type = '<Service>' + self.service_type_usps + '</Service>'

        if self.service_type_usps == 'First Class':
            service_type += '<FirstClassMailType>' + self.first_class_mail_type_usps + '</FirstClassMailType>'

        weight = math.modf(self.weight)
        pounds = int(weight[1])
        ounces = round(weight[0],2) * 16

        container = self.container_usps and '<Container>' + self.container_usps + '</Container>' or '<Container/>'

        size = '<Size>' + self.size_usps + '</Size>'
        if self.size_usps == 'LARGE':
            size += '<Width>' + self.width_usps + '</Width>'
            size += '<Length>' + self.length_usps + '</Length>'
            size += '<Height>' + self.height_usps + '</Height>'

            if stockpicking_lnk.container_usps == 'Non-Rectangular' or stockpicking_lnk.container_usps == 'Variable' or stockpicking_lnk.container_usps == '':
                size += '<Girth>' + self.girth_usps + '</Girth>'

#        data.append("""<?xml version=\"1.0\"?>
#        <RateV4Request USERID="%s">
#            <Revision/><Package ID="1ST">""" + service_type +
#                """<ZipOrigination>%s</ZipOrigination>
#                <ZipDestination>%s</ZipDestination>
#                <Pounds>%s</Pounds><Ounces>%s</Ounces>""" + container + size +
#                """<Machinable>true</Machinable></Package></RateV4Request>""" % (self.usps_info.user_id,self.shipper.zip,self.receipient.zip,str(pounds),str(ounces)))
        data.append('<?xml version="1.0" ?><RateV4Request USERID="' + self.usps_info.user_id + '"><Revision/><Package ID="1ST">' + service_type + '<ZipOrigination>' + self.shipper.zip + '</ZipOrigination><ZipDestination>' + self.receipient.zip + '</ZipDestination><Pounds>' + str(pounds) + '</Pounds><Ounces>' + str(ounces) + '</Ounces>' + container + size + '<Machinable>true</Machinable></Package></RateV4Request>')

        data.append("https://secure.shippingapis.com/ShippingAPITest.dll?API=RateV4&" if self.usps_info.test else "http://production.shippingapis.com/ShippingAPI.dll?API=RateV4&")
        return data

    def _parse_response_body(self, root):
        return USPSRateResponse(root, self.weight, self.sr_no)

class USPSRateResponse(object):
    def __init__(self, root, weight, sr_no):
        self.root = root
        self.rate = root.findtext('Package/Postage/Rate')
        self.service_type = root.findtext('Package/Postage/MailService').replace("&lt;sup&gt;&amp;reg;&lt;/sup&gt;","")
        self.weight = weight
        self.sr_no = sr_no


    def __repr__(self):
        return (self.service_type, self.rate, self.weight, self.sr_no)



#canada
class CanadaPostShipping(Shipping):
    def __init__(self, weight, shipper,receipient):
        super(CanadaPostShipping, self).__init__(weight,shipper,receipient)

    def send(self,):
        datas = self._get_data()
        request = ' '.join(datas[0].split())
        api_url = datas[1]
        headers = datas[2]

        print "canada post api_url: ",api_url
        print "canada request param: ",request
        try:
            '''
              Post the data and return the XML response
            '''
            conn = Request(url=api_url, data=request, headers = headers)

            f = urlopen(conn)
            response_text = f.read()
            print "canada post response_text: ",response_text
            response = self.__parse_response(response_text)
        except URLError, e:
            if hasattr(e, 'reason'):
                print 'Could not reach the server, reason: %s' % e.reason
            elif hasattr(e, 'code'):
                print 'Could not fulfill the request, code: %d' % e.code
            raise
        return response

    def __parse_response(self, response_text):
        root = etree.fromstring(response_text)

        error_tag = root.find('{http://www.canadapost.ca/ws/messages}message')
        if error_tag:
            nm = canadapost_namespaces['messages']
            erCode = root.findtext('{%s}message/{%s}code'.replace("%s", nm))
            erText = root.findtext('{%s}message/{%s}description'.replace("%s", nm), nm)
            raise Exception('CanadaPost error. ErCode: %s , ErText: %s' % (erCode, erText))
        else:
            response = self._parse_response_body(root)
        return response

class CanadaPostRateRequest(CanadaPostShipping):
    def __init__(self, canadapost_info, size_canadapost, width_canadapost, length_canadapost, height_canadapost, weight, shipper, receipient, cust_default, sys_default):
        self.type = 'CanadaPost'
        self.canadapost_info = canadapost_info
        self.size_canadapost = size_canadapost
        self.width_canadapost = width_canadapost
        self.length_canadapost = length_canadapost
        self.height_canadapost = height_canadapost

        self.sr_no = 1
        #self.sr_no = 2 if self.sr_no == 9 and sys_default and sys_default.split('/')[0] == self.type and sys_default.split('/')[1] == self.service_type_usps else self.sr_no

        super(CanadaPostRateRequest, self).__init__(weight,shipper,receipient)

    def _get_data(self):

        data = []
        if self.canadapost_info.test:
            url = "https://ct.soa-gw.canadapost.ca/rs/ship/price"
            key = self.canadapost_info.devKey.split(":")
        else:
            url = "https://soa-gw.canadapost.ca/rs/ship/price"
            key = self.canadapost_info.prodKey.split(":")

        if self.receipient.country_code == 'CA':
            destination = """<destination>
                                <domestic>
                                    <postal-code>%s</postal-code>
                                </domestic>
                            </destination>""" % (self.receipient.zip.upper()) #only accepted with uppercase
        elif self.receipient.country_code == 'US':
            destination = """<destination>
                                <united-states>
                                    <zip-code>%s</zip-code>
                                </united-states>
                            </destination>""" % (self.receipient.zip.upper())
        else:
            destination = """<destination>
                                <international>
                                    <country-code>%s</country-code>
                                </international>
                            </destination>""" % (self.receipient.country_code)
        size = ""
        if self.size_canadapost == "LARGE":
            size = """<dimensions>
                        <length>%s</length>
                        <width>%s</width>
                        <height>%s</height>
                      </dimensions>""" % (self.length_canadapost,
                                          self.width_canadapost,
                                          self.height_canadapost)
        data.append("""
            <?xml version="1.0" encoding="utf-8"?>
            <mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate">
                <customer-number>%s</customer-number>
                <parcel-characteristics>
                    <weight>%s</weight>
                    %s
                </parcel-characteristics>
                <origin-postal-code>%s</origin-postal-code>
                %s
            </mailing-scenario>""" % (self.canadapost_info.customerNumber,
                                      self.weight,
                                      size,
                                      self.shipper.zip,
                                      destination))

        data.append(url)

        base64string = base64.encodestring('%s:%s' % (key[0], key[1])).replace('\n', '')
        headers = {
            "Content-Type" : "application/vnd.cpc.ship.rate+xml",
            "Accept" : "application/vnd.cpc.ship.rate+xml",
            "Accept-Language" : "en-CA",
            "Authorization" : "Basic %s" % base64string
        }
        data.append(headers)
        return data

    def _parse_response_body(self, root):
        data = []
        nm = canadapost_namespaces['rate']
        for price_quote in root.findall('{%s}price-quote' % nm):
            data.append(CanadaPostRateResponse(price_quote, self.weight, self.sr_no))
        return data

class CanadaPostRateResponse(object):
    def __init__(self, root, weight, sr_no):
        nm = canadapost_namespaces['rate']
        self.root = root
        self.service_code = root.findtext("{%s}service-code" % nm)
        self.rate = root.findtext('{%s}price-details/{%s}due'.replace("%s", nm))
        self.service_type = root.findtext("{%s}service-name" % nm)
        self.deliveryDate = root.findtext('{%s}service-standard/{%s}expected-delivery-date')
        self.weight = weight
        self.sr_no = sr_no

    def __repr__(self):
        return (self.service_type, self.rate, self.weight, self.sr_no)

class CanadaPostConfirmRequest(CanadaPostShipping):
    def __init__(self, canadapost_info, shippingresp, weight, shipper,receipient):
        self.type = 'CanadaPost'
        self.service_code = shippingresp.code
        self.shipper = shipper
        self.receipient = receipient
        self.weight = weight
        self.canadapost_info = canadapost_info
        self.size_canadapost = shippingresp.picking_id.size_canadapost
        self.width_canadapost = shippingresp.picking_id.width_canadapost
        self.length_canadapost = shippingresp.picking_id.length_canadapost
        self.height_canadapost = shippingresp.picking_id.height_canadapost
        super(CanadaPostConfirmRequest, self).__init__(weight,shipper,receipient)

    def _get_data(self):
        group_id = ""
        data = []
        if self.canadapost_info.test:
            url = "https://ct.soa-gw.canadapost.ca/rs/%s/%s/shipment" % (self.canadapost_info.customerNumber, self.canadapost_info.customerNumber)
            key = self.canadapost_info.devKey.split(":")
        else:
            url = "https://soa-gw.canadapost.ca/rs/%s/%s/shipment" % (self.canadapost_info.customerNumber, self.canadapost_info.customerNumber)
            key = self.canadapost_info.prodKey.split(":")
        size = ""
        if self.size_canadapost == "LARGE":
            size = """<dimensions>
                        <length>%s</length>
                        <width>%s</width>
                        <height>%s</height>
                      </dimensions>""" % (self.length_canadapost,
                                          self.width_canadapost,
                                          self.height_canadapost)
        data.append("""
            <?xml version="1.0" encoding="UTF-8"?>
            <shipment xmlns="http://www.canadapost.ca/ws/shipment">
                <group-id>grp1</group-id>
                <requested-shipping-point>%s</requested-shipping-point>
                <delivery-spec>
                    <service-code>%s</service-code>
                    <sender>
                        <name>%s</name>
                        <company>%s</company>
                        <contact-phone>%s</contact-phone>
                        <address-details>
                            <address-line-1>%s</address-line-1>
                            <city>%s</city>
                            <prov-state>%s</prov-state>
                            <country-code>%s</country-code>
                            <postal-zip-code>%s</postal-zip-code>
                        </address-details>
                    </sender>
                    <destination>
                        <name>%s</name>
                        <company>%s</company>
                        <address-details>
                            <address-line-1>%s</address-line-1>
                            <city>%s</city>
                            <prov-state>%s</prov-state>
                            <country-code>%s</country-code>
                            <postal-zip-code>%s</postal-zip-code>
                        </address-details>
                    </destination>
                    <parcel-characteristics>
                        <weight>%s</weight>
                        %s
                    </parcel-characteristics>
                    <print-preferences>
                        <output-format>8.5x11</output-format>
                    </print-preferences>
                    <preferences>
                        <show-packing-instructions>true</show-packing-instructions>
                        <show-postage-rate>false</show-postage-rate>
                        <show-insured-value>true</show-insured-value>
                    </preferences>
                    <settlement-info>
                        <contract-id>%s</contract-id>
                        <intended-method-of-payment>Account</intended-method-of-payment>
                    </settlement-info>
                </delivery-spec>
            </shipment>""" % (  self.shipper.zip,
                                self.service_code,
                                self.shipper.name,
                                self.shipper.company_name,
                                self.shipper.phone,
                                self.shipper.address1,
                                self.shipper.city,
                                self.shipper.state_code,
                                self.shipper.country_code,
                                self.shipper.zip,
                                self.receipient.name,
                                self.receipient.company_name,
                                self.receipient.address1,
                                self.receipient.city,
                                self.receipient.state_code,
                                self.receipient.country_code,
                                self.receipient.zip,
                                self.weight,
                                size,
                                self.canadapost_info.customerNumber))
        data.append(url)

        base64string = base64.encodestring('%s:%s' % (key[0], key[1])).replace('\n', '')
        headers = {
            "Content-Type" : "application/vnd.cpc.shipment-v2+xml",
            "Accept" : "application/vnd.cpc.shipment-v2+xml",
            "Accept-Language" : "en-CA",
            "Authorization" : "Basic %s" % base64string
        }
        data.append(headers)

        return data

    def _parse_response_body(self, root):
        return CanadaPostConfirmResponse(root, self.canadapost_info)


class CanadaPostConfirmResponse(object):
    def __init__(self, root, canadapost_info):
        self.canadapost_info = canadapost_info
        nm = canadapost_namespaces['shipment']
        self.root = root
        self.shipment_id = root.findtext("{%s}shipment-id" % nm)
        self.shipment_status = root.findtext("{%s}shipment-status" % nm)
        self.tracking_pin = root.findtext("{%s}tracking-pin" % nm)
        links = root.findall("{%s}links/{%s}link".replace("%s", nm))
        for link in links:
            rel = link.attrib['rel']
            href = link.attrib['href']
            media_type = link.attrib['media-type']
            if (rel == "self"):
                self.link_self = {
                    "href": href,
                    "type" : media_type
                }
            elif (rel == "details"):
                self.details = {
                    "href": href,
                    "type" : media_type
                }
            elif (rel == "group"):
                self.group = {
                    "href": href,
                    "type" : media_type
                }
            elif (rel == "price"):
                self.price = {
                    "href": href,
                    "type" : media_type
                }
            elif (rel == "label"):
                self.label = {
                    "href": href,
                    "type" : media_type
                }
                loader = CanadaPostLoadLink(self.canadapost_info, self.label)
                self.graphic_image = loader.load()
                self.image_format = "PDF"

    def __repr__(self):
        return (self.shipment_id, self.shipment_status, self.tracking_pin)

class CanadaPostLoadLink(object):
    def __init__(self, canadapost_info, link):
        self.canadapost_info = canadapost_info
        self.href = link['href']
        self.type = link['type']

    def load(self):
        if self.canadapost_info.test:
            key = self.canadapost_info.devKey.split(":")
        else:
            key = self.canadapost_info.prodKey.split(":")

        base64string = base64.encodestring('%s:%s' % (key[0], key[1])).replace('\n', '')
        headers = {
            "Content-Type" : self.type,
            "Accept-Language" : "en-CA",
            "Authorization" : "Basic %s" % base64string
        }

        conn = Request(url=self.href, headers = headers)
        f = urlopen(conn)
        response_text = f.read()
        return response_text