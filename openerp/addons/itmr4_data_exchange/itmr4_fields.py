# -*- coding: utf-8 -*-
from osv import fields, osv


class itmr4_fields(osv.osv):
    _name = 'itmr4.fields'

    def _get_prod_type_size(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        product_tmpl_obj = self.pool.get('product.template')
        field_names = ['str_depth', 'str_length', 'str_height', 'str_width']
        for object in self.browse(cr, uid, ids):
            res[object.id] = ''
            fields = product_tmpl_obj.read(cr, uid, object.product_tmpl_id.id, field_names)
            fields_values = [fields[x] or ' ' for x in field_names]
            res[object.id] = '/'.join(fields_values)
        return res

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=True, ondelete='cascade', ),
        'product_tmpl_id': fields.related('product_id', 'product_tmpl_id', string='Product Template', readonly=True, type="many2one", relation="product.template", ),
        'part_code': fields.related('product_id', 'default_code', type='char', readonly=True, string='Partner Code', ),
        'desc1': fields.related('product_tmpl_id', 'name', type='char', readonly=True, string='Desc 1', ),
        'desc2': fields.related('product_tmpl_id', 'categ_id', type='char', readonly=True, string='Desc 2', ),
        'harm_code': fields.related('product_id', 'prod_tarif_classification', type='char', readonly=True, string='Harmonized Code', ),
        'coe': fields.related('product_tmpl_id', 'made_in', string='Country of Export', readonly=True, type='many2one', relation="res.country", ),
        'tt': fields.char('Tariff Treatment', size=3, ),
        'cust_ctry': fields.char('Customs Country', size=3, ),
        'annex': fields.char('Annex Code', size=3, ),
        'part_group': fields.char('Part Group', size=10, ),
        'ad_code': fields.char('AD Code', size=6, ),
        'ad_rate': fields.float('AD Rate', ),
        'ad_rate_type': fields.char('AD Rate Type', size=1, ),
        'ad_uom_code': fields.related('product_tmpl_id', 'uom_id', string='AD UOM Code', readonly=True, type='many2one', relation="product.uom", ),
        'part_type_code': fields.char('Part Type Code', size=3, ),
        'sls_uom_code': fields.char('Sls UOM Code', size=3, ),
        'exc_tax_ref_num': fields.char('Exc Tax Ref Num', size=10, ),
        'part_category': fields.char('Part Category', size=10, ),
        'ext_fibre_coverage': fields.boolean('Ext Fibre Coverage', ),
        'weight': fields.related('product_tmpl_id', 'weight', type='float', readonly=True, string='Weight', ),
        'weight_uom': fields.char('Weight UOM', size=3, ),
        'ip_comm_code': fields.char('IP Comm Code', size=15, ),
        'ip_cat_code': fields.char('IP Cat Code', size=5, ),
        'ip_agr_code': fields.char('IP Agr Code', size=10, ),
        'ip_from_us_or_mx': fields.char('IP from US/MX', size=1, ),
        'rulling': fields.char('Ruling#', size=20, ),
        'cert_org_code': fields.char('Cert Org Code', size=20, ),
        'cert_eff_date': fields.date('Cert Eff Date', ),
        'cert_exp_date': fields.date('Cert Exp Date', ),
        'def_proj_code': fields.char('Def Proj Code', size=20, ),
        'stocking_number': fields.char('Stocking Number', size=20, ),
        'coo_code': fields.related('product_tmpl_id', 'made_in', 'code', type="char", readonly=True, string='COO Code', ),
        'oic_rem_num': fields.char('OIC Remission Number', size=16, ),
        'part_class_code': fields.char('Part Class Code', size=10, ),
        'cert_type': fields.char('Cert Type', size=6, ),
        'part_id': fields.char('Part ID', size=7, ),
        'spec_handling': fields.char('Special Handling', size=128, ),
        'stocking_percent': fields.float('Stocking %', ),
        'last_used_date': fields.date('Last Used Date', ),
        'qty_in_pkg': fields.float('Quantity/Package', ),
        'tpl_possible': fields.boolean('TPL Possible', ),
        'part_note': fields.char('Part Note', size=120, ),
        'cert_sts': fields.related('product_tmpl_id', 'certified', type='char', readonly=True, string='Certification Status', ),
        'duty_rate': fields.related('product_tmpl_id', 'prod_duty_rate', type='float', readonly=True, string='Duty Rate', ),
        'duty_type': fields.char('Duty Type', size=1, ),
        'ogd_code': fields.char('OGD Code', size=10, ),
        'ogd_sub_type': fields.char('OGD Sub Type', size=20, ),
        'vfd_code': fields.char('VFD Code', size=3, ),
        'vendor_part_number': fields.char('Vendor Part#', size=20, ),
        'client_part_number': fields.char('Client Part#', size=20, ),
        'airs_req_id': fields.char('Airs Reg ID', size=20, ),
        'airs_req_ver': fields.char('Airs Req Version', size=4, ),
        'airs_code': fields.char('Airs Code', size=6, ),
        'end_use': fields.char('End Use', size=10, ),
        'airs_misc_id': fields.char('Airs Misc ID', size=3, ),
        'prod_model': fields.char('Product Model', size=25, ),
        'prod_type_size': fields.function(_get_prod_type_size, string='Product Type Size', type='char', method=True,),
        'reason_imp_code': fields.char('Reason Imp Code', size=10, ),
        'compl_lbl_code': fields.char('Compl Lbl Code', size=3, ),
        'compl_tire_code': fields.char('Compl Tire Code', size=3, ),
        'tiin_code': fields.char('Tiin Code', size=3, ),
        'ad_rate2': fields.float('AD Rate 2', ),
        'ad_rate2_type': fields.char('AD Rate2 Type', size=1, ),
        'ad_currency': fields.char('AD Currency', size=3, ),
        'hs_code_eff_date': fields.date('HS Code Eff Date', ),
        'hs_code_exp_date': fields.date('HS Code Exp Date', ),
        'annex_code_eff_date': fields.date('Annex Code Eff Date', ),
        'annex_code_exp_date': fields.date('Annex Code Exp Date', ),
        'qty_conv_uom_from': fields.char('Qty Conv UoM From', size=128, ),
        'qty_conv_uom_to': fields.char('Qty Conv UoM To', size=128, ),
        'qty_conv_uom_factor': fields.char('Qty Conv UoM Factor', size=128, ),
        'qty_conv_description': fields.char('Qty Conv Description', size=128, ),
        'product_make': fields.char('Product Make', size=128, ),
        'sent_date': fields.date('ITMR4 sent date', readonly=True, ),
    }

    _defaults = {
        'cust_ctry': 'CA',
    }

    _sql_constraints = [
        ('product_id_unique', 'unique(product_id)', 'Item should have only one data'),
    ]

    def crn_check_new_items(self, cr, uid, limit=None, context=None):
        return self._check_new_items(cr, uid, limit=limit, context=context)

    def _check_new_items(self, cr, uid, limit=None, context=None, ):

        limit_str = "LIMIT %s" % limit if limit and limit > 0 else ''

        sql = """
            SELECT
                pp.id as product_id
            FROM product_product pp
                LEFT JOIN product_template pt on pp.product_tmpl_id = pt.id
                LEFT JOIN itmr4_fields itmr4 on pp.id = itmr4.product_id
            WHERE pt.purchase_ok = true
                and itmr4.id is NULL
            %s
        """ % (limit_str)
        cr.execute(sql)
        data = cr.dictfetchall()
        for vals in data:
            self.create(cr, uid, vals)

        return True

itmr4_fields()
