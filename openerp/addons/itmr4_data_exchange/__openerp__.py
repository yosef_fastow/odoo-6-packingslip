{
    "name": "ITMR4 Data Exchange",
    "version": "0.0.1",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "delmar_products"
    ],
    "description": "",
    "category": "Project Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "view/itmr4_view.xml",
        "data/cron_data.xml",
    ],
    'js': [],
    "application": True,
    "active": False,
    "installable": True,
}
