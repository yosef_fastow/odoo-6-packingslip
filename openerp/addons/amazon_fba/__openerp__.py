{
    'name': 'Amazon FBA',
    'version': '1.0',
    'complexity': "easy",
    'description': """
This module allows your user to easily and efficiently participate in enterprise innovation.
============================================================================================

It allows everybody to express ideas about different subjects.
Then, other users can comment on these ideas and vote for particular ideas.
Each idea has a score based on the different votes.
The managers can obtain an easy view of best ideas from all the users.
Once installed, check the menu 'Ideas' in the 'Tools' main menu.""",
    'author': 'OpenERP SA',
    'website': 'http://openerp.com',
    'depends': [
        'base_tools',
        'tagging',
        'sale_integration'
    ],
    'init_xml': [],
    'update_xml': [
        'stock_fba_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
