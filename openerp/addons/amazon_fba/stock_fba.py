from osv import osv
from osv import fields
from tools.translate import _
from datetime import datetime, timedelta
import time
from mws import mws
import json
from collections import defaultdict

class fba_inventory(osv.osv):
    _name = "fba.inventory"

    host = "https://mws.amazonservices.com"
    aws_access_key_id = "AKIAI26SJL27PRN7B6OQ"
    secret_key = "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"
    merchant_id = "A214PRX0SXKCGE"
    marketplace_id = "ATVPDKIKX0DER"

    _columns = {
        'name': fields.char('Name', size=64, required=True),
        'date': fields.datetime('Inventory after date', help="After date", required=True, select=True),
        'user_check': fields.boolean('User date'),
        'url': fields.char('Url', size=128),
        'customer': fields.many2one('res.partner', 'Customer'),
        'tag': fields.many2one('tagging.tags', 'Tag'),
        'tag_fba': fields.many2one('tagging.tags', 'Tag FBA'),
        'send_feed': fields.boolean('Send Feed'),
        'switch_on': fields.boolean('Switch Products'),
        'aws_access_key_id': fields.char('AWS Access Key ID', size=128),
        'merchant_id': fields.char('Merchant ID:', size=128),
        'marketplace_id': fields.char('Marketplace ID', size=128),
        'secret_key': fields.char('Secret Key', size=128),
        'history_ids': fields.one2many('fba.history.inventory', 'id_note', 'History Inventory'),
        }

    def create(self, cr, uid, vals, context=None):
        """
        Create a new record for a model fba_inventory
        @param cr: A database cursor
        @param user: ID of the user currently logged in
        @param vals: provides data for new record
        @param context: context arguments, like lang, time zone

        @return: Returns an id of the new record
        """
        res_id = super(fba_inventory,self).create(cr, uid, vals, context=context)
        return res_id

    def get_inventory(self, cr, uid, name="", context=None):
        missing_products = list()
        settings = self.browse(cr, uid, name)[0]
        token_param = 'amazon.fba.get.inventory.' + settings.name.lower()
        tokeninv_system_param = self.pool.get('ir.config_parameter')
        tokeninv_system_param_get = tokeninv_system_param.get_param(cr, uid, token_param)

        if (settings.name.lower() == "amazon_dsfba"):
            loc_search = '% / FBA / Stock / DS'
        elif (settings.name.lower() == "amazon_ukfba"):
            loc_search = '% / FBA / Stock / UK'
        elif (settings.name.lower() == "amazon_difba"):
            loc_search = '% / FBA / Stock / DI'
        elif (settings.name.lower() == "amazon_jpfba"):
            loc_search = '% / FBA / Stock / JP'
        elif (settings.name.lower() == "commerce"):
            loc_search = '% / FBA / Stock / DEL'

        inventoryfba = mws.Inventory(str(settings.aws_access_key_id), str(settings.secret_key), str(settings.merchant_id), str(settings.url))
        skulist_system_param = self.pool.get('ir.config_parameter')
        sku_string = skulist_system_param.get_param(cr, uid, 'amazon_dsfba.sku_list')
        if (settings.user_check == True):
            fba_inventory = settings
            date = fba_inventory.date.replace(" ", "T")
        else:
            date = (datetime.now() - timedelta(hours=10)).isoformat()
        if (sku_string != ' '):
            sku_list = sku_string.split(',')
            response = inventoryfba.list_inventory_supply(skus=tuple(sku_list))
        else:
            if (tokeninv_system_param_get == "new"):
                response = inventoryfba.list_inventory_supply(datetime=date)
            elif (tokeninv_system_param_get == "False"):
                response = inventoryfba.list_inventory_supply(datetime=date)
            else:
                response = inventoryfba.list_inventory_supply_by_next_token(str(tokeninv_system_param_get))
        count = 0
        result = []
        # raise osv.except_osv(_('Inventory'),_(str(response.parsed)))
        # stock_picking[0].note = str(response.parsed)
        # self.pool.get('stock.picking.fba').write(cr, uid, 1, {'note': str(response.parsed)})
        #parsed = json.loads(str(response.parsed))
        log_inventory = []
        if (response.parsed.get('InventorySupplyList').get('member',False)):
            response_array = response.parsed['InventorySupplyList']['member']
            product_data = self.pool.get('product.product')
            ring_size_data = self.pool.get('ring.size')
            item_add_to_tag_list = list()
            item_null_qty_list = defaultdict(list)
            item_remove_from_tag_list = list()
            item_positive_qty_list = defaultdict(list)
            list_all_products = list()
            products = defaultdict(list)
            list_all_products_2 = list()
            item_null_list = list()
            response_string_flag = False
            for item in response_array:
                if (isinstance(item, str) and not response_string_flag):
                    item = response_array
                    response_string_flag = True
                elif (isinstance(item, str) and response_string_flag):
                    break
                if not (item['SellerSKU'] and item['SellerSKU']['value']):
                    continue

                item_qty = item['InStockSupplyQuantity']['value']
                sku_parts = item['SellerSKU']['value'].split('/')
                size_name = sku_parts[-1:]
                seller_sku = '/'.join(sku_parts[:-1])
                if (item['ASIN']['value']):
                    asin =  item['ASIN']['value']
                else:
                    asin = ""
                size_id = False
                size = 'False'
                try:
                    size_name = '%.1f' % (float(size_name[0]))
                except Exception:
                    size_name = False
                    seller_sku = item['SellerSKU']['value']

                size_ids = ring_size_data.search(cr, uid, [('name','=',size_name)])
                if size_ids:
                    size_id = size_ids[0]
                    size = size_ids[0]

                product, size_customer_field = self.pool.get('product.product').search_product_by_id(cr, uid, settings.customer.id, seller_sku.upper())

                if product:
                    prod_id = product.id
                    products[prod_id].append(size_id)
                else:
                    product_not = seller_sku + ' not found' + '\n'
                    print 'Product ' + seller_sku + ' was not found'
                    missing_products.append(product_not)
                    continue


                location_obj = self.pool.get('stock.location')
                loc_ids = location_obj.search(cr, uid, [('complete_name', 'ilike', loc_search)])
                if loc_ids :
                    location_id = loc_ids[0]
                else :
                    return False

                sql = "select id from stock_move order by id desc limit 1"
                cr.execute(sql)
                res_prev = cr.fetchone()
                id_prod_prev = res_prev[0]

                self.pool.get('sale.order').sync_product_stock_simple(cr, uid,prod_id,size_id,item_qty,location_id,context=context )

                sql = "select id, product_id from stock_move order by id desc limit 1"
                cr.execute(sql)
                res_new = cr.fetchone()
                id_prod_new = res_new[0]

                if (not (id_prod_new == id_prod_prev)):
                    sql = "select default_code, name_template from product_product where id = %s" % int(res_new[1])
                    cr.execute(sql)
                    result_sql = cr.fetchone()
                    result_sql_1 = u"{0}".format(result_sql[1])
                    product_move = str(result_sql[0]) + " " + result_sql_1

                    result.append(product_move)
                print item_qty

                list_all_products.append(item['SellerSKU']['value'] + ' = ' + item_qty)

                # Check Amazon FBA Inventory, if qty=0 FBA->ADS tag
                if (settings.switch_on and (int(item_qty) == 0)):
                    item_null_qty_list[prod_id].append(size)
                    item_null_list.append(item['SellerSKU']['value'])

                # Check Amazon FBA Inventory, if qty!=0 ADS->FBA tag
                elif (settings.switch_on and (int(item_qty) != 0)):
                    item_positive_qty_list[prod_id].append(size)

            item_to_mfn = list()
            item_to_afn = list()
            item_to_mfn_update = []
            item_to_mfn_create = []
            item_to_afn_update = []
            item_to_afn_create = []
            item_to_afn_fields = []
            if (len(item_null_qty_list) > 0):
                item_null_qty_list_tuple = self.list_in_tuple(item_null_qty_list.keys())
                (item_to_mfn_update, item_to_mfn_create, item_to_mfn_fields) = self.check_field(cr, uid, settings, item_null_qty_list, False)
                item_to_mfn = self.check_product(cr, uid, settings, item_null_qty_list_tuple, False)
            if (len(item_positive_qty_list) > 0):
                item_positive_qty_tuple = self.list_in_tuple(item_positive_qty_list.keys())
                (item_to_afn_update, item_to_afn_create, item_to_afn_fields) = self.check_field(cr, uid, settings, item_positive_qty_list, True)
                item_to_afn = self.check_product(cr, uid, settings, item_positive_qty_tuple, True)

            if (len(list_all_products) > 0):
                all_items = ',\n'.join(list_all_products)
                log_inventory.append({
                    'title': 'Inventory got for ' + str(len(list_all_products)) + ' products',
                    'msg': all_items,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            all_items_response = json.dumps(response.parsed, indent=2)
            log_inventory.append({
                'title': 'Inventory response',
                'msg': all_items_response,
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            msg_update_mfn = ''
            msg_create_mfn = ''
            if (item_to_mfn_update and len(item_to_mfn_update)>0):
                item_to_mfn_update_string = ',\n'.join(self.get_products(cr,uid, item_to_mfn_update))
                msg_update_mfn = "Updated MFN products:\n" + item_to_mfn_update_string + '\n'
            if (item_to_mfn_create and len(item_to_mfn_create)>0):
                item_to_mfn_create_string = ',\n'.join(self.get_products(cr,uid, item_to_mfn_create))
                msg_create_mfn = "Created MFN products:\n" + item_to_mfn_create_string + '\n'
            if (settings.switch_on):
                log_inventory.append({
                        'title': 'MFN: ' + str(len(item_to_mfn_update)) + ' items updated and ' + str(len(item_to_mfn_create)) + ' item created',
                        'msg': msg_update_mfn + msg_create_mfn,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

            msg_update_afn = ''
            msg_create_afn = ''
            if (item_to_afn_update and len(item_to_afn_update)>0):
                item_to_afn_update_string = ',\n'.join(self.get_products(cr,uid, item_to_afn_update))
                msg_update_afn = "Updated AFN products:\n" + item_to_afn_update_string + '\n'
            if (item_to_afn_create and len(item_to_afn_create)>0):
                item_to_afn_create_string = ',\n'.join(self.get_products(cr,uid, item_to_afn_create))
                msg_create_afn = "Created AFN products:\n" + item_to_afn_create_string + '\n'
            if (settings.switch_on):
                log_inventory.append({
                        'title': 'AFN: ' + str(len(item_to_afn_update)) + ' items updated and ' + str(len(item_to_afn_create)) + ' item created',
                        'msg': msg_update_afn + msg_create_afn,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

            msg_product_tag = ''
            if (len(item_to_mfn) > 0):
                item_to_tag_string = ',\n'.join(item_to_mfn)
                msg_product_tag = str(len(item_to_mfn)) + " products are added to " + str(settings.tag.name) + " tag:\n"+item_to_tag_string+"\n"

            if (settings.switch_on and len(item_null_list) > 0):
                item_null = ',\n'.join(item_null_list)
                msg_null_qty = str(len(item_null_list)) + " products have qty=0:\n" + item_null + "\n"
                log_inventory.append({
                    'title': str(len(item_null_list)) + ' items that have qty=0 and ' + str(len(item_to_mfn)) + ' item added to tag',
                    'msg': msg_null_qty + msg_product_tag,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

            if (settings.switch_on and len(item_to_afn) > 0):
                item_remove_from_tag = ',\n'.join(item_to_afn)
                msg_product_remove_from_tag = ''
                log_inventory.append({
                    'title': str(len(item_to_afn)) + ' items added to ' + str(settings.tag_fba.name) + ' tag',
                    'msg': item_remove_from_tag+"\n",
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

            response_token = response.parsed.get('NextToken', {}).get('value', False)
            if (settings.switch_on and (item_null_qty_list.keys() or item_to_afn_fields)):
                customer_name = settings.name.split('FBA')[0]
                self.pool.get('sale.integration').integrationApiSwitchProduct(cr, uid, name=customer_name, tag_fba=settings.tag_fba.id, product_ids_null=item_null_qty_list, product_ids_positive=item_to_afn_fields, send_feed=settings.send_feed)

        else:
            log_inventory.append({
                'title': 'ERROR Can\'t find InventorySupplyList members',
                'msg': '',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            response_token = False

        tokeninv_system_param = self.pool.get('ir.config_parameter')
        tokeninv_system_param_set = tokeninv_system_param.set_param(cr, uid, token_param, str(response_token))
        count = count+1

        result_list_products = ""
        if (result == []):
            result_list_products = "Empty"
        else:
            for product in result:
                product_replace = product.replace("'", "''")
                result_list_products += product_replace + ";\n"
        count_updated_products = len(result)

        missing_product_log = ""
        if (missing_products == []):
            missing_product_log = ""
        else:
            for product in missing_products:
                product_replace = product.replace("'", "''")
                missing_product_log += product_replace
        if (missing_product_log != ""):
            log_inventory.append({
                    'title': str(len(missing_products)) + ' products not found',
                    'msg': missing_product_log,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        log_inventory.append({
                'title': 'Inventory updated for '+ str(count_updated_products) +' products',
                'msg': result_list_products,
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })
        if(len(log_inventory) > 0):
            self.log(cr, uid, settings, log_inventory)
        return True

    def check_field(self, cr, uid, settings, product_sku, item_qty):
        if (item_qty):
            fullfilment = 'AFN'
            tag = settings.tag_fba.id
        else:
            fullfilment = 'MFN'
            tag = settings.tag.id

        field_update_list = []
        field_create_list = []
        field_list = []
        field_name_id = self.pool.get("product.multi.customer.fields.name").search(cr, uid, [('label', '=', 'Fullfilment Notes'), ('customer_id', '=', settings.customer.id)])[0]
        field_switch_allow_name_id = self.pool.get("product.multi.customer.fields.name").search(cr, uid, [('label', '=', 'Switch Allow'), ('customer_id', '=', settings.customer.id)])[0]
        for prod in product_sku.keys():
            for size_id in product_sku[prod]:
                if size_id == 'False': size_id = False
                field_id = self.pool.get("product.multi.customer.fields").search(cr, uid, [('product_id', '=', prod), ('size_id', '=', size_id), ('partner_id', '=', settings.customer.id), ('field_name_id', '=', field_name_id)])
                field_switch_allow_id = self.pool.get("product.multi.customer.fields").search(cr, uid, [('product_id', '=', prod), ('size_id', '=', size_id), ('partner_id', '=', settings.customer.id), ('field_name_id', '=', field_switch_allow_name_id)])
                size = size_id or None
                field_value = self.pool.get("product.multi.customer.fields").browse(cr, uid, field_id)
                field_switch_allow_value = self.pool.get("product.multi.customer.fields").browse(cr, uid, field_switch_allow_id)

                if (field_value and field_value[0].value == fullfilment and fullfilment == 'AFN'):
                    continue
                if (field_value and field_value[0].value != fullfilment):
                    if (field_switch_allow_value and field_switch_allow_value[0].value == '1'):
                        field_update = self.pool.get("product.multi.customer.fields").write(cr, uid, field_id, {"value": fullfilment})
                        field_update_list.append(field_id[0])
                        field = field_value[0]
                        field_list.append(field)
                        print 'update' + str(field_id[0])
                elif (not field_value):
                    field = {
                        'partner_id' : settings.customer.id,
                        'product_id' : prod,
                        'size_id' : size,
                        'field_name_id' : field_name_id,
                        'value' : fullfilment
                    }
                    field_create = self.pool.get("product.multi.customer.fields").create(cr, uid, field)
                    field_create_list.append(field_create)
                    field_browse = self.pool.get("product.multi.customer.fields").browse(cr, uid, [field_create])[0]
                    field_list.append(field_browse)
                    print 'create' + str(field_create)
        cr.commit()
        return field_update_list, field_create_list, field_list

    def get_products(self, cr, uid, products):
        result_list = []
        if (len(products)>0):
            products_string = ','.join([str(i) for i in products])
            sql = """select pp.default_code, rs.name, pmcf.value
            from product_multi_customer_fields as pmcf
            left join product_product as pp on pmcf.product_id = pp.id
            left join product_ring_size_default_rel as prs on (pp.product_tmpl_id = prs.product_id and pmcf.size_id = prs.size_id)
            left join ring_size as rs on prs.size_id = rs.id
            where pmcf.id in (%s)
            """ % (products_string)
            cr.execute(sql)
            result = cr.fetchall()
            for item in result:
                if (item[1]):
                    item_res = item[0] + '/' + item[1] + ' = ' + item[2]
                else:
                    item_res = item[0] + ' = ' + item[2]
                result_list.append(item_res)
        return result_list

    def get_product_ids(self, cr, uid, products):
        result_list = []
        if (len(products)>0):
            products_string = ','.join([str(i) for i in products])
            sql = """select pp.id
            from product_multi_customer_fields as pmcf
            left join product_product as pp on pmcf.product_id = pp.id
            left join product_ring_size_default_rel as prs on (pp.product_tmpl_id = prs.product_id and pmcf.size_id = prs.size_id)
            left join ring_size as rs on prs.size_id = rs.id
            where pmcf.id in (%s)
            """ % (products_string)
            cr.execute(sql)
            result = cr.fetchall()
            for item in result:
                result_list.append(item[0])
        return result_list

    def check_product(self, cr, uid, settings, product_sku, item_qty):
        empty_list = []
        tag_product_search = """select distinct pp.default_code
        from product_product as pp
        left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
        left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
        where pp.id in %s
        and pp.type = 'product'
        and pmcfn.label = 'Fullfilment Notes'
        and pmcf.value = 'MFN'
        and pp.id not in (
        select product_id
        from taggings_product as tp
        where tp.tag_id = %s
        )""" % (product_sku, settings.tag.id)
        cr.execute(tag_product_search)
        tag_product_search_result = cr.fetchall()
        tag_product_search_list = []
        if (not item_qty):
            tag_product_remove = """delete from taggings_product
            where tag_id = %s and product_id in (
            select tp.product_id
            from taggings_product as tp
            left join product_product as pp on tp.product_id = pp.id
            left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            where tag_id = %s and pp.id in %s and pp.type = 'product'
            and pmcfn.label = 'Fullfilment Notes' and pmcf.value = 'AFN'
            except
            select tp.product_id
            from taggings_product as tp
            left join product_product as pp on tp.product_id = pp.id
            left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            where tag_id = %s and pp.id in %s and pp.type = 'product'
            and pmcfn.label = 'Fullfilment Notes' and pmcf.value = 'MFN'
            )""" % (settings.tag.id, settings.tag.id, product_sku, settings.tag.id, product_sku)
            cr.execute(tag_product_remove)
            if (len(tag_product_search_result) > 0):
                for product in tag_product_search_result:
                    tag_product_search_list.append(str(product[0]))
                tag_product_list_query = self.list_in_tuple(tag_product_search_list)
                tag_product_insert = """ insert into taggings_product (product_id, tag_id)
                select pp.id, tt.id
                from product_product as pp
                left join tagging_tags as tt on tt.id = %s
                where pp.default_code in %s and pp.type = 'product'""" % (settings.tag.id, tag_product_list_query)
                cr.execute(tag_product_insert)
            return tag_product_search_list

        elif (item_qty):
            tag_product_remove = """delete from taggings_product as tp
            where tag_id = %s and product_id in (
            select tp.product_id
            from taggings_product as tp
            left join product_product as pp on tp.product_id = pp.id
            left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            where tag_id = %s and pp.id in %s and pp.type = 'product'
            and pmcfn.label = 'Fullfilment Notes' and pmcf.value = 'MFN'
            except
            select tp.product_id
            from taggings_product as tp
            left join product_product as pp on tp.product_id = pp.id
            left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            where tag_id = %s and pp.id in %s and pp.type = 'product'
            and pmcfn.label = 'Fullfilment Notes' and pmcf.value = 'AFN'
            )""" % (settings.tag_fba.id, settings.tag_fba.id, product_sku, settings.tag_fba.id, product_sku)
            cr.execute(tag_product_remove)

            tag_fba_product_search = """select distinct pp.default_code
            from product_product as pp
            left join product_multi_customer_fields as pmcf on pmcf.product_id = pp.id
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            where pp.id in %s
            and pp.type = 'product'
            and pmcfn.label = 'Fullfilment Notes'
            and pmcf.value = 'AFN'
            and pp.id not in (
            select product_id
            from taggings_product as tp
            where tp.tag_id = %s
            )""" % (product_sku, settings.tag_fba.id)
            cr.execute(tag_fba_product_search)
            tag_fba_product_search_result = cr.fetchall()
            tag_fba_product_search_list = []
            if (len(tag_fba_product_search_result) > 0):
                for product in tag_fba_product_search_result:
                    tag_fba_product_search_list.append(str(product[0]))
                tag_fba_product_list_query = self.list_in_tuple(tag_fba_product_search_list)

                tag_product_insert = """ insert into taggings_product (product_id, tag_id)
                select pp.id, tt.id
                from product_product as pp
                left join tagging_tags as tt on tt.id = %s
                where pp.default_code in %s and pp.type = 'product'""" % (settings.tag_fba.id, tag_fba_product_list_query)
                cr.execute(tag_product_insert)
            return tag_fba_product_search_list

        return empty_list

    def list_in_tuple(self, list_items):
        if (len(list_items) > 1):
            result = tuple(list_items)
        elif (len(list_items) == 1 and type(list_items[0]) is int):
            result = '(' + str(list_items[0]) + ')'
        elif (len(list_items) == 1 and type(list_items[0]) is str):
            result = '(\'' + str(list_items[0]) + '\')'
        elif (len(list_items) == 0):
            result = []

        return result

    def log(self, cr, uid, settings, logs=[]):
        for log_line in logs:
            try:
                name = unicode(log_line['title'], errors='ignore')
            except:
                name = unicode(log_line['title'])
            try:
                tmp = unicode(log_line['msg'], errors='ignore')
            except:
                tmp = unicode(log_line['msg'])
            self.pool.get('res.log').create(cr, uid, {
                'name': name,
                'res_model': log_line.get('model', self._name),
                'res_id': log_line.get('id', 0),
                #'debug_information': tmp,
                'log_type': 'fba_inventory_log'
            })
            self.pool.get('fba.inventory.log').create(cr, uid, {
                'name': log_line['title'],
                'debug_information': tmp,
                'inventory_name': settings.id,
                'date': log_line.get('create_date', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()))
            })

fba_inventory()

class fba_history_inventory(osv.osv):
    _name = "fba.history.inventory"
    _description = "History Inventory Get List"
    _rec_name = 'content'

    _columns = {
        'id_note': fields.many2one('fba.inventory', 'Stock Picking FBA', required=True, ondelete='cascade'),
        'content': fields.text('List of products', required=True),
        'count_products': fields.text('Count of updated products', required=True),
        'create_date': fields.datetime('Update date', readonly=True),
    }

fba_history_inventory()

class fba_inventory_log(osv.osv):

    _name = "fba.inventory.log"

    _columns = {
        'name': fields.char('Message', size=512),
        'debug_information': fields.text('Debug information'),
        #'create_date': fields.datetime('Creation Date', readonly=True, select=1),
        'user_id': fields.many2one('res.users', 'User'),
        # 'customer_id': fields.many2one('res.partner', 'Customer'),
        'inventory_name': fields.many2one('fba.inventory', 'Inventory name'),
        # 'customer_id': fields.selection(_get_type_api, 'Api services', readonly=True, select=1),
        'date': fields.datetime('Creation Date', readonly=True, select=1)
    }

    _order = "date DESC"

    _defaults = {
        'user_id': lambda self, cr, uid, ctx: uid
    }

fba_inventory_log()