# -*- coding: utf-8 -*-

{
    'name': 'Delmar Discounts',
    'version': '0.1',
    'category': 'Sales Management',
    'complexity': "easy",
    'description': """
    """,
    'author': 'Kovalevsky Dmitry @ ProgForce',
    "website": "http://www.progforce.com/",
    'depends': [
        'base',
        'delmar_products',
        'pf_utils',
    ],
    'init_xml': [
    ],
    'update_xml': [
        "view/product_discounts_view.xml",
        "view/product_discounts_import_view.xml",
        "view/product_discounts_import_logs_view.xml"
    ],
    'demo_xml': [],
    'installable': True,
    "active": True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
