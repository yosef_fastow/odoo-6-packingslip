from osv import osv, fields
import csv
import base64
from tools.translate import _
from datetime import datetime
import time
import re
from StringIO import StringIO


class product_discount_temp(osv.osv):
    _name = 'product.discount.temp'

    _columns = {
        "partner": fields.char("Partner", size=128, required=True, ),
        "product": fields.char("Product", size=128, required=True, ),
        "date_from": fields.char("Date From", size=128, required=True, ),
        "date_to": fields.char("Date To", size=128, required=True, ),
        "percent": fields.char("Percent", size=128, required=False, ),
        "force_price": fields.char("Force price", size=128, required=False, ),
    }
product_discount_temp()


class product_discount_import(osv.osv):
    _name = 'product.discount.import'

    _columns = {
        "csv_import": fields.binary(string="Input file"),
        "state": fields.selection([('draft', 'Draft'), ('confirm', 'Confirmed')], 'Status', select=True, required=True, readonly=True),
        'csv_separator': fields.selection([(',', 'comma'), ('\t', 'tab')], 'Select csv separator')
    }

    _defaults = {
        'state': 'draft',
        'csv_import': '',
        'csv_separator': ','
    }

    def process_discount_csv(self, cr, uid, csv_data, csv_separator):
        delimiter = csv_separator
        source_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        import_rows = []

        for row_str in source_rows:
            all_data = [row for row in csv.reader([row_str], delimiter=str(delimiter))][0]
            row_data = []
            for col in all_data:
                row_data.append(str(col).replace("'", '').replace("\"", '').replace(" ", '').replace("\t", '').replace("$", '').strip())
            import_rows.append(",".join(row_data))

        head_row = str(import_rows[0]).lower().split(",")
        idx = {}

        for col in head_row:
            idx[col] = head_row.index(col)

        required_cols = [
            'partner',
            'product',
            'date_from',
            'date_to',

        ]

        alter_cols = [
            'percent',
            'force_price',
        ]

        import_errors = []
        missing_cols = [x for x in required_cols if x not in head_row]
        if missing_cols:
            import_errors.append('Wrong csv format. Missing the following columns: %s.\n\nImport was aborted.' % (', '.join(missing_cols)))

        present_alter_cols = [x for x in alter_cols if x in head_row]
        if not present_alter_cols:
            import_errors.append("Wrong csv format. At least one of the following columns should present in file: %s" % (', '.join(alter_cols)))

        if import_errors:
            import_msg = "\n".join(import_errors)
            self.pool.get('product.discount.import.logs').create(
                cr, uid, {
                    'msg': import_msg,
                    'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                    'user_id': uid,
                })
            cr.commit()
            raise osv.except_osv(_('Warning'), import_msg)

        self.write_log(cr, uid, 'Import was started.')

        try:
            tmp_file = StringIO()
            tmp_file.write("\n".join(import_rows[1:]))
            tmp_file.seek(0)
            cr.copy_from(
                tmp_file,
                'product_discount_temp',
                sep=csv_separator,
                columns=tuple(required_cols + present_alter_cols)
            )
        except Exception, e:
            raise osv.except_osv(_('Warning'), """Can't update temporary table. \nImport was aborted. \n\n %s""" % (e))

        cr.commit()

        import_warnings = self.validate_products(cr, uid)
        import_warnings += self.validate_price(cr, uid)
        import_warnings += self.validate_customers(cr, uid)
        import_warnings += self.validate_date(cr, uid)
        import_warnings += self.get_total_rows_count(cr, uid)

        for warning in import_warnings:
            self.write_log(cr, uid, warning)

        raise osv.except_osv(_('Report'), ('Import was started.\nSee logs for details.'))

    def validate_products(self, cr, uid):

        warnings = []

        sql = """   SELECT distinct pdt.product
                    FROM product_discount_temp pdt
                        LEFT JOIN product_code pp ON (
                            CASE
                                WHEN pdt.product ~ '\\\\' THEN SUBSTRING(pdt.product, 0, POSITION('\\' in pdt.product))
                                WHEN (pdt.product ~ '\\.' AND SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1) ~ '\\.') THEN SUBSTRING(pdt.product, 0, POSITION('.' in pdt.product))
                                ELSE pdt.product
                            END) = pp.name
                    WHERE pp.id IS NULL
                    GROUP BY pdt.product"""
        cr.execute(sql)
        unknown = cr.fetchall() or []
        unknown_list = [x[0] for x in unknown if x]

        if unknown_list:
            warnings.append('Unknown products: %s' % (', '.join(unknown_list)))

            sql = """   DELETE
                        FROM product_discount_temp
                        WHERE id IN (
                            SELECT pdt.id FROM product_discount_temp pdt
                                LEFT JOIN product_code pp ON (
                                    CASE
                                        WHEN pdt.product ~ '\\\\'
                                            THEN SUBSTRING(pdt.product, 0, POSITION('\\' in pdt.product))
                                        WHEN (pdt.product ~ '\\.' AND SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1) ~ '\\.')
                                            THEN SUBSTRING(pdt.product, 0, POSITION('.' in pdt.product))
                                        ELSE pdt.product
                                    END) = pp.name
                            WHERE pp.id IS NULL
                        )"""

            cr.execute(sql)
            warnings.append('Error: unknown product. Were removed %s records(s) from import' % (cr.rowcount))
            cr.commit()

        return warnings

    def validate_price(self, cr, uid):

        warnings = []

        sql = """   DELETE
                    FROM product_discount_temp pdt
                    WHERE str_to_float(force_price) = 0 and str_to_float(percent) = 0
        """
        cr.execute(sql)
        removed = cr.rowcount
        cr.commit()
        if removed:
            warnings.append('Error: wrong price. Were removed %s records(s) from import.' % (removed))

        return warnings

    def validate_customers(self, cr, uid):
        warnings = []

        cr.execute("""
            UPDATE product_discount_temp
            SET partner = trim(lower(partner))
        """)

        sql = """   SELECT pdt.partner, count(*) as count
                    FROM product_discount_temp pdt
                        LEFT JOIN res_partner rp ON pdt.partner in (lower(rp.name), lower(rp.ref))
                    WHERE rp.id IS NULL
                    GROUP BY pdt.partner"""
        cr.execute(sql)
        unknown = cr.fetchall() or []
        unknown_list = ["`%s` %s record(s)" % (x[0], x[1]) for x in unknown]

        if unknown_list:
            warnings.append('Unknown / empty customers: %s' % (', '.join(unknown_list)))

            sql = """   DELETE
                        FROM product_discount_temp
                        WHERE id IN (
                            SELECT pdt.id FROM product_discount_temp pdt
                                LEFT JOIN res_partner rp ON pdt.partner in (lower(rp.name), lower(rp.ref))
                            WHERE rp.id IS NULL
                        )"""

            cr.execute(sql)
            warnings.append('Error: customer was not found. Were removed %s records(s) from import' % (cr.rowcount))
            cr.commit()

        return warnings

    def validate_date(self, cr, uid):
        warnings = []

        sql = """select id, date_from, date_to, product from product_discount_temp"""
        cr.execute(sql)

        lines = cr.fetchall() or []

        ids = [x[0] for x in lines if not self.date_format(x[1]) or not self.date_format(x[2])]
        if ids:
            skus = [str(x[3]) for x in lines if x[0] in ids]
            cr.execute('DELETE FROM product_discount_temp where id IN %s', (tuple(ids),))
            warnings.append('Error: invalid date format for items %s. Correct formats: mm/dd/yyyy, mm-dd-yyyy, yyyy-mm-dd' % (skus))
            cr.commit()

        return warnings

    def get_total_rows_count(self, cr, uid):
        cr.execute("""
            SELECT count(*) as count
            FROM product_discount_temp
        """)
        res = cr.fetchone()
        return ['Total rows to import: %s' % (res[0])]

    def import_confirm(self, cr, uid, ids, *args):
        dt_start = datetime.now()

        cur = self.browse(cr, uid, ids[0])
        if cur.csv_import:
            csv_data = base64.decodestring(cur.csv_import)
            if csv_data:
                self.process_discount_csv(cr, uid, csv_data, cur.csv_separator)
        else:
            raise osv.except_osv('File is not selected', '')

        print "Total time %s " % (datetime.now() - dt_start)
        return True

    def copy_to_table(self, cr, uid):
        print "Discounts: copy to table"
        sql = """SELECT COUNT(*)
                 FROM product_discount_temp"""

        cr.execute(sql)
        disc_exist = cr.fetchone()[0]

        if disc_exist:
            start = datetime.now()
            discounts = self.pool.get('product.discount')
            del_list = []

            sql = """
                SELECT
                    pdt.id as "id",
                    rp.id as "customer_id",
                    pp.product_id as "product_id",
                    rs.id as "size_id",
                    pdt.date_from as "date_from",
                    pdt.date_to as "date_to",
                    str_to_float(pdt.percent) as "percent",
                    str_to_float(pdt.force_price) as "force_price"
                FROM product_discount_temp pdt
                    LEFT JOIN product_code pp ON (
                        CASE
                            WHEN pdt.product ~ '\\\\'
                                THEN SUBSTRING(pdt.product, 0, POSITION('\\' in pdt.product))
                            WHEN (pdt.product ~ '\\.' AND SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1) ~ '\\.')
                                THEN SUBSTRING(pdt.product, 0, POSITION('.' in pdt.product))
                            ELSE pdt.product
                        END) = pp.name
                    LEFT JOIN res_partner rp ON pdt.partner in (lower(rp.name), lower(rp.ref))
                    LEFT JOIN ring_size rs ON rs.name =
                        CASE
                            WHEN
                                LENGTH(COALESCE(
                                (CASE WHEN pdt.product ~ '\\\\' THEN SUBSTRING(pdt.product, POSITION('\\' in pdt.product) + 1  ) ELSE NULL END),
                                (CASE WHEN pdt.product ~ '\\.' THEN SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1  ) ELSE NULL END)
                                )::varchar(4)) = 1
                            THEN COALESCE(
                                (CASE WHEN pdt.product ~ '\\\\' THEN SUBSTRING(pdt.product, POSITION('\\' in pdt.product) + 1  ) ELSE NULL END),
                                (CASE WHEN pdt.product ~ '\\.' THEN SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1  ) ELSE NULL END)
                                )::varchar(4) || '.0'
                            ELSE
                                COALESCE(
                                (CASE WHEN pdt.product ~ '\\\\' THEN SUBSTRING(pdt.product, POSITION('\\' in pdt.product) + 1  ) ELSE NULL END),
                                (CASE WHEN pdt.product ~ '\\.' THEN SUBSTRING(pdt.product, POSITION('.' in pdt.product) + 1  ) ELSE NULL END)
                                )::varchar(4)
                            END
                    WHERE pp.id IS NOT NULL and rp.id is not null
                    GROUP BY pdt.id, rp.id , pp.product_id, rs.id
                    LIMIT 5000
                    """

            cr.execute(sql)

            import_list = cr.fetchall()

            for i in import_list:

                percent = i[6]
                force_price = i[7]
                if not (percent or force_price):
                    del_list.append(i[0])
                    continue

                date_from, date_to = self.date_format(i[4]), self.date_format(i[5])
                if not date_from or not date_to:
                    del_list.append(i[0])
                    continue

                obj = {'customer_id': i[1],
                       'product_id': i[2],
                       'size_id': i[3],
                       'date_from': date_from,
                       'date_to': date_to,
                       'percent': percent,
                       'force_price': force_price
                       }

                del_list.append(i[0])

                discounts.create(cr, uid, obj)

            if del_list:
                sql = """DELETE FROM product_discount_temp WHERE id IN (%s)""" % (', '.join(['%i' % x for x in del_list]))
                cr.execute(sql)

                sql = """
                        DELETE FROM product_discount WHERE id IN (
                            SELECT unnest(ids[1:array_length(ids, 1) -1])
                            FROM (
                                SELECT ARRAY_AGG(id order by id asc) as ids
                                FROM product_discount
                                GROUP BY customer_id, product_id, size_id, date_from, date_to
                                HAVING COUNT(*) > 1
                            )a
                        )"""

                cr.execute(sql)

                msg = 'Import status: '

                if disc_exist < 5000:
                    msg += 'finished'
                else:
                    msg += '%d discounts remains' % (disc_exist - 5000)

                self.write_log(cr, uid, msg)

            print "Total time %s " % (datetime.now() - start)
            return True
        return True

    def date_format(self, date):
        if date.find('/') != -1:
            sep = '/'
        elif date.find('-') != -1:
            sep = '-'
        else:
            return False

        if len(date.split(sep)[0]) == 4:
            return date
        else:
            if int(date.split(sep)[0]) > 12:
                return False

        try:
            return datetime.strptime(date, "%m" + sep + "%d" + sep + "%Y").strftime("%Y" + sep + "%m" + sep + "%d")
        except Exception, e:
            return False


    def write_log(self, cr, uid, import_msg):
        self.pool.get('product.discount.import.logs').create(cr, uid, {
            'msg': import_msg,
            'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            'user_id': uid
        })
        cr.commit()

    def f7(self, seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if x not in seen and not seen_add(x)]

product_discount_import()


class product_discount_import_logs(osv.osv):
    _name = 'product.discount.import.logs'

    _columns = {
        'date': fields.datetime('Import date'),
        'user_id': fields.many2one('res.users', 'User'),
        'msg': fields.text('Message')
    }

    _order = "date DESC"

product_discount_import_logs()
