from osv import osv, fields
from openerp import SUPERUSER_ID
import time


class product_discount(osv.osv):
    _name = 'product.discount'

    def percent_onchange(self, cr, uid, ids, percent, force_price):
        res = {'value': {}}
        if not (percent or force_price):
            res['warning'] = {'title': 'Error', 'message': 'Percent or Force price should be not equal to zero!'}
        return res

    def _select_customer_price(self, cr, uid, ids, name, args, context=None):
        res = {x: 0 for x in ids}

        sql = """
            select pd.id, str_to_float(ff.value) as price
            from product_discount pd
                left join product_multi_customer_fields_name fn on
                    fn.customer_id = pd.customer_id
                    and fn.label = 'Customer Price'
                left join product_multi_customer_fields ff on
                    ff.field_name_id = fn.id
                    and ff.product_id = pd.product_id
                    and coalesce(ff.size_id, 0) = case when fn.expand_ring_sizes is not true then 0 else coalesce(pd.size_id, 0) end
            where pd.id in %s
        """
        cr.execute(sql, (tuple(ids),))
        sql_res = cr.fetchall()
        res.update({x[0]: x[1] for x in sql_res})
        return res

    def _select_discount_price(self, cr, uid, ids, name, args, context=None):
        res = {}
        for ap in self.browse(cr, uid, ids):
            if ap.force_price:
                res[ap.id] = ap.force_price
            else:
                res[ap.id] = ap.price and (ap.price - (ap.price * (ap.percent / 100))) or False
        return res

    def _price_search(self, cr, uid, obj, name, args, context):
        ids = set()
        if name == 'discount_price':
            price_type = 'coalesce(pap.force_price, pap.price - (pap.price * (pd.percent / 100)))::Numeric(32,2)'
        else:
            price_type = 'pap.price::Numeric(32,2)'

        for cond in args:
            amount = cond[2]
            if isinstance(cond[2], (list, tuple)):
                if cond[1] in ['in', 'not in']:
                    amount = tuple(cond[2])
                else:
                    continue
            else:
                if cond[1] in ['=like', 'like', 'not like', 'ilike', 'not ilike', 'in', 'not in', 'child_of']:
                    continue

            cr.execute("""
                SELECT pd.id
                FROM product_discount pd
                    LEFT JOIN product_acc_price pap ON pd.customer_id = pap.customer_id
                        AND pd.product_id = pap.product_id
                GROUP BY pd.id
                HAVING {field} {condition} %(amount)s
            """.format(field=price_type, condition=cond[1]), {
                'amount': amount
                })

            res_ids = set(id[0] for id in cr.fetchall())
            ids = ids and (ids & res_ids) or res_ids
        if ids:
            return [('id', 'in', tuple(ids))]
        return [('id', '=', '0')]

    _columns = {
        'customer_id': fields.many2one('res.partner', 'Customer'),
        'product_id': fields.many2one('product.product', 'Product',domain="[('type', '!=', 'consu')]"),
        'size_id':  fields.many2one('ring.size', 'Ring size'),
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'percent': fields.float('%', digits=(2, 2), required=False),
        'force_price': fields.float('Force price', digits=(2, 2), required=False),
        'price': fields.function(_select_customer_price, string='Original Price', method=True, store=False, type='float', fnct_search=_price_search),
        'discount_price': fields.function(_select_discount_price, string='Discount Price', method=True, store=False, type='float', fnct_search=_price_search)
    }
    _defaults = {
        'percent': None
    }

    def create(self, cr, uid, vals, context=None):
        duplicates = self.check_for_duplicate_discounts(cr, uid, vals)
        if context and duplicates:
            raise osv.except_osv(('Overlapping Discount Detected'), (duplicates))
        elif duplicates:
            partner = self.pool.get("res.partner").browse(cr, uid, vals['customer_id']).name
            product = self.pool.get("product.product").browse(cr, uid, vals['product_id']).default_code
            size_string = ''
            if vals['size_id']:
                size = self.pool.get("ring.size").browse(cr, uid, vals['size_id']).name
                size_string = "Ring Size: {} \n".format(size) 
            msg = """Overlapping Discount Detected
            \nCustomer:  {} \nProduct: {} \n{}Date From: {} \nDate To: {}
            """.format(partner, product, size_string, vals['date_from'],vals['date_to'])
            self.pool.get('product.discount.import').write_log(cr, uid, msg)
            return False
        else: 
            return super(product_discount, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        discounts = self.browse(cr, uid, ids)[0]
        check_vals = {
            'customer_id': discounts.customer_id.id,
            'product_id': discounts.product_id.id,
            'size_id': discounts.size_id.id,
            'date_from': discounts.date_from,
            'date_to': discounts.date_to
        }
        for k, v in vals.items():
            if k in check_vals:
                check_vals[k] = v

        duplicates = self.check_for_duplicate_discounts(cr, uid, check_vals, [discounts])
        if duplicates:
            raise osv.except_osv(('Overlapping Discount Detected'), (duplicates))
        else: 
            return super(product_discount, self).write(cr, uid, ids, vals, context=context)

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if not args:
            # No default auto query
            return []
        return super(product_discount, self).search(cr, uid, args, offset, limit, order, context, count)

    def get_result_price(self, cr, uid, product_id, size_id=None, partner_id=None, price=None, context=None):

        result_price = price

        cur_date = time.strftime('%Y-%m-%d', time.gmtime())

        active_settings = self.search(
            cr, SUPERUSER_ID, [
                ('product_id', '=', product_id),
                ('size_id', 'in', [size_id, 0]),
                ('customer_id', 'in', [partner_id, 0]),
                ('date_from', '<=', cur_date),
                ('date_to', '>=', cur_date),
                '|', ('percent', '!=', False), ('force_price', '!=', False)
            ],
            limit=1,
            order='customer_id,size_id'
            )

        if active_settings:
            discount = self.read(cr, SUPERUSER_ID, active_settings[0], ['percent', 'discount_price', 'force_price'])

            d_percent = discount['percent']

            if discount['force_price'] or not price:
                result_price = discount['discount_price']

            elif d_percent:
                price = float(price)
                result_price = price - (price * (d_percent / 100.0))

        return result_price

    def check_for_duplicate_discounts(self, cr, uid, vals, discount_ids=[]):
        extra_fields = ''
        if vals['size_id']:
            extra_fields = "AND size_id = {}".format(vals['size_id'])
        if discount_ids:
            for discount_id in discount_ids:
                extra_fields += "AND id != {}".format(discount_id.id)

        # four parts of query for overlapping discounts:
        #1: date to is in between and existing discount
        #2: date from is in between and existing discount
        #3: new discount starts before an existing discount and ends after an existing discount
        #4: new discount starts in middle of an existing discount and ends in middle of an existing discount
        sql = """
        SELECT * FROM product_discount
        WHERE customer_id = {customer_id}
            AND  product_id = {product_id}
            {extra_fields}
            AND (
                (date_to >= '{date_from}'::date AND date_to <= '{date_to}'::date)
            OR 
                (date_from >= '{date_from}'::date AND date_from <= '{date_to}'::date)
            OR
                (date_to <= '{date_to}'::date AND date_from >= '{date_from}'::date)
            OR
                (date_to >= '{date_to}'::date AND date_from <= '{date_from}'::date)
            )
        """.format(
            customer_id=vals['customer_id'],
            product_id=vals['product_id'],
            extra_fields=extra_fields,
            date_from=vals['date_from'],
            date_to=vals['date_to']
        )
        
        cr.execute(sql)
        import_list = cr.fetchall()
        total_results = len(import_list)
        if total_results > 0:
            return """This discount is overlapping the time frame of an existing discount.
                Please update the Date From or Date To and try again!"""
        return False

product_discount()
