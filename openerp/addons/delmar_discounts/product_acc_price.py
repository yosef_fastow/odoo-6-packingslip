from osv import osv, fields


class product_acc_price(osv.osv):
    _name = 'product.acc.price'

    _columns = {
        'customer_id': fields.many2one('res.partner', 'Customer'),
        'product_id': fields.many2one('product.product', 'Product'),
        'sku_no': fields.char('Sku No', size=100),
        'desc': fields.text('Description'),
        'desc1': fields.text('Description 1'),
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'prod_tarif': fields.char('Prod Tarif', size=100),
        'prod_manuf': fields.char('Manufactured', size=100),
        'grand_total': fields.char('Grand Total', size=100),
        'kid': fields.char('Kid', size=100),
        'price': fields.float('Price', digits=(2, 2)),
        'ts': fields.char('TS', size=100)
    }

product_acc_price()
