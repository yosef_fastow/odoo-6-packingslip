from osv import osv


class product_multi_customer_names(osv.osv):
    _inherit = "product.multi.customer.names"

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        if context is None:
            context = {}
        if(context.get('customer_id')):
            fields.append('prod_def')
            all_fields = super(product_multi_customer_names, self).read(cr, uid, ids, fields=fields, context=context, load='_classic_read')
            custom_fields_id = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('customer_id', '=', context.get('customer_id'))])
            custom_fields = self.pool.get('product.multi.customer.fields.name').read(cr, uid, custom_fields_id, ['name_id', 'label'])
            res = []
            for field in all_fields:
                add = False

                for custom_field in custom_fields:
                    if(field['id'] == custom_field['name_id'][0]):
                        field['label'] = custom_field['label']
                        add = True
                        break
                if(add == True or field['prod_def'] in [1, 3]):
                    res.append(field)
        else:
            res = super(product_multi_customer_names, self).read(cr, uid, ids, fields=fields, context=context, load='_classic_read')
        return res

product_multi_customer_names()


class product_product(osv.osv):

    _inherit = 'product.product'

    def write(self, cr, user, ids, vals, context=None):
        for key in vals.keys():
            if(key.find('property') != -1):
                del vals[key]
        return super(product_product, self).write(cr, user, ids, vals, context=context)

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        if context is None:
            context = {}

        if fields is None:
            fields = []

        res = super(product_product, self).read(cr, uid, ids, fields=fields, context=context, load=load)

        task_id = context.get('task_id', None)

        if(task_id != None and 'property_0' in fields):
            task = self.pool.get('filling.task').browse(cr, uid, task_id)
            res = []
            display_fields = task.display_fields
            for prod_id in ids:
                i = 0
                obj = {'id': prod_id}
                value = False
                for field in display_fields:
                    field_name_id = self.pool.get('product.multi.customer.fields.name').search(cr, uid, ['&', ("name_id", '=', field.id), ('customer_id', '=', task.partner_id.id)])
                    if(len(field_name_id) > 0):
                        field_name_id = field_name_id[0]
                        custom_field_id = self.pool.get('product.multi.customer.fields').search(cr, uid, ['&', '&', ('partner_id', '=', task.partner_id.id), ('field_name_id', '=', field_name_id), ('product_id', '=', prod_id)])
                        if(len(custom_field_id) > 0):
                            value = self.pool.get('product.multi.customer.fields').browse(cr, uid, custom_field_id[0]).value
                    else:
                        if(field.prod_def == 3):
                            additional_id = self.pool.get("product.multi.customer.additional.fields").search(cr, uid, ['&', ('name_id', '=', field.id), ('product_id', '=', prod_id)], context=context)
                            if(len(additional_id) > 0):
                                value = self.pool.get("product.multi.customer.additional.fields").browse(cr, uid, additional_id[0], context=context).value
                        else:
                            value = getattr(self.browse(cr, uid, prod_id), field.name, False)
                            if(isinstance(value, osv.orm.browse_record)):
                                value = value.name_get()[0][1]
                            elif(isinstance(value, osv.orm.browse_null)):
                                value = ""

                    obj['property_' + str(i)] = value
                    i += 1
                res.append(obj)

        if (task_id != None and 'multi_customer_fields' in fields and 'multi_customer_additional_fields' in fields):
            task = self.pool.get('filling.task').read(cr, uid, task_id)
            for prod in res:
                prod['multi_customer_fields'] = self.pool.get('product.multi.customer.fields').search(cr, uid, ['&', ("field_name_id", "in", task['customer_fields']), ('product_id', '=', prod['id'])])
                prod['multi_customer_additional_fields'] = self.pool.get('product.multi.customer.additional.fields').search(cr, uid, ['&', ("name_id", "in", task['additional_fields']), ('product_id', '=', prod['id'])])

        return res

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):

        res = super(product_product, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
        if(context.get('task_id', False) and res['arch'].find('identify_filling_task') != -1):
            task = self.pool.get('filling.task').browse(cr, uid, context.get('task_id'))
            i = 0
            if(len(task.display_fields) > 0):
                tmp = '<tree string="Products">'
                for field in task.display_fields:

                    custom_fields_id = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('name_id', '=', field.id), ('customer_id', '=', task.partner_id.id)])

                    label = field.label
                    if(len(custom_fields_id) > 0):
                        tmpStr = self.pool.get('product.multi.customer.fields.name').read(cr, uid, custom_fields_id[0])['label']
                        if(tmpStr):
                            label = tmpStr

                    res['fields']['property_' + str(i)] = {'selectable': True,
                       'size': 256,
                       'string': label,
                       'type': 'char'
                    }
                    tmp += '<field name="property_' + str(i) + '"/>'

                    i += 1
                tmp += '</tree>'
                res['arch'] = tmp
        if(context.get('task_id', False) and res['arch'].find('replace_me') != -1):
            task = self.pool.get('filling.task').browse(cr, uid, context.get('task_id'))
            i = 0
            tmp = ''
            if(len(task.display_fields) > 0):
                for field in task.display_fields:

                    custom_fields_id = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('name_id', '=', field.id), ('customer_id', '=', task.partner_id.id)])

                    label = field.label
                    if(len(custom_fields_id) > 0):
                        tmpStr = self.pool.get('product.multi.customer.fields.name').read(cr, uid, custom_fields_id[0])['label']
                        if(tmpStr):
                            label = tmpStr

                    res['fields']['property_' + str(i)] = {
                       'size': 256,
                       'select': True,
                       'selectable': True,
                       'string': label,
                       'type': 'char',
                       'readonly': True,
                       'views': {}
                    }

                    tmp += '<field name="property_' + str(i) + '" readonly="True"/>'

                    i += 1
            res['arch'] = res['arch'].replace('<separator string="replace_me"/>', tmp)
        return res

product_product()
