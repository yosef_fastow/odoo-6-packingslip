{
    "name" : "Filling task",
    "version" : "0.1",
    "author" : "Ivan Burlutskiy @ Prog-Force",
    "website" : "http://www.progforce.com/",
    "depends" : [
        "base",
        "pf_utils",
        "stock",
        "project",
        "sale",
        "delmar_products",
        "openerp_shipping",
        "fetchmail",
        "email_template",
        "tagging",
        "tagging_product",
        "multi_customer",
    ],
    "description" : """

    """,
    "category": "Project Management",
    "init_xml" : [
    ],
    "demo_xml" : [],
    "update_xml" : [
        'security/security_filling_task.xml',
        'security/ir.model.access.csv',
        'filling_view.xml',
        'board_project_view.xml',
        'data/email_template_data.xml',
        'product_view.xml'
    ],
    "application": True,
    "active": False,
    "installable": True,
}
