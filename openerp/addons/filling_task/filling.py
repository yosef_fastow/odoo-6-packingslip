import time
### import datetime
from osv import fields, osv


class filling_task(osv.osv):

    _name = "filling.task"
    _description = 'Filling Task'
    _date_name = "date_start"

    _columns = {
        'name': fields.char('Task Summary', size=128, required=True, select=True),
        'description': fields.text('Description'),
        'priority': fields.selection([('4', 'Very Low'), ('3', 'Low'), ('2', 'Medium'), ('1', 'Important'), ('0', 'Very important')], 'Priority', select=True),
        'sequence': fields.integer('Sequence', select=True, help="Gives the sequence order when displaying a list of tasks."),
        'state': fields.selection([('draft', 'New'), ('generation', 'Task Generation'), ('start', 'Ready to Start'), ('open', 'In Progress'), ('pending', 'Pending'), ('done', 'Done'), ('cancelled', 'Cancelled')], 'State', readonly=True, required=True),
        'create_date': fields.datetime('Create Date', readonly=True, select=True),
        'date_start': fields.datetime('Starting Date', select=True),
        'date_submit': fields.datetime('Submit Date', select=True),
        'date_end': fields.datetime('Ending Date', select=True),
        'date_deadline': fields.date('Deadline', select=True),
        'notes': fields.text('Notes'),
        'user_id': fields.many2one('res.users', 'Assigned to'),
        'partner_id': fields.many2one('res.partner', 'Customer'),

        'progress': fields.float('Progress'),

        'product_id': fields.many2many('product.product', 'filling_task_product', 'task_id', 'product_id', 'Taxes'),  # , readonly=True, states={'open': [('readonly', False)]}),

        'customer_fields': fields.many2many('product.multi.customer.fields.name', 'filling_task_field', 'task_id', 'field_id', 'Customers fields'),

        'additional_fields': fields.many2many('product.multi.customer.names', 'filling_task_additional_field', 'task_id', 'field_id', 'Additional fields'),

        'display_fields': fields.many2many('product.multi.customer.names', 'filling_task_display_field', 'task_id', 'field_id', 'Display fields'),

        'id': fields.integer('ID', readonly=True),
        'user_email': fields.related('user_id', 'user_email', type='char', string='User Email', readonly=True),

        'create_uid': fields.many2one('res.users', 'Creator', readonly=True),
        "tagging_id": fields.many2one("tagging.tags", "Tags", readonly=True, states={'draft': [('readonly', False)]}),
    }

    _defaults = {
        'state': 'draft',
        'priority': '2',
        'sequence': 10,
        'user_id': lambda obj, cr, uid, context: uid,
        'progress': 0
    }

    _order = "priority, sequence, date_start, name, id"

    def task_generate_fields(self, cr, uid, ids, generate_ids=None, product_ids=None, cron_id=0, context=None):
        print "Task Generate Fields"
        if not product_ids: product_ids = []
        if not generate_ids: generate_ids = []
        if generate_ids and product_ids and cron_id:
            context = {'fields_ids': generate_ids}
            self.pool.get('product.product').read(cr, uid, product_ids, fields=['multi_customer_fields', 'multi_customer_additional_fields'], context=context)
            self.write(cr, uid, ids, {'state': 'start'}, context=context)
            self.pool.get('ir.cron').unlink(cr, uid, [cron_id], context)

    # def read(self, cr, uid, ids, fields={}, context={}, load='_classic_read'):
    #     res = super(filling_task, self).read(cr, uid, ids, fields=fields, context=context, load=load)
    #     if not context:
    #         context={}
    #     if fields and 'product_id' in fields:
    #         for prod in res:
    #             display_fields_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('name_id', 'in', prod['display_fields']), ('customer_id', '=', prod['partner_id'])])
    #             context.update({'filling_task': prod['id']})
    #             if prod['customer_fields'] or prod['additional_fields']:
    #                 context.update({'fields_ids': display_fields_ids})
    #                 self.pool.get('product.product').read(cr, uid, prod['product_id'], fields=['multi_customer_fields', 'multi_customer_additional_fields'], context=context)
    #     return res

    def do_generate(self, cr, uid, ids, context=None):
        if not context: context = {}
        cur = self.browse(cr, uid, ids[0])
        if cur.display_fields or cur.customer_fields and cur.product_id:
            display_fields_names = [field.id for field in cur.display_fields]
            display_fields_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('name_id', 'in', display_fields_names), ('customer_id', '=', cur.partner_id and cur.partner_id.id or False)])
            generate_ids = list(set(display_fields_ids + [field.id for field in cur.customer_fields]))
            product_ids = [product.id for product in cur.product_id]
            self.write(cr, uid, ids, {'state': 'generation'}, context=context)
            cron_obj = self.pool.get('ir.cron')
            vals = {
                'name': cur.name + ' Task Generation',
                'user_id': uid,
                'model': self._name,
                'interval_type': 'minutes',
                'function': 'task_generate_fields',
                'args': '(%s,%s,%s,0)' % (ids, generate_ids, product_ids),
                'numbercall': -1,
                'priority': 4,
                'type': 'task', # defined in pf_utils/ir/ir_cron.py
            }
            cron_id = cron_obj.create(cr, uid, vals)
            cron_obj.write(cr, uid, [cron_id],{'args': '(%s,%s,%s,%d)' % (ids, generate_ids, product_ids, cron_id)})
            return True
        return False

    def do_start(self, cr, uid, ids, context=None):
        if not context: context = {}
        if not isinstance(ids, list):
            ids = [ids]
        tasks = self.browse(cr, uid, ids, context=context)
        for t in tasks:
            data = {'state': 'open'}
            if not t.date_start:
                data['date_start'] = time.strftime('%Y-%m-%d %H:%M:%S')
            self.write(cr, uid, [t.id], data, context=context)

            self.send_mail(cr, uid, ids, 'Automated Assigne Filling Task Notification Mail to User', context=context)

        return True

    def do_draft(self, cr, uid, ids, context=None):
        if not context: context = {}
        self.write(cr, uid, ids, {'state': 'draft'}, context=context)
        self.send_mail(cr, uid, ids, 'Automated Draft Filling Task Notification Mail to User', context=context)
        return True

    def do_submit(self, cr, uid, ids, context=None):
        if not context: context = {}
        self.write(cr, uid, ids, {'state': 'pending', 'progress': 100, 'date_submit': time.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
        self.send_mail(cr, uid, ids, 'Automated Submit Filling Task Notification Mail to Owner', context=context)
        return True

    def do_approve(self, cr, uid, ids, context=None):
        if not context: context = {}
        self.write(cr, uid, ids, {'state': 'done', 'date_end': time.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
        self.send_mail(cr, uid, ids, 'Automated Approve Filling Task Notification Mail to User', context=context)
        return True

    def do_reject(self, cr, uid, ids, context=None):
        if not context: context = {}
        self.write(cr, uid, ids, {'state': 'open', 'date_start': time.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
        self.send_mail(cr, uid, ids, 'Automated Reject Task Notification Mail to User', context=context)
        return True

    def do_cancel(self, cr, uid, ids, context=None):
        if not context: context = {}
        self.write(cr, uid, ids, {'state': 'cancelled', 'date_end': time.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
        return True

    def on_partner_id(self, cr, uid, id, context=None):
        if not context: context = {}
        return {'value': {'customer_fields': []}}

    def on_tagging_id(self, cr, uid, id, tagging_id, context=None):
        if not context: context = {}
        res = self.pool.get("tagging.tags").read(cr, uid, tagging_id, context=context)
        return {'value': {'product_id': res['product_ids']}}

    def send_mail(self, cr, uid, ids, tpl_name, context=None):
        tpl_obj = self.pool.get('email.template')
        tpl_inst = tpl_obj.search(cr, uid, [('name', '=', tpl_name)])[0]

        web_root_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')

        models_data = self.pool.get('ir.model.data')
        action = models_data.get_object_reference(cr, uid, 'filling_task', 'action_view_filling_task')
        if action:
            context['act_id'] = action[1]

        context['web_root_url'] = web_root_url

        if not isinstance(ids, list):
            ids = [ids]
        tasks = self.browse(cr, uid, ids, context=context)

        for t in tasks:
            tpl_obj.send_mail(cr, uid, tpl_inst, t.id, False, context)
        return True

filling_task()
