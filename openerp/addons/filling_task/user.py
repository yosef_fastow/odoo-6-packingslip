from osv import fields, osv


class res_users(osv.osv):
    _inherit = 'res.users'

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context: context = {}
        for i in xrange(len(args)):
            if args[i] == 'user_id':
                models_data = self.pool.get('ir.model.data')

                groups = []
                groups.append(models_data.get_object_reference(cr, uid, 'project', 'group_project_manager')[1])
                groups.append(models_data.get_object_reference(cr, uid, 'filling_task', 'user_filling_group')[1])

                a = ('groups_id', 'in', groups)
                del args[i]
                args.append(a)
                break

        res = super(res_users, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

        return res
res_users()