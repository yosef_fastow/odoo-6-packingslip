openerp.multi_customer = function(openerp) {

    openerp.web.form.widgets.add('many2one_char', 'openerp.multi_customer.many2one_char');
    openerp.multi_customer.many2one_char = openerp.web.form.FieldChar.extend({
        set_value: function(value) {
            this._super.apply(this, arguments);
            var show_value = value[1];
            this.$element.find('input').val(show_value);
            return show_value;
        }
    });
    openerp.multi_customer.origin_format_value = openerp.web.format_value;


    openerp.web.form.widgets.add('many2one_search', 'openerp.multi_customer.many2one_search');
    openerp.multi_customer.many2one_search = openerp.web.form.FieldOne2Many.extend({
        load_views: function() {
            var self = this;
            var modes = this.node.attrs.mode;
            modes = !! modes ? modes.split(",") : ["tree"];
            var views = [];
            _.each(modes, function(mode) {
                var view = {
                    view_id: false,
                    view_type: mode == "tree" ? "list" : mode,
                    options: {
                        sidebar: false,
                        limit: -1,
                        pager: false,
                        addable: false,
                        deletable: false
                    }
                };
                if(self.field.views && self.field.views[mode]) {
                    view.embedded_view = self.field.views[mode];
                }
                if(view.view_type === "list") {
                    view.options.selectable = self.multi_selection;
                    if(self.is_readonly()) {
                        view.options.isClarkGable = false;
                    }
                } else if(view.view_type === "form") {
                    if(self.is_readonly()) {
                        view.view_type = 'page';
                    }
                    view.options.not_interactible_on_create = true;
                }
                views.push(view);
            });
            this.views = views;

            this.viewmanager = new openerp.web.ViewManager(this, this.dataset, views, {});
            this.viewmanager.template = 'One2Many.viewmanager';
            this.viewmanager.registry = openerp.web.views.extend({
                list: 'openerp.web.form.One2ManyListView',
                form: 'openerp.web.form.One2ManyFormView',
                page: 'openerp.web.PageView'
            });
            var once = $.Deferred().then(function() {
                self.init_form_last_update.resolve();
            });
            var def = $.Deferred().then(function() {
                self.initial_is_loaded.resolve();
            });
            this.viewmanager.on_controller_inited.add_last(function(view_type, controller) {
                if(view_type == "list") {
                    controller.o2m = self;
                    if(self.is_readonly()) controller.set_editable(false);
                } else if(view_type == "form" || view_type == 'page') {
                    if(view_type == 'page' || self.is_readonly()) {
                        $(".oe_form_buttons", controller.$element).children().remove();
                    }
                    controller.on_record_loaded.add_last(function() {
                        once.resolve();
                    });
                    controller.on_pager_action.add_first(function() {
                        self.save_any_view();
                    });
                } else if(view_type == "graph") {
                    self.reload_current_view()
                }

                self.$search_str = $("input[name*='search_str']");
                self.$search_str.change(function() {
                    self.on_search_action();
                });

                self.$search_partner = $("input[name*='search_partner_id']");
                // self.$search_partner.val($.cookie("partner"));

                self.$search_partner.bind('on_set_value', function(input, value){
                    // $.cookie("partner", value);
                    self.$search_partner.val(value);
                    self.on_search_action();
                });

                def.resolve();
            });
            this.viewmanager.on_mode_switch.add_first(function(n_mode, b, c, d, e) {
                $.when(self.save_any_view()).then(function() {
                    if(n_mode === "list") $.async_when().then(function() {
                        self.reload_current_view();
                    });
                });
            });
            this.is_setted.then(function() {
                $.async_when().then(function() {
                    self.viewmanager.appendTo(self.$element);
                });
            });
            return def;
        },
        on_search_action: function() {
            var self = this;

            $.extend($.expr[':'], {
                'containsi': function(elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });

            if (!self.$search_str || !self.$search_partner) {
                return true;
            }

            var search_str =self.$search_str.val(),
                search_partner = self.$search_partner.val(),
                rows = self.$element.find('.ui-widget-content tr'),
                search = [];
            rows.show();

            search_str = $.trim(search_str);
            $.each(search_str.split('"'), function(index, value){
                var str = $.trim(value);
                if(str != '') {
                    if(index%2==0){
                        $.each(str.split(' '), function(index, value){
                            search.push(value);
                        });
                    } else {
                        search.push(str);
                    }
                }
            });

            $.each(rows, function(index, row) {
                if(search_partner != '' && !$("td.oe-field-cell[data-field*='partner_id']:contains('" + search_partner + "')", row).length){
                    $(row).hide();
                    return true;
                }
                if(search.length != 0) {
                    var do_hide = true;
                    $.each(search, function(index, param) {
                        if($('td.oe-field-cell:containsi("' + param + '")', row).length) {
                            do_hide = false;
                            return false;
                        }
                    });
                    if (do_hide) {
                      $(row).hide();
                    }
                }
            });
        }
    });

    openerp.web.format_value = function(value, descriptor, value_if_empty) {
        val = openerp.multi_customer.origin_format_value(value, descriptor, value_if_empty);
        if(descriptor.widget === "many2one_char" && _.isArray(val)) {
            val = val[1];
        }
        return val;
    };
};