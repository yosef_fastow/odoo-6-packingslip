# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.safe_eval import safe_eval
from tools.translate import _
import logging
from openerp.addons.pf_utils.utils.str_utils import str_encode

import re
import math


_logger = logging.getLogger(__name__)


class product_multi_customer_method(osv.osv):
    _name = 'product.multi.customer.method'

    _columns = {
        'name': fields.char('Name', size=256, ),
        'description': fields.text('Description', ),
        'code': fields.text('Code', ),
        'product_id': fields.many2one('product.product', 'Product', ),
        'size_id': fields.many2one('ring.size', 'Size', ),
        'customer_id': fields.many2one('res.partner', 'Customer', ),
        'field_id': fields.integer('Field ID', ),
        'field_name_id': fields.integer('Field Name ID', ),
        'result': fields.char('Result', size=256, ),
        'args_str': fields.char('Arguments', size=256, ),
    }

    _defaults = {
        'args_str': '()',
    }

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'The name must be unique !')
    ]

    def test_code(self, cr, uid, ids, context=None):
        for test_obj in self.browse(cr, uid, ids):
            cxt = {
                'product_id': test_obj.product_id.id or False,
                'size_id': test_obj.size_id.id or False,
                'customer_id': test_obj.customer_id.id or False,
                'field_id': test_obj.field_id or False,
                'field_name_id': test_obj.field_name_id or False,
                'args_str': test_obj.args_str or False,
            }
            test = self.run(cr, uid, test_obj.id, show_errors=True, context=cxt)
            self.write(cr, uid, test_obj.id, {'result': test})

        return True

    def run(self, cr, uid, id, show_errors=False, context=None):
        if context is None:
            context = {}

        result = ''
        cxt = context.copy()

        gcxt = {}
        for key in __builtins__:
            gcxt[key] = __builtins__[key]

        gcxt.update({
            'self': self,
            'cr': cr,
            'uid': uid,
            'args': (),
            're': re,
            'math': math,
        })

        try:
            args_str = cxt.get('args_str', False)
            if args_str and args_str.strip():
                gcxt['args'] = eval(args_str)
        except Exception, e:
            msg = 'ARGs: {0}\nError: {1}'.format(args_str, e.message)
            if show_errors:
                raise osv.except_osv(_('Error'), msg)
            else:
                _logger.error(msg)

        method = self.read(cr, uid, id, ['code', 'name'])
        if method.get('code', False):

            try:
                safe_eval(method['code'], globals_dict=gcxt, locals_dict=cxt, mode="exec", nocopy=True)
                if 'result' in cxt:
                    result = str_encode(cxt['result'])
            except Exception, e:
                msg = 'Method: {method}\nError: {error}\nArgs: {args}'.format(
                    method=method['name'],
                    error=e.message,
                    args=cxt,
                    )
                if show_errors:
                    raise osv.except_osv(_('Error'), msg)
                else:
                    _logger.error(msg)

        return result

product_multi_customer_method()
