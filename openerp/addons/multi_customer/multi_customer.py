# -*- coding: utf-8 -*-

from openerp.osv import osv, fields
import hashlib
import json
import re
from math import *
from string import *
import datetime
from openerp.tools.translate import _
import logging
from openerp.addons.pf_utils.utils.safe_eval import SAFE_DICT
from openerp.addons.pf_utils.utils.str_utils import str_encode
from json import loads as json_loads
from product_upc import UpcOverlimit
from pf_utils.utils.re_utils import str2float
from delmar_pricing.factory_cost import FactoryCostFormula

_logger = logging.getLogger(__name__)


class Setting(object):
    CUSTOMER_FIELDS = 0
    INIT_FIELDS = 1
    ADDITIONAL_FIELDS = 3
    SYSTEM_FIELD = 4


class product_multi_customer_composite_category(osv.osv):
    _rec_name = 'category'
    _name = "product.multi.customer.composite.category"

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'category': fields.many2one('product.category', 'Category', ondelete='cascade', required=True),
        'composite': fields.char('Composite Value', size=512, required=True),

        'model_object_field': fields.many2one('product.multi.customer.names', string="Field",
            help="Select target field from the related document model.\n"
            "If it is a relationship field you will be able to select "
            "a target field at the destination of the relationship."),
        'copyvalue': fields.char('Expression', size=256, help="Final placeholder expression, to be copy-pasted in the desired template field."),
    }

    def onchange_model_object_value_field(self, cr, uid, ids, model_object_field, context=None):
        result = {
            'copyvalue': False,
        }

        if model_object_field:
            field_value = self.pool.get('product.multi.customer.names').browse(cr, uid, model_object_field, context=context)

            if field_value:
                field_name = field_value.name

                result.update({
                    'copyvalue': "${%s}" % (field_name),
                })

        return {'value': result}


class product_multi_customer_names(osv.osv):
    _rec_name = 'label'
    _name = "product.multi.customer.names"

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Name', size=512, required=True),
        'label': fields.char('Label', size=512, required=True),
        'prod_def': fields.integer('prod_def', readonly=True),
        'def_additional_val': fields.char('Default Value', size=512),
        'dict_values': fields.text('Dictionary Values'),
    }
    _defaults = {
        'prod_def': Setting.CUSTOMER_FIELDS
    }

    def create(self, cr, uid, vals, context=None):
        if not context: context = {}
        vals['prod_def'] = context.get('prod_def', Setting.CUSTOMER_FIELDS)
        name_id = super(product_multi_customer_names, self).create(cr, uid, vals, context=context)

        return name_id

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}
        if(context.get('prod_def', Setting.CUSTOMER_FIELDS) == Setting.ADDITIONAL_FIELDS):
            args.append(('prod_def', '=', Setting.ADDITIONAL_FIELDS))
        return super(product_multi_customer_names, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def get_all_records(self, cr, uid, context=None):
        if not context:
            context = {}
        fields = []
        for field_id in self.search(cr, uid, [('prod_def', '=', Setting.ADDITIONAL_FIELDS)], context={}):
            field = self.browse(cr, uid, field_id)
            fields.append(
                {
                    "id": field_id,
                    "name": field.name,
                    "value": field.def_additional_val
                }
            )

        return fields

    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        for prod in self.browse(cr, uid, ids, context=context):
            if prod.prod_def != Setting.SYSTEM_FIELD:
                super(product_multi_customer_names, self).unlink(cr, uid, prod.id, context=context)
            else:
                raise osv.except_osv(_('Error'), _('Have no permissions to delete system field'))

product_multi_customer_names()


class product_multi_customer_property_category(osv.osv):
    _name = "product.multi.customer.property.category"

    _columns = {
        'name': fields.char('Name', size=512, required=True),
        # 'description': fields.char('Description', size=512, required=True),
    }


class product_multi_customer_file_label(osv.osv):
    _name = "product.multi.customer.file.label"

    _rec_name = 'label_in_file'

    _columns = {
        'label_in_file': fields.char('Label in file', size=512, required=True),
        'file_type': fields.selection([('inventory_fields', 'inventory fields'), ('price_fields', 'price fields')], 'Type of importing file', required=True,
            help="Collects customer fields in category that presented list of columns of particular file type"),
        'required': fields.boolean('Required'),
        'fill_value': fields.char('Fill value', required=False, size=512, help="Value to fill Customer Field if column of import file is not specified"),
        'field_name_ids': fields.many2many('product.multi.customer.fields.name', 'product_multi_customer_file_label_field', 'label_id', 'field_id', 'Fields for this label'),
    }


class product_multi_customer_fields_name_type(osv.osv):
    _name = 'product.multi.customer.fields.name.type'

    _columns = {
        'pmcfn_id': fields.one2many('product.multi.customer.fields.name', 'type_id', 'Type'),
        'name': fields.char('Type Name', size=256, ),
        'description': fields.text('Description', ),
    }

product_multi_customer_fields_name_type()


class product_multi_customer_fields_name(osv.osv):
    _rec_name = 'label'
    _name = 'product.multi.customer.fields.name'

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'label': fields.char('Label', size=512, required=True),

        'export_label': fields.char('Export Label', size=512, required=True),

        'formula_id': fields.char('Formula ID', size=512, help="This value can use in the formulas"),
        'name_id': fields.many2one('product.multi.customer.names', 'Name', ondelete='cascade', required=True),
        'customer_id': fields.many2one('res.partner', 'Customers', ondelete='cascade', required=False),
        'default_value': fields.char('Default Value', size=512, required=False, help="Default field - enter the value to make it default (the same value will be applied to all the products)"),
        'for_all': fields.integer('For all Customers', readonly=True),

        'expand_by_customer': fields.boolean('Expand by customers'),

        'expand_ring_sizes': fields.boolean('Expand by ring sizes'),
        'dynamic_field': fields.boolean('Dynamic field'),

        'system_field': fields.boolean('System field'),

        'property_category_id': fields.many2many('product.multi.customer.property.category', 'product_multi_customer_property_category_field', 'field_id', 'property_category_id', 'Property Categories'),
        'product_category_id': fields.many2many('product.category', 'product_multi_customer_product_category_field', 'field_id', 'product_category_id', 'Product Categories'),
        'product_uom_id': fields.many2one('product.uom', 'UOM', required=False),
        'fill_instruction': fields.char('Fill Instruction', size=512, required=False, help="Enter the instructions to fill this customer field"),
        'required': fields.boolean('Required'),

        'dict_values': fields.text('Dictionary Values'),

        'composite': fields.text('Composite Value', required=False, help="Composite Value - enter a formula that will be used to calculate value from product attributes (every product will have it's own calculated value)"),

        # Fake fields used to implement the placeholder assistant
        'model_object_field': fields.many2one('product.multi.customer.names', string="Field", # domain="[('prod_def','in',(1,3))]",
                                              help="Select target field from the related document model.\n"
                                                   "If it is a relationship field you will be able to select "
                                                   "a target field at the destination of the relationship."),

        'copyvalue': fields.char('Expression', size=256, help="Final placeholder expression, to be copy-pasted in the desired template field."),

        'category_composite': fields.many2many('product.multi.customer.composite.category', 'product_multi_customer_compisite_product', 'field_id', 'composite_id', 'Composite Value by Product Category'),
        'labels_in_importing_file': fields.many2many('product.multi.customer.file.label', 'product_multi_customer_file_label_field', 'field_id', 'label_id', 'Labels in importing file', help='List of file headers (column names) that will be mapped on this customer field in the Import Wizard.'),

        'show_on_product_catalog': fields.boolean('Show on product catalog'),
        'show_length_on_product_catalog': fields.boolean('Show length indicator on product catalog'),
        'type_id': fields.many2one('product.multi.customer.fields.name.type', 'Type', ondelete='cascade', ),
        'show_on_lightning_attrs_tab': fields.boolean('Show on lightning attributes tab'),
    }

    _defaults = {
        'formula_id': '',
        'for_all': 0,
        'expand_ring_sizes': False,
        'expand_by_customer': True,
        'system_field': False,
        'show_on_product_catalog': False
    }

    _sql_constraints = [
        ('key_uniq', 'unique(label,customer_id)', 'The key must be unique !'),
        ('formula_uniq', 'unique(formula_id,customer_id)', 'Customer should not have several fields with same "formula id" !'),
        ('type_id_uniq', 'unique(type_id,customer_id)', 'The type must be unique !'),
    ]

    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        for field in self.browse(cr, uid, ids, context=context):
            if field.name_id.prod_def != Setting.SYSTEM_FIELD:
                super(product_multi_customer_fields_name, self).unlink(cr, uid, field.id, context=context)
            else:
                raise osv.except_osv(_('Error'), _('Have no permissions to delete system field'))

    def create(self, cr, uid, vals, context=None):
        self.validate_lightning_attribute_flag(cr, uid, None, vals, context=context)
        if 'formula_id' in vals:
            vals['formula_id'] = vals['formula_id'].strip() if isinstance(vals['formula_id'], (str, unicode)) else vals['formula_id']

        if(vals['customer_id'] == False and vals['expand_by_customer'] == True):
            customer_ids = self.pool.get('res.partner').search(cr, uid, [], context=context)
            fields_ids = []
            vals['for_all'] = 1
            for customer_id in customer_ids:
                vals['customer_id'] = customer_id
                fields_ids.append(super(product_multi_customer_fields_name, self).create(cr, uid, vals, context=context))
        else:
            fields_ids = [super(product_multi_customer_fields_name, self).create(cr, uid, vals, context=context)]

        return fields_ids.pop()

    def write(self, cr, uid, ids, vals, context=None):
        self.validate_lightning_attribute_flag(cr, uid, ids, vals, context=context)
        if 'formula_id' in vals:
            vals['formula_id'] = vals['formula_id'].strip() if isinstance(vals['formula_id'], (str, unicode)) else vals['formula_id']
        return super(product_multi_customer_fields_name, self).write(cr, uid, ids, vals, context=context)

    def validate_lightning_attribute_flag(self, cr, uid, ids, vals, context=None):
        lightning = vals.get('show_on_lightning_attrs_tab')
        customer = vals.get('customer_id')
        ringsize = vals.get('expand_ring_sizes')
        fields_to_get = []

        if (lightning == None):
            fields_to_get.append('show_on_lightning_attrs_tab')
        if (customer == None):
            fields_to_get.append('customer_id')
        if (ringsize == None):
            fields_to_get.append('expand_ring_sizes')
        if (lightning == None and customer == None and ringsize == None):
            return

        def check_rule(lightning, customer, ringsize):
            if lightning and (customer or ringsize):
                raise osv.except_osv('Validation Error', '"Show On Lightning Attributes Tab" option is available only for customer field without customer and without option expand by ring size')

        if ids == None or not fields_to_get:
            check_rule(lightning, customer, ringsize)
        else:
            ids = ids if isinstance(ids, (list, tuple)) else [ids]
            result = self.read(cr, uid, ids, fields_to_get, context=context)
            for item in result:
                lightning = item.get('show_on_lightning_attrs_tab') if lightning is None else lightning
                customer = item.get('customer_id') if customer is None else customer
                ringsize = item.get('expand_ring_sizes') if ringsize is None else ringsize
                check_rule(lightning, customer, ringsize)

    def get_all_records(self, cr, uid, expand_ring_sizes=None, dynamic_field=None, context=None):
        if not context:
            context = {}

        fields = []
        domain = []
        if expand_ring_sizes == True or expand_ring_sizes == False:
            domain.append(('expand_ring_sizes', '=', expand_ring_sizes))

        if dynamic_field == True or dynamic_field == False:
            domain.append(('dynamic_field', '=', dynamic_field))

        if context.get('customer_id', False):
            domain.append(('customer_id', 'in', [False, context.get('customer_id')]))

        context_fields_ids = context.get('fields_ids', False)
        if type(context_fields_ids) is list:
            domain.append(('id', 'in', context_fields_ids))
        fields_ids = self.search(cr, uid, domain)
        if not fields_ids:
            return fields
        # get composite data
        cr.execute(
            """SELECT rel.field_id, pc.id, pmccc.composite
                from product_multi_customer_compisite_product rel
                left join product_multi_customer_composite_category pmccc on rel.composite_id = pmccc.id
                left join product_category pc on pmccc.category = pc.id
                where rel.field_id in %s
            """, (tuple(fields_ids),)
        )
        composite_res = cr.dictfetchall()
        composite_dict = {}
        for composite_line in composite_res:
            if composite_line['field_id'] not in composite_dict:
                composite_dict[composite_line['field_id']] = []
            composite_dict[composite_line['field_id']].append({
                'id': composite_line['id'],
                'composite': composite_line['composite']
            })
        # get fields data
        cr.execute(
            """SELECT fn.id, name, default_value, composite, expand_ring_sizes, expand_by_customer, dynamic_field, customer_id, coalesce(fn.write_date, fn.create_date) as write_date
                from product_multi_customer_fields_name fn
                left join product_multi_customer_names n on fn.name_id = n.id
                where fn.id in %s
            """, (tuple(fields_ids),)
        )
        res = cr.dictfetchall()
        for field in res:
            fields.append(
                {
                    "id": field['id'],
                    "name": field['name'],
                    "value": field['default_value'],
                    "composite": field['composite'],
                    "expand_ring_sizes": field['expand_ring_sizes'],
                    "expand_by_customer": field['expand_by_customer'],
                    "dynamic_field": field['dynamic_field'],
                    "customer_id": field['customer_id'],
                    "category_composite": composite_dict.get(field['id'], []),
                    "write_date": field['write_date'],
                }
            )

        return fields

    def onchange_model_object_value_field(self, cr, uid, ids, model_object_field, context=None):
        result = {
            'copyvalue': False,
        }

        if model_object_field:
            field_value = self.pool.get('product.multi.customer.names').browse(cr, uid, model_object_field, context=context)

            if field_value:
                field_name = field_value.name

                result.update({
                    'copyvalue': "${%s}" % (field_name),
                })

        return {'value': result}

product_multi_customer_fields_name()


class product_multi_customer_fields(osv.osv):
    _description = ''
    _name = 'product.multi.customer.fields'
    _columns = {
        'product_id': fields.many2one('product.product', 'Parent Product', ondelete='cascade', required=True),
        'field_name_id': fields.many2one('product.multi.customer.fields.name', 'Name', ondelete='cascade', required=True),
        'value': fields.text('Value'),
        'partner_id': fields.related('field_name_id', 'customer_id', type="many2one", relation="res.partner", string="Customer", store=True),
        'size_id': fields.many2one('ring.size', 'Ring size'),
        'need_to_update': fields.boolean('Need to Update'),
        'show_on_lightning_attrs_tab': fields.related('field_name_id', 'show_on_lightning_attrs_tab', type='boolean', rel='product.multi.customer.fields.name', string="Show on lightning attributes tab", store=False),
        'check':fields.boolean('Boolean Field'),
        'end_date':fields.datetime('End Date'),
    }

    _order = 'product_id, partner_id, field_name_id, size_id'

    def name_get(self, cr, uid, ids, context=None):
        res = {}

        if ids:
            cr.execute("""
                SELECT
                    ff.id,
                    concat(pp.default_code, '/', coalesce(rs.name, '0'), '|', rp.ref, '|', fn.label ) as name
                FROM product_multi_customer_fields ff
                    left join product_multi_customer_fields_name fn on ff.field_name_id = fn.id
                    left join product_product pp on pp.id = ff.product_id
                    left join ring_size rs on rs.id = ff.size_id
                    left join res_partner rp on rp.id = ff.partner_id
                where 1=1
                    and ff.id in %s
                """, (tuple(ids), )
            )
            res = cr.fetchall() or []

        return res

    # def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
    #     res = super(product_multi_customer_fields, self).read(cr, user, ids, fields=fields, context=context, load=load)
    #     return res

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if args:
            return super(product_multi_customer_fields, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []

    def _get_delmar_customer_sku_seq(self, cr, uid, customer_ref):
        seq_obj = False
        if customer_ref != '':

            if customer_ref.lower() == 'wmt':
                customer_seq_name = 'walmart_sku'
            else:
                customer_seq_name = 'multi.customer.delmar.sku.' + customer_ref.lower()

            if not self.pool.get('ir.sequence').search(cr, uid, [('name', '=', customer_seq_name)]):
                seq = {
                    'name': customer_seq_name,
                    'implementation': 'standard',
                    'prefix': customer_ref.upper(),
                    'padding': 9 - len(customer_ref),
                    'number_increment': 1,
                    'number_next': 1,
                }

                self.pool.get('ir.sequence').create(cr, uid, seq)
            seq_ids = self.pool.get('ir.sequence').search(cr, uid, [('name', '=', customer_seq_name)])
            if seq_ids:
                seq_obj = self.pool.get('ir.sequence').browse(cr, uid, seq_ids[0])
        return seq_obj


    def generate_delmar_customer_sku(self, cr, uid, ids=None, customer_ref="", str_ids="", base_line=False):
        customer_field_label = 'Customer ID Delmar'

        ids = ids or len(str_ids) != 0 and str_ids.split(',')

        if not ids:
            return False

        seq_obj = self._get_delmar_customer_sku_seq(cr, uid, customer_ref)

        if seq_obj:
            seq_name = "ir_sequence_" + str(seq_obj.id)
            padding = 'fm' + '0' * int(seq_obj.padding)
            cr.execute("""SELECT
                customer_fields_name.id customer_names_id,
                res_partner.id res_partner_id,
                res_partner.ref res_partner_ref,
                customer_fields_name.expand_ring_sizes
            FROM
                product_multi_customer_fields_name customer_fields_name
                LEFT JOIN res_partner ON res_partner.id = customer_fields_name.customer_id
            WHERE
                customer_fields_name.label = %s
                AND res_partner.ref = %s
                AND res_partner.id != 201""", (customer_field_label, customer_ref))
            # 201 because IC2 have two customer
            field = cr.fetchone()

            if field:

                sql_params = {
                    'uid': uid,
                    'field_name_id': field[0],
                    'partner_id': field[1],
                    'prefix': seq_obj.prefix,
                    'seq_name': seq_name,
                    'padding': padding,
                    'customer_field_label': customer_field_label,
                    'ids': tuple(ids)
                }

                # Insert base line
                cr.execute("""INSERT INTO
                        product_multi_customer_fields (create_uid, create_date, size_id, field_name_id, partner_id, product_id, "value")
                    SELECT
                        %(uid)s, (now() at time zone 'UTC'), NULL, %(field_name_id)s, %(partner_id)s, pp.id, %(prefix)s || to_char(nextval(%(seq_name)s), %(padding)s)
                    FROM
                        product_product pp
                        LEFT JOIN res_partner res ON res.id = %(partner_id)s
                        LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                        LEFT JOIN product_multi_customer_fields_name flds_name ON flds_name.label IN (%(customer_field_label)s) AND flds_name.customer_id = res.id
                        LEFT JOIN product_multi_customer_fields flds ON pp.id = flds.product_id AND flds.field_name_id = flds_name.id AND flds.size_id IS NULL
                    WHERE
                        pp.id IN %(ids)s
                        AND flds.value IS NULL
                """, sql_params)

                # insert for sizes value if expand by size is true
                if field[3] and not base_line:
                   cr.execute("""INSERT INTO
                            product_multi_customer_fields (create_uid, create_date, size_id, field_name_id, partner_id, product_id, "value")
                        SELECT
                            %(uid)s, (now() at time zone 'UTC'), prs.size_id, %(field_name_id)s, %(partner_id)s, pp.id,
                            base_sku || CASE WHEN prs.size_id IS NULL THEN '' ELSE '-' || substring('00' || cast(rs.name as float)*100, length(rs.name)-1, 4) END
                        FROM
                            (
                                SELECT
                                    pp.id,
                                    pp.product_tmpl_id,
                                    flds.value base_sku
                                FROM
                                    product_product pp
                                    LEFT JOIN res_partner res ON res.id = %(partner_id)s
                                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                                    LEFT JOIN product_multi_customer_fields_name flds_name ON flds_name.label IN (%(customer_field_label)s) AND flds_name.customer_id = res.id
                                    LEFT JOIN product_multi_customer_fields flds ON pp.id = flds.product_id AND flds.field_name_id = flds_name.id AND flds.size_id IS NULL
                                WHERE
                                    pp.id IN %(ids)s
                                    AND flds.value IS NOT NULL
                            ) pp
                            LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                            LEFT JOIN product_ring_size_default_rel prs ON prs.product_id = pt.id
                            LEFT JOIN product_multi_customer_fields_name flds_name ON flds_name.label = %(customer_field_label)s AND flds_name.customer_id = %(partner_id)s
                            LEFT JOIN product_multi_customer_fields flds ON pp.id = flds.product_id AND flds.field_name_id = flds_name.id AND (prs.size_id = flds.size_id OR (prs.size_id IS NULL AND flds.size_id IS NULL))
                            LEFT JOIN ring_size rs ON prs.size_id = rs.id
                        WHERE flds.value IS NULL""", sql_params)

        return True

    def create(self, cr, uid, vals, context=None):
        if ('product_id' in vals):
            product = self.pool.get('product.product').browse(cr, uid, vals['product_id'])
            product_sizeable = product.sizeable and product.ring_size_ids or False

            if not product_sizeable:
                vals.update({
                    'check': True,
                    'size_id': None
                })
        return super(product_multi_customer_fields, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if ('partner_id' in vals):
            prev_partner = self.browse(cr, uid, ids[0]).partner_id
            prev_partner_name = prev_partner and prev_partner.name or '<no customer>'

            new_partner_name = '<no customer>'
            if vals['partner_id']:
                new_partner = self.pool.get('res.partner').browse(cr, uid, vals['partner_id'])
                if new_partner:
                    new_partner_name = new_partner.name
            if (prev_partner and prev_partner.id):
                raise osv.except_osv(
                    _('Error'),
                    _(
                        'You can not change partner {0} to {1} after creation'.format(
                            prev_partner_name,
                            new_partner_name
                        )
                    )
                )
        if ('product_id' in vals):
            product = self.pool.get('product.product').browse(cr, uid, vals['product_id'])
            product_sizeable = product.sizeable and product.ring_size_ids or False

            if not product_sizeable:
                vals['check'] = True
            else:
                vals['check'] = False
        return super(product_multi_customer_fields, self).write(cr, uid, ids, vals, context=context)


    def phantomInventoryEndDateSolution(self, cr, uid, ids, context=None):
        currentDate = datetime.datetime.today()
        currentTempDate = str(currentDate)[0:10] + ' 00:00:00'
        phantomIDs = self.search(cr,uid,[
            ('field_name_id.label', '=', 'Phantom Inventory'), '!', ('end_date', '=', False)
                ])

        for tempID in phantomIDs:
            try:
                vals = {'value':'0'}
                phantomObjects = self.browse(cr,uid,tempID)
                phantomEndDate = phantomObjects.end_date
                tempEndDate = str(phantomEndDate)[0:10]
                finalCurrentDate = datetime.datetime.strptime(str(currentDate)[0:10], '%Y-%m-%d')
                endDate = datetime.datetime.strptime(str(tempEndDate), '%Y-%m-%d')
                if finalCurrentDate >= endDate:
                    phantomObjects.value = '0'
                    vals = {'value': phantomObjects.value}
                    self.write(cr, uid, tempID, vals, context=context)         
            except:
                continue



    def onchange_product_id(self, cr, uid, ids, product_id=None, size_id=None, context={}):
        res = {'value': {'check': False}}
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        product_sizeable = product.sizeable and product.ring_size_ids or False

        if not product_sizeable:
            res = {'value': {'check': True}}
        else:
            res = {'value': {'check': False}}
        return res

product_multi_customer_fields()


class product_multi_customer_additional_fields(osv.osv):
    _rec_name = 'name_id'
    _name = 'product.multi.customer.additional.fields'
    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name_id': fields.many2one('product.multi.customer.names', 'Name', ondelete='cascade', required=True),
        'value': fields.char('Value', size=256),
        'product_id': fields.many2one('product.product', 'Parent Product', ondelete='cascade', required=True)
    }

product_multi_customer_additional_fields()


class product_product(osv.osv):

    _factory_cost_formula = None

    _inherit = 'product.product'

    def _get_qty_by_ring_size(self, cr, uid, ids, name, args, context=None):
        res = {}
        models_data = self.pool.get('ir.model.data')
        m_field = models_data.get_object_reference(cr, uid, 'multi_customer', 'qty_by_ring_size')

        for prod_id in ids:
            ring_size_fields = self.pool.get('product.multi.customer.fields').search(cr, uid, [('product_id', '=', prod_id), ('field_name_id', '=', m_field[1])])
            res[prod_id] = ring_size_fields

        return res

    def _get_none(self, cr, uid, ids, name, args, context=None):
        return {x: False for x in ids}

    def _select_partners(self, cr, uid, obj, name, args, context):
        cr.execute('select name, id from res_partner order by name')
        res_list = cr.fetchall()
        res = []
        for elem in res_list:
            res.append((elem[0], (str)(elem[0]) + ':' + elem[1]))
        return tuple(res)

    _columns = {
        'search_partner_id': fields.function(_get_none, string='Customers', type='many2one', relation='res.partner', readonly=False,  store=False),
        'search_str': fields.function(_get_none, string='Search', type='char', readonly=False,  store=False),
        'multi_customer_fields': fields.one2many('product.multi.customer.fields', 'product_id', 'Customers fields', help='List of products that are part of this pack.'),
        'qty_by_ring_size': fields.function(_get_qty_by_ring_size, string="Qty by ring size", type='one2many',  obj='product.multi.customer.fields', method=True, store=False),
        'multi_customer_additional_fields': fields.one2many('product.multi.customer.additional.fields', 'product_id', 'Additional fields', help=''),
        'hash_customer_fields': fields.text(),
        'hash_customer_additional_fields': fields.text(),
        'hash_ring_sizes': fields.text(),
        'hash_expand_ring_sizes_fields': fields.text(),
    }

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        res = super(product_product, self).default_get(cr, uid, fields_list, context=context)
        if context.get('def_product_id', False):
            def_product_id = self.read(cr, uid, [context.get('def_product_id')], fields=['multi_customer_fields'])[0]
            res.update(def_product_id)

        return res

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        # print "Main"
        if not context:
            context = {}
        if fields and ('multi_customer_fields' in fields or 'multi_customer_additional_fields' in fields):
            if context.get('form_view_ref', False) == 'filling_task.filling_task_product_fields_form' and context.get('task_id', False):
                task = self.pool.get('filling.task').browse(cr, uid, context.get('task_id'))
                context.update({'fields_ids': [field.id for field in task.customer_fields]})
            if context.get('update_customer_fields', True):
                # import pdb; pdb.set_trace()
                ### print('Read!')
                ### ttt = datetime.datetime.utcnow()
                pp_fields = [
                    'id',
                    'sizeable',
                    'categ_id',
                    'ring_size_ids',
                    'hash_customer_fields',
                    'hash_customer_additional_fields',
                    'hash_ring_sizes',
                    'hash_expand_ring_sizes_fields',
                ]
                res = super(product_product, self).read(cr, uid, ids, fields=pp_fields, context=context, load=load)
                ### print('Generation!')
                ### print (datetime.datetime.utcnow() - ttt).total_seconds()
                ### ttt = datetime.datetime.utcnow()
                self.generate_customer_fields(cr, uid, ids, res, context=context)
                ### print('End!')
                ### print (datetime.datetime.utcnow() - ttt).total_seconds()
                ### ttt = datetime.datetime.utcnow()
                if type(ids) is list and len(ids)>1:
                    return res
                else:
                    new_cr = self.pool.db.cursor()
                    res = super(product_product, self).read(new_cr, uid, ids, fields=fields, context=context, load=load)
                    new_cr.close()
                    return res

        res = super(product_product, self).read(cr, uid, ids, fields=fields, context=context, load=load)
        return res

    def generate_customer_fields(self, cr, uid, ids, vals=None, context=None):
        ### ttt = datetime.datetime.utcnow()
        if not context:
            context = {}
        if not vals:
            vals = []
        new_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        all_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=False, dynamic_field=False)
        all_fields_ids = set([cfn['id'] for cfn in all_fields])
        progress = 0
        export_obj = self.pool.get('export.extract')
        ### print "All fields get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()
        if context.get('customer_id', False) or type(context.get('fields_ids', False)) is list:
            static_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=False, dynamic_field=False, context=context)
            static_fields_ids = set([cfn['id'] for cfn in static_fields])
        else:
            static_fields = all_fields
            static_fields_ids = all_fields_ids

        ### print "Static get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()

        dynamic_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=False, dynamic_field=True, context=context)

        ### print "Dynamic get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()

        all_expand_ring_sizes_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=True, dynamic_field=False)
        all_expand_ring_sizes_fields_ids = set([cfn['id'] for cfn in all_expand_ring_sizes_fields])

        ### print "All Expand get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()

        if context.get('customer_id', False) or type(context.get('fields_ids', False)) is list:
            static_expand_ring_sizes_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=True, dynamic_field=False, context=context)
            static_expand_ring_sizes_fields_ids = set([cfn['id'] for cfn in static_expand_ring_sizes_fields])
        else:
            static_expand_ring_sizes_fields = all_expand_ring_sizes_fields
            static_expand_ring_sizes_fields_ids = all_expand_ring_sizes_fields_ids
        ### print "Static expand get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()

        dynamic_expand_ring_sizes_fields = self.pool.get('product.multi.customer.fields.name').get_all_records(cr, uid, expand_ring_sizes=True, dynamic_field=True, context=context)

        ### print "Dynamic expand get:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()
        ### ttt = datetime.datetime.utcnow()

        # additional_field_ids = self.pool.get('product.multi.customer.names').get_all_records(cr, uid, context=context)
        # hash_additional_fields = hashlib.md5(json.dumps(additional_field_ids, sort_keys=True)).hexdigest()

        i = 0
        total = len(vals)
        ### itt = datetime.datetime.utcnow()
        db = self.pool.db
        for prod in vals:
            step_cr = db.cursor()
            i += 1
            _logger.info('Generate customer fields [%s/%s]' % (i, total))
            ### print i
            ### print (datetime.datetime.utcnow() - itt).total_seconds()
            ### itt = datetime.datetime.utcnow()
            write_vals = {}
            prod_id = prod.get('id')

            prod['obj.categ_name'] = None
            if prod.get('categ_id') and isinstance(prod['categ_id'], tuple):
                prod['obj.categ_name'] = prod['categ_id'][1]
                if isinstance(prod['obj.categ_name'], unicode):
                    prod['obj.categ_name'] = prod['obj.categ_name'].encode('utf-8')
                    if '/ Ring' in prod['obj.categ_name']:
                        prod['obj.categ_name'] = 'Ring'
            try:
                # Additional fields
                # if prod.get('hash_customer_additional_fields', "") != hash_additional_fields:
                #     self._generate_additional_fields(step_cr, uid, prod, additional_field_ids, context=context)
                #     write_vals.update({'hash_customer_additional_fields': hash_additional_fields})

                # Customer fields
                old_hash_fields = prod.get('hash_customer_fields', "") or ""
                old_hash_fields_time = old_hash_fields.find('/') > -1 and old_hash_fields[old_hash_fields.find('/')+1:] or False
                old_hash_fields = old_hash_fields_time and old_hash_fields[:old_hash_fields.find('/')] or old_hash_fields
                try:
                    old_all_fields_ids = set([int(x) for x in old_hash_fields.split(",")])
                except ValueError:
                    old_all_fields_ids = set([])
                if old_hash_fields_time:
                    changed_all_fields_ids = set([cfn['id'] for cfn in all_fields if cfn['write_date'] > old_hash_fields_time])
                else:
                    changed_all_fields_ids = all_fields_ids
                new_hash_fields_ids = (old_all_fields_ids & all_fields_ids) - changed_all_fields_ids
                regenerate_all_fields_ids = static_fields_ids - new_hash_fields_ids
                regenerate_all_fields = [cfn for cfn in static_fields if cfn['id'] in regenerate_all_fields_ids]

                if regenerate_all_fields:
                    generate_res = self._generate_customer_fields(step_cr, uid, prod, regenerate_all_fields, context=context)
                    if generate_res:
                        hash_fields = str(sorted(new_hash_fields_ids | regenerate_all_fields_ids))[1:-1].replace(' ', '') + '/' + new_time
                        write_vals.update({'hash_customer_fields': hash_fields})

                self._generate_customer_fields(step_cr, uid, prod, dynamic_fields, context=context)

                # Expand ring sizes
                old_hash_expand_ring_sizes_fields = prod.get('hash_expand_ring_sizes_fields', "") or ""
                old_hash_expand_ring_sizes_fields_time = old_hash_expand_ring_sizes_fields.find('/') > -1 and old_hash_expand_ring_sizes_fields[old_hash_expand_ring_sizes_fields.find('/')+1:] or False
                old_hash_expand_ring_sizes_fields = old_hash_expand_ring_sizes_fields_time and old_hash_expand_ring_sizes_fields[:old_hash_expand_ring_sizes_fields.find('/')] or old_hash_expand_ring_sizes_fields
                try:
                    old_all_expand_ring_sizes_fields_ids = set([int(x) for x in old_hash_expand_ring_sizes_fields.split(",")])
                except ValueError:
                    old_all_expand_ring_sizes_fields_ids = set([])
                if old_hash_expand_ring_sizes_fields_time:
                    changed_all_expand_ring_sizes_fields_ids = set([cfn['id'] for cfn in all_expand_ring_sizes_fields if cfn['write_date'] > old_hash_expand_ring_sizes_fields_time])
                else:
                    changed_all_expand_ring_sizes_fields_ids = all_expand_ring_sizes_fields_ids
                new_hash_expand_ring_sizes_fields_ids = (old_all_expand_ring_sizes_fields_ids & all_expand_ring_sizes_fields_ids) - changed_all_expand_ring_sizes_fields_ids
                regenerate_all_expand_ring_sizes_fields_ids = static_expand_ring_sizes_fields_ids - new_hash_expand_ring_sizes_fields_ids
                regenerate_all_expand_ring_sizes_fields = [cfn for cfn in static_expand_ring_sizes_fields if cfn['id'] in regenerate_all_expand_ring_sizes_fields_ids]
                hash_ring_sizes = hashlib.md5(json.dumps(prod.get('ring_size_ids'), sort_keys=True)).hexdigest()
                size_range = context.get('size_range', False)
                _logger.info("SIZE_RANGE: %s" % size_range)
                _logger.info("CONTEXT: %s" % context)
                if (prod.get('hash_ring_sizes') != hash_ring_sizes or size_range):
                    if static_expand_ring_sizes_fields:
                        generate_res = self._generate_size_dependent_fields(step_cr, uid, prod, field_ids=static_expand_ring_sizes_fields, context=context)
                        _logger.info("GENERATED FOR HASH OR RANGE: %s" % size_range)
                        if not size_range:
                            if generate_res:
                                hash_expand_ring_sizes_fields = str(sorted(static_expand_ring_sizes_fields_ids))[1:-1].replace(' ', '') + '/' + new_time
                                write_vals.update({
                                    'hash_ring_sizes': hash_ring_sizes,
                                    'hash_expand_ring_sizes_fields': hash_expand_ring_sizes_fields,
                                })
                elif regenerate_all_expand_ring_sizes_fields:
                    generate_res = self._generate_size_dependent_fields(step_cr, uid, prod, field_ids=regenerate_all_expand_ring_sizes_fields, context=context)
                    if generate_res:
                        hash_expand_ring_sizes_fields = str(sorted(regenerate_all_expand_ring_sizes_fields_ids | new_hash_expand_ring_sizes_fields_ids))[1:-1].replace(' ', '') + '/' + new_time
                        write_vals.update({'hash_expand_ring_sizes_fields': hash_expand_ring_sizes_fields})
                self._generate_size_dependent_fields(step_cr, uid, prod, field_ids=dynamic_expand_ring_sizes_fields, context=context)
                if write_vals:
                    super(product_product, self).write(step_cr, uid, prod_id, write_vals)
                if context.get('export_process_id') and progress != int(i * 100.0 / len(vals)):
                    progress = int(i * 100.0 / len(vals))
                    export_vals = {'progress': progress * 0.5}
                    if progress == 100:
                        export_vals.update(status='Build csv')
                    export_obj.write(cr, uid, [context.get('export_process_id')], export_vals)
                    cr.commit()
                step_cr.commit()
            except Exception, e:
                _logger.info('Failed generation fields for product id = %s\n' % (prod_id,))
                _logger.error("Exception:\n{0}".format(e.message))
            finally:
                step_cr.close()
        ### print "Only generation:"
        ### print (datetime.datetime.utcnow() - ttt).total_seconds()

    def _generate_additional_fields(self, cr, uid, prod=None, field_ids=None, context=None):
        """
            Update additional fields
        """
        if not context:
            context = {}
        if not field_ids:
            field_ids = []
        if not prod:
            prod = {}

        prod_id = prod.get('id', False)
        if not prod_id:
            return True

        p_add_fields = self.pool.get('product.multi.customer.additional.fields')

        for field in field_ids:
            value_field = p_add_fields.search(cr, uid, ['&', ('product_id', '=', prod_id), ('name_id', '=', field['id'])])

            if not value_field:
                value = self.get_additional_field_value(cr, uid, prod, field, context=context)

                fields_vals = {
                    "name_id": field['id'],
                    "product_id": prod_id,
                    "value": value,
                }
                p_add_fields.create(cr, uid, fields_vals, context=context)

    def _generate_customer_fields(self, cr, uid, prod=None, field_ids=None, context=None):
        """
            Customer fields
        """
        if not context:
            context = {}
        if not field_ids:
            field_ids = []
        if not prod:
            prod = {}

        res = True

        prod_id = prod.get('id', False)
        if not prod_id:
            return res

        p_fields = self.pool.get("product.multi.customer.fields")
        for field in field_ids:
            its_pricelist_value = self._check_pricelist_field(field)
            size_specific_ids = p_fields.search(cr, uid, ['&', '&', ('product_id', '=', prod_id), ('field_name_id', '=', field['id']), ('size_id', '!=', None)])
            p_fields.unlink(cr, uid, size_specific_ids)

            value_field = p_fields.search(cr, uid, ['&', ('product_id', '=', prod_id), ('field_name_id', '=', field['id'])])
            if not value_field:

                fields_vals = {
                    "product_id": prod_id,
                    "field_name_id": field['id']
                }
                cm_field_id = p_fields.create(cr, uid, fields_vals, context=context)

                if (its_pricelist_value):
                    value = self._get_pricelist_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)
                else:
                    value = self.get_customer_field_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)

                if value is None:
                    p_fields.unlink(
                        cr, uid, cm_field_id, context=context)
                    res = False
                    continue

                fields_vals = {'value': value}
                p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)

            elif field['dynamic_field'] and (field.get('composite', False) or field.get('default_value', False)):
                for cm_field_id in value_field:
                    if (its_pricelist_value):
                        value = self._get_pricelist_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)
                    else:
                        value = self.get_customer_field_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)

                    if value is None:
                        p_fields.unlink(
                            cr, uid, cm_field_id, context=context)
                        res = False
                        continue

                    fields_vals = {'value': value}
                    p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)

        return res

    def get_option_sizes(self, cr, uid, customer_id=None, product_id=None):
        res_ids = []
        field_name_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, ['&', ('label', '=', 'Options'), ('customer_id', '=', customer_id)])
        _logger.info("Fields ids: %s" % field_name_ids)
        if not (field_name_ids and field_name_ids[0]):
            return res_ids

        field_ids = self.pool.get('product.multi.customer.fields').search(cr, uid, ['&', ('field_name_id', '=', field_name_ids[0]), ('product_id', '=', product_id)])
        if not (field_ids and field_ids[0]):
            return res_ids

        field = self.pool.get('product.multi.customer.fields').browse(cr, uid, field_ids[0])
        size_ids = self.pool.get('ring.size').search(cr, uid, [('name', 'in', [str(float(x)) for x in field.value.split(",")])])
        if size_ids:
            res_ids += size_ids
        _logger.info("Sizes to add: %s" % res_ids)
        return res_ids

    def _generate_size_dependent_fields(self, cr, uid, prod=None, field_ids=None, context=None):
        """
           Size dependent
        """

        if not context:
            context = {}
        if not field_ids:
            field_ids = []
        if not prod:
            prod = {}

        res = True

        prod_id = prod.get('id', False)
        if not prod_id:
            return res

        p_fields = self.pool.get("product.multi.customer.fields")
        if prod['sizeable']:
            _logger.info("SIZEABLE, fileds: %s" % field_ids)
            for field in field_ids:
                its_pricelist_value = self._check_pricelist_field(field)
                full_size_id_arr = prod['ring_size_ids'] + [False]
                size_id_arr = full_size_id_arr[:]
                _logger.info("CONTEXT: %s" % context)
                if context.get('size_range'):
                    _logger.info('USE SIZE_RANGE')
                    size_id_arr = context['size_range'] + [False]
                    _logger.info("SIZE ID ARR: %s" % size_id_arr)
                    full_size_id_arr += size_id_arr
                elif context.get('use_option_sizes', False):
                    _logger.info('USE OPTION SIZES')
                    size_id_arr = self.get_option_sizes(cr, uid, context.get('customer_id'), prod_id) + [False]
                    _logger.info("SIZE ID ARR: %s" % size_id_arr)
                    full_size_id_arr += size_id_arr
                elif not context.get('expand') and context.get('base_line'):
                    size_id_arr = [False]
                # TODO
                # WTF: Need clarify what is it: feature or bug.
                # All values for sizes out of range will be removed.
                on_remove_ids = p_fields.search(
                    cr,
                    uid,
                    [
                        ('product_id', '=', prod_id),
                        ('field_name_id', '=', field['id']),
                        ('size_id', 'not in', full_size_id_arr),
                    ]
                )

                p_fields.unlink(cr, uid, on_remove_ids)

                for size_id in size_id_arr:
                    value_ids = p_fields.search(
                        cr,
                        uid,
                        [
                            ('product_id', '=', prod_id),
                            ('field_name_id', '=', field['id']),
                            ('size_id', '=', size_id),
                        ]
                    )

                    if not value_ids:
                        fields_vals = {
                           "product_id": prod_id,
                           "field_name_id": field['id'],
                           "size_id": size_id,
                        }
                        cm_field_id = p_fields.create(cr, uid, fields_vals, context=context)
                        if (its_pricelist_value):
                            value = self._get_pricelist_value(cr, uid, prod, field, size_id=size_id, cm_field_id=cm_field_id, context=context)
                        else:
                            value = self.get_customer_field_value(cr, uid, prod, field, size_id=size_id, cm_field_id=cm_field_id, context=context)

                        if value is None:
                            p_fields.unlink(
                                cr, uid, cm_field_id, context=context)
                            res = False
                            continue

                        fields_vals = {'value': value}
                        p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)

                    elif field['dynamic_field'] and (field.get('composite', False) or field.get('default_value', False)):
                        for cm_field_id in value_ids:
                            if (its_pricelist_value):
                                value = self._get_pricelist_value(cr, uid, prod, field, size_id=size_id, cm_field_id=cm_field_id, context=context)
                            else:
                                value = self.get_customer_field_value(cr, uid, prod, field, size_id=size_id, cm_field_id=cm_field_id, context=context)

                            if value is None:
                                p_fields.unlink(
                                    cr, uid, cm_field_id, context=context)
                                res = False
                                continue

                            fields_vals = {'value': value}
                            p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)

        else:
            _logger.info("NOT SIZEABLE")
            for field in field_ids:
                its_pricelist_value = self._check_pricelist_field(field)
                value_field = p_fields.search(cr, uid, ['&', ('product_id', '=', prod_id), ('field_name_id', '=', field['id'])])
                if not value_field:

                    fields_vals = {
                        "product_id": prod_id,
                        "field_name_id": field['id']
                    }
                    cm_field_id = p_fields.create(cr, uid, fields_vals, context=context)
                    if (its_pricelist_value):
                        value = self._get_pricelist_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)
                    else:
                        value = self.get_customer_field_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)

                    if value is None:
                        p_fields.unlink(
                            cr, uid, cm_field_id, context=context)
                        res = False
                        continue

                    fields_vals = {'value': value}
                    p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)
                elif field['dynamic_field'] and (field.get('composite', False) or field.get('default_value', False)):
                    for cm_field_id in value_field:
                        if (its_pricelist_value):
                            value = self._get_pricelist_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)
                        else:
                            value = self.get_customer_field_value(cr, uid, prod, field, size_id=False, cm_field_id=cm_field_id, context=context)

                        if value is None:
                            p_fields.unlink(
                                cr, uid, cm_field_id, context=context)
                            res = False
                            continue

                        fields_vals = {'value': value}
                        p_fields.write(cr, uid, cm_field_id, fields_vals, context=context)

        return res

    def get_additional_field_value(self, cr, uid, prod=None, field=None, context=None):
        if not context:
            context = {}
        if not field:
            field = {}
        if not prod:
            prod = {}

        field_name = field['name']
        if field_name:
            field_name = field_name.lower()

        value = ""
        if(field['value'] == False):
            value = prod.get(field_name, "no_value")
            if(value == "no_value"):
                new_read = super(product_product, self).read(cr, uid, prod.get('id'), fields=[field_name], context=context)
                value = new_read.get(field_name, "")
        elif(field['value'] != "empty"):
            value = field['value']

        return value

    def fix_float_arg(self, m):
        try:
            float_val = eval(m.group(2), self.safe_dict)
            float_val = re.sub(r"([^\d\.])", lambda x: '', repr(float_val))
            float_val = float("%05.2f" % float(float_val or 0))
        except (SyntaxError, TypeError, ValueError) as e:
            float_val = e.message

        return repr(float_val) or '0'

    def recursive_fix_float_arg(self, float_str, safe_dict):
        self.safe_dict = safe_dict
        m_ = re.search(r"float\(.*?\)", float_str)
        while m_ and m_.group():
            if not isinstance(float_str, (str, unicode)):
                float_str = repr(float_str)
            float_str = re.sub(r"(float\()([^\(]*?)(\))", self.fix_float_arg, float_str)
            float_str = re.sub(r"^u'(.*?)'$", lambda x: x.group(1), float_str)
            m_ = re.match(r"float\(.*?\)", float_str)

        return str(float_str)

    def get_customer_field_value(self, cr, uid, prod=None, field=None, size_id=False, cm_field_id=False, context=None):
        if not context:
            context = {}
        if not field:
            field = {}
        if not prod:
            prod = {}
        prod_id = prod.get('id', False)

        value = field['value']
        field_name = field['name']
        if field_name:
            field_name = field_name.lower()

        customer_id = field.get('customer_id', False)
        customer_id_for_print = customer_id or context.get('customer_id')

        if(field['value'] == "empty"):
            value = ""
        elif (field['composite'] or len(field['category_composite']) > 0):
            value = field['composite']
            for categ_comp in field['category_composite']:
                if(prod['categ_id'][0] == categ_comp['id']):
                    value = categ_comp['composite']

            if(value):

                builtin_list = re.finditer(r'@(?P<method>\w+)(\((?P<args>.*?)\))?', value)
                for match in builtin_list:
                    group = match.groupdict()
                    res = None
                    if group['method'] == 'upc':
                        res = self._get_upc(cr, uid, customer_id, prod_id, size_id) or ""
                        value = value.replace(match.group(), res)

                    elif group['method'] == 'profit':
                        res = self._get_profit(cr, uid, customer_id, prod_id)
                        value = value.replace(match.group(), res)

                    elif group['method'] == 'factory_cost':
                        res = self._get_factory_cost(cr, uid, prod_id, group.get('args', None))
                        value = value.replace(match.group(), str(res))

                    elif group['method'] == 'fake_factory_cost':
                        import json
                        po = json.loads(group.get('args')).get('po_name')
                        invoice = json.loads(group.get('args')).get('invoice')
                        res = self._get_factory_cost(cr, uid, prod_id, po=po, invoice=invoice)
                        value = value.replace(match.group(), str(res))

                    elif group['method'] == 'invoice_cost':
                        res = self._get_invoice_cost(cr, uid, prod_id, group.get('args', None))
                        value = value.replace(match.group(), str(res))
                        _logger.info('invoice_cost [%s]' % (value))

                    elif group['method'] == 'invc_mould':
                        res = self._get_mould(cr, uid, prod_id, group.get('args'))
                        value = value.replace(match.group(), str(res))
                        _logger.info('invc_mould [%s]' % (value))

                """
                *   Calculate product_multi_customer_method
                *   Replace @method() with corresponding result
                *
                """
                regex = re.compile("(<%=\s*(\w+)(\(.*?\))*\s*%>)", re.MULTILINE)
                methods = regex.findall(value)

                if methods:
                    base_method_cxt = {
                        'product_id': prod_id,
                        'size_id': size_id,
                        'customer_id': customer_id,
                        'field_name_id': field.get('id', False),
                        'field_id': cm_field_id,
                        'args_str': '',
                    }
                    method_obj = self.pool.get('product.multi.customer.method')

                    for method_str, method, args_str in methods:
                        res_str = ''
                        method_cxt = base_method_cxt.copy()
                        if args_str:
                            method_cxt['args_str'] = args_str
                        method_ids = method_obj.search(cr, uid, [('name', '=', method)])
                        if method_ids:
                            res_str = str_encode(method_obj.run(cr, uid, method_ids[0], context=method_cxt))
                        value = value.replace(method_str, res_str)

                """
                *   Calculate multi_customer fields and math operations
                *   Replace @method() with corresponding result
                *
                """
                result = re.finditer(ur"\${([^}\n]+)}", value)
                for match in result:

                    prod_val = ''
                    source_str = match.groups()[0]

                    withOperation = False
                    for i in ('+', '-', '*', '/', '(', ')', 'if', 'else', 'or', 'and'):
                        if(source_str.find(i) != -1):
                            withOperation = True
                            break

                    if(withOperation):
                        """
                        *   Calculate base functions
                        *
                        """
                        res = re.finditer(ur"([A-Za-z]\w+)(?!\w*?\()(?!\w*?\")(?!\w*?')", source_str, re.I)

                        safe_dict = SAFE_DICT.copy()

                        for val in res:
                            if not (val.groups()[0].replace('.', '', 1).isdigit()):
                                tmp = self.get_val_for_name(cr, uid, prod, val.groups()[0], field, size_id=size_id, context=context)
                                safe_dict[val.groups()[0]] = tmp

                        try:
                            formula = self.recursive_fix_float_arg(source_str, safe_dict)
                            prod_val = eval(formula, safe_dict)
                        except Exception, e:
                            message = "customer_id: {customer_id}, product_id: {product_id}, size_id: {size_id}, field_name: {field_name}, composite value: \"\"\" [[ {source} ]] \"\"\"\n{error}".format(
                                field_name=field_name,
                                source=source_str,
                                error=e.message,
                                customer_id=customer_id_for_print,
                                product_id=prod_id,
                                size_id=size_id,
                                )
                            _logger.error(message)
                            prod_val = "eval error"
                            if(getattr(e, 'message', False)):
                                prod_val = e.message
                    else:
                        """
                        *   Calculate multi_customer_fields
                        *
                        """
                        prod_val = self.get_val_for_name(cr, uid, prod, source_str, field, size_id=size_id, context=context)

                    value = value.replace(match.group(), "%s" % (prod_val))

        elif(field['value'] == False):
            expand_ring_sizes = field.get('expand_ring_sizes')
            if (field_name == 'ring_size_ids'):
                if expand_ring_sizes == True and size_id:
                    return self.pool.get('ring.size').browse(cr, uid, size_id, context=context).name
                elif expand_ring_sizes == False:
                    sizes = []
                    if prod['ring_size_ids'] and context.get('size_range'):
                        size_ids = context.get('size_range')
                    else:
                        size_ids = prod['ring_size_ids']
                    for size in self.pool.get('ring.size').browse(cr, uid, size_ids, context=context):
                        sizes.append(size.name)
                    return ', '.join(sizes)
            else:
                value = prod.get(field_name, "no_value")
                if(value == "no_value"):
                    new_read = super(product_product, self).read(cr, uid, prod_id, fields=[field_name], context=context)
                    value = new_read.get(field_name, "")
            # value = prod.get(field_name, "")

        if(isinstance(value, tuple)):
            value = value[1]

        return value

    def _get_upc(self, cr, uid, customer_id, prod_id, size_id=None):
        product_upc_obj = self.pool.get('product.upc')
        try:
            res = product_upc_obj.get_or_generate(
                cr, uid, prod_id, customer_id, size_id
            )
            return res
        except UpcOverlimit:
            return None
        except Exception, e:
            _logger.error("@upc Exception:\n{error}".format(error=e.message))
            return None

    def _get_factory_cost(self, cr, uid, product_id, invoice_count=1, po=None, invoice=None):

        if not self._factory_cost_formula:
            self._factory_cost_formula = FactoryCostFormula()

        return self._factory_cost_formula.get(cr, product_id, invoice_count=invoice_count, po=po, invoice=invoice)

    def _get_invoice_cost(self, cr, uid, product_id, invoice_count=1):
        if not self._factory_cost_formula:
            self._factory_cost_formula = FactoryCostFormula()
        return self._factory_cost_formula.getinvc(cr, product_id, invoice_count=invoice_count)

    def _get_mould(self, cr, uid, product_id, args):

        import json
        po = json.loads(args).get('po_name')
        invoice = json.loads(args).get('invoice')

        sql = """
        
            select
                sum(( case when amt in (null,'','Existing') then 0 else cast( amt as float ) end )) as mould_value
            from
                dbo.shipping_details
            where
                po = '{po}'
                and type = 'Mould'
        
        """.format(
            po=po,
        )

        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_main_servers(cr, uid, single=True)
        if not server_id:
            raise Exception("MS SQL server not found")
        params = {}
        res = server_obj.make_query(
            cr, uid, server_id,
            sql=sql, params=params,
            select=True, commit=False, return_dict=True
        )

        return res[0].get('mould_value')


    def _get_profit(self, cr, uid, customer_id, prod_id):
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')
        try:
            profits_pool = json_loads(
                pricelist_callbacks_obj.get_actual_profit(
                    cr, uid,
                    {
                        'profile_it': False,
                        'store_to_customer_fields': False,
                        'return_result': True,
                        'data': {customer_id: [prod_id]},
                    }
                )
            )

            if profits_pool.get('errors'):
                raise Exception("; ".join(profits_pool['errors']))

            product_profits = profits_pool.get(str(customer_id), {}).get(str(prod_id), {})
            if not product_profits:
                raise Exception("Can't generate profit")

            if product_profits.get('errors'):
                raise Exception("; ".join(product_profits['errors']))

            profit = str2float(product_profits.get('profit'), 0.0)
            if not profit:
                raise Exception("Profit is wrong")

            return "{}%".format((1 - profit) * 100)

        except Exception, e:
            _logger.error("@profit Exception:\n{error}".format(error=e.message))
            return None


    def _check_pricelist_field(self, field=None):
        result = False
        if (field is None):
            field = {}
        if (isinstance(field, dict)):
            composit = field.get('composite', False)
            if (composit and composit == '@pricelist_value'):
                result = True
        return result

    def _get_pricelist_value(self, cr, uid, prod=None, field=None, size_id=None, cm_field_id=False, context=None):
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')
        result = ''
        if not field:
            field = {}
        if not prod:
            prod = {}
        prod_id = prod.get('id', False)
        field_name_id = field.get('id', False)
        customer_id = field.get('customer_id', False)

        if (prod_id and customer_id):
            ctx = {
                'profile_it': False,
                'store_to_customer_fields': True,
                'overwrite': True,
                'return_result': False,
                'fill_result_step_by_step': False,
                'fill_profits': False,
                'erp_export': True,
                'field_name_id': False,
                'data': {},
            }
            product_ids = []
            if (size_id):
                product_ids = [[prod_id, [size_id]]]
            else:
                product_ids = [prod_id]
            ctx.update({'data': {customer_id: product_ids}})
            if (field_name_id):
                ctx.update({
                    'field_name_id': field_name_id,
                })
            res = pricelist_callbacks_obj.calculate_pricelists(cr, uid, ctx)
            if (field_name_id):
                if (not size_id):
                    size_id_str = 'IS NULL'
                else:
                    size_id_str = "= '{0}'".format(size_id)
                sql = """SELECT
                            pmcf.value
                        FROM
                            product_multi_customer_fields AS pmcf
                        LEFT JOIN
                            product_product AS pp
                                ON pmcf.product_id = pp.id
                        LEFT JOIN
                            res_partner AS rp
                                ON pmcf.partner_id = rp.id
                        LEFT JOIN
                            ring_size AS rs
                                ON pmcf.size_id = rs.id
                        WHERE 1=1
                            AND pp.id = {product_id}
                            AND pmcf.size_id {size_id}
                            AND rp.id = {partner_id}
                            AND pmcf.field_name_id = {field_name_id}
                """.format(
                    product_id=prod_id,
                    size_id=size_id_str,
                    partner_id=customer_id,
                    field_name_id=field_name_id
                )
                cr.execute(sql)
                res = cr.fetchall()
                result = res and res[0] and res[0][0] or ''
        return result

    def get_val_for_name(self, cr, uid, prod, name, field, size_id=False, context=None):
        if not context:
            context = {}
        prod_id = prod.get('id', False)

        if (name == 'ring_size_ids'):
            if field.get('expand_ring_sizes') == True:
                if size_id:
                    return self.pool.get('ring.size').browse(cr, uid, size_id, context=context).name
                else:
                    return ''
            elif field.get('expand_ring_sizes') == False:
                sizes = []
                if prod['ring_size_ids'] and context.get('size_range'):
                    size_ids = context.get('size_range')
                else:
                    size_ids = prod['ring_size_ids']
                for size in self.pool.get('ring.size').browse(cr, uid, size_ids, context=context):
                    if size:
                        sizes.append(size.name)
                return ', '.join(sizes)

        prod_val = prod.get(name, "no_value")
        # import pdb; pdb.set_trace()
        if(prod_val == "no_value"):
            new_read = super(product_product, self).read(cr, uid, prod_id, fields=[name], context=context)
            prod_val = new_read.get(name, "no_value")

        if(prod_val == "no_value"):

            name_ids = self.pool.get('product.multi.customer.names').search(cr, uid, [('name', '=', name)], context=context)
            cmn_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, ['&', ('formula_id', '=', name), ('customer_id', 'in', [field['customer_id'], False])], order="customer_id", context=context)
            if not cmn_ids:
                if name_ids:
                    cmn_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, ['&', ('name_id', 'in', name_ids), ('customer_id', 'in', [field['customer_id'], False])], order="customer_id", context=context)
            if cmn_ids:

                cm_ids = self.pool.get('product.multi.customer.fields').search(cr, uid, ['&', ('field_name_id', 'in', cmn_ids), ('product_id', '=', prod_id)])
                if cm_ids:
                    if field.get('expand_ring_sizes') == True:
                        cm_size_ids = self.pool.get('product.multi.customer.fields').search(cr, uid, [('id', 'in', cm_ids), ('size_id', '=', size_id or False)], context=context)
                        if cm_size_ids:
                            prod_val = self.pool.get('product.multi.customer.fields').read(cr, uid, cm_size_ids[0])['value']
                    else:
                        prod_vals = []
                        for cm in self.pool.get('product.multi.customer.fields').read(cr, uid, cm_ids):
                            prod_vals.append(cm['value'] or "")
                        prod_val = ', '.join(prod_vals)
            else:
                add_ids = self.pool.get("product.multi.customer.additional.fields").search(cr, uid, ['&', ('product_id', '=', prod_id), ('name_id', 'in', name_ids)], context=context)
                if add_ids:
                    prod_val = self.pool.get("product.multi.customer.additional.fields").read(cr, uid, add_ids[0])['value']

        if(isinstance(prod_val, tuple)):
            prod_val = prod_val[1]

        if (prod_val == []):
            prod_val = ''

        return prod_val

    def get_val_by_label(self, cr, uid, product_id, customer_id, label, size_id=False, context=None):
        if not context:
            context = {}
        field_value = False
        cm_fields_name_ids = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('customer_id', '=', customer_id), ('label', '=', label)], context=context)

        cm_fields_ids = self.pool.get('product.multi.customer.fields').search(cr, uid, [('field_name_id', 'in', cm_fields_name_ids), ('product_id', '=', product_id), ('size_id', '=', size_id)])

        if cm_fields_ids:
            field = self.pool.get('product.multi.customer.fields').browse(cr, uid, cm_fields_ids[0])
            field_value = field.value

        return field_value

    def search_product_by_id(self, cr, uid, customer_id, search_value, search_field=False, context=None):
        """
        :param context:Use param use_size with size_id to check field that have same name for multiple sizes
        """
        if not context:
            context = {}
        res_product = False
        res_size = False

        search_value = (search_value or '').strip()

        if search_value:

            ids = self.search(cr, uid, ['|', ('default_code', '=', search_value), ('id_delmar', '=', search_value), ('type', '=', 'product')])
            ids = list(set(ids))

            if(ids and len(ids) == 1):
                res_product = self.browse(cr, uid, ids[0])

            else:
                if(not search_field):
                    search_field = ('upc', 'os_product_sku', 'customer_sku', 'customer_id_delmar', 'default_code', 'pgg_customer_sku', 'pgg_customer_id_delmar', 'customer_id_delmar_2', 'customer_sku_2')

                search_value = search_value.lstrip('0')
                if search_value:
                    # DLMR-2075
                    # Add size to query for one customer_sku for all sizes
                    where_size = ''
                    if context.get('use_size', False):
                        where_size = 'AND cf.size_id={}'.format(context.get('use_size'))
                    # END DLMR-2075

                    where_customer = ""
                    if customer_id:
                        where_customer = "AND fn.customer_id = %s" % (customer_id)

                    for s_field in search_field:

                        cr.execute("""  SELECT
                                            DISTINCT    cf.product_id as product_id,
                                                        cf.size_id as size_id
                                        FROM
                                                        product_multi_customer_names cn
                                            INNER JOIN  product_multi_customer_fields_name fn on (
                                                fn.name_id = cn.id
                                                """ + where_customer + """
                                            )
                                            INNER JOIN  product_multi_customer_fields cf on cf.field_name_id = fn.id
                                        WHERE 1=1
                                          AND cn.name = '{}'
                                          {}
                                             AND (cf."value" ilike  '{}' or 
                                                  cf."value" ilike  '0{}' or 
                                                  cf."value" ilike  ' {}' or
                                                  cf."value" ilike  '0 {}' or 
                                                  cf."value" ilike  ' 0{}' or
                                                  cf."value" ilike  '{} ' or 
                                                  cf."value" ilike  '0{} ' or 
                                                  cf."value" ilike  ' {} ' or
                                                  cf."value" ilike  '0 {} ' or 
                                                  cf."value" ilike  ' 0 {} ' 
                                                  ) 
                        """.format(s_field, where_size, search_value, search_value, search_value, search_value, search_value, search_value, search_value, search_value, search_value, search_value))
                        res = cr.fetchall()
                        if res and len(res) == 1:
                            product_id, size_id = res[0]

                            res_product = self.pool.get("product.product").browse(cr, uid, product_id)
                            res_size = self.pool.get("ring.size").browse(cr, uid, size_id)

                            if res_product:
                                break

        return res_product, res_size

    def search_product_by_cf(self, cr, uid, partner_id, search_value, search_field=False, limit=None, context=None):
        res = []

        search_value = (search_value or '').strip()
        if search_value:

            _from = "product_multi_customer_fields cf"
            _where = [
                'AND cf."value" = %s',
            ]
            _params = [
                search_value
            ]
            _limit = limit and "LIMIT %d" % (limit) or False

            if partner_id:
                _where.append("AND fn.customer_id = %s")
                _params.append(partner_id)
            if search_field:
                _from = """
                    product_multi_customer_names cn
                        INNER JOIN  product_multi_customer_fields_name fn on (
                            fn.name_id = cn.id
                        )
                        INNER JOIN  product_multi_customer_fields cf on cf.field_name_id = fn.id
                """
                _where.append("AND cn.name in %s")
                if isinstance(search_field, (str, unicode)):
                    search_field = [search_field]
                _params.append(tuple(search_field))

            _where = " ".join(_where)

            sql = """
                    SELECT
                        DISTINCT    cf.product_id as product_id,
                                    cf.size_id as size_id
                    FROM """ + _from + """
                    WHERE 1=1 """ + _where + _limit

            cr.execute(sql, _params)
            res = cr.dictfetchall()

        return res

    def customer_fields_save(self, cr, uid, ids, context=None):
        return True

product_product()


class res_partner(osv.osv):
    _inherit = 'res.partner'
    _columns = {
        'multi_customer_fields_name': fields.one2many('product.multi.customer.fields.name', 'customer_id',
                                                      'Customers fields',
                                                      help='List of products that are part of this pack.'),
    }

    def create(self, cr, uid, vals, context=None):
        customer_id = super(res_partner, self).create(cr, uid, vals, context=context)
        fields_ids = self.pool.get("product.multi.customer.fields.name").search(cr, uid, [('for_all', '=', 1)],
                                                                                context=context)
        exist_name_id = []
        for field in self.pool.get("product.multi.customer.fields.name").browse(cr, uid, fields_ids):
            if (field.name_id.id not in exist_name_id):
                values = {
                    'label': field.label,
                    'name_id': field.name_id.id,
                    'customer_id': customer_id,
                    'default_value': field.default_value,
                    'export_label': field.export_label,
                    'for_all': 1,
                    'type_id': field.type_id.id,
                    'expand_ring_sizes': field.expand_ring_sizes,
                    'labels_in_importing_file': [(6, 0, [x.id for x in field.labels_in_importing_file])],

                }

                self.pool.get('product.multi.customer.fields.name').create(cr, uid, values, context=context)

                exist_name_id.append(field.name_id.id)
        return customer_id


res_partner()
