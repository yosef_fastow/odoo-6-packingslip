# -*- coding: utf-8 -*-

{
    'name': 'Multi-Customer',
    'version': '1.0',
    'category': 'Tools',
    'complexity': "expert",
    'description': "Multi-Customer",
    'author': '',
    'website': '',
    'depends': [
        'pf_utils',
        'product',
        'delmar_products',
    ],
    'init_xml': [
        'data/product_multi_customer_names_data.xml',
        'data/phantom_inventory_end_date_cron.xml'
    ],
    'update_xml': [
        "security/multi_customer_security.xml",
        'security/ir.model.access.csv',
        'views/multi_customer_view.xml',
        'views/res_partner_view.xml',
        'views/product_view.xml',
        'views/product_view_extend.xml',
        'views/pmcfn_type_view.xml',
        'views/multi_customer_menu_view.xml',
    ],
    'js': ['static/src/js/multi_customer.js'],
    'demo_xml': [],
    'installable': True,
    "active": False,
    'auto_install': False,
    "application": True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
