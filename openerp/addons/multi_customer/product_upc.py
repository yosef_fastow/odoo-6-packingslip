import logging
from osv import osv, fields
import requests

_logger = logging.getLogger(__name__)


class UpcOverlimit(Exception):
    pass


class product_upc(osv.osv):
    _name = 'product.upc'
    _table = 'upc_data'

    _columns = {
        'upc': fields.char('UPC', size=14, required=True),
        'upc_uniq_num': fields.integer('UPC Unique Number', required=True),
        'customer': fields.char('Customer', size=64),
        'delmar_id': fields.char('Delmar Id', size=64),
        'customer_sku': fields.char('Customer SKU', size=64),
        'ring_size': fields.char('Ring Size', size=5),
    }

    _sql_constraints = [
        (
            'upc_data_upc_uniq_num',
            'UNIQUE(upc_uniq_num)',
            'UPC Unique Number must be unique!'
        ),
        (
            'upc_data_upc',
            'UNIQUE(upc)',
            'UPC must be unique!'
        ),
        (
            'upc_data_uniq',
            'UNIQUE(customer, delmar_id, ring_size)',
            'UPC must be unique!'
        ),
    ]

    def generate_remote(self, cr, uid, delmar_id=None, customer_ref=None, size=''):
        if not (delmar_id or customer_ref):
            return None
        url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'export.get_upc_url')
        data = {
            'delmar_id': delmar_id,
            'customer': customer_ref,
            'size': size,
            'uid': uid
        }
        # headers = {'Accept': 'application/'}
        r = requests.get(url=url, params=data, verify=False)
        if r.status_code == 200:
            return str(r.text).strip()
        else:
            _logger.info("Invalid response from upc_generator service: {}, url: {}, request: {}".format(r.text, url, data))
        return None

    def get_or_generate(self, cr, uid, product_id, customer_id, size_id=None):
        delmar_id = self.pool.get('product.product').browse(cr, uid, product_id).default_code
        customer = self.pool.get('res.partner').browse(cr, uid, customer_id).ref
        upc = None
        if size_id:
            size = self.pool.get('ring.size').browse(cr, uid, size_id).name
        else:
            size = ''
        # try to get existed
        upc_ids = self.search(cr, uid, [
            ('delmar_id', '=', delmar_id),
            ('customer', '=', customer),
            ('ring_size', '=', size),
        ])

        if upc_ids:
            # if already exists - return it
            upc = self.browse(cr, uid, upc_ids[0]).upc or None
            _logger.info("UPC exists: %s" % upc)
        else:
            # generate new if not exists using remote upc_generator
            upc = self.generate_remote(cr, uid, delmar_id=delmar_id, size=size, customer_ref=customer)
            _logger.info("UPC generated: %s" % upc)
        return upc


product_upc()
