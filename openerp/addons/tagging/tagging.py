# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class tags(osv.osv):
    _name = "tagging.tags"
    _columns = {
        "name": fields.char("Tag", size=64, required=True, translate=True),
        "description": fields.char("Short Description", size=256, translate=True),
        "notes": fields.text("Notes"),
        "active": fields.boolean("Active"),
    }
    _defaults = {
        "active": lambda *a: True,
    }
    _sql_constraints = [
        ('tagging_tags_name_unique', 'unique (name)', _('The tag names must be unique!')),
    ]

    def unlink(self, cr, uid, ids, context=None):
        if self.pool.get('product.catalog.settings').search(cr, uid, [('tag_id', 'in', ids)]):
            raise osv.except_osv(_('Error!'),
                                 _("""Tag {} used in product catalog settings record(s). 
                                   Please, remove it firstly from there.""".format(self.browse(cr, uid, ids[0]).name)))
        else:
            super(tags, self).unlink(cr, uid, ids, context=context)
        return True

tags()

class tagging_related_tags(osv.osv):
    _inherit = "tagging.tags"
    _name = _inherit

    _columns = {
        "related_tags_ids": fields.many2many("tagging.tags", "tagging_related_tags", "tag_id", "related_tag_id", string="Related Tags"),
        
    }
tagging_related_tags()
