{
    "name": "local_settings",
    "category": "Hidden",
    "description": "Local CSS Settings for WEB-GUI",
    "depends": ['base'],
    'active': True,
    'js': [],
    'css': [
        "static/src/css/local.css",
    ],
    'qweb': [],
}
