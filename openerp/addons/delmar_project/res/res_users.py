# -*- coding: utf-8 -*-
##############################################################################

from osv import fields, osv


class users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'task_categ_ids': fields.many2many('project.task.category', 'project_task_users_rel', 'categ_id', 'user_id', 'Categories', ),
    }

users()
