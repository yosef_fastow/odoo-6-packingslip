from osv import fields, osv
import time


class email_template(osv.osv):
    _inherit = "email.template"

    _columns = {
        "send_mail": fields.boolean('Active'),
        'email_from': fields.char('From', size=512, help="Sender address (placeholders may be used here)"),
        'email_to': fields.char('To', size=512, help="Comma-separated recipient addresses (placeholders may be used here)"),
        'email_cc': fields.char('Cc', size=512, help="Carbon copy recipients (placeholders may be used here)"),
        'email_bcc': fields.char('Bcc', size=512, help="Blind carbon copy recipients (placeholders may be used here)"),
        'reply_to': fields.char('Reply-To', size=512, help="Preferred response address (placeholders may be used here)"),
    }

    _defaults = {
        'send_mail': True
    }

    def generate_email(self, cr, uid, template_id, res_id, context=None):
        values = super(email_template, self).generate_email(cr, uid, template_id, res_id, context=context)
        values['date'] = time.strftime('%Y-%m-%d %H:%M:%S')
        return values

email_template()
