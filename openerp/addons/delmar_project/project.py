from osv import osv, fields
import time

from tools.translate import _


MAIL_TMPL_NAME = {
    'new': 'task_email_template_user_new_task',
    'draft': 'task_email_template_user_draft_task',
    'open': 'task_email_template_user_assigne_task',
    'cancelled': 'task_email_template_user_cancel_task',
    'done': 'task_email_template_manager_done_task',
    'changed': 'task_email_template_user_change_task',
}


class project_task_type(osv.osv):
    _inherit = 'project.task.type'

    _defaults = {
        'project_default': True,
    }
project_task_type()


class project_task_category(osv.osv):
    _name = "project.task.category"
    _description = 'Task category'

    _columns = {
        'name': fields.char('Category', size=256, required=True),
        'user_ids': fields.many2many('res.users', 'project_task_users_rel', 'user_id', 'categ_id', 'Users', ),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The category name must be unique !')
    ]
project_task_category()


class project_task(osv.osv):
    _inherit = "project.task"

    _columns = {
        'id': fields.integer('ID', readonly=True,),
        'create_uid': fields.many2one('res.users', 'Creator', readonly=True,),
        'categ_id': fields.many2one('project.task.category', 'Category', required=True)
    }

    def create(self, cr, uid, vals, context=None):
        result = super(project_task, self).create(cr, uid, vals, context=context)
        self.send_mail(cr, uid, [result], MAIL_TMPL_NAME['new'], context=context)
        return result

    def write(self, cr, uid, ids, vals, context=None):

        if isinstance(ids, (int, long)):
            ids = [ids]

        sorted_types = self._get_types(cr, uid, True, context)

        if vals and 'type_id' in vals:
            typeid = vals.get('type_id')
            for t in self.browse(cr, uid, ids, context=context):
                if typeid and typeid in sorted_types and t.type_id != typeid:
                    index = sorted_types.index(typeid)
                    if index:
                        if index == len(sorted_types)-1:
                            vals['state'] = 'done'
                            vals['kanban_state'] = 'done'
                        elif t.state == 'draft' or t.state == 'done':
                            vals['state'] = 'open'
                else:
                    vals['state'] = 'draft'

        elif vals and 'state' in vals:
            state = vals.get('state')
            if state == 'done':
                vals['kanban_state'] = 'done'

            if len(sorted_types) > 0:
                # FIXME: Strange. Condition only on > 0, but used sorted_types[1]

                if state == 'draft':
                    vals['type_id'] = None
                    vals['date_start'] = time.strftime('%Y-%m-%d %H:%M:%S')
                    vals['date_end'] = None

                elif state == 'open':
                    vals['type_id'] = sorted_types[1]

                elif state == 'done':
                    vals['type_id'] = sorted_types[-1]

        if vals and 'kanban_state' not in vals and 'type_id' in vals:
            new_stage = vals.get('type_id')
            vals_reset_kstate = dict(vals, kanban_state='normal')
            for t in self.browse(cr, uid, ids, context=context):
                write_vals = vals_reset_kstate if t.type_id != new_stage else vals
                super(project_task, self).write(cr, uid, [t.id], write_vals, context=context)
            result = True
        else:
            result = super(project_task, self).write(cr, uid, ids, vals, context=context)
        if vals and ('type_id' in vals) or ('remaining_hours' in vals) or ('user_id' in vals) or ('state' in vals) or ('kanban_state' in vals):
            self._store_history(cr, uid, ids, context=context)

        if vals:
            if len(vals) > 1 or 'sequence' not in vals:
                mail_type = None
                if vals.get('state', False) in ['draft', 'open', 'cancelled', 'done']:
                    mail_type = vals['state']
                elif 'user_id' in vals:
                    mail_type = 'open'
                if mail_type:
                    mail_tmpl_name = MAIL_TMPL_NAME[mail_type]
                    self.send_mail(cr, uid, ids, mail_tmpl_name, context=context)

        return result

    def do_open(self, cr, uid, ids, context=None):

        if ids:
            if not isinstance(ids, list):
                ids = [ids]
            tasks = self.browse(cr, uid, ids, context=context)
            for t in tasks:
                data = {'state': 'open'}
                if not t.date_start:
                    data['date_start'] = time.strftime('%Y-%m-%d %H:%M:%S')
                    data['date_end'] = None
                self.write(cr, uid, [t.id], data, context=context)
                message = _("The task '%s' is opened.") % (t.name,)
                self.log(cr, uid, t.id, message)
        return True

    def send_mail(self, cr, uid, ids, tpl_name, context=None):

        if context is None:
            context = {}

        tpl_insts = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'delmar_project', tpl_name)[1],

        if tpl_insts and tpl_insts[0]:
            tpl_inst = tpl_insts[0]
            tpl = self.pool.get('email.template').read(cr, uid, tpl_inst)
            if tpl and tpl['send_mail']:
                context['web_root_url'] = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.alias')

                if not context['web_root_url']:
                    context['web_root_url'] = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')

                models_data = self.pool.get('ir.model.data')
                action = models_data.get_object_reference(cr, uid, 'project', 'action_view_task')
                context['act_id'] = action[1]

                server_obj = self.pool.get('fetchmail.server')
                servers = server_obj.search(cr, uid, [('state', '=', 'done')])
                if servers and servers[0]:
                    context['reply_to'] = server_obj.browse(cr, uid, servers[0]).user

                if not isinstance(ids, list):
                    ids = [ids]
                for task_id in ids:
                    new_ctx = context.copy()
                    cr.execute("""
                        select e.work_email
                        from project_task t
                            inner join res_users u on (t.id = %(task_id)s and t.create_uid = u.id)
                            inner join hr_department d on (u.context_department_id = d.id)
                            inner join hr_employee e on (e.id = d.manager_id)
                    """, {'task_id': task_id})
                    res_tuples = cr.fetchall()
                    for res_tuple in res_tuples:
                        if res_tuple[0]:
                            new_ctx['manager_email'] = res_tuple[0]

                    self.pool.get('email.template').send_mail(cr, uid, tpl_inst, task_id, True, new_ctx)
        return True

    def _change_type(self, cr, uid, ids, next, context=None):
        # print 'Change type '
        sorted_types = self._get_types(cr, uid, next, context=context)
        if sorted_types:
            for task in self.browse(cr, uid, ids):
                typeid = task.type_id.id

                vals = {}
                index = None
                if not typeid:
                    index = 0
                    vals['type_id'] = sorted_types[0]
                elif typeid and typeid in sorted_types and sorted_types.index(typeid) != len(sorted_types)-1:
                    index = sorted_types.index(typeid) + 1

                if index:
                    vals['type_id'] = sorted_types[index]
                elif next == False:
                    vals['type_id'] = None

                self.write(cr, uid, task.id, vals)
        return True

    def _get_types(self, cr, uid, next, context=None):
        types = []
        def_types = self.pool.get('project.task.type').search(cr, uid, [('project_default', '=', True)])
        types_seq = {}

        for type in self.pool.get('project.task.type').browse(cr, uid, def_types):
            types_seq[type.id] = type.sequence
        if next:
            types = sorted(types_seq.items(), lambda x, y: cmp(x[1], y[1]))
        else:
            types = sorted(types_seq.items(), lambda x, y: cmp(y[1], x[1]))
        sorted_types = [x[0] for x in types]

        return sorted_types

    def next_type(self, cr, uid, ids, context=None):
        return self._change_type(cr, uid, ids, True, context=context)

    def prev_type(self, cr, uid, ids, context):
        return self._change_type(cr, uid, ids, False, context=context)

project_task()


class project_work(osv.osv):
    _inherit = "project.task.work"
    _columns = {
        'name': fields.text('Work summary',),
        'create_date': fields.datetime('Date', readonly=True,),
    }

    _order = "create_date desc"

project_work()
