from osv import fields, osv
from openerp.addons.base_report_designer.openerp_sxw2rml import sxw2rml
from StringIO import StringIO
import base64
import addons
import tools


class customer_report(osv.osv):
    _name = 'customer.report'

    def _name_get(self, cr, uid, ids, prop, unknow_none, context=None):
        result = {}
        for rec in self.browse(cr, uid, ids, context=context):
            result[rec.id] = rec.report_id.name + ' / ' + (rec.customer_id.name or 'No customer') + ' / ' + (rec.categ_id.name or 'No category')

        return result

    _columns = {
        'name': fields.function(_name_get, string='Name', size=128, type="char", store=True, select=True),
        'categ_id': fields.many2one('customer.report.category', 'Category', required=False),
        'report_id': fields.many2one('ir.actions.report.xml', 'Report', required=True, ondelete="cascade"),
        'customer_id': fields.many2one('res.partner', 'Customer', required=True, domain="[('customer', '=', True)]"),
        'report_sxw_content_data': fields.binary('SXW content'),
        'report_rml_content_data': fields.binary('RML content'),
        'location_id': fields.many2one('stock.location', 'Location', required=False),
        'report_rml_path': fields.char('Report file', size=256, help="The path to the report file"),
    }

    _sql_constraints = [
        ('key_uniq', 'unique (categ_id, report_id, customer_id)',
            'The key (categoty, customer, report) must be unique !')
    ]

    def get_file(self, cr, uid, rec_id, context=None):
        rec = self.browse(cr, uid, rec_id, context=context)
        fp = None
        try:
            fp = tools.file_open(rec.report_rml_path, mode='rb')
            data = fp.read()
        except:
            data = False
        finally:
            if fp:
                fp.close()
        return data


    def change_sxw(self, cr, uid, id, sxw_file, context=None):
        if not context:
            context = {}
        result = {
            'report_sxw_content_data': sxw_file,
            'report_rml_content_data': False,
        }
        return {'value': result}

    def change_rml(self, cr, uid, id, rml_file, context=None):
        if not context:
            context = {}
        result = {
            'report_rml_content_data': rml_file,
            'report_sxw_content_data': False,
        }
        return {'value': result}

    def create(self, cr, uid, vals, context=None):
        if vals.get('report_sxw_content_data', False):
            vals['report_rml_content_data'] = self.sxw2rml(cr, uid, vals.get('report_sxw_content_data'))
        return super(customer_report, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('report_sxw_content_data', False):
            vals['report_rml_content_data'] = self.sxw2rml(cr, uid, vals.get('report_sxw_content_data'))
        return super(customer_report, self).write(cr, uid, ids, vals, context=context)

    def sxw2rml(self, cr, uid, file_sxw):
        rml = False
        try:
            sxwval = StringIO(base64.decodestring(file_sxw))
            fp = open(addons.get_module_resource('base_report_designer', 'openerp_sxw2rml', 'normalized_oo2rml.xsl'), 'rb')
            rml = str(sxw2rml(sxwval, xsl=fp.read()))
            if rml:
                rml = base64.encodestring(rml)
        except:
            raise osv.except_osv('Error', 'Can not convert to sxw to rml')

        return rml

customer_report()


class customer_report_category(osv.osv):
    _name = 'customer.report.category'

    _columns = {
        'name': fields.char('Name', size=64, required=True),
    }

customer_report_category()


class customer_report_template(osv.osv):
    _name = 'customer.report.template'
    _columns = {
        'categ_id': fields.many2one('customer.report.category', 'Category', required=False),
    }

    def create_report(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        data = None

        datas = self.browse(cr, uid, ids)
        if datas:
            data = datas[-1]

        resp = {
            'type': 'ir.actions.report.xml',
            'report_name': context.get('report_name', False),
            'datas': {
                'model': context.get('report_model', False),
                'report_type': 'pdf',
                'id': context.get('active_id', False),
                'ids': context.get('active_ids', False),
            },
            'context': {
                'categ_id': data and data.categ_id and data.categ_id.id or False,
            }
        }
        return resp

customer_report_template()
