from openerp.report.report_sxw import report_sxw

from lxml import etree
import openerp.pooler as pooler

import StringIO
import cStringIO

import tools
from tools.translate import _

import time
import logging
import base64
import os
import netsvc
from osv import osv
from openerp.osv.osv import except_osv
from datetime import datetime, timedelta


MIN_ORDERS_IN_BATCH = 24
BATCH_SIZE = 50

_logger = logging.getLogger(__name__)


def create(self, cr, uid, ids, data, context={}):
    def _not_scheduled(ids):
        scheduler_obj = pool.get('packing.scheduler')
        active_scheduler_ids = scheduler_obj.search(cr, uid, [('type', '=', 'hourly')])
        active_schedulers = scheduler_obj.browse(cr, uid, active_scheduler_ids)
        active_ids = sum(map(lambda x: [int(x) for x in x.order_ids.split(',')], active_schedulers), [])
        ids = list(set(ids) - set(active_ids))
        return ids

    pool = pooler.get_pool(cr.dbname)
    # Create a cron and out
    # Wrap only batch print packing list on delivery order screen
    # Skip single print packing list on delivery order screen and daily pack slip scheduler export_csv_background
    if ('list' in self.name) and (len(ids) > MIN_ORDERS_IN_BATCH) and (not context.get('run_by_scheduler')) and (not context.get('report_id')):
        # Filter out orders already scheduled
        ids = _not_scheduled(ids)
        if not ids:
            # Exactly the same set of orders was already put in the print queue
            raise osv.except_osv(_('Warning'), _('Orders already printing by scheduler. Please, check packing list exports screen...'))

        cron_obj = pool.get('ir.cron')
        picking_export_obj = pool.get('picking.export')
        scheduler_obj = pool.get('packing.scheduler')
        scheduler_obj.add_multi_orders_task(cr, uid, ids)

        params = {
            'sort': 'asc',
        }
        vals = {
            'user_id': uid,
            'model': 'picking.export',
            'interval_type': 'minutes',
            'function': 'hourly_packslip_scheduling',
            'args': "(%s,%s)" % (ids, params),
            'numbercall': -1,
            'priority': 4,
            'type': 'printing_packings' if uid == 1 else 'packing_slip_manual',
            'doall': False,
            # 'nextcall': new_datetime,
        }

        name_datetime = datetime.now()
        filename = '{}_packings'.format(name_datetime.strftime('%Y%m%d_%H%M%S'))
        vals.update({'name': filename})

        cron_id = cron_obj.create(cr, uid, vals)
        params['cron_id'] = cron_id
        cron_obj.write(cr, uid, [cron_id], {'args': "(%s,%s)" % (ids, params)})

        picking_export_obj.create(cr, uid, {
            'sort': 'asc',
            'pdf_filename': filename + '.pdf',
            'state': 'confirm',
            'info': 'Request has been submitted',
            'status': 'In Process',
            'cron_id': cron_id,
            'scheduled_datetime': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
        })
        # picking_export_obj.write(cr, uid, picking_export_id, {'state': 'confirm', 'info': 'Request has been submitted', 'status': 'In Process', 'pdf_filename': filename + '.pdf', 'cron_id': cron_id})

        msg = """
        Your list of orders exceed current limit {}, hence it was placed in queue.
        You can check and download printed pdfs on packing list exports screen.
        """.format(MIN_ORDERS_IN_BATCH)
        raise osv.except_osv(_('Warning'), _(msg))

    if self.internal_header:
        context.update(internal_header=self.internal_header)
    # skip osv.fields.sanitize_binary_value() because we want the raw bytes in all cases
    context.update(bin_raw=True)
    ir_obj = pool.get('ir.actions.report.xml')
    if '.batch' in self.name:
        context.update(batch_report=True)
    report_xml_ids = ir_obj.search(
        cr, uid,
        [('report_name', '=', self.name[7:].replace('.batch', ''))],
        context=context,
        )
    if report_xml_ids:
        report_xml = ir_obj.browse(cr, uid, report_xml_ids[0], context=context)
    else:
        title = ''
        report_file = tools.file_open(self.tmpl, subdir=None)
        try:
            rml = report_file.read()
            report_type = data.get('report_type', 'pdf')
            class a(object):
                def __init__(self, *args, **argv):
                    for key, arg in argv.items():
                        setattr(self, key, arg)
            report_xml = a(title=title, report_type=report_type, report_rml_content=rml, name=title, attachment=False, header=self.header)
        finally:
            report_file.close()
    if report_xml.header:
        report_xml.header = self.header
    report_type = report_xml.report_type
    if report_type in ['sxw', 'odt']:
        fnct = self.create_source_odt
    elif report_type in ['pdf', 'raw', 'txt', 'html']:
        fnct = self.create_source_pdf
    elif report_type == 'html2html':
        fnct = self.create_source_html2html
    elif report_type == 'mako2html':
        fnct = self.create_source_mako2html
    else:
        raise NotImplementedError(_('Unknown report type: %s') % report_type)
    fnct_ret = fnct(cr, uid, ids, data, report_xml, context=context)
    if not fnct_ret:
        return False, False
    return fnct_ret


def _get_customer_id(self, obj, report_xml):
    customer = False
    if report_xml.model == 'sale.order.line':
        customer = obj.order_id.partner_id
    elif report_xml.model == 'stock.picking':
        customer = obj.sale_id.partner_id
    elif 'partner_id' in obj._columns:
        customer = obj.partner_id
    if customer and customer.parent_partner_id:
        customer = customer.parent_partner_id
    return customer and customer.id or False


def create_source_pdf(self, cr, uid, ids, data, report_xml, context=None):
    _logger.info("Run Delmar_reports/report_sxw -> create_source_pdf")
    if not context:
        context = {}
    print_context = {}
    if context.get('batch_report', False):
        print_context = {'batch_report': True}
    pool = pooler.get_pool(cr.dbname)
    attach = report_xml.attachment

    wf_service = netsvc.LocalService("workflow")
    picking_data = pool.get('stock.picking')
    user = pool.get('res.users').browse(cr, uid, uid)

    objs = self.getObjects(cr, uid, ids, context)
    results = []
    e_str = False
    for obj in objs:
        categ_id = context.get('categ_id', False)
        if report_xml.model == 'stock.picking':
            is_unverified = context.get('unverified', False)
            is_picking = report_xml.report_name in ['flash.stock.picking.list', 'stock.picking.list', 'stock.picking.list.batch']
            try:
                if is_picking and not is_unverified:
                    if obj.state == 'assigned' and context.get('process_order', True):
                        process_order = picking_data.action_process(cr, uid, [obj.id], print_context)
                        if process_order:
                            if process_order['datas'] and process_order['datas']['ids']:
                                result = self.create_source_pdf(cr, uid, process_order['datas']['ids'], data, report_xml, context=context)
                                results.append(result)
                                continue
                        else:
                            raise Exception()
                picking_data.write(cr, uid, obj.id, {'report_history': (obj.report_history and obj.report_history + '\n' or '') + ("%s %s: \"%s\"" % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name, report_xml.name))})

            except Exception, e:
                e_str = e and str(e) or "Order hasn't been processed"
                _logger.warn("Failed. Can't process order " + str(obj.name))
                _logger.error("Error " + e_str)
                picking_data.write(cr, uid, obj.id, {'report_history': (obj.report_history and obj.report_history + '\n' or '') + ("%s %s: Error! %s" % (time.strftime('%m-%d %H:%M:%S', time.gmtime()), user.name, e_str))})
                obj_state = picking_data.read(cr, uid, obj.id, ['state']) and picking_data.read(cr, uid, obj.id, ['state']).get('state', False)
                if obj_state == 'picking' and is_picking:
                    picking_data.action_process(cr, uid, [obj.id], print_context)
                    wf_service.trg_validate(uid, 'stock.picking', obj.id, 'action_reship', cr)
                continue

        customer_id = self._get_customer_id(obj, report_xml)
        _logger.info("Delmar_reports/report_sxw -> customer_id: %s" % customer_id)
        try:
            customer_rml_content = False
            if customer_id:
                loc_id = False
                if report_xml.model == 'stock.picking':
                    loc_id = obj.move_lines and obj.move_lines[0] and obj.move_lines[0].location_id and obj.move_lines[0].location_id.id
                    if obj.sale_id.ship2store and not categ_id:
                        categ_s2s = pool.get('customer.report.category').search(cr, uid, [('name', '=', 'Ship to Store')])
                        if categ_s2s:
                            categ_id = categ_s2s[0]

                    if obj.sale_id.gift and not categ_id:
                        gift = pool.get('customer.report.category').search(cr, uid, [('name', '=', 'Gift')])
                        if gift:
                            categ_id = gift[0]
                customer_report_obj = pool.get('customer.report')
                customer_report_ids = customer_report_obj.search(cr, uid, [
                    ('report_id', '=', report_xml.id),
                    ('customer_id', '=', customer_id),
                    '|', ('report_rml_content_data', '!=', False), ('report_rml_path', 'not in', [False, '']),
                    ('categ_id', '=', categ_id),
                    ('location_id', '=', False)
                ])
                _logger.info("Delmar_reports/report_sxw -> report_ids search by: %s" % [('report_id', '=', report_xml.id),
                    ('customer_id', '=', customer_id),
                    '|', ('report_rml_content_data', '!=', False), ('report_rml_path', 'not in', [False, '']),
                    ('categ_id', '=', categ_id),
                    ('location_id', '=', False)])
                _logger.info("Delmar_reports/report_sxw -> report_ids: %s" % customer_report_ids)
                customer_report_loc_ids = customer_report_obj.search(cr, uid, [
                    ('report_id', '=', report_xml.id),
                    ('customer_id', '=', customer_id),
                    '|', ('report_rml_content_data', '!=', False), ('report_rml_path', 'not in', [False, '']),
                    ('categ_id', '=', categ_id),
                    ('location_id', '!=', False)
                ])

                _logger.info("Delmar_reports/report_sxw -> report_loc_ids: %s" % customer_report_loc_ids)
                if len(customer_report_ids) + len(customer_report_loc_ids) > 0:
                    customer_report = False
                    if loc_id:
                        loc_obj = pool.get('stock.location')
                        for customer_report_id in customer_report_loc_ids:
                            customer_report_cur = customer_report_obj.browse(cr, uid, customer_report_id)
                            if customer_report_cur.location_id and loc_obj.search(cr, uid, [('location_id', 'child_of', customer_report_cur.location_id.id), ('id', '=', loc_id)]):
                                customer_report = customer_report_cur
                                break
                    if not customer_report:
                        customer_report = customer_report_obj.browse(cr, uid, customer_report_ids[0])
                    _logger.info("Delmar_reports/report path: %s" % customer_report.report_rml_path)
                    customer_rml_content = pool.get('customer.report').get_file(cr, uid, customer_report.id) or base64.decodestring(customer_report.report_rml_content_data)
                context.update({'customer_rml_content': customer_rml_content})
        except Exception, e:
            _logger.error(e)
            context.update({'customer_rml_content': False})

        if attach:
            aname = eval(attach, {'object': obj, 'time': time})
            result = False
            if report_xml.attachment_use and aname and context.get('attachment_use', True):
                aids = pool.get('ir.attachment').search(cr, uid, [('datas_fname', '=', aname + '.pdf'), ('res_model', '=', self.table), ('res_id', '=', obj.id)])
                if aids:
                    brow_rec = pool.get('ir.attachment').browse(cr, uid, aids[0])
                    if not brow_rec.datas:
                        continue
                    d = base64.decodestring(brow_rec.datas)
                    results.append((d, 'pdf'))
                    continue

            result = self.create_single_pdf(cr, uid, [obj.id], data, report_xml, context)
            if not result:
                return False
            if aname:
                try:
                    name = aname + '.' + result[1]
                    # Remove the default_type entry from the context: this
                    # is for instance used on the account.account_invoices
                    # and is thus not intended for the ir.attachment type
                    # field.
                    ctx = dict(context)
                    ctx.pop('default_type', None)
                    pool.get('ir.attachment').create(cr, uid, {
                        'name': aname,
                        'datas': base64.encodestring(result[0]),
                        'datas_fname': name,
                        'res_model': self.table,
                        'res_id': obj.id,
                        }, context=ctx
                    )
                except Exception:
                    #TODO: should probably raise a proper osv_except instead, shouldn't we? see LP bug #325632
                    _logger.error('Could not create saved report attachment', exc_info=True)
        else:
            result = self.create_single_pdf(cr, uid, [obj.id], data, report_xml, context)
        results.append(result)

    if not results and e_str:
        return ['']

    # if report_xml.name in ('321 Driver Report'):
    #     # for all reports that are a listing on many items.
    #     # This prints all selected objects on a single page/report instead of
    #     # one report for each object.
    #     results = [self.create_single_pdf(cr, uid, ids, data, report_xml, context)]

    if results:
        if results[0][1] == 'pdf':
            from openerp.report.pyPdf import PdfFileWriter, PdfFileReader
            output = PdfFileWriter()
            for r in results:
                reader = PdfFileReader(cStringIO.StringIO(r[0]))
                for page in range(reader.getNumPages()):
                    output.addPage(reader.getPage(page))
            s = cStringIO.StringIO()
            output.write(s)
            return s.getvalue(), results[0][1]

    return self.create_single_pdf(cr, uid, ids, data, report_xml, context)


def create_single_pdf(self, cr, uid, ids, data, report_xml, context=None):
    if not context:
        context = {}
    logo = None
    context = context.copy()
    title = report_xml.name
    pool = pooler.get_pool(cr.dbname)
    picking_data = pool.get('stock.picking')

    rml = context.get('customer_rml_content', False)
    if not rml:
        rml = report_xml.report_rml_content
    # if no rml file is found
    if not rml:
        return False
    rml_parser = self.parser(cr, uid, self.name2, context=context)
    objs = self.getObjects(cr, uid, ids, context)

    rml_parser.set_context(objs, data, ids, report_xml.report_type)
    if context.get('sort_type') == 'items':
        rml = rml.replace('lines_sort_by_bin', 'lines_sort_by_delmar_id')
    processed_rml = etree.XML(rml)
    if report_xml.model == 'stock.picking':
        prod_states = ('picking', 'outcode_except', 'shipped', 'done', 'returned', 'unverified')
        obj_state = picking_data.read(cr, uid, objs[0].id, ['state'])
        if obj_state and obj_state.get('state', False) not in prod_states and not objs[0].sale_id.flash:
            processed_rml = self.get_watermark(processed_rml)

    if report_xml.header:
        rml_parser._add_header(processed_rml, self.header)
    processed_rml = self.preprocess_rml(processed_rml, report_xml.report_type)
    if rml_parser.logo:
        logo = base64.decodestring(rml_parser.logo)
    create_doc = self.generators[report_xml.report_type]
    pdf = create_doc(etree.tostring(processed_rml), rml_parser.localcontext, logo, title.encode('utf8'))
    return (pdf, report_xml.report_type)

def get_watermark(self, processed_rml):
    page_template = processed_rml.find('template').find('pageTemplate')
    if not page_template.find('pageGraphics'):
        pageGraphics = etree.Element('pageGraphics')
        page_template.append(pageGraphics)
    pageGraphics = page_template.find('pageGraphics')
    page_size = isinstance(eval(processed_rml.find('template').get('pageSize')), tuple) and eval(processed_rml.find('template').get('pageSize'))
    image_width = 451.0
    image_height = 525.0
    x_coord = int((page_size[0] - image_width)/2)
    y_coord = int((page_size[1] - image_height)/2)
    watermark = etree.Element('image', {'file': 'watermark.gif','x': str(x_coord),'y': str(y_coord), 'width': str(image_width), 'height': str(image_height)})
    pageGraphics.insert(0, watermark)
    image_path = os.path.join(os.path.dirname(__file__), 'static/images/watermark.gif')
    image_file = open(image_path, 'r')
    image_base64 = base64.b64encode(image_file.read())
    watermark_image = etree.Element('image', {'name': 'watermark.gif'})
    watermark_image.text = image_base64
    if not processed_rml.find('stylesheet').find('images'):
        images = etree.Element('images')
        processed_rml.find('stylesheet').append(images)
    processed_rml.find('stylesheet').find('images').insert(0, watermark_image)

    return processed_rml


report_sxw.create = create
report_sxw._get_customer_id = _get_customer_id
report_sxw.create_source_pdf = create_source_pdf
report_sxw.create_single_pdf = create_single_pdf
report_sxw.get_watermark = get_watermark
