{
    "name": "delmar_reports",
    "version": "0.1",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "fetchdb",
        "stock",
    ],
    "description": """

    """,
    "category": "Project Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'data/delmar_report.xml',
        'data/system_params.xml',
        'wizard/report_picking_wizard.xml',
        'view/report_view.xml',
        'reports/report_gift.xml',
        'reports/report_barcode.xml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
