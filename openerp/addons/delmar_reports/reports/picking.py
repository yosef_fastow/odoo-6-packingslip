# -*- coding: utf-8 -*-
##############################################################################

import barcode
import time
import requests
import os
from barcode.writer import ImageWriter
from json import JSONDecoder as JDecode
from base64 import b64encode
import base64
import shutil
from zplgrf import GRF
import urllib2
import math
from openerp.addons.stock.report.picking import picking
from report import report_sxw
import pooler
import ast

class ChildBarcode:
    def __init__(self, qty, customer_sku, name, vendor_sku, size):
        self.qty = qty
        self.customer_sku = customer_sku
        self.name = name
        self.vendor_sku = vendor_sku
        self.size = size


class manifest(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        self.__total_sub_price = 0;
        self.__total_sub_ship = 0;
        self.__total_sub_tax = 0;
        self.__message = False;
        super(manifest, self).__init__(cr, uid, name, context=context)
        self.context = context.copy()
        self.localcontext.update({
            'time': time,
            'get_total_lines': self._get_total_lines,
            'sort_orders_by_scn': self._sort_orders_by_scn,
            'get_qtytotal':self._get_qtytotal
        })
        self.rml_header = ""

    def _get_qtytotal(self,move_lines):
        total = 0.0
        uom = move_lines[0].product_uom.name
        for move in move_lines:
            total+=move.product_qty
        return {'quantity':total,'uom':uom}


def __init__(self, cr, uid, name, context):
    self.__total_sub_price = 0;
    self.__total_sub_ship = 0;
    self.__total_sub_tax = 0;
    self.__message = False;
    super(picking, self).__init__(cr, uid, name, context=context)
    self.context = context.copy()
    self.page_count = 0
    self.localcontext.update({
        'time': time,
        'str': str,
        'int': int,
        'float': float,
        'get_qtytotal': self._get_qtytotal,
        'get_total_customerCost_on_qty': self._get_total_customerCost_on_qty,
        'get_custom_field': self._get_custom_field,
        'get_acc_price': self._get_acc_price,
        'get_service_type_ups': self._get_service_type_ups,
        'sum_2_costs': self._sum_2_costs,
        'triple_sum': self._triple_sum,
        'simple_sum': self._simple_sum,
        'sum_args': self._sum_args,
        'sterling_lines': self._sterling_lines,
        'fmj_lines': self._fmj_lines,
        'indexed_lines': self._indexed_lines,
        # calc price functions
        # 'get_custom_price': self._get_custom_price,
        'get_total_custom_price': self._get_total_custom_price,
        'get_total_retail_price': self._get_total_retail_price,
        'get_total_shipCost_tax': self._get_total_shipCost_tax,
        'get_total_sub_price': self._get_total_sub_price,
        'get_total_result': self._get_total_result,
        'get_total_paid': self._get_total_paid,
        'get_customerCost_sum': self._get_customerCost_sum,
        'get_customerCost': self._get_customerCost,
        'get_shipCost': self._get_shipCost,
        'get_totalCost': self._get_totalCost,
        'get_subtotalCost': self._get_subtotalCost,
        'get_totalCost_sum': self._get_totalCost_sum,
        'lines_sort_by_bin': self._lines_sort_by_bin,
        'lines_sort_by_delmar_id': self._lines_sort_by_delmar_id,
        'lines_sort_by_asin': self._lines_sort_by_asin,
        'lines_sort_by_linenumb': self._lines_sort_by_linenumb,
        'get_gmtime': self._get_gmtime,
        'get_order_comment': self._get_order_comment,
        'get_order_line_comment': self._get_order_line_comment,
        'return_address': self._return_address,
        '_str_to_int':self._str_to_int,
        'get_retailCost': self._get_retailCost,
        'get_subretailCost': self._get_subretailCost,
        'get_retailCost_sum': self._get_retailCost_sum,
        'get_packing_code': self._get_packing_code,
        'get_bluestem_shipping': self._get_bluestem_shipping,
        'get_additional_order_field': self._get_additional_order_field,
        'get_additional_order_line_field':  self._get_additional_order_line_field,
        'get_additional_order_line_field_sum': self._get_additional_order_line_field_sum,
        'get_merchant_sku_part': self._get_merchant_sku_part,
        'time_format': self._time_format,
        'get_gst': self._get_gst,
        'get_pst': self._get_pst,
        'get_total': self._get_total,
        'tax_indicators': self._tax_indicators,
        'ship_indicators': self._ship_indicators,
        'get_address_ship2': self._get_address_ship2,
        'paginate_page': self._paginate_page,
        'paginated_move_lines': self._paginated_move_lines,
        'get_address_customer': self._get_address_customer,
        'get_packing_message': self._get_packing_message,
        'get_account_number': self._get_account_number,
        'get_order_account_number': self._get_order_account_number,
        'from_json': self._from_json,
        'get_json': self._get_json,
        'get_json_barcode': self._get_json_barcode,
        'get_image_from_url': self._get_image_from_url,
        'sum_by_additional_order_line_field': self._sum_by_additional_order_line_field,
        'get_packingslip_image': self._get_packingslip_image,
        'get_shipping_label': self._get_shipping_label,
        'get_321': self._get_321,
        'is_qc': self._check_is_qc,
        'get_order_type_description': self._get_order_type_description,
        'get_line_product': self._get_line_product,
        'get_kohl_shipping_name': self._get_kohl_shipping_name,
        'get_barcode_list': self._get_barcode_list,
        'get_upc_barcode': self._get_upc_barcode,
        'get_upc_num': self._get_upc_num,
        'get_upc_barcode_list': self._get_upc_barcode_list,
    })
    self.rml_header = ""

def _ship_indicators(self, move, _type):
    # Types: shipping, handing, delivery
    result = ''
    _taxes = ast.literal_eval(_get_additional_order_line_field(self, move, 'taxBreakouts'))
    gst = ''
    pst = ''
    shipping = ''
    handing = ''
    delivery = ''
    for _tax in _taxes:
        if 'GST_HST_Total' in _tax['taxType']:
            gst = _tax['taxType'].replace('GST_HST_Total', '').replace('_','')
        if 'PST_Total' in _tax['taxType']:
            pst = _tax['taxType'].replace('PST_Total', '').replace('_','')
    if pst == 'HD' and gst == 'HD':
        shipping = 'B'
        handing = 'B'
    elif pst == 'H' and gst == 'H':
        handing = 'B'
    elif pst == 'D' and gst == 'D':
        delivery = 'B'
    elif pst == 'D' and gst == 'H':
        handing = 'T'
        delivery = 'P'
    elif pst == 'H' and gst == 'D':
        handing = 'P'
        delivery = 'T'
    elif pst == 'HD' and gst == 'H':
        handing = 'B'
        delivery = 'P'
    elif pst == 'H' and gst == 'HD':
        handing = 'B'
        delivery = 'T'
    elif pst == 'HD' and gst == 'D':
        handing = 'P'
        delivery = 'B'
    elif pst == 'D' and gst == 'HD':
        handing = 'T'
        delivery = 'B'
    elif pst == 'HD' and gst == '':
        handing = 'P'
        delivery = 'P'
    elif pst == '' and gst == 'HD':
        handing = 'T'
        delivery = 'T'
    elif pst == 'H' and gst == '':
        handing = 'P'
    elif pst == '' and gst == 'H':
        handing = 'T'
    elif pst == 'D' and gst == '':
        delivery = 'P'
    elif pst == '' and gst == 'D':
        delivery = 'T'

    result = {
        'shipping':shipping,
        'handing':handing,
        'delivery':delivery,
    }

    return result[_type]

def _get_total(self, move, _type):
    # Types: extended, total
    result = ''
    lineMerchandise = _get_additional_order_line_field(self, move, 'lineMerchandise') or 0
    creditAmount = _get_additional_order_line_field(self, move, 'creditAmount') or 0
    gst = _get_gst(self, move, _type) or 0
    pst = _get_pst(self, move, _type) or 0
    result = (float(lineMerchandise) + float(pst) + float(gst) ) - float(creditAmount)

    return str(round(result, 4))

def _get_qtytotal(self, move_lines):
    total = 0.0
    uom = move_lines[0].product_uom.name
    for move in move_lines:
        total += move.product_qty or 0.0
    return {'quantity': total, 'uom': uom}

def _get_total_customerCost_on_qty(self, picking):
    result = 0.0
    for move_line in picking.move_lines:
        result += (float(move_line.sale_line_id.customerCost or 0) * move_line.product_qty)

    return '$%.2f' % (result)

def _get_address_customer(self, picking, _field):
    # Fields: name1, address1, address2, city, state, postalCode, country
    result = ''
    res = _get_additional_order_field(self, picking, 'customer_address')
    if res:
        customer = ast.literal_eval(res)
        if customer.has_key(_field):
            result = customer[_field]

    return result

def _get_address_ship2(self, picking, _field):
    # Fields: name1, address1, address2, city, state, postalCode, country
    result = ''
    res = _get_additional_order_field(self, picking, 'ship2')
    if res:
        ship2 = ast.literal_eval(res)
        if ship2.has_key(_field):
            result = ship2[_field]

    return result

def _paginate_page(self, pickings, num):
    return_list = []
    for picking in pickings:
        iteration = int(math.ceil(len(picking.move_lines) / float(num)))
        for i in range(0,iteration):
            return_list.append(picking)
    return return_list

def _paginated_move_lines(self, items, num):
    num = int(num)
    begin = (int(self.page_count)) * num
    end = begin + num
    self.page_count += 1
    return items[begin:end]

def _tax_indicators(self, move, _type):
    # Types: total, extended
    result = ''
    gst = _get_gst(self, move, _type)
    pst = _get_pst(self, move, _type)
    if (not pst and not gst) or (pst == 0 and gst == 0):
        result = 'E'
    if (pst and gst) and pst > 0 and gst > 0:
        result = 'B'
    if (not pst or pst == 0) and (gst and gst > 0):
        result = 'P'
    if (not gst or gst == 0) and (pst and pst > 0):
        result = 'T'

    return result

def _get_merchant_sku_part(self, sku, part):
    # Types: 'D'- 'Div', 'I' - 'Item', 'S' - 'SKU'
    result = ''
    str_list = sku.split(' ')
    if sku:
        if len(str_list)<2:
            str_list.append('')
        if len(str_list)<3:
            str_list.append('')
        if part == 'D':
            result = str_list[0][:2]
        if part == 'I':
            result = str_list[1][:5]
        if part == 'SKU':
            result = str_list[2][:3]

    return result

def _time_format(self, full_date, format_str):
    result = ''
    if full_date and full_date != 'False':
        try:
            parsed_time = time.strptime(full_date, "%Y-%m-%d %H:%M:%S")
            result = time.strftime(format_str, parsed_time)
        except Exception:
            try:
                parsed_time = time.strptime(full_date, "%Y-%m-%d")
                result = time.strftime(format_str, parsed_time)
            except Exception:
                try:
                    parsed_time = time.strptime(full_date, "%Y%m%d")
                    result = time.strftime(format_str, parsed_time)
                except Exception:
                    result = full_date

    return result

def _get_gst(self, move, _type):
    # Types
    # TODO: fix GST/PST extended with value in key
    types = {'extended':'GST_HST_Extended','total':'GST_HST_Total'}
    result = ''
    _taxes = ast.literal_eval(_get_additional_order_line_field(self, move, 'taxBreakouts'))
    for _tax in _taxes:
        if types[_type] in _tax['taxType']:
            result = _tax['value']
            if _type == 'extended':
                result = _tax['taxType'].replace(types[_type]+'_','')

    return result

def _get_pst(self, move, _type):
    # Types
    # TODO: fix GST/PST extended with value in key
    types = {'extended':'PST_Extended','total':'PST_Total'}
    result = ''
    _taxes = ast.literal_eval(_get_additional_order_line_field(self, move, 'taxBreakouts'))
    for _tax in _taxes:
        if types[_type] in _tax['taxType']:
            result = _tax['value']
            if _type == 'extended':
                result = _tax['taxType'].replace(types[_type]+'_','')

    return result

def set_context(self, objects, data, ids, report_type = None):
    super(picking, self).set_context(objects, data, ids, report_type=report_type)
    customer_name = self.pool.get('stock.picking').browse(self.cr, self.uid, ids)[0].real_partner_id.name
    if customer_name == 'Kohls':
        self.rml_header = """
<header>
    <pageTemplate>
        <frame id="first" x1="1.3cm" y1="0.9cm" height="26.3cm" width="19.0cm"/>
        <pageGraphics>
            <translate dx="-4"/>
            <lineMode width="1"/>
                <lines>47 700  207 700   227 700  387 700   217 710  217 765   217 690  217 630  397 690  397 630
            </lines>
            <lineMode width="1" dash="4,3"/>
                <lines>397 765  577 765   577 765  577 700   577 700  397 700   397 700  397 765
            </lines>
        </pageGraphics>
    </pageTemplate>
</header>
"""
    else:
        self.rml_header = """
<header>
</header>
"""

def _get_custom_field(self, move_line, label, partner=True, size=True):
    value = False
    try:
        sale_line = move_line.sale_line_id
        size_id = size and sale_line.size_id and sale_line.size_id.id or False
        partner_id = partner and move_line.picking_id.real_partner_id.id or False
        product_id = sale_line.product_id.id
        value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, product_id, partner_id, label, size_id)
        # Get base line value if sized one not found
        if size_id and not value:
            value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, product_id, partner_id, label, False)
    except:
        value = False
    return value

def _get_acc_price(self, move_line,field):
    value = False
    param = '"%s"' % field
    try:
        product = move_line.sale_line_id.product_id.default_code or False
        if product:
            sql = """SELECT %s FROM acc_price WHERE "ITEM_" = %%s""" % (param)
            self.cr.execute(sql, (product, ))
            values = self.cr.dictfetchall()
            if values:
                for line in values:
                    value = line[field]
                    break
    except:
        value = False
    return value

def _sum_2_costs(self, op1, op2, minus=False):
    try:
        if op1:
            op1 = float(op1.replace('$',''))
    except ValueError:
        op1 = 0.0
    try:
        if op2:
            op2 = float(op2.replace('$',''))
    except ValueError:
        op2 = 0.0
    if minus:
        op2 *= -1
    return '$%.2f' % (op1 + op2)

def _triple_sum(self, op1, op2, op3):
    try:
        if op1:
            op1 = float(op1.replace('$',''))
    except ValueError:
        op1 = 0.0
    try:
        op2 = float(op2)
    except ValueError:
        op2 = 0.0
    try:
        op3 = float(op3)
    except ValueError:
        op3 = 0.0
    return '$%.2f' % (op1 + op2 + op3)

def _simple_sum(self, op1, op2=0):
    try:
        ret = sum(op1, op2)
    except:
        ret = False
    return ret


# DLMR-1930
def _sum_args(self, *args, **kvargs):
    result = 0
    for param in args:
        try:
            result += float(param)
        except Exception:
            pass
    return round(result, kvargs.get('rnd', 2))


def _sterling_lines(self, move_lines):
    ret = []
    comment_sku = ''
    for line in move_lines:
        if line.state == 'confirmed' or line.state=='done' or line.state=='assigned':
            ret.append([len(ret)+1, line, '', ''])
            if line.sale_line_id.lineRef3:
                partner_name = line.picking_id and line.picking_id.real_partner_id and line.picking_id.real_partner_id.name.strip() or ''
                if line.sale_line_id.lineRef3 == 'JRP':
                    if partner_name in ('Sterling Kay', 'Sterling Kay Outlet'):
                        comment_sku = '0342-13S-0000'
                    elif partner_name == 'Sterling Jared':
                        comment_sku = '0342-26J-0000'
                    elif partner_name == 'Sterling Family':
                        comment_sku = '0342-30F-ECOM'
                    ret.append([len(ret)+1, line, 'jrp', comment_sku])
                else:
                    if partner_name in ('Sterling Kay', 'Sterling Kay Outlet'):
                        comment_sku = '0352-13S-OSIS'
                    elif partner_name == 'Sterling Jared':
                        comment_sku = '0352-26J-OSIS'
                    ret.append([len(ret)+1, line, 'esp', comment_sku])
    for i in range(len(ret), 6):
        ret.append([i+1, False, '', ''])
    return ret

def _fmj_lines(self, move_lines):
    ret = []
    for line in move_lines:
        if line.state == 'confirmed' or line.state=='done' or line.state=='assigned':
            ret.append([len(ret)+1, line, ''])
    for i in range(len(ret), 10):
        ret.append([i+1, False, ''])
    return ret

def _indexed_lines(self, move_lines):
    ret = []
    for index, line in enumerate(move_lines):
        ret.append([index+1, line])
    return ret

def _get_service_type_ups(self, code):
    return self.pool.get('stock.picking').get_ups_servicetype_name(None, None, None, code)

# return formated string value of product price
# def _get_custom_price_value(self, move_line, label):
#     #import pdb; pdb.set_trace()
#     value = 0
#     try:
#         partner_id = move_line.partner_id.id
#         value = float(self.pool.get('product.product').get_val_by_label(self.cr, self.uid, move_line.product_id.id, partner_id, label))
#     except:
#         value = 0
#     return value

# def _get_custom_price(self, move_line, label):
    # return '$%.2f' % _get_custom_price_value(self, move_line, label)

# TODO: refactor all functions as this one:
# 1) add return format (if necessary)
# 2) add TypeError in except, it take place if value is None
# 3) remove "if <value> != None" conditions, they don't work anyway, see p. 2)
def _get_total_custom_price(self, price_unit, qty, line_sum=False, return_format='$%.2f'):
    try:
        price_unit = float(price_unit)
    except (ValueError, TypeError):
        price_unit = 0.0

    total_price = price_unit * qty

    if line_sum:
        self.__total_sub_price += total_price

    if not return_format:
        return total_price
    else:
        return return_format % total_price

def _get_total_shipCost_tax(self, shipCost, tax, qty):
    try:
        shipCost = float(shipCost)
    except ValueError:
        shipCost = 0.0
    try:
        tax = float(tax)
    except ValueError:
        tax = 0.0

    sub_cost = 0
    sub_tax = 0

    if shipCost != None:
        sub_cost = shipCost * qty

    if tax != None:
        sub_tax = tax * qty

    self.__total_sub_ship += sub_cost
    self.__total_sub_tax += sub_tax

def _str_to_int(self, str):
    try:
        return int(str)
    except Exception, e:
        raise

def _get_total_sub_price(self):
    return '$%.2f' % self.__total_sub_price

def _get_total_result(self, op1):
    if op1 == 'shipCost':
        return '$%.2f' % self.__total_sub_ship
    else:
        return '$%.2f' % self.__total_sub_tax

def _get_total_paid(self, shipping=0.0, gift_wrap=0.0, tax=0.0):
    try:
        shipping = float(shipping)
    except ValueError:
        shipping = 0.0

    try:
        gift_wrap = float(gift_wrap)
    except ValueError:
        gift_wrap = 0.0

    try:
        tax = float(tax)
    except ValueError:
        tax = 0.0

    return '$%.2f' % (self.__total_sub_price + shipping + gift_wrap + tax)

def _get_customerCost_sum(self, move_lines):
    return round(sum([float(line.sale_line_id.price_unit or '0.00') for line in move_lines]), 2)

def _get_customerCost(self, move_line):
    return round(float(move_line.sale_line_id.price_unit or '0.00'), 2)

def _get_shipCost(self, move_line):
    return round(float(move_line.sale_line_id.shipCost or '0.00'), 2)

def _get_subtotalCost(self, move_line):
    return round(float(self._get_customerCost(move_line) * move_line.product_qty), 2)

def _get_totalCost(self, move_line):
    return round(float(self._get_subtotalCost(move_line)) + float(self._get_shipCost(move_line)), 2)

def _get_totalCost_sum(self, move_lines):
    return round(sum([float(self._get_totalCost(line)) for line in move_lines]), 2)

def _get_retailCost(self, move_line):
    return round(float(move_line.sale_line_id.customerCost or '0.00'), 2)

def _get_subretailCost(self, move_line):
    return round(float(self._get_retailCost(move_line) * move_line.product_qty), 2)

def _get_retailCost_sum(self, move_lines):
    return round(sum([float(self._get_subretailCost(line)) for line in move_lines]), 2)

def _lines_sort_by_bin(self, move_lines):
    return sorted([move for move in move_lines if (move.state == 'confirmed' or move.state=='done' or move.state=='assigned')], key=lambda line: line.bin_id and line.bin_id.name or line.bin or '')

def _lines_sort_by_delmar_id(self, move_lines):
    return sorted([move for move in move_lines if (move.state == 'confirmed' or move.state == 'done' or move.state == 'assigned')], key=lambda line: line.product_id and line.product_id.default_code or '')

def _lines_sort_by_linenumb(self, move_lines):
    return sorted([move for move in move_lines if (move.state == 'confirmed' or move.state=='done' or move.state=='assigned')], key=lambda line: line.sale_line_id and self._str_to_int(line.sale_line_id.merchantLineNumber) or '')

def _lines_sort_by_asin(self, move_lines):
    return sorted([move for move in move_lines if (move.state == 'confirmed' or move.state=='done' or move.state=='assigned')], key=lambda line: line.sale_line_id and line.sale_line_id.merchantSKU or '')

def _get_order_comment(self, picking, comment_type):
    cr = self.cr
    uid = self.uid
    order_id = picking.sale_id and picking.sale_id.id or 0
    comment_obj = pooler.get_pool(cr.dbname).get('sale.order.sterling.comments')
    comments_ids = comment_obj.search(cr, uid, [('order_id', '=', order_id), ('type', '=', comment_type)])
    comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
    return comment

def _get_order_line_comment(self, line, comment_type, is_sale_order_line=False):
    cr = self.cr
    uid = self.uid
    if is_sale_order_line:
        line_id = line.id or 0
    else:
        line_id = line.sale_line_id and line.sale_line_id.id or 0
    comment_obj = pooler.get_pool(cr.dbname).get('sale.order.line.sterling.comments')
    comments_ids = comment_obj.search(cr, uid, [('line_id', '=', line_id), ('type', '=', comment_type)])
    comment = comments_ids and comment_obj.browse(cr, uid, comments_ids[0]).text or ''
    return comment

def _get_gmtime(self, format):
    return time.strftime(format, time.gmtime())

def _return_address(self, picking):
    addr = None
    addr_id = self.pool.get('stock.picking').get_addresses(self.cr, self.uid, picking.id, addresses=['return'])[picking.id]['return']
    if addr_id:
        addr = self.pool.get('res.partner.address').browse(self.cr, self.uid, addr_id)

    return addr

def _get_packing_code(self, move_line):
    packing_code = False
    try:
        location_id = move_line.location_id.id
        product = move_line.sale_line_id.product_id
        picking = move_line.picking_id
        warehouse_id = self.pool.get('stock.location').get_warehouse(self.cr, self.uid, location_id)
        warehouse_name = self.pool.get('stock.location').get_wh_loc(self.cr, self.uid, location_id)
        customer_id = picking.real_partner_id.id
        product_category_id = product.categ_id.id
        # cost of order line
        customer_cost = 0
        try:
            customer_cost = float(move_line.sale_line_id.customerCost)
        except:
            pass
        value = customer_cost or move_line.sale_line_id.price_unit or 0.0
        product_obj = self.pool.get('product.product')
        brand = product_obj.get_val_by_label(self.cr, self.uid, product.id, False, 'Brand', False) or False
        packing_codes = self.pool.get('stock.packing.material').get_packing_material(
            self.cr, self.uid, warehouse_id,
            product.id, product_category_id,
            customer_id, value, brand, location_id=location_id
        )
        packing_code = ' '.join([code['box_code'] for code in packing_codes if code['box_code']])

        # add set mark to code
        if move_line.set_product_id and move_line.set_product_id.id and move_line.picking_id and move_line.picking_id.is_set and move_line.picking_id.state != 'done':
            packing_code = ' '.join([packing_code, '(s!)'])

        if warehouse_name[0] == 'CAFERNDP':
            packing_code = packing_code + ' NDP'

    except:
        pass
    return packing_code

def _get_bluestem_shipping(self, merchandise_amount, amount_total, qty=1):
    try:
        qty = int(qty)
        merchandise_amount = float(merchandise_amount)
        amount_total = float(amount_total)
    except ValueError:
        qty = 1
        merchandise_amount = 0.0
        amount_total = 0.0
    return '%.2f' % (amount_total/qty - merchandise_amount)

def setCompany(self, company_id):
    user_obj = pooler.get_pool(self.cr.dbname).get('res.users')
    if company_id:
        self.localcontext['company'] = company_id
        self.localcontext['logo'] = company_id.logo
        self.localcontext['uid'] = user_obj.browse(self.cr, self.uid, self.uid)
#         self.rml_header = """
# <header>
#     <pageTemplate>
#         <frame id="first" x1="1.3cm" y1="0.9cm" height="26.3cm" width="19.0cm"/>
#         <pageGraphics>
#             <drawRightString x="19.0cm" y="2.0cm"> [[ formatLang(time.strftime("%Y-%m-%d %H:%M:%S"), date=True) ]] [[ uid.name ]]</drawRightString>
#         </pageGraphics>
#     </pageTemplate>
# </header>
# """
        self.rml_header = company_id.rml_header
        self.rml_header2 = company_id.rml_header2
        self.rml_header3 = company_id.rml_header3
        self.logo = company_id.logo

def _get_additional_order_field(self, picking, field_name):
    res = ''
    sale_order = picking.sale_id
    if sale_order and sale_order.additional_fields:
        for item in sale_order.additional_fields:
            if item.name == field_name:
                res = item.value
                break
    return res


def _check_is_qc(self, picking):
    res = False
    sh_state_code = picking.sale_id and picking.sale_id.partner_shipping_id and picking.sale_id.partner_shipping_id.state_id.code or ''
    if sh_state_code == 'QC':
        res = True
    return res

def _get_kohl_shipping_name(self, picking):
    # default value
    shipping_type_name = picking.carrier
    if picking.sale_id:
        if 'UNSP_SC' in picking.sale_id.carrier:
            # set static shipping name for Unspecified SC
            shipping_type_name = 'Continental US - Priority Air'
        elif 'UNSP_CG' in picking.sale_id.carrier:
            # set static shipping name for Unspecified CG
            shipping_type_name = 'Continental US - Standard Ground'
            # exception for hawaii, alaska, etc..
            if picking.address_id and picking.address_id.state_id and picking.address_id.state_id.code in ['AK','HI','AA','AE','AP']:
                shipping_type_name = 'Alaska/Hawaii & APO/FPO - Standard Ground'

    # add flat_rate
    if picking.flat_rate_priority_id:
        shipping_type_name = '{0} {1}'.format(shipping_type_name, picking.flat_rate_priority_id.name)

    return shipping_type_name

def _get_additional_order_line_field(self, move_line, field_name):
    res = ''
    sale_line = move_line.sale_line_id
    if sale_line and sale_line.additional_fields:
        for item in sale_line.additional_fields:
            if item.name == field_name:
                res = item.value
                break
    return res


# DLMR-1930
def _get_additional_order_line_field_sum(self, move_lines=None, field_name=None):
    summ = 0
    for move in move_lines:
        summ += float(self._get_additional_order_line_field(move, field_name))
    return round(summ, 2)


def _get_packing_message(self, return_param):
    if return_param == 1:
        return self.__message
    else:
        if self.__message == False:
            self.__message = True

def _get_account_number(self, customer_id ,shp_service):
    account_number = ''
    accounts = self.pool.get('res.partner.account')
    service = shp_service.lower()
    accounts_ids = accounts.search(self.cr, self.uid, [('service', '=', service), ('partner_id', '=', customer_id)])
    if accounts_ids:
        account_number = accounts.read(self.cr, self.uid, accounts_ids[0], ['account']).get('account', False)
    return account_number

def _get_order_account_number(self, picking):
    account_number = ''
    shp_service = picking.shp_service
    if shp_service.lower() in ('ups', 'fedex', 'purolator'):
        account_number = picking.shp_account
    return account_number

def _get_total_retail_price(self, retail_price, qty):
    result = None
    try:
        retail_price = float(retail_price)
        qty = int(qty)
        result = "$ {total}".format(total=retail_price * qty)
    except ValueError:
        result = 0
    finally:
        return result

def _from_json(self, json_line, separator='\n'):
    decoder = JDecode()
    value_array = decoder.decode(json_line)
    values = [el for el in value_array if el != '.']

    return separator.join(values)

def _get_json(self, json_line, key='sscc_id'):
    decoder = JDecode()
    return decoder.decode(json_line)[key]

def _get_json_barcode(self, json_line):
    decoder = JDecode()
    barcode = decoder.decode(json_line)['sscc_id']
    return '(' + barcode[0:2] + ') ' + barcode[2:3] + ' ' + barcode[3:10] + ' ' + barcode[10:19] + ' ' + barcode[19:20]

def _get_image_from_url(self, url):
    result = False
    if not url.startswith('http://'):
        url = 'http://'+url
    response = urllib2.urlopen(url)
    if response.code == 200:
        img = response.read()
        result = b64encode(img)

    return result

def _sum_by_additional_order_line_field(self, picking, name):
    result = 0
    for move_line in picking.move_lines:
        value = self._get_additional_order_line_field(move_line, name)
        try:
            result += float(value) * move_line.product_qty
        except:
            pass

    return result

def _get_packingslip_image(self, picking, page_num=1):
    p_export_obj = self.pool.get('picking.export')
    po = picking.name
    sql = """select img_file{}
            from picking_export
        where 1=1 
            and po = '{}'
    """.format(page_num, po)
    self.cr.execute(sql);
    img_file = self.cr.fetchall()
    if img_file:
        return img_file[0][0]
    else:
        return ""

def _get_shipping_label(self, picking):
    p_export_obj = self.pool.get('picking.export')
    # import pdb; pdb.set_trace()
    po = picking.name
    sql = """select zpl_file
            from picking_export
        where 1=1 
            and po = '{}'
    """.format(po)
    self.cr.execute(sql);
    zpl_file = self.cr.fetchall()

    if zpl_file:
        zpl_file = zpl_file[0][0]
        zpl_file = base64.decodestring(zpl_file)
        # return shipping_label
        url = 'http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/'
        files = {'file' : zpl_file}
        #headers = {'Accept' : 'application/pdf'} # omit this line to get PNG images back
        response = requests.post(url, files=files, stream=True)
        upc_image = ''
        if response.status_code == 200:
            response.raw.decode_content = True
            with open('/tmp/shipping_label.png', 'wb') as out_file: # change file name for PNG images
                shutil.copyfileobj(response.raw, out_file)
            with open("/tmp/shipping_label.png", "rb") as imageFile:
                upc_image = base64.b64encode(imageFile.read())
            os.system("rm -f upca_barcode.png")
        else:
            print('Error: ' + response.text)
        return upc_image

        # shipping_label = base64.decodestring(shipping_label)
        # if shipping_label:
        #     url = "http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
        #     headers = {"Content-type": "application/x-www-form-urlencoded"}
        #     response = requests.post(url=url, headers=headers, data=shipping_label)
        #     png_label = response.text
        #     response.close()
        #     f = open('/tmp/shipping_label.png',"w+")
        #     f.write(png_label)
        #     f.close()

        #     upc_image = ""
        #     with open("/tmp/shipping_label.png", "rb") as imageFile:
        #         upc_image = base64.b64encode(imageFile.read())
        #     os.system("rm -f upca_barcode.png")
            

            # prg_label = b64encode(png_label)
           
            # grfs = GRF.from_zpl(shipping_label)
            # for i, grf in enumerate(grfs):
            #     png_label = grf.to_image()
            
    return ""



def _get_321(self, picking, price=None):
    picking_obj = self.pool.get('stock.picking')
    ids = self.context.get('active_ids', [])
    data = []
    if ids:
        self.cr.execute("""
            select address_grp_fg 
                from stock_picking
            where 1=1 
                and id in %s
        """, (tuple(ids),));
        addresses = self.cr.fetchall()

        self.cr.execute("""
            select address_grp_fg,array_to_string(array_agg(id), ',') as ids,sum(total_price) as total_price from stock_picking 
                where address_grp_fg in %s
                and stock_picking."type" = 'out'
                    and(
                        stock_picking."state" in(
                            'picking',
                            'outcode_except',
                            'shipped',
                            'returned',
                            'done',
                            'bad_format_tracking'
                        )
                        )
                    and ((stock_picking."state" not in(
                                        'done',
                                        'returned',
                                        'cancel'
                                    )
                                    or stock_picking."state" is null
                    ) or(
                                (
                                    stock_picking."state" = 'done'
                                )
                                and(
                                    stock_picking."shp_date" >= now() at time zone 'utc' - interval '1 day'
                                )
                        )
                    )
                group by address_grp_fg
                having count(id) > 1
                ;
        """, (tuple(addresses),))
        data = self.cr.dictfetchall()

    if self._is_comb(picking, data):
        result = 'COMB '
        if picking.real_partner_id.ref in ['QVC']:
            result = 'C '
        result += '321 ' if self._is_comb321(picking, data) else ''
    else:
        result = '321 ' if picking_obj.is_321(self.cr, picking, price) else ''

    if picking.sale_id.packslip_message and len(picking.sale_id.packslip_message) > 300:
        picking.sale_id.packslip_message = picking.sale_id.packslip_message[0:300]

    return result


def _is_comb(self, picking, data_disc):
    for data in data_disc:
        data_ids = data.get('ids')
        if data_ids.split(',') > 1 and str(picking.id) in data.get('ids'):
            return True

    return False

def _is_comb321(self, picking, data_disc):
    for data in data_disc:
        data_ids = data.get('ids')
        if data_ids.split(',') > 1 and str(picking.id) in data.get('ids') and data.get('total_price') < 800:
            return True

    return False


def _get_order_type_description(self, picking):
    code = picking.sale_id.carrier
    if (code == 'CG_01') or (code == 'CG_SE_01'):
        result = 'Post Office Delivery'
    elif code == 'ASLR_R8':
        result = 'Locker Box Delivery'
    else:
        result = 'Home Delivery'
    return result


def _get_line_product(self, move_line, size=True):
    # Take product from sale line
    product = move_line.sale_line_id
    # Add size where needed
    size_slug = (product.size_id and ('/' + product.size_id.name.replace('.0', '')) or '') if size else ''

    return product.product_id.default_code + size_slug or ''

# def _get_barcode_list(self, po_number, move_lines):
#     result = []
#     i = 0
#     for value in move_lines:
#         qty = int(float(value.product_qty))
#         size = value.sale_line_id.size_id.name.replace('.0','')
#         vendor_sku = value.sale_line_id.vendorSku
#         result.append([])
#         result[i].append({
#             'type': 'parent',
#             'po': po_number,
#             'vendor_sku': vendor_sku,
#             'size': size,
#             'qty': qty,
#         })
#         if qty > 0:
#             i = i + 1
#             for j in range(0, int(float(qty))):
#                 result.append([])
#                 result[i].append({
#                     'type': 'child',
#                     'customer_sku': self._get_custom_field(value, 'Customer SKU'),
#                     'name': value.name,
#                     'vendor_sku': vendor_sku,
#                     'size': size,
#                 })
#         i = i + 1
#
#     import pdb;pdb.set_trace()


def _get_upc_barcode_list(self, picking_id, limit=None):
    move_obj = self.pool.get('stock.move')
    pick_obj = self.pool.get('stock.picking')
    prod_obj = self.pool.get('product.product')

    picking = pick_obj.browse(self.cr, self.uid, picking_id)
    if picking.state in ['done']:
        move_ids = move_obj.search(self.cr, self.uid, [('picking_id', '=', picking.id)], order='is_set ASC, bin ASC')
    else:
        move_ids = move_obj.search(self.cr, self.uid, ['|', '&', ('set_parent_order_id', '=', picking.id), ('is_set', '=', True), '&', ('picking_id', '=', picking.id), ('is_set', '=', False)], order='is_set ASC, bin ASC')
    move_lines = move_obj.browse(self.cr, self.uid, move_ids)
    item_num = 0
    rows, row, items = [], [], []
    for line in move_lines:
        if line.state == 'confirmed' or line.state == 'done' or line.state == 'assigned' or picking.state not in ['done']:
            delmar_id = line.sale_line_id and line.sale_line_id.vendorSku or line.sale_line_id.product_id.default_code or None
            size = line.sale_line_id.size_id.name.replace('.0', '') if line.sale_line_id.size_id else 'OS'
            upc = prod_obj.get_val_by_label(self.cr, self.uid, line.sale_line_id.product_id.id, picking.real_partner_id.id, 'UPC', line.sale_line_id.size_id and line.sale_line_id.size_id.id or False) or None
            item = {
                'delmar_id': delmar_id,
                'size': size,
                'upc': upc
            }
            items.extend([item] * int(line.product_qty + 1))
    # break for 3 items per line
    for item in items:
        row.append(item)
        item_num += 1
        if len(row) == 3 or item_num == len(items):
            rows.append(row)
            row = []

    return rows

def _get_upc_num(self, picking, num=1):
    move_obj = self.pool.get('stock.move')
    pick_obj = self.pool.get('stock.picking')
    prod_obj = self.pool.get('product.product')
    move_ids = move_obj.search(self.cr, self.uid, [('picking_id', '=', picking.id)])
    move_lines = move_obj.browse(self.cr, self.uid, move_ids)
    upc_list = []
    for line in move_lines:
        upc = prod_obj.get_val_by_label(self.cr, self.uid, line.sale_line_id.product_id.id, picking.real_partner_id.id, 'UPC', line.sale_line_id.size_id and line.sale_line_id.size_id.id or False) or ""
        product_qty = int(line.product_qty)
        for i in range(0, product_qty):
            upc_list.append(upc)
    if len(upc_list) >= num:
        return upc_list[num-1]
    return ""

def _get_upc_barcode(self, picking_id, time=1):
    import pdb; pdb.set_trace()
    move_lines = self._get_upc_num(picking_id)
    if len(move_lines) > time:
        UPCA=barcode.get_barcode_class('upca')
        upca = UPCA(upc, writer=ImageWriter())
        upca.save('/tmp/upca_barcode')
        upc_image = ""
        with open("/tmp/upca_barcode.png", "rb") as imageFile:
            upc_image = base64.b64encode(imageFile.read())
        os.system("rm -f upca_barcode.png")

    return upc_image


def _get_barcode_list(self, po_number, picking_id):
    # find sorted move_lines
    sm_obj = self.pool.get('stock.move')
    pick = self.pool.get('stock.picking').browse(self.cr, self.uid, picking_id)
    if pick.state in ['done']:
        sm_ids = sm_obj.search(self.cr, self.uid, [('picking_id', '=', picking_id)], order='is_set ASC, bin ASC')
    else:
        sm_ids = sm_obj.search(self.cr, self.uid, ['|', '&', ('set_parent_order_id', '=', picking_id), ('is_set', '=', True), '&', ('picking_id', '=', picking_id), ('is_set', '=', False)], order='is_set ASC, bin ASC')
    move_lines = sm_obj.browse(self.cr, self.uid, sm_ids)
    # initial values
    result = []
    one_line_array = []
    i_ola = 0
    i_res = 0
    for value in move_lines:
        if value.state == 'confirmed' or value.state == 'done' or value.state == 'assigned' or pick.state not in ['done']:
            qty = int(float(value.product_qty))
            size = value.sale_line_id.size_id.name.replace('.0', '') if value.sale_line_id.size_id else False
            vendor_sku = value.sale_line_id.vendorSku
            image = value.sale_line_id.product_id.product_image

            one_line_array.append({
                'type': 'parent',
                'product_image': image,
                'po': po_number,
                'vendor_sku': vendor_sku,
                'size': size,
                'qty': qty,
            })
            i_ola = i_ola + 1

            if i_ola == 3:
                result.append([])
                result[i_res].append(one_line_array)
                i_res = i_res + 1
                one_line_array = []
                i_ola = 0

            if qty > 0:
                for j in range(0, int(float(qty))):
                    one_line_array.append({
                        'type': 'child',
                        'product_image': image,
                        'customer_sku': self._get_custom_field(value, 'Customer SKU'),
                        'name': value.name[:50],
                        'vendor_sku': vendor_sku,
                        'size': size,
                    })
                    i_ola = i_ola + 1

                    if i_ola == 3:
                        result.append([])
                        result[i_res].append(one_line_array)
                        i_res = i_res + 1
                        one_line_array = []
                        i_ola = 0

    if i_ola > 0:
        for i in range(i_ola, 3):
            one_line_array.append({
                'type': 'none',
            })
        result.append([])
        result[i_res].append(one_line_array)

    return result


def _get_total_lines(self, manifest):
    """ gets total lines in the 321 Driver report DLMR-50 """
    total = 0
    for sub_manifest in manifest:
        for tracking_list in sub_manifest.line_ids:
            picking = tracking_list.picking_id
            total += len(picking.move_lines)
    return total

def _sort_orders_by_scn(self, manifest):
    """ sorts items in the 321 Driver report DLMR-50 by its SCN number """
    move_lines = []
    for sub_manifest in manifest:
        box_id = ''
        for tracking_list in sub_manifest.line_ids:
            if tracking_list.sub_manifest_id:
                box_id = tracking_list.sub_manifest_id.name or ''
            picking = tracking_list.picking_id
            for move in picking.move_lines:
                move.picking = picking
                move.box_id = box_id
                move_lines.append(move)
    move_lines.sort(key=lambda x: x.export_ref_ca)
    for i in range(len(move_lines)):
        if i % 2 == 1:
            move_lines[i].table_style = "Table2"
        else:
            move_lines[i].table_style = "Table1"
    return move_lines


picking.__init__ = __init__
picking.set_context = set_context
picking.setCompany = setCompany
picking.get_qtytotal = _get_qtytotal
picking._get_total_customerCost_on_qty = _get_total_customerCost_on_qty
picking._get_custom_field = _get_custom_field
picking._get_acc_price = _get_acc_price
picking._get_service_type_ups = _get_service_type_ups
picking._sum_2_costs = _sum_2_costs
picking._triple_sum = _triple_sum
picking._simple_sum = _simple_sum
picking._sum_args = _sum_args
picking._sterling_lines = _sterling_lines
picking._fmj_lines = _fmj_lines
picking._indexed_lines = _indexed_lines
# picking._get_custom_price = _get_custom_price
picking._get_total_custom_price = _get_total_custom_price
picking._get_total_shipCost_tax = _get_total_shipCost_tax
picking._get_total_sub_price = _get_total_sub_price
picking._get_total_result = _get_total_result
picking._get_total_paid = _get_total_paid
picking._get_customerCost_sum = _get_customerCost_sum
picking._get_customerCost = _get_customerCost
picking._get_shipCost = _get_shipCost
picking._get_totalCost = _get_totalCost
picking._get_subtotalCost = _get_subtotalCost
picking._get_totalCost_sum = _get_totalCost_sum
picking._lines_sort_by_bin = _lines_sort_by_bin
picking._lines_sort_by_delmar_id = _lines_sort_by_delmar_id
picking._lines_sort_by_asin = _lines_sort_by_asin
picking._lines_sort_by_linenumb = _lines_sort_by_linenumb
picking._get_gmtime = _get_gmtime
picking._get_order_comment = _get_order_comment
picking._get_order_line_comment = _get_order_line_comment
picking._str_to_int = _str_to_int
picking._get_retailCost = _get_retailCost
picking._get_subretailCost = _get_subretailCost
picking._get_retailCost_sum = _get_retailCost_sum
picking._get_packing_code = _get_packing_code
picking._get_bluestem_shipping = _get_bluestem_shipping
picking._get_additional_order_field = _get_additional_order_field
picking._get_additional_order_line_field = _get_additional_order_line_field
picking._get_additional_order_line_field_sum = _get_additional_order_line_field_sum
picking._get_merchant_sku_part = _get_merchant_sku_part
picking._time_format = _time_format
picking._get_gst = _get_gst
picking._get_pst = _get_pst
picking._get_total = _get_total
picking._tax_indicators = _tax_indicators
picking._ship_indicators = _ship_indicators
picking._get_address_ship2 = _get_address_ship2
picking._paginate_page = _paginate_page
picking._paginated_move_lines = _paginated_move_lines
picking._get_address_customer = _get_address_customer
picking._get_packing_message = _get_packing_message
picking._return_address = _return_address
picking._get_account_number = _get_account_number
picking._get_order_account_number = _get_order_account_number
picking._get_total_retail_price = _get_total_retail_price
picking._from_json = _from_json
picking._get_json = _get_json
picking._get_json_barcode = _get_json_barcode
picking._get_image_from_url = _get_image_from_url
picking._sum_by_additional_order_line_field = _sum_by_additional_order_line_field
picking._get_packingslip_image = _get_packingslip_image
picking._get_shipping_label = _get_shipping_label
picking._get_321 = _get_321
picking._check_is_qc = _check_is_qc
picking._get_order_type_description = _get_order_type_description
picking._get_line_product = _get_line_product
picking._get_kohl_shipping_name = _get_kohl_shipping_name
picking._get_upc_num = _get_upc_num
picking._get_upc_barcode = _get_upc_barcode
picking._get_barcode_list = _get_barcode_list
picking._get_upc_barcode_list = _get_upc_barcode_list
picking._is_comb = _is_comb
picking._is_comb321 = _is_comb321

manifest.__init__
manifest._get_total_lines = _get_total_lines
manifest._sort_orders_by_scn = _sort_orders_by_scn


report_sxw.report_sxw(
    'report.stock.picking.gift',
    'stock.picking',
    'addons/delmar_reports/static/reports_examples/customer_report_Sterling_Kay.xml',
    parser=picking
)

report_sxw.report_sxw(
    'report.flash.stock.picking.list',
    'stock.picking',
    'addons/delmar_reports/static/reports_examples/customer_report_flash_default.xml',
    parser=picking
)

report_sxw.report_sxw(
    'report.stock.picking.barcode',
    'stock.picking',
    'addons/delmar_reports/static/reports_examples/customer_report_default_barcode.xml',
    parser=picking
)

report_sxw.report_sxw(
    'report.stock.picking.barcode.upc',
    'stock.picking',
    'addons/delmar_reports/static/reports_examples/customer_report_default_upc_barcode.xml',
    parser=picking
)

report_sxw.report_sxw(
    'report.stock.picking.all.materials',
    'stock.picking',
    'addons/delmar_reports/static/reports_examples/customer_report_default_shipping_label.xml',
    parser=picking,
    header=False
)

report_sxw.report_sxw(
    'report.stock.picking.driver',
    'stock.picking.manifest',
    'addons/delmar_reports/static/reports_examples/321_driver_report.rml',
    parser=manifest,
    header=False
)
