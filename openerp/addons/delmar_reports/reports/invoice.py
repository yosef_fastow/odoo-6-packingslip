# -*- coding: utf-8 -*-
##############################################################################


import time
from report import report_sxw


class invoice(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(invoice, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'float': float,
            'contains_category': self._contains_category,
            '_get_custom_field': self._get_custom_field,
            '_get_custom_field_so_line': self._get_custom_field_so_line,
            '_get_customerCost_total': self._get_customerCost_total,
            '_get_customerCost_total_new': self._get_customerCost_total_new,
            '_get_subtotal_customer_price': self._get_subtotal_customer_price,
            '_get_total_customer_price': self._get_total_customer_price,
            '_get_total_customer_price_sum': self._get_total_customer_price_sum,
            '_get_total_customer_price_order_sum': self._get_total_customer_price_order_sum,
            'get_customerCost': self._get_customerCost,
            'get_shipCost': self._get_shipCost,
            'get_subtotalCost': self._get_subtotalCost,
            'get_subtotal_customer_Cost': self._get_subtotal_customer_Cost,
            'get_totalCost': self._get_totalCost,
            'get_totalCost_sum': self._get_totalCost_sum,
            'get_totalCostOrder_sum': self._get_totalCostOrder_sum,
            'get_tax_value': self._get_tax_value,
            'get_freight': self._get_freight,
            'get_discount': self._get_discount,
            'get_percent_of_price': self._get_percent_of_price,
            'get_partner_name': self._get_partner_name,
            'get_status_tax_header': self._get_status_tax_header,
            'get_rowHeights': self._get_rowHeights,
            'get_desc_rowHeights': self._get_desc_rowHeights,
            'get_321': self._get_321,
            'get_system_param': self._get_system_param,
            'is_canadian_customer': self._is_canadian_customer,
            'is_delmar_first_canadian': self._is_delmar_first_canadian,
            'is_not_delmar_first_canadian': self._is_not_delmar_first_canadian,
            'get_product_manufacture': self._get_product_manufacture,
            'get_line_product': self._get_line_product,
        })
        self.rml_header = ""

    def set_context(self, objects, data, ids, report_type=None):
        super(invoice, self).set_context(objects, data, ids, report_type=report_type)
        self.rml_header = """
    <header>
    </header>
    """

    def _get_custom_field(self, move_line, label):
        value = False
        try:
            size_id = move_line.size_id and move_line.size_id.id or False
            partner_id = move_line.picking_id.real_partner_id.id
            value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, move_line.product_id.id, partner_id, label, size_id)
        except Exception:
            value = False
        return value

    def _get_custom_field_so_line(self, move_line, label, size=True):
        value = False
        try:
            size_id = size and move_line.sale_line_id and move_line.sale_line_id.size_id.id or None
            partner_id = move_line.picking_id.real_partner_id.id or None
            product_id = move_line.sale_line_id and move_line.sale_line_id.product_id.id or None
            main_partner_id = self.pool.get('res.partner').get_main_customer(self.cr, self.uid, partner_id)
            value = self.pool.get('product.product').get_val_by_label(self.cr, self.uid, product_id, main_partner_id, label, size_id)
        except Exception:
            value = False
        return value

    def _get_customerCost_total(self, move_lines):
        return round(sum([self._get_subtotalCost(line) for line in move_lines]), 2)

    def _get_price_unit(self, move_line):
        return round(move_line.sale_line_id.price_unit or 0.00, 2)

    def _get_customerCost(self, move_line):
        value = move_line.sale_line_id.price_unit
        try:
            retail_value = float(move_line.sale_line_id.customerCost)
            if retail_value:
                value = retail_value
        except Exception:
            pass
        return round(value or 0.00, 2)

    def _get_shipCost(self, move_line):
        return round(float(move_line.sale_line_id.shipCost or '0.00'), 2)

    def _get_subtotalCost(self, move_line):
        return round(float(self._get_price_unit(move_line) * move_line.product_qty), 2)

    def _get_subtotal_customer_Cost(self, move_line):
        value = move_line.sale_line_id.price_unit
        try:
            retail_value = float(move_line.sale_line_id.customerCost)
            if retail_value:
                value = retail_value
        except Exception:
            pass
        value = value or 0.00
        return round(float(value * move_line.product_qty), 2)

    def _get_customerCost_total_new(self, move_lines):
        return round(sum([self._get_subtotal_customer_Cost(line) for line in move_lines]), 2)

    def _get_subtotal_customer_price(self, move_line):
        return round(float(self._get_custom_field_so_line(move_line, 'Customer Price') or 0) * move_line.product_qty, 2)

    def _get_totalCost(self, move_line):
        return round(float(self._get_subtotalCost(move_line)), 2)

    def _get_total_customer_price(self, move_line):
        return round(float(self._get_subtotal_customer_price(move_line)), 2)

    def _get_totalCost_sum(self, picking, type_result='float'):
        result = round(sum([float(self._get_totalCost(line)) for line in picking.move_lines]), 2)
        return self._get_result(result, type_result)

    def _get_total_customer_price_sum(self, picking, type_result='float'):
        result = round(sum([float(self._get_total_customer_price(line)) for line in picking.move_lines]), 2)
        return self._get_result(result, type_result)

    def _get_tax_value(self, picking, name_tax, type_result='float', context=None):
        if(context is None):
            context = {}
        context.update({'for_pdf_invoice': True})
        result = None
        discount = self._get_discount(picking)
        freight = self._get_freight(picking)

        sale_taxes = self.pool.get("delmar.sale.taxes").compute_stock_picking_taxes(self.cr, self.uid, picking.id, discount, freight, context=context)
        general_tax = sale_taxes.get(name_tax, None)
        if(general_tax is not None):
            result = self._get_result(general_tax, type_result)
        else:
            result = self._get_result(False, type_result)

        return result

    def _contains_category(self, picking, category_name='diamond'):
        for move_line in picking.move_lines:
            product = move_line.product_id.prod_sub_type or ''
            category = move_line.product_id.categ_id
            if category_name in category.name.lower() or category_name in product.lower():
                return 1
            pack_line_ids = move_line.product_id.pack_line_ids
            if pack_line_ids:
                for pack_line in pack_line_ids:
                    product = pack_line.product_id.prod_sub_type or ''
                    category = pack_line.product_id.categ_id
                    if category_name in category.name.lower() or category_name in product.lower():
                        return 1
        return 0

    def _get_freight(self, picking, type_result='float'):
        freight = 0
        # Avi: Please add credit for freight ...
        # ... please only do this for these two invoices below as an exception,
        # as normally we do not want to refund freight
        is_PIIORD_0730758_0_WR = picking.name in ('PIIORD-0730758-0-WR', 'PIIORD-0729789-0-WR','PIIORD-0803310-0R','DRB0825-002161-0R')
        # Kim: Delmar only refund the merchandise. Do not refund freight.
        if (picking.state != 'returned' or is_PIIORD_0730758_0_WR) and not picking.real_partner_id.invoice_without_ship_cost:
            freight = picking.shp_handling

        return self._get_result(freight, type_result)

    def _get_percent_of_price(self, picking, percents=None, type_result='float'):
        if not percents:
            percents = [0.0]

        if not isinstance(percents, (list)):
            percents = [percents]

        total_price = float(self._get_totalCost_sum(picking))
        for percent in percents:
            discount = percent * total_price / 100
            total_price -= discount

        # FIXME: facking bicycle (reason - abs in _get_result)
        sign = 1 if picking.state == 'returned' else -1
        return self._get_result(sign * discount, type_result)

    def _get_discount(self, picking, type_result='float'):
        # TODO: For PII need to show 2 different successive discounts (2%, 1.5%)
        # They described in invoice template.
        # On customer level stored total discount = 3.47%
        # Best way - change float to list of discouts and rewrite corresponding methods
        discounts = [picking.real_partner_id.discount]
        return self._get_percent_of_price(picking, percents=discounts, type_result=type_result)

    def _get_totalCostOrder_sum(self, picking, type_result='float'):
        result = round(
            (
                self._get_totalCost_sum(picking, 'float') +
                self._get_freight(picking, 'float') -
                self._get_discount(picking, 'float') +
                self._get_tax_value(picking, 'gst', 'float') +
                self._get_tax_value(picking, 'qst', 'float') +
                self._get_tax_value(picking, 'hst', 'float') +
                self._get_tax_value(picking, 'pst', 'float') + float(0.001)
            ),
            2
        )
        return self._get_result(result, type_result)

    def _get_total_customer_price_order_sum(self, picking, type_result='float'):
        result = round(
            (
                self._get_total_customer_price_sum(picking, 'float') +
                self._get_freight(picking, 'float') -
                self._get_discount(picking, 'float') +
                self._get_tax_value(picking, 'gst', 'float') +
                self._get_tax_value(picking, 'qst', 'float') +
                self._get_tax_value(picking, 'hst', 'float') +
                self._get_tax_value(picking, 'pst', 'float')
            ),
            2
        )
        return self._get_result(result, type_result)

    def _get_partner_name(self, picking):
        partner_name = ""
        if(picking and picking.real_partner_id and picking.real_partner_id.name):
            partner_name = picking.real_partner_id.name
        return str(partner_name)

    def _get_status_tax_header(self, picking, name_tax, context=None):
        if(context is None):
            context = {}
        context.update({'for_pdf_invoice': True})
        discount = self._get_discount(picking)
        freight = self._get_freight(picking)
        sale_taxes = self.pool.get("delmar.sale.taxes").compute_stock_picking_taxes(self.cr, self.uid, picking.id, discount, freight, context=context)
        general_tax = sale_taxes.get(name_tax, None)
        return (general_tax is not None)

    def _get_rowHeights(self, picking, context=None):
        if(context is None):
            context = {}
        discount = self._get_discount(picking)
        freight = self._get_freight(picking)
        sale_taxes = self.pool.get("delmar.sale.taxes").compute_stock_picking_taxes(self.cr, self.uid, picking.id, discount, freight, context=context)
        len_s_t = len(sale_taxes) - 1
        return str(56.5 - 11*len_s_t)

    def _get_desc_rowHeights(self, move_line, context=None):
        if(context is None):
            context = {}
        res = 20
        if(
            (
                move_line and
                move_line.sale_line_id and
                move_line.sale_line_id.name
            ) or
            (
                move_line and
                move_line.sale_line_id and
                move_line.sale_line_id.product_id and
                move_line.sale_line_id.product_id.name
            )
        ):
            res += 40
        return str(res)

    def _get_result(self, _result, _type_result):
        positive_ = "${:.2f}"
        negative_ = "-${:.2f}"
        returned_result = None
        if(_type_result == 'float'):
            if(not _result):
                returned_result = 0
            else:
                if(_result < 0):
                    _result = abs(_result)
                returned_result = _result
        else:
            if(_result is False):
                returned_result = False
            else:
                float_result = abs(_result)
                if(_result < 0):
                    returned_result = negative_.format(float_result)
                else:
                    returned_result = positive_.format(float_result)
        return returned_result

    def _get_321(self, picking, price=None):
        picking_obj = self.pool.get('stock.picking')
        result = '321 ' if picking_obj.is_321(self.cr, picking, price) else ''
        return result

    def _get_system_param(self, param):
        return self.pool.get('ir.config_parameter').get_param(self.cr, self.uid, param)

    def _is_canadian_customer(self, picking):
        is_canadian = False
        try:
            is_canadian = picking.real_partner_id.company_id.country_id.code == 'CA'
        except:
            print "ERROR: %s. Can't determine company's country" % (picking.name)
        return is_canadian

    def _is_delmar_first_canadian(self, picking):
        return picking.real_partner_id.company_id.name.lower() == 'delmar first canadian'

    def _is_not_delmar_first_canadian(self, picking):
        return picking.real_partner_id.company_id.name.lower() != 'delmar first canadian'

    def _get_product_manufacture(self, move_line):
        origin = 'China'
        product = move_line.sale_line_id.product_id
        if product.product_tmpl_id and product.product_tmpl_id.prod_manufacture:
            origin = product.product_tmpl_id.prod_manufacture
        return origin.capitalize()

    def _get_line_product(self, move_line, size=True):
        product = move_line.sale_line_id
        size_slug = (product.size_id and ('/' + product.size_id.name.replace('.0', '')) or '') if size else ''
        return product.product_id.default_code + size_slug or ''

report_sxw.report_sxw('report.stock.picking.invoice', 'stock.picking', 'addons/delmar_reports/static/reports_examples/customer_report_default_picking_invoice.xml', parser=invoice)
report_sxw.report_sxw('report.stock.picking.invoice321', 'stock.picking', 'addons/delmar_reports/static/reports_examples/customer_report_default_picking_invoice321.xml', parser=invoice)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
