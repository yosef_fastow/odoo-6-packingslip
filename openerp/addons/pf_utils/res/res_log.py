from osv import osv


class res_log(osv.osv):
    _name = 'res.log'
    _inherit = 'res.log'

    def set_read(self, cr, uid, context=None):
        update_log = """
            UPDATE res_log
            SET
                "read" = true,
                write_date = (now() at time zone 'UTC')::timestamp,
                write_uid = %s
            WHERE read is not true
                and create_date < (now() at time zone 'UTC')::timestamp - interval '1 hour'
        """ % (uid)
        cr.execute(update_log)
        return True

res_log()
