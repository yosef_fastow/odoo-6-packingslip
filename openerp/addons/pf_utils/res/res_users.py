# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce.com
#    Copyright (C) 2015
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields,osv


class users(osv.osv):
    _inherit = "res.users"

    def _is_user_readonly(self, cr, uid):
        res = False
        if uid == 1:
            return res
        cr.execute("""SELECT rel.uid
                FROM res_groups_users_rel rel
                LEFT JOIN res_groups g
                    ON rel.gid = g.id
                WHERE rel.uid = %s
                    AND lower(g.name) = 'readonly'
            """,
            (int(uid), )
        )
        if cr.fetchone():
            res = True
        return res

    def context_get(self, cr, uid, context=None):
        result = super(users, self).context_get(cr, uid, context=context)
        if self._is_user_readonly(cr, uid):
            result.update({
                'is_readonly': True,
            })
        return result

    def authenticate(self, db, login, password, user_agent_env):
        return self.login(db, login, password)

users()
