openerp.pf_utils = function(openerp) {

    var _t  = openerp.web._t,
        _lt = openerp.web._lt;

    var QWeb = openerp.web.qweb;

    openerp.pf_utils.TimeWidget = openerp.web.DateTimeWidget.extend({
        jqueryui_object: 'datetimepicker',
        type_of_date: "time",
        init: function(parent) {
            this._super(parent);
        },
        start: function() {
            var self = this;
            this.$element.css('white-space', 'nowrap');
            this.$input = this.$element.find('input.oe_datepicker_master');
            this.$input_picker = this.$element.find('input.oe_datepicker_container');
            this.$input.change(this.on_change);
            this.picker({
                onSelect: this.on_picker_select,
                timeOnly: true
            });
            this.$element.find('img.oe_datepicker_trigger').click(function() {
                if (!self.readonly && !self.picker('widget').is(':visible')) {
                    self.picker('setDate', self.value ? openerp.web.auto_str_to_date(self.value) : new Date());
                    self.$input_picker.show();
                    self.picker('show');
                    self.$input_picker.hide();
                }
            });
            this.set_readonly(false);
            this.value = false;
        },
        parse_client: function(v) {
            return openerp.web.parse_value(v, {"widget": 'datetime'});
        }
    });

    openerp.web.form.widgets.add('time', 'openerp.pf_utils.FieldTime');
    openerp.pf_utils.FieldTime = openerp.web.form.FieldDatetime.extend({
        build_widget: function() {
            return new openerp.pf_utils.TimeWidget(this);
        }
    });

    openerp.web.page.readonly.add('image_updateable','openerp.web.page.FieldBinaryImageReaonly');
    openerp.web.form.widgets.add('image_updateable', 'openerp.pf_utils.image_updateable');
    openerp.pf_utils.image_updateable = openerp.web.form.FieldBinaryImage.extend({
        set_value: function(value) {
            var old_value = this.value;
            this._super.apply(this, arguments);
            this.set_image_maxwidth();

            if (value && old_value != value && !(this.view.datarecord[this.name] || false)) {
                this.$image.attr('src', 'data:image/png' + ';base64,' + value);
            } else {
                var url = '/web/binary/image?session_id=' + this.session.session_id + '&model=' +
                    this.view.dataset.model +'&id=' + (this.view.datarecord.id || '') + '&field=' + this.name + '&t=' + (new Date().getTime());
                this.$image.attr('src', url);
            }
        }
    });

    /*------------------------------------------------*/

    openerp.pf_utils.views = {};

    openerp.pf_utils.views.ActionManager = openerp.web.ActionManager;
    openerp.web.ActionManager = openerp.pf_utils.views.ActionManager.extend({
        do_action: function(action, on_close) {
            if (_.isNumber(action)) {
                var self = this;
                return self.rpc("/web/action/load", { action_id: action }, function(result) {
                    self.do_action(result.result, on_close);
                });
            }
            if (!action.type) {
                console.error("No type for action", action);
                return;
            }
            var type = action.type.replace(/\./g,'_');
            var popup = action.target === 'new';
            action.flags = _.extend({
                views_switcher : !popup,
                search_view : !popup,
                action_buttons : !popup,
                sidebar : !popup,
                pager : !popup,
                display_title : !popup
            }, action.flags || {});
            if (action.context && action.context.flags) {
                action.flags = _.extend(action.flags, action.context['flags'] || {});
            }
            if (!(type in this)) {
                console.error("Action manager can't handle action of type " + action.type, action);
                return;
            }
            return this[type](action, on_close);
        }
    });

    function human_filesize(size) {
        var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        var i = 0;
        if(typeof(size) === "string"){size = parseInt(size);}
        while (size >= 1024) {
            size /= 1024;
            ++i;
        }
        return size.toFixed(2) + ' ' + units[i];
    }

    openerp.pf_utils.formats = {};

    openerp.pf_utils.formats.format_cell = openerp.web.format_cell;
    openerp.web.format_cell = function (row_data, column, options) {
        switch (column.widget || column.type) {
            case 'image':
                var src = _.str.sprintf('/web/binary/image?session_id=%s&model=%s&field=%s&id=%d', openerp.connection.session_id, options.model, column.id, options.id);
                return _.template("<img src='<%-src%>' class='oe-binary-image' border='0' style='max-width: <%-width%>px; max-height: <%-height%>px;' >", {
                    src: src,
                    width: column.img_width || 100,
                    height: column.img_height || 100
                });

            case "binary2":
                var text = _t("DOWNLOAD");
                var download_url = _.str.sprintf('/web/binary/saveas?session_id=%s&model=%s&field=%s&id=%d', openerp.connection.session_id, options.model, column.id, options.id);
                var _size = row_data['size'].value
                if (column.filename) {
                    download_url += '&filename_field=' + column.filename;
                }
                if(_size && _size === 'File not found!'){
                    return '<div>NO FILE</div>';
                }
                if(_size){
                    _size = human_filesize(_size)
                    text = _.str.sprintf('%s(%s)', text, _size);
                }
                return _.template('<a href="<%-href%>"><%-text%></a>', {
                    text: text,
                    href: download_url
                });

            case "bool_mark":
                return _.str.sprintf('<input type="checkbox" class="bool_mark" %s />',
                    row_data[column.id].value ? '' : '' );

            case "bool_mark_mass_confirm":
                if (row_data['grant_confirm'].value && row_data['grant_bulk_confirm'].value) {
                    return _.str.sprintf('<input type="checkbox" class="bool_mark" %s %s />',
                        row_data[column.id].value ? '' : '', row_data['grant_confirm'].value ? '' : 'disabled=disabled');
                } else {
                    return _.str.sprintf('');
                }

            case "bool_mark_mass":
                //console.log(row_data['grant_verify'].value);
                if (row_data['grant_verify'].value && row_data['grant_bulk_verify'].value) {
                    return _.str.sprintf('<input type="checkbox" class="bool_mark" %s %s />',
                        row_data[column.id].value ? '' : '', row_data['grant_verify'].value ? '' : 'disabled=disabled');
                } else {
                    return _.str.sprintf('');
                }

            case "log_list":
                var raw_errors = String(row_data['log'].value);
                console.log(raw_errors);
                var _logs = raw_errors.split("\n");
                var _str = "";
                for(var i in _logs) {
                    if(_logs[i]){
                        var _msg = _logs[i];
                        _str = _str + _msg + "<br>";
                    }
                }
                console.log(_.str.sprintf('_str: %s', _str ));
                return _str

            case 'progress_with_label':
                return _.template(
                    '<div class="progress-with-label-div"><progress class="progress-with-label" value="<%-value%>" max="100"><%-value%>%</progress><span class="progress-with-label-span"><%-value%>%</span></div>', {
                        value: row_data[column.id].value
                    });

            case "state_in_tree_view":
                var _state = row_data[column.id].value;
                var _state_string = '';
                var _selection = column.selection;
                if(_selection)
                {
                    for (var i in _selection) {
                        if(_selection[i][0] === _state)
                        {
                            _state_string = _selection[i][1];
                            break;
                        }
                    }
                }
                var src_image = '#';
                if(!_state_string){_state_string = _state}
                src_image = '/web/static/src/img/state/' + _state + '.png';
                return _.str.sprintf('<img src="%s"/>   %s', src_image, _state_string);

//            todo DLMR-699 check
            case "counter":
                return _.template(
                        '<div class="view_with_increment_and_decrement"> <button > + </button> <input type="text" id="inc" value="0"></input><button "> - </button></div>');

        }

        if (column.widget == 'link_') {
            return _.str.sprintf('<a href="%s" target="_blank">Link</a>',
                row_data[column.id].value || '');
        }

        return openerp.pf_utils.formats.format_cell(row_data, column, options);
    };

    openerp.pf_utils.formats.format_value = openerp.web.format_value;
    openerp.web.format_value = function(value, descriptor, value_if_empty) {
        val = openerp.pf_utils.formats.format_value(value, descriptor, value_if_empty);
        if(descriptor.widget === "many2one_search_only" && _.isArray(val)) {
            val = val[1];
        }
        return val;
    };

    /*------------------------------------------------*/

    openerp.pf_utils.form = {};

    /*FormView*/
    openerp.web.views.add('form', 'openerp.pf_utils.form.FormView');
    openerp.pf_utils.form.FormView = openerp.web.FormView.extend({
        can_be_discarded: function() {
            if ((this.options || {})['can_be_discarded']) {
                return !this.$element.is('.oe_form_dirty');
            }
            return !this.$element.is('.oe_form_dirty') || this.$element.find("span:contains('Save/Next')") || confirm(_t("Warning, the record has been modified, your changes will be discarded."));
        },
        on_processed_onchange: function(response, processed) {
            try {
                var result = response;
                if (result.value) {
                    for (var f in result.value) {
                        if (!result.value.hasOwnProperty(f)) { continue; }
                        var field = this.fields[f];
                        // If field is not defined in the view, just ignore it
                        if (field) {
                            var value = result.value[f];
                            if (field.get_value() != value) {
                                field.set_value(value);
                                field.dirty = true;
                                if (!_.contains(processed, field.name)) {
                                    this.do_onchange(field, processed);
                                }
                            }
                        }
                    }
                    this.on_form_changed();
                }
                if (!_.isEmpty(result.warning)) {
                    $(QWeb.render("CrashManagerWarning", result.warning)).dialog({
                        title: result.warning['title'] || _t('Warning'),
                        modal: true,
                        buttons: [
                            {text: _t("Ok"), click: function() { $(this).dialog("close"); }}
                        ]
                    });
                }
                if (!_.isEmpty(result.notification)) {
                    $(QWeb.render("CrashManagerNotification", result.notification)).dialog({
                        title: result.warning['title'] || _t('Notification'),
                        modal: true,
                        buttons: [
                            {text: _t("Ok"), click: function() { $(this).dialog("close"); }}
                        ]
                    });
                }
                if (result.domain) {
                    function edit_domain(node) {
                        var new_domain = result.domain[node.attrs.name];
                        if (new_domain) {
                            node.attrs.domain = new_domain;
                        }
                        _(node.children).each(edit_domain);
                    }
                    edit_domain(this.fields_view.arch);
                }
                return $.Deferred().resolve();
            } catch(e) {
                console.error(e);
                return $.Deferred().reject();
            }
        }
    });

    /*FieldMany2Many*/
    openerp.pf_utils.form.FieldMany2Many = openerp.web.form.FieldMany2Many;
    openerp.web.form.FieldMany2Many = openerp.pf_utils.form.FieldMany2Many.extend({
        init: function(view, node) {
            this._super(view, node);
            var modifiers = this.modifiers || {};
            this.popup_modifiers = modifiers.popup || {};
            this.multi_selection = modifiers['multi_selection'] || false;
        },
        load_view: function() {
            var self = this;
            this.list_view = new openerp.web.form.Many2ManyListView(this, this.dataset, false, {
                        'addable': self.is_readonly() ? null : _t("Add"),
                        'deletable': self.is_readonly() ? false : true,
                        'selectable': self.multi_selection,
                        'isClarkGable': self.is_readonly() ? false : true,
                        'popup': this.popup_modifiers || {}
                });
            var embedded = (this.field.views || {}).tree;
            if (embedded) {
                this.list_view.set_embedded_view(embedded);
            }
            this.list_view.m2m_field = this;
            var loaded = $.Deferred();
            this.list_view.on_loaded.add_last(function() {
                self.initial_is_loaded.resolve();
                loaded.resolve();
            });
            $.async_when().then(function () {
                self.list_view.appendTo($("#" + self.list_id));
            });
            return loaded;
        }
    });

    openerp.web.page.FieldMany2ManyReadonly = openerp.web.form.FieldMany2Many.extend({
        force_readonly: true
    });

    /**
     * @class
     * @extends openerp.web.form.Many2ManyListView
     */
    openerp.pf_utils.form.Many2ManyListView = openerp.web.form.Many2ManyListView;
    openerp.web.form.Many2ManyListView = openerp.pf_utils.form.Many2ManyListView.extend({
        start: function() {
            this.apply_options();
            return this._super.apply(this, arguments);
        },
        do_activate_record: function(index, id) {
            var self = this;
            var pop = new openerp.web.form.FormOpenPopup(this);
            var options = this.options || {};
            pop.show_element(this.dataset.model, id, this.m2m_field.build_context(), _.extend({
                title: _t("Open: ") + this.name,
                readonly: this.widget_parent.is_readonly(),
                all_ids: self.dataset.ids
            },
            options.popup || {}));
            pop.on_write_completed.add_last(function() {
                self.reload_content();
            });
        },
        do_button_action: function (name, id, callback) {
            var self = this;
            var def = $.Deferred().then(callback).then(function() {
                self.widget_parent.widget_parent.reload();
            });
            return this._super(name, id, _.bind(def.resolve, def));
        },
        apply_options: function() {
            this.options = _.extend(this.options, this.m2m_field.modifiers.options || {});
        }
    });

    /*FieldOne2Many*/
    openerp.pf_utils.form.FieldOne2Many = openerp.web.form.FieldOne2Many;
    openerp.web.form.FieldOne2Many = openerp.pf_utils.form.FieldOne2Many.extend({
        load_views: function() {
            var self = this;

            var modes = this.node.attrs.mode;
            modes = !!modes ? modes.split(",") : ["tree"];
            var views = [];
            _.each(modes, function(mode) {
                var view = {
                    view_id: false,
                    view_type: mode == "tree" ? "list" : mode,
                    options: self['modifiers'] && self.modifiers['options'] || {}
                };
                view.options.sidebar = false;
                if (self.field.views && self.field.views[mode]) {
                    view.embedded_view = self.field.views[mode];
                }
                if(view.view_type === "list") {
                    view.options.selectable = self.multi_selection;
                    if (self.is_readonly()) {
                        view.options.addable = null;
                        view.options.deletable = null;
                        view.options.isClarkGable = false;
                    }
                } else if (view.view_type === "form") {
                    if (self.is_readonly()) {
                        view.view_type = 'page';
                    }
                    view.options.not_interactible_on_create = true;
                }
                views.push(view);
            });
            this.views = views;

            this.viewmanager = new openerp.web.ViewManager(this, this.dataset, views, {});
            this.viewmanager.template = 'One2Many.viewmanager';
            this.viewmanager.registry = openerp.web.views.extend({
                list: 'openerp.web.form.One2ManyListView',
                form: 'openerp.web.form.One2ManyFormView',
                page: 'openerp.web.PageView'
            });
            var once = $.Deferred().then(function() {
                self.init_form_last_update.resolve();
            });
            var def = $.Deferred().then(function() {
                self.initial_is_loaded.resolve();
            });
            this.viewmanager.on_controller_inited.add_last(function(view_type, controller) {
                if (view_type == "list") {
                    controller.o2m = self;
                    if (self.is_readonly())
                        controller.set_editable(false);
                } else if (view_type == "form" || view_type == 'page') {
                    if (view_type == 'page' || self.is_readonly()) {
                        $(".oe_form_buttons", controller.$element).children().remove();
                    }
                    controller.on_record_loaded.add_last(function() {
                        once.resolve();
                    });
                    controller.on_pager_action.add_first(function() {
                        self.save_any_view();
                    });
                } else if (view_type == "graph") {
                    self.reload_current_view()
                }
                def.resolve();
            });
            this.viewmanager.on_mode_switch.add_first(function(n_mode, b, c, d, e) {
                $.when(self.save_any_view()).then(function() {
                    if(n_mode === "list")
                        $.async_when().then(function() {self.reload_current_view();});
                });
            });
            this.is_setted.then(function() {
                $.async_when().then(function () {
                    self.viewmanager.appendTo(self.$element);
                });
            });

            self.is_loaded.done(function() {
                self.handle_default_sort(self);
            });
            return def;
        },
        handle_default_sort: function(self) {
            var attrs;
            if (self.node && self.node.attrs && self.node.attrs.attrs) {
                attrs_str = self.node.attrs.attrs;
                attrs_str = attrs_str.replace(/\'/g, '"');
                attrs_str = attrs_str.replace(/False/g, 'false');
                try {
                    attrs = JSON.parse(attrs_str);
                } catch (e) {
                    attrs = null;
                }
            }

            if(attrs && attrs['options'] && attrs['options']['default_sort']) {
                var options = attrs['options'];
                var dir = options['default_sort_dir'] == 'up' ? 1 : 2;
                for (var i = 0; i < dir; ++i) {
                    self.widget_children[options['default_sort_view_idx']].$element.find("th.oe-sortable[data-id='" + options['default_sort'] +"']").trigger('click');
                }
            }
        }
    });

    openerp.pf_utils.form.One2ManyListView = openerp.web.form.One2ManyListView;
    openerp.web.form.One2ManyListView = openerp.pf_utils.form.One2ManyListView.extend({
        reload_record: function (record) {
            if (record == null && this.records)
            {
                record = this.records.records[0]
            }
            // Evict record.id from cache to ensure it will be reloaded correctly
            this.dataset.evict_from_cache(record.get('id'));

            return this._super(record);
        },
        do_button_action: function (name, id, callback) {
            var self = this;
            var on_button_action = !!self.options ? self.options.on_button_action : 'reload';
            var def = $.Deferred().then(callback).then(function() {
                if (on_button_action == 'remove') {
                    self.dataset.on_unlink([id]);
                    self.records.remove(self.records.get(id));
                    self.configure_pager(self.dataset);
                    self.compute_aggregates();
                } else {
                    self.o2m.view.reload();
                }
            });


            return this.do_button_action_super(name, id, _.bind(def.resolve, def));
        },
        do_activate_record: function(index, id) {
            var self = this;
            var popup_options = this.options['popup'] || {};
            if (!popup_options['deny']) {
                var pop = new openerp.web.form.FormOpenPopup(self.o2m.view);
                pop.show_element(self.o2m.field.relation, id, self.o2m.build_context(), {
                    title: _t("Open: ") + self.name,
                    auto_write: false,
                    alternative_form_view: self.o2m.field.views ? self.o2m.field.views["form"] : undefined,
                    parent_view: self.o2m.view,
                    child_name: self.o2m.name,
                    read_function: function() {
                        return self.o2m.dataset.read_ids.apply(self.o2m.dataset, arguments);
                    },
                    form_view_options: {'not_interactible_on_create':true},
                    readonly: self.o2m.is_readonly(),
                    all_ids: self.dataset.ids
                });
                pop.on_write.add(function(id, data) {
                    self.o2m.dataset.write(id, data, {}, function(r) {
                        self.o2m.reload_current_view();
                    });
                });
            }
        },
        do_button_action_super: function (name, id, callback) {
            var action = _.detect(this.columns, function (field) {
                return field.name === name;
            });
            if (!action) { return; }
            if ('confirm' in action && !window.confirm(action.confirm)) {
                return;
            }

            var c = new openerp.web.CompoundContext();
            c.set_eval_context(_.extend({
                active_id: id,
                active_ids: [id],
                active_model: this.dataset.model
            }, this.records.get(id).toContext()));
            if (action.context) {
                c.add(action.context);
            }
            action.context = c;
            this.do_execute_action(action, this.dataset, id, callback);

        }

    });


    /*------------------------------------------------*/

    openerp.pf_utils.page = {};

    openerp.web.page.readonly.add('time', 'openerp.web.page.FieldCharReadonly');


    /*------------------------------------------------*/

    openerp.pf_utils.widgets = {}

    /*WidgetButton*/
    openerp.pf_utils.WidgetButton = openerp.web.form.WidgetButton;
    openerp.web.form.WidgetButton = openerp.pf_utils.WidgetButton.extend({
        execute_action: function() {
            var self = this;
            var exec_action = function() {
                if (self.node.attrs.confirm) {
                    var def = $.Deferred();
                    var dialog = $('<div>' + self.node.attrs.confirm + '</div>').dialog({
                        title: _t('Confirm'),
                        modal: true,
                        buttons: [
                            {text: _t("Cancel"), click: function() {
                                    def.resolve();
                                    $(this).dialog("close");
                                }
                            },
                            {text: _t("Ok"), click: function() {
                                    self.on_confirmed().then(function() {
                                        def.resolve();
                                    });
                                    $(this).dialog("close");
                                }
                            }
                        ]
                    });
                    return def.promise();
                } else {
                    return self.on_confirmed();
                }
            };
            if (!this.node.attrs.special) {
                this.save_selected_ids();
                return this.view.recursive_save().pipe(exec_action);
            } else {
                this.save_selected_ids();
                return exec_action();
            }
        },
        on_confirmed: function() {
            var self = this;

            var ext_context = {'selected_ids': this.node.attrs.selected_ids || []};

            var context = this.node.attrs.context;
            if (context && context.__ref) {
                context = new openerp.web.CompoundContext(context);
                context.set_eval_context(this._build_eval_context(ext_context));
            } else {
                context = _.extend({}, context, ext_context);
            }
            return this.view.do_execute_action(
                _.extend({}, this.node.attrs, {context: context}),
                this.view.dataset, this.view.datarecord.id, function () {
                    self.view.reload();
                });
        },
        save_selected_ids: function(){
            var mark_print_ids = $(".bool_mark:checked").parent().parent().map(function() {
                return parseInt($(this).attr("data-id"));
            }).get();
            this.node.attrs['selected_ids'] = mark_print_ids;
        }
    });

    /*FieldBinaryFile*/
    openerp.web.form.widgets.add('field_binary_without_saveas', 'openerp.pf_utils.widgets.field_binary_without_saveas');
    openerp.pf_utils.widgets.field_binary_without_saveas = openerp.web.form.FieldBinaryFile.extend(
        {
            template: 'FieldBinaryFileWithoutSaveAs'
        }
    );

    /*FileWithoutExtAndSize*/
    openerp.web.form.widgets.add('file_with_ext_and_size', 'openerp.pf_utils.widgets.file_with_ext_and_size');
    openerp.pf_utils.widgets.file_with_ext_and_size = openerp.web.form.FieldBinaryFile.extend(
        {
            template: 'FileWithoutSaveAs',
            on_file_uploaded_and_valid: function(size, name, content_type, file_base64) {
                this._super.apply(this, arguments);
                this.set_field('file_type', content_type);
                this.set_field('file_name', name);
                this.set_field('size', size);
            },
            set_field: function(name_field, value) {
                if (this.view.fields[name_field]) {
                    this.view.fields[name_field].set_value(value);
                    console.log("[set field " + name_field + " : " + value + " ]");
                }
            },
        }
    );

    openerp.web.form.widgets.add('field_save_as', 'openerp.pf_utils.widgets.field_save_as');
    openerp.pf_utils.widgets.field_save_as = openerp.web.form.FieldBinaryFile.extend(
        {
            template: 'SaveAs'
        }
    );

    /*FieldChar*/
    openerp.web.form.widgets.add('char_always_edit', 'openerp.pf_utils.widgets.char_always_edit');
    openerp.pf_utils.widgets.char_always_edit = openerp.web.form.FieldChar.extend({
        init: function(view, node) {
            this._super(view, node);
            this.readonly = false;
        },
        on_ui_change: function() {
            this.dirty = true;
            this.validate();
            if (this.is_valid()) {
                this.set_value_from_ui();
                this.view.do_onchange(this);
                this.view.on_form_changed(true);
                // this.view.do_notify_change();
            } else {
                this.update_dom(true);
            }
        }
    });

    /*FieldSelection*/
    openerp.web.form.widgets.add('selection_always_edit', 'openerp.pf_utils.widgets.selection_always_edit');
    openerp.pf_utils.widgets.selection_always_edit = openerp.web.form.FieldSelection.extend({
        init: function(view, node) {
            this._super(view, node);
            this.readonly = false;
        }
    });

    /*FieldMany2One*/
    openerp.pf_utils.FieldMany2One = openerp.web.form.FieldMany2One;
    openerp.web.form.FieldMany2One = openerp.pf_utils.FieldMany2One.extend({
        get_search_result: function(request, response) {
            var search_val = request.term;
            var self = this;

            if (this.abort_last) {
                this.abort_last();
                delete this.abort_last;
            }
            var dataset = new openerp.web.DataSetStatic(this, this.field.relation, self.build_context());

            dataset.name_search(search_val, self.build_domain(), 'ilike',
                    this.limit + 1, function(data) {
                self.last_search = data;
                // possible selections for the m2o
                var values = _.map(data, function(x) {
                    return {
                        label: _.str.escapeHTML(x[1]),
                        value:x[1],
                        name:x[1],
                        id:x[0]
                    };
                });

                // search more... if more results that max
                if (values.length > self.limit) {
                    values = values.slice(0, self.limit);
                    values.push({label: _t("<em>   Search More...</em>"), action: function() {
                        dataset.name_search(search_val, self.build_domain(), 'ilike'
                        , false, function(data) {
                            self._change_int_value(null);
                            self._search_create_popup("search", data);
                        });
                    }});
                }

                response(values);
            });
            this.abort_last = dataset.abort_last;
        }
    });

    openerp.web.page.readonly.add('many2one_search_only', 'openerp.web.page.FieldMany2OneReadonly');
    openerp.web.form.widgets.add('many2one_search_only', 'openerp.pf_utils.many2one_search_only');
    openerp.pf_utils.many2one_search_only = openerp.web.form.FieldMany2One.extend({
        init: function(view, node) {
            node.attrs.widget = "many2one";
            this._super(view, node);
        },
        start: function() {
            this._super();
            var self = this;

            var init_context_menu_def = $.Deferred().then(function(e) {
                var rdataset = new openerp.web.DataSetStatic(self, "ir.values", self.build_context());
                rdataset.call("get", ['action', 'client_action_relate',
                    [[self.field.relation, false]], false, rdataset.get_context()], false, 0)
                    .then(function(result) {
                    self.related_entries = result;

                    var $cmenu = $("#" + self.cm_id);
                    $cmenu.find("#" + self.cm_id + "_create").remove();
                    $cmenu.find("#" + self.cm_id + "_open").remove();
                    $cmenu.append(QWeb.render("FieldMany2One.search_only_context_menu", {widget: self}));
                    var bindings = {};
                    bindings[self.cm_id + "_search"] = function() {
                        self._search_create_popup("search");
                    };
                    _.each(_.range(self.related_entries.length), function(i) {
                        bindings[self.cm_id + "_related_" + i] = function() {
                            self.open_related(self.related_entries[i]);
                        };
                    });
                    var cmenu = self.$menu_btn.contextMenu(self.cm_id, {'noRightClick': true,
                        bindings: bindings, itemStyle: {"color": ""},
                        onContextMenu: function() {
                            if(self.value) {
                                $("#" + self.cm_id + " .oe_m2o_menu_item_mandatory").removeClass("oe-m2o-disabled-cm");
                            } else {
                                $("#" + self.cm_id + " .oe_m2o_menu_item_mandatory").addClass("oe-m2o-disabled-cm");
                            }
                            if (!self.readonly) {
                                $("#" + self.cm_id + " .oe_m2o_menu_item_noreadonly").removeClass("oe-m2o-disabled-cm");
                            } else {
                                $("#" + self.cm_id + " .oe_m2o_menu_item_noreadonly").addClass("oe-m2o-disabled-cm");
                            }
                            return true;
                        }, menuStyle: {width: "200px"}
                    });
                    $.async_when().then(function() {self.$menu_btn.trigger(e);});
                });
            });
            var ctx_callback = function(e) {init_context_menu_def.resolve(e); e.preventDefault()};
            this.$menu_btn.click(ctx_callback);
        },
        get_search_result: function(request, response) {
            var search_val = request.term;
            var self = this;

            if (this.abort_last) {
                this.abort_last();
                delete this.abort_last;
            }
            var dataset = new openerp.web.DataSetStatic(this, this.field.relation, self.build_context());

            dataset.name_search(search_val, self.build_domain(), 'ilike',
                    this.limit + 1, function(data) {
                self.last_search = data;
                // possible selections for the m2o
                var values = _.map(data, function(x) {
                    return {
                        label: _.str.escapeHTML(x[1]),
                        value:x[1],
                        name:x[1],
                        id:x[0]
                    };
                });

                // search more... if more results that max
                if (values.length > self.limit) {
                    values = values.slice(0, self.limit);
                    values.push({label: _t("<em>   Search More...</em>"), action: function() {
                        dataset.name_search(search_val, self.build_domain(), 'ilike'
                        , false, function(data) {
                            self._change_int_value(null);
                            self._search_create_popup("search", data);
                        });
                    }});
                }

                response(values);
            });
            this.abort_last = dataset.abort_last;
        }

    });

    openerp.web.form.widgets.add('many2one_always_edit', 'openerp.pf_utils.widgets.many2one_always_edit');
    openerp.pf_utils.widgets.many2one_always_edit = openerp.pf_utils.many2one_search_only.extend({
        init: function(view, node) {
            node.attrs.widget = "many2one";
            this._super(view, node);
            this.readonly = false;
        }
    });

    openerp.web.form.widgets.add('many2many_task', 'openerp.pf_utils.widgets.many2many_task');
    openerp.pf_utils.widgets.many2many_task = openerp.web.form.FieldMany2Many.extend({
        load_view: function() {
            var self = this;

            this.list_view = new openerp.web.form.Many2ManyListView(this, this.dataset, false, {
                'addable': null,
                'deletable': false,
                'selectable': self.multi_selection,
                'isClarkGable': true
            });

            var embedded = (this.field.views || {}).tree;
            if (embedded) {
                this.list_view.set_embedded_view(embedded);
            }
            this.list_view.m2m_field = this;
            var loaded = $.Deferred();
            this.list_view.on_loaded.add_last(function() {
                self.initial_is_loaded.resolve();
                loaded.resolve();
            });
            $.async_when().then(function () {
                self.list_view.appendTo($("#" + self.list_id));
            });
            return loaded;
        }
    });

    /*FieldBoolean*/
    openerp.web.form.widgets.add('boolean_always_edit', 'openerp.pf_utils.widgets.boolean_always_edit');
    openerp.pf_utils.widgets.boolean_always_edit = openerp.web.form.FieldBoolean.extend({
        init: function(view, node) {
            this._super(view, node);
            this.readonly = false;
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            this.$element.find("input[name='mark_use']").click(function() {
                $(".bool_mark").prop('checked', self.$element.find("input[name='mark_use']").is(':checked'))
            });
        }
    });

    /* ISDD selector for lines */
    openerp.web.form.widgets.add('relative_lines_selector', 'openerp.pf_utils.widgets.relative_lines_selector');
    openerp.pf_utils.widgets.relative_lines_selector = openerp.web.form.FieldBoolean.extend({
        init: function(view, node) {
            this._super(view, node);
            this.readonly = false;
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            this.$element.click(function(){
                ts = self.$element.closest(".ui-widget-content")
                ts.find(".bool_mark").prop('checked', self.$element.children("input[type='checkbox']").is(':checked'))
            });
        }
    });

    /*Field*/
    openerp.web.form.widgets.add('bool_btn', 'openerp.pf_utils.widgets.bool_btn');
    openerp.pf_utils.widgets.bool_btn = openerp.web.form.Field.extend({
        template: 'WidgetButton',
        init: function(view, node) {
            this._super(view, node);
            // view.btn_string = this.string;
            // this.string = null;
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            // this.$element.find('button').append('<span>' + this.field.string + '</span>');
            this.$element.parent().find('label').hide();
            this.$element.find('button').width('100%');
            this.$element.find('button').click(self.on_ui_change);
        },
        set_value: function(value) {
        },
        set_value_from_ui: function() {
            this.value = true;
            this._super();
        },
        update_dom: function() {
            this._super.apply(this, arguments);
            this.$element.find('button').prop('disabled', this.readonly);
        },
        focus: function($element) {
            this._super($element || this.$element.find('button:first'));
        }
    });

    /*Field*/
    openerp.web.form.widgets.add('bool_btn_inv', 'openerp.pf_utils.widgets.bool_btn_inv');

    openerp.pf_utils.widgets.bool_btn_inv = openerp.web.form.Field.extend({
        template: 'WidgetButton',
        init: function(view, node) {
            this._super(view, node);
            // view.btn_string = this.string;
            // this.string = null;
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            // this.$element.find('button').append('<span>' + this.field.string + '</span>');
            this.$element.parent().find('label').hide();
            this.$element.find('button').width('100%');
            this.$element.find('button').click(self.on_ui_change);
        },
        set_value: function(value) {
            this.value = value;
            this.$element.find('button').removeClass('disabled');
        },
        set_value_from_ui: function() {
            this.value = true;
            this._super();
        },
        update_dom: function() {
            this._super.apply(this, arguments);
            this.$element.find('button').prop('disabled', this.readonly);
            if (this.value === false) {
                this.$element.find('button').removeAttr('disabled');
            }
        },
        on_ui_change: function() {
            this.$element.find('button').addClass('disabled');
            this.$element.find('button').attr('disabled', 'disabled')
            // Taken from _super():
            this.dirty = true;
            this.validate();
            if (this.is_valid()) {
                this.set_value_from_ui();
                // but with some changes
                slf = this;
                setTimeout(function(){  //Beginning of code that should run AFTER the timeout
                    //lots more code
                    slf.view.do_onchange(slf);
                    slf.view.on_form_changed(true);
                    slf.view.do_notify_change();
                }, 200, slf);
                //this.view.do_onchange(this);
                //this.view.on_form_changed(true);
                //this.view.do_notify_change();
            } else {
                this.update_dom(true);
            }

        },
        focus: function($element) {
            this._super($element || this.$element.find('button:first'));
        }
    });

    /*HtmlRaw*/
    openerp.web.form.widgets.add('raw_html', 'openerp.pf_utils.widgets.raw_html');
    openerp.pf_utils.widgets.raw_html = openerp.web.form.Field.extend({
        template: 'WidgetRawHTML',
        set_value: function(value) {
            if(value !== undefined){
                var _value = value;
                var _container = this.$element.find('#container_raw_html');
                if(_container !== undefined){
                    _container.empty();
                    _container.append(_value);
                }
            }
            this._super.apply(this, arguments);
        },
    });

    /**
     * @class
     * @extends openerp.web.form.FormOpenPopup
     */
    openerp.pf_utils.FormOpenPopup = openerp.web.form.FormOpenPopup;
    openerp.web.form.FormOpenPopup = openerp.pf_utils.FormOpenPopup.extend({
        show_element: function(model, row_id, context, options) {
            options = _.extend({'action_buttons': true, 'pager': true}, options || {});
            this._super(model, row_id, context, options);
        },
        setup_form_view: function() {
            var self = this;
            var FormClass = this.options.readonly
                    ? openerp.web.views.get_object('page')
                    : openerp.web.views.get_object('form');
            this.view_form = new FormClass(this, this.dataset, false, self.options.form_view_options);
            if (this.options.alternative_form_view) {
                this.view_form.set_embedded_view(this.options.alternative_form_view);
            }
            this.view_form.appendTo(this.$element.find("#" + this.element_id + "_view_form"));
            this.view_form.on_loaded.add_last(function() {
                var $buttons = self.view_form.$element.find(".oe_form_buttons");
                var $pager = self.view_form.$element.find(".oe_form_pager");
                $buttons.html(QWeb.render("FormOpenPopup.form.buttons"));
                var $nbutton = $buttons.find(".oe_formopenpopup-form-save");
                $nbutton.click(function() {
                    self.view_form.do_save().then(function() {
                        self.stop();
                    });
                });
                var $cbutton = $buttons.find(".oe_formopenpopup-form-close");
                $cbutton.click(function() {
                    self.stop();
                });

                var $sbutton = self.view_form.$element.find(".oe_form_button button:contains('Save/Next')");
                $sbutton.click(function() {
                    self.view_form.do_save().then(self.view_form.on_pager_action('next'));
                });

                if (self.options.readonly) {
                    $nbutton.hide();
                    $cbutton.text(_t("Close"));
                }

                if (!self.options.pager) {
                    $pager.hide();
                }

                if (!self.options.action_buttons) {
                    $buttons.hide();
                }
                self.view_form.do_show();
            });
            this.dataset.on_write.add(this.on_write);
        }
    });

    /**
     * @class
     * @extends openerp.web.ListView.List
     */
    openerp.pf_utils.ListView_List = openerp.web.ListView.List;
    openerp.web.ListView.List = openerp.pf_utils.ListView_List.extend({
        init: function (group, opts) {
            var self = this;
            this.group = group;
            this.view = group.view;
            this.session = this.view.session;

            this.options = opts.options;
            this.columns = opts.columns;
            this.dataset = opts.dataset;
            this.records = opts.records;

            this.record_callbacks = {
                'remove': function (event, record) {
                    var $row = self.$current.find(
                            '[data-id=' + record.get('id') + ']');
                    var index = $row.data('index');
                    $row.remove();
                    self.refresh_zebra(index);
                },
                'reset': function () { return self.on_records_reset(); },
                'change': function (event, record) {

                    /*
                        This is shit code to fix: file content instead of
                        file size in tree after for binaries
                    */
                    console.log('oops');
                    for (var i=0; i<self.columns.length; i++) {
                        if (self.columns[i].type == "binary" && isNaN(parseInt(record.attributes[self.columns[i].name], 10)) && record.attributes[self.columns[i].name]) {
                            console.log('oops #1');
                            var bytes = record.attributes[self.columns[i].name].length - 1;
                            console.log('oops #2');
                            record.attributes[self.columns[i].name] = bytesToSize(bytes, 2);
                            console.log('oops #3');
                        }
                    }
                    var $row = self.$current.find('[data-id=' + record.get('id') + ']');
                    $row.replaceWith(self.render_record(record));
                },
                'add': function (ev, records, record, index) {
                    var $new_row = $('<tr>').attr({
                        'data-id': record.get('id')
                    });

                    if (index === 0) {
                        $new_row.prependTo(self.$current);
                    } else {
                        var previous_record = records.at(index-1),
                            $previous_sibling = self.$current.find(
                                    '[data-id=' + previous_record.get('id') + ']');
                        $new_row.insertAfter($previous_sibling);
                    }

                    self.refresh_zebra(index, 1);
                }
            };
            _(this.record_callbacks).each(function (callback, event) {
                this.records.bind(event, callback);
            }, this);

            this.$_element = $('<tbody class="ui-widget-content">')
                .appendTo(document.body)
                .delegate('th.oe-record-selector', 'click', function (e) {
                    e.stopPropagation();
                    var selection = self.get_selection();
                    $(self).trigger(
                            'selected', [selection.ids, selection.records]);
                })
                .delegate('td.oe-record-delete button', 'click', function (e) {
                    e.stopPropagation();
                    var $row = $(e.target).closest('tr');
                    $(self).trigger('deleted', [[self.row_id($row)]]);
                })
                .delegate('td.oe-field-cell button', 'click', function (e) {
                    e.stopPropagation();
                    var $target = $(e.currentTarget),
                          field = $target.closest('td').data('field'),
                           $row = $target.closest('tr'),
                      record_id = self.row_id($row);

                    // note: $.data converts data to number if it's composed only
                    // of digits, nice when storing actual numbers, not nice when
                    // storing strings composed only of digits. Force the action
                    // name to be a string
                    $(self).trigger('action', [field.toString(), record_id, function () {
                        return self.reload_record(self.records.get(record_id));
                    }]);
                })
                .delegate('a', 'click', function (e) {
                    e.stopPropagation();
                })
                .delegate('tr', 'dblclick', function (e) {
                    e.stopPropagation();
                    var row_id = self.row_id(e.currentTarget);
                    if (row_id !== undefined) {
                        if (!self.dataset.select_id(row_id)) {
                            throw "Could not find id in dataset"
                        }
                        var view;
                        if ($(e.target).is('.oe-record-edit-link-img') || self.options.form_only) {
                            view = 'form';
                        }
                        self.row_clicked(e, view);
                    }
                });
        },
        render_cell: function (record, column) {
            var value;
            if(column.type === 'reference') {
                value = record.get(column.id);
                var ref_match;
                // Ensure that value is in a reference "shape", otherwise we're
                // going to loop on performing name_get after we've resolved (and
                // set) a human-readable version. m2o does not have this issue
                // because the non-human-readable is just a number, where the
                // human-readable version is a pair
                if (value && (ref_match = /([\w\.]+),(\d+)/.exec(value))) {
                    // reference values are in the shape "$model,$id" (as a
                    // string), we need to split and name_get this pair in order
                    // to get a correctly displayable value in the field
                    var model = ref_match[1],
                        id = parseInt(ref_match[2], 10);
                    new openerp.web.DataSet(this.view, model).name_get([id], function(names) {
                        if (!names.length) { return; }
                        record.set(column.id, names[0][1]);
                    });
                }
            } else if (column.type === 'many2one') {
                value = record.get(column.id);
                // m2o values are usually name_get formatted, [Number, String]
                // pairs, but in some cases only the id is provided. In these
                // cases, we need to perform a name_get call to fetch the actual
                // displayable value
                if (typeof value === 'number' || value instanceof Number) {
                    // fetch the name, set it on the record (in the right field)
                    // and let the various registered events handle refreshing the
                    // row
                    new openerp.web.DataSet(this.view, column.relation)
                            .name_get([value], function (names) {
                        if (!names.length) { return; }
                        record.set(column.id, names[0]);
                    });
                }
            }

            var current_column_cut_options = null;
            if (this.options && this.options.cut_fields) {
                var cut_fields = this.options.cut_fields;
                for (var i = 0; i < cut_fields.length; ++i) {
                    if (column.name == cut_fields[i].name) {
                        current_column_cut_options = cut_fields[i];
                        break;
                    }
                }
            }

            var result = openerp.web.format_cell(record.toForm().data, column, {
                model: this.dataset.model,
                id: record.get('id')
            });

            if (current_column_cut_options) {
                switch (current_column_cut_options.type) {
                    case 'separator':
                        if (!current_column_cut_options.separator) {
                            current_column_cut_options.separator = ',';
                        }
                        if (!current_column_cut_options.num_in_line || current_column_cut_options.num_in_line < 1) {
                            current_column_cut_options.num_in_line = 1;
                        }
                        split_arr = result.split(current_column_cut_options.separator);
                        num_chunk = Math.ceil(split_arr.length / current_column_cut_options.num_in_line);
                        var lines = [];
                        for (var i = 0; i < num_chunk; ++i) {
                            chunk = split_arr.slice(i * current_column_cut_options.num_in_line, (i + 1) * current_column_cut_options.num_in_line);
                            lines.push(chunk.join(current_column_cut_options.separator));
                        }
                        result = lines.join(current_column_cut_options.separator + '<br>');
                        break;
                }
            }

            return result;
        }
    });

    /**
     * Client actions.
     */
    openerp.web.redirect = function(url, wait) {
        // Don't display a dialog if some xmlhttprequest are in progress
        if (openerp.client && openerp.client.crashmanager) {
            openerp.client.crashmanager.active = false;
        }

        var wait_server = function() {
            openerp.session.rpc("/web/webclient/version_info", {}).done(function() {
                window.location = url;
            }).fail(function() {
                setTimeout(wait_server, 250);
            });
        };

        if (wait) {
            setTimeout(wait_server, 1000);
        } else {
            window.location = url;
        }
    };

    openerp.web.Reload = function(parent, action) {
        var params = action.params || {};
        var menu_id = params.menu_id || false;
        var l = window.location;

        var sobj = $.deparam(l.search.substr(1));
        sobj.ts = new Date().getTime();
        var search = '?' + $.param(sobj);

        var hash = l.hash;
        if (menu_id) {
            hash = "#menu_id=" + menu_id;
        }
        var url = l.protocol + "//" + l.host + l.pathname + search + hash;

        openerp.web.redirect(url, params.wait);
    };
    openerp.web.client_actions.add("reload", "openerp.web.Reload");

    openerp.pf_utils.ExtendedSearchGroup = openerp.web.search.ExtendedSearchGroup;
    openerp.web.search.ExtendedSearchGroup = openerp.pf_utils.ExtendedSearchGroup.extend({
        get_domain: function() {
            var props = _(this.widget_children).chain().map(function(x) {
                return x.get_proposition();
            }).compact().value();
            var choice = this.$element.find(".searchview_extended_group_choice").val();
            var op = choice == "all" ? "&" : "|";
            var prefix = choice == "none" ? ["!"] : [];
            return prefix.concat(_.map(_.range(_.max([0,props.length - 1])), function() { return op; }), props);
        },
    });

    openerp.pf_utils.DateTimeField = openerp.web.search.Field.extend({
        template: "SearchView.date",
        start: function() {
            this._super();
            this.datewidget = new openerp.web.DateTimeWidget(this);
            this.datewidget.prependTo(this.$element);
            this.datewidget.$element.find("input").attr("size", 15).attr("autofocus", this.attrs.default_focus === "1" ? "autofocus" : null).removeAttr("style");
            this.datewidget.set_value(this.defaults[this.attrs.name] || false)
        },
        get_value: function() {
            return this.datewidget.get_value() || null
        },
        clear: function() {
            this.datewidget.set_value(false)
        },
    });
    openerp.web.search.fields.add('datetime_picker', 'openerp.pf_utils.DateTimeField');

};
