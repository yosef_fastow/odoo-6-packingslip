# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import orm

import calendar
import datetime
import operator
import logging

from osv import fields, osv, expression
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv.orm import except_orm
import openerp.netsvc as netsvc
import json


_logger = logging.getLogger(__name__)


def _read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
    """
    Get the list of records in list view grouped by the given ``groupby`` fields

    :param cr: database cursor
    :param uid: current user id
    :param domain: list specifying search criteria [['field_name', 'operator', 'value'], ...]
    :param list fields: list of fields present in the list view specified on the object
    :param list groupby: fields by which the records will be grouped
    :param int offset: optional number of records to skip
    :param int limit: optional max number of records to return
    :param dict context: context arguments, like lang, time zone
    :param list orderby: optional ``order by`` specification, for
                         overriding the natural sort ordering of the
                         groups, see also :py:meth:`~osv.osv.osv.search`
                         (supported only for many2one fields currently)
    :return: list of dictionaries(one dictionary for each record) containing:

                * the values of fields grouped by the fields in ``groupby`` argument
                * __domain: list of tuples specifying the search criteria
                * __context: dictionary with argument like ``groupby``
    :rtype: [{'field_name_1': value, ...]
    :raise AccessError: * if user has no read rights on the requested object
                        * if user tries to bypass access rules for read on the requested object

    If the context parameter contains having_several, show groups with 2 or more elements

    """
    context = context or {}
    self.check_read(cr, uid)
    if not fields:
        fields = self._columns.keys()

    query = self._where_calc(cr, uid, domain, context=context)
    self._apply_ir_rules(cr, uid, query, 'read', context=context)

    # Take care of adding join(s) if groupby is an '_inherits'ed field
    groupby_list = groupby
    qualified_groupby_field = groupby
    if groupby:
        if isinstance(groupby, list):
            groupby = groupby[0]
        qualified_groupby_field = self._inherits_join_calc(groupby, query)

    if groupby:
        assert not groupby or groupby in fields, "Fields in 'groupby' must appear in the list of fields to read (perhaps it's missing in the list view?)"
        groupby_def = self._columns.get(groupby) or (self._inherit_fields.get(groupby) and self._inherit_fields.get(groupby)[2])
        assert groupby_def and groupby_def._classic_write, "Fields in 'groupby' must be regular database-persisted fields (no function or related fields), or function fields with store=True"

    # TODO it seems fields_get can be replaced by _all_columns (no need for translation)
    fget = self.fields_get(cr, uid, fields)
    flist = ''
    group_count = group_by = groupby
    if groupby:
        if fget.get(groupby):
            groupby_type = fget[groupby]['type']
            if groupby_type in ('date', 'datetime'):
                qualified_groupby_field = "to_char(%s,'yyyy-mm')" % qualified_groupby_field
                flist = "%s as %s " % (qualified_groupby_field, groupby)
            elif groupby_type == 'boolean':
                qualified_groupby_field = "coalesce(%s,false)" % qualified_groupby_field
                flist = "%s as %s " % (qualified_groupby_field, groupby)
            else:
                flist = qualified_groupby_field
        else:
            # Don't allow arbitrary values, as this would be a SQL injection vector!
            raise except_orm(_('Invalid group_by'),
                             _('Invalid group_by specification: "%s".\nA group_by specification must be a list of valid fields.')%(groupby,))

    aggregated_fields = [
        f for f in fields
        if f not in ('id', 'sequence')
        if fget[f]['type'] in ('integer', 'float', 'boolean')
        if (f in self._columns and getattr(self._columns[f], '_classic_write'))]
    for f in aggregated_fields:
        if fget[f]['type'] == 'boolean':
            group_operator = fget[f].get('group_operator', False)
            if not group_operator:
                continue
            elif group_operator == 'sum':
                qualified_field = 'cast("%s"."%s" as int)' % (self._table, f)
            else:
               qualified_field = '"%s"."%s"' % (self._table, f)

        else:
            group_operator = fget[f].get('group_operator', 'sum')
            qualified_field = '"%s"."%s"' % (self._table, f)

        if flist:
            flist += ', '
        flist += "%s(%s) AS %s" % (group_operator, qualified_field, f)

    gb = groupby and (' GROUP BY ' + qualified_groupby_field) or ''

    from_clause, where_clause, where_clause_params = query.get_sql()
    where_clause = where_clause and ' WHERE ' + where_clause
    limit_str = limit and ' limit %d' % limit or ''
    offset_str = offset and ' offset %d' % offset or ''

    havings = []
    if context.get('having_several', False):
        havings.append("AND count(*) > 1")
    for operation, field, sign, value in context.get('havings', []):
        havings.append("AND %s(%s) %s %s" % (operation, field, sign, value))

    having_str = havings and ' having 1=1 %s' % (" ".join(havings)) or ''
    if len(groupby_list) < 2 and context.get('group_by_no_leaf'):
        group_count = '_'
    cr.execute('SELECT min(%s.id) AS id, count(%s.id) AS %s_count' % (self._table, self._table, group_count) + (flist and ',') + flist + ' FROM ' + from_clause + where_clause + gb + having_str + limit_str + offset_str, where_clause_params)
    alldata = {}
    groupby = group_by
    for r in cr.dictfetchall():
        for fld, val in r.items():
            if val == None: r[fld] = False
        alldata[r['id']] = r
        del r['id']

    order = orderby or groupby
    data_ids = self.search(cr, uid, [('id', 'in', alldata.keys())], order=order, context=context)
    # the IDS of records that have groupby field value = False or '' should be sorted too
    data_ids += filter(lambda x:x not in data_ids, alldata.keys())
    data = self.read(cr, uid, data_ids, groupby and [groupby] or ['id'], context=context)
    # restore order of the search as read() uses the default _order (this is only for groups, so the size of data_read should be small):
    data.sort(lambda x,y: cmp(data_ids.index(x['id']), data_ids.index(y['id'])))

    for d in data:
        if groupby:
            d['__domain'] = [(groupby, '=', alldata[d['id']][groupby] or False)] + domain
            if not isinstance(groupby_list, (str, unicode)):
                if groupby or not context.get('group_by_no_leaf', False):
                    d['__context'] = {'group_by': groupby_list[1:]}
        if groupby and groupby in fget:
            if d[groupby] and fget[groupby]['type'] in ('date', 'datetime'):
                dt = datetime.datetime.strptime(alldata[d['id']][groupby][:7], '%Y-%m')
                days = calendar.monthrange(dt.year, dt.month)[1]

                d[groupby] = datetime.datetime.strptime(d[groupby][:10], '%Y-%m-%d').strftime('%B %Y')
                d['__domain'] = [(groupby, '>=', alldata[d['id']][groupby] and datetime.datetime.strptime(alldata[d['id']][groupby][:7] + '-01', '%Y-%m-%d').strftime('%Y-%m-%d') or False),\
                                 (groupby, '<=', alldata[d['id']][groupby] and datetime.datetime.strptime(alldata[d['id']][groupby][:7] + '-' + str(days), '%Y-%m-%d').strftime('%Y-%m-%d') or False)] + domain
            del alldata[d['id']][groupby]
        d.update(alldata[d['id']])
        del d['id']

    if groupby and groupby in self._group_by_full:
        data = self._read_group_fill_results(cr, uid, domain, groupby, groupby_list,
                                             aggregated_fields, data, read_group_order=order,
                                             context=context)

    return data

orm.BaseModel.read_group = _read_group


def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False, access_rights_uid=None):
    """
    Private implementation of search() method, allowing specifying the uid to use for the access right check.
    This is useful for example when filling in the selection list for a drop-down and avoiding access rights errors,
    by specifying ``access_rights_uid=1`` to bypass access rights check, but not ir.rules!
    This is ok at the security level because this method is private and not callable through XML-RPC.

    :param access_rights_uid: optional user ID to use when checking access rights
                              (not for ir.rules, this is only for ir.model.access)
    """
    if context is None:
        context = {}
    self.check_read(cr, access_rights_uid or user)

    # For transient models, restrict access to the current user, except for the super-user
    if self.is_transient() and self._log_access and user != SUPERUSER_ID:
        args = expression.AND(([('create_uid', '=', user)], args or []))

    query = self._where_calc(cr, user, args, context=context)
    self._apply_ir_rules(cr, user, query, 'read', context=context)
    order_by = self._generate_order_by(order, query)
    from_clause, where_clause, where_clause_params = query.get_sql()

    limit_str = limit and ' limit %d' % limit or ''
    offset_str = offset and ' offset %d' % offset or ''
    where_str = where_clause and (" WHERE %s" % where_clause) or ''

    distinct = context.get('distinct', False)

    if count:
        cr.execute('SELECT count("%s".id) FROM ' % self._table + from_clause + where_str + limit_str + offset_str, where_clause_params)
        res = cr.fetchall()
        return res[0][0]

    if distinct:
        group_by = ' GROUP BY ' + distinct
        cr.execute('SELECT min("%s".id) FROM ' % self._table + from_clause + where_str + group_by + order_by + limit_str + offset_str, where_clause_params)
    else:
        cr.execute('SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str, where_clause_params)
    res = cr.fetchall()
    return [x[0] for x in res]


def recalculate_stored_fields(self, cr, uid, ids, context=None):
    for f_, t_ in self._columns.iteritems():
        if isinstance(t_, fields.function) and t_.store:
            self._store_set_values(cr, uid, ids, [f_], context)


def clean_wkf_workitems(self, cr, uid, context=None):
    cr.execute("""
        DELETE
        FROM wkf_workitem
        WHERE id IN (
            SELECT w2.id
            FROM (
                SELECT count(*) AS COUNT,
                       wit.inst_id,
                       wit.act_id,
                       wit.state,
                       max(wit.id) AS id
                FROM wkf_workitem wit
                    left join wkf_instance win on win.id = wit.inst_id
                WHERE wit.state = 'complete'
                    and win.res_type = %(model)s
                GROUP BY wit.inst_id,
                         wit.act_id,
                         wit.state
                HAVING COUNT(*) > 1
             ) gr
                LEFT JOIN wkf_workitem w2 ON w2.inst_id = gr.inst_id
            AND w2.act_id = gr.act_id
            AND w2.STATE = gr.STATE
            AND w2.id <> gr.id
        )
    """, {'model': self._name})
    return True


def write(self, cr, user, ids, vals, context=None):
    readonly = None
    for field in vals.copy():
        fobj = None
        if field in self._columns:
            fobj = self._columns[field]
        elif field in self._inherit_fields:
            fobj = self._inherit_fields[field][2]
        if not fobj:
            continue
        groups = fobj.write

        if groups:
            edit = False
            for group in groups:
                module = group.split(".")[0]
                grp = group.split(".")[1]
                cr.execute("""
                    select count(*)
                    from res_groups_users_rel
                    where gid IN (
                        select res_id
                        from ir_model_data
                        where name=%s and module=%s and model=%s
                    ) and uid=%s""", (
                        grp, module, 'res.groups', user
                    )
                )
                readonly = cr.fetchall()
                if readonly[0][0] >= 1:
                    edit = True
                    break

            if not edit:
                vals.pop(field)

    if not context:
        context = {}
    if not ids:
        return True
    if isinstance(ids, (int, long)):
        ids = [ids]

    self._check_concurrency(cr, ids, context)
    self.check_write(cr, user)

    result = self._store_get_values(cr, user, ids, vals.keys(), context) or []

    # No direct update of parent_left/right
    vals.pop('parent_left', None)
    vals.pop('parent_right', None)

    parents_changed = []
    parent_order = self._parent_order or self._order
    if self._parent_store and (self._parent_name in vals):
        # The parent_left/right computation may take up to
        # 5 seconds. No need to recompute the values if the
        # parent is the same.
        # Note: to respect parent_order, nodes must be processed in
        # order, so ``parents_changed`` must be ordered properly.
        parent_val = vals[self._parent_name]
        if parent_val:
            query = "SELECT id FROM %s WHERE id IN %%s AND (%s != %%s OR %s IS NULL) ORDER BY %s" % \
                            (self._table, self._parent_name, self._parent_name, parent_order)
            cr.execute(query, (tuple(ids), parent_val))
        else:
            query = "SELECT id FROM %s WHERE id IN %%s AND (%s IS NOT NULL) ORDER BY %s" % \
                            (self._table, self._parent_name, parent_order)
            cr.execute(query, (tuple(ids),))
        parents_changed = map(operator.itemgetter(0), cr.fetchall())

    upd0 = []
    upd1 = []
    upd_todo = []
    updend = []
    direct = []
    totranslate = context.get('lang', False) and (context['lang'] != 'en_US')
    for field in vals:
        if field in self._columns:
            if self._columns[field]._classic_write and not (hasattr(self._columns[field], '_fnct_inv')):
                if (not totranslate) or not self._columns[field].translate:
                    upd0.append('"'+field+'"='+self._columns[field]._symbol_set[0])
                    upd1.append(self._columns[field]._symbol_set[1](vals[field]))
                direct.append(field)
            else:
                upd_todo.append(field)
        else:
            updend.append(field)
        if field in self._columns \
                and hasattr(self._columns[field], 'selection') \
                and vals[field]:
            self._check_selection_field_value(cr, user, field, vals[field], context=context)

    if self._log_access and (self._log_empty_update or upd0):
        upd0.append('write_uid=%s')
        upd0.append("write_date=(now() at time zone 'UTC')")
        upd1.append(user)

    if len(upd0):
        self.check_access_rule(cr, user, ids, 'write', context=context)
        for sub_ids in cr.split_for_in_conditions(ids):
            cr.execute('update ' + self._table + ' set ' + ','.join(upd0) + ' ' \
                       'where id IN %s', upd1 + [sub_ids])
            if cr.rowcount != len(sub_ids):
                raise except_orm(_('AccessError'),
                                 _('One of the records you are trying to modify has already been deleted (Document type: %s).') % self._description)

        if totranslate:
            # TODO: optimize
            for f in direct:
                if self._columns[f].translate:
                    src_trans = self.pool.get(self._name).read(cr, user, ids, [f])[0][f]
                    if not src_trans:
                        src_trans = vals[f]
                        # Inserting value to DB
                        self.write(cr, user, ids, {f: vals[f]})
                    self.pool.get('ir.translation')._set_ids(cr, user, self._name+','+f, 'model', context['lang'], ids, vals[f], src_trans)


    # call the 'set' method of fields which are not classic_write
    upd_todo.sort(lambda x, y: self._columns[x].priority-self._columns[y].priority)

    # default element in context must be removed when call a one2many or many2many
    rel_context = context.copy()
    for c in context.items():
        if c[0].startswith('default_'):
            del rel_context[c[0]]

    for field in upd_todo:
        for id in ids:
            result += self._columns[field].set(cr, self, id, field, vals[field], user, context=rel_context) or []

    unknown_fields = updend[:]
    for table in self._inherits:
        col = self._inherits[table]
        nids = []
        for sub_ids in cr.split_for_in_conditions(ids):
            cr.execute('select distinct "'+col+'" from "'+self._table+'" ' \
                       'where id IN %s', (sub_ids,))
            nids.extend([x[0] for x in cr.fetchall()])

        v = {}
        for val in updend:
            if self._inherit_fields[val][0] == table:
                v[val] = vals[val]
                unknown_fields.remove(val)
        if v:
            self.pool.get(table).write(cr, user, nids, v, context)

    if unknown_fields:
        _logger.warning(
            'No such field(s) in model %s: %s.',
            self._name, ', '.join(unknown_fields))
    self._validate(cr, user, ids, context)

    # TODO: use _order to set dest at the right position and not first node of parent
    # We can't defer parent_store computation because the stored function
    # fields that are computer may refer (directly or indirectly) to
    # parent_left/right (via a child_of domain)
    if parents_changed:
        if self.pool._init:
            self.pool._init_parent[self._name] = True
        else:
            order = self._parent_order or self._order
            parent_val = vals[self._parent_name]
            if parent_val:
                clause, params = '%s=%%s' % (self._parent_name,), (parent_val,)
            else:
                clause, params = '%s IS NULL' % (self._parent_name,), ()

            for id in parents_changed:
                cr.execute('SELECT parent_left, parent_right FROM %s WHERE id=%%s' % (self._table,), (id,))
                pleft, pright = cr.fetchone()
                distance = pright - pleft + 1

                # Positions of current siblings, to locate proper insertion point;
                # this can _not_ be fetched outside the loop, as it needs to be refreshed
                # after each update, in case several nodes are sequentially inserted one
                # next to the other (i.e computed incrementally)
                cr.execute('SELECT parent_right, id FROM %s WHERE %s ORDER BY %s' % (self._table, clause, parent_order), params)
                parents = cr.fetchall()

                # Find Position of the element
                position = None
                for (parent_pright, parent_id) in parents:
                    if parent_id == id:
                        break
                    position = parent_pright + 1

                # It's the first node of the parent
                if not position:
                    if not parent_val:
                        position = 1
                    else:
                        cr.execute('select parent_left from '+self._table+' where id=%s', (parent_val,))
                        position = cr.fetchone()[0] + 1

                if pleft < position <= pright:
                    raise except_orm(_('UserError'), _('Recursivity Detected.'))

                if pleft < position:
                    cr.execute('update '+self._table+' set parent_left=parent_left+%s where parent_left>=%s', (distance, position))
                    cr.execute('update '+self._table+' set parent_right=parent_right+%s where parent_right>=%s', (distance, position))
                    cr.execute('update '+self._table+' set parent_left=parent_left+%s, parent_right=parent_right+%s where parent_left>=%s and parent_left<%s', (position-pleft, position-pleft, pleft, pright))
                else:
                    cr.execute('update '+self._table+' set parent_left=parent_left+%s where parent_left>=%s', (distance, position))
                    cr.execute('update '+self._table+' set parent_right=parent_right+%s where parent_right>=%s', (distance, position))
                    cr.execute('update '+self._table+' set parent_left=parent_left-%s, parent_right=parent_right-%s where parent_left>=%s and parent_left<%s', (pleft-position+distance, pleft-position+distance, pleft+distance, pright+distance))

    result += self._store_get_values(cr, user, ids, vals.keys(), context)
    result.sort()

    done = {}
    for order, object, ids_to_update, fields_to_recompute in result:
        key = (object, tuple(fields_to_recompute))
        done.setdefault(key, {})
        # avoid to do several times the same computation
        todo = []
        for id in ids_to_update:
            if id not in done[key]:
                done[key][id] = True
                todo.append(id)
        self.pool.get(object)._store_set_values(cr, user, todo, fields_to_recompute, context)

    wf_service = netsvc.LocalService("workflow")
    for id in ids:
        wf_service.trg_write(user, self._name, id, cr)
    return True

def decode_json_fields(self, json_data):
    default = {}
    if not json_data:
        return default
    result = json.loads(json_data)
    return result if isinstance(result, dict) else default

def audit_fill_uid(self, cr, uid, record_id, result, original_uid):
    if not original_uid:
        record = self.read(cr, uid, record_id, ['write_uid', 'create_uid'])
        if record:
            original_uid = record.get('write_uid') or record.get('create_uid')
    current_uid = original_uid or 1 # if inserted by admin without uids
    for transact in result:
        if 'write_uid' in transact['fields']:
            current_uid = transact['fields']['write_uid'][1]
        direct_query = 'write_date' not in transact['fields']
        direct_query &= 'create_date' not in transact['fields']
        transact['uid'] = 1 if direct_query else current_uid
    return result

def clean_not_changed_data(self, result):
    for transact in result:
        transact['fields'] = {
            f: v for f, v in transact['fields'].iteritems() if v[0] != v[1]
        }
    return result

def get_history_changes(self, cr, uid, record_id, context=None):
    result = []
    actions_map = {
        'I': 'create',
        'U': 'write',
        'D': 'delete',
    }
    cr.execute("""SELECT
            original_data,
            new_data,
            action,
            transaction_id,
            action_tstamp
        FROM audit.logged_actions
        WHERE table_name = '%s'
        AND record_id = %d
        ORDER BY event_id
    """ % (self._table, record_id))
    query_res = cr.dictfetchall()
    if not query_res:
        return result
    transact_id = None
    transact_details = None

    original_uid = None
    for query_row in query_res:
        if transact_id != query_row['transaction_id']:
            transact_id = query_row['transaction_id']
            transact_fields = {}
            transact_details = {
                'action_date': query_row['action_tstamp'],
                'action': actions_map.get(query_row['action']),
                'fields': transact_fields,
                'uid': None,
                'transact_id': transact_id,
            }
            result.append(transact_details)
        elif query_row['action'] == 'D':
            transact_details['action'] = actions_map['D']

        old_data = self._decode_json_fields(query_row['original_data'])
        for old_field in old_data:
            if old_field not in transact_fields:
                transact_fields[old_field] = [old_data[old_field], None]
            if not original_uid and old_field == 'write_uid':
                original_uid = old_data[old_field]

        new_data = self._decode_json_fields(query_row['new_data'])
        for new_field in new_data:
            if new_field not in transact_fields:
                transact_fields[new_field] = [None, None]
            transact_fields[new_field][1] = new_data[new_field]
            if not original_uid and new_field == 'create_uid':
                original_uid = new_data[new_field]
    result = self._audit_fill_uid(cr, uid, record_id, result, original_uid)
    return self._clean_not_changed_data(result)

orm.BaseModel._log_empty_update = True

orm.BaseModel._search = search
orm.BaseModel.write = write
orm.BaseModel.recalculate_stored_fields = recalculate_stored_fields
orm.BaseModel.clean_wkf_workitems = clean_wkf_workitems
orm.BaseModel.get_history_changes = get_history_changes
orm.BaseModel._decode_json_fields = decode_json_fields
orm.BaseModel._audit_fill_uid = audit_fill_uid
orm.BaseModel._clean_not_changed_data = clean_not_changed_data
