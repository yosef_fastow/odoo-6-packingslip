# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce.com
#    Copyright (C) 2014
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import calendar
import time
import logging
import threading
from datetime import datetime

# from openerp.addons.pf_utils.utils.decorators import profile_method

import psycopg2

import openerp
from osv import fields, osv
from tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


def _get_type_options(self, cursor, user_id, context=None):
    options = (
        ('printing_packings', 'Printing Packings'),
        ('packing_slip_manual', 'Manual Printing Packings'),
        ('get_orders', 'Get Orders'),
        ('create_orders', 'Create Orders'),
        ('load_orders', 'Load Orders'),
        ('update_qty', 'Update QTY'),
        ('set_done', 'Set Done'),
        ('export', 'Export Template'),
        ('task', 'Task Generation'),
        ('other', 'Other'),
    )
    return options


class ir_cron(osv.osv):
    _inherit = "ir.cron"

    _columns = {
        'description': fields.char('Description', size=256, help="Cron description."),
        'last_duration': fields.float('Last cron duration in seconds', help="Last run duration."),
        'type': fields.selection(_get_type_options, 'Type', help="Type specific for workers."),
        #
        'schedule_by_day': fields.boolean("Schedule by days"),
        'weekday0': fields.boolean('Mo'),
        'weekday1': fields.boolean('Tu'),
        'weekday2': fields.boolean('We'),
        'weekday3': fields.boolean('Th'),
        'weekday4': fields.boolean('Fr'),
        'weekday5': fields.boolean('Sa'),
        'weekday6': fields.boolean('Su'),
    }

    def init(self, cr):
        cr.execute("""
            CREATE OR REPLACE FUNCTION get_crons(
                allowed_types_sql varchar[] DEFAULT ARRAY[]::varchar[],
                forbidden_types_sql varchar[] DEFAULT ARRAY[]::varchar[]
            )
            RETURNS SETOF ir_cron AS $$
            DECLARE
                r ir_cron%rowtype;
                day_field varchar;
                allowed_cnd varchar = '';
                forbidde_cnd varchar = '';
            BEGIN
                day_field := concat('weekday', (EXTRACT(DOW FROM NOW())) -1);
                IF ARRAY_LENGTH(allowed_types_sql, 1) > 0 THEN
                    allowed_cnd := ' AND type = ANY ('||quote_nullable(allowed_types_sql)||')';
                END IF;
                IF ARRAY_LENGTH(forbidden_types_sql, 1) > 0 THEN
                    forbidde_cnd := ' AND type != ANY ('||quote_nullable(forbidden_types_sql)||')';
                END IF;
                FOR r IN
                    EXECUTE FORMAT('
                        SELECT *
                        FROM ir_cron
                        WHERE numbercall != 0
                            AND active AND nextcall <= (now() at time zone ''UTC'')
                            AND (
                                schedule_by_day IS NOT True OR %I = True
                            ) %s %s
                        ORDER BY priority, nextcall
                    ', day_field, allowed_cnd, forbidde_cnd)
                LOOP
                    RETURN NEXT r;
                END LOOP;
                RETURN;
            END;
            $$
            LANGUAGE plpgsql;
        """)

    @classmethod
    def _acquire_job(cls, db_name, allowed_types=False, forbidden_types=False):
        # TODO remove 'check' argument from addons/base_action_rule/base_action_rule.py
        """ Try to process one cron job.

        This selects in database all the jobs that should be processed. It then
        tries to lock each of them and, if it succeeds, run the cron job (if it
        doesn't succeed, it means the job was already locked to be taken care
        of by another thread) and return.

        If a job was processed, returns True, otherwise returns False.
        """
        db = openerp.sql_db.db_connect(db_name)
        cr = db.cursor()
        try:
            allowed_types_sql = "ARRAY[" + "'{}'".format("', '".join(allowed_types)) + "]::varchar[]" if allowed_types else 'null'
            forbidden_types_sql = "ARRAY[" + "'{}'".format("', '".join(forbidden_types)) + "]::varchar[]" if forbidden_types else 'null'
            # Careful to compare timestamps with 'UTC' - everything is UTC as of v6.1.
            cr.execute("""SELECT * FROM get_crons(
                allowed_types_sql := {allowed_types_sql},
                forbidden_types_sql := {forbidden_types_sql}
                )""".format(allowed_types_sql=allowed_types_sql, forbidden_types_sql=forbidden_types_sql))
            for job in cr.dictfetchall():
                openerp.modules.registry.RegistryManager.check_registry_signaling(db_name)
                registry = openerp.pooler.get_pool(db_name)
                task_cr = db.cursor()
                try:
                    # Try to grab an exclusive lock on the job row from within the task transaction
                    acquired_lock = False
                    if registry[cls._name]._check_lock(job['id']):
                        _logger.debug('Another process/thread is already busy executing job `%s`, skipping it.', job['name'])
                        continue
                    registry[cls._name]._lock_job(job['id'])
                    task_cr.execute("""
                        SELECT *
                        FROM ir_cron
                        WHERE id=%s
                            AND numbercall != 0
                            AND nextcall <= (now() at time zone 'UTC')
                        FOR UPDATE NOWAIT""",
                       (job['id'],),
                       log_exceptions=False
                   )
                    acquired_lock = True
                except psycopg2.OperationalError, e:
                    if e.pgcode == '55P03':
                        # Class 55: Object not in prerequisite state; 55P03: lock_not_available
                        _logger.debug('Another process/thread is already busy executing job `%s`, skipping it.', job['name'])
                        continue
                    else:
                        # Unexpected OperationalError
                        raise
                finally:
                    if not acquired_lock:
                        # we're exiting due to an exception while acquiring the lot
                        task_cr.close()
                        registry[cls._name]._unlock_job(job['id'])

                # Got the lock on the job row, run its code
                _logger.debug('Starting job `%s`.', job['name'])

                registry[cls._name]._process_job(task_cr, job)
                openerp.modules.registry.RegistryManager.signal_caches_change(db_name)
                return True

        except psycopg2.ProgrammingError, e:
            if e.pgcode == '42P01':
                # Class 42 — Syntax Error or Access Rule Violation; 42P01: undefined_table
                # The table ir_cron does not exist; this is probably not an OpenERP database.
                _logger.warning('Tried to poll an undefined table on database %s.', db_name)
            else:
                raise
        except Exception, ex:
            _logger.warning('Exception in cron:', exc_info=True)

        finally:
            cr.commit()
            cr.close()

        return False

    def _run_jobs_multithread(self):
        # TODO remove 'check' argument from addons/base_action_rule/base_action_rule.py
        """ Process the cron jobs by spawning worker threads.

        This selects in database all the jobs that should be processed. It then
        tries to lock each of them and, if it succeeds, spawns a thread to run
        the cron job (if it doesn't succeed, it means the job was already
        locked to be taken care of by another thread).

        The cursor used to lock the job in database is given to the worker
        thread (which has to close it itself).

        """
        db = self.pool.db
        cr = db.cursor()
        db_name = db.dbname
        try:
            jobs = {}  # mapping job ids to jobs for all jobs being processed.
            now = datetime.now()
            # Careful to compare timestamps with 'UTC' - everything is UTC as of v6.1.
            cr.execute("""SELECT * FROM get_crons()""")
            for job in cr.dictfetchall():
                if not openerp.cron.get_thread_slots():
                    break
                jobs[job['id']] = job

                task_cr = db.cursor()
                try:
                    # Try to grab an exclusive lock on the job row from within the task transaction
                    acquired_lock = False
                    task_cr.execute("""
                        SELECT *
                        FROM ir_cron
                        WHERE id=%s
                        FOR UPDATE NOWAIT""",
                       (job['id'],),
                       log_exceptions=False
                    )
                    acquired_lock = True
                except psycopg2.OperationalError, e:
                    if e.pgcode == '55P03':
                        # Class 55: Object not in prerequisite state; 55P03: lock_not_available
                        _logger.debug('Another process/thread is already busy executing job `%s`, skipping it.', job['name'])
                        continue
                    else:
                        # Unexpected OperationalError
                        raise
                finally:
                    if not acquired_lock:
                        # we're exiting due to an exception while acquiring the lot
                        task_cr.close()

                if self._check_lock(job['id']):
                    _logger.debug('Another process/thread is already busy executing job `%s`, skipping it.', job['name'])
                    continue

                # http://bugs.python.org/issue7980
                datetime.strptime('2012-01-01', '%Y-%m-%d')
                # Got the lock on the job row, now spawn a thread to execute it in the transaction with the lock
                task_thread = threading.Thread(target=self._run_job, name=job['name'], args=(task_cr, job, now))
                # force non-daemon task threads (the runner thread must be daemon, and this property is inherited by default)
                task_thread.setDaemon(False)
                openerp.cron.take_thread_slot()
                task_thread.start()
                _logger.debug('Cron execution thread for job `%s` spawned', job['name'])

            # Find next earliest job ignoring currently processed jobs (by this and other cron threads)
            find_next_time_query = """SELECT min(nextcall) AS min_next_call FROM get_crons()"""
            if jobs:
                cr.execute(find_next_time_query + " WHERE id NOT IN %s", (tuple(jobs.keys()),))
            else:
                cr.execute(find_next_time_query)
            next_call = cr.dictfetchone()['min_next_call']

            if next_call:
                next_call = calendar.timegm(time.strptime(next_call, DEFAULT_SERVER_DATETIME_FORMAT))
            else:
                # no matching cron job found in database, re-schedule arbitrarily in 1 day,
                # this delay will likely be modified when running jobs complete their tasks
                next_call = time.time() + (24*3600)

            openerp.cron.schedule_wakeup(next_call, db_name)

        except Exception, ex:
            _logger.warning('Exception in cron:', exc_info=True)

        finally:
            cr.commit()
            cr.close()

ir_cron()
