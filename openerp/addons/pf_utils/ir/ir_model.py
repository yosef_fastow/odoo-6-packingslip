# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce.com
#    Copyright (C) 2015
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields,osv
from osv.orm import except_orm, browse_record
import tools
from tools.translate import _


class ir_model_access(osv.osv):
    _inherit = 'ir.model.access'

    def check_group_rule(self, cr, uid, model_name, mode):
        cr.execute('SELECT MAX(CASE WHEN perm_' + mode + ' THEN 1 ELSE 0 END) '
                   '  FROM ir_model_access a '
                   '  JOIN ir_model m ON (m.id = a.model_id) '
                   '  JOIN res_groups_users_rel gu ON (gu.gid = a.group_id) '
                   ' WHERE m.model = %s '
                   '   AND gu.uid = %s '
                   , (model_name, uid,)
                   )
        return cr.fetchone()[0]

    @tools.ormcache()
    def check(self, cr, uid, model, mode='read', raise_exception=True, context=None):
        result = super(ir_model_access, self).check(cr, uid, model, mode, raise_exception, context)
        model_name = isinstance(model, browse_record) and model.model or model
        if (
            result
            and mode != 'read'
            and self.pool.get('res.users')._is_user_readonly(cr, uid)
            and raise_exception
            and not self.check_group_rule(cr, uid, model_name, mode)
        ):
            raise except_orm(_('Error'), _("Access mode: %s for model %s is forbidden for readonly users!") %(mode, model_name))
        return result

ir_model_access()