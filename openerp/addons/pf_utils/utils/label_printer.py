# -*- coding: utf-8 -*-

from urllib import urlencode
import json
import time

class LabelWizPrinter(object):

    def __init__(self, pool, cr, uid):
        param_model = pool.get('ir.config_parameter')
        param_name = 'base.url.for.summary.link'
        self.base_url = param_model.get_param(cr, uid, param_name)
        self.base_url += '/inventory_report/label_wiz/label_printer?'

    def print_label(self, product_id, size_id, template, force=None):
        url_params = {
            'product_id': product_id,
            'size_id': size_id,
            'template': template,
            'serverTemplate': '1',
            'sid': int(round(time.time() * 1000)),
            'autoprint': force.get('autoprint', 0)
        }

        if force:
            url_params['force'] = json.dumps(force)

        return self.send_request(url_params)

    def print_packing_label(self, picking_id, template):
        url_params = {
            'picking_id': picking_id,
            'template': template,
            'serverTemplate': '1',
        }
        return self.send_request(url_params)

    def send_request(self, url_params):
        return {
            'name': 'Print label',
            'type': 'ir.actions.act_url',
            'url': self.base_url + urlencode(url_params),
            'target': 'new'
        }
