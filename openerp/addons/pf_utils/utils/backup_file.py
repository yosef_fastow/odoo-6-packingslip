# -*- coding: utf-8 -*-
from os import access as os_access
from os import R_OK
from os import W_OK
from os import makedirs as os_makedirs
from os import pardir as os_pardir
from os.path import dirname as os_path_dirname
from os.path import basename as os_path_basename
from os.path import abspath as os_path_abspath
from os.path import join as os_path_join
from os.path import exists as os_path_exists
from os.path import realpath as os_path_realpath
from paramiko import SSHClient, AutoAddPolicy
import socket
import errno
from tempfile import TemporaryFile
import subprocess
import logging

_logger = logging.getLogger(__name__)

access_mode_map = {
    'r': {
        'modes': (R_OK, ),
        'error_msg': "Couldn't have access for reading from file: '{0}'",
    },
    'rb': {
        'modes': (R_OK, ),
        'error_msg': "Couldn't have access for reading bytes from file: '{0}'",
    },
    'r+': {
        'modes': (R_OK, W_OK, ),
        'error_msg': "Couldn't have access for reading/writing file: '{0}'",
    },
    'rb+': {
        'modes': (R_OK, W_OK, ),
        'error_msg': "Couldn't have access for reading/writing file: '{0}'",
    },
    'w': {
        'modes': (W_OK, ),
        'error_msg': "Couldn't have access for writing to file: '{0}'",
    },
    'wb': {
        'modes': (W_OK, ),
        'error_msg': "Couldn't have access for writing bytes to file: '{0}'",
    },
    'w+': {
        'modes': (R_OK, W_OK, ),
        'error_msg': "Couldn't have access for reading/writing file: '{0}'",
    },
    'wb+': {
        'modes': (W_OK, ),
        'error_msg': "Couldn't have access for reading/writing file: '{0}'",
    },
    'a': {
        'modes': (W_OK, ),
        'error_msg': "Couldn't have access for append to file: '{0}'",
    },
    'ab': {
        'modes': (W_OK, ),
        'error_msg': "Couldn't have access for append to file: '{0}'",
    },
    'a+': {
        'modes': (R_OK, W_OK, ),
        'error_msg': "Couldn't have access for reading/append to file: '{0}'",
    },
    'ab+': {
        'modes': (R_OK, W_OK, ),
        'error_msg': "Couldn't have access for reading/append to file: '{0}'",
    },
    'nothing': {
        'modes': (),
        'error_msg': "Unknow open parametr for file: {0}",
    },
}


nedd_change_over_ssh_map = {
    'DelmarERPAPI02': 'api',
    'DelmarERPAPI03': 'api',
    'DelmarERPAPI04': 'api',
    'DelmarERPProd': 'api',
}


ssh_profiles = {
    'api': {
        'host':  '192.168.100.42',
        'username':  'delmar',
        'password': 'JprogI!4800',
    },
}


class BackupFileAccessError(Exception):
    def __init__(self, full_filename, access_mode):
        self.message = access_mode_map.get(access_mode, {'error_msg': 'Error: {0}'})['error_msg'].format(full_filename)
        _logger.exception(self.message)


class BackupFileError(Exception):
    def __init__(self, message):
        self.message = message
        _logger.exception(self.message)


# TODO:
# 1: need inheritance writeline, writelines, readline, readlines methods
# 2: need replace __str__ method
class BackupFile(file):

    def write(self, *args, **kwargs):
        try:
            return super(BackupFile, self).write(*args, **kwargs)
        except IOError:
            if (ssh_chmod(self.name)):
                return super(BackupFile, self).write(*args, **kwargs)
            else:
                raise BackupFileAccessError(self.name, self.mode)

    def read(self, *args, **kwargs):
        try:
            return super(BackupFile, self).read(*args, **kwargs)
        except IOError:
            if (ssh_chmod(self.name)):
                return super(BackupFile, self).read(*args, **kwargs)
            else:
                raise BackupFileAccessError(self.name, self.mode)


class backup_file_open(object):

    def __init__(self, filename, access_mode='r', *args, **kwargs):
        dirname = os_path_dirname(filename)
        basename = os_path_basename(filename)
        if ((not dirname) or (not basename)):
            raise BackupFileError('Only working with full filename')
        smart_makedirs(dirname)
        ssh_chmod(filename)
        self.f = open(filename, access_mode, *args, **kwargs)

    def __enter__(self):
        return self.f

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()


def parent_dir(input_dir):
    return os_path_abspath(os_path_join(input_dir, os_pardir))


def smart_makedirs(input_dir):
    result = None
    if (not os_path_exists(input_dir)):
        path_list = []
        _path = input_dir
        while (_path != '/'):
            _path = parent_dir(_path)
            if (_path == '/'):
                break
            path_list.append({
                'path': _path,
                'exists': os_path_exists(_path),
                'readable': os_access(_path, R_OK),
                'writable': is_writable(_path)
            })
        availaible_parent_not_writable = [a['path'] for a in path_list if a['exists'] and not a['writable']]
        for path in availaible_parent_not_writable:
            ssh_chmod(path)
        try:
            os_makedirs(input_dir)
        except OSError as os_err:
            if (os_err.errno == errno.EEXIST):
                pass
            else:
                raise
        result = True
    else:
        result = False
    if (not os_access(input_dir, W_OK)):
        ssh_chmod(input_dir)
    return result


def ssh_chmod(PATH):
    result = None
    host = socket.gethostname()
    profile_name = nedd_change_over_ssh_map.get(host, False)
    if (profile_name):
        settings = ssh_profiles.get(profile_name, False)
        if (settings):
            try:
                chmod_command = '{chmod_script} "{PATH}"'.format(
                    chmod_script='/home/delmar/delmar_openerp/openerp/addons/pf_utils/utils/chmod_file',
                    PATH=PATH
                )
                ssh = SSHClient()
                ssh.set_missing_host_key_policy(AutoAddPolicy())
                ssh.connect(settings['host'], username=settings['username'], password=settings['password'])
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(chmod_command)
                err_ssh_msg = ssh_stderr.read()
                if (err_ssh_msg):
                    result = False
                    _logger.error(str(err_ssh_msg))
                else:
                    result = True
                ssh.close()
            except Exception as ex:
                result = False
                _logger.exception(str(ex))
    else:
        parent_dir = os_path_dirname(os_path_realpath(__file__))
        sub_res = subprocess.check_output(['{parent_dir}/chmod_file'.format(parent_dir=parent_dir), PATH])
        if (sub_res.strip() in ['False', 'None', False, None]):
            result = False
        else:
            result = True
    return result


def is_writable(dirname):
    try:
        tempfile = TemporaryFile(dir=dirname)
        tempfile.close()
    except OSError:
        return False
    return True
