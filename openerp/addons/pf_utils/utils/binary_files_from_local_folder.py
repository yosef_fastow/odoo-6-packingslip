# -*- coding: utf-8 -*-
from os import path, access, R_OK
import base64


def _get_file(self, cr, uid, ids, *args, **kwargs):
    data = ""
    PATH = ""

    result = {}
    for h_id in ids:
        result[h_id] = False

    if len(ids) > 1:
        return result

    records = self.browse(cr, uid, ids)
    for r in records:
        try:
            if 'download_readable' in args:
                PATH = str(r.file_readable)
            else:
                PATH = str(r.full_filename)
        except AttributeError:
            PATH = str(r.imported_filename)
        if path.exists(PATH) and path.isfile(PATH) and access(PATH, R_OK):
            with open(PATH, 'rb') as f:
                data = base64.b64encode(f.read())
            result[r.id] = data
        else:
            result[r.id] = False
    return result


def _get_size(self, cr, uid, ids, *args, **kwargs):
    PATH = ""

    result = {}
    for h_id in ids:
        result[h_id] = False

    records = self.browse(cr, uid, ids)
    for r in records:
        try:
            PATH = str(r.full_filename)
        except AttributeError:
            PATH = str(r.imported_filename)
        if path.exists(PATH) and path.isfile(PATH) and access(PATH, R_OK):
            size = path.getsize(PATH)
            result[r.id] = size
        else:
            result[r.id] = "File not found!"
    return result


def _diff_files(from_file_data, to_file_data):
    import difflib
    lines1 = str(from_file_data).strip().split('\n')
    lines2 = str(to_file_data).strip().split('\n')
    result_lines = []
    result = None
    for line in difflib.unified_diff(lines1, lines2, fromfile='before', tofile='after'):
        result_lines.append(str(line).replace('\n', ''))
    if(result_lines):
        result = "\n".join(result_lines)
    else:
        result = False
    return result


def print_as_hex(s):
    """
    Print a string as hex bytes.
    """

    print(":".join("{0:x}".format(ord(c)) for c in s))


def get_starting_chunk(filename=None, data=None, length=1024):
    result = None
    if(filename):
        with open(filename, 'rb') as f:
            chunk = f.read(length)
            result = chunk
    elif(data):
        if(len(data) > length):
            result = data[:length]
        else:
            result = data
    else:
        result = False
    return result


_printable_extended_ascii = b'\n\r\t\f\b'
if bytes is str:
    # Python 2 means we need to invoke chr() explicitly
    _printable_extended_ascii += b''.join(map(chr, range(32, 256)))
else:
    # Python 3 means bytes accepts integer input directly
    _printable_extended_ascii += bytes(range(32, 256))


def is_binary_string(bytes_to_check):
    if not bytes_to_check:
        return False

    if b'\x00' in bytes_to_check:
        return True
    control_chars = bytes_to_check.translate(None, _printable_extended_ascii)
    nontext_ratio = float(len(control_chars)) / float(len(bytes_to_check))
    return nontext_ratio > 0.3
