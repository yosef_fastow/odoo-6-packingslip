# -*- coding: utf-8 -*-
from csv import Sniffer as csv_sniffer
from cStringIO import StringIO as IO
from csv import Error as csvError
from csv import DictReader, DictWriter
import csv
import codecs


def detect_dialect(data):
    result = None
    if(data):
        _dialect = None
        _sniffer = csv_sniffer()
        _pseudo_file = IO(data)
        try:
            _dialect = _sniffer.sniff(_pseudo_file.read(2000))
        except csvError:
            _dialect = False
        finally:
            _pseudo_file.seek(0)
            result = _dialect
    else:
        result = False
    return result


def strip_lower_del_spaces_from_header(csv_data, _dialect=None):
    result = False
    out_file = IO()
    if(not _dialect):
        _dialect = detect_dialect(csv_data)
    dr = DictReader(IO(csv_data), dialect=_dialect)
    old_fieldnames = dr.fieldnames
    new_fieldnames = []
    old_new_accord = {}
    for old_fieldname in old_fieldnames:
        new_fieldname = str(old_fieldname).strip().lower().replace(' ', '')
        new_fieldnames.append(new_fieldname)
        old_new_accord[new_fieldname] = old_fieldname
    dw = DictWriter(out_file, dialect=_dialect, fieldnames=new_fieldnames)
    dw.writerow(dict((fn, fn) for fn in new_fieldnames))
    for row in dr:
        new_row = {}
        for new_fieldname in new_fieldnames:
            if(row.get(old_new_accord[new_fieldname], None) is not None):
                new_row[new_fieldname] = row[old_new_accord[new_fieldname]]
        dw.writerow(new_row)
    result = out_file.getvalue()
    return result


def strip_lower_del_spaces_from_ids(ids):
    for name_id in ids:
        ids[name_id] = str(ids[name_id]).strip().lower().replace(' ', '')
    return ids


class UTF8Recoder:
    """
    Iterator that reads an encoded stream and re-encodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")


class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = IO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([unicode(s).encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and re-encode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)
