# -*- coding: utf-8 -*-
from yaml import load, dump
from os import access, F_OK, W_OK, R_OK
import logging
from yaml import YAMLError

try:
    from yaml import CLoader as Loader, CSafeDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

_logger = logging.getLogger(__name__)


class YamlObject(object):

    def __init__(self, work_with_file=None):
        super(YamlObject, self).__init__()
        self.filename = work_with_file

    def deserialize(self, _data=None):
        result = None
        if(self.filename is not None):
            if not access(self.filename, F_OK):
                _logger.error("no find file: %s!" % self.filename)
                result = None
            else:
                if access(self.filename, R_OK):
                    with open(self.filename, "r") as _file:
                        try:
                            result = load(_file, Loader=Loader)
                        except YAMLError, _error:
                            if hasattr(_error, 'problem_mark'):
                                mark = _error.problem_mark
                                _logger.error("Error position: (%s:%s)" % (mark.line+1, mark.column+1))
                            else:
                                _logger.error("Error yaml load: %s" % (str(_error)))
                else:
                    _logger.error("no access reading file: %s" % self.filename)
                    result = None
        else:
            if(_data is not None):
                try:
                    result = load(_data, Loader=Loader)
                except YAMLError, _error:
                    if hasattr(_error, 'problem_mark'):
                        mark = _error.problem_mark
                        _logger.error("Error position: (%s:%s)" % (mark.line+1, mark.column+1))
                    else:
                        _logger.error("Error yaml load: %s" % (str(_error)))
            else:
                _logger.error("not find deserialize data!")
                result = None
        return result

    def serialize(self, _data=None):
        result = None
        if(self.filename is not None):
            if not access(self.filename, F_OK):
                _logger.error("no find file: %s, created new file!" % self.filename)
                with open(self.filename, "w+"):
                    pass
            if access(self.filename, W_OK):
                with open(self.filename, "w") as _file:
                    if(_data is None):
                        _logger.error("not find data!")
                        result = None
                    else:
                        result = dump(
                            _data,
                            Dumper=Dumper,
                            allow_unicode=True,
                            canonical=True,
                            indent=4,
                            explicit_start=True,
                            explicit_end=True
                        )
                        _file.write(result)
            else:
                _logger.error("no access writing to file: %s" % self.filename)
                result = None
        else:
            if(_data is not None):
                result = dump(
                    _data,
                    Dumper=Dumper,
                    allow_unicode=True,
                    canonical=True,
                    indent=4,
                    explicit_start=True,
                    explicit_end=True
                )
            else:
                _logger.error("not find serialize data!")
                result = None
        return result
