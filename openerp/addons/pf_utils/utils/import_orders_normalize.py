# -*- coding: utf-8 -*-
from os import access, R_OK  # , W_OK
from os.path import exists, isfile, dirname, basename
from chardet import detect as detect_encoding
import unicodedata
from binary_files_from_local_folder import get_starting_chunk, is_binary_string
from csv_utils import detect_dialect
from backuplocalfileutils import file_for_writing


class NormalizeDataImportOrder(Exception):
    def __init__(self, message):
        super(NormalizeDataImportOrder, self).__init__()
        self.message = message


def data_to_ascii(data=None, from_file=None):
    result = None
    input_data = False
    if(data):
        input_data = data
    else:
        if(from_file):
            with open(from_file, 'r+') as f:
                input_data = f.read()
    try:
        if(not input_data):
            raise NormalizeDataImportOrder('Not find data')
        input_data = input_data.replace('\0', '')
        _encoding = detect_encoding(input_data)
        encoding = False
        if(_encoding['encoding'] == 'ascii'):
            result = input_data
        elif(_encoding['encoding'] is None):
            result = False
        else:
            encoding = _encoding['encoding']
        if(encoding):
            encoded_data = input_data.decode(encoding)
            ascii_data = unicodedata.normalize('NFKD', encoded_data).encode('ascii', 'ignore')
            result = ascii_data
    except UnicodeDecodeError:
        result = False
    finally:
        return result


def fix_line_terminators(data):
    import string
    data = string.replace(data, '\r\n', '\n')
    data = string.replace(data, '\r', '\n')
    return data


def _get_header_and_data(csv_file_data):
    all_data = fix_line_terminators(csv_file_data).split('\n')
    all_data = [x.strip() for x in all_data if x]
    if(not isinstance(all_data, list)):
        raise NormalizeDataImportOrder("Couldn't split up header_csv and main_csv")
    if(not len(all_data) >= 4):
        raise NormalizeDataImportOrder("A small numbers of rows in csv")
    header_csv = '{csv_data}\r\n'.format(csv_data="\r\n".join(all_data[:2]))
    main_csv = '{csv_data}\r\n'.format(csv_data="\r\n".join(all_data[2:]))
    return (header_csv, main_csv)


def _get_lines_from_csv(_data):
    from cStringIO import StringIO as IO
    from csv import DictReader
    lines = []
    csv_dialect = detect_dialect(_data)
    if(not csv_dialect):
        raise NormalizeDataImportOrder(
            "Couldn't detect dialect for\n===CSV===\n->\n{csv_data}\n<-".format(csv_data=_data)
        )
    if(csv_dialect.delimiter == 'u'):
        csv_dialect.delimiter = ','
    csv_dialect.doublequote = True
    _pseudo_file = IO(_data)
    dr = DictReader(_pseudo_file, dialect=csv_dialect)
    fieldnames = dr.fieldnames
    # next(dr)
    for row in dr:
        current_row = []
        for field in fieldnames:
            if field == 'DelmarID':
                current_row.append({field: row[field].strip("[]{}()")})
            else:
                current_row.append({field: row[field]})
        lines.append(current_row)
    return lines


def normalize_lines(_lines, replace_double_quote_to=''):
    result = []
    for _line in _lines:
        line = []
        for _dict in _line:
            for key in _dict:
                normalize_value = str(_dict[key]).replace('"', replace_double_quote_to)
                line.append({key: normalize_value})
        result.append(line)
    return result


def normalize_header_and_main_lines(header_lines, main_lines):
    len_header_row = len(header_lines[0])
    len_main_row = len(main_lines[0])
    if(len_main_row < len_header_row):
        raise NormalizeDataImportOrder('header row can not be higher than the main row')
    if(len_header_row < len_main_row):
        for header_row in header_lines:
            while(len(header_row) < len_main_row):
                header_row.append({'': ''})
    return(normalize_lines(header_lines), normalize_lines(main_lines))


def generate_csv_data_from_lines(lines):
    from cStringIO import StringIO as IO
    from csv import writer, QUOTE_ALL
    pseudo_file = IO()
    fieldnames = []
    for _dict in lines[0]:
        for key in _dict:
            fieldnames.append(key)
    dw = writer(pseudo_file, delimiter=',', quotechar='"', quoting=QUOTE_ALL, lineterminator='\r\n')
    dw.writerow(fieldnames)
    for line in lines:
        row = []
        for _dict in line:
            for key in _dict:
                row.append(_dict[key])
        dw.writerow(row)
    return pseudo_file.getvalue()


def generate_file_data(header_lines, main_lines):
    header_data = generate_csv_data_from_lines(header_lines)
    main_data = generate_csv_data_from_lines(main_lines)
    result = "{header_data}{main_data}".format(header_data=header_data, main_data=main_data)
    return result


def import_orders_normalize(PATH=None, data=None, rewriting_file=True, raise_exceptions=False):
    result = {
        'normalize_ok': False,
        'csv_data': '',
    }
    try:
        if(PATH):
            chunk = get_starting_chunk(filename=PATH)
        elif(data):
            chunk = get_starting_chunk(data=data)
        else:
            raise NormalizeDataImportOrder("Not found PATH or data!")
        if(is_binary_string(chunk)):
            raise NormalizeDataImportOrder("File {PATH} is a non text formatted file".format(PATH=PATH))
        if(PATH):
            ascii_data = data_to_ascii(from_file=PATH)
        elif(data):
            ascii_data = data_to_ascii(data=data)
        if(not ascii_data):
            raise NormalizeDataImportOrder("Couldn't convert to ASCII")
        (header_csv, main_csv) = _get_header_and_data(ascii_data)
        header_lines = _get_lines_from_csv(header_csv)
        main_lines = _get_lines_from_csv(main_csv)
        (header_lines, main_lines) = normalize_header_and_main_lines(header_lines, main_lines)
        new_file_data = generate_file_data(header_lines, main_lines)
        if(rewriting_file and PATH):
            _dirname = dirname(PATH)
            _basename = basename(PATH)
            file_for_writing(_dirname, _basename, str(new_file_data), 'wb').create()
        result.update({
            'normalize_ok': True,
            'csv_data': new_file_data,
        })

    except NormalizeDataImportOrder as ndio_ex:
        result.update({
            'normalize_ok': False,
            'csv_data': '',
        })
        if(raise_exceptions):
            raise
        else:
            print('NormalizeDataImportOrder: {ex_message}.'.format(ex_message=ndio_ex.message))
    except Exception as ex:
        result.update({
            'normalize_ok': False,
            'csv_data': '',
        })
        if(raise_exceptions):
            raise
        else:
            print('UnknownException: {ex_message}.'.format(ex_message=ex.message))
    finally:
        return result


def normalize_csv(PATH=None, data=None, rewriting_file=True, raise_exceptions=False):
    result = {
        'normalize_ok': False,
        'csv_data': '',
        'lines': [],
    }
    try:
        if(PATH):
            chunk = get_starting_chunk(filename=PATH)
        elif(data):
            chunk = get_starting_chunk(data=data)
        else:
            raise NormalizeDataImportOrder("Not found PATH or data!")
        if(is_binary_string(chunk)):
            raise NormalizeDataImportOrder("File {PATH} is a non text formatted file".format(PATH=PATH))
        if(PATH):
            ascii_data = data_to_ascii(from_file=PATH)
        elif(data):
            ascii_data = data_to_ascii(data=data)
        if(not ascii_data):
            raise NormalizeDataImportOrder("Couldn't convert to ASCII")
        lines = _get_lines_from_csv(ascii_data)
        lines = normalize_lines(lines, replace_double_quote_to='\'\'')
        new_file_data = generate_csv_data_from_lines(lines)
        if(rewriting_file and PATH):
            _dirname = dirname(PATH)
            _basename = basename(PATH)
            file_for_writing(_dirname, _basename, str(new_file_data), 'wb').create()
        result.update({
            'normalize_ok': True,
            'csv_data': new_file_data,
            'lines': _get_lines_from_csv(new_file_data)
        })

    except NormalizeDataImportOrder as ndio_ex:
        result.update({
            'normalize_ok': False,
            'csv_data': '',
            'lines': [],
        })
        if(raise_exceptions):
            raise
        else:
            print('NormalizeDataImportOrder: {ex_message}.'.format(ex_message=ndio_ex.message))
    except Exception as ex:
        result.update({
            'normalize_ok': False,
            'csv_data': '',
            'lines': [],
        })
        if(raise_exceptions):
            raise
        else:
            print('UnknownException: {ex_message}.'.format(ex_message=ex.message))
    finally:
        return result

if(__name__ == '__main__'):
    import sys
    import pprint
    PATH = sys.argv[1]
    if(exists(PATH)):
        if(isfile(PATH)):
            if(access(PATH, R_OK)):
                rewriting_file = False
                try:
                    if(sys.argv[2] == 'rewriting_file'):
                        rewriting_file = True
                except IndexError:
                    pass
                result = import_orders_normalize(sys.argv[1], rewriting_file=rewriting_file)
                print('<==========RESULT\n')
                pprint.pprint(result)
                print('\nRESULT==========>')
            else:
                print('<==========RESULT\n')
                print("Couldn't access reading file: '{PATH}'".format(PATH=PATH))
                print('\nRESULT==========>')
        else:
            print('<==========RESULT\n')
            print('PATH: "{PATH}" NOT A FILE'.format(PATH=PATH))
            print('\nRESULT==========>')
    else:
        print('<==========RESULT\n')
        print('PATH: {PATH} NOT EXISTS!'.format(PATH=PATH))
        print('\nRESULT==========>')
