def _get_one(data):
    res = data
    if isinstance(data, list) and data:
        res = data[0]

    return res


def get_image_url(id_delmar):
    return "http://partner.delmarintl.ca/Catalog/{id_delmar}.jpg".format(id_delmar=(id_delmar or '').replace('/', '~'))
