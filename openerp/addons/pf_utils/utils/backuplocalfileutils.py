# -*- coding: utf-8 -*-
from os import makedirs, chmod, access, F_OK, W_OK, listdir
from os.path import join, exists, isdir, isfile
import logging
import errno

_logger = logging.getLogger(__name__)


def get_list_files(root_path):
    ListFiles = []
    try:
        filenames = listdir(root_path)
    except OSError as os_err:
        if os_err.errno == errno.ENOENT:
            filenames = []
    for filename in filenames:
        path = join(root_path, filename)
        if not isdir(path):
            ListFiles.append(path)
        else:
            ListFiles += get_list_files(path)
    return ListFiles


def is_grant_access(_path):
    if not access(_path, W_OK):
        _logger.warning("Not rules for write to file/directory: %s" % _path)
        chmod(_path, 0777)
    if access(_path, W_OK):
        _logger.info("Write permissions to the file/directory: %s provided" % _path)
    else:
        _logger.warning("Write permissions to the file/directory: %s not provided" % _path)


class file_for_writing(object):
    _filename = ""
    _path = ""
    _paramOpen = ""
    _data = ""
    _fullname = ""

    def __init__(self, path, filename, data, paramOpen):
        self._filename = filename
        self._path = path
        self._data = data
        self._paramOpen = paramOpen
        self._fullname = join(self._path, self._filename)

    def create(self):
        if not exists(self._path):
            _logger.info("Path: %s not exists, create dir." % self._path)
            makedirs(self._path)
            chmod(self._path, 0777)

        if access(self._fullname, F_OK):
            _logger.info("File: %s exists! rewrite this file" % self._fullname)
            is_grant_access(self._fullname)

        if isdir(self._path):
            _logger.info("Path: %s exists." % self._path)
            is_grant_access(self._path)
            with open(self._fullname, self._paramOpen) as f:
                try:
                    f.write(self._data)
                except Exception, e:
                    _logger.error(
                        """
                        Error write file: %s
                        message: %s""" % self._fullname, e.message)
            if isfile(self._fullname):
                return True
            else:
                return False
        else:
            _logger.error("%s not a directory!" % self._path)
            return False
