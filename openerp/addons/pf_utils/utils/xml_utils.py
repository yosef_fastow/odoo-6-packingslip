import re
from lxml import objectify
from lxml.etree import tostring, parse, XMLParser, XMLSyntaxError
from StringIO import StringIO


def delete_namespaces(element=None, xml=None, step=None):
    """
        1) If step is "first":
    required: element(<type 'lxml.etree._ElementTree'>), step
    returned: element with a remove namespaces(<type 'lxml.etree._ElementTree'>)
    or None
        2) if step is "second":
    required: xml(<type 'str'>), step
    returned: string with a remove namespaces(like xmlns:xsi="...")
    or empty string
        3) else return None
    """
    if step == "first":
        if element:
            try:
                root = element.getroot()
                for elem in root.getiterator():
                    try:
                        i = elem.tag.find('}')
                    except AttributeError:
                        continue
                    if i >= 0:
                        elem.tag = elem.tag[i+1:]
                objectify.deannotate(root, cleanup_namespaces=True)
                result = element
            except Exception:
                print "Can't remove namespaces(1st step)"
                result = None
            finally:
                return result
        else:
            return None

    elif step == "second":
        if xml:
            result = re.sub(''' xmlns:xsi=".*"''', "", xml)
            result = re.sub(''' xmlns=".*"''', "", result)
            return result
        else:
            return u""
    elif step == "replace_version":
        if xml:
            result = re.sub('''version="2.0"''', '''version="1.0"''', xml)
            return result
        else:
            return None
    else:
        return None


def clean_namespaces(xml):
    new_xml = None
    parser = XMLParser(remove_blank_text=True)
    tree = None
    try:
        tree = parse(StringIO(xml), parser)
    except XMLSyntaxError:
        xml = delete_namespaces(xml=xml, step="replace_version")
        tree = parse(StringIO(xml), parser)
    if tree:
        tree = delete_namespaces(element=tree, step="first")
        if tree:
            new_xml = tostring(tree, pretty_print=True)
            new_xml = delete_namespaces(xml=new_xml, step="second")
    return new_xml


def _try_parse(raw_value, _type, default=-100500):
    import re
    result = False
    correct_on_start = False
    try:
        correct_on_start = eval("isinstance(locals()['raw_value'], {type})".format(type=_type), {'raw_value': raw_value})
    except (NameError, SyntaxError, TypeError):
        pass
    if (correct_on_start):
        result = raw_value
    else:
        raw_number = re.sub(r'([^\d\.])', '', str(raw_value))
        try:
            if raw_number == '' and _type in ('int', 'float'):
                raise IndexError

            if (_type == 'int'):
                result = int(float(raw_number))
            elif (_type == 'float'):
                result = float(raw_number)
            else:
                if (default != -100500):
                    result = default
        except IndexError:
            if (default != -100500):
                result = default
            else:
                result = raw_value
    return result
