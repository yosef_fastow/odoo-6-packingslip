# -*- coding: utf-8 -*-


def str_encode(string):
    try:
        string = string and isinstance(string, unicode) and string.encode('utf-8', 'ignore') or str(string).encode('utf-8')
    except:
        string = ''

    return string
