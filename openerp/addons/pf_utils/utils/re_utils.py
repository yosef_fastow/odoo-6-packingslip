# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from itertools import permutations
from dateutil.parser import parse as parse_datetime
import re


def str2float(val, default=None):

    if isinstance(val, (str, unicode)):
        val = re.sub(r'[^\d\.]', '', val)

    if val:
        return float(val)

    if default is not None:
        return default

    raise ValueError('Could not convert `%s` to float' % val)


def pretty_float(val, format="%0.2f"):
    val = str2float(val)
    return format % val


def size_without_decimals(string):
    if not string:
        return string

    return re.sub(r'\.0+$', '', string)


def raw_string(s):
    if isinstance(s, str):
        s = s.encode('string-escape')
    elif isinstance(s, unicode):
        s = s.encode('unicode-escape')
    return s


def clean_code(dirty):
    return re.sub('[^a-zA-Z0-9-\.]', '', dirty)


def f_d(_format, date_time=None):
    if (date_time is None):
        date_time = datetime.now()
    _format = "{{:{0}}}".format(_format)
    return _format.format(date_time)


def date_range(date_start, date_end, input_fmt='%Y-%m-%d'):
    if (isinstance(date_start, (str, unicode))):
        date_start = datetime.strptime(date_start, input_fmt).date()
    if (isinstance(date_end, (str, unicode))):
        date_end = datetime.strptime(date_end, input_fmt).date()
    while date_start <= date_end:
        yield date_start
        date_start = date_start + timedelta(days=1)


def date_range_string(date_start, date_end, input_fmt='%Y-%m-%d', output_fmt='%Y-%m-%d'):
    return [f_d(output_fmt, x) for x in date_range(date_start, date_end, input_fmt=input_fmt)]


"""
A function for flexible datetime parsing.
"""


def str2date(string):
    "Parse a string into a datetime object."

    for fmt in dateformats():
        try:
            return datetime.strptime(string, fmt)
        except ValueError:
            pass

    try:
        return parse_datetime(string)
    except ValueError:
        pass

    raise ValueError("'%s' is not a recognized date/time" % string)


def dateformats():
    "Yield all combinations of valid date formats."

    years = ("%Y",)
    months = ("%b", "%B")
    days = ("%d",)
    times = ("%I%p", "%I:%M%p", "%H:%M", "")

    for year in years:
        for month in months:
            for day in days:
                for args in ((day, month), (month, day)):
                    date = " ".join(args)
                    for time in times:
                        for combo in permutations([year, date, time]):
                            yield " ".join(combo).strip()

if __name__ == "__main__":
    tests = """
    10 May 2003 4pm
    3 feb 2011 23:32
    17 aug 1962
    Aug 22 1943 19:22
    14:55 aug 15 1976
    21 march 2005 6am
    2002 apr 10 5:32am
    14 Octember 2023
    2015-07-27T18:50:08Z
    2016-02-29 00:00:00
    """

    for string in tests.strip().split("\n"):
        s = string.strip()
        print "%-20s" % s,
        try:
            print str2date(s)
        except ValueError:
            print "invalid date/time"
