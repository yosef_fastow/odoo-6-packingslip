# -*- coding: utf-8 -*-


SAFE_LIST = ['or', 'and', 'if', 'else', 'elif',
    'acos', 'acosh', 'asin', 'asinh', 'atan',
    'atan2', 'atanh', 'ceil', 'copysign', 'cos',
    'cosh', 'degrees', 'e', 'exp', 'fabs',
    'factorial', 'floor', 'fmod', 'frexp', 'fsum',
    'hypot', 'isinf', 'isnan', 'ldexp', 'log',
    'log10', 'log1p', 'modf', 'pi', 'pow',
    'radians', 'sin', 'sinh', 'sqrt', 'tan',
    'tanh', 'trunc', 'ascii_letters', 'ascii_lowercase', 'ascii_uppercase',
    'atof', 'atof_error', 'atoi', 'atoi_error', 'atol',
    'atol_error', 'capitalize', 'capwords', 'center', 'count',
    'digits', 'expandtabs', 'find', 'hexdigits', 'index',
    'index_error', 'join', 'joinfields', 'letters', 'ljust',
    'lower', 'lowercase', 'lstrip', 'maketrans', 'octdigits',
    'printable', 'punctuation', 'replace', 'rfind', 'rindex',
    'rjust', 'rsplit', 'rstrip', 'sorted', 'split',
    'splitfields', 'strip', 'swapcase', 'translate', 'upper',
    'uppercase', 'whitespace', 'zfill', 'not', 'is'
]

BUILTIN_CONSTANTS_LIST = ['True', 'False', 'None', 'NotImplemented', 'Ellipsis']

SAFE_DICT = dict([(k, globals().get(k, None)) for k in SAFE_LIST])