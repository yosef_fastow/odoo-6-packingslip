# -*- coding: utf-8 -*-
from unidecode import unidecode
import unicodedata
from functools import wraps, partial
import time
from funcsigs import signature
from funcsigs import _empty as empty_default_param
import inspect
import new
from json import dumps as json_dumps


ENCODE_NORMALIZE_OPTIONS = ('NFC', 'NFKC', 'NFD', 'NFKD')
ENCODE_NORMALIZE = ENCODE_NORMALIZE_OPTIONS[3]
ENCODE_TARGET_TYPE_OPTIONS = ('ascii',)
ENCODE_TARGET_TYPE = ENCODE_TARGET_TYPE_OPTIONS[0]
ENCODE_ACTION_OPTIONS = ('ignore',)
ENCODE_ACTION = ENCODE_ACTION_OPTIONS[0]


def unicode_to_ascii_old(source_text):
    converted_text = unicodedata.normalize(ENCODE_NORMALIZE, source_text).\
        encode(ENCODE_TARGET_TYPE, ENCODE_ACTION)
    return converted_text


# For use: from openerp.addons.pf_utils.utils.decorators import return_ascii
# @return_ascii
# def function(...):
#     pass
def return_ascii(function_to_decorate):
    def wrapper(*args, **kwargs):
        _result = function_to_decorate(*args, **kwargs)
        result = _result
        if (isinstance(_result, unicode)):
            try:
                result = unidecode(_result)
            except:
                try:
                    result = unicode_to_ascii_old(_result)
                except:
                    pass
        return result
    return wrapper


def to_ascii(data):
    result = None
    try:
        result = unidecode(data)
    except:
        try:
            result = unicode_to_ascii_old(data)
        except:
            pass
    return result


def prodonly(return_value=None):
    def decorate(function_to_decorate):
        @wraps(function_to_decorate)
        def wrapper(*args, **kwargs):
            result = return_value
            self, cr, uid = args[:3]
            cur_env = self.pool.get('ir.config_parameter').get_param(cr, uid, 'system.environment')
            if (cur_env == 'production'):
                result = function_to_decorate(*args, **kwargs)

            return result
        return wrapper
    return decorate


def timethis(func):
    '''
    Decorator that reports the execution time.
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


def timethis_to_log(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        func_self = args[0]
        start = time.time()
        r = func(*args, **kwargs)
        end = time.time()
        print(end-start)
        msg = 'Countdown {0} for function \'{1}\''.format(end-start, func.__name__)
        func_self.log({
            'title': msg,
            'type': 'info',
            'message': msg,
        })
        return r
    return wrapper


class Arg(object):
    def __init__(self, start_value, method_to_fix):
        super(Arg, self).__init__()
        self.start_value = start_value
        self.method_to_fix = method_to_fix

    @property
    def availiable_methods(self):
        all_methods = inspect.getmembers(Arg, predicate=inspect.ismethod)
        return [x[0] for x in all_methods if x[0] != '__init__']

    @availiable_methods.setter
    def availiable_methods(self, value):
        raise AttributeError("You can't set this property")

    @property
    def method_to_fix(self):
        return self._method_to_fix

    @method_to_fix.setter
    def method_to_fix(self, value):
        if (value in self.availiable_methods):
            self._method_to_fix = value
        else:
            raise AttributeError("Not availiable method: {0}".format(value))

    @property
    def fixed(self):
        return eval('self.{method}()'.format(method=self.method_to_fix))

    @fixed.setter
    def fixed(self, value):
        raise AttributeError("You can't set this property")

    def first_int_of_list(self):
        result = None
        if (isinstance(self.start_value, list)):
            if (self.start_value):
                result = self.start_value[0]
            else:
                result = None
        else:
            return self.start_value
        return result

    def list_of_ints(self):
        result = None
        if (isinstance(self.start_value, (int, long))):
            result = [self.start_value]
        elif (isinstance(self.start_value, list)):
            result = self.start_value
        else:
            pass
        return result

    def feploel(self):
        '''Return first element of list or empty list if not isinstance list'''
        result = []
        if (isinstance(self.start_value, list) and self.start_value):
            result = self.start_value[0]
        return result

    def loe(self):
        '''Return list of element'''
        result = self.start_value
        if (not isinstance(self.start_value, list)):
            result = [self.start_value]
        return result


def fix_arg(value, method):
    arg = Arg(value, method)
    return arg.fixed


def if_default(func=None, fill_params=None, fix_args=None):
    if (func is None):
        return partial(if_default, fill_params=fill_params, fix_args=fix_args)
    func.__fill_default_params__ = fill_params
    func.__fix_args__ = fix_args if fix_args else {}

    @wraps(func)
    def wrapper(*args, **kwargs):
        _func = func
        list_args = list(args)
        dict_kwargs = kwargs
        sig = signature(func)
        sig_params = sig.parameters.values()
        default_values = (-100500 if sig_param.default == empty_default_param else sig_param.default for sig_param in sig_params)
        varnames = func.__code__.co_varnames
        func_lines = inspect.getsourcelines(func)[0]
        if ('pdb.set_trace()' in inspect.getsource(func)):
            debug = True
        else:
            debug = False
        definition_line = None
        next_after_definition = None
        fixed_indent_func_lines = []
        for line in func_lines:
            if (line.strip().startswith('def ')):
                definition_line = line
                break
        indent_of_def = len(definition_line) - len(definition_line.lstrip())
        for line in func_lines:
            fixed_indent_func_lines.append(line[indent_of_def:])
        for i, line in enumerate(fixed_indent_func_lines):
            if (line.strip().startswith('@if_default(')):
                if_default_dec_line = line
            if (line.strip().startswith('def ')):
                definition_line = line
                next_after_definition = fixed_indent_func_lines[i + 1]
                break
        indent = ' ' * (len(next_after_definition) - len(next_after_definition.lstrip()))
        fill_func_lines = []
        name_with_index = {i: name for i, name in enumerate(varnames)}
        for i, default in enumerate(default_values):
            if (default != -100500):
                if (func.__fill_default_params__.get(name_with_index[i], -100500) != -100500):
                    new_default = func.__fill_default_params__[name_with_index[i]]
                    if (name_with_index[i] in dict_kwargs):
                        fill_func_lines.append(
                            '{indent}if ({param_name} == {default}):\n'.format(
                                indent=indent,
                                param_name=name_with_index[i],
                                default=default
                            )
                        )
                        fill_func_lines.append(
                            '{indent}{indent}{param_name} = {new_default}\n'.format(
                                indent=indent,
                                param_name=name_with_index[i],
                                new_default=new_default
                            )
                        )
                    else:
                        try:
                            real_param = list_args[i]
                            if (real_param == default):
                                fill_func_lines.append(
                                    '{indent}if ({param_name} == {default}):\n'.format(
                                        indent=indent,
                                        param_name=name_with_index[i],
                                        default=default
                                    )
                                )
                                fill_func_lines.append(
                                    '{indent}{indent}{param_name} = {new_default}\n'.format(
                                        indent=indent,
                                        param_name=name_with_index[i],
                                        new_default=new_default
                                    )
                                )
                        except IndexError:
                            fill_func_lines.append(
                                '{indent}if ({param_name} == {default}):\n'.format(
                                    indent=indent,
                                    param_name=name_with_index[i],
                                    default=default
                                )
                            )
                            fill_func_lines.append(
                                '{indent}{indent}{param_name} = {new_default}\n'.format(
                                    indent=indent,
                                    param_name=name_with_index[i],
                                    new_default=new_default
                                )
                            )
        func_name = func.__name__
        new_func_name = "new_{0}".format(func.__name__)
        new_func_lines = []
        index_with_name = {name: i for i, name in enumerate(varnames)}
        for name_arg_to_fix in func.__fix_args__:
            method = func.__fix_args__[name_arg_to_fix]
            index = index_with_name[name_arg_to_fix]
            now_value = list_args[index]
            list_args[index] = fix_arg(now_value, method)
        for line in fixed_indent_func_lines:
            if (line == if_default_dec_line):
                continue
            if (line == definition_line):
                new_func_lines.append(line.replace(func_name, new_func_name))
                for new_line in fill_func_lines:
                    new_func_lines.append(new_line)
                # ONLY PRINTED FOR DEBUG
                if (debug):
                    new_func_lines.append('{indent}print("==== DEBUG INFO ====")\n'.format(indent=indent))
                    new_func_lines.append('{indent}print("original_function: {func_name}")\n'.format(indent=indent, func_name=func_name))
                    new_func_lines.append('{indent}print("replaced to function: {new_func_name}")\n'.format(indent=indent, new_func_name=new_func_name))
                    new_func_lines.append('{indent}import inspect\n'.format(indent=indent))
                    new_func_lines.append('{indent}frame = inspect.currentframe()\n'.format(indent=indent))
                    new_func_lines.append('{indent}recent_args, _, _, recent_values = inspect.getargvalues(frame)\n'.format(indent=indent))
                    new_func_lines.append('{indent}print("==== RECENT ARGS ===")\n'.format(indent=indent))
                    new_func_lines.append('{indent}for i in recent_args:\n'.format(indent=indent))
                    new_func_lines.append('{indent}    print "%s = %s" % (i, recent_values[i])\n'.format(indent=indent))
                    new_func_lines.append('{indent}print("====================")\n'.format(indent=indent))
                continue
            new_func_lines.append(line)
        new_code_func = ''.join(new_func_lines)
        if (debug):
            print(new_code_func)
        exec new_code_func in locals(), _func.func_globals
        rewrittenfunc = new.function(
            eval("_func.func_globals['{new_func_name}'].func_code".format(new_func_name=new_func_name)),
            _func.func_globals,
            eval("_func.func_globals['{new_func_name}'].func_name".format(new_func_name=new_func_name)),
            eval("_func.func_globals['{new_func_name}'].func_defaults".format(new_func_name=new_func_name)),
        )
        return rewrittenfunc(*list_args, **kwargs)
    return wrapper


def return_json(function_to_decorate):
    def wrapper(*args, **kwargs):
        result = function_to_decorate(*args, **kwargs)
        result = json_dumps(result)
        return result
    return wrapper


@return_ascii
def test_dec_unicode():
    test_str = \
        u"""
        ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾
        ¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛ
        ÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷ø
        ùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕ
        ĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲ
        ĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏ
        ŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬ
        ŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ
        """
    print(test_str)
    return test_str


@if_default(fill_params={'ids': [], 'context': {}, 'ololo': 'string'})
def test_fill_fill_args(self, cr, uid, ids=None, context=None):
    print(self, cr, uid, ids, context)
    return True


@if_default(fill_params={'working_list': []})
def fixed_my_func(working_list=None):
    working_list.append("a")
    print working_list


def not_fixed_my_func(working_list=[]):
    working_list.append("a")
    print working_list


@if_default(fill_params={'context': {}}, fix_args={'ids': 'list_of_ints'})
def test(self, cr, uid, ids, context=None):
    return True


if (__name__ == '__main__'):
    print(test_dec_unicode())
    print(test_fill_fill_args('self', 'cr', 'uid', [2]))
    not_fixed_my_func()
    not_fixed_my_func()
    not_fixed_my_func()
    test('self', 'cr', 1, 5)
    fixed_my_func()
    fixed_my_func()
    print('to_ascii', to_ascii('FRANCIS MERIÃƒÃ‚O DE DANIELE'))


def profile_method(sortby='cumulative'):
    def profile_wrapper(function_to_decorate):
        def wrapper(*args, **kwargs):
            import cProfile
            import pstats
            import StringIO

            pr = cProfile.Profile()
            pr.enable()

            result = function_to_decorate(*args, **kwargs)

            pr.disable()
            s = StringIO.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
            ps.print_stats()
            print "Profiling:"
            print s.getvalue()

            return result
        return wrapper
    return profile_wrapper


@profile_method()
def test_profile_method():
    a = [8, 3, 9, 15, 29, 7, 10]
    for j in range(1, len(a)):
        key = a[j]
        i = j - 1
        while (i >= 0) and (a[i] > key):
            a[i+1] = a[i]
            i = i - 1
            a[i+1] = key

    return a


def try_or_log_exception(logger, exception_class=Exception):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exception_class as e:
                logger.error(e)

        return wrapper
    return decorator
