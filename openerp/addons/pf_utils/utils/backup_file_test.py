# -*- coding: utf-8 -*-
from backup_file import backup_file_open

with backup_file_open('/orders/temp/test_file', 'w') as bf:
    bf.write('test data')
