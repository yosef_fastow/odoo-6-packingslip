import os
import shutil
import time
from subprocess import Popen

FTP_ROOT = '/tmp/ftp'


class TestFtp():

    def __init__(self, *argv):
        shutil.rmtree(FTP_ROOT, True)
        os.mkdir(FTP_ROOT)
        for filename in argv:
            shutil.copy2(filename, FTP_ROOT)

    def __enter__(self):
        self.ftp = Popen(
            ['python2', '-m', 'pyftpdlib', '-w', '-d', FTP_ROOT],
            stdout=open(os.devnull, 'w'),
        )
        time.sleep(3)

    def __exit__(self, type, value, traceback):
        self.ftp.terminate()
        shutil.rmtree(FTP_ROOT, True)
