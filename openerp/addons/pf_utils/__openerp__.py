# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP Module
#
#    Copyright (C) 1996-2012 Pokémon.
#    Author Yuri Ivanenko
#
##############################################################################

{
    "name": "Progforce Utils",
    "version": "0.1",
    "author": "Yuri Ivanenko @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "base_tools",
        "web",
        "ace_editor"
    ],
    "description": """
    Module contains .js libs, extended widgets and other utils for OpenERP


    Widget's extensions:

    * One2Many / Many2Many

        Options:
            - popup:
                deny - (bool) will deny popup on click on line

            - addable - (bool)
            - isClarkGable - (bool) show/hide pen line's button
            - deletable - (bool)
            - editable - (bool)
            - on_button_action - (varchar) ['remove']
                Default behavior - reload full parent after line button action
                remove = remove line, do not reload form

            usage -> attrs="{'options': {'addable': False, 'isClarkGable': False, 'deletable': False, 'on_button_action': 'remove', 'popup': {'deny': True}}}"


    * Many2One

        Widgets:
            - many2one_search_only
                Deny quick create.
                Hide buttons create and Open.
            - many2one_always_edit

    * Datetime

        Widgets:
            - time


    * BinaryFile

        Widgets:
            - field_binary_without_saveas
            - file_with_ext_and_size


    * Selection:

        Widgets:
            - selection_always_edit


    * Boolean

        Widgets:
            - boolean_always_edit
            - bool_btn


    * Char

        Widgets:
            - char_always_edit


    * Action Window

        Flags:
            action_buttons - (bool) show/hide form buttons,
            deletable - (bool) show/hide "delete" buttons,
            pager - (bool) show/hide pager,
            views_switcher - (bool) show/hide page switcher (form. list, ...),
            can_be_discarded - (bool) do not show "dirty" message

            usage ->
                <field name="context">{'flags': {'action_buttons': False, 'deletable': False, 'pager': False, 'views_switcher': False, 'can_be_discarded': True}, 'target': 'new'}</field>

    * OnChange

        Standard keys: value, domain, warning.
        Added keys:
            - notification - show notification based on response



    """,
    "category": "Tools",
    "js": [
        'static/src/js/lib/*.js',
        'static/src/js/*.js'
    ],
    "css": [
        'static/src/css/*.css'
    ],
    "qweb": [
        'static/src/xml/*.xml'
    ],
    'update_xml': [
        'ir/view/ir_cron_view.xml',
        'ir/view/ir_notification_view.xml',
        'ir/view/ir_mail_server_filter_recipient_view.xml',

        'data/cron_data.xml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
