{
    "name": "delmar_cafer_management",
    "version": "1.0.2",
    "author": "Evgeniy Zaytsev @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "delmar_iframe",
        "fetchdb",
        "account",
        "delmar_sale",
    ],
    "description": """

    """,
    "category": "Tools",
    "init_xml": [
        'data/system_params.xml',
    ],
    "demo_xml": [],
    "update_xml": [
        'security/security_cafer_management.xml',
        'security/ir.model.access.csv',
        'delmar_cafer_management_view.xml',
        'res/delmar_cafer_management_log_view.xml'
    ],
    "active": False,
    "application": True,
    "installable": True,
    'auto_install': False,
}
