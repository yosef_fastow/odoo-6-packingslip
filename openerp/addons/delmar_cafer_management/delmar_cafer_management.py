import logging

import pooler
from osv import fields, osv
import datetime
import hashlib
import pytz


_logger = logging.getLogger(__name__)

class delmar_cafer_management(osv.osv):
    _name = "delmar.cafer.management"
    _description = "Delmar Warehouse Management"

    _columns = {
        'name': fields.char('Name', size=256, required=True, readonly=True),
        'shipping_info_url': fields.char('Shipping Info URL', size=256, required=True, readonly=True),
        'singfield_shipping_info_url': fields.char('Singfield Shipping Info URL', size=256, required=True, readonly=True),
        'cafer_transactions_url': fields.char('CAFER Transactions URL', size=256, required=True, readonly=True),
        'cafer_receiving_list_url': fields.char('CAFER Receiving URL', size=256, required=True, readonly=True),
        'cafer_stock_count_url': fields.char('Stock Count', size=256, required=True, readonly=True),
        'cafer_duty_payment_url': fields.char('CAFER Duty Payment', size=256, required=True, readonly=True),
        'cafer_manual_receiving_url': fields.char('CAFER Manual Receiving', size=256, required=True, readonly=True),
        'showroom_count_url': fields.char('Showroom count', size=256, required=True, readonly=True),
        'showroom_manage_url': fields.char('Showroom manage', size=256, required=True, readonly=True),
        'stock_stat_url': fields.char('Stock statistic', size=256, required=True, readonly=True),
        'new_waiting_availability_url': fields.char('New Waiting Availability', size=256, required=True, readonly=True),
        'print_zpl_url': fields.char('Show/Print ZPL', size=256, required=True, readonly=True),
        'wh_stock_report_url': fields.char('Warehouse Stock Report', size=256, required=True, readonly=True),
        'invoice_reconciliation_url': fields.char('Invoice Reconciliation', size=256, required=True, readonly=True),
        'year_to_date_url': fields.char('Year To Date', size=256, required=True, readonly=True),
        'uploading_invoice_url': fields.char('Uploading Invoice URL', size=256, required=True, readonly=True),
        'uploading_singfield_url': fields.char('Uploading Singfield URL', size=256, required=True, readonly=True),
        'new_waiting_availability_url_external': fields.char('New Waiting Availability External', size=256, required=True, readonly=True),
        'product_sets_url': fields.char('Product Sets', size=256, required=True, readonly=True),
        'manual_invoice_loader_url': fields.char('Load Manual Invoice', size=256, required=True, readonly=True),
        'transactions_report_url': fields.char('Transactions', size=256, required=True, readonly=True),
        'tag_import_url': fields.char('Tag Import', size=256, required=True, readonly=True),
        'label_wizard_url': fields.char('Label Wizard', size=256, required=True, readonly=True),
        'label_wizard_upc_url': fields.char('Label Wizard UPC', size=256, required=True, readonly=True),
        'accpac_invoice_url': fields.char('AccPac Invoice', size=256, required=True, readonly=True),
    }

    # _defaults = {
    #     'shipping_info_url': 'http://localhost:8077/inventory_report/uploading',
    #     'cafer_transactions_url': 'http://localhost:8077/inventory_report',
    # }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if args:
            return super(delmar_cafer_management, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []


    def default_get(self, cr, uid, fields_list=[], context={}):
        res = super(delmar_cafer_management, self).default_get(cr, uid, fields_list, context=context)
        config_data = self.pool.get('ir.config_parameter')
        user_data = self.pool.get('res.users')

        server_names = {}
        param_ids = config_data.search(cr, uid, [('key', 'like', 'cafer.mangement.server.name')])
        if param_ids:
            params_obj = config_data.browse(cr, uid, param_ids)
            if params_obj:
                for param_obj in params_obj:
                    server_names[param_obj.key] = param_obj.value
            else:
                return False

        user_id = ''
        user_name = 'undefined'
        user_password = ''

        curr_user = user_data.read(cr, uid, uid, fields=['name', 'groups_id', 'password'], context=context)
        if curr_user:
            user_id = str(uid)
            user_name = curr_user['name']
            user_password = curr_user['password']

        now = datetime.datetime.now(pytz.timezone('America/Montreal'))

        user_id_param = 'userid=' + user_id
        user_name_param = 'username=' + user_name
        token_key = str(user_id) + str(user_password)
        user_token = 'token=' + hashlib.md5(token_key + now.strftime("%Y-%m-%d %H")).hexdigest()

        param_str = '?' + user_id_param + '&' + user_name_param + '&' + user_token

        token = hashlib.md5(str(uid) + now.strftime("%Y-%m-%d %H")).hexdigest()

        res.update({
            'shipping_info_url': server_names.get('cafer.mangement.server.name.shipping_info_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/uploading' + param_str,
            'singfield_shipping_info_url': server_names.get('cafer.mangement.server.name.singfield_shipping_info_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/uploading_singfield' + param_str,
            'cafer_transactions_url': server_names.get('cafer.mangement.server.name.cafer_transactions_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report' + param_str,
            'cafer_stock_count_url': server_names.get('cafer.mangement.server.name.cafer_stock_count_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/stock_count' + param_str,
            'cafer_receiving_list_url': server_names.get('cafer.mangement.server.name.cafer_receiving_list_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/receiving_singfield' + param_str,
            'cafer_duty_payment_url': server_names.get('cafer.mangement.server.name.cafer_duty_payment_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/auto_pay' + param_str,
            'cafer_manual_receiving_url': server_names.get('cafer.mangement.server.name.cafer_manual_receiving_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/manual_receiving' + param_str,
            'showroom_count_url': server_names.get('cafer.mangement.server.name.showroom_count_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/showroom_count' + param_str,
            'showroom_manage_url': server_names.get('cafer.mangement.server.name.showroom_manage_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/showroom_manage' + param_str,
            'stock_stat_url': server_names.get('cafer.mangement.server.name.stock_stat_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/count_manage/stock_stat' + param_str,
            'new_waiting_availability_url': server_names.get('cafer.mangement.server.name.new_waiting_availability_url', server_names.get('cafer.mangement.server.name', '')) + '/waiting-availability?token=' + token,
            'print_zpl_url': server_names.get('cafer.mangement.server.name.print_zpl_url', server_names.get('cafer.mangement.server.name', '')) + '/shipping_screen/userid={userid}/password={password}'.format(userid=user_id, password=user_password),
            'wh_stock_report_url': server_names.get('cafer.mangement.server.name.wh_stock_report_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/stock_report' + param_str,
            'invoice_reconciliation_url': server_names.get('cafer.mangement.server.name.invoice_reconciliation_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/reports/report_start_page?report_name=invoice_reconciliation&direct=true',
            'year_to_date_url': server_names.get('cafer.mangement.server.name.year_to_date_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/reports/report_start_page?report_name=sales_inventory_report&direct=true',
            'uploading_invoice_url': server_names.get('cafer.mangement.server.name.uploading_invoice_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/uploading_invoice' + param_str,
            'uploading_singfield_url': server_names.get('cafer.mangement.server.name.uploading_singfield_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/uploading_singfield' + param_str,
            'new_waiting_availability_url_external': server_names.get('cafer.mangement.server.name.new_waiting_availability_url_external', server_names.get('cafer.mangement.server.name', '')) + '/waiting-availability?token=' + token,
            'product_sets_url': server_names.get('cafer.mangement.server.name.product_sets_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/product_sets' + param_str,
            'manual_invoice_loader_url': server_names.get('cafer.mangement.server.name.manual_invoice_loader_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/load_manual_invoice' + param_str,
            'transactions_report_url': server_names.get('cafer.mangement.server.name.transactions_report_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/transactions_report' + param_str,
            'tag_import_url': server_names.get('cafer.mangement.server.name.tag_import_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/tag_import' + param_str,
            'label_wizard_url': server_names.get('cafer.mangement.server.name.tag_import_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/label_wiz/#/',
            'label_wizard_upc_url': server_names.get('cafer.mangement.server.name.tag_import_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/label_wiz_upc/#/',
            'accpac_invoice_url': server_names.get('cafer.mangement.server.name.tag_import_url', server_names.get('cafer.mangement.server.name', '')) + '/inventory_report/accpac_invoice/' + param_str + '&empty=true',
        })
        return res

    def view_init(self, cr, uid, fields, context=None):

        cr = pooler.get_db(cr.dbname).cursor()
        log_obj = pooler.get_pool(cr.dbname).get('delmar.cafer.management.log')
        log_obj.create(cr, 1, {'user_id': uid, 'create_date': datetime.datetime.now()})

delmar_cafer_management()
