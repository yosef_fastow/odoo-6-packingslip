# -*- coding: utf-8 -*-
from osv import fields, osv
import re
from osv.expression import TERM_OPERATORS
import logging
import traceback
from openerp.addons.pf_utils.utils.decorators import to_ascii

logger = logging.getLogger('res.partner')


# TODO, need to transfer all code(written by ProgForce) for res_partner from another modules to here
class res_partner(osv.osv):
    _inherit = 'res.partner'

    def _get_countries(self, cr, uid, ids, *args, **kwargs):

        res = {}
        if not ids:
            return res
        for row in self.browse(cr, uid, ids):
            lst = []
            for line in row.address:
                if line.country_id.name:
                    lst.append(to_ascii(line.country_id.name))
            lst = set(lst)
            res[row.id] = ', '.join(lst)
        return res

    def _get_size_tolerance_range(self, cr, uid, context=None):
        #todo choose data
        return [(float(x)/10, str(float(x)/10)) for x in range(0, 165, 5)]

    def _get_default_tolerance(self, cr, uid, context=None):
        tolerance_obj = self.pool.get('ring.size.tolerance')
        tolerance_id = tolerance_obj.search(cr, uid, [('name', '=', '0.5')])
        tolerance = self.pool.get('ring.size.tolerance').browse(cr, uid, tolerance_id)

        return tolerance[0].id

    _columns = {
        'short_name': fields.char('Short name', size=128, ),
        'parent_partner_id': fields.many2one(
            'res.partner',
            'Parent Customer',
        ),
        'min_qty': fields.float('PO Min. Qty'),
        'emails_for_sending_if_order_fail': fields.text(
            'Emails list for sending if order failed', ),
        'all_countries': fields.function(
            _get_countries,
            string='All countries',
            readonly=True,
            type='string',
        ),
        'auto_split_sets': fields.boolean('Auto Split Set', help='Split sets products to components'),
        'discount': fields.float('Discount (%)', digits=(5,2)),
        'order_support_user_id': fields.many2one('res.users', 'Order Support'),
        'auto_size_tolerance': fields.boolean('Auto size tolerance allowed',
                                              help="Check this to try auto allocate size tolerance for this customer"),
        'size_tolerance': fields.many2one('ring.size.tolerance', 'Tolerance', select=True,required=True),
        'min_price': fields.float('Min $'),
        'max_price': fields.float('Max $'),
        'no_resize': fields.boolean('No Resize', help="if No resize option is not set up, items with price lower then 60$ will have no resize option in any case."),
        'si_ftype_updateQTY': fields.selection(
            [
                ('csv', 'Default, CSV'),
                ('xls', 'XLS')
            ], 'UpdateQTY Export File type', required=True,
            help="What type of file would be exported for updateQTY, commercehub only"),
        'terms_of_use': fields.text("Terms of Use"),
        # DELMAR-162
        'force_feed_zero': fields.boolean('Force Zero Inventory Feed', help='Sent all inventory with zero quantity', ),
    }

    _defaults = {
        'parent_partner_id': None,
        'discount': 0.0,
        'auto_split_sets': True,
        'auto_size_tolerance': False,
        'size_tolerance': _get_default_tolerance,
        'si_ftype_updateQTY': 'csv',
        'force_feed_zero': False,
    }

res_partner()

class users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'supported_partner_ids': fields.one2many(
            'res.partner',
            'order_support_user_id',
            'Supported Customers'),
    }


def _fill_childs_for_partner_id_args(obj, cr, uid, args, partner_id_field_name='partner_id'):
    partner_obj = obj.pool.get('res.partner')
    str_args = str(args)
    logger.debug('INPUT DOMAIN: ' + str_args)
    # match_string like this:
    # ur'\[\'partner_id\',(\s|)\'(?P<operand>(=|!=|<=|<|>|>=|=?|=like|=ilike|like|not like|ilike|not ilike|in|not in|child_of))\',(\s|)(?P<value>(\d*|\[.*\]|\(.*\)|\'\w*\'))\]'
    # see example here https://regex101.com/r/fZ3jK2/1
    match_string = \
        ur'\[\'' + \
        partner_id_field_name + \
        ur'\',(\s|)\'(?P<operand>(' + \
        ur'|'.join(TERM_OPERATORS) + \
        ur'))\',(\s|)(?P<value>(\d*|\[.*\]|\(.*\)|\'\w*\'))\]'
    logger.debug('MATCH STRING: ' + match_string)
    p = re.compile(match_string)
    partner_matches = re.search(p, str_args)
    if partner_matches:
        operand = partner_matches.group('operand')
        logger.debug('INPUT OPERAND: ' + operand)
        value = partner_matches.group('value')
        logger.debug('INPUT VALUE: ' + value)
        # TODO: need support all operands!
        if operand in ['=', 'in', 'not in', 'like', 'ilike']:
            partner_ids = []
            partner_id = eval(value)
            if operand in ['like', 'ilike']:
                sub_partner_ids = partner_obj.search(
                    cr, uid,
                    [
                        ('name', operand, partner_id),
                        ('parent_partner_id', '=', False)
                    ]
                )
                if sub_partner_ids:
                    partner_id = sub_partner_ids
                    operand = 'in'
                else:
                    partner_id = False
            else:
                partner_id = eval(value)
            if partner_id:
                if operand == '=':
                    partner_id = [partner_id]
                partner_ids = partner_obj.search(
                    cr, uid,
                    [
                        (
                            'parent_partner_id',
                            'in',
                            partner_id
                        )
                    ]
                )
                partner_ids.extend(partner_id)
                operand = 'in' if operand == '=' else operand
                new_partner_domain = \
                    '[' + ', '.join(
                        [
                            '\'' + partner_id_field_name + '\'',
                            '\'' + operand + '\'',
                            '[' + ', '.join([str(x) for x in partner_ids]) + ']'
                        ]
                    ) + ']'
                logger.debug('NEW PARTNER DOMAIN: ' + new_partner_domain)
                str_args = str_args.replace(partner_matches.group(), new_partner_domain)
                logger.debug('NEW DOMAIN: ' + str_args)
                args = eval(str_args)

    return args


def fill_childs_for_partner_id_args(obj, cr, uid, args, partner_id_field_name='partner_id'):
    try:
        result = _fill_childs_for_partner_id_args(
            obj, cr, uid, args, partner_id_field_name=partner_id_field_name)
    except Exception:
        msg = 'FAILED fill_childs_for_partner_id_args:\n\n' + traceback.format_exc()
        logger.error(msg)
        result = args
    return result


class sale_order(osv.osv):
    _inherit = 'sale.order'

    def search(
            self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args = fill_childs_for_partner_id_args(self, cr, uid, args, 'partner_id')
        result = super(sale_order, self).search(
            cr, uid, args, offset, limit,
            order, context=context, count=count
        )
        return result

sale_order()


class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def search(
            self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args = fill_childs_for_partner_id_args(self, cr, uid, args, 'real_partner_id')
        result = super(stock_picking, self).search(
            cr, uid, args, offset, limit,
            order, context=context, count=count
        )
        return result

stock_picking()


class order_history(osv.osv):
    _inherit = 'order.history'

    def search(
            self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args = fill_childs_for_partner_id_args(self, cr, uid, args, 'partner_id')
        result = super(order_history, self).search(
            cr, uid, args, offset, limit,
            order, context=context, count=count
        )
        return result

order_history()


class packing_export(osv.osv):
    _inherit = 'picking.export'

    def search(
            self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args = fill_childs_for_partner_id_args(self, cr, uid, args, 'real_partner_id')
        result = super(packing_export, self).search(
            cr, uid, args, offset, limit,
            order, context=context, count=count
        )
        return result

packing_export()
