# -*- coding: utf-8 -*-
from osv import fields, osv


# TODO, need to transfer all code(written by ProgForce) for res_partner_address from another modules to here
class res_partner_address(osv.osv):
    _inherit = 'res.partner.address'

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
        'vcdid': fields.char('RC (Vcd Id)', size=128),
        'san_address': fields.char('SAN', size=128),
        'street3': fields.char('Street3', size=128),
        's_company': fields.char('Company', size=100),
        'prod_country_id': fields.many2one('res.country', 'Product Country'),
    }

    def __init__(self, pool, cr):
        """Add a new state value"""
        selection = super(res_partner_address, self)._columns['type'].selection or []
        selection += [
            ('bulk', 'Bulk'),
        ]
        super(res_partner_address, self)._columns['type'].selection = list(set(selection))
        return super(res_partner_address, self).__init__(pool, cr)

res_partner_address()
