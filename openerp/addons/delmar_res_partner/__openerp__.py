{
    "name": "Delmar res_partner",
    "version": "1.0.1",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "sale_integration",
        "delmar_sale",
    ],
    "description": """
    1. Inheritance 'res.partner' for Delmar actions
    """,
    "category": "Project Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "view/res_partner_view.xml",
        "view/sale_order_view.xml",
        "view/flash_order_view.xml",
        "view/stock_picking_view.xml",
        "view/order_history_view.xml",
        "view/picking_export_view.xml",
    ],
    'js': [],
    "application": True,
    "active": False,
    "installable": True,
}
