# Romanian translation for openobject-addons
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openobject-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openobject-addons\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2012-02-08 00:37+0000\n"
"PO-Revision-Date: 2012-02-17 09:10+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Romanian <ro@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2012-02-18 07:04+0000\n"
"X-Generator: Launchpad (build 14814)\n"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:37
msgid "Refund"
msgstr "Rambursare"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:22
msgid "Fax"
msgstr "Fax"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "Disc.(%)"
msgstr "Discount (%)"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:19
msgid "Tel"
msgstr "Tel"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "Description"
msgstr "Descriere"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:35
msgid "Supplier Invoice"
msgstr "Factură furnizor"

#. module: report_webkit_sample
#: model:ir.actions.report.xml,name:report_webkit_sample.report_webkit_html
msgid "WebKit invoice"
msgstr "Factură WebKit"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "Price"
msgstr "Preț"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:44
msgid "Invoice Date"
msgstr "Data facturii"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "Taxes"
msgstr "Taxe"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "QTY"
msgstr "Cantitate"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:25
msgid "E-mail"
msgstr "E-mail"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:64
msgid "Amount"
msgstr "Sumă"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:64
msgid "Base"
msgstr "Bază"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:33
msgid "Invoice"
msgstr "Factură"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:39
msgid "Supplier Refund"
msgstr "Restituire furnizor"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:44
msgid "Document"
msgstr "Document"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:49
msgid "Unit Price"
msgstr "Preţ unitar"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:44
msgid "Partner Ref."
msgstr "Ref. partener"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:28
msgid "VAT"
msgstr "TVA"

#. module: report_webkit_sample
#: report:addons/report_webkit_sample/report/report_webkit_html.mako:76
msgid "Total"
msgstr "Total"
