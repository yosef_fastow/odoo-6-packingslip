{
    "name": "product_catalog",
    "version": "1.0.0",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "delmar_products"
    ],
    "description": """

    """,
    "category": "Project Management",
    "demo_xml": [],
    "update_xml": [
        'view/product_catalog_view.xml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
