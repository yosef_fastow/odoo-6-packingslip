import logging
from osv import osv, fields


_logger = logging.getLogger(__name__)


class product_catalog_settings(osv.osv):
    _name = 'product.catalog.settings'

    _columns = {
        'product_id': fields.many2one('product.product', 'Product', ondelete='cascade'),
        "tag_id": fields.many2one('tagging.tags', 'Tag', required=False),
        "customer_id": fields.many2one('res.partner', 'Customer', domain="[('customer', '=', True)]"),
        "supplier_id": fields.many2one('res.partner', 'Supplier', domain="[('supplier', '=', True)]"),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', ),
        'supplier_instructions': fields.char('Supplier instructions', size=512),
        'reference_notes': fields.char('Reference notes', size=512),
        'pricing_comments': fields.char('Purchasing comments', size=512),
    }

    def create(self, cr, uid, vals, context=None):
        record_id = None
        if not vals.get('product_id') and 'tag_id' in vals:
            tag_id = vals.get('tag_id')
            products = self.pool.get('tagging.tags').browse(cr, uid, tag_id).product_ids
            for product in products:
                vals.update({'product_id': product.id})
                record_id = super(product_catalog_settings, self).create(cr, uid, vals, context=context)
        else:
            record_id = super(product_catalog_settings, self).create(cr, uid, vals, context=context)
        return record_id

    def get_order_notes(self, cr, uid, params, context=None):
        """Get notes for product order on catalog side
         params = {
            "product_id": 90115,
            "customer_refs": [
                "WMT"
            ],
            "supplier_ref": "Ch01",
            "warehouse": "CAFER"
        }"""
        _logger.info("Get order notes for catalog")

        params = params or {}

        product_id = params.get('product_id')
        customer_refs = params.get('customer_refs', [])
        supplier_ref = params.get('supplier_ref')
        warehouse = params.get('warehouse')

        # Check if any params not filled then return empty response or error
        if not all((product_id, customer_refs, supplier_ref, warehouse)): return {}

        wh_obj = self.pool.get('stock.warehouse')
        rp_obj = self.pool.get('res.partner')
        wh_id = wh_obj.search(cr, uid, [('name', '=', warehouse)])
        customer_ids = rp_obj.search(cr, uid, [('ref', 'in', customer_refs), ('customer', '=', True)])
        supplier_id = rp_obj.search(cr, uid, [('ref', '=', supplier_ref), ('supplier', '=', True)])

        # TODO: discuss & or | which rule from below to use
        row_ids = self.search(cr, uid, ['&', '&', '&',
                                        '|', ('product_id', '=', product_id), ('product_id', '=', None),
                                        '|', ('customer_id', 'in', customer_ids), ('customer_id', '=', None),
                                        '|', ('supplier_id', 'in', supplier_id), ('supplier_id', '=', None),
                                        '|', ('warehouse_id', 'in', wh_id), ('warehouse_id', '=', None),
                                        ])
        rows = self.browse(cr, uid, row_ids)

        supplier_instructions = []
        reference_notes = []
        pricing_comments = []

        for row in rows:
            supplier_instructions.append(row.supplier_instructions or '')
            reference_notes.append(row.reference_notes or '')
            pricing_comments.append(row.pricing_comments or '')

        ret = dict(supplier_instructions='; '.join(supplier_instructions),
                   reference_notes='; '.join(reference_notes),
                   pricing_comments='; '.join(pricing_comments),
                   )

        return ret

product_catalog_settings()
