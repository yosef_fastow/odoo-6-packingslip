# -*- coding: utf-8 -*-
{
    "name": "Tracking Validation",
    "version": "0.1",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        'delmar_sale',
    ],
    "description": """ Module for Validation UPS, USPS, FedEx Tracking Numbrers
    """,
    "category": "Sales Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'view/tracking_validation_view.xml',
        'view/tracking_validation_line_view.xml',
    ],
    'js': [],
    "application": True,
    "active": False,
    "installable": True,
}
