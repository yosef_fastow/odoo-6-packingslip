# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2014
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import logging
import re
from openerp.addons.pf_utils.utils.re_utils import raw_string

logger = logging.getLogger('tracking_validation')


class tracking_validation(osv.osv):
    _name = "tracking.validation"

    _columns = {
        'name': fields.char('Name Validation', size=256, required=True, ),
        'active': fields.boolean('Active'),
        'service_id': fields.many2one('res.shipping.service', 'Service', required=True),
        'validators': fields.one2many('tracking.validation.line', 'trv_id', 'Validators', ),
    }

    _defaults = {
        'active': True,
    }

tracking_validation()


class tracking_validation_line(osv.osv):
    _name = "tracking.validation.line"

    _columns = {
        'id': fields.integer('ID', readonly=True, ),
        'trv_id': fields.many2one('tracking.validation', 'Report', required=True, ),
        'len_tracking': fields.integer('Length Tracking', required=True, ),
        'expression': fields.text('Validation Expression', required=True, ),
        'test_tracking': fields.text('Test Tracking', ),
    }

    def validate(self, cr, uid, ids, tracking):
        result = {
            'match': None,
            'message': None,
        }
        if not ids:
            return result
        if isinstance(ids, (int, long)):
            ids = [ids]
        from re import error
        for validator in self.browse(cr, uid, ids):
            try:
                expr = re.compile(raw_string(validator.expression))
                match = expr.match(tracking)
                if match:
                    result.update({
                        'match': match,
                        'message': 'OK',
                    })
                    break
            except error as RE_ERROR:
                result.update({
                    'match': False,
                    'message': RE_ERROR.message,
                })

        return result

    def test_validate(self, cr, uid, ids, context=None):
        _id = ids[0]
        if(context is None):
            context = {}
        test_tracking = self.browse(cr, uid, _id).test_tracking or ''
        result = self.validate(cr, uid, _id, test_tracking)
        match = str(result['match'].group()) if result['match'] else 'None'
        message = str(result['message'])
        raise osv.except_osv(
            _('Tracking Validation'),
            _('match: {0}, result: {1}'.format(match, message))
        )


tracking_validation_line()
