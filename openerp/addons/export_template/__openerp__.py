# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP Module
#
#    Copyright (C) 1996-2012 Pokémon.
#    Author Yuri Ivanenko
#
##############################################################################
{
    "name": "Export template",
    "version": "1.0",
    "depends": [
        "base",
        "base_tools",
        "multi_customer",
        "tagging_product",
        "pf_utils",
        "delmar_sale",
    ],
    "author": "Yuri Ivanenko, progforce.com",
    "category": "Tools",
    "description": """
    Module for creating and editing export templates.
    """,
    "init_xml": [],
    'update_xml': [
        "view/export_template.xml",
        "view/export_history.xml",
    ],
    'js': [
        'static/src/js/export_template.js',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'qweb' : [
        "static/src/xml/*.xml",
    ],
}
