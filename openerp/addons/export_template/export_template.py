# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from cStringIO import StringIO
import csv
import base64
import simplejson
import datetime
import logging
import traceback
from openerp.addons.base.ir.ir_cron import str2tuple


_logger = logging.getLogger(__name__)

# fixing _csv.Error: field larger than field limit (131072)
from sys import maxsize
csv.field_size_limit(maxsize)


class export_template(osv.osv):
    _name = "export.template"

    _columns = {
        "name": fields.char("Template Name", size=256, required=True),
        "customer_id": fields.many2one('res.partner', 'Customer', domain="[('customer', '=', True)]"),
        "customer_fields_ids": fields.many2many("product.multi.customer.fields.name", "export_template_fields_name", "export_template_id", "field_name_id", string=""),
        "order_ids": fields.text("order ids"),
        "identify_field": fields.many2one('product.multi.customer.fields.name', 'Field for product identification', help="Need for import"),
        "size_field": fields.many2one('product.multi.customer.fields.name', 'Field for size identification', help="Need for import"),
        "default": fields.boolean("Default"),
    }

    def onchange_customer_fields_ids(self, cr, uid, ids, customer_fields_ids, context=None):

        order_str = False
        if customer_fields_ids:
            fields_ids_list = customer_fields_ids[0][2]
            if fields_ids_list:
                order_str = str(fields_ids_list[0])
                for fields_id in fields_ids_list[1:]:
                    order_str = order_str + ',' + str(fields_id)
        return {'value': {'order_ids': order_str}}


export_template()


class export_extract(osv.osv):

    _name = "export.extract"

    _columns = {
        "create_uid": fields.many2one('res.users', 'Creator'),
        "create_date": fields.datetime('Creation date', readonly=True),
        "since_date": fields.date('Since date', readonly=True, store=False),
        "to_date": fields.date('To date', readonly=True, store=False),
        "customer_id": fields.many2one('res.partner', 'Customer', ondelete='cascade', domain="[('customer', '=', True)]"),
        "template_id": fields.many2one('export.template', 'Template', required=True, ondelete='cascade'),
        "tag_id": fields.many2one('tagging.tags', 'Tag', required=False),
        "order_tag_id": fields.many2one('tagging.order', 'Orders Tag', required=False),
        "csv_file": fields.binary(string="CSV Export", readonly=True),
        "csv_filename": fields.char("", size=256),
        "state": fields.selection([
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('done', 'Done'),
            ('batch', 'Batch')
        ], 'State', select=True, required=True, readonly=True),
        "export_type": fields.char("Export Type", size=256),
        "preview": fields.text(),
        "expand": fields.boolean("Expand by sizes"),
        "half_size": fields.boolean("Including Half Size"),
        "from_size": fields.many2one('ring.size', 'From Size'),
        "to_size": fields.many2one('ring.size', 'To Size'),
        "base_line": fields.boolean("Base Line", help="If this is checked the export will add a row (for every ring) that no specified by size."),
        "use_option_sizes": fields.boolean("Use sizes from Option field"),
        "stats": fields.text("Export Info"),
        "status": fields.char("Export state", size=256),
        "progress": fields.float("Progress"),
        "create_by_catalog": fields.boolean("Create by catalog"),
        "generate": fields.boolean("Generate Fields", help="Uncheck it only if you are sure that all fields are generated already."),
    }

    _defaults = {
        'state': 'draft',
        'expand': False,
        'half_size': True,
        'csv_file': '',
        'export_type': 'basic',
        'preview': '',
        'status': 'Start',
        'progress': 0.0,
        'generate': False,
    }

    _order = "create_date desc"

    def onchange_option_size(self, cr, uid, ids, sizelist=None, context=None):
        _logger.warn("Selected sizelist: %s" % sizelist)
        return {'value': {}, 'domain': {}}

    def send_changes_to_catalog(self, cr, uid, tag_id):
        return True

    def search_args_since_date(self, cr, user, args, context=None):
        if not context:
            context = {}
        try:
            since_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'since_date')).next()
            since_date_index = args.index(since_date_arg)
            args[since_date_index][0] = 'create_date'
            args[since_date_index][1] = '>'
            return args
        except StopIteration:
            return args

    def search_args_to_date(self, cr, user, args, context=None):
        if not context:
            context = {}
        try:
            to_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'to_date')).next()
            to_date_index = args.index(to_date_arg)
            args[to_date_index] = ['create_date', '<=', args[to_date_index][2] + ' 24:00:00']
            return args
        except StopIteration:
            return args

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}
        args = self.search_args_since_date(cr, user, args, context=context)
        args = self.search_args_to_date(cr, user, args, context=context)
        return super(export_extract, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def default_get(self, cr, uid, fields_list=None, context=None):
        if not context:
            context = {}
        if not fields_list:
            fields_list = []
        res = super(export_extract, self).default_get(cr, uid, fields_list,
                                                      context=context)

        if context.get('export_id', False):
            fields = [
                'customer_id',
                'template_id',
                'tag_id',
                'order_tag_id',
                'expand',
                'base_line',
                'from_size',
                'to_size',
                'half_size',
                'generate',
            ]
            export = self.read(cr, uid, context.get('active_id'), fields=fields)
            if export and export['id']:
                del export['id']
            for field in fields:
                if isinstance(export.get(field), tuple):
                    export[field] = export.get(field)[0]

            res.update({'state': 'draft'})
            res.update(export)

        return res

    def to_csv_str(self, d):
        if d is None:
            return ''
        if type(d) in (float, int, bool):
            d = str(d)
        d = d.replace('\n', ' ').replace('\t', ' ')
        try:
            d = d.encode('utf-8')
        except UnicodeError:
            pass
        return d

    def _default_fields_filling(self, cr, uid, prod_dict, default_fields_ids):
        prod = self.pool.get("product.product").browse(cr, uid, prod_dict['product_id'])
        for default_fields_id in default_fields_ids:
            prod_val = getattr(prod, default_fields_id.label, "")
            if(isinstance(prod_val, osv.orm.browse_record)):
                prod_val = prod_val.name_get()[0][1]
            elif(isinstance(prod_val, osv.orm.browse_null)):
                prod_val = ""
            prod_dict['fields'][default_fields_id.id] = prod_val

    def expand_row(self, cr, uid, prod_dict, order, expand, base_line):
        fields = prod_dict['fields']
        main_row = [fields[int(f_id)] for f_id in order if int(f_id) in fields]
        prod_rows = []
        sizes = []
        if prod_dict['sizes']:
            size_obj = self.pool.get("ring.size")
            sizes = size_obj.browse(cr, uid, list(prod_dict['sizes']))
        sizes_order = {float(size.name): size.id for size in sizes}
        if (expand or base_line) and prod_dict['sizes']:
            if base_line:
                if expand:
                    sizes_order.update({0: 'None'})
                else:
                    sizes_order = {0: 'None'}
            for size_key in sorted(sizes_order.keys()):
                prod_row = []
                for main_row_el in main_row:
                    if type(main_row_el) is dict:
                        if sizes_order[size_key] in main_row_el:
                            prod_row.append(self.to_csv_str(main_row_el[sizes_order[size_key]]))
                        else:
                            prod_row.append('')
                    else:
                        prod_row.append(self.to_csv_str(main_row_el))
                prod_rows.append(prod_row)
        else:
            prod_row = []
            for main_row_el in main_row:
                if type(main_row_el) is dict:
                    to_concat = []
                    for size_key in sorted(sizes_order.keys()):
                        if sizes_order[size_key] in main_row_el:
                            to_concat.append(str(main_row_el[sizes_order[size_key]]))
                    prod_row.append(self.to_csv_str(", ".join(to_concat)))
                else:
                    prod_row.append(self.to_csv_str(main_row_el))
            prod_rows.append(prod_row)
        return prod_rows

    def get_size_range(self, cr, uid, half_size=True, from_size=False, to_size=False):
        if from_size or to_size:
            try:
                from_size_float = from_size and float(from_size.name) or 0.0
            except (ValueError, TypeError):
                from_size_float = 0.0
            try:
                to_size_float = to_size and float(to_size.name) or 9999.0
            except (ValueError, TypeError):
                to_size_float = 9999.0
            if from_size_float > to_size_float:
                from_size_float, to_size_float = to_size_float, from_size_float

            cr.execute("""SELECT id, name FROM ring_size""")
            all_sizes = cr.fetchall()
            res = []
            for size in all_sizes:
                try:
                    size_float = float(size[1])
                    size_format = (half_size and not size_float % 0.5) or (not half_size and not size_float % 1.0)
                    size_range = size_float >= from_size_float and size_float <= to_size_float
                    if size_range and size_format:
                        res.append(size[0])
                except (ValueError, TypeError):
                    continue
            return res
        else:
            return False

    def _prepare_params(self, cr, uid, params):
        # Is there a way to get this mapping from self._columns ?
        m2o_map = {
            'tag_id': 'tagging.tags',
            'order_tag_id': 'tagging.order',
            'template_id': 'export.template',
            'from_size': 'ring.size',
            'to_size': 'ring.size',
        }
        for field in m2o_map:
            if params.get(field) and isinstance(params[field], (int, long)):
                model_obj = self.pool.get(m2o_map[field])
                params[field] = model_obj.browse(cr, uid, params[field])
        return params

    def extract_export_csv(self, cr, uid, ids, params, context=None):
        params = self._prepare_params(cr, uid, params)
        template_id = params.get('template_id')
        if not template_id:
            return False
        tag_id = params.get('tag_id', False)
        order_tag_id = params.get('order_tag_id', False)
        expand = params.get('expand', False)
        base_line = params.get('base_line', False)
        half_size = params.get('half_size', True)
        from_size = params.get('from_size', False)
        to_size = params.get('to_size', False)
        generate = params.get('generate', True)

        export_obj = self.browse(cr, uid, ids[0])
        _logger.info("Export optional size: %s" % export_obj.use_option_sizes)
        order = template_id.order_ids and template_id.order_ids.split(",")
        fields_ids = [field.id for field in template_id.customer_fields_ids]
        if not order:
            order = [str(field_id) for field_id in fields_ids]
        elif set(fields_ids) != set([int(elem) for elem in order]):
            self.write(cr, uid, ids, {
                'progress': 0.0,
                'status': 'Error! Need to check and save the export template',
            })
            cr.commit()
            return "order_ids error"

        self.write(cr, uid, ids, {
            'progress': 0.0,
            'status': 'Generation fields',
        })
        cr.commit()
        result = {}
        csv_filename = template_id.name and template_id.name + '.csv' or 'export.csv'
        products = self._get_products(tag_id, order_tag_id)
        prod_ids = [prod.id for prod in products if prod.type == 'product']
        if not prod_ids or not template_id.customer_fields_ids:
            return result
        if context and context.get('preview'):
            prod_ids = prod_ids[:5]
        # Creation customer and default fields ids lists
        fields_ids = []
        default_fields_ids = []
        row_fields_obj_tmpl = []
        product_obj = self.pool.get('product.product')
        prod_model_fields = product_obj.fields_get(cr, uid)
        customer_id = export_obj.customer_id and export_obj.customer_id.id
        field_name_obj = self.pool.get('product.multi.customer.fields.name')
        for field in template_id.customer_fields_ids:
            if field.label in prod_model_fields:

                # will get value from model product_product

                default_fields_ids.append(field)
                row_fields_obj_tmpl.append(field.id)
            else:
                cust_field_id = None

                # will get value from customer field with
                # particular label and selected customer_id

                cust_field_ids = field_name_obj.search(
                    cr, uid,
                    [
                        ('label', '=', field.label),
                        ('customer_id', '=', customer_id)
                    ]
                )

                if cust_field_ids:
                    cust_field_id = cust_field_ids[0]

                # will get value from customer field with
                # particular label but with customer_id = NULL

                if not cust_field_id:
                    cust_field_ids = field_name_obj.search(
                        cr, uid,
                        [
                            ('label', '=', field.label),
                            ('customer_id', '=', False)
                        ]
                    )

                    if cust_field_ids:
                        cust_field_id = cust_field_ids[0]

                if cust_field_id:
                    fields_ids.append(cust_field_id)
                    row_fields_obj_tmpl.append(cust_field_id)

                    order_index = order.index(str(field.id))
                    order[order_index] = cust_field_id

        row_fields_tmpl = {field: None for field in row_fields_obj_tmpl}

        vals = []
        labels = []
        expanded_labels = []
        if fields_ids:
            if export_obj.use_option_sizes:
                size_range = False
            else:
                size_range = self.get_size_range(cr, uid, half_size, from_size, to_size)

            # Generation fields
            if generate:
                export_process_id = 0
                if ids:
                    export_process_id = ids[0] if isinstance(ids, (list, tuple)) else ids
                customer = template_id.customer_id or export_obj.customer_id
                fields_context = {
                    'fields_ids': fields_ids,
                    'customer_id': customer and customer.id or False,
                    'export_process_id': export_process_id,
                    'size_range': size_range,
                    'use_option_sizes': export_obj.use_option_sizes or False,
                    'base_line': base_line,
                    'expand': expand,
                }
                product_obj.read(cr, uid, prod_ids, fields=['multi_customer_fields'], context=fields_context)


            # SELECT fields values
            sql_params = {
                'prod_ids': tuple(prod_ids),
                'fields_ids': tuple(fields_ids),
            }
            where_size = ''
            if size_range:
                where_size = "AND (size_id IS NULL OR size_id IN %(size_range)s)"
                sql_params.update({'size_range': tuple(size_range)})
            sql = """SELECT product_id, field_name_id, value, size_id
                FROM product_multi_customer_fields
                WHERE product_id IN %(prod_ids)s
                    AND field_name_id IN %(fields_ids)s
                    """ + where_size + """
                ORDER BY product_id, field_name_id"""
            _logger.info("SQL_1: %s parms: %s" % (sql, sql_params))
            cr.execute(sql, sql_params)
            vals = cr.fetchall()


            # SELECT fields names
            cr.execute("""SELECT id, export_label, expand_ring_sizes
                FROM product_multi_customer_fields_name
                WHERE id IN %s""", (tuple(fields_ids),))
            labels = cr.dictfetchall()
            expanded_labels = [label['id'] for label in labels if label['expand_ring_sizes']]

        if not fields_ids and default_fields_ids:
            vals = [
                (
                    product_id,
                    None,
                    None,
                    None
                ) for product_id in prod_ids
            ]

        self.write(cr, uid, ids, {'progress': 55.0})
        cr.commit()
        # Creation row templates (dicts)

        used_prod_ids = []
        res = []
        prod_dict = False

        # create first row (header)
        head_row = []
        for fields_id in order:
            fields_id = int(fields_id)
            if fields_id in fields_ids:
                col_names = [label['export_label'] for label in labels if label['id'] == fields_id]
            else:
                col_names = [default_field.label for default_field in default_fields_ids if default_field.id == fields_id]
            head_row.append(col_names and self.to_csv_str(col_names[0]) or '')

        _logger.info("Head row: %s" % head_row)
        i = 0
        len_vals = len(vals)
        fill_progress = 0
        # Main cycle
        _logger.info("Vals: %s" % vals)
        for val_list in vals:
            i += 1
            val = {
                'product_id': val_list[0],
                'field_name_id': val_list[1],
                'value': val_list[2],
                'size_id': val_list[3],
            }
            # If product is new
            if val['product_id'] not in used_prod_ids:
                # Work with previous product (row)
                if used_prod_ids and prod_dict:
                    # Default fields
                    self._default_fields_filling(cr, uid, prod_dict, default_fields_ids)
                    # Expand and csv-formatting
                    res += self.expand_row(cr, uid, prod_dict, order, expand, base_line)
                used_prod_ids.append(val['product_id'])
                prod_dict = {
                    'product_id': val['product_id'],
                    'sizes': None,
                    'fields': row_fields_tmpl.copy(),
                    'deny_some_sizes': False,
                    'granted_sizes': None
                    }
                # DLMR-1207
                if export_obj.use_option_sizes:
                    product = self.pool.get('product.product').browse(cr, uid, val['product_id'])
                    product_sizeable = product.sizeable or product.ring_size_ids or False
                    granted_sizes = self.pool.get('product.product').get_option_sizes(cr, uid, customer_id=export_obj.customer_id.id,
                                                                      product_id=val['product_id'])
                    # skip if no option size were found
                    if (not granted_sizes) or (not product_sizeable):
                        prod_dict = False
                        continue

                    if product_sizeable:
                        prod_dict.update({
                            'deny_some_sizes': True,
                            'granted_sizes': granted_sizes
                        })
            # size depended field
            if val['field_name_id'] and prod_dict:
                if val['size_id'] and val['field_name_id'] in expanded_labels:
                    # Add Size id in the set of size ids
                    if prod_dict['sizes']:
                        if (not prod_dict['deny_some_sizes']) or (val['size_id'] in prod_dict['granted_sizes']):
                            prod_dict['sizes'].add(val['size_id'])
                    else:
                        if (not prod_dict['deny_some_sizes']) or (val['size_id'] in prod_dict['granted_sizes']):
                            prod_dict['sizes'] = set([val['size_id']])

                    if prod_dict['fields'][val['field_name_id']] is None:
                        prod_dict['fields'][val['field_name_id']] = {}
                    # normal => base line field
                    elif type(prod_dict['fields'][val['field_name_id']]) is not dict:
                        prod_dict['fields'][val['field_name_id']] = {'None': prod_dict['fields'][val['field_name_id']]}
                    prod_dict['fields'][val['field_name_id']].update({val['size_id']: val['value']})
                # base line field
                elif type(prod_dict['fields'][val['field_name_id']]) is dict:
                    prod_dict['fields'][val['field_name_id']].update({'None': val['value']})
                # normal field
                else:
                    prod_dict['fields'][val['field_name_id']] = val['value']
            if fill_progress != int(i * 9.0 / len_vals):
                fill_progress = int(i * 9.0 / len_vals)
                self.write(cr, uid, ids, {'progress': 55.0 + fill_progress * 5.0})
                cr.commit()

        if vals and prod_dict:
            # Default fields
            self._default_fields_filling(cr, uid, prod_dict, default_fields_ids)
            # Expand and csv-formatting
            res += self.expand_row(cr, uid, prod_dict, order, expand, base_line)

        # Write in csv file
        fp = StringIO()
        writer = csv.writer(fp, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, lineterminator='\n')

        writer.writerow(head_row)
        for res_row in res:
            writer.writerow(res_row)

        fp.seek(0)
        data = fp.read()

        fp.seek(0)
        preview = list(csv.reader(fp, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL))
        if not (context and context.get('preview')):
            preview = preview[:10]

        fp.truncate()
        fp.close()

        result = [csv_filename, base64.encodestring(data), preview]

        return result

    def extract_export_csv_background(self, cr, uid, ids, params, cron_id=0, context=None):
        if not isinstance(params, dict):
            return False
        if isinstance(ids, (int, long)):
            ids = [ids]
        if self.search(cr, uid, [('id', 'in', ids)]):
            try:
                data = self.extract_export_csv(cr, uid, ids, params,
                                               context=context)
            except Exception:
                _logger.error(traceback.format_exc())
                return False
            if isinstance(data, list) and data[0] and data[1]:
                self.write(cr, uid, ids, {
                    'csv_filename': data[0],
                    'csv_file': data[1],
                    'state': 'confirm',
                    'status': 'Done',
                    'preview': simplejson.dumps({'records': data[2]})
                })

                export = self.read(cr, uid, ids[0], ['create_by_catalog'])
                if export['create_by_catalog']:
                    tag_id = params.get('tag_id', False)
                    self.send_changes_to_catalog(cr, uid, tag_id)
        if cron_id:
            self.pool.get('ir.cron').unlink(cr, uid, [cron_id], context)
        return True

    def export_confirm(self, cr, uid, ids, *args):
        cur = self.browse(cr, uid, ids[0])
        if not (cur.tag_id or cur.order_tag_id) or not cur.template_id:
            return False
        params = {
            'tag_id': cur.tag_id,
            'order_tag_id': cur.order_tag_id,
            'template_id': cur.template_id,
            'expand': cur.expand,
            'base_line': cur.base_line,
            'half_size': cur.half_size,
            'from_size': cur.from_size,
            'to_size': cur.to_size,
            'generate': cur.generate,
        }
        data = self.extract_export_csv(cr, uid, ids, params, context=None)
        if data == "order_ids error":
            raise osv.except_osv(
                _('Error'),
                _('Need to check and save the export template')
            )

        if isinstance(data, list) and data[0] and data[1]:
            self.write(cr, uid, ids, {
                'csv_filename': data[0],
                'csv_file': data[1],
                'state': 'confirm',
                'status': 'Done',
                'preview': simplejson.dumps({'records': data[2]})
            })

        return True

    def _is_already_running(self, cr, uid, cur, export_cron):
        cron_params = str2tuple(export_cron.args)
        if len(cron_params) < 2:
            return False
        dict_params = cron_params[1]
        if dict_params.get('template_id') != cur.template_id.id:
            return False
        if cur.tag_id:
            if dict_params.get('tag_id') == cur.tag_id.id:
                return True
        elif dict_params.get('order_tag_id') == cur.order_tag_id.id:
            return True
        return False

    def export_confirm_batch(self, cr, uid, ids, *args):
        if isinstance(ids, (int, long)):
            ids = [ids]
        cur = self.browse(cr, uid, ids[0])
        cron_obj = self.pool.get('ir.cron')
        if not (cur.tag_id or cur.order_tag_id) or not cur.template_id:
            return False
        export_cron_ids = cron_obj.search(cr, uid, [('name', 'ilike', '_export')])
        for export_cron in cron_obj.browse(cr, uid, export_cron_ids):
            if self._is_already_running(cr, uid, cur, export_cron):
                self.write(cr, uid, ids, {
                    'status': 'Not started',
                    'progress': 0.0
                })
                cr.commit()
                raise osv.except_osv(
                    _('Error'),
                    'There is another running export with the same tag and template'
                )

        params = {
            'tag_id': cur.tag_id and cur.tag_id.id or 0,
            'order_tag_id': cur.order_tag_id and cur.order_tag_id.id or 0,
            'template_id': cur.template_id.id,
            'expand': cur.expand,
            'base_line': cur.base_line,
            'half_size': cur.half_size,
            'from_size': cur.from_size and cur.from_size.id or 0,
            'to_size': cur.to_size and cur.to_size.id or 0,
            'generate': cur.generate,
        }

        vals = {
            'name': cur.template_id.name + '_export',
            'user_id': uid,
            'model': self._name,
            'function': 'extract_export_csv_background',
            'args': '(%s,%s,0)' % (ids, params),
            'numbercall': -1,
            'interval_type': 'minutes',
            'priority': 4,
            'doall': False,
            'type': 'export', # defined in pf_utils/ir/ir_cron.py
        }
        cron_id = cron_obj.create(cr, uid, vals)
        cron_obj.write(cr, uid, [cron_id], {
            'args': '(%s,%s,%d)' % (ids, params, cron_id)
        })
        stats = cur.stats or ''
        stats += '\nResult of Export you will can download from "Export History" list'
        self.write(cr, uid, ids, {
            'status': 'Start',
            'progress': 0.0,
            'state': 'batch',
            'stats': stats
        })

    def _get_products(self, product_tag, order_tag):
        products = []
        if product_tag:
            products.extend(product_tag.product_ids)
        if order_tag:
            for oh in order_tag.order_ids:
                if oh.sm_id:
                    products.append(oh.sm_id.product_id)
                elif oh.so_id:
                    products.extend([l.product_id for l in oh.so_id.order_line])
        return list(set(products))

    def _estimate_time(self, cur, preview_time, count_products):
        expected_time = preview_time
        if count_products >= 5:
            expected_time *= count_products / 5.0
        if expected_time <= 90:
            result = '1m'
        elif expected_time < 3570:
            result = str(int((expected_time + 30) / 60)) + 'm'
        else:
            result = str(int((expected_time + 30) / 3600)) + 'h '
            result += str(int(((expected_time + 30) % 3600) / 60)) + 'm'
        return result

    def preview_export(self, cr, uid, ids, context=None):
        cur = self.browse(cr, uid, ids[0])
        if not (cur.tag_id or cur.order_tag_id) or not cur.template_id:
            return False
        start_time = datetime.datetime.utcnow()
        params = {
            'tag_id': cur.tag_id,
            'order_tag_id': cur.order_tag_id,
            'template_id': cur.template_id,
            'expand': cur.expand,
            'base_line': cur.base_line,
            'half_size': cur.half_size,
            'from_size': cur.from_size,
            'to_size': cur.to_size,
            'generate': cur.generate,
        }
        data = self.extract_export_csv(cr, uid, ids, params,
                                       context={'preview':True})
        if data == "order_ids error":
            raise osv.except_osv(
                _('Error'),
                _('Need to check and save the export template')
            )
        products = self._get_products(cur.tag_id, cur.order_tag_id)
        fields_count = len(cur.template_id.customer_fields_ids)
        preview_time = (datetime.datetime.utcnow() - start_time).total_seconds()
        expected_time = self._estimate_time(cur, preview_time, len(products))
        stats = 'Expected full export time: ' + expected_time
        stats += '\nCount of products: ' + str(len(products))
        stats += '\nCount of columns: ' + str(fields_count)
        if isinstance(data, list) and data[0] and data[1]:
            self.write(cr, uid, ids, {
                'csv_filename': data[0],
                'csv_file': data[1],
                'stats': stats,
                'status': 'Preview',
                'preview': simplejson.dumps({'records': data[2]}),
            })

        return False

    def export_reject(self, cr, uid, ids, *args):

        context = {}
        result = []
        for export in self.browse(cr, uid, ids, context=context):

            if export.csv_file != False and export.csv_file != '':

                new_id = self.copy(cr, uid, export.id, default={
                    'csv_file': False,
                    'state': 'draft'
                }, context=context)

                result.append(new_id)

            if result and len(result):
                res_id = result[0]

                data_obj = self.pool.get('ir.model.data')

                form_view_id = data_obj._get_id(cr, uid, 'export_template', 'export_extract_form')
                form_view = data_obj.read(cr, uid, form_view_id, ['res_id'])

                return {
                    'name': _('Export template'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'export.extract',
                    'view_id': False,
                    'res_id': res_id,
                    'views': [(form_view['res_id'], 'form')],
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                }

        return True


export_extract()
