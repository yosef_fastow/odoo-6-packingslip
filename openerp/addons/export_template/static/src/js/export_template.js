openerp.export_template = function (openerp)
{
    var _t = openerp.web._t,
        _lt = openerp.web._lt;

    var QWeb = openerp.web.qweb;

    $(document).ready(function() {
        // Initialise the table
        $(".oe-listview-content").live('mouseenter', function(){
            if ($("span.oe_view_title_text").text() == "Create templates" && $(this).find("button:contains('Add')").length != 0
                && $.grep(this.parentNode.parentNode.parentNode.classList, function(e){ return e.search('customer_fields_ids')>0; }).length>0
            ){
                $(this).children('tbody').tableDnD();
                $(this).children('tbody').bind('mouseleave', function(){
                    ids_str = "";
                    $.each($(this).find('tr[data-id]'), function(i, v){
                        ids_str += ',' + v.getAttribute('data-id');
                    });
                    ids_str = ids_str.substr(1);
                    $("label:contains('order ids')").parent().next().children().val(ids_str);
                    $("label:contains('order ids')").parent().next().children().change();
                });
            }
        });
    });


    openerp.web.form.widgets.add('field_binary_csv', 'openerp.export_template.field_binary_csv');
    openerp.export_template.field_binary_csv = openerp.web.form.FieldBinaryFile.extend(
        {
            template: 'FieldBinaryFileCsv',
            start: function() {
                var self = this;
                this._super.apply(this, arguments);
                this.$element.find('button.oe-binary-file-preview').click(this.on_preview);
            },
            on_preview: function() {
                if (!this.export_preview_dialog) {
                    this.export_preview_dialog = new openerp.web.DataExportCsv();
                }
                this.export_preview_dialog.start(this);
            }
        }
    );

    openerp.web.DataExportCsv = openerp.web.Dialog.extend({
        template: 'ExportCsvDataView',
        dialog_title: _t("Export Data"),
        init: function(parent, dataset){
            var self = this;
            this._super(parent, {});
        },
        start: function(field) {
            var self = this;
            this._super();
            this.open({
                buttons: [
                    {text: _t("Close"), click: function() { self.stop(); }},
                    {text: _t("Save"), click: function() { self.on_save_as(field); }}
                ],
                close: function(event, ui) {
                    self.stop();
                }
            });
            this.create_tree(field);
        },
        on_save_as: function(field) {
            $.blockUI();
            field.session.get_file({
                url: '/web/binary/saveas_ajax',
                data: {data: JSON.stringify({
                    model: field.view.dataset.model,
                    id: (field.view.datarecord.id || ''),
                    field: field.name,
                    filename_field: (field.node.attrs.filename || ''),
                    context: field.view.dataset.get_context()
                })},
                complete: $.unblockUI,
                error: openerp.webclient.crashmanager.on_rpc_error
            });
        },
        create_tree: function(field){

            var result = $.parseJSON(field.view.datarecord.preview);
            this.$element.find('#result').empty();
            var headers, result_node = this.$element.find("#result");

            if (result.records) {
                headers = result.records[0];
            }

            result_node.append(QWeb.render('ExportCsvDataView.result', {
                'headers': headers,
                'records': result.records.slice(1)
            }));

            this.$element.find('form').removeClass('oe-import-no-result');

        },
        stop: function() {
            this.$element.remove();
            this._super();
        }
    });
}