{
    'name': 'Jasper Reports Page',
    'version': '1.0',
    "category": "Project Management",
    'complexity': "normal",
    'description': """
    """,
    'author': 'Progforce',
    'depends': [
        'base',
        'web_dashboard',
    ],
    'update_xml': [
        'security/base_sequrity.xml',
        'security/ir.model.access.csv',
        'data/board_report_data.xml',
        'view/jasper_board_view.xml',
        'view/jasper_board_admin_view.xml',
    ],
    "js": [
        'static/src/js/jasperboard.js'
    ],
    "css": [
        'static/src/css/jasperboard.css'
    ],
    'qweb': [
        "static/src/xml/jasperboard.xml",
    ],
    'active': False,
    'installable': True,
    'auto_install': False,
}
