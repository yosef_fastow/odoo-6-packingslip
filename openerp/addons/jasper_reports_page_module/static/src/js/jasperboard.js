openerp.jasper_reports_page_module = function(openerp) {
    var QWeb = openerp.web.qweb,
        _t = openerp.web._t;

    if (!openerp.jasper_reports_page_module) {
        /** @namespace */
        openerp.jasper_reports_page_module = {};
    }


    /*
     * JasperBoard
     * This client action designed to be used as a dashboard widget display
     */


    openerp.jasper_reports_page_module.JasperBoard = openerp.web.View.extend({
        template: 'JasperBoard',
        model: 'jasper.board.line',
        category: false,
        init: function (parent) {
            this._super(parent);
            this.user = _.extend(new openerp.web.DataSet(this, 'res.users'), {
                index: 0,
                ids: [this.session.uid]
            });

            this.dataset = new openerp.web.DataSetSearch(this, this.model);
        },
        start: function(){
            this._super();
            var self = this;

            return this.user.read_index(['groups_id', 'users_id']).pipe(function(record) {
                var todos_filter = [
                    '|',
                    '&', ['groups_id', '=', false], ['users_id', '=', false],
                    '|', ['groups_id', 'in', record['groups_id']], ['users_id', 'in', record['id']]
                ];
                if (self.category) {
                    todos_filter.push(['category_ids.name', 'in', [self.category]]);
                }
                return $.when(
                    self.dataset.read_slice(
                        ['id', 'name', 'href', 'use_token'],
                        { domain: todos_filter }
                    )
                );
            }, null).then(this.on_records_loaded);

        },
        on_records_loaded: function (records) {
            var lines_todos = _(records).chain()
                .map(function (record) {
                    if (record) {
                        return {
                            id: record.id,
                            name: record.name,
                            href: record.href
                        };
                    }
                })
                .value();
            this.$element.html(QWeb.render(this.template + '.content', {
                lines: lines_todos
            }));
        }
    });

    /*
     * JasperGeneral
     * This client action designed to be used as a dashboard widget display
     */

    openerp.web.client_actions.add( 'board.jasper.generic', 'openerp.jasper_reports_page_module.JasperGeneric');
    openerp.jasper_reports_page_module.JasperGeneric = openerp.jasper_reports_page_module.JasperBoard.extend({
        category: 'Generic'
    });

    openerp.web.client_actions.add( 'board.jasper.general', 'openerp.jasper_reports_page_module.JasperGeneral');
    openerp.jasper_reports_page_module.JasperGeneral = openerp.jasper_reports_page_module.JasperBoard.extend({
        category: 'General'
    });

    /*
     * JasperOverstock
     * This client action designed to be used as a dashboard widget display
     */

    openerp.web.client_actions.add( 'board.jasper.overstock', 'openerp.jasper_reports_page_module.JasperOverstock');
    openerp.jasper_reports_page_module.JasperOverstock = openerp.jasper_reports_page_module.JasperBoard.extend({
        category: 'Overstock'
    });

};
