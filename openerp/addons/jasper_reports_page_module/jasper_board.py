# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from osv import fields, osv
import hashlib
import datetime
import pytz


class jasper_board_line(osv.osv):
    _name = 'jasper.board.line'

    _columns = {
        'name': fields.char('Name', size=256, required=True,),
        'href': fields.char('href', size=256, required=True,),
        'sequence': fields.integer('Sequence'),
        'category_ids': fields.many2many('jasper.board.category', 'jasperboard_category_line_rel', 'line_id', 'category_id', 'Categories'),
        'groups_id': fields.many2many('res.groups', 'res_groups_jasper_board_line_rel', 'line_id', 'gid', 'Groups'),
        'users_id': fields.many2many('res.users', 'res_users_jasper_board_line_rel', 'line_id', 'uid', 'Users'),
        'use_token': fields.boolean('Use token'),
    }

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        if context is None:
            context = {}
        res = super(jasper_board_line, self).read(cr, uid, ids, fields=fields, context=context, load=load)
        idx = 0
        widget_fields = set(['id', 'name', 'href', 'use_token'])
        if fields and widget_fields == set(fields):
            for r in res:
                if r['use_token'] and r['href']:
                    prefix = '&'
                    if r['href'].find('?') == -1:
                        prefix = '?'
                    now = datetime.datetime.now(pytz.timezone('America/Montreal'))
                    token = hashlib.md5(str(uid) + now.strftime("%Y-%m-%d %H")).hexdigest()
                    r['href'] = r['href'] + prefix + 'token=' + token
                res[idx] = r
                idx = idx + 1
        return res

    _order = "sequence,name,id"

jasper_board_line()


class jasper_board_category(osv.osv):
    _name = 'jasper.board.category'

    _columns = {
        'name': fields.char('Name', size=256, required=True,),
        'line_ids': fields.many2many('jasper.board.line', 'jasperboard_category_line_rel', 'category_id', 'line_id',  'Lines'),
    }

jasper_board_category()
