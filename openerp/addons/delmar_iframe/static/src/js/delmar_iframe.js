openerp.delmar_iframe = function(openerp) {
    // console.log('openerp.delmar_iframe', this, arguments);
    openerp.web.form.widgets.add('form_iframe', 'openerp.delmar_iframe.IframeWidget');
    openerp.delmar_iframe.IframeWidget = openerp.web.form.Field.extend({
        template: 'iframe',

        init: function(view, node) {
            // console.log('init', this, arguments);
            this._super.apply(this, arguments);
            this.context = node.attrs.context || {};
            this.$sidebar = $('#oe_secondary_menu');
        },

        start: function() {
            // console.log(t=this);
            // console.log('start', this.get_value(), this.value, this, arguments);

            this._super.apply(this, arguments);
            return;
        },

        set_value: function(value) {
            this._super.apply(this, arguments);
            console.log('start', this.get_value(), this.value, value, arguments);

            var val = this.get_value()

            if (typeof(val) != 'undefined') {
                if (this.context['hide_menu'] || this.context['hide_menu'] === undefined) {
                    this.$sidebar.hide();
                }
                this.$iframe = this.$element.find('iframe.oe-iframe');
                this.$iframecontainer = this.$element.find('.oe-iframe-view');
                this.$iframe.attr('src', val);
                console.log('url', val, this.$iframe);

                var $container = $('td.view-manager-main-content');
                this.$iframecontainer.appendTo($container);
                $container.find('div:not(.oe-view-manager-header, .oe-view-manager-logs)').remove();

            }
        },

        stop: function(){
            // console.log('stop', this, arguments);
            this._super.apply(this, arguments);

            this.$sidebar.show();
        }
    });
}
