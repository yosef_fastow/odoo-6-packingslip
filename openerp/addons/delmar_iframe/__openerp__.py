{
    "name" : "delmar_iframe",
    "version" : "0.1",
    "author" : "Ivan Burlutskiy @ Prog-Force",
    "website" : "http://www.progforce.com/",
    "depends" : ["base", "web"],
    "description" : """

    """,
    "category": "Tools",
    "js": [
        'static/src/js/*.js'
    ],
    "css": [
        'static/src/css/*.css'
    ],
    "qweb": [
        'static/src/xml/*.xml'
    ],
    'active': True
}
