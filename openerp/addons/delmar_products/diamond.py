from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class diamond_shape(osv.osv):
    _name = "diamond.shape"
    _description = 'Diamond Shape'

    _columns = {
        'name': fields.char('Shape', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The shape of the diamond must be unique !')
    ]
diamond_shape()


class diamond_category(osv.osv):
    _name = "diamond.category"
    _description = 'Diamond Category'

    _columns = {
        'name': fields.char('Category', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The category of the diamond must be unique !')
    ]
diamond_category()


class diamond_finish(osv.osv):
    _name = "diamond.finish"
    _description = 'Diamond Finish'

    _columns = {
        'name': fields.char('Finish', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The finish of the diamond must be unique !')
    ]
diamond_finish()


class diamond_sieve(osv.osv):
    _name = "diamond.sieve"
    _description = 'Diamond Sieve'

    _columns = {
        'name': fields.char('Sieve', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The sieve of the diamond must be unique !')
    ]
diamond_sieve()


class diamond_color(osv.osv):
    _name = "diamond.color"
    _description = 'Diamond Color'

    _columns = {
        'name': fields.char('Color', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The color of the diamond must be unique !')
    ]
diamond_color()


class diamond_color_category(osv.osv):
    _name = "diamond.color_category"
    _description = 'Diamond Color_Category'

    _columns = {
        'name': fields.char('Color_Category', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The color_category of the diamond must be unique !')
    ]
diamond_color_category()


class diamond_clarity(osv.osv):
    _name = "diamond.clarity"
    _description = 'Diamond Clarity'

    _columns = {
        'name': fields.char('Clarity', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The clarity of the diamond must be unique !')
    ]
diamond_clarity()


class diamond_internal_code(osv.osv):
    _name = "diamond.internal_code"
    _description = 'Diamond Internal_Code'

    _columns = {
        'name': fields.char('Internal_Code', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The internal_code of the diamond must be unique !')
    ]
diamond_internal_code()


class diamond_note(osv.osv):
    _name = "diamond.note"
    _description = 'Diamond Note'

    _columns = {
        'name': fields.char('Note', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The note of the diamond must be unique !')
    ]
diamond_note()


class diamond_stone_fraction(osv.osv):
    _name = "diamond.stone_fraction"
    _description = 'Diamond Stone_Fraction'

    _columns = {
        'name': fields.char('Stone_Fraction', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The stone_fraction of the diamond must be unique !')
    ]
diamond_stone_fraction()
