from osv import osv, fields


class product_notes(osv.osv):
    _name = "product.notes"
    _description = 'Product notes history'

    _columns = {
        'create_date': fields.datetime('Creation Date', readonly=True),
        'write_date': fields.datetime('Update Date', readonly=True),
        'create_uid': fields.many2one('res.users', 'Created By', readonly=True),
        'write_uid': fields.many2one('res.users', 'Modify By', select=True),
        'body': fields.text('Note', required=True),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'product_id': fields.many2one('product.product', 'Product', required=True, domain=[('type', '<>', 'service')]),
    }

    _rec_name = 'body'

product_notes()
