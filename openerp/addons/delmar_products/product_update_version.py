from osv import osv, fields


class product_update_version(osv.osv):
    _name = 'product.update.version'

    def name_get(self, cr, uid, ids, context=None):
        result = {}
        if ids:
            cr.execute('''
                SELECT
                    pv.id as id,
                    concat(pv."version", '/', coalesce(pp_c.default_code, ''), '/', coalesce(pp_p.default_code, '')) as name
                FROM product_update_version pv
                    left join product_product pp_c on pv.product_id = pp_c.id
                    left join product_product pp_p on pv.parent_product_id = pp_p.id
                WHERE pv.id in %s
                ''', (tuple(ids), )
            )
            result = cr.fetchall() or []

        return result

    _columns = {
        'create_date': fields.datetime('Update Date', readonly=True),
        'parent_product_id': fields.many2one(
            'product.product', 'Parent Product', ondelete='cascade'),
        'product_id': fields.many2one(
            'product.product', 'Product', required=True, ondelete='cascade'),
        'version': fields.char(
            'Version', type='char', size=128, required=True),
    }

    _order = "id desc"

product_update_version()
