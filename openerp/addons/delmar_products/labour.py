from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class labour_category(osv.osv):
    _name = "labour.category"
    _description = 'Labour Category'

    _columns = {
        'name' : fields.char('Category', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The category of the labour must be unique !')
    ]
labour_category()

class labour_quality(osv.osv):
    _name = "labour.quality"
    _description = 'Labour Quality'

    _columns = {
        'name' : fields.char('Quality', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The quality of the labour must be unique !')
    ]
labour_quality()

class labour_difficulty(osv.osv):
    _name = "labour.difficulty"
    _description = 'Labour Difficulty'

    _columns = {
        'name' : fields.char('Difficulty', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The difficulty of the labour must be unique !')
    ]
labour_difficulty()

