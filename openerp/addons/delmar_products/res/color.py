from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class res_color(osv.osv):
    _name = "res.color"
    _description = 'Color'

    _columns = {
        'name': fields.char('Name', size=256, required=True),
        'short_name': fields.char('Short Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the color must be unique !')
    ]

res_color()
