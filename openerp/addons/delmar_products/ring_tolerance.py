from osv import osv, fields
import re
from tools.translate import _


class ring_size_tolerance(osv.osv):
    _name = "ring.size.tolerance"
    _description = 'Ring sizes tolerance'

    _columns = {
        'name': fields.float('tolerance'),
    }

    def check_sizes_tolerance(self, cr, uid, size_ids, size_tolerance):

        if not size_tolerance:
            return False

        if len(size_ids) != 2:
            return False

        size_obj = self.pool.get('ring.size')
        sizes = size_obj.browse(cr, uid, size_ids)
        float_size1 = float(sizes[0].name)
        float_size2 = float(sizes[1].name)
        size_from = float_size1 - size_tolerance
        size_to = float_size1 + size_tolerance

        if size_from <= float_size2 and float_size2 <= size_to:
            return True

        return False


ring_size_tolerance()
