from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class style_tmpl(osv.osv):
    _name = "style.tmpl"
    _description = 'Stile Template'

    _columns = {
        'name' : fields.char('Template', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The template of the stile must be unique !')
    ]
style_tmpl()


class style_tmpl_cat(osv.osv):
    _name = "style.tmpl_cat"
    _description = 'Stile Template Category'

    _columns = {
        'name' : fields.char('Template category', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The template category of the stile must be unique !')
    ]
style_tmpl_cat()
