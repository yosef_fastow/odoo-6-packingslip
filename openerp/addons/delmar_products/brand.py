from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)

class brand(osv.osv):

    _name = "brand"
    _description = "List Of Brands"
    _columns = {
        'name': fields.char('Name', size=256, required=True),
        'brand_msrp': fields.float('Brand MSRP'),
        'brand_title': fields.char('Brand Title', size=256),
        'brand_logo': fields.char('Brand Logo', size=256),
    }
    
brand()