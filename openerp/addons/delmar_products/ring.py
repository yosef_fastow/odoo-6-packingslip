from osv import osv, fields
import re
from tools.translate import _


class ring_size(osv.osv):
    _name = "ring.size"
    _description = 'Ring sizes'

    _columns = {
        'name': fields.char('Size', size=4, required=True),
    }

    _sql_constraints = [
        ('ring_size_name_unique', 'unique (name)', _('The ring size names must be unique!')),
    ]

    def init(self, cr):
        cr.execute("""
            CREATE OR REPLACE FUNCTION get_4digit_size(size_name VARCHAR) RETURNS VARCHAR AS $$
            BEGIN
                RETURN CASE WHEN size_name IS NULL THEN '0000' ELSE right('00' || CAST(size_name as FLOAT)*100, 4) END;
            END
            $$
            LANGUAGE plpgsql;
        """)

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}
        name_args = [term for term in args if (
            type(term) in (list, tuple)
            and term[0] == 'name'
            and isinstance(term[2], (str, unicode))
        )]
        for name_arg in name_args:
            if name_arg[2]:

                try:
                    float_size = float(name_arg[2])
                    if float_size:
                        if float_size >= 100.0:
                            float_size = float_size / 100
                        new_name_arg = ('name', name_arg[1], str(float_size))
                        args[args.index(name_arg)] = new_name_arg
                except:
                    pass

        return super(ring_size, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

    # FIXME: this is a duplicate of stock_move.get_size_postfix method
    def get_4digit_name(self, cr, uid, size_id, context=None):
        result = '0000'
        if size_id:
            size = self.browse(cr, uid, size_id, context=context)
            if size:
                result = self.convert_to_4digit_name(cr, uid, size.name)
        return result

    def convert_to_4digit_name(self, cr, uid, size_str, context=None):
        result = '0000'
        if size_str:
            try:
                result = re.sub('\.', '', "%05.2f" % float(size_str))
            except:
                pass

        return result

    def convert_to_regular_name(self, cr, uid, size_str, context=None):
        result = ''
        if size_str:
            try:
                float_size = float(re.sub('[^\d]', '', size_str)) / 100
                result = str(float_size) if float_size > 1.5 else size_str
            except:
                pass

        return result

    def split_itemid_to_id_delmar_size(self, cr, uid, itemid):
        id_delmar = itemid
        size = '0000'
        if '/' not in itemid:
            return id_delmar, size
        split_itemid = itemid.split('/')
        size_part = split_itemid[-1]
        if size_part in ('0000', '0', '0.0'):
            id_delmar = '/'.join(split_itemid[:-1])
        else:
            size_ids = self.search(cr, uid, [('name', '=', size_part)])
            if size_ids:
                id_delmar = '/'.join(split_itemid[:-1])
                size = self.get_4digit_name(cr, uid, size_ids[0])
        return id_delmar, size

    def get_map_id_name(self, cr, uid):
        cr.execute("SELECT id, name FROM ring_size")
        return {size['id']: size['name'] for size in cr.dictfetchall()}

ring_size()
