from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class stone_shape(osv.osv):
    _name = "stone.shape"
    _description = 'Shape'

    _columns = {
        'name': fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the stone must be unique !')
    ]

stone_shape()


class stone_clarity(osv.osv):
    _name = "stone.clarity"
    _description = 'Diamond clarity'

    _columns = {
        'name': fields.char('Clarity', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The clarity of the stone must be unique !')
    ]

stone_clarity()


class stone_color(osv.osv):
    _name = "stone.color"
    _description = 'Stone Color'

    _columns = {
        'name': fields.char('Color', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The color of the stone must be unique !')
    ]
stone_color()
