from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class finding_finding(osv.osv):
    _name = "finding.finding"
    _description = 'Finding Finding'

    _columns = {
        'name' : fields.char('Finding', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The finding of the finding must be unique !')
    ]
finding_finding()


class finding_category(osv.osv):
    _name = "finding.category"
    _description = 'Finding Category'

    _columns = {
        'name' : fields.char('Category', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The category of the finding must be unique !')
    ]
finding_category()



class finding_group(osv.osv):
    _name = "finding.group"
    _description = 'Finding Group'

    _columns = {
        'name' : fields.char('Group', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The group of the finding must be unique !')
    ]
finding_group()


class finding_attachment_type(osv.osv):
    _name = "finding.attachment_type"
    _description = 'Finding Attachment_Type'

    _columns = {
        'name' : fields.char('Attachment_Type', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The attachment_type of the finding must be unique !')
    ]
finding_attachment_type()
