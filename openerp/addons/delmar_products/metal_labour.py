from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class metal_labour_metal(osv.osv):
    _name = "metal_labour.metal"
    _description = 'Metal_Labour Metal'

    _columns = {
        'name' : fields.char('Metal', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The metal of the metal_labour must be unique !')
    ]
metal_labour_metal()
