{
    "name": "delmar_products",
    "version": "1.0.5",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "base",
        "base_tools",
        "stock"
    ],
    "description": """

    """,
    "category": "Project Management",
    "init_xml": [
        'res/color_data.xml',
        'data/pearl_data.xml',
        'data/metal_data.xml',
        'data/diamond_data.xml',
        'data/gemstone_data.xml',
        'data/decoration_data.xml',
        'data/labour_data.xml',
        'data/finishing_labour_data.xml',
        'data/metal_labour_data.xml',
        'data/plating_labour_data.xml',
        'data/style_data.xml',
        'data/finding_data.xml',
        'data/ring_data.xml',
        'data/cron_data.xml',
        'data/config_parameter_data.xml',
        'security/ir.model.access.csv',
    ],
    "demo_xml": [],
    "update_xml": [
        'view/product_view.xml',
        'view/product_notes.xml',
        'view/product_mode.xml',
        'view/res_view.xml',
        'view/gemstone_view.xml',
        'res/color_data.xml',
    ],
    'js': [
        'static/src/js/delmar_products.js',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
