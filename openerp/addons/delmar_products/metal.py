from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)

class metal(osv.osv):
    _name = "metal.metal"
    _description = 'Metal'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the metal must be unique !')
    ]

metal()


class metal_finish(osv.osv):
    _name = "metal.finish"
    _description = 'Metal finishes'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the metal finish must be unique !')
    ]

metal_finish()


class metal_manufacturing(osv.osv):
    _name = "metal.manufacturing"
    _description = 'Metal manufacturing'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the metal manufacturing must be unique !')
    ]

metal_manufacturing()


class metal_plating(osv.osv):
    _name = "metal.plating"
    _description = 'Metal plating'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the metal plating must be unique !')
    ]

metal_plating()


class metal_stamp(osv.osv):
    _name = "metal.stamp"
    _description = 'Metal Stamp'

    _columns = {
        'name' : fields.char('Stamp', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The stamp of the metal must be unique !')
    ]
metal_stamp()


class metal_measure(osv.osv):
    _name = "metal.measure"
    _description = 'Metal Measure'

    _columns = {
        'name' : fields.char('Measure', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The measure of the metal must be unique !')
    ]
metal_measure()


class metal_alloy(osv.osv):
    _name = "metal.alloy"
    _description = 'Metal Alloy'

    _columns = {
        'name' : fields.char('Alloy', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The alloy of the metal must be unique !')
    ]
metal_alloy()
