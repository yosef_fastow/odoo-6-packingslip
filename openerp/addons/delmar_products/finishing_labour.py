from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)

class finishing_labour_finishing(osv.osv):
    _name = "finishing_labour.finishing"
    _description = 'Finishing_Labour Finishing'

    _columns = {
        'name' : fields.char('Finishing', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The finishing of the finishing_labour must be unique !')
    ]
finishing_labour_finishing()

class finishing_labour_metal(osv.osv):
    _name = "finishing_labour.metal"
    _description = 'Finishing_Labour Metal'

    _columns = {
        'name' : fields.char('Metal', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The metal of the finishing_labour must be unique !')
    ]
finishing_labour_metal()
