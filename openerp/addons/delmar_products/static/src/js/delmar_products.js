openerp.delmar_products = function (openerp) {

    $('select[name="categ_id"]').live('focus', function(){
        // Keep track of the selected option.
        var selectedValue = $(this).val();

        // sort it out
        $(this).html($("option", $(this)).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));

        // Select one option.
        $(this).val(selectedValue);
    });

    attributes = [];

    attributes["product"] = ["tmw", "tdw", "tgw", "width", "height", "length", "made_in", "ring_size_ids"];
    attributes["diamond"] = ["dmd_*", "cmp_weight"];
    attributes["gemstone"] = ["gemst_*", "cmp_weight"];
    attributes["pearl"] = ["pearl_*"];
    attributes["metal"] = ["metal_*", "weight_measure_id", "cmp_weight"];
    attributes["metal_labour"] = ["met_lab_*"];
    attributes["plating_labour"] = ["plat_lab_*"];
    attributes["finishing_labour"] = ["fin_lab_*"];
    attributes["finding"] = ["finding_*"];



    function show_field(name){
        // visible div containing input or select
        $('input[name=' + name + ' ], select[name=' + name + ' ]').parent().css("display","block").addClass("visible_search")
        // visible label above input
        .parent().children('label').css("display","block").addClass("visible_search");

    }

    /* function showing additional fields
    *
    */
    function show_additional_fields(names) {
        for (name in names){

            if (names[name].indexOf("*") > -1){
                $('input, select').each(function(i, e){
                    thisName = e.getAttribute('name');
                    if (thisName){
                        if (thisName.indexOf(names[name].replace(/\*/g, "")) == 0){
                            show_field(thisName);
                        }
                    }
                });
            }
            else{
                show_field(names[name]);
            }
        }
    }

    $('select[name="categ_id"]').live('change', function(){

        var type;

        // selected type (without whitespaces) from prod_type selection
        var selected = $('select[name="categ_id"] option:selected').text().replace(/(.*)\//g, "").replace(/\s/g, "");
        $('select[name="prod_type"] option').each(function(){
            if ( selected == this.text.replace(/\s/g, "").replace(/:(.*)/, "") ){
                type = this.text.replace(/\s/g, "").replace(/(.*):/, "");
            }
        });

        // array of names fields to show
        var names = false;
        for (key in attributes){
            names = (type == key) ? attributes[key] : names;
        }

        // hide old fields
        $('.visible_search').css("display","none").removeClass("visible_search");

        // show new fields
        if (names) {
            show_additional_fields(names);
        }
    });


}