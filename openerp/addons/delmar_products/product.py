import re
from osv import osv, fields
import logging
from functools import partial
import datetime
from openerp.addons.pf_utils.utils.re_utils import clean_code


logger = logging.getLogger('product.product')


class product_template(osv.osv):
    _inherit = "product.template"
    _log_empty_update = False

    def _get_type(self, cr, uid, ids, name, args, context=None):

        default_type = 'undefined'

        res = {}
        for obj_id in ids:
            categ = self.browse(cr, uid, obj_id).categ_id
            if categ:
                categ_obj = self.pool.get('product.category')
                res[obj_id] = categ_obj.get_prod_type(cr, uid, categ.id)
            else:
                res[obj_id] = default_type

        return res

    def select_prod_types(self, cr, uid, context=None):
        if not context:
            context = {}

        cr.execute('select name, id from product_category limit 100')
        res_list = cr.fetchall()
        res = []
        categ_obj = self.pool.get('product.category')
        for elem in res_list:
            res.append((elem[0], (str)(elem[0]) + ':' + (str)(categ_obj.get_prod_type(cr, uid, elem[1]))))
        return tuple(res)

    _columns = {
        'name': fields.char('Name', size=1024, required=True, translate=True, select=True),
        'prod_type': fields.function(_get_type, string="Product type", type='selection', selection=select_prod_types, method=True),
        'cmp_weight': fields.float('Weight', digits=(12, 4)),
        'stone_number': fields.integer('Stone number'),
        'filemaker_id': fields.integer('Filemaker id'),

        'style_tmpl_name_id': fields.many2one('style.tmpl', 'Style template'),
        'style_tmpl_cat_name_id': fields.many2one('style.tmpl_cat', 'Style template category'),

        'categ_id': fields.many2one('product.category','Product type', required=True, change_default=True, domain="[('type','=','normal')]" ),
        'categ_name': fields.related('categ_id', 'name', type='char', store=True, readonly=True),
        'sizeable': fields.related('categ_id', 'sizeable', string="Sizeable", type='boolean', readonly=True, ),

        # dimensions
        # [IB - 2016-02-09]: genius Eren's decision, because in delmar there is no standard unit of measure
        'str_length': fields.char("Length", size=125, ),
        'str_ext_length': fields.char("Ext Length", size=125, ),
        'str_width': fields.char("Width", size=125, ),
        'str_height': fields.char("Height", size=125, ),
        'str_depth': fields.char("Depth", size=125, ),

        # ring
        'ring_size_ids': fields.many2many('ring.size', 'product_ring_size_default_rel', 'product_id', 'size_id', 'Ring sizes'),

        # diamond
        'dmd_category_id': fields.many2one('diamond.category', 'Category'),
        'dmd_shape_id': fields.many2one('diamond.shape', 'Shape'),
        'dmd_finish_id': fields.many2one('diamond.finish', 'Finish'),
        'dmd_dimension_min': fields.float('Dimension min'),
        'dmd_dimension_max': fields.float('Dimension max'),
        'dmd_sieve_min_id': fields.many2one('diamond.sieve', 'Sieve min'),
        'dmd_stone_fraction': fields.many2one('diamond.stone_fraction', 'Stone fraction'),
        'dmd_sieve_max_id': fields.many2one('diamond.sieve', 'Sieve max'),
        'dmd_weight': fields.related('cmp_weight', string="Weight", type="float", store=False, digits=(12, 4)),
        'dmd_total_weight': fields.related('tdw', string="Total diamonds weight", type="float", store=False),
        'dmd_carat_min': fields.float('Carat min'),
        'dmd_carat_max': fields.float('Carat max'),
        'dmd_length': fields.related('length', string="Length", type="float", store=False),
        'dmd_width': fields.related('width', string="Width", type="float", store=False),
        'dmd_color_id': fields.many2one('diamond.color', 'Color'),
        'dmd_color_categ_id': fields.many2one('diamond.color_category', 'Color category'),
        'dmd_material': fields.char('Material', size=256),
        'dmd_clarity_id': fields.many2one('diamond.clarity', 'Clarity'),
        'dmd_internal_code_id': fields.many2one('diamond.internal_code', 'Internal code'),
        'dmd_note_id': fields.many2one('diamond.note', 'Note'),
        'dmd_est_price': fields.char('Est Price', size=256),
        'dmd_code': fields.char('Code', size=256),
        'dmd_decoration_setting_type_id': fields.many2one('decoration.setting', 'Decoration setting type'),
        'dmd_certified': fields.boolean('Certified'),

        # gemstone
        'gemst_sku': fields.char('SKU', size=256),
        'gemst_length': fields.related('length', string="Length", type="float", store=False),
        'gemst_width': fields.related('width', string="Width", type="float", store=False),
        'gemst_shape_id': fields.many2one('gemstone.shape', 'Shape'),
        'gemst_type_id': fields.many2one('gemstone.type', 'Type'),
        'gemst_weight': fields.related('cmp_weight', string="Weight", type="float", store=False, digits=(12,4)),
        'gemst_total_weight': fields.related('tgw', string="Total gemstones weight", type="float", store=False),
        'gemst_avg_weight': fields.float('Avg weight'),
        'gemst_quality': fields.selection([('B', 'B'), ('A', 'A'), ('AA', 'AA'), ('AAA', 'AAA')], 'Quality'),
        'gemst_local_price': fields.float('Local price'),
        'gemst_local_currency_id': fields.many2one('res.currency', 'Local currency'),
        'gemst_price_per': fields.selection([('carat','Carat'),
                                            ('piece','Piece')],'Price per'),
        'gemst_decoration_setting_type_id': fields.many2one('decoration.setting', 'Decoration setting type'),

        # finding
        'finding_id': fields.many2one('finding.finding', 'Finding'),
        'finding_category_id': fields.many2one('finding.category', 'Finding category'),
        'finding_group_id': fields.many2one('finding.group', 'Finding group'),
        'finding_attachment_type_id': fields.many2one('finding.attachment_type', 'Finding attachment type'),

        #
        'finding_color': fields.char('Finding color', size=256),



        # pearl
        'pearl_type_id': fields.many2one('pearl.type', 'Type'),
        'pearl_surface_id': fields.many2one('pearl.surface', 'Surface'),
        'pearl_luster': fields.selection([('Low', 'Low'),
                                            ('Medium', 'Medium'),
                                            ('High', 'High')], 'Luster'),
        'pearl_shape_id': fields.many2one('pearl.shape', 'Shape'),
        'pearl_color_id': fields.many2one('pearl.color', 'Color'),
        'pearl_quality': fields.selection([('A', 'A'),
                                           ('AA', 'AA'),
                                           ('AAA', 'AAA'),
                                           ('B', 'B'),
                                           ('A-', 'A-'),
                                           ('AB', 'AB')],  # Does anybody aware about the meaning of this abbreviation?
                                          'Quality'),
        'pearl_class': fields.selection([('Cultured', 'Cultured'),('Simulated','Simulated')], 'Class'),
        'pearl_size_type': fields.selection([('Single', 'Single'),('String','String')],'Size type'),
        'pearl_size_single_id': fields.many2one('pearl.size_single', 'Single size'),
        'pearl_size_strand_id': fields.many2one('pearl.size_strand', 'Strand size'),
        'pearl_local_price': fields.float('Local price'),
        'pearl_local_currency_id': fields.many2one('res.currency', 'Local currency'),
        'pearl_attachment_id': fields.many2one('pearl.attachment', 'Attachment'),

        # pearl for mapping
        'pearl_production_information': fields.char('Production Information', size=256),

        # metal
        'metal_id': fields.many2one('metal.metal', 'Metal'),
        'metal_alloy_id': fields.many2one('metal.alloy', 'Metal alloy'),
        'metal_stamp_id': fields.many2one('metal.stamp', 'Metal stamp'),
        'weight_measure_id': fields.many2one('metal.measure', 'Metal measure'),
        'metal_finish_id': fields.many2one('metal.finish', 'Metal finish'),
        'metal_manufacturing_id': fields.many2one('metal.manufacturing', 'Metal manufacturing'),
        'metal_plating_id': fields.many2one('metal.plating', 'Metal plating'),
        'metal_weight': fields.related('cmp_weight', string="Weight", type="float", digits=(12,4), store=False),

        # product
        'tw': fields.float('Total weight'),
        'tmw': fields.float('Total metals weight'),
        'tgw': fields.float('Total gemstones weight'),
        'tdw': fields.float('Total diamonds weight'),
        'min_tdw': fields.float('Min diamonds weight'),
        'min_ctr_diamond': fields.float('Min ctr diamond'),
        'side_diamond': fields.float('Side diamond'),
        'width': fields.float('Width'),
        'height': fields.float('Height'),
        'length': fields.float('Length'),
        'ext_length': fields.float('Ext Length'),
        'depth': fields.float('Depth'),
        'band_width': fields.char('Band width', size=50),
        'made_in': fields.many2one('res.country', 'Made in'),
        'id_delmar': fields.char('Delmar ID', size=256),
        'asin': fields.char('ASIN', size=256),
        'id_style': fields.char('Designer style ID', size=256),
        'customer_name': fields.char('Customer name', size=256),
        'gold_14k': fields.float('Gold of 14K'),
        'fc_margin': fields.float('FC Margin'),
        'retail_margin': fields.float('Retail Margin'),

        'prod_quality': fields.many2one('labour.quality', 'Quality'),
        'gold_loss': fields.float('Gold Loss'),
        'increment': fields.float('Increment'),
        'increment_multiplier': fields.float('Increment multiplier'),
        'labour': fields.float('Labour'),
        'duty_mark': fields.boolean('Duty mark'),
        'duty_price': fields.float('Duty'),
        'shipping_mark': fields.boolean('Shipping mark'),
        'shipping_price': fields.float('Shipping'),
        'certified': fields.boolean('Certified'),

        'prod_ring_style': fields.char('Ring style', size=256),

        # concatinated fields for components' data
        'prod_stone_type': fields.char('Stone type', size=256),
        'prod_stone_weight': fields.char('Stone weight', size=256),
        'prod_total_stones': fields.integer('Total stones'),
        'prod_stone_color': fields.char('Stone color', size=256),
        'prod_stone_clarity': fields.char('Stone clarity', size=256),
        'prod_metal_stamp': fields.char('Metal stamp', size=256),
        'prod_metal_type': fields.char('Metal type', size=256),
        'prod_stone_shape': fields.char('Stone shape', size=256),
        'prod_stone_size': fields.char('Stone size', size=256),
        'prod_stone_decoration_setting_type': fields.char('Stone decoration setting type', size=256),

        'prod_dmd_carat_max': fields.char('Carat max', size=256),
        'prod_dmd_carat_min': fields.char('Carat min', size=256),
        'prod_dmd_category': fields.char('Category', size=256),
        'prod_dmd_certified': fields.char('Certified', size=256),
        'prod_dmd_clarity': fields.char('Clarity', size=256),
        'prod_dmd_color_categ': fields.char('Color category', size=256),
        'prod_dmd_color': fields.char('Color', size=256),
        'prod_dmd_decoration_setting_type': fields.char('Decoration setting type', size=256),
        'prod_dmd_dimension_max': fields.char('Dimension max', size=256),
        'prod_dmd_dimension_min': fields.char('Dimension min', size=256),
        'prod_dmd_finish': fields.char('Finish', size=256),
        'prod_dmd_material': fields.char('Material', size=256),
        'prod_dmd_shape': fields.char('Shape', size=256),
        'prod_dmd_sieve_max': fields.char('Sieve max', size=256),
        'prod_dmd_sieve_min': fields.char('Sieve min', size=256),
        'prod_dmd_stone_fraction': fields.char('Stone fraction', size=256),
        'prod_finding_attachment_type': fields.char('Finding attachment type', size=256),
        'prod_finding_category': fields.char('Finding category', size=256),
        'prod_finding_color': fields.char('Finding color', size=256),
        'prod_finding_group': fields.char('Finding group', size=256),
        'prod_finding': fields.char('Finding', size=256),
        'prod_gemst_decoration_setting_type': fields.char('Decoration setting type', size=256),
        'prod_gemst_local_currency': fields.char('Local currency', size=256),
        'prod_gemst_price_per': fields.char('Price per', size=256),
        'prod_gemst_quality': fields.char('Quality', size=256),
        'prod_gemst_shape': fields.char('Shape', size=256),
        'prod_gemst_type': fields.char('Type', size=256),
        'prod_metal_alloy': fields.char('Metal alloy', size=256),
        'prod_metal_finish': fields.char('Metal finish', size=256),
        'prod_metal_manufacturing': fields.char('Metal manufacturing', size=256),
        'prod_metal_plating': fields.char('Metal plating', size=256),
        'prod_pearl_attachment': fields.char('Attachment', size=256),
        'prod_pearl_class': fields.char('Class', size=256),
        'prod_pearl_color': fields.char('Color', size=256),
        'prod_pearl_local_currency': fields.char('Local currency', size=256),
        'prod_pearl_local_price': fields.char('Local price', size=256),
        'prod_pearl_luster': fields.char('Luster', size=256),
        'prod_pearl_quality': fields.char('Quality', size=256),
        'prod_pearl_shape': fields.char('Shape', size=256),
        'prod_pearl_size_single': fields.char('Size', size=256),
        'prod_pearl_size_strand': fields.char('Size', size=256),
        'prod_pearl_size_type': fields.char('Size type', size=256),
        'prod_pearl_surface': fields.char('Surface', size=256),
        'prod_pearl_type': fields.char('Type', size=256),
        'prod_weight_measure': fields.char('Metal measure', size=256),

        'prod_fin_finishing': fields.char('Labour Finishing', size=256),
        'prod_fin_lab_category': fields.char('Category', size=256),
        'prod_fin_lab_finishing': fields.char('Finishing', size=256),
        'prod_fin_lab_local_currency': fields.char('Local currency', size=256),
        'prod_fin_lab_local_supplier': fields.char('Supplier', size=256),
        'prod_fin_lab_metal': fields.char('Metal', size=256),
        'prod_fin_lab_quality': fields.char('Category', size=256),
        'prod_met_lab_category': fields.char('Category', size=256),
        'prod_met_lab_cost_type': fields.char('Cost type', size=256),
        'prod_met_lab_difficulty': fields.char('Difficulty', size=256),
        'prod_met_lab_links_max': fields.char('Links Max', size=256),
        'prod_met_lab_links_min': fields.char('Links Min', size=256),
        'prod_met_lab_local_currency': fields.char('Local currency', size=256),
        'prod_met_lab_local_price': fields.char('Local price', size=256),
        'prod_met_lab_local_supplier': fields.char('Supplier', size=256),
        'prod_met_lab_metal': fields.char('Metal', size=256),
        'prod_met_lab_quality': fields.char('Quality', size=256),
        'prod_met_lab_weight_max': fields.char('Weight Max', size=256),
        'prod_met_lab_weight_min': fields.char('Weight Min', size=256),
        'prod_plat_lab_category': fields.char('Category', size=256),
        'prod_plat_lab_color': fields.char('Color', size=256),
        'prod_plat_lab_local_currency': fields.char('Local currency', size=256),
        'prod_plat_lab_local_price': fields.char('Local price', size=256),
        'prod_plat_lab_local_supplier': fields.char('Supplier', size=256),
        'prod_plat_lab_metal': fields.char('Metal', size=256),
        'prod_plat_lab_multitone': fields.char('Multitone', size=256),
        'prod_plat_lab_platyng_type': fields.char('Plating Type', size=256),
        'prod_plat_lab_quality': fields.char('Quality', size=256),

        # product (for FCDC mapping)
        'prod_active': fields.char('Active', size=256),
        'prod_color': fields.char('Color', size=256),
        'prod_cut_shape': fields.char('Cut shape', size=256),
        'prod_finish': fields.char('Finish', size=256),
        'prod_metal': fields.char('Metal', size=256),
        'prod_tarif_classification': fields.char('Tarif classification', size=256),
        'prod_tarif_classification_ca': fields.char('Tarif classification CA', size=256),
        'prod_tarif_classification_hk': fields.char('Tarif classification HK', size=256),
        'override_auto_tariff': fields.boolean('Override auto-tariff', help='If option is on the value of Short Description CA field will be automatically used for Description on Ship Invoice screen', ),
        'prod_imported_mark': fields.boolean('Imported mark'),
        'search_str': fields.char('Search str', size=256),
        'supplier_1': fields.char('Supplier 1', size=256),
        'supplier_2': fields.char('Supplier 2', size=256),

        # for all components
        'id_delmar_familly': fields.char('ID delmar familly', size=256),
        'igi_information': fields.char('IGI Information', size=256),
        'price_cost_a': fields.float('Price cost A'),
        'price_cost_us': fields.float('Price cost US'),
        'price_cost_visualised_a': fields.float('Price cost visualised A'),
        'price_srp_ca': fields.integer('Price SRP CA'),
        'prod_diam_mm': fields.char('Prod diam mm', size=256),
        'prod_duty_rate': fields.float('Prod duty rate'),
        'prod_sub_type': fields.char('Prod sub type', size=256),
        'prod_currency_calc_graphic': fields.char('Prod currency calc graphic', size=256),
        'price_cost_showas': fields.char('Price cost show as', size=256),
        'price_total_a': fields.float('Price total A'),

        # Metal Labour
        'met_lab_category_id': fields.many2one('labour.category', 'Category'),
        'met_lab_metal_id': fields.many2one('metal_labour.metal', 'Metal'),
        'met_lab_quality': fields.many2one('labour.quality', 'Quality'),
        'met_lab_local_price': fields.float('Local price'),
        'met_lab_weight_min': fields.float('Weight Min'),
        'met_lab_weight_max': fields.float('Weight Max'),
        'met_lab_links_min': fields.integer('Links Min'),
        'met_lab_links_max': fields.integer('Links Max'),
        'met_lab_cost_type': fields.selection([
            ('item', 'Item'),
            ('gram', 'Gram')
            ],'Cost type'),
        'met_lab_local_currency_id': fields.many2one('res.currency', 'Local currency'),
        'met_lab_local_supplier_id': fields.many2one('res.partner', 'Supplier'),
        'met_lab_difficulty_id': fields.many2one('labour.difficulty', 'Difficulty'),


        # Plating Labour
        'plat_lab_quality': fields.many2one('labour.quality', 'Quality'),
        'plat_lab_category_id': fields.many2one('labour.category', 'Category'),
        'plat_lab_metal_id': fields.many2one('plating_labour.metal', 'Metal'),
        'plat_lab_platyng_type_id': fields.many2one('plating_labour.plating_type', 'Plating Type'),
        'plat_lab_color': fields.selection([('White', 'White'),
                                            ('Yellow', 'Yellow'),
                                            ('Pink', 'Pink')], 'Color'),
        'plat_lab_multitone': fields.boolean('Multitone'),
        'plat_lab_local_price': fields.float('Local price'),
        'plat_lab_local_currency_id': fields.many2one('res.currency', 'Local currency'),
        'plat_lab_local_supplier_id': fields.many2one('res.partner', 'Supplier'),


        # finishing labour
        'fin_lab_quality': fields.many2one('labour.quality', 'Quality'),
        'fin_lab_category_id': fields.many2one('labour.category', 'Category'),
        'fin_lab_metal_id': fields.many2one('finishing_labour.metal', 'Metal'),
        'fin_lab_finishing_id': fields.many2one('finishing_labour.finishing', 'Finishing'),
        'fin_lab_local_price': fields.float('Local price'),
        'fin_lab_local_currency_id': fields.many2one('res.currency', 'Local currency'),
        'fin_lab_local_supplier_id': fields.many2one('res.partner', 'Supplier'),

        'supplier_sku': fields.char('Supplier SKU', size=128),
        'image_url': fields.char('Image URL', size=128),
        'prod_manufacture': fields.char('Prod Manufacture', size=128),
        'prod_type_category': fields.char('Type Category', size=128),
        'prod_type_extended': fields.char('Type Extended', size=128),
        'prod_type_str': fields.char('Type', size=128),
        'collection': fields.integer('Collection'),
        'pearl_grade': fields.char('Pearl Grade', size=128),
        'component_cut_shape': fields.related('prod_cut_shape', string="Cut Shape", type="char"),
        'component_diam_mm': fields.related('prod_diam_mm', string="Component diam mm", type="char"),
        'component_dmd_decoration_set_type': fields.related('prod_dmd_decoration_setting_type', string="Decoration setting type", type="char"),
        'component_gemst_decoration_setting_type': fields.related('prod_gemst_decoration_setting_type', string="Decoration setting type", type="char"),
        'component_finding_color': fields.related('prod_finding_color', string="Finding color", type="char"),
        'component_pearl_quality': fields.related('prod_pearl_quality', string="Pearl quality", type="char"),

        #brand
        'brands': fields.many2one('brand','Brand'),
        'brand_msrp': fields.float('Brand MSRP'),
        'brand_title': fields.char('Brand Title', size=256),
        'brand_logo': fields.char('Brand Logo', size=256),
        'microplating': fields.char('Micro Plating', size=128),

        'created_from_erp_screen': fields.boolean('Created From ERP Screen'),

        'micron_flag': fields.boolean('Micron Flag'),
        'micron': fields.float('Micron'),
    }

    _defaults = {
        'type': lambda *a: 'product',
        'asin': '',
        'created_from_erp_screen': False,
        'override_auto_tariff': False,
    }

product_template()


class product_category(osv.osv):
    _inherit = "product.category"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True, select=True, readonly=True),
        'parent_id': fields.many2one('product.category','Parent Category', select=True, ondelete='cascade', readonly=True),
        'prod_type': fields.selection([
            ('product', 'Product'),
            ('diamond', 'Diamond'),
            ('gemstone', 'Gemstone'),
            ('metal_stone', 'Metal / Stone'),
            ('pearl', 'Pearl'),
            ('metal', 'Metal'),
            ('finding', 'Finding'),
            ('metal_labour', 'Metal Labour'),
            ('plating_labour', 'Plating Labour'),
            ('finishing_labour', 'Finishing Labour')], 'Type'),
        'sizeable': fields.boolean('Sizeable'),
        'short_name': fields.char('Abbreviation', size=64,),
    }

    _defaults = {
        'sizeable': False,
    }

    def get_prod_type(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        res = False
        categ = self.browse(cr, uid, ids)
        if categ.prod_type:
            return categ.prod_type
        if categ.parent_id:
            res = self.get_prod_type(cr, uid, categ.parent_id.id)

        return res

product_category()


class product_set_components(osv.osv):
    _name = 'product.set.components'
    _rec_name = 'parent_product_id'
    _columns = {
        'parent_product_id': fields.many2one('product.product', "Parent", required=True, ),
        'cmp_product_id': fields.many2one('product.product', "Component", required=True, ),
        'qty': fields.integer('Qty', required=True, ),
        'sizeable': fields.boolean('sizeable')
    }

product_set_components()


class product_product(osv.osv):
    _inherit = 'product.product'
    _log_empty_update = False

    def _get_set_parents(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            cr.execute("""
                SELECT p_pc.id, array_to_string(array_agg(p_pp.id order by p_pp.default_code), ',')
                FROM product_product p_pc
                    LEFT JOIN mssql_prg_sets_components m_pc on m_pc.id_delmar ilike p_pc.default_code
                    LEFT JOIN mssql_prg_sets m_ps on m_ps.id = m_pc.set_id
                    LEFT JOIN product_product p_pp on p_pp.default_code ilike m_ps.id_delmar and p_pp.type = 'product'
                WHERE 1=1
                    AND p_pc.id in %s
                GROUP BY p_pc.id
                """, (tuple(ids),))
            lines = cr.fetchall() or []
            for pp_id, cmp_ids in lines:
                res[pp_id] = res[pp_id] = [int(x) for x in (cmp_ids or '').split(',') if x]
        return res

    def _get_internal_code(self, cr, uid, ids, *args, **kwargs):
        res = {}

        for product in self.browse(cr, uid, ids):
            res[product.id] = re.sub(r'[^a-zA-Z0-9-./]', '', str(product.default_code).upper())

        return res

    def get_product_bins(self, cr, uid, ids, loc_ids, size_ids, force_bins=None, excluding_bins=None, context=None):
        if not excluding_bins:
            excluding_bins = []
        if not force_bins:
            force_bins = []
        res = set([])

        loc_ids = [str(x) for x in loc_ids if type(x) is int]
        size_ids = [str(x) for x in size_ids if type(x) is int]

        where_location = ""
        where_location_a = ""
        if loc_ids:
            where_location = " sm.location_dest_id in (%s) OR " % (",".join(loc_ids),)
            where_location_a = " AND a.location_dest_id in (%s) " % (",".join(loc_ids),)

        where_size = " AND a.size_id IS NULL "
        if size_ids:
            where_size = " AND a.size_id IN (%s) " % (",".join(size_ids),)

        where_bin_name = ""
        if excluding_bins:
            where_bin_name = " AND sb.name NOT IN ('%s')" % (",".join(excluding_bins),)

        for prod_id in ids:
            sql = """
                SELECT -- distinct on (a.product_id, a.size_id)
                a.product_id, a.size_id, a.location_dest_id, a.bin, a.bin_id, a.qty
                FROM (
                    SELECT sm.product_id,
                        sm.size_id,
                        CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id,
                        sm.bin,
                        sm.bin_id,
                        sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as qty
                    FROM stock_move sm
                        LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                        LEFT JOIN stock_bin sb on sb.id = sm.bin_id
                    WHERE sm.state IN ('done', 'assigned')
                        AND sm.product_id = %s
                        AND ( %s sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                        %s
                    GROUP BY (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END),
                    sm.bin,
                    sm.bin_id,
                    sm.product_id,
                    sm.size_id
                    ORDER BY CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END
                ) a
                WHERE 1=1 %s %s
            """ % (prod_id, where_location, where_bin_name, where_location_a, where_size,)
            cr.execute(sql)

            bins = cr.dictfetchall()
            for shelf in bins:
                if shelf['bin_id'] or False:
                    res.add(shelf['bin_id'])
                elif shelf['bin'] or False:
                    force_bins.append(shelf['bin'])

            if force_bins:
                bin_obj = self.pool.get('stock.bin')
                force_bin_list = bin_obj.search(cr, uid, [('name', 'in', force_bins)])

                if force_bin_list:
                    for bin in force_bin_list:
                        res.add(bin)
        return tuple(res)

    def get_mssql_product_bins(self, cr, uid, ids, loc_ids, size_ids, excluding_bins=None, context=None):
        if not context:
            context = {}
        """
        :param context: {'warehouse': 'CAFER'}
        :return: tuple('bin_id_0', 'bin_id_1')
        """
        bins = []
        bin_obj = self.pool.get("stock.bin")

        if excluding_bins is None:
            excluding_bins = []

        for excluding_bin in excluding_bins:
            if excluding_bin != False:
                bins.append(excluding_bin)

        res = set([])

        where_warehouse = ""
        whs_names = []

        where_location = ""
        loc_names = []

        where_sizes = 'AND tr."Size" IN (\'0000\')'
        sizes = []

        where_excluding_bins = ""

        if loc_ids:
            for loc_id in loc_ids:

                loc = self.pool.get('stock.location').read(cr, uid, loc_id, ['name', 'warehouse_id'])
                loc_name = loc.get('name', False)
                if loc_name:
                    loc_names.append(loc_name)

                whs_name = loc.get('warehouse_id', False) and loc.get('warehouse_id', False)[1]
                if whs_name:
                    whs_names.append(whs_name)

        if size_ids:
            for size_id in size_ids:
                if size_id and isinstance(size_id, int):
                    size = self.pool.get('ring.size').read(cr, uid, size_id, ['name']).get('name', False)
                    if '.' in size:
                        size = size.replace('.', '')
                    if len(size) > 2:
                        size = '%s0' % size
                    else:
                        size = '0%s0' % size
                    sizes.append(size)

        # DLMR-1263
        # MANUAL ADD WAREHOUSE
        if 'warehouse' in context:
            whs_names.append(context.get('warehouse'))

        whs_names = tuple(whs_names)
        loc_names = tuple(loc_names)
        sizes = tuple(sizes)

        if whs_names:
            where_warehouse = "AND tr.WAREHOUSE in ('%s')" % (",".join(whs_names),)

        # if loc_names:
        #     where_location = "AND tr.STOCKID in ('%s')" % (",".join(loc_names),)

        if sizes:
            where_sizes = 'AND tr."Size" IN (\'%s\')' % (",".join(sizes),)

        if bins:
            where_excluding_bins = "AND tr.BIN not in ('%s')" % (",".join(bins),)

        for prod_id in ids:
            delmar_id = self.pool.get('product.product').read(cr, uid, prod_id, ['default_code']).get('default_code', False)
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_main_servers(cr, uid, single=True)
            if server_id:
                sql = """
                    SELECT DISTINCT BIN
                    FROM TRANSACTIONS as tr
                    WHERE tr.ID_DELMAR = '%s'
                    %s
                    %s
                    %s
                    %s
                    AND tr.STATUS <> 'Posted'
                """ % (delmar_id, where_sizes, where_warehouse, where_location, where_excluding_bins,)
                result = server_data.make_query(cr, uid, server_id, sql, select=True, commit=False)

                if result:
                    for line in result:
                        # if bin not created in erp
                        for bin in line:
                            bin_id = bin_obj.search(cr, uid, [('name', '=', bin)])
                            if bin_id:
                                bin_id = bin_id[0]
                            else:
                                bin_id = bin_obj.create(cr, uid, {'name': bin})
                            if bin_id:
                                res.add(bin_id)

        return tuple(res)

    _columns = {
        'creation_date': fields.datetime('Create Date'),
        'name_template': fields.related('product_tmpl_id', 'name', string="Name", type='char', size=1024, store=True, select=True),
        'set_component_ids': fields.one2many(
            'product.set.components', 'parent_product_id',
            'Set componets', help="Set componets",
        ),
        'set_parent_ids': fields.one2many(
            'product.set.components', 'cmp_product_id',
            'Set parents', help="Set parents",
        ),
        'internal_code': fields.function(
            _get_internal_code,
            type='char',
            size=256,
            readonly=True,
            store={
                'product.product': (
                    lambda self, cr, uid, ids, context=None: ids,
                    ['default_code'],
                    10
                )
            }
        ),
        'web_id': fields.char('Web ID', size=256, readonly=True),
        'type': fields.related('product_tmpl_id', 'type', string="Type", type='char', store=True, select=True),
        'total_qty': fields.integer('Total count'),
        'stock_move_ids': fields.one2many('stock.move', 'product_id', 'Product Moves', help='List of moves for current product.', ),
        'short_description': fields.text('Short Description CA', ),
        'custom_description': fields.text('Short Description US', ),
        'update_description': fields.boolean('Manually Update Description',
            help="""Descriptions of products are updated regularly, based on an automated description.
When checked, this functionality will be paused until the checkbox is unchecked again."""),
        'dnr': fields.boolean('DNR',),
        'liquidation': fields.boolean('Liquidation', ),
        'designer': fields.boolean('Designer', ),
        'sr': fields.boolean('SR', ),
        'exc': fields.boolean('Exc', ),
        'notes': fields.one2many('product.notes', 'product_id', 'Notes', help='Product notes history.', ),
        'def_supplier_id': fields.many2one('res.partner', 'Default Supplier', domain='[("supplier", "=", True)]'),
        'mode_history': fields.one2many('product.mode', 'product_id', 'Mode History'),
        'update_version_history_ids': fields.one2many('product.update.version', 'product_id', 'Update Version History'),
        'instruction_notes': fields.text('Instruction Notes', ),
        'lightning_attributes': fields.one2many('product.multi.customer.fields', 'product_id', 'Lightning attributes', domain=[("show_on_lightning_attrs_tab", "=", True)]),
    }

    _defaults = {
        'total_qty': 0,
        'dnr': False,
        'liquidation': False,
        'designer': False,
        'sr': False,
        'update_description': False,
    }

    def copy(self, cr, uid, id, default=None, context=None):

        if context is None:
            context = {}

        context['generate_customer_fields'] = False

        if not default:
            default = {}

        cm_fields = self.pool.get('product.multi.customer.fields').search(
            cr, uid, [
                ('product_id', '=', id),
                ('field_name_id.label', 'in', ('Customer Price', 'Customer Price Formula'))
            ]
        )

        default.update({
            'multi_customer_fields': [(6, None, cm_fields)],
            'multi_customer_additional_fields': False,
            'hash_customer_fields': False,
            'hash_customer_additional_fields': [(6, 0, [])],
            'hash_ring_sizes': False,
            'hash_expand_ring_sizes_fields': False,
            'log_ids': False,
            'mode_history': False,
            'notes': False,
            'orderpoint_ids': False,
            'product_shipping_ids': False,
            'stock_move_ids': False,
            'tagging_ids': False,
            'update_version_history_ids': False,
            'gta_history_ids': False,
            })

        return super(product_product, self).copy(cr, uid, id, default, context=context)

    # cron method
    def update_sets_packlines(self, cr, uid, context=None):
        cr.execute("""SELECT DISTINCT parent_product_id
            FROM product_set_components""")
        parent_ids = (row[0] for row in cr.fetchall())
        for parent_id in parent_ids:
            self.update_set_packlines_from_components(cr, uid, parent_id)

    def update_set_packlines_from_components(self, cr, uid, product_id):
        product_set = self.browse(cr, uid, product_id)
        if not product_set or not product_set.set_component_ids:
            return False
        pack_line_data = {}
        for set_component in product_set.set_component_ids:
            for pack_line in set_component.cmp_product_id.pack_line_ids:
                component = pack_line.product_id
                if component.id not in pack_line_data:
                    pack_line_data[component.id] = {'quantity': 0, 'total': 0}
                pack_line_component = pack_line_data[component.id]
                total_qty = set_component.qty * pack_line.quantity
                pack_line_component['quantity'] += total_qty
                pack_line_component['total'] += total_qty * pack_line.cost
        for component_id in pack_line_data:
            if pack_line_data[component_id]['quantity'] <= 0:
                del pack_line_data[component_id]
                continue
            component = pack_line_data[component_id]
            component['cost'] = component['total'] / component['quantity']
        result = self.update_components(cr, uid, product_id, pack_line_data)
        return result

    def split_sets_to_components(self, cr, uid, product_id, size=0, qty=1):
        default_size = 0 if isinstance(size, (int, long)) else '0000'
        product = self.browse(cr, uid, product_id)
        if not product.set_component_ids:
            result = [{
                'default_code': product.default_code,
                'product_id': product_id,
                'size': size,
                'qty': qty,
            }]
            return result
        result = []
        for relation in product.set_component_ids:
            component = relation.cmp_product_id
            comp_id = component.id
            comp_qty = relation.qty
            comp_size = size if component.categ_id.sizeable else default_size
            result.append({
                'default_code': component.default_code,
                'product_id': comp_id,
                'size': comp_size,
                'qty': qty * relation.qty,
            })
        return result

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(product_product, self).default_get(cr, uid, fields, context=context)
        if 'creation_date' in fields:
            res.update(creation_date=datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))
        return res

    def init(self, cr):
        cr.execute("""

CREATE OR REPLACE FUNCTION update_product_qty() RETURNS TRIGGER AS $update_product_qty$

    DECLARE
    location_ids INTEGER ARRAY;
    incoming_cnt FLOAT;
    outgoing_cnt FLOAT;

    result_cnt FLOAT;

    BEGIN

        if  (   TG_OP = 'UPDATE' and
                (
                    (NEW.state = OLD.state and NEW.product_qty = OLD.product_qty) or
                    (OLD.state = 'assigned' and NEW.state = 'done') or
                    (OLD.state = 'reserved' and NEW.state = 'done')
                )
            ) then
            return NEW;
        else
            location_ids := ARRAY(
                SELECT id
                    FROM stock_location
                    WHERE active = True AND usage = 'internal'
                        AND chained_picking_type is null
                        AND chained_location_type = 'none'

                EXCEPT

                SELECT lot_stock_id
                    FROM stock_warehouse
                    WHERE virtual = true

            );

            incoming_cnt := (select sum(product_qty)
                    from stock_move
                    where
                        location_id <> ANY (location_ids)
                        and location_dest_id = ANY(location_ids)
                        and product_id IN (NEW.product_id)
                        and state IN ('confirmed', 'done', 'reserved')
                    group by product_id,product_uom);

            outgoing_cnt := (select sum(product_qty)
                    from stock_move
                    where
                        location_id = ANY (location_ids)
                        and location_dest_id <> ANY(location_ids)
                        and product_id IN (NEW.product_id)
                        and state IN ('confirmed', 'done', 'reserved')
                    group by product_id,product_uom);

            result_cnt := coalesce(incoming_cnt,0) - coalesce(outgoing_cnt, 0);

            UPDATE product_product
            SET total_qty = result_cnt
            WHERE id = NEW.product_id;

            return NEW;
        end if;
    END;
$update_product_qty$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_product_qty_on_del() RETURNS TRIGGER AS $update_product_qty$

    DECLARE
    location_ids INTEGER ARRAY;
    incoming_cnt FLOAT;
    outgoing_cnt FLOAT;

    result_cnt FLOAT;

    BEGIN
        location_ids := ARRAY(
            SELECT id
                from stock_location
                where active = True and usage = 'internal'
                    and chained_picking_type is null
                    and chained_location_type = 'none'

            EXCEPT

            SELECT lot_stock_id
                FROM stock_warehouse
                WHERE virtual = true

        );

        incoming_cnt := (select sum(product_qty)
                from stock_move
                where
                    location_id <> ANY (location_ids)
                    and location_dest_id = ANY(location_ids)
                    and product_id IN (OLD.product_id)
                    and state IN ('confirmed', 'done', 'reserved')
                group by product_id,product_uom);

        outgoing_cnt := (select sum(product_qty)
                from stock_move
                where
                    location_id = ANY (location_ids)
                    and location_dest_id <> ANY(location_ids)
                    and product_id IN (OLD.product_id)
                    and state IN ('confirmed', 'done', 'reserved')
                group by product_id,product_uom);

        result_cnt := coalesce(incoming_cnt,0) - coalesce(outgoing_cnt, 0);

        UPDATE product_product
        SET total_qty = result_cnt
        WHERE id = OLD.product_id;

        return OLD;

    END;
$update_product_qty$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS check_update ON stock_move;
CREATE TRIGGER check_update
    AFTER INSERT OR UPDATE ON stock_move
    FOR EACH ROW
    EXECUTE PROCEDURE update_product_qty();

DROP TRIGGER IF EXISTS check_update_on_del ON stock_move;
CREATE TRIGGER check_update_on_del
    AFTER DELETE ON stock_move
    FOR EACH ROW
    EXECUTE PROCEDURE update_product_qty_on_del();


CREATE OR REPLACE FUNCTION reset_product_qty() RETURNS TRIGGER AS $reset_product_qty$
    BEGIN
    UPDATE product_product SET total_qty = 0;
    RETURN NULL;
    END;
$reset_product_qty$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS check_truncate ON stock_move;
CREATE TRIGGER check_truncate
    AFTER TRUNCATE ON stock_move
    EXECUTE PROCEDURE reset_product_qty();

        """)

        cr.execute("""
CREATE OR REPLACE FUNCTION create_mssql_tables() RETURNS VOID  AS '
    DECLARE
        is_exists FLOAT;
    BEGIN
        is_exists := (SELECT count(*) FROM pg_class WHERE relname = ''mssql_prg_sets'');
        IF is_exists = 0
        THEN
            CREATE TABLE public.mssql_prg_sets (
                id integer PRIMARY KEY,
                id_delmar varchar,
                createdate varchar
            );
            CREATE INDEX prg_sets_id_delmar ON public.mssql_prg_sets (id_delmar);
        END IF;
    END;
' LANGUAGE plpgsql;

select "create_mssql_tables"();

        """)

    def prepare_data(self, cr, uid, vals):
        if not vals:
            return vals

        fields = ['default_code', 'web_id', 'id_style', 'id_delmar']
        for field in fields:
            if vals.get(field):
                vals[field] = clean_code(vals[field]).upper()

        return vals

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if context.get('uid'):
            vals.update({'created_from_erp_screen': True})
        vals = self.prepare_data(cr, uid, vals)
        res = super(product_product, self).create(cr, uid, vals, context)
        self.fsku_check_import(cr, uid, [res], context)
        # DLMR-1499
        if uid == 1:
            # self.update_product_short_description(cr, uid, product_id=res)
            self.write(cr, uid, res, {'short_description': vals.get('default_code', '')})
        # END DLMR-1499
        return res

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}
        vals = self.prepare_data(cr, uid, vals)
        res = None
        # DLMR-1499
        if uid == 1:
            if type(ids) == int or type(ids) == long:
                ids = [ids]
            for product_id in ids:
                product = self.browse(cr, uid, product_id)
                update_description = product and product.update_description or False
                if 'update_description' in vals:
                    update_description = vals['update_description']
                if not update_description:
                    short_description = self.generate_product_short_description(cr, uid, product_id=product_id)
                    if short_description:
                        vals.update({'short_description': short_description})
                    # DLMR-1788
                    custom_description = self.generate_product_custom_description(cr, uid, product_id=product_id)
                    if custom_description:
                        vals.update({'custom_description': custom_description})
                    # END DLMR-1788
                res = super(product_product, self).write(cr, uid, product_id, vals, context)
        # END DLMR-1499
        else:
            res = super(product_product, self).write(cr, uid, ids, vals, context)
        self.fsku_check_import(cr, uid, ids, context)
        return res

    # DLMR-1499
    # Just for fun, or for xml-rpc maybe
    def update_product_short_description(self, cr, uid, product_id=None, default_code=None):
        if default_code and not product_id:
            prod_ids = self.search(cr, uid, ['&', ('default_code', '=', default_code), ('type', '=', 'product')])
            product_id = prod_ids and len(prod_ids) > 0 and prod_ids[0] or None
        short_description = self.generate_product_short_description(cr, uid, product_id=product_id)
        if short_description:
            self.write(cr, uid, product_id, {'short_description': short_description})
            return True
        return False

    # DLMR-1788
    # Returns product custom description
    def generate_product_custom_description(self, cr, uid, product_id=None, default_code=None):
        if default_code and not product_id:
            prod_ids = self.search(cr, uid, ['&', ('default_code', '=', default_code), ('type', '=', 'product')])
            product_id = prod_ids and len(prod_ids) > 0 and prod_ids[0] or None
        if product_id:
            try:
                query = """select
                      (case when coalesce(cmp.prod_metal,pt.prod_metal) ~ '^(\d{2}[kK])|(Silver)|([a-zA-Z])$' then initcap(coalesce(cmp.prod_metal,pt.prod_metal))||' ' else '' end)
                      ||pc.name
                      ||(case when cmp.diamond_qty>0 or cmp.pearl_qty>0 or gms.gemstone is not null then ' w/' else '' end)
                      ||(case when cmp.diamond_qty>0 then 'Diamonds ' else '' end)
                      ||(case when gms.gemstone is null and cmp.pearl_qty>0 and cmp.diamond_qty>0 then '& ' else '' end)
                      ||(case when cmp.pearl_qty>0 then 'Pearls ' else '' end)
                      ||(case when gms.gemstone is not null and (cmp.pearl_qty>0 or cmp.diamond_qty>0) then '& ' else '' end)
                      ||(case when gms.gemstone is not null then gms.gemstone else '' end)
                       as new_desc,
                      pp.custom_description as old_desc
                    from product_product pp
                        left join product_template pt on pt.id = pp.product_tmpl_id and pt.prod_metal is not null
                        left join product_category pc on pc.id=pt.categ_id
                        left join (
                          select
                            ppl.parent_product_id,
                            sum(case when pt.categ_id in(11) then 1 else 0 end) as diamond_qty,
                            sum(case when pt.categ_id in (16) then 1 else 0 end) as pearl_qty,
                            string_agg(distinct(case when pt.categ_id in (14) then replace(ma.name,' Gold','') end ),' ')  as prod_metal
                          from product_pack_line ppl
                          left join product_product pp on pp.id=ppl.product_id
                          left join product_template pt on pp.product_tmpl_id=pt.id
                          left join metal_alloy ma on ma.id=pt.metal_alloy_id
                          group by ppl.parent_product_id
                        ) cmp on cmp.parent_product_id=pp.id
                        left join (
                          SELECT ppl.parent_product_id, string_agg(DISTINCT(CASE WHEN pt.prod_sub_type IS NULL
                            THEN gt.name
                                  ELSE pt.prod_sub_type END), ' ') AS gemstone
                          FROM product_pack_line ppl
                            INNER JOIN product_product pp ON pp.id = ppl.product_id
                            INNER JOIN product_template pt ON pt.id = pp.product_tmpl_id
                            LEFT JOIN gemstone_type gt ON gt.id = pt.gemst_type_id
                          WHERE pt.categ_id = 13
                          group by ppl.parent_product_id
                        ) gms on gms.parent_product_id=pp.id
                    where 1=1
                        and (pt.type = 'product' or pc.name='Finding')
                        and pc.parent_id<>9
                        and (pt.prod_metal is not null or cmp.prod_metal is not null)
                         and pp.id=%d
                    order by pp.id desc""" % product_id
                cr.execute(query)
                line = cr.fetchone()
                if line and len(line) > 0 and len(line[0]) > 0:
                    return line[0]
            except Exception, e:
                logger.error("Cannot generate product custom (US) description: {}".format(e.message))
            pass
        return None
    # END DLMR-1788

    # DLMR-1499
    # Returns product short description based on DLMR-1499
    def generate_product_short_description(self, cr, uid, product_id=None, default_code=None):
        if default_code and not product_id:
            prod_ids = self.search(cr, uid, ['&', ('default_code', '=', default_code), ('type', '=', 'product')])
            product_id = prod_ids and len(prod_ids) > 0 and prod_ids[0] or None
        if product_id:
            try:
                query = """select
                      (case when coalesce(cmp.prod_metal,pt.prod_metal) ~ '^(\d{2}[kK])|(Silver)|([a-zA-Z])$' then initcap(coalesce(cmp.prod_metal,pt.prod_metal))||' ' else '' end)
                      ||pc.name
                      ||(case when cmp.diamond_qty>0 or cmp.pearl_qty>0 or cmp.gemstone_qty>0 then ' w/' else '' end)
                      ||(case when cmp.diamond_qty>0 then 'Diamonds ' else '' end)
                      ||(case when cmp.gemstone_qty=0 and cmp.pearl_qty>0 and cmp.diamond_qty>0 then '& ' else '' end)
                      ||(case when cmp.pearl_qty>0 then 'Pearls ' else '' end)
                      ||(case when cmp.gemstone_qty>0 and (cmp.pearl_qty>0 or cmp.diamond_qty>0) then '& ' else '' end)
                      ||(case when cmp.gemstone_qty>0 then 'Gemstones ' else '' end)
                       as new_desc,
                      pp.short_description as old_desc
                    from product_product pp
                        left join product_template pt on pt.id = pp.product_tmpl_id and pt.prod_metal is not null
                        left join product_category pc on pc.id=pt.categ_id
                        left join (
                          select
                            ppl.parent_product_id,
                            sum(case when pt.categ_id in(11) then 1 else 0 end) as diamond_qty,
                            sum(case when pt.categ_id in(13) then 1 else 0 end) as gemstone_qty,
                            sum(case when pt.categ_id in (16) then 1 else 0 end) as pearl_qty,
                            string_agg(distinct(case when pt.categ_id in (14) then replace(ma.name,' Gold','') end ),' ')  as prod_metal
                          from product_pack_line ppl
                          left join product_product pp on pp.id=ppl.product_id
                          left join product_template pt on pp.product_tmpl_id=pt.id
                          left join metal_alloy ma on ma.id=pt.metal_alloy_id
                          group by ppl.parent_product_id
                        ) cmp on cmp.parent_product_id=pp.id
                    where 1=1
                        and (pt.type = 'product' or pc.name='Finding')
                        and pc.parent_id<>9
                        and (pt.prod_metal is not null or cmp.prod_metal is not null)
                        and pp.id=%d
                    order by pp.id desc""" % product_id
                cr.execute(query)
                line = cr.fetchone()
                if line and len(line) > 0 and len(line[0]) > 0:
                    return line[0]
            except Exception, e:
                logger.error("Cannot generate product short (CA) description: {}".format(e.message))
                pass
        return None
    # END DLMR-1499

    def get_gold_stamp(self, string):

        if not string:
            return None
        if not isinstance(string, (str, unicode)):
            return None

        regexp = re.compile(r"\b(?P<stamp>[\d]+)k[wpy]?\b", re.IGNORECASE)
        for match in regexp.finditer(string):
            return match.groupdict().get('stamp')
        return None

    def fsku_check_import(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        is_import_active_str = self.pool.get('ir.config_parameter').get_param(
            cr, uid, 'import.product.fsku', '')

        product = self.browse(cr, uid, ids[0])
        if (not context.get('uid') or
                not product.created_from_erp_screen or
                product.type != 'product' or
                is_import_active_str.strip().lower() != 'true'):
            return

        self.fsku_import(cr, uid, ids)

    def fsku_import(self, cr, uid, ids):
        assert len(ids) == 1

        res = False

        db_server = self.pool.get('fetchdb.server')
        server_id = db_server.get_sale_servers(cr, uid, single=True)

        ms_conn = db_server.connect(cr, uid, server_id)

        if not ms_conn:
            return res

        ms_cr = ms_conn.cursor()

        product = self.browse(cr, uid, ids[0])

        try:
            res = self.fsku_import_transaction(cr, uid, ms_conn, ms_cr, db_server, server_id, product)
            if not res:
                raise Exception('Product {} wasn\'t imported into fsku tables'.format(
                    product.default_code or product.name))
        except Exception as e:
            logger.error(e)
            ms_conn.rollback()
        else:
            ms_conn.commit()
        finally:
            ms_conn.close()

        return res

    def fsku_import_transaction(self, cr, uid, ms_conn, ms_cr, db_server, server_id, product):
        execute = partial(
            db_server.make_query,
            cr,
            uid,
            server_id,
            select=False,
            commit=False,
            connection=ms_conn,
            cursor=ms_cr)

        if not product.default_code:
            return False

        res = execute("""
        DELETE FROM fskuAttributes
        WHERE id_delmar = ?
        """, [product.default_code])

        if not res:
            return False

        id_delmar_family_list = [pack_line.product_id.default_code
                                 for pack_line in product.pack_line_ids]

        if id_delmar_family_list:
            res = execute(
                """
                DELETE FROM fskuComponents
                WHERE id_delmar = ? AND id_delmar_family IN ({})
                """.format(', '.join(['?'] * len(id_delmar_family_list))),
                [product.default_code] + id_delmar_family_list)

            if not res:
                return False

        attributes = [
            product.default_code,
            product.short_description,
            product.custom_description,
            product.pearl_grade,
            product.prod_active,
            product.prod_color,
            product.duty_price,
            product.shipping_price,
            product.prod_cut_shape,
            product.name,
            product.prod_diam_mm,
            product.prod_finish,
            product.prod_manufacture,
            product.prod_metal,
            product.prod_tarif_classification,
            product.prod_type_category,
            product.prod_type_extended,
            product.categ_id.name,
            product.image_url,
            product.supplier_sku,
            product.search_str,
            product.supplier_1,
            product.supplier_2,
            product.length,
            product.height,
            product.depth,
            product.collection,
            1,
            'ERP',
            '%.2f' % (product.standard_price) if product.standard_price else '',
        ]
        attributes = [None if attribute is False else attribute for attribute in attributes]

        res = execute(
            """
            SET ANSI_WARNINGS  OFF;
            INSERT INTO fskuAttributes (
                prod_export_time,
                id_delmar,
                label_description_small,
                num_of_ring_size,
                pearlGrade,
                prod_active,
                prod_color,
                prod_cost_duty,
                prod_cost_shipping,
                prod_cut_shape,
                prod_description_line,
                prod_diameter_mm,
                prod_finish,
                prod_manufacture,
                prod_metal,
                prod_tarif_classification,
                prod_type_category,
                prod_type_extended,
                prod_type,
                image_url,
                supplier_sku,
                search_str,
                supplier_1,
                supplier_2,
                length,
                height,
                depth,
                collection,
                from_erp,
                source_system,
                price_cost
            ) VALUES (CONVERT(VARCHAR, GETDATE(), 120), {});
            """.format(', '.join(['?'] * len(attributes))),
            attributes)

        if not res:
            return False

        components = map(partial(self.build_fsku_components_line, cr, uid),
                         product.pack_line_ids)

        if components:
            res = execute(
                """
                SET ANSI_WARNINGS  OFF;
                INSERT INTO fskuComponents (
                    prod_export_time,
                    dia_clarity,
                    dia_color,
                    duty_mark,
                    id_delmar,
                    id_delmar_family,
                    igi_information,
                    price_cost_a,
                    price_cost_us,
                    price_cost_visuallised_a,
                    price_srp_ca,
                    prod_cut_shape,
                    prod_description_line,
                    prod_diam_mm,
                    prod_duty_rate,
                    prod_qty,
                    prod_quality,
                    prod_type,
                    shipping,
                    tdw,
                    prod_currency_calc_graphic,
                    price_cost_showas,
                    price_total_a,
                    prod_color,
                    prod_weight,
                    plating,
                    from_erp
                ) VALUES {}
                """.format(', '.join([
                    '(CONVERT(VARCHAR, GETDATE(), 120), {})'.format(', '.join(['?'] * len(component)))
                    for component in components
                ])),
                sum(components, []))

            if not res:
                return False

        return True

    def build_fsku_components_line(self, cr, uid, pack_line):
        component = pack_line.product_id
        component_type = component.categ_id.name.lower()

        cut_shape = None
        quality = None
        tdw = None
        if component_type == 'diamond':
            cut_shape = component.dmd_shape_id.name
            tdw = pack_line.total_weight
        elif component_type == 'gemstone':
            cut_shape = component.gemst_shape_id.name
            quality = component.gemst_quality
        elif component_type == 'pearl':
            cut_shape = component.pearl_shape_id.name
            quality = component.pearl_quality

        line = [
            component.dmd_clarity_id.name if component_type == 'diamond' else None,
            component.dmd_color_id.name if component_type == 'diamond' else None,
            component.duty_mark,
            pack_line.parent_product_id.default_code,
            component.default_code,
            component.igi_information,
            pack_line.cost,
            component.price_cost_us,
            component.price_cost_visualised_a,
            component.price_srp_ca,
            cut_shape,
            component.name,
            component.prod_diam_mm,
            component.prod_duty_rate,
            pack_line.quantity,
            quality,
            component_type,
            component.shipping_mark,
            tdw,
            component.prod_currency_calc_graphic,
            component.price_cost_showas,
            component.price_total_a,
            component.prod_color,
            pack_line.weight,
            component.prod_metal_plating,
            1,
        ]
        return [None if column is False else column for column in line]

    def on_change_categ_id(self, cr, uid, id, categ_id, context=None):
        if not context:
            context = {}
        result = {
            'prod_type': 'undefined',
        }

        if categ_id:
            categ_obj = self.pool.get('product.category')
            result.update({'categ_name': categ_obj.read(cr, uid, categ_id, context=context)['name']})
            prod_type = categ_obj.get_prod_type(cr, uid, categ_id)
            if prod_type == 'product':
                result.update({'type': 'product'})
            else:
                result.update({'type': 'consu'})
            result.update({
                'prod_type': prod_type,
                'ring_size_ids': [],
            })

        return {'value': result}

    def search_args_by_several_ids(self, cr, user, args, context=None):
        if not context:
            context = {}
        try:
            default_code_domain = (arg for arg in args if ((isinstance(arg, list) or isinstance(arg, tuple)) and arg[0] == 'default_code')).next()
            ids_list = default_code_domain[2].replace('\n', ' ').replace('\t', ' ').replace(',', ' ')

            ids_list = [x for x in ids_list.split(' ') if x]
            first_term_index = args.index(default_code_domain)
            new_args = args[:first_term_index] + ['|', '|', '|', ['id_delmar', 'in', ids_list], ['web_id', 'in', ids_list], ['default_code', 'in', ids_list]] + args[first_term_index:]
            return new_args
        except:
            return args

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not context:
            context = {}

        new_args = self.search_args_by_several_ids(cr, user, args, context=context)
        if context.get('not0', False):
            new_args.append(('total_qty', '>', 0))
        if context.get('filter_dnr', False):
            new_args.append(('dnr', '=', True))

        return super(product_product, self).search(cr, user, new_args, offset=offset, limit=limit, order=order, context=context, count=count)

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
        if args is None:
            args = []

        if context is None:
            context = {}

        names = []
        mode = context.get('mode', 'common')
        custome_search = context.get('custome_search', True)
        skip_regular_search = context.get('skip_regular_search', False)
        skip_size = context.get('skip_size', False)

        if mode == 'common' and not skip_regular_search:
            logger.info("Common search '%s'", name)

            search_args = [('name', operator, name)]
            addition_params = ['default_code', 'id_delmar', 'web_id']
            custom_params = ['default_code', 'upc', 'os_product_sku']

            extend_search = custome_search and len(name) > 2

            if extend_search and addition_params:
                search_args = ['|'] * len(addition_params) + search_args
                for param in addition_params:
                    search_args.append((param, operator, name))
            ids = super(product_product, self).search(cr, uid, args + search_args, limit=limit)

            if custome_search and custom_params:
                cr.execute("""
                    SELECT val.product_id
                    FROM
                        product_multi_customer_fields val
                        LEFT JOIN product_multi_customer_fields_name f on (val.field_name_id = f.id)
                        LEFT JOIN product_multi_customer_names n on (n.id = f.name_id)
                    WHERE n.name in %s and (val.value = %s)
                """, (tuple(custom_params), name, )
                )

                res = cr.fetchall()
                if res:
                    for p in res:
                        ids += p

            if ids and len(ids) > 1:
                ids = list(set(ids))

            names = self.name_get(cr, uid, ids, context=context)

        elif mode in ['simple', 'default_code']:
            logger.info("Simple search '%s'", name)
            product_ids = []

            limit_str = ''
            if limit:
                limit_str = 'LIMIT %s' % (limit)

            def get_ids(cr, sql, search_string, operator):

                if operator in ['like', 'ilike']:
                    search_string = '%' + search_string + '%'

                cr.execute(sql, {'search_str': search_string})
                res = cr.fetchall()
                if res:
                    return [x[0] for x in res]
                else:
                    return []

            if mode == 'simple':

                search_string = name
                partner_id = context.get('partner_id', False)
                partner_where = ""
                if partner_id:
                    partner_where = " AND (partner_id = %s OR partner_id IS NULL) " % (partner_id)

                sql = """   SELECT DISTINCT product_id
                            FROM (
                                SELECT product_id, priority
                                FROM product_code
                                WHERE 1=1
                                    AND name %s %s
                                    %s
                                ORDER BY priority DESC
                            )a
                            %s;
                """ % (operator, '%(search_str)s', partner_where, limit_str)
                product_ids = get_ids(cr, sql, name, operator)

            else:
                search = [name]
                if skip_size and '/' in name:
                    without_size = re.sub(r'(.*)(\/[\d\.]+$)', '\\1', name)
                    if without_size:
                        search.append(without_size)

                sql = """   SELECT id
                            FROM product_product
                            WHERE 1=1
                                AND type = 'product'
                                AND default_code %s %s
                            %s;
                """ % (operator, '%(search_str)s', limit_str)

                for search_string in search:
                    product_ids = get_ids(cr, sql, search_string, operator)
                    if product_ids:
                        break

            if product_ids:
                names = self.name_get(cr, uid, self.exists(cr, uid, product_ids), context=context)

            if not names:
                new_context = context.copy()
                new_context.update({'mode': 'common'})
                names = self.name_search(cr, uid, name=name, args=args, operator=operator, context=new_context, limit=limit)

        return names

    def add_non_exist_size_to_product(self, cr, uid, size_id, product_id):
        if product_id and size_id:
            product_obj = self.browse(cr, uid, product_id)
            product_sizes = [x.id for x in product_obj.ring_size_ids]
            if size_id not in product_sizes:
                product_tmpl_id = product_obj.product_tmpl_id and product_obj.product_tmpl_id.id
                update_sql = """INSERT INTO product_ring_size_default_rel
                    (product_id, size_id)
                    VALUES
                    (%s, %s)
                """ % (product_tmpl_id, size_id)
                cr.execute(update_sql)

                return True

        return False


    def get_total_weight(self, cr, uid, product_id=False, context=None):
        component_type = context.get('component_type','')
        if not product_id:
            return 0.00
        check_sql = """
            select case when exists (select parent_product_id from product_set_components where parent_product_id={product_id})  then 
(
select
      sum(case 
        when pc.name ilike '%%metal%%' then pl.quantity 
        else coalesce(pl.weight ,cht.weight ,0) * coalesce(pl.quantity, 0)
      end)as "total"
    from product_product p
    left join product_set_components psc on psc.cmp_product_id=p.id
    left join product_template t on t.id = p.product_tmpl_id
    left join product_pack_line pl on pl.parent_product_id = p.id
    left join product_product chp on chp.id = pl.product_id
    left join product_template cht on cht.id = chp.product_tmpl_id
    left join product_category pc on pc.id = cht.categ_id
    where 1=1
      and psc.parent_product_id = {product_id} 
      and pc.name ilike '%%{component_type}%%'
      and pc.name not ilike '%%labor%%'
)
 else(     
      select
      sum(case 
        when pc.name ilike '%%metal%%' then pl.quantity 
        else coalesce(pl.weight ,cht.weight ,0) * coalesce(pl.quantity, 0)
      end)as "total"
    from product_product p
    left join product_template t on t.id = p.product_tmpl_id
    left join product_pack_line pl on pl.parent_product_id = p.id
    left join product_product chp on chp.id = pl.product_id
    left join product_template cht on cht.id = chp.product_tmpl_id
    left join product_category pc on pc.id = cht.categ_id
    where 1=1
      and p.id = {product_id} 
      and pc.name ilike '%%{component_type}%%'
      and pc.name not ilike '%%labor%%'
) end
          """.format(product_id=product_id, component_type=component_type)

        cr.execute(check_sql)
        res = cr.dictfetchone()
        result = round(float(res['total']), 2) if res['total'] else 0.00
        return result

product_product()


class product_code(osv.osv):
    _name = 'product.code'

    _columns = {
        'name': fields.char('Value', type='char', size=256, required=True),
        'priority': fields.integer('Priority'),
        'product_id': fields.many2one('product.product', 'Product', required=True, ondelete='cascade'),
        'partner_id': fields.many2one('res.partner', 'Customer', ondelete='cascade'),
        'size': fields.float('Size'),
    }

    def _sync_product_code(self, cr, uid, context=None):

        print "\n\nSync product codes"

        sql = " TRUNCATE TABLE product_code RESTART IDENTITY; "
        cr.execute(sql)
        # Suspect that RESTART IDENTITY reset sequence value to 0
        # Set initial sequence value 1 instead of 0
        # Can be redundant
        sql = "ALTER SEQUENCE product_code_id_seq START 1;"
        cr.execute(sql)

        conf_obj = self.pool.get('ir.config_parameter')

        product_product_fields = []
        pp_settings_obj = conf_obj.get_param(cr, uid, 'search.product.product.fields')
        if pp_settings_obj:
            try:
                product_product_fields = eval(pp_settings_obj)
            except:
                pass

        # standard fields
        for pp_field in product_product_fields:
            if not pp_field.get('name', False):
                continue

            sql = """   INSERT INTO product_code ("product_id", "name", "priority")
                        SELECT
                            pp.id as product_id,
                            pp.%s as name,
                            %s as priority
                        FROM product_product pp
                        WHERE 1=1
                            AND pp.%s IS NOT NULL
                            AND pp.type = 'product';
            """ % (pp_field['name'], pp_field.get('priority', 0), pp_field['name'])

            try:
                cr.execute(sql)
            except:
                pass

        product_template_fields = []
        pt_settings_obj = conf_obj.get_param(cr, uid, 'search.product.template.fields')
        if pt_settings_obj:
            try:
                product_template_fields = eval(pt_settings_obj)
            except:
                pass

        for pt_field in product_template_fields:
            if not pt_field.get('name', False):
                continue

            sql = """   INSERT INTO product_code ("product_id", "name", "priority")
                        SELECT
                            pp.id as product_id,
                            pt.%s as name,
                            %s as priority
                        FROM product_product pp
                            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
                        WHERE 1=1
                            AND pt.%s IS NOT NULL
                            AND pp.type = 'product';
            """ % (pt_field['name'], pt_field.get('priority', 0), pt_field['name'])
            try:
                cr.execute(sql)
            except:
                pass

        product_custom_fields = []
        pc_settings_obj = conf_obj.get_param(cr, uid, 'search.product.custom.fields')
        if pc_settings_obj:
            try:
                product_custom_fields = eval(pc_settings_obj)
            except:
                pass

        for pc_field in product_custom_fields:
            if not pc_field.get('name', False):
                continue

            sql = """   INSERT INTO product_code ("product_id", "name", "partner_id", "priority", "size")
                        SELECT
                            val.product_id as product_id,
                            val.value as name,
                            val.partner_id as partner_id,
                            %s as priority,
                            rs.name::NUMERIC AS size
                        FROM
                            product_multi_customer_fields val
                            LEFT JOIN product_multi_customer_fields_name f on (val.field_name_id = f.id)
                            LEFT JOIN product_multi_customer_names n on (n.id = f.name_id)
                            LEFT JOIN product_product pp on (pp.id = val.product_id)
                            LEFT JOIN ring_size rs ON (val.size_id = rs.id)
                        WHERE 1=1
                            AND n.name = '%s'
                            AND val.value IS NOT NULL
                            AND pp.type = 'product';
            """ % (pc_field.get('priority', 0), pc_field['name'])
            try:
                cr.execute(sql)
            except:
                pass

        return True


product_code()


class product_instruction_notes(osv.osv):
    _name = 'product.instruction.notes'

    _columns = {
        'name': fields.text('Note', required=True),
        'order': fields.integer('Order column'),
    }

    _sql_constraints = [('name_unique', 'unique(name)', 'Instruction note already exists')]

product_instruction_notes()
