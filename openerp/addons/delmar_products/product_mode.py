from osv import osv, fields


class product_mode_name(osv.osv):
    _name = 'product.mode.name'

    _columns = {
        'name': fields.char('Name', type='char', size=256, required=True),
        'type': fields.selection(
            [('on_off', 'On/Off'), ('date_range', 'Date range')],
            'Type',
            required=True)
    }

    _sql_constraints = [
        (
            'name_uniq',
            'UNIQUE(name)',
            'Mode name must be unique!'
        ),
    ]

product_mode_name()


class product_mode(osv.osv):
    _name = 'product.mode'
    _rec_name = 'mode_name_id'

    _columns = {
        'create_uid': fields.many2one(
            'res.users', 'Created By', readonly=True),
        'create_date': fields.datetime('Creation Date', readonly=True),
        'product_id': fields.many2one(
            'product.product', 'Product', required=True, ondelete='cascade', ),
        'mode_name_id': fields.many2one(
            'product.mode.name', 'Mode', required=True),
        'is_active': fields.boolean('Active'),
        'start_date': fields.datetime('Start date'),
        'end_date': fields.datetime('End date'),
        'partner_id': fields.many2one(
            'res.partner', 'Customer'),
    }

product_mode()
