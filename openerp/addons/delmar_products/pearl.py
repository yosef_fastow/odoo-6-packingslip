from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class pearl_type(osv.osv):
    _name = "pearl.type"
    _description = 'Pearl type'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the pearl type must be unique !')
    ]

pearl_type()


class pearl_surface(osv.osv):
    _name = "pearl.surface"
    _description = 'Pearl Surface'

    _columns = {
        'name' : fields.char('Surface', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The surface of the pearl must be unique !')
    ]
pearl_surface()


class pearl_shape(osv.osv):
    _name = "pearl.shape"
    _description = 'Pearl shape'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the shape must be unique !')
    ]

pearl_shape()


class pearl_color(osv.osv):
    _name = "pearl.color"
    _description = 'Pearl Color'

    _columns = {
        'name' : fields.char('Color', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The color of the pearl must be unique !')
    ]
pearl_color()


class pearl_surface(osv.osv):
    _name = "pearl.surface"
    _description = 'Pearl surface'

    _columns = {
        'name' : fields.char('Name', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The name of the pearl surface must be unique !')
    ]

pearl_surface()



class pearl_size_single(osv.osv):
    _name = "pearl.size_single"
    _description = 'Pearl Size_Single'

    _columns = {
        'name' : fields.char('Size', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The size_single of the pearl must be unique !')
    ]
pearl_size_single()



class pearl_size_strand(osv.osv):
    _name = "pearl.size_strand"
    _description = 'Pearl Size_Strand'

    _columns = {
        'name' : fields.char('Size_Strand', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The size_strand of the pearl must be unique !')
    ]
pearl_size_strand()



class pearl_attachment(osv.osv):
    _name = "pearl.attachment"
    _description = 'Pearl Attachment'

    _columns = {
        'name' : fields.char('Attachment', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The attachment of the pearl must be unique !')
    ]
pearl_attachment()

