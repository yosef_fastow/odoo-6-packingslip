from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)



class plating_labour_metal(osv.osv):
    _name = "plating_labour.metal"
    _description = 'Plating_Labour Metal'

    _columns = {
        'name' : fields.char('Metal', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The metal of the plating_labour must be unique !')
    ]
plating_labour_metal()


class plating_labour_plating_type(osv.osv):
    _name = "plating_labour.plating_type"
    _description = 'Plating_Labour Plating_Type'

    _columns = {
        'name' : fields.char('Plating_Type', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The plating_type of the plating_labour must be unique !')
    ]
plating_labour_plating_type()
