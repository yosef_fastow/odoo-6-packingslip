from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)


class gemstone_shape(osv.osv):
    _name = "gemstone.shape"
    _description = 'Gemstone Shape'

    _columns = {
        'name': fields.char('Shape', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The shape of the gemstone must be unique !')
    ]
gemstone_shape()


class gemstone_type(osv.osv):
    _name = "gemstone.type"
    _description = 'Gemstone Type'

    _columns = {
        'name': fields.char('Type', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The type of the gemstone must be unique !')
    ]
gemstone_type()


class gemstone(osv.osv):
    _name = "gemstone"
    _description = 'Gemstone'

    _columns = {
        'name': fields.char('Name', size=256, required=True),
        'short_name': fields.char('Name', size=256, required=False),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (trim(lower(name)))',
            'The name of the gemstone must be unique !')
    ]
gemstone()
