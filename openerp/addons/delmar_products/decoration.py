from osv import osv, fields

import logging
_logger = logging.getLogger(__name__)

class decoration_setting(osv.osv):
    _name = "decoration.setting"
    _description = 'Decoration Setting'

    _columns = {
        'name' : fields.char('Setting', size=256, required=True),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
            'The setting of the decoration must be unique !')
    ]
decoration_setting()