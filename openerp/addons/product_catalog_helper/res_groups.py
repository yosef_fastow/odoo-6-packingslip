# -*- coding: utf-8 -*-
from osv import osv
from catalog import catalog_job
from redis import ConnectionError


class res_groups(osv.osv):
    _inherit = "res.groups"

    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_groups, self).write(cr, uid, ids, vals, context=context)

        if res:
            try:
                job_obj = catalog_job.Job(cr, uid, self.pool)
                job_obj.send('res.groups', {'ids': ids, 'type': 'groups'})
            except ConnectionError:
                pass
        return res

res_groups()
