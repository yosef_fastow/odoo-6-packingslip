# -*- coding: utf-8 -*-

from osv import fields, osv


class res_partner(osv.osv):
    _inherit = 'res.partner'

    def find_by_ref(self, cr, uid, ref, context=None):
        cr.execute("SELECT id FROM res_partner WHERE ref = %s", (ref,))
        res = cr.fetchall()
        return res and res[0] or []

res_partner()