# -*- coding: utf-8 -*-

from osv import fields, osv
from catalog import catalog_job


class res_users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'not_send_email': fields.boolean('Not Send Email')
    }

    def create(self, cr, uid, vals, context=None):
        res = super(res_users, self).create(cr, uid, vals, context=context)

        if res:
            job_obj = catalog_job.Job(cr, uid, self.pool)
            job_obj.send('res.users', {'uid': res, 'type': 'users'})

        return res

    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_users, self).write(cr, uid, ids, vals, context=context)
        if vals.get('password', False) or vals.get('new_password', False):
            self.update_user_on_pc(cr, uid, ids, context)

        return res

    def update_user_on_pc(self, cr, uid, ids, context=None):
        job_obj = catalog_job.Job(cr, uid, self.pool)
        job_obj.send('res.users', {'ids': ids, 'type': 'users'})


res_users()
