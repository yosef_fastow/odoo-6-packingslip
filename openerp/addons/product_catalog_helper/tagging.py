# -*- coding: utf-8 -*-

from osv import osv
from catalog import catalog_job
from datetime import datetime


class taggings_product(osv.osv):
    _inherit = "tagging.tags"
    _name = _inherit

    def product_catalog_export(self, cr, uid, ids, context=None):
        tag_obj = self.read(cr, uid, ids[0])

        job_obj = catalog_job.Job(cr, uid, self.pool)

        user = self.pool.get('res.users').read(cr, uid, uid, ['name'])

        tag = {
            'name': tag_obj['name'],
            'note': tag_obj['notes'] or '',
            'description': tag_obj['description'] or '',
            'products': tag_obj['product_ids'],
            'create_date': datetime.now().date().strftime("%Y-%m-%d %H:%M:%S"),
            'create_user': user['name']
        }

        job_obj.send('tagging.tags', tag)

        return True

    def unlink(self, cr, uid, ids, context=None):
        tag_names = self.name_get(cr, uid, ids) or []
        super(taggings_product, self).unlink(cr, uid, ids, context)

        if tag_names:
            job_obj = catalog_job.Job(cr, uid, self.pool)
            for tag_name in tag_names:
                tag = {
                    'name': tag_name[1],
                    'action': 'delete',
                }
                job_obj.send('tagging.tags', tag)

        return True
