# -*- coding: utf-8 -*-
import redis
import json
import random
import string


def randomstring(length):
    letters = string.lowercase + string.ascii_uppercase + string.digits
    return ''.join(random.choice(letters) for i in range(length))
"""
{"host": "192.168.99.81",
 "port": 6379,
 "default_qname": "queues:prod",
 "type_job_name": {
    "tagging.tags": {
        "job": "Progforce\Catalog\Console\Queue\TagQueue@updateFromOpenerp",
        "qname": "queues:tags"
    },
    "res.users": {
        "job": "Progforce\Catalog\Console\ImportOpenerpUsers@job",
        "qname": "queues:mono"
    },
    "res.groups": {
        "job": "Progforce\Catalog\Console\ImportOpenerpUsers@job",
        "qname": "queues:mono"
    },
    "export.template": {
        "job": "Progforce\Catalog\Console\Product\ImportProduct@job"
    },
    "product.product": {
        "job": "Progforce\Catalog\Console\Product\ImportProduct@job"
    }
 }
}
"""

class Job(object):

    redisq = False

    job = ''

    def __init__(self, cr, uid, pool):
        self.pool = pool

        conf_obj = self.pool.get('ir.config_parameter')
        settings_obj = conf_obj.get_param(cr, uid, 'product.catalog.queue')
        if settings_obj:
            self.qsettings = eval(settings_obj)
            self.redisq = redis.StrictRedis(host=self.qsettings['host'], port=self.qsettings['port'])

    def send(self, job_type, data):
        if self.redisq:
            qname = self.qsettings['type_job_name'][job_type].get('qname', self.qsettings['default_qname'])
            job = {
                'job': self.qsettings['type_job_name'][job_type]['job'],
                'data': data,
                'id': randomstring(32),
                'attempts': 1
            }
            self.redisq.rpush(qname, json.dumps(job))
        return True

