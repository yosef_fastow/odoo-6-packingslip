import urllib
import urllib2
from hashlib import md5
from datetime import datetime


class Request(object):

    def __init__(self, cr, uid, pool):
        self.pool = pool

        conf_obj = self.pool.get('ir.config_parameter')
        self.host = conf_obj.get_param(cr, uid, 'product.catalog.host')
        self.uid = uid

    def send(self, sub_url, data=None):

        if data is None:
            data = {}

        data['token'] = str(md5(str(self.uid) + datetime.utcnow().strftime('%Y-%m-%d %H')).hexdigest())

        if not self.host:
            raise Exception('Unknown Catalog\' host')

        url = "{}/{}".format(self.host, sub_url)
        if data:
            data = urllib.urlencode(data)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)

        return response
