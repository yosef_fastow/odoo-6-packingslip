# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from json import loads as json_loads
from openerp.addons.product_catalog_helper.catalog import catalog_request


class product_product(osv.osv):
    _inherit = "product.product"

    _columns = {
        'button_update_on_pc': fields.boolean('Update on P/C', ),
    }

    def update_on_pc(self, cr, uid, ids, context=None):
        response = None
        try:
            req_obj = catalog_request.Request(cr, uid, self.pool)
            raw_response = req_obj.send(
                'api/importProduct?id=' + ','.join(str(id) for id in ids)
            )
            response = raw_response.read()
        except Exception:
            pass

        data = None
        try:
            # FOR EXAMPLE: {"imported_products":["FC0ATM-HKKV"],"not_imported_products":[]}
            data = json_loads(response)
        except (ValueError, TypeError):
            pass

        result = {}
        if data:
            if (
                'imported_products' in data and
                data['imported_products'] and
                isinstance(data['imported_products'], (list, tuple, set))
            ):
                result.update({
                    'warning': {
                        'title': _('Info!'),
                        'message': _('Updated products: {}'.format(','.join('`' + str(x) + '`' for x in data['imported_products'])))
                    }
                })
            elif (
                'not_imported_products' in data and
                data['not_imported_products'] and
                isinstance(data['not_imported_products'], (list, tuple, set))
            ):
                result.update({
                    'warning': {
                        'title': _('Info!'),
                        'message': _('Not updated products: {}'.format(','.join('`' + str(x) + '`' for x in data['not_imported_products'])))
                    }
                })
        else:
            result.update({
                'warning': {
                    'title': _('Info!'),
                    'message': _('Not updated products: {}'.format(','.join('`' + str(x) + '`' for x in ids)))
                }
            })
        print(result)
        return result

product_product()
