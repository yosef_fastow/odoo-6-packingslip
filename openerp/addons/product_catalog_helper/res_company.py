from osv import fields, osv

class res_company(osv.osv):
    _inherit = "res.company"
    _columns = {
        'bill_name': fields.char('Bill Name', size=256),
    }

res_company()