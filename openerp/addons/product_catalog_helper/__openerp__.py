# -*- coding: utf-8 -*-

{
    'name': 'Product Catalog Helpers',
    'version': '1.0.1',
    'category': 'Tools',
    'complexity': "expert",
    'description': """
        XML_RPC function for Product Catalog
    """,
    "js": [
        'static/src/js/*.js'
    ],
    'author': '',
    'website': '',
    'depends': [
        'sale',
        'tagging',
        'base',
        'export_template',
        'multi_customer',
    ],
    'init_xml': [
    ],
    'update_xml': [
        'view/tagging_view.xml',
        'view/product_view.xml',
        'view/res_user_view.xml',
        'view/res_company_view.xml',
        'data/system_params.xml',
    ],
    'demo_xml': [],
    'installable': True,
    "active": False,
    'auto_install': False,
    "application": True
}
