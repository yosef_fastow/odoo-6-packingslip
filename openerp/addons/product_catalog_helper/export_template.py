# -*- coding: utf-8 -*-

from osv import fields, osv
from catalog import catalog_job


class export_extract(osv.osv):
    _inherit = "export.extract"

    catalog_options = ["customer_fields"]

    def send_changes_to_catalog(self, cr, uid, tag_id):

        if isinstance(tag_id, (int, long)):
            tag_id = self.pool.get('tagging.tags').browse(cr, uid, tag_id)

        if tag_id:
            job_obj = catalog_job.Job(cr, uid, self.pool)

            for prod in tag_id.product_ids:
                job_obj.send('export.template', {
                    'product_id': prod.id,
                    'sku' : prod.default_code,
                    'options': self.catalog_options
                })

        return True

export_extract()