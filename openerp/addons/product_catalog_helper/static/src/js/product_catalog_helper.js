$(function() {
  var checkMenu = function(){
    var $first = $('#oe_menu table td').first();
    var conn = openerp.sessions.session0.connection;
    if($first.length > 0 && conn.user_context.is_readonly != true) {
      var $el = $first.clone();
      $el.find('a')
        .attr('target','_blank')
        .html('Product Catalog')
        .removeAttr('data-menu')
        .addClass('product-catalog-btn');
      $first.before($el);
      $el.find('a').click(function(){
        var d = new Date();
        var date = d.getUTCFullYear() + '-' + ('0'+(d.getUTCMonth()+1)).slice(-2) + '-' + ('0'+d.getUTCDate()).slice(-2) + ' ' + ('0'+d.getUTCHours()).slice(-2);
        var token = $.md5(conn.uid + date);
        $(this).attr('href','http://catalog.delmarintl.ca/login/' + token);
      });
    }
    else {
      setTimeout(arguments.callee, 50);
    }
  }();
});
