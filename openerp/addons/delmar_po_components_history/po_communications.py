# -*- coding: utf-8 -*-
from osv import fields, osv

from openerp.addons.pf_utils.utils.str_utils import str_encode


class po_communications(osv.osv):
    _name = 'po.communications'
    _check_is_null_column = 'type'

    TYPE_SELECTION = [
        ('internal', 'Internal'),
        ('forum', 'Forum'),
        ('rejected', 'Rejected'),
        ('flag', 'Flag'),
    ]

    _columns = {
        'note_date': fields.datetime('Creation date', readonly=True, select=1),
        'create_user': fields.char('Created by', size=256, ),
        'create_date': fields.datetime('Created at', ),
        'po_id': fields.many2one(
            'purchase.order',
            'Purchase Order',
        ),
        'product_id': fields.many2one(
            'product.product',
            'Product',
        ),
        'component_id': fields.many2one(
            'product.product',
            'Component',
        ),
        'subject': fields.text('Subject', ),
        'text': fields.text('Text', ),
        'status': fields.char('Status', size=256, ),
        'type': fields.selection(TYPE_SELECTION, 'Message Type'),
        'partner_id': fields.many2one(
            'res.partner',
            'Supplier',
        ),
        'assignee': fields.char('Assignee', size=256, ),
        'assignee_name': fields.char('Assignee Name', size=256, ),
        'username': fields.char('Username', size=256, ),
        'communication_id': fields.integer('communication_id', ),
        'wp_data': fields.text('WP Data', ),
        'deleted': fields.boolean('Deleted', ),
        'is_new': fields.boolean('Is New', ),
        'is_closed': fields.boolean('Is Closed', ),
        'item_recid': fields.char('Item RecID', size=256),
        'po_no': fields.char('PO NO', size=256),
        'id_delmar': fields.char('ID Delmar', size=256),
        'prod_recid': fields.char('Prod RecID', size=256),
        'gem_recid': fields.char('Gem RecID', size=256),
        'po_supplier_id': fields.char('PO Supplier ID', size=256),
        'comp_po_no': fields.char('Comp PO NO', size=256),
        'sequenceno': fields.integer('Sequence NO'),
        'partner_flag': fields.integer('Partner Flag'),
        'nextid': fields.integer('Next ID'),
        'bothclose': fields.integer('Both Close'),
        'updated_at': fields.datetime('Last Update Date'),
        'thread_id': fields.integer('Thread ID'),
    }

    _defaults = {
        'type': 'forum',
        'deleted': False,
        'is_new': True,
        'is_closed': False,
    }

    def soft_delete_communication(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        note_id = vals.get('note_id') or False
        flag = vals.get('deleted') or True # for undelete, maybe
        if note_id:
            self.write(cr, uid, note_id, {'deleted': flag}, context=context)

        return True

    def mark_read(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            read_flag = vals.get('read_flag') or True
            read_flag = not read_flag # Inverse flag because in DB it's equal is new
            note_id = vals.get('note_id') or False
            if note_id:
                self.write(cr, uid, note_id, {'is_new': (read_flag)}, context=context)
                rec_id = self.read(cr, uid, note_id, ['communication_id'])

                server_data = self.pool.get('fetchdb.server')
                server_id = server_data.get_po_servers(cr, uid, single=True)
                if server_id:
                    rec_sql = "UPDATE Supplier_Communications SET New = ? WHERE Rec_ID = ?"
                    params = (int(read_flag), rec_id['communication_id'])
                    server_data.make_query(cr, uid, server_id, rec_sql, params=params, select=False, commit=True)

        return True

    def mark_closed(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            close_flag = vals.get('close_flag') or True
            note_id = vals.get('note_id') or False
            if note_id:
                rec_id = self.read(cr, uid, note_id, ['communication_id', 'po_id', 'product_id', 'sequenceno', 'thread_id'])
                group_id = self.search(cr, uid, [
                    ('po_id', '=', rec_id['po_id'][0]),
                    ('product_id', '=', rec_id['product_id'][0]),
                    ('thread_id', '=', rec_id['thread_id']),
                ])
                self.write(cr, uid, group_id, {'is_closed': close_flag}, context=context)

                server_data = self.pool.get('fetchdb.server')
                server_id = server_data.get_po_servers(cr, uid, single=True)
                if server_id:
                    rec_sql = """UPDATE Supplier_Communications SET \"Close\" = ?, Status = 'Closed' WHERE ID = ?"""
                    params = (int(close_flag), int(rec_id['thread_id']))
                    server_data.make_query(cr, uid, server_id, rec_sql, params=params, select=False, commit=True)

        return True


    def create_communication(self, cr, uid, vals, context=None):
        partner_ref = vals.get('partner_id') or False
        if partner_ref:
            partner_id = self.pool.get('res.partner').search(cr, uid, [('ref', '=', partner_ref)])
            if partner_id:
                vals['partner_id'] = partner_id[0]

        res = self.create(cr, uid, vals, context) # record id

        return res

    def get_wp_1line_rec_id(self, cr, uid, po_name, default_code, context=None):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        if server_id:
            rec_sql = "SELECT rec_ID FROM wp_1line WHERE PO_number = ? AND PO_product_ID_delmar = ?"
            params = (po_name, default_code)
            rec_id = server_data.make_query(cr, uid, server_id, rec_sql, params=params)

            return rec_id
        else:
            return False

    def create(self, cr, uid, vals, context=None):
        message_type = vals.get('type') or 'forum'
        create_date = vals.pop('create_date', None)
        if message_type in ('forum'):
            # FIXME: can we move this to the pf_utils function to use it all over the system?
            text = vals.get('text', u'').encode('ascii', 'ignore')
            po_id = vals.get('po_id', False)
            product_id = vals.get('product_id', False)
            partner_id = vals.get('partner_id', False)
            status = vals.get('status', None)
            username = vals.get('username', None)
            assignee = vals.get('assignee', None)
            assignee_name = vals.get('assignee_name', None)
            sequenceno = vals.get('sequenceno', None)
            component_id = vals.get('component_id', None)
            thread_id = vals.get('thread_id', None)
            create_user = vals.get('create_user', None)

            if not text:
                return False

            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_po_servers(cr, uid, single=True)
            if not server_id:
                raise

            po_obj = self.pool.get('purchase.order').browse(cr, uid, po_id)
            po_name = po_obj.name
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
            default_code = product_obj.default_code
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
            supplier = partner.ref

            rec = self.get_wp_1line_rec_id(cr, uid, po_name, default_code, context)

            rec_id = rec and rec[0] and rec[0][0] or None

            gem_recid = None
            if component_id:
                component_obj = self.pool.get('product.product').browse(cr, uid, component_id)
                if not component_obj.is_null():
                    component_code = component_obj and component_obj.default_code
                    gem_recid_sql = "SELECT Rec_ID FROM wp_gem where PO_number = ? and d_delmar_family = ?"
                    params = (po_name, component_code)
                    gem_recid = server_data.make_query(cr, uid, server_id, gem_recid_sql, params=params, select=True, commit=False)
                    gem_recid = (gem_recid and gem_recid[0] and gem_recid[0][0]) or None

                    if not gem_recid:
                        diam_recid_sql = "SELECT Rec_ID from DiamondOrd where PO_number = ? and id_delmar_family = ?"
                        params = (po_name, component_code)
                        gem_recid = server_data.make_query(cr, uid, server_id, diam_recid_sql, params=params, select=True, commit=False)
                        gem_recid = (gem_recid and gem_recid[0] and gem_recid[0][0]) or None

            thread_insert = {'subquery': '(SELECT IDENT_CURRENT(\'Supplier_Communications\'))'} if not thread_id else thread_id

            insert_table = 'Supplier_Communications'
            insert_values = {
                "Item_RecID": rec_id or None,
                "Date_created": create_date or {'subquery': 'getdate()'},
                "created_by": create_user or None,
                "Text": text or None,
                "PO_NO": po_name or None,
                "ID_Delmar": default_code or None,
                "Assignee": assignee or None,
                "Assignee_Name": assignee_name or None,
                "Gem_RecID": gem_recid or None,
                "Username": username or None,
                "PO_supplier_ID": supplier or None,
                "status": 'Open',
                "New": 1,
                "Partner_Flag": 1,
                "Close": 0,
                "SequenceNo": sequenceno or None,
                "ID": thread_insert,
            }

            server_data.insert_record(
                cr, uid, server_id,
                insert_table,
                insert_values,
                select=False,
                commit=True
            )
            get_rec_id_sql = "SELECT MAX(Rec_ID) FROM Supplier_Communications WHERE PO_NO = ? AND Text = ? GROUP BY PO_NO, Text"
            params = (po_name, text)
            ms_id = server_data.make_query(cr, uid, server_id, get_rec_id_sql, params=params, select=True, commit=False)
            if ms_id and ms_id[0]:
                ms_rec_id = ms_id[0][0] or False
                vals.update({'communication_id': ms_rec_id})

                if not thread_id:
                    get_thread_id_sql = "SELECT ID FROM Supplier_Communications WHERE Rec_ID = ?"
                    params = (ms_rec_id,)
                    ms_res = server_data.make_query(cr, uid, server_id, get_thread_id_sql, params=params, select=True, commit=False)
                    ms_thread_id = (ms_res and ms_res[0] and ms_res[0][0]) or None
                    vals.update({'thread_id': ms_thread_id})

            if gem_recid:
                vals.update({'gem_recid': gem_recid})

        res = super(po_communications, self).create(cr, uid, vals, context)

        # create_date should be the same
        if create_date:
            self.write(cr, uid, res, {'create_date': create_date})

        return self.read(cr, uid, res)


po_communications()
