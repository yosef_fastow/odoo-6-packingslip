# -*- coding: utf-8 -*-
import re

from pf_utils.utils.helper import _get_one

from osv import fields, osv

class po_components_history(osv.osv):
    _name = 'po.components.history'
    _check_is_null_column = 'po_id'

    _batch_map = {
        'Gemstone': 'LM',
        'Pearl': 'PC',
    }

    _columns = {
        'po_id': fields.many2one(
            'purchase.order',
            'Purchase Order',
        ),
        'po_line_id': fields.many2one(
            'purchase.order.line',
            'Purchase Order Line',
        ),
        'product_id': fields.related(
            'po_line_id', 'product_id',
            string='Product',
            readonly=True,
            type="many2one",
            relation="product.product",
        ),
        'size_id': fields.related(
            'po_line_id', 'size_id',
            string='Size',
            readonly=True,
            type="many2one",
            relation="ring.size",
        ),
        'component_id': fields.many2one(
            'product.product',
            'Parent Component',
        ),
        'qty': fields.integer('Quantity', ),
        'qty_of_sets': fields.integer('Quantity Of Sets', ),
        'rec_id': fields.char('rec_id', size=256, ),
        'batch_number': fields.char('Batch Number', size=256, ),
        'status': fields.char('Status', size=256, ),
        'price': fields.float('Price', ),
        'weight': fields.float('Weight', ),
        'cost': fields.float('Cost', ),
        'eta': fields.datetime('ETA', ),
        'ordered': fields.boolean('Ordered', ),
        'requested_date': fields.datetime('Requested Date', ),
        'required_date': fields.datetime('Required Date', ),
        'wp_data': fields.text('WP Data', ),
        'type': fields.text('Type', ),
        'alexone': fields.text('AlexOne MS filed', ),
        'ord_from': fields.char('Order from', size=256, ),
        'confirm_flag': fields.boolean('Confirm Flag'),
    }

    _defaults = {
        'confirm_flag': False,
    }

    def _get_next_batch_no(self, cr, uid, prefix, mssql_conn, mssql_curs):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        batch_number_query = """SELECT MAX(subsrt) + 1 FROM (
                SELECT
                    "Batch_#",
                    subsrt = SUBSTRING("Batch_#", pos, LEN("Batch_#"))
                FROM (
                    SELECT "Batch_#", pos = PATINDEX('%[0-9]%', "Batch_#")
                    FROM wp_gem_batch
                ) batches
            ) a WHERE "Batch_#" LIKE ?
        """
        batch_number = server_data.make_query(
            cr, uid, server_id,
            batch_number_query, params=(prefix + '%',),
            select=True, commit=False
        )
        return batch_number[0][0] if batch_number else 0

    def update_batch_table(self, cr, uid, values, mssql_conn, mssql_curs, context=None):
        if not context:
            context = {}
        params = []
        if not values:
            return True
        po_name = context.get('po')
        po_id = context.get('po_id')
        po_line_ids = context.get('po_line_ids') or []
        default_code = context.get('default_code')
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        if not server_id:
            return False

        for batch_type in values:
            lm_or_pc  = self._batch_map[batch_type]
            sequence = self._get_next_batch_no(cr, uid, lm_or_pc,
                                               mssql_conn, mssql_curs)
            for ord_from in values[batch_type]:
                insert_batch_sql = """
                    IF EXISTS (
                            SELECT "Batch_#"
                            FROM wp_gem_batch
                            WHERE convert(varchar, Date_created, 103) = convert(varchar, getdate(), 103)
                            and "Batch_#" like %(prefix)s
                        )
                        BEGIN
                            UPDATE wp_gem_batch set No_items = No_items + %(qty)s
                            OUTPUT INSERTED."Batch_#" as batch
                            WHERE "Batch_#" in (
                                SELECT "Batch_#"
                                FROM wp_gem_batch
                                WHERE convert(varchar, Date_created, 103) = convert(varchar, getdate(), 103)
                                and "Batch_#" like %(prefix)s
                            )
                        END
                    ELSE
                        BEGIN
                            INSERT INTO wp_gem_batch
                            ("Batch_#", No_items, No_Items_Confirmed, Date_created, Valid, Show)
                            OUTPUT INSERTED."Batch_#" as batch
                            VALUES
                            (%(new_no)s, %(qty)s, 0, getdate(), 0, 1)
                        END"""

                prefix = lm_or_pc + ('L' if ord_from.lower() == 'local' else 'H')
                qty = values[batch_type][ord_from] or 0
                params = {
                    'prefix': prefix + '%',
                    'qty': qty,
                    'new_no': prefix + str(sequence),
                }

                answer = server_data.make_query(
                    cr, uid, server_id,
                    insert_batch_sql,
                    params=params,
                    select=True,
                    commit=False,
                    connection=mssql_conn,
                    cursor=mssql_curs
                )
                if answer is False:
                    return False

                batch_number = answer and answer[0] and answer[0][0]
                if not (batch_number and po_name and po_id and default_code):
                    continue
                update_gem_sql = """UPDATE wp_gem
                    SET "Batch_#" = ?
                    WHERE PO_number = ?
                        AND Prod_type = ?
                        AND PO_product_ID_delmar = ?
                        AND Fill_by = ?
                """
                params = (
                    batch_number,
                    po_name,
                    batch_type,
                    default_code,
                    ord_from,
                )
                answer = server_data.make_query(
                    cr, uid, server_id,
                    update_gem_sql,
                    params=params,
                    select=False,
                    commit=False,
                    connection=mssql_conn,
                    cursor=mssql_curs,
                )
                if answer is False:
                    return False
                component_ids = self.search(cr, uid, [
                    ('po_id', '=', po_id),
                    ('type', '=', batch_type),
                    ('po_line_id', 'in', po_line_ids),
                    ('ord_from', '=', ord_from),
                ])
                self.write(cr, uid, component_ids, {'batch_number': batch_number})

        return True

    def fill_all_components(self, cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, context=None):
        if not context:
            context = {}
        po = self.pool.get('purchase.order').browse(cr, uid, po_id)
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        product = _get_one(product)
        product_default_code = product.default_code

        clear_sql = "DELETE FROM wp_compo2order WHERE PO_product_ID_delmar = ?"
        server_data.make_query(
            cr, uid, server_id,
            clear_sql,
            params=(product_default_code,),
            select=False,
            commit=False,
            connection=mssql_conn,
            cursor=mssql_curs,
        )
        for pack_line in product.pack_line_ids:
            component = pack_line.product_id
            category = component and component.categ_id
            component_type = category and category.name
            if component_type in ['Labor', 'Metal']:
                continue
            shape = None
            color = None
            prod_quality = None

            if component_type == 'Diamond':
                shape = component.prod_cut_shape
                color = component.prod_dmd_color
            elif component_type == 'Gemstone':
                shape = component.prod_cut_shape or component.prod_gemst_shape
            elif component_type == 'Pearl':
                shape = component.prod_cut_shape or component.pearl_shape_id and component.pearl_shape_id.name
                color = component.pearl_color_id and component.pearl_color_id.name

            insert_table = 'wp_compo2order'
            insert_values = {
                "PO_product_ID_delmar": product_default_code or None,
                "Date_Shipped": None,
                "Order_status": None,
                "PO_No_ID_Delmar": None,
                "Prod_cut_shape": shape or None,
                "Prod_diam_mm": component.prod_diam_mm or None,
                "Prod_sub_type": component.prod_sub_type or None,
                "Prod_type": component_type or None,
                "Prod_weight": pack_line.weight or None,
                "Qty_to_order": None,
                "Rec_ID": {'subquery': '(SELECT max(Rec_ID) + 1 FROM wp_compo2order)'},
                "ETA": None,
                "OrderNumber": None,
                "Required_Date": None,
                "Ordered_by": None,
                "Product_Supplier": None,
                "Set_qty": None,
                "Gemstone_Description": None,
                "Gemstone_qty_per_set": None,
                "FM_Flag": None,
                "Total_qty_gems_needed": None,
                "Total_qty_gems_ordered": None,
                "Gem_weight_per_pc": None,
                "Notes": None,
                "Product_description": product.name,
                "Batch_#": None,
                "Dia_Req_date": None,
                "prod_qty": pack_line.quantity,
                "TotalGem2Order": None,
                "prod_quality": None,
                "dia_color": color or None,
                "price_SRP_CA": component.price_srp_ca or None,
                "prod_cost_showas": component.price_cost_showas or None,
                "prod_description_line": component.name or None,
                "prod_plating": None,
                "ID_delmar_family": component.default_code or None,
            }

            answer = server_data.insert_record(
                cr, uid, server_id,
                insert_table,
                insert_values,
                select=False,
                commit=False,
                connection=mssql_conn,
                cursor=mssql_curs
            )
            if answer is False:
                return False

        return True

    def update_component_prod_recid(self, cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, context=None):
        # Update Prod_Recid for components after order MS creating (cursor commit)
        if not context:
            context = {}
        po_obj = self.pool.get('purchase.order').browse(cr, uid, po_id)
        product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
        po_name = po_obj.name
        default_code = product_obj.default_code

        line_rec_id_sql = "SELECT TOP 1 Rec_ID FROM wp_1line WHERE PO_number = ? and PO_product_ID_delmar = ?"
        params = (po_name, default_code)
        answer = server_data.make_query(
            cr, uid, server_id,
            line_rec_id_sql,
            params=params,
            select=True,
            commit=False
        )

        if answer and answer[0] and answer[0][0]:
            rec_id = answer[0][0]

            # Gemstone update
            gem_update_sql = """UPDATE wp_gem
                SET Prod_Recid = ?
                WHERE OrderNumber = ? and PO_product_ID_delmar = ?
            """
            params = (rec_id, po_name, default_code)
            answer = server_data.make_query(
                cr, uid, server_id,
                gem_update_sql,
                params=params,
                select=False,
                commit=True
            )

            # Diamonds update
            # Maybe it should be Rec_ID2, maybe...

        return True

    def update_alexone_column(self, cr, uid, vals):
        status = True
        po_id = vals.get('po_id', None)
        product_id = vals.get('product_id', None)
        component_id = vals.get('component_id', None)
        alexone = vals.get('alexone', None)

        if po_id and product_id and component_id:
            po_obj = self.pool.get('purchase.order').browse(cr, uid, po_id)
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
            component_obj = self.pool.get('product.product').browse(cr, uid, component_id)
            po_name = po_obj.name
            default_code = product_obj.default_code
            component_code = component_obj.default_code
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_po_servers(cr, uid, single=True)

            history_ids = self.search(cr, uid, [
                ('po_id', '=', po_id),
                ('product_id', '=', product_id),
                ('component_id', '=', component_id)
            ])
            if history_ids:
                history_id = _get_one(history_ids)
                self.write(cr, uid, history_id, {'alexone': alexone})
            else:
                status = False

            if server_id:

                gem_update_sql = """UPDATE wp_gem
                    SET AlexOne = ?
                    WHERE OrderNumber = ? and PO_product_ID_delmar = ? and d_delmar_family = ?
                """
                dmd_update_sql = """UPDATE DiamondOrd
                    SET AlexOne = ?
                    WHERE PO_number = ? and PO_product_ID_delmar = ? and id_delmar_family = ?
                """
                for sql in [gem_update_sql, dmd_update_sql]:
                    params = (alexone, po_name, default_code, component_code)
                    answer = server_data.make_query(
                        cr, uid, server_id,
                        sql,
                        params=params,
                        select=False,
                        commit=True
                    )

                    if answer is False:
                        status = False

        return status

    def update_ordfrom_column(self, cr, uid, vals):
        status = True
        po_id = vals.get('po_id', None)
        product_id = vals.get('product_id', None)
        component_id = vals.get('component_id', None)
        ord_from = vals.get('ord_from', None)

        if po_id and product_id and component_id:
            po_obj = self.pool.get('purchase.order').browse(cr, uid, po_id)
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
            component_obj = self.pool.get('product.product').browse(cr, uid, component_id)
            po_name = po_obj.name
            default_code = product_obj.default_code
            component_code = component_obj.default_code
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_po_servers(cr, uid, single=True)

            history_ids = self.search(cr, uid, [
                ('po_id', '=', po_id),
                ('product_id', '=', product_id),
                ('component_id', '=', component_id)
            ])
            if history_ids:
                history_id = _get_one(history_ids)
                self.write(cr, uid, history_id, {'ord_from': ord_from})
            else:
                status = False

            if server_id:

                gem_update_sql = """UPDATE wp_gem
                    SET Fill_by = ?
                    WHERE OrderNumber = ? and PO_product_ID_delmar = ? and d_delmar_family = ?
                """

                params = (ord_from, po_name, default_code, component_code)
                answer = server_data.make_query(
                    cr, uid, server_id,
                    gem_update_sql,
                    params=params,
                    select=False,
                    commit=True
                )

                if answer is False:
                    status = False

        return status

po_components_history()
