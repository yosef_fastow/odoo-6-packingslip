# -*- coding: utf-8 -*-
from collections import namedtuple
import datetime
from json import dumps as json_dumps
from json import loads as json_loads
import logging
from pf_utils.utils.helper import _get_one
from pf_utils.utils.re_utils import str2date
import time
import netsvc
from osv import fields, osv

pch_item = namedtuple('pch_item', ['product_id', 'size_id'])
logger = logging.getLogger(__name__)


class DataDecoder(object):
    def __init__(self):
        super(DataDecoder, self).__init__()

    def decode(self, data):
        new_data = None
        if isinstance(data, (str, unicode)):
            try:
                new_data = json_loads(data)
            except Exception:
                pass
        elif isinstance(data, dict):
            new_data = data
        else:
            new_data = False
        return new_data


class purchase_order(osv.osv):
    _name = 'purchase.order'
    _inherit = 'purchase.order'
    _check_is_null_column = 'name'

    data_decoder = DataDecoder()

    STATE_SELECTION = [
        ('draft', 'Request for Quotation'),
        ('wait', 'Waiting'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('created', 'Created'),
        ('except_picking', 'Shipping Exception'),
        ('except_invoice', 'Invoice Exception'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ('partial', 'Partial'),
    ]

    def _auto_init(self, cr, context=None):
        super(purchase_order, self)._auto_init(cr, context)
        cr.execute('SELECT indexname FROM pg_indexes WHERE indexname = \'purchase_order_location_id\'')
        if not cr.fetchone():
            cr.execute('CREATE INDEX purchase_order_location_id ON purchase_order (location_id)')

    def _get_line_partner_id(self, cr, uid, ids, *args, **kwargs):
        res = {}
        if (not ids):
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        cr.execute("""
            SELECT
                po.id,
                min(lp.partner_id) as partner_id
            FROM purchase_order po
                LEFT JOIN purchase_order_line pl on po.id = pl.order_id
                LEFT JOIN purchase_order_line_partner lp on pl.id = lp.pol_id
            WHERE po.id IN %(ids)s
            GROUP BY po.id
            """, {
            'ids': tuple(ids),
        })

        sql_res = cr.fetchall()
        res = {x[0]: x[1] for x in sql_res}
        return res

    _columns = {
        'state': fields.selection(
            STATE_SELECTION, 'State', readonly=True,
            help="The state of the purchase order or the quotation request. A quotation is a purchase order in a 'Draft' state. Then the order has to be confirmed by the user, the state switch to 'Confirmed'. Then the supplier must confirm the order to change the state to 'Approved'. When the purchase order is paid and received, the state becomes 'Done'. If a cancel action occurs in the invoice or in the reception of goods, the state becomes in exception."
            , select=True
        ),
        'related_po_ids': fields.many2many(
            'purchase.order',
            'po_related_to_po',
            'parent_po_id',
            'children_po_id',
            'Related PO'
        ),
        'related_component_ids': fields.one2many('po.components.history', 'po_id', 'Related Components'),
        'related_communications_ids': fields.one2many('po.communications', 'po_id', 'Communications'),
        'wp_data': fields.text('WP Data', ),
        'date_ordered': fields.date('Date Ordered'),

        # Catalog fields
        'factory': fields.text('Factory', ),
        'wp_name': fields.text('WP Name', ),
        'line_counter': fields.integer('Count Of Lines', ),
        'reserved': fields.boolean('Reserved', ),
        'release_date': fields.datetime('Release date', ),
        'wh_location': fields.char('WH location', size=1024, ),
        'line_partner_id': fields.function(
            _get_line_partner_id,
            type="many2one",
            relation="res.partner",
            string='Partner',
            readonly=True,
        ),
    }

    PO_rel_data = False


    def split_vals(self, input_vals):
        pch_vals = {
            pch_item(
                product_id=line[2]['product_id'],
                size_id=line[2]['size_id'] if line[2]['size_id'] else None
            ): line[2]['pch_vals']
            for line in input_vals['order_line'] if line and line[2] and 'pch_vals' in line[2]
            }
        vals = input_vals
        for order_line in vals['order_line']:
            del order_line[2]['pch_vals']
        return vals, pch_vals

    def get_po_relation_ids(self, cr, uid, line_id):
        sql = """SELECT
                rel.parent_po_line_id,
                rel.parent_product_id,
                rel2.children_po_line_id,
                pol.product_id as children_product_id
            FROM (
                SELECT rel.parent_po_line_id, pol.product_id as parent_product_id
                    FROM po_lines_rel as rel
                    LEFT JOIN purchase_order_line pol on pol.id = rel.parent_po_line_id
                WHERE rel.children_po_line_id = %s
                    OR rel.parent_po_line_id = %s
                GROUP BY rel.parent_po_line_id, pol.product_id
            ) as rel
            LEFT JOIN po_lines_rel as rel2 on rel2.parent_po_line_id = rel.parent_po_line_id
            LEFT JOIN purchase_order_line pol on pol.id = rel2.children_po_line_id
        """ % (line_id, line_id)
        cr.execute(sql)
        res = cr.dictfetchall()

        return res

    def create_relation(self, cr, uid, line, context):
        if line and 'parent_po_line_id' in line and 'children_po_line_id' in line:
            if line['parent_po_line_id'] and line['children_po_line_id']:
                check_sql = """
                    SELECT parent_po_line_id FROM po_lines_rel
                    where parent_po_line_id = {parent_id}
                    AND children_po_line_id = {children_id}
                """.format(parent_id=line['parent_po_line_id'], children_id=line['children_po_line_id'])
                cr.execute(check_sql)
                res = cr.dictfetchall()
                if not res:
                    sql = """INSERT INTO po_lines_rel
                        (parent_po_line_id, children_po_line_id)
                        VALUES
                        ({parent_id}, {children_id})
                    """.format(parent_id=line['parent_po_line_id'], children_id=line['children_po_line_id'])
                    cr.execute(sql)

            return True

        return False

    def update_related_po_line(self, cr, uid, rel_data, context=None):
        if not context:
            context = {}
        answer = False

        child_delmar_ids = rel_data.get('child_delmar_ids') or ""
        po_childs = rel_data.get('po_childs') or ""
        po_name = rel_data.get('po_name') or False
        default_code = rel_data.get('default_code') or False
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)

        if server_id and po_name and default_code:

            po_childs = ','.join(['\''+x+'\'' for x in po_childs.split(',')])

            item_recid_sql = """SELECT CAST(Item_RECID as varchar) as Item_RECID FROM wp_1line"""
            if child_delmar_ids:
                child_delmar_ids = ','.join(['\''+x+'\'' for x in child_delmar_ids.split(',')])
                item_recid_sql += """ WHERE PO_number in (%s) AND PO_product_ID_delmar in (%s)""" % (po_childs, child_delmar_ids)
            else:
                item_recid_sql += """ WHERE PO_number in (%s)""" % (po_childs)

            answer = server_data.make_query(
                cr, uid, server_id,
                item_recid_sql,
                select=True,
                commit=False
            )

            item_recids = None
            if answer:
                item_recids = ','.join([x[0] for x in answer if x[0]])

            update_sql = """
                UPDATE wp_1line
                SET relatedPO = ?
                WHERE PO_number = ? and PO_product_ID_delmar = ?
            """

            params = (item_recids, po_name, default_code)

            answer = server_data.make_query(
                cr, uid, server_id,
                update_sql,
                params=params,
                select=False,
                commit=True
            )

        return (answer == True)

    def fill_po_order(self, cr, uid, po_id, server_data, server_id, mssql_conn, mssql_curs, context=None):
        if not context:
            context = {}
        po = self.browse(cr, uid, po_id)
        po_name = po.name
        warehouse = po.warehouse_id
        date_ordered = po.date_ordered

        # Check existing order
        check_sql = "SELECT PO_no_order FROM wp_Order WHERE PO_no_order = '%s'" % (po_name)
        check = server_data.make_query(cr, uid, server_id, check_sql)
        if check is False:
            return False

        if len(check) == 0:
            user_name = po.create_uid and po.create_uid.name or None
            logger.info('Catalog111 ' + str(po.warehouse_id.id))
            logger.info('Catalog113 ' + str(po.warehouse_id.partner_address_id.id))
            #logger.info('Catalog114 ' + str(po.warehouse_id.partner_address_id.name))
            logger.info('Catalog114 ' + str(po.warehouse_id.name))
            logger.info('Catalog115 ' + str(po.partner_id.id))
            logger.info('Catalog116 ' + str(po.partner_id.name))

            company_name = po.partner_id and po.partner_id.name
            if company_name:
                logger.info('Catalog110 ' + str(company_name))
                company_name = company_name[:40]
            supplier_id = po.partner_id.ref
            warehouse_name = po.warehouse_id and po.warehouse_id.partner_address_id and po.warehouse_id.partner_address_id.name
            supplier_instructions = po.supplier_instructions
            instructions = 'All Merchandise Shipped must have ' \
                           'METAL STAMP, COUNTRY OF ORIGIN STAMP & DELMAR "D" TRADEMARK'

            reference_notes = po.reference_notes
            # lines = len(set([x.product_id for x in po.order_line if x.product_id == product_id]))
            lines = 1

            # Address
            street = None
            city = None
            postal_code = None
            country = None
            state = None
            street2 = None
            po_ship_via = None
            if warehouse and warehouse.partner_address_id:
                logger.info('Catalog150 ' + str(warehouse.id))
                logger.info('Catalog151 ' + str(warehouse.partner_address_id.id))

                street = warehouse.partner_address_id.street
                street2 = warehouse.partner_address_id.street2
                city = warehouse.partner_address_id.city
                postal_code = warehouse.partner_address_id.zip
                country = warehouse.partner_address_id.country_id and warehouse.partner_address_id.country_id.name
                country_code = warehouse.partner_address_id.country_id and warehouse.partner_address_id.country_id.code
                state = warehouse.partner_address_id.state_id and warehouse.partner_address_id.state_id.name

                # Ship via account
                conf_obj = self.pool.get('ir.config_parameter')
                settings_obj = conf_obj.get_param(cr, uid, 'po_fedex_direction')
                if settings_obj:
                    po_ship_via_obj = eval(settings_obj)
                    res_addr_id = self.pool.get('res.partner.address').search(cr, uid, [
                        ('partner_id', '=', po.partner_id and po.partner_id.id)
                    ])
                    res_addr_obj = self.pool.get('res.partner.address').browse(cr, uid, res_addr_id)
                    partner_code = (res_addr_obj[0].country_id and res_addr_obj[0].country_id.code)
                    if (country_code and country_code.lower() == 'ca') or (partner_code and partner_code.lower() == 'ca'):
                        po_ship_via = 'FedEx account # ' + po_ship_via_obj['ca']
                    else:
                        po_ship_via = 'FedEx account # ' + po_ship_via_obj['other']

            insert_table = 'wp_Order'
            insert_values = {
                "PO_no_order": po_name or None,
                "PO_supplier_ID": supplier_id or None,
                "PO_supplier_name": company_name or None,
                "PO_Confirm_flag": None,
                "PO_Confirm_Date": None,
                "PO_Confirming_to": None,
                "PO_ship_to": warehouse_name or None,
                "PO_ship_address_city": city or None,
                "PO_ship_address_civic_no": street or None,
                "PO_ship_address_country": country or None,
                "PO_ship_address_postalcode": postal_code or None,
                "PO_ship_address_state": state or None,
                "PO_ship_address_suite": street2 or None,
                "supplier_confirmed_date": None,
                "PO_status": 'New',
                "PO_date_required_internal": None,
                "PO_New": lines or None,
                "PO_supplier_notes": supplier_instructions or None,
                "Rec_ID": {'subquery': '(SELECT max(Rec_ID)+1 FROM wp_Order)'},
                "New_Status": 'New',
                "FM_Delivered_Flag": 0,
                "Price_Conf_Status": 'Incomplete',
                "Rpr": None,
                "PO_date_required_tosupplier": None,
                "PO_ship_via": po_ship_via or None,
                "PO_ship_instruction": instructions or None,
                "PO_ordered_by": user_name or None,
                "PO_supplier_notes_on_PO": reference_notes or None,
                "PO_date_ordered": date_ordered or None,
            }
            answer = server_data.insert_record(
                cr, uid, server_id,
                insert_table,
                insert_values,
                select=False,
                commit=False,
                connection=mssql_conn,
                cursor=mssql_curs
            )
            # TODO: parse answer on error
            if not answer:
                return False
        else:
            # Update exist order with count of lines (for example for sets)
            update_po_lines_counter_sql = "UPDATE wp_Order SET PO_New = coalesce(PO_New, 0)  + 1 WHERE PO_no_order = '%s'" % (po_name)
            answer = server_data.make_query(
                cr, uid, server_id,
                update_po_lines_counter_sql,
                select=False,
                commit=False,
                connection=mssql_conn,
                cursor=mssql_curs
            )
            if answer is False:
                return False

        return True

    def fill_po_lines(self, cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, context=None, all_po=False):
        if not context:
            context = {}
        po = self.browse(cr, uid, po_id)
        po_name = po.name
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        default_code = product.default_code
        weight = product.weight or 0.0
        micron = product.micron

        # Check existing line with requested product
        check_sql = "SELECT PO_number, PO_product_ID_delmar FROM wp_1line WHERE PO_number = ? and PO_product_ID_delmar = ?"
        params = (po_name, default_code)
        check = server_data.make_query(cr, uid, server_id, check_sql, params=params)
        if check is False:
            return False

        if len(check) == 0:
            supplier_sku = None
            supplier_id = None
            company_name = None
            user_name = po.create_uid and po.create_uid.name or None
            order_lines = [x for x in po.order_line if ((x.product_id and x.product_id.id) == product_id) and x.product_qty]
            if po.partner_id:
                logger.info('Catalog120 ' + str(po.partner_id.id))
                logger.info('Catalog121 ' + str(po.partner_id.name))

                supplier_id = po.partner_id.ref
                company_name = po.partner_id.name

                mcf_name_obj = self.pool.get('product.multi.customer.fields.name')
                field_name_id = mcf_name_obj.search(cr, uid, [
                    ('label', '=', 'Supplier SKU'),
                    ('customer_id', '=', po.partner_id.id),
                ])

                if field_name_id:
                    supplier_sku_sql = """SELECT value as sku
                        FROM product_multi_customer_fields
                        WHERE field_name_id = {field_name_id}
                            and partner_id = {partner_id}
                            and product_id = {product_id}""".format(
                            field_name_id=field_name_id[0],
                            partner_id=po.partner_id.id,
                            product_id=product_id
                        )
                    cr.execute(supplier_sku_sql)
                    sku_res = cr.dictfetchone()
                    if sku_res:
                        supplier_sku = sku_res['sku']

                    if not supplier_sku:
                        # DLMR-468, try to get sku from pp/pt
                        supplier_sku = default_code
                        supply_sql = """
                            select coalesce(pt.supplier_sku, pt.id_style, pp.default_code, null) as supplier_sku
                            from product_product pp
                            inner join product_template pt on pt.id=pp.product_tmpl_id
                            where pp.def_supplier_id={partner_id} and pp.id={product_id};
                        """.format(
                            partner_id=po.partner_id.id,
                            product_id=product_id
                        )
                        cr.execute(supply_sql)
                        res = cr.dictfetchone()
                        if res:
                            supplier_sku = res['supplier_sku']
                        # create customer field
                        mcf_obj = self.pool.get('product.multi.customer.fields')
                        to_insert = {
                            'size_id': None,
                            'field_name_id': field_name_id[0],
                            'partner_id': po.partner_id.id,
                            'product_id': product_id,
                            'value': supplier_sku,
                            'id_delmar': default_code,
                        }
                        mcf_obj.create(cr, uid, to_insert)

            date_ordered = po.date_ordered
            po_header_address = []
            if po.company_id:
                logger.info('Catalog130 ' + str(po.company_id.id))
                logger.info('Catalog131 ' + str(po.company_id.partner_id.id))

                company = po.company_id
                po_header_address.append(company.name or '')
                res_addr_id = self.pool.get('res.partner.address').search(cr, uid, [
                    ('partner_id', '=', company.partner_id and company.partner_id.id)
                ])
                res_addr_obj = self.pool.get('res.partner.address').browse(cr, uid, res_addr_id)
                if res_addr_obj:
                    po_header_address.append(res_addr_obj[0].street or '')
                    po_header_address.append(res_addr_obj[0].city or '')
                    po_header_address.append(res_addr_obj[0].state_id and res_addr_obj[0].state_id.name or '')
                    po_header_address.append(res_addr_obj[0].country_id and res_addr_obj[0].country_id.name or '')
                    po_header_address.append(res_addr_obj[0].zip or '')
                    phone = res_addr_obj[0].phone and 'Tel.: ' + res_addr_obj[0].phone or ''
                    fax = res_addr_obj[0].fax and 'Fax.: ' + res_addr_obj[0].fax or ''
                    po_header_address.append((phone + ' ' + fax).strip())
            po_header_address = "\n".join(po_header_address)[:200]

            ship_to_label = []
            warehouse_name = ''
            if po.warehouse_id and po.warehouse_id.partner_address_id:
                logger.info('Catalog140 ' + str(po.warehouse_id.id))
                logger.info('Catalog141 ' + str(po.warehouse_id.partner_address_id.id))

                wh_addr_obj = po.warehouse_id.partner_address_id
                warehouse_name = wh_addr_obj.name or ''
                ship_to_label.append(warehouse_name or '')
                ship_to_label.append(wh_addr_obj.street or '')
                ship_to_label.append(wh_addr_obj.city or '')
                ship_to_label.append(wh_addr_obj.state_id and wh_addr_obj.state_id.name or '')
                ship_to_label.append(wh_addr_obj.country_id and wh_addr_obj.country_id.name or '')
                ship_to_label.append(wh_addr_obj.zip or '')
            ship_to_label = ','.join(ship_to_label)[:200]

            pack_lines = product.pack_line_ids
            count_component = len([x.id for x in pack_lines if x.po_requires])

            to_update = {
                'partner_names': '',
                'size_qty': [],
                'size_names': [],
                'product_qty_total': 0,
                'product_qty': 0,
                'date_planned': False,
                'name_template': False,
                'prod_type': False,
                'prod_color': False,
                'prod_metal': False,
                'prod_plating': False,
                'prod_cost': False,
                'insert_size_qty_fields': [],
                'insert_size_label_fields': [],
            }
            po_customerid_list = []
            po_customername_list = []
            po_customerid = None

            if order_lines:
                for res in order_lines[0].partner_ids:
                    if res.fcdc_code:
                        po_customerid_list.append(res.fcdc_code)
                    elif res.ref:
                        po_customerid_list.append(res.ref)

                    if res.fcdcw_name:
                        po_customername_list.append(res.fcdcw_name)
                    elif res.name:
                        po_customername_list.append(res.name)

                po_customerid = ', '.join(po_customerid_list)
                for line in order_lines:

                    seq_obj_name = 'purchase.order.wp_po'
                    line_rec_id = self.pool.get('ir.sequence').get(cr, uid, seq_obj_name)

                    product_qty = line.product_qty
                    size_obj = line.size_id
                    to_update['date_planned'] = line.date_planned
                    to_update['partner_names'] = ', '.join(po_customername_list)
                    to_update['prod_cost'] = line.price_unit
                    line_product = line.product_id
                    to_update['status'] = 'Repair' if (line.status and line.status.lower() == 'repair') else 'Ordered'
                    if all_po:
                        to_update['status'] = line.status.capitalize()
                    if line_product:
                        to_update['name_template'] = product and product.name
                        to_update['prod_type'] = line_product.categ_id and line_product.categ_id.name
                        to_update['prod_metal'] = line_product.prod_metal_stamp or line_product.prod_metal or False
                        to_update['prod_color'] = line_product.prod_color or False
                        plating = line_product.metal_plating_id or False
                        to_update['prod_plating'] = plating and plating.name or line_product.prod_metal_plating or False
                    if size_obj:
                        po_size_id = self.pool.get('po.ring.size').search(cr, uid, [
                            ('size_id', '=', size_obj.id)
                        ])
                        po_size = self.pool.get('po.ring.size').read(cr, uid, po_size_id, ['ms_prefix'])
                        po_size = _get_one(po_size)
                        insert_size_qty_fields = 'prod_ring_size_'+po_size['ms_prefix']
                        insert_size_label_fields = 'prod_ring_label_'+po_size['ms_prefix']

                        to_update['size_names'].append(size_obj.name.replace('.0', ''))
                        to_update['size_qty'].append(product_qty)
                        to_update['insert_size_qty_fields'].append(insert_size_qty_fields)
                        to_update['insert_size_label_fields'].append(insert_size_label_fields)
                        to_update['product_qty_total'] += product_qty
                    to_update['product_qty'] += product_qty

                    # Related lines PO section
                    related_po = None
                    related_po_names = []
                    rel_data = False
                    if line.related_po_lines_ids and to_update['prod_type'] != 'Finding':
                        # If line is parent
                        rel_lines = line.related_po_lines_ids
                        related_po_names += [x.order_id.name for x in rel_lines if x.order_id and x.order_id.name]

                    if to_update['prod_type'] == 'Finding':
                        rel_lines_sql = """SELECT
                            array_to_string(array_agg(po.name), ',') as po_childs,
                            rel.default_code as default_code,
                            rel.name as po_name
                            from (
                                select rel.parent_po_line_id, po.name, default_code
                                from po_lines_rel as rel
                                left join purchase_order_line as pol on pol.id = rel.parent_po_line_id
                                left join purchase_order as po on po.id = pol.order_id
                                left join product_product as pp on pp.id = pol.product_id
                                where rel.children_po_line_id = %s
                            ) as rel
                            left join po_lines_rel as rel2 on rel2.parent_po_line_id = rel.parent_po_line_id
                            left join purchase_order_line as pol on pol.id = rel2.children_po_line_id
                            left join purchase_order as po on po.id = pol.order_id
                            group by rel.name, rel.default_code""" % (line.id)

                        cr.execute(rel_lines_sql)
                        rel_data = cr.dictfetchall()

                fm_rec_id = None
                fm_rec_id_sql = "(select \"rec_ID\" as rec_id from fm_prod_9_08 where \"ID_delmar\" = '%s')" % (default_code)
                cr.execute(fm_rec_id_sql)
                fm_rec_id_res = cr.dictfetchall()
                if fm_rec_id_res:
                    fm_rec_id = fm_rec_id_res[0]['rec_id']

                image_required = 'Need New Images: Yes' if line.image_required else ''
                video_required = 'Need New Videos: Yes' if line.video_required else ''
                if related_po_names:
                    related_po_line = "','".join(related_po_names)
                    item_recid_sql = """SELECT Item_RECID FROM wp_1line WHERE PO_number in ('%s')""" % (related_po_line)
                    item_recids = server_data.make_query(
                        cr, uid, server_id,
                        item_recid_sql,
                        select=True,
                        commit=False,
                        connection=mssql_conn,
                        cursor=mssql_curs
                    )
                    if item_recids:
                        related_po = ','.join(set([str(x[0]) for x in item_recids]))
                if rel_data:
                    # related_po_childs = ','.join(set(related_po))
                    self.PO_rel_data = rel_data
                    # answer = self.update_related_po_line(cr, uid, _get_one(rel_data), server_data, server_id, mssql_conn, mssql_curs, context=context)
                    # if not answer:
                    #     return False

                insert_table = 'wp_1line'
                insert_values = {
                    "PO_number": po_name or None,
                    "PO_product_ID_delmar": default_code or None,
                    "PO_item_status": to_update['status'],
                    "PO_product_supplier_name": company_name or None,
                    "PO_product_supplier_no": default_code or None,
                    "ship_code": None,
                    "PO_date_required": to_update['date_planned'],
                    "PO_date_ordered": date_ordered or None,
                    "PO_date_confirmed": None,
                    "prod_type": to_update['prod_type'] or None,
                    "metal_type": to_update['prod_metal'] or None,
                    "metal_color": to_update['prod_color'] or None,
                    "prod_cost": None,
                    "prod_notes": to_update['name_template'][:150] or None,
                    "prod_ring_size_total": to_update['product_qty_total'],
                    "prod_qty": to_update['product_qty'],
                    "PO_product_ship_to": warehouse_name or None,
                    "prod_qty_left_toship": to_update['product_qty'],
                    "Confirm_Flag": None,
                    "PO_supplier_ID": supplier_id or None,
                    "gold_market": None,
                    "prod_fcost": None,
                    "PO_supplier_notes": line.supplier_instructions and line.supplier_instructions[:200] or None,
                    "rec_ID": line_rec_id,
                    "DIA_Status": None,
                    "Finding_Status": None,
                    "Gem_Status": None,
                    "Pearl_Status": None,
                    "Sample_Status": None,
                    "PO_product_weightperpiece": weight,
                    "List_ETA": 1,
                    "PO_Confirm_Flag": 0,
                    "Customer_SKU_#": default_code or None,
                    "Customer": to_update['partner_names'][:400],
                    "Image_Width": None,
                    "Image_Length": None,
                    "Image_Height": None,
                    "Image_Band_Width": None,
                    "Images_Valid": int(line.image_required == True) or None,
                    "Videos_Valid": int(line.video_required == True) or None,
                    "Images_Show": None,
                    "Needed_Height": None,
                    "Needed_Length": None,
                    "Needed_Width": None,
                    "Needed_Band_Width": None,
                    "rec#": {'subquery': '(select CAST(CAST(max(case when isnumeric("rec#") = 1 then CAST("rec#" As NUMERIC) else 0 end) + 1 AS BIGINT) AS VARCHAR) from wp_1line)'},
                    "Needed_Depth": None,
                    "Image_Depth": None,
                    "Image_Width_R": None,
                    "Image_Length_R": None,
                    "Image_Height_R": None,
                    "Image_Band_Width_R": None,
                    "Image_Depth_R": None,
                    "Nmb_lines": None,
                    "SpecialLabelCustomer": None,
                    "Image_Width_R_2": None,
                    "Image_Length_R_2": None,
                    "Image_Height_R_2": None,
                    "Image_Band_Width_R_2": None,
                    "Image_Depth_R_2": None,
                    "Image_Width_R_3": None,
                    "Image_Length_R_3": None,
                    "Image_Height_R_3": None,
                    "Image_Band_Width_R_3": None,
                    "Image_Depth_R_3": None,
                    "PO_Price_Conf": None,
                    "Prod_Weight_Entered": round(weight, 1),
                    "Need_New_Images_Text": image_required or None,
                    "Need_New_Videos_Text": video_required or None,
                    "supplier_SKU": supplier_sku and supplier_sku[:50] or None,
                    "Item_RECID": fm_rec_id or None,
                    "PO_Header_Address": po_header_address or None,
                    "PO_ID_Barcode": None,
                    "PO_supplier_instruction": line.supplier_instructions and line.supplier_instructions[:500] or None,
                    "Prod_Metal": to_update['prod_metal'] or None,
                    "Prod_color": to_update['prod_color'] or None,
                    "PO_product_qty_approved": to_update['product_qty'],
                    "prod_plating": to_update['prod_plating'] or None,
                    "prod_weight": None,
                    "prod_cost_showas": None,
                    "prod_description_line": to_update['name_template'][:150] or None,
                    "prod_cut_shape": None,
                    "prod_diam_mm": None,
                    "dia_color": None,
                    "price_SRP_CA": None,
                    "Pricing_Gem_Wt": None,
                    "BIN": None,
                    "ShiptoLabel": ship_to_label or None,
                    "PO_No_ID_Delmar": po_name+' '+default_code,
                    "POout_1Line_Quarter": None,
                    "Temp_Image": None,
                    "Po_CustomerID": po_customerid or None,
                    "PO_date_required_suplier": None,
                    "Count_component": count_component,
                    "PO_product_description": to_update['name_template'][:150] or None,
                    "prod_ring_label": None,
                    "prod_ring_size": None,
                    "prod_shape": None,
                    "prod_size": None,
                    "prod_total_gem_needed": None,
                    "prod_total_gem_weight": None,
                    "prod_weight_individual": None,
                    "prod_weight_total": None,
                    "label_ring_qty": None,
                    "label_ring_size": None,
                    "PO_Number_stars": "*" + po_name + "*",
                    "PoModelFile": None,
                    "Rec_ID2": {'subquery': '(select CAST(CAST(max(case when isnumeric("rec#") = 1 then CAST("rec#" As NUMERIC) else 0 end) + 1 AS BIGINT) AS VARCHAR) from wp_1line)'},
                    "PO_date_required_internal": None,
                    "PO_ordered_by": user_name or None,
                    "ID_Ice": None,
                    "PO_product_Price_unit": None,
                    "PO_product_price_total": None,
                    "relatedPO": related_po or None,
                    "received_qty": None,
                    "repull_flag": None,
                    "Image_Ext_Length": None,
                    "Image_Ext_Length_R": None,
                    "Image_Ext_Length_R_2": None,
                    "Image_Ext_Length_R_3": None,
                    "Micron": micron or None,
                    "ProdLength": product.str_length or None,
                    "ProdExtLength": product.str_ext_length or None,
                }
                if to_update['insert_size_qty_fields'] and to_update['size_qty']:
                    i = 0
                    for field in to_update['insert_size_qty_fields']:
                        insert_values.update({
                            field: to_update['size_qty'][i],
                        })
                        i += 1
                if to_update['insert_size_label_fields'] and to_update['size_names']:
                    j = 0
                    for field in to_update['insert_size_label_fields']:
                        insert_values.update({
                            field: to_update['size_names'][j],
                        })
                        j += 1
                answer = server_data.insert_record(
                    cr, uid, server_id,
                    insert_table,
                    insert_values,
                    select=False,
                    commit=False,
                    connection=mssql_conn,
                    cursor=mssql_curs
                )
                if not answer:
                    return False
        else:
            # Jack Pot! Record exist. For rollback_po method - will not delete already exist order
            return 'record_exist'

        return True

    def check_exist_component(self, cr, uid, default_code, component_code, po_name, component_type, server_data, server_id, mssql_conn, mssql_curs):
        if component_type == "Diamond":
            sql = """SELECT PO_number FROM DiamondOrd WHERE PO_product_ID_delmar = ? AND id_delmar_family = ? AND PO_number = ?"""
        elif component_type in ('Gemstone', 'Pearl'):
            sql = """SELECT PO_number FROM wp_gem WHERE PO_product_ID_delmar = ? AND d_delmar_family = ? AND PO_number = ?"""

        component_ids = server_data.make_query(
            cr, uid, server_id,
            sql,
            params=[default_code, component_code, po_name],
            select=True,
            commit=False,
            connection=mssql_conn,
            cursor=mssql_curs
        )
        return bool(component_ids)

    def put_po_components_to_wp_po(self, cr, uid, po_id, product_id):
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        mssql_conn = server_data.connect(cr, uid, server_id)
        history_obj = self.pool.get('po.components.history')

        if not mssql_conn:
            return False

        mssql_curs = mssql_conn.cursor()

        try:
            result = self.fill_po_components(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs)

            if result is False:
                raise osv.except_osv('Error !', 'MSSQL action error.')
        except:
            mssql_conn.rollback()
            raise
        else:
            mssql_conn.commit()
        finally:
            mssql_conn.close()

        history_obj.update_component_prod_recid(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs)

        return result

    def fill_po_components(self, cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, context=None):
        if not context:
            context = {}
        added_components = []

        po = self.browse(cr, uid, po_id)
        po_name = po.name
        component_ids = self.pool.get('po.components.history').search(cr, uid, [
            ('po_id', '=', po_id),
            ('product_id', '=', product_id)
        ])
        components = self.pool.get('po.components.history').browse(cr, uid, component_ids)
        supplier_id = po.partner_id and po.partner_id.ref
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        default_code = product.default_code
        product_description = product and product.name
        po_customerid = None
        po_customer_name = None
        po_customerid_list = []
        po_customername_list = []

        user_name = po.create_uid and po.create_uid.name or None

        order_lines = [x for x in po.order_line if ((x.product_id and x.product_id.id) == product_id) and x.product_qty]
        if order_lines:
            for res in order_lines[0].partner_ids:
                if res.fcdc_code:
                    po_customerid_list.append(res.fcdc_code)
                elif res.ref:
                    po_customerid_list.append(res.ref)

                if res.fcdcw_name:
                    po_customername_list.append(res.fcdcw_name)
                elif res.name:
                    po_customername_list.append(res.name)

            po_customerid = po_customerid_list and po_customerid_list[0] or ''
            po_customer_name = po_customername_list and po_customername_list[0] or ''

        values = {}
        pack_line_data = {pack_line.product_id.id: pack_line for pack_line in product.pack_line_ids}
        for component in components:
            notes = None
            insert_values, insert_table = None, None
            component_type = component.type
            ord_from = component.ord_from
            product_component = component.component_id
            if component_type not in ('Gemstone', 'Pearl', 'Diamond') or not product_component:
                continue
            if component.alexone:
                notes = component.alexone

            # Getting data from component itself
            component_name = product_component.default_code
            component_desc = product_component.name
            set_qty = component.qty
            product_qty = component.qty_of_sets
            component_status = component.status
            required_date = component.required_date
            requested_date = component.requested_date
            # Diamond requested day should be request date + 9 days.
            # TODO: move to system params
            dia_req_date = str2date(requested_date) if requested_date else datetime.datetime.now()
            dia_req_date += datetime.timedelta(days=9)
            total_qty = set_qty * product_qty

            # Getting data from pack line
            pack_data = pack_line_data.get(product_component.id, None)
            qty_set = pack_data and pack_data.quantity or None
            cost_uom = pack_data and pack_data.cost_uom or None
            cmp_weight = pack_data and pack_data.weight or None
            expected_cost = cost_uom and round(float(cost_uom) * 0.85, 2) or None

            warehouse_name = po.warehouse_id and po.warehouse_id.partner_address_id and po.warehouse_id.partner_address_id.name
            diamond_order_wh = po.warehouse_id and po.warehouse_id.diamond_order_wh or warehouse_name
            name_and_po = po_name+' '+default_code
            color = None
            prod_gemst_quality = None
            expected_price = None  # product_component.expected_price
            prod_sub_type = product_component.prod_sub_type
            prod_diam_mm = product_component.prod_diam_mm
            prod_quality = product_component.prod_quality
            igi_information = product_component.igi_information
            shape = product_component.prod_cut_shape
            if component_type == 'Diamond':
                color = (
                    product_component.prod_dmd_color or
                    product_component.dmd_color_id and
                    product_component.dmd_color_id.name
                )
            elif component_type == 'Gemstone':
                if product_component.prod_gemst_quality:
                    prod_quality = product_component.prod_gemst_quality
                    prod_gemst_quality = product_component.prod_gemst_quality
            elif component_type == 'Pearl':
                if product_component.prod_pearl_quality:
                    prod_quality = product_component.prod_pearl_quality
                color = product_component.pearl_color_id and product_component.pearl_color_id.name

            if self.check_exist_component(cr, uid, default_code, component_name, po_name, component_type, server_data, server_id, mssql_conn, mssql_curs):
                continue

            if component_type == 'Diamond':
                insert_table = 'DiamondOrd'
                insert_values = {
                    "PO_date_required": required_date or None,
                    "PO_product_supplier_no": supplier_id or None,
                    "PO_number": po_name or None,
                    "Po_CustomerID": po_customerid or None,
                    "PO_product_qty_approved": product_qty,
                    "PO_product_ID_delmar": default_code or None,
                    "Prod_description_line": product_description and product_description[:150] or None,
                    "Prod_weight": cmp_weight or None,
                    "Prod_qty": qty_set or None,
                    "Qty_to_order": total_qty,
                    "Cost_CT": cost_uom or None,
                    "Prod_cut_shape": shape or None,
                    "Prod_diam_mm": prod_diam_mm or None,
                    "Dia_color": color and color[:10] or None,
                    "Prod_weight_CT": None,
                    "Rec_ID": {
                        'subquery': '(SELECT CAST(CAST(max(cast(Rec_ID As FLOAT)) + 1 AS BIGINT) AS VARCHAR) FROM DiamondOrd)'},
                    "Rec_ID2": {
                        'subquery': '(SELECT CAST(CAST(max(cast(Rec_ID2 As FLOAT)) + 1 AS BIGINT) AS VARCHAR) FROM DiamondOrd)'},
                    "PO_product_shipto": diamond_order_wh or None,
                    "IGI": igi_information or None,
                    "original_customer_name": po_customer_name and po_customer_name[:20] or None,
                    "AlexOne": notes and notes[:200] or None,
                    "CreateDate": requested_date or None,
                    "id_delmar_family": component_name or None,
                    "status": component_status or None,
                }

            elif component_type in ('Gemstone', 'Pearl'):
                insert_table = 'wp_gem'
                insert_values = {
                    "PO_product_ID_delmar": default_code or None,
                    "Date_Shipped": None,
                    "Order_status": 'New',
                    "PO_No_ID_Delmar": name_and_po or None,
                    "Prod_cut_shape": shape or None,
                    "Prod_diam_mm": prod_diam_mm or None,
                    "Prod_sub_type": prod_sub_type or None,
                    "Prod_type": component_type or None,
                    "Prod_weight": cmp_weight or None,
                    "Rec_ID": {
                        'subquery': '(SELECT CAST(CAST(MIN(cast(Rec_ID As FLOAT)) - 1 AS BIGINT) AS VARCHAR) FROM wp_gem)'},
                    "ETA": None,
                    "OrderNumber": po_name or None,
                    "Required_Date": required_date or None,
                    "Ordered_by": None,
                    "Product_Supplier": supplier_id or None,
                    "Set_qty": product_qty,
                    "Gemstone_Description": (str(set_qty) + ' x ' + component_desc) or None,
                    "Gemstone_qty_per_set": set_qty,
                    "SpareGems": 0,
                    "Total_qty_gems_needed": total_qty,
                    "Total_qty_gems_ordered": total_qty,
                    "Gem_weight_per_pc": 0,
                    "Notes": '',
                    "Product_description": product_description and product_description[:150] or None,
                    "Batch_#": '',
                    "OrderDate": requested_date or None,
                    "Confirm_Flag": 0,
                    "Date_Confirm": 0,
                    "Cost_Confirm": 0,
                    "Cancel_Confirm": 0,
                    "Gem_Quality": prod_gemst_quality or None,
                    "Prod_Recid": None,
                    "Fill_by": ord_from or None,
                    "Weight_Confirm": 0,
                    "Expected_Cost": expected_cost or None,
                    "Gemstone_Weight": cmp_weight or None,
                    "Change_Flag": None,
                    "Change Flag": None,
                    "SIMILAR": None,
                    "Similar Text": None,
                    "Cost_p_ct": None,
                    "Consolidator": user_name or None,
                    "Contractor": supplier_id or None,
                    "PO_number": po_name or None,
                    "PO_product_qty_approved": product_qty,
                    "Component_description_Line": (str(set_qty) + ' x ' + component_desc) or None,
                    "Comments1": None,
                    "MaxGemCostCt": '',
                    "Prod_weight_unit": None,
                    "AlexOne": notes and notes[:200] or None,
                    "Po_CustomerID": po_customerid or None,
                    "Expected_Price": expected_price or None,
                    "Dia_Req_date": dia_req_date.strftime('%Y-%m-%d'),
                    "Prod_qty": product_qty,
                    "Qty_to_order": set_qty,
                    "TotalGem2Order": total_qty,
                    "Prod_quality": prod_quality or None,
                    "prod_description_line": product_description and product_description[:150] or None,
                    "PO_item_status": 'Ordered',
                    "d_delmar_family": component_name or None,
                    "Pearl_color": color or None,
                }

                if component_type not in values:
                    values.update({component_type: {}})
                if ord_from not in values[component_type]:
                    values[component_type].update({ord_from: 0})
                values[component_type][ord_from] += 1

            if insert_values and insert_table:
                answer = server_data.insert_record(
                    cr, uid, server_id,
                    insert_table,
                    insert_values,
                    select=False,
                    commit=False,
                    connection=mssql_conn,
                    cursor=mssql_curs
                )
                if answer is False:
                    return False
                added_components.append(component_name)

        if values:
            context.update({
                'po': po_name,
                'po_id': po.id,
                'po_line_ids': [x.id for x in order_lines],
                'default_code': default_code
            })
            answer = self.pool.get('po.components.history').update_batch_table(
                cr, uid, values,
                mssql_conn,
                mssql_curs,
                context=context
            )
            if answer is False:
                return False

        return added_components or True

    def create_with_po_components_history(self, cr, uid, in_vals, context=None):
        vals = self.data_decoder.decode(in_vals)
        vals, pch_vals = self.split_vals(vals)
        po_id = self.create(cr, uid, vals, context)
        po_name = vals.get('wp_name') or None
        if po_name:
            self.write(cr, uid, po_id, {'name': po_name})
        else:
            po_name = self.browse(cr, uid, po_id).name
        cr.commit()
        if pch_vals:
            try:
                self.create_po_history_lines(cr, uid, po_id, pch_vals, context=context)
            except Exception, e:
                logger.error(e)
                self.unlink(cr, uid, po_id)
                cr.commit()
                raise
        return {'id': po_id, 'name': po_name}

    def create_po_history_lines(self, cr, uid, po_id, pch_vals, context=None):
        po = self.browse(cr, uid, po_id)
        pch_obj = self.pool.get('po.components.history')
        for order_line in po.order_line:
            product_id = order_line.product_id and order_line.product_id.id or None
            size_id = order_line.size_id and order_line.size_id.id or None
            vals = pch_vals.get(pch_item(product_id=product_id, size_id=size_id), {})
            if vals:
                for sub_vals in vals:
                    sub_vals.update({
                        'po_id': po_id,
                        'po_line_id': order_line.id,
                    })
                    sub_vals = {key: value if value else None for key, value in sub_vals.iteritems()}
                    pch_obj.create(cr, uid, sub_vals)
        return True

    def create_po_history_lines_xml(self, cr, uid, po_id, pch_vals, context=None):
        if pch_vals:
            po = self.browse(cr, uid, po_id)
            pch_obj = self.pool.get('po.components.history')
            for order_line in po.order_line:
                for sub_vals in pch_vals:
                    sub_vals.update({
                        'po_id': po_id,
                        'po_line_id': order_line.id,
                    })
                    sub_vals = {key: value if value else None for key, value in sub_vals.iteritems()}
                    ids = pch_obj.search(cr, uid, [('po_id', '=', po_id), ('po_line_id', '=', order_line.id), ('component_id', '=', sub_vals.get('component_id'))])
                    if len(ids) > 0:
                        pch_obj.write(cr, uid, ids[0], sub_vals)
                    else:
                        pch_obj.create(cr, uid, sub_vals)
                break
        return True

    def change_po_state(self, cr, uid, vals, mssql_conn=None, mssql_curs=None, context=None):
        po_id = False
        if vals:
            po_id = vals.get('po_id') or False
            product_id = vals.get('product_id') or False
            po_number = vals.get('po_number') or False
            delmar_id = vals.get('delmar_id') or False
            update_ms = vals.get('update_ms') or False
            state = vals.get('state') or False
            status = vals.get('status') or False
            date_ordered = vals.get('date_ordered') or False
            requested_date = vals.get('requested_date') or False
            all_po = vals.get('all_po') or False
        # XMLRPC method for PO state change
        if not context:
            context = {}
        result = True
        if po_id:
            line_obj = self.pool.get('purchase.order.line')
            history_obj = self.pool.get('po.components.history')
            order_status = 'new'

            po = self.browse(cr, uid, po_id)
            if po:
                po_line_ids = line_obj.search(cr, uid, [
                    ('order_id', '=', po_id),
                    ('product_id', '=', product_id)
                ])
                if po_line_ids:
                    # Update order line
                    to_write = {}
                    if state:
                        to_write = {'state': state, }
                    if status:
                        to_write.update({'status': status, })
                    if date_ordered:
                        to_write.update({'date_ordered': date_ordered})
                    line_obj.write(cr, uid, po_line_ids, to_write, context=context)

                    # Update order
                    if not update_ms:
                        line_states = set([x.state for x in po.order_line])
                        if line_states:
                            order_status = list(line_states)[0] if len(line_states) == 1 else 'partial'
                        to_write = {'state': order_status}
                        if date_ordered:
                            to_write.update({'date_ordered': date_ordered})
                        self.write(cr, uid, po_id, to_write, context=context)

                    # Update component history
                    to_write = {}
                    if requested_date:
                        to_write.update({'requested_date': requested_date})
                    history_ids = history_obj.search(cr, uid, [('po_id', '=', po_id)])
                    if history_ids:
                        if to_write:
                            history_obj.write(cr, uid, history_ids, to_write)
                        if status == 'Order rejected':
                            to_write = {'status': 'Order rejected'}
                            reject_ids = history_obj.search(cr, uid, [
                                ('id', 'in', history_ids),
                                ('status', 'in', ['new', 'ordered'])])
                            history_obj.write(cr, uid, reject_ids, to_write)

                    if state == 'created' or status == 'repair' or (('poro' in po_number.lower()) and all_po):

                        if state == 'created':
                            self.create_po_onhold_task(cr, uid, po_id)

                        server_data = self.pool.get('fetchdb.server')
                        server_id = server_data.get_po_servers(cr, uid, single=True)

                        close_conn = False
                        if mssql_conn is None:
                            mssql_conn = server_data.connect(cr, uid, server_id)
                            close_conn = True

                            if not mssql_conn:
                                return {'status': False}
                            mssql_curs = mssql_conn.cursor()

                        try:
                            result = self.fill_po_order(cr, uid, po_id, server_data, server_id, mssql_conn, mssql_curs)

                            if result:
                                result = self.fill_po_lines(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, all_po=all_po)

                            # FIXME: [YT] not sure if this is correct way to make this check...
                            # Just do not want to ruin anything.
                            if result == 'record_exist':
                                raise osv.except_osv(('Error !'), ('This PO and Item already exist in MSSQL server!'))

                            if result:
                                result = self.fill_po_components(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs)
                            if result:
                                result = history_obj.fill_all_components(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs)

                            if result is False:
                                raise osv.except_osv(('Error !'), ('MSSQL action error.'))
                        except:
                            if close_conn:
                                mssql_conn.rollback()
                            raise
                        else:
                            if close_conn:
                                mssql_conn.commit()
                        finally:
                            if close_conn:
                                mssql_conn.close()

                        # TODO: save error to logs
                        history_obj.update_component_prod_recid(cr, uid, po_id, product_id, server_data, server_id, mssql_conn, mssql_curs, context=context)

                        # Global var
                        if self.PO_rel_data:
                            # TODO: save error to logs
                            self.update_related_po_line(cr, uid, _get_one(self.PO_rel_data), context=context)
                    else:
                        if update_ms and status:
                            if status == 'Order rejected':
                                self.reject_ms_order(cr, uid, po_number, delmar_id, context=context)
                            else:
                                ms_state = {
                                    'PO_item_status': status.title(), #Delivered, Partial
                                }
                                line_obj.update_ms_order_line(cr, uid, po_number, delmar_id, ms_state, context=None)

            else:
                result = 'PO not found'
        else:
            result = False

        return {'status': result}

    def delete_ms_empty_order(self, cr, uid, po_number, server_data, server_id, mssql_conn, mssql_curs, context=None):
        params = {
            'po_number': po_number,
        }
        sql = """DELETE FROM wp_Order
                 WHERE (SELECT COUNT(*) FROM wp_1line WHERE PO_number = %(po_number)s) = 0
                 and PO_no_order = %(po_number)s
        """
        answer = server_data.make_query(
                cr, uid, server_id,
                sql,
                params=params,
                select=False,
                commit=False,
                connection=mssql_conn,
                cursor=mssql_curs
        )
        return answer is not None

    def delete_ms_order_line(self, cr, uid, po_number, delmar_id, server_data, server_id, mssql_conn, mssql_curs, context=None):
        params = {
            'po_number': po_number,
            'delmar_id': delmar_id,
        }

        sql = """UPDATE wp_gem_batch SET No_Items = No_Items - groups.qty
                FROM
                    (SELECT Batch_#, count(*) as qty
                    FROM wp_gem
                    WHERE OrderNumber = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s
                    GROUP BY Batch_#) groups
                WHERE wp_gem_batch.Batch_# = groups.Batch_#;

                DELETE FROM wp_gem WHERE OrderNumber = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s;
                DELETE FROM DiamondOrd WHERE PO_number = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s;
                DELETE FROM wp_1line WHERE PO_number = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s;
        """

        answer = server_data.make_query(
                cr, uid, server_id,
                sql,
                params=params,
                select=False,
                commit=False,
                connection=mssql_conn,
                cursor=mssql_curs
        )

        return answer is not None

    def reject_ms_order(self, cr, uid, po_number, delmar_id, context=None):
        if not context:
            context = {}

        # cancel Order in MS
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)

        params = {
            'po_number': po_number,
            'delmar_id': delmar_id,
        }
        sql = """
            UPDATE wp_1line
            SET PO_item_status = 'Order Rejected'
            WHERE PO_number = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s;

            UPDATE wp_gem
            SET PO_item_status = 'Order Rejected'
            WHERE OrderNumber = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s and PO_item_status = 'Ordered';

            UPDATE DiamondOrd
            SET status = 'rejected'
            WHERE PO_number = %(po_number)s and PO_product_ID_delmar = %(delmar_id)s and status = 'new';
        """

        answer = server_data.make_query(
                cr, uid, server_id,
                sql,
                params=params,
                select=False,
                commit=True,
        )
        return answer is not None

    def cancel_order_line(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        result = True

        po_id = vals['po_id']
        product_id = vals['product_id']
        po_numb = vals['po_number']
        delmar_id = vals['delmar_id']

        # cancel Order in MS
        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        mssql_conn = server_data.connect(cr, uid, server_id)

        if not mssql_conn:
            return False

        mssql_curs = mssql_conn.cursor()
        try:
            # changes PO line state to approved
            vals['status'] = 'new'
            self.change_po_state(cr, uid, vals, mssql_conn, mssql_curs)

            result = self.delete_ms_order_line(cr, uid, po_numb, delmar_id, server_data, server_id, mssql_conn, mssql_curs)
            if result:
                result = self.delete_ms_empty_order(cr, uid, po_numb, server_data, server_id, mssql_conn, mssql_curs)
            if result:
                # Clears rec_id
                line_obj = self.pool.get('purchase.order.line')
                po_line_ids = line_obj.search(cr, uid, [
                    ('order_id', '=', po_id),
                    ('product_id', '=', product_id)
                ])
                if po_line_ids:
                    component_obj = self.pool.get('po.components.history')
                    component_ids = component_obj.search(cr, uid, [('po_line_id', 'in', po_line_ids)])
                    if component_ids:
                        component_obj.write(cr, uid, component_ids, {'rec_id': None})
            else:
                raise osv.except_osv(('Error !'), ('MSSQL action error.'))
        except:
            mssql_conn.rollback()
            raise
        else:
            mssql_conn.commit()
        finally:
            mssql_conn.close()

        return result

    def change_order_data(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        result = True
        if vals:
            if 'order' in vals and vals['order'] and 'po_id' in vals['order'] and vals['order']['args']:
                warehouse = vals['order']['args'].get('warehouse_id') or False
                partner_id = vals['order']['args'].get('partner_id') or False
                vals['order']['args']['release_date'] = vals['order']['args'].get('release_date') or None
                if warehouse:
                    warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [
                        ('name', '=', warehouse)
                    ])
                    if warehouse_ids:
                        vals['order']['args']['warehouse_id'] = warehouse_ids[0]
                    else:
                        vals['order']['args'].pop('warehouse_id')
                if partner_id:
                    partner_address_ids = self.pool.get('res.partner.address').search(cr, uid, [
                        ('partner_id', '=', int(partner_id))
                    ])
                    if partner_address_ids:
                        vals['order']['args'].update({'partner_address_id': partner_address_ids[0]})
                if not partner_id:
                    vals['order']['args'].pop('partner_id')

                # Update order
                try:
                    self.write(cr, uid, vals['order']['po_id'], vals['order']['args'], context=context)
                except Exception, e:
                    result = False

            if 'line' in vals and vals['line'] and 'po_id' in vals['line'] and 'product_id' in vals['line'] and vals['line']['args']:
                product_id = vals['line']['product_id'] or False
                po_id = vals['line']['po_id'] or False
                sizes = []
                if 'customer_ids' in vals['line']['args'] and vals['line']['args']['customer_ids']:
                    customer_ids = self.pool.get('res.partner').search(cr, uid, [('ref', 'in', vals['line']['args']['customer_ids'])])
                    vals['line']['args']['partner_ids'] = [(6, 0, customer_ids)]
                    vals['line']['args'].pop('customer_ids')
                if 'product_qty' in vals['line']['args'] and vals['line']['args']['product_qty']:
                    if isinstance(vals['line']['args']['product_qty'], (int, long)):
                        sizes = {'no_size': vals['line']['args']['product_qty']}
                    else:
                        sizes = vals['line']['args']['product_qty']
                        vals['line']['args'].pop('product_qty')

                po = self.browse(cr, uid, po_id)

                for size_name in sizes:
                    size_id = None
                    size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                    if size_ids:
                        size_id = size_ids[0]
                    vals['line']['args'].update({'product_qty': sizes[size_name]})
                    vals['line']['args'].update({'size_id': size_id})

                    create_uid = po.create_uid and po.create_uid.id
                    vals['line']['args'].update({'order_id': po_id})
                    vals['line']['args'].update({'partner_id': po and po.partner_id and po.partner_id.id or False})
                    vals['line']['args'].update({'product_id': product_id})
                    # vals['line']['args'].update({'status': 'new'})

                    exist_line_ids = self.pool.get('purchase.order.line').search(cr, uid, [
                        ('order_id', '=', po_id),
                        ('product_id', '=', product_id),
                        ('size_id', '=', size_id)
                    ])
                    try:
                        if exist_line_ids:
                            po_line_id = _get_one(exist_line_ids)
                            self.pool.get('purchase.order.line').write(cr, uid, po_line_id, vals['line']['args'], context=context)
                        else:
                            po_line_id = self.pool.get('purchase.order.line').create(cr, uid, vals['line']['args'], context=context)
                        self.pool.get('purchase.order.line').write(cr, uid, po_line_id, {
                            'create_uid': create_uid
                        })

                    except Exception, e:
                        result = False

                if 'components' in vals and vals['components']:
                    for component in vals['components']:
                        if 'po_id' in component and 'component_id' in component and component['args']:
                            component_id = component['component_id'] or False
                            po_id = component['po_id'] or False
                            exist_line_ids = self.pool.get('purchase.order.line').search(cr, uid, [
                                ('order_id', '=', po_id),
                                ('product_id', '=', product_id)
                            ])
                            history_ids = self.pool.get('po.components.history').search(cr, uid, [
                                ('po_id', '=', po_id),
                                ('component_id', '=', component_id),
                                ('po_line_id', 'in', exist_line_ids)
                            ]) or False

                            if history_ids:
                                try:
                                    self.pool.get('po.components.history').write(cr, uid, history_ids, component['args'], context=context)
                                except Exception, e:
                                    logger.error(e)
                                    result = False

        return result

    def update_qty(self, cr, uid, vals, context=None):
        """
        TODO: [YT] need to add size to product if there is no one (into product_ring_size_rel)
        TASK: http://redmine.pfrus.com/issues/21279
        """
        if not context:
            context = {}
        result = False
        if vals:
            po_line_obj = self.pool.get('purchase.order.line')
            history_obj = self.pool.get('po.components.history')
            po_id = vals.get('po_id', None)
            product_id = vals.get('product_id', None)
            set_qty = vals.get('qty', None)
            size_name = vals.get('size', None)
            if po_id and product_id and set_qty:
                po = self.browse(cr, uid, po_id)
                create_uid = po.create_uid and po.create_uid.id or None
                set_qty = float(set_qty)
                manually_changed_qty = False
                params = [
                    ('order_id', '=', po_id),
                    ('product_id', '=', product_id),
                ]
                if size_name:
                    size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                    size_id = _get_one(size_id)
                    params.append(('size_id', '=', size_id))

                line_id = po_line_obj.search(cr, uid, params)
                if line_id:
                    po_line_obj.write(cr, uid, line_id, {'product_qty': set_qty})
                    res = po_line_obj.read(cr, uid, line_id, ['manually_changed_component_qty_flag'])
                    res = _get_one(res)
                    if res:
                        manually_changed_qty = res['manually_changed_component_qty_flag']
                elif size_name:
                    exist_line_id = po_line_obj.search(cr, uid, [
                        ('order_id', '=', po_id),
                        ('product_id', '=', product_id),
                    ])
                    exist_line_id = _get_one(exist_line_id)
                    _default = {
                        'product_qty': set_qty,
                        'size_id': size_id
                    }
                    new_line_id = po_line_obj.copy(cr, create_uid, exist_line_id, default=_default)
                    res = po_line_obj.read(cr, uid, exist_line_id, ['manually_changed_component_qty_flag'])
                    res = _get_one(res)
                    if res:
                        manually_changed_qty = res['manually_changed_component_qty_flag']
                    relation_ids = self.get_po_relation_ids(cr, uid, exist_line_id)
                    if relation_ids:
                        for relation_id in relation_ids:
                            if relation_id['children_po_line_id'] == exist_line_id and relation_id['parent_product_id'] != product_id:
                                relation_id['children_po_line_id'] = new_line_id
                                self.create_relation(cr, uid, relation_id, context)
                                break
                            if relation_id['parent_po_line_id'] == exist_line_id and relation_id['children_product_id'] != product_id:
                                relation_id['parent_po_line_id'] = new_line_id
                                self.create_relation(cr, uid, relation_id, context)
                                break

                # Update qty_of_sets on components
                if not manually_changed_qty:
                    # Getting total qty of this product (needed for sizes)
                    line_ids = po_line_obj.search(cr, uid, [
                        ('order_id', '=', po_id),
                        ('product_id', '=', product_id)
                    ])
                    all_qty = po_line_obj.read(cr, uid, line_ids, ['product_qty'])
                    total_qty = sum(map(lambda x: x.get('product_qty'), all_qty)) or 0
                    component_ids = history_obj.search(cr, uid, [
                        ('po_id', '=', po_id),
                        ('product_id', '=', product_id)
                    ])
                    history_obj.write(cr, uid, component_ids, {'qty_of_sets': total_qty})

                result = True

        return result

    def create_po_onhold_task(self, cr, uid, po_id, context=None):
        if not po_id:
            return False

        if self.pool.get('stock.onhold.task').search(cr, uid, [('po_id', '=', po_id)]):
            return False

        po = self.read(cr, uid, po_id, ['name', 'release_date', 'wh_location', 'reserved', 'warehouse_id', 'line_partner_id'])
        if not po:
            return False

        required_fields = ['release_date', 'wh_location', 'reserved', 'warehouse_id']

        for key in required_fields:
            if not po.get(key, False):
                return False

        onhold_location_id = self.pool.get('stock.location').search(
            cr, uid, [
                ('name', '=ilike', po['wh_location']),
                ('warehouse_id', '=', po['warehouse_id'][0])
            ],
            limit=1)
        if not onhold_location_id:
            logger.error("Can't find location {location} in {wh} warehouse".format(location=po['wh_location'][0], wh=po['warehouse_id']))
            return False
        else:
            onhold_location_id = onhold_location_id[0]

        wh = self.pool.get('stock.warehouse').browse(cr, uid, po['warehouse_id'][0])
        release_location_id = wh.lot_release_id and wh.lot_release_id.id or None
        if not release_location_id:
            return False

        onhold_data = {
            'name': "{po_name}_{prefix}".format(po_name=po['name'], prefix=time.strftime('%Y%m%d_%H%M%S', time.gmtime())),
            'date_to': po['release_date'],
            'warehouse_id': wh.id,
            'location_id': release_location_id,
            'location_dest_id': onhold_location_id,
            'partner_id': po['line_partner_id'] and po['line_partner_id'][0] or None,
            'po_id': po_id,
            'manager_id': uid,
        }

        task_obj = self.pool.get("stock.onhold.task")
        task_id = task_obj.create(cr, uid, onhold_data, context=context)
        if not task_id:
            raise osv.except_osv('Error !', "Can't create OnHold task.")

        task_obj.fill_lines_from_po(cr, uid, task_id)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'stock.onhold.task', task_id, 'button_po_confirm', cr)

        return True

    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        history_obj = self.pool.get('po.components.history')
        components_ids = history_obj.search(cr, uid, [
            ('po_id', 'in', ids)
        ])
        if components_ids:
            history_obj.unlink(cr, uid, components_ids, context=None)

        return super(purchase_order, self).unlink(cr, uid, ids, context=context)

purchase_order()


class purchase_order_line(osv.osv):
    _name = 'purchase.order.line'
    _inherit = 'purchase.order.line'

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('created', 'Created'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ]

    COLOR_SELECTION = [
        (None, ''),
        ('red', 'Red'),
    ]

    _columns = {
        'create_uid': fields.many2one('res.users', 'Creator', readonly=True, ),
        'partner_ids': fields.many2many(
            'res.partner',
            'purchase_order_line_partner',
            'pol_id',
            'partner_id',
            'Customers'
        ),
        'related_po_lines_ids': fields.many2many(
            'purchase.order.line',
            'po_lines_rel',
            'parent_po_line_id',
            'children_po_line_id',
            'Related PO Lines'
        ),
        'status': fields.char('Status', size=256, ),
        'supplier_instructions': fields.char('PO Supplier Instructions', size=256, ),
        'wp_data': fields.text('WP Data', ),
        'image_required': fields.boolean('Image Required', ),
        'video_required': fields.boolean('Video Required', ),
        'state': fields.selection(STATE_SELECTION, 'State', required=True, readonly=True, ),
        'price_unit': fields.float('Price Unit', ),
        'eta': fields.datetime('ETA'),
        'date_ordered': fields.date('Date Ordered'),
        'date_confirmed': fields.date('PO_date_confirmed'),
        'prod_cost': fields.float('prod_cost'),
        'prod_fcost': fields.float('Final cost'),
        'color_flag': fields.selection(COLOR_SELECTION, 'Color', ),
        'reference_notes': fields.char('PO Ref Note', size=512, ),
        'pricing_comments': fields.char('PO Pricing Comments', size=512, ),
        'promo_date': fields.datetime('Promo Date', ),
        'urgent': fields.boolean('Urgent', ),
        'complete_qty_to_receive': fields.integer('Complete QTY to receive', ),

        # Catalog fields
        'manually_changed_component_qty_flag': fields.boolean('Manually Changed Component QTY'),
        'components_counter': fields.integer('Count Of Components', ),
        'mount_only': fields.boolean('Mount Only', ),
        'sample': fields.boolean('Sample flag'),
        'supp_internal_notes': fields.text('Supplier Internal Notes', ),
    }

    _default = {
        'color_flag': False,
        'manually_changed_component_qty_flag': False,
        'sample': False,
        'complete_qty_to_receive': 0,
    }

    def update_ms_order_line(self, cr, uid, po_number, delmar_id, vals=None, context=None):

        if not vals:
            return False

        params = {
            '__po_number__': po_number,
            '__delmar_id__': delmar_id,
        }

        params.update(vals)

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_po_servers(cr, uid, single=True)
        if not server_id:
            raise

        set_params = ['"{field}"=%({field})s'.format(field=x) for x in vals]

        update_sql = """UPDATE wp_1line SET {set_params}
                WHERE PO_number = %(__po_number__)s and PO_product_ID_delmar = %(__delmar_id__)s
        """.format(set_params=", ".join(set_params))

        answer = server_data.make_query(cr, uid, server_id, update_sql, params=params, select=False, commit=True)
        return answer is not None

    def update_line(self, cr, uid, erp_vals, ms_vals, context=None):
        if not context:
            context = {}
        if erp_vals or ms_vals:
            po_id = context.get('po_id', False)
            product_id = context.get('product_id', False)
            po_name = context.get('po_name', False)
            default_code = context.get('default_code', False)
            if po_id and product_id and erp_vals:
                po_line_ids = self.search(cr, uid, [
                    ('order_id', '=', int(po_id)),
                    ('product_id', '=', int(product_id)),
                ])
                if po_line_ids:
                    self.write(cr, uid, po_line_ids, erp_vals, context=context)

            if po_name and default_code and ms_vals:
                self.update_ms_order_line(cr, uid, po_name, default_code, ms_vals, context=None)

            return True

        return False

    def change_image_require_flag(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            po_id = vals.get('po_id') or False
            product_id = vals.get('product_id') or False
            image_required = vals.get('image_required') or False
            video_required = vals.get('video_required') or False

            if po_id and product_id:
                po_line_ids = self.search(cr, uid, [
                    ('order_id', '=', po_id),
                    ('product_id', '=', product_id),
                ])
                if po_line_ids:
                    update_dict = {'image_required': image_required,
                                   'video_required': video_required,
                                   }
                    self.write(cr, uid, po_line_ids, update_dict, context=context)
                    po_lines = self.browse(cr, uid, po_line_ids)
                    for item in (('video', video_required,), ('image', image_required,),):
                        text = 'Need New {} Yes'.format('Images' if item[0] == 'image' else 'Videos') if item[1] else ''

                        server_data = self.pool.get('fetchdb.server')
                        server_id = server_data.get_po_servers(cr, uid, single=True)
                        if not server_id:
                            raise
                        rec_sql = """UPDATE wp_1line SET Need_New_{0}_Text = ?, 
                                    {0}_Valid = ? 
                                    WHERE PO_number = ? 
                                    AND PO_product_ID_delmar = ?""".format('Images' if item[0] == 'image' else 'Videos')
                        default_code = po_lines[0].product_id and po_lines[0].product_id.default_code or False
                        po_name = po_lines[0].order_id and po_lines[0].order_id.name or False

                        params = (text, int(item[1]), po_name, default_code)
                        res = server_data.make_query(cr, uid, server_id, rec_sql, params=params, select=False, commit=True)
                        if not res:
                            raise

                    return True

        return False

    def change_field_val(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            po_id = vals.get('po_id') or False
            product_id = vals.get('product_id') or False
            field = vals.get('fld') or None
            val = vals.get('val') or None
            if po_id and product_id:
                po_line_ids = self.search(cr, uid, [
                    ('order_id', '=', int(po_id)),
                    ('product_id', '=', int(product_id)),
                ])
                if po_line_ids:
                    self.write(cr, uid, po_line_ids, {field: val}, context=context)

                    return True

        return False

    def change_generic_note(self, cr, uid, vals, context=None):
        if vals:
            status = vals.get('status') or None
            fld = vals.get('fld') or None
            if status and fld and self.change_field_val(cr, uid, vals, context=None):
                ms_update_status = ['created', 'ordered', 'partial', 'delivered', 'order rejected', 'repair', ]
                if fld == 'supplier_instructions' and status in ms_update_status:

                    po_number = vals.get('po_number') or None
                    delmar_id = vals.get('delmar_id') or None
                    val = vals.get('val') or None
                    note_vals = {
                        'PO_supplier_instruction': val,
                        'PO_supplier_notes': val,
                    }

                    res = self.update_ms_order_line(cr, uid, po_number, delmar_id, note_vals, context=None)
                    return res
        return False

    def change_color_flag(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            po_id = vals.get('po_id') or False
            product_id = vals.get('product_id') or False
            color_flag = vals.get('color_flag') or None
            if po_id and product_id:
                po_line_ids = self.search(cr, uid, [
                    ('order_id', '=', int(po_id)),
                    ('product_id', '=', int(product_id)),
                ])
                if po_line_ids:
                    self.write(cr, uid, po_line_ids, {'color_flag': color_flag}, context=context)

                    return True

        return False

    def change_component_confirm_flag(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        result = False
        if not (vals and 'rec_id' in vals and 'confirm_flag' in vals):
            return result
        rec_id = vals['rec_id']
        if not rec_id:
            return result
        confirm_flag = vals['confirm_flag']
        try:
            confirm_flag = int(confirm_flag)
        except:
            return False

        component_obj = self.pool.get('po.components.history')
        component_ids = component_obj.search(cr, uid, [
            ('rec_id', '=', rec_id)
        ])
        if component_ids:
            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_po_servers(cr, uid, single=True)
            if server_id:
                update_sql = """UPDATE wp_gem
                    SET Cost_confirm = ?
                    WHERE Rec_ID = ?
                """
                params = (confirm_flag, rec_id)
                ms_res = server_data.make_query(cr, uid, server_id, update_sql, params=params, select=False, commit=True)
                if ms_res is False:
                    raise osv.except_osv('Error !', 'MSSQL action error.')

            component_obj.write(cr, uid, component_ids, {'confirm_flag': confirm_flag}, context=context)
            result = True

        return result

    def change_confirm_flag(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if vals:
            po_id = vals.get('po_id') or False
            product_id = vals.get('product_id') or False
            po_confirmed = vals.get('po_confirmed') or True
            try:
                po_confirmed = int(po_confirmed)
            except:
                po_confirmed = 1

            po = self.pool.get('purchase.order').browse(cr, uid, po_id)
            po_name = po.name
            product = self.pool.get('product.product').browse(cr, uid, product_id)
            default_code = product.default_code
            line_ids = self.search(cr, uid, [
                ('order_id', '=', po_id),
                ('product_id', '=', product_id),
            ])
            if line_ids:
                line_objs = self.browse(cr, uid, line_ids)
                for line_obj in line_objs:
                    wp_data_raw = line_obj.wp_data
                    if wp_data_raw:
                        try:
                            wp_data = json_loads(wp_data_raw)
                            wp_data['PO_Confirm_Flag'] = po_confirmed
                            self.write(cr, uid, line_obj.id, {'wp_data': json_dumps(wp_data)})
                        except:
                            raise

                server_data = self.pool.get('fetchdb.server')
                server_id = server_data.get_po_servers(cr, uid, single=True)
                if server_id:
                    update_sql = """UPDATE wp_1line
                        SET PO_Confirm_Flag = ?
                        WHERE PO_number = ?
                        AND PO_product_ID_delmar = ?
                    """
                    params = (po_confirmed, po_name, default_code)
                    res = server_data.make_query(cr, uid, server_id, update_sql, params=params, select=False, commit=True)
                    if res is False:
                        raise

        return True

    def create(self, cr, uid, vals, context=None):
        po_size_obj = self.pool.get('po.ring.size')
        if not context:
            context = {}
        if 'ms_size_id' in vals:
            if vals['ms_size_id']:
                size_id = po_size_obj.search(cr, uid, [
                    ('ms_prefix', '=', str(vals['ms_size_id']))
                ])
                size_id = _get_one(size_id)
                size_obj = po_size_obj.browse(cr, uid, size_id)
                if not size_obj.is_null():
                    size_id = size_obj.size_id and size_obj.size_id.id
                    vals.update({'size_id': size_id})

            del vals['ms_size_id']

        size_id = vals.get('size_id', None)
        product_id = vals.get('product_id', None)

        # Add new size to product if not exist
        if size_id and product_id:
            self.pool.get('product.product').add_non_exist_size_to_product(cr, uid, size_id, product_id)

        new_line = super(purchase_order_line, self).create(cr, uid, vals, context=context)
        return new_line

purchase_order_line()
