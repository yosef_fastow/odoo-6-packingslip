# -*- coding: utf-8 -*-
from osv import fields, osv

class po_ring_size(osv.osv):
    _name = 'po.ring.size'
    _check_is_null_column = 'ms_prefix'

    _columns = {
        'ms_prefix': fields.char('MS Prefix', size=56, ),
        'size_id': fields.many2one('ring.size', 'Size relation', ),
    }

po_ring_size()
