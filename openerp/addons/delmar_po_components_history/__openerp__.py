{
    "name": "PO Components history",
    "version": "1.0.2",
    "author": "Shepilov Vladislav @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "purchase",
        "product",
        "openerp_nan_product_pack",
    ],
    "description": """
    History for added Components to Purchase Order
    """,
    "category": "Order Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'data/sequence_data.xml',
        'view/po_communications_view.xml',
        'view/po_communications_menu_view.xml',
        'view/po_components_history_view.xml',
        'view/po_components_history_menu_view.xml',
        'view/purchase_order_line_view.xml',
        'view/purchase_order_view.xml',
        'view/stock_warehouse_view.xml',
        'view/gold_silver_history.xml',
        'view/default_widgets.xml',
    ],
    'js': [],
    'test': [
        'test/create_bin.yml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
