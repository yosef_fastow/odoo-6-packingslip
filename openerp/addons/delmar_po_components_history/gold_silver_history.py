from osv import fields, osv
from tools.translate import _
import base64
import csv
from cStringIO import StringIO
from datetime import datetime


METAL_SELECTION = [
    ('gold', 'Gold'),
    ('silver', 'Silver'),
    ('platinum', 'Platinum'),
]


class gold_silver_history(osv.osv_memory):
    _name = 'gold.silver.history'

    def _available_qty_by_lines(self, cr, uid, lines):
        available = 0.0
        for line in lines:
            available += (line.qty or 0.0) - (line.used_qty or 0.0)
        return available

    def _available_qty(self, cr, uid, ids, name, args, context=None):
        res = {}
        for history in self.browse(cr, uid, ids, context=context):
            res[history.id] = self._available_qty_by_lines(cr, uid,
                                                           history.line_ids)
        return res

    _columns = {
        'metal': fields.selection(METAL_SELECTION, 'Gold/Silver', select=True),
        'line_ids': fields.many2many(
            'gold.silver.purchase', 'gold_silver_rel',
            'history_id', 'line_id', 'Lines',
        ),
        'available': fields.function(
            _available_qty,
            string='Available (g)',
            type='float',
            method=True,
        ),
        'partner_id': fields.many2one('res.partner', 'Supplier'),
        'saved': fields.boolean('Form is Saved'),
    }

    _defaults = {
        'saved': False,
    }

    def save_form(self, cr, uid, ids, context=None):
        history_id = ids[0] if isinstance(ids, list) else ids
        history_data = self.read(cr, uid, history_id, ['metal', 'partner_id'])
        metal = history_data.get('metal')
        partner_id = history_data.get('partner_id')
        if isinstance(partner_id, (list, tuple)):
            partner_id = partner_id[0]
        line_ids = self._get_lines(cr, uid, metal, partner_id)
        self.write(cr, uid, ids, {
            'saved': True,
            'line_ids': [(6, 0, line_ids)],
        }, context=context)
        return True

    def _get_lines(self, cr, uid, metal, partner_id=False):
        args = [('metal', '=', metal)]
        if partner_id:
            args.append(('partner_id', '=', partner_id))
        return self.pool.get('gold.silver.purchase').search(cr, uid, args)

    def onchange_get_lines(self, cr, uid, ids, partner_id, metal, context=None):
        line_ids = self._get_lines(cr, uid, metal, partner_id) if metal else []
        lines = self.pool.get('gold.silver.purchase').browse(cr, uid, line_ids)
        available = self._available_qty_by_lines(cr, uid, lines)
        if ids:
            self.write(cr, uid, ids, {
                'line_ids': [(6, 0, line_ids)],
                'partner_id': partner_id,
                'metal': metal,
            }, context=context)
        return {'value': {'line_ids': line_ids, 'available': available}}

gold_silver_history()


class gold_silver_purchase(osv.osv):
    _name = 'gold.silver.purchase'

    def _get_used_qty(self, cr, uid, ids, name, args, context=None):
        res = {}
        for purchase in self.browse(cr, uid, ids, context=context):
            res[purchase.id] = sum([log.qty or 0.0 for log in purchase.log_ids])
        return res

    def _get_log_purchase(self, cr, uid, ids, context=None):
        logs = self.pool.get('gold.silver.log').browse(cr, uid, ids)
        return [log.purchase_id.id for log in logs if log.purchase_id]

    _columns = {
        'create_uid': fields.many2one('res.users', 'User', readonly=True),
        'metal': fields.selection(METAL_SELECTION, 'Metal', select=True),
        'history_ids': fields.many2many(
            'gold.silver.history', 'gold_silver_rel',
            'line_id', 'history_id', 'Histories',
        ),
        'log_ids': fields.one2many('gold.silver.log', 'purchase_id', 'Log'),
        'qty': fields.float('Qty (g)',),
        'used_qty': fields.function(
            _get_used_qty,
            string='Used (g)',
            type="float",
            method=True,
            store={
                'gold.silver.purchase': (
                    lambda self, cr, uid, ids, context=None: ids,
                    ['log_ids'],
                    10,
                ),
                'gold.silver.log': (_get_log_purchase, ['purchase_id'], 10),
            },
        ),
        'price': fields.float('Price $/oz'),
        'partner_id': fields.many2one('res.partner', 'Supplier'),
        'purchase_date': fields.date('Purchase Date', select=True),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('used', 'Used'),
        ], 'State'),
    }

    _defaults = {
        'state': 'draft',
        'used_qty': 0,
    }

    _order = 'purchase_date desc'

    def create(self, cr, uid, vals, context=None):
        if not (context and context.get('force')):
            if not vals.get('qty') or vals.get('qty') < 0:
                raise osv.except_osv(_('Warning!'), 'Qty must be positive')
            if not vals.get('price') or vals.get('price') < 0:
                raise osv.except_osv(_('Warning!'), 'Price must be positive')
        return super(gold_silver_purchase, self).create(cr, uid, vals,
                                                        context=context)

    def confirm_purchase(self, cr, uid, ids, context=None):
        purchase_id = ids[0] if isinstance(ids, list) else ids
        purchase = self.browse(cr, uid, purchase_id)
        args = [
            ('metal', '=', purchase.metal),
        ]
        partner = purchase.partner_id
        if partner:
            args.append(('partner_id', 'in', [partner.id, False, None]))
        history_obj = self.pool.get('gold.silver.history')
        history_ids = history_obj.search(cr, uid, args, context=context)
        vals = {
            'state': 'confirmed',
            'history_ids': [(6, 0, history_ids)]
        }
        if purchase.qty < 0:
            vals['qty'] = abs(purchase.qty)
        if purchase.price < 0:
            vals['price'] = abs(purchase.price)
        self.write(cr, uid, ids, vals, context=context)
        return True

    def get_metals_price(self, cr, uid, partner_id=False, force=False):
        result = {}
        for metal in METAL_SELECTION:
            purchases = self.get_available_purchases(cr, uid, metal[0],
                                                     partner_id, force)
            result[metal[0]] = purchases and purchases[0].get('price') or 0
        return result

    def get_available_purchases(self, cr, uid, metal, partner_id=False, force=False):
        where = "AND partner_id = {0}".format(partner_id) if partner_id else ''
        cr.execute("""SELECT id, qty - coalesce(used_qty, 0) as qty, price
            FROM gold_silver_purchase
            WHERE metal = '{0}'
            AND coalesce(used_qty, 0) < qty
            {1}
            ORDER BY purchase_date
        """.format(metal, where))
        res = cr.dictfetchall()
        if force and not res:
            cr.execute("""SELECT id, qty - coalesce(used_qty, 0) as qty, price
                FROM gold_silver_purchase
                WHERE metal = '{0}'
                {1}
                ORDER BY purchase_date DESC
                LIMIT 1
            """.format(metal, where))
            res = cr.dictfetchall()
            if not res:
                vals = {
                    'metal': metal,
                    'qty': 0,
                    'price': 0,
                    'partner_id': partner_id,
                    'purchase_date': datetime.utcnow(),
                    'state': 'confirmed'
                }
                inserted_id = self.create(cr, uid, vals, context={'force': True})
                res = [{'id': inserted_id, 'qty': vals['qty'], 'price': vals['price']}]
        return res

    def get_used_purchases(self, cr, uid, metal, partner_id=False):
        where = ''
        if partner_id:
            where = """AND partner_id = {0}
            """.format(partner_id)
        cr.execute("""SELECT id, used_qty, price
            FROM gold_silver_purchase
            WHERE metal = '{0}'
            AND used_qty > 0
            {1}
            ORDER BY purchase_date desc
        """.format(metal, where))
        return [{'id': l[0], 'qty': l[1], 'price': l[2]} for l in cr.fetchall()]

gold_silver_purchase()


class gold_silver_log(osv.osv):
    _name = 'gold.silver.log'
    _columns = {
        'create_uid': fields.many2one('res.users', 'User', readonly=True),
        'create_date': fields.datetime('Date and Time', readonly=True),
        'purchase_id': fields.many2one('gold.silver.purchase', 'Purchase'),
        'qty': fields.float('Qty (g)'),
        'reason': fields.text('Reason', required=True),
        'price': fields.float('Price $/g'),
        'change_id': fields.many2one('gold.silver.change', 'Change'),
    }

gold_silver_log()


class gold_silver_change(osv.osv):
    _name = 'gold.silver.change'
    _columns = {
        'qty': fields.float(
            'Add qty to the Used Qty (g)',
            help="Negative value will mean returning gold/silver to reuse",
        ),
        'reason': fields.text('Reason', required=True),
        'price': fields.float('Price $/g', readonly=True),
        'completed': fields.boolean('Completed', readonly=True),
        'metal': fields.selection(METAL_SELECTION, 'Metal', required=True),
        'log_ids': fields.one2many('gold.silver.log', 'change_id', 'Log Lines'),
    }

    _defaults = {
        'completed': False,
    }

    # Can be used via xml-rpc
    def add_logs(self, cr, uid, qty, metal, reason, change_id=False, partner_id=False, force=False):
        purchase_obj = self.pool.get('gold.silver.purchase')
        log_obj = self.pool.get('gold.silver.log')
        if qty == 0:
            raise osv.except_osv(_('Warning!'), 'Qty can\'t be 0')
        is_return = qty < 0
        sign = -1 if is_return else 1
        if is_return:
            purchases = purchase_obj.get_used_purchases(cr, uid, metal, partner_id)
        else:
            purchases = purchase_obj.get_available_purchases(cr, uid, metal, partner_id, force)
        # available_qty = sum(purchase['qty'] for purchase in purchases)
        # if available_qty < abs(qty):
        #     error_tmpl = 'Not enough qty. Only {0} g. are {1}'
        #     used_or_available = 'used' if is_return else 'available'
        #     error_msg = error_tmpl.format(available_qty, used_or_available)
        #     if partner_id:
        #         error_msg += ' for partner_id {0}'.format(partner_id)
        #     raise osv.except_osv(_('Error!'), error_msg)
        qty_left = qty
        list_vals = []
        price = None
        last_purchase = purchases[-1] if len(purchases) > 0 else None
        for purchase in purchases:
            qty_to_log = min(purchase['qty'], abs(qty_left)) * sign
            if last_purchase['id'] == purchase['id']:
                qty_to_log = abs(qty_left) * sign
            list_vals.append({
                'purchase_id': purchase['id'],
                'qty': qty_to_log,
                'reason': reason,
                'change_id': change_id,
                'price': purchase['price'] or 0.0,
            })
            qty_left -= qty_to_log
            # can't use "qty_left == 0" because of python float is inaccurate
            if qty_left * sign < 0.000001: # accurancy = 1 microgram
                break
        if len(list_vals) > 1:
            i = 0
            # latest used price. There's index [0] if return and [-1] if usage
            price = list_vals[-(not is_return)]['price']
            for vals in list_vals:
                i += 1
                vals['reason'] += ' [portion {0}/{1}, real price: ${2}]'.format(
                    i,
                    len(list_vals),
                    vals['price'],
                )
                vals['price'] = price
        for vals in list_vals:
            new_id = log_obj.create(cr, uid, vals)
            vals['id'] = new_id
        return list_vals

    def confirm_use(self, cr, uid, ids, context=None):
        change_id = ids[0] if isinstance(ids, list) else ids
        change = self.browse(cr, uid, change_id)
        self.add_logs(cr, uid,
                      change.qty, change.metal, change.reason, change_id)
        self.write(cr, uid, ids, {'completed': True}, context=context)
        return True

gold_silver_change()


class gold_silver_export(osv.osv_memory):
    _name = 'gold.silver.export'
    _columns = {
        "csv_file": fields.binary(string="Export to CSV", readonly=True),
        "csv_filename": fields.char("", size=256),
        'done': fields.boolean("Done")
    }

    _defaults = {
        'done': False,
        'csv_file': ''
    }

    def action_gold_silver_export(self, cr, uid, ids, context=None):
        history_ids = context.get('active_ids', [])
        history_id = history_ids[0] if isinstance(ids, list) else history_ids
        history_obj = self.pool.get('gold.silver.history')
        history = history_obj.browse(cr, uid, history_id)
        header = [
            'User',
            'Purchase Date',
            'Supplier',
            'Metal',
            'Qty (g)',
            'Used (g)',
            'Price $/oz',
        ]
        result = [header]
        for line in history.line_ids:
            result.append([
                line.create_uid and line.create_uid.name,
                line.purchase_date,
                line.partner_id and line.partner_id.name,
                line.metal,
                line.qty,
                line.used_qty,
                line.price,
            ])
        fp = StringIO()
        writer = csv.writer(fp, quotechar='"', delimiter=',',
                            quoting=csv.QUOTE_ALL, lineterminator='\n')
        def covert_to_str(value):
            return (el if type(el) is unicode else str(el)).encode('utf-8')
        for data in result:
            row = [covert_to_str(el) for el in data]
            writer.writerow(row)
        fp.seek(0)
        data = fp.read()
        fp.seek(0)
        fp.truncate()
        fp.close()
        return self.write(cr, uid, ids, {
            'done': True,
            'csv_file': base64.encodestring(data),
            'csv_filename': 'MetalHistory.csv'
        }, context=context)

gold_silver_export()