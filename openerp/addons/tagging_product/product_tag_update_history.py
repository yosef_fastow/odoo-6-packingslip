from osv import osv, fields


class product_tag_update_history(osv.osv):
    _name = 'product.tag.update.history'

    _columns = {
        'user_responsible': fields.char("User", size=64, readonly=True),
        'update_date': fields.datetime("Updated Date", readonly=True),
        'product_id': fields.many2one("product.product", "Product"),
        'res_id': fields.many2one('tagging.tags', 'Tag'),
        'description': fields.text('Activity'),
    }


    _order = "id desc"

product_tag_update_history()


class tagging_history_update(osv.osv):
    _inherit = "tagging.tags"
    _name = _inherit

    _columns = {
        'product_tag_history_ids': fields.one2many("product.tag.update.history", "res_id", "product_id"),
    }
    
tagging_history_update()