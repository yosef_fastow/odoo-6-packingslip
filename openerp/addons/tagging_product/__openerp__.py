# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP Module
#
#    Author Yuri Ivanenko
#
##############################################################################
{
    "name": "Tagging (Product)",
    "version": "1.0",
    "depends": [
        "tagging",
        "product",
        "sale",
        "crm",
        "delmar_products",
        "pf_utils"
     ],
    "author": "Yuri Ivanenko, progforce.com",
    "category": "Tools",
    "init_xml": [],
    "description": "Tags Products: Extend Tagging Module",
    'update_xml': [
        "view/tagging_view.xml",
        "view/product_tag_update_history_view.xml",
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
