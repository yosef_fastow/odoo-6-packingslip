# -*- coding: utf-8 -*-

from osv import osv, fields
import re
from osv import expression
import logging
import base64
import csv
from cStringIO import StringIO
import time
import datetime

_logger = logging.getLogger(__name__)

class taggings_product(osv.osv):
    _inherit = "tagging.tags"
    _name = _inherit

    def _get_latest_touch_uid(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for rec in self.browse(cr, uid, ids, context=context):
            create_uid = rec.create_uid and rec.create_uid.id or False
            write_uid = rec.write_uid and rec.write_uid.id or False
            result[rec.id] = write_uid or create_uid
        return result

    _columns = {
        "name": fields.char("Tag", size=512, required=True, translate=True),
        "product_ids": fields.many2many("product.product", "taggings_product", "tag_id", "product_id", string="Products"),
        "product_list": fields.text("Delmar ids list"),
        "result_info": fields.text("Result Info"),
        "product_list_confirm": fields.boolean("Add ids"),
        "product_list_remove": fields.boolean("Remove ids"),
        "product_list_search": fields.boolean("Search ids"),
        "create_uid": fields.many2one('res.users', 'Created by', readonly=True, ),
        "write_uid": fields.many2one('res.users', "Modify By", readonly=True, ),
        "latest_touch_uid": fields.function(
            _get_latest_touch_uid,
            type='many2one',
            relation='res.users',
            string='Latest Touch User',
            store=True,
        ),
        'history_csv_document': fields.binary(string="Export to CSV", readonly=True),
        'history_csv_document_name': fields.char("", size=256, ),
        'done': fields.boolean("Done"),
        "product_search": fields.char("Product", required=False, size=32, store=False)
    }

    def search(self, cr, uid, args, *arguments, **key_arguments):
        for arg in args:
            if arg and isinstance(arg, list):
                if arg[0] == 'latest_touch_uid':
                    arg_index = args.index(arg)
                    args1 = [('create_uid', arg[1], arg[2])]
                    args2 = [('write_uid', arg[1], arg[2])]
                    new_arg = expression.OR((args1, args2))
                    args = args[:arg_index] + new_arg + args[arg_index + 1:]
                # search for tags with defined product
                if arg[0] == 'product_search':
                    # try to find product by default code
                    prod_obj = self.pool.get('product.product')
                    prod_ids = prod_obj.search(cr, uid, [('default_code', '=', arg[2].strip()), ('type', '=', 'product')])
                    if not prod_ids:
                        continue
                    # update search request
                    query = """
                        SELECT distinct tt.id
                          FROM taggings_product tp
                          LEFT JOIN tagging_tags tt ON tt.id=tp.tag_id
                        WHERE tp.product_id={}
                          AND tt.create_date >= (NOW() - INTERVAL '18 months')  
                    """.format(prod_ids[0])
                    cr.execute(query)
                    all_ids = [x.get('id') for x in cr.dictfetchall()]
                    if all_ids:
                        arg_index = args.index(arg)
                        prod_arg = [('id', 'in', all_ids)]
                        args = args[:arg_index] + prod_arg + args[arg_index + 1:]

        return super(taggings_product, self).search(cr, uid, args, *arguments, **key_arguments)

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, uid, [], context=context)
        lst = [
            unicode(x.name).strip().lower() for x in self.browse(cr, uid, sr_ids, context=context)
            if x.name and x.id not in ids
        ]
        for row in self.browse(cr, uid, ids, context=context):
            name = unicode(row.name) if row and row.name else u'None'
            if (name.strip().lower() in lst):
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Name for Tag must be unique!', ['name'])]

    def onchange_product_list(self, cr, uid, ids, product_list, product_ids, product_tag_history_ids, context=None):
        if product_list:
            product_obj = self.pool.get('product.product')
            ids_list = re.split(r'[;,\s]', product_list or '')
            ids_list = [el.strip() for el in set(ids_list) if el]

            ids_list = self.get_products_by_cf_value(cr, ids_list)

            d_res = product_obj.search(cr, uid, ['&', '|', ('default_code', 'in', ids_list), ('id_delmar', 'in', ids_list), ('type', '=', 'product')], context=context)
            product_ids_set = set(product_ids[0][2])
            d_res_set = set(d_res)

            found_products = product_obj.read(cr, uid, d_res, ['default_code', 'id_delmar', 'id'])
            found_default_codes = []
            found_ids_delmar = []
            for product in found_products:
                found_default_codes.append(product.get('default_code'))
                found_ids_delmar.append(product.get('id_delmar'))
            not_found = set([id_list for id_list in ids_list if id_list not in found_default_codes and id_list not in found_default_codes])

            were_in_tag_ids = list(product_ids_set & d_res_set)
            were_in_tag_products = [product for product in found_products if product.get('id') in were_in_tag_ids]
            were_in_tag_default_codes = []
            were_in_tag_ids_delmar = []
            for product in were_in_tag_products:
                were_in_tag_default_codes.append(product.get('default_code'))
                were_in_tag_ids_delmar.append(product.get('id_delmar'))
            were_in_tag = set([id_list for id_list in ids_list if id_list in were_in_tag_default_codes or id_list in were_in_tag_ids_delmar])

            add_in_tag_ids = list(d_res_set - product_ids_set)
            add_in_tag_products = [product for product in found_products if product.get('id') in add_in_tag_ids]
            add_in_tag_default_codes = []
            add_in_tag_ids_delmar = []
            for product in add_in_tag_products:
                add_in_tag_default_codes.append(product.get('default_code'))
                add_in_tag_ids_delmar.append(product.get('id_delmar'))
            add_in_tag = set([id_list for id_list in ids_list if id_list in add_in_tag_default_codes or id_list in add_in_tag_ids_delmar])

            product_ids[0][2] = list(product_ids_set | d_res_set)

            result_info = ''
            if not_found:
                result_info += "%d items not found in system; " % (len(not_found),)
            if were_in_tag:
                result_info += "%d items were already in tag; " % (len(were_in_tag),)
            result_info += "%d items has been added in tag.\n\n" % (len(add_in_tag),)
            if not_found:
                result_info += 'Not found in system:\n' + '\n'.join(not_found) + '\n\n'
            if were_in_tag:
                result_info += 'Were in tag:\n' + '\n'.join(were_in_tag) + '\n\n'
            if add_in_tag:
                result_info += 'New in tag:\n' + '\n'.join(add_in_tag)

            # record product tag history
            if ids:
                for prod_id in add_in_tag_ids:
                    product_tag_history_ids.append((0, 0, {
                        'res_id': self.pool.get('tagging.tags').browse(cr,uid,ids)[0].id,
                        'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                        'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                        'product_id': prod_id,
                        'description': "Item has been added in tag.",
                    }))
            if not ids:
                product_tag_history_ids.append((0, 0, {
                    'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                    'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'description': "Tag created.",
                }))
            return {'value': {'product_ids': product_ids, 'product_tag_history_ids': product_tag_history_ids, 'product_list_confirm': False, 'product_list': '', 'result_info': result_info}}
        return {}

    def onchange_product_list_remove(self, cr, uid, ids, product_list, product_ids, product_tag_history_ids, context=None):
        if product_list:
            product_obj = self.pool.get('product.product')
            ids_list = re.split(r'[;,\s]', product_list or '')
            ids_list = [el.strip() for el in set(ids_list) if el]

            ids_list = self.get_products_by_cf_value(cr, ids_list)

            d_res = self.pool.get('product.product').search(cr, uid, ['&', '|', ('default_code', 'in', ids_list), ('id_delmar', 'in', ids_list), ('type', '=', 'product')], context=context)
            product_ids_set = set(product_ids[0][2])
            d_res_set = set(d_res)

            found_products = product_obj.read(cr, uid, d_res, ['default_code', 'id_delmar', 'id'])
            found_default_codes = []
            found_ids_delmar = []
            for product in found_products:
                found_default_codes.append(product.get('default_code'))
                found_ids_delmar.append(product.get('id_delmar'))
            not_found = set([id_list for id_list in ids_list if id_list not in found_default_codes and id_list not in found_default_codes])

            remove_in_tag_ids = list(product_ids_set & d_res_set)
            remove_in_tag_products = [product for product in found_products if product.get('id') in remove_in_tag_ids]
            remove_in_tag_default_codes = []
            remove_in_tag_ids_delmar = []
            for product in remove_in_tag_products:
                remove_in_tag_default_codes.append(product.get('default_code'))
                remove_in_tag_ids_delmar.append(product.get('id_delmar'))
            remove_in_tag = set([id_list for id_list in ids_list if id_list in remove_in_tag_default_codes or id_list in remove_in_tag_ids_delmar])

            not_in_tag_ids = list(d_res_set - product_ids_set)
            not_in_tag_products = [product for product in found_products if product.get('id') in not_in_tag_ids]
            not_in_tag_default_codes = []
            not_in_tag_ids_delmar = []
            for product in not_in_tag_products:
                not_in_tag_default_codes.append(product.get('default_code'))
                not_in_tag_ids_delmar.append(product.get('id_delmar'))
            not_in_tag = set([id_list for id_list in ids_list if id_list in not_in_tag_default_codes or id_list in not_in_tag_ids_delmar])

            product_ids[0][2] = list(product_ids_set - d_res_set)

            result_info = ''
            if not_found:
                result_info += "%d items not found in system; " % (len(not_found),)
            if not_in_tag:
                result_info += "%d items weren't in tag; " % (len(not_in_tag),)
            result_info += "%d items to remove from tag.\n\n " % (len(remove_in_tag),)
            if not_found:
                result_info += 'Not found in system:\n' + '\n'.join(not_found) + '\n\n'
            if not_in_tag:
                result_info += "Weren't tag:\n" + '\n'.join(not_in_tag) + '\n\n'
            if remove_in_tag:
                result_info += 'To remove from tag:\n' + '\n'.join(remove_in_tag)

            # record product tag history
            for prod_id in remove_in_tag_ids:
                product_tag_history_ids.append((0, 0, {
                    'res_id': self.pool.get('tagging.tags').browse(cr,uid,ids)[0].id,
                    'user_responsible': self.pool.get('res.users').browse(cr, uid, uid).name or '',
                    'update_date': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'product_id': prod_id,
                    'description': "Item has been removed from tag.",
                }))
            return {'value': {'product_ids': product_ids, 'product_tag_history_ids': product_tag_history_ids, 'product_list_confirm': False, 'product_list': '', 'result_info': result_info}}
        return {}

    def onchange_product_list_search(self, cr, uid, ids, product_list, product_ids, context=None):
        if product_list:
            product_obj = self.pool.get('product.product')
            ids_list = re.split(r'[;,\s]', product_list or '')
            ids_list = [el.strip() for el in set(ids_list) if el]

            ids_list = self.get_products_by_cf_value(cr, ids_list)

            d_res = self.pool.get('product.product').search(cr, uid, ['&', '|', ('default_code', 'in', ids_list), ('id_delmar', 'in', ids_list), ('type', '=', 'product')], context=context)
            product_ids_set = set(product_ids[0][2])
            d_res_set = set(d_res)

            found_products = product_obj.read(cr, uid, d_res, ['default_code', 'id_delmar', 'id'])
            found_default_codes = []
            found_ids_delmar = []
            for product in found_products:
                found_default_codes.append(product.get('default_code'))
                found_ids_delmar.append(product.get('id_delmar'))
            not_found = set([id_list for id_list in ids_list if id_list not in found_default_codes and id_list not in found_default_codes])

            in_tag_ids = list(product_ids_set & d_res_set)
            in_tag_products = [product for product in found_products if product.get('id') in in_tag_ids]
            in_tag_default_codes = []
            in_tag_ids_delmar = []
            for product in in_tag_products:
                in_tag_default_codes.append(product.get('default_code'))
                in_tag_ids_delmar.append(product.get('id_delmar'))
            in_tag = set([id_list for id_list in ids_list if id_list in in_tag_default_codes or id_list in in_tag_ids_delmar])

            not_in_tag_ids = list(d_res_set - product_ids_set)
            not_in_tag_products = [product for product in found_products if product.get('id') in not_in_tag_ids]
            not_in_tag_default_codes = []
            not_in_tag_ids_delmar = []
            for product in not_in_tag_products:
                not_in_tag_default_codes.append(product.get('default_code'))
                not_in_tag_ids_delmar.append(product.get('id_delmar'))
            not_in_tag = set([id_list for id_list in ids_list if id_list in not_in_tag_default_codes or id_list in not_in_tag_ids_delmar])

            product_ids[0][2] = list(product_ids_set - d_res_set)

            result_info = ''
            if not_found:
                result_info += "%d items not found in system; " % (len(not_found),)
            if not_in_tag:
                result_info += "%d items are not in tag; " % (len(not_in_tag),)
            result_info += "%d items are in tag.\n\n" % (len(in_tag),)
            if not_found:
                result_info += 'Not found in system:\n' + '\n'.join(not_found) + '\n\n'
            if not_in_tag:
                result_info += 'Not in tag:\n' + '\n'.join(not_in_tag) + '\n\n'
            if in_tag:
                result_info += 'In tag:\n' + '\n'.join(in_tag)
            return {'value': {'product_list_confirm': False, 'product_list': '', 'result_info': result_info}}
        return {}

    def get_products_by_cf_value(self, cr, values):
        """
        Get input value and search products by customer fields value
        :param cr: 
        :param values: 
        :return: list of product`s default_code
        """
        join_values = "', '".join(values)
        sql = '''
        SELECT distinct  pp.default_code AS default_code
         FROM product_product pp inner join product_code pc on pp.id=pc.product_id
         WHERE pc.name IN ('%s') and pc.priority in (2,10,11,12)
        ''' % (join_values)
        cr.execute(sql)
        products = cr.fetchall()
        product_list = []
        [product_list.extend(list(product)) for product in products]
        return product_list


    def create(self, cr, uid, vals=None, context=None):
        if ('name' in vals):
            vals.update({
                'name': unicode(vals['name']).strip()
            })
        return super(taggings_product, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if ('name' in vals):
            vals.update({
                'name': unicode(vals['name']).strip()
            })
        return super(taggings_product, self).write(cr, uid, ids, vals, context=context)

    # DELMAR-92
    # export product tag history into csv
    def export(self, cr, uid, ids, context=None):
        tag_id = ids[0]
        tag_obj = self.pool.get('tagging.tags')
        tag = tag_obj.browse(cr, uid, tag_id)

        header = [
            'Tag Id',
            'User',
            'Updated Date',
            'Delmar Id',
            'Activity',
        ]
        result = [header]
        for line in tag.product_tag_history_ids:
            if line.product_id:
                result.append([
                    line.res_id.name,
                    line.user_responsible,
                    line.update_date.split('.')[0],
                    self.pool.get('product.product').browse(cr, uid, self.pool.get('product.product').search(cr, uid, [
                    ('id', '=', line.product_id.id)]))[0].default_code,
                    line.description,
                ])
            else:
                result.append([
                    line.res_id.name,
                    line.user_responsible,
                    line.update_date.split('.')[0],
                    '',
                    line.description,
                ])
        fp = StringIO()
        writer = csv.writer(fp, quotechar='"', delimiter=',',
                            quoting=csv.QUOTE_ALL, lineterminator='\n')

        def covert_to_str(value):
            return (el if type(el) is unicode else str(el)).encode('utf-8')

        for data in result:
            row = [covert_to_str(el) for el in data]
            writer.writerow(row)
        fp.seek(0)
        data = fp.read()
        fp.seek(0)
        fp.truncate()
        fp.close()
        return self.write(cr, uid, ids, {
            'done': True,
            'history_csv_document': base64.encodestring(data),
            'history_csv_document_name': '%s.csv' % (tag.name)
        }, context=context)
    # END DELMAR-92

taggings_product()


class product_taggings(osv.osv):
    _inherit = "product.product"
    _name = _inherit

    _columns = {
        "tagging_ids": fields.many2many("tagging.tags", "taggings_product", "product_id", "tag_id", string="Tags"),
    }
product_taggings()


class crm_lead(osv.osv):
    _inherit = "crm.lead"

    _columns = {
        "tagging_id": fields.many2one("tagging.tags", "Tag"),
        "pricelist_id": fields.many2one("product.pricelist", "Pricelist"),
    }
crm_lead()
