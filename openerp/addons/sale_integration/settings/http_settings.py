# -*- coding: utf-8 -*-
from osv import fields, osv
from datetime import datetime, timedelta


class http_settings(osv.osv):
    _name = "http.settings"

    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'base_url': fields.char('Base URL', size=128, required=True),
        'authentication': fields.selection(
            (
                ('no_auth', 'No Auth'),
                ('base', 'Base'),
                ('HTTPBasicAuth', 'HTTPBasicAuth'),
                ('HTTPProxyAuth', 'HTTPProxyAuth'),
                ('HTTPDigestAuth', 'HTTPDigestAuth'),
            ),
            'Authentication', select=1, required=True,),
        'username': fields.char('Username', size=128, required=True),
        'password': fields.char('Password', size=128, required=True),
        'timeout': fields.float('Timeout', required=True, digits=(10, 2)),
        'verify': fields.boolean('Verify Certificate', ),
        'password_send_expiration': fields.boolean('Notify password expiration'),
        'password_renewed': fields.boolean('Password was renewed', help='Check if password was renewed after expiration'),
        'password_expiration_period': fields.integer('Expiration period, days', size=3, required=False),
        'password_renew_date': fields.datetime('Next renew date', required=False, readonly=True),
        'password_notification_emails': fields.char('Notification emails', size=256, required=False, help='Comma-separated list of email addresses'),
    }

    _defaults = {
        'authentication': 'no_auth',
        'timeout': 10.0,
        'verify': False,
        'password_send_expiration': False,
        'password_renewed': False,
        'password_expiration_period': 90
    }

    def write(self, cr, uid, ids, vals, context=None):
        if 'password_renewed' in vals and vals.get('password_renewed', False):
            for setting in self.browse(cr, uid, ids):
                next_expiration = setting.password_expiration_period
                if 'password_expiration_period' in vals:
                    next_expiration = vals.get('password_expiration_period', 0)
                if next_expiration > 0:
                    vals.update({
                        'password_renew_date': datetime.today() + timedelta(days=next_expiration),
                        'password_renewed': False
                    })
        return super(http_settings, self).write(cr, uid, ids, vals, context)

    def notify_password_expiration(self, cr, uid, recipients=[]):
        if not recipients:
            recipients = []
        ids = self.search(cr, uid, [('password_send_expiration', '=', True)])
        if len(ids) > 0:
            for setting in self.browse(cr, uid, ids):
                # recipients
                to_recipients = []
                if setting.password_notification_emails:
                    to_recipients = recipients + [r.strip() for r in setting.password_notification_emails.split(',')]
                # message template
                mail_message = {
                    'to_send': False,
                    'subtype': 'plain',
                    'from': 'erp.delmar@gmail.com',
                    'to': [r for r in to_recipients],
                    'subject': 'Password expiration {}',
                    'body': """
                        Attention!
                        Connection password for HTTP Settings "{}"
                        {}
                        Please renew password!
                    """,
                }
                # timedelta
                try:
                    daysdiff = datetime.strptime(setting.password_renew_date, "%Y-%m-%d %H:%M:%S") - datetime.now()
                except:
                    daysdiff = datetime.strptime(setting.password_renew_date, "%Y-%m-%d %H:%M:%S.%f") - datetime.now()
                    pass

                # daydelta int
                daysdelta = int(daysdiff.total_seconds()/86400)
                back_hint = "would be expired in {} day(s)!".format(daysdelta)
                # notification levels
                if daysdelta <= 1:
                    if daysdelta < 0:
                        back_hint = "was expired {} days ago".format(str(-1 * daysdelta))
                    mail_message.update({
                        'to_send': True,
                        'subject': str(mail_message.get('subject')).format('ALERT'),
                    })
                elif daysdelta <= 5:
                    mail_message.update({
                        'to_send': True,
                        'subject': str(mail_message.get('subject')).format('notification'),
                    })
                # add body to message
                mail_message.update({
                    'body': str(mail_message.get('body')).format(setting.name, back_hint)
                })
                # add message to queue
                if mail_message.get('to_send', False):
                    mid = self.pool.get('mail.message').schedule_with_attach(
                        cr, uid,
                        email_from=mail_message.get('from'),
                        email_to=mail_message.get('to'),
                        subject=mail_message.get('subject'),
                        body=mail_message.get('body'),
                        subtype=mail_message.get('subtype'), context=None)

        return True

    def to_dict(self, cr, uid, ids, *args, **kwargs):
        res = {}
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for row in self.browse(cr, uid, ids):
            dict_row = row.read(row.fields_get_keys())[0]
            res[row.id] = dict_row
        return res

http_settings()


class http_params(osv.osv):
    _name = "http.params"
    _rec_name = 'mhs_id'

    _columns = {
        'mhs_id': fields.many2one('multi.http.settings', 'Multi HTTP Settings'),
        'name': fields.char('Name', size=128, ),
        'value': fields.char('Value', size=512, ),
    }

http_params()
