# -*- coding: utf-8 -*-
from osv import fields, osv


class stock_import_settings(osv.osv):
    _name = "stock.import.settings"

    _columns = {
        'type_api_id': fields.many2one('sale.integration', 'Service provider'),
        'partner_id': fields.many2one('res.partner', 'Customer', ),
        'name_setting': fields.char('Name import setting', size=128),
        'yaml_setting': fields.text('Yaml Setting', ),
    }

stock_import_settings()
