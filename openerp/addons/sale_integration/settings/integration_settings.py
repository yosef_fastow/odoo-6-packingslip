# -*- coding: utf-8 -*-
from osv import fields, osv


class sale_integration_settings(osv.osv):
    _name = "sale.integration.settings"
    _columns = {
        'name': fields.char('System name', size=512),
        'label': fields.char('Label', size=512),
        'value': fields.text('Value'),
        'type_api_id': fields.many2one('sale.integration', 'Service provider'),
    }

sale_integration_settings()