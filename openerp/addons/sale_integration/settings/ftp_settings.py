# -*- coding: utf-8 -*-
from osv import fields, osv

def _get_action(self, cr, uid, context=None):
    actions = [
        ('inventory', 'Update QTY'),
        ('load_orders', 'Load Orders'),
        ('load_cancel_orders', 'Load Cancel Orders'),
        ('confirm_orders', 'Confirm Orders'),
        ('confirm_cancel', 'Confirm Cancel Orders'),
        ('invoice_orders', 'Invoice Orders'),
        ('return_orders', 'Return Orders'),
        ('load_return_orders', 'Load Return Orders'),
        ('confirm_load', 'Confirm Load'),
        ('confirm_shipment', 'Confirm Shipment'),
        ('routing_request', 'Routing Request'),
        ('acknowledgement', 'Acknowledgement'),
        ('get_orders_id', 'Get Orders Id'),
        ('delete_file', 'Delete File'),
        ('functional_acknowledgement', 'Functional Acknowledgement'),
        ('get_token', 'Get Token'),
        ('get_file_token', 'Get File Token'),
        ('get_fulfillment_nodes', 'Get Fulfillment Nodes'),
        ('upload_file', 'Upload File'),
        ('auxiliary_1', 'Auxiliary 1'),
        ('auxiliary_2', 'Auxiliary 2'),
    ]
    return sorted(actions, key=lambda r: r[1])


class multi_ftp_settings(osv.osv):
    _name = 'multi.ftp.settings'

    _columns = {
        'ftp_setting_id': fields.many2one('ftp.settings', 'FTP Setting', required=True),
        'action': fields.selection(_get_action, 'Action', select=1, required=True,),
        'ftp_path': fields.char('Path', size=128),
        'notification_for_customer': fields.many2one('res.partner', 'Customer', ),
        'emails_list': fields.text('Emails List', ),
        'ftp_file_mask': fields.char('Mask', size=256, help="Python regular expression", ),
        'use_decrypt': fields.boolean('Use Decrypt', ),
        'use_encrypt': fields.boolean('Use Encrypt', ),
    }

    _defaults = {
        'ftp_path': '/',
        'notification_for_customer': None,
        'emails_list': '',
        'use_decrypt': False,
        'use_encrypt': False,
    }

multi_ftp_settings()


class multi_http_settings(osv.osv):
    _name = 'multi.http.settings'

    _columns = {
        'http_setting_id': fields.many2one('http.settings', 'HTTP Setting', required=True),
        'action': fields.selection(_get_action, 'Action', select=1, required=True,),
        'method': fields.selection(
            (
                ('GET', 'GET'),
                ('POST', 'POST'),
                ('PUT', 'PUT'),
                ('DELETE', 'DELETE'),
            ), 'Method', select=1, required=True,
        ),
        'sub_url': fields.char('Sub URL', size=128),
        'params': fields.one2many('http.params', 'mhs_id', 'Params', ),
    }

    _defaults = {
        'sub_url': '',
    }

    def to_dict(self, cr, uid, ids, *args, **kwargs):
        res = {}
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for row in self.browse(cr, uid, ids):
            dict_row = row.read(row.fields_get_keys())[0]
            dict_row.update({
                'http_setting_id': row.http_setting_id.to_dict()[row.http_setting_id.id]
            })
            res[row.id] = dict_row
        return res

multi_http_settings()
