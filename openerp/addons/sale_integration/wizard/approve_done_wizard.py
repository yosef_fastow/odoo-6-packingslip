# -*- coding: utf-8 -*-

from osv import fields, osv
import time


class stock_picking_done_approve_wizard(osv.osv_memory):
    _name = "stock.picking.done.approve.wizard"
    _rec_name = "picking_id"

    _columns = {
        'picking_id': fields.many2one('stock.picking', 'Order name', ),
        'picking_name': fields.related('picking_id', 'name', type='char', readonly=True, string="Order Name", ),
        'validate_field': fields.char("Verify order name", size=512, required=False, help="Put order name to validate it."),
        'notes': fields.text("Notes", ),
    }

    def approve(self, cr, uid, ids, context=None):
        if not ids:
            return False

        if isinstance(ids, (int, long)):
            ids = ids[0]

        assert len(ids) == 1, 'Approve may only be done one at a time'

        validator = self.browse(cr, uid, ids[0])

        if validator.picking_name.upper() != validator.validate_field.upper().strip():
            raise osv.except_osv('Error', 'Wrong order number')

        user = self.pool.get('res.users').browse(cr, uid, uid)
        history = validator.picking_id.report_history or ''
        history += '\n%s %s: Approved flash order done with notes:\n%s' % (
            time.strftime('%m-%d %H:%M:%S', time.gmtime()),
            user.name or '',
            validator.notes
            )

        self.pool.get('stock.picking').write(cr, uid, validator.picking_id.id, {
            'manual_done_approved': True,
            'report_history': history,
        })

        return {'type': 'ir.actions.act_window_close'}

stock_picking_done_approve_wizard()
