# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from osv import fields, osv


class delmar_sale_taxes(osv.osv):
    _name = "delmar.sale.taxes"

    _columns = {
        'country_id': fields.many2one('res.country', 'Country',),
        'state_id': fields.many2one('res.country.state', 'State',),
        'state_code': fields.related('state_id', 'code', type='char', readonly=True, string="State Code"),
        'partner_id': fields.many2one('res.partner', 'Partner', domain="[('customer', '=', True)]"),
        'tax_name': fields.selection((('gst', 'GST'), ('pst', 'PST'), ('qst', 'QST'), ('hst', 'HST')), 'Tax Name', select=1, required=True,),
        'tax_value': fields.float('Tax Value', required=True, digits=(10, 5)),
        'active': fields.boolean('Active',),
    }

    _defaults = {
        'active': True,
        'tax_value': 0,
    }

    def change_country_id(self, cr, uid, ids, country_id=None, context=None):
        domain = {}
        result = {
            'country_id': country_id,
            'state_id': False,
        }
        if country_id:
            domain = {'state_id': [('country_id', '=', country_id)]}
        return {'value': result, 'domain': domain}

    def compute_sale_taxes(self, cr, uid, order_id, context=None):
        order = self.pool.get('sale.order').browse(cr, uid, order_id)
        taxes = {'amount_tax': 0, 'gst': 0, 'pst': 0, 'qst': 0, 'hst': 0}
        if order and order.partner_id.company_id.country_id.code == 'CA':
            lines = order.order_line
            sale_order = order
            order_cost = 0
            for line in lines:
                order_cost += float(line.price_unit) * float(line.product_uom_qty)
            shipping_address = sale_order.partner_shipping_id
            if shipping_address and shipping_address.state_id and shipping_address.country_id:
                ids = self.search(cr, uid, [
                    ('state_id', '=', shipping_address.state_id.id),
                    ('country_id', '=', shipping_address.country_id.id),
                    ('active', '=', True)
                ])
                sale_taxes_obj = self.browse(cr, uid, ids)
                for item in sale_taxes_obj:
                    taxes[item.tax_name] = item.tax_value and float(item.tax_value) * float(order_cost)
                    taxes['amount_tax'] += taxes[item.tax_name]
        return taxes

    def compute_moveline_taxes(self, cr, uid, move_id, discount=0, freight=0, context=None):
        if(context is None):
            context = {}
        # init taxes for line dict
        taxes = {'amount_tax': 0, 'gst': 0, 'pst': 0, 'qst': 0, 'hst': 0}
        # init moveline object
        move = self.pool.get('stock.move').browse(cr, uid, move_id)
        if not move:
            return taxes
        # try to calculate taxes for moveline
        try:
            line_cost = float(move.sale_line_id.price_unit) * float(move.product_qty)
            sale_order = move.sale_line_id.order_id
            country_id, state_id = self.get_country_state(cr, uid, sale_order)
            if country_id and state_id:
                ids = self.search(cr, uid, [
                    ('state_id', '=', state_id),
                    ('country_id', '=', country_id),
                    ('active', '=', True)])
                sale_taxes_obj = self.browse(cr, uid, ids)
                for item in sale_taxes_obj:
                    taxes[item.tax_name] = item.tax_value and float(item.tax_value) * float(line_cost)
                    taxes['amount_tax'] += round(taxes[item.tax_name], 2)

        except Exception as e:
            print("Line taxes calculate error: {0}".format(e.message))
        # return computed taxes dict
        return taxes

    def compute_stock_picking_taxes(self, cr, uid, order_id, discount=0, freight=0, context=None):
        if(context is None):
            context = {}
        for_pdf_invoice = context.get('for_pdf_invoice', False)
        if(for_pdf_invoice):
            taxes = {'amount_tax': 0}
        else:
            taxes = {'amount_tax': 0, 'gst': 0, 'pst': 0, 'qst': 0, 'hst': 0}
        order = self.pool.get('stock.picking').browse(cr, uid, order_id)
        if not order:
            return taxes
        try:
            sale_order = order.sale_id
            order_cost = 0
            for move in order.move_lines:
                order_cost += float(move.sale_line_id.price_unit) * float(move.product_qty)
            order_cost += freight - discount

            country_id, state_id = self.get_country_state(cr, uid, sale_order)

            if country_id and state_id:
                ids = self.search(cr, uid, [
                    ('state_id', '=', state_id),
                    ('country_id', '=', country_id),
                    ('active', '=', True)])
                sale_taxes_obj = self.browse(cr, uid, ids)
                for item in sale_taxes_obj:
                    taxes[item.tax_name] = item.tax_value and float(item.tax_value) * float(order_cost)
                    taxes['amount_tax'] += round(taxes[item.tax_name], 2)
        except Exception as err:
            print("Taxes calculate error: {0}".format(err.message))
        return taxes

    def compute_sale_order_taxes(self, cr, uid, order, order_lines):
        context = {}
        for_pdf_invoice = context.get('for_pdf_invoice', False)
        if(for_pdf_invoice):
            taxes = {'amount_tax': 0}
        else:
            taxes = {'amount_tax': 0, 'gst': 0, 'pst': 0, 'qst': 0, 'hst': 0}

        order_cost = 0
        for line in order_lines:
            order_cost += float(line.price_unit) * float(line.product_uom_qty)

        country_id, state_id = self.get_country_state(cr, uid, order)

        if country_id and state_id:
            ids = self.search(cr, uid, [
                ('state_id', '=', state_id),
                ('country_id', '=', country_id),
                ('active', '=', True)])
            sale_taxes_obj = self.browse(cr, uid, ids)
            for item in sale_taxes_obj:
                taxes[item.tax_name] = item.tax_value and float(item.tax_value) * float(order_cost)
                taxes['amount_tax'] += round(taxes[item.tax_name], 2)
        return taxes

    def get_country_state(self, cr, uid, sale_order):
        partner = sale_order.partner_id
        country_id, state_id = None, None
        # HARDCODE
        # Task #20435 <erel@progforce.com> "Delmar Jun 30": HBC: - missing GST. Tax should be always from QC
        # Task #19814: For ALL orders need to put tax, (even if country not canada) like for QC
        if partner and partner.ref == 'HBC':
            country_id = self.pool.get('res.country').search(cr, uid, [('name', '=', 'Canada')])[0]
            state_ids = self.pool.get('res.country.state').search(cr, uid, [
                ('country_id', '=', country_id),
                ('code', '=', 'QC')
            ])
            state_id = state_ids[0]
        else:
            shipping_address = sale_order.partner_shipping_id
            if shipping_address:
                country_id = shipping_address.country_id and shipping_address.country_id.id
                state_id = shipping_address.state_id and shipping_address.state_id.id
        return country_id, state_id

    def get_raw_taxes(self, cr, uid, country_id, state_ids, context=None):
        if isinstance(state_ids, (int, long)):
            state_ids = [state_ids]
        raw_taxes = {}
        ids = self.search(
            cr, uid, [
                ('state_id', 'in', state_ids),
                ('country_id', '=', country_id),
                ('active', '=', True)
            ]
        )
        for item in self.browse(cr, uid, ids):
            raw_taxes.update({item.tax_name: item.tax_value})
        return raw_taxes

delmar_sale_taxes()
