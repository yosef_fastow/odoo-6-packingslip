# -*- coding: utf-8 -*-

from osv import fields, osv


class sale_order_fields(osv.osv):
    _name = "sale.order.fields"
    _columns = {
        'order_id': fields.many2one('sale.order', 'Sale Order Line'),
        'name': fields.char('System Name', size=512),
        'label': fields.char('Label', size=512),
        'value': fields.char('Value', size=2048),

    }
sale_order_fields()


class sale_order_line_fields(osv.osv):
    _name = "sale.order.line.fields"
    _columns = {
        'line_id': fields.many2one('sale.order.line', 'Sale Order Line'),
        'name': fields.char('System Name', size=512),
        'label': fields.char('Label', size=512),
        'value': fields.char('Value', size=512),
    }

sale_order_line_fields()
