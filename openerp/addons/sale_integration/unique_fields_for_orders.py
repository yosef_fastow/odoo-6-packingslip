# -*- coding: utf-8 -*-
from osv import fields, osv
from tools.translate import _
from datetime import datetime


class unique_fields_for_orders(osv.osv):
    _name = 'unique.fields.for.orders'

    base_check_used_sql = '''SELECT
                                a.id,
                                CASE
                                    WHEN (now() at time zone 'utc' < a.end_date AND a.picking_id IS NOT NULL) THEN TRUE
                                    ELSE FALSE
                                END AS used,
                                a."value"
                            FROM ( SELECT
                                    uffo.id,
                                    uffo.use_date + (ufnfo.expiration_date || ' ' ||
                                    CASE
                                        WHEN ufnfo.hours IS TRUE THEN 'hour'
                                        WHEN ufnfo.days IS TRUE THEN 'day'
                                        WHEN ufnfo.months IS TRUE THEN 'month'
                                        WHEN ufnfo.years IS TRUE THEN 'year'
                                        ELSE ''
                                    END)::interval AS end_date,
                                    uffo.picking_id,
                                    uffo."value"
                                FROM
                                    unique_fields_for_orders AS uffo
                                LEFT JOIN
                                    unique_fields_name_for_orders AS ufnfo
                                        ON uffo.type_id = ufnfo.id
                                WHERE 1=1
                                    AND {where_sql}
                            ) a'''

    def _get_used(self, cr, uid, ids, *args, **kwargs):
        if isinstance(ids, (int, long)):
            ids = [ids]
        sql = self.base_check_used_sql.format(where_sql='uffo.id IN %s')
        cr.execute(sql, (tuple(ids), ))
        return {x['id']: x['used'] for x in cr.dictfetchall()}

    _columns = {
        'value': fields.text('Value', required=True, ),
        'type_id': fields.many2one('unique.fields.name.for.orders', 'Type', ),
        'use_date': fields.datetime('Use Date', ),
        'picking_id': fields.many2one('stock.picking', 'Used for', ),
        'used': fields.function(
            _get_used,
            method=True,
            string='Used',
            type='char',
        ),
        'history_ids': fields.one2many(
            'unique.fields.for.orders.history',
            'uffo_id',
            string='History',
            readonly=True
        ),
    }

    _sql_constraints = [
        ('value_uniq', 'unique(value)', 'The value be unique!'),
    ]

    def check_choice(self, cr, uid, ids, picking_id):
        data = self.read(cr, uid, ids, ['used', 'picking_id'])
        for row in data:
            row_picking_id = row['picking_id'] and row['picking_id'][0] or False
            if row_picking_id == picking_id or not row_picking_id:
                return row['id']
        return False

    def fetch_one_unused(self, cr, uid, type_name, picking_id, limit=1):
        self.clean_all_unused(cr, uid)
        res_ids = self.search(
            cr, uid, [
                '|',
                ('history_ids.picking_id', '=', picking_id),
                ('picking_id', '=', picking_id)
            ]
        )
        availabile_id = self.check_choice(cr, uid, res_ids, picking_id)
        if availabile_id:
            res = self.read(cr, uid, availabile_id, ['id', 'value'])
        else:
            sql = '''SELECT
                        b.id,
                        b."value"
                    FROM (
                        {base_check_used_sql}
                    ) b
                    WHERE 1=1
                        AND b.used IS FALSE
                    ORDER BY b.id
                    LIMIT {limit}
            '''.format(limit=limit,
                base_check_used_sql=self.base_check_used_sql).format(
                    where_sql='ufnfo."name" = %s')
            cr.execute(sql, (type_name, ))
            res = cr.dictfetchall()
            if limit == 1:
                res = res and res[0] or False
        return res

    def use_for(self, cr, uid, ids, picking_id):
        self.clean_all_unused(cr, uid)
        self.write(
            cr, uid, ids, {
                'use_date': datetime.utcnow(),
                'picking_id': picking_id,
            }
        )
        return True

    def clean_all_unused(self, cr, uid, context=None):
        ids = self.search(cr, uid, [('picking_id', '!=', False)])
        now_used = {
            x['id']: x for x in self.read(
                cr, uid, ids, ['use_date', 'picking_id', 'used'])
        }
        for row_id, row_data in now_used.items():
            use_date = row_data['use_date']
            picking_id = row_data['picking_id'] and row_data['picking_id'][0] or False
            used = row_data['used']
            if picking_id and not used:
                self.write(
                    cr, uid, row_id, {
                        'use_date': False,
                        'picking_id': False,
                        'history_ids': [(0, 0, {'use_date': use_date, 'picking_id': picking_id})]
                    }
                )
        return True

    def generate_sscc_id(self, cr, uid, ids, application_identifier='00', extension_digit='0', context=None):
        def get_last_digit(upc):
            try:
                upc_odd_sum = 0
                upc_even_sum = 0
                for upc_index in range(len(upc)):
                    if upc_index % 2 == 0:
                        upc_odd_sum += int(upc[upc_index])
                    else:
                        upc_even_sum += int(upc[upc_index])
                last_digit = (upc_even_sum + (upc_odd_sum * 3)) % 10
                last_digit = 10 - last_digit
                last_digit = str(last_digit)[-1]
            except ValueError:
                return False
            return last_digit

        if isinstance(ids, (int, long)):
            ids = [ids]
        res = {}
        if not ids:
            return {}
        for row in self.browse(cr, uid, ids):
            serial_reference_number = row.value.strip()
            if len(serial_reference_number) < 16:
                serial_reference_number = serial_reference_number.rjust(16, '0')
            elif len(serial_reference_number) > 16:
                raise ValueError('serial_reference_number must have 16 digits')
            else:
                pass
            if not serial_reference_number.isdigit():
                raise ValueError('serial_reference_number must have only digits')
            base_sscc_id = '{application_identifier}{extension_digit}{serial_reference_number}'.format(
                # Application Identifier (AI)
                # Indicates the data field that contains the SSCC. In the human-
                # readable interpretation of the encoded data printed below the bar
                # code, the AI is placed in parentheses ( ) and separated from the
                # SSCC-18 by a single space. Do not encode parentheses in the bar
                # code.
                # For Kmart, this value is hard-coded 00.
                application_identifier=application_identifier,
                # Extension Digit
                # Used to increase the capacity of the Serial Reference (no defined
                # logic). You may use any numeral except 4 as the Extension Digit in
                # your SSCC bar codes for SHMC.
                # For Kmart, this value is hard-coded 4.
                extension_digit=extension_digit,
                # GS1 Company Prefix Serial Reference Number
                # The Company Prefix (“Manufacturer’s ID”) is assigned by the GS1
                # U.S. (former UCC) or a GS1 Member Organization to a user
                # company. The Serial Reference is a number assigned by the holder
                # of the GS1 Company Prefix to uniquely identify a shipping container.
                # A Serial Reference Number must not be re-used for a minimum of 12
                # months.
                serial_reference_number=serial_reference_number
            )
            last_digit = get_last_digit(base_sscc_id)
            if not last_digit:
                raise ValueError('Failed generate last digit for `SSCC ID`')
            sscc_id = base_sscc_id + last_digit
            res[row.id] = sscc_id
        return res

unique_fields_for_orders()


class unique_fields_name_for_orders(osv.osv):
    _name = 'unique.fields.name.for.orders'

    def _get_expiration_date_string(self, cr, uid, ids, *args, **kwargs):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = {}
        if not ids:
            return res
        for row in self.browse(cr, uid, ids):
            res[row.id] = unicode(row.expiration_date)
            if row.hours:
                res[row.id] += u' hours'
            elif row.days:
                res[row.id] += u' days'
            elif row.months:
                res[row.id] += u' months'
            elif row.years:
                res[row.id] += u' years'
            else:
                raise NotImplementedError('Unknown decision for expiration_date!')
        return res

    _columns = {
        'name': fields.char('Name', size=128, required=True, ),
        'expiration_date': fields.integer('Expiration Date', required=True, ),
        'hours': fields.boolean('Hours', ),
        'days': fields.boolean('Days', ),
        'months': fields.boolean('Months', ),
        'years': fields.boolean('Years', ),
        'expiration_date_string': fields.function(
            _get_expiration_date_string,
            string='Expiration Date',
            readonly=True,
            type='char',
            store={
                'unique.fields.name.for.orders': (
                    lambda self, cr, uid, ids, c={}: ids,
                    ['expiration_date', 'hours', 'days', 'months', 'years'],
                    10
                ),
            },
        ),
    }

    def create(self, cr, uid, vals, context=None):
        if not (
            vals.get('hours', False) or
            vals.get('days', False) or
            vals.get('months', False) or
            vals.get('years', False)
        ):
            raise osv.except_osv(
                _('WARNING!'),
                _('One of checkbox of hours, days, months, years is required')
            )
        return super(unique_fields_name_for_orders, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        expiration_vals = self.read(cr, uid, ids[0], ['hours', 'days', 'months', 'years'], context=context)
        del expiration_vals['id']
        expiration_vals.update(
            {x: vals.get(x, False) for x in ['hours', 'days', 'months', 'years']}
        )
        if not (
            expiration_vals.get('hours', False) or
            expiration_vals.get('days', False) or
            expiration_vals.get('months', False) or
            expiration_vals.get('years', False)
        ):
            raise osv.except_osv(
                _('WARNING!'),
                _('One of checkbox of hours, days, months, years is required')
            )
        vals.update(expiration_vals)
        return super(unique_fields_name_for_orders, self).write(cr, uid, ids, vals, context=context)

unique_fields_name_for_orders()


class unique_fields_for_orders_history(osv.osv):
    _name = 'unique.fields.for.orders.history'

    _columns = {
        'uffo_id': fields.many2one(
            'unique.fields.for.orders',
            string='Unique fields for orders',
            ondelete='cascade'
        ),
        'picking_id': fields.many2one('stock.picking', 'Used for', ),
        'use_date': fields.datetime('Use Date', ),
    }

unique_fields_for_orders_history()
