# -*- coding: utf-8 -*-
from osv import fields, osv


class jet_taxonomy(osv.osv):
    _name = 'jet.taxonomy'
    _rec_name = 'jet_node_name'

    def _parent_id(self, cr, uid, ids, name, args, context=None):
        res = {}
        if ids:
            for row in self.browse(cr, uid, ids):
                parent_ids = self.search(cr, uid, [('jet_node_id', '=', int(row.parent_id_str))])
                res[row.id] = parent_ids and parent_ids[0] or False
        return res

    _columns = {
        'jet_node_id': fields.char(size=256, string='jet_node_id', ),
        'jet_node_name': fields.char(size=256, string='jet_node_name', ),
        'jet_node_path': fields.char(size=256, string='jet_node_path', ),
        'amazon_node_ids': fields.one2many(
            'amazon.node.id',
            'jet_taxonomy_id',
            string='Amazon Node IDS'
        ),
        'parent_id_str': fields.char(size=256, string='parent_id', ),
        'parent_id': fields.function(
            _parent_id,
            string="Parent",
            type="many2one",
            relation="jet.taxonomy",
            method=True,
        ),
        'jet_level': fields.char(size=256, string='jet_level', ),
        'suggested_tax_code': fields.char(size=512, string='suggested_tax_code', ),
        'active': fields.boolean("Active", ),
    }

    _order = 'jet_node_id'

jet_taxonomy()


class load_jet_taxonomy(osv.osv_memory):
    _name = 'load.jet.taxonomy'

    _columns = {
        'logs': fields.text(string='Logs', ),
    }

    def load_from_jet_cron(self, cr, uid, context=None):
        return self.load_from_jet(cr, uid, None, context=context)

    def load_from_jet(self, cr, uid, ids, context=None):
        jet_taxonomy_obj = self.pool.get('jet.taxonomy')
        amazon_node_id_obj = self.pool.get('amazon.node.id')
        conf_obj = self.pool.get('ir.config_parameter')
        (settings, api) = self.pool.get('sale.integration').getApi(cr, uid, 'Jet Partner')

        last_taxonomy_setting = 'jet.last.updated.taxonomy'
        offset = int(conf_obj.get_param(cr, uid, last_taxonomy_setting))

        taxonomys, last_checked = api.getAllTaxonomy(offset=offset)

        created = 0
        updated = 0
        for i, taxonomy in enumerate(taxonomys):
            if taxonomy.get('amazon_node_ids'):
                input_amazon_node_ids = [str(a) for a in taxonomy['amazon_node_ids']]
                del taxonomy['amazon_node_ids']
                amazon_node_ids = amazon_node_id_obj.search(
                    cr, uid, [('amazon_node_id', 'in', [str(a) for a in input_amazon_node_ids])]
                )
                in_erp_amazon_node_ids = [
                    x.amazon_node_id for x in amazon_node_id_obj.browse(cr, uid, amazon_node_ids)
                ]
                not_in_erp_amazon_node_ids = list(set(input_amazon_node_ids) - set(in_erp_amazon_node_ids))
                for a_node_id in not_in_erp_amazon_node_ids:
                    amazon_node_id_obj.create(cr, uid, {'amazon_node_id': a_node_id})
                amazon_node_ids = amazon_node_id_obj.search(
                    cr, uid, [('amazon_node_id', 'in', input_amazon_node_ids)]
                )
                taxonomy['amazon_node_ids'] = [[4, x] for x in amazon_node_ids]

            taxonomy['parent_id_str'] = taxonomy['parent_id']
            del taxonomy['parent_id']
            jet_taxonomy_ids = jet_taxonomy_obj.search(
                cr, uid, [('jet_node_id', '=', taxonomy['jet_node_id'])])

            if jet_taxonomy_ids:
                jet_taxonomy_obj.write(cr, uid, jet_taxonomy_ids, taxonomy)
                updated += 1
            else:
                jet_taxonomy_obj.create(cr, uid, taxonomy)
                created += 1

        conf_obj.set_param(cr, uid, last_taxonomy_setting, str(last_checked))

        if ids:
            self.write(
                cr, uid, ids,
                {'logs': 'Created {0} taxonomys\nUpdated {1} taxonomys'.format(created, updated)}
            )

        return True

load_jet_taxonomy()


class amazon_node_id(osv.osv):
    _name = 'amazon.node.id'

    _columns = {
        'jet_taxonomy_id': fields.many2one('jet.taxonomy', string='Jet Taxonomy ID', ondelete='cascade'),
        'amazon_node_id': fields.char(size=256, string='amazon_node_id', unique="True"),
    }

    _sql_constraints = [
        ('amazon_node_id_uniq', 'unique(amazon_node_id)', 'The amazon_node_id be unique!'),
    ]

amazon_node_id()
