# -*- coding: utf-8 -*-
from osv import fields, osv
import xlwt
import cStringIO
import base64


class delivery_order_export(osv.osv):
    _name = "delivery.order.export"
    _columns = {
        "xls_file": fields.binary(string="CSV Export", readonly=True),
        "xls_filename": fields.char("", size=256),
        'state': fields.boolean("state")
    }

    _defaults = {
        'state': False,
        'xls_file': ''
    }

    def action_amazon_export(self, cr, uid, ids, context=None):
        pick_ids = context.get('active_ids', [])
        out = None
        if(pick_ids):
            ShippingConfirmation = xlwt.Workbook()
            sheet = ShippingConfirmation.add_sheet("ShippingConfirmation")
            sheet.write(0, 0, 'order-id')
            sheet.write(0, 1, 'order-item-id')
            sheet.write(0, 2, 'quantity')
            sheet.write(0, 3, 'ship-date')
            sheet.write(0, 4, 'carrier-code')
            sheet.write(0, 5, 'carrier-name')
            sheet.write(0, 6, 'tracking-number')
            sheet.write(0, 7, 'ship-method')
            row = 0
            for deliveri_id in pick_ids:
                deliver_obj = self.pool.get("stock.picking").browse(cr, uid, deliveri_id)
                sale_obj = deliver_obj.sale_id
                for line in sale_obj.order_line:
                    row += 1
                    sheet.write(row, 0, str(sale_obj.external_customer_order_id))
                    sheet.write(row, 1, str(line.external_customer_line_id))
                    sheet.write(row, 2, str(int(line.product_uom_qty)))
                    sheet.write(row, 3, deliver_obj.shp_date)
                    sheet.write(row, 4, str(deliver_obj.carrier_code))
                    sheet.write(row, 5, str(deliver_obj.carrier))
                    sheet.write(row, 6, str(deliver_obj.tracking_ref))
                    sheet.write(row, 7,  str(deliver_obj.carrier_code))
            buf = cStringIO.StringIO()
            ShippingConfirmation.save(buf)
            out = base64.encodestring(buf.getvalue())
            buf.close()
        return self.write(cr, uid, ids, {'state': True, 'xls_file': out, 'xls_filename': 'ShippingConfirmation.xls'}, context=context)

delivery_order_export()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
