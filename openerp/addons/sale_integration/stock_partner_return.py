# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from osv import fields, osv


class stock_partner_return(osv.osv):
    _name = "stock.partner.return"

    def _get_name(self, cr, uid, ids, fn, args, context=None):
        res = {}
        for row in self.read(cr, uid, ids, fields=['value'], context=context):
            res[row.get('id')] = row.get('value', False)
        return res

    _columns = {
        'name': fields.function(_get_name, type='char', method=True,),
        'partner_id': fields.many2one('res.partner', 'Partner', domain="[('customer', '=', True)]"),
        'key': fields.char('Key', size=256, required=True, select=1,),
        'value': fields.char('Value', size=256, required=True,),
        'active': fields.boolean('Active',),
        'default_customer': fields.boolean('Default', ),
    }

    _defaults = {
        'active': True,
    }

    _sql_constraints = [
        ('key_uniq', 'unique (key,partner_id)',
            'The key must be unique !'),
    ]

    def get_default_customer(self, cr, uid, partner_id=False, context=None):
        ids = self.search(cr, uid, [('partner_id', '=', partner_id), ('default_customer', '=', True)], context=context)
        if len(ids) > 0:
            return self.browse(cr, uid, ids[0], context=context)
        return False

    def get_value(self, cr, uid, key, partner_id=False, default=False, context=None):
        ids = None
        if not key:
            return default

        args = [
            ('key', '=', key),
            ('active', '=', True)
        ]

        if partner_id:
            ids = self.search(cr, uid, args + [('partner_id', '=', partner_id)], context=context)

        if not ids:
            ids = self.search(cr, uid, args, context=context)

        if not ids:
            return default

        param = self.browse(cr, uid, ids[0], context=context)
        value = param.value

        return value

stock_partner_return()
