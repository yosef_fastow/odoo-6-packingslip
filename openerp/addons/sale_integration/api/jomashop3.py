# -*- coding: utf-8 -*-
from httpauthapiclient import HTTPWithAuthApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
from openerp.addons.pf_utils.utils.re_utils import f_d
from json import loads as json_loads
from json import dumps as json_dumps
from customer_parsers.jomashop_input_parser import Parser
from apiopenerp import ApiOpenerp
import requests
import cStringIO
import csv
import logging
import json
import base64

logger = logging.getLogger(__name__)


# Connection api class
# Used to connect to api with authorization
class Jomashop3RESTApi(HTTPWithAuthApiClient):
    auth_key = ""

    def __init__(self, settings):
        super(Jomashop3RESTApi, self).__init__(settings)

    @property
    def sub_request_kwargs(self):
        request = {
            'headers': {
                'Accept': 'application/json',
                'Authorization': self.get_api_key
            },
        }
        return request

    @property
    def get_api_key(self):
        if not self.auth_key or self.auth_key == 'no_auth':
            # self.use_http_settings('get_token')
            # url = self.url
            url = self.base_url + '/v1/session'
            data = {
                "user": {
                    "email": self.username,
                    "password": self.password
                }
            }
            headers = {'Content-type': 'application/json'}
            r = requests.post(url, json.dumps(data), headers=headers)
            if r.status_code == 200:
                self.auth_key = r.headers.get('authorization')

        return self.auth_key


# TODO: WTF?
class param(object):
    def __init__(self, key, value):
        super(param, self).__init__()
        self.name = str(key)
        self.value = str(value)


class Jomashop3RESTApiClient(AbsApiClient):
    use_local_folder = True

    def __init__(self, settings_variables):
        multi_http_settings_dict = {}
        if 'multi_http_settings' in settings_variables:
            for mhs in settings_variables['multi_http_settings']:
                multi_http_settings_dict.update(mhs.to_dict())
        settings_variables['multi_http_settings_dict'] = multi_http_settings_dict
        super(Jomashop3RESTApiClient, self).__init__(settings_variables, Jomashop3RESTOpenerp, False, False)

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        Function for loading orders from Jomashop3REST.API
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """

        orders = []
        if save_flag:
            backup_api = Jomashop3RESTApiBackupOrders(self.settings)
            order_ids = backup_api.process('only_result')
            if order_ids:
                for order_id in order_ids:
                    order_api = Jomashop3RESTApiGetOrder(self.settings, order_id)
                    order_api.process('load')
                    orders.extend(order_api.orders_list)
                    self.extend_log(order_api)
        else:
            order_api = Jomashop3RESTApiLoadOrders(self.settings)
            orders = order_api.process('read_new')
            self.extend_log(order_api)
        return orders

    def updateQTY(self, lines, mode=None):
        self.settings['main_product_field'] = 'customer_id_delmar'
        # self.checkAttributes()
        update_qty_api = Jomashop3RESTApiUpdateQTY(self.settings, lines)
        update_qty_api.process('only_result')
        self.extend_log(update_qty_api)
        self.check_and_set_filename_inventory(update_qty_api)
        return update_qty_api.revision_lines

    def confirmShipment(self, lines, context=None):
        res = True
        confirm_api = Jomashop3RESTApiConfirmOrders(self.settings, lines)
        res_confirm = confirm_api.process('send')
        self.extend_log(confirm_api)
        res = 'Confirm result:\n{0}'.format(str(res_confirm))
        return res

    def processingOrderLines(self, lines, state='accepted'):
        res = True
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            reject_api = Jomashop3RESTApiRejectOrders(self.settings, lines, state)
            res = reject_api.process('only_result')
            self.extend_log(reject_api)
        return res


class Jomashop3RESTApiUpdateQTY(Jomashop3RESTApi):
    def __init__(self, settings, lines):
        super(Jomashop3RESTApiUpdateQTY, self).__init__(settings)
        self.settings = settings
        self.file_type = 'Inventory'
        self.use_http_settings('inventory')
        self.lines = lines
        self.history = {}
        self.filename_local = f_d('jomashop_offer_import_%Y_%m_%d_%H_%M.csv', datetime.utcnow())
        self.revision_lines = {
            'good': [],
            'bad': [],
        }
        self.variations = {}

    @staticmethod
    def _write_csv_lines(lines, context=None):
        if not lines:
            lines = []
        quote_style = csv.QUOTE_ALL
        output = cStringIO.StringIO()
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
        writer = csv.writer(output, dialect='csv', quoting=quote_style)
        for line in lines:
            writer.writerow([isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in line])
        return output

    @property
    def sub_request_kwargs(self):
        request = super(Jomashop3RESTApiUpdateQTY, self).sub_request_kwargs
        attachment = self.prepare_feed()
        headers = request.get('headers', {})
        # headers.update({'cache-control': "no-cache"})
        headers.update({'Content-Type': "application/json"})
        request.update({
            "data": json_dumps({
                    "file_name": self.filename_local,
                    "file": "data:text/csv;base64,{}".format(str(base64.b64encode(str(attachment.getvalue()), 'utf-8')))
            })
        })
        return request

    def check_response(self, response):
        super(Jomashop3RESTApiUpdateQTY, self).check_response(response)
        if response and response.text:
            self.history['response'] = json_loads(response.text)
            csv_back = str(self.history.get('feed')).decode()
            self.backup_file(self.filename_local, csv_back)
        return True

    def prepare_feed(self):
        feed = list()
        # init feed csv header line
        feed.append([
            "Vendor SKU",
            "Brand",
            "Price",
            "Status",
            "Quantity"
        ])
        main_field = self.settings['main_product_field']

        for line in self.lines:
            price = line.get('customer_price', None)
            if not line.get(main_field, False):
                self.append_to_revision_lines(line, 'bad')
                continue

            status = 'active'
            if line['qty'] in (0, '0'):
                status = 'inactive'
                if line['dnr_flag'] not in (1, '1'):
                    status = 'out_of_stock'

            feed_line = [
                line.get(main_field, line.get('customer_sku', line.get('sku', line.get('default_code')))),
                '',
                price,
                status,
                line['qty']
            ]
            feed.append(feed_line)
            self.append_to_revision_lines(line, 'good')

        feed_file = self._write_csv_lines(feed)
        self.history.update({'feed': feed_file.getvalue()})
        return feed_file


class Jomashop3RESTApiGetOrder(Jomashop3RESTApi):
    def __init__(self, settings, order_id):
        super(Jomashop3RESTApiGetOrder, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.parser = Parser(self)
        self.sub_url += '/{}'.format(str(order_id))
        self.filename = f_d('orders_%Y%m%d_%H%M%S_%f', datetime.utcnow())
        self.orders_list = []

    def process_response(self, response):
        response_data = json_loads(super(Jomashop3RESTApiGetOrder, self).process_response(response))
        orders_list = []
        if response_data.get('order', False):
            self.orders_list.append(response_data.get('order'))
            orders_list.append(json_dumps(response_data.get('order')))
        return orders_list


class Jomashop3RESTApiLoadOrders(Jomashop3RESTApi):
    def __init__(self, settings):
        super(Jomashop3RESTApiLoadOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.parser = Parser(self)


class Jomashop3RESTApiBackupOrders(Jomashop3RESTApi):
    def __init__(self, settings):
        super(Jomashop3RESTApiBackupOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.sub_url += '/template.csv'
        self.fieldnames = ["Tracking Number", "Invoice Number", "SO#", "SKU", "Qty", "Cost", "Product Name", "GiftMessage", "Source", "Ship Method", "Ship Full Name", "Phone", "Billing Full Name", "Billing Phone", "Ship Address", "Ship Address2", "Ship City", "Ship State", "Ship Country", "Ship Zip", "Placed at", "UniqueID"]
        self.filename_local = f_d('jomashop_orders_%Y_%m_%d_%H_%M.csv.backup', datetime.utcnow())

    def check_response(self, response):
        super(Jomashop3RESTApiBackupOrders, self).check_response(response)
        if response and response.text:
            csv_lines = csv.DictReader(cStringIO.StringIO(response.text), fieldnames=self.fieldnames)
            if sum(1 for line in csv_lines) > 1:
                self.backup_file(self.filename_local, str(response.text).decode())
        return True

    def process_response(self, response):
        response_data = super(Jomashop3RESTApiBackupOrders, self).process_response(response)
        orders_list = set()
        if response and response_data:
            csv_lines = csv.DictReader(cStringIO.StringIO(response_data), fieldnames=self.fieldnames)
            next(csv_lines, None)
            for line in csv_lines:
                if type(line.get('SO#')) == str and line.get('SO#') is not None:
                    orders_list.add(line.get('SO#'))
        return list(orders_list)


class Jomashop3RESTApiConfirmOrders(Jomashop3RESTApi):
    def __init__(self, settings, lines):
        super(Jomashop3RESTApiConfirmOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.lines = lines
        self.filename = "{:ship_confirm_%Y%m%d%H%M.json}".format(datetime.now())
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(id=self.lines[0]['po_number'])
                self.sub_url += "/fulfill"

    @property
    def sub_request_kwargs(self):
        request = super(Jomashop3RESTApiConfirmOrders, self).sub_request_kwargs
        headers = request.get('headers', {})
        headers.update({'Content-Type': 'application/json'})
        request.update({'data': json_dumps(self.create_confirm_dict(self.lines))})
        return request

    def check_response(self, response):
        try:
            super(Jomashop3RESTApiConfirmOrders, self).check_response(response)
        except Exception:
            pass
            if response and response.status_code == 400:
                resp = json_loads(response.text)
                logger.info("Fulfill failed: %s" % resp)
                return True
            else:
                return False
        return True

    def process_response(self, response):
        # process
        res = super(Jomashop3RESTApiConfirmOrders, self).process_response(response)
        return True

    @staticmethod
    def create_confirm_dict(lines):
        skus = [{
            'sku': x.get('merchantSKU', ''),
            'qty': x.get('qty', 0),
        } for x in lines]
        confirm_data = {
            'order': {
                'fulfilled': skus,
                'invoice_number': lines[0].get('invoice', ''),
                'tracking_number': lines[0].get('tracking_number', ''),
                'shipped_at': datetime.now().strftime("%Y-%m-%d %H:%M:%S %z"),
            }
        }
        return confirm_data


class Jomashop3RESTApiRejectOrders(Jomashop3RESTApi):

    def __init__(self, settings, lines, state='cancel'):
        super(Jomashop3RESTApiRejectOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.lines = lines
        self.state = state
        self.filename = "{:reject_order_%Y%m%d%H%M.xml}".format(datetime.now())
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(id=self.lines[0]['po_number'])
                self.sub_url += "/reject"

    def check_response(self, response):
        try:
            super(Jomashop3RESTApiRejectOrders, self).check_response(response)
        except Exception:
            pass
            if response and response.status_code == 400:
                resp = json_loads(response.text)
                logger.info("Reject response: %s" % resp)
                return True
            else:
                return False
        return True

    def process_response(self, response):
        res = super(Jomashop3RESTApiRejectOrders, self).process_response(response)
        # logger.info("Reject response: %s" % res)
        return True

    @property
    def sub_request_kwargs(self):
        request = super(Jomashop3RESTApiRejectOrders, self).sub_request_kwargs
        reject_order = {}
        if self.lines:
            reject_order = self.create_reject_dict(lines=self.lines)
        headers = request.get('headers', {})
        headers.update({'Content-Type': 'application/json'})
        request.update({'data': json_dumps(reject_order)})
        return request

    @staticmethod
    def create_reject_dict(lines):
        skus = [x.get('merchantSKU', '') for x in lines]
        reject_data = {
            'order': {
                'rejected_skus': skus,
            }
        }
        return reject_data


class Jomashop3RESTOpenerp(ApiOpenerp):

    def __init__(self):
        super(Jomashop3RESTOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        address_name = order and order['address'] and order['address']['ship'] and order['address']['ship']['name'] or False
        order_obj = {'bulk_transfer': False}
        if address_name == 'Jomashop.com - c/o Dropship':
            order_obj = {'bulk_transfer': True}
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'merchantSKU': line['merchantSKU'],
            'additional_fields': [],
            #"qty": line['request_order_quantity'],
            #'price_unit': line['price_unit'],
            'external_customer_line_id': line['merchantSKU'],
        }

        for field, value in line.get('additional_fields', {}).iteritems():
            field_obj = (
                0, 0, {
                    'name': field.lower().replace(' ', '_'),
                    'label': field,
                    'value': value,
                }
            )
            line_obj['additional_fields'].append(field_obj)

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'],
                                                                                      line[field])
                if size and size.id:
                    line_obj['size_id'] = size.id

                if product:
                    line_obj["product_id"] = product.id
                    """
                    # REMOVED FOR DLMR-2219
                    # to get price from xml, not from customer price field
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                     context['customer_id'],
                                                                                     'Customer Price')
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj["notes"] = "Can't find product cost.\n"
                    """
        return line_obj

