# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient

from utils import feedutils as feed_utils
from configparser import ConfigParser
import os
import logging
from datetime import datetime

_logger = logging.getLogger(__name__)


class OlveApi(FtpClient):

    def __init__(self, settings):
        super(OlveApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/olve_settings.ini' % (os.path.dirname(__file__)))


class OlveApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(OlveApiClient, self).__init__(
            settings_variables, False, False, False)

    def updateQTY(self, lines, mode=None):
        updateApi = OlveApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class OlveApiUpdateQTY(OlveApi):
    filename = "inventory.txt"
    filename_local = ""
    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(OlveApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.confirmLines = lines
        self.filename_local = "olve_inventory_%s.txt" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        datas = ["VendorSKU,UPC,QTY,Status\n"]
        for line in self.confirmLines:
            status = 'Out Stock'
            if line.get('qty', False):
                status = 'In Stock'
            line['status'] = status

            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line(',', True)
            if SkuRecord:
                datas.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(datas)[:-2]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
