# -*- coding: utf-8 -*-
from baseapiclient import BaseApiClient
import traceback
import cStringIO
from io import BytesIO
import logging
from datetime import datetime
from os import remove as remove_path
from os import listdir
from os.path import join as os_path_join
from os.path import dirname as os_path_dirname

from ftpobjects import FTPNormal, FTPOverSSHObject, FTPOverSSLObject

import smtplib
from email.mime.text import MIMEText
from openerp.addons.pf_utils.utils.decorators import to_ascii

_logger = logging.getLogger(__name__)


class FtpClientException(Exception):
    def __init__(self, customer, message):
        super(FtpClientException, self).__init__()
        self.message = 'FtpClientException for {customer}: {message}'.format(
            customer=customer,
            message=message,
        )


class FtpClient(BaseApiClient):

    _customer_ref = False
    _mako_template = False
    _ftp_object = False
    _email_title = ''
    _emails = ''
    _mail_from = 'noreply@openerp.com'
    customer = ''
    _log = []
    _raised_errors = None

    def __init__(self, settings=None):
        super(FtpClient, self).__init__()
        self.raised_errors = False
        self.current_dir = os_path_dirname(__file__)
        if settings is not None:
            self.copy_attributes_from_settings(settings)
        self._log = []

    @property
    def raised_errors(self):
        return self._raised_errors

    @raised_errors.setter
    def raised_errors(self, value):
        if (value not in [True, False]):
            raise Exception('raised_errors property may be only True or False!')
        self._raised_errors = value

    def process(self, method, check_ftp=False):
        """
        @param method: load or read/read_new or send
        @return:
        """
        res = ''
        if (self.raised_errors):
            return res
        try:
            if method == 'load':
                res = self.load()
            elif method == 'read':
                res = self.read()
            elif method == 'send':
                res = self.send()
            elif method == 'send_mail':
                res = self.send_mail()
            elif method == 'read_new':
                res = self.read_new(check_ftp)
            else:
                res = False

        except Exception:
            msg = traceback.format_exc()
            self.log({
                'title': msg,
                'type': 'error',
                'message': msg,
            })
            res = False

        return res

    def read(self):
        self.log({
            'type': 'info',
            'message': "This method deprecated, please use read_new for new API's",
        })
        msg = ''
        data = []
        ret = []
        filenames = listdir(self._path_to_local_dir)
        if filenames:
            for filename in filenames:
                response = None
                with open(os_path_join(self._path_to_local_dir, filename), 'r') as fh:
                    file_data = fh.read()
                    if (self._use_decrypt):
                        response = self.set_decrypt_data(file_data)
                        self.check_response(response)
                    else:
                        response = file_data
                if response:
                    if (hasattr(response, 'data') and response.data):
                        response = response.data
                    data.append(response)
                msg += 'filename: ' + str(filename) + '\ndata: \n ' + str(response) + '\n'
            ret = self.parse_response(data)
            self.log({
                'title': "GetFilesFromLocalFolder {0}".format(self.customer),
                'message': 'files in local folder: \n{0}'.format(msg),
            })
            if ret:
                for filename in filenames:
                    remove_path(os_path_join(self._path_to_local_dir, filename))
                return ret
        else:
            _logger.debug("GetFilesFromLocalFolder %s files in local folder is not found" % self.customer)

        return ret

    def load(self):
        now = "{:%Y%m%d_%H%M}".format(datetime.now())
        msg = ""
        self._ftp_object.connect()
        filenames = self._ftp_object.get_filenames()
        customer_ref = self._customer_ref
        if(filenames):
            count_files = 0
            for ftp_filename in filenames:
                filename = ftp_filename
                if(customer_ref):
                    import re
                    if(re.search("""<ftp\[.*\]>""", ftp_filename) is None):
                        filename = '<ftp[{0}]>{1}'.format(customer_ref, ftp_filename)
                write_data = self._ftp_object.get_file_data(ftp_filename)
                if (not self.check_sender_is_correct(write_data)):
                    continue
                if(write_data):
                    res_confirm_load = self.process_confirm_load(ftp_filename, write_data)
                    if not res_confirm_load:
                        continue
                    count_files += 1
                    write_data = self.process_file_data(write_data)
                    _msg = self._ftp_object.save_file(filename, write_data)
                    msg += _msg
                    self._ftp_object.backup_file('{0}_{1}'.format(filename, now), write_data)
                    if(self._remove_flag):
                        self._ftp_object.delete_file_on_ftp(ftp_filename)
                else:
                    msg += 'Error GetFilesFromFTP {0} empty data'.format(self.customer)
            self.log({
                'title': "GetFilesFromFTP [count files: {count}] {customer}".format(
                    count=count_files,
                    customer=self.customer
                ),
                'type': 'debug',
                'message': 'get files from ftp: \n{0}'.format(msg),
            })
        else:
            self.log({
                'type': 'debug',
                'message': 'GetFilesFromFTP method {0} files on ftp not found'.format(self.customer),
            })

        self._ftp_object.disconnect()
        return True

    def send(self):
        result = True
        upload_data = self.upload_data()
        if not isinstance(upload_data, (list, tuple)):
            upload_data = [upload_data]

        for data in upload_data:
            data = self.prepare_data(data)

            # DLMR-1117
            binary = False
            if isinstance(data, BytesIO):
                binary = True
                IO = BytesIO(data.getvalue())
                encrypt_flag = False
                encrypt_data = None
            else:
                if (self._use_encrypt):
                    encrypt_data, encrypt_flag = self.set_encrypt_data(data)
                else:
                    encrypt_data, encrypt_flag = data, False
                IO = cStringIO.StringIO(str(to_ascii(encrypt_data)))
            # END DLMR-1117

            ftp_filename = self.filename
            local_filename = self.actual_local_filename

            self._ftp_object.connect()
            self._ftp_object.backup_file(local_filename, str(IO.getvalue()))

            if encrypt_flag:
                self._ftp_object.backup_file(self.local_filename_for_not_encrypted_file, str(data))

            result &= self._ftp_object.upload_file_to_ftp(ftp_filename, IO)

            if self._ftp_object.send_correct:
                self.log({
                    'title': "SendFilesToFTP {0}".format(self.customer),
                    'type': 'debug',
                    'message': "filename: \n {filename}\n data: \n{data}\nencrypt: {encrypt_data}\n".format(
                        filename=str(ftp_filename),
                        data=str(to_ascii(data)) if not binary else '',
                        encrypt_data=str(to_ascii(encrypt_data)) if not binary else ''
                    )
                })
            else:
                _logger.warn("File was not sent: %s" % ftp_filename)

            self._ftp_object.disconnect()

        return result

    def send_mail(self):
        result = True
        upload_data = self.upload_data()
        if not isinstance(upload_data, (list, tuple)):
            upload_data = [upload_data]

        for data in upload_data:
            data = self.prepare_data(data)

            # print(data)  # Uncomment this only for debug.
            if (self._use_encrypt):
                encrypt_data, encrypt_flag = self.set_encrypt_data(data)
            else:
                encrypt_data, encrypt_flag = data, False

            subject = self._email_title
            body = """
                %s:

                %s
            """ % (subject, data)
            address = self._emails
            msg = MIMEText(data)
            msg['Subject'] = subject
            msg['From'] = self._mail_from
            msg['To'] = address

            s = smtplib.SMTP('localhost')
            s.sendmail(self._mail_from, address.split(','), msg.as_string())
            s.quit()

            if self._ftp_object.send_correct:
                self.log({
                    'title': "SendFilesToFTP {0}".format(self.customer),
                    'type': 'debug',
                    'message': "filename: \n {filename}\n data: \n{data}\nencrypt: {encrypt_data}\n".format(
                        filename=str(ftp_filename),
                        data=str(to_ascii(data)),
                        encrypt_data=str(encrypt_data)
                    )
                })

            self._ftp_object.disconnect()

        return result

    def check_response(self, data):
        if not data:
            return False
        return True

    def use_ftp_settings(self, setting_name, root_dir=None, by_id=None):
        try:
            self._use_ftp_settings(setting_name, root_dir=root_dir, by_id=by_id)
            self.raised_errors = False
        except FtpClientException as ftp_client_ex:
            self.log({
                'title': "use_ftp_settings {customer} exception!".format(customer=self.customer),
                'type': 'exception',
                'message': ftp_client_ex.message,
            })
            self.raised_errors = True
        except Exception as ex:
            self.log({
                'title': "Unknown {customer} exception!".format(customer=self.customer),
                'type': 'exception',
                'message': ex.message,
            })
            self.raised_errors = True
        return True

    def _use_ftp_settings(self, setting_name, root_dir=None, by_id=None):
        if (not self.customer and not self.is_import_orders):
            raise FtpClientException('NONE', "Couldn't set external_customer_id! It's required!")
        attribute_list = [
            '_path_to_backup_local_dir',
            '_path_to_local_dir',
            '_remove_flag',
            '_root_dir',
            '_customer_ref',
            '_ftp_host',
            '_ftp_user',
            '_ftp_passwd',
            '_ftp_timeout',
            '_ftp_port',
            '_path_to_ftp_dir',
            '_ftp_ssl',
            '_ftp_ssh',
            '_ftp_passive_mode',
            '_use_decrypt',
            '_use_encrypt',
            '_setting_action',
        ]
        result = False
        current_setting = {}
        if root_dir is None:
            root_dir = self.customer
        if root_dir is None:
            raise FtpClientException(self.customer, 'Not installed root_dir!')
        for setting in self.multi_ftp_settings:
            if(setting.action == setting_name):
                if(by_id):
                    if(setting.id != by_id):
                        continue
                current_setting.update({
                    'setting_name': setting_name,
                    'ftp_path': setting['ftp_path'],
                    'ftp_file_mask': setting['ftp_file_mask'],
                    'root_dir': root_dir,
                    'setting_action': setting.action,
                })
                for key in setting.ftp_setting_id._columns.keys():
                    current_setting.update({
                        str(key): setting.ftp_setting_id[key],
                    })
                notification_for_customer = setting.notification_for_customer
                emails_list = setting.emails_list
                if(notification_for_customer and emails_list):
                    customer_ref = notification_for_customer.ref
                    external_customer_id = notification_for_customer.external_customer_id
                else:
                    customer_ref = False
                    external_customer_id = False
                current_setting.update({
                    'customer_ref': customer_ref,
                    'attribute_list': attribute_list,
                    'external_customer_id': external_customer_id,
                    'use_decrypt': setting.use_decrypt,
                    'use_encrypt': setting.use_encrypt,
                })
                break

        if(current_setting):
            if(current_setting['ftp_security'] == 'ssh'):
                self._ftp_object = FTPOverSSHObject(self, current_setting)
            elif(current_setting['ftp_security'] == 'ssl'):
                self._ftp_object = FTPOverSSLObject(self, current_setting)
            else:
                self._ftp_object = FTPNormal(self, current_setting)
            result = True
        else:
            raise FtpClientException(
                self.customer,
                "Couldn't find Multi FTP Settings for {setting_name}".format(setting_name=setting_name)
            )

        return result

    def process_file_data(self, data):
        return data

    def check_sender_is_correct(self, data):
        return True

    @property
    def local_filename_for_not_encrypted_file(self):
        result = None
        if (
            hasattr(self, '_local_filename_for_not_encrypted_file') and
            self._local_filename_for_not_encrypted_file
        ):
            result = self._local_filename_for_not_encrypted_file
        else:
            result = '{0}.txt'.format(self.actual_local_filename)
        return result

    @local_filename_for_not_encrypted_file.setter
    def local_filename_for_not_encrypted_file(self, value):
        if not self.raised_errors:
            self._local_filename_for_not_encrypted_file = value

    @property
    def is_import_orders(self):
        if (
            hasattr(self, '_is_import_orders')
            and self._is_import_orders
        ):
            return self._is_import_orders
        else:
            return False

    @is_import_orders.setter
    def is_import_orders(self, value):
        self._is_import_orders = value

    def process_confirm_load(self, ftp_filename, file_data):
        return True


class BaseFTPApiGetOrders(FtpClient):

    def __init__(self, settings):
        super(BaseFTPApiGetOrders, self).__init__()
        self.copy_attributes_from_settings(settings)
