# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from utils import feedutils as feed_utils
from configparser import ConfigParser
import os
import logging
from datetime import datetime

_logger = logging.getLogger(__name__)


class TSCApi(FtpClient):

    def __init__(self, settings):
        super(TSCApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/tsc_settings.ini' % (os.path.dirname(__file__)))


class TSCApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(TSCApiClient, self).__init__(
            settings_variables, False, False, False)

    def confirmShipment(self, lines):
        confirmApi = TSCApiConfirmOrders(self.settings, lines)
        res = bool(confirmApi.process('send'))
        self.extend_log(confirmApi)

        if res and self.is_invoice_required:
            invoiceApi = TSCApiInvoiceOrders(self.settings, lines)
            res = bool(invoiceApi.process('send'))
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = TSCApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class TSCApiConfirmOrders(TSCApi):

    def __init__(self, settings, lines):
        super(TSCApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:TSCConfirm%Y%m%d-%H%M%S.csv}".format(datetime.now())

    def upload_data(self):
        lines = self.lines
        for line in lines:
            date_now = datetime.now()
            if line.get('shp_date', False):
                date_now = datetime.strptime(
                    line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y %I:%M:%S %p")
            line['product_qty'] = int(line['product_qty'])
        return {'lines': lines}


class TSCApiInvoiceOrders(TSCApi):

    def __init__(self, settings, lines):
        super(TSCApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.type_tpl = "string"
        self.use_mako_templates('invoice')
        self.lines = lines
        self.filename = "TSCInvoice_{order}_{prefix}.csv".format(order=str(lines[0]['origin']), prefix=datetime.now().strftime("%m%d%Y%H%M"))

    def upload_data(self):
        lines = self.lines
        for line in lines:
            product_qty = 0
            if(line.get('product_qty', False)):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass

            line['product_qty'] = product_qty

        return {'lines': lines}


class TSCApiUpdateQTY(TSCApi):
    filename = "inventory.csv"
    filename_local = ""
    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(TSCApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.filename_local = "tsc_inventory_%s.csv" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        datas = ["Delmar ID,Customer Sku,Qty\n"]
        for line in self.updateLines:

            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line(',', True)
            if SkuRecord:
                datas.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(datas)[:-2]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
