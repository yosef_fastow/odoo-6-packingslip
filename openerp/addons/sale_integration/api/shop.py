# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.backup_file import backup_file_open
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import time
import os
import logging
from lxml import etree
import paramiko
import cStringIO
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from apiopenerp import ApiOpenerp
from openerp.addons.pf_utils.utils.xml_utils import _try_parse

_logger = logging.getLogger(__name__)


class ShopApi(ApiClient):

    def __init__(self, settings):
        self.saveFlag = settings["saveFlag"] if 'saveFlag' in settings else False
        self.customer = settings["customer"]
        self.multi_ftp_settings = settings["multi_ftp_settings"]
        self.current_dir = os.path.dirname(__file__)
        self._log = []

    def get_ftp_setting(self, ftp_settings, setting_name):
        current_setting = False
        for setting in ftp_settings:
            if setting.action == setting_name:
                current_setting = setting.ftp_setting_id
                ftp_path = setting['ftp_path']
                break
        if not current_setting:
            return False, False
        return current_setting, ftp_path

    def prepare_data(self, data):
        data = """
            <?xml version="1.0" encoding="UTF-8"?>
                <shop_ca_feed id="%s" version="0.1">
                %s
                </shop_ca_feed>
                """ % (self._file_type, data)

        self.filename = self.name + '_' + datetime.now().strftime("%Y%m%d.%H%M%S").replace('.', '_') + '.xml'
        self._log.append({
            'title': "Send %s To Shop" % self.name,
            'msg': data,
            'type': 'send',
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })

        return ' '.join(data.split()).replace('> <', '><')

    def send(self):
        now = time.strftime('%Y%m%d_%H%M', time.gmtime())
        msg = ""
        if(self._method == "get"):
                response = ""
                try:
                    if(self.saveFlag):
                        t = paramiko.Transport((self._ftp_host, 22))
                        t.connect(username=self._ftp_user, password=self._ftp_passwd)
                        sftp = paramiko.SFTPClient.from_transport(t)
                        sftp.chdir(self._path_to_ftp_dir)
                        filenames = sftp.listdir('.')
                        if(filenames):
                            count_files = 0
                            for filename in filenames:
                                IO = cStringIO.StringIO()
                                if filename and filename[0] == '.':
                                    continue
                                try:
                                    write_data = sftp.open(filename, 'r').read()
                                    count_files += 1
                                except Exception, e:
                                    continue
                                if write_data:
                                    blf_utils.file_for_writing(self._path_to_local_dir, filename + '_' + now, str(write_data), 'wb').create()
                                    blf_utils.file_for_writing(self._path_to_backup_local_dir, filename + '_' + now, str(write_data), 'wb').create()
                                    msg += 'filename: ' + self._path_to_ftp_dir + str(filename) + '\ndata: \n' + str(write_data) + '\n'

                                    if self._remove_flag:
                                        sftp.remove(filename)
                                else:
                                    _logger.error('error method send get (%s): %s ' % (self.customer, e))
                                    msg += 'error: ' + 'write data is empty'
                            try:
                                sftp.close()
                            except EOFError:
                                t.close()
                            self._log.append({
                                'title': "GetFilesFromFTP [count files: %s] method: %s (%s)" % (str(count_files), self._method, self.customer),
                                'msg': 'get files from ftp: \n' + msg,
                                'type': 'send',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                        else:
                            print "GetFilesFromFTP method: %s (%s) files on ftp not found" % (self._method, self.customer)

                    else:
                        data = []
                        filenames = os.listdir(self._path_to_local_dir)
                        if(filenames):
                            for filename in filenames:
                                fh = open(os.path.join(self._path_to_local_dir, filename), 'r')
                                response = self.set_decrypt_data(str(fh.read()))
                                fh.close()
                                self.check_response(response)
                                if response:
                                    data.append(response)
                                msg += 'filename: ' + str(filename) + '\ndata: \n ' + str(response) + '\n'
                            ret = self.parse_response(data)
                            self._log.append({
                                'title': "GetFilesFromLocalFolder method: %s (%s)" % (self._method, self.customer),
                                'msg': 'files in local folder: \n' + msg,
                                'type': 'send',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            if(ret):
                                for filename in filenames:
                                    os.remove(os.path.join(self._path_to_local_dir, filename))
                                return ret
                        else:
                            print "GetFilesFromLocalFolder method: %s (%s) files in local folder is not found" % (
                                self._method,
                                self.customer
                            )

                except Exception, e:
                    print 'error method send get: %s  (%s)' % (e, self.customer)
                    self._log.append({
                        'title': 'error method send get (%s)' % (self.customer),
                        'msg': e,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

        elif(self._method == "put"):
                response = ""
                try:
                    upload_data = self.upload_data()
                    response, encrypt_flag = self.set_encrypt_data(upload_data)

                    IO = cStringIO.StringIO(str(response))
                    with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename), 'w') as bf:
                        bf.write(str(IO.getvalue()))

                    if encrypt_flag:
                        with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename + '.txt'), 'w') as bf:
                            bf.write(str(upload_data))

                    t = paramiko.Transport((self._ftp_host, 22))
                    t.connect(username=self._ftp_user, password=self._ftp_passwd)
                    sftp = paramiko.SFTPClient.from_transport(t)
                    sftp.chdir(self._path_to_ftp_dir)
                    sftp.open(self.filename, 'w').write(IO.getvalue())
                    self._log.append({
                        'title': "%s method: %s (%s)" % (self.name, self._method, self.customer),
                        'msg': 'filename: \n %s\nresponse: \n%s\nencrypt responce: %s\n' % (str(self.filename), str(upload_data), str(response)),
                        'type': 'send',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

                    try:
                        sftp.close()
                    except EOFError:
                        t.close()

                    return self.parse_response(response)

                except Exception, e:
                    print 'error method send put: %s, (%s)' % (e, self.customer)
                    self._log.append({
                        'title': 'error method send put',
                        'msg': e,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })


class ShopApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(ShopApiClient, self).__init__(
            settings_variables, ShopOpenerp, False, False)
        self.load_orders_api = ShopApiGetOrdersXML

    def loadOrders(self, saveInFolder=False):
        self.settings["saveFlag"] = saveInFolder and saveInFolder or False
        ordersApi = ShopApiGetOrdersXML(self.settings)
        orders = ordersApi.send()
        self.extend_log(ordersApi)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = ShopApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        self.settings["response"] = ""
        updateApi = ShopApiUpdateQTY(self.settings, lines)
        updateApi.send()
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        self.settings["response"] = ""
        confirmApi = ShopApiSendStatus(self.settings, lines)
        res = confirmApi.send()
        self.extend_log(confirmApi)
        return res

    def cancelOrder(self, lines):
        self.settings["response"] = ""
        orderApi = ShopApiSendStatus(self.settings, lines, 'cancel')
        res = orderApi.send()
        self.extend_log(orderApi)
        return res

    def returnOrders(self, lines):
        self.settings["response"] = ""
        returnApi = ShopApiSendStatus(self.settings, lines, 'return')
        returnApi.send()
        self.extend_log(returnApi)
        return True


class ShopApiGetOrdersXML(ShopApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(ShopApiGetOrdersXML, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'load_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"], True)
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_local_dir': '%s' % str(os.path.join(settings['input_local_path'], "shop", "orders")),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "shop", "orders", "now")
            }
            self.set_ftp_method('get')
            self.set_path_list(path_list)

    def extend_main_order_xml(self, main_xml, extend_xml):
        main = etree.XML(main_xml)
        extend = etree.XML(extend_xml)
        extend_order_items = extend.findall('order_item')
        for order_item in extend_order_items:
            main.append(order_item)
        return main

    def parse_response(self, response):
        if(not response):
            return []
        ordersList = []
        for xml in response:
            try:
                tree = etree.XML(xml)
                orders = {}
                for order in tree.findall('order'):
                    order_id = order.findtext('order_id')
                    main_xml = orders.get(order_id, False)
                    if(main_xml):
                        order = self.extend_main_order_xml(main_xml, etree.tostring(order))
                    orders.update({
                        order_id: etree.tostring(order, pretty_print=True)
                    })
                for key in orders:
                    ordersObj = {}
                    ordersObj['lines'] = []
                    ordersObj['file_id'] = tree.get('id')
                    ordersObj['xml'] = """<shop_ca_feed id="%s" version="0.1">\n %s</shop_ca_feed>""" % (ordersObj['file_id'], orders[key])
                    ordersObj['name'], ordersObj['external_customer_order_id'] = key, key
                    items = etree.XML(ordersObj['xml']).find('order').findall('order_item')
                    for item in items:
                        ordersObj['lines'].append({'external_customer_line_id': item.findtext('order_item_id', False)})
                    ordersList.append(ordersObj)

            except Exception, e:
                print "Can't parse file"
                self._log.append({
                    'title': 'Error: %s' % e,
                    'msg': response,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                return []

        return ordersList


class ShopApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        from lxml.etree import parse, XMLParser
        from StringIO import StringIO
        ordersObj = {}

        if(self.xml == ""):
            return ordersObj

        parser = XMLParser(remove_blank_text=True)
        orders = parse(StringIO(self.xml), parser)
        if(orders is not None):
            ordersObj['partner_id'] = 'Shop_ca'
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['order'] = {}
            ordersObj['lines'] = []

            if orders.find('order') is not None:
                order = orders.find('order')
                ordersObj['order_id'] = order.findtext('order_id', False)
                ordersObj['poNumber'] = order.findtext('order_id', False)
                ordersObj['external_date_order'] = order.findtext('order_create_date', False)
                ordersObj['tax'] = order.findtext('total_tax', False)
                ordersObj['shipping'] = order.findtext('total_shipping_cost', False)
                ordersObj['discount_total'] = order.findtext('total_discount', False)
                ordersObj['total'] = order.findtext('grand_total', False)
                ordersObj['carrier'] = 'CPCPE'
                ordersObj['address']['order']['name'] = "%s %s" % (order.findtext('billing_first_name', False) or '', order.findtext('billing_last_name', False) or '')
                ordersObj['address']['order']['phone'] = order.findtext('billing_phone_number', False)
                ordersObj['address']['order']['email'] = order.findtext('billing_email_address', False)
                ordersObj['address']['order']['address1'] = order.findtext('billing_address_one', False)
                ordersObj['address']['order']['address2'] = order.findtext('billing_address_two', False)
                ordersObj['address']['order']['city'] = order.findtext('billing_city', False)
                ordersObj['address']['order']['state'] = order.findtext('billing_province', False)
                ordersObj['address']['order']['zip'] = order.findtext('billing_postalcode', False)
                ordersObj['address']['order']['country'] = order.findtext('billing_country', False)

                order_item = order.find('order_item')

                ordersObj['address']['ship']['name'] = "%s %s" % (order_item.findtext('shipping_first_name', False) or '', order_item.findtext('shipping_last_name', False) or '')
                ordersObj['address']['ship']['phone'] = order_item.findtext('shipping_phone', False)
                ordersObj['address']['ship']['email'] = order_item.findtext('shipping_email', False)
                ordersObj['address']['ship']['address1'] = order_item.findtext('shipping_address_one', False)
                ordersObj['address']['ship']['address2'] = order_item.findtext('shipping_address_two', False)
                ordersObj['address']['ship']['city'] = order_item.findtext('shipping_city', False)
                ordersObj['address']['ship']['state'] = order_item.findtext('shipping_province', False)
                ordersObj['address']['ship']['zip'] = order_item.findtext('shipping_postalcode', False)
                ordersObj['address']['ship']['country'] = order_item.findtext('shipping_country', False)

                order_items = order.findall('order_item')
                for item in order_items:
                    lineObj = {}
                    lineObj['id'] = item.findtext('order_item_id', False)
                    lineObj['balance_due'] = item.findtext('item_discount', False)
                    lineObj['shipCost'] = item.findtext('item_shipping_cost', False)
                    lineObj['customer_id_delmar'] = item.findtext('sku', False)
                    lineObj['merchantSKU'] = lineObj['customer_id_delmar']
                    lineObj['qty'] = _try_parse(item.findtext('quantity', False), 'int')
                    lineObj['name'] = item.findtext('title', False)
                    lineObj['tax'] = item.findtext('item_tax', False)
                    lineObj['cost'] = item.findtext('item_price', False)
                    lineObj['linePrice'] = item.findtext('extended_item_price', False)
                    ordersObj['lines'].append(lineObj)
        return ordersObj

class ShopApiUpdateQTY(ShopApi):

    name = "SHOP_INV"
    _file_type = "inventory"
    updateLines = []

    def __init__(self, settings, lines):
        super(ShopApiUpdateQTY, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'inventory')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "shop", "inventory", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        self.updateLines = lines
        self.shop_supplier_id = settings['shop_supplier_id']
        self.shop_store_name = settings['shop_store_name']
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = []
        if self.updateLines:
            for line in self.updateLines:
                if line.get('customer_id_delmar', False):
                    store_name = line.get('store_name', False)
                    if store_name in (False, 'False', None, 'None'):
                        store_name = self.shop_store_name
                    lines.append(
                        """ <product>
                                <supplier_id>%s</supplier_id>
                                <store_name>%s</store_name>
                                <sku>%s</sku>
                                <quantity>%s</quantity>
                                <out_of_stock_quantity/>
                                <restock_date></restock_date>
                                <standard_fulfillment_latency/>
                                <priority_fulfillment_latency/>
                                <backorderable>true</backorderable>
                                <return_not_desired/>
                                <inventory_as_of_date>%s</inventory_as_of_date>
                                <external_inventory_id/>
                            </product>
                        """ % (
                            self.shop_supplier_id,
                            store_name,
                            line['customer_id_delmar'],
                            line['qty'],
                            time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        )
                    )
                    self.append_to_revision_lines(line, 'good')
                else:
                    self.append_to_revision_lines(line, 'bad')
        data = ''.join(lines)
        return self.prepare_data(data)

    def parse_response(self, response):
        if(not response):
            return False
        return True

class ShopApiSendStatus(ShopApi):

    confirmLines = []
    _file_type = "order_update"

    def __init__(self, settings, lines, status="confirm"):
        super(ShopApiSendStatus, self).__init__(settings)
        self.shop_supplier_id = settings['shop_supplier_id']
        self.shop_store_name = settings['shop_store_name']
        self.confirmLines = lines
        self.status = status
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'confirm_shipment')

        if status == "confirm":
            local_dir = "confirm_ship"
            self.name = "SHOP_CONFIRM"
        elif (status == "cancel"):
            local_dir = "cancel"
            self.name = "SHOP_CANCEL"
        else:
            local_dir = "return_orders"
            self.name = "SHOP_RETURN"

        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "shop", local_dir, "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)

    def upload_data(self):
        data = """
                    <order>
                        <supplier_id>%s</supplier_id>
                        <store_name>%s</store_name>
                        <order_id>%s</order_id>
                        %s
                    </order>
                """
        external_customer_order_id = self.confirmLines[0]['external_customer_order_id']
        lines = []

        datetime_format = "%Y-%m-%d %H:%M:%S"

        for line in self.confirmLines:
            shp_date = time.gmtime() if not line.get('shp_date', False) else time.strptime(line['shp_date'], "%Y-%m-%d %H:%M:%S")
            shp_date = time.strftime(datetime_format, shp_date)
            shipping_tags = ""
            item_state = ""
            cancel_reason = ""

            if (self.status == "confirm"):
                item_state = "Shipped"
                shipping_tags = """
                                    <carrier_code>%s</carrier_code>
                                    <carrier_name>%s</carrier_name>
                                    <shipping_method>%s</shipping_method>
                                    <tracking_number>%s</tracking_number>
                                """ % (line['carrier_code'],  # carrier_code
                                       line['shp_service'],  # carrier_name
                                       line['carrier'],  # shipping_method
                                       line['tracking_ref'])  # tracking_number
            elif (self.status == "cancel"):
                item_state = "Cancelled"
                cancel_reason = "<cancel_reason>Other</cancel_reason>"
            else:
                item_state = "Return Approved"

            lines.append("""
                            <order_item>
                                <order_item_id>%s</order_item_id>
                                <item_state>%s</item_state>
                                <item_state_date>%s</item_state_date>
                                %s
                                %s
                            </order_item>
                    """ % (
                            line['external_customer_line_id'],  # order_item_id
                            item_state,  # item_state
                            shp_date ,  # item_state_date
                            shipping_tags, # shipping_tags
                            cancel_reason # cancel_reason
                        )
                )

        return self.prepare_data(data % (self.shop_supplier_id, self.shop_store_name, external_customer_order_id, ''.join(lines)))

    def parse_response(self, response):
        if(not response):
            return False
        return True


class ShopOpenerp(ApiOpenerp):

    def __init__(self):
        super(ShopOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if (context is None):
            context = {}
        line_obj = {
            'id': line['id'],
            'balance_due': line['balance_due'],
            'shipCost': line['shipCost'],
            'customer_id_delmar': line['customer_id_delmar'],
            'merchantSKU': line['merchantSKU'],
            'qty': line['qty'],
            'name': line['name'],
            'tax': line['tax'],
            'cost': line['cost'],
            'linePrice': line['linePrice'],
            'notes': '',
        }

        product = False

        field_list = ['customer_id_delmar']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr, uid, context['customer_id'], line[field], tuple(field_list))
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['cost'] = product_cost
            else:
                line_obj['cost'] = False
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
        else:
            line_obj["notes"] += "Can't find product by sku {0}.\n".format(line['sku'])
            line_obj["product_id"] = 0

        return line_obj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
