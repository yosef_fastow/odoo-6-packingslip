# -*- coding: utf-8 -*-
from lxml import etree
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import time
import random
import logging
from apiopenerp import ApiOpenerp

_logger = logging.getLogger(__name__)


class WalmartApi(FtpClient):
    vendor_id = ""

    def __init__(self, settings):
        super(WalmartApi, self).__init__(settings)

    def prepare_data(self, data):

        date = datetime.now().strftime("%Y%m%d.%H%M%S")
        rand = random.randint(100000, 999999)
        data = """
            <?xml version="1.0" encoding="UTF-8"?>
            <WMI>
                <WMIFILEHEADER FILEID="%s.%s.%s" FILETYPE="%s" VERSION="4.0.0">
                    <FH_TO ID="2677" NAME="Walmart.com"/>
                    <FH_FROM ID="%s" NAME="Delmar Mfg LLC">
                        <FH_CONTACT NAME="Moshe Balyasnyy" EMAIL="Moshe@delmarintl.ca" PHONE="5148754800"/>
                    </FH_FROM>
                </WMIFILEHEADER>
                %s
            </WMI>""" % (self.vendor_id, date, str(rand), self._file_type, self.vendor_id, data)
        self.filename = self.name + "_" + self.vendor_id + '_' + date.replace('.', '_') + '_' + str(rand) + '.xml'
        self._log.append({
            'title': "Send %s To Walmart" % self.name,
            'msg': data,
            'type': 'send',
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })
        return ' '.join(data.split())

    def process_confirm_load(self, ftp_filename, file_data):
        orders_list = self.parse_response([file_data])
        file_id = {}
        for i in xrange(0, len(orders_list)):
            if file_id.get(orders_list[i]['file_id'], False) is False:
                file_id[orders_list[i]['file_id']] = 1
                self.WMTAbs.confirmLoad(orders_list[i]['file_id'], orders_list[i]['file_type'])
        return True

    def check_response(self, response):
        res = True
        if not response:
            return False
        return res


class WalmartApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(WalmartApiClient, self).__init__(
            settings_variables, WalmartOpenerp, False, False)
        # NEED FIX: FAST DISABLE RELOAD ORDERS
        # self.settings["order_type"] = 'order'
        # self.load_orders_api = WalmartApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='order'):
        orders_list = []
        self.settings["order_type"] = order_type
        ordersApi = WalmartApiGetOrdersXML(self.settings, self)
        if save_flag:
            ordersApi.process('load')
        else:
            ordersApi.use_ftp_settings('load_orders')
            orders_list += ordersApi.process('read')
            ordersApi.use_ftp_settings('load_cancel_orders')
            orders_list += ordersApi.process('read')
        self.extend_log(ordersApi)
        return orders_list

    def confirmLoad(self, file_id, file_type):
        ordersApi = WalmartApiConfirmLoad(self.settings, file_id, file_type)
        orders = ordersApi.process('send')
        self.extend_log(ordersApi)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = WalmartApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def processingOrderLines(self, lines, state='accepted'):
        self.settings["response"] = ""
        processingApi = WalmartApiProcessingOrderLines(self.settings, lines, state)
        res = processingApi.process('send')
        self.extend_log(processingApi)
        return res

    def confirmShipment(self, lines):
        self.settings["response"] = ""
        confirmApi = WalmartApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        self.settings["response"] = ""
        updateApi = WalmartApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class WalmartApiConfirmLoad(WalmartApi):

    name = "WMI_Confirm"
    _file_type = "FFC"

    def __init__(self, settings, file_id, file_type):
        super(WalmartApiConfirmLoad, self).__init__(settings)
        self.confirmLoad = {'file_id': file_id, 'file_type': file_type}
        self.use_ftp_settings('confirm_load')

    def upload_data(self):
        data = """<WMIFILECONFIRM FILEID="%s" FILETYPE="%s"/>""" % (self.confirmLoad['file_id'], self.confirmLoad['file_type'])

        return data


class WalmartApiProcessingOrderLines(WalmartApi):

    name = "WMI_Order_Status"
    _file_type = "FOS"

    processingLines = []
    #rejected
    #accepted

    def __init__(self, settings, lines, state):
        super(WalmartApiProcessingOrderLines, self).__init__(settings)
        self.processingLines = lines
        self.state = state
        self.use_ftp_settings('confirm_orders')

    def upload_data(self):
        data = "<WMIORDERSTATUS>%s</WMIORDERSTATUS>"
        lines = ""
        for line in self.processingLines:
            qty_str = ''
            statuscode = 'LI'
            if self.state == 'accept_cancel':
                statuscode = 'LC'
            if line.get('cancel_code', False):
                statuscode = line['cancel_code']
                if line.get('product_qty', False):
                    qty_str = 'QUANTITY="%s" ' % str(line['product_qty'])
            lines += '<OS_LINESTATUS REQUESTNUMBER="%s" LINENUMBER="%s" STATUSCODE="%s" %s/>' % (line["external_customer_order_id"], line['external_customer_line_id'], statuscode, qty_str)
        return data % lines


class WalmartApiGetOrdersXML(WalmartApi):

    def __init__(self, settings, WMTAbs):
        super(WalmartApiGetOrdersXML, self).__init__(settings)
        self.WMTAbs = WMTAbs
        if settings['order_type'] == 'order':
            self.use_ftp_settings('load_orders')
        else:
            self.use_ftp_settings('load_cancel_orders')

    def parse_response(self, response):
        if len(response) == 0:
            return []
        ordersList = []
        for xml in response:
            try:
                tree = etree.XML(xml)

                header = tree.find('WMIFILEHEADER')
                file_id = header.get('FILEID')
                file_type = header.get('FILETYPE')

                if tree.find('WMIORDERREQUEST'):
                    root = tree.find('WMIORDERREQUEST')
                    orders = root.findall('OR_ORDER')
                    for order in orders:
                            ordersObj = {}
                            ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                            ordersObj['name'] = order.get('REQUESTNUMBER')
                            ordersObj['file_id'] = file_id
                            ordersObj['file_type'] = file_type
                            ordersObj['external_customer_order_id'] = order.get('REQUESTNUMBER')
                            ordersObj['lines'] = []
                            for line in order.findall('OR_ORDERLINE'):
                                ordersObj['lines'].append({'external_customer_line_id': line.get('LINENUMBER')})
                            ordersList.append(ordersObj)

                elif tree.find('WMIORDERCANCEL'):
                    root = tree.find('WMIORDERCANCEL')
                    orders = root.findall('OC_LINECANCEL')
                    for order in orders:
                            ordersObj = {}
                            ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                            ordersObj['name'] = "CANCEL " + order.get('REQUESTNUMBER') + " LINE ID " + order.get('LINENUMBER')
                            ordersObj['file_id'] = file_id
                            ordersObj['file_type'] = file_type
                            ordersObj['external_customer_order_id'] = order.get('REQUESTNUMBER')
                            ordersObj['lines'] = [{'external_customer_line_id': order.get('LINENUMBER')}]
                            ordersList.append(ordersObj)
            except Exception, e:
                print "Can't parse file"
                self._log.append({
                    'title': 'Error: %s' % e,
                    'msg': response,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                return []

        return ordersList


class WalmartApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        if self.xml == "":
            return {}
        order = etree.XML(self.xml)
        ordersObj = {}

        if order is not None:
            ordersObj['partner_id'] = 'walmart'
            if order.tag == 'OC_LINECANCEL':
                ordersObj['external_customer_order_id'] = order.get('REQUESTNUMBER')
                ordersObj['lines'] = [order.get('LINENUMBER')]
                return ordersObj
            # header = tree.find('WMIFILEHEADER')
            # file_id = header.get('FILEID')

            ordersObj['order_id'] = order.get('REQUESTNUMBER')
            if order.get('ORDERNUMBER', False):
                ordersObj['poHdrData'] = {
                    'custOrderNumber': order.get('ORDERNUMBER')
                }
            ordersObj['poNumber'] = order.get('REQUESTNUMBER')
            ordersObj['external_date_order'] = "%s-%s-%s" % (order.xpath('OR_DATEPLACED/@YEAR')[0], order.xpath('OR_DATEPLACED/@MONTH')[0], order.xpath('OR_DATEPLACED/@DAY')[0])
            ordersObj['address'] = {}

            shipping = order.find('OR_SHIPPING')
            ordersObj['carrier'] = shipping.get('CARRIERMETHODCODE')
            ordersObj['store_number'] = shipping.get('STORENUMBER')
            ordersObj['TOGETHERCODE'] = shipping.get('TOGETHERCODE')
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['phone'] = shipping.xpath('OR_PHONE/@PRIMARY')[0]
            ordersObj['address']['ship']['name'] = shipping.xpath('OR_POSTAL/@NAME')[0]
            ordersObj['address']['ship']['address1'] = shipping.xpath('OR_POSTAL/@ADDRESS1')[0]
            ordersObj['address']['ship']['address2'] = shipping.xpath('OR_POSTAL/@ADDRESS2') and shipping.xpath('OR_POSTAL/@ADDRESS2')[0]
            ordersObj['address']['ship']['city'] = shipping.xpath('OR_POSTAL/@CITY')[0]
            ordersObj['address']['ship']['state'] = shipping.xpath('OR_POSTAL/@STATE')[0]
            ordersObj['address']['ship']['zip'] = shipping.xpath('OR_POSTAL/@POSTALCODE')[0]
            ordersObj['address']['ship']['country'] = shipping.xpath('OR_POSTAL/@COUNTRY')[0]
            ordersObj['address']['ship']['email'] = shipping.findtext('OR_EMAIL')
            ordersObj['delivery_date_order'] = "%s-%s-%s" % (shipping.xpath('OR_DELIVERYDATE/@YEAR')[0], shipping.xpath('OR_DELIVERYDATE/@MONTH')[0], shipping.xpath('OR_DELIVERYDATE/@DAY')[0])
            if shipping.xpath('OR_EXPECTEDSHIPDATE/@YEAR'):
                ordersObj['latest_ship_date_order'] = "%s-%s-%s" % (shipping.xpath('OR_EXPECTEDSHIPDATE/@YEAR')[0], shipping.xpath('OR_EXPECTEDSHIPDATE/@MONTH')[0], shipping.xpath('OR_EXPECTEDSHIPDATE/@DAY')[0])

            ordersObj['total'] = order.xpath('OR_BILLING/@ORDERPRICE')[0]
            or_billing = order.find('OR_BILLING')
            #ordersObj['OR_BILLING_OR_PAYMENT'] = or_billing.xpath('OR_PAYMENT/@METHOD')[0]
            #ordersObj['OR_BILLING_OR_PHONE'] = or_billing.xpath('OR_PHONE/@PRIMARY')[0]
            #ordersObj['OR_BILLING_NAME'] = or_billing.xpath('OR_POSTAL/@NAME')[0]
            #ordersObj['OR_BILLING_ADDRESS1'] = or_billing.xpath('OR_POSTAL/@ADDRESS1')[0]
            #ordersObj['OR_BILLING_ADDRESS2'] = or_billing.xpath('OR_POSTAL/@ADDRESS2')[0]
            #ordersObj['OR_BILLING_CITY'] = or_billing.xpath('OR_POSTAL/@CITY')[0]
            #ordersObj['OR_BILLING_STATE'] = or_billing.xpath('OR_POSTAL/@STATE')[0]
            #ordersObj['OR_BILLING_POSTALCODE'] = or_billing.xpath('OR_POSTAL/@POSTALCODE')[0]
            #ordersObj['OR_BILLING_COUNTRY'] = or_billing.xpath('OR_POSTAL/@COUNTRY')[0]
            #ordersObj['OR_BILLING_OR_EMAIL'] = or_billing.findtext('OR_EMAIL')

            ordersObj['address']['order'] = {}
            ordersObj['address']['order']['name'] = or_billing.xpath('OR_POSTAL/@NAME')[0]
            if ordersObj['address']['order']['name'] == '0':
                ordersObj['address']['order']['name'] = or_billing.xpath('OR_PAYMENT/@METHOD')[0]

            ordersObj['address']['order']['address1'] = or_billing.xpath('OR_POSTAL/@ADDRESS1')[0]
            ordersObj['address']['order']['address2'] = or_billing.xpath('OR_POSTAL/@ADDRESS2') and or_billing.xpath('OR_POSTAL/@ADDRESS2')[0]
            ordersObj['address']['order']['city'] = or_billing.xpath('OR_POSTAL/@CITY')[0]
            ordersObj['address']['order']['state'] = or_billing.xpath('OR_POSTAL/@STATE')[0]
            ordersObj['address']['order']['zip'] = or_billing.xpath('OR_POSTAL/@POSTALCODE')[0]
            ordersObj['address']['order']['country'] = or_billing.xpath('OR_POSTAL/@COUNTRY')[0]
            ordersObj['address']['order']['phone'] = or_billing.xpath('OR_PHONE/@PRIMARY')[0]
            ordersObj['address']['order']['email'] = or_billing.findtext('OR_EMAIL')

            ordersObj['tcnumber'] = order.xpath('OR_RETURNS/@TCNUMBER')[0]
            ordersObj['METHODCODE'] = order.xpath('OR_RETURNS/@METHODCODE')[0]
            or_returns = order.find('OR_RETURNS')
            ordersObj['OR_RETURNS_NAME'] = or_returns.xpath('OR_POSTAL/@NAME')[0]
            ordersObj['OR_RETURNS_ADDRESS1'] = or_returns.xpath('OR_POSTAL/@ADDRESS1')[0]
            ordersObj['OR_RETURNS_ADDRESS2'] = or_returns.xpath('OR_POSTAL/@ADDRESS2') and or_returns.xpath('OR_POSTAL/@ADDRESS2')[0]
            ordersObj['OR_RETURNS_CITY'] = or_returns.xpath('OR_POSTAL/@CITY')[0]
            ordersObj['OR_RETURNS_STATE'] = or_returns.xpath('OR_POSTAL/@STATE')[0]
            ordersObj['OR_RETURNS_POSTALCODE'] = or_returns.xpath('OR_POSTAL/@POSTALCODE')[0]
            ordersObj['OR_RETURNS_COUNTRY'] = or_returns.xpath('OR_POSTAL/@COUNTRY')[0]
            ordersObj['OR_RETURNS_OR_PERMIT_NUMBER'] = or_returns.xpath('OR_PERMIT/@NUMBER')[0]
            ordersObj['OR_RETURNS_OR_PERMIT_CITY'] = or_returns.xpath('OR_PERMIT/@CITY')[0]
            ordersObj['OR_RETURNS_OR_PERMIT_POSTALCODE'] = or_returns.xpath('OR_PERMIT/@POSTALCODE')[0]

            ordersObj['OR_SHIPTOSTORE'] = ''

            if order.find('OR_SHIPTOSTORE') is not None:
                ordersObj['OR_SHIPTOSTORE'] = order.xpath('OR_SHIPTOSTORE/@VENDORID')[0]

            ordersObj["Comments"] = []

            if ordersObj['OR_SHIPTOSTORE'] != '':
                ordersObj['orderType'] = 'SS'
                commentobj = {}
                commentobj["CommentType"] = 'OL'
                commentobj["Text"] = ''
                ordersObj["Comments"].append(commentobj)

            is_signature = False
            ordersObj['lines'] = []
            for line in order.findall('OR_ORDERLINE'):
                lineObj = {}
                lineObj['id'] = line.get('LINENUMBER')
                lineObj['linePrice'] = line.get('LINEPRICE')
                lineObj['customer_sku'] = line.xpath('OR_ITEM/@ITEMNUMBER')[0]
                lineObj['upc'] = line.xpath('OR_ITEM/@UPC')[0]
                lineObj['sku'] = line.xpath('OR_ITEM/@SKU')[0]
                #if lineObj['sku'].find('/') != -1:
                #    lineObj['size'] = lineObj['sku'][lineObj['sku'].find('/') + 1: len(lineObj['sku'])]
                #    lineObj['sku'] = lineObj['sku'][:lineObj['sku'].find('/')]
                lineObj['merchantSKU'] = lineObj['customer_sku']
                lineObj['name'] = line.xpath('OR_ITEM/@DESCRIPTION')[0]
                lineObj['qty'] = line.xpath('OR_ITEM/@QUANTITY')[0]
                lineObj['customerCost'] = line.xpath('OR_PRICE/@RETAIL')[0]
                lineObj['tax'] = line.xpath('OR_PRICE/@TAX')[0]
                lineObj['shipCost'] = line.xpath('OR_PRICE/@SHIPPING')[0]
                lineObj['cost'] = line.xpath('OR_COST/@AMOUNT')[0]

                for dyn_field in line.findall('OR_DYNAMICDATA'):
                    if dyn_field.get('NAME') == 'Size' and lineObj.get('size', False) is False:
                        lineObj['size'] = dyn_field.get('VALUE')

                or_vas = line.find('OR_VAS')

                if or_vas is not None:
                    is_vas_data = or_vas.find('OR_VASDATA')
                    if is_vas_data is not None:
                        if len(is_vas_data.xpath('@NAME')) > 0 and len(is_vas_data.xpath('@VALUE')) > 0:
                            vas_name = or_vas.xpath('OR_VASDATA/@NAME')[0]
                            vas_value = or_vas.xpath('OR_VASDATA/@VALUE')[0]
                            additional_fields = {}
                            if vas_name == 'SOD' and vas_value == 'Y':
                                additional_fields.update({
                                    'signature': 4,
                                })
                                is_signature = True

                ordersObj['lines'].append(lineObj)

            if is_signature is True:
                ordersObj['additional_fields'] = additional_fields

            for i in ('OR_LASTDELIVERYMSG', 'OR_MARKETINGMSG', 'OR_RETURNSMSG'):
                ordersObj[i] = []
                ordersObj[i].append(order.xpath(i+'/@LINE1')[0])
                ordersObj[i].append(order.xpath(i+'/@LINE2')[0])
                ordersObj[i].append(order.xpath(i+'/@LINE3')[0])
                ordersObj[i].append(order.xpath(i+'/@LINE4')[0])

            ordersObj['asnnumber_prefix'] = '00' + '00' + '57932'
        return ordersObj


class WalmartApiConfirmShipment(WalmartApi):

    name = "WMI_Order_Status"
    _file_type = 'FOS'
    confirmLines = []

    def __init__(self, settings, lines):
        super(WalmartApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines

    def upload_data(self):

        data = """<WMIORDERSTATUS><OS_PACKAGEINVOICE REQUESTNUMBER="%s" STATUSCODE="PS">
                    %s
                    %s
                    <OS_INVOICE>
                        %s
                    </OS_INVOICE>
                  </OS_PACKAGEINVOICE></WMIORDERSTATUS>"""

        os_package = """<OS_PACKAGE ASNNUMBER="%s" PACKAGEID="001" CARRIERMETHODCODE="%s" TRACKINGNUMBER="%s" WEIGHT="1" />"""
        os_shipdate = """<OS_SHIPDATE DAY="%s" MONTH="%s" YEAR="%s" HOUR="%s" MINUTE="%s" />"""
        lines = ""

        carrier_code = self.confirmLines[0]['carrier_code']
        tracking_number = self.confirmLines[0]['tracking_number']
        shp_date = self.confirmLines[0]['shp_date']
        asn_number = self.confirmLines[0]['asn_number']
        external_customer_order_id = self.confirmLines[0]['external_customer_order_id']
        for line in self.confirmLines:
            lines += """
                            <OS_SHIPPING SUPPLIERSHIPPING="0" THIRDPARTYSHIPPING="0" />
                            <OS_LINECOST LINENUMBER="%s" QUANTITY="%s" ITEMCOST="%s" />
                    """ % (line['external_customer_line_id'],
                           int(line['product_qty']),
                           str(line['unit_cost']).replace('.0', ''))

        os_package = os_package % (asn_number, carrier_code, tracking_number)

        if shp_date:
            dt = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            dt = datetime.now()
        dates = {}
        for i in ('month', 'day', 'hour', 'minute'):
            dates[i] = eval("dt." + i)
            if dates[i] < 10:
                dates[i] = '0' + str(dates[i])
        os_shipdate = os_shipdate % (str(dates['day']), str(dates['month']), str(dt.year), str(dates['hour']), str(dates['minute']))
        return data % (external_customer_order_id, os_package, os_shipdate, lines)

    def parse_response(self, response):
        if not response:
            return False
        return True


class WalmartApiUpdateQTY(WalmartApi):

    name = "WMI_Inventory"
    _file_type = 'FII'
    updateLines = []

    def __init__(self, settings, lines):
        super(WalmartApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.confirmLines = lines
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        data = "<WMIITEMINVENTORY>%s</WMIITEMINVENTORY>"
        lines = []
        for line in self.updateLines:
            code = 'AC'
            bo = ''
            if line.get('build_to_order', False) == 'BO':
                bo = '<II_DAYS MIN="2" MAX="3"></II_DAYS>'
                code = 'BO'

            # if line['qty'] == 0 and str(line['dnr_flag']) == '1':
            #     code = 'NA'
            #     bo = ''
            #if line['qty'] != 0 and line['qty'] < 3:
            #    line['qty'] = 3
            if line['customer_sku'] and line['customer_id_delmar'] and line['upc']:
                lines.append("""
                    <II_ITEM ITEMNUMBER="%s" SKU="%s" UPC="%s">
                        <II_AVAILABILITY CODE="%s">
                            <II_ONHANDQTY>%s</II_ONHANDQTY>
                            %s
                        </II_AVAILABILITY>
                    </II_ITEM>
                    """ % (line['customer_sku'], line['customer_id_delmar'], line['upc'], code, line['qty'], bo)
                )
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        data %= "".join(lines)
        return data

    def parse_response(self, response):
        if not response:
            return False
        return True


class WalmartOpenerp(ApiOpenerp):

    def __init__(self):
        super(WalmartOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def get_additional_confirm_shipment_information_for_line(
            self, cr, uid,
            partner_id, order, order_line,
            context=None):

        line = {
            "asn_number": order.asn_number
        }

        return line
