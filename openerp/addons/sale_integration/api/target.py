# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from ftplib import FTP
import time
import cStringIO
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging
from utils.ediparser import EdiParser
import random
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
import json
from apiopenerp import ApiOpenerp
from openerp.addons.pf_utils.utils.yamlutils import YamlObject

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',           'Vendor number',            '67380'),
    ('vendor_name',             'Vendor name',              'Delmar MFG'),
    ('sender_id',               'Sender Id',                '67380'),
    ('sender_id_qualifier',     'Sender Id Qualifier',      '11'),
    ('receiver_id',             'Receiver Id',              'TGTDVS'),
    ('receiver_id_qualifier',   'Receiver Id Qualifier',    'ZZ'),
    ('edi_x12_version',         'EDI X12 Version',          '4010'),
    ('filename_format',         'Filename Format',          'DEL{edi_type}_{date}.out'),
    ('line_terminator',         'Line Terminator',          r'~\n'),
    ('repetition_separator',    'Repetition Separator',     '}'),
    ('environment_mode',        'Environment Mode',         'P'),
)

# name: value
DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}


class TargetApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class TargetApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(TargetApiClient, self).__init__(
            settings_variables, TargetOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = TargetApiGetOrders

    def loadOrders(self, save_flag=False):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = TargetApiConfirmLoad(self.settings, orders)
        api.process('send')
        self.extend_log(api)

    def getOrdersObj(self, order_json):
        ordersApi = TargetApiGetOrderObj(order_json)
        order = ordersApi.getOrderObj()
        return order

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = TargetApiProcessingOrderLines(self.settings, lines, state)
        res = ordersApi.process('send')
        self.extend_log(ordersApi)
        return res

    def confirmShipment(self, lines):
        ordersApi = TargetApiConfirmShipment(self.settings, lines)
        res = ordersApi.process('send')
        self.extend_log(ordersApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = TargetApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class TargetApiGetOrders(TargetApi):

    number_of_transaction = ''
    ack_control_number = ''

    def __init__(self, settings):
        super(TargetApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')

    def parse_response(self, data=None):
        if bool(data) is False:
            return []

        orders = []
        for edi in data:
            parse_orders = self.getOrderObj(edi)
            for order in parse_orders:
                orderObj = {
                    'name': order['poNumber'],
                    'ack_control_number': self.ack_control_number,
                    'number_of_transaction': self.number_of_transaction,
                    'functional_group': self.functional_group
                }
                if order['Cancellation'] is True:
                    orderObj['name'] = 'CANCEL ' + orderObj['name']

                orderObj['xml'] = json.dumps(order)
                orders.append(orderObj)
        return orders

    def getOrderObj(self, edi):
        if edi == '':
            return {}

        message = EdiParser(edi.strip())
        return_orders = []
        ordersObj = {}
        for segment in message:
            elements = segment.split(message.delimiters[1])
            if elements[0] == 'GS':
                self.ack_control_number = elements[6]
                self.functional_group = elements[1]
                ordersObj['ack_control_number'] = self.ack_control_number
                ordersObj['functional_group'] = self.functional_group

            elif elements[0] == 'ST':
                ordersObj['address'] = {}
                ordersObj['partner_id'] = 'target'
                ordersObj['lines'] = []
                ordersObj['Cancellation'] = False
                address_type = ''
                if elements[1] == '850':  # Order
                    pass
                elif elements[1] == '860':  # Change
                    pass
                elif elements[1] == '864':  # Errors
                    return {}
                elif elements[1] == '997':  # AKK
                    return {}
            elif elements[0] == 'BCH':  # Change
                if elements[1] == '01':  # Cancellation
                    ordersObj['Cancellation'] = True
                elif elements[1] == '04':  # Change
                    pass
                ordersObj['poNumber'] = elements[3]
                ordersObj['order_id'] = elements[3]
                if len(elements) > 5:
                    ordersObj['external_date_order'] = elements[5]  # CCYYMMDD
                if len(elements) > 11:
                    ordersObj['external_date_cancel'] = elements[11]  # CCYYMMDD
            elif elements[0] == 'BEG':  # Beginning Segment for Purchase Order
                ordersObj['poNumber'] = elements[3]
                ordersObj['order_id'] = elements[3]
                ordersObj['external_date_order'] = elements[5]  #
                if len(elements) > 6:
                    ordersObj['release'] = elements[6]

            elif elements[0] == 'REF':  # Reference Identification
                # When REF01 = D7, this will contain the order type.
                #     32 = Amazon.com,  36 is Target.com
                # When REF01 = OQ, this will contain the Target.com guest order ID
                # When REF01 = VR, this will contain the Target.com assigned vendor number
                # When REF01 = WS, this will contain the EDC
                if elements[1] == 'VR':  # Vendor ID Number
                    ordersObj['vendor_id_number'] = elements[2]  # Division name for packing slip
                elif elements[1] == 'OQ':  # Order Number (Qualifies a code that identifies the authorizing documentation for a household goods)
                    ordersObj['cust_order_number'] = elements[2]
                elif elements[1] == 'WS':  # Warehouse storage location number
                    ordersObj['warehouse_location'] = elements[2]
                elif elements[1] == 'D7':  # Coverage Code (Type of protection provided by an insurance policy)
                    ordersObj['coverage_code'] = elements[2]

            elif elements[0] == 'TD5':  # Carrier Details (Routing Sequence/Transit Time)
                ordersObj['carrier'] = elements[3]  # Code identifying a party or other code (Carrier SCAC Code)
                ordersObj['routing'] = elements[5]  # Free-form description of the routing or requested routing for shipment, or the originating carrier's identity
                # Code    Description
                #     NS       No Signature Required
                #     SG       Signature Required
                ordersObj['carrier'] += '_' + elements[12]  # Service Level Code
                ordersObj['carrier'] += '_' + ordersObj['routing']
                ordersObj['carrier_code'] = elements[3]
                ordersObj['signature'] = elements[5]
                ordersObj['service_code'] = elements[12]
            elif elements[0] == 'N1':
                if elements[1] == 'BT':  # Bill-to-Party
                    address_type = ''
                elif elements[1] == 'SO':  # Sold To If Different From ShipTo
                    address_type = 'order'
                    ordersObj['address']['order'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['order']['name'] = elements[2]
                elif elements[1] == 'ST':  # Ship To
                    address_type = 'ship'
                    ordersObj['address']['ship'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['ship']['name'] = elements[2]
            elif elements[0] == 'N2' and address_type != '':
                ordersObj['address'][address_type]['name2'] = elements[1]
            elif elements[0] == 'N3' and address_type != '':
                ordersObj['address'][address_type]['address1'] = elements[1]
                if len(elements) > 2:
                    ordersObj['address'][address_type]['address2'] = elements[2]
            elif elements[0] == 'N4' and address_type != '':
                ordersObj['address'][address_type]['city'] = elements[1] if len(elements) > 1 else None
                ordersObj['address'][address_type]['state'] = elements[2] if len(elements) > 2 else None
                ordersObj['address'][address_type]['zip'] = elements[3] if len(elements) > 3 else None
                ordersObj['address'][address_type]['country'] = elements[4] if len(elements) > 4 else None
            elif elements[0] == 'PO1':
                lineObj = {
                    'id': elements[1],
                    'qty': elements[2],
                    'cost': elements[4],
                    'name': 'NEED_DATA'
                }
                for i in (6, 8, 10, 12):
                    if len(elements) > i:
                        if elements[i] == 'IN':  # Target.com item ID
                            lineObj['customer_sku'] = lineObj['optionSku'] = elements[i + 1]
                        elif elements[i] == 'SK':  # Vendor SKU
                            lineObj['sku'] = lineObj['vendorSku'] = elements[i + 1]
                        elif elements[i] == 'UP':  # UPC
                            lineObj['merchantSKU'] = lineObj['upc'] = elements[i + 1]
                        elif elements[i] == 'CB':
                            lineObj['dpci'] = elements[i + 1]
                    else:
                        break
                ordersObj['lines'].append(lineObj)
            elif elements[0] == 'PID':
                if elements[2] == '08':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['name'] = elements[5]
            elif elements[0] == 'POC':  # Change
                if elements[2] == 'DI':  # Delete Item(s)
                    lineObj = {
                        'id': elements[1],
                        'qty': elements[3],
                        'qty_left': elements[4]  # Quantity Left to Receive
                    }
                    for i in (8, 10, 12):
                        if len(elements) > i:
                            if elements[i] == 'IN':  # Target.com item ID
                                lineObj['customer_sku'] = lineObj['optionSku'] = elements[i + 1]
                            elif elements[i] == 'SK':  # Vendor SKU
                                lineObj['sku'] = lineObj['vendorSku'] = elements[i + 1]
                            elif elements[i] == 'UP':  # UPC
                                lineObj['merchantSKU'] = lineObj['upc'] = elements[i + 1]
                        else:
                            break
                    ordersObj['lines'].append(lineObj)
            elif elements[0] == 'PKG':
                if len(elements) > 5 and elements[5] == 'GIFTWRAP':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['gift_wrap_indicator'] = True
            elif elements[0] == 'GE':
                self.number_of_transaction = elements[1]
                ordersObj['number_of_transaction'] = self.number_of_transaction
            elif elements[0] == 'MSG':
                ordersObj['lines'][len(ordersObj['lines']) - 1]['message_gift'] = elements[1] or ''
            elif elements[0] == 'SE':
                return_orders.append(ordersObj)

        return return_orders

class TargetApiConfirmLoad(TargetApi):
    orders = []

    def __init__(self, settings, orders):
        super(TargetApiConfirmLoad, self).__init__(settings)
        self.orders = orders
        self.use_ftp_settings('confirm_load')
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:

            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml

            if not order.get('ack_control_number', False):
                raise Exception('Required ack_control_number not find')
            if order.get('ack_control_number', False) and order.get('ack_control_number', False) not in numbers:
                numbers.append(order.get('ack_control_number'))
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))

                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order.get('functional_group', ''),
                    'ack_control_number': order.get('ack_control_number', '')
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': int(order.get('number_of_transaction', ''))
                }))

                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))

        return data


class TargetApiGetOrderObj():

    order_json = ''

    def __init__(self, order_json):
        self.order_json = order_json

    def getOrderObj(self):
        if bool(self.order_json) is False:
            return {}

        return json.loads(self.order_json)


class TargetApiProcessingOrderLines(TargetApi):

    processingLines = []

    def __init__(self, settings, lines, state):
        super(TargetApiProcessingOrderLines, self).__init__(settings)
        self.processingLines = lines
        self.state = state
        self.use_ftp_settings('confirm_orders')

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            return self.accept_cancel()
        self.edi_type = '855'

        lines = []
        statuscode = 'AC'
        if self.state == 'accepted':
            statuscode = 'AC'  # conveys that you are acknowledging the entire purchase order and there are no changes.
            lines = self.processingLines
            self.processingLines = self.processingLines[0]
        elif self.state == 'cancel_line':
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.
        elif self.state == 'cancel':
            lines = self.processingLines
            self.processingLines = self.processingLines[0]
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.
        #   #statuscode = 'RJ'  # conveys that you are acknowledging the order but changes need to be made to the order.

        segments = []
        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'

        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.now().strftime('%Y%m%d')

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(vendor_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'vendor_number': self.vendor_number
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]*[4]*[5]*[6]SK*[7]%(vendorSku)s"
        ack_cancel = "ACK*[1]%(line_status)s*[2]*[3]*[4]*[5]%(date)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]*[16]*[17]*[18]*[19]*[20]*[21]*[22]*[23]*[24]*[25]*[26]*[27]*[28]*[29]%(cancel_code)s"
        ack = "ACK*[1]%(line_status)s*[2]%(qty)s*[3]EA"
        date_now = datetime.now()
        i = 0
        for (i, line) in enumerate(lines):
            # if elements[i] == 'IN':  # Target.com item ID
            #     lineObj['customer_sku'] = lineObj['optionSku'] = elements[i + 1]
            # elif elements[i] == 'SK':  # Vendor SKU
            #     lineObj['sku'] = lineObj['vendorSku'] = elements[i + 1]
            # elif elements[i] == 'UP':  # UPC
            #     lineObj['merchantSKU'] = lineObj['upc'] = elements[i + 1]
            insert_obj = {
                'line_number': line['external_customer_line_id'],
                'qty': int(line['product_qty']),
                'vendorSku': line['vendorSku'],
                #'merchantSKU': line['merchantSKU'],
                #'optionSku': line['optionSku']
            }

            segments.append(self.insertToStr(po1, insert_obj))

            if self.state == 'accepted':
                segments.append(self.insertToStr(ack, {
                    'line_status': 'IA',  # Item Accepted
                    'qty': int(line['product_qty'])
                }))
            else:
                segments.append(self.insertToStr(ack_cancel, {
                    'line_status': 'IR',  # Item Rejected
                    'date': date_now.strftime('%Y%m%d'),   # CCYYMMDD
                    'cancel_code': line['cancel_code'],   # CCYYMMDD
                }))
        if len(lines) > 0:
            ctt = 'CTT*[1]%(count_po1)s'
            segments.append(self.insertToStr(ctt, {
                'count_po1': i + 1
            }))
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '865'
        segments = []
        statuscode = 'AC'
        st = 'ST*[1]865*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA*[1]01*[2]%(statuscode)s*[3]%(po_number)s*[4]*[5]*[6]%(po_date)s'

        date = self.processingLines[0].get('order_date', False) or datetime.utcnow()
        if(isinstance(date, (str, unicode))):
            try:
                date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                date = datetime.now()

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'order_number': self.vendor_number
        }))

        cust_order_number = self.processingLines[0].get('cust_order_number', False)
        if not cust_order_number:
            cust_order_number = self.processingLines[0].get('order_additional_fields', {}).get('cust_order_number', False)
        segments.append(self.insertToStr(ref, {
            'code': 'OQ',  # Order Number
            'order_number': cust_order_number
        }))

        for line in self.processingLines:
            poc = "POC*[1]%(external_customer_line_id)s*[2]DI*[3]%(ordered_qty)s*[4]%(left_to_receive)s*[5]EA*[6]*[7]*[8]SK*[9]7500014628*[10]IN*[11]%(optionSku)s*[12]UP*[13]%(merchantSKU)s"
            segments.append(self.insertToStr(poc, {
                'external_customer_line_id': str(line['external_customer_line_id']),
                'ordered_qty': str(int(line['ordered_qty'])),
                'left_to_receive': str(int(line['left_to_receive'])),
                'optionSku': line['optionSku'],
                'merchantSKU': line['merchantSKU'],
            }))

            # item_status_code
            # ID - Item Deleted
            # IR - Item Rejected
            ack = "ACK*[1]%(item_status_code)s"
            item_status_code = 'ID'
            if self.state == 'cancel':
                item_status_code = 'IR'
                if line.get('cancel_code'):
                    ack += "*[2]*[3]*[4]*[5]*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]*[16]*[17]*[18]*[19]*[20]*[21]**[22]*[23]*[24]*[25]*[26]*[27]*[28]*[29]{cancel_code}".format(cancel_code=line['cancel_code'])

            segments.append(self.insertToStr(ack, {
                'item_status_code': item_status_code,
            }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CA'})


class TargetApiConfirmShipment(TargetApi):

    confirmLines = []

    def __init__(self, settings, lines):
        super(TargetApiConfirmShipment, self).__init__(settings)
        self.confirmLines = lines
        self.use_ftp_settings('confirm_shipment')
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0004'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.now()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '00',  # 00 Original
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]B*[2]2*[3]%(carrier)s*[4]%(transportation_method)s*[5]%(routing)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]%(service_level_code)s*[13]'
        # transportation method
        # A - Air
        # M - Motor (Common Carrier)
        # U - Private Parcel Service

        # routing
        # Code    Description
        # NS - No Signature Required
        # SG - Signature Required
        # CD - Curbside
        # TD - To the Door
        # ID - Through the Door
        # RC - Room of Choice
        # WG - White Glove
        # WA - White Glove Assembly
        # CS - Curbside with Signature Required
        # TS - To the Door with Signature Required
        # IS - Through the Door with Signature Required
        # RS - Room of choice with Signature Required
        # WS - White Glove with Signature Required
        # AS - White Glove Assembly with Signature Required
        # Send code only.  Description present for information only.

        # service_level_code
        # DS - Door Service
        # G2 - Standard Service
        # ND - Next Day Air
        # Delivery during business day hours of next business day
        # PO - P.O. Box/Zip Code
        # SC - Second Day Air
        # Delivery during business day hours no later than second business day

        td5_data = {
            'transportation_method': 'U',
            'carrier': self.confirmLines[0]['carrier_code'],
            'routing': self.confirmLines[0].get('order_additional_fields', {}).get('signature', '') or '',
            'service_level_code': self.confirmLines[0].get('order_additional_fields', {}).get('service_code', '') or ''
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(data)s'

        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'data': self.vendor_number
        }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            'code': '011',  # ???
            'date': shp_date.strftime('%Y%m%d')
        }))

        n1 = 'N1*[1]%(code)s*[2]%(data)s*[3]93*[4]%(ship_from_loc)s'
        segments.append(self.insertToStr(n1, {
            'code': 'SF',  # Ship From
            'data': self.vendor_name,
            'ship_from_loc': 'TAA3'  # Code identifying a party or other code (Ship from location)
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))

        lin = 'LIN*[1]%(line_id)s*[2]%(code_SK)s*[3]%(vendorSku)s*[4]%(code_UP)s*[5]%(merchantSKU)s*[6]%(code_IN)s*[7]%(optionSku)s'
        sn = 'SN1*[1]*[2]%(qty)s*[3]EA'
        pid = 'PID*[1]%(code)s*[2]*[3]*[4]*[5]%(desc)s'

        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            # if elements[i] == 'IN':  # Target.com item ID
            #     lineObj['customer_sku'] = lineObj['optionSku'] = elements[i + 1]
            # elif elements[i] == 'SK':  # Vendor SKU
            #     lineObj['sku'] = lineObj['vendorSku'] = elements[i + 1]
            # elif elements[i] == 'UP':  # UPC
            #     lineObj['merchantSKU'] = lineObj['upc'] = elements[i + 1]
            segments.append(self.insertToStr(lin, {
                'line_id': line['external_customer_line_id'],
                'code_SK': bool(line['optionSku']) and 'SK' or '',
                'vendorSku': line['vendorSku'],
                'code_UP': bool(line['merchantSKU']) and 'UP' or '',
                'merchantSKU': line['merchantSKU'],
                'code_IN': bool(line['vendorSku']) and 'IN' or '',
                'optionSku': line['optionSku']
            }))
            segments.append(self.insertToStr(sn, {
                'qty': int(line['product_qty'])
            }))

            segments.append(self.insertToStr(pid, {
                'code': 'F',  # Free-form
                'desc': line['name'].strip()
            }))

        man = 'MAN*[1]ZZ*[2]%(tracking_number)s'
        segments.append(self.insertToStr(man, {
            'tracking_number': self.confirmLines[0]['tracking_number']
        }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})

    def parse_response(self, response):
        if not response:
            return False
        return True


class TargetApiUpdateQTY(TargetApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(TargetApiUpdateQTY, self).__init__(settings)
        self.updateLines = lines
        self.use_ftp_settings('inventory')
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bia = 'BIA*[1]00*[2]MB*[3]%(reference_identification)s*[4]%(date)s'
        shp_date = datetime.now()

        bia_number = str(random.randrange(100000, 999999))

        segments.append(self.insertToStr(bia, {
            'reference_identification': bia_number,  # unique number identifying this transaction
            'date': shp_date.strftime('%Y%m%d')  # CCYYMMDD
        }))

        ref = 'REF*[1]%(code)s*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'data': self.vendor_number
        }))

        n1 = 'N1*[1]%(code)s*[2]%(name)s'
        segments.append(self.insertToStr(n1, {
            'code': 'DS',  # Vendor
            'name': self.vendor_name
        }))

        lin = 'LIN*[1]%(line_number)s*[2]%(code_SK)s*[3]%(sku)s*[4]%(code_IN)s*[5]%(default_code)s'
        #*[6]code_IN*[7]%(optionSku)s
        #pid = 'PID*[1]%(code)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(desc)s'
        #qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        sdq = 'SDQ*[1]EA*[2]54*[3]AVAIL*[4]%(qty)s*[5]%(code)s*[6]%(num_qty)s*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]*[16]*[17]*[18]*[19]*[20]*[21]*[22]*[23]TAA3'
        i = 0
        for (i, line) in enumerate(self.updateLines):
            if not bool(line['upc']) or not bool(line['customer_id_delmar']):
                self.append_to_revision_lines(line, 'bad')
                continue
            # if elements[i] == 'IN':  # Target.com item ID
            #     lineObj['customer_sku'] = lineObj['optionSku'] = elements[i + 1]
            # elif elements[i] == 'SK':  # Vendor SKU
            #     lineObj['sku'] = lineObj['vendorSku'] = elements[i + 1]
            # elif elements[i] == 'UP':  # UPC
            #     lineObj['merchantSKU'] = lineObj['upc'] = elements[i + 1]
            segments.append(self.insertToStr(lin, {
                'line_number': str(line.get('customer_sku', False) and line['customer_sku'][-4:]),
                'code_SK': bool(line['customer_id_delmar']) and 'SK' or '',
                'sku': line['customer_id_delmar'],
                #'code_UP': bool(line['merchantSKU']) and 'UP' or '',
                #'merchantSKU': line['merchantSKU'],
                'code_IN': bool(line['upc']) and 'IN' or '',
                'default_code': line['upc']
            }))

            try:
                eta_date = datetime.strptime(line.get('eta'), '%m/%d/%Y')
            except:
                eta_date = False

            segments.append(self.insertToStr(dtm, {
                'code': '128',  # Replacement Effective (For use only if the quantity status is On Order or Back Order)
                'date': (eta_date or shp_date).strftime('%Y%m%d')
            }))

            segments.append(self.insertToStr(sdq, {
                'qty': str(line['qty']),  # Product
                'code': line['qty'] > 0 and '' or 'ONBK',
                'num_qty': line['qty'] > 0 and '' or '1',
            }))

            self.append_to_revision_lines(line, 'good')

            # segments.append(self.insertToStr(pid, {
            #     'code': 'F',  # Free-form
            #     'characteristic_code': '08',  # Product
            #     'desc': line['description'].strip()
            # }))

            # segments.append(self.insertToStr(qty, {
            #     'code': '33',  # Quantity Available for Sale (stock quantity)
            #     'qty': str(line['qty']),  # Product
            #     'uof_code': 'EA'  # Each
            # }))

        ctt = 'CTT*[1]%(count_lin)s'
        segments.append(self.insertToStr(ctt, {
            'count_lin': i + 1
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})

    def parse_response(self, response):
        if not response:
            return False
        return True


class TargetOpenerp(ApiOpenerp):

    def __init__(self):
        super(TargetOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields': [
                (0, 0, {'name': 'carrier_code', 'label': 'Carrier Code',
                        'value': order.get('carrier_code', '')}),
                (0, 0, {'name': 'signature', 'label': 'Signature',
                        'value': order.get('signature', '')}),
                (0, 0, {'name': 'service_code', 'label': 'Service code',
                        'value': order.get('service_code', '')}),
                (0, 0, {'name': 'warehouse_location', 'label': 'Warehouse location',
                        'value': order.get('warehouse_location', '')}),
                (0, 0, {'name': 'release', 'label': 'Release #',
                        'value': order.get('release', '')})
            ],
            'cust_order_number': order.get('cust_order_number', False),
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            #'merchantSKU': line['merchantSKU'],
            'merchantSKU': line.get('merchantSKU', False),
            'optionSku': line['optionSku'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty'],
            'gift_wrap_indicator': line.get('gift_wrap_indicator', False),
            'additional_fields': [
                (0, 0, {'name': 'message_gift', 'label': 'Message Gift',
                        'value': line.get('message_gift', '')}),
                (0, 0, {'name': 'dpci', 'label': 'DPCI',
                        'value': line.get('dpci', '')}),
            ]
        }
        product = False
        field_list = ['vendorSku', 'merchantSKU', 'optionSku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[
                                                                                          field],
                                                                                      ('default_code', 'customer_sku', 'customer_id_delmar',
                                                                                       'upc')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        lineObj = {
            'po_number': sale_order['po_number'],
            'cust_order_number': sale_order.cust_order_number or sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR'
        }
        if vendor_address is not False:
            lineObj['address1'] = vendor_address.street
            lineObj['city'] = vendor_address.city
            lineObj['state'] = vendor_address.state_id.code
            lineObj['zip'] = vendor_address.zip
            lineObj['country'] = vendor_address.country_id.code

        result = []
        for order_line in sale_order.order_line:
            line = lineObj.copy()
            line['vendorSku'] = order_line.vendorSku
            line['product_qty'] = int(order_line.product_uom_qty)
            line['external_customer_line_id'] = order_line.external_customer_line_id
            result.append(line)

        return result

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
            so.create_date,
            so.name,
            so.id as order_id,
            st.id as picking_id,
            so.po_number,
            coalesce(so.cust_order_number, so.external_customer_order_id) as cust_order_number,
            sol.external_customer_line_id,
            sol.product_uom_qty as product_qty,
            sol."vendorSku"
        FROM
            sale_order so
            INNER JOIN stock_picking st ON so.id = st.sale_id
            INNER JOIN sale_order_line sol ON sol.order_id = so.id
        WHERE
            so.po_number = %s AND
            so.partner_id IN %s""", (cancel_obj['poNumber'], tuple(customer_ids)))

        return cr.dictfetchall()
