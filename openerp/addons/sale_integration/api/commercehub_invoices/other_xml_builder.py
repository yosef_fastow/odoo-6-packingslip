# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from osv import osv
from dateutil import tz
import configparser


class CommercehubCreateInvoiceXmlMain(object):

    def get_ini_settings(self, params, line, skip_invalid_line=False):
        result = {}
        for key, value in params:
            if value and value[0] == '$':
                if isinstance(line[value[1:]], (unicode, str)):
                    line[value[1:]] = line[value[1:]].strip()
                line_value = line[value[1:]] if line[value[1:]] not in ('False', 'None') else ''
                if skip_invalid_line and line_value != 0 and not line_value and value[1:] != 'eta_date':
                    return False
                else:
                    result[key] = line_value
            else:
                result[key] = value
        return result

    def createXmlInvoice(self, lines, customer, current_dir, setting):

        customer_setting = '%s/commercehub_settings/%s/invoice.ini' % (current_dir, customer)
        config = configparser.ConfigParser()
        config.read(customer_setting)
        invoice_xml = ""
        invoice_amount = 0

        for line in lines:
            invoice_amount += float(line.get("amount_tax", 0.0)) + float(line['unit_cost']) * int(line['product_qty'])
        for line in lines:
            line['invoice_amount'] = "%.2f" % float(invoice_amount)
        for line in lines:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('EST')
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                utc = dt.replace(tzinfo=from_zone)
                dt_est = utc.astimezone(to_zone)
            else:
                dt = datetime.now()
                utc = dt.replace(tzinfo=from_zone)
                dt_est = datetime.now(to_zone)

            line['date'] = dt_est.strftime("%Y%m%d")
            trxDueDate = dt_est + timedelta(days=30)
            line['trxDueDate'] = trxDueDate.strftime("%Y%m%d")
            line['int_product_qty'] = int(line['product_qty'])
            line['line_amount'] = "%.2f" % (float(line['unit_cost']) * int(line['product_qty']))
            line['amount_total'] = "%.2f" % float(line["amount_total"])
            line['unit_cost'] = "%.2f" % float(line['unit_cost'])
            line['DetailRecord'] = self.get_ini_settings(config['DetailRecord'].items(), line)
            line['HeaderRecord'] = self.get_ini_settings(config['HeaderRecord'].items(), line)
            trxBalanceDue = line['HeaderRecord']['invoice_amount'] and float(line['HeaderRecord']['invoice_amount']) or 0
            discount_percentage = line['HeaderRecord']['discount_percentage'] and float(line['HeaderRecord']['discount_percentage']) or 0
            line['trxDiscount'] = line['HeaderRecord']['discount_available']
            if invoice_xml == "":
                invoice_xml = """<?xml version="1.0" encoding="utf-8"?>
                        <InvoiceMessageBatch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <partnerID roleType="vendor">delmarmfg</partnerID>
                        <hubInvoice>
                            <participatingParty roleType="merchant" participationCode="To:">%s</participatingParty>
                            <partnerTrxID>%s</partnerTrxID>
                            <partnerTrxDate>%s</partnerTrxDate>
                            <poNumber>%s</poNumber>
                            <trxDueDate>%s</trxDueDate>
                            <trxBalanceDue>%s</trxBalanceDue>
                            <trxDiscount>%s</trxDiscount>
                            <trxData>
                                %s
                            </trxData>
                """ % (
                    line['HeaderRecord']['invoice_id'],
                    line['HeaderRecord']['invoice_number'],
                    line['HeaderRecord']['invoice_date'],
                    line['HeaderRecord']['purchase_order_number'],
                    line['trxDueDate'],
                    line['HeaderRecord']['invoice_amount'],
                    line['trxDiscount'],
                    """<discountBreakout
                            discDateCode="%s"
                            discDaysDue="%s"
                            discPercent="%s"
                            discTypeCode="%s"
                            netDaysDue="%s"
                        >%s</discountBreakout>""" % (line['HeaderRecord']['discount_date_indicator'],
                    line['HeaderRecord']['discount_due_days'],
                    line['HeaderRecord']['discount_percentage'],
                    line['HeaderRecord']['discount_type'],
                    line['HeaderRecord']['net_due_days'],
                    line['HeaderRecord']['discount_available']
                    ) if line['HeaderRecord']['discount_date_indicator'] else '<discountBreakout discDateCode="1" discDaysDue="15" discPercent="0.0" discTypeCode="01" netDaysDue="30">0.0</discountBreakout>'
                )

            invoice_xml += """
                <hubAction>
                    <action>v_invoice</action>
                    <merchantLineNumber>%s</merchantLineNumber>
                    <trxQty>%s</trxQty>
                    <trxUnitCost>%s</trxUnitCost>
                    <invoiceDetailLink invoiceDetailID="INV1"/>
                    <packageDetailLink packageDetailID="PAC1"/>
                </hubAction>
            """ % (
                line['DetailRecord']['purchase_order_line_number'],
                line['DetailRecord']['invoice_quantity'],
                line['DetailRecord']['unit_cost'])

        if not setting['partner_person_place_id']:
            raise Exception('params partner_person_place_id empty')

        try:
            invoice_xml += """
                        <invoiceDetail invoiceDetailID="INV1">
                            <remitTo personPlaceID="PP1"/>
                        </invoiceDetail>
                        <packageDetail packageDetailID="PAC1">
                            <shipDate>%s</shipDate>
                        </packageDetail>
                        <personPlace personPlaceID="PP1">
                            <name1>Delmar Mfg., LLC</name1>
                            <address1>4058 jean talon ouest</address1>
                            <city>Montreal</city>
                            <state>QC</state>
                            <country>CAN</country>
                            <postalCode>H4P1V5</postalCode>
                            <partnerPersonPlaceId>%s</partnerPersonPlaceId>
                        </personPlace>
                    </hubInvoice>
                    <messageCount>%s</messageCount>
                </InvoiceMessageBatch>
            """ % (line['DetailRecord']['package_ship_date'], setting['partner_person_place_id'], 1)
        except Exception as err:
            print("params not found: ".format(err.message))

        return invoice_xml


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
