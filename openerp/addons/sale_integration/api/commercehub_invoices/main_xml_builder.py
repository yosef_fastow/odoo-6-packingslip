# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil import tz
import configparser


class CommercehubCreateInvoiceXmlMain(object):

    def get_ini_settings(self, params, line, skip_invalid_line=False):
        result = {}
        for key, value in params:
            if value and value[0] == '$':
                if isinstance(line[value[1:]], (unicode, str)):
                    line[value[1:]] = line[value[1:]].strip()
                line_value = line[value[1:]] if line[value[1:]] not in ('False', 'None') else ''
                if skip_invalid_line and line_value != 0 and not line_value and value[1:] != 'eta_date':
                    return False
                else:
                    result[key] = line_value
            else:
                result[key] = value
        return result

    def createXmlInvoice(self, lines, customer, current_dir, setting):

        customer_setting = '%s/commercehub_settings/%s/invoice.ini' % (current_dir, customer)
        config = configparser.ConfigParser()
        config.read(customer_setting)
        invoice_xml = ""
        invoice_amount = 0
        amount_tax = 0
        gst = 0
        hst = 0
        qst = 0
        pst = 0
        for line in lines:
            invoice_amount += float(line.get('unit_cost', 0.0)) * float(line['product_qty'])
            amount_tax += float(line.get('gst', 0.0)) + float(line.get('pst', 0.0)) + float(line.get('hst', 0.0)) + float(line.get('qst', 0.0))
            qst += float(line.get('qst', 0.0))
            gst += float(line.get('gst', 0.0))
            hst += float(line.get('hst', 0.0))
            pst += float(line.get('pst', 0.0))
        invoice_amount += amount_tax
        for line in lines:
            line['invoice_amount'] = "%.2f" % float(invoice_amount)
            line['gst'] = "%.2f" % gst
            line['pst'] = "%.2f" % pst
            line['hst'] = "%.2f" % hst
            line['qst'] = "%.2f" % qst
            line['amount_tax'] = "%.2f" % amount_tax
        for line in lines:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('EST')
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                utc = dt.replace(tzinfo=from_zone)
                dt_est = utc.astimezone(to_zone)
            else:
                dt = datetime.now()
                utc = dt.replace(tzinfo=from_zone)
                dt_est = datetime.now(to_zone)

            line['date'] = dt_est.strftime("%Y%m%d")
            line['int_product_qty'] = int(line['product_qty'])
            line['line_amount'] = "%.2f" % (float(line['unit_cost']) * int(line['product_qty']))
            line['amount_total'] = "%.2f" % float(line["amount_total"])
            line['unit_cost'] = "%.2f" % float(line['unit_cost'])
            line['gst'] = "%.2f" % float(line['gst'])
            line['pst'] = "%.2f" % float(line['pst'])
            line['hst'] = "%.2f" % float(line['hst'])
            line['qst'] = "%.2f" % float(line['qst'])

            line['DetailRecord'] = self.get_ini_settings(config['DetailRecord'].items(), line)
            line['DetailRecord']['qst'] = line['DetailRecord'].get('qst', '0.00')
            line['HeaderRecord'] = self.get_ini_settings(config['HeaderRecord'].items(), line)

            if invoice_xml == "":
                invoice_xml = """<?xml version="1.0" encoding="utf-8"?>
                        <InvoiceMessageBatch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <partnerID roleType="vendor">delmarmfg</partnerID>
                        <hubInvoice>
                            <participatingParty roleType="merchant" participationCode="To:">{invoice_id}</participatingParty>
                            <partnerTrxID>{invoice_number}</partnerTrxID>
                            <partnerTrxDate>{invoice_date}</partnerTrxDate>
                            <poNumber>{purchase_order_number}</poNumber>
                            <trxCurrency>{currency}</trxCurrency>
                            <trxTax>{amount_tax}</trxTax>
                            <trxBalanceDue>{invoice_amount}</trxBalanceDue>
                            <trxData>
                                <taxBreakout taxType="VA">{hst}</taxBreakout>
                                <taxBreakout taxType="CG">{gst}</taxBreakout>
                                <taxBreakout taxType="ST">{qst}</taxBreakout>
                                <orderDate>{invoice_date}</orderDate>
                            </trxData>
                """.format(**line['HeaderRecord'])

            invoice_xml += """
                <hubAction>
                    <action>v_invoice</action>
                    <merchantLineNumber>{purchase_order_line_number}</merchantLineNumber>
                    <trxQty>{invoice_quantity}</trxQty>
                    <trxUnitCost>{unit_cost}</trxUnitCost>
                    <invoiceDetailLink invoiceDetailID="INV1"/>
                </hubAction>
            """.format(**line['DetailRecord'])

        invoice_xml += """
                    <invoiceDetail invoiceDetailID="INV1">
                        <remitTo personPlaceID="PP1"/>
                    </invoiceDetail>
                    <personPlace personPlaceID="PP1">
                        <name1>Delmar Mfg., LLC</name1>
                        <address1>4058 jean talon ouest</address1>
                        <city>Montreal</city>
                        <state>QC</state>
                        <country>CAN</country>
                        <postalCode>H4P1V5</postalCode>
                    </personPlace>
                </hubInvoice>
                <messageCount>1</messageCount>
            </InvoiceMessageBatch>
        """

        return invoice_xml


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
