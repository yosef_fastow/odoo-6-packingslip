# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil import tz
import configparser
import math


class CommercehubCreateInvoiceXmlMain(object):

    def get_ini_settings(self, params, line, skip_invalid_line=False):
        result = {}
        for key, value in params:
            if value and value[0] == '$':
                if isinstance(line[value[1:]], (unicode, str)):
                    line[value[1:]] = line[value[1:]].strip()
                line_value = line[value[1:]] if line[value[1:]] not in ('False', 'None') else ''
                if skip_invalid_line and line_value != 0 and not line_value and value[1:] != 'eta_date':
                    return False
                else:
                    result[key] = line_value
            else:
                result[key] = value
        return result

    def createXmlInvoice(self, lines, customer, current_dir, setting):

        customer_setting = '%s/commercehub_settings/%s/invoice.ini' % (current_dir, customer)
        config = configparser.ConfigParser()
        config.read(customer_setting)
        invoice_xml = ""
        invoice_amount = 0
        amount_tax = 0

        # set taxes and calculate invoice total amounts
        total_gst = 0.0
        total_hst = 0.0
        total_qst = 0.0
        for line in lines:
            line['line_amount'] = float(line.get('unit_cost', 0.0)) * int(line['product_qty'])
            # do not print taxes for Zales
            if 'ZLS' in line['invoice']:
                line['gst'] = line['qst'] = line['hst'] = line['pst'] = line['line_tax'] = "%.2f" % 0.0
            else:
                line_amount = line.get('line_amount', 0.0)
                line['gst'] = "%.2f" % float(line.get('gst', 0.0))
                line['hst'] = "%.2f" % float(line.get('hst', 0.0))
                line['qst'] = "%.2f" % float(line.get('qst', 0.0))
                total_gst += float(line.get('gst', 0.0))
                total_hst += float(line.get('hst', 0.0))
                total_qst += float(line.get('qst', 0.0))
#                line['gst'] = "%.2f" % ((math.trunc((line_amount/100.)*5.25 * 100.)) / 100.)
#                line['qst'] = "%.2f" % ((math.trunc((line_amount/100.)*10.25 * 100.)) / 100.)
#                line['hst'] = "%.2f" % ((math.trunc((line_amount/100.)*15.25 * 100.)) / 100.)
                line['pst'] = line.get('qst', 0.00)
                line['line_tax'] = float(line.get('gst', 0.0)) + float(line.get('qst', 0.0)) + float(line.get('hst', 0.0))

            amount_tax += float(line.get('line_tax', 0.0))
            invoice_amount += float(line.get('line_amount', 0.0)) + float(line.get('line_tax', 0.0)) + float(line.get('shp_handling', 0.0))

        # final checks and XML-creation
        for line in lines:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('EST')
            if line['shp_date']:
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                utc = dt.replace(tzinfo=from_zone)
                dt_est = utc.astimezone(to_zone)
            else:
                dt = datetime.now()
                utc = dt.replace(tzinfo=from_zone)
                dt_est = datetime.now(to_zone)

            # set total invoice amount for each line :? WTF?
            line['invoice_amount'] = "%.2f" % float(invoice_amount)
            line['amount_tax'] = "%.2f" % float(amount_tax)

            # use customer prefix to clear invoice number
            customer_prefix = 'WMC'
            if customer == 'signetjewelers':
                customer_prefix = 'ZLS' if 'ZLS' in line['invoice'] else 'PPL' if 'PPL' in line['invoice'] else 'WMC'
            if customer == 'macys':
                customer_prefix = 'MCYB'

            line['total_gst'] = "%.2f" % total_gst
            line['total_hst'] = "%.2f" % total_hst
            line['total_qst'] = "%.2f" % total_qst
            line['date'] = dt_est.strftime("%Y%m%d")
            line['int_product_qty'] = int(line['product_qty'])
            line['line_amount'] = "%.2f" % float(line['line_amount'])
            line['amount_total'] = "%.2f" % float(line["amount_total"])
            line['unit_cost'] = "%.2f" % float(line['unit_cost'])

            line['DetailRecord'] = self.get_ini_settings(config['DetailRecord'].items(), line)
            line['HeaderRecord'] = self.get_ini_settings(config['HeaderRecord'].items(), line)

            # clear customer prefix for WMC/PPL/ZLS
            if customer_prefix in ('WMC', 'PPL', 'ZLS'):
                invoice_number = line['HeaderRecord']['invoice_number'].replace(customer_prefix, '') + datetime.utcnow().strftime('%m%d%H%M')
            if customer_prefix in ('MCYB'):
                invoice_number = line['HeaderRecord']['invoice_number'].replace(customer_prefix, '') + datetime.utcnow().strftime('%H%M')

            # hardcode currency for PPL
            if customer_prefix == 'PPL':
                line['HeaderRecord']['currency'] = 'CAD'
            #if customer_prefix == 'ZLS':
            line['HeaderRecord']['shipping_charges'] = "%.2f" % float(line['shp_handling'])

            if customer == 'hbc':
                invoice_number = "%08d" % (
                int(line['HeaderRecord']['invoice_number'].replace(setting['customer_root_dir'], '')))
                line['HeaderRecord']['currency'] = 'USD'

            vendor_sku = ""
            if customer in ['bjs', 'macys']:
                line['HeaderRecord']['currency'] = 'USD'
                vendor_sku = """<trxVendorSKU>{vendor_sku}</trxVendorSKU>""".format(**line['DetailRecord'])

            line_tax_xml = """<trxLineTax>{line_tax}</trxLineTax>""".format(**line['DetailRecord'])
            if customer_prefix in ['PPL']:
                line_tax_xml = ''

            line['HeaderRecord']['invoice_number'] = invoice_number
            if invoice_xml == "":
                invoice_xml = """<?xml version="1.0" encoding="utf-8"?>
                        <InvoiceMessageBatch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <partnerID roleType="vendor">delmarmfg</partnerID>
                        <hubInvoice>
                            <participatingParty roleType="merchant" participationCode="To:">{invoice_id}</participatingParty>
                            <partnerTrxID>{invoice_number}</partnerTrxID>
                            <partnerTrxDate>{invoice_date}</partnerTrxDate>
                            <poNumber>{purchase_order_number}</poNumber>
                            <trxCurrency>{currency}</trxCurrency>
                            <trxTax>{amount_tax}</trxTax>
                            <trxShipping>{shipping_charges}</trxShipping>
                            <trxBalanceDue>{invoice_amount}</trxBalanceDue>
                            <orderDate>{invoice_date}</orderDate>
                """.format(**line['HeaderRecord'])
                if customer_prefix == 'PPL':
                    people_extra = """<trxData>
                            <taxBreakout taxType="VA">{total_hst}</taxBreakout>
                            <taxBreakout taxType="CG">{total_gst}</taxBreakout>
                            <taxBreakout taxType="ST">{total_qst}</taxBreakout>
                        </trxData>
                    """.format(**line['HeaderRecord'])
                    invoice_xml += people_extra

            invoice_xml_text = """
                            <hubAction>
                    <action>v_invoice</action>
                    <merchantLineNumber>{purchase_order_line_number}</merchantLineNumber>
                    
                    %s
                    
                    <trxQty>{invoice_quantity}</trxQty>
                    <trxUnitCost>{unit_cost}</trxUnitCost>
                    %s
                    """ % (vendor_sku, line_tax_xml)
            if customer_prefix != 'PPL':
                invoice_xml_text += """
                    <trxItemData>
                        <taxBreakout taxType="VA">{hst}</taxBreakout>
                        <taxBreakout taxType="CG">{gst}</taxBreakout>
                        <taxBreakout taxType="ST">{qst}</taxBreakout>
                    </trxItemData>
                    """
            invoice_xml_text += """
                    <invoiceDetailLink invoiceDetailID="INV1"/>
                </hubAction>
            """

            if customer in ['macys']:
                invoice_xml_text = """
                                <hubAction>
                        <action>v_invoice</action>
                        <merchantLineNumber>{purchase_order_line_number}</merchantLineNumber>

                        %s

                        <trxQty>{invoice_quantity}</trxQty>
                        <trxUnitCost>{unit_cost}</trxUnitCost>
                        <trxLineTax>{line_tax}</trxLineTax>
                        <invoiceDetailLink invoiceDetailID="INV1"/>
                    </hubAction>
                            """ % (vendor_sku)

            invoice_xml += invoice_xml_text.format(**line['DetailRecord'])

        invoice_xml += """
                    <invoiceDetail invoiceDetailID="INV1">
                        <remitTo personPlaceID="PP1"/>
                    </invoiceDetail>
                    <personPlace personPlaceID="PP1">
                        <name1>Delmar Mfg., LLC</name1>
                        <address1>4058 jean talon ouest</address1>
                        <city>Montreal</city>
                        <state>QC</state>
                        <country>CAN</country>
                        <postalCode>H4P1V5</postalCode>
                    </personPlace>
                </hubInvoice>
                <messageCount>1</messageCount>
            </InvoiceMessageBatch>
        """

        return invoice_xml

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
