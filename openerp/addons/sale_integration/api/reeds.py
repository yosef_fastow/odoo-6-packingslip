# -*- coding: utf-8 -*-

from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from utils import feedutils as feed_utils
from configparser import ConfigParser
import os
import logging
from datetime import datetime

_logger = logging.getLogger(__name__)


class ReedsApi(FtpClient):

    def __init__(self, settings):
        super(ReedsApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/reeds_settings.ini' % (os.path.dirname(__file__)))


class ReedsApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(ReedsApiClient, self).__init__(
            settings_variables, False, False, False)

    def updateQTY(self, lines, mode=None):
        updateApi = ReedsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class ReedsApiUpdateQTY(ReedsApi):
    filename = "inventory.csv"
    filename_local = ""
    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(ReedsApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.filename_local = "reeds_inventory_%s.csv" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        datas = ["Model Number,Customer Sku,UPC,Cost,Qty\n"]
        for line in self.updateLines:
            if not line['upc']:
                line['upc'] = "000000000000"

            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line(',', True)
            if SkuRecord:
                SkuRecord = SkuRecord.replace('000000000000', '')
                datas.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(datas)[:-2]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
