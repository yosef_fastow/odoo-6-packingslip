# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from datetime import datetime
from pf_utils.utils.re_utils import f_d
import logging

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class GenericFtpInvApi(FtpClient):

    def __init__(self, settings):
        super(GenericFtpInvApi, self).__init__(settings)


class GenericFtpInvApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(GenericFtpInvApiClient, self).__init__(settings_variables, None, None, None)

    def updateQTY(self, lines, mode=None):
        updateApi = GenericFtpInvApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        confirmApi = GenericFtpInvApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res


class GenericFtpInvApiUpdateQTY(GenericFtpInvApi):

    def __init__(self, settings, lines):
        super(GenericFtpInvApiUpdateQTY, self).__init__(settings)
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'], "generic_ftp_inv/%s" % (settings['customer'] or 'common'), "inventory", "now"
        ) or ""
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = f_d('inventory_%Y%m%d%_H%M%S.csv', datetime.utcnow())
        self.filename_local = f_d('inventory_%Y%m%d%_H%M%S.csv', datetime.utcnow())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        for line in self.lines:
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class GenericFtpInvApiConfirmShipment(GenericFtpInvApi):

    def __init__(self, settings, lines):
        super(GenericFtpInvApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d('Confirm%Y%m%d-%H:%M:%S:%f.csv', datetime.utcnow())

    def upload_data(self):
        lines = self.lines
        all_qty = 0
        for line in lines:
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1
            all_qty += qty
        lines[0]['all_qty'] = all_qty
        return {'lines': lines}
