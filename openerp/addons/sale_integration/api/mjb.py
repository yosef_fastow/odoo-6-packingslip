# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


class MjbApi(FtpClient):

    def __init__(self, settings):
        super(MjbApi, self).__init__(settings)


class MjbApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(MjbApiClient, self).__init__(
            settings_variables, False, False, DEFAULT_VALUES)

    def confirmShipment(self, lines):
        confirmApi = MjbApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = MjbApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class MjbApiConfirmOrders(MjbApi):

    def __init__(self, settings, lines):
        super(MjbApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:MJBConfirm%Y%m%d-%H%M%S.csv}".format(datetime.now())

    def upload_data(self):
        lines = self.lines
        for line in lines:
            date_now = datetime.now()
            if line.get('shp_date', False):
                date_now = datetime.strptime(
                    line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y %I:%M:%S %p")
            line['product_qty'] = int(line['product_qty'])
        return {'lines': lines}


class MjbApiUpdateQTY(MjbApi):

    def __init__(self, settings, lines):
        super(MjbApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "inv.csv"
        self.filename_local = "{:MJBINVENTORY%Y%m%d%H%M%S.csv}".format(
            datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if((line.get('sku', False)) and (line.get('sku', False) == '') and (line.get('customer_sku', False))):
                self.append_to_revision_lines(line, 'bad')
                continue
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}
