# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from apiopenerp import ApiOpenerp
from customer_parsers.ice_au_input_csv_parser import CSVParser
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from datetime import datetime
import traceback
import logging
from openerp.addons.pf_utils.utils.decorators import return_ascii

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('bulk_carriers_list',       'Bulk Carrier List',       "['Landmark']"),
)

DEFAULT_VALUES = {
    'use_ftp': True
}


class IceAUApi(FtpClient):

    def __init__(self, settings):
        super(IceAUApi, self).__init__(settings)

    @return_ascii
    def prepare_data(self, data):
        data = super(IceAUApi, self).prepare_data(data)
        return data

    def upload_data(self):
        return {'lines': self.lines}


class IceAUApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(IceAUApiClient, self).__init__(
            settings_variables, IceAUOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.settings['order_type'] = 'normal'
        self.load_orders_api = IceAUApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='normal'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        self.settings["order_type"] = order_type
        api = IceAUApiGetOrdersXML(self.settings)
        if save_flag:
            orders = api.process('load')
        else:
            orders = api.process('read_new')
        self.extend_log(api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = IceAUApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        confirmApi = IceAUApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = IceAUApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class IceAUApiGetOrdersXML(IceAUApi):

    def __init__(self, settings):
        super(IceAUApiGetOrdersXML, self).__init__(settings)
        self.order_type = settings['order_type']
        self.use_ftp_settings('load_orders')
        if(self.order_type == 'normal'):
            self.use_ftp_settings('load_orders')
        else:
            self.use_ftp_settings('load_cancel_orders')
            self.bulk_carriers_list = eval(self.bulk_carriers_list) if self.bulk_carriers_list else ['Landmark']
        self.parser = CSVParser(self.customer, ',', '"', self.bulk_carriers_list)


class IceAUApiGetOrderObj(YamlOrder):

    def getOrderObj(self):
        order_obj = {}

        if(self.serialized_order == ""):
            return order_obj

        yaml_obj = YamlObject()
        try:
            order = yaml_obj.deserialize(_data=self.serialized_order)
            if(order is not None):
                order_obj['partner_id'] = order.get('partner_id', False)
                order_obj['order_id'] = order.get('order_id', False)
                order_obj['carrier'] = order.get('carrier', False)
                order_obj['external_date_order'] = order.get('external_date_order', False)
                order_obj['address'] = {}
                order_obj['address']['ship'] = {}
                order_obj['address']['ship']['name'] = order['address']['ship'].get('name', '')
                order_obj['address']['ship']['address1'] = order['address']['ship'].get('address1', '')
                order_obj['address']['ship']['address2'] = order['address']['ship'].get('address2', '')
                order_obj['address']['ship']['city'] = order['address']['ship'].get('city', '')
                order_obj['address']['ship']['state'] = order['address']['ship'].get('state', '')
                order_obj['address']['ship']['zip'] = order['address']['ship'].get('zip', '')
                order_obj['address']['ship']['country'] = order['address']['ship'].get('country', '')
                order_obj['address']['ship']['phone'] = order['address']['ship'].get('phone', '')
                order_obj['bulk_transfer'] = order.get('bulk_transfer', False)

                order_obj['shipping'] = 0.0
                order_obj['tax'] = 0.0
                order_obj['discount_total'] = 0.0

                order_obj['ice_total_due'] = order.get('total_due', 0)
                order_obj['ice_tax_percentage'] = order.get('tax_percentage', 0)

                for i in ('shipping_fee', 'shipping_tax'):
                    order_obj['shipping'] += float(order.get(i, 0.0))
                    order_obj[i] = order.get(i, 0)

                order_obj['lines'] = []
                if(not isinstance(order['lines'], list)):
                    order['lines'] = [order['lines']]
                for line_item in order['lines']:
                    line_item_obj = {}
                    line_item_obj['id'] = line_item.get('id', False)
                    line_item_obj['vendorSku'] = line_item.get('vendor_sku', '')
                    line_item_obj['originvendorSku'] = line_item.get('vendor_sku', False)
                    line_item_obj['merchantSKU'] = line_item.get('merchant_sku', False)
                    line_item_obj['size'] = line_item.get('size', False)
                    line_item_obj['qty'] = line_item.get('qty', False)
                    line_item_obj['name'] = line_item.get('name', False)
                    line_item_obj['customerCost'] = line_item.get('cost', False)
                    line_item_obj['cost'] = line_item.get('cost', False)
                    line_item_obj['shipCost'] = 0
                    line_item_obj['gift_message'] = line_item.get('gift_message', '')

                    line_item_obj['ice_sub_total'] = line_item.get('sub_total', 0.0)
                    line_item_obj['ice_row_total'] = line_item.get('row_total', 0.0)
                    line_item_obj['ice_tax_amount'] = line_item.get('tax_amount', 0.0)
                    line_item_obj['ice_discount'] = line_item.get('discount', 0.0)

                    order_obj['tax'] += float(line_item.get('tax_amount', 0.0))
                    order_obj['discount_total'] += float(line_item.get('discount', 0.0))

                    if('/' in line_item_obj['vendorSku']):
                        try:
                            sku = line_item_obj['vendorSku']
                            if float(sku[sku.find("/") + 1:]) == float(line_item_obj['size']):
                                line_item_obj['vendorSku'] = sku[:sku.find("/")]
                        except Exception:
                            pass
                    line_item_obj['sku'] = line_item_obj['vendorSku']

                    order_obj['lines'].append(line_item_obj)

                order_obj['tax'] = str(order_obj['tax'])
                order_obj['shipping'] = str(order_obj['shipping'])
        except Exception:
            _title_msg = 'error for order: %s' % order.get('order_id', False)
            _msg = traceback.format_exc()
            self._log.append({
                'title': "Deserialize order error: %s" % self.customer,
                'msg': '\n'.join([_title_msg, _msg]),
                'type': 'deserialize error',
                'create_date': "{:'%Y-%m-%d %H:%M:%S}".format(datetime.now())
            })
        return order_obj


class IceAUApiUpdateQTY(IceAUApi):

    def __init__(self, settings, lines):
        super(IceAUApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "{:ICEAUINVENTORY%Y%m%d%H%M%f.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if(line.get('sku', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}


class IceAUApiConfirmOrders(IceAUApi):

    def __init__(self, settings, lines):
        super(IceAUApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:ICEAUSHIPPED%Y%m%d%H%M%f.csv}".format(datetime.now())

    def upload_data(self):

        lines = self.lines
        for line in lines:
            date_now = datetime.now()
            if line.get('shp_date', False):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                line['date'] = date_now.strftime("%d/%m/%Y")
                line['product_qty'] = int(line['product_qty'])
            else:
                line['date'] = ''
                line['product_qty'] = ''
        return {'lines': lines}


class IceAUOpenerp(ApiOpenerp):

    def __init__(self):

        super(IceAUOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'customerCost': line['cost'] or False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': False,
            'size': line['size'],
        }

        if('/' in line['sku']):
            try:
                _sku = line['sku']
                _size = float(_sku[_sku.find("/") + 1:])
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except Exception:
                pass

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        field_list = ['sku', 'merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])

        if product:
            line_obj["product_id"] = product.id
            # # DELMAR-186
            # if line_obj["name"] == None or line_obj["name"] == "empty product description in order":
            #     line_obj["name"] = product.name
            # # END DELMAR-186
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj
