# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree


class CommercehubApiGetOrderObjPPower(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjPPower, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['merchandiseCost'] = order.findtext('merchandise')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            if not ordersObj.get('billTo', False):
                ordersObj['billTo'] = order.findall('invoiceTo') and order.find('invoiceTo').get('personPlaceID') or ''
            if ordersObj['poHdrData']:
                if not ordersObj['poHdrData']['packListData']:
                    ordersObj['poHdrData']['packListData'] = {}
                ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''
                ordersObj['poHdrData']['poTypeCode'] = order.findall('poHdrData') and order.find('poHdrData').findtext('poTypeCode') or ''
        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjPPower, self).getLineObj(lineItem)
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        return lineItemObj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
