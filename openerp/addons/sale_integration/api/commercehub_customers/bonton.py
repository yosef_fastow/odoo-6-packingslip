# -*- coding: utf-8 -*-
from main_parser import (
    CommercehubApiGetOrderObjMain,
    CommercehubApiOpenerp
)


class CommercehubApiGetOrderObjBonton(CommercehubApiGetOrderObjMain):

    def __init__(self):
        self.order_fields.extend([
            'multiSource',
            'paymentMethod',
            'salesDivision',
        ])
        self.hdr_fields.extend([
            'merchBatchNum',
            'merchDept',
            'reservationNum',
            'giftIndicator',
            'giftMessage',
            'vendorMessage',
            'sigRequiredFlag',
        ])
        self.packlist_fields.extend([
            'rmPhone2',
            'packslipMessage',
        ])
        self.line_fields.extend([
            'merchantLineId',
            'merchantProductId',
            'unitPrice',
            'lineTax',
            'vendorWarehouseId',
            'expectedShipDate',
        ])
        self.line_data_fields.extend([
            'prodColor',
            'prodSize',
            'encodedPrice',
            'giftWrapIndicator',
            'giftRegistry',
            'factoryOrderNumber',
        ])
        self.addr_fields.extend([
            'address3',
        ])
        self.addr_data_fields.extend([
            'ReceiptID',
        ])


class CommercehubApiBontonOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiBontonOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}

        additional_fields = []

        order_keys = [
            'multiSource',
            'paymentMethod',
            'salesDivision',
            'custOrderDate',
        ]
        self._fill_additional_fields(additional_fields, order_keys, order)

        poHdrData = order.get('poHdrData')
        if poHdrData:
            hdr_keys = [
                'merchBatchNum',
                'merchDept',
                'reservationNum',
                'giftIndicator',
                'giftMessage',
                'vendorMessage',
                'sigRequiredFlag',
            ]
            self._fill_additional_fields(additional_fields, hdr_keys, poHdrData)

        packListData = poHdrData and poHdrData.get('packListData')
        if packListData:
            keys = [
                'rmEmail',
                'rmPhone1',
                'rmPhone2',
                'packslipMessage',
            ]
            self._fill_additional_fields(additional_fields, keys, packListData)

        ship_addr = order.get('address').get('ship')
        ship_receipt = ship_addr.get('personPlaceData', {}).get('ReceiptID')
        if ship_receipt:
            additional_fields.append((0, 0, {
                'name': 'ship_receipt_id',
                'label': 'Ship ReceiptID',
                'value': ship_receipt
            }))

        order_addr = order.get('address').get('order')
        order_receipt = order_addr.get('personPlaceData', {}).get('ReceiptID')
        if order_receipt:
            additional_fields.append((0, 0, {
                'name': 'order_receipt_id',
                'label': 'Order ReceiptID',
                'value': order_receipt
            }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            'external_customer_line_id': line['lineItemId'] or line['merchantSKU'],
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line.get('vendorSku', ""),
            'size': line.get('size', ""),
            'tax': line.get('tax', ""),
            'customerCost': "%.2f" % float(line.get('unitPrice', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': str(line.get('merchantLineNumber', "")),
        }

        if not line_obj["tax"] and line.get('lineTax', False):
            line_obj["tax"] = line.get('lineTax')

        product_obj = self.pool.get('product.product')
        product = False
        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        customer_id = context['customer_id']
        for field in field_list:
            if not line.get(field, False):
                continue
            product, size = product_obj.search_product_by_id(
                cr, uid, customer_id, line[field]
            )
            if not product:
                continue
            if size and size.id:
                line_obj["size_id"] = size.id
            size_id = line_obj.get('size_id', False)
            product_cost = product_obj.get_val_by_label(
                cr, uid, product.id, customer_id, 'Customer Price', size_id
            )
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
            break

        if(product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by upc %s.\n" % (line['UPC'])
            line_obj["product_id"] = 0

        poLineData = line.get('poLineData')
        if poLineData:
            keys = [
                'prodColor',
                'prodSize',
                'encodedPrice',
                'giftWrapIndicator',
                'giftRegistry',
                'factoryOrderNumber',
            ]
            self._fill_additional_fields(additional_fields, keys, poLineData)

        line_obj.update({'additional_fields': additional_fields})

        return line_obj
