# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree

SETTINGS_FIELDS = (
    ('partner_person_place_id',               'partner Person Place Id',            '153114348'),
)


class CommercehubApiGetOrderObjSears(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjSears, self).getOrderObj(xml)

        order = etree.XML(xml)

        if ordersObj:
            ordersObj['tax'] = order.findtext('tax')
            ordersObj['credits'] = order.findtext('credits')
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['handling'] = order.findtext('handling')
            ordersObj['salesDivision'] = order.findtext('salesDivision')

            ordersObj['poHdrData']['reqShipDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext('reqShipDate') or ''
            ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext('giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
            ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')

            ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''

            if ordersObj['address']['ship'].get('name2', False):
                ordersObj['address']['ship']['name'] += ' ' + ordersObj['address']['ship'].get('name2', '')
            if ordersObj['address']['order'].get('name2', False) and ordersObj['address']['ship'] != ordersObj['address']['order']:
                ordersObj['address']['order']['name'] += ' ' + ordersObj['address']['order'].get('name2', '')

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjSears, self).getLineObj(lineItem)

        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or False
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineId')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] + '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['custOrderLineNumber'] = lineItem.findtext('custOrderLineNumber')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        if not lineItemObj.get('description', False):
            lineItemObj['description'] = lineItem.findtext('merchantSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}

                if po_line_data.findall('prodSize'):
                    lineItemObj['poLineData']['prodSize'] = po_line_data.findtext('prodSize')
                    lineItemObj['manufacturerSKU'] += '-' + lineItemObj['poLineData']['prodSize']
                if po_line_data.findall('lineReqShipDate'):
                    lineItemObj['poLineData']['lineReqShipDate'] = po_line_data.findtext('lineReqShipDate')
                if po_line_data.findall('custOrderLineNumber'):
                    lineItemObj['poLineData']['custOrderLineNumber'] = po_line_data.findtext('custOrderLineNumber')
                if po_line_data.findall('giftWrapIndicator'):
                    lineItemObj['poLineData']['giftWrapIndicator'] = po_line_data.findtext('giftWrapIndicator')
                if po_line_data.findall('merchDept'):
                    lineItemObj['poLineData']['merchant_department'] = po_line_data.findtext('merchDept')
                if po_line_data.findall('factoryOrderNumber'):
                    lineItemObj['poLineData']['factoryOrderNumber'] = po_line_data.findtext('factoryOrderNumber')
                if po_line_data.findtext('personalizationData'):
                    personalization_data = po_line_data.find('personalizationData')
                    if personalization_data:
                        if not po_line_data.get('personalizationData', False):
                            lineItemObj['poLineData']['personalizationData'] = {}
                        if personalization_data.findall('nameValuePair'):
                            lineItemObj['poLineData']['personalizationData']['nameValuePair'] = personalization_data.findtext('nameValuePair')

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjSears, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['name2'] = personPlace.findtext('name2')
                adresses_list['ship'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')
                    try:
                        _attnLine = personPlace.find('personPlaceData').findtext('attnLine')
                        if not (adresses_list['ship'][0]['personPlaceData']).has_key('attnLine'):
                            adresses_list['ship'][0]['personPlaceData'].update({'attnLine':_attnLine})
                        else:
                            adresses_list['ship'][0]['personPlaceData']['attnLine'] = _attnLine
                    except Exception, e:
                        pass

            if personPlace.get('personPlaceID') == adresses_list['order'][0]['PersonPlaceId']:
                adresses_list['order'][0]['name2'] = personPlace.findtext('name2')
                adresses_list['order'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['order'][0].get('personPlaceData', False):
                        adresses_list['order'][0]['personPlaceData'] = {}
                    adresses_list['order'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

        return adresses_list


class CommercehubApiSearsOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiSearsOpenerp, self).__init__()
        self._generate_sscc_id = True

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        additional_fields.append((0, 0, {
            'name': 'gift_message',
            'label': 'Gift Message',
            'value': order.get('gift_message', ''),
        }))

        for _attr in ['VcdId', 'attnLine']:
            if(
               order.get('address').get('ship') and\
               order.get('address').get('ship').get('personPlaceData') and\
               order.get('address').get('ship').get('personPlaceData').get(_attr, '')
            ):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get('address').get('ship').get('personPlaceData').get(_attr, '')
                }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []


        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "gift_message": line.get('poLineData', {}).get('giftMessage', ""),
        }

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        for name_field in ['merchant_department']:
            if line.get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field,
                    'value': line.get(name_field)
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res


