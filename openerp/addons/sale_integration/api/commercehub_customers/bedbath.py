# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree

class CommercehubApiGetOrderObjBedBath(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjBedBath, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            ordersObj['buyingContract'] = order.findtext('buyingContract')
            if ordersObj['poHdrData']:
                if order.findall('poHdrData'):
                    ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
                    ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')
                ordersObj['poHdrData']['merchDivision'] = order.findtext('merchDivision')
                ordersObj['poHdrData']['customerPaymentType'] = order.findtext('customerPaymentType')
                ordersObj['poHdrData']['custOrderNumber'] = order.findtext('custOrderNumber')
                ordersObj['poHdrData']['custOrderDate'] = order.findtext('custOrderDate')
                if not ordersObj['poHdrData']['packListData']:
                    ordersObj['poHdrData']['packListData'] = {}
                ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjBedBath, self).getLineObj(lineItem)
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')

        if lineItem.findall('poLineData'):
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            lineItemObj['poLineData']['lineNote1'] = lineItem.find('poLineData').findall('lineNote1') and lineItem.find('poLineData').findtext('lineNote1') or ''
            lineItemObj['poLineData']['giftRegistry'] = lineItem.find('poLineData').findall('giftRegistry') and lineItem.find('poLineData').findtext('giftRegistry') or ''

        return lineItemObj

class CommercehubApiBedBathOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiBedBathOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            "manufacturerSKU": line.get('manufacturerSKU', 'walmartca'),
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', "")
        }

        field_list = ['UPC', 'merchantSKU', 'vendorSku','manufacturerSKU']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        if line.get('poLineData').get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
                }))

        if line.get('poLineData').get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
