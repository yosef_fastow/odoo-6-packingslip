# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree


class CommercehubApiGetOrderObjKmart(CommercehubApiGetOrderObjMain):

    def __init__(self):
        self.hdr_fields.extend([
            'reservationNum',
        ])
        self.addr_fields.extend([
            'partnerPersonPlaceId',
        ])
        self.order_fields.extend([
            'ERPCustOrderNumber'
        ])

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjKmart, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod')
            ordersObj['merchandiseCost'] = order.findtext('merchandiseCost')
            ordersObj['tax'] = order.findtext('tax')
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['handling'] = order.findtext('handling')
            ordersObj['sub_total'] = order.findtext('subtotal')
            ordersObj['total'] = order.findtext('total')
            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['reqShipDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext('reqShipDate') or ''

            if ordersObj['address']['ship'].get('name2', False):
                ordersObj['address']['ship']['name'] += ' ' + ordersObj['address']['ship'].get('name2', '')

            if ordersObj['address']['order'].get('name2', False) and ordersObj['address']['ship'] != ordersObj['address']['order']:
                ordersObj['address']['order']['name'] += ' ' + ordersObj['address']['order'].get('name2', '')

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjKmart, self).getLineObj(lineItem)
        lineItemObj['description2'] = lineItem.findtext('description2')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')
        lineItemObj['customerOrderLineNumber'] = lineItem.findtext('customerOrderLineNumber')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['shopping_cart_sku'] = lineItem.findtext('shoppingCartSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}
                if po_line_data.findall('customerOrderLineNumber'):
                    lineItemObj['poLineData']['giftWrapIindicator'] = po_line_data.findtext('giftWrapIindicator')
                    lineItemObj['poLineData']['giftMessage'] = po_line_data.findtext('giftMessage')

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjKmart, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

            if personPlace.get('personPlaceID') == adresses_list['order'][0]['PersonPlaceId']:
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['order'][0].get('personPlaceData', False):
                        adresses_list['order'][0]['personPlaceData'] = {}
                    adresses_list['order'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

        return adresses_list


class CommercehubApiKmartOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiKmartOpenerp, self).__init__()
        self._generate_sscc_id = True

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}
        additional_fields = []
        poHdrData = order.get('poHdrData')
        billTo = order.get('address', {}).get('order')

        if order.get('ERPCustOrderNumber'):
            additional_fields.append((0, 0, {
                'name': 'ERPCustOrderNumber',
                'label': 'ERPCustOrderNumber',
                'value': order['ERPCustOrderNumber']
            }))

        if billTo and billTo.get('partnerPersonPlaceId'):
            additional_fields.append((0, 0, {
                'name': 'partnerPersonPlaceId',
                'label': 'partnerPersonPlaceId',
                'value': billTo['partnerPersonPlaceId']
            }))

        if poHdrData and poHdrData.get('reservationNum'):
            additional_fields.append((0, 0, {
                'name': 'reservationNum',
                'label': 'reservationNum',
                'value': poHdrData['reservationNum'],
            }))

        if poHdrData and poHdrData.get('reservationNum'):
            for _attr in ['packslipMessage']:
                if(poHdrData.get('packListData').get(_attr, '')):
                    additional_fields.append((0, 0, {
                        'name': _attr,
                        'label': _attr,
                        'value': poHdrData.get('packListData').get(_attr, '')
                    }))

        for _attr in ['VcdId', 'attnLine']:
            if(
               order.get('address').get('ship') and\
               order.get('address').get('ship').get('personPlaceData') and\
               order.get('address').get('ship').get('personPlaceData').get(_attr, '')
            ):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get('address').get('ship').get('personPlaceData').get(_attr, '')
                }))

        order_obj = {
            'additional_fields': additional_fields
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "gift_message": line.get('poLineData', {}).get('giftMessage', ""),
            "long_description": line.get('description2', ""),
            "expected_ship_date": line.get('expectedShipDate', ""),
        }

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        for name_field in ['shopping_cart_sku']:
            if line.get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field,
                    'value': line.get(name_field)
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res
