# -*- coding: utf-8 -*-
from main_parser import (
    CommercehubApiGetOrderObjMain,
    CommercehubApiOpenerp
)
from lxml import etree
from datetime import datetime

order_keys = [
    'paymentMethod',
    'salesDivision',
    'custOrderDate',
]
hdr_keys = [
    'giftIndicator',
    'giftMessage',
]
line_fields = [
    'unitPrice',
    'unitCostAdjustmentAllowed',
]
line_data_keys = ['lineNote1']


class CommercehubApiUpdateQTYFileSignetJewelers(object):

    def prepare_xls_data(self, updateQtyApi):
        updateLines = []
        for line in updateQtyApi.confirmLines:
            if line.get('customer_id_delmar', False) in [None, 'None', False] or line.get('customer_sku', False) in [
                None, 'None', False]:
                updateQtyApi.append_to_revision_lines(line, 'bad')
                continue
            updateQtyApi.append_to_revision_lines(line, 'good')
            line['qty'] = int(line.get('qty', 0)) if str(line.get('qty')).strip() else 0
            line['eta_qty'] = 1 if str(line.get('eta_date')).strip() else ''
            line['eta_date'] = datetime.strftime(datetime.strptime(line.get('eta_date', None), "%Y%m%d"),
                                                 "%m/%d/%Y") if str(line.get('eta_date')).strip() else ''
            updateLines.append(line)
        return updateLines


class CommercehubApiGetOrderObjSignetJewelers(CommercehubApiGetOrderObjMain):

    def __init__(self):
        super(
            CommercehubApiGetOrderObjSignetJewelers, self
        ).__init__()
        self.order_fields.extend(order_keys)
        self.hdr_fields.extend(hdr_keys)
        self.line_fields.extend(line_fields)
        self.line_data_fields.extend(line_data_keys)

    def getAddresses(self, order):
        adresses_list = super(
            CommercehubApiGetOrderObjSignetJewelers, self).getAddresses(order)
        shipFrom = (
                order.findall('shipFrom') and
                order.find('shipFrom').get('vendorShipID') or ''
        )
        addr_fields = self.addr_fields
        addr_fields.extend(['vendorShipID'])
        for vendorShipID in order.findall('vendorShipInfo'):
            vendorShipIDObj = {
                field: vendorShipID.findtext(field) for field in addr_fields
            }
            vendorShipIDObj['vendorShipID'] = vendorShipID.get('vendorShipID')
            if vendorShipIDObj['country']:
                vendorShipIDObj['country'] = vendorShipIDObj['country'][:2]
            if shipFrom == vendorShipIDObj['vendorShipID']:
                adresses_list.update({
                    'shipFrom': vendorShipIDObj,
                })

        for personPlace in order.findall('personPlace'):
            if personPlace.findtext('personPlaceData'):
                adresses_list['ship'][0]['personPlaceData']['addrType'] = personPlace.find(
                    'personPlaceData').findtext('addrType')

        return adresses_list

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjSignetJewelers, self).getOrderObj(xml)

        order = etree.XML(xml)

        if ordersObj:
            ordersObj['poHdrData']['reqShipDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext(
                'reqShipDate') or ''
            ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext(
                'giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
            ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
            if ordersObj['poHdrData']['giftMessage'] == None and ordersObj['poHdrData']['giftIndicator']:
                ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftIndicator')
                if len(ordersObj['poHdrData']['giftIndicator']) > 1:
                    ordersObj['poHdrData']['giftIndicator'] = 'y'
            ordersObj['poHdrData']['custOrderNumber'] = order.find('poHdrData').findtext('custOrderNumber') or ''
            ordersObj['poHdrData']['offerCurrency'] = order.find('poHdrData').findtext('offerCurrency') or ''
            if ordersObj['poHdrData']['giftIndicator'] is None:
                gift_message_flag = ordersObj.get('poHdrData', {}).get('giftMessage', '')
                if isinstance(gift_message_flag, str) and gift_message_flag.strip():
                    ordersObj['poHdrData']['giftIndicator'] = "y"

            ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find(
                'poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext(
                'packslipMessage') or ''

            addrType = ordersObj.get('address',{}).get('ship',{}).get('personPlaceData',{}).get('addrType')
            if addrType and addrType == 'S':
                ordersObj['orderType'] = 'SS'

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjSignetJewelers, self).getLineObj(lineItem)
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or 0

        if lineItem.findall('poLineData'):
            personalization_data = lineItem.find('poLineData').findall('personalizationData') or ''
            lineItemObj['additional_fields'] = []
            if lineItem.find('poLineData').findtext('lineNote1') != '':
                lineItemObj['additional_fields'].append((0, 0, {
                    'name': 'lineNote1',
                    'label': 'lineNote1',
                    'value': lineItem.find('poLineData').findtext('lineNote1'),
                }))

            if lineItem.find('poLineData').findtext('giftMessage') != '':
                lineItemObj['additional_fields'].append((0, 0, {
                    'name': 'gift_message',
                    'label': 'giftMessage',
                    'value': lineItem.find('poLineData').findtext('giftMessage'),
                }))

            if personalization_data:
                text_counter = 1
                for nameValuePair in personalization_data[0].findall(
                        'nameValuePair'
                ):
                    if nameValuePair.get('name', '') == 'Ring Size':
                        lineItemObj['size'] = nameValuePair.text
                        lineItemObj['additional_fields'].append((0, 0, {
                            'name': 'size',
                            'label': 'size',
                            'value': nameValuePair.text,
                        }))
                    if nameValuePair.get('name', '') == 'Initials':
                        lineItemObj['additional_fields'].append((0, 0, {
                            'name': 'initials',
                            'label': 'Initials',
                            'value': nameValuePair.text,
                        }))
                    if nameValuePair.get('name', '') == 'Stone':
                        lineItemObj['additional_fields'].append((0, 0, {
                            'name': 'stone',
                            'label': 'Stone',
                            'value': nameValuePair.text,
                        }))
                    if nameValuePair.get('name', '') == 'text':
                        if ('text1' not in lineItemObj['poLineData']):
                            lineItemObj['additional_fields'].append((0, 0, {
                                'name': 'text%d' % text_counter,
                                'label': 'text%d' % text_counter,
                                'value': nameValuePair.text,
                            }))
                            text_counter += 1
                        else:
                            lineItemObj['additional_fields'].append((0, 0, {
                                'name': 'text%d' % text_counter,
                                'label': 'text%d' % text_counter,
                                'value': nameValuePair.text,
                            }))

        return lineItemObj


class CommercehubApiSignetJewelersOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiSignetJewelersOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []
        self._fill_additional_fields(additional_fields, order_keys, order)

        poHdrData = order.get('poHdrData')
        if poHdrData:
            self._fill_additional_fields(
                additional_fields, hdr_keys, poHdrData)

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        notes = ""
        try:
            unit_price = float(line.get('unitCost',False) or 0)
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            'external_customer_line_id': (
                    line['lineItemId'] or line['merchantSKU']
            ),
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line.get('vendorSku', ""),
            'size': line.get('size', ""),
            'tax': line.get('tax', ""),
            'customerCost': "%.2f" % float(line.get('unitPrice', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': str(line.get('merchantLineNumber', "")),
        }

        if not line_obj["tax"] and line.get('lineTax', False):
            line_obj["tax"] = line.get('lineTax')

        product_obj = self.pool.get('product.product')
        product = False
        field_list = ['UPC', 'merchantSKU', 'vendorSku']

        for field in field_list:
            if not line.get(field, False):
                continue

            if line_obj["size"] and not line_obj.get("size_id", False):
                size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line_obj["size"])))])
                if size_ids:
                    line_obj["size_id"] = size_ids[0]

            product, size = product_obj.search_product_by_id(
                cr, uid, context.get('customer_id'), line[field]
            )
            if not product:
                continue
            if size and size.id and not line_obj.get("size_id",False):
                line_obj["size_id"] = size.id
            break

        if (product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by upc {}.\n".format(
                line['UPC'])
            line_obj["product_id"] = 0

        line_obj.update({
            'additional_fields': line.get('additional_fields', {})
        })

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res
