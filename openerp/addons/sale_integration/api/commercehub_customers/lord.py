from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree

SETTINGS_FIELDS = (
    ('partner_person_place_id',               'partner Person Place Id',            ''),
)


class CommercehubApiGetOrderObjLord(CommercehubApiGetOrderObjMain):
    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjLord, self).getOrderObj(xml)

        order = etree.XML(xml)

        if ordersObj:
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext(
            'giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
            ordersObj['giftMessage'] = ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
            ordersObj['paymentMethod'] = order.find('poHdrData').findtext('customerPaymentType')

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjLord, self).getLineObj(lineItem)

        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] +\
                '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        if not lineItemObj.get('description', False):
            lineItemObj['description'] = lineItem.findtext('merchantSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}

                    lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall(
                        'prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
                    if not lineItemObj['poLineData']['prodColor']:
                        lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall(
                            'vendorColorDesc') and lineItem.find('poLineData').findtext('vendorColorDesc') or ''

                    lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall(
                        'prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
                    if not lineItemObj['poLineData']['prodSize']:
                        lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall(
                            'vendorSizeDesc') and lineItem.find('poLineData').findtext('vendorSizeDesc') or ''


                    taxBreakouts = po_line_data.findall('taxBreakout')
                    tax = 0
                    for tb in taxBreakouts:
                        tax += float(tb.text)
                    lineItemObj['tax'] = lineItemObj['poLineData']['tax'] = tax


        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjLord, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['name2'] = personPlace.findtext('name2')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')
                    adresses_list['ship'][0]['personPlaceData']['ReceiptID'] = personPlace.find('personPlaceData').findtext('ReceiptID')
                    adresses_list['order'][0]['personPlaceData']['VcdId'] = adresses_list['ship'][0]['personPlaceData']['VcdId']
                    adresses_list['order'][0]['personPlaceData']['ReceiptID'] = adresses_list['ship'][0]['personPlaceData']['ReceiptID']
                    try:
                        _attnLine = personPlace.find('personPlaceData').findtext('attnLine')
                        if not (adresses_list['ship'][0]['personPlaceData']).has_key('attnLine'):
                            adresses_list['ship'][0]['personPlaceData'].update({'attnLine':_attnLine})
                        else:
                            adresses_list['ship'][0]['personPlaceData']['attnLine'] = _attnLine
                    except Exception, e:
                        pass
                    adresses_list['ship'][0]['personPlaceData']['s_company'] = personPlace.find(
                            'personPlaceData').findtext('companyName')


        return adresses_list


class CommercehubApiLordOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiLordOpenerp, self).__init__()
        self._generate_sscc_id = True

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        giftMessage = order.get('poHdrData', '')
        if giftMessage:
            giftMessage = order.get('poHdrData', '').get('giftMessage')

        additional_fields.append((0, 0, {
            'name': 'gift_message',
            'label': 'Gift Message',
            'value': giftMessage,
        }))


        for _attr in ['VcdId', 'attnLine', 'companyName']:
            if(
               order.get('address').get('ship') and\
               order.get('address').get('ship').get('personPlaceData') and\
               order.get('address').get('ship').get('personPlaceData').get(_attr, '')
            ):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get('address').get('ship').get('personPlaceData').get(_attr, '')
                }))
        ship_addr = order.get('address').get('ship')
        ship_receipt = ship_addr.get('personPlaceData').get('ReceiptID')
        if ship_receipt:
            additional_fields.append((0, 0, {
                'name': 'ship_receipt_id',
                'label': 'Ship ReceiptID',
                'value': ship_receipt
            }))

        order_addr = order.get('address').get('order')
        order_receipt = order_addr.get('personPlaceData').get('ReceiptID')
        if order_receipt:
            additional_fields.append((0, 0, {
                'name': 'order_receipt_id',
                'label': 'Order ReceiptID',
                'value': order_receipt
            }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
            customer_cost = float(line.get('customerCost', 0.0))
        except (ValueError, TypeError), e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0
            customer_cost = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'customerCost': "%.2f" % customer_cost,
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "prodColor": line.get('poLineData', {}).get('prodColor', ""),
            "prodSize": line.get('poLineData', {}).get('prodSize', ""),
            "tax": line.get('poLineData', {}).get('tax', ""),
        }


        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        poLineData = line.get('poLineData', {})

        if poLineData.get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': poLineData.get('prodColor', '')
                }))

        if poLineData.get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': poLineData.get('prodSize', '')
                }))

        line_obj.update({'additional_fields': additional_fields})
        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res