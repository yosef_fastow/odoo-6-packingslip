from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree


class CommercehubApiGetOrderObjMacys(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjMacys, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod') or ''
            ordersObj['salesDivision'] = order.findtext('salesDivision') or ''
            if ordersObj['poHdrData']:
                poHdrData = order.findall('poHdrData')
                if poHdrData:
                    ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage', '')
                    gift_message_flag = ordersObj.get('poHdrData', {}).get('giftMessage', '')
                    if isinstance(gift_message_flag, str) and gift_message_flag.strip():
                        ordersObj['poHdrData']['giftIndicator'] = "y"

                    ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')
                    ordersObj['poHdrData']['attnLine'] = order.findall('poHdrData') and order.find(
                        'poHdrData').findtext('attnLine') or ''
                    if poHdrData[0].findtext('packListData'):
                        ordersObj['poHdrData']['packListData']['rmPhone1'] = poHdrData[0].find('packListData').findtext('rmPhone1')
                        ordersObj['poHdrData']['packListData']['rmEmail'] = poHdrData[0].find(
                            'packListData').findtext('rmEmail')

            if order.find('poHdrData').findall('URL'):
                ordersObj['poHdrData']['URL'] = order.find('poHdrData').findtext('URL') or ''

            ordersObj['partnerLocationId'] = order.find('vendorShipInfo').findtext('partnerLocationId') or ''
            giftRegistryArr=[]
            ordersObj['poHdrData']['giftRegistry'] = ''
            for lineItem in order.findall('lineItem'):
                parametr = lineItem.find('poLineData').findtext(
                    'giftRegistry')
                if parametr:
                    giftRegistryArr.append(parametr)
            if len(giftRegistryArr):
                ordersObj['poHdrData']['giftRegistry'] = giftRegistryArr[0]

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjMacys, self).getLineObj(lineItem)
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or False
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineId')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] + '-' + (
                    lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['custOrderLineNumber'] = lineItem.findtext('custOrderLineNumber')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')
        lineItemObj['poLineData']['giftRegistry'] = lineItem.find('poLineData').findall(
            'giftRegistry') and lineItem.find('poLineData').findtext('giftRegistry') or ''
        return lineItemObj

    def getAddresses(self, order):
        addresses_list = super(CommercehubApiGetOrderObjMacys, self).getAddresses(order)
        if order.findall('personPlace'):
            shipTo = addresses_list['ship'][0]['PersonPlaceId']
            billTo = addresses_list['order'][0]['PersonPlaceId']
            customer = order.findall('customer') and order.find('customer').get('personPlaceID') or ''
            for personPlace in order.findall('personPlace'):
                personPlaceObj = {}
                if personPlace.get('personPlaceID') in (shipTo, billTo):
                    personPlaceObj['postalCodeExt'] = personPlace.findtext('postalCodeExt')
                if personPlace.get('personPlaceID') == shipTo:
                    addresses_list['ship'][0].update(personPlaceObj)
                elif personPlace.get('personPlaceID') == billTo:
                    addresses_list['order'][0].update(personPlaceObj)


        return addresses_list


class CommercehubApiMacysOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiMacysOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []
        poHdrData = order.get('poHdrData')
        billTo = order.get('address', {}).get('order')
        ship_addr = order.get('address').get('ship')
        shipPersonPlaceData = ship_addr.get('personPlaceData', {})
        billPersonPlaceData = billTo.get('personPlaceData', {})
        poHdrData = order.get('poHdrData', '')
        if poHdrData:
            additional_fields.append((0, 0, {
                'name': 'gift_message',
                'label': 'Gift Message',
                'value':  poHdrData.get('giftMessage', ''),
            }))
            additional_fields.append((0, 0, {
                'name': 'gift_registry',
                'label': 'Gift Registry',
                'value': poHdrData.get('giftRegistry', ''),
            }))
            _url = poHdrData.get('URL', '')
            additional_fields.append((0, 0, {
                'name': 'url',
                'label': 'url',
                'value': _url,
            }))

        if shipPersonPlaceData:
            additional_fields.append((0, 0, {
                'name': 'ship_receipt_id',
                'label': 'Ship ReceiptID',
                'value': shipPersonPlaceData.get('ReceiptID','')
            }))
            additional_fields.append((0, 0, {
                'name': 'ship_attnLine',
                'label': 'Ship attnLine',
                'value': shipPersonPlaceData.get('attnLine','')
            }))

        if billPersonPlaceData:
            additional_fields.append((0, 0, {
                'name': 'bill_receipt_id',
                'label': 'Bill ReceiptID',
                'value': billPersonPlaceData.get('ReceiptID','')
            }))
            additional_fields.append((0, 0, {
                'name': 'bill_attnLine',
                'label': 'Bill attnLine',
                'value': billPersonPlaceData.get('attnLine','')
            }))
        packListData = order.get('poHdrData', '').get('packListData')
        if packListData:
            additional_fields.append((0, 0, {
                'name': 'rmPhone1',
                'label': 'rmPhone1',
                'value': packListData.get('rmPhone1',''),
            }))
            additional_fields.append((0, 0, {
                'name': 'rmEmail',
                'label': 'rmEmail',
                'value': packListData.get('rmEmail',''),
            }))
        additional_fields.append((0, 0, {
            'name': 'return_code',
            'label': 'Return Code',
            'value': order.get('partnerLocationId',''),
        }))
        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        line_obj = {
            "notes": notes,
            "name": line['name'],
            "description": line['description'],
            'cost': line['cost'],
            'id': line['line_id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSKU', False) or "",
            "manufacturerSKU": line.get('manufacturerSKU', ''),
            'size': line.get('size', False) or "",
            'qty': line.get('qty', False) or "",
            'unitOfMeasure': line.get('unitOfMeasure', False) or "",
            'price_unit': "%.2f" % float(line.get('unitCost', 0.0)),
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            'external_customer_line_id': line.get('lineItemId') or line.get('merchantSKU'),
            'gift_registry': line.get('lineItemId') or line.get('giftRegistry'),
            'customerCost': line.get('customerCost', 0.0)
        }

        # customer_sku
        field_list = ['UPC', 'merchantSKU', 'vendorSku', 'manufacturerSKU']

        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res
