from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree

SETTINGS_FIELDS = (
    ('partner_person_place_id',               'partner Person Place Id',            ''),
)


class CommercehubApiGetOrderObjSterling(CommercehubApiGetOrderObjMain):
    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjSterling, self).getOrderObj(xml)

        order = etree.XML(xml)

        if ordersObj:
            ordersObj['tax'] = order.findtext('tax')
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            ordersObj['poHdrData']['giftIndicator'] = order.find('poHdrData').findtext('giftIndicator')
            #order.get('poHdrData','{}').get('giftIndicator','')
            ordersObj['giftMessage'] = ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
            #order.get('poHdrData','{}').get('giftMessage','')
            ordersObj['paymentMethod'] = order.find('poHdrData').findtext('customerPaymentType')
            ordersObj['custOrderDate'] = ordersObj['poHdrData']['custOrderDate'] = order.findtext(
            'custOrderDate')

            addrType = ordersObj.get('address',{}).get('ship',{}).get('personPlaceData',{}).get('addrType')
            if addrType:
                if addrType == 'S':
                    ordersObj['orderType'] = 'SS'
                elif addrType == 'C':
                    ordersObj['orderType'] = 'DC'

            if order.findtext('buyingContract'):
                ordersObj["Comments"]= []
                ordersObj["Comments"].append({"CommentType": 'LA', "Text": order.findtext('buyingContract')})
                ordersObj["Comments"].append({"CommentType": 'TN', "Text": order.findtext('buyingContract')})
            packslipMessage = ordersObj.get('poHdrData', {}).get('packListData', {}).get('packslipMessage')
            if packslipMessage and len(ordersObj['lines']) > 0:
                ordersObj['lines'][0]["Comments"]= []
                ordersObj['lines'][0]["Comments"].append({"CommentType": 'DB', "Text": packslipMessage})


        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjSterling, self).getLineObj(lineItem)

        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] +\
                '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        if not lineItemObj.get('description', False):
            lineItemObj['description'] = lineItem.findtext('merchantSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}

                    lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall(
                        'prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
                    if not lineItemObj['poLineData']['prodColor']:
                        lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall(
                            'vendorCoSterlingesc') and lineItem.find('poLineData').findtext('vendorCoSterlingesc') or ''

                    lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall(
                        'prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
                    if not lineItemObj['poLineData']['prodSize']:
                        lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall(
                            'vendorSizeDesc') and lineItem.find('poLineData').findtext('vendorSizeDesc') or ''


                    taxBreakouts = po_line_data.findall('taxBreakout')
                    tax = 0
                    for tb in taxBreakouts:
                        tax += float(tb.text)
                    lineItemObj['tax'] = lineItemObj['poLineData']['tax'] = tax

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjSterling, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['name2'] = personPlace.findtext('name2')

                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    if not adresses_list['order'][0].get('personPlaceData', False):
                        adresses_list['order'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId') or ''
                    adresses_list['ship'][0]['personPlaceData']['ReceiptID'] = personPlace.find('personPlaceData').findtext('ReceiptID') or ''
                    adresses_list['order'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId') or ''
                    adresses_list['order'][0]['personPlaceData']['ReceiptID'] = personPlace.find('personPlaceData').findtext('ReceiptID') or ''
                    try:
                        _attnLine = personPlace.find('personPlaceData').findtext('attnLine')
                        if not (adresses_list['ship'][0]['personPlaceData']).has_key('attnLine'):
                            adresses_list['ship'][0]['personPlaceData'].update({'attnLine':_attnLine})
                        else:
                            adresses_list['ship'][0]['personPlaceData']['attnLine'] = _attnLine
                    except Exception, e:
                        pass
                    adresses_list['ship'][0]['personPlaceData']['s_company'] = personPlace.find(
                            'personPlaceData').findtext('companyName')

                    adresses_list['ship'][0]['personPlaceData']['partnerPersonPlaceId'] = personPlace.findtext(
                            'partnerPersonPlaceId')
                    adresses_list['ship'][0]['personPlaceData']['addrType'] = personPlace.find(
                            'personPlaceData').findtext('addrType')

        return adresses_list


class CommercehubApiSterlingOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiSterlingOpenerp, self).__init__()
        self._generate_sscc_id = True

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        giftMessage = order.get('poHdrData', '')
        if giftMessage:
            giftMessage = order.get('poHdrData', '').get('giftMessage')

        packslipMessage = order.get('poHdrData', {}).get('packListData',{}).get('packslipMessage')
        if packslipMessage:
            additional_fields.append((0, 0, {
                'name': 'packslipMessage',
                'label': 'packslipMessage',
                'value': packslipMessage,
            }))

        if order.get('buyingContract', False):
            additional_fields.append((0, 0, {
                'name': 'buyingContract',
                'label': 'buyingContract',
                'value': order.get('buyingContract', False),
            }))


        additional_fields.append((0, 0, {
            'name': 'gift_message',
            'label': 'Gift Message',
            'value': giftMessage,
        }))

        additional_fields.append((0, 0, {
            'name': 'salesDivision',
            'label': 'salesDivision',
            'value': order.get('salesDivision', ''),
        }))
        additional_fields.append((0, 0, {
            'name': 'custOrderDate',
            'label': 'custOrderDate',
            'value': order.get('custOrderDate', ''),
        }))

        for _attr in ['VcdId', 'attnLine', 'companyName']:
            if(
               order.get('address').get('ship') and\
               order.get('address').get('ship').get('personPlaceData') and\
               order.get('address').get('ship').get('personPlaceData').get(_attr, '')
            ):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get('address').get('ship').get('personPlaceData').get(_attr, '')
                }))

        ship_addr = order.get('address').get('ship')
        ship_receipt = ship_addr.get('personPlaceData', {}).get('ReceiptID')

        if ship_receipt:
            additional_fields.append((0, 0, {
                'name': 'ship_receipt_id',
                'label': 'Ship ReceiptID',
                'value': ship_receipt
            }))

        partnerPersonPlaceId = ship_addr.get('personPlaceData', {}).get('partnerPersonPlaceId')
        if partnerPersonPlaceId:
            additional_fields.append((0, 0, {
                'name': 'partnerPersonPlaceId',
                'label': 'partnerPersonPlaceId',
                'value': partnerPersonPlaceId
            }))

        order_addr = order.get('address').get('order')
        order_receipt = order_addr.get('personPlaceData').get('ReceiptID')
        if order_receipt:
            additional_fields.append((0, 0, {
                'name': 'order_receipt_id',
                'label': 'Order ReceiptID',
                'value': order_receipt
            }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
            customer_cost = float(line.get('customerCost', 0.0))
        except (ValueError, TypeError), e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0
            customer_cost = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['line_id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'customerCost': "%.2f" % customer_cost,
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "prodColor": line.get('poLineData', {}).get('prodColor', ""),
            "prodSize": line.get('poLineData', {}).get('prodSize', ""),
            "tax": line.get('poLineData', {}).get('tax', ""),
            'external_customer_line_id': line['line_id'],
            'Comments': {}
        }

        if line.get('Comments', False):
            line_obj['Comments'].update({'Text': line['Comments'][0]['Text'],
            'CommentType': line['Comments'][0]['CommentType']})


        field_list = ['UPC', 'merchantSKU', 'vendorSku', 'customer_price']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        poLineData = line.get('poLineData', {})

        if poLineData.get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': poLineData.get('prodColor', '')
                }))

        if poLineData.get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': poLineData.get('prodSize', '')
                }))

        line_obj.update({'additional_fields': additional_fields})
        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res