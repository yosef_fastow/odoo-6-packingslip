# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree


class CommercehubApiGetOrderObjJcpenney(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjJcpenney, self).getOrderObj(xml)
        order = etree.XML(xml)
        ordersObj['poHdrData']['merchDept'] = order.findall('poHdrData') and order.find('poHdrData').findtext('merchDept') or ''
        ordersObj['poHdrData']['poTypeCode'] = order.findall('poHdrData') and order.find('poHdrData').findtext('poTypeCode') or ''
        ordersObj['poHdrData']['vendorMessage'] = order.findall('poHdrData') and order.find('poHdrData').findtext('vendorMessage') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjJcpenney, self).getLineObj(lineItem)
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')

        if lineItem.findall('poLineData'):
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            personalization_data = lineItem.find('poLineData').findall('personalizationData') or ''
            if (personalization_data):
                lineItemObj['poLineData']['personalizationData'] = {}
                for nameValuePair in personalization_data[0].findall('nameValuePair'):
                    if nameValuePair.get('name', '') == 'height':
                        lineItemObj['poLineData']['personalizationData']['height'] = nameValuePair.text
                    if nameValuePair.get('name', '') == 'heightUnit':
                        lineItemObj['poLineData']['personalizationData']['heightUnit'] = nameValuePair.text
                    if nameValuePair.get('name', '') == 'widthUnit':
                        lineItemObj['poLineData']['personalizationData']['widthUnit'] = nameValuePair.text
                    if nameValuePair.get('name', '') == 'lengthDepth':
                        lineItemObj['poLineData']['personalizationData']['lengthDepth'] = nameValuePair.text
                    if nameValuePair.get('name', '') == 'JCP_PID04':
                        lineItemObj['poLineData']['personalizationData']['JCP_PID04'] = nameValuePair.text
                lineItemObj['poLineData']['lineNote1'] = lineItem.find('poLineData').findall('lineNote1') and lineItem.find('poLineData').findtext('lineNote1')

        return lineItemObj

    # def getAddresses(self, order):
    #     adresses_list = super(CommercehubApiGetOrderObjJcpenney, self).getAddresses(order)
    #     adresses_list['invoiceTo'] = []
    #     adresses_list['customer'] = []
    #     for personPlace in order.findall('personPlace'):
    #         personPlaceObj = {}
    #         personPlaceObj['name1'] = personPlace.findtext('name1')
    #         personPlaceObj['dayPhone'] = personPlace.findtext('dayPhone')
    #         personPlaceObj['PersonPlaceId'] = personPlace.get('personPlaceID')
    #         customer = order.findall('customer') and order.find('customer').get('personPlaceID') or ''
    #         invoiceTo = order.findall('invoiceTo') and order.find('invoiceTo').get('personPlaceID') or ''
    #         if customer == personPlaceObj['PersonPlaceId']:
    #             adresses_list['customer'].append(personPlaceObj)
    #         if invoiceTo == personPlaceObj['PersonPlaceId']:
    #             adresses_list['invoiceTo'].append(personPlaceObj)

    #     return adresses_list


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
