# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree


class CommercehubApiGetOrderObjDrugstore(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjDrugstore, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['reqShipDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext('reqShipDate') or ''

        # for lineItem in ordersObj['lines']:
        #     lineItem['cost'] = lineItem['unitPrice']

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjDrugstore, self).getLineObj(lineItem)
        lineItemObj['customerOrderLineNumber'] = lineItem.findtext('customerOrderLineNumber')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}
                if po_line_data.findall('customerOrderLineNumber'):
                    lineItemObj['poLineData']['customerOrderLineNumber'] = po_line_data.findtext('customerOrderLineNumber')

        return lineItemObj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
