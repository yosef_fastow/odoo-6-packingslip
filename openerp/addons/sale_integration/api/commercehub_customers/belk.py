# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain,CommercehubApiOpenerp
from lxml import etree


class CommercehubApiGetOrderObjBelk(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjBelk, self).getOrderObj(xml)
        order = etree.XML(xml)
        ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext('giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
        for lineItem in ordersObj['lines']:
            if not lineItem.get('id', False):
                lineItem['id'] = lineItem['line_id']
            ordersObj['poHdrData']['giftIndicator'] != 'y'
            # if lineItem.get('poLineData', {}).get('giftMessage', False):
            #     ordersObj['poHdrData']['giftIndicator'] = 'y'

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjBelk, self).getLineObj(lineItem)
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')

        if lineItem.findall('poLineData'):
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}

            lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
            if not lineItemObj['poLineData']['prodColor']:
                lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('vendorColorDesc') and lineItem.find('poLineData').findtext('vendorColorDesc') or ''

            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            if not lineItemObj['poLineData']['prodSize']:
                lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('vendorSizeDesc') and lineItem.find('poLineData').findtext('vendorSizeDesc') or ''

            lineItemObj['poLineData']['giftMessage'] = lineItem.find('poLineData').findall('giftMessage') and lineItem.find('poLineData').findtext('giftMessage') or ''
        return lineItemObj


class CommercehubApiBelkOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiBelkOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):

        if context is None:
            context = {}

        additional_fields = []
        if order.get('address').get('ship') and order.get('address').get('ship').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'ship_postalCodeExt',
                'label': 'Ship PostalCodeExt',
                'value': order.get('address').get('ship').get('postalCodeExt', '')
                }))

        if order.get('address').get('order') and order.get('address').get('order').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'order_postalCodeExt',
                'label': 'Order PostalCodeExt',
                'value': order.get('address').get('order').get('postalCodeExt', '')
                }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost') or 0.0),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "gift_message": line.get('poLineData', {}).get('giftMessage', ""),
        }

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        if line.get('poLineData').get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
                }))

        if line.get('poLineData').get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
