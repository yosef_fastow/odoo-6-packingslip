# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree

SETTINGS_FIELDS = (
    ('partner_person_place_id',               'partner Person Place Id',            '243549128'),
)

class CommercehubApiGetOrderObjFMJ(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjFMJ, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')
                ordersObj['poHdrData']['vendorMessage'] = order.find('poHdrData').findtext('vendorMessage')
                ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext('giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
                if order.find('poHdrData').findall('discountTerms'):
                    ordersObj['poHdrData']['discountTerms'] = {}
                    ordersObj['poHdrData']['discountTerms']['text'] = order.find('poHdrData').findtext('discountTerms')
                    ordersObj['poHdrData']['discountTerms']['discTypeCode'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discTypeCode') or ''
                    ordersObj['poHdrData']['discountTerms']['discDateCode'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discDateCode') or ''
                    ordersObj['poHdrData']['discountTerms']['discPercent'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discPercent') or ''
                    ordersObj['poHdrData']['discountTerms']['discDaysDue'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discDaysDue') or ''
                    ordersObj['poHdrData']['discountTerms']['netDaysDue'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('netDaysDue') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjFMJ, self).getLineObj(lineItem)
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['lineTax'] = lineItem.findtext('lineTax')
        lineItemObj['lineCredits'] = lineItem.findtext('lineCredits')
        lineItemObj['lineBalanceDue'] = lineItem.findtext('lineBalanceDue')

        if lineItem.findall('poLineData'):
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            lineItemObj['poLineData']['giftMessage'] = lineItem.find('poLineData').findall('giftMessage') and lineItem.find('poLineData').findtext('giftMessage') or ''
            lineItemObj['poLineData']['prodDescription3'] = lineItem.find('poLineData').findall('prodDescription3') and lineItem.find('poLineData').findtext('prodDescription3') or ''
            lineItemObj['poLineData']['creditAmount'] = lineItem.find('poLineData').findall('creditAmount') and lineItem.find('poLineData').findtext('creditAmount') or ''
            lineItemObj['poLineData']['lineReqShipDate'] = lineItem.find('poLineData').findall('lineReqShipDate') and lineItem.find('poLineData').findtext('lineReqShipDate') or ''
            lineItemObj['poLineData']['lineReqDelvDate'] = lineItem.find('poLineData').findall('lineReqDelvDate') and lineItem.find('poLineData').findtext('lineReqDelvDate') or ''
            lineItemObj['poLineData']['lineNote2'] = lineItem.find('poLineData').findall('lineNote2') and lineItem.find('poLineData').findtext('lineNote2') or ''

        return lineItemObj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
