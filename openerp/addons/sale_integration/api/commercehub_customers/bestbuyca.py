# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree


class CommercehubApiGetOrderObjBestBuyCa(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjBestBuyCa, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['merchandiseCost'] = order.findtext('merchandise')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            if not ordersObj.get('billTo', False):
                ordersObj['billTo'] = order.findall('invoiceTo') and order.find('invoiceTo').get('personPlaceID') or ''
            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['offerCurrency'] = order.findall('poHdrData') and order.find('poHdrData').findtext('offerCurrency') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjBestBuyCa, self).getLineObj(lineItem)
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['description2'] = lineItem.findtext('description2')

        return lineItemObj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
