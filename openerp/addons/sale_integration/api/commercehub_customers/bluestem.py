# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree


class CommercehubApiGetOrderObjBluestem(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjBluestem, self).getOrderObj(xml)
        order = etree.XML(xml)

        if ordersObj:
            total_customer_cost = order.findtext('total')
            ordersObj['partner_id'] = order.findtext('salesDivision')
            ordersObj['merchandise_amount'] = order.findtext('merchandise')
            ordersObj['merchandiseCost'] = order.findtext('merchandiseCost')
            ordersObj['vendor_warehouse'] = order.findtext('vendorWarehouseId')
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            ordersObj['shipping_and_handling'] = order.findtext('shipping')
            ordersObj['shipping_total'] = total_customer_cost
            ordersObj['shipping_tax'] = order.findtext('tax')
            ordersObj['poHdrData']['packListData']['packslipTemplate'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipTemplate') or ''
            ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')
            ordersObj['poHdrData']['vendorMessage'] = order.find('poHdrData').findtext('vendorMessage')
            ordersObj['latest_ship_date_order'] = order.find('poHdrData').findtext('cancelAfterDate')
            try:
                total_customer_cost = float(total_customer_cost)
            except:
                total_customer_cost = 0.00
            
            if ordersObj['lines'] and total_customer_cost:
                customer_cost = "%.4f" % float(total_customer_cost / len(ordersObj['lines']))
                ordersObj['total_per_item'] = customer_cost
                for line in ordersObj['lines']:
                    line['customerCost'] = customer_cost

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjBluestem, self).getLineObj(lineItem)

        lineItemObj['unitPrice'] = lineItem.findall('lineMerchandiseCost') and lineItem.findtext('lineMerchandiseCost') or ''
        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                lineItemObj['poLineData']['prodColor'] = po_line_data.findall('prodColor') and po_line_data.findtext('prodColor') or ''
                lineItemObj['poLineData']['prodSize'] = po_line_data.findall('prodSize') and po_line_data.findtext('prodSize') or ''

                if po_line_data.findtext('personalizationData'):
                    personalization_data = po_line_data.find('personalizationData')
                    if personalization_data:
                        lineItemObj['poLineData']['personalizationData'] = {}
                        for pair in personalization_data.findall('nameValuePair') or []:
                            lineItemObj['poLineData']['personalizationData'][pair.get('name')] = pair.text

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjBluestem, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['dayPhone'] = personPlace.findtext('nightPhone')

            if personPlace.get('personPlaceID') == adresses_list['order'][0]['PersonPlaceId']:
                adresses_list['order'][0]['dayPhone'] = personPlace.findtext('nightPhone')
        return adresses_list


#
# TODO: CommercehubApiOpenerp inheritance
#
class CommercehubApiBluestemOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiBluestemOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}

        additional_fields = []

        if(order.get('poHdrData') and order.get('poHdrData').get('packListData')):
            for _attr in ['rmEmail', 'rmPhone1', 'packslipMessage']:
                if(order.get('poHdrData').get('packListData').get(_attr, '')):
                    additional_fields.append((0, 0, {
                        'name': _attr,
                        'label': _attr,
                        'value': order.get('poHdrData').get('packListData').get(_attr, '')
                    }))
        additional_fields_keys = [
            'merchandise_amount',
            'merchandiseCost',
            'shipping_and_handling',
            'shipping_total',
            'shipping_tax',
            'vendor_warehouse',
            'total_per_item',
        ]
        for _attr in additional_fields_keys:
            if(order.get(_attr, '')):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get(_attr, '0.0')
                }))
        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            'external_customer_line_id': line['lineItemId'] or line['merchantSKU'],
            "name": line['name'],
            'cost': line['cost'],
            'customerCost': line.get('customerCost', ""),
            'merchantSKU': line['merchantSKU'],
            'long_description': line.get('description2', ""),
            'vendorSku': line.get('vendorSku', ""),
            'size': line.get('size', ""),
            'tax': line.get('tax', ""),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': str(line.get('merchantLineNumber', "")),
        }

        if not line_obj["tax"] and line.get('lineTax', False):
            line_obj["tax"] = line.get('lineTax')

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        poLineData = line.get('poLineData', {})

        if poLineData.get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': poLineData.get('prodColor', '')
                }))

        if poLineData.get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': poLineData.get('prodSize', '')
                }))

        if poLineData.get('personalizationData'):
            for key, value in poLineData.get('personalizationData').items():
                additional_fields.append((0, 0, {
                    'name': key.lower().replace(" ", "_"),
                    'label': key,
                    'value': value
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj
