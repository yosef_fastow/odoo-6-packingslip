# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp

class CommercehubApiGetOrderObjMeijer(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):

        ordersObj = super(CommercehubApiGetOrderObjMeijer, self).getOrderObj(xml)

        # order_id = ordersObj['orderId']
        # po_number = ordersObj['poNumber']
        # ordersObj['order_id'] = po_number
        # ordersObj['poNumber'] = order_id

        return ordersObj


class CommercehubApiMeijerOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiMeijerOpenerp, self).__init__()

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res

