# -*- coding: utf-8 -*-
from lxml import etree
from ..apiopenerp import ApiOpenerp
from json import dumps as json_dumps


class CommercehubApiGetOrderObjMain(object):

    order_fields = [
        'participatingParty',
        'sendersIdForReceiver',
        'orderId',
        'lineCount',
        'poNumber',
        'orderDate',
        'shippingCode',
        'custOrderDate',
        'custOrderNumber',
        'buyingContract'
    ]
    hdr_fields = [
        'custOrderNumber',
        'custOrderDate',
        'promoID',
        'merchandiseTypeCode',
    ]
    packlist_fields = [
        'rmEmail',
        'rmPhone1',
        'packslipMessage'
    ]
    line_fields = [
        'lineItemId',
        'orderLineNumber',
        'merchantLineNumber',
        'qtyOrdered',
        'UPC',
        'description',
        'merchantSKU',
        'vendorSKU',
        'unitCost',
        'shippingCode',
        'description2',
        'manufacturerSKU',
        'unitPrice',
        'unitOfMeasure',
    ]
    line_data_fields = []
    addr_fields = [
        'name1',
        'name2',
        'address1',
        'address2',
        'address3',
        'city',
        'state',
        'country',
        'postalCode',
        'postalCodeExt',
        'email',
        'dayPhone',
        's_company'
    ]
    addr_data_fields = [
        'attnLine',
        'ReceiptID'
    ]

    def getOrderObj(self, xml):

        if(xml == ''):
            return {}
        order = etree.XML(xml)
        ordersObj = {}
        if(order is not None):
            shipTo = order.find('shipTo')
            billTo = order.find('billTo')
            ordersObj = {
                'shipTo': shipTo and shipTo.get('personPlaceID') or '',
                'billTo': billTo and billTo.get('personPlaceID') or '',
            }
            ordersObj.update({
                field: order.findtext(field) for field in self.order_fields
            })
            poHdrData = order.find('poHdrData')
            ordersObj['poHdrData'] = {
                field: poHdrData and poHdrData.findtext(field) or '' for field in self.hdr_fields
            }
            packListData = poHdrData and poHdrData.find('packListData')
            ordersObj['poHdrData']['packListData'] = {
                field: packListData and packListData.findtext(field) or '' for field in self.packlist_fields
            }
            if ordersObj['poHdrData']['merchandiseTypeCode'] in ('D2S', 'X2S'):
                ordersObj['orderType'] = 'SS'
            ordersObj['address'] = self.getAddresses(order)

            if ordersObj.get('orderType', '') == 'SS':
                if not ordersObj.get('Comments'):
                    ordersObj['Comments'] = []
                ordersObj['Comments'].append({
                    'CommentType': 'OL',
                    'Text': ''
                })

            ordersObj['lines'] = []
            for lineItem in order.findall('lineItem'):
                lineItemObj = self.getLineObj(lineItem)
                ordersObj['lines'].append(lineItemObj)

        # mapping
        ordersObj['order_id'] = ordersObj['orderId']
        ordersObj['partner_id'] = ordersObj['participatingParty']
        ordersObj['external_date_order'] = ordersObj['orderDate']
        ordersObj['carrier'] = ordersObj['shippingCode']

        for lineItem in ordersObj['lines']:
            lineItem['qty'] = lineItem['qtyOrdered']
            lineItem['id'] = lineItem['merchantSKU']
            lineItem['name'] = lineItem['description']
            lineItem['cost'] = lineItem.get('unitCost',False)
            lineItem['customerCost'] = lineItem.get('unitPrice', False)
            lineItem['shipMethod'] = lineItem['shippingCode']
            lineItem['carrier'] = lineItem['shippingCode']
            lineItem['vendorSku'] = lineItem['vendorSKU']
            lineItem['line_id'] = lineItem['lineItemId']


        if not ordersObj.get('shippingCode', False):
            for lineItem in ordersObj['lines']:
                if lineItem.get('shippingCode', False) and not ordersObj.get('shippingCode', False):
                    ordersObj['carrier'] = lineItem['shippingCode']

        addr_types = ['ship', 'order']
        for addr_type in addr_types:
            addrs = ordersObj['address'][addr_type]
            if not addrs:
                continue
            addr = addrs[0]
            ordersObj['address'][addr_type] = addr
            addr['name'] = addr['name1']
            if addr.get('name2'):
                addr['name'] += ' ' + addr['name2']
            addr['zip'] = addr['postalCode']
            if addr['dayPhone'] is not None:
                addr['phone'] = str(addr['dayPhone']).strip().replace(' ', '-')
            else:
                addr['phone'] = addr['dayPhone']

        if not ordersObj['address']['order']:
            del ordersObj['address']['order']

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = {field: lineItem.findtext(field) for field in self.line_fields}
        po_line_data = lineItem.find('poLineData')
        if not po_line_data:
            return lineItemObj
        lineItemObj['poLineData'] = {
            field: po_line_data.findtext(field) or '' for field in self.line_data_fields
        }
        if po_line_data.findall('unitShippingWeight'):
            lineItemObj['poLineData']['unitShippingWeight'] = po_line_data.findtext('unitShippingWeight')
            lineItemObj['poLineData']['weightUnit'] = po_line_data.find('unitShippingWeight').get('weightUnit')
        return lineItemObj

    def getAddresses(self, order):
        adresses_list = {'ship': [], 'order': []}
        billTo = order.findall('billTo') and order.find('billTo').get('personPlaceID') or ''
        shipTo = order.findall('shipTo') and order.find('shipTo').get('personPlaceID') or ''
        for personPlace in order.findall('personPlace'):
            personPlaceObj = {
                field: personPlace.findtext(field) for field in self.addr_fields
            }
            personPlaceObj['PersonPlaceId'] = personPlace.get('personPlaceID')
            if personPlaceObj['country']:
                personPlaceObj['country'] = personPlaceObj['country'][:2]
            if billTo == personPlaceObj['PersonPlaceId']:
                adresses_list['order'].append(personPlaceObj)
            # FIXME: maybe elif? / YI
            if shipTo == personPlaceObj['PersonPlaceId']:
                personPlaceObj['attnLine'] = personPlace.get('attnLine')
                adresses_list['ship'].append(personPlaceObj)
            personPlaceData = personPlace.find('personPlaceData')
            if not personPlaceData:
                continue
            personPlaceObj['personPlaceData'] = {
                field: personPlaceData.findtext(field) for field in self.addr_data_fields
            }

        return adresses_list


class CommercehubApiOpenerp(ApiOpenerp):

    def __init__(self):
        self._generate_sscc_id = False
        super(CommercehubApiOpenerp, self).__init__()

    def _fill_additional_fields(self, fields, keys, data):
        for key in keys:
            if data.get(key):
                fields.append((0, 0, {
                    'name': key,
                    'label': key,
                    'value': data[key]
                }))

    def get_additional_confirm_shipment_information(
            self, cr, uid, sale_obj, deliveri_id, lines, context=None
    ):
        line = {
            'serial_reference_number': '',
            'serial_reference_number_id': False,
            'sscc_id': False,
        }
        if self._generate_sscc_id and sale_obj.ship2store:
            serial_reference_number_obj = {'id': False, 'value': False}
            order_additional_fields = lines[0]['order_additional_fields']
            if 'serial_reference_number' in order_additional_fields:
                serial_reference_number_obj = eval(order_additional_fields['serial_reference_number'])
            else:
                raise ValueError('Not Available any serial_reference_number')
            line.update({
                'serial_reference_number': serial_reference_number_obj['value'],
                'serial_reference_number_id': serial_reference_number_obj['id'],
                'sscc_id': serial_reference_number_obj['sscc_id'],
            })
        return line

    def after_correct_confirm_shipment_callback(self, cr, uid, sale_obj, picking_obj, lines):
        if sale_obj.ship2store:
            if 'serial_reference_number_id'in lines[0] and lines[0]['serial_reference_number_id']:
                self.pool.get('unique.fields.for.orders').use_for(
                    cr, uid, lines[0]['serial_reference_number_id'], picking_obj.id)
        return True

    def get_additional_shipping_information_refs(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        res = {
            'Ref1': sale_order.cust_order_number or '',
            'Ref2': sale_order.po_number or ''
        }

        return res

    def fill_line_product_data(self, cr, uid, field_list, line, line_obj, context=None):
        product = False
        for field in field_list:
            if line.get(field, False):
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if product:
                    if size and size.id:
                        line_obj["size_id"] = size.id
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    if not product_cost:
                        product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price')
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])

        if(product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by upc %s.\n" % (line['UPC'])
            line_obj["product_id"] = 0

        return True

    def write_unique_fields_for_orders(self, cr, uid, picking):

        if self._generate_sscc_id and picking and picking.sale_id and picking.sale_id.ship2store:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            serial_reference_number_obj = \
                uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id)
            if not serial_reference_number_obj:
                raise ValueError('Not Available any serial_reference_number')
            serial_reference_number_obj.update({
                'sscc_id': uffo_obj.generate_sscc_id(
                    cr, uid,
                    [serial_reference_number_obj['id']],
                    application_identifier='00',
                    extension_digit='4'
                ).get(serial_reference_number_obj['id'], '')
            })
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if srn_row_ids:
                sale_order_fields_obj.write(
                    cr, uid, srn_row_ids, {
                        'name': 'serial_reference_number',
                        'label': 'serial_reference_number',
                        'value': json_dumps(serial_reference_number_obj)
                    }
                )
            else:
                self.pool.get('sale.order').write(
                    cr, uid, picking.sale_id.id, {
                        'additional_fields': [(0, 0, {
                            'name': 'serial_reference_number',
                            'label': 'serial_reference_number',
                            'value': json_dumps(serial_reference_number_obj)
                        })]
                    }
                )
            uffo_obj.use_for(cr, uid, serial_reference_number_obj['id'], picking.id)
        return True
