from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree

class CommercehubApiGetOrderObjBjs(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjBjs, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod')

        # ordersObj['upc'] = line.findtext('UPCCode')
        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjBjs, self).getLineObj(lineItem)
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or False
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineId')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] + '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['custOrderLineNumber'] = lineItem.findtext('custOrderLineNumber')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        return lineItemObj

class CommercehubApiBjsOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiBjsOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

"""
    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        customerCost = line.get('customerCost', 0.0)


        line_obj = {
            "notes": notes,
            "name": line['name'],
            "description": line['description'],
            'cost': line['cost'],
            'id': line['line_id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSKU', False) or "",
            "manufacturerSKU": line.get('manufacturerSKU', ''),
            'size': line.get('size', False) or "",
            'qty': line.get('qty', False) or "",
            'unitOfMeasure': line.get('unitOfMeasure', False) or "",
            'price_unit': "%.2f" % float(line.get('unitCost', 0.0)),
            'merchantLineNumber': line.get('merchantLineNumber', "")
        }

        # customer_sku
        field_list = ['UPC', 'merchantSKU', 'vendorSku','manufacturerSKU']

        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)


        return line_obj
"""
