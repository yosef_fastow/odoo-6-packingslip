# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain
from lxml import etree


class CommercehubApiGetOrderObjStageStores(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjStageStores, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['shipFrom'] = order.findall('shipFrom') and order.find('shipFrom').get('vendorShipID') or ''
            ordersObj['salesDivision'] = order.findtext('salesDivision')
            ordersObj['ERPCustOrderNumber'] = order.findtext('ERPCustOrderNumber')
            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')
                ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
                if order.find('poHdrData').findall('URL'):
                    ordersObj['poHdrData']['URL'] = {}
                    ordersObj['poHdrData']['URL']['text'] = order.find('poHdrData').findtext('URL')
                    ordersObj['poHdrData']['URL']['type'] = order.find('poHdrData').findall('URL') and order.find('poHdrData').find('URL').get('type') or ''
                if not ordersObj['poHdrData']['packListData']:
                    ordersObj['poHdrData']['packListData'] = {}
                ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjStageStores, self).getLineObj(lineItem)
        lineItemObj['merchantProductId'] = lineItem.findtext('merchantProductId')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')

        if lineItem.findall('poLineData'):
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            lineItemObj['poLineData']['giftMessage'] = lineItem.find('poLineData').findall('giftMessage') and lineItem.find('poLineData').findtext('giftMessage') or ''

        return lineItemObj

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
