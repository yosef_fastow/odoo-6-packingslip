# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree


class CommercehubApiGetOrderObjQVC(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjQVC, self).getOrderObj(xml)
        order = etree.XML(xml)
        if ordersObj:
            ordersObj['paymentMethod'] = order.findtext('paymentMethod')
            ordersObj['merchandiseCost'] = order.findtext('merchandise')
            ordersObj['tax'] = order.findtext('tax')
            ordersObj['credits'] = order.findtext('credits')
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['total'] = order.findtext('total')
            ordersObj['customer'] = order.findall('customer') and order.find('customer').get('personPlaceID') or ''
            ordersObj['shipFrom'] = order.findall('shipFrom') and order.find('shipFrom').get('vendorShipID') or ''
            ordersObj['memberNumber'] = order.findtext('memberNumber')
            ordersObj['salesDivision'] = order.findtext('salesDivision')

            if ordersObj['poHdrData']:
                ordersObj['poHdrData']['merchBatchNum'] = order.find('poHdrData').findtext('merchBatchNum')
                ordersObj['poHdrData']['poTypeCode'] = order.find('poHdrData').findtext('poTypeCode')

                if order.find('poHdrData').findall('discountTerms'):
                    ordersObj['poHdrData']['discountTerms'] = {}
                    ordersObj['poHdrData']['discountTerms']['text'] = order.find('poHdrData').findtext('discountTerms')
                    ordersObj['poHdrData']['discountTerms']['discTypeCode'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discTypeCode') or ''
                    ordersObj['poHdrData']['discountTerms']['discDateCode'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discDateCode') or ''
                    ordersObj['poHdrData']['discountTerms']['discPercent'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discPercent') or ''
                    ordersObj['poHdrData']['discountTerms']['discDaysDue'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('discDaysDue') or ''
                    ordersObj['poHdrData']['discountTerms']['netDaysDue'] = order.find('poHdrData').findall('discountTerms') and order.find('poHdrData').find('discountTerms').get('netDaysDue') or ''

                if not ordersObj['poHdrData']['packListData']:
                    ordersObj['poHdrData']['packListData'] = {}
                ordersObj['poHdrData']['packListData']['rmPhone2'] = order.find('poHdrData').find('packListData').findtext('rmPhone2')
                ordersObj['poHdrData']['packListData']['rmPhone3'] = order.find('poHdrData').find('packListData').findtext('rmPhone3')
                ordersObj['poHdrData']['packListData']['packslipMessage'] = order.find('poHdrData').find('packListData').findtext('packslipMessage')

            for vendorShip in order.findall('vendorShipInfo'):

                vendorShipObj = {}
                # addresses_list['shipFrom'] = []

                vendorShipObj['name'] = vendorShip.findtext('name1')
                vendorShipObj['address1'] = vendorShip.findtext('address1')
                vendorShipObj['address2'] = vendorShip.findtext('address2')
                vendorShipObj['city'] = vendorShip.findtext('city')
                vendorShipObj['state'] = vendorShip.findtext('state')
                vendorShipObj['country'] = vendorShip.findtext('country')
                vendorShipObj['zip'] = vendorShip.findtext('postalCode')
                vendorShipObj['postalCodeExt'] = vendorShip.findtext('postalCodeExt')
                vendorShipObj['email'] = vendorShip.findtext('email')
                vendorShipObj['phone'] = vendorShip.findtext('dayPhone')
                vendorShipObj['vendorShipID'] = vendorShip.get('vendorShipID')

                # if vendorShipObj['vendorShipID'] == shipFrom:
                ordersObj['shipFrom'] = vendorShipObj

            # ordersObj['address']['shipFrom'] =

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjQVC, self).getLineObj(lineItem)
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['description2'] = lineItem.findtext('description2')
        lineItemObj['merchantProductId'] = lineItem.findtext('merchantProductId')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['lineShipping'] = lineItem.findtext('lineShipping')
        lineItemObj['lineTax'] = lineItem.findtext('lineTax')
        lineItemObj['lineSubtotal'] = lineItem.findtext('lineSubtotal')
        lineItemObj['shippingHub'] = lineItem.findtext('shippingHub')
        lineItemObj['lineCredits'] = lineItem.findtext('lineCredits')
        lineItemObj['vendorWarehouseId'] = lineItem.findtext('vendorWarehouseId')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate')
        lineItemObj['requestedArrivalDate'] = lineItem.findtext('requestedArrivalDate')

        if lineItem.findall('shippingLabel'):
            lineItemObj['shippingLabel'] = {}
            lineItemObj['shippingLabel']['ShippingLabelId'] = lineItem.findall('shippingLabel') and lineItem.find('shippingLabel').get('ShippingLabelId') or ''
            lineItemObj['shippingLabel']['vendor'] = lineItem.findall('shippingLabel') and lineItem.find('shippingLabel').get('vendor') or ''
            lineItemObj['shippingLabel']['InsertDate'] = lineItem.findall('shippingLabel') and lineItem.find('shippingLabel').get('InsertDate') or ''
            lineItemObj['shippingLabel']['OrderId'] = lineItem.findall('shippingLabel') and lineItem.find('shippingLabel').get('OrderId') or ''
            lineItemObj['shippingLabel']['LineitemId'] = lineItem.findall('shippingLabel') and lineItem.find('shippingLabel').get('LineitemId') or ''
            lineItemObj['shippingLabel']['trackingNumber'] = lineItem.find('shippingLabel').findall('trackingNumber') and lineItem.find('shippingLabel').findtext('trackingNumber') or ''
            lineItemObj['shippingLabel']['shippingCode'] = lineItem.find('shippingLabel').findall('shippingCode') and lineItem.find('shippingLabel').findtext('shippingCode') or ''
            lineItemObj['shippingLabel']['boxNumber'] = lineItem.find('shippingLabel').findall('boxNumber') and lineItem.find('shippingLabel').findtext('boxNumber') or ''

            if lineItem.find('shippingLabel').findall('grossWeight'):
                lineItemObj['shippingLabel']['grossWeight'] = {}
                lineItemObj['shippingLabel']['grossWeight']['weightUnit'] = lineItem.find('shippingLabel').findall('grossWeight') and lineItem.find('shippingLabel').find('grossWeight').get('weightUnit') or ''
                lineItemObj['shippingLabel']['grossWeight']['text'] = lineItem.find('shippingLabel').findtext('grossWeight')
            if lineItem.find('shippingLabel').findall('heightDimension'):
                lineItemObj['shippingLabel']['heightDimension'] = {}
                lineItemObj['shippingLabel']['heightDimension']['dimensionUnit'] = lineItem.find('shippingLabel').findall('heightDimension') and lineItem.find('shippingLabel').find('heightDimension').get('dimensionUnit') or ''
                lineItemObj['shippingLabel']['heightDimension']['text'] = lineItem.find('shippingLabel').findtext('heightDimension')
            if lineItem.find('shippingLabel').findall('widthDimension'):
                lineItemObj['shippingLabel']['widthDimension'] = {}
                lineItemObj['shippingLabel']['widthDimension']['dimensionUnit'] = lineItem.find('shippingLabel').findall('widthDimension') and lineItem.find('shippingLabel').find('widthDimension').get('dimensionUnit') or ''
                lineItemObj['shippingLabel']['widthDimension']['text'] = lineItem.find('shippingLabel').findtext('heightDimension')
                lineItemObj['shippingLabel']['depthDimension'] = {}
                lineItemObj['shippingLabel']['depthDimension']['dimensionUnit'] = lineItem.find('shippingLabel').findall('depthDimension') and lineItem.find('shippingLabel').find('depthDimension').get('dimensionUnit') or ''
                lineItemObj['shippingLabel']['depthDimension']['text'] = lineItem.find('shippingLabel').findtext('depthDimension')
            if lineItem.find('shippingLabel').findall('returnShipMethod'):
                lineItemObj['shippingLabel']['returnShipMethod'] = {}
                lineItemObj['shippingLabel']['returnShipMethod']['description'] = lineItem.find('shippingLabel').findall('returnShipMethod') and lineItem.find('shippingLabel').find('returnShipMethod').get('description') or ''
                lineItemObj['shippingLabel']['returnShipMethod']['permit-number'] = lineItem.find('shippingLabel').findall('returnShipMethod') and lineItem.find('shippingLabel').find('returnShipMethod').get('permit-number') or ''
                lineItemObj['shippingLabel']['returnShipMethod']['permit-issuing-city'] = lineItem.find('shippingLabel').findall('returnShipMethod') and lineItem.find('shippingLabel').find('returnShipMethod').get('permit-issuing-city') or ''
                lineItemObj['shippingLabel']['returnShipMethod']['permit-issuing-state'] = lineItem.find('shippingLabel').findall('returnShipMethod') and lineItem.find('shippingLabel').find('returnShipMethod').get('permit-issuing-state') or ''

            lineItemObj['shippingLabel']['returnTrackingNumber'] = lineItem.find('shippingLabel').findall('returnTrackingNumber') and lineItem.find('shippingLabel').findtext('returnTrackingNumber') or ''
            lineItemObj['shippingLabel']['returnRoutingNumber'] = lineItem.find('shippingLabel').findall('returnRoutingNumber') and lineItem.find('shippingLabel').findtext('returnRoutingNumber') or ''
            lineItemObj['shippingLabel']['returnServicePostalCode'] = lineItem.find('shippingLabel').findall('returnServicePostalCode') and lineItem.find('shippingLabel').findtext('returnServicePostalCode') or ''
            lineItemObj['shippingLabel']['returnRelatedMessage'] = lineItem.find('shippingLabel').findall('returnRelatedMessage') and lineItem.find('shippingLabel').findtext('returnRelatedMessage') or ''
            lineItemObj['shippingLabel']['returnShipping'] =  lineItem.find('shippingLabel').findall('returnShipping') and lineItem.find('shippingLabel').findtext('returnShipping') or ''
            lineItemObj['shippingLabel']['returnDeliveryConfirmationNumber'] = lineItem.find('shippingLabel').findall('returnDeliveryConfirmationNumber') and lineItem.find('shippingLabel').findtext('returnDeliveryConfirmationNumber') or ''

        if lineItem.findall('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if not lineItemObj.get('poLineData', False):
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['prodColor'] = po_line_data.findall('prodColor') and po_line_data.findtext('prodColor') or ''
            lineItemObj['poLineData']['prodSize'] = po_line_data.findall('prodSize') and po_line_data.findtext('prodSize') or ''
            lineItemObj['poLineData']['prodCSdesc'] = po_line_data.findall('prodCSdesc') and po_line_data.findtext('prodCSdesc') or ''
            lineItemObj['poLineData']['lineDiscAmount'] = po_line_data.findall('lineDiscAmount') and po_line_data.findtext('lineDiscAmount') or ''
            lineItemObj['poLineData']['creditAmount'] = po_line_data.findall('creditAmount') and po_line_data.findtext('creditAmount') or ''
            lineItemObj['poLineData']['lineReqShipDate'] = po_line_data.findall('lineReqShipDate') and po_line_data.findtext('lineReqShipDate') or ''
            lineItemObj['poLineData']['lineReqDelvDate'] = po_line_data.findall('lineReqDelvDate') and po_line_data.findtext('lineReqDelvDate') or ''
            lineItemObj['poLineData']['permitNumber'] = po_line_data.findall('permitNumber') and po_line_data.findtext('permitNumber') or ''
            lineItemObj['poLineData']['permitIssuingCity'] = po_line_data.findall('permitIssuingCity') and po_line_data.findtext('permitIssuingCity') or ''
            lineItemObj['poLineData']['permitIssuingState'] = po_line_data.findall('permitIssuingState') and po_line_data.findtext('permitIssuingState') or ''
            lineItemObj['poLineData']['UPSTrackingNumber1'] = po_line_data.findall('UPSTrackingNumber1') and po_line_data.findtext('UPSTrackingNumber1') or ''
            lineItemObj['poLineData']['boxNumber1'] = po_line_data.findall('boxNumber1') and po_line_data.findtext('boxNumber1') or ''

        return lineItemObj

    def getAddresses(self, order):
        addresses_list = super(CommercehubApiGetOrderObjQVC, self).getAddresses(order)
        if order.findall('personPlace'):

            shipTo = addresses_list['ship'][0]['PersonPlaceId']
            billTo = addresses_list['order'][0]['PersonPlaceId']

            # shipFrom = order.findall('shipFrom') and order.find('shipFrom').get('vendorShipID') or ''
            customer = order.findall('customer') and order.find('customer').get('personPlaceID') or ''

            for personPlace in order.findall('personPlace'):
                personPlaceObj = {}

                if personPlace.get('personPlaceID') in (shipTo, billTo):
                    personPlaceObj['postalCodeExt'] = personPlace.findtext('postalCodeExt')

                if personPlace.get('personPlaceID') == shipTo:
                    addresses_list['ship'][0].update(personPlaceObj)
                elif personPlace.get('personPlaceID') == billTo:
                    addresses_list['order'][0].update(personPlaceObj)


                if personPlace.get('personPlaceID') in customer:
                    # addresses_list['customer'] = []
                    # addresses_list['customer'][0] = {}

                    personPlaceObj['name'] = personPlace.findtext('name1')
                    personPlaceObj['name2'] = personPlace.findtext('name2')
                    personPlaceObj['address1'] = personPlace.findtext('address1')
                    personPlaceObj['address2'] = personPlace.findtext('address2')
                    personPlaceObj['city'] = personPlace.findtext('city')
                    personPlaceObj['state'] = personPlace.findtext('state')
                    personPlaceObj['country'] = personPlace.findtext('country')
                    personPlaceObj['zip'] = personPlace.findtext('postalCode')
                    personPlaceObj['postalCodeExt'] = personPlace.findtext('postalCodeExt')
                    personPlaceObj['email'] = personPlace.findtext('email')
                    personPlaceObj['phone'] = personPlace.findtext('dayPhone')
                    personPlaceObj['PersonPlaceId'] = personPlace.get('personPlaceID')

                    # addresses_list['customer'] = personPlaceObj

            # for vendorShip in order.findall('vendorShipInfo'):

            #     vendorShipObj = {}
            #     # addresses_list['shipFrom'] = []

            #     vendorShipObj['name'] = vendorShip.findtext('name1')
            #     vendorShipObj['address1'] = vendorShip.findtext('address1')
            #     vendorShipObj['address2'] = vendorShip.findtext('address2')
            #     vendorShipObj['city'] = vendorShip.findtext('city')
            #     vendorShipObj['state'] = vendorShip.findtext('state')
            #     vendorShipObj['country'] = vendorShip.findtext('country')
            #     vendorShipObj['zip'] = vendorShip.findtext('postalCode')
            #     vendorShipObj['postalCodeExt'] = vendorShip.findtext('postalCodeExt')
            #     vendorShipObj['email'] = vendorShip.findtext('email')
            #     vendorShipObj['phone'] = vendorShip.findtext('dayPhone')
            #     vendorShipObj['vendorShipID'] = vendorShip.get('vendorShipID')

            #     if vendorShipObj['vendorShipID'] == shipFrom:
            #         addresses_list['shipFrom'] = vendorShipObj

                # addresses_list.update(personPlaceObj)

            # for addresses in ('ship', 'order', 'shipFrom', 'customer'):
            #     if not addresses_list[addresses]:
            #         addresses_list[addresses] = []
            #     addresses_list[addresses][0].update()

        return addresses_list


# NEED TO CHECK
#
# TODO: CommercehubApiOpenerp inheritance
#

class CommercehubApiQVCOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiQVCOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):

        if context.get('customer_id', False):
            customer = self.pool.get('res.partner').browse(cr, uid, context['customer_id'])
        if context is None:
            context = {}

        additional_fields = []
        if order.get('memberNumber'):
            additional_fields.append((0, 0, {
                'name': 'member_number',
                'label': 'Member Number',
                'value': order.get('memberNumber', '')
                }))
        if order.get('lines', []):
        # and order.get('lines')[0].get('shippingLabel') and order.get('lines')[0].get('shippingLabel').get('returnRelatedMessage') or '':
            returnRelatedMessage = ''
            returnShipping = ''
            returnRoutingNumber = ''
            returnTrackingNumber = ''
            returnDeliveryConfirmationNumber = ''
            prodColor = ''
            prodSize = ''
            for line in order.get('lines'):
                if line.get('shippingLabel') and line.get('shippingLabel').get('returnRelatedMessage') and not returnRelatedMessage:
                    returnRelatedMessage = line.get('shippingLabel').get('returnRelatedMessage')
                    additional_fields.append((0, 0, {
                        'name': 'return_related_message',
                        'label': 'Return Related Messaged',
                        'value': returnRelatedMessage
                        }))
                if line.get('shippingLabel') and line.get('shippingLabel').get('returnShipping') and not returnShipping:
                    returnShipping = line.get('shippingLabel').get('returnShipping')
                    additional_fields.append((0, 0, {
                        'name': 'return_shipping',
                        'label': 'Return Shipping',
                        'value': returnShipping
                        }))
                if line.get('shippingLabel') and line.get('shippingLabel').get('returnRoutingNumber') and not returnRoutingNumber:
                    returnRoutingNumber = line.get('shippingLabel').get('returnRoutingNumber')
                    additional_fields.append((0, 0, {
                        'name': 'return_routing_number',
                        'label': 'Return Routing Number',
                        'value': returnRoutingNumber
                        }))
                if line.get('shippingLabel') and line.get('shippingLabel').get('returnTrackingNumber') and not returnTrackingNumber:
                    returnTrackingNumber = line.get('shippingLabel').get('returnTrackingNumber')
                    additional_fields.append((0, 0, {
                        'name': 'return_tracking_number',
                        'label': 'Return Tracking Number',
                        'value': returnTrackingNumber
                        }))
                if line.get('shippingLabel') and line.get('shippingLabel').get('returnDeliveryConfirmationNumber') and not returnDeliveryConfirmationNumber:
                    returnDeliveryConfirmationNumber = line.get('shippingLabel').get('returnDeliveryConfirmationNumber')
                    additional_fields.append((0, 0, {
                        'name': 'return_delivery_confirmation_number',
                        'label': 'Return Delivery Confirmation Number',
                        'value': returnDeliveryConfirmationNumber
                        }))
                if line.get('poLineData').get('prodColor') and not prodColor:
                    prodColor = line.get('poLineData').get('prodColor')
                    additional_fields.append((0, 0, {
                        'name': 'prod_color',
                        'label': 'Product Color',
                        'value': prodColor
                        }))

                if line.get('poLineData').get('prodSize') and not prodSize:
                    prodSize = line.get('poLineData').get('prodSize')
                    additional_fields.append((0, 0, {
                        'name': 'prod_size',
                        'label': 'Product Size',
                        'value': prodSize
                        }))
                    break
        if order.get('address').get('ship') and order.get('address').get('ship').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'ship_postalCodeExt',
                'label': 'Ship PostalCodeExt',
                'value': order.get('address').get('ship').get('postalCodeExt', '')
                }))

        if order.get('address').get('order') and order.get('address').get('order').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'order_postalCodeExt',
                'label': 'Order PostalCodeExt',
                'value': order.get('address').get('order').get('postalCodeExt', '')
                }))

        if order.get('address').get('ship', '') and order.get('address').get('ship', '').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'ship_postal_code_ext',
                'label': 'Ship PostalCodeExt',
                'value': order.get('address').get('ship').get('postalCodeExt', '')
                }))

        if order.get('address').get('order', '') and order.get('address').get('order', '').get('postalCodeExt', ''):
            additional_fields.append((0, 0, {
                'name': 'order_postal_code_ext',
                'label': 'Order PostalCodeExt',
                'value': order.get('address').get('order').get('postalCodeExt', '')
                }))

        if order.get('address').get('order', '') and order.get('address').get('order', '').get('name2', ''):
            additional_fields.append((0, 0, {
                'name': 'order_name2',
                'label': 'Order Name2',
                'value': order.get('address').get('order').get('name2', '')
                }))

        if order.get('shipFrom', ''):

            additional_fields.append((0, 0, {
                'name': 'shipFrom_name',
                'label': 'Ship From Name',
                'value': order.get('shipFrom').get('name', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_address1',
                'label': 'Ship From Address1',
                'value': order.get('shipFrom').get('address1', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_address2',
                'label': 'Ship From Address2',
                'value': order.get('shipFrom').get('address2', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_city',
                'label': 'Ship From City',
                'value': order.get('shipFrom').get('city', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_state',
                'label': 'Ship From State',
                'value': order.get('shipFrom').get('state', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_country',
                'label': 'Ship From Country',
                'value': order.get('shipFrom').get('country', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_zip',
                'label': 'Ship From Zip',
                'value': order.get('shipFrom').get('zip', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_postalCodeExt',
                'label': 'Ship From PostalCodeExt',
                'value': order.get('shipFrom').get('postalCodeExt', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_email',
                'label': 'Ship From Email',
                'value': order.get('shipFrom').get('email', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_phone',
                'label': 'Ship From Phone',
                'value': order.get('shipFrom').get('phone', '')
                }))

            additional_fields.append((0, 0, {
                'name': 'shipFrom_vendorShipID',
                'label': 'Ship From vendorShipID',
                'value': order.get('shipFrom').get('vendorShipID', '')
                }))

            # order_fields.append({
            #     'name': 'return_related_message',
            #     'label': 'Return Related Messaged',
            #     'value': order.get('returnRelatedMessage', 0)
            #     })

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        # if context.get('customer_id', False):
        #     customer = self.pool.get('res.partner').browse(cr, uid, context['customer_id'])
        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['id'],
            'credit_amount': line.get('poLineData', {}).get('creditAmount', ""),
            'expected_ship_date': line.get('expectedShipDate', ""),
            'long_description': line.get('description2', ""),
            'vendorSku': line.get('vendorSku', ""),
            'size': line.get('size', ""),
            'tax': line.get('tax', ""),
            'customerCost': line.get('customerCost', 0.0) and "%.2f" % (float(line['customerCost'] or 0.0)) or False,
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', "")
        }

        if not line_obj["tax"] and line.get('lineTax', False):
            line_obj["tax"] = line.get('lineTax')

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        if line.get('poLineData').get('prodCSdesc'):
            additional_fields.append((0, 0, {
                'name': 'prod_cs_desc',
                'label': 'Product CS Description',
                'value': line.get('poLineData').get('prodCSdesc', '')
                }))

        if line.get('poLineData').get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
                }))

        if line.get('poLineData').get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
                }))

        if line.get('lineShipping'):
            additional_fields.append((0, 0, {
                'name': 'line_shipping',
                'label': 'Line Shipping',
                'value': line.get('lineShipping', '')
                }))

        if line.get('lineSubtotal'):
            additional_fields.append((0, 0, {
                'name': 'line_subtotal',
                'label': 'Line Subtotal',
                'value': line.get('lineSubtotal', '')
                }))

        if line.get('lineCredits'):
            additional_fields.append((0, 0, {
                'name': 'line_credits',
                'label': 'Line Credits',
                'value': line.get('lineCredits', '')
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj
