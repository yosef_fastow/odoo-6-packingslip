# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree
from lxml.etree import XML, tostring
from ..apiopenerp import ApiOpenerp

class CommercehubApiGetOrderObjSearsCanada(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        try:
            xml = tostring(XML(xml).find('hubOrder'))
        except TypeError:
            pass
        ordersObj = super(CommercehubApiGetOrderObjSearsCanada, self).getOrderObj(xml)

        order = XML(xml)

        if ordersObj:
            ordersObj['salesDivision'] = order.findtext('salesDivision') or ''
            ordersObj['paymentMethod'] = order.findtext('paymentMethod') or ''
            ordersObj['custOrderDate'] = order.findtext('custOrderDate') or ''
            ordersObj['orderDate'] = order.findtext('orderDate') or ''
            if ordersObj['address']['ship'].get('name2', False):
                ordersObj['address']['ship']['name'] += ' ' + ordersObj['address']['ship'].get('name2', '')
            if ordersObj['address']['order'].get('name2', False) and ordersObj['address']['ship'] != ordersObj['address']['order']:
                ordersObj['address']['order']['name'] += ' ' + ordersObj['address']['order'].get('name2', '')
            # Add ship to / customer address objects
            ordersObj['vendorShipInfo'] = order.find('vendorShipInfo') or ''
            ordersObj['customer_address'] = False
            try:
                customer_handle = order.find('customer')
                personPlaces = order.findall('personPlace')
                for place in personPlaces:
                    if place.get('personPlaceID') == customer_handle.get('personPlaceID'):
                        ordersObj['customer_address'] = place
                        break
            except Exception, e:
                pass
            try:
                ordersObj['poHdrData']['merchDivision'] = order.find('poHdrData').findtext('merchDivision') or ''
            except TypeError:
                ordersObj['poHdrData']['merchDivision'] = ''
            except KeyError:
                ordersObj['poHdrData'] = {}
                ordersObj['poHdrData']['merchDivision'] = ''
            try:
                if not ordersObj['poHdrData'].has_key('packListData'):
                    ordersObj['poHdrData']['packListData'] = {}
                ordersObj['poHdrData']['packListData']['packslipMessage'] = order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''
            except Exception, e:
                pass
            xml_line_items = order.findall('lineItem')
            for line in ordersObj['lines']:
                for raw_line in xml_line_items:
                    if(raw_line.findtext('lineItemId') == line['lineItemId']):
                        line['SKUdescription'] = raw_line.findtext('description') or ''
                        line['SKUdescription2'] = raw_line.findtext('description2') or ''
                        line['lineMerchandise'] = raw_line.findtext('lineMerchandise') or ''
                        line['lineTotal'] = raw_line.findtext('lineTotal') or ''
                        line['lineHandling'] = raw_line.findtext('lineHandling') or ''
                        line['lineShipping'] = raw_line.findtext('lineShipping') or ''
                        line['expectedShipDate'] = raw_line.findtext('expectedShipDate') or ''
                        _po_line_data = raw_line.find('poLineData')
                        line['poLineData'] = {}
                        if(_po_line_data):
                            line['poLineData']['psReceivingInstructions'] = _po_line_data.findtext('psReceivingInstructions') or ''
                            line['poLineData']['encodedPrice'] = _po_line_data.findtext('encodedPrice') or ''
                            line['poLineData']['creditAmount'] = _po_line_data.findtext('creditAmount') or ''
                        else:
                            line['poLineData']['psReceivingInstructions'] = ''
                            line['poLineData']['encodedPrice'] = ''
                            line['poLineData']['creditAmount'] = ''
                        break

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjSearsCanada, self).getLineObj(lineItem)

        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or False
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineId')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] + '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['custOrderLineNumber'] = lineItem.findtext('custOrderLineNumber')

        if not lineItemObj.get('description', False):
            lineItemObj['description'] = lineItem.findtext('merchantSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}
                tax_Breakouts = po_line_data.findall('taxBreakout')
                tbs = []
                for tb in tax_Breakouts:
                    tb_object = {}
                    tb_object['value'] = tb.text
                    tb_object['taxType'] = tb.attrib.get('taxType', '')
                    tbs.append(tb_object)
                lineItemObj['taxBreakouts'] = tbs

                if po_line_data.findall('prodSize'):
                    lineItemObj['poLineData']['prodSize'] = po_line_data.findtext('prodSize')
                    lineItemObj['manufacturerSKU'] += '-' + lineItemObj['poLineData']['prodSize']
                if po_line_data.findall('lineReqShipDate'):
                    lineItemObj['poLineData']['lineReqShipDate'] = po_line_data.findtext('lineReqShipDate')
                if po_line_data.findall('custOrderLineNumber'):
                    lineItemObj['poLineData']['custOrderLineNumber'] = po_line_data.findtext('custOrderLineNumber')
                if po_line_data.findall('giftWrapIndicator'):
                    lineItemObj['poLineData']['giftWrapIndicator'] = po_line_data.findtext('giftWrapIndicator')
                if po_line_data.findall('merchDept'):
                    lineItemObj['poLineData']['merchant_department'] = po_line_data.findtext('merchDept')
                if po_line_data.findall('factoryOrderNumber'):
                    lineItemObj['poLineData']['factoryOrderNumber'] = po_line_data.findtext('factoryOrderNumber')
                if po_line_data.findtext('personalizationData'):
                    personalization_data = po_line_data.find('personalizationData')
                    if personalization_data:
                        if not po_line_data.get('personalizationData', False):
                            lineItemObj['poLineData']['personalizationData'] = {}
                        if personalization_data.findall('nameValuePair'):
                            lineItemObj['poLineData']['personalizationData']['nameValuePair'] = personalization_data.findtext('nameValuePair')

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjSearsCanada, self).getAddresses(order)

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['name2'] = personPlace.findtext('name2')
                adresses_list['ship'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')
                    try:
                        _attnLine = personPlace.find('personPlaceData').findtext('attnLine')
                        if not (adresses_list['ship'][0]['personPlaceData']).has_key('attnLine'):
                            adresses_list['ship'][0]['personPlaceData'].update({'attnLine':_attnLine})
                        else:
                            adresses_list['ship'][0]['personPlaceData']['attnLine'] = _attnLine
                    except Exception, e:
                        pass

            if personPlace.get('personPlaceID') == adresses_list['order'][0]['PersonPlaceId']:
                adresses_list['order'][0]['name2'] = personPlace.findtext('name2')
                adresses_list['order'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['order'][0].get('personPlaceData', False):
                        adresses_list['order'][0]['personPlaceData'] = {}
                    adresses_list['order'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

        return adresses_list


class CommercehubApiSearsCanadaOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiSearsCanadaOpenerp, self).__init__()

    def get_ship2(self, order):
        # Find second shipt 2 address
        ship2 = {}
        if order['vendorShipInfo']:
            ship2['name'] = order['vendorShipInfo'].findtext('name1') or ''
            ship2['address1'] = order['vendorShipInfo'].findtext('address1') or ''
            ship2['address2'] = order['vendorShipInfo'].findtext('address2') or ''
            ship2['city'] = order['vendorShipInfo'].findtext('city') or ''
            ship2['state'] = order['vendorShipInfo'].findtext('state') or ''
            ship2['postalCode'] = order['vendorShipInfo'].findtext('postalCode') or ''
            ship2['country'] = order['vendorShipInfo'].findtext('country') or ''
            ship2['dayPhone'] = order['customer_address'].findtext('dayPhone') or ''

        return ship2

    def get_customer(self, order):
        # Find customer address
        customer = {}
        if order['customer_address']:
            customer['name'] = order['customer_address'].findtext('name1') or ''
            customer['address1'] = order['customer_address'].findtext('address1') or ''
            customer['address2'] = order['customer_address'].findtext('address2') or ''
            customer['city'] = order['customer_address'].findtext('city') or ''
            customer['state'] = order['customer_address'].findtext('state') or ''
            customer['postalCode'] = order['customer_address'].findtext('postalCode') or ''
            customer['country'] = order['customer_address'].findtext('country') or ''
            customer['dayPhone'] = order['customer_address'].findtext('dayPhone') or ''

        return customer

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}

        additional_fields = []
        if(order.get('poHdrData') and order.get('poHdrData').get('packListData')):
            for _attr in ['rmEmail','rmPhone1', 'packslipMessage']:
                if(order.get('poHdrData').get('packListData').get(_attr, '')):
                    additional_fields.append((0, 0, {
                        'name': _attr,
                        'label': _attr,
                        'value': order.get('poHdrData').get('packListData').get(_attr, '')
                    }))

        for _attr in ['VcdId', 'attnLine']:
            if(
               order.get('address').get('ship') and\
               order.get('address').get('ship').get('personPlaceData') and\
               order.get('address').get('ship').get('personPlaceData').get(_attr, '')
            ):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get('address').get('ship').get('personPlaceData').get(_attr, '')
                }))

        ship2 = self.get_ship2(order)
        if ship2:
            additional_fields.append((0, 0, {
                'name': 'ship2',
                'label': 'Ship To',
                'value': ship2,
            }))
        customer = self.get_customer(order)
        if customer:
            additional_fields.append((0, 0, {
                'name': 'customer_address',
                'label': 'Customer Address',
                'value': customer,
            }))

        if(order.get('poHdrData') and order.get('poHdrData').get('merchDivision', '')):
            additional_fields.append((0, 0, {
                'name': 'merchDivision',
                'label': 'merch Division',
                'value': order.get('poHdrData').get('merchDivision', '')
            }))

        for _attr in ['salesDivision', 'paymentMethod', 'custOrderDate', 'orderDate']:
            if(order.get(_attr, '')):
                additional_fields.append((0, 0, {
                    'name': _attr,
                    'label': _attr,
                    'value': order.get(_attr, '')
                }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "gift_message": line.get('poLineData', {}).get('giftMessage', ""),
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku', 'UPC']
        for field in field_list:
            if line.get(field, False):
                if field == 'merchantSKU' and ' ' in line[field]:
                    sku_parts = line[field].split(' ')
                    cr.execute("""  SELECT
                                    DISTINCT    cf.product_id
                                FROM
                                                product_multi_customer_names cn
                                    INNER JOIN  product_multi_customer_fields_name fn on (
                                        fn.name_id = cn.id
                                        AND fn.customer_id = {}
                                    )
                                    INNER JOIN  product_multi_customer_fields cf on cf.field_name_id = fn.id
                                WHERE 1=1
                                    AND cn.name = {}
                                              AND (cf."value" ilike  '{}' or
                                                  cf."value" ilike  '0{}' or
                                                  cf."value" ilike  ' {}' or
                                                  cf."value" ilike  '0 {}' or
                                                  cf."value" ilike  ' 0{}' or
                                                  cf."value" ilike  '{} ' or
                                                  cf."value" ilike  '0{} ' or
                                                  cf."value" ilike  ' {} ' or
                                                  cf."value" ilike  '0 {} ' or
                                                  cf."value" ilike  ' 0 {} '
                                                  )
                    """.format(context['customer_id'], 'customer_sku', sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1], sku_parts[1]))
                    res = cr.fetchall()
                    product, size = None, None
                    if res and len(res) == 1:
                        product_id = res[0][0]
                        product = self.pool.get('product.product').browse(cr, uid, product_id)
                        if len(sku_parts) > 2:
                            try:
                                size_obj = self.pool.get('ring.size')
                                size_float = float(sku_parts[2])/10.0
                                size_ids = size_obj.search(cr, uid, [
                                    ('name', '=', str(size_float))
                                ])
                                if size_ids:
                                    size = size_obj.browse(cr, uid, size_ids[0])
                            except:
                                pass
                else:
                    product, size = self.pool.get('product.product').search_product_by_id(
                        cr, uid, context['customer_id'], line[field])
                if product:
                    if size and size.id:
                        line_obj["size_id"] = size.id
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])

        if(product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by upc %s.\n" % (line['UPC'])
            line_obj["product_id"] = 0

        for name_field in ['lineMerchandise', 'lineHandling', 'lineShipping', 'SKUdescription', 'SKUdescription2', 'lineTotal', 'expectedShipDate']:
            if line.get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field.replace('line', 'line '),
                    'value': line.get(name_field)
                }))
        for name_field in ['taxBreakouts']:
            if line.get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field,
                    'value': line.get(name_field)
                }))
        for name_field in ['psReceivingInstructions', 'encodedPrice', 'creditAmount']:
            if line.get('poLineData').get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field,
                    'value': line.get('poLineData').get(name_field)
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj
