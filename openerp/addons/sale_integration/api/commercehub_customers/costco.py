# -*- coding: utf-8 -*-
from main_parser import CommercehubApiGetOrderObjMain, CommercehubApiOpenerp
from lxml import etree
import xlwt
import cStringIO
from io import BytesIO
from addons.pf_utils.utils.re_utils import f_d
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class CommercehubApiUpdateQTYFileCostco(object):

    def prepare_xls_data(self, updateQtyApi):
        updateLines = []
        for line in updateQtyApi.confirmLines:
            if line.get('sku', False) in [None, 'None', False] or line.get('customer_sku', False) in [None, 'None', False]:
                updateQtyApi.append_to_revision_lines(line, 'bad')
                continue
            updateQtyApi.append_to_revision_lines(line, 'good')
            line['qty'] = int(line.get('qty', 0)) if str(line.get('qty')).strip() else 0
            line['eta_qty'] = 1 if str(line.get('eta_date')).strip() else ''
            line['eta_date'] = datetime.strftime(datetime.strptime(line.get('eta_date', None), "%Y%m%d"), "%m/%d/%Y") if str(line.get('eta_date')).strip() else ''
            updateLines.append(line)
        return updateLines
        

class CommercehubApiGetOrderObjCostco(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjCostco, self).getOrderObj(xml)

        order = etree.XML(xml)

        if ordersObj:
            ordersObj['tax'] = order.findtext('tax')
            ordersObj['credits'] = order.findtext('credits')
            ordersObj['shipping'] = order.findtext('shipping')
            ordersObj['handling'] = order.findtext('handling')
            ordersObj['salesDivision'] = order.findtext('salesDivision')

            ordersObj['orderDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext('custOrderDate') or order.findtext('custOrderDate') or ''
            ordersObj['poHdrData']['reqShipDate'] = order.findall('poHdrData') and order.find('poHdrData').findtext('custOrderDate') or ''
            ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext('giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()
            ordersObj['poHdrData']['giftMessage'] = order.find('poHdrData').findtext('giftMessage')
            if ordersObj['poHdrData']['giftIndicator'] is None:
                gift_message_flag = ordersObj.get('poHdrData', {}).get('giftMessage', '')
                if isinstance(gift_message_flag, str) and gift_message_flag.strip():
                    ordersObj['poHdrData']['giftIndicator'] = "y"

            ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''

            if ordersObj['address']['ship'].get('name2', False):
                ordersObj['address']['ship']['name'] += ' ' + ordersObj['address']['ship'].get('name2', '')
            if ordersObj['address']['order'].get('name2', False) and ordersObj['address']['ship'] != ordersObj['address']['order']:
                ordersObj['address']['order']['name'] += ' ' + ordersObj['address']['order'].get('name2', '')

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjCostco, self).getLineObj(lineItem)

        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice') or False
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineNumber')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['merchant_department'] = lineItem.findtext('merchDept') or ''
        lineItemObj['manufacturerSKU'] = '0' + lineItemObj['merchant_department'] + '-' + (lineItem.findtext('manufacturerSKU') or '')
        lineItemObj['custOrderLineNumber'] = lineItem.findtext('orderLineNumber')
        lineItemObj['expectedShipDate'] = lineItem.findtext('expectedShipDate') # requestedArrivalDate - when expected to arrive

        if not lineItemObj.get('description', False):
            lineItemObj['description'] = lineItem.findtext('merchantSKU')

        if lineItem.findtext('poLineData'):
            po_line_data = lineItem.find('poLineData')
            if po_line_data:
                if not lineItemObj.get('poLineData', False):
                    lineItemObj['poLineData'] = {}

                if po_line_data.findall('prodSize'):
                    lineItemObj['poLineData']['prodSize'] = po_line_data.findtext('prodSize')
                    lineItemObj['manufacturerSKU'] += '-' + lineItemObj['poLineData']['prodSize']
                if po_line_data.findall('lineReqShipDate'):
                    lineItemObj['poLineData']['lineReqShipDate'] = po_line_data.findtext('lineReqShipDate')
                if po_line_data.findall('custOrderLineNumber'):
                    lineItemObj['poLineData']['custOrderLineNumber'] = po_line_data.findtext('custOrderLineNumber')
                if po_line_data.findall('giftWrapIndicator'):
                    lineItemObj['poLineData']['giftWrapIndicator'] = po_line_data.findtext('giftWrapIndicator')
                if po_line_data.findall('merchDept'):
                    lineItemObj['poLineData']['merchant_department'] = po_line_data.findtext('merchDept')
                if po_line_data.findall('factoryOrderNumber'):
                    lineItemObj['poLineData']['factoryOrderNumber'] = po_line_data.findtext('factoryOrderNumber')
                if po_line_data.findtext('personalizationData'):
                    personalization_data = po_line_data.find('personalizationData')
                    if personalization_data:
                        if not po_line_data.get('personalizationData', False):
                            lineItemObj['poLineData']['personalizationData'] = {}
                        if personalization_data.findall('nameValuePair'):
                            lineItemObj['poLineData']['personalizationData']['nameValuePair'] = personalization_data.findtext('nameValuePair')

        return lineItemObj

    def getAddresses(self, order):
        addresses_list = super(CommercehubApiGetOrderObjCostco, self).getAddresses(order)

        for addr_type in ['ship', 'order']:
            if addresses_list.get(addr_type) and not addresses_list[addr_type][0].get('country'):
                addresses_list[addr_type][0]['country'] = 'CA'

        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == addresses_list['ship'][0]['PersonPlaceId']:
                addresses_list['ship'][0]['name2'] = personPlace.findtext('name2')
                addresses_list['ship'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not addresses_list['ship'][0].get('personPlaceData', False):
                        addresses_list['ship'][0]['personPlaceData'] = {}
                    addresses_list['ship'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

            if personPlace.get('personPlaceID') == addresses_list['order'][0]['PersonPlaceId']:
                addresses_list['order'][0]['name2'] = personPlace.findtext('name2')
                addresses_list['order'][0]['nightPhone'] = personPlace.findtext('nightPhone')
                if personPlace.findtext('personPlaceData'):
                    if not addresses_list['order'][0].get('personPlaceData', False):
                        addresses_list['order'][0]['personPlaceData'] = {}
                    addresses_list['order'][0]['personPlaceData']['VcdId'] = personPlace.find('personPlaceData').findtext('VcdId')

        return addresses_list


class CommercehubApiCostcoOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiCostcoOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        gift_message = order.get('poHdrData', {}).get('giftMessage', False) or order.get('gift_message', '')

        additional_fields.append((0, 0, {
            'name': 'gift_message',
            'label': 'Gift Message',
            'value': gift_message,
        }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            "name": line['name'],
            'cost': line['cost'],
            'id': line['id'],
            'merchantSKU': line.get('merchantSKU', False) or "",
            'vendorSku': line.get('vendorSku', False) or "",
            'optionSku': line.get('optionSku', False) or "",
            'size': line.get('size', False) or "",
            'customerCost': "%.2f" % float(line.get('customerCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': line.get('merchantLineNumber', ""),
            "gift_message": line.get('poLineData', {}).get('giftMessage', ""),
        }

        field_list = ['UPC', 'merchantSKU', 'vendorSku']
        self.fill_line_product_data(cr, uid, field_list, line, line_obj, context)

        for name_field in ['merchant_department']:
            if line.get(name_field):
                additional_fields.append((0, 0, {
                    'name': name_field,
                    'label': name_field,
                    'value': line.get(name_field)
                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = self.get_additional_shipping_information_refs(cr, uid, sale_order_id, ship_data, context)
        return res
