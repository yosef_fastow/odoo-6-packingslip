# -*- coding: utf-8 -*-
from main_parser import (
    CommercehubApiGetOrderObjMain,
    CommercehubApiOpenerp
)
from lxml import etree


SETTINGS_FIELDS = (
    ('vendor_warehouse',               'Vendor Warehouse ID',            '001'),
)


class CommercehubApiGetOrderObjKohls(CommercehubApiGetOrderObjMain):

    def getOrderObj(self, xml):
        ordersObj = super(CommercehubApiGetOrderObjKohls, self).getOrderObj(xml)
        order = etree.XML(xml)
        ordersObj['paymentMethod'] = order.findtext('paymentMethod')
        ordersObj['merchandise'] = order.findtext('merchandise')
        ordersObj['poHdrData']['creditType'] = order.findall('poHdrData') and order.find('poHdrData').findall('creditBreakout') and order.find('poHdrData').findall('creditBreakout')[0].get('creditType') or ''
        ordersObj['poHdrData']['creditBreakout'] = order.findall('poHdrData') and order.find('poHdrData').findall('creditBreakout') and order.find('poHdrData').findtext('creditBreakout') or ''
        ordersObj['poHdrData']['taxType'] = order.findall('poHdrData') and order.find('poHdrData').findall('taxBreakout') and order.find('poHdrData').find('taxBreakout').get('taxType') or ''
        ordersObj['poHdrData']['taxBreakout'] = order.findall('poHdrData') and order.find('poHdrData').findall('taxBreakout') and order.find('poHdrData').findtext('taxBreakout') or ''
        ordersObj['poHdrData']['packListData']['packslipMessage'] = order.findall('poHdrData') and order.find('poHdrData').findall('packListData') and order.find('poHdrData').find('packListData').findtext('packslipMessage') or ''
        ordersObj['poHdrData']['giftIndicator'] = order.findall('poHdrData') and order.find('poHdrData').findtext('giftIndicator') and order.find('poHdrData').findtext('giftIndicator').lower()

        ordersObj['vendor_warehouse'] = order.findtext('vendorWarehouseId') or ''

        return ordersObj

    def getLineObj(self, lineItem):
        lineItemObj = super(CommercehubApiGetOrderObjKohls, self).getLineObj(lineItem)
        lineItemObj['merchantLineId'] = lineItem.findtext('merchantLineId')
        lineItemObj['unitOfMeasure'] = lineItem.findtext('unitOfMeasure')
        lineItemObj['unitPrice'] = lineItem.findtext('unitPrice')
        lineItemObj['lineShipping'] = lineItem.findtext('lineShipping')
        lineItemObj['lineHandling'] = lineItem.findtext('lineHandling')

        if lineItem.findall('poLineData'):
            if not lineItemObj['poLineData']:
                lineItemObj['poLineData'] = {}
            lineItemObj['poLineData']['handlingType'] = lineItem.find('poLineData').findall('handlingBreakout') and lineItem.find('poLineData').find('handlingBreakout').get('handlingType') or ''
            lineItemObj['poLineData']['handlingBreakout'] = lineItem.find('poLineData').findall('handlingBreakout') and lineItem.find('poLineData').findtext('handlingBreakout') or ''
            lineItemObj['poLineData']['taxBreakout'] = lineItem.find('poLineData').findall('taxBreakout') and lineItem.find('poLineData').findtext('taxBreakout') or ''
            lineItemObj['poLineData']['taxType'] = lineItem.find('poLineData').findall('taxBreakout') and lineItem.find('poLineData').find('taxBreakout').get('taxType') or ''
            lineItemObj['poLineData']['prodColor'] = lineItem.find('poLineData').findall('prodColor') and lineItem.find('poLineData').findtext('prodColor') or ''
            lineItemObj['poLineData']['prodSize'] = lineItem.find('poLineData').findall('prodSize') and lineItem.find('poLineData').findtext('prodSize') or ''
            lineItemObj['poLineData']['fullRetail'] = lineItem.find('poLineData').findall('fullRetail') and lineItem.find('poLineData').findtext('fullRetail') or ''
            lineItemObj['poLineData']['factoryOrderNumber'] = lineItem.find('poLineData').findall('factoryOrderNumber') and lineItem.find('poLineData').findtext('factoryOrderNumber') or ''
            lineItemObj['poLineData']['lineNote1'] = lineItem.find('poLineData').findall('lineNote1') and lineItem.find('poLineData').findtext('lineNote1')
            lineItemObj['poLineData']['lineNote2'] = lineItem.find('poLineData').findall('lineNote2') and lineItem.find('poLineData').findtext('lineNote2')
            lineItemObj['poLineData']['lineNote1'] = lineItem.find('poLineData').findall('lineNote1') and lineItem.find('poLineData').findtext('lineNote1')
            lineItemObj['poLineData']['giftMessage'] = lineItem.find('poLineData').findall('giftMessage') and lineItem.find('poLineData').findtext('giftMessage')

        return lineItemObj

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjKohls, self).getAddresses(order)
        for personPlace in order.findall('personPlace'):
            if personPlace.get('personPlaceID') == adresses_list['ship'][0]['PersonPlaceId']:
                adresses_list['ship'][0]['name2'] = personPlace.findtext('name2')
                adresses_list['ship'][0]['country'] = personPlace.findtext('country')
                if personPlace.findtext('personPlaceData'):
                    if not adresses_list['ship'][0].get('personPlaceData', False):
                        adresses_list['ship'][0]['personPlaceData'] = {}
                    adresses_list['ship'][0]['personPlaceData']['ReceiptID'] = personPlace.find('personPlaceData').findtext('ReceiptID')
            if personPlace.get('personPlaceID') == adresses_list['order'][0]['PersonPlaceId']:
                adresses_list['order'][0]['name2'] = personPlace.findtext('name2')

        return adresses_list


class CommercehubApiKohlsOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiKohlsOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}

        additional_fields = []

        vendor_warehouse = order.get('vendor_warehouse', False)
        if not vendor_warehouse:
            for item in settings.settings:
                item_name = str(item.name).strip()
                if item_name == 'vendor_warehouse' and item.value:
                    vendor_warehouse = item.value
                    break

        additional_fields.append((0, 0, {
            'name': 'vendor_warehouse',
            'label': 'Vendor Warehouse ID',
            'value': vendor_warehouse,
        }))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
