# -*- coding: utf-8 -*-
from main_parser import (
    CommercehubApiGetOrderObjMain,
    CommercehubApiOpenerp
)
from json import dumps as json_dumps

order_hdr_keys = ['poTypeCode', 'giftMessage']
line_fields = ['expectedShipDate']
line_data_fields = ['lineNote1']


class CommercehubApiGetOrderObjGrouponGoods(CommercehubApiGetOrderObjMain):

    def __init__(self):
        self.hdr_fields.extend(order_hdr_keys)
        self.line_fields.extend(line_fields)
        self.line_data_fields.extend(line_data_fields)

    def getAddresses(self, order):
        adresses_list = super(CommercehubApiGetOrderObjGrouponGoods, self).getAddresses(order)
        shipFrom = order.findall('shipFrom') and order.find('shipFrom').get('vendorShipID') or ''
        addr_fields = self.addr_fields
        addr_fields.extend(['vendorShipID'])
        for vendorShipID in order.findall('vendorShipInfo'):
            vendorShipIDObj = {
                field: vendorShipID.findtext(field) for field in addr_fields
            }
            vendorShipIDObj['vendorShipID'] = vendorShipID.get('vendorShipID')
            if vendorShipIDObj['country']:
                vendorShipIDObj['country'] = vendorShipIDObj['country'][:2]
            if shipFrom == vendorShipIDObj['vendorShipID']:
                adresses_list.update({
                    'shipFrom': vendorShipIDObj,
                })

        return adresses_list


class CommercehubApiGrouponGoodsOpenerp(CommercehubApiOpenerp):

    def __init__(self):
        super(CommercehubApiGrouponGoodsOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}

        additional_fields = []

        poHdrData = order.get('poHdrData')
        if poHdrData:
            self._fill_additional_fields(additional_fields, order_hdr_keys, poHdrData)

        if 'shipFrom' in order['address']:
            order['address']['shipFrom'] = json_dumps(order['address']['shipFrom'])
            self._fill_additional_fields(additional_fields, ['shipFrom'], order['address'])

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        if context is None:
            context = {}

        additional_fields = []

        notes = ""
        try:
            unit_price = float(line.get('unitCost', 0.0))
        except ValueError, e:
            notes += e.message + """
            In xml field <unitCost></unitCost> contain string value which do not convert to float
            """
            unit_price = 0.0

        line_obj = {
            "notes": notes,
            'external_customer_line_id': line['lineItemId'] or line['merchantSKU'],
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line.get('vendorSku', ""),
            'size': line.get('size', ""),
            'tax': line.get('tax', ""),
            'customerCost': "%.2f" % float(line.get('UnitCost', 0.0)),
            'price_unit': "%.2f" % unit_price,
            'merchantLineNumber': str(line.get('merchantLineNumber', "")),
            'expected_ship_date': line.get('expectedShipDate', ""),
        }

        product_obj = self.pool.get('product.product')
        product = False
        field_list = ['vendorSku', 'merchantSKU', 'UPC']
        customer_id = context['customer_id']
        for field in field_list:
            if not line.get(field, False):
                continue
            product, size = product_obj.search_product_by_id(
                cr, uid, 'customer_id', line[field]
            )
            if not product:
                continue
            if size and size.id:
                line_obj["size_id"] = size.id
            size_id = line_obj.get('size_id', False)
            product_cost = product_obj.get_val_by_label(
                cr, uid, product.id, customer_id, 'Customer Price', size_id
            )
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
            break

        if(product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by upc %s.\n" % (line['UPC'])
            line_obj["product_id"] = 0

        poLineData = line.get('poLineData')
        if poLineData:
            self._fill_additional_fields(additional_fields, line_data_fields, poLineData)

        line_obj.update({'additional_fields': additional_fields})

        return line_obj
