# -*- coding: utf-8 -*-
import logging
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from commercehub_edi_customers import main_edi_parser
from commercehub_edi_customers import main_edi_builder
from commercehub_edi_customers import bestbuyca_edi
from openerp.addons.pf_utils.utils.decorators import return_ascii
from pf_utils.utils.re_utils import f_d
from apiopenerp import ApiOpenerp

import json

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',         'Vendor number',         False),
    ('vendor_name',           'Vendor name',           False),
    ('sender_id',             'Sender Id',             'delmar'),
    ('sender_id_qualifier',    'Sender Id Qualifier',   'ZZ'),
    ('receiver_id',           'Receiver Id',           False),
    ('receiver_id_qualifier', 'Receiver Id Qualifier', False),
)

# name: value
DEFAULT_VALUES = {
    'use_ftp': True
}


class CommercehubApiEdi(FtpClient):
    def __init__(self, settings):
        super(CommercehubApiEdi, self).__init__(settings)

    def get_root_dir(self, settings):
        if (settings["customer_root_dir"]):
            path = str(settings["customer_root_dir"])
        else:
            path = str(self.customer)
        root_dir = "commercehub_edi/{0}".format(path)
        return root_dir

    def log(self, log_line=None):
        if(log_line):
            title = log_line.get('title', False)
            msg_type = log_line.get('type', False)
            message = log_line.get('message', '')
            if(msg_type):
                self.append_to_logger(msg_type, message)
            if(title):
                if(msg_type):
                    title = '{0} method: {1} for {2}: {3}'.format(
                        str(msg_type).capitalize(),
                        self.name,
                        self.customer,
                        title
                    )
                else:
                    title = 'INFO method: {0} for {1}: {2}'.format(
                        self.name,
                        self.customer,
                        title
                    )
                self.append_to_log(title, message)
        return True


class CommercehubApiEdiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        apierp = CommercehubEDIOpenerp
        setting_fields = None
        try:
            exec('apierp = {customer}_edi.CommercehubApiEdiOpenerp'.format(customer=settings_variables['customer']))
        except (NameError, SyntaxError):
            apierp = CommercehubEDIOpenerp
        try:
            exec(
                'from commercehub_edi_customers.{customer}_edi import SETTINGS_FIELDS as CUSTOMER_SETTINGS_FIELDS'.format(
                    customer=settings_variables['customer']
                )
            )
            setting_fields = CUSTOMER_SETTINGS_FIELDS
        except (NameError, SyntaxError):
            setting_fields = SETTINGS_FIELDS
        super(CommercehubApiEdiClient, self).__init__(
            settings_variables, apierp, setting_fields, DEFAULT_VALUES
        )
        self.load_orders_api = CommercehubApiEdiGetOrdersXML

    @property
    def customers_with_invoice(self):
        return ['bestbuyca']

    @customers_with_invoice.setter
    def customers_with_invoice(self, value):
        raise Exception('You can\'t set property customers_with_invoice')

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        self._mail_messages = self.load_orders_api._mail_messages
        return orders

    def confirmShipment(self, lines, context=None):
        if(context is None):
            context = {}
        # TODO
        # confirmApi = CommercehubApiEdiConfirmOrders(self.settings, lines)
        # confirmApi.process('send')
        # self.extend_log(confirmApi)
        if (self.is_invoice_required and self.settings['customer'] in self.customers_with_invoice and context.get('invoice', True)):
            invoiceApi = CommercehubApiEdiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)
        return True

    def getOrdersObj(self, xml):
        ordersApi = CommercehubApiEdiGetOrderObj(xml, self.settings['customer'])
        order = ordersApi.getOrderObj()
        return order


class CommercehubApiEdiGetOrdersXML(CommercehubApiEdi):

    name = "GetOrdersFromLocalFolder"
    customer_root_dir = ""
    customer = ""
    _mail_messages = []

    def __init__(self, settings):
        super(CommercehubApiEdiGetOrdersXML, self).__init__(settings)
        self.customer = str(settings.get("customer", ""))
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))

    def parse_response(self, response):
        if(not response):
            return []
        orders_list = []
        if (self.customer == 'bestbuyca'):
            parser = bestbuyca_edi.Parser(self.customer)
        else:
            parser = main_edi_parser.CommercehubApiEdiGetOrderObjMain(self.customer)
        for data in response:
            try:
                parsed_orders = parser.getOrderObj(data)
                if parsed_orders:
                    for order in parsed_orders:
                        order_obj = {
                            'name': order['poNumber'],
                            'ack_control_number': parser.ack_control_number,
                            'number_of_transaction': parser.number_of_transaction,
                            'functional_group': parser.functional_group
                        }
                        if order['Cancellation'] is True:
                            order_obj['name'] = 'CANCEL {0}'.format(order_obj['name'])
                            mail_message = {
                                'email_from': 'erp.delmar@progforce.com',
                                'emailto_list': ['delmar@isddesign.com'],
                                'subject': 'Commercehub EDI 860 Cancel request',
                                'body': 'Customer {0}\nORDER JSON: {1}'.format(self.customer, json.dumps(order)),
                                'attachments': None,
                            }
                            self._mail_messages.append(mail_message)
                        elif order['Change'] is True:
                            self.log({
                                'title': "ORDER {0}: OPERATION CHANGE NOT SUPPORT!".format(order_obj['name']),
                                'type': 'warning',
                                'message': json.dumps(order),
                            })
                            mail_message = {
                                'email_from': 'erp.delmar@progforce.com',
                                'emailto_list': ['delmar@isddesign.com'],
                                'subject': 'Commercehub EDI 860 Change request',
                                'body': 'Customer {0}\nORDER JSON: {1}'.format(self.customer, json.dumps(order)),
                                'attachments': None,
                            }
                            self._mail_messages.append(mail_message)
                            order_obj['name'] = 'CHANGE {0}'.format(order_obj['name'])
                        order_obj['order_type'] = 'edi'
                        order_obj['xml'] = json.dumps(order)
                        orders_list.append(order_obj)
            except Exception:
                import traceback
                self.log({
                    'title': "UNKNOWN ERROR!",
                    'type': 'error',
                    'message': str(traceback.format_exc()),
                })

        return orders_list


class CommercehubApiEdiGetOrderObj():

    name = "GetOrderObj"

    def __init__(self, xml, customer):
        self.xml = xml
        self.customer = customer

    def getOrderObj(self):

        ordersObj = None
        try:
            ordersObj = json.loads(self.xml)
        except Exception:
            pass

        if ordersObj is None:
            parser = main_edi_parser.CommercehubApiGetOrderObjMain()

            ordersObj = parser.getOrderObj(self.xml)
        return ordersObj


class CommercehubApiEdiInvoiceOrders(CommercehubApiEdi):

    name = "InvoiceOrders"
    lines = []

    def __init__(self, settings, lines):
        super(CommercehubApiEdiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders', self.get_root_dir(settings))
        self.lines = lines
        self.filename = f_d("{order_name}_invoice_%d%m%y_%H%M%S.edi.out".format(order_name=str(lines[0]['origin'])))
        self.settings = {key: value for key, value in settings.iteritems() if key in settings['settings_variables_keys']}
        self.settings.update({
            'customer': self.customer,
        })

    @return_ascii
    def upload_data(self):
        if (self.customer == 'bestbuyca'):
            invoice_builder = bestbuyca_edi
        else:
            invoice_builder = main_edi_builder
        invoice_generator = invoice_builder.CommercehubEdiInvoiceGenerator(self)
        data = invoice_generator.upload_data()
        return data


class CommercehubEDIOpenerp(ApiOpenerp):

    def __init__(self):

        super(CommercehubEDIOpenerp, self).__init__()
        self.confirm_fields_map = {
            'upc': 'UPC',
        }