# -*- coding: utf-8 -*-
from inspect import getmembers as _getmembers
from inspect import ismethod as _ismethod
from mako.template import Template
import os


class ApiOpenerp(object):

    pool = {}

    def __init__(self):
        super(ApiOpenerp, self).__init__()
        self.__confirm_fields_map = None
        self.excluded_methods = ['__init__']

    @property
    def confirm_fields_map(self):
        return self.__confirm_fields_map

    @confirm_fields_map.setter
    def confirm_fields_map(self, confirm_fields_map):
        self.__confirm_fields_map = confirm_fields_map

    @property
    def apierp_class(self):
        return self._apierp_class

    @apierp_class.setter
    def apierp_class(self, apierp_class):
        self._apierp_class = apierp_class

    @property
    def pool(self):
        return self._pool

    @pool.setter
    def pool(self, pool):
        self._pool = pool

    @property
    def available_methods(self):
        all_methods = _getmembers(self.apierp_class, predicate=_ismethod)
        self.excluded_methods.extend(
            [
                x[0] for x in all_methods
                if
                (x[1].__doc__ is not None) and
                x[1].__doc__.strip().lower().startswith('cap:') and
                x[0] not in self.excluded_methods
            ]
        )
        return [x[0] for x in all_methods if x[0] not in self.excluded_methods]

    @available_methods.setter
    def available_methods(self, value):
        raise AttributeError("You can't set property available_methods")

    def fill_line(self, cr, uid, settings, line, context=None):
        '''CAP: fill_line method'''
        return {}

    def fill_order(self, cr, uid, settings, order, context=None):
        '''CAP: fill_order method'''
        return {}

    def getCost(self, cr, uid, lineObj, context=None):
        product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, lineObj['product_id'], context['customer_id'],
                                                                         'Customer Price', lineObj['size_id'])
        if product_cost:
            lineObj['cost'] = product_cost
        else:
            lineObj["notes"] += "Can't find product cost.\n" % (str(context['line']))
        return lineObj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        '''CAP: get_accept_information method'''
        pass

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        '''CAP: get_cancel_accept_information method'''
        pass

    def get_additional_invoice_information(self, cr, uid, sale_order_id, invoice_data, context=None):
        '''CAP: get_additional_invoice_information method'''
        pass

    def get_additional_shipping_information(self, cr, uid, sale_order_id, context=None):
        '''CAP: get_additional_shipping_information method'''
        pass

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        '''CAP: get_additional_confirm_shipment_information method'''
        pass

    def get_additional_processing_information(self, cr, uid, settings, picking, order_line, context=None):
        '''CAP: get_additional_processing_information method'''
        pass

    def get_additional_confirm_shipment_information_for_line(self, cr, uid, main_partner_id, order, order_line,
                                                             context=None):
        '''CAP: get_additional_confirm_shipment_information_for_line method'''

    def after_correct_confirm_shipment_callback(self, cr, uid, sale_obj, picking_obj, lines):
        '''CAP: after_confirm_shipment_callback method'''

    def write_unique_fields_for_orders(self, cr, uid, picking):
        '''CAP: write_unique_fields_for_orders method'''

    def get_confirm_fields(self, cr, uid, partner_id, order_line, context=None):
        line = {}

        if not self.confirm_fields_map:
            return line
        product_obj = self.pool.get("product.product")
        product_id = order_line.product_id.id
        size_id = order_line.size_id.id or False

        for key, label in self.confirm_fields_map.iteritems():
            line[key] = product_obj.get_val_by_label(cr, uid, product_id, partner_id, label, size_id)
        return line

    def generate_fail_creation_order_email(self, cr, uid, sale_order_id):
        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        emailto_list = ['delmar@isddesign.com']
        emails_for_sending_if_order_fail = (
            sale_order and
            sale_order.partner_id and
            sale_order.partner_id.emails_for_sending_if_order_fail or
            None
        )
        if emails_for_sending_if_order_fail:
            emailto_list.extend(
                [x for x in str(emails_for_sending_if_order_fail).strip().split(',')]
            )
        conf_obj = self.pool.get('ir.config_parameter')
        web_base_url = conf_obj.get_param(cr, uid, 'web.base.alias')
        tmpl_filename = os.path.join(
            os.path.abspath(
                os.path.dirname(__file__)), 'email_templates/shipping_exception.html')
        tmpl = Template(open(tmpl_filename).read())
        body = tmpl.render(data={
            'web_base_url': web_base_url,
            'sale_order_name': sale_order.name,
            'sale_order_id': sale_order.id,
        })
        result = {
            'emailto_list': list(set(emailto_list)),
            'email_from': 'erp.delmar@gmail.com',
            'subject': 'ORDER IN SHIPPING EXCEPTION `{}`'.format(sale_order.name),
            'body': body,
            'subtype': 'html',
            'attachments': {},
        }
        return result

    def generate_duplicate_order_email(self, cr, uid, xml_obj, external_customer_order_id):
        emailto_list = ['delmar@isddesign.com']
        emails_for_sending_if_order_fail = (
            xml_obj and
            xml_obj.type_api_id and
            xml_obj.type_api_id.customer_ids and
            xml_obj.type_api_id.customer_ids[0] and
            xml_obj.type_api_id.customer_ids[0].emails_for_sending_if_order_fail or
            None
        )
        if emails_for_sending_if_order_fail:
            emailto_list.extend(
                [x for x in str(emails_for_sending_if_order_fail).strip().split(',')]
            )
        conf_obj = self.pool.get('ir.config_parameter')
        web_base_url = conf_obj.get_param(cr, uid, 'web.base.alias')
        tmpl_filename = os.path.join(
            os.path.abspath(
                os.path.dirname(__file__)), 'email_templates/order_duplicate.html')
        tmpl = Template(open(tmpl_filename).read())
        body = tmpl.render(data={
            'web_base_url': web_base_url,
            'sale_order_name': external_customer_order_id,
        })
        result = {
            'emailto_list': list(set(emailto_list)),
            'email_from': 'erp.delmar@gmail.com',
            'subject': 'ORDER NOT CREATED `{}`'.format(external_customer_order_id),
            'body': body,
            'subtype': 'html',
            'attachments': {},
        }
        return result

    def generate_duplicate_xml_email(self, cr, uid, xml_name, duplicate_xml_ids):
        if not duplicate_xml_ids:
            return False
        mail_obj = self.pool.get('mail.message')
        subject = 'XML NOT CREATED `{}`'.format(xml_name)
        if mail_obj.search(cr, uid, [('subject', '=', subject)]):
            return False
        emailto_list = ['delmar@isddesign.com']
        so_obj = self.pool.get('sale.order')
        so_ids = so_obj.search(cr, uid, [('sale_integration_xml_id', 'in', duplicate_xml_ids)])
        if not so_ids:
            return False
        so = so_obj.browse(cr, uid, so_ids[0])
        emails_for_sending_if_order_fail = (
            so and
            so.partner_id and
            so.partner_id.emails_for_sending_if_order_fail or
            None
        )
        if emails_for_sending_if_order_fail:
            emailto_list.extend(
                [x for x in str(emails_for_sending_if_order_fail).strip().split(',')]
            )
        conf_obj = self.pool.get('ir.config_parameter')
        web_base_url = conf_obj.get_param(cr, uid, 'web.base.alias')
        tmpl_filename = os.path.join(
            os.path.abspath(os.path.dirname(__file__)), 'email_templates/xml_duplicate.html')
        tmpl = Template(open(tmpl_filename).read())
        body = tmpl.render(data={
            'web_base_url': web_base_url,
            'xml_name': xml_name,
            'duplicate_so_ids': so_obj.read(cr, uid, so_ids, ['name']),
        })
        result = {
            'emailto_list': list(set(emailto_list)),
            'email_from': 'erp.delmar@gmail.com',
            'subject': subject,
            'body': body,
            'subtype': 'html',
            'attachments': {},
        }
        return result

    def wrap_additional_fields(self, additional_fields):
        return [
            (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
            for key, value in additional_fields.iteritems()
        ]
