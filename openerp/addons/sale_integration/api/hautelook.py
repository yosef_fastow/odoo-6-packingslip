# -*- coding: utf-8 -*-

#  Copyright (c) 2020
#  Project: delmar_openerp
#  File: hautelook.py
#  Author: snake@SOLVVE
#  Updated: 3/23/20, 6:57 PM
#
#  HAUTELOOK customer's sale integration api
#  DELMAR-131

from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
# TODO: switch to Hautelook parser? or not? Platform is the same - DSCO
from customer_parsers.hautelook_dsco_edi_parser import EDIParser
import random
from datetime import datetime, timedelta
from apiopenerp import ApiOpenerp
from dateutil import tz
from openerp.addons.pf_utils.utils.yamlutils import YamlObject

# Logger
import logging
_logger = logging.getLogger(__name__)


# integration global variables
DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}
CUSTOMER_PARAMS = {
    'timezone': 'EST',
}
# DSCO settings variables
SETTINGS_FIELDS = (
    ('vendor_warehouse',            'Vendor warehouse ID',      '001'),
    ('vendor_number',               'Vendor number',            1143),
    ('vendor_name',                 'Vendor name',              "Hautelook"),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              'DELMAR'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    'ZZ'),
    ('vendor_warehouse',            'Vendor Warehouse ID',      'uschp'), # '001'
    ('vendor_warehouse_name',       'Vendor Warehouse Name',    'USCHP'), # 'UNITED CA/US'
)


class HautelookApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, EDIParser)


class HautelookApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(HautelookApiClient, self).__init__(
            settings_variables, HautelookOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = HautelookApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):

        ordersApi = HautelookApiAcknowledgmentOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = HautelookApiConfirmAndInvoiceOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = HautelookApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmLoad(self, orders):
        api = HautelookApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True


class HautelookApiGetOrders(HautelookApi):
    """EDI/V4030 X12/850: 850 Purchase Order"""

    def __init__(self, settings):
        super(HautelookApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class HautelookApiConfirmAndInvoiceOrders(HautelookApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(HautelookApiConfirmAndInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if external_date_order_str:
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        segments.append(self.insertToStr('CUR*BY*USD', {}))

        ref = 'REF*[1]%(code)s*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'code': 'CN',
            'data': self.invoiceLines[0]['tracking_number']
        }))
        ref = 'REF*[1]%(code)s*[2]%(data)s*[3]%(desc)s'

        ship_method = ''
        shipping_service = ''
        carrier = self.invoiceLines[0]['carrier']
        if carrier:
            carrier = carrier.split(', ')
            try:
                ship_method = carrier[1]
                shipping_service = carrier[0]
            except ValueError:
                pass

        segments.append(self.insertToStr(ref, {
            'code': 'ZZ',
            'data': shipping_service,
            'desc': 'ship_carrier'
        }))
        segments.append(self.insertToStr(ref, {
            'code': 'ZZ',
            'data': ship_method,
            'desc': 'ship_method'
        }))
        segments.append(self.insertToStr(ref, {
            'code': 'ZZ',
            'data': self.invoiceLines[0]['carrier_code'],
            'desc': 'shipping_service_level_code'
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'LU',
            'data': self.vendor_warehouse,
            'desc': 'ship_from_location_code'
        }))


        itd = 'ITD*' \
              '[1]%(code)s*' \
              '[2]%(terms_basis_date_code)s*' \
              '[3]*' \
              '[4]*' \
              '[5]*' \
              '[6]%(terms_net_due_date)s*' \
              '[7]%(terms_net_days)s*' \
              '[8]*' \
              '[9]*' \
              '[10]*' \
              '[11]*' \
              '[12]*' \
              '[13]%(terms_net_days_str)s'
        terms_net_days = 5
        terms_net_due_date = ''
        if (external_date_order):
            terms_net_due_date = external_date_order + timedelta(days=terms_net_days)
        else:
            terms_net_due_date = datetime.utcnow() + timedelta(days=terms_net_days)
        terms_net_due_date = terms_net_due_date.strftime('%Y%m%d')

        segments.append(self.insertToStr(itd, {
            'code': '05',  # Discount Not Applicable
            'terms_basis_date_code': 3,
            'terms_net_due_date': terms_net_due_date,
            'terms_net_days': str(terms_net_days),
            'terms_net_days_str': 'Net {0} Days'.format(terms_net_days),
        }))

        it1 = 'IT1*' \
              '[1]%(line_number)s*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]%(unit_price)s*' \
              '[5]QT*' \
              '[6]SK*' \
              '[7]%(sku)s'
        line_number = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['unit_cost'],
                'sku': line['merchantSKU'],
            }))

            ref = 'REF*[1]%(code)s*[2]%(data)s'
            segments.append(self.insertToStr(ref, {
                'code': 'CN',
                'data': line['tracking_number']
            }))
            ref = 'REF*[1]%(code)s*[2]%(data)s*[3]%(desc)s'

            segments.append(self.insertToStr(ref, {
                'code': 'ZZ',
                'data': shipping_service,
                'desc': 'ship_carrier'
            }))
            segments.append(self.insertToStr(ref, {
                'code': 'ZZ',
                'data': ship_method,
                'desc': 'ship_method'
            }))
            segments.append(self.insertToStr(ref, {
                'code': 'ZZ',
                'data': line['carrier_code'],
                'desc': 'shipping_service_level_code'
            }))
        tds = 'TDS*[1]%(total_amount)s*[2]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),
        }))
        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class HautelookApiAcknowledgmentOrders(HautelookApi):
    """EDI/V4030 X12/855: 855 Purchase Order Acknowledgment"""

    def __init__(self, settings, lines, state):
        super(HautelookApiAcknowledgmentOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel','cancel'):
            return self.accept_cancel()

        self.edi_type = '855'

        lines = []
        statuscode = 'AD'
        segments = []
        date = datetime.utcnow()
        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'
        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.utcnow().strftime('%Y%m%d')
        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
        }
        segments.append(self.insertToStr(bak, bak_data))

        segments.append(self.insertToStr('CUR*BY*USD', {}))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Customer Order Number
            'order_number': self.processingLines['external_customer_order_id'],
        }))

        dmt = 'DMT*[1]004*[2]%(date)s*[3]%(time)s'
        segments.append(self.insertToStr(dmt, {
            'date': date.strftime('%Y%m%d'),
            'time': date.strftime('%H:%M:%s'),
        }))

        td5 = 'TD5*[1]Z*[2]ZZ*[3]%(code)s*[4]ZZ*[5]%(ship_method)s*[6]*[7]ZZ*[8]%(ship_service_level_code)s'
        segments.append(self.insertToStr(td5, {
            'code': self.processingLines['carrier'],
            'ship_method': self.processingLines['order_additional_fields'].get('dsco_ship_method',''),
            'ship_service_level_code': self.processingLines['order_additional_fields'].get('ship_service_level_code','')
        }))

        n9 = 'N9*[1]CO*[2]%(consumer_order_number)s'
        segments.append(self.insertToStr(n9, {
            'consumer_order_number': self.processingLines['order_additional_fields'].get('consumer_order_number','')
        }))

        n1 = 'N1*[1]ST*[2]%(vendor_name)s'  # VN Vendor
        # vendor_warehouse = self.processingLines['order_additional_fields']['vendor_warehouse']
        # n1 = 'N1*[1]SE*[2]%(warehouse)s*[3]ZZ*[4]%(warehouse)s'
        segments.append(self.insertToStr(n1, {
            'vendor_name': self.vendor_name,
            # 'warehouse': vendor_warehouse
        }))

        n3 = 'N3*[1]%(address1)s*[2]%(address2)s'

        address1 = self.processingLines['ship_to_address']['address1']
        address2 = self.processingLines['ship_to_address']['address2'] or ''
        if (not address1):
            raise Exception('Ship to address is required for 855(Purchase Order Acknowledgment)!')
        segments.append(self.insertToStr(n3, {
            'address1': address1,
            'address2': address2,
        }))

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'
        segments.append(self.insertToStr(n4, {
            'city': self.processingLines.get('city', ''),
            'state': self.processingLines.get('state', ''),
            'zip': self.processingLines.get('zip', ''),
            'country': self.processingLines.get('country', ''),
        }))

        per = 'PER*[1]IC*[2]*[3]TE*[4]%(phone)s*[5]EM*[6]%(email)s'
        segments.append(self.insertToStr(per, {
            'phone': self.processingLines.get('phone', ''),
            'email': self.processingLines.get('email', ''),
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(optionSku_qualifier)s*[7]%(optionSku)s*[8]%(merchantSKU_qualifier)s*[9]%(merchantSKU)s"
        lin = 'LIN*[1]*[2]%(optionSku_qualifier)s*[3]%(optionSku)s'
        ack = "ACK*[1]%(line_status)s*[2]%(qty)s*[3]EA*[4]369*[5]%(date)s*[6]*[7]%(optionSku_qualifier)s*[8]%(optionSku)s"
        i = 1
        for line in self.processingLines['lines']:
            optionSku = line['merchantSku'] or ''
            optionSku_qualifier = optionSku and 'SK' or ''
            segments.append(self.insertToStr(po1, {
                'line_number': str(i),
                'qty': line['product_qty'],
                'unit_price': line['price_unit'],
                'merchantSKU_qualifier': '',
                'merchantSKU': '',
                'optionSku': optionSku,
                'optionSku_qualifier': optionSku_qualifier,
            }))
            segments.append(self.insertToStr(lin, {
                'optionSku': optionSku,
                'optionSku_qualifier': optionSku_qualifier,

            }))
            segments.append(self.insertToStr(ack, {
                'line_status': 'IA',
                'qty': line['product_qty'],
                'date': date.strftime('%Y%m%d'),
                'optionSku': optionSku,
                'optionSku_qualifier': optionSku_qualifier,
            }))
            i += 1

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '870'
        segments = []

        st = 'ST*[1]870*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))
        # date = datetime.strptime(self.processingLines[0]['order_additional_fields']['dsco_create_date'], '%Y-%m-%d %H:%M:%S.%f')
        # date = self.processingLines[0]['order_additional_fields']['dsco_create_date']
        bsr = 'BSR*[1]2*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'
        bsr_data = {
            'statuscode': 'PP',  # confirms that you are able to cancel the PO
            'po_number': self.vendor_number,
            'po_date':  datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
        }
        segments.append(self.insertToStr(bsr, bsr_data))

        segments.append(self.insertToStr('HL*[1]1*[2]0*[3]O', {}))

        prf = 'PRF*%(po)s'
        prf_data = {
            'po':  self.processingLines[0]['po_number'],
        }
        segments.append(self.insertToStr(prf, prf_data))

        ref = 'REF*[1]%(code)s*[2]%(reason)s*[3]%(desc)s'
        ref_data = {
            'code':  'TD',
            'desc': 'order_cancel_reason',
            'reason': 'Out of Stock' # self.processingLines[0].find("CancelReason")
        }
        segments.append(self.insertToStr(ref, ref_data))

        hl = 'HL*[1]%(parent_id)s*[2]%(line_id)s*[3]I'
        line_number = 1
        po1 = "PO1*" \
              "[1]%(line_number)s*" \
              "[2]%(qty)s*" \
              "[3]EA*" \
              "[4]%(unit_price)s*" \
              "[5]*" \
              "[6]SK*" \
              "[7]%(optionSku)s"

        isr = "ISR*[1]IC*[2]*[3]%(cancel_code)s"

        for line in self.processingLines:
            next_line_number = line_number+1
            hl_data = {
                'parent_id': str(line_number),
                'line_id': str(next_line_number),
            }
            segments.append(self.insertToStr(hl, hl_data))
            segments.append(self.insertToStr(po1, {
                'line_number': str(line_number),
                'qty': line['product_qty'],
                'unit_price': line['price_unit'],
                'optionSku': line['merchantSKU'] or '',
            }))

            line_number = next_line_number
            segments.append(self.insertToStr(isr, {
                'cancel_code': 'W01'
                # W01 - Out of Stock
                # W13 - Not Enough Stock
                # A83 - Discontinued Item
                # A03 - Incorrect Address
                # 051 - Invalid Ship Instructions
                # D50 - Can’t Ship on Time
                # ABN - Cancelled at Retailer Request
                # A13 - Other
            }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),
        }))
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CA'})


class HautelookApiUpdateQTY(HautelookApi):
    """EDI/V4030 X12/846: 846 Inventory Inquiry"""

    def __init__(self, settings, lines):
        super(HautelookApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]00*[2]MM*[3]%(inventory_feed_qualifier)s*[4]%(date)s*[5]%(time)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'date': date.strftime('%Y%m%d'),
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'time': date.strftime('%H:%M:%s'),
        }))

        segments.append(self.insertToStr('CUR*SE*USD', {}))

        ref = 'REF*[1]IA*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'data': 'HAUTELOOK',
        }))

        lin = 'LIN*' \
              '[1]%(line_number)s*' \
              '[2]SK*' \
              '[3]%(sku)s*' \
              '[4]UP*' \
              '[5]%(upc)s*' \
              '[6]BP*' \
              '[7]%(partner_sku)s*' \
              '[8]*' \
              '[9]*' \
              '[10]*' \
              '[11]*' \
              '[12]*' \
              '[13]*' \
              '[14]*' \
              '[15]*' \
              '[16]*' \
              '[17]*' \
              '[18]*' \
              '[19]*' \
              '[20]*' \
              '[21]*' \
              '[22]*' \
              '[23]'

        pid = 'PID*[1]%(code)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(desc)s'
        ctp = 'CTP*[1]AS*[2]WHL*[3]%(cost)s'
        qty = 'QTY*[1]33*[2]%(qty)s*[3]EA'
        sch = 'SCH*%(eta_qty)s*EA***018*%(eta_date)s'
        ref = 'REF*[1]ZZ*[2]%(status)s*[3]%(desc)s'
        n1 = 'N1*[1]SE*[2]%(warehouse_name)s*[3]ZZ*[4]%(warehouse_code)s'

        line_number = 0
        for line in self.updateLines:
            if not (bool(line['upc']) or bool(line['customer_id_delmar']) or bool(line['customer_sku'])):
                continue

            line_number += 1
            upc = line.get('upc', '0')
            if not upc:
                upc = '0'
            lin_data = {
                'sku': line['sku'],
                'line_number': line_number,
                'upc': "%012d" % int(upc),
                'partner_sku': line['sku']
            }

            segments.append(self.insertToStr(lin, lin_data))

            segments.append(self.insertToStr(pid, {
                'code': 'F',  # Free-form
                'characteristic_code': '08',  # Product
                'desc': str(line['description'])[0:80]
            }))
            segments.append(self.insertToStr(ctp, {
                'cost': str(line.get('customer_price', '').replace('$', '')),
            }))
            segments.append(self.insertToStr(qty, {
                'qty': str(line['qty']),  # Product
            }))
            line_qty = line.get('qty', False)
            line_status = 'out-of-stock'
            dnr_flag = True if line.get('dnr_flag', False) == '1' else False
            if line_qty:
                line_status = 'in-stock'

            if not line_qty:
                line_eta_date = '20391231' if line.get('eta') in ['0', 0, None, 'None'] else line.get('eta')
                line_eta_qty = 0 if line.get('eta_qty', 0)  in ['0', 0, None, 'None'] else line.get('eta_qty')
                if dnr_flag:
                    line_status = 'discontinued'
                    line_eta_date = '20391231'

                if not line_eta_qty and line_eta_date != '20391231' and not dnr_flag:
                    line_eta_qty = 1

                segments.append(self.insertToStr(sch, {
                    'eta_qty': line_eta_qty,
                    'eta_date': line_eta_date,
                }))
            else:
                # if user has more then one warehouse we need to send the N1 line with the warehouse name
                # and code. We only send this line when the qty of the item is more then 0.
                segments.append(self.insertToStr(lin, lin_data))
                
                segments.append(self.insertToStr(qty, {
                    'qty': str(line['qty']),  # Product
                }))
                
                segments.append(self.insertToStr(n1, {
                    'warehouse_name': self.vendor_warehouse_name,
                    'warehouse_code': self. vendor_warehouse
                }))

            segments.append(self.insertToStr(ref, {
                'desc': 'status',
                'status': line_status,
            }))
            self.append_to_revision_lines(line, 'good')
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'GS'})


class HautelookApiFunctionalAcknoledgment(HautelookApi):

    orders = []

    def __init__(self, settings, orders):
        super(HautelookApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):
        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            order = yaml_obj.deserialize(_data=order_yaml['xml'])
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class HautelookOpenerp(ApiOpenerp):

    def __init__(self):
        super(HautelookOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = {
            "notes": "",
            "name": line['name'] if 'name' in line else '',
            'customerCost': line['customerCost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': 'vendor_sku' in line and line['vendor_sku'] or '',
            'tax_type': line.get('poLineData').get('taxType', ''),
            'qty': line['qty'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = None
        field_list = ['sku', 'upc', 'merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                search_field = line[field]
                line_obj['sku'] = line[field]
                if '/' in line_obj['sku']:
                    try:
                        _sku = line_obj['sku']
                        _size = _sku[_sku.find("/") + 1:]
                        if _size:
                            search_field = _sku[:_sku.find("/")]
                            line_obj['sku'] = search_field
                            line_obj['size'] = _size
                    except Exception, e:
                        _logger.warning("Find SKU exception: %s" % e.message)
                        pass
                # try to find product
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr, uid, context['customer_id'],
                    search_field, ('default_code', 'customer_sku', 'upc')
                )
                # if product was found
                if product:
                    # define size
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line_obj.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', line_obj.get('size'))])
                        except Exception, e:
                            _logger.debug("No sizes found for size name: %s" % line_obj.get('size'))
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    # finish searching for product if it was found
                    break

        # if product was found
        if product:
            line_obj["product_id"] = product.id
            if not line_obj['name'] or line_obj['name'] == '':
                line_obj['name'] = product.name_template
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0
        # return updated line
        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')

        order_lines = []
        for line in sale_order.order_line:
            line_additional_fields = {x.name: x.value for x in line.additional_fields or []}
            order_lines.append({
                'vendorSku': line.vendorSku,
                'merchantSku': line.merchantSKU,
                'customer_sku': line_additional_fields.get('customer_sku', ''),
                'upc': line_additional_fields.get('upc', ''),
                'product_qty': int(line.product_uom_qty),
                'price_unit': line.price_unit,
            })

        result = {
            'po_number': sale_order['po_number'],
            'external_customer_order_id': sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR',
            'address1': '',
            'city': '',
            'state': '',
            'zip': '',
            'country': '',
            'ship_to_address': {
                'edi_code': 'ST',
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'country': '',
                'zip': '',
            },
            'lines': order_lines,
            'carrier': sale_order.carrier,
            'order_additional_fields': {x.name: x.value for x in sale_order.additional_fields or []},
        }
        if vendor_address:
            result['address1'] = vendor_address.street
            result['city'] = vendor_address.city
            result['state'] = vendor_address.state_id.code
            result['zip'] = vendor_address.zip
            result['country'] = vendor_address.country_id.code

        if (sale_order.partner_shipping_id):
            result['ship_to_address'].update({
                'name': sale_order.partner_shipping_id.name or '',
                'address1': sale_order.partner_shipping_id.street or '',
                'address2': sale_order.partner_shipping_id.street2 or '',
                'city': sale_order.partner_shipping_id.city or '',
                'state': sale_order.partner_shipping_id.state_id.code or '',
                'country': sale_order.partner_shipping_id.country_id.code or '',
                'zip': sale_order.partner_shipping_id.zip or '',
            })

        return result

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['poNumber'], tuple(customer_ids)))

        return cr.dictfetchall()

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        customer_skus = [line.vendorSku for line in sale_order.order_line if line.vendorSku]
        customer_skus_str = ' ;'.join(customer_skus)
        res = {
            'Ref2': customer_skus_str[:30] or ''
        }
        return res

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):

        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_order_id': sale_obj.external_customer_order_id,
            'product_qty': order_line.product_uom_qty,
            'external_customer_line_id': order_line.external_customer_line_id,
            'vendorSku': order_line.vendorSku,
        }

        return line
