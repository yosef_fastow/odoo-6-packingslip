# -*- coding: utf-8 -*-
from httpauthapiclient import HTTPWithAuthApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
from openerp.addons.pf_utils.utils.re_utils import f_d
from openerp.addons.pf_utils.utils.str_utils import str_encode
from json import loads as json_loads
from json import dumps as json_dumps
from openerp.addons.pf_utils.utils.re_utils import str2date
from openerp.addons.pf_utils.utils.re_utils import str2float
from openerp.addons.pf_utils.utils.re_utils import pretty_float
from openerp.addons.pf_utils.utils.helper import get_image_url
from customer_parsers.jet_input_parser import Parser
from threading import Thread
from Queue import LifoQueue
from apiopenerp import ApiOpenerp
import requests
import cStringIO
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from time import sleep
from utils.ediparser import check_none
import logging

logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_http': True,
}


class JetApi(HTTPWithAuthApiClient):
    def __init__(self, settings):
        super(JetApi, self).__init__(settings)

    @property
    def sub_request_kwargs(self):
        request = {
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': self.token_type + ' ' + self.id_token
            },
        }

        return request

    @property
    def file_token(self):
        token = self._cur_file_token if hasattr(self, '_cur_file_token') else self.new_file_token
        return token

    @property
    def new_file_token(self):
        getTokenApi = JetApiGetFileToken(self.settings)
        token = getTokenApi.process('only_result')
        self._cur_file_token = token
        return self._cur_file_token

    @file_token.setter
    def file_token(self, value):
        raise AttributeError('You can\'t set file token property')

    def process_response(self, response):
        response_data = {}
        response_text = super(JetApi, self).process_response(response)
        if response_text:
            response_data = json_loads(response_text)
        return response_data

    def date_to_str(self, date_str):
        date = None
        if date_str:
            try:
                date = date_str.strftime(self.get_date_format())
            except:
                pass
        return date

    def str_to_date(self, date):
        return datetime.strptime(date, self.get_date_format())

    def get_date_format(self):
        return "%Y-%m-%dT%H:%M:%S.%s%z"

    def check_response(self, response):

        accepted_codes = [
            requests.codes.ok,
            requests.codes.created,
            requests.codes.accepted,
            requests.codes.no_content,
        ]

        if response.status_code not in accepted_codes:
            log_msg = {
                'title': "Receive %s Jet" % self.name,
                'msg': response.text,
                'type': 'receive',
                'create_date': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%s-05:00')
            }
            self._log.append(log_msg)
            response.raise_for_status()

        return True


class JetApiGetToken(HTTPWithAuthApiClient):

    def __init__(self, settings):
        super(JetApiGetToken, self).__init__(settings)
        self.use_http_settings('get_token')

    @property
    def sub_request_kwargs(self):
        return {
            'data': json_dumps({
                'user': self.username,
                'pass': self.password,
            }),
            'headers': {'Content-Type': 'application/json'},
        }

    def process_response(self, response):
        response_data = super(JetApiGetToken, self).process_response(response)
        response_data = json_loads(response_data)
        return response_data


class param(object):
    def __init__(self, key, value):
        super(param, self).__init__()
        self.name = str(key)
        self.value = str(value)


class JetApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        multi_http_settings_dict = {}
        if 'multi_http_settings' in settings_variables:
            for mhs in settings_variables['multi_http_settings']:
                multi_http_settings_dict.update(mhs.to_dict())
        settings_variables['multi_http_settings_dict'] = multi_http_settings_dict
        super(JetApiClient, self).__init__(
            settings_variables, JetOpenerp, False, False)
        self.settings.update(self.token)

    @property
    def token(self):
        getTokenApi = JetApiGetToken(self.settings)
        token = getTokenApi.process('only_result')
        self.extend_log(getTokenApi)
        return {
            'id_token': token['id_token'],
            'token_type': token['token_type'],
            'expires_on': str2date(token['expires_on']),
        }

    @token.setter
    def token(self, value):
        raise AttributeError('You can\'t set token property')

    #DLMR-1070
    def grantReturnOrder(self, order):
        if not order or not order.id:
            return False
        grant = False
        logger.debug("Checking return for order: %s" % order.name)
        retApi = JetApiLoadReturnOrders(self.settings)
        ret_links = retApi.process('only_result')
        logger.debug("Return links: %s" % ret_links)
        for link in ret_links:
            ret_get = JetApiGetReturnOrders(self.settings, link)
            ret_data = ret_get.process('only_result')
            logger.debug("Return data: %s" % ret_data)
            if ret_data.get('merchant_order_id', None) and ret_data.get('merchant_order_id') == order.sale_id.external_customer_order_id:
                grant = True
                break
        return grant

    # DLMR-1070
    def returnOrders(self, lines):
        # check return granted
        po_number = lines[0].get('po_number')
        return_id = None
        # get list of return orders
        retApi = JetApiLoadReturnOrders(self.settings)
        ret_links = retApi.process('only_result')
        for link in ret_links:
            # get return order data
            ret_get = JetApiGetReturnOrders(self.settings, link)
            ret_data = ret_get.process('only_result')
            if ret_data.get('reference_order_id', None) and ret_data.get('reference_order_id') == po_number:
                return_id = ret_data.get('merchant_return_authorization_id', None)
                break

        # process return if order was found
        if return_id:
            returnApi = JetApiReturnOrders(self.settings, lines, return_id)
            return_res = returnApi.process('only_result')
            logger.debug("Return api result: %s" % return_res)
            self.extend_log(returnApi)
            return True

        logger.error("Return order PO#%s was not found in Jet system!" % po_number)
        return False

    def getTaxonomy(self, taxonomy_url):
        getTaxonomyApi = JebApiGetTaxonomy(self.settings, taxonomy_url)
        res = getTaxonomyApi.process('only_result', skip=[404])
        return res

    def taxonomy_links_thread(self):
        while True:
            url = self.queue.get()
            if url:
                taxonomy_res = self.getTaxonomy(url)
                if taxonomy_res:
                    self.taxonomy_links_res.append(taxonomy_res)
                sleep(2)
            self.queue.task_done()

    def getAllTaxonomy(self, items_in_batch=None, offset=0, limit=None):
        urls = []

        if items_in_batch is None:
            items_in_batch = int(self.settings.get('taxonomy_batch_size', 100))

        if limit is None:
            limit = int(self.settings.get('taxonomy_batch_limit', 5))

        batch_i = 0
        while batch_i < limit:
            getUrlsApi = JebApiGetTaxonomyLinks(
                self.settings,
                limit=items_in_batch,
                offset=offset + batch_i * items_in_batch
            )
            batch_i += 1
            sub_urls = getUrlsApi.process('only_result', skip=[404])
            if sub_urls:
                urls.extend(sub_urls)
            else:
                limit = limit - batch_i
                batch_i = 0
                offset = 0

        self.taxonomy_links_res = []
        self.queue = LifoQueue()
        for tread_i in range(25):  # Number of threads
            t = Thread(target=self.taxonomy_links_thread)
            t.daemon = True
            t.start()
        for url in urls:
            self.queue.put(url)
        self.queue.join()

        return (self.taxonomy_links_res, offset + batch_i * items_in_batch)

    def loadOrders(self, save_flag=False, order_type='order'):
        '''
        Function for loading orders from JET.API
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        '''
        orderApi = JetApiGetOrders(self.settings)
        if save_flag:
            orders = orderApi.process('load')
        else:
            orders = orderApi.process('read_new')
        self.extend_log(orderApi)
        return orders

    def updateQTY(self, lines, mode=None):

        self.settings['main_product_field'] = 'customer_id_delmar'
        self.checkAttributes()

        # Get list of the nodes
        nodeApi = JetApiGetFulfillmentNodes(self.settings)
        nodes = nodeApi.process('only_result')
        self.settings.update(nodes)

        # SKU
        updateSKUApi = JetApiUpdateMerchantSKU(self.settings, lines)
        updateSKUApi.process('only_result')
        self.extend_log(updateSKUApi)

        # VARIATIONS
        updateVarApi = JetApiUpdateVariations(self.settings, updateSKUApi.variations)
        updateVarApi.process('only_result')
        self.extend_log(updateVarApi)

        # Price
        updatePriceApi = JetApiUpdatePrice(self.settings, updateSKUApi.revision_lines)
        updatePriceApi.process('only_result')
        self.extend_log(updatePriceApi)

        # Qty
        updateQtyApi = JetApiUpdateQty(self.settings, updateSKUApi.revision_lines)
        updateQtyApi.process('only_result')
        self.extend_log(updateQtyApi)

        # Archive
        updateQtyArchive = JetApiArchive(self.settings, lines)
        updateQtyArchive.process('only_result')
        self.extend_log(updateQtyArchive)

        self.check_and_set_filename_inventory(updateQtyApi)
        return updateSKUApi.revision_lines

    def confirmShipment(self, lines, context=None):
        res = True
        confirmApi = JetApiConfirmOrders(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        res = 'Confirm result:\n{0}'.format(str(res_confirm))
        logger.info("CONF RES: %s" % res)
        return res

    def processingOrderLines(self, lines, state='accepted'):
        res = True
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            ordersApi = JetApiCancelOrders(self.settings, lines, state)
            res = ordersApi.process('only_result')
            self.extend_log(ordersApi)
        return res

    def confirmLoad(self, orders):
        if orders:
            yaml_obj = YamlObject()
            for order_yaml in orders:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
                confirm_order = {
                    'id': order['poNumber'],
                    'order_items': [{
                        'order_item_id': x['id'],
                        'order_item_acknowledgement_status': 'fulfillable',
                    } for x in order['lines']]
                }
                api = JetApiAcknowledgmentOrders(self.settings, confirm_order, state='accepted')
                api.process('send')
                self.extend_log(api)
        return True

    def checkAttributes(self):

        try:
            self.settings['ring_size_attribute_id'] = int(self.settings.get('ring_size_attribute_id'))
        except:
            raise Exception("Jet inventory wrong `ring_size_attribute_id`")


class JetApiGetFileToken(JetApi):
    def __init__(self, settings):
        super(JetApiGetFileToken, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('get_file_token')


class JetApiGetFulfillmentNodes(JetApi):
    def __init__(self, settings):
        super(JetApiGetFulfillmentNodes, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('get_fulfillment_nodes')


class JetApiGetOrders(JetApi):
    '''
    This call retrieves all sales orders that were created
    or modified by fulfillment activity during the time
    range specified in the request.
    '''

    def __init__(self, settings):
        super(JetApiGetOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.parser = Parser(self)
        self.filename = f_d('ORDERS_%Y%m%d_%H%M%S_%f', datetime.utcnow())

    def process_response(self, response):
        response_data = super(JetApiGetOrders, self).process_response(response)
        write_data = []
        if 'order_urls' in response_data:
            for url in response_data['order_urls']:
                obj_api_order = JetApiGetOrder(self.settings, url)
                order_data = obj_api_order.process('only_result', skip=[404])
                if order_data:
                    write_data.append(order_data)
        return write_data


class JetApiGetOrder(JetApi):

    def __init__(self, settings, url):
        super(JetApiGetOrder, self).__init__(settings)
        self.use_http_settings('load_orders')
        self.sub_url = url

    def process_response(self, response):
        response_data = super(JetApiGetOrder, self).process_response(response)
        response_data.update({
            'urls': {
                'base_url': self.base_url,
                'sub_url': self.sub_url,
                'full_url': self.url,
            }
        })
        return json_dumps(response_data)


class JetApiAcknowledgmentOrders(JetApi):
    '''
    This call send acknowledgement status for orders.
    '''

    def __init__(self, settings, order, state):
        super(JetApiAcknowledgmentOrders, self).__init__(settings)
        self.order = order
        self.state = state
        self.use_http_settings('acknowledgement')
        self.filename_local = f_d('jet_ack_{id}%Y_%m_%d_%H_%M.txt'.format(**order), datetime.utcnow())

        if hasattr(self, 'sub_url'):
            self.sub_url = self.sub_url.format(**order)

    @property
    def sub_request_kwargs(self):
        request = super(JetApiAcknowledgmentOrders, self).sub_request_kwargs
        request.update({
            'data': json_dumps({
                'acknowledgement_status': 'accepted',
                'order_items': self.order.get('order_items', [])
            })
        })
        return request

    def check_response(self, response):
        try:
            super(JetApiAcknowledgmentOrders, self).check_response(response)
        except Exception, e:
            if response.status_code == 400 and response.text:
                resp = json_loads(response.text)
                for error in resp.get('errors', []):
                    if 'Order is acknowledged' in error:
                        return True

            raise e

        return True


# DLMR-1070
class JetApiReturnOrders(JetApi):
    def __init__(self, settings, lines, jet_return_id=None):
        super(JetApiReturnOrders, self).__init__(settings)
        self.use_http_settings('return_orders')
        self.lines = lines
        self.jet_return_id = jet_return_id

        # create url
        if hasattr(self, 'sub_url'):
            self.sub_url = self.sub_url.format(jet_return_id=jet_return_id)

        logger.warn("SUB URL: %s" % self.sub_url)
        # get return data from jet
        data_url = "/returns/state/{jet_return_id}".format(jet_return_id=self.jet_return_id)
        data_obj = JetApiGetReturnOrders(settings, data_url)
        self.jet_return_data = data_obj.process('only_result') or None

    @property
    def sub_request_kwargs(self):
        request = super(JetApiReturnOrders, self).sub_request_kwargs
        ret_obj = {}
        ret_items = []

        if not self.jet_return_data:
            return request

        # main order data
        ret_obj.update({
            'merchant_order_id': self.jet_return_data.get('merchant_order_id'),
            'agree_to_return_charge': True,
            'alt_order_id': self.jet_return_data.get('alt_order_id'),
        })

        # gen jet lines
        jet_lines = {x.get('order_item_id'): x for x in self.jet_return_data.get('return_merchant_SKUs')}

        # items
        for local_line in self.lines:
            logger.debug("ADD FIELDS: %s" % local_line.get('additional_fields'))
            if local_line.get('external_customer_line_id') in jet_lines and local_line.get('ordered_qty') == jet_lines[local_line.get('external_customer_line_id')].get('return_quantity'):
                ret_items.append({
                    "order_item_id": local_line.get('external_customer_line_id'),
                    "alt_order_item_id": jet_lines[local_line.get('external_customer_line_id')].get('alt_order_item_id'),
                    "total_quantity_returned": int(float(local_line.get('ordered_qty'))),
                    "order_return_refund_qty": int(float(local_line.get('ordered_qty'))),
                    #"return_refund_feedback": jet_lines[local_line.get('external_customer_line_id')].get('reason') or "other",
                    "refund_amount": {
                        #"principal": float(local_line.get('ItemPrice')),
                        "principal": jet_lines[local_line.get('external_customer_line_id')].get('requested_refund_amount').get('principal'),
                        "tax": 0,
                        #"shipping_cost": float(local_line.get('shipCost')),
                        "shipping_cost": jet_lines[local_line.get('external_customer_line_id')].get('requested_refund_amount').get('shipping_cost'),
                        #"shipping_cost": 0,
                        "shipping_tax": 0
                    }
                })
            else:
                raise Exception("This line not exists in JET's return request order: id={ex_line_id}, sku={sku}, qty={qty}".
                                format(ex_line_id=local_line.get('external_customer_line_id'), sku=local_line.get('merchantSKU'), qty=local_line.get('ordered_qty')))

        # add items to request
        ret_obj.update({'items': ret_items})

        # add return object to request
        request.update({'data': json_dumps(ret_obj)})
        return request

    def check_response(self, response):
        logger.info("RUN CHECK MY RESPONSE")
        try:
            super(JetApiReturnOrders, self).check_response(response)
        except Exception, e:
            if response.status_code == 400 and response.text:
                resp = json_loads(response.text)
                logger.warn("RESPONSE: %s" % resp)
                # for error in resp.get('errors', []):
                #   if 'Order is acknowledged' in error:
                #       return True

            raise e

        return True


# DLMR-1070
class JetApiGetReturnOrders(JetApi):
    def __init__(self, settings, url):
        super(JetApiGetReturnOrders, self).__init__(settings)
        self.use_http_settings('load_return_orders')
        self.sub_url = url
        logger.debug("Url to get order data: %s" % url)


# DLMR-1070
class JetApiLoadReturnOrders(JetApi):
    def __init__(self, settings):
        super(JetApiLoadReturnOrders, self).__init__(settings)
        self.use_http_settings('load_return_orders')

    def process_response(self, response):
        response_data = super(JetApiLoadReturnOrders, self).process_response(response)
        urls = []
        if 'return_urls' in response_data:
            urls = response_data['return_urls']
        return urls


class JetApiConfirmOrders(JetApi):

    def __init__(self, settings, lines):
        super (JetApiConfirmOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.lines = lines
        self.filename = "{:ShipConfirm_%Y%m%d%H%M.xml}".format(datetime.now())
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(id=self.lines[0]['po_number'])

    @property
    def sub_request_kwargs(self):
        request = super(JetApiConfirmOrders, self).sub_request_kwargs
        orderObj = {}
        if self.lines:
            root_line = self.lines[0]

            orderLines = []
            for line in self.lines:
                lineObj = {
                    'alt_shipment_item_id': line['external_customer_line_id'],  # Optional seller supplied ID for an item
                    'merchant_sku': line['merchantSKU'],  # The merchant SKU that is in the given shipment
                    'response_shipment_sku_quantity': int(line['product_qty']),  # Quantity of the given SKU that was shipped in the given shipment
                    'response_shipment_cancel_qty': 0,  # Quantity of the given SKU that was cancelled in the given shipment
                }
                orderLines.append(lineObj)

            # responce_shipment_date = self.date_to_str(datetime.strptime(root_line['shp_date'], "%Y-%m-%dT%H:%M:%S.000000-0500"))
            responce_shipment_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.0%f-05:00")
            orderObj = {
                "shipments": [{
                    "alt_shipment_id": root_line['invoice'],  # Optional merchant supplied shipment ID.
                    "shipment_tracking_number": root_line['tracking_ref'],  # Packaging tracking number that the carrier is using
                    "response_shipment_date": responce_shipment_date,  # Date/Time that a given shipment was shipped
                    "carrier": root_line['carrier_code'].strip(" "),  # The carrier that will complete final delivery of the shipment.
                    "shipment_items": orderLines,
                }]
            }

        request.update({"data": json_dumps(orderObj)})
        return request

    def check_response(self, response):
        try:
            super(JetApiConfirmOrders, self).check_response(response)
        except Exception, e:
            if response.status_code == 400 and response.text:
                resp = json_loads(response.text)
                for error in resp.get('errors', []):
                    if 'Order is complete' in error:
                        return True
            raise e

        return True


class JetApiCancelOrders(JetApi):

    def __init__(self, settings, lines, state='cancel'):
        super(JetApiCancelOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        lines[0]['tracking_number'] = ''
        self.lines = lines
        self.state = state
        self.filename = "{:Cancel_%Y%m%d%H%M.xml}".format(datetime.now())
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(id=self.lines[0]['po_number'])

    @property
    def sub_request_kwargs(self):
        request = super(JetApiCancelOrders, self).sub_request_kwargs
        orderObj = {}

        if self.lines:
            root_line = self.lines[0]

            orderLines = []
            for line in self.lines:
                line['tracking_number'] = ''
                lineObj = {
                    'alt_shipment_item_id': line['external_customer_line_id'],  # Optional seller supplied ID for an item
                    'merchant_sku': line['merchantSKU'],  # The merchant SKU that is in the given shipment
                    'response_shipment_sku_quantity': 0,  # Quantity of the given SKU that was shipped in the given shipment
                    'response_shipment_cancel_qty': int(line['product_qty']),  # Quantity of the given SKU that was cancelled in the given shipment
                }
                orderLines.append(lineObj)

            orderObj = {
                'shipments': [{
                    'alt_shipment_id': root_line['invoice'],
                    'shipment_items': orderLines,
                }]
            }

        request.update({'data': json_dumps(orderObj)})
        return request

    def check_response(self, response):
        try:
            super(JetApiCancelOrders, self).check_response(response)
        except Exception, e:
            if response.status_code == 400 and response.text:
                resp = json_loads(response.text)
                for error in resp.get('errors', []):
                    if 'Order is complete' in error:
                        return True
            raise e

        return True


class JebApiGetTaxonomyLinks(JetApi):

    def __init__(self, settings, limit=1000, offset=0):
        super(JebApiGetTaxonomyLinks, self).__init__(settings)
        self.use_http_settings('auxiliary_1')
        self.params = [
            param('limit', limit),
            param('offset', offset)
        ]

    def process_response(self, response):
        response_data = super(JebApiGetTaxonomyLinks, self).process_response(response)
        urls = []
        if 'node_urls' in response_data:
            urls = response_data['node_urls']
        return urls


class JebApiGetTaxonomy(JetApi):

    def __init__(self, settings, url):
        super(JebApiGetTaxonomy, self).__init__(settings)
        self.use_http_settings('auxiliary_2')
        self.sub_url = url


class JetApiUploadFile(JetApi):
    def __init__(self, settings):
        super(JetApiUploadFile, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('upload_file')
        self.history = {}

    def upload_file(self, data):
        import gzip
        fgz = cStringIO.StringIO()
        gzip_obj = gzip.GzipFile(filename=self.filename_local, mode='wb', fileobj=fgz)
        gzip_obj.write(json_dumps(data))
        gzip_obj.flush()
        response = requests.put(
            self.file_token['url'],
            data=fgz.getvalue(),
            headers={"x-ms-blob-type": "BlockBlob"}
        )

        self.check_response(response)

    @property
    def sub_request_kwargs(self):
        request = super(JetApiUploadFile, self).sub_request_kwargs

        feed = self.prepare_feed()
        self.upload_file(feed)
        self.history = {
            'token': self.file_token,
            'feed': feed
        }

        request.update({
            "data": json_dumps({
                "url": self.file_token['url'],
                "file_type": self.file_type,
                "file_name": self.filename_local
            })
        })

        return request

    def check_response(self, response):
        super(JetApiUploadFile, self).check_response(response)
        if response and response.text:
            self.history['response'] = json_loads(response.text)
        self.backup_file(self.filename_local, json_dumps(self.history))

        return True


class JetApiUpdateMerchantSKU(JetApiUploadFile):

    def __init__(self, settings, lines):
        super(JetApiUpdateMerchantSKU, self).__init__(settings)
        self.settings = settings
        self.file_type = 'MerchantSKUs'
        self.lines = lines
        self.filename_local = f_d('jet_sku_%Y_%m_%d_%H_%M.json', datetime.utcnow())
        self.revision_lines = {
            'good': [],
            'bad': [],
        }
        self.variations = {}

    def prepare_feed(self):
        feed = {}
        main_field = self.settings['main_product_field']
        for line in self.lines:

            price = str2float(line['customer_price'], 0.0)

            if not (line[main_field] and price and line['upc']):
                self.append_to_revision_lines(line, 'bad')
                continue

            descr = check_none(line.get('long_description'))
            if not descr:
                descr = check_none(line.get('description'))

            if descr:
                descr = str_encode(descr[:1999])

            feed_line = {
                "brand": line.get('brand_name') or 'Mimi & Max',
                "product_title": str_encode(line.get('brand_title') or line.get('description') or '')[:499],
                "product_description": descr,
                'legal_disclaimer_description': str_encode(line['description'] or '')[:499],
                'manufacturer': 'Delmar Manufacturer',
                'mfr_part_number': str(line['customer_id_delmar']).strip(),
                'exclude_from_fee_adjustments': False,
                'fulfillment_time': 1,
                'prop_65': False,
                "multipack_quantity": 1,
                "number_units_for_price_per_unit": 1,
                "type_of_unit_for_price_per_unit": "each",
                "attributes_node_specific": [],
                }

            self.add_image(feed_line, line)
            self.add_alternate_images(feed_line, line)
            self.add_product_codes(feed_line, line)
            self.add_browse_node(feed_line, line)
            self.add_ring_size(feed_line, line)

            feed[line[main_field]] = feed_line
            self.append_to_revision_lines(line, 'good')

        return feed

    def add_browse_node(self, feed_line, values):
        browse_node_id = values.get('browse_node_id')
        if browse_node_id:
            feed_line["jet_browse_node_id"] = int(browse_node_id)

    def add_ring_size(self, feed_line, values):
        if values.get('size'):
            feed_line['attributes_node_specific'].append({
                "attribute_id": self.settings['ring_size_attribute_id'],
                "attribute_value": pretty_float(values['size'])
            })
            self.append_to_variations(values)

    def add_image(self, feed_line, values):
        image = values.get('imaged')
        if not image or image.lower() in ['false', 'none']:
            image = get_image_url(values.get('default_code', ''))
        feed_line["main_image_url"] = image

    def add_alternate_images(self, feed_line, values):
        alt_images = []
        for i in range(1, 9):
            alt_image = values.get('alternate_image_{}'.format(i))
            if alt_image and alt_image.lower() not in ['false', 'none']:
                alt_images.append({
                    "image_slot_id": i, "image_url": alt_image
                })

        if alt_images:
            feed_line['alternate_images'] = alt_images

    def add_product_codes(self, feed_line, values):
        product_codes = []
        if values.get('upc'):
            product_codes.append({
                "standard_product_code": values['upc'], "standard_product_code_type": "UPC"
            })
        if product_codes:
            feed_line['standard_product_codes'] = product_codes

    def append_to_variations(self, line):
        if not line.get('size'):
            return

        product_id = line['product_id']
        value = line[self.settings['main_product_field']]
        if not value:
            return

        if not self.variations.get(product_id):
            self.variations[product_id] = []

        self.variations[product_id].append(value)


class JetApiUpdatePrice(JetApiUploadFile):

    def __init__(self, settings, revision_lines):
        super(JetApiUpdatePrice, self).__init__(settings)
        self.settings = settings
        self.revision_lines = revision_lines
        self.file_type = 'Price'
        self.filename_local = f_d('jet_price_%Y_%m_%d_%H_%M.json', datetime.utcnow())

    def prepare_feed(self):
        feed = {}
        main_field = self.settings['main_product_field']
        for line in self.revision_lines.get('good'):

            price = str2float(line['customer_price'], 0.0)

            feed_line = {
                "price": price,
            }

            if self.fulfillment_nodes:
                nodes = []
                for node in self.fulfillment_nodes:
                    if node.get('jet_fulfillment_node_id'):
                        nodes.append({
                            "fulfillment_node_id": node['jet_fulfillment_node_id'],
                            "fulfillment_node_price": price
                        })
                if nodes:
                    feed_line.update({'fulfillment_nodes': nodes})

            feed[line[main_field]] = feed_line

        return feed


class JetApiUpdateQty(JetApiUploadFile):

    def __init__(self, settings, revision_lines):
        super(JetApiUpdateQty, self).__init__(settings)
        self.settings = settings
        self.revision_lines = revision_lines
        self.file_type = 'Inventory'
        self.filename_local = f_d('jet_qty_%Y_%m_%d_%H_%M.json', datetime.utcnow())

    def prepare_feed(self):

        feed = {}
        main_field = self.settings['main_product_field']
        nodes = [x['jet_fulfillment_node_id'] for x in self.fulfillment_nodes if x.get('jet_fulfillment_node_id')]
        if nodes:
            for line in self.revision_lines.get('good', []):

                line_nodes = [{

                    "fulfillment_node_id": node,
                    "quantity": int(line['qty'] or 0)
                } for node in nodes]

                feed[line[main_field]] = {
                    "fulfillment_nodes": line_nodes
                }

        return feed


class JetApiArchive(JetApiUploadFile):

    def __init__(self, settings, lines):
        super(JetApiArchive, self).__init__(settings)
        self.settings = settings
        self.lines = lines
        self.file_type = 'Archive'
        self.filename_local = f_d('jet_arc_%Y_%m_%d_%H_%M.json', datetime.utcnow())

    def prepare_feed(self):
        feed = {}

        main_field = self.settings.get('main_product_field') or 'customer_id_delmar'

        for line in self.lines:
            is_archived = False

            if line.get(main_field) and not line['upc']:
                is_archived = True

            feed[line[main_field]] = {
                "is_archived": is_archived
            }

        return feed


class JetApiUpdateVariations(JetApiUploadFile):

    def __init__(self, settings, variations):
        super(JetApiUpdateVariations, self).__init__(settings)
        self.settings = settings
        self.variations = variations or {}
        self.file_type = 'Variation'
        self.filename_local = f_d('jet_var_%Y_%m_%d_%H_%M.json', datetime.utcnow())

    def prepare_feed(self):

        feed = {
            min(variants): {
                "relationship": "Variation",
                "variation_refinements": [self.settings['ring_size_attribute_id']],
                "children_skus": variants
            }
            for item, variants in self.variations.items()
        }

        return feed


class JetOpenerp(ApiOpenerp):

    def __init__(self):
        super(JetOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        carrier = None

        if order['additional_fields']['order_detail']:
            # FIXME: extra json load (check yaml_orders_list in base_parser).
            #
            order_detail = json_loads(order['additional_fields']['order_detail'])

            carrier = order_detail.get('request_shipping_method', None) or "{service}__{level}".format(
                service=order_detail.get('request_shipping_carrier', None),
                level=order_detail.get('request_service_level', None)
            )

        return {
            'carrier': carrier
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'merchantSKU': line['merchantSKU'],
            'additional_fields': [],
            "qty": line['request_order_quantity']
        }

        for field, value in line.get('additional_fields', {}).iteritems():
            field_obj = (
                0, 0, {
                    'name': field.lower().replace(' ', '_'),
                    'label': field,
                    'value': value,
                    }
                )
            line_obj['additional_fields'].append(field_obj)

        product = False
        field_list = ['merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if size and size.id:
                    line_obj['size_id'] = size.id
                else:
                    line_obj['size_id'] = False

                if product:
                    line_obj["product_id"] = product.id
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price')
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj["notes"] = "Can't find product cost.\n"

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        lines = [{
            'order_item_id': x.external_customer_line_id,
            'order_item_acknowledgement_status': 'fulfillable',
        } for x in sale_order.order_line]
        orderObj = {
            'id': sale_order['po_number'],
            'order_items': lines,
        }

        return orderObj
