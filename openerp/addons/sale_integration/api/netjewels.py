# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from configparser import ConfigParser
from ftpapiclient import FtpClient
from utils import feedutils as feed_utils
from datetime import datetime
import logging
import os

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class NetJewelsApi(FtpClient):

    def __init__(self, settings):
        super(NetJewelsApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/netjewels_settings.ini' % (os.path.dirname(__file__)))


class NetJewelsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(NetJewelsApiClient, self).__init__(settings_variables, None, None, None)


    def updateQTY(self, lines, mode=None):
        updateApi = NetJewelsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class NetJewelsApiUpdateQTY(NetJewelsApi):

    def __init__(self, settings, lines):
        super(NetJewelsApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.lines = lines
        self.filename = "{:inventory_%Y%m%d%_H%M%S.csv}".format(datetime.now())
        self.filename_local = "{:inventory_%Y%m%d%_H%M%S.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        datas = ["Sku,Size,Qty    \n"]
        for line in self.lines:
            if not line['size']:
                line['size'] = ' '
            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line(',')
            if SkuRecord:
                datas.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(datas)[:-2]
