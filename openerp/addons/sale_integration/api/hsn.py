# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging
from utils.ediparser import EdiParser
import random
import json
import ast
from apiopenerp import ApiOpenerp
import time

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',          'Vendor number',         '423077'),
    ('vendor_name',            'Vendor name',           'Delmar MFG'),
    ('sender_id',              'Sender Id',             'delmar'),
    ('sender_id_qualifier',    'Sender Id Qualifier',   'ZZ'),
    ('receiver_id',            'Receiver Id',           '076902113T'),
    ('receiver_id_qualifier',  'Receiver Id Qualifier'  '01'),
    ('exchange_reasons',       'Exchange Reason Codes', '''{
        '01': 'Damaged',
        '02': 'Defective',
        '03': 'Not Received ',
        '04': 'Empty Box',
        '05': 'Damaged - No Return',
        '06': 'Wrong Size',
        '07': 'Wrong Color',
        '08': 'Damaged Package',
        '09': 'Damaged Package - No Return',
        '10': 'Defective - No Return',
        '11': 'Empty Box - Damaged Package',
        '12': 'Missing Quantity',
        '13': 'Missing Qty - Damaged Package',
        '14': 'Received Not Ordered/Received Wrong Item',
        '15': 'No Manifest - Not Received',
        '16': 'Missing Parts',
    }'''),
    ('edi_x12_version',             'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.edi'),
    ('line_terminator',             'Line Terminator',          r'~\n'),
    ('repetition_separator',        'Repetition Separator',     '}'),
    ('environment_mode',            'Environment Mode',         'P'),
)

# name: value
DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}


class HsnApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)

    def upload_data(self):
        return {
            'lines': self.lines,
            'vendor_number': self.vendor_number,
        }


class HsnQtyApi(FtpClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)


class HsnApiClient(AbsApiClient):

    settings = {
        "backup_local_path": "",
        "external_customer_id": ""
    }

    use_local_folder = True

    def __init__(self, settings_variables):
        super(HsnApiClient, self).__init__(
            settings_variables, HsnOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = HsnApiGetOrders

    def loadOrders(self, save_flag=False):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = HsnApiConfirmLoad(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def getOrdersObj(self, order_json):
        ordersApi = HsnApiGetOrderObj(order_json)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        ordersApi = HsnApiConfirmShipment(self.settings, lines)
        res = ordersApi.process('send')
        self.extend_log(ordersApi)
        return res

    def returnOrders(self, lines):
        ReturnApi = HsnApiReturnOrders(self.settings, lines)
        ReturnApi.process('send')
        self.extend_log(ReturnApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = HsnApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class HsnApiGetOrders(HsnApi):

    number_of_transaction = ''
    ack_control_number = ''

    def __init__(self, settings):
        super(HsnApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')

    def parse_response(self, data=None):
        if bool(data) is False:
            return []

        orders = []
        for edi in data:
            parse_orders = self.getOrderObj(edi)
            for order in parse_orders:
                orderObj = {
                    'name': order['poNumber'],
                    'ack_control_number': self.ack_control_number,
                    'number_of_transaction': self.number_of_transaction,
                    'functional_group': self.functional_group
                }
                if order['Cancellation'] is True:
                    orderObj['name'] = 'CANCEL ' + orderObj['name']

                orderObj['xml'] = json.dumps(order)
                orders.append(orderObj)
        return orders

    def getOrderObj(self, edi):
        if edi == '':
            return {}

        message = EdiParser(edi.strip())
        return_orders = []
        line_id = 1
        for segment in message:
            elements = segment.split(message.delimiters[1])

            if elements[0] == 'GS':
                self.ack_control_number = elements[6]
                self.functional_group = elements[1]

            elif elements[0] == 'ST':
                ordersObj = {
                    'address': {},
                    'partner_id': 'hsn',
                    'lines': [],
                    'Cancellation': False,
                    'ack_control_number': self.ack_control_number,
                }
                address_type = ''
                if elements[1] == '850':  # Order
                    pass
                elif elements[1] == '860':  # Change
                    pass
                elif elements[1] == '997':  # Aknowlegment skip this file
                    return return_orders

            elif elements[0] == 'BEG':  # Beginning Segment for Purchase Order
                ordersObj['poNumber'] = elements[3]
                ordersObj['order_id'] = elements[3]
                ordersObj['release'] = elements[4]
                ordersObj['external_date_order'] = elements[5]

                if len(elements) > 6:
                    address_type = 'order'
                    ordersObj['address']['order'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }

                    ordersObj['address']['order']['name'] = ' '.join(elements[6].split())

            elif elements[0] == 'REF':  # Reference Identification

                if elements[1] == 'IT':
                    ordersObj['order_reference_number'] = elements[2]

                elif elements[1] == 'OP':
                    ordersObj['original_order_number'] = elements[2]
                    ordersObj['reason_code'] = elements[3]
                    try:
                        exchange_reason = ast.literal_eval(self.exchange_reasons).get(ordersObj['reason_code'], False)
                        if exchange_reason:
                            ordersObj['reason_str'] = exchange_reason
                    except Exception:
                        _logger.warning('Can\'t parse exchange reasons')
                    ordersObj['note'] = 'Exchange %s with reason "%s"' % (
                        ordersObj['original_order_number'],
                        ordersObj.get('reason_str', ordersObj['reason_code'])
                    )

            elif elements[0] == 'PO1':

                lineObj = {
                    'qty': elements[2],
                    'cost': elements[4],
                    'name': ''
                }

                if elements[6]:

                    if elements[6] == 'UP':
                        lineObj['upc'] = elements[7]

                    elif elements[6] == 'VN':
                        lineObj['vendorSku'] = elements[7]

                if elements[8]:

                    if elements[8] == 'IT':
                        lineObj['merchantSKU'] = lineObj['customer_sku'] = elements[9]

                if elements[1]:
                    lineObj['id'] = elements[1]

                elif not elements[1]:
                    lineObj['id'] = line_id
                    line_id += 1

                ordersObj['lines'].append(lineObj)

            elif elements[0] == 'CTP':

                if elements[3] != '0':
                    customer_cost = float('%s.%s' % (elements[3][:-2], elements[3][-2:]))
                else:
                    customer_cost = 0.0
                ordersObj['lines'][len(ordersObj['lines']) - 1]['customerCost'] = customer_cost

            elif elements[0] == 'PID':

                if elements[1] == 'F':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['name'] = ' '.join(elements[5].split())

            elif elements[0] == 'SAC':
                sac_element = float('%s.%s' % (elements[5][:-2], elements[5][-2:]))

                if elements[2] == 'G830':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['postage_handling'] = sac_element

                elif elements[2] == 'H850':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['tax'] = sac_element

                elif elements[2] == 'C310':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['item_discount'] = sac_element

                elif elements[2] == 'I131':

                    if elements[5] != '0':
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['line_total'] = sac_element

                    if elements[15]:
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['payment_type'] = elements[15]

            elif elements[0] == 'TD5':  # Carrier Details
                ordersObj['carrier'] = elements[3]
                ordersObj['carrier_description'] = elements[5]

            elif elements[0] == 'N1':
                ordersObj['address']['ship'] = {
                    'name': '',
                    'name2': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'zip': '',
                    'country': '',
                    'phone': '',
                    'email': ''
                }

                if elements[1] == 'ST':
                    address_type = 'ship'
                    ordersObj['address']['ship']['name'] = ' '.join(elements[2].split())

            elif elements[0] == 'N3' and address_type != '':
                ordersObj['address'][address_type]['address1'] = elements[1]

                if len(elements) > 2:
                    ordersObj['address'][address_type]['address2'] = elements[2]

            elif elements[0] == 'N4' and address_type != '':
                ordersObj['address'][address_type]['city'] = elements[1]
                ordersObj['address'][address_type]['state'] = elements[2]
                ordersObj['address'][address_type]['zip'] = elements[3]
                ordersObj['address'][address_type]['country'] = 'US'

                if elements[4]:
                    ordersObj['address'][address_type]['country'] = elements[4]
                ordersObj['address'][address_type]['phone'] = elements[6]

                if not ordersObj['address'].get('order', False):
                    ordersObj['address']['order'] = ordersObj['address'][address_type]

            elif elements[0] == 'GE':
                self.number_of_transaction = elements[1]

            elif elements[0] == 'MSG':

                if 'gift_header' not in ordersObj:
                    ordersObj['gift_header'] = ' '.join(elements[1].split()) or ' '

                else:
                    ordersObj['gift_message'] = elements[1] or ''

            elif elements[0] == 'SE':
                return_orders.append(ordersObj)
                line_id = 1

        return return_orders


class HsnApiConfirmLoad(HsnApi):

    orders = []

    def __init__(self, settings, orders):
        super(HsnApiConfirmLoad, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        for order in self.orders:
            order_data = json.loads(order.get("xml", "{}"))
            if order.get('ack_control_number') and order['ack_control_number'] not in numbers:
                numbers.append(order_data['ack_control_number'])
                segments = []
                # SEGMENT ST - TRANSACTION SET HEADER
                st = 'ST*[1]%(code)s*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'code': self.edi_type,
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))

                # SEGMENT AK1 – FUNCTIONAL GROUP RESPONSE HEADER
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order_data['ack_control_number']
                }))

                # HsnApiConfirmLoad
                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))

                # SEGMENT AK9 FUNCTIONAL GROUP TRAILER
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))

        return data


class HsnApiGetOrderObj():

    order_json = ''

    def __init__(self, order_json):
        self.order_json = order_json

    def getOrderObj(self):
        if bool(self.order_json) is False:
            return {}

        return json.loads(self.order_json)


class HsnApiConfirmShipment(HsnApi):

    confirmLines = []

    def __init__(self, settings, lines):
        super(HsnApiConfirmShipment, self).__init__(settings)
        self.confirmLines = lines
        self.use_ftp_settings('confirm_shipment')
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }
        segments.append(self.insertToStr(st, st_data))

        # SEGMENT BSN - BEGINNING SEGMENT FOR SHIP NOTICE
        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.now()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '00',
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        # SEGMENT HL - HIERARCHICAL LEVEL
        hl_number = 0
        hl = 'HL*[1]%(hl_number)s*[2]*[3]%(code)s'

        # SEGMENT LIN – ITEM IDENTIFICATION
        # 005: Y = Ship to Zip code exists in DAS table, N = Ship to Zip code does not exist in DAS tabl
        lin = 'LIN*[1]*[2]IT*[3]%(merchantSKU)s*[4]TW*[5]Y'

        # SEGMENT PRF - PURCHASE ORDER REFERENCE
        prf = 'PRF*[1]%(po_number)s'

        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'code': 'I'  # Item
            }))

            segments.append(self.insertToStr(lin, {
                'merchantSKU': line['merchantSKU']
            }))

            segments.append(self.insertToStr(prf, {
                'po_number': self.confirmLines[0]['po_number']
            }))

        # SEGMENT REF*CN – ORDER TRACKING NUMBER
        ref_cn = 'REF*[1]CN*[2]%(data)s*[3]%(ups_zip_code)s*[4]TIP*[5]%(weight)s*[6]TDT*[7]%(dimensional_weight)s'
        segments.append(self.insertToStr(ref_cn, {
            'data': self.confirmLines[0]['tracking_number'],
            'ups_zip_code': '',  # TODO
            'weight': self.confirmLines[0]['weight'] or 1,
            'dimensional_weight': self.confirmLines[0]['weight'] or 1,
        }))

        # SEGMENT REF*IV – VENDOR INVOICE NUMBER
        ref_iv = 'REF*[1]IV*[2]%(invoice)s*'
        segments.append(self.insertToStr(ref_iv, {
            'invoice': self.confirmLines[0]['invoice'],
        }))

        # SEGMENT DTM - DATE/TIME REFERENCE
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        #SEGMENT N1 - NAME
        n1 = 'N1*[1]VN*[2]%(vendor_name)s*[3]92*[4]%(vendor_number)s'
        segments.append(self.insertToStr(n1, {
            'vendor_name': self.vendor_name,
            'vendor_number': self.vendor_number,
        }))

        # SEGMENT SAC*G830 – HANDLING ALLOWANCE
        sac = 'SAC*[1]C*[2]G830*[3]*[4]*[5]%(amount_total)s'

        segments.append(self.insertToStr(sac, {
            # HOTFIX: need to check this
            'amount_total': '000'
        }))

        # SEGMENT CTT - TRANSACTION TOTALS
        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        # SEGMENT SE - TRANSACTION SET TRAILER
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class HsnApiInvoice(HsnApi):

    invoiceLines = []

    def __init__(self, settings, lines):
        super(HsnApiInvoice, self).__init__(settings)
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        # SEGMENT ST - TRANSACTION SET HEADER
        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': self.edi_type,
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        # SEGMENT BIG - BEGINNING SEGMENT FOR STANDARD INVOICES
        bia = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]*[4]%(po_number)s'
        invoice_date = datetime.now()
        segments.append(self.insertToStr(bia, {
            'date': invoice_date.strftime('%Y%m%d'),
            'po_number': self.invoiceLines[0]['po_number']  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
        }))

        # SEGMENT N1 - NAME
        n1 = 'N1*[1]%(entity_code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'
        segments.append(self.insertToStr(n1, {
            'entity_code': 'CT',
            'name': 'Jewelry',
            'identification_code_qualifier': '92',
            'identification_code': '99',
        }))

        segments.append(self.insertToStr(n1, {
            'entity_code': 'SU',
            'name': self.vendor_name,
            'identification_code_qualifier': '92',
            'identification_code': self.vendor_number,
        }))

        # SEGMENT ITD – TERMS OF SALE
        itd = 'ITD*[1]%(terms_type_code)s*[2]%(terms_date_code)s*[3]%(terms_discount_percent)s*[4]*[5]%(terms_discount_days_due)s*[6]*[7]%(terms_net_days)s*[8]%(terms_discount_amount)s'
        segments.append(self.insertToStr(itd, {
            'terms_type_code': 'ZZ',
            'terms_date_code': '2',  # (1=shipdate, 2=delivery date, 3=invoice date)
            'terms_discount_percent': 'NEED_DATA',  # Terms discount percentage, expressed as a percent, available to the purchaser if an invoice is paid on or before the Terms Discount Due Date
            'terms_discount_days_due': 'NEED_DATA',  # Number of days in the terms discount period by which payment is due if terms discount is earned
            'terms_net_days': 'NEED_DATA',  # Number of days until total invoice amount is due (discount not applicable)
            'terms_discount_amount': 'NEED_DATA',  # Total amount of terms discount
        }))

        # SEGMENT DTM - DATE/TIME REFERENCE
        dtm = 'DTM*[1]011*[2]%(shipping_date)s'
        segments.append(self.insertToStr(dtm, {
            'shipping_date': invoice_date.strftime('%Y%m%d'),
        }))

        # SEGMENT IT1 - BASELINE ITEM DETAIL (INVOICE)
        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(product_qualifier)s*[7]%(vendorSku)s'

        # SEGMENT PID - PRODUCT/ITEM DESCRIPTION
        pid = 'PID*[1]F*[2]*[3]*[4]*[5]%(description)s'

        # SEGMENT REF – REFERENCE NUMBERS
        ref = 'REF*[1]BV*[2]%(line_number)s'

        # ctp = 'CTP*[1]*[2]*[3]*[4]*[5]*[6]*[7]*[8]%(line_price)s'
        line_number = 0
        for line in self.invoiceLines:
            line_number += 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(line['qty']),
                'unit_price': line['unit_price'],
                'product_qualifier': 'VA',  # [6] “ VA “ or “ UP ” (Vendor’s Style Number or UPC code)
                'vendorSku': line['vendorSku'],
            }))

            segments.append(self.insertToStr(pid, {
                'description': line['name'],
            }))

            segments.append(self.insertToStr(ref, {
                'line_number': str(line_number),
            }))

        # SEGMENT CAD – CARRIER DETAIL
        cad = 'CAD*[1]L*[2]*[3]*[4]*[5]%(carrier)s'
        segments.append(self.insertToStr(cad, {
            'carrier': self.confirmLines[0]['carrier_code'],
        }))

        # SEGMENT SAC - ALLOWANCE, CHARGE, OR SERVICE
        sac = 'SAC*[1]C*[2]D240*[3]*[4]*[5]%(total_amount_with_descount)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15] FREIGHT'
        segments.append(self.insertToStr(sac, {
            'total_amount_with_descount': self.invoiceLines[0]['total_amount']
        }))

        ctt = 'CTT*[1]%(line_number)s'
        segments.append(self.insertToStr(ctt, {
            'line_number': line_number
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class HsnApiReturnOrders(HsnApi):

    lines = []

    def __init__(self, settings, lines):
        super(HsnApiReturnOrders, self).__init__(settings)
        self.lines = lines
        self.use_ftp_settings('return_orders')
        self.edi_type = '812'

    def upload_data(self):
        segments = []
        date_now = datetime.now()

        # SEGMENT ST - TRANSACTION SET HEADER
        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }
        segments.append(self.insertToStr(st, st_data))

        # SEGMENT BCD - BEGINNING CREDIT/DEBIT ADJUSTMENT
        bcd = 'BCD*[1]%(return_date)s*[2]%(credit_number)s*[3]L*[4]%(amount)s*[5]C*[6]*[7]*[8]*[9]*[10]%(po_number)s'

        # SEGMENT N1 - NAME
        n1 = 'N1*[1]%(entity_code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'

        # SEGMENT CDD - CREDIT/DEBIT ADJUSTMENT DETAIL
        # CDD 001 ADJUSTMENT REASON CODE
        # HSN return reason code
        # Valid reason codes:
        # 03 - defective
        # 07 - wrong item/size shipped
        # 08 - item failed after use
        # 16 - Missing parts
        # 17 – No reason indicated
        # 21 - color not as shown
        # 22 - Just did not like
        # 24 - difficult to operate/assemble
        # 35 - price/value
        # 36 - damaged in shipping
        # 46 - Refused/Returned to       Customer –  too old
        # 82 - Product durability
        cdd = 'CDD*[1]%(return_reason)s*[2]*[3]*[4]*[5]*[6]*[7]%(return_qty)s*[8]EA*[9]*[10]SLP*[11]%(unit_price)s'

        # SEGMENT LIN – ITEM IDENTIFICATION
        lin = 'LIN*[1]*[2]IT*[3]%(item_number)s'

        amount = 0.0
        for line in self.lines:
            amount += float(line['ItemPrice']) * line['product_qty']

        for line in self.lines:

            segments.append(self.insertToStr(bcd, {
                'return_date': date_now.strftime('%Y%m%d'),
                'credit_number': line['external_customer_order_id'],
                'amount': str(int(amount * 100)),
                'po_number': line['po_number'],
            }))

            segments.append(self.insertToStr(n1, {
                'entity_code': 'SU',
                'name': self.vendor_name,
                'identification_code_qualifier': '92',
                'identification_code': self.vendor_number,
            }))

            segments.append(self.insertToStr(cdd, {
                'return_reason': line.get('return_reason', False) or '17',
                'return_qty': int(line['product_uos_qty']),
                'unit_price': line['ItemPrice'],
            }))

            segments.append(self.insertToStr(lin, {
                'item_number': line['merchantSKU'],
            }))

        # SEGMENT AK9 FUNCTIONAL GROUP TRAILER
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CD'})


class HsnApiUpdateQTY(HsnQtyApi):

    def __init__(self, settings, lines):
        super(HsnApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    @property
    def filename(self):
        if (
            hasattr(self, '_filename') and
            self._filename
        ):
            pass
        else:
            self._filename = "{:ProductQuantities-%Y-%m-%d-%H%M%S.xml}".format(datetime.now())
        return self._filename

    def prepare_data(self, data):
        """
            Prepare data before send
        """

        data['vendor_number'] = self.vendor_number
        data['date'] = time.strftime("%d/%m/%Y %I:%M:%S %p")

        return super(HsnApiUpdateQTY, self).prepare_data(data)

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if not line.get('customer_id_delmar', False):
                self.append_to_revision_lines(line, 'bad')
                continue
            self.append_to_revision_lines(line, 'good')

        return {'lines': self.revision_lines['good']}


class HsnOpenerp(ApiOpenerp):

    def __init__(self):
        super(HsnOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields': [
                (0, 0, {'name': 'carrier_description', 'label': 'Carrier Description',
                        'value': order.get('carrier_description', '')}),
                (0, 0, {'name': 'release', 'label': 'Release #',
                        'value': order.get('release', '')}),
                (0, 0, {'name': 'reason_code', 'label': 'Reason Code',
                        'value': order.get('reason_code', '')}),
                (0, 0, {'name': 'reason_str', 'label': 'Reason String',
                        'value': order.get('reason_string', '')}),
                (0, 0, {'name': 'gift_header', 'label': 'Gift Message Header',
                        'value': order.get('gift_header', '')}),
                (0, 0, {'name': 'gift_message', 'label': 'Gift Message',
                        'value': order.get('gift_message', '')}),
                (0, 0, {'name': 'order_reference_number', 'label': 'Order Reference Number',
                        'value': order.get('order_reference_number', '')}),
            ]
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = {
            "notes": "",
            "name": line.get('name', ''),
            'cost': line.get('cost', ''),
            'customer_sku': line.get('customer_sku', ''),
            'upc': line.get('upc', ''),
            'vendorSku': line.get('vendorSku', ''),
            'merchantSKU': line.get('merchantSKU', ''),
            'qty': line.get('qty', ''),
            'additional_fields': [
                (0, 0, {'name': 'line_total', 'label': 'Line Total',
                        'value': line.get('line_total', '')}),
                (0, 0, {'name': 'postage_handling', 'label': 'Postage Handling',
                        'value': line.get('postage_handling', '')}),
                (0, 0, {'name': 'item_discount', 'label': 'Item Discount',
                        'value': line.get('item_discount', '')}),
                (0, 0, {'name': 'payment_type', 'label': 'Payment Type',
                        'value': line.get('payment_type', '')})
            ]
        }

        if 'customerCost' in line:
            line_obj['customerCost'] = line.get('customerCost', 0.0)

        product = False
        field_list = ['vendorSku', 'merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[field],
                                                                                      ('default_code', 'customer_sku', 'customer_id_delmar', 'upc')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line_obj['sku'])
            line_obj["product_id"] = 0

        return line_obj
