# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class RossSimonsApi(FtpClient):

    def __init__(self, settings):
        super(RossSimonsApi, self).__init__(settings)


class RossSimonsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(RossSimonsApiClient, self).__init__(settings_variables, None, None, None)

    def updateQTY(self, lines, mode=None):
        updateApi = RossSimonsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class RossSimonsApiUpdateQTY(RossSimonsApi):

    def __init__(self, settings, lines):
        super(RossSimonsApiUpdateQTY, self).__init__(settings)
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'], "generic_ftp_inv/%s" % (settings['customer'] or 'common'), "inventory", "now"
        ) or ""
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "{:inventory_%Y%m%d%_H%M%S.csv}".format(datetime.now())
        self.filename_local = "{:inventory_%Y%m%d%_H%M%S.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        for line in self.lines:
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}
