# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.rhd_edi_parser import EDIParser
import random
from datetime import datetime
from apiopenerp import ApiOpenerp
import logging
from dateutil import tz
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from datetime import datetime
from utils.ediparser import check_none
import re

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '7011358'),
    ('vendor_name',                 'Vendor name',              'BTS-SENDER'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier', 'Sender Id Qualifier', 'ZZ'),
    ('receiver_id', 'Receiver Id', '7086797400'),
    ('receiver_id_qualifier', 'Receiver Id Qualifier', '12'),
    ('contact_name',                'Contact name',             ''),
    ('contact_phone',               'Contact phone',            ''),
    ('edi_x12_version',             'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'Delmar_{edi_type}_{date}.txt'),
    ('line_terminator',             'Line Terminator',          r'~\r\n'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      'U'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}

# DLMR-2075
ex_long = re.compile('^[0]{7}\.\d{6,7}$')
ex_short = re.compile('^0\.\d{6,7}$')


# DLMR-2075
def switch_zeroed_sku(sku=None):
    new_value = None
    # check is long or short
    if ex_long.match(sku):
        # check length after dot
        [zr, dg] = str(sku).split('.', 2)
        cut_len = len(dg) + 2
        # remove zeroes
        new_value = sku[-cut_len:]
    elif ex_short.match(sku):
        new_value = '{}{}'.format('0' * 6, sku)
    return new_value


# DLMR-2075
def format_zeroed_sku(sku=None, to_long=True):
    new_value = None
    # check is long or short
    if ex_long.match(sku):
        # check length after dot
        [zr, dg] = str(sku).split('.', 2)
        cut_len = len(dg) + 2
        new_value = sku if to_long else sku[-cut_len:]
    elif ex_short.match(sku):
        new_value = '{}{}'.format('0' * 6, sku) if to_long else sku
    return new_value


class RhdApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class RhdApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(RhdApiClient, self).__init__(
            settings_variables, RhdOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = RhdApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)

        return orders

    def confirmLoad(self, orders):
        api = RhdApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        confirmApi = RhdApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = RhdApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = RhdApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)

        return updateApi.revision_lines

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = RhdApiProcessingOrderLines(self.settings, lines, state)
        res = ordersApi.process('send')
        self.extend_log(ordersApi)

        return res


class RhdApiGetOrders(RhdApi):

    def __init__(self, settings):
        super(RhdApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'

class RhdOpenerp(ApiOpenerp):

    def __init__(self):

        super(RhdOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line['name'],
            'sku': line['sku'],
            'customer_sku': line['customer_sku'],
            'upc': line.get('upc',''),
            'cost': line['cost'],
            'customerCost': line['cost'],
            'qty': line['qty'],
            'optionSku': line.get('optionSku', ''),
            'vendorSku': line['vendorSku'],
            'color': line.get('color', ''),
	    'customer_id_delmar': line.get('customer_id_delmar', ''),
            'line_id':line['id'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = {}
        field_list = ['sku', 'customer_sku', 'upc']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                _logger.info("Default search field: {}, value: {}".format(field, line[field]))
                # DLMR-2075
                # Check size
                product_search_context = {}
                if line.get('size', False):
                    try:
                        size_ids = self.pool.get('ring.size').search(cr, uid, [
                            ('name', '=', str(float(line['size'])))])
                    except Exception:
                        size_ids = []
                    if len(size_ids) > 0:
                        product_search_context.update({'use_size': size_ids[0]})
                _logger.info("Product search context: %s" % product_search_context)
                # END DLMR-2075
                # Try to find product by default
                product, size = self.pool.get('product.product').\
                    search_product_by_id(cr, uid, context['customer_id'], line[field], ('default_code', 'customer_sku', 'upc'), product_search_context)
                # Re-format customer sku leading zeroes
                if not product:
                    new_value = switch_zeroed_sku(line.get(field))
                    # got new value
                    if new_value:
                        _logger.info("Zeroed search field: {}, value: {}".format(field, new_value))
                        # check product again
                        product, size = self.pool.get('product.product').\
                            search_product_by_id(cr, uid, context['customer_id'], new_value, ('default_code', 'customer_sku', 'upc'), product_search_context)
                # END DLMR-2075

                # check size if product found
                if product:

                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False

                    if not line.get('price_unit'):
                        retail = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                   context['customer_id'],
                                                                                   'Retail Price',
                                                                                   line_obj.get('size_id', False))
                        if retail:
                            line_obj['customerCost'] = retail
                        else:
                            line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        country_obj = self.pool.get('res.country')
        state_obj = self.pool.get('res.country.state')
        tax_obj = self.pool.get("delmar.sale.taxes")
        raw_taxes = {}
        line = {
            'address_services': {
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                    'province_code': ''
                },
            }
        }
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })
        country_id = None
        country_ids = country_obj.search(cr, uid, [('name', '=', 'Canada')])
        if country_ids:
            country_id = country_ids[0]
        state_ids = state_obj.search(cr, uid, [
            ('country_id', '=', country_id),
            ('name', 'in', [u'Quebec', u'Quèbec'])
        ])
        if country_id and state_ids:
            raw_taxes = tax_obj.get_raw_taxes(cr, uid, country_id, state_ids)
        line.update({'raw_taxes': raw_taxes})
        deliver_obj = self.pool.get("stock.picking").browse(cr, uid, deliver_id)
        shp_handling = deliver_obj.shp_handling or 0.0
        line.update({'shp_handling': shp_handling})
        return line

    def get_additional_processing_information(self, cr, uid, settings, picking, order_line, context=None):
        upc = self.pool.get('product.product').get_val_by_label(
            cr,
            uid,
            order_line.product_id.id,
            order_line.sale_line_id.order_partner_id.id,
            'UPC',
            order_line.size_id.id or False,
        )
        result = {'upc': upc}
        return result

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        lineObj = {
            'po_number': sale_order['po_number'],
            'cust_order_number': sale_order.cust_order_number or sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR'
        }
        if vendor_address is not False:
            lineObj['address1'] = vendor_address.street
            lineObj['city'] = vendor_address.city
            lineObj['state'] = vendor_address.state_id.code
            lineObj['zip'] = vendor_address.zip
            lineObj['country'] = vendor_address.country_id.code

        result = []

        if (not sale_order.sale_integration_xml_id):
            return result

        for order_line in sale_order.order_line:
            line = lineObj.copy()
            line['vendorSku'] = order_line.vendorSku
            line['product_qty'] = int(order_line.product_uom_qty)
            line['external_customer_line_id'] = order_line.external_customer_line_id
            result.append(line)

        return result

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        line = {}
        if (not sale_obj.sale_integration_xml_id):
            raise ValueError('Not have sale_integration_xml_id')
        return line

class RhdApiProcessingOrderLines(RhdApi):
    processingLines = []

    def __init__(self, settings, lines, state):
        super(RhdApiProcessingOrderLines, self).__init__(settings)
        self.processingLines = lines
        self.state = state
        self.use_ftp_settings('acknowledgement')

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            return self.accept_cancel()
        self.edi_type = '855'

        lines = []

        statuscode = 'AK'
        if self.state == 'accepted':
            statuscode = 'AK'  # conveys that you are acknowledging the entire purchase order and there are no changes.
            lines = self.processingLines
            self.processingLines = self.processingLines[0]
        elif self.state == 'cancel_line':
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.
        elif self.state == 'cancel':
            lines = self.processingLines
            self.processingLines = self.processingLines[0]
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.

        segments = []
        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'

        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.now().strftime('%Y%m%d')

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(vendor_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'vendor_number': self.vendor_number
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]*[4]*[5]*[6]SK*[7]%(vendorSku)s"
        ack_cancel = "ACK*[1]%(line_status)s*[2]*[3]*[4]*[5]%(date)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]*[16]*[17]*[18]*[19]*[20]*[21]*[22]*[23]*[24]*[25]*[26]*[27]*[28]*[29]%(cancel_code)s"
        ack = "ACK*[1]%(line_status)s*[2]%(qty)s*[3]EA"
        date_now = datetime.now()
        i = 0
	line_number = 0
        for (i, line) in enumerate(lines):
	    line_number += 1
            insert_obj = {
                'line_number': line_number,
                'qty': int(line['product_qty']),
                'vendorSku': line['vendorSku'] or '',
            }

            segments.append(self.insertToStr(po1, insert_obj))
            if self.state == 'accepted':
                segments.append(self.insertToStr(ack, {
                    'line_status': 'IA',  # Item Accepted
                    'qty': int(line['product_qty'])
                }))
            else:
                segments.append(self.insertToStr(ack_cancel, {
                    'line_status': 'IR',  # Item Rejected
                    'date': date_now.strftime('%Y%m%d'),   # CCYYMMDD
                    'cancel_code': line['cancel_code'],   # CCYYMMDD
                }))
        if len(lines) > 0:
            ctt = 'CTT*[1]%(count_po1)s'
            segments.append(self.insertToStr(ctt, {
                'count_po1': i + 1
            }))
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PO'})

    def accept_cancel(self):
        self.edi_type = '865'
        segments = []
        statuscode = 'AC'
        st = 'ST*[1]865*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA*[1]01*[2]%(statuscode)s*[3]%(po_number)s*[4]*[5]*[6]%(po_date)s'

        date = self.processingLines[0].get('order_date', False) or datetime.utcnow()
        if(isinstance(date, (str, unicode))):
            try:
                date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                date = datetime.now()

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'order_number': self.vendor_number
        }))

        cust_order_number = self.processingLines[0].get('cust_order_number', '')
        if not cust_order_number:
            cust_order_number = self.processingLines[0].get('order_additional_fields', {}).get('cust_order_number', '')
        segments.append(self.insertToStr(ref, {
            'code': 'OQ',  # Order Number
            'order_number': cust_order_number
        }))

        for line in self.processingLines:
            poc = "POC*[1]%(external_customer_line_id)s*[2]DI*[3]%(ordered_qty)s*[4]%(left_to_receive)s*[5]EA*[6]*[7]*[8]SK*[9]7500014628*[10]IN*[11]%(optionSku)s*[12]UP*[13]%(merchantSKU)s"
            segments.append(self.insertToStr(poc, {
                'external_customer_line_id': str(line['external_customer_line_id']),
                'ordered_qty': str(int(line['ordered_qty'])),
                'left_to_receive': str(int(line['left_to_receive'])),
                'optionSku': line['optionSku'],
                'merchantSKU': line['merchantSKU'],
            }))

            ack = "ACK*[1]%(item_status_code)s"
            item_status_code = 'ID'
            if self.state == 'cancel':
                item_status_code = 'IR'
                if line.get('cancel_code'):
                    ack += "*[2]*[3]*[4]*[5]*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]*[16]*[17]*[18]*[19]*[20]*[21]**[22]*[23]*[24]*[25]*[26]*[27]*[28]*[29]{cancel_code}".format(cancel_code=line['cancel_code'])

            segments.append(self.insertToStr(ack, {
                'item_status_code': item_status_code,
            }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PO'})


class RhdApiUpdateQTY(RhdApi):
    """EDI/V4010 X12/846: 846 Inventory Inquiry/Advice"""

    def __init__(self, settings, lines):
        super(RhdApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        aggregated_lines = []
        sortedlines = sorted(self.updateLines, key=lambda key: key['customer_sku'])
        for line in sortedlines:
            if not aggregated_lines or aggregated_lines[-1]['customer_sku'] != line['customer_sku']:
                aggregated_lines.append(line)
            else:
                aggregated_lines[-1]['qty'] += line['qty']

        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]%(purpose_code)s*[2]MB*[3]%(inventory_feed_qualifier)s*[4]%(date)s*[5]%(time)s*[6]%(action_code)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'purpose_code': '04',
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'date': date.strftime('%Y%m%d'),
            'time': date.strftime('%H%M'),
            'action_code': '2',
        }))

        lin = 'LIN*[1]%(id)s*[2]%(product_qualifier)s*[3]%(product_identifying_number)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        line_id = 0
        for line in aggregated_lines:
            if not line.get('customer_sku', False):
                self.append_to_revision_lines(line, 'bad')
                continue

            line_id += 1
            segments.append(self.insertToStr(lin, {
                'product_qualifier': 'UP',
                'product_identifying_number': format_zeroed_sku(line['customer_sku'], to_long=True),    # DLMR-2075
                'id': line_id
            }))

            segments.append(self.insertToStr(qty, {
                'code': '17',
                'qty': str(line['qty']),
                'uof_code': 'EA'
            }))

            self.append_to_revision_lines(line, 'good')

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PO'})

class RhdApiInvoiceOrders(RhdApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(RhdApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        now_dtm = datetime.utcnow()
        invoice_number = check_none(self.invoiceLines[0]['invoice']).replace('RHD', '') + now_dtm.strftime('.%H%M%S')

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': invoice_number,
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        it1 = 'IT1*' \
              '[1]%(line_number)s*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]%(unit_price)s*' \
              '[5]*' \
              '[6]*' \
              '[7]%(invoice)s'

        line_number = 0
        total_qty_invoiced = 0
        total_price_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = line['product_qty']
            except ValueError:
                qty = 1

            product_identifying_number = ''
	    
            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': int(qty),
                'unit_price': line['unit_cost'],
                'product_identifying_number': str(product_identifying_number),
                'style_number': line.get('customer_id_delmar', ''),
                'invoice': line.get('customer_id_delmar', '') or line.get('vendorSku','') 
            }))

            total_qty_invoiced += qty
            total_price_invoiced += qty * float(line['unit_cost'])

        tds = 'TDS*[1]%(total_amount)s'

        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(total_price_invoiced or 0.0)).replace('.', ''),
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PO'})

class RhdApiConfirmShipment(RhdApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(RhdApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(po)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]%(str_code)s'

        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        str_code = '0001'
        if self.confirmLines[0]['action'] == 'v_cancel':
            str_code = '0004'
        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'po': self.confirmLines[0]['po_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time,
            'str_code': str_code,
        }

        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),
            'hl_number_prev': str(''),
            'code': 'S'

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]B*[2]*[3]*[4]*[5]%(carrier)s'

        # todo carrier_code
        td5_data = {
            'carrier': self.confirmLines[0].get('carrier_code', '')
        }
        segments.append(self.insertToStr(td5, td5_data))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))


        if self.confirmLines[0]['action'] == 'v_ship':
            hl_number_prev = hl_number
            hl_number += 1
            hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'P'  # Order
            }))
            man = 'MAN*[1]CP*[2]%(track)s'
            segments.append(self.insertToStr(man, {
                'track': self.confirmLines[0]['tracking_number'],
            }))


        lin = 'LIN*' \
              '[1]%(line_id)s*' \
              '[2]VN*' \
              '[3]%(sku)s*' \
              '[4]UP*' \
              '[5]%(upc)s'

        item_statuses = ['AC', 'ID']

        sn1 = 'SN1*' \
              '[1]*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]*' \
              '[5]%(qty_ordered)s*' \
              '[6]%(man_code)s*' \
              '[7]*' \
              '[8]%(item_status)s'

        if self.confirmLines[0]['action'] == 'v_cancel':
            sn1 = 'SN1*' \
                  '[1]*' \
                  '[2]%(qty)s*' \
                  '[3]EA*' \
                  '[4]*' \
                  '[5]%(qty_cancel)s*' \
                  '[6]%(man_code)s*' \
                  '[7]*' \
                  '[8]%(item_status)s'

        hl_number_prev = hl_number
	line_id = 0
        for line in self.confirmLines:
	    line_id +=1
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            product_qualifier = None
            product_identifying_number = line.get('order_additional_fields',{}).get('internal_vendor_number','')
            segments.append(self.insertToStr(lin, {
                'line_id': line_id,
                'product_qualifier': product_qualifier,

                'upc': line.get('upc', ''),
                'sku': line.get('customer_id_delmar', '') or line.get('vendorSku',''),

                'customer_id_delmar': line.get('customer_id_delmar', ''),
                'merchantSKU': line['merchantSKU'],
            }))
            if line['action'] == 'v_cancel':
                segments.append(self.insertToStr(sn1, {
                    'qty': 0,
                    'qty_cancel': line['product_qty'],
                    'man_code': 'EA',
                    'item_status': 'ID',
                }))
            else:
                segments.append(self.insertToStr(sn1, {
                    'qty': line['product_qty'],
                    'qty_ordered': '',
                    'man_code': '',
                    'item_status': 'AC',
                }))

            ref = 'REF*[1]CN*[2]%(sku)s*[3]%(data)s'
            segments.append(self.insertToStr(ref, {
                'sku':  line.get('customer_id_delmar', '') or line['vendorSku'] or '',
                'data': line['order_additional_fields'] and
                        line['order_additional_fields'].get('internal_vendor_number', None) or
                        self.vendor_number
            }))
	    print segments


        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PO'})

class RhdApiFunctionalAcknoledgment(RhdApi):
    orders = []

    def __init__(self, settings, orders):
        super(RhdApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        k = 0
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                k += 1
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak2 = 'AK2*[1]850*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak2, {
                    'ack_control_number': "%04d" % int(order['ack_control_number'])
                }))
                ak5 = 'AK5*[1]A'
                segments.append(self.insertToStr(ak5, {
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))

                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                isa_control_number = str(random.randrange(100000000, 999999999))
                date_now = datetime.utcnow()
                receiver_id = self.receiver_id
                if hasattr(self, 'repetition_separator'):
                    repetition_separator = self.repetition_separator
                else:
                    repetition_separator = '*'

                if hasattr(self, 'standard_identifier'):
                    standard_identifier = self.standard_identifier
                else:
                    standard_identifier = 'U'


                params = {
                    'group': 'FA',
                    'isa_data': {
                        'sender_id_qualifier': self.sender_id_qualifier,
                        'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                        'receiver_id_qualifier': self.receiver_id_qualifier,
                        'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                        'edi_x12_version_isa': self.edi_x12_version_isa,
                        'isa_control_number': isa_control_number,
                        'date': date_now.strftime('%y%m%d'),
                        'time': date_now.strftime('%H%M'),
                        'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                        'repetition_separator': repetition_separator,
                        'standard_identifier': standard_identifier
                    }
                }


                data.append(self.wrap(segments, params))
        return data
