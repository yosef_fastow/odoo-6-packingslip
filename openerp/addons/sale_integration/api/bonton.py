import os
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from customer_parsers.bonton_csv_parser import CSVParser

from utils import feedutils as feed_utils
from configparser import ConfigParser
from pf_utils.utils.re_utils import f_d
import logging
from datetime import datetime
from apiopenerp import ApiOpenerp

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


class BontonApi(FtpClient):

    def __init__(self, settings):
        super(BontonApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/bonton_settings.ini' % os.path.dirname(__file__))


class BontonApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(BontonApiClient, self).__init__(
            settings_variables, BontonOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = BontonApiGetOrdersXML

    # def __init__(self, settings):
    #     super(JomashopApiGetOrdersXML, self).__init__(settings)
    #     self.use_ftp_settings('load_orders')
    #     self.parser = CSV_parser(customer=self.customer)
    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, serialized_order):
        ordersApi = YamlOrder(serialized_order)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = BontonApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        res = []
        confirmApi = BontonApiConfirmShipment(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        res.append('Confirm result:\n{0}\n'.format(str(res_confirm)))

        if self.is_invoice_required:
            invoiceApi = BontonApiInvoiceOrders(self.settings, lines)
            res_invoice = invoiceApi.process('send')
            self.extend_log(invoiceApi)
            res.append('Invoice result:\n{0}'.format(str(res_invoice)))

        return "\n\n".join(res)

    def processingOrderLines(self, lines, state='accepted'):
        res = False
        processed_lines = []
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):

            ordersApi = BontonApiCancelOrders(self.settings, lines, state)
            res_send = ordersApi.send()
            res &= res_send
            if res_send:
                processed_lines.append(lines)
            self.extend_log(ordersApi)
        else:
            res = True
        return res

    #def returnOrders(self, lines):
    #    ReturnApi = BontonApiReturnOrders(self.settings, lines)
    #    ReturnApi.process('send')
    #    self.extend_log(ReturnApi)
    #    return True

class BontonApiGetOrdersXML(BontonApi):

    def __init__(self, settings):
        super(BontonApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSVParser(customer=self.customer)

class BontonApiUpdateQTY(BontonApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(BontonApiUpdateQTY, self).__init__(settings)

        self.use_ftp_settings('inventory')
        self.lines = lines
        self.filename = f_d("Inventory_%Y%m%d%H%M%f.csv")
        self.type_tpl = "string"
        self.revision_lines = {
            'bad': [],
            'good': [],
        }

    def upload_data(self):
        data = '"sku", "status","quantity_on_order","estimated_availability_date","quantity_available" \n'
        for line in self.lines:
            if not bool(line['customer_id_delmar']):
		self.append_to_revision_lines(line, 'bad')
                continue
            line_eta_qty = 0
            line_eta_date = '0000-00-00'
            line_qty = line.get('qty', False)
            line_status = 'out-of-stock'
            dnr_flag = True if line.get('dnr_flag', False) == '1' else False
            if line_qty:
                line_status = 'in-stock'

            if not line_qty:
                line_eta_date = '2039-12-31' if line.get('eta') in ['0', 0, None, 'None'] else line.get('eta')
                line_eta_qty = 0 if line.get('eta_qty', 0) in ['0', 0, None, 'None'] else int(line.get('eta_qty'))
                if dnr_flag and not line_qty:
                    line_status = 'discontinued'
                    line_eta_date = '2039-12-31'

                if not line_eta_qty and line_eta_date != '2039-12-31' and not dnr_flag:
                    line_eta_qty = 1

            line['eta_qty'] = line_eta_qty
            line['eta_date'] = line_eta_date
            line['status'] = line_status
            data += feed_utils.FeedUtills(self.config['InventorySettings'].items(), line).create_csv_line(
                quote=True)
            self.append_to_revision_lines(line, 'good')
        return data

class BontonApiConfirmShipment(BontonApi):
    confirmLines = []

    def __init__(self, settings, lines):
        super(BontonApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.confirmLines = lines
        self.filename = "Order_Shipment_%s.csv" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        ship_method = ''
        shipping_service = ''
        carrier = self.confirmLines[0]['carrier']
        if carrier:
            carrier = carrier.split(', ')
            try:
                ship_method = carrier[1]
                shipping_service = carrier[0]
            except ValueError:
                pass

        data = '"po_number", "package_tracking_number", "package_ship_carrier", "package_ship_method", "shipping_service_level_code", "package_ship_cost", "package_ship_date", "line_item_sku", "line_item_quantity" \n'
        for line in self.confirmLines:
            line['carrier'] = shipping_service
            line['method'] = ship_method
            line['package_tracking_number'] = line.get("tracking_number", "")
            line['int_product_qty'] = int(line['product_qty'])
            if (line['shp_date']):
                line['shp_date'] = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S").strftime(
                    "%m/%d/%Y %H:%M")
            line['shipping_service_level_code'] = line.get('order_additional_fields').get('shipping_service_level_code', '')
            data += feed_utils.FeedUtills(self.config['ShippingConfirmation'].items(), line).create_csv_line(
                quote=True)
        return data


class BontonApiCancelOrders(BontonApi):

    def __init__(self, settings, lines, state):
        super(BontonApiCancelOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.lines = lines
        self.state = state
        self.filename = "Order_Cancel_%s.csv" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel','cancel'):
            return self.accept_cancel()
        return False

    def accept_cancel(self):
        data = '"po_number", "order_cancel_reason", "line_item_sku", "line_item_cancel_reason" \n'
        lines = self.lines
        for line in self.lines:
            data += feed_utils.FeedUtills(self.config['Cancel'].items(), line).create_csv_line(
                quote=True)

        return data


class BontonApiReturnOrders(BontonApi):
    def __init__(self, settings, lines, state):
        super(BontonApiReturnOrders, self).__init__(settings)
        self.use_ftp_settings('return')
        self.lines = lines
        self.state = state
        self.filename = "Return_%s.csv" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        data = '"return_number", "return_date"\n'
        lines = self.lines
        for line in self.cancelLines:
            line['date'] = line['shp_date'] = datetime.now().strftime("%m/%d/%Y %H:%M")
            data += feed_utils.FeedUtills(self.config['Return'].items(), line).create_csv_line(
                quote=True)
        return data


class BontonApiInvoiceOrders(BontonApi):

    def __init__(self, settings, lines):
        super(BontonApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.type_tpl = "string"
        # self.use_mako_templates('invoice')
        self.lines = lines
        self.filename = "Invoice_%s.csv" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        lines = self.lines
        invoice_total_amount = float(lines[0].get('amount_total', 0.0))
        data = '"invoice_id", "po_number", "invoice_total_amount", "invoice_date", "line_item_sku", "line_item_unit_price", "line_item_basis_of_unit_price", "line_item_quantity", "line_item_unit_of_measure", "invoice_terms_type", "invoice_terms_net_due_date", "invoice_terms_net_days", "ship_date" \n'
        for line in self.lines:
            line['date'] = line['shp_date'] = datetime.now().strftime("%m/%d/%Y %H:%M")
            line['int_product_qty'] = int(line['product_qty'])
            line['invoice_total_amount'] = invoice_total_amount
            data += feed_utils.FeedUtills(self.config['Invoice'].items(), line).create_csv_line(
                quote=True)
        return data

class BontonOpenerp(ApiOpenerp):

    def __init__(self):

        super(BontonOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = {
            "notes": "",
            "name": line['name'],
            'merchantSKU': line['item'],
            'optionSku': line['partner_sku'],
            'vendorSku': line['warehouse_code'],
             # 'tax_type': line.get('poLineData').get('taxType', ''),
            'qty': line['qty'],
            'customerCost': line['customerCost'],
            'cost': line['cost'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        additional_fields = []

        if line.get('color'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('color', '')
            }))

        if line.get('size'):
            additional_fields.append((0, 0, {
                'name': 'size',
                'label': 'size',
                'value': line.get('size', '')
            }))



        line_obj.update({'additional_fields': additional_fields})
        product = {}
        field_list = ['merchantSKU','customer_sku', 'item']

        for field in field_list:
            if line.get(field, False):
                search_field = line[field]
                line_obj['sku'] = line[field]
                if('/' in line_obj['sku']):
                    try:
                        _sku = line_obj['sku']
                        _size = _sku[_sku.find("/") + 1:]
                        if(_size):
                            line_obj['sku'] = _sku[:_sku.find("/")]
                            search_feild = _sku[:_sku.find("/")]
                            line_obj['size'] = _size
                    except:
                        pass
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      search_field,
                                                                                      ('default_code', 'customer_sku',
                                                                                       'upc', 'customer_id_delmar')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line_obj.get('size', False):
                        try:
                            size_str = line_obj['size']
                            size_ids = size_obj.search(
                                cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False

                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['item'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')

        order_lines = []
        for line in sale_order.order_line:
            line_additional_fields = {x.name: x.value for x in line.additional_fields or []}
            order_lines.append({
                'vendorSku': line.vendorSku,
                'merchantSku': line.merchantSKU,
                'customer_sku': line_additional_fields.get('customer_sku', ''),
                'upc': line_additional_fields.get('upc', ''),
                'product_qty': int(line.product_uom_qty),
                'price_unit': line.price_unit,
            })

        result = {
            'po_number': sale_order['po_number'],
            'external_customer_order_id': sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR',
            'address1': '',
            'city': '',
            'state': '',
            'zip': '',
            'country': '',
            'ship_to_address': {
                'edi_code': 'ST',
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'country': '',
                'zip': '',
            },
            'lines': order_lines,
            'carrier': sale_order.carrier,
            'order_additional_fields': {x.name: x.value for x in sale_order.additional_fields or []},
        }
        if vendor_address:
            result['address1'] = vendor_address.street
            result['city'] = vendor_address.city
            result['state'] = vendor_address.state_id.code
            result['zip'] = vendor_address.zip
            result['country'] = vendor_address.country_id.code

        if (sale_order.partner_shipping_id):
            result['ship_to_address'].update({
                'name': sale_order.partner_shipping_id.name or '',
                'address1': sale_order.partner_shipping_id.street or '',
                'address2': sale_order.partner_shipping_id.street2 or '',
                'city': sale_order.partner_shipping_id.city or '',
                'state': sale_order.partner_shipping_id.state_id.code or '',
                'country': sale_order.partner_shipping_id.country_id.code or '',
                'zip': sale_order.partner_shipping_id.zip or '',
            })

        return result

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['poNumber'], tuple(customer_ids)))

        return cr.dictfetchall()

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        customer_skus = [line.vendorSku for line in sale_order.order_line if line.vendorSku]
        customer_skus_str = ' ,'.join(customer_skus)
        res = {
            'Ref2': customer_skus_str[:30] or ''
        }
        return res

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):

        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_order_id': sale_obj.external_customer_order_id,
            'product_qty': order_line.product_uom_qty,
            'external_customer_line_id': order_line.external_customer_line_id,
            'vendorSku': order_line.vendorSku,
        }

        return line
