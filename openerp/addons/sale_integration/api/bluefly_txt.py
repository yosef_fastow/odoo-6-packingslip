# -*- coding: utf-8 -*-
from datetime import datetime
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
import logging
from configparser import ConfigParser
from apiopenerp import ApiOpenerp
from pf_utils.utils.decorators import return_ascii
from utils import feedutils as feed_utils
from customer_parsers.bluefly_txt_parser import CSVParser
from abc import ABCMeta
import os
from openerp.addons.pf_utils.utils.re_utils import f_d
from openerp.addons.pf_utils.utils.helper import get_image_url


_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}

SETTINGS_FIELDS = (
    ('supplier_id',               'Supplier ID',            'Delmar'),  # Fake ID

)


class BlueflyTxtApi(FtpClient):

    def __init__(self, settings):
        super(BlueflyTxtApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/bluefly_txt_settings.ini' % (os.path.dirname(__file__)))
        self.load_orders_api = BlueflyTxtOrders
        self.delimiter = '\t'


class BlueflyTxtApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(BlueflyTxtApiClient, self).__init__(
            settings_variables, BlueflyTxtOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES
        )
        self.load_orders_api = BlueflyTxtOrders

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state=None):
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            ordersApi = BlueflyTxtApiCancelOrders(self.settings, lines)
            ordersApi.process('send')
            self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = BlueflyTxtApiConfirmOrders(self.settings, lines)
        confirmApi.process('send')
        self.extend_log(confirmApi)
        return True

    def updateQTY(self, lines, mode=None):
        if not mode or mode not in UPDATE_CLASSES:
            mode = 'product'

        updateApi = UPDATE_CLASSES[mode](self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def returnOrders(self, lines):
        returnApi = BlueflyTxtApiReturnOrders(self.settings, lines)
        returnApi.process('send')
        self.extend_log(returnApi)
        return True


class BlueflyTxtOrders(BlueflyTxtApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(BlueflyTxtOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))
        self.parser = CSVParser(self)


class BlueflyTxtApiSend(BlueflyTxtApi):
    __metaclass__ = ABCMeta

    def __init__(self, settings, lines, feed_name, ftp_setting, template, skip_invalid_lines=False):
        super(BlueflyTxtApiSend, self).__init__(settings)
        self.use_ftp_settings(ftp_setting, self.get_root_dir(settings))
        self.lines = lines
        self.filename = f_d("{feed_name}_%Y-%m-%d_%H%M%S.txt".format(feed_name=feed_name))
        self.template = template
        self.skip_invalid_lines = skip_invalid_lines
        if not skip_invalid_lines:
            self.revision_lines = {
                'bad': [],
                'good': []
            }

    def prepare_line(self, line):
        return line

    @return_ascii
    def upload_data(self):

        feed = []
        line_items = self.config[self.template + 'Setting'].items()
        header_items = [(x.upper(), y) for x, y in line_items]

        feed.append(
            feed_utils.FeedUtills(header_items, None)._create_line(
                self.delimiter, skip_invalid_line=False, csv_flag=True, header=True
            )
        )
        for line in self.lines:

            line = self.prepare_line(line)
            feed_line = feed_utils.FeedUtills(line_items, line).create_csv_line(self.delimiter, self.skip_invalid_lines)
            if feed_line:
                feed.append(feed_line)
            if self.skip_invalid_lines:
                target = 'good' if feed_line else 'bad'
                self.append_to_revision_lines(line, target)

        return "".join(feed)[:-2]


class BlueflyTxtApiConfirmOrders(BlueflyTxtApiSend):

    def __init__(self, settings, lines):
        super(BlueflyTxtApiConfirmOrders, self).__init__(
            settings, lines, 'OrderStatus', 'confirm_shipment', 'OrderStatus'
        )

    def prepare_line(self, line):
        image = line.get('image_path')
        if not image or image.lower() in ['false', 'none']:
            image = get_image_url(line.get('default_code', ''))
        line["main_image_url"] = image

        dt = line.get('shp_date') and datetime.strptime(line['shp_date'], "%Y-%m-%d %H:%M:%S") or datetime.now()
        line['shp_date'] = dt.strftime("%m/%d/%Y")

        line['product_qty'] = int(line['product_qty'])

        return line


class BlueflyTxtApiCancelOrders(BlueflyTxtApiSend):

    def __init__(self, settings, lines):
        super(BlueflyTxtApiCancelOrders, self).__init__(
            settings, lines, 'OrderStatus_Cancellation', 'confirm_cancel', 'OrderStatusCancellation'
        )

    def prepare_line(self, line):
        line['product_qty'] = int(line['product_qty'])

        return line


class BlueflyTxtApiReturnOrders(BlueflyTxtApiSend):

    def __init__(self, settings, lines):
        super(BlueflyTxtApiReturnOrders, self).__init__(
            settings, lines, 'RefundRequest', 'return_orders', 'Return'
        )

    def prepare_line(self, line):
        dt = datetime.now()
        line['product_qty'] = int(line['product_qty'])
        line['date'] = dt.strftime("%m/%d/%Y")
        return line


########
# Update Qty
#
class BlueflyTxtUpdate(BlueflyTxtApiSend):
    __metaclass__ = ABCMeta

    def __init__(self, settings, lines, feed_name=None):
        super(BlueflyTxtUpdate, self).__init__(
            settings, lines, '{supplier_id}_{feed_name}'.format(supplier_id=settings['supplier_id'], feed_name=feed_name), 'inventory', feed_name, True
        )
        self.supplier_id = settings.get('supplier_id')

    def prepare_line(self, line):
        line['supplier_id'] = self.supplier_id
        return line


class BlueflyTxtApiUpdateProduct(BlueflyTxtUpdate):
    def __init__(self, settings, lines):
        super(BlueflyTxtApiUpdateProduct, self).__init__(settings, lines, feed_name='Product')


class BlueflyTxtApiUpdateQTY(BlueflyTxtUpdate):
    def __init__(self, settings, lines):
        super(BlueflyTxtApiUpdateQTY, self).__init__(settings, lines, feed_name='Inventory')


class BlueflyTxtApiUpdatePrice(BlueflyTxtUpdate):
    def __init__(self, settings, lines):
        super(BlueflyTxtApiUpdatePrice, self).__init__(settings, lines, feed_name='Price')

#
# End Update Qty
########


class BlueflyTxtOpenerp(ApiOpenerp):

    def __init__(self):
        super(BlueflyTxtOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line.copy()
        line_obj.update({
            'notes': '',
            'additional_fields': self.wrap_additional_fields(
                line.get('additional_fields', {})
            )
        })

        product_obj = self.pool.get('product.product')
        search_by = ('customer_id_delmar', 'upc', 'customer_sku')
        product = {}
        field_list = ['sku', 'vendorSku']
        for field_name in field_list:
            field = line.get(field_name, None)
            if field:

                # Strange prefix from customer for vendorSku
                if field_name == 'vendorSku':
                    field = field.replace('501-12454-', '')
                    if not field:
                        continue

                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], field, search_by)
                if product:
                    line_obj['size_id'] = size and size.id or False
                    break

        if product:
            line_obj['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id']
            )
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(line['sku'])
            line_obj['product_id'] = False

        return line_obj


UPDATE_CLASSES = {
    'product': BlueflyTxtApiUpdateProduct,
    'qty': BlueflyTxtApiUpdateQTY,
    'price': BlueflyTxtApiUpdatePrice,
}
