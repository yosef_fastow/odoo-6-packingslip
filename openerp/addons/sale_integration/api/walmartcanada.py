# -*- coding: utf-8 -*-
import random
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.walmart_canada_input_edi_parser import WalmartCanadaEDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime
import pytz
import logging
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from json import loads as json_loads


_logger = logging.getLogger(__name__)

def normalize_fckng_json_yopta(string):
    return string.replace("'", '"').replace(" None,", ' "",')


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '057932'),
    ('vendor_name',                 'Vendor name',              'Walmart Canada'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '8122CANT'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    'ZZ'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '5010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.out'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('GLN',                         'Global Location Number',   '0605388000002'),
    ('repetition_separator',        'Repetition Separator',     '^'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('carrier_settings',             'Carrier Settings',          '''
{
    "carrier_method_codes": {
        "001": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "Regular Parcel"
        },
        "002": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "Expedited Parcel"
        },
        "003": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "Xpresspost"
        },
        "004": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "Priority Next A.M."
        },
        "005": {
            "carrier_name": "First Team Transport",
            "service_level_description": "Freight"
        },
        "011": {
            "carrier_name": "Purolator",
            "service_level_description": "Freight"
        },
        "012": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express 9AM"
        },
        "013": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express 10:30AM"
        },
        "014": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express"
        },
        "015": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express Evening"
        },
        "016": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Ground"
        },
        "017": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Ground Evening"
        },
        "018": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express U.S. 9AM"
        },
        "019": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express U.S. 10:30AM"
        },
        "020": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Express U.S."
        },
        "021": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Ground U.S."
        },
        "022": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: Same Day/Next Flight Out"
        },
        "023": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: Same Day/Direct Drive"
        },
        "024": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: On & Gone"
        },
        "025": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: Hot"
        },
        "026": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: Rush"
        },
        "027": {
            "carrier_name": "Purolator",
            "service_level_description": "Purolator Same Day: Regular"
        },
        "035": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Standard"
        },
        "036": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Expedited"
        },
        "037": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Express Saver"
        },
        "038": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Express"
        },
        "039": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Express Early A.M."
        },
        "040": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Express Critical"
        },
        "041": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Freight LTL"
        },
        "042": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Air Freight Consolidated"
        },
        "043": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Air Freight Direct"
        },
        "044": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Express Freight"
        },
        "045": {
            "carrier_name": "UPS",
            "service_level_description": "UPS 3 Day Freight NGS"
        },
        "046": {
            "carrier_name": "UPS",
            "service_level_description": "UPS 3 Day Freight"
        },
        "047": {
            "carrier_name": "UPS",
            "service_level_description": "UPS 2nd Day Air Freight NGS"
        },
        "048": {
            "carrier_name": "UPS",
            "service_level_description": "UPS 2nd Day Air Freight"
        },
        "049": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Next Day Air Freight NGS"
        },
        "050": {
            "carrier_name": "UPS",
            "service_level_description": "UPS Next Day Air Freight"
        },
        "051": {
            "carrier_name": "FedEx",
            "service_level_description": "FedEx Next Flight"
        },
        "052": {
            "carrier_name": "FedEx",
            "service_level_description": "FedEx First Overnight"
        },
        "053": {
            "carrier_name": "FedEx",
            "service_level_description": "FedEx Priority Overnight"
        },
        "054": {
            "carrier_name": "FedEx",
            "service_level_description": "FedEx 2Day"
        },
        "055": {
            "carrier_name": "FedEx",
            "service_level_description": "FedEx Ground"
        },
        "056": {
            "carrier_name": "FedEx",
            "service_level_description": "Freight"
        },
        "061": {
            "carrier_name": "Same Day Worldwide",
            "service_level_description": "SDCR-FREIGHT"
        },
        "080": {
            "carrier_name": "ASL-FREIGHT",
            "service_level_description": "ASL-FREIGHT"
        },
        "081": {
            "carrier_name": "ASL-DDS",
            "service_level_description": "ASL-DDS"
        },
        "082": {
            "carrier_name": "ASL-DDS_BB",
            "service_level_description": "ASL-DDS_BB"
        },
        "090": {
            "carrier_name": "FEDEX-FREIGHT PRIORITY",
            "service_level_description": "FEDEX-FREIGHT PRIORITY"
        },
        "100": {
            "carrier_name": "LOOMIS-GROUND",
            "service_level_description": "LOOMIS-GROUND"
        },
        "101": {
            "carrier_name": "LOOMIS-EXPRESS",
            "service_level_description": "LOOMIS-EXPRESS"
        },
        "110": {
            "carrier_name": "CANPAR-GROUND",
            "service_level_description": "CANPAR-GROUND"
        },
        "111": {
            "carrier_name": "CANPAR-EXPRESS",
            "service_level_description": "CANPAR-EXPRESS"
        },
        "112": {
            "carrier_name": "LOOMIS",
            "service_level_description": "LOOMIS"
        },
        "113": {
            "carrier_name": "CANPAR",
            "service_level_description": "CANPAR"
        },
        "161": {
            "carrier_name": "WalmartSCAC-FREIGHT",
            "service_level_description": "WalmartSCAC-FREIGHT"
        },
        "900": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "CPC-STANDARDLETTERMAIL"
        },
        "901": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "CPC-MEDIUMLETTERMAIL"
        },
        "902": {
            "carrier_name": "Canada Post Corp.",
            "service_level_description": "CPC-OTHERLETTERMAIL"
        },
        "999": {
            "carrier_name": "Other",
            "service_level_description": "Other"
        }
    },
    "carrier_name_codes": {
        "100": "Canada Post",
        "101": "Purolator",
        "102": "Same Day Worldwide",
        "103": "WalmartSCAC",
        "105": "UPS",
        "110": "ASL",
        "111": "FedEx",
        "112": "Loomis",
        "113": "Canpar"
    },
    "level_codes": {
        "CX": "Express Service",
        "DT": "Electronic Delivery",
        "R5": "Manifest Freight",
        "SG": "Standard Ground"
    }
}

'''),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class WalmartCanadaApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)

    def prepare_address_segment(self, prefix, parts=None):

        segment = None
        if parts:
            address = "*".join([check_none(x) for x in parts]).rstrip('*')
            if address:
                segment = "{prefix}*{address}".format(prefix=prefix, address=address)
        return segment


class WalmartCanadaApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(WalmartCanadaApiClient, self).__init__(
            settings_variables, WalmartCanadaOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = WalmartCanadaApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = WalmartCanadaApiFunctionalAcknowledegment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = WalmartCanadaApiAcknowledgementOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = WalmartCanadaApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = WalmartCanadaApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        self.settings["response"] = ""
        updateApi = WalmartCanadaApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class WalmartCanadaApiGetOrders(WalmartCanadaApi):
    """EDI/V5010 X12/850: Walmart Canada Purchase Orders"""

    def __init__(self, settings):
        super(WalmartCanadaApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = WalmartCanadaEDIParser(self)
        self.edi_type = '850'


class WalmartCanadaApiFunctionalAcknowledegment(WalmartCanadaApi):

    orders = []

    def __init__(self, settings, orders):
        super(WalmartCanadaApiFunctionalAcknowledegment, self).__init__(settings)
        self.use_ftp_settings('functional_acknowledgement')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))

                ak1 = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak1, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak5 = 'AK5*[1]%(acknowledgement_code)s'
                segments.append(self.insertToStr(ak5, {
                    # A - Accepted
                    # E - Accepted But Errors Were Noted
                    # M - Rejected, Message Authentication Code (MAC) Failed
                    # R - Rejected
                    # W - Rejected, Assurance Failed Validity Tests
                    # X - Rejected, Content After Decryption Could Not Be Analyzed
                    'acknowledgement_code': 'A',
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class WalmartCanadaApiAcknowledgementOrders(WalmartCanadaApi):
    """EDI/V5010 X12/855: 855 Purchase Order Acknowledgement"""

    def __init__(self, settings, lines, state):
        super(WalmartCanadaApiAcknowledgementOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        self.state = state

    def upload_data(self):

        self.edi_type = '855'
        cancel_feed = self.state in ('accept_cancel', 'rejected_cancel', 'cancel')

        statuscode = 'AT'
        if (self.state == 'accepted'):
            statuscode = 'AT'
        elif (self.state == 'accepted_with_details'):
            statuscode = 'AD'

        segments = []

        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'

        now_dtm = datetime.utcnow()
        now_date = now_dtm.strftime('%Y%m%d')
        now_time = now_dtm.strftime('%H%M')

        processing_date = self.processingLines[0].get('date', False) or now_date

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines[0]['po_number'],
            'po_date': processing_date,  # CCYYMMDD
            'ack_date': now_date,  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        ref_codes = [
            'LU',  # Location Number
            'VR',  # Vendor ID Number
        ]

        for ref_code in ref_codes:
            segments.append(self.insertToStr(ref, {
                'code': ref_code,
                'order_number': self.processingLines[0].get('order_additional_fields', {}).get(ref_code),
            }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s*[4]%(time_code)s'
        segments.append(self.insertToStr(dtm, {
            'code': '205',  # Transmitted
            'date': now_date,  # Date the feed is generated
            'time': now_time,  # Time the feed is generated
            # Code identifying the time.
            # In accordance with International Standards Organization standard 8601,
            # time can be specified by a + or - and an indication in hours
            # in relation to Universal Time Coordinate (UTC) time; since + is a restricted character,
            # + and - are substituted by P and M in the codes that follow
            'time_code': 'GM',  # Greenwich Mean Time
        }))

        po1 = 'PO1*[1]%(line_number)s*[2]*[3]*[4]*[5]*[6]%(code)s*[7]%(identifier)s'
        ref = 'REF*[1]%(code)s*[2]%(value)s'
        ack = 'ACK*[1]%(code)s*[2]%(qty)s*[3]EA'

        i = 0
        for line in self.processingLines:
            i += 1
            qty = line['product_qty']
            template = po1

            insert_obj = {
                'line_number': str(i),
                'code': 'PL',  # Purchaser's Order Line Number
                'identifier': line['external_customer_line_id'],
            }
            segments.append(self.insertToStr(template, insert_obj))

            segments.append(self.insertToStr(ack, {
                'code': 'R4' if cancel_feed else 'IA',
                'qty': qty,
            }))

        ctt = 'CTT*[1]%(count_lines)s'
        segments.append(self.insertToStr(ctt, {
            'count_lines': i,
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})


class WalmartCanadaApiConfirmShipment(WalmartCanadaApi):
    """EDI/V5010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(WalmartCanadaApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = '0001'
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        shp_time = shp_date.strftime('%H%M')

        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        now_dtm = datetime.utcnow()
        now_date = now_dtm.strftime('%Y%m%d')
        now_time = now_dtm.strftime('%H%M')
        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s*[4]%(time_code)s'
        segments.append(self.insertToStr(dtm, {
            'code': '205',  # Transmitted
            'date': now_date,  # Date the feed is generated
            'time': now_time,  # Time the feed is generated
            # Code identifying the time.
            # In accordance with International Standards Organization standard 8601,
            # time can be specified by a + or - and an indication in hours
            # in relation to Universal Time Coordinate (UTC) time; since + is a restricted character,
            # + and - are substituted by P and M in the codes that follow
            'time_code': 'GM',  # Greenwich Mean Time
        }))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s*[4]%(hierarchical_child_code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S',  # Shipment,
            'hierarchical_child_code': '1',

        }
        segments.append(self.insertToStr(hl, hl_data))

        normalized_json_str = normalize_fckng_json_yopta(self.confirmLines[0]['order_additional_fields']['carrier_details'])

        carrier_details = json_loads(normalized_json_str)

        carrier_code = self.confirmLines[0]['carrier_code']
        carrier_codes = [str(x).strip() for x in carrier_code.split(',')]
        carrier_method_code = (
            carrier_codes and
            carrier_codes[0] and
            carrier_details['carrier_method_code'] or
            ''
        )
        normalized_json_str = normalize_fckng_json_yopta(self.carrier_settings.strip())
        carrier_settings = json_loads(normalized_json_str)
        raw_availabile_carrier_method_codes = carrier_settings.get('carrier_method_codes', {})
        availabile_carrier_method_codes = [x for x in raw_availabile_carrier_method_codes]
        if carrier_method_code not in availabile_carrier_method_codes:
            raise NotImplementedError(
                'Unknow carrier method code: {0}. Availabile carrier method codes: {1}'.format(
                    str(carrier_method_code),
                    ','.join(['\'' + str(x) + '\'' for x in availabile_carrier_method_codes])
                )
            )

        carrier_name_code = (
            carrier_codes and
            carrier_codes[1] and
            carrier_details['carrier_name_code'] or
            ''
        )

        # availabile carrier name codes
        # 100 - Canada Post
        # 101 - Purolator
        # 102 - Same Day
        raw_availabile_carrier_name_codes = carrier_settings.get('carrier_name_codes', {})
        availabile_carrier_name_codes = [x for x in raw_availabile_carrier_name_codes]
        if carrier_name_code not in availabile_carrier_name_codes:
            raise NotImplementedError(
                'Unknow carrier name code: {0}. Availabile carrier name codes: {1}'.format(
                    str(carrier_name_code),
                    ','.join(['\'' + str(x) + '\'' for x in availabile_carrier_name_codes])
                )
            )

        level_code = (
            carrier_codes and
            carrier_codes[2] and
            carrier_details['level_code'] or
            ''
        )
        # availabile level codes
        # CX - Express Service
        # R5 - Manifest Freight
        # SG - Standard Ground
        # DT - Electronic Delivery
        raw_availabile_level_codes = carrier_settings.get('level_codes', {})
        availabile_level_codes = [x for x in raw_availabile_level_codes]
        if level_code not in availabile_level_codes:
            raise NotImplementedError(
                'Unknow level code: {0}. Availabile level codes: {1}'.format(
                    str(level_code),
                    ','.join(['\'' + str(x) + '\'' for x in availabile_level_codes])
                )
            )

        td5 = 'TD5*[1]O*[2]93*[3]%(carrier_method_code)s*[4]*[5]%(carrier_name_code)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]%(level_code)s*[13]*[14]*[15]CA'
        td5_data = {
            'carrier_method_code': carrier_method_code,
            'carrier_name_code': carrier_name_code,
            'level_code': level_code,
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'code': 'LU',
            'data': 'CA'
        }))
        segments.append(self.insertToStr(ref, {
            'code': 'VR',  # Vendor ID Number
            'data': self.confirmLines[0]['order_additional_fields']['VR']
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s*[4]%(time_code)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
            'time': shp_time,
            'time_code': 'GM',
        }))

        cur = 'CUR*[1]%(code)s*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'code': 'VN',
            'currency_code': self.confirmLines[0]['order_additional_fields']['currency'],
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'O',  # Order
            'hierarchical_child_code': '1',
        }))

        prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]*[5]*[6]*[7]DS'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'P',  # Pack
            'hierarchical_child_code': '1',
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        lin = 'LIN*[1]%(external_customer_line_id)s*[2]%(customer_sku_qualifier)s*[3]%(customer_sku)s'
        sln = 'SLN*[1]%(sequence_number)s*[2]%(po_line_number)s*[3]I*[4]%(qty)s*[5]EA*[6]*[7]*[8]*[9]UP*[10]%(upc)s*[11]BP*[12]%(win_number)s'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I',  # Item
                'hierarchical_child_code': '0',
            }))

            customer_sku = line['merchantSKU'] or line['customer_sku'] or line['additional_fields'].get('customer_sku', '') or ''
            customer_sku_qualifier = customer_sku and 'SK' or ''
            upc = line['additional_fields'].get('upc', '') or line['upc'] or ''
            if not upc:
                raise NotImplementedError('UPC is required!')

            segments.append(self.insertToStr(lin, {
                'external_customer_line_id': line['external_customer_line_id'],
                'customer_sku': check_none(customer_sku),
                'customer_sku_qualifier': customer_sku_qualifier,
            }))

            qty = line['product_qty']
            try:
                qty = int(qty)
            except ValueError:
                qty = 1

            # WIN is Cutomer ID Delmar
            win_number = check_none(line['customer_id_delmar'])
            if not win_number or len(win_number) > 13:
                raise NotImplementedError('Bad WIN number')

            segments.append(self.insertToStr(sln, {
                'sequence_number': line['additional_fields'].get(
                    'sequence_number', ''),
                'po_line_number': line['external_customer_line_id'],
                'qty': qty,
                'upc': check_none(upc),
                'win_number': win_number,
                'serial_number': '',
            }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class WalmartCanadaApiInvoiceOrders(WalmartCanadaApi):
    """EDI/V5010 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(WalmartCanadaApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        now_dtm = datetime.utcnow()

        invoice_number = check_none(self.invoiceLines[0]['invoice']).replace('WMC', '') + now_dtm.strftime('%m%d%H%M%S')
        tracking_number = self.invoiceLines[0]['tracking_number']

        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': invoice_number,
            'po_number': self.invoiceLines[0]['po_number'],  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        cur = 'CUR*[1]%(code)s*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'code': 'VN',
            'currency_code': self.invoiceLines[0]['order_additional_fields']['currency'],
        }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        normalized_json_str = normalize_fckng_json_yopta(self.invoiceLines[0]['order_additional_fields'].get('vendor_id_number', '{}'))
        vendor_id_number = json_loads(normalized_json_str)
        segments.append(self.insertToStr(ref, {
            'code': 'IA',
            'order_number': '{code}*{name}'.format(
                code=vendor_id_number.get('ship_node', ''),
                name=vendor_id_number.get('vendor_name', '')
            )
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'SI',
            'order_number': tracking_number,
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'DD',
            'order_number': 'CAN'
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'TC',
            'order_number': self.invoiceLines[0]['carrier']
        }))

        carrier_codes = self.invoiceLines[0]['carrier_code'].split(',')
        if len(carrier_codes) == 3:
            segments.append(self.insertToStr(ref, {
                'code': 'TH',
                'order_number': carrier_codes[0]
            }))

        original_addresses = self.invoiceLines[0].get('original_addresses', {})
        for address_type in ['ST']:
            address = original_addresses.get(address_type, {})
            if address:
                n1 = self.prepare_address_segment('N1', ['ST', address['name']])
                if n1:
                    segments.append(n1)

                n2 = self.prepare_address_segment('N2', [address['last_name'], address['middle_name']])
                if n2:
                    segments.append(n2)

                n3_1 = self.prepare_address_segment('N3', [address['address1'], address['address2']])
                if n3_1:
                    segments.append(n3_1)

                if check_none(address['address3']):
                    n3_2 = self.prepare_address_segment('N3', [address['address3']])
                    if n3_2:
                        segments.append(n3_2)

                n4 = self.prepare_address_segment('N4', [
                    address['city'],
                    address['state'],
                    address['zip'],
                    address['country'],
                ])
                if n4:
                    segments.append(n4)

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s*[4]%(time_code)s'

        segments.append(self.insertToStr(dtm, {
            # 003 - Invoice
            'code': '003',
            'date': now_dtm.strftime('%Y%m%d'),
            'time': now_dtm.strftime('%H%M'),
            'time_code': 'GM',
        }))

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
            'time': shp_date.strftime('%H%M'),
            'time_code': 'GM',
        }))

        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(customer_sku_qualifier)s*[7]%(customer_sku)s*[8]%(vendor_number_qualifier)s*[9]%(vendor_number)s*[10]%(po_line_number_qualifier)s*[11]%(po_line_number)s'
        ctp = 'CTP*[1]AS*[2]TOT*[3]*[4]*[5]*[6]*[7]*[8]%(line_total)s'
        pid = 'PID*[1]%(description_type)s*[2]*[3]*[4]*[5]%(data)s*[6]*[7]*[8]*[9]EN'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            customer_sku = check_none(line['merchantSKU'])
            customer_sku_qualifier = customer_sku and 'SK' or ''
            vendor_number_qualifier = check_none(line['vendorSku']) and 'VN' or ''
            vendor_number = line['vendorSku'] or ''

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['price_unit'],
                'customer_sku_qualifier': customer_sku_qualifier,
                'customer_sku': customer_sku,
                'vendor_number_qualifier': vendor_number_qualifier,
                'vendor_number': vendor_number,
                'po_line_number_qualifier': 'A7',
                'po_line_number': line['external_customer_line_id'],
            }))

            line_amount = line['price_unit']
            tax_identifiers = {
                'HST': 'hst',
                'GST': 'gst',
                'PST': 'pst',
                'QST': 'qst',
            }

            txi = 'TXI*[1]GS*[2]%(tax_amount)s*[3]%(tax_rate)s*[4]*[5]*[6]*[7]*[8]*[9]*[10]%(tax_name)s'
            for tax_identifier, name_tax in tax_identifiers.items():
                amount = float(line[name_tax] or 0) * float(line['unit_cost'] or 0) * qty / float(line['amount_untaxed'] or 0)
                raw_taxes = self.invoiceLines[0]['raw_taxes']
                rate = float(raw_taxes.get(name_tax, 0.0)) * 100
                if amount and rate:
                    line_amount += amount
                    segments.append(self.insertToStr(txi, {
                        'tax_amount': '{0:.2f}'.format(amount),
                        'tax_rate': '{0:.2f}'.format(rate),
                        'tax_name': tax_identifier,
                    }))

            segments.append(self.insertToStr(ctp, {
                'line_total': '{0:.2f}'.format(line_amount),
            }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'data': line['name'],
            }))

            total_qty_invoiced += qty

        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'IN'})


class WalmartCanadaApiUpdateQTY(WalmartCanadaApi):
    name = "WMCA_INV"
    _file_type = 'FII'
    lines = []

    def __init__(self, settings, lines):
        super(WalmartCanadaApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates('inventory')
        self.lines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.rand = random.randint(1, 9999999999)
        utc = pytz.utc
        self.date = utc.localize(datetime.now())

    @property
    def filename(self):
        if (
            hasattr(self, '_filename') and
            self._filename
        ):
            pass
        else:
            self._filename = "{0}_{1}_{2}_{3}.xml".format(
                self.name,
                self.vendor_id,
                "{:%Y%m%d_%H%M%S}".format(self.date),
                str(self.rand)
            )
        return self._filename

    def upload_data(self):

        additional_info = {
            'date': self.date,
            'vendor_id': self.vendor_id,
            # 'external_customer_id': self.customer,
            # email request from Moshe: URGENT!!! Walmart Canada feed need a fix
            # at 26 october 2015, 20:54 MSK
            'external_customer_id': '111111',
            'rand': self.rand,
        }
        for line in self.lines:
            line['code_to_send'] = 'AC'
            if line.get('build_to_order', False) == 'BO':
                line['code_to_send'] = 'BO'
            if line.get('customer_sku', False):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good'], 'additional_info': additional_info}


class WalmartCanadaOpenerp(ApiOpenerp):

    def __init__(self):

        super(WalmartCanadaOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line
        line_obj.update({
            'notes': '',
            'sku': line['merchantSKU'],
            'customerCost': line['retail_price'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc', 'walmart_ca_sku_id')
        product = {}
        field_list = ['sku', 'merchantSKU', 'vendorSKU']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field], search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by sku %s.\n' % (line_obj['sku'])
            line_obj['product_id'] = 0

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        tax_obj = self.pool.get("delmar.sale.taxes")
        raw_taxes = {}
        line = {
            'original_addresses': {},
        }
        shipping_address = sale_obj and sale_obj.partner_shipping_id
        country_id = shipping_address.country_id.id
        state_id = shipping_address.state_id.id
        if country_id and state_id:
            raw_taxes = tax_obj.get_raw_taxes(cr, uid, country_id, [state_id])
        line.update({'raw_taxes': raw_taxes})

        order_additional_fields = {x.name: x.value for x in sale_obj.additional_fields or []}
        addresses = order_additional_fields.get('addresses', {})
        if addresses:
            try:
                addresses = json_loads(addresses)
            except ValueError:
                normalized_json_str = normalize_fckng_json_yopta(addresses)
                addresses = json_loads(normalized_json_str)
            revert_address_type_map = {value: key for key, value in address_type_map.items()}
            for type_address, value in addresses.items():
                line['original_addresses'].update({
                    revert_address_type_map.get(type_address, 'unknown'): value
                })
        return line

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):
        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_line_id': order_line.external_customer_line_id,
            'product_qty': order_line.product_uom_qty,

        }

        return line
