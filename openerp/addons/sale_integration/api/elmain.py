# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from customer_parsers.elmain_input_csv_parser import CSVParser
from datetime import datetime
import logging
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class ElMainApi(FtpClient):

    def __init__(self, settings):
        super(ElMainApi, self).__init__(settings)
        self._remove_flag = False

    def upload_data(self):
        return {'lines': self.lines}


class ElMainApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(ElMainApiClient, self).__init__(
            settings_variables, ElMainOpenerp, False, DEFAULT_VALUES)
        self.settings["order_type"] = 'normal'
        self.load_orders_api = ElMainApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='normal'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        self.settings["order_type"] = order_type
        api = ElMainApiGetOrdersXML(self.settings)
        if save_flag:
            orders = api.process('load')
        else:
            orders = api.process('read_new')
        self.extend_log(api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        self.settings["response"] = ""
        processingApi = ElMainApiCancelOrderLines(self.settings, lines, state)
        res = processingApi.process('send')
        self.extend_log(processingApi)
        return res

    def confirmShipment(self, lines):
        confirmApi = ElMainApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = ElMainApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class ElMainApiGetOrdersXML(ElMainApi):

    def __init__(self, settings):
        super(ElMainApiGetOrdersXML, self).__init__(settings)
        self.order_type = settings['order_type']
        self.use_ftp_settings('load_orders')
        self.parser = CSVParser(self.customer)


class ElMainApiUpdateQTY(ElMainApi):

    def __init__(self, settings, lines):
        super(ElMainApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "11mainInventory.csv"  # + order number
        self.filename_local = "{:11MINVENTORY%Y%m%d%H%M.csv}".format(
            datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if((not line.get('sku', False)) and line.get('sku', False) == ''):
                self.append_to_revision_lines(line, 'bad')
                continue
            if(line['customer_price'] is None):
                line['customer_price'] = ''
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class ElMainApiConfirmOrders(ElMainApi):

    def __init__(self, settings, lines):
        super(ElMainApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("Confirm%Y%m%d-%H%M.csv")

    def upload_data(self):
        lines = self.lines
        for line in lines:
            amount_line = 0.0
            if(line.get('price_unit', False) and line.get('product_qty', False)):
                price_unit = 0.0
                product_qty = 0
                try:
                    price_unit = float(line['price_unit'])
                except ValueError:
                    pass
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
                if(price_unit and product_qty):
                    amount_line = price_unit * product_qty
            line['amount_line'] = amount_line
            date_now = f_d("%m/%d/%y")
            if line.get('shp_date', False):
                date_obj = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                date_now = f_d("%m/%d/%y", date_obj)
            line['date'] = date_now
            line['product_qty'] = int(line['product_qty'])
        return {'lines': lines}


class ElMainApiCancelOrderLines(ElMainApi):

    def __init__(self, settings, lines, state):
        super(ElMainApiCancelOrderLines, self).__init__(settings)
        self.lines = lines
        self.state = state
        self.type_tpl = "string"
        self.use_mako_templates('cancel')
        self.use_ftp_settings('confirm_orders')
        self.filename = f_d('Cancel%Y%m%d-%H%M.csv')

    def upload_data(self):
        lines = self.lines
        return {'lines': lines}


class ElMainOpenerp(ApiOpenerp):

    def __init__(self):
        super(ElMainOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        additional_fields = [
            (0, 0, {'name': 'package_id', 'label': 'Package ID', 'value': line.get('package_id', '')}),
            (0, 0, {'name': 'package_status', 'label': 'Package Status', 'value': line.get('package_status', '')}),
        ]
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': '0.0',
            'customerCost': line['price_unit'],
            'customer_sku': line['customer_sku'],
            'vendorSku': line['customer_sku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': line['sku'],
            'size': line['size'],
            'id': line['id'],
            'additional_fields': additional_fields,
        }

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        sku_list = [str(x['sku']) for x in cancel_obj['lines']]
        sql = """SELECT
                sol.id as line_id,
                so.id as order_id,
                sp.id as picking_id,
                sol.name as name,
                sol.external_customer_line_id,
                so.external_customer_order_id,
                so.external_customer_order_id as po_number,
                so.create_date as order_date,
                coalesce(sp.tracking_ref, '') as tracking_number,
                coalesce(sp.shp_service, '') as shp_service,
                coalesce(solf."value", '') as package_id
            FROM
                sale_order_line as sol
                INNER JOIN sale_order as so ON so.id = sol.order_id
                INNER JOIN stock_picking as sp ON so.id = sp.sale_id
                LEFT JOIN product_multi_customer_fields as pmcf on pmcf.value IN %s
                LEFT JOIN product_multi_customer_fields_name as pmcfn on pmcfn.id = pmcf.field_name_id and pmcfn.label = 'Customer SKU'
                LEFT JOIN sale_order_line_fields as solf ON solf.line_id = sol.id and solf.name = 'package_id'
            WHERE
                so.external_customer_order_id = %s AND
                so.partner_id IN %s
        """
        params = (tuple(sku_list), cancel_obj['origin_order_id'], tuple(customer_ids))

        cr.execute(sql, params)
        lines = cr.dictfetchall()
        for line in lines:
            line['additional_fields'] = {'package_id': line['package_id']}

        return lines
