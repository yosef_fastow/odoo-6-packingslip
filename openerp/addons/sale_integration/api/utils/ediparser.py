# -*- coding: utf-8 -*-
import array
import string
import cStringIO
import re
from openerp.addons.pf_utils.utils.import_orders_normalize import data_to_ascii

try:
    # If available use the psyco optimizing routines.  This will speed
    # up execution by 2x.
    import psyco.classes
    base_class = psyco.classes.psyobj
except ImportError:
    base_class = object

alphanums = string.ascii_letters + string.digits


def check_none(value):
    return '' if value in [None, False, 'None', 'False'] else value


def normalize_line_terminators(data):
    if isinstance(data, (str, unicode)):
        data = data.replace('\r\n', '\n')
        data = data.replace('\r', '\n')
    else:
        raise NotImplementedError('Only str or unicode objects can be used')
    return data


class BadFile(Exception):
    """Raised when file corruption is detected."""


class BSC(base_class):

    def __init__(self):
        super(BSC, self).__init__()

    def __getitem__(self, index):
        segments = self.segments
        if isinstance(index, int):
            return segments[index]
        elif isinstance(index, (str, unicode)):
            if '<-' in index:
                return self.__loop__(index)
            elif ':' in index:
                return self.__xpath__(index)
            else:
                return Segments(
                    [segment for segment in segments if segment[0] == index]
                )
        else:
            return None

    def __str__(self):
        return str(self.segments)


class Segments(BSC):

    def __init__(self, segments):
        super(Segments, self).__init__()
        self.segments = segments

    def first(self):
        return self.segments[0]

    def __xpath__(self, xpath):
        segments = self.segments
        xpaths = xpath.split(':')
        sub_segments = []
        for segment in segments:
            sub_segments = segments
            for i, sub_xpath in enumerate(xpaths):
                new_sub_segments = []
                for seg in sub_segments:
                    condition = False
                    try:
                        condition = seg[i] == sub_xpath
                        if sub_xpath == '*':
                            condition = True
                    except:
                        pass
                    if condition:
                        new_sub_segments.append(seg)
                sub_segments = new_sub_segments
        return Segments(sub_segments)

    def __loop__(self, xpath_str):
        segments = self.segments
        if '|' in xpath_str:
            xpaths_raw = xpath_str.split('|')
            if len(xpaths_raw) != 2:
                raise Exception('String must be PO1<-SLN|REF for __loop__ method')
            else:
                xpaths = xpaths_raw[0]
                sub_xpath = xpaths_raw[1]
        else:
            xpaths = xpath_str
            sub_xpath = False
        general_xpath = xpaths.split('<-')
        res_segments = []
        if len(general_xpath) != 2:
            raise Exception('String must be PO1<-SLN for __loop__ method')
        else:
            start = general_xpath[0]
            end = general_xpath[1]
            inside_segments = []
            matches = re.findall(r"\(.*\)", end)
            if matches:
                matches = matches and matches[0]
                inside_segments = matches[1:-1].split(',')
                if inside_segments:
                    end = end.replace(matches, '')
            current = None
            for segment in segments:
                if(
                    current and
                    inside_segments and
                    segment[0] not in inside_segments and
                    segment[0] != start and
                    segment[0] != end
                ):
                    res_segments.append(current)
                    current = None
                if segment[0] == start:
                    if not current:
                        current = [segment]
                    else:
                        res_segments.append(current)
                        current = [segment]
                elif current and segment[0] == end:
                    current.append(segment)
                    res_segments.append(current)
                    current = None
                if current and segment[0] != start:
                    current.append(segment)
        if sub_xpath and res_segments:
            try:
                sub_xpath = int(sub_xpath)
            except ValueError:
                pass
            if isinstance(sub_xpath, int):
                res_segments = Segments(res_segments[sub_xpath])
            else:
                res_segments = [Segments(x).__xpath__(sub_xpath) for x in res_segments]
        else:
            res_segments = [Segments(x) for x in res_segments]
        return res_segments


class EdiParser(BSC):
    """Parse out segments from the X12 raw data files.

    Raises the BadFile exception when data corruption is detected.

    Attributes:
        delimiters
            A string where
            [0] == segment separator
            [1] == element separator
            [2] == sub-element separator
            [3] == repetition separator (if ISA version >= 00405
    """
    def __init__(self, edi=None, type=''):
        self.delimiters = ''
        ascii_edi = data_to_ascii(edi)
        ascii_edi = normalize_line_terminators(ascii_edi)
        self.edi = ascii_edi or edi
        if self.edi:
            self.fp = cStringIO.StringIO(self.edi)
            self.in_isa = False
            if type != 'only_create':
                message = EdiParser(self.edi, type='only_create')
                self.segments = [segment.split(message.delimiters[1]) for segment in message]

    def __xpath__(self, xpath):
        segs = Segments(self.segments)
        return segs.__xpath__(xpath)

    def __loop__(self, xpath_str):
        segs = Segments(self.segments)
        return segs.__loop__(xpath_str)

    def __iter__(self):
        """Return the iterator for use in a for loop"""
        return self

    def next(self):
        """return the next segment from the file or raise StopIteration

        Here we'll return the next segment, this will be a 'bare' segment
        without the segment terminator.

        We're using the array module.  Written in C this should be very
        efficient at adding and converting to a string.
        """
        seg = array.array('c')
        if not self.in_isa:
            #We're at the begining of a file or interchange so we need
            #to handle it specially.  We read in the first 105 bytes,
            #ignoring new lines.  After that we read in the segment
            #terminator.
            while len(seg) != 106:
                i = self.fp.read(1)
                if i == '\0':
                    continue
                if i == '':
                    if len(seg) == 0:
                        # We have reached the end of the file normally.
                        raise StopIteration
                    else:
                        # We have reached the end of the file, this is an error
                        # since we are in the middle of an ISA loop.
                        raise BadFile('Unexpected EOF found')
                if len(seg) < 105:
                    # While we're still gathering the 'main' portion of the
                    # ISA, we ignore NULLs and newlines.
                    if i != '\n':
                        # We're still in the 'middle' of the ISA, we won't
                        # accept NULLs or line feeds.
                        try:
                            seg.append(i)
                        except TypeError:
                            # This should never occur in a valid file.
                            print('Type error on appending "%s"' % i)
                else:
                    # We're at the end of the ISA, we'll accept *any*
                    # character except the NULL as the segment terminator for
                    # now.  We'll check for validity next.
                    if i == '\n':
                        # Since we're breaking some lines at position
                        # 80 on a given line, we need to also check the
                        # first character after the line break to make
                        # sure that the newline is supposed to be the
                        # terminator.  If it is, we just backup to
                        # reset the file pointer and move on.
                        pos = self.fp.tell()
                        next_char = self.fp.read(1)
                        if next_char != 'G':
                            i = next_char
                        else:
                            self.fp.seek(pos)
                    try:
                        seg.append(i)
                    except TypeError:
                        print('Type error on appending "%s"' % i)

            self.version = seg[84:89].tostring()
            self.delimiters = seg[105] + seg[3] + seg[104]
            if self.version >= '00405':
                self.delimiters = seg[105] + seg[3] + seg[104] + seg[83]

            # Verify that the delimiters are valid.
            for delim in self.delimiters:
                if delim in alphanums:
                    raise BadFile('"%s" is not a valid delimiter' % delim)
                    # Set the flag to process everything else as normal segments.
            self.in_isa = True

            # Pop off the segment terminator.
            seg.pop()
            return seg.tostring()
        else:
            #We're somewhere in the body of the X12 message.  We just
            #read until we find the segment terminator and return the
            #segment.  (We still ignore line feeds unless the line feed
            #is the segment terminator.
            fp_read = self.fp.read
            while 1:
                i = fp_read(1)
                if i == '\0':
                    continue
                if i == self.delimiters[0]:
                    # End of segment found, exit the loop and return the
                    # segment.
                    segment = seg.tostring()
                    if segment.startswith('IEA'):
                        self.in_isa = False
                    return segment
                elif i != '\n':
                    try:
                        seg.append(i)
                    except TypeError:
                        raise BadFile('Corrupt characters found in data or unexpected EOF')


if __name__ == '__main__':
    # Sample usage
    fp = open('/home/felix/Workspace/test_edi/edi.txt', "r")
    content = fp.read()
    fp.close()
    message = EdiParser(content)
    for segment in message:
        elements = segment.split(message.delimiters[1])
        print(elements)
        # Dispatch based on elements[0]...
