# -*- coding: utf-8 -*-

# Deprecated code. Need check, is it required or we may be removed at all
def collect_ring_sizes(confirmLines, line_sku, relationshipdetails_str, line_size):
    cust_first_flag = ""
    ring_sizes = {}

    for line in confirmLines:
        if cust_first_flag != line[line_sku] and line.get(line_sku, False):
            cust_first_flag = line[line_sku]
            ring_sizes[line[line_sku]] = relationshipdetails_str

        if line.get(line_sku, False) and line.get(line_size, False):
            ring_sizes[line[line_sku]] += str(line[line_size]) + ';'

    for line in ring_sizes:
        if ring_sizes[line] == relationshipdetails_str:
            ring_sizes[line] = ""
        if ring_sizes[line] != relationshipdetails_str:
            ring_sizes[line] = str(ring_sizes[line][:-1]).replace(".0", "")
    return ring_sizes
