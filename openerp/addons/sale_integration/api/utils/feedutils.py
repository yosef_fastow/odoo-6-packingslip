# -*- coding: utf-8 -*-
import csv
import cStringIO
import logging
from datetime import datetime


_logger = logging.getLogger(__name__)


class FeedUtills(object):

    def __init__(self, ini_settings, line):
        self.ini_settings = ini_settings
        self.line_data = line

    def _create_line(self, delimiter, skip_invalid_line=False, csv_flag=False, header=False, quote=False):
        result = []
        if header:
            for key, value in self.ini_settings:
                result.append(key)
        else:
            result = self._prepare_line_data(skip_invalid_line=skip_invalid_line)

        if not result:
            return False

        result = [type(el) is unicode and el.encode('utf-8') or str(el).encode('utf-8') for el in result]
        if csv_flag:
            if quote:
                quote_style = csv.QUOTE_ALL
            else:
                quote_style = csv.QUOTE_MINIMAL
            output = cStringIO.StringIO()
            writer = csv.writer(output, delimiter=delimiter, quoting=quote_style)
            writer.writerow(result)
            return output.getvalue()
        return "|".join(result) + "\n"

    def _prepare_line_data(self, skip_invalid_line=False):
        result = []
        for key, expr in self.ini_settings:
            if expr:
                alternatives = expr.split(' or ')
                count_alternatives = len(alternatives)
                for index, value in enumerate(alternatives):
                    value = (value or '').strip()
                    if value and value[0] == '$':
                        if value[1:] in ('False', 'None', ''):
                            result.append('')
                            break
                        line_value = ''
                        if value[1:] in self.line_data:
                            if isinstance(self.line_data[value[1:]], (unicode, str)):
                                self.line_data[value[1:]] = self.line_data[value[1:]].strip()
                            line_value = self.line_data[value[1:]] if self.line_data[value[1:]] not in ('False', 'None') else ''
                        if line_value != 0 and not line_value and value[1:] not in ['eta_date', 'eta', 'eta_qty']:
                            if index + 1 < count_alternatives:
                                continue
                            else:
                                if skip_invalid_line:
                                    return False
                                else:
                                    result.append('')
                                    break
                        else:
                            result.append(line_value)
                            break
                    else:
                        result.append(value)
                        break
            else:
                result.append(expr)
        return result

    def create_csv_line(self, delimiter=',', skip_invalid_line=False, quote=False):
        return self._create_line(delimiter, skip_invalid_line, csv_flag=True, quote=quote)

    def create_delimited_line(self, skip_invalid_line=False):
        return self._create_line(skip_invalid_line)

    def create_csv_header(self, delimiter=','):
        return self._create_line(delimiter, csv_flag=True, header=True)


class LineUtils(object):

    def __init__(self, line):
        self.line_data = line

    def validate_eta(self, field='eta', time_format="%Y%m%d"):
        self.line_data['eta_date'] = ''
        if (self.line_data[field] and self.line_data['qty'] in (0, '0')):
            try:
                eta = datetime.strptime(self.line_data[field], "%Y-%m-%d")
                if eta > datetime.now():
                    self.line_data['eta_date'] = eta.strftime(time_format)
                else:
                    raise ValueError('Too old ETA')
            except ValueError:
                _logger.error("""Wrong ETA date: {0}""".format(self.line_data[field]))

        return self.line_data

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: