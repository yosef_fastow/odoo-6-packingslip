# -*- coding: utf-8 -*-
import xml.dom.minidom as MD
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape, unescape
import re


class XmlUtills(object):
    obj = ""
    pretty_print = False

    def __init__(self, obj, pretty_print=False):
        self.obj = obj
        self.pretty_print = pretty_print

    def to_obj(self):
        return DictWrapper(self.obj).parsed

    def to_xml(self):
        if self.pretty_print:
            parsed_string = self._get_xml(self.obj)
            parsed_string = parsed_string.replace("&", "and")
            parsed_string = re.sub(r"[\x00-\x1F\x7F]", '', parsed_string)
            return MD.parseString(parsed_string).toprettyxml()
            #return MD.parseString(self._get_xml(self.obj).replace("&", "and")).toprettyxml()
        return self._get_xml(self.obj)

    def _get_xml(self, obj, objname=None):
        if(obj is None):
            return ""
        if not objname:
            objname = "Order"
        adapt = {
            dict: self._get_xml_dict,
            list: self._get_xml_list,
            tuple: self._get_xml_list,
        }
        if obj.__class__ in adapt:
            return adapt[obj.__class__](obj, objname)
        else:
            return "<%(n)s>%(o)s</%(n)s>" % {'n': objname, 'o': escape(isinstance(obj, unicode) and obj.decode('utf-8').encode('utf-8') or str(obj).decode('utf-8').encode('utf-8'))}

    def _get_xml_dict(self, indict, objname=None):
        h = "<%s>" % objname
        for k, v in indict.items():
            h += self._get_xml(v, k)
        h += "</%s>" % objname
        return h

    def _get_xml_list(self, inlist, objname=None):
        h = ""
        for i in inlist:
            h += self._get_xml(i, objname)
        return h


def remove_namespace(xml):
    regex = re.compile(' xmlns(:ns2)?="[^"]+"|(ns2:)|(xml:)')
    return regex.sub('', xml)


class DictWrapper(object):

    def __init__(self, xml, rootkey=None):
        self.original = xml
        self._rootkey = rootkey
        self._mydict = xml2dict().fromstring(remove_namespace(xml))
        self._response_dict = self._mydict.get(self._mydict.keys()[0],
                                               self._mydict)

    @property
    def parsed(self):
        if self._rootkey:
            return self._response_dict.get(self._rootkey)
        else:
            return self._response_dict


class object_dict(dict):

    def __init__(self, initd=None):
        if initd is None:
            initd = {}
        dict.__init__(self, initd)

    def __getattr__(self, item):

        d = self.__getitem__(item)

        if isinstance(d, dict) and 'value' in d and len(d) == 1:
            value = d['value']
            if value and isinstance(value, (str, unicode)):
                return unescape(value)
        else:
            return d

    def __setattr__(self, item, value):
        self.__setitem__(item, value)

    def getvalue(self, item, value=None):
        return self.get(item, {}).get('value', value)


class xml2dict(object):

    def __init__(self):
        pass

    def _parse_node(self, node):
        node_tree = object_dict()
        if node.text:
            node_tree.value = node.text
        for (k, v) in node.attrib.items():
            k, v = self._namespace_split(k, object_dict({'value': v}))
            node_tree[k] = v
        for child in node.getchildren():
            tag, tree = self._namespace_split(child.tag,
                                              self._parse_node(child))
            if tag not in node_tree:
                node_tree[tag] = tree
                continue
            old = node_tree[tag]
            if not isinstance(old, list):
                node_tree.pop(tag)
                node_tree[tag] = [old]
            node_tree[tag].append(tree)

        return node_tree

    def _namespace_split(self, tag, value):
        result = re.compile("\{(.*)\}(.*)").search(tag)
        if result:
            value.namespace, tag = result.groups()

        return (tag, value)

    def parse(self, file):
        f = open(file, 'r')
        return self.fromstring(f.read())

    def fromstring(self, s):
        s = s.encode('utf-8')
        t = ET.fromstring(s)
        root_tag, root_tree = self._namespace_split(t.tag, self._parse_node(t))
        return object_dict({root_tag: root_tree})
