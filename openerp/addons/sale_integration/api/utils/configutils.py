from yaml import dump as dump_config
from yaml import load as load_config
from os import access, F_OK, W_OK, R_OK


class Config():

    def __init__(self, filename=None):
        self.filename = filename

    def read(self):
        if not access(self.filename, F_OK):
            print("no find configuration file: %s!" % self.filename)
            result = False
        else:
            if access(self.filename, R_OK):
                with open(self.filename, "r") as config:
                    result = load_config(config.read())
            else:
                print("no access reading file: %s" % self.filename)
                result = False
        return result

    def write(self, data_to_config=None):
        result = False
        if not access(self.filename, F_OK):
            print("no find configuration file: %s, created new file!" % self.filename)
            with open(self.filename, "w+"):
                pass
        if access(self.filename, W_OK):
            with open(self.filename, "w") as config:
                if(data_to_config is None):
                    print("not find data to config!")
                    result = False
                else:
                    config.write(dump_config(data_to_config, default_style='"'))
                    result = True
        else:
            print("no access writing to file: %s" % self.filename)
        return result
