# -*- coding: utf-8 -*-
# from urllib2 import Request, urlopen, URLError
from lxml import etree
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime, timedelta
import time
from mws import mws
import os.path
import logging
from openerp.addons.pf_utils.utils.backup_file import backup_file_open
from pf_utils.utils.re_utils import f_d

# next_token = "token"

_logger = logging.getLogger(__name__)


class AmazonApi(ApiClient):

    nm = 'https://mws.amazonservices.com/Orders/2013-09-01'
    host = ""
    aws_access_key_id = ""
    secret_key = ""
    merchant_id = ""
    marketplace_id = []
    auth_token = ""
    next_token = "token"
    list_id_orders = "id"
    time_delta = 0
    send_feed = False

    def __init__(self, settings):
        self.host = settings["host"]
        self.aws_access_key_id = settings["aws_access_key_id"]
        self.secret_key = settings["secret_key"]
        self.merchant_id = settings["merchant_id"]
        self.marketplace_id = settings["marketplace_id"]
        self.auth_token = settings.get("auth_token", '')
        self.customer = settings["customer"]
        self._log = []

    def feeds_request(self, feed_type, data):
        feeds = mws.Feeds(str(self.aws_access_key_id), str(self.secret_key), str(self.merchant_id), str(self.host))

        # FIXME: [YT] temporary workaround for bug with comma-separated marketplaces

        marketplace_ids = self.marketplace_id or []
        if isinstance(marketplace_ids, (str, unicode)):
            marketplace_ids = [x.strip() for x in marketplace_ids.split(",") if x.strip()]

        response = feeds.submit_feed(
            feed=data,
            feed_type=feed_type,
            marketplaceids=marketplace_ids,
        )
        self._log.append({
            'title': "Response data, method: %s" % (self.name),
            'msg': 'Response data: \n %s' % (response.original),
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })

    def send(self):
        data = self.get_request_data()
        if (self.name == 'Acknowledgement'):
            feed_type = "_POST_ORDER_ACKNOWLEDGEMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'UpdateQTY'):
            feed_type = "_POST_INVENTORY_AVAILABILITY_DATA_"
            self.feeds_request(feed_type, data)
            try:
                with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename), 'w') as bf:
                    bf.write(str(data))
            except Exception, e:
                _logger.error(
                    """
                    Eror creating file inventory!
                    filename: %s,
                    _path_to_backup_local_dir: %s,
                    error message: %s
                    """
                    % (
                    self.filename,
                    self._path_to_backup_local_dir,
                    e.message))

        if (self.name == 'ConfirmOrders'):
            feed_type = "_POST_ORDER_FULFILLMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'ReturnOrders'):
            feed_type = "_POST_PAYMENT_ADJUSTMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'loadOrders'):
            response = {}
            try:
                marketplace_ids = self.marketplace_id or []
                if isinstance(marketplace_ids, (str, unicode)):
                    marketplace_ids = [x.strip() for x in marketplace_ids.split(",") if x.strip()]

                orders = mws.Orders(str(self.aws_access_key_id), str(self.secret_key), str(self.merchant_id), str(self.host), auth_token=str(self.auth_token))
                order_list = orders.list_orders(
                    marketplace_ids,
                    created_after=data['CreatedAfter'],
                    orderstatus=data['OrderStatus']
                )
                lines = []
                if (isinstance(order_list.parsed.get('Orders', {}).get('Order'), list)):
                    order_list_items = order_list.parsed.get('Orders', {}).get('Order', False)
                else:
                    order_list_items = [order_list.parsed.get('Orders', {}).get('Order', False)]
                for item in order_list_items:
                    time.sleep(5)
                    if not item:
                        continue
                    line = {}
                    line['amazon_order_id'] = item.AmazonOrderId
                    line['list_order_items'] = orders.list_order_items(amazon_order_id=item.AmazonOrderId)
                    lines.append(line)
                if not lines:
                    self._log.append({
                        'title': "Response data, method: %s" % (self.name),
                        'msg': 'Response data: Orders not found',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    return False
                response['lines'] = lines
                response['order_list'] = order_list
                xml_lines = ""
                for line in lines:
                    xml_lines += '\n %s ' % (line['list_order_items'].original)
                self._log.append({
                    'title': "Response data, method: %s" % (self.name),
                    'msg': 'Response data: \n Order list: \n %s \n Order items: \n %s' % (order_list.original, xml_lines),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            except Exception, e:
                print 'error method send loadOrders (Amazon): %s' % e
                self._log.append({
                    'title': 'error method send loadOrders (Amazon)',
                    'msg': e,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename), 'w') as bf:
                bf.write(str(response))
            return self.parse_response(response)

        if (self.name == 'SearchProducts'):
            products = mws.Products(str(self.aws_access_key_id), str(self.secret_key), str(self.merchant_id), str(self.host))
            response = []
            i = 0
            start = datetime.now().strftime('%H:%M:%S')
            while (data):
                i += 1
                print 'start time: %s' % (start)
                print 'current time: %s' % (datetime.now().strftime('%H:%M:%S'))
                print (i, data[:5])
                try:
                    resp = products.get_matching_product_for_id(str(self.marketplace_id[0]), 'SellerSKU', data[:5])
                    response.append(resp)
                    data = data[5:]
                except mws.MWSError, e:
                    if not resp:
                        resp = ''
                    if e[0] == '400 Client Error: None':
                        print '400 Client Error: None'
                        self._log.append({
                            'title': '400 Client Error: None',
                            'msg': 'error: %s \n response: %s \n, data: %s \n ' % (e, str(resp), str(data[:5])),
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                        data = data[5:]
                    elif e[0] == '503 Server Error: None':
                        print '503 Server Error: None'
                        i -= 1
                        time.sleep(0.6)
                except Exception, e:
                    if not resp:
                        resp = ''
                    print 'error method send SearchProducts (Amazon): %s' % e
                    self._log.append({
                        'title': 'error method send SearchProducts (Amazon)',
                        'msg': 'error: %s \n response: %s \n, data: %s \n ' % (e, str(resp), str(data[:5])),
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
            return self.parse_response(response)

        self._log.append({
            'title': "Request data, method: %s" % (self.name),
            'msg': 'Request data: \n' + str(data),
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })

    def sendFBA(self):
        data = self.get_request_data()
        response = {}

        if (self.name == 'Acknowledgement'):
            feed_type = "_POST_ORDER_ACKNOWLEDGEMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'UpdateQTY'):
            feed_type = "_POST_INVENTORY_AVAILABILITY_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'ConfirmOrders'):
            feed_type = "_POST_ORDER_FULFILLMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'ReturnOrders'):
            feed_type = "_POST_PAYMENT_ADJUSTMENT_DATA_"
            self.feeds_request(feed_type, data)

        if (self.name == 'SwitchProduct'):
            if (not self.send_feed):
                print 'Test'
            else:
                print 'not Test'
                # feed_type = "_POST_INVENTORY_AVAILABILITY_DATA_"
                # self.feeds_request(feed_type, data)

        if (self.name == 'loadOrdersFBA'):
            try:
                marketplace_ids = self.marketplace_id or []
                if isinstance(marketplace_ids, (str, unicode)):
                    marketplace_ids = [x.strip() for x in marketplace_ids.split(",") if x.strip()]

                orders = mws.Orders(str(self.aws_access_key_id), str(self.secret_key), str(self.merchant_id), str(self.host), auth_token=str(self.auth_token))
                if (self.next_token == "token" and self.list_id_orders == "id"):
                    order_list = orders.list_orders(
                        marketplace_ids,
                        created_after=data['CreatedAfter'],
                        fulfillment_channels=(['AFN'])
                    )
                elif (self.next_token != "token" and self.list_id_orders == "id"):
                    order_list = orders.list_orders_by_next_token(self.next_token)
                elif (self.list_id_orders != "id"):
                    list_orders_get = self.list_id_orders.split(',')
                    list_orders_get_array_item = []
                    list_orders_get_array = []
                    count = 0
                    for order in list_orders_get:
                        list_orders_get_array_item.append(str(order))
                        count = count + 1
                        if (count < 30):
                            continue
                        else:
                            list_orders_get_array.append(list_orders_get_array_item)
                            list_orders_get_array_item = []
                            count = 0
                    if (list_orders_get_array_item != []):
                        list_orders_get_array.append(list_orders_get_array_item)
                    order_list = []
                    # for item in list_orders_get_array:
                    #     orders_get = orders.get_order(item)
                    order_list = orders.get_order(list_orders_get_array[0])
                flag_token = True
                count = 0
                responseList = []
                # while(flag_token):
                lines = []
                if (isinstance(order_list.parsed.get('Orders', {}).get('Order'), list)):
                    order_list_items = order_list.parsed.get('Orders', {}).get('Order')
                else:
                    order_list_items = [order_list.parsed.get('Orders', {}).get('Order')]
                for item in order_list_items:
                    time.sleep(5)
                    if not item:
                        continue
                    line = {}
                    line['amazon_order_id'] = item.AmazonOrderId
                    connected = False
                    count_iter_items = 0
                    while not connected:
                        try:
                            line['list_order_items'] = orders.list_order_items(amazon_order_id=item.AmazonOrderId)
                            lines.append(line)
                            connected = True
                        except mws.MWSError, error:
                            response = getattr(error, 'response', None) or {}
                            status_code = response.get('status_code', False) or False
                            if (status_code and status_code in (500, 503)):
                                count_iter_items = count_iter_items + 1
                                if (count_iter_items >= 2):
                                    break
                                time.sleep(10)
                            else:
                                raise error
                        except Exception, e:
                            print 'error method send loadOrdersFBA get list_order_items(Amazon): %s' % e
                            self._log.append({
                                'title': 'error method send loadOrdersFBA get list_order_items(Amazon)',
                                'msg': e,
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            break
                response['lines'] = lines
                response['order_list'] = order_list
                xml_lines = ""
                for line in lines:
                    xml_lines += '\n %s ' % (line['list_order_items'].original)
                self._log.append({
                    'title': "Response data, method: %s" % (self.name),
                    'msg': 'Response data: \n Order list: \n %s \n Order items: \n %s' % (order_list.original, xml_lines),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                # responseList.append(response)
                self.next_token = order_list.parsed.get('NextToken', {}).get('value', False)
                # fr = open("/tmp/next_token.xml", "w")
                # fr.write(self.next_token)
                # fr.close()
                # if (next_token):
                #     flag_token = True
                #     order_list = orders.list_orders_by_next_token(next_token)
                #     count = count + 1
                #     print next_token
                # else:
                #     flag_token = False
            except Exception, e:
                print 'error method send loadOrdersFBA (Amazon): %s' % e
                self._log.append({
                    'title': 'error method send loadOrdersFBA (Amazon)',
                    'msg': e,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            return self.parse_response(response)
        self._log.append({
            'title': "Request data, method: %s" % (self.name),
            'msg': 'Request data: \n' + str(data),
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })

    def check_response(self, response):
        return True


class AmazonApiClient(AbsApiClient):

    next_token_Amazon = ""

    settings = {
        "host": "",
        "aws_access_key_id": "",
        "secret_key": "",
        "merchant_id": "",
        "marketplace_id": [],
        "auth_token": "",
        "customer": "",
        "backup_local_path": ""
    }

    def __init__(self, settings_variables):
        super(AmazonApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = AmazonApiGetOrdersXML

    def updateQTY(self, lines, mode=None):
        updateApi = AmazonApiUpdateQTY(self.settings, lines)
        updateApi.send()
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def SwitchProduct(self, lines, linesAFN, sendFeed):
        # import pdb; pdb.set_trace()
        updateApi = AmazonApiSwitchProduct(self.settings, lines, linesAFN)
        updateApi.send_feed = sendFeed
        updateApi.sendFBA()
        self.extend_log(updateApi)
        return True

    def loadOrders(self):
        orders = self.load_orders_api.send()
        self.extend_log(self.load_orders_api)
        return orders

    def loadOrdersFBA(self, token="", id_list="id", time=1, limit=0):
        ordersApi = AmazonApiGetOrdersFBAXML(self.settings, limit)
        ordersApi.next_token = token
        ordersApi.time_delta = time
        ordersApi.list_id_orders = id_list
        print ordersApi.next_token
        orders = ordersApi.sendFBA()
        self.extend_log(ordersApi)
        self.next_token_Amazon = ordersApi.next_token
        return orders

    def getOrdersObj(self, xml):
        ordersApi = AmazonApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def Acknowledgement(self, order):
        AcknowledgementApi = AmazonApiAcknowledgement(self.settings, order)
        try:
            AcknowledgementApi.send()
            self.extend_log(AcknowledgementApi)
        except:
            pass
        return True

    def confirmShipment(self, lines):
        # FIXME: [IB] Right now only for amazon, but need to add general setting
        if self.settings.get('skip_shipping_confirmation', '').lower().strip() in ['true', '1', 'yes']:
            return True

        confirmApi = AmazonApiConfirmOrders(self.settings, lines)
        confirmApi.send()
        self.extend_log(confirmApi)
        return True

    def returnOrders(self, lines):
        ReturnApi = AmazonApiReturnOrders(self.settings, lines)
        try:
            ReturnApi.send()
            self.extend_log(ReturnApi)
        except:
            pass
        return True

    def searchProducts(self, products):
        searchApi = AmazonApiSearchProducts(self.settings, products)
        tag = searchApi.send()
        self.extend_log(searchApi)
        return tag


class AmazonApiGetOrdersXML(AmazonApi):

    name = "loadOrders"

    def __init__(self, settings):
        super(AmazonApiGetOrdersXML, self).__init__(settings)
        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            settings['customer'],
            'load_orders',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'amazon'),
            datetime.utcnow()
        )

    def get_request_data(self):
        data = {
            'CreatedAfter': (datetime.now().date() - timedelta(days=30)).isoformat(),
            'OrderStatus': ('Unshipped', 'PartiallyShipped')
        }
        return data

    def parse_response(self, response):
        if(not response):
            return []
        ordersList = []
        tree = etree.XML(response['order_list'].original)
        root = tree.find('{%s}ListOrdersResult'.replace("%s", self.nm))
        orders_tag = root.find('{%s}Orders'.replace("%s", self.nm))
        orders = orders_tag.findall('{%s}Order'.replace("%s", self.nm))

        for order in orders:
            for line in response['lines']:
                if(line['amazon_order_id'] == order.findtext('{%s}AmazonOrderId'.replace("%s", self.nm))):
                    tree = etree.XML(line['list_order_items'].original)
                    root = tree.find('{%s}ListOrderItemsResult'.replace("%s", self.nm))
                    order_items = root.find('{%s}OrderItems'.replace("%s", self.nm))
                    order.append(order_items)

                    ordersObj = {}
                    ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                    ordersObj['name'] = order.findtext('{%s}AmazonOrderId'.replace("%s", self.nm))
                    ordersList.append(ordersObj)
        return ordersList

class AmazonApiGetOrdersFBAXML(AmazonApi):

    name = "loadOrdersFBA"

    def __init__(self, settings, limit):
        super(AmazonApiGetOrdersFBAXML, self).__init__(settings)
        self.limit = limit

    def get_request_data(self):
        data = {
            'CreatedAfter': (datetime.now().date() - timedelta(days=int(self.time_delta))).isoformat(),
            'OrderStatus': ('Unshipped', 'PartiallyShipped')
        }
        print time
        return data

    def parse_response(self, response):
        if(not response):
            return []
        ordersList = []
        # for response in responseList:
        tree = etree.XML(response['order_list'].original)
        root = tree.find('{%s}ListOrdersResult'.replace("%s", self.nm)) or tree.find('{%s}ListOrdersByNextTokenResult'.replace("%s", self.nm)) or tree.find('{%s}GetOrderResult'.replace("%s", self.nm))
        orders_tag = root.find('{%s}Orders'.replace("%s", self.nm))
        orders = orders_tag.findall('{%s}Order'.replace("%s", self.nm))

        for order in orders:
            if (order.findtext('{%s}OrderStatus'.replace("%s", self.nm)) == 'Canceled' or order.findtext('{%s}OrderStatus'.replace("%s", self.nm)) == 'Pending'):
                continue
            for line in response['lines']:
                if(line['amazon_order_id'] == order.findtext('{%s}AmazonOrderId'.replace("%s", self.nm))):
                    tree = etree.XML(line['list_order_items'].original)
                    root = tree.find('{%s}ListOrderItemsResult'.replace("%s", self.nm))
                    order_items = root.find('{%s}OrderItems'.replace("%s", self.nm))
                    order.append(order_items)

                    ordersObj = {}
                    ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                    ordersObj['name'] = order.findtext('{%s}AmazonOrderId'.replace("%s", self.nm))
                    ordersList.append(ordersObj)
                    if(self.limit != 0 and len(ordersList) > self.limit):
                        break
        return ordersList


class AmazonApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        ordersObj = {}
        if(self.xml == ""):
            return ordersObj
        order = mws.DictWrapper(self.xml).parsed
        if(order is not None):
            ordersObj['partner_id'] = order.get('MarketplaceId', {}).get('value', False)
            ordersObj['order_id'] = order.get('AmazonOrderId', {}).get('value', False)
            ordersObj['carrier'] = order.get('ShipmentServiceLevelCategory', {}).get('value', False)
            ordersObj['external_date_order'] = order.get('PurchaseDate', {}).get('value', False)
            ordersObj['fulfillment_channels'] = order.get('FulfillmentChannel', {}).get('value', False)
            shipping_address = order.get('ShippingAddress', {})
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['name'] = shipping_address.get('Name', {}).get('value', False)
            ordersObj['address']['ship']['address1'] = shipping_address.get('AddressLine1', {}).get('value', False)
            ordersObj['address']['ship']['address2'] = shipping_address.get('AddressLine2', {}).get('value', False)
            ordersObj['address']['ship']['city'] = shipping_address.get('City', {}).get('value', False)
            ordersObj['address']['ship']['state'] = shipping_address.get('StateOrRegion', {}).get('value', False)
            ordersObj['address']['ship']['zip'] = shipping_address.get('PostalCode', {}).get('value', False)
            ordersObj['address']['ship']['country'] = shipping_address.get('CountryCode', {}).get('value', False)
            ordersObj['address']['ship']['phone'] = shipping_address.get('Phone', {}).get('value', False)
            ordersObj['lines'] = []
            if (isinstance(order.get('OrderItems', {}).get('OrderItem', {}), list)):
                lines = order.get('OrderItems', {}).get('OrderItem', {})
            else:
                lines = [order.get('OrderItems', {}).get('OrderItem', {})]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('OrderItemId', {}).get('value', False)
                sku = lineItem.get('SellerSKU', {}).get('value', False)
                if(sku.find("/") == -1):
                    lineItemObj['sku'] = sku
                    lineItemObj['size'] = False
                else:
                    lineItemObj['sku'] = sku[:sku.find("/")]
                    lineItemObj['size'] = sku[sku.find("/") + 1:]
                lineItemObj['merchantSKU'] = lineItemObj['sku']
                lineItemObj['qty'] = lineItem.get('QuantityOrdered', {}).get('value', False)
                lineItemObj['ASIN'] = lineItem.get('ASIN', {}).get('value', False)
                lineItemObj['name'] = lineItem.get('Title', {}).get('value', False)
                if float(lineItemObj['qty']):
                    if (lineItem.get('ItemPrice', {}).get('CurrencyCode', {}).get('value', False) == 'JPY'):
                        lineItemObj['cost'] = float(lineItem.get('ItemPrice', {}).get('Amount', {}).get('value', 0)) / (float(lineItemObj['qty'])*100)
                    else:
                        lineItemObj['cost'] = float(lineItem.get('ItemPrice', {}).get('Amount', {}).get('value', 0)) / float(lineItemObj['qty'])
                        # lineItemObj['cost'] = lineItem.get('ItemPrice', {}).get('Amount', {}).get('value', False)
                else:
                    lineItemObj['cost'] = 0.0
                lineItemObj['shipCost'] = lineItem.get('ShippingPrice', {}).get('Amount', {}).get('value', False)
                ordersObj['lines'].append(lineItemObj)
        return ordersObj

class AmazonApiAcknowledgement(AmazonApi):
    confirmLines = []
    name = "Acknowledgement"

    def __init__(self, settings, lines):
        super(AmazonApiAcknowledgement, self).__init__(settings)
        self.confirmLines = lines

    def get_request_data(self):
        data = """<?xml version="1.0"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
        <Header>
            <DocumentVersion>1.01</DocumentVersion>
            <MerchantIdentifier>%s</MerchantIdentifier>
        </Header>
        <MessageType>OrderAcknowledgment</MessageType>
        %s
        </AmazonEnvelope>
        """

        lines = ""
        message_id = 0
        order = self.confirmLines
        for line in self.confirmLines['lines']:
            message_id += 1
            lines += """
            <Message>
                <MessageID>%s</MessageID>
                <OrderAcknowledgement>
                    <AmazonOrderID>%s</AmazonOrderID>
                    <StatusCode>%s</StatusCode>
                    <Item>
                        <AmazonOrderItemCode>%s</AmazonOrderItemCode>
                    </Item>
                </OrderAcknowledgement>
            </Message>
            """ % (message_id, order['order_id'], order['state'], line['order_item_id'])
        print data % (self.merchant_id, lines)
        return data % (self.merchant_id, lines)


class AmazonApiUpdateQTY(AmazonApi):

    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(AmazonApiUpdateQTY, self).__init__(settings)
        self.confirmLines = lines
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'], "amazon", "inventory", "now") or ""
        self.filename = "AMAZONINVENTORY%s.xml" % \
            datetime.now().strftime("%Y%m%d%H%M%S%f")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def get_request_data(self):

        data = """<?xml version="1.0" encoding="utf-8" ?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
            <Header>
                <DocumentVersion>1.01</DocumentVersion>
                <MerchantIdentifier>%s</MerchantIdentifier>
            </Header>
            <MessageType>Inventory</MessageType>
            %s
        </AmazonEnvelope>
        """
        lines = ""
        message_id = 0
        fulfillment_latency = 1
        if self.customer in ('AMK'):
            fulfillment_latency = 7

        for line in self.confirmLines:
            message_id += 1

            if not line.get('customer_sku', False):
                continue

            sku = str(line['customer_sku'])
            if (sku[-2:] == '.0'):
                sku = sku[:-2]

            lines += """
            <Message>
                <MessageID>%s</MessageID>
                <OperationType>Update</OperationType>
                <Inventory>
                    <SKU>%s</SKU>
                    <Quantity>%s</Quantity>
                    <FulfillmentLatency>%s</FulfillmentLatency>
                </Inventory>
            </Message>
            """ % (message_id, sku, line['qty'], fulfillment_latency)
            self.append_to_revision_lines(line, "good")
        return data % (self.merchant_id, lines)


class AmazonApiSwitchProduct(AmazonApi):

    confirmLinesMFN = []
    confirmLinesAFN = []

    name = "SwitchProduct"

    def __init__(self, settings, lines, linesAFN):
        super(AmazonApiSwitchProduct, self).__init__(settings)
        self.confirmLinesMFN = lines
        self.confirmLinesAFN = linesAFN

    def get_request_data(self):
        # import pdb; pdb.set_trace()
        data = """<?xml version="1.0" encoding="utf-8" ?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
            <Header>
                <DocumentVersion>1.01</DocumentVersion>
                <MerchantIdentifier>%s</MerchantIdentifier>
            </Header>
            <MessageType>Inventory</MessageType>
            %s
        </AmazonEnvelope>
        """
        lines = ""
        message_id = 0
        fulfillment_center_id = "AMAZON_NA"
        # if self.customer in ('AMK'):
        #     fulfillment_latency = 5

        #Переключение на MFN
        for line in self.confirmLinesMFN:
            message_id += 1
            sku = str(line['sku'])
            if (sku[-2:] == '.0'):
                sku = sku[:-2]

            lines += """
            <Message>
                <MessageID>%s</MessageID>
                <OperationType>Update</OperationType>
                <Inventory>
                    <SKU>%s</SKU>
                    <FulfillmentCenterID>DEFAULT</FulfillmentCenterID>
                    <Quantity>%s</Quantity>
                    <SwitchFulfillmentTo>MFN</SwitchFulfillmentTo>
                </Inventory>
            </Message>
            """ % (message_id, sku, line['qty'])

        #Переключение на AFN
        for line in self.confirmLinesAFN:
            message_id += 1
            sku = str(line['sku'])
            if (sku[-2:] == '.0'):
                sku = sku[:-2]

            lines += """
            <Message>
                <MessageID>%s</MessageID>
                <OperationType>Update</OperationType>
                <Inventory>
                    <SKU>%s</SKU>
                    <FulfillmentCenterID>%s</FulfillmentCenterID>
                    <Lookup>FulfillmentNetwork</Lookup>
                    <SwitchFulfillmentTo>AFN</SwitchFulfillmentTo>
                </Inventory>
            </Message>
            """ % (message_id, sku, fulfillment_center_id)
        # import pdb; pdb.set_trace()
        return data % (self.merchant_id, lines)


class AmazonApiConfirmOrders(AmazonApi):

    confirmLines = []
    name = "ConfirmOrders"

    def __init__(self, settings, lines):
        super(AmazonApiConfirmOrders, self).__init__(settings)
        self.confirmLines = lines

    def get_request_data(self):
        data = """<?xml version="1.0" encoding="UTF-8"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
        <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>%s</MerchantIdentifier>
        </Header>
        <MessageType>OrderFulfillment</MessageType>
        %s
        </AmazonEnvelope>
        """

        lines = ""
        message_id = 0
        for line in self.confirmLines:
            message_id += 1
            dt = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            lines += """
            <Message>
                <MessageID>%s</MessageID>
                <OrderFulfillment>
                    <AmazonOrderID>%s</AmazonOrderID>
                    <FulfillmentDate>%s</FulfillmentDate>
                    <FulfillmentData>
                        <CarrierCode>%s</CarrierCode>
                        <ShippingMethod>%s</ShippingMethod>
                        <ShipperTrackingNumber>%s</ShipperTrackingNumber>
                    </FulfillmentData>
                    <Item>
                        <AmazonOrderItemCode>%s</AmazonOrderItemCode>
                        <Quantity>%s</Quantity>
                    </Item>
                </OrderFulfillment>
            </Message>
            """ % (message_id,
                   line['external_customer_order_id'],
                   dt,
                   line["carrier_code"],
                   line["carrier"],
                   line['tracking_number'],
                   str(line['external_customer_line_id']),
                   int(line['product_qty']))
        return data % (self.merchant_id, lines)


class AmazonApiReturnOrders(AmazonApi):
    returnLines = []
    name = "ReturnOrders"

    def __init__(self, settings, lines):
        super(AmazonApiReturnOrders, self).__init__(settings)
        self.returnLines = lines

    def get_request_data(self):
        data = """<?xml version="1.0" encoding="UTF-8"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-
        envelope.xsd">
            <Header>
                <DocumentVersion>1.01</DocumentVersion>
                <MerchantIdentifier>%s</MerchantIdentifier>
            </Header>
            <MessageType>OrderAdjustment</MessageType>
            %s
        </AmazonEnvelope>
        """

        lines = ""
        message_id = 0
        for line in self.returnLines:
            message_id += 1
            if(line['shipCost']):
                shipCost = str(line['shipCost'])
            else:
                shipCost = '0.00'
            lines += """
            <Message>
            <MessageID>%s</MessageID>
                <OrderAdjustment>
                    <AmazonOrderID>%s</AmazonOrderID>
                    <AdjustedItem>
                        <MerchantOrderItemID>%s</MerchantOrderItemID>
                        <AdjustmentReason>CustomerReturn</AdjustmentReason>
                        <ItemPriceAdjustments>
                            <Component>
                                <Type>Principal</Type>
                                <Amount currency="USD">%s</Amount>
                            </Component>
                            <Component>
                                <Type>Shipping</Type>
                                <Amount currency="USD">%s</Amount>
                            </Component>
                        </ItemPriceAdjustments>
                    </AdjustedItem>
                </OrderAdjustment>
            </Message>
            """ % (message_id,
                   line['external_customer_order_id'],
                   str(line['external_customer_line_id']),
                   str(line['ItemPrice']),
                   shipCost
                   )
        return data % (self.merchant_id, lines)


class AmazonApiSearchProducts(AmazonApi):
    name = "SearchProducts"
    list_products = []

    def __init__(self, settings, products):
        super(AmazonApiSearchProducts, self).__init__(settings)
        self.list_products = products

    def get_request_data(self):
        return self.list_products

    def parse_response(self, response):
        if(not response):
            return []
        tag = []
        for items in response:
            for item in items.parsed:
                if item.get('status', {}).get('value', '') and item.status == 'Success':
                    tag.append(item.Id)
        return tag
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
