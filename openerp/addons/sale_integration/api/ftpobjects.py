# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from os.path import join as join_path
from os import listdir
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from openerp.addons.pf_utils.utils.backup_file import backup_file_open

_logger = logging.getLogger(__name__)

from ftplib import FTP_TLS
import socket
import ssl


class FTP_TLS_fixed(FTP_TLS):
    def __init__(self, host='', user='', passwd='', acct='', keyfile=None,
                 certfile=None, context=None, timeout=60):
        FTP_TLS.__init__(
            self, host, user, passwd, acct, keyfile, certfile, context, timeout)

    def connect(self, host='', port=0, timeout=-999):
        """Connect to host.  Arguments are:
        - host: hostname to connect to (string, default previous host)
        - port: port to connect to (integer, default previous port)
        """
        if host != '':
            self.host = host
        if port > 0:
            self.port = port
        if timeout != -999:
            self.timeout = timeout
        try:
            self.sock = socket.create_connection(
                (self.host, self.port), self.timeout)
            self.af = self.sock.family
            # add this line!!!
            self.sock = ssl.wrap_socket(self.sock, self.keyfile, self.certfile, ssl_version=ssl.PROTOCOL_TLSv1)
            # add end
            self.file = self.sock.makefile('rb')
            self.welcome = self.getresp()
        except Exception as e:
            print e
        return self.welcome


class FTPObjectException(Exception):
    pass


class FTPObject(object):
    settings = None
    _ftp_host = ''
    _ftp_user = ''
    _ftp_passwd = ''
    _ftp_use_keys = False
    _ftp_timeout = 10
    _ftp_port = False
    _path_to_local_dir = ''
    _path_to_backup_local_dir = ''
    _path_to_ftp_dir = ''
    _remove_flag = False
    _ftp_ssl = False
    _ftp_ssh = False
    _ftp_passive_mode = False
    _ftp_file_mask = None
    _parent = None
    _root_dir = None
    _settings_name = None
    _connected = None
    _customer_ref = None
    _not_to_upload_the_same_files = False
    _use_decrypt = None
    _use_encrypt = None
    _send_correct = None
    _send_failed = None
    _setting_action = None

    def __init__(self, parent, settings):
        super(FTPObject, self).__init__()
        self.settings = settings
        self._parent = parent
        self._root_dir = settings['root_dir']
        self._setting_name = settings['setting_name']
        self.set_ftp_settings()

    def set_ftp_settings(self):
        port = self.settings['ftp_port']
        ftp_ssl = (self.settings['ftp_security'] == 'ssl')
        ftp_ssh = (self.settings['ftp_security'] == 'ssh')
        ftp_path = self.settings['ftp_path']
        if not port:
            if ftp_ssh:
                port = 22
            elif ftp_ssl:
                port = 990
            else:
                port = 21

        self._ftp_host = self.settings["ftp_host"]
        self._ftp_user = self.settings["ftp_user"]
        self._ftp_passwd = self.settings["ftp_pass"]
        self._ftp_use_keys = self.settings["ftp_use_keys"]
        self._remove_flag = True
        self._ftp_ssl = ftp_ssl
        self._ftp_ssh = ftp_ssh
        self._ftp_passive_mode = self.settings['ftp_passive_mode']
        self._ftp_port = port
        self._ftp_file_mask = self.settings['ftp_file_mask']
        self._ftp_timeout = self.settings['ftp_timeout'] or 10
        self._use_decrypt = self.settings['use_decrypt'] or False
        self._use_encrypt = self.settings['use_encrypt'] or False
        path_list = {
            'path_to_ftp_dir': ftp_path,
            'path_to_local_dir': self._parent.create_local_dir(
                self._parent.input_local_path,
                self._root_dir,
                self._setting_name
            ),
            'path_to_backup_local_dir': self._parent.create_local_dir(
                self._parent.backup_local_path,
                self._root_dir,
                self._setting_name,
                "now"
            )
        }
        self.set_path_list(path_list)
        self._customer_ref = self.settings['customer_ref']
        self._setting_action = self.settings['setting_action']
        self.copy_attributes_to_parent(self.settings.get('attribute_list', None))
        if getattr(self._parent, 'not_to_upload_the_same_files', False):
            self._not_to_upload_the_same_files = True
        return True

    def copy_attributes_to_parent(self, attribute_list=None):
        if attribute_list is None:
            attribute_list = []
        for attribute_name in attribute_list:
            if hasattr(self, attribute_name):
                exec ("self._parent.{0} = self.{0}".format(attribute_name))
        return True

    def set_path_list(self, path_list):
        """
        @param path_list: dict('path_to_ftp_dir': '', 'path_to_local_dir': '', 'path_to_backup_local_dir': '')
        @return: None
        """
        for path in path_list:
            if not path[-1] == '/':
                path += '/'
        self._path_to_ftp_dir = path_list.get('path_to_ftp_dir', '/')
        self._path_to_local_dir = path_list.get('path_to_local_dir', '/')
        self._path_to_backup_local_dir = path_list.get('path_to_backup_local_dir', '/tmp/')
        return True

    def connect(self):
        self._connected = False
        return False

    def disconnect(self):
        self._connected = False
        return False

    def get_filenames(self):
        return []

    def exclude_bad_filenames(self, _filenames=None):
        import re
        filenames = []
        if _filenames is None:
            _filenames = []
        file_mask = re.compile(self._ftp_file_mask) if self._ftp_file_mask else None
        for filename in _filenames:
            if (
                (not filename) or
                (filename.startswith('.')) or
                (file_mask and not file_mask.match(filename))
            ):
                continue
            else:
                filenames.append(filename)
        return filenames

    def full_(self, filename):
        return join_path(self._path_to_ftp_dir, filename)

    def file_exist_in_folder(self, file_data, folder):
        import time
        from os.path import isdir
        file_exist = False
        try:
            latest_symbols = folder[-10:]
            time.strptime(latest_symbols, '%Y_%m_%d')
            path = join_path("/", "/".join(folder.split('/')[1:-1]))
            if isdir(path):
                folder = path
        except Exception:
            pass
        self._parent.log({
            'type': 'debug',
            'message': "Search duplicates files in {0}".format(folder),
        })
        try:
            filenames = blf_utils.get_list_files(folder)
        except OSError as os_err:
            if os_err.strerror == 'No such file or directory':
                filenames = []
            else:
                raise
        for filename in filenames:
            with open(filename) as f:
                backup_file_data = f.read()
            if file_data == backup_file_data:
                file_exist = True
                break
        return file_exist

    def save_file(self, filename, write_data):
        create_file = True

        if self._not_to_upload_the_same_files:
            file_exist = self.file_exist_in_folder(write_data, self._path_to_backup_local_dir)
            if file_exist:
                create_file = False

        if create_file:
            now = "{:%Y%m%d_%H%M}".format(datetime.now())
            filename = join_path(self._path_to_local_dir, filename + '_' + now)
            with backup_file_open(filename, 'wb') as f:
                f.write(str(write_data))
            result = "filename: {full_filename}\ndata: \n{data}\n".format(
                full_filename=self.full_(filename),
                data=write_data
            )
        else:
            result = "File: {data} not creating in folder: {folder}".format(
                data=write_data,
                folder=self._path_to_local_dir
            )
        return result

    def backup_file(self, filename, write_data):
        with backup_file_open(join_path(self._path_to_backup_local_dir, filename), 'wb') as f:
            f.write(str(write_data))
        return True

    @property
    def send_correct(self):
        return self._send_correct

    @send_correct.setter
    def send_correct(self, value):
        self._send_correct = value

    @property
    def send_failed(self):
        return self._send_failed

    @send_failed.setter
    def send_failed(self, msg):
        self._send_failed = True
        self.send_correct = False
        self._parent.log({
            'title': 'Send File to FTP/SFTP Failed!',
            'type': 'error',
            'message': msg,
        })


class FTPNormal(FTPObject):
    _ftp = None

    def __init__(self, parent, settings):
        super(FTPNormal, self).__init__(parent, settings)

    def connect(self):
        self._connected = None
        from ftplib import FTP
        import ftplib
        self._ftp = FTP()
        try:
            self._ftp.connect(self._ftp_host, port=self._ftp_port, timeout=self._ftp_timeout)
            self._ftp.login(self._ftp_user, self._ftp_passwd)
            self._ftp.set_pasv(self._ftp_passive_mode)
            self._connected = True
        except ftplib.all_errors as resp:
            self._ftp = None
            msg = "Can't connect to ftp. {0}".format(resp)
            self._parent.log({
                'title': msg,
                'type': 'error',
                'message': msg,
            })
            self._connected = False
        finally:
            return self._connected

    def disconnect(self):
        if self._connected:
            try:
                self._ftp.quit()
            except EOFError:
                self._ftp.close()
        self._connected = False
        return True

    def get_filenames(self):
        from ftplib import error_perm
        filenames = []
        if self._connected:
            self._ftp.set_debuglevel(2)
            self._ftp.cwd(self._path_to_ftp_dir)
            try:
                filenames = self._ftp.nlst()
            except error_perm as resp:
                msg = resp.message
                if msg != "550 No files found.":
                    msg = "No files in this directory {0}".format(self._path_to_ftp_dir)
                    self._parent.log({
                        'title': msg,
                        'type': 'debug',
                        'message': msg,
                    })
            try:
                self._ftp.retrlines('MLSD', lambda line: filenames.append(line.split()[-1]))
            except error_perm as resp:
                msg = resp.message
                if msg != "502 Command MLSD not implemented":
                    self._parent.log({
                        'type': 'debug',
                        'message': msg,
                    })
            filenames = list(set(filenames))
            if not filenames:
                msg = "No files in this directory {0}".format(self._path_to_ftp_dir)
                self._parent.log({
                    'title': msg,
                    'type': 'debug',
                    'message': msg,
                })
        else:
            raise FTPObjectException('Not connected now!')
        if filenames:
            filenames = self.exclude_bad_filenames(filenames)
        return filenames

    def get_file_data(self, filename):
        from ftplib import error_perm
        import cStringIO
        io = cStringIO.StringIO()
        if self._connected:
            try:
                self._ftp.retrbinary('RETR ' + filename, io.write)
                result = io.getvalue()
            except error_perm:
                result = False
        else:
            raise FTPObjectException('Not connected now!')
        return result

    def delete_file_on_ftp(self, filename):
        from ftplib import error_perm
        if self._connected:
            try:
                self._ftp.delete(filename)
            except error_perm:
                self._parent.log({
                    'title': 'Not delete file on ftp.',
                    'type': 'debug',
                    'message': 'Not delete file {0} on FTP'.format(filename),
                })
        else:
            raise FTPObjectException('Not connected now!')
        return True

    def upload_file_to_ftp(self, filename, IO):
        if self._connected:
            try:
                self._ftp.cwd(self._path_to_ftp_dir)
                self._ftp.storbinary('STOR ' + filename, IO)
                self.send_correct = True
                _logger.warn('UPLOAD_FTPNormal: %s%s' % (self._path_to_ftp_dir, filename))
            except Exception as ex:
                self.send_failed = str(ex)
        else:
            self.send_failed = 'Not connected now!'
            return False
        return self.send_correct


class FTPOverSSHObject(FTPObject):
    _connection = None
    _sftp = None

    def __init__(self, parent, settings):
        super(FTPOverSSHObject, self).__init__(parent, settings)
        self._default_key = 'id_rsa_42'
        self._ssh_keys_path = '/home/delmar/.ssh/'

    def _set_key(self):
        self._ssh_key = None
        if self._default_key in listdir(self._ssh_keys_path):
            self._ssh_key =  self._ssh_keys_path + self._default_key

    def connect(self):
        self._connected = None
        from paramiko import Transport, SFTPClient, SSHClient, AutoAddPolicy
        from paramiko.ssh_exception import SSHException, AuthenticationException, BadHostKeyException
        try:
            if self._ftp_use_keys:
                self._set_key()
                # Use generic shell login (using Keys)
                self._connection = SSHClient()
                self._connection.set_missing_host_key_policy(AutoAddPolicy())
                self._connection.connect(self._ftp_host, self._ftp_port, self._ftp_user, self._ftp_passwd, key_filename=self._ssh_key)
                self._sftp = self._connection.open_sftp()
            else:
                # Use direct connection to FTP server
                self._connection = Transport((self._ftp_host, self._ftp_port))
                self._connection.connect(username=self._ftp_user, password=self._ftp_passwd)
                self._sftp = SFTPClient.from_transport(self._connection)
            self._connected = True
        except AuthenticationException as auth_err:
            title = 'AuthenticationException'
            msg = str(auth_err)
            self._parent.log({
                'title': title,
                'type': 'error',
                'message': msg,
            })
            self._connected = False
        except SSHException as ssh_err:
            title = 'SSHException'
            msg = "Can't connect to sftp. {message}".format(message=ssh_err.message)
            self._parent.log({
                'title': title,
                'type': 'error',
                'message': msg,
            })
            self._connected = False
        except BadHostKeyException as key_err:
            title = 'BadHostKeyException'
            msg = "Can't connect to sftp. {message}".format(message=key_err.message)
            self._parent.log({
                'title': title,
                'type': 'error',
                'message': msg,
            })
            self._connected = False
        finally:
            return self._connected

    def disconnect(self):
        if self._connected:
            self._sftp.close()
            self._connection.close()
        self._connected = False
        return True

    def get_filenames(self):
        filenames = []
        if self._connected:
            try:
                filenames = self._sftp.listdir(self._path_to_ftp_dir)
            except IOError as io_err:
                if io_err.strerror == 'No such file':
                    msg = 'No such folder {0} on server {1}'.format(self._path_to_ftp_dir, self._ftp_host)
                    self._parent.log({
                        'title': msg,
                        'type': 'debug',
                        'message': msg,
                    })
                else:
                    raise
        else:
            raise FTPObjectException('Not connected now!')
        if filenames:
            filenames = self.exclude_bad_filenames(filenames)
        return filenames

    def get_file_data(self, filename):
        import cStringIO
        io = cStringIO.StringIO()
        if self._connected:
            try:
                self._sftp.getfo(self.full_(filename), io)
                result = io.getvalue()
            except IOError:
                result = False
        else:
            raise FTPObjectException('Not connected now!')
        return result

    def delete_file_on_ftp(self, filename):
        if self._connected:
            try:
                self._sftp.unlink(self.full_(filename))
            except IOError as io_err:
                if io_err.strerror == 'Permission denied':
                    pass
                else:
                    raise
        else:
            raise FTPObjectException('Not connected now!')
        return True

    def upload_file_to_ftp(self, filename, IO):
        if self._connected:
            try:
                self._sftp.putfo(IO, self.full_(filename))
                self.send_correct = True
                _logger.warn('UPLOAD_FTPOverSSH: %s' % self.full_(filename))
            except Exception as ex:
                self.send_failed = str(ex)
        else:
            self.send_failed = 'Not connected now!'
        return self.send_correct


class FTPOverSSLObject(FTPObject):
    _ftps = None

    def __init__(self, parent, settings):
        super(FTPOverSSLObject, self).__init__(parent, settings)

    def connect(self):
        self._connected = None
        import ftplib
        self._ftps = FTP_TLS_fixed()
        try:
            self._ftps.connect(self._ftp_host, port=self._ftp_port, timeout=self._ftp_timeout)
            self._ftps.login(self._ftp_user, self._ftp_passwd)
            self._ftps.prot_p()
            self._ftps.set_pasv(self._ftp_passive_mode)
            self._connected = True
        except ftplib.all_errors as resp:
            self._ftps = None
            msg = "Can't connect to ftp over SSL. {0}".format(resp)
            self._parent.log({
                'title': msg,
                'type': 'error',
                'message': msg,
            })
            self._connected = False
        finally:
            return self._connected

    def disconnect(self):
        if self._connected:
            try:
                self._ftps.quit()
            except EOFError:
                self._ftps.close()
        self._connected = False
        return True

    def get_filenames(self):
        from ftplib import error_perm
        from ssl import SSLError
        filenames = []
        if self._connected:
            self._ftps.set_debuglevel(2)
            self._ftps.cwd(self._path_to_ftp_dir)
            try:
                filenames = self._ftps.nlst()
            except error_perm as resp:
                if str(resp) == "550 No files found.":
                    msg = "No files in this directory {0}".format(self._path_to_ftp_dir)
                    self._parent.log({
                        'title': msg,
                        'type': 'debug',
                        'message': msg,
                    })
                else:
                    raise
            except SSLError as ssl_err:
                if 'timed out' in ssl_err.message:
                    self._parent.log({
                        'title': 'SSLError',
                        'type': 'error',
                        'message': """
                            Most commonly occurs when using active FTPS mode.
                            Please check to use passive mode.
                        """,
                    })
                else:
                    raise
        else:
            raise FTPObjectException('Not connected now!')
        if filenames:
            filenames = self.exclude_bad_filenames(filenames)
        return filenames

    def get_file_data(self, filename):
        from ftplib import error_perm
        import cStringIO
        IO = cStringIO.StringIO()
        if self._connected:
            try:
                self._ftps.retrbinary('RETR ' + filename, IO.write)
                result = IO.getvalue()
            except error_perm:
                result = False
        else:
            raise FTPObjectException('Not connected now!')
            result = False
        return result

    def delete_file_on_ftp(self, filename):
        from ftplib import error_perm
        if self._connected:
            try:
                self._ftps.delete(filename)
            except error_perm:
                self._parent.log({
                    'title': 'Not delete file on ftps.',
                    'type': 'debug',
                    'message': 'Not delete file {0} on FTPS'.format(filename),
                })
        else:
            raise FTPObjectException('Not connected now!')
        return True

    def upload_file_to_ftp(self, filename, IO):
        if self._connected:
            try:
                self._ftps.cwd(self._path_to_ftp_dir)
                self._ftps.storbinary('STOR ' + filename, IO)
                self.send_correct = True
                _logger.warn('UPLOAD_FTPOverSSL: %s%s' % (self._path_to_ftp_dir, filename))
            except Exception as ex:
                self.send_failed = str(ex)
        else:
            self.send_failed = 'Not connected now!'
        return self.send_correct
