# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from customer_parsers.jtv_input_csv_parser import CSVParser
from datetime import datetime
import logging
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class JTVApi(FtpClient):

    def __init__(self, settings):
        super(JTVApi, self).__init__(settings)
        self._remove_flag = False

    def upload_data(self):
        return {'lines': self.lines}


class JTVApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(JTVApiClient, self).__init__(
            settings_variables, JTVOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = JTVApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='normal'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        self.settings["order_type"] = order_type
        api = JTVApiGetOrdersXML(self.settings)
        if save_flag:
            orders = api.process('load')
        else:
            orders = api.process('read_new')
        self.extend_log(api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        processingApi = JTVApiCancelOrderLines(self.settings, lines, state)
        res = processingApi.process('send')
        self.extend_log(processingApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = JTVApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class JTVApiGetOrdersXML(JTVApi):

    def __init__(self, settings):
        super(JTVApiGetOrdersXML, self).__init__(settings)
        self.order_type = settings.get('order_type', 'normal')
        self.use_ftp_settings('load_orders')
        self.parser = CSVParser(self.customer, settings)


class JTVApiUpdateQTY(JTVApi):

    def __init__(self, settings, lines):
        super(JTVApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "JTVInventory.csv"  # + order number
        self.filename_local = "{:JTVINVENTORY%Y%m%d%H%M.xml}".format(
            datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if((not line.get('sku', False)) and line.get('sku', False) == ''):
                self.append_to_revision_lines(line, 'bad')
                continue
            if(not line.get('customer_price', False)):
                line['customer_price'] = ''
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class JTVApiConfirmOrders(JTVApi):

    def __init__(self, settings, lines):
        super(JTVApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("Confirm%Y%m%d-%H%M.csv")


class JTVApiCancelOrderLines(JTVApi):

    def __init__(self, settings, lines, state):
        super(JTVApiCancelOrderLines, self).__init__(settings)
        self.lines = lines
        self.state = state
        self.type_tpl = "string"
        self.use_mako_templates('cancel')
        self.use_ftp_settings('confirm_orders')
        self.filename = f_d('Cancel%Y%m%d-%H%M.csv')

    def upload_data(self):
        lines = self.lines
        return {'lines': lines}


class JTVOpenerp(ApiOpenerp):

    def __init__(self):
        super(JTVOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        additional_fields = [
            (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
            for key, value in line.get('additional_fields', {}).iteritems()
        ]

        price_unit = (line.get('additional_fields', False) and line['additional_fields'].get('price_unit', False)) or 0
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': '0.0',
            'customerCost': price_unit,
            'customer_sku': line['customer_sku'],
            'vendorSku': line['customer_sku'],
            'qty': line['qty'],
            'sku': line['sku'],
            'size': line['size'],
            'id': line['id'],
            'additional_fields': additional_fields,
        }

        product = False

        field_list = ['customer_sku', 'sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'customer_id_delmar',
                        'default_code',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
