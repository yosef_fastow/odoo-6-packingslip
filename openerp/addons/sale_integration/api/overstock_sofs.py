# -*- coding: utf-8 -*-
from httpauthapiclient import HTTPWithAuthApiClient
from abstract_apiclient import AbsApiClient
from customer_parsers.sofs_input_xml_parser import XMLParser
from apiopenerp import ApiOpenerp
from datetime import datetime
from datetime import timedelta
from pf_utils.utils.re_utils import f_d

import logging
logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_http': True,
}


class OverstockSOFSApi(HTTPWithAuthApiClient):
    def __init__(self, settings):
        super(OverstockSOFSApi, self).__init__(settings)

    def parse_files(self, filenames=None):
        # Use only latest file with bunch of orders
        if filenames:
            filenames = [max(filenames)]
        return super(OverstockSOFSApi, self).parse_files(filenames)


class OverstockSOFSApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(OverstockSOFSApiClient, self).__init__(
            settings_variables, OverstockSOFSOpenerp, False, False)
        self.load_orders_api = OverstockSOFSApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = OverstockSOFSApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = OverstockSOFSApiUpdateQTY(self.settings, lines)
        updateApi.upload_data()
        res = updateApi.getXml()
        # Comment if not want to save data on Inventory management
        self.backupInventoryFeed(res, updateApi)
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return res

    def backupInventoryFeed(self, res, api):
        api.use_mako_templates('upload_file')
        feed = api.prepare_data({'lines': res})
        api.backup_file(api.actual_local_filename, feed)
        self.check_and_set_filename_inventory(api)

    def uploadFeed(self, lines):
        updateApi = OverstockSOFSApiUploadFeed(self.settings, lines)
        res = updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        return res


class OverstockSOFSApiGetOrders(OverstockSOFSApi):
    '''This call retrieves all sales orders that were created
    or modified by fulfillment activity during the time
    range specified in the request.
    '''

    def __init__(self, settings):
        super(OverstockSOFSApiGetOrders, self).__init__(settings)
        self.use_http_settings('load_orders')
        self.parser = XMLParser(self)
        self.filename = f_d('ORDERS_%Y%m%d_%H%M%S_%f', datetime.utcnow())

    @property
    def sub_request_kwargs(self):
        return {
            'headers': {'Accept': 'application/xml', 'Content-Type': 'application/xml'},
        }


class OverstockSOFSApiConfirmShipment(OverstockSOFSApi):

    def __init__(self, settings, lines):
        super(OverstockSOFSApiConfirmShipment, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d('CONFIRM%Y%m%d-%H%M.xml', datetime.utcnow())

    @property
    def sub_request_kwargs(self):
        return {
            'data': self.prepare_data(self.upload_data()),
            'headers': {'Content-Type': 'application/xml'},
        }

    def _split_carrier(self, carrier):
        carrier_parts = carrier.split(',')
        res1 = carrier_parts[0].strip()
        res2 = False
        if len(carrier_parts) > 1:
            res2 = carrier_parts[1].strip()
        return res1, res2

    def upload_data(self):
        lines = self.lines
        for line in lines:
            amount_line = 0.0
            if(line.get('price_unit', False) and line.get('product_qty', False)):
                price_unit = 0.0
                product_qty = 0
                try:
                    price_unit = float(line['price_unit'])
                except ValueError:
                    pass
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
                if(price_unit and product_qty):
                    amount_line = price_unit * product_qty
            line['amount_line'] = amount_line
            if line.get('shp_date', False):
                date_obj = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            else:
                date_obj = datetime.utcnow()
            date_now = f_d("%m/%d/%y", date_obj)
            carrier, service = self._split_carrier(line['carrier_code'])
            if not service:
                service = self._split_carrier(line['carrier'])[1] or 'GROUND'
            line['carrier_code'] = carrier
            line['servicelevel'] = service
            line['date'] = date_now
            line['timestamp'] = date_obj.isoformat() + '+00:00'
            line['product_qty'] = int(line['product_qty'])
        return {'lines': lines}


class OverstockSOFSApiUpdateQTY(OverstockSOFSApi):

    def __init__(self, settings, lines):
        super(OverstockSOFSApiUpdateQTY, self).__init__(settings)
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename_local = f_d('INVENTORY%Y%m%d%H%M.xml', datetime.utcnow())
        self.filename_readable = f_d('SOFS-READABLE%Y%m%d%H%M.csv', datetime.utcnow())
        self._path_to_backup_local_dir = self.create_local_dir(
            self.backup_local_path, "overstock_sofs", "inventory", "now"
        )
        self._clean_revision_lines()

    def _clean_revision_lines(self):
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if not line.get('customer_id_delmar'):
                self.append_to_revision_lines(line, 'bad')
                continue
            line['customer_sku'] = line['customer_id_delmar']
            line['backorderTimestamp'] = ''

            """
            if line.get('eta', False) not in (0, '0', False) and line.get('actual_qty') in (0, '0', False):
                if line.get('previous_qty', False) in (0, '0', False):
                    try:
                        if int(line['eta'][-4:]) >= 1900:
                            date_obj = datetime.strptime(line['eta'], "%m/%d/%Y")
                            date_obj = timedelta(days=28) + date_obj
                            line['backorderTimestamp'] = date_obj.isoformat() + '.000000+00:00'

                        if line.get('ordered_qty') not in (0,'0', False):
                            line['qty'] = int(line.get('ordered_qty'))
                        else:
                            line['qty'] = 1
                    except:
                        pass
            """
            #if(line['customer_price'] is None):
            #    line['customer_price'] = ''
            now_time = datetime.utcnow()
            line['timestamp'] = now_time.isoformat() + '+00:00'
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}

    def getXml(self):
        res = []
        for line in self.revision_lines['good']:
            if line['qty_tolerance'] > 3:
                line['qty'] = line['qty'] - int(line['qty_tolerance']) + 3
            xml = self.prepare_data({'lines': [line]})
            res.append({'xml': xml, 'line': line})
        return res


class OverstockSOFSApiUploadFeed(OverstockSOFSApi):

    def __init__(self, settings, lines):
        super(OverstockSOFSApiUploadFeed, self).__init__(settings)
        self.use_http_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates('upload_file')
        self.lines = lines
        self.filename_local = f_d('INVENTORY%Y%m%d%H%M%s_%f.xml', datetime.utcnow())
        self._path_to_backup_local_dir = self.create_local_dir(self.backup_local_path, "overstock_sofs", "inventory", "now")

    @property
    def sub_request_kwargs(self):
        request = super(OverstockSOFSApiUploadFeed, self).sub_request_kwargs
        headers = request.get('headers', {})
        headers.update({'Content-Type': 'application/xml', 'Accept': 'application/xml'})
        xml_data = self.prepare_data({'lines': self.lines})
        request.update({'data': xml_data, 'headers': headers})
        logger.info("OVERSTOCK REQUEST DATA: %s" % request.get('data'))
        logger.info("OVERSTOCK REQUEST HEADERS: %s" % request.get('headers'))
        return request


class OverstockSOFSOpenerp(ApiOpenerp):

    def __init__(self):
        super(OverstockSOFSOpenerp, self).__init__()

    def generate_duplicate_xml_email(self, cr, uid, xml_name, duplicate_xml_ids):
        return False

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = order.get('additional_fields')
        if additional_fields:
            wrapped_fields = self.wrap_additional_fields(additional_fields)
            return {'additional_fields': wrapped_fields}
        else:
            return {}

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        id_delmar_os = self.pool.get(
            'sale.integration').search_id_delmar_by_OS_sku(
                cr, uid, line['sku'])
        additional_fields = line.get('additional_fields', {})
        line_obj = {
            'notes': '',
            'sku': line['sku'],
            'price_unit': line.get('price_unit', False),
            'customerCost': line.get('cost', False),
            'merchantSKU': line['customer_sku'],
            'optionSku': additional_fields.get(
                    'salesChannelSKU', ''),
            'merchantLineNumber': additional_fields.get(
                    'salesChannelLineNumber', ''),
            'additional_fields': self.wrap_additional_fields(
                additional_fields),
            'id_delmar_os': id_delmar_os,
        }

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = (
            'default_code', 'customer_sku', 'upc',
            'os_product_sku', 'customer_id_delmar')
        product = {}
        field_list = ('optionSku', 'id_delmar_os', 'sku', 'merchantSKU',)

        for field_name in field_list:
            field = line_obj[field_name]
            if field:
                line_obj['sku'] = field
                #if('/' in line_obj['sku']):
                #    try:
                #        _sku = line_obj['sku']
                #        _size = float(_sku[_sku.find("/") + 1:])
                #        if(_size):
                #            line_obj['sku'] = _sku[:_sku.find("/")]
                #            field = _sku[:_sku.find("/")]
                #            line_obj['size'] = _size
                #    except:
                #        pass

                product, size = product_obj.search_product_by_id(
                    cr, uid, context['customer_id'], field, search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line_obj.get('size', False):
                        try:
                            size_str = str(float(line_obj['size']))
                            size_ids = size_obj.search(
                                cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
            if not line_obj.get('price_unit', False):
                price_unit = product_obj.get_val_by_label(
                    cr, uid, product.id, context['customer_id'],
                    'Customer Price', line_obj.get('size_id', False)
                )
                if price_unit:
                    line['price_unit'] = price_unit
                else:
                    line_obj['notes'] += \
                        'Can\'t find price unit for name %s' % (line['name'])
        else:
            line_obj['notes'] = \
                'Can\'t find product by sku {0}.\n'.format(line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_insert_sale_integration_line(self, cr, uid, settings, updateqty_res):
        main_sql = """INSERT INTO
                sale_integration_{type_api}_xml
                (
                    create_date, send, delmar_id, customer_sku, size,
                    phantom_inventory, force_zero, status,
                    qty, actual_qty, xml, sale_integration_id, product_id, previous_qty
                )
            VALUES\n""".format(type_api=settings.type_api)
        sub_sqls = []
        for updateqty_line in updateqty_res:


            sub_sql = """(
                                                    (now() at time zone 'UTC'), {send}, '{delmar_id}', '{customer_sku}', '{size}',
                                                    {phantom_inventory}, {force_zero}, {status},
                                                    {qty}, {actual_qty}, '{xml}', {sale_integration_id}, {product_id}, {previous_qty}
                                                )""".format(
                send=0,
                delmar_id=updateqty_line['line']['sku'],
                customer_sku=updateqty_line['line']['customer_sku'],
                size=updateqty_line['line']['size'],
                phantom_inventory=updateqty_line['line'].get('phantom_inventory', None) or 0,
                force_zero=updateqty_line['line'].get('force_zero', None) or 0,
                status=updateqty_line['line']['qty'] > 0 and 1 or 0,
                qty=updateqty_line['line']['qty'],
                actual_qty=updateqty_line['line']['actual_qty'],
                xml=updateqty_line['xml'],
                sale_integration_id=settings.id,
                product_id=updateqty_line['line']['product_id']

            )
            sub_sqls.append(sub_sql)
        main_sql += ",\n".join(sub_sqls) + ';'

        return main_sql

    def get_prev_qtys(self, cr, uid, settings):
        cr.execute("""select customer_sku, actual_qty from sale_integration_overstock_sofs_xml where sale_integration_id = {id} order by id desc """.format(id=settings.id))
        res = {}
        for el in cr.dictfetchall():
            if res.get(el['customer_sku'], None) == None:
                res.update({el['customer_sku']: el['actual_qty']})

        return res
