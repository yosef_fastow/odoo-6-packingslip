# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.boscov_edi_parser import EDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime, timedelta
import logging
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import random
from json import dumps as json_dumps
from json import loads as json_loads
from math import ceil
from pf_utils.utils.re_utils import f_d
from dateutil import tz

_logger = logging.getLogger(__name__)

SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '72209'),
    ('vendor_name',                 'Vendor name',              'Boscov'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '014492501SDF'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    '01'),
    ('contact_name',                'Contact name',             ''),
    ('contact_phone',               'Contact phone',            ''),
    ('edi_x12_version',             'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.x12'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      '^'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class BoscovApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class BoscovApiClient(AbsApiClient):
    use_local_folder = True

    def __init__(self, settings_variables):
        super(BoscovApiClient, self).__init__(
            settings_variables, BoscovOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = BoscovApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = BoscovApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):

        confirmApi = BoscovApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required and lines[0]['action'] == 'v_ship':
            invoiceApi = BoscovApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = BoscovApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def returnOrders(self, lines):
        ReturnApi = BoscovApiReturnOrders(self.settings, lines)
        ReturnApi.process('send')
        self.extend_log(ReturnApi)
        return True


class BoscovApiGetOrders(BoscovApi):
    """EDI/V5010 X12/850: AAFESS2S Purchase Orders"""

    def __init__(self, settings):
        super(BoscovApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class BoscovOpenerp(ApiOpenerp):
    number_of_transaction = 1

    def __init__(self):

        super(BoscovOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
                [
                    (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                    for key, value in order.get('additional_fields', {}).iteritems()
                ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line
        additional_fields = []
        line_obj.update({
            'notes': '',
            'sku': line['vendorSku'],
            'vendorSku': line['vendorSku'],
            'cost': line['cost'],
            'upc': line['upc'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        if line.get('poLineData').get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
            }))

        if line.get('poLineData').get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
            }))
        if line.get('poLineData').get('UP'):
            additional_fields.append((0, 0, {
                'name': 'UP',
                'label': 'UP',
                'value': line.get('poLineData').get('UP', '')
            }))
        line_obj.update({'additional_fields': additional_fields})

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}

        additional_fields = {x[2]['name']: x[2]['value'] for x in line.get('additional_fields', []) or [] if x and x[2]}
        field_list = ['vendorSku', 'sku', 'upc', 'merchantSKU']

        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field],
                                                                 search_by)
                if product:
                    line_obj['size_id'] = size and size.id or False
                    line_obj['product_id'] = product.id
                    break
        if product and not line_obj['product_id']:
            line_obj['product_id'] = product.id

        if not  product:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(
                ','.join(
                    [
                        additional_fields.get(x, None)
                        for x in field_list if additional_fields.get(x, None)
                    ]
                )
            )
            line_obj['product_id'] = False

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        line = {
            'address_services': {
                'remmit_to': {
                    'edi_code': 'RI',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
            }
        }
        if (sale_obj.partner_invoice_id):
            line['address_services']['remmit_to'].update({
                'name': 'FIRST CANADIAN DIAMOND CUTTING WORKS',
                'address1': sale_obj.partner_invoice_id.street or '',
                'address2': sale_obj.partner_invoice_id.street2 or '',
                'city': sale_obj.partner_invoice_id.city or '',
                'state': sale_obj.partner_invoice_id.state_id.code or '',
                'country': sale_obj.partner_invoice_id.country_id.code or '',
                'zip': sale_obj.partner_invoice_id.zip or '',
            })
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })
        return line

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):
        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_line_id': order_line.external_customer_line_id,
            'product_qty': order_line.product_uom_qty,

        }

        return line


class BoscovApiConfirmShipment(BoscovApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(BoscovApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]%(str_code)s'

        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        str_code = '0001'
        if self.confirmLines[0]['action'] == 'v_cancel':
            str_code = '0004'
        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'] or False,
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time,
            'str_code': str_code,
        }

        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),
            'hl_number_prev': str(''),
            'code': 'S'

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]B*[2]*[3]*[4]*[5]%(carrier)s'

        # todo carrier_code
        td5_data = {
            'carrier': self.confirmLines[0].get('carrier_code', '')
        }
        segments.append(self.insertToStr(td5, td5_data))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))


        if self.confirmLines[0]['action'] == 'v_ship':
            hl_number_prev = hl_number
            hl_number += 1
            hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'P'  # Order
            }))
            man = 'MAN*[1]CP*[2]%(track)s'
            segments.append(self.insertToStr(man, {
                'track': self.confirmLines[0]['tracking_number'],
            }))


        lin = 'LIN*' \
              '[1]%(line_id)s*' \
              '[2]VN*' \
              '[3]%(sku)s*' \
              '[4]UP*' \
              '[5]%(upc)s'

        item_statuses = ['AC', 'ID']

        sn1 = 'SN1*' \
              '[1]*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]*' \
              '[5]%(qty_ordered)s*' \
              '[6]%(man_code)s*' \
              '[7]*' \
              '[8]%(item_status)s'

        if self.confirmLines[0]['action'] == 'v_cancel':
            sn1 = 'SN1*' \
                  '[1]*' \
                  '[2]%(qty)s*' \
                  '[3]EA*' \
                  '[4]*' \
                  '[5]%(qty_cancel)s*' \
                  '[6]%(man_code)s*' \
                  '[7]*' \
                  '[8]%(item_status)s'

        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            product_qualifier = None
            product_identifying_number = line.get('order_additional_fields',{}).get('internal_vendor_number','')
	    up = False
	    if line.get('additional_fields', False):
		up = line['additional_fields'].get('UP', False)
            segments.append(self.insertToStr(lin, {
                'line_id': line['external_customer_line_id'],
                'product_qualifier': product_qualifier,

                'upc': up,
                'sku': line['vendorSku'],

                'customer_id_delmar': line['customer_id_delmar'],
                'merchantSKU': line['merchantSKU'],
            }))

            if line['action'] == 'v_cancel':
                segments.append(self.insertToStr(sn1, {
                    'qty': 0,
                    'qty_cancel': line['product_qty'],
                    'man_code': 'EA',
                    'item_status': 'ID',
                }))
            else:
                segments.append(self.insertToStr(sn1, {
                    'qty': line['product_qty'],
                    'qty_ordered': '',
                    'man_code': '',
                    'item_status': 'AC',
                }))

            ref = 'REF*[1]TD*[2]%(sku)s*[3]%(data)s'
            segments.append(self.insertToStr(ref, {
                'sku': line['vendorSku'],
                'data': line['order_additional_fields'] and
                        line['order_additional_fields'].get('internal_vendor_number', None) or
                        self.vendor_number
            }))


        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class BoscovApiUpdateQTY(BoscovApi):
    """EDI/V4030 X12/846: 846 Inventory Inquiry"""

    def __init__(self, settings, lines):
        super(BoscovApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]00*[2]SI*[3]%(inventory_feed_qualifier)s*[4]%(date)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'date': date.strftime('%Y%m%d'),
            'inventory_feed_qualifier': inventory_feed_qualifier,
        }))

        ref = 'REF*[1]IO*[2]Boscovs'
        segments.append(self.insertToStr(ref, {
            'data': self.vendor_number,
        }))

        lin = 'LIN*' \
              '[1]%(product_qualifier)s*' \
              '[2]VN*' \
              '[3]%(product_identifying_number)s*' \
              '[4]UP*' \
              '[5]%(sku)s'
#             '[5]%(sku)s*' \
#              '[6]SK*' \
#              '[7]%(customer_id_delmar)s'

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        ctp = 'CTP*[1]*[2]UCP*[3]%(price)s'
        qty = 'QTY*[1]%(qty_q)s*[2]%(qty)s'
        for line in self.updateLines:
            if line.get('upc', False) in (None, False) or line.get('customer_id_delmar', False) in (None, False):
                continue
            eta_date = False
            try:
                if line.get('eta', False):
                    tmp_date = datetime.strptime(line['eta'], "%m/%d/%Y").replace(tzinfo=self.tz)
                    if tmp_date > date:
                        eta_date = tmp_date
            except Exception:
                pass
            segments.append(self.insertToStr(lin, {
                'product_qualifier': '1',
                'product_identifying_number': line.get('customer_id_delmar', False) or '',
                'sku': str(line['upc']),
                'customer_id_delmar': line.get('customer_id_delmar', False) or ''
            }))

            dtm_code = '036'  # Available
            dtm_date = date

            segments.append(self.insertToStr(dtm, {
                'code': dtm_code,
                'date': dtm_date.strftime('%Y%m%d')
            }))

            # todo: no unit_price here!
            segments.append(self.insertToStr(ctp, {
                'price': str(line.get('unit_price', ''))
            }))

            segments.append(self.insertToStr(qty, {
                'qty': str(line['qty']),
                'qty_q': str(61)
            }))
            self.append_to_revision_lines(line, 'good')
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))
        return self.wrap(segments, {'group': 'IB'})


class BoscovApiInvoiceOrders(BoscovApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(BoscovApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        it1 = 'IT1*' \
              '[1]%(line_number)s*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]%(unit_price)s*' \
              '[5]*' \
              '[6]%(pq)s*' \
              '[7]%(product_identifying_number)s'

        line_number = 0
        total_qty_invoiced = 0
        total_price_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = line['product_qty']
            except ValueError:
                qty = 1

            product_qualifier = None
            product_identifying_number = ''
	    pq = ''
            for p_qualifier in product_qualifiers:
                if (line['additional_fields'].get(p_qualifier, False)):
                    product_qualifier = p_qualifier
                    product_identifying_number = line['additional_fields'][p_qualifier]
		    pq = p_qualifier
                    break
            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': int(qty),
                'unit_price': line['unit_cost'],
                'product_identifying_number': str(product_identifying_number),
		'pq': str(pq)
                # 'sku': line['vendorSku'],
                # 'customer_id_delmar': line['customer_id_delmar'],
                # 'merchantSKU': line['merchantSKU'],
            }))
            total_qty_invoiced += qty
            total_price_invoiced += qty * float(line['unit_cost'])

        tds = 'TDS*[1]%(total_amount)s'

        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(total_price_invoiced or 0.0)).replace('.', ''),
        }))

        # C040 Delivery
        # D500 Handling
        # D240 Freight
        # H770 Tax – State Tax
        # codes_indefine = ['C040', 'D500', 'D240', 'H770']
        #sac = 'SAC*[1]C*[2]C040*[3]*[4]*[3]%(monetary_amount)s'
        #segments.append(self.insertToStr(sac, {
        #    'monetary_amount': total_price_invoiced or 0,
        #}))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class BoscovApiReturnOrders(BoscovApi):
    """EDI/V4030 X12/180: 180 Return"""
    lines = []

    def __init__(self, settings, lines):
        super(BoscovApiReturnOrders, self).__init__(settings)
        self.lines = lines
        self.use_ftp_settings('return_orders')
        self.edi_type = '180'

    def upload_data(self):
        segments = []
        date_now = datetime.now()

        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': '180',
            'st_number': st_number,
        }
        segments.append(self.insertToStr(st, st_data))
        bgn = 'BGN*[1]00*[2]%(ref)s*[3]%(date)s'

        bgn_data = {
            'ref': self.lines[0]['order_additional_fields'].get('REF02', None),
            'date': date_now.strftime('%Y%m%d'),
        }
        segments.append(self.insertToStr(bgn, bgn_data))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.lines[0]['po_number'],
        }))

        bli = 'BLI*[1]VN*[2]%(indef_num1)s*[3]%(qty)s*[4]*[5]*[6]*[7]*[8]UP*[9]%(indef_num2)s'
        n9 = 'N9*[1]RZ*%(number_of_transaction)s'
        rdr = 'RDR*[1]*[2]SP'
        prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]*[5]%(assigned_identification)s'
        dtm = 'DTM*[1]050*[2]%(date)s'
	line_number = 0
        for line in self.lines:
	    line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(bli, {
                'indef_num1': str(line.get('customer_id_delmar', '')),  # line['vendorSku'],
                'qty': qty,
                'indef_num2': str(line.get('upc', ''))
            }))
	   
            segments.append(self.insertToStr(n9, {
                'number_of_transaction': str(line_number),
            }))
            segments.append(self.insertToStr(rdr, {}))

            segments.append(self.insertToStr(prf, {
                'po_number': line['po_number'],
                'assigned_identification': str(line_number),
            }))

            segments.append(self.insertToStr(dtm, {
                'date': date_now.strftime('%Y%m%d'),
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'AN'})

class BoscovApiFunctionalAcknoledgment(BoscovApi):
    orders = []

    def __init__(self, settings, orders):
        super(BoscovApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        k = 0
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                k += 1
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak2 = 'AK2*[1]850*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak2, {
                    'ack_control_number': "%04d" % int(order['ack_control_number'])
                }))
                ak5 = 'AK5*A'
                segments.append(self.insertToStr(ak5, {
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order.get('number_of_transaction',1)
                }))

                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))
                data.append(self.wrap(segments, {'group': 'FA'}))
        return data

