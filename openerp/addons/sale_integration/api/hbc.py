# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.hbc_input_edi_parser import HBCEDIParser
import random
from datetime import datetime
from apiopenerp import ApiOpenerp
import logging
from dateutil import tz

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            1224),
    ('vendor_name',                 'Vendor name',              "Hudson’s Bay Company"),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '2123913344'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    'ZZ'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'DELMAR_{edi_type}_{date}.out'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '}'),
    ('environment_mode',            'Environment Mode',         'P'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


# TODO: In this customer mixed PO and External Cuatomer order id.
# Need to fix: parser, confirmations, packing...


class HBCApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class HBCApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(HBCApiClient, self).__init__(
            settings_variables, HBCOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = HBCApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = HBCApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        #if self.is_invoice_required:
        #    invoiceApi = HBCApiInvoiceOrders(self.settings, lines)
        #    invoiceApi.process('send')
        #    self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = HBCApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def processingOrderLines(self, lines, state='accepted'):
        # ordersApi = HBCApiProcessingOrderLines(self.settings, lines, state)
        # res = ordersApi.process('send')
        # self.extend_log(ordersApi)
        return True


class HBCApiGetOrders(HBCApi):
    """EDI/V4010 X12/850: VendorNet 850 EDI Purchase Order"""

    def __init__(self, settings):
        super(HBCApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = HBCEDIParser(self)
        self.edi_type = '850'


class HBCApiConfirmShipment(HBCApi):
    """EDI/V4010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(HBCApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]%(hierarchical_structure_code)s'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time,
            'hierarchical_structure_code': '0001',  # Shipment, Order, Packaging, Item
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        # Availaible only:
        # CANP – Canada Post Standard
        # FEDX– FedEx Standard
        # PURG – Purolator Ground
        # UPSN – UPS Ground
        availaible_carriers = ['CANP', 'FEDX', 'PURG', 'UPSN']
        carrier = self.confirmLines[0]['carrier_code']
        if carrier not in availaible_carriers:
            raise Exception(
                'Bad carrier: {carrier}, availaible only: {availaible_carriers}'.format(
                    carrier=carrier,
                    availaible_carriers=', '.join(['"{0}"'.format(carr) for carr in availaible_carriers])
                )
            )

        td5 = 'TD5*[1]*[2]*[3]%(carrier)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier_code']
        }
        segments.append(self.insertToStr(td5, td5_data))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        # Loop HL Order
        hl_number_prev = hl_number
        hl_number += 1

        hl_data = {
            'hl_number': str(hl_number),  # unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),  # Identification number of the next higher hierarchical data segment that the data segment being described is subordinate to
            'code': 'O'  # Order

        }
        segments.append(self.insertToStr(hl, hl_data))

        prf = 'PRF*[1]%(po_number)s'
        po_number = self.confirmLines[0]['order_additional_fields'].get('customer#', False)
        if not po_number:
            raise NotImplementedError('Not found order number')
        prf_data = {
            'po_number': po_number,
        }
        segments.append(self.insertToStr(prf, prf_data))

        # Loop HL (Packaging)
        hl_number_prev = hl_number
        hl_number += 1

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),  # Identification number of the next higher hierarchical data segment that the data segment being described is subordinate to
            'code': 'P'  # Package

        }
        segments.append(self.insertToStr(hl, hl_data))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        lin = 'LIN*[1]%(line_id)s*[2]%(product_qualifier)s*[3]%(product_identifying_number)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            segments.append(self.insertToStr(lin, {
                'line_id': line['external_customer_line_id'],
                'product_qualifier': 'UP',
                'product_identifying_number': line['upc'],
            }))

            segments.append(self.insertToStr(sn1, {
                'qty': line['product_qty']
            }))

        # ctt = 'CTT*[1]%(count_hl)s'
        # segments.append(self.insertToStr(ctt, {
        #     'count_hl': hl_number
        # }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class HBCApiInvoiceOrders(HBCApi):
    """EDI/V4010 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(HBCApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['order_additional_fields'].get(
                'customer#',
                self.invoiceLines[0]['po_number']
            ),
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        ref = 'REF*[1]%(code)s*[2]%(province_code)s'
            # ZZ - Mutually Defined
            # REF02 must contain the relevant two character Canadian (for Hudson’s Bay Company
            # vendors) province code from which items have been shipped. Acceptable values are:
            # AB - Alberta
            # BC - British Columbia
            # MB - Manitoba
            # NB - New Brunswick
            # NS - Nova Scotia
            # NT - Northwest Territories
            # NU - Nunavut
            # ON - Ontario
            # PE - Prince Edward Island
            # QC - Quebec
            # SK - Saskatchewan
            # YT - Yukon
            # NL - Newfoundland and Labrador
        province_codes = ['AB', 'BC', 'MB', 'NB', 'NS', 'NT', 'NU', 'ON', 'PE', 'QC', 'SK', 'YT', 'NL']
        province_code = self.invoiceLines[0]['address_services']['ship_to']['state']
        if province_code not in province_codes:
            raise Exception(
                'Bad province code: {province_code}, availaible only: {province_codes}'.format(
                    province_code=province_code,
                    province_codes=', '.join(['"{0}"'.format(carr) for carr in province_codes])
                )
            )
        segments.append(self.insertToStr(ref, {
            'code': 'ZZ',
            'province_code': province_code,
        }))

        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(product_qualifier)s*[7]%(product_identifying_number)s'
        pid = 'PID*[1]%(description_type)s*[2]*[3]*[4]*[5]%(data)s'
        line_number = 0
        for line in self.invoiceLines:
            if line['upc'] not in [None, 'None', False, 'False']:
                line_number += 1
                try:
                    qty = int(line['product_qty'])
                except ValueError:
                    qty = 1

                segments.append(self.insertToStr(it1, {
                    'line_number': str(line_number),
                    'qty': str(qty),
                    'unit_price': line['unit_cost'],
                    'product_qualifier': 'UP',
                    'product_identifying_number': line['upc'] or '',
                }))

                # GS - Goods and Services Tax (QST); Combined Handling Fee and Product Cost Tax
                # PG - State or Provincial Tax on Goods (GST); Product Cost Tax
                # PS - State or Provincial Tax on Services (GST); Handling Fee Tax
                # VA - Value Added Tax (HST) ; Product Cost Tax
                # ZZ - Mutually Defined (HST); Handling Fee Tax
                tax_identifiers = {
                    'GS': 'qst',
                    'PG': 'gst',
                    'PS': 'pst',
                    'VA': 'hst',
                    'ZZ': 'hst',
                }
                txi = 'TXI*[1]%(tax_identifier)s*[2]%(amount_tax)s'
                for tax_identifier, name_tax in tax_identifiers.iteritems():
                    amount = float(line[name_tax] or 0) * line['unit_cost'] * line['product_qty'] / line['amount_untaxed']
                    if amount:
                        segments.append(self.insertToStr(txi, {
                            'tax_identifier': tax_identifier,
                            'amount_tax': '{0:.2f}'.format(amount),
                        }))

                segments.append(self.insertToStr(pid, {
                    'description_type': 'F',  # Free-form
                    'data': line['name'],
                }))

        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        sac = 'SAC*[1]C*[2]D500*[3]*[4]*[5]%(handling)s'
        segments.append(self.insertToStr(sac, {
            'handling': '{0:.2f}'.format(float(self.invoiceLines[0]['shp_handling'])).replace('.', ''),
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class HBCApiUpdateQTY(HBCApi):
    """EDI/V4010 X12/846: 846 Inventory Inquiry/Advice"""

    def __init__(self, settings, lines):
        super(HBCApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]%(purpose_code)s*[2]MB*[3]%(inventory_feed_qualifier)s*[4]%(date)s*[5]%(time)s*[6]%(action_code)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'purpose_code': '04',  # Change
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'date': date.strftime('%Y%m%d'),
            'time': date.strftime('%H%M'),
            'action_code': '2',  # Change (Update)
        }))

        lin = 'LIN*[1]*[2]%(product_qualifier)s*[3]%(product_identifying_number)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        for line in self.updateLines:

            if not line['upc']:
                self.append_to_revision_lines(line, 'bad')
                continue

            segments.append(self.insertToStr(lin, {
                'product_qualifier': 'UP',  # UPC
                'product_identifying_number': line['upc'],
            }))

            segments.append(self.insertToStr(qty, {
                'code': '17',  # Quantity on Hand
                'qty': str(line['qty']),  # Product
                'uof_code': 'EA'  # Each
            }))

            self.append_to_revision_lines(line, 'good')

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class HBCApiProcessingOrderLines(HBCApi):
    """EDI/V4010 X12/870: 870 Order Status Report"""

    def __init__(self, settings, lines, state):
        super(HBCApiProcessingOrderLines, self).__init__(settings)
        self.processing_lines = lines
        self.state = state
        self.use_ftp_settings('confirm_orders')

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            return self.accept_cancel()
        return False

    def accept_cancel(self):
        self.edi_type = '870'
        segments = []
        date_now = datetime.utcnow()
        st = 'ST*[1]870*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsr = 'BSR*[1]%(status_report_code)s*[2]%(order_item_code)s*[3]%(reference_identification)s*[4]%(date)s'
        segments.append(self.insertToStr(bsr, {
            'status_report_code': '2',  # Prearranged Schedule or Agreement
            'order_item_code': self.processing_lines[0]['po_number'],
            'reference_identification': '',
            'date': date_now.strftime('%Y%m%d'),
        }))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.processing_lines[0]['order_additional_fields'].get('customer#', False) or self.processing_lines[0]['po_number'] or None,
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(price_unit)s*[5]%(product_qualifier)s*[6]%(product_identifying_number)s"
        isr = 'ISR*[1]%(code)s*[2]%(date)s*[3]%(reason_code)s'
        qty = 'QTY*[1]%(code)s*[2]%(quantity_canceled)s'
        hl_number_prev = hl_number
        for i, line in enumerate(self.processing_lines):
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            segments.append(self.insertToStr(po1, {
                'line_number': str(i + 1),
                'qty': line['product_qty'],
                'price_unit': line['price_unit'],
                'product_qualifier': 'UP',
                'product_identifying_number': line['upc'],
            }))

            segments.append(self.insertToStr(isr, {
                'code': 'IC',  # Item Canceled
                'date': date_now.strftime('%Y%m%d'),
                'reason_code': '',
            }))

            segments.append(self.insertToStr(qty, {
                'code': 'QV',  # Quantity Canceled
                'quantity_canceled': line['product_qty'],
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'RS'})


class HBCOpenerp(ApiOpenerp):

    def __init__(self):

        super(HBCOpenerp, self).__init__()
        self.confirm_fields_map = {
            'upc': 'UPC',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line['name'],
            'sku': line['sku'],
            'customer_sku': line['customer_sku'],
            'upc': line.get('upc', False),
            'cost': line['cost'],
            'customerCost': line['retail_cost'],
            'qty': line['qty'],
            'optionSku': line.get('optionSku', False),
            'merchantSKU': line['customer_sku'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = {}
        field_list = ['sku', 'customer_sku', 'upc']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[
                                                                                          field],
                                                                                      ('default_code', 'customer_sku',
                                                                                       'upc')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        country_obj = self.pool.get('res.country')
        state_obj = self.pool.get('res.country.state')
        tax_obj = self.pool.get("delmar.sale.taxes")
        raw_taxes = {}
        line = {
            'address_services': {
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                    'province_code': ''
                },
            }
        }
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })
        country_id = None
        country_ids = country_obj.search(cr, uid, [('name', '=', 'Canada')])
        if country_ids:
            country_id = country_ids[0]
        state_ids = state_obj.search(cr, uid, [
            ('country_id', '=', country_id),
            ('name', 'in', [u'Quebec', u'Quèbec'])
        ])
        if country_id and state_ids:
            raw_taxes = tax_obj.get_raw_taxes(cr, uid, country_id, state_ids)
        line.update({'raw_taxes': raw_taxes})
        deliver_obj = self.pool.get("stock.picking").browse(cr, uid, deliver_id)
        shp_handling = deliver_obj.shp_handling or 0.0
        line.update({'shp_handling': shp_handling})
        return line

    def get_additional_processing_information(self, cr, uid, settings, picking, order_line, context=None):
        upc = self.pool.get('product.product').get_val_by_label(
            cr,
            uid,
            order_line.product_id.id,
            order_line.sale_line_id.order_partner_id.id,
            'UPC',
            order_line.size_id.id or False,
        )
        result = {'upc': upc}
        return result
