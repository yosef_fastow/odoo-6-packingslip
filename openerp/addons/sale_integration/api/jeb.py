# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from ftpapiclient import FtpClient
from datetime import datetime
from customer_parsers.jeb_input_csv_parser import CSV_parser
from apiopenerp import ApiOpenerp
import logging

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class JebApi(FtpClient):

    def __init__(self, settings):
        super(JebApi, self).__init__(settings)


class JebApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(JebApiClient, self).__init__(
            settings_variables, JebOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = JebGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = YamlOrder(xml)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = JebApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines, context=None):
        res = True
        confirmApi = JebApiConfirmOrders(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        res = 'Confirm result:\n{0}'.format(str(res_confirm))
        return res


class JebGetOrdersXML(JebApi):

    def __init__(self, settings):
        super(JebGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSV_parser(self.customer)


class JebApiUpdateQTY(JebApi):

    def __init__(self, settings, lines):
        super(JebApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "JEBInv.csv"
        self.filename_local = "{:JEBInv%Y%m%d%H%M.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def get_sku_and_size_str(self, line):
        _size_str = ''
        _sku = line.get('sku', '')
        if('/' in _sku):
            try:
                _size_str = str(
                    float(_sku[_sku.find("/") + 1:])).replace('.0', '')
                _sku = _sku[:_sku.find("/")]
            except ValueError:
                pass
        return _sku, _size_str

    def upload_data(self):
        lines = self.lines
        for line in lines:
            _sku = line.get('sku', False)
            if((not _sku) or (_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
            line['sku_str'], line['size_str'] = self.get_sku_and_size_str(line)
        return {'lines': self.revision_lines['good']}


class JebApiConfirmOrders(JebApi):

    def __init__(self, settings, lines):
        super(JebApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:ShipConfirm_%Y%m%d%H%M.xml}".format(datetime.now())

    def upload_data(self):
        lines = self.lines
        for line in lines:
            product_qty = 0
            if(line.get('product_qty', False)):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
            line['product_qty'] = product_qty
            if(line.get('external_date_order', False)):
                try:
                    _date = datetime.strptime(line['external_date_order'], '%Y-%m-%d')
                    _date = "{:%d/%m/%y}".format(_date)
                    line['external_date_order'] = _date
                except ValueError:
                    line['external_date_order'] = ''
            else:
                line['external_date_order'] = ''
            date_now = datetime.now()
            line['date_now'] = "{:%Y-%m-%d}".format(date_now)
            if(line.get('shp_date', False)):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = "{:%Y-%m-%d}".format(date_now)
        return {'lines': lines}


class JebOpenerp(ApiOpenerp):

    def __init__(self):
        super(JebOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}

        additional_fields = []

        line_obj = {
            'id': line['id'],
            'customer_sku': line['delmar_id'],
            'sku': line['sku'],
            'name': line['name'],
            'size': line['size'],
            'qty': line['qty'],
            'price_unit': line['price_unit'],
            'cost': line['cost'],
            'notes': "",
        }

        if line.get('gift_message', False):
            additional_fields.append((0, 0, {
                'name': 'gift_message',
                'label': 'Gift Message',
                'value': line['gift_message']
            }))

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['customer_sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = False

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
            if(not line_obj.get('cost', False)):
                product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if product_cost:
                    line_obj['price_unit'] = product_cost
                    line_obj['cost'] = product_cost
                else:
                    line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
            else:
                line_obj['price_unit'] = line_obj['cost']
        else:
            line_obj["notes"] = "Can't find product by sku {0}.\n".format(line['sku'])
            line_obj["product_id"] = 0
        line_obj.update({'additional_fields': additional_fields})

        return line_obj
