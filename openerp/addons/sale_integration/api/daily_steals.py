# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from customer_parsers.daily_steals_csv_parser import CSVParser
from datetime import datetime
import logging
from apiopenerp import ApiOpenerp

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


class DailyStealsApi(FtpClient):

    def __init__(self, settings):
        super(DailyStealsApi, self).__init__(settings)

    def upload_data(self):
        return {'lines': self.lines}


class DailyStealsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(DailyStealsApiClient, self).__init__(
            settings_variables, DailyStealsOpenerp, False, DEFAULT_VALUES)
        self.settings["order_type"] = 'normal'
        self.load_orders_api = DailyStealsApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = DailyStealsApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res


class DailyStealsApiGetOrders(DailyStealsApi):

    def __init__(self, settings):
        super(DailyStealsApiGetOrders, self).__init__(settings)
        self.order_type = settings['order_type']
        self.use_ftp_settings('load_orders')
        if self.order_type == 'normal':
            self.use_ftp_settings('load_orders')
        else:
            self.use_ftp_settings('load_cancel_orders')
        self.parser = CSVParser(self)


class DailyStealsApiConfirmOrders(DailyStealsApi):

    def __init__(self, settings, lines):
        super(DailyStealsApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:Confirm%Y%m%d-%H:%M:%S:%f.csv}".format(datetime.now())


class DailyStealsOpenerp(ApiOpenerp):

    def __init__(self):

        super(DailyStealsOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields': self.wrap_additional_fields(order.get('additional_fields', {}))
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line.copy()
        line_obj.update({
            'notes': '',
            'additional_fields': self.wrap_additional_fields(line.get('additional_fields', {}))
        })

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}
        field_list = ['sku']
        for field_name in field_list:
            field = line.get(field_name, None)
            if field:
                line_obj['sku'] = field
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], field, search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(line['sku'])
            line_obj['product_id'] = False

        return line_obj
