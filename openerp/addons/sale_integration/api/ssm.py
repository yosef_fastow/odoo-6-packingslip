# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from httpauthapiclient import HTTPWithAuthApiClient
from datetime import datetime, timedelta
from customer_parsers.ssm_input_xml_parser import XML_parser
from apiopenerp import ApiOpenerp
import logging
from pf_utils.utils.re_utils import f_d
from json import loads as json_loads
import random
import hmac
import hashlib

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('location_id',             'location_id',              '189628629'),
    ('pick_up_now_eligible',    'pick_up_now_eligible',     'false'),
    ('low_inventory_threshold', 'low_inventory_threshold',  '1'),
    ('seller_id',               'Seller ID',                '17590'),
    ('email_address',           'Email address',            'avifr@delmarintl.ca'),
    ('secret_key',              'Secret Key',               '50gw7+5D4dniOjX3E9+T2z661ZDa6DSfjRxP26t6QlQ='),
)


DEFAULT_VALUES = {
    'use_http': True,
}


class SsmHTTPApi(HTTPWithAuthApiClient):
    def __init__(self, settings):
        super(SsmHTTPApi, self).__init__(settings)

    @property
    def sub_request_kwargs(self):
        _kwargs = {
            'headers': {
                'Content-Type': 'application/xml',
                'authorization': self.get_auth_str()
            },
        }

        if hasattr(self, 'upload_data') and hasattr(self, 'prepare_data'):
            _kwargs['data'] = self.prepare_data(self.upload_data())

        return _kwargs

    def get_auth_str(self):

        timestamp = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

        string_to_sign = '{seller_id}:{email_address}:{timestamp}'.format(
            seller_id=self.seller_id,
            email_address=self.email_address,
            timestamp=timestamp
        )

        signature = hmac.new(
            bytearray(self.secret_key, 'utf-8'),
            msg=bytearray(string_to_sign, 'utf-8'),
            digestmod=hashlib.sha256
        ).hexdigest()

        auth_str = 'HMAC-SHA256 emailaddress={email},timestamp={timestamp},signature={signature}'.format(
            email=self.email_address,
            timestamp=timestamp,
            signature=signature,
        )

        return auth_str


class SsmApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(SsmApiClient, self).__init__(
            settings_variables, SsmOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = SsmGetOrdersXML

    def loadOrders(self, save_flag=False):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def updateQTY(self, lines, mode=None):
        updateApi = SsmApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = SsmApiAcknowledgmentOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = SsmApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def returnOrders(self, lines):
        returnApi = SsmApiReturnOrders(self.settings, lines)
        returnApi.process('send')
        self.extend_log(returnApi)
        return True


class SsmGetOrdersXML(SsmHTTPApi):

    def __init__(self, settings):
        super(SsmGetOrdersXML, self).__init__(settings)
        self.use_http_settings('load_orders')
        self.parser = XML_parser(self)
        self.filename = f_d('SSM_ORDERS_%Y%m%d_%H%M%S_%f.xml', datetime.utcnow())


class SsmApiUpdateQTY(SsmHTTPApi):

    def __init__(self, settings, lines):
        super(SsmApiUpdateQTY, self).__init__(settings)
        self.use_http_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename_local = f_d('INVENTORY%Y%m%d%H%M%S.xml', datetime.utcnow())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        inventory_timestamp = f_d('%Y-%m-%dT%H:%M:%S', (datetime.utcnow()))
        for line in lines:
            line['location_id'] = self.location_id
            line['low_inventory_threshold'] = self.low_inventory_threshold
            line['pick_up_now_eligible'] = self.pick_up_now_eligible
            line['inventory_timestamp'] = inventory_timestamp
            _sku = line.get('sku', False)
            if((not _sku) or (_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}

    def process_response(self, response):
        response = super(SsmApiUpdateQTY, self).process_response(response)
        self.log({
            'title': 'UpdateQTY response',
            'type': 'debug',
            'message': response,
        })
        return response


class SsmApiAcknowledgmentOrders(SsmHTTPApi):

    def __init__(self, settings, lines, state):
        super(SsmApiAcknowledgmentOrders, self).__init__(settings)
        self.use_http_settings('acknowledgement')
        self.type_tpl = "string"
        self.use_mako_templates('acknowledgement')
        self.lines = lines
        self.filename = f_d('ACKNOWLEDGEMENT%Y%m%d-%H%M%S.xml', datetime.utcnow())
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            return self.accept_cancel()

    def accept_cancel(self):
        lines = self.lines
        for line in lines:
            line['po-number'] = line['po_number']
            line['po-date'] = line.get('external_date_order', '')
            line['line-number'] = line['additional_fields'].get('line-number', '')
            line['item-id'] = line['additional_fields'].get('item-id', '')
            line['line-status'] = 'Canceled'
            line['cancel-reason'] = line.get('cancel_code', False)
        return {'lines': lines}


class SsmApiConfirmOrders(SsmHTTPApi):

    def __init__(self, settings, lines):
        super(SsmApiConfirmOrders, self).__init__(settings)
        self.use_http_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d('CONFIRM%Y%m%d-%H%M%S.xml', datetime.utcnow())

    def upload_data(self):
        lines = self.lines
        normalized_json_str = self.lines[0]['order_additional_fields']['shipping-detail']
        carrier_details = json_loads(normalized_json_str)
        carrier_code = self.lines[0]['carrier_code']
        carrier_codes = [str(x).strip() for x in carrier_code.split('|')]
        shipping_carrier = (
            len(carrier_codes) > 1 and carrier_codes[1] or
            carrier_details['carrier'] or
            ''
        )
        shipping_method = (
            len(carrier_codes) > 1 and carrier_codes[0] or
            carrier_details['shipping-method'] or
            ''
        )
        for line in lines:
            line['asn-number'] = line['invoice']
            line['po-number'] = line['po_number']
            line['po-date'] = line.get('external_date_order', '')
            line['tracking-number'] = line['tracking_number']
            ship_date = datetime.utcnow()
            if(line.get('shp_date', False)):
                    ship_date = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['ship-date'] = "{:%Y-%m-%d}".format(ship_date)
            line['shipping-carrier'] = shipping_carrier
            line['shipping-method'] = shipping_method
            line['line-number'] = line['additional_fields'].get('line-number', '')
            line['item-id'] = line['additional_fields'].get('item-id', '')
            if line.get('product_qty', False):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    product_qty = 0
            line['quantity'] = product_qty
        return {'lines': lines}


class SsmApiReturnOrders(SsmHTTPApi):

    def __init__(self, settings, lines):
        super(SsmApiReturnOrders, self).__init__(settings)
        self.use_http_settings('return_orders')
        self.type_tpl = "string"
        self.use_mako_templates('return')
        self.lines = lines
        self.filename = f_d('RETURN%Y%m%d-%H%M%S.xml', datetime.utcnow())

    def upload_data(self):
        lines = self.lines
        lines[0]['date-time-stamp'] = f_d('%Y-%m-%dT%H%M%S', datetime.utcnow())
        lines[0]['po-number'] = lines[0]['po_number']
        lines[0]['po-date'] = lines[0].get('external_date_order', '')
        for line in lines:
            line['line-number'] = line['additional_fields'].get('line-number', '')
            line['item-id'] = line['additional_fields'].get('item-id', '')
            line['return-unique-id'] = str(random.randrange(10000000, 99999999))
            line['return-reason'] = line['return_reason']
            line['return-date'] = f_d('%Y-%m-%d', datetime.utcnow())
            line['quantity'] = line['ordered_qty']
        return {'lines': lines}


class SsmOpenerp(ApiOpenerp):

    def __init__(self):
        super(SsmOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            'id': line['id'],
            'customer_sku': line['customer_sku'],
            'vendorSku': line['customer_sku'],
            'sku': line['sku'],
            'name': line['name'],
            'size': line.get('size', False),
            'qty': line['qty'],
            'customerCost': line['shipCost'],
            'notes': "",
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = False

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'customer_id_delmar', 'customer_sku', 'default_code'
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            if(not line_obj.get('cost', False)):
                product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if product_cost:
                    line_obj['price_unit'] = product_cost
                else:
                    line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
            else:
                line_obj['price_unit'] = line_obj['cost']
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
