# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging
from abstract_apiclient import YamlOrder
from openerp.addons.pf_utils.utils.re_utils import f_d
from customer_parsers import vendornet_csv_parser
from apiopenerp import ApiOpenerp
from json import dumps as json_dumps
from json import loads as json_loads
from utils.ediparser import check_none
from customer_parsers.base_edi import address_type_map

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '057932'),
    ('vendor_name',                 'Vendor name',              'AAFESDS'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '001695568GT'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    '14'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '5010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.x12'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      '^'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class AAFESDSApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class AAFESDSApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(AAFESDSApiClient, self).__init__(
            settings_variables, AAFESDSOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = AAFESDSApiGetOrdersXML

    def confirmShipment(self, lines):

        res = None
        manual_order = lines[0]['manual_order']
        if not manual_order:
            confirmApi = AAFESDSApiConfirmShipment(self.settings, lines)
            res = confirmApi.process('send')
            self.extend_log(confirmApi)
            if self.is_invoice_required:
                invoiceApi = AAFESDSApiInvoiceOrders(self.settings, lines)
                invoiceApi.process('send')
                self.extend_log(invoiceApi)
        else:
            res = True
        return res


    def loadOrders(self, save_flag=False):
        orders = []
        if (save_flag):
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')

        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, serialized_order):
        ordersApi = YamlOrder(serialized_order)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = AAFESDSApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

class AAFESDSApiUpdateQTY(AAFESDSApi):
    filename = "inv.txt"
    lines = []
    name = "UpdateQTY"
    cur_ftp_setting = ""
    date_format = "%Y%m%d"

    def __init__(self, settings, lines):
        super(AAFESDSApiUpdateQTY, self).__init__(settings)
        self.filename_local = f_d("inv(%Y%m%d%H%M%f).csv")
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates('inventory')
        self.lines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        self.lines = self.prepareUpdateQTYLines(self.lines)

        feed_date = datetime.now().strftime(self.date_format)

        for line in self.lines:

            line['feed_date'] = feed_date

            if (line.get('customer_price', False)):
                line['customer_price'] = "%.2f" % line['customer_price']
            if (
                line.get('qty', False) is not False and
                line.get('customer_price', False) and
                line.get('customer_id_delmar')
            ):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}

class AAFESDSApiGetOrdersXML(AAFESDSApi):

    def __init__(self, settings):
        super(AAFESDSApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = vendornet_csv_parser.VendornetApiGetOrdersXML(self)

    def parse_response(self, response):
        csv_parser = vendornet_csv_parser.VendornetApiGetOrdersXML(self)
        ordersList = csv_parser.parse_response(response)
        return ordersList

class AAFESDSApiConfirmShipment(AAFESDSApi):
    """EDI/V4010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(AAFESDSApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'
        self._default_weight = 1

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = '0001'
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        shp_time = shp_date.strftime('%H%M')

        bsn_data = {
            # 00 - Original
            # 01 - Cancellation
            # 17 - Cancel, to be Reissued
            'purpose_code': '00',
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S',  # Shipment,

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]*[2]*[3]%(carrier_code)s*[4]*[5]%(carrier)s'
        td5_data = {
            'carrier_code': self.confirmLines[0]['carrier_code'],
            'carrier': self.confirmLines[0]['carrier'],
        }
        segments.append(self.insertToStr(td5, td5_data))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'O',  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number'],
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'P',  # Pack
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s*[3]*[4]*[5]%(tracking_number)s'
        segments.append(self.insertToStr(man, {
            'code': 'GM',  # EAN.UCC Serial Shipping Container Code (SSCC) and Application Identifier
            'number': self.confirmLines[0]['sscc_id'],
            'tracking_number': self.confirmLines[0]['tracking_number'],
        }))

        lin = 'LIN*[1]*[2]%(item_name_identificator)s*[3]%(purchase_item_code)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I',  # Item
            }))

            item_name_identificator = 'PI'
            purchase_item_code = line.get('vendorSku', False) or line.get('merchantSKU', False) or line.get('upc', '')

            segments.append(self.insertToStr(lin, {
                'item_name_identificator': item_name_identificator,
                'purchase_item_code': purchase_item_code,
            }))

            qty = line['product_qty']
            try:
                qty = int(qty)
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(sn1, {
                'qty': qty,
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})

class AAFESDSApiInvoiceOrders(AAFESDSApi):
    """EDI/V5010 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(AAFESDSApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]*[4]%(po_number)s'
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
        }))

        cur = 'CUR*[1]%(code)s*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'code': 'SE',
            'currency_code': check_none(self.invoiceLines[0]['order_additional_fields'].get('currency')) or 'USD',
        }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        ref_codes = [
            'IA',  # Internal Vendor Number
            'BM',  # Bill of Lading Number
        ]

        for ref_code in ref_codes:
            ref_value = self.invoiceLines[0]['order_additional_fields'].get(ref_code, False)
            if ref_value:
                segments.append(self.insertToStr(ref, {
                    'code': ref_code,
                    'order_number': ref_value,
                }))

        original_addresses = self.invoiceLines[0]['order_additional_fields'].get('addresses', {})
        if isinstance(original_addresses, (str, unicode)):
            original_addresses = json_loads(original_addresses)
        for address_type in ['ST']:
            address = original_addresses.get(address_type_map.get(address_type, ''), {})
            if address:
                if address_type in ['ST', 'BY']:
                    n1 = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(facility_number)s'
                    segments.append(self.insertToStr(n1, {
                        'code': address_type,
                        'name': check_none(address['name']),
                        'identification_code_qualifier': '92',
                        'facility_number': check_none(self.invoiceLines[0]['order_additional_fields'].get('facility_number', '')),
                    }))

                    # n3 = 'N3*[1]%(address1)s*[2]%(address2)s'
                    # segments.append(self.insertToStr(n3, {
                    #     'address1': check_none(address['address1']),
                    #     'address2': check_none(address['address2']),
                    # }))

                    country_code = self.invoiceLines[0].get('order_additional_fields', {}).get('ship_to_country')
                    if not country_code or country_code.lower() in ['null', 'none', 'false']:
                        country_code = self.invoiceLines[0]['ship_to_country']

                    n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'
                    segments.append(self.insertToStr(n4, {
                        'city': address['city'],
                        'state': address['state'],
                        'postal_code': address['zip'],
                        'country': country_code,
                    }))

        duns = self.invoiceLines[0]['order_additional_fields'].get('DUNS') or ''
        for address_type in ['RI']:
            n1 = 'N1*[1]%(code)s*[2]*[3]%(identification_code_qualifier)s*[4]%(duns_number)s'
            segments.append(self.insertToStr(n1, {
                'code': 'RI',
                'identification_code_qualifier': '1',
                'duns_number': duns[:9],
            }))

        terms_of_sale = json_loads(self.invoiceLines[0]['order_additional_fields'].get('terms_of_sale', '{}'))
        itd = 'ITD*[1]%(terms_type_code)s*[2]*[3]%(terms_discount_percent)s*[4]%(terms_discount_due_date)s*[5]%(terms_discount_days_due)s*[6]%(terms_net_due_date)s*[7]%(terms_net_days)s*[8]*[9]*[10]*[11]*[12]%(description)s'
        segments.append(self.insertToStr(itd, {
            'terms_type_code': check_none(str(terms_of_sale.get('terms_type_code', {}).get('original', ''))),
            'terms_discount_percent': check_none(str(terms_of_sale.get('terms_discount_percent'))),
            'terms_discount_due_date': check_none(str(terms_of_sale.get('terms_discount_due_date'))),
            'terms_discount_days_due': check_none(str(terms_of_sale.get('terms_discount_days_due'))),
            'terms_net_due_date': check_none(str(terms_of_sale.get('terms_net_due_date'))),
            'terms_net_days': check_none(str(terms_of_sale.get('terms_net_days'))),
            'description': check_none(str(terms_of_sale.get('description'))),
        }))

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        it1 = 'IT1*[1]*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(upc_qualifier)s*[7]%(upc)s'
        ctp = 'CTP*[1]*[2]%(code)s*[3]%(value)s'
        pid = 'PID*[1]%(description_type)s*[2]*[3]*[4]*[5]%(data)s'
        po4 = 'PO4*[1]%(qty)s'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            upc = line['additional_fields'].get('UP', '') or ''
            upc_qualifier = upc and 'UP' or ''

            segments.append(self.insertToStr(it1, {
                'qty': str(qty),
                'unit_price': line['price_unit'],
                'upc_qualifier': upc_qualifier,
                'upc': line.get('vendorSku', ''),
            }))

            RPP = line['additional_fields'].get('RPP', '') or ''
            if RPP:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RPP)),
                }))
            RTL = line['additional_fields'].get('RTL', '') or ''
            if RTL:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RTL)),
                }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'data': line['name'],
            }))

            segments.append(self.insertToStr(po4, {
                'qty': qty,
            }))

            total_qty_invoiced += qty

        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'IN'})


class AAFESDSOpenerp(ApiOpenerp):

    def __init__(self):

        super(AAFESDSOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def write_unique_fields_for_orders(self, cr, uid, picking):
        if picking and picking.sale_id and picking.sale_id.sale_integration_xml_id:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            serial_reference_number_obj = \
                uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id)
            if not serial_reference_number_obj:
                raise ValueError('Not Available any serial_reference_number')
            serial_reference_number_obj.update({
                'sscc_id': uffo_obj.generate_sscc_id(
                    cr, uid,
                    [serial_reference_number_obj['id']],
                    application_identifier='00',
                    extension_digit='0'
                ).get(serial_reference_number_obj['id'], '')
            })
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if srn_row_ids:
                sale_order_fields_obj.write(
                    cr, uid, srn_row_ids, {
                        'name': 'serial_reference_number',
                        'label': 'serial_reference_number',
                        'value': json_dumps(serial_reference_number_obj)
                    }
                )
            else:
                self.pool.get('sale.order').write(
                    cr, uid, picking.sale_id.id, {
                        'additional_fields': [(0, 0, {
                            'name': 'serial_reference_number',
                            'label': 'serial_reference_number',
                            'value': json_dumps(serial_reference_number_obj)
                        })]
                    }
                )
            uffo_obj.use_for(cr, uid, serial_reference_number_obj['id'], picking.id)
        return True

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'customerCost': line['customerCost'],
            "product_id": False,
            "size_id": False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'additional_fields': [
                (0, 0, {'name': 'store', 'label': 'Store',
                        'value': line.get('store', '')}),
                (0, 0, {'name': 'giftIndicator', 'label': 'giftIndicator',
                        'value': line.get('additional_fields',{}).get('giftIndicator', '')}),
                (0, 0, {'name': 'LINE_COMMENT3', 'label': 'LINE_COMMENT3',
                        'value': line.get('additional_fields',{}).get('LINE_COMMENT3', '')}),
                (0, 0, {'name': 'LINE_COMMENT1', 'label': 'LINE_COMMENT1',
                        'value': line.get('additional_fields',{}).get('LINE_COMMENT1', '')}),
                (0, 0, {'name': 'LINE_COMMENT2', 'label': 'LINE_COMMENT2',
                        'value': line.get('additional_fields',{}).get('LINE_COMMENT2', '')}),
                (0, 0, {'name': 'LINE_COMMENT4', 'label': 'LINE_COMMENT4',
                        'value': line.get('additional_fields',{}).get('LINE_COMMENT4', '')}),
                (0, 0, {'name': 'GIFT_WRAP_XREF', 'label': 'GIFT_WRAP_XREF',
                        'value': line.get('additional_fields',{}).get('GIFT_WRAP_XREF', '')}),
            ]
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not line_obj.get('size_id', False):
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif(line.get('size', False)):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if(len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]
                        else:
                            line_obj['size_id'] = False
                            line['size'] = False
                    else:
                        line_obj['size_id'] = False
                if not line_obj.get('product_id', False):
                    if product:
                        line_obj["product_id"] = product.id
                        line_obj['cost'] = line.get('cost', False)
                        if not line_obj['cost'] or line_obj['cost'] == 0:
                            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                             context['customer_id'],
                                                                                             'Customer Price',
                                                                                             line_obj['size_id'])
                            if product_cost:
                                line['cost'] = product_cost
                                break
                            else:
                                line['cost'] = False
                                line_obj["notes"] = "Can't find product cost.\n"

        return line_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)

        res = {
            'Ref1': sale_order.external_customer_order_id or '',
            'Ref2': sale_order.po_number or ''
        }

        return res

    def get_additional_confirm_shipment_information(
            self, cr, uid, sale_obj, deliveri_id, lines, context=None
    ):
        line = {
            'serial_reference_number': '',
            'serial_reference_number_id': False,
            'sscc_id': False,
        }
        serial_reference_number_obj = {'id': False, 'value': False}
        order_additional_fields = lines[0]['order_additional_fields']
        if 'serial_reference_number' in order_additional_fields:
            serial_reference_number_obj = json_loads(order_additional_fields['serial_reference_number'])
        else:
            raise ValueError('Not Available any serial_reference_number')
        line.update({
            'serial_reference_number': serial_reference_number_obj['value'],
            'serial_reference_number_id': serial_reference_number_obj['id'],
            'sscc_id': serial_reference_number_obj['sscc_id'],
        })
        return line

    def after_correct_confirm_shipment_callback(self, cr, uid, sale_obj, picking_obj, lines):
        if 'serial_reference_number_id'in lines[0] and lines[0]['serial_reference_number_id']:
            self.pool.get('unique.fields.for.orders').use_for(
                cr, uid, lines[0]['serial_reference_number_id'], picking_obj.id)
        return True

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['po_number'], tuple(customer_ids)))

        return cr.dictfetchall()
