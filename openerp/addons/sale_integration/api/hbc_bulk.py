# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.hbc_bulk_edi_parser import EDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime, timedelta
import logging
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import random
from json import dumps as json_dumps
from json import loads as json_loads
from math import ceil
from pf_utils.utils.re_utils import f_d
#import tz
from dateutil import tz

_logger = logging.getLogger(__name__)

# TODO: change SETTINGS_FIELDS to proper settings for hbc bulk
# TASK: DELMAR-85
SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '202377222'),
    ('vendor_name',                 'Vendor name',              'HBC Packed By Store'),
    ('sender_id',                   'Sender Id',                'Delmar'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id_qualifier_810', 'Sender Id Qualifier Invoice', '01'),
    ('receiver_id_qualifier_856', 'Sender Id Qualifier Shipment', '01'),
    ('receiver_id_qualifier_850', 'Sender Id Qualifier 850', '01'),
    ('receiver_id_qualifier_997', 'Sender Id Qualifier FA', '01'),
    ('receiver_id_810', 'Receiver Id Invoice', '202377222'),
    ('receiver_id_856', 'Receiver Id Shipment', '202377222ASN'),
    ('receiver_id_997', 'Receiver Id FA', '202377222'),
    ('receiver_id', 'Receiver Id', '202377222'),
    # ('receiver_id_qualifier',       'Receiver Id Qualifier',    '01'),
    ('contact_name',                'Contact name',             ''),
    ('contact_phone',               'Contact phone',            ''),
    ('edi_x12_version',         'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.edi'),
    ('line_terminator',             'Line Terminator',          r'^'),
    ('repetition_separator',        'Repetition Separator',     '|'),
    ('environment_mode',            'Environment Mode',         'T'),
    ('standard_identifier',         'Standard Identifier',      'U'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class HBCBulkApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class HBCBulkApiClient(AbsApiClient):
    use_local_folder = True
    # TODO: modify integration with the proper format for HBC BULK
    # TASKS: loadOrders, ConfirmLoad: DELMAR-97
    # confirmShipment: DELMAR-98
    def __init__(self, settings_variables):
        super(HBCBulkApiClient, self).__init__(
            settings_variables, HBCBulkOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = HBCBulkApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = HBCBulkApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        confirmApi = HBCBulkApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        if self.is_invoice_required and lines[0]['action'] == 'v_ship':
            invoiceApi = HBCBulkApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res


class HBCBulkApiGetOrders(HBCBulkApi):
    """EDI/V5010 X12/850: AAFESS2S Purchase Orders"""

    def __init__(self, settings):
        super(HBCBulkApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class HBCBulkOpenerp(ApiOpenerp):
    # TODO: gets fille_line and fill_order to match with HBC BULK
    # TASK: DELMAR-97
    number_of_transaction = ''

    def __init__(self):

        super(HBCBulkOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def write_unique_fields_for_orders(self, cr, uid, picking):
        if picking and picking.sale_id and picking.sale_id.sale_integration_xml_id:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            additional_fields = picking.sale_id.additional_fields
            store_numbers = []
            serial_reference_list = []
            for additional_field in additional_fields:
                if additional_field.name == 'store_numbers':
                    store_numbers = json_loads(additional_field.value)

            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if not srn_row_ids:
                total_stores = len(store_numbers)    
                serial_reference_number_obj = \
                    uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id, limit=total_stores)
                if not serial_reference_number_obj:
                    raise ValueError('Not Available any serial_reference_number')
                for serial_reference_number in serial_reference_number_obj:
                    serial_reference_list.append(
                        uffo_obj.generate_sscc_id(
                            cr, uid,
                            [serial_reference_number['id']],
                            application_identifier='00',
                            extension_digit='0'
                        ).get(serial_reference_number['id'], '')
                    )
                    uffo_obj.use_for(cr, uid, serial_reference_number['id'], picking.id)
                if len(serial_reference_list) > 0 and not srn_row_ids:
                    self.pool.get('sale.order').write(
                        cr, uid, picking.sale_id.id, {
                            'additional_fields': [(0, 0, {
                                'name': 'serial_reference_number',
                                'label': 'serial_reference_number',
                                'value': json_dumps(serial_reference_list)
                            })]
                        }
                    )
        return True

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
                [
                    (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                    for key, value in order.get('additional_fields', {}).iteritems()
                ],
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line
        additional_fields = []
        line_obj.update({
            'notes': '',
            'sku': line['vendorSku'],
            'vendorSku': line['vendorSku'],
            'cost': line['cost'],
            'customerCost': line['customerCost'],
            'upc': line['upc'],
            'merchantSKU': line['upc'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        if line.get('poLineData').get('prodColor'):
            line_obj['additional_fields'].append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
            }))

        if line.get('poLineData').get('prodSize'):
            line_obj['additional_fields'].append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
            }))

        if line.get('poLineData').get('UP'):
            line_obj['additional_fields'].append((0, 0, {
                'name': 'UP',
                'label': 'UP',
                'value': line.get('poLineData').get('UP', '')
            }))

        if line.get('poPackData'):
            inner_packs = line.get('poPackData')
            store_number_list = []
            for i in range(len(inner_packs)):
                index = str(i + 1)
                line_obj['additional_fields'].append((0, 0, {
                    'name': 'store_number_' + index,
                    'label': 'store_number_' + index,
                    'value': inner_packs[i]['store_number']
                }))
                line_obj['additional_fields'].append((0, 0, {
                    'name': 'qty_ordered_' + index,
                    'label': 'qty_ordered_' + index,
                    'value': inner_packs[i]['qty_ordered']
                }))
                store_number_list.append({
                    'store_number': inner_packs[i]['store_number'],
                    'qty_ordered': inner_packs[i]['qty_ordered']
                })
            line_obj['additional_fields'].append((0, 0, {
                'name': 'store_numbers',
                'label': 'store_numbers',
                'value': json_dumps(store_number_list)
            }))

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}

        field_list = ['vendorSku', 'sku', 'upc', 'merchantSKU']

        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field],
                                                                 search_by)
                if product:
                    line_obj['name'] = product.name
                    line_obj['size_id'] = size and size.id or False
                    break
        if product:
            line_obj['product_id'] = product.id
            if not line.get('cost'):
                product_cost = self.pool.get('product.product').get_val_by_label(
                    cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if product_cost:
                    line_obj['cost'] = product_cost
                else:
                    line['cost'] = False
                    line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku.'
            line_obj['product_id'] = False

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, lines, context=None):
        line = {
            'serial_reference_number': '',
            'serial_reference_number_id': False,
            'sscc_id': False,
            'address_services': {
                'remmit_to': {
                    'edi_code': 'RI',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
            }
        }
        if (sale_obj.partner_invoice_id):
            line['address_services']['remmit_to'].update({
                'name': 'FIRST CANADIAN DIAMOND CUTTING WORKS',
                'address1': sale_obj.partner_invoice_id.street or '',
                'address2': sale_obj.partner_invoice_id.street2 or '',
                'city': sale_obj.partner_invoice_id.city or '',
                'state': sale_obj.partner_invoice_id.state_id.code or '',
                'country': sale_obj.partner_invoice_id.country_id.code or '',
                'zip': sale_obj.partner_invoice_id.zip or '',
            })
        if (sale_obj.partner_shipping_id):
            store_code = sale_obj.partner_shipping_id.function or ''
            if len(store_code) > 5:
                store_code = store_code[2:]
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
                'store_code': store_code,
            })

            serial_reference_number_obj = {'id': False, 'value': False}
            order_additional_fields = lines[0]['order_additional_fields']
            if 'serial_reference_number' in order_additional_fields:
                serial_reference_number_obj = json_loads(order_additional_fields['serial_reference_number'])
            else:
                raise ValueError('Not Available any serial_reference_number')
            line.update({
                'serial_reference_number': serial_reference_number_obj,
            })

        return line

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):
        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_line_id': order_line.external_customer_line_id,
            'product_qty': order_line.product_uom_qty,

        }

        return line


class HBCBulkApiConfirmShipment(HBCBulkApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(HBCBulkApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]%(str_code)s'

        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        str_code = '0001'
        if self.confirmLines[0]['action'] == 'v_cancel':
            str_code = '0004'
        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time,
            'str_code': str_code,
        }

        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),
            'hl_number_prev': str(''),
            'code': 'S'

        }
        segments.append(self.insertToStr(hl, hl_data))

        ref = 'REF*[1]%(code)s*[2]%(detail)s'
        segments.append(self.insertToStr(ref, {
            'code': 'BM',
            'detail': self.confirmLines[0]['bill_of_lading'] or self.confirmLines[0]['tracking_number'] or False
        }))
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        n1 = 'N1*[1]ST*[2]*[3]92*[4]%(store_number)s'
        store_code = str(self.confirmLines[0]['address_services']['ship_to'].get('store_code', '9999'))
        if len(store_code) > 5:
            store_code = store_code[2:]
        segments.append(self.insertToStr(n1, {
            'store_number': store_code
        }))

        stores = []
        for line in self.confirmLines:
            store_numbers = json_loads(line['additional_fields'].get('store_numbers'))
            for store in store_numbers:
                store_number = store.get('store_number')
                if store_number not in stores:
                    stores.append(store_number)
        sscc_id_list = json_loads(self.confirmLines[0]['order_additional_fields'].get('serial_reference_number', []))

        for index in range(len(stores)):

            hl_number_prev = hl_number
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': '1',
                'code': 'O'  # Order
            }))

            prf = 'PRF*[1]%(po_number)s'
            segments.append(self.insertToStr(prf, {
                'po_number': self.confirmLines[0]['po_number']
            }))

            ref = 'REF*[1]%(code)s*[2]%(detail)s'

            n1 = 'N1*[1]BY*[2]*[3]92*[4]%(store_number)s'

            segments.append(self.insertToStr(n1, {
                'store_number': stores[index]
            }))

            sn1 = 'SN1*' \
                  '[1]*' \
                  '[2]%(qty)s*' \
                  '[3]CA' #EA

        
            hl_number_prev = hl_number
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'P'  # Item
            }))

            pack_num = hl_number
            
            segments.append(self.insertToStr(sn1, {
                'qty': 1,
            }))

            po4= 'PO4*[1]*[2]*[3]*[4]*[5]*[6]%(weight)s*[7]LB*[8]*[9]*[10]%(length)s*[11]%(width)s*[12]%(height)s*[13]%(unit_of_meas_code)s'
            unit_of_meas_code = ['IN', 'FT', 'CM', 'MR']

            segments.append(self.insertToStr(po4, {
                'weight': '1',
                'length': '1',
                'width': '1',
                'height': '1',
                'unit_of_meas_code': 'FT'
            }))

            man = 'MAN*[1]GM*[2]%(container_code)s'
            if len(sscc_id_list) >= index:
                sscc_id = sscc_id_list[index]
                segments.append(self.insertToStr(man, {
                    'container_code': sscc_id
                }))

            dtm = 'DTM*[1]036*[2]%(date_expiration)s'

            segments.append(self.insertToStr(dtm, {
                'date_expiration': 'N/A' # Date of expiration
            }))
            
            for line in self.confirmLines:
                store_numbers_field = json_loads(line['additional_fields'].get('store_numbers'))
                for store_dict in store_numbers_field:
                    if store_dict['store_number'] == stores[index] and int(store_dict['qty_ordered']) > 0:
                        qty_ordered = store_dict['qty_ordered']

                        hl_number_prev = hl_number
                        hl_number += 1
                        segments.append(self.insertToStr(hl, {
                            'hl_number': str(hl_number),
                            'hl_number_prev': str(pack_num),
                            'code': 'I'  # Item
                        }))

                        lin = 'LIN*[1]%(line_id)s*[2]%(product_qualifier)s*[3]%(product_identifying_number)s*[4]IN*' \
                              '[5]%(sku)s*[6]IT*[7]%(vendor_style)s*[8]BO*[9]%(color)s*[10]IZ*[11]%(size)s'

                        segments.append(self.insertToStr(lin, {
                            'line_id': line['external_customer_line_id'],
                            'product_qualifier': 'UP',
                            'product_identifying_number': line.get('upc'),
                            'sku': line.get('vendorSku'),
                            'vendor_style': line.get('optionSku'), # ????
                            'color': line['additional_fields'].get('prod_color', ''),
                            'size': line['additional_fields'].get('prod_size', '')
                        }))

                        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'

                        segments.append(self.insertToStr(sn1, {
                            'qty': int(qty_ordered)
                        }))
                
        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(hl_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        isa_control_number = str(random.randrange(100000000, 999999999))
        gs_control_number = str(random.randrange(1000, 99999999))
        date_now = datetime.utcnow()
        receiver_id = self.receiver_id_856
        if hasattr(self, 'repetition_separator'):
            repetition_separator = self.repetition_separator
        else:
            repetition_separator = '*'

        if hasattr(self, 'standard_identifier'):
            standard_identifier = self.standard_identifier
        else:
            standard_identifier = 'U'

        params = {
            'group': 'SH',
            'isa_data': {
                'sender_id_qualifier': self.sender_id_qualifier,
                'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                'receiver_id_qualifier': self.receiver_id_qualifier_856,
                'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                'edi_x12_version_isa': '00401',
                'isa_control_number': isa_control_number,
                'date': date_now.strftime('%y%m%d'),
                'time': date_now.strftime('%H%M'),
                'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                'repetition_separator': repetition_separator,
                'standard_identifier': standard_identifier,
            },
            'gs_data': {
                'group': 'SH',
                'sender_id': self.sender_id,
                'receiver_id': receiver_id,
                'date': date_now.strftime('%Y%m%d'),
                'time': date_now.strftime('%H%M'),
                'gs_control_number': gs_control_number,
                'edi_x12_version_gs': '00' + self.edi_x12_version + 'VICS',
            }
        }

        return self.wrap(segments, params)


class HBCBulkApiInvoiceOrders(HBCBulkApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(HBCBulkApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s*' \
               '[5]*[6]*[7]*[8]*[9]*[10]%(asn_shipment_nbr)s'
        invoice_date = datetime.now()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'], # same number as a po_number from 850
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
            'asn_shipment_nbr': self.invoiceLines[0]['tracking_number'],
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        cur = 'CUR*[1]BY*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {'currency_code': 'CAD'})) # CAD OR USD gotten from 850 cur

        n1 = 'N1*[1]%(code)s*[2]%(vendor_name)s*[3]92*[4]%(number)s'
        store_code = self.invoiceLines[0]['address_services']['ship_to'].get('store_code', '9999')
        if len(store_code) > 5:
            store_code = store_code[2:]
        store_code = '00' + str(store_code)
        segments.append(self.insertToStr(n1, {
            'code': 'BY',
            'vendor_name': '',
            'number': store_code,
        }))
        segments.append(self.insertToStr(n1, {
            'code': 'SU',
            'vendor_name': self.vendor_name,
            'number': self.vendor_number
        }))

        import calendar
        from calendar import monthrange
        now = datetime.now()
        itd = 'ITD*[1]%(term_type_code)s*[2]%(term_basis_date_code)s*[3]%(term_discount_percent)s*' \
              '[4]%(terms_due_date)s*[5]%(term_discount_day_off)s*[6]*[7]%(term_net_days)s*[8]*[9]*[10]*[11]*[12]*[13]%(count_day_od_mounth)s'
        
        # segments.append(self.insertToStr(itd, {
        #     'term_type_code': '01', # '05', '02', '12', '08'
        #     'term_basis_date_code': '15' # 02
        #     'term_discount_percent': '',
        #     'term_due_date': '?',
        #     'term_discount_day_off': '?',
        #     'term_net_days': '',
        #     'count_day_od_mounth': calendar.monthrange(now.year, now.month)[1]
        # }))

        dmt = 'DMT*[1]011*[2]%(date)s'
        # segments.append(self.insertToStr(dmt, {
        #     'date': invoice_date
        # }))

        it1 = 'IT1*' \
              '[1]%(line_number)s*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]%(unit_price)s*' \
              '[5]WE*' \
              '[6]UP*' \
              '[7]%(product_identifying_number)s'
              
        #'[8]VA*'
        # '[9]%(vendor_style)s'

        sdq = "SDQ*[1]EA*[2]92%(store_information)s"
        
        line_number = 0
        total_qty_invoiced = 0
        total_price_invoiced = 0
        total_taxed = 0
        taxes = {'GST': 0, 'HST': 0, 'QST': 0}
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': qty,
                'unit_price': line['unit_cost'],
                'product_identifying_number': line.get('merchantSKU')
            }))

            store_numbers = json_loads(line['additional_fields'].get('store_numbers'))
            store_information = ''
            for stores in store_numbers:
                store_information = store_information + '*' + str(stores['store_number']) + '*' + str(stores['qty_ordered'])
            if store_information:
                segments.append(self.insertToStr(sdq, {
                    'store_information': store_information
                }))

            total_qty_invoiced += qty
            total_price_invoiced += qty * float(line['unit_cost'])
            total_taxed += (float(line.get('gst', 0.0)) + float(line.get('pst', 0.0)) +
                            float(line.get('hst', 0.0)) + float(line.get('qst', 0.0)))
            
            taxes['QST'] += float(line.get('qst', 0.0))
            taxes['GST'] += float(line.get('gst', 0.0))
            taxes['HST'] += float(line.get('hst', 0.0))


        tds = 'TDS*[1]%(total_amount)s*[2]%(amount_subject_to_terms)s*[3]*[4]'
        total_gross = float(total_price_invoiced or 0.0) + float(total_taxed or 0.0)
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(total_gross or 0.0)).replace('.', ''),
            # Example: $5000.00 would be sent as 500000
            'amount_subject_to_terms': '{0:.2f}'.format(float(total_price_invoiced or 0.0)).replace('.', '')
        }))

        cad = 'CAD*[1]%(code)s*[2]*[3]*[4]%(carrier_code)s**[5]%(carrier)s*[6]*[7]BM*[8]%(number)s'
        # segments.append(self.insertToStr(cad, {
        #     'code': 'A', # or AE, C, L, M, U 
        #     'carrier_code': self.invoiceLines[0]['carrier_code'],
        #     'carrier': self.invoiceLines[0]['carrier'],
        #     'number': self.invoiceLines[0]['tracking_number']
        # }))

        sac = 'SAC*[1]%(code)s*[2]%(name)s*[3]*[4]*[5]%(value)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]*[13]*[14]*[15]%(reason)s'

        tax_name = 'GST'
        if taxes['GST'] <= 0:
            tax_name = 'HST'
        state = self.invoiceLines[0]['address_services']['ship_to'].get('state', 'ON')

        segments.append(self.insertToStr(sac, {
            'code': 'C',
            'name': 'D360' , # 'D360'  GST or HST'H770'  QST
            'value': '{0:.2f}'.format(float(taxes[tax_name] or 0.0)).replace('.', ''), 
            'reason': state + tax_name # GST HST QST
        }))

        segments.append(self.insertToStr(sac, {
            'code': 'C',
            'name': 'H770' , # 'D360'  GST or HST'H770'  QST
            'value': '{0:.2f}'.format(float(taxes['QST'] or 0.0)).replace('.', ''), 
            'reason': state + 'QST' # GST HST QST
        }))


        iss = 'ISS*[1]%(number_of_cartons)s*[2]EA*[3]%(weight)s*[4]LB'
        segments.append(self.insertToStr(iss, {
            'number_of_cartons': total_qty_invoiced,
            'weight': self.invoiceLines[0]['weight'] or 1
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        isa_control_number = str(random.randrange(100000000, 999999999))
        gs_control_number = str(random.randrange(1000, 99999999))
        date_now = datetime.utcnow()
        receiver_id = self.receiver_id_810
        if hasattr(self, 'repetition_separator'):
            repetition_separator = self.repetition_separator
        else:
            repetition_separator = '*'

        if hasattr(self, 'standard_identifier'):
            standard_identifier = self.standard_identifier
        else:
            standard_identifier = 'U'

        params = {
            'group': 'IN',
            'isa_data': {
                'sender_id_qualifier': self.sender_id_qualifier,
                'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                'receiver_id_qualifier': self.receiver_id_qualifier_810,
                'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                'edi_x12_version_isa': '00401',
                'isa_control_number': isa_control_number,
                'date': date_now.strftime('%y%m%d'),
                'time': date_now.strftime('%H%M'),
                'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                'repetition_separator': repetition_separator,
                'standard_identifier': standard_identifier,
            },
            'gs_data': {
                'group': 'IN',
                'sender_id': self.sender_id,
                'receiver_id': receiver_id,
                'date': date_now.strftime('%Y%m%d'),
                'time': date_now.strftime('%H%M'),
                'gs_control_number': gs_control_number,
                'edi_x12_version_gs': '00' + self.edi_x12_version + 'VICS',
            }
        }
        return self.wrap(segments, params)


class HBCBulkApiFunctionalAcknoledgment(HBCBulkApi):
    orders = []

    def __init__(self, settings, orders):
        super(HBCBulkApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        k = 0
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                k += 1
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak2 = 'AK2*[1]850*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak2, {
                    'ack_control_number': "%04d" % int(order['ack_control_number'])
                }))
                ak5 = 'AK5*[1]A'
                segments.append(self.insertToStr(ak5, {
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))

                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                isa_control_number = str(random.randrange(100000000, 999999999))
                gs_control_number = str(random.randrange(1000, 99999999))
                date_now = datetime.utcnow()
                receiver_id = self.receiver_id_997
                if hasattr(self, 'repetition_separator'):
                    repetition_separator = self.repetition_separator
                else:
                    repetition_separator = '*'

                if hasattr(self, 'standard_identifier'):
                    standard_identifier = self.standard_identifier
                else:
                    standard_identifier = 'U'


                params = {
                    'group': 'FA',
                    'isa_data': {
                        'sender_id_qualifier': self.sender_id_qualifier,
                        'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                        'receiver_id_qualifier': self.receiver_id_qualifier_997,
                        'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                        'edi_x12_version_isa': '00401',
                        'isa_control_number': isa_control_number,
                        'date': date_now.strftime('%y%m%d'),
                        'time': date_now.strftime('%H%M'),
                        'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                        'repetition_separator': repetition_separator,
                        'standard_identifier': standard_identifier
                    },
                    'gs_data': {
                        'group': 'FA',
                        'sender_id': self.sender_id,
                        'receiver_id': receiver_id,
                        'date': date_now.strftime('%Y%m%d'),
                        'time': date_now.strftime('%H%M'),
                        'gs_control_number': gs_control_number,
                        'edi_x12_version_gs': '00' + self.edi_x12_version + 'VICS',
                    }
                }


                data.append(self.wrap(segments, params))
        return data
