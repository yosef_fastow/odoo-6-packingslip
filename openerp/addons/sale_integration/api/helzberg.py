# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.helzberg_input_edi_parser import EDIParser
import random
from datetime import datetime, timedelta
from apiopenerp import ApiOpenerp
from dateutil import tz
import logging
from math import ceil
from openerp.addons.pf_utils.utils.yamlutils import YamlObject

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            1143),
    ('vendor_name',                 'Vendor name',              "Helzberg"),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '006965735'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    '01'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('contact_email',               'Contact Email',            'avi@delmarintl.ca'),
    ('edi_x12_version',             'EDI X12 Version',          '4030'),
    ('filename_format',             'Filename Format',          'DEL{edi_type}_{date}.out'),
    ('line_terminator',             'Line Terminator',          r'\n'),
    ('repetition_separator',        'Repetition Separator',     '|'),
    ('environment_mode',            'Environment Mode',         'P'),   
    ('contact_address',             'Contact Address',          '1083 Main Street'),    
    ('contact_city',                'Contact City',             'Champlain'),
    ('contact_state',               'Contact State',            'NY'),
    ('contact_postal_code',         'Contact Postal Code',      '12919'),   
    ('contact_country',             'Contact Country',          'US'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class HelzbergApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, EDIParser)


class HelzbergApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(HelzbergApiClient, self).__init__(
            settings_variables, HelzbergOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = HelzbergApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = HelzbergApiAcknowledgmentOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):

        confirmApi = HelzbergApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = HelzbergApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = HelzbergApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmLoad(self, orders):
        numbers = []
        yaml_obj = YamlObject()
        for order_yaml in orders:
            order = yaml_obj.deserialize(_data=order_yaml['xml'])
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                api = HelzbergApiFunctionalAcknoledgment(self.settings, [order_yaml])
                api.process('send')
                self.extend_log(api)
        return True


class HelzbergApiGetOrders(HelzbergApi):
    """EDI/V4030 X12/850: 850 Purchase Order"""

    def __init__(self, settings):
        super(HelzbergApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class HelzbergApiChangeOrders(HelzbergApi):
    """EDI/V4030 X12/860: 860 Purchase Order Change"""

    def __init__(self, settings):
        super(HelzbergApiChangeOrders, self).__init__(settings)
        self.edi_type = '860'


class HelzbergApiInvoiceOrders(HelzbergApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(HelzbergApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],  
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        ref = 'REF*[1]%(code)s*[2]%(data)s'
        # segments.append(self.insertToStr(ref, {
        #     # 11 - Account 
        #     'code': '11',
        #     'data': self.invoiceLines[0]['order_additional_fields'].get('customer_order_number', None) # ???????
        # }))

        segments.append(self.insertToStr(ref, {
            # VN - Vendor Number
            'code': 'VN',
            'data': self.invoiceLines[0]['order_additional_fields'] and
            self.invoiceLines[0]['order_additional_fields'].get('internal_vendor_number', None) or
            self.vendor_number
        }))

        per = "PER*[1]AR*[2]%(contact_name)s*[3]WP*[4]%(contact_phone)s*[5]EM*[6]%(contact_email)s"

        segments.append(self.insertToStr(per, {
            'contact_name': self.contact_name,
            'contact_phone': self.contact_phone,
            'contact_email': self.contact_email
        }))

        n1_st = 'N1*[1]%(adddress_identify_code)s*[2]%(name)s*[3]1*[4]%(identification_code)s'
        segments.append(self.insertToStr(n1_st, {
            'adddress_identify_code': 'RI',
            'name':  self.contact_name,
            'identification_code': '0010' # ??????
        }))

        n3_st = 'N3*[1]%(address1)s'
        segments.append(self.insertToStr(n3_st, {
            'address1': self.contact_address
        }))

        n4_vn = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s'
        segments.append(self.insertToStr(n4_vn, {
            'city': self.contact_city,
            'state': self.contact_state,
            'postal_code': self.contact_postal_code,
        }))

        terms_net_days = 5
        terms_net_due_date = ''
        if (external_date_order):
            terms_net_due_date = external_date_order + timedelta(days=terms_net_days)
        else:
            terms_net_due_date = datetime.utcnow() + timedelta(days=terms_net_days)
        terms_net_due_date = terms_net_due_date.strftime('%Y%m%d')

        itd = 'ITD*[1]%(code)s*[2]%(terms_basis_date_code)s*[3]%(discount_percentage)s*[4]%(discount_due_date)s*[5]%(discount_days_due)s*[6]%(terms_net_due_date)s*[7]%(terms_net_days)s*[8]*%(discount_amount)s[9]*[10]*[11]*[12]%(discount_amount_str)s'
        
        total_amount = float(self.invoiceLines[0]['amount_total'] or 0.0)
        discount_percentage = self.invoiceLines[0]['discount_percentage'] or ''
        discount_amount = 0.00
        if discount_percentage:
            try:
                discount_amount = (total_amount / 100) * float(discount_percentage)
            except:
                pass

        net_due_days = self.invoiceLines[0].get('net_due_days', '0')

        segments.append(self.insertToStr(itd, {
            'code': '14',  
            # 05 - Discount Not Applicable
            # 08 - Basic Discount Offered
            # 12 - 10 Days After End of Month (10 EOM)
            # 14 - Previously agreed upon
            'terms_basis_date_code': 3,
            # 1 Ship Date
            # 2 Delivery Date
            # 3 Invoice Date
            # 7 Effective Date
            'discount_percentage': discount_percentage,
            'discount_due_date': self.invoiceLines[0].get('net_due_date', ''),
            'discount_days_due': self.invoiceLines[0].get('net_due_days', ''),
            'terms_net_due_date': self.invoiceLines[0].get('net_due_date', ''),
            'terms_net_days': self.invoiceLines[0].get('net_due_days', ''),
            'discount_amount': '{0:.2f}'.format(discount_amount).replace('.', ''),
            'discount_amount_str': 'Net {0} Days'.format(net_due_days),
        }))

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]IN*[7]%(customer_sku)s*[8]VN*[9]%(customer_id_delmar)s'
        qty_segment = 'QTY*[1]%(qty_id)s*[2]%(qty)s'
        pid = 'PID*[1]F*[2]*[3]*[4]*[5]%(data)s'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['unit_cost'],
                'customer_sku': line['vendorSku'],
                'customer_id_delmar': line['merchantSKU'],
            }))

            segments.append(self.insertToStr(qty_segment, {
                'qty_id': '38', # ORIGINAL QTY
                'qty': qty
            }))

            segments.append(self.insertToStr(qty_segment, {
                'qty_id': '39', # SHIPPED QTY
                'qty': qty
            }))

            segments.append(self.insertToStr(qty_segment, {
                'qty_id': 'BQ', # BACKORDER QTY
                'qty': '0'
            }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'data': line['name'],
            }))

            total_qty_invoiced += qty

        cad = 'CAD*[1]*[2]*[3]*[4]%(carrier_code)s*[5]%(carrier)s*[6]*[7]%(reference_identification_qualifier)s*[8]%(reference_identification)s'
        segments.append(self.insertToStr(cad, {
            'carrier_code': self.invoiceLines[0]['carrier_code'],
            'carrier': self.invoiceLines[0]['carrier'],
            # 2I - Tracking Number
            # BM - Bill of Lading Number
            # CN - Carrier's Reference Number (PRO/Invoice)
            'reference_identification_qualifier': 'CN',
            'reference_identification': self.invoiceLines[0]['tracking_number'],
        }))

        tds = 'TDS*[1]%(total_amount)s*[2]%(total_after_discount)s*[3]%(total_after_discount)s*[4]%(discount_amount)s'

        total_amount_after_discount = total_amount - discount_amount
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(total_amount).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
            'total_after_discount': '{0:.2f}'.format(total_amount_after_discount).replace('.', ''),
            'discount_amount': '{0:.2f}'.format(discount_amount).replace('.', ''),
        }))

        ctt = 'CTT*[1]%(items_count)s*[2]%(total_qty_invoiced)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
            'total_qty_invoiced': total_qty_invoiced,
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class HelzbergApiAcknowledgmentOrders(HelzbergApi):
    """EDI/V4030 X12/855: 855 Purchase Order Acknowledgment"""

    def __init__(self, settings, lines, state):
        super(HelzbergApiAcknowledgmentOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel'):
            return self.accept_cancel()

        self.edi_type = '855'

        lines = []
        statuscode = 'AT'
        if (self.state == 'accepted'):
            statuscode = 'AT'
        elif (self.state == 'accepted_with_details'):
            # PO1/ACK segment usage required when using this code.
            # All ACK01 values must contain IA.
            statuscode = 'AC'

        segments = []

        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s'

        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.utcnow().strftime('%Y%m%d')

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        #MTX = 'MTX*[1]ZZZ*[2]%(message)'

        total_lines = 0
        for line in self.processingLines['lines']:
            po1 = "PO1*[1]*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]WH*[6]IN*[7]%(optionSku)s*[8]VN*[9]%(merchantSKU)s"

            segments.append(self.insertToStr(po1, {
                'qty': line['qty'],
                'unit_price': line['unit_price'],
                'optionSku': line['vendorSku'],
                'merchantSKU': line['merchantSKU']
            }))
            total_lines += 1

        ctt = "CTT*[1]%(po1_total)s"
        segments.append(self.insertToStr(ctt, {'po1_total': str(total_lines)}))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '865'
        segments = []

        st = 'ST*[1]865*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA*[1]01*[2]%(statuscode)s*[3]%(po_number)s*[4]*[5]*[6]%(po_date)s'

        date = datetime.strptime(self.processingLines[0]['create_date'], '%Y-%m-%d %H:%M:%S.%f')

        statuscode = 'AT'
        if self.state == 'accept_cancel':
            statuscode = 'AT'
        elif self.state == 'rejected_cancel':
            statuscode = 'RJ'

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
            #'ackn_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CA'})


class HelzbergApiConfirmShipment(HelzbergApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(HelzbergApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))
        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['invoice'], # self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        td1 = "TD1*[1]CTN*[2]%(lading_qty)s*[3]*[4]*[5]*[6]G*[7]%(weight)s*[8]LB"

        lading_qty = sum([int(x['product_qty']) for x in self.confirmLines if int(x['product_qty'])])
        if not lading_qty:
            lading_qty = 1
        weight = self.confirmLines[0]['weight']
        try:
            weight = float(weight)
            if not weight:
                weight = 1
        except:
            weight = 1

        segments.append(self.insertToStr(td1, {
            'lading_qty': lading_qty,
            'weight': int(ceil(weight * 0.0022)),
        }))

        td5 = 'TD5*[1]O*[2]2*[3]%(carrier)s*[4]%(transport_method)s*[5]%(carrier_name)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier_code'],
            'transport_method': 'U', # A = Air, U = Ground
            'carrier_name': self.confirmLines[0]['carrier']
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]CN*[2]%(data)s'

        segments.append(self.insertToStr(ref, {
            'data': self.confirmLines[0]['tracking_number'] # self.confirmLines[0]['invoice']
        }))

        per = "PER*[1]IC*[2]%(contact_name)s*[3]WP*[4]%(contact_phone)s*[5]EM*[6]%(contact_email)s"

        segments.append(self.insertToStr(per, {
            'contact_name': self.contact_name,
            'contact_phone': self.contact_phone,
            'contact_email': self.contact_email
        }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
            'time': shp_date.strftime('%H%M%S')
        }))

        # loop N1
        n1 = 'N1*[1]%(code)s*[2]%(data)s*[3]92*[4]0010'


        n3 = 'N3*[1]%(address1)s'
        n3_data = {
            'address1': self.confirmLines[0]['ship_to_address_line_1']
        }
        if self.confirmLines[0]['ship_to_address_line_2']:
            n3 += '*[2]%(address2)s'
            n3_data['address2'] = self.confirmLines[0]['ship_to_address_line_2']

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        # N1 SF
        segments.append(self.insertToStr(n1, {
            # SF - Ship From
            # ST - Ship To
            'code': 'ST',  # Ship To
            'data':  self.confirmLines[0]['ship_to_name_1']
        }))

        segments.append(self.insertToStr(n3, n3_data))

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['ship_to_city'],
            'state': self.confirmLines[0]['ship_to_state'],
            'zip': self.confirmLines[0]['ship_to_postal_code'],
            'country': self.confirmLines[0]['ship_to_country']
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]%(date)s*[5]*[6]%(contract_number)s'
        external_date_order_str = self.confirmLines[0]['external_date_order'] or ''
        external_date_order = shp_date
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number'],
            'date': external_date_order.strftime('%Y%m%d'),
            'contract_number': self.confirmLines[0]['invoice']
        }))

        td5 = 'TD5*[1]*[2]*[3]*[4]*[5]*[6]CC'
        segments.append(self.insertToStr(td5, {}))

        product_qualifiers = ['EN', 'UK', 'UP']

        lin = 'LIN*[1]*[2]IN*[3]%(customer_sku)s*[4]VN*[5]%(customer_id_delmar)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA*[4]*[5]%(original_qty)s*[6]EA'
        pid = 'PID*[1]%(description_type)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(data)s'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            segments.append(self.insertToStr(lin, {
                'customer_sku': line['vendorSku'],
                'customer_id_delmar': line['merchantSKU']
            }))
            segments.append(self.insertToStr(sn1, {
                'qty': int(line['product_uom_qty']),
                'original_qty': int(line['product_uom_qty'])
            }))
            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'characteristic_code': '08',
                # 08 Product
                # 75 Buyer's Color Description
                # 91 Buyer's Item Size Description
                'data': line['name'],
            }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class HelzbergApiUpdateQTY(HelzbergApi):
    """EDI/V4030 X12/846: 846 Inventory Inquiry"""

    def __init__(self, settings, lines):
        super(HelzbergApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]00*[2]ZZ*[3]%(inventory_feed_qualifier)s*[4]%(date)s*[5]%(time)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'date': date.strftime('%Y%m%d'),
            'time': date.strftime('%H%M%S'),
        }))

        n1 = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(inventory_feed_qualifier)s'
        segments.append(self.insertToStr(n1, {
            'code': 'SU',  # Vendor
            'name': self.sender_id,
            'identification_code_qualifier': '12',
            # 01 Duns (Dun & Bradstreet)
            # 02 SCAC (Standard Carrier Alpha Code)
            # 08 UCC EDI Communications ID (Comm ID)
            # 12 Phone (Telephone Companies)
            # 14 Duns Plus Suffix
            # ZZ Mutually Defined
            'inventory_feed_qualifier': self.contact_phone,
        }))

        n3 = 'N3*[1]%(address1)s'

        segments.append(self.insertToStr(n3, {
            'address1': self.contact_address,
        }))

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'

        segments.append(self.insertToStr(n4, {
            'city': self.contact_city,
            'state': self.contact_state,
            'postal_code': self.contact_postal_code,
            'country': self.contact_country
        }))

        per = 'PER*[1]%(code)s*[2]%(name)s*[3]%(com_number_code)s*[4]%(com_number)s*[5]EM*[6]%(con_email)s'
        segments.append(self.insertToStr(per, {
            'code': 'IC',  # Information Contact
            'name': self.contact_name,
            'com_number_code': 'WP',  # Telephone
            'com_number': self.contact_phone,
            'con_email': self.contact_email,
        }))

        product_identifying_mapping = {
            'IN': ('sku', 13),
            'VN': ('merchantSKU', 14),
        }

        lin = 'LIN*[1]*[2]%(product_qualifier)s*[3]%(product_identifying_number)s*[4]VN*[5]%(sku)s'
        pid = 'PID*[1]%(code)s*[2]*[3]*[4]*[5]%(desc)s'
        ctp = 'CTP*[1]*[2]QTE*[3]%(price)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'

        total_lin = 0

        for line in self.updateLines:
            product_qualifier = 'IN'
            if (not line.get('customer_sku', False)) or (not line.get('customer_id_delmar', False)):
                self.append_to_revision_lines(line, 'bad')
                continue
            
            self.append_to_revision_lines(line, 'good')
            total_lin += 1
            lin2 = lin
            size_code = ''
            size = ''
            lin_data = {
                'product_qualifier': product_qualifier,
                'product_identifying_number': str(line['customer_sku']),
                'sku': str(line['customer_id_delmar']),
            }
            if line.get('size'):
                lin2 = lin + '*[6]%(color_code)s*[7]%(color)s*[8]%(size_code)s*[9]%(size)s'
                lin_data['color_code'] = '' # CM: National Retail Merchants Association Color Code, VE: Vendor Color
                lin_data['color'] = ''
                lin_data['size_code'] = 'SZ'
                lin_data['size'] = str(line['size']) + '0'

            segments.append(self.insertToStr(lin2, lin_data))

            segments.append(self.insertToStr(pid, {
                'code': 'F',  # Free-form
                'desc': str(line['description'])[0:80]
            }))

            segments.append(self.insertToStr(ctp, {
                'price': str(line['customer_price']).replace('.00', '').replace('.0', '')
            }))

            segments.append(self.insertToStr(qty, {
                'code': 'V2',  # Quantity Available for Sale (stock quantity)
                'qty': str(line['qty']),  # Product
                'uof_code': 'EA'  # Each
            }))

        ctt = 'CTT*[1]%(total_segments)s'

        segments.append(self.insertToStr(ctt, {
            'total_segments': str(total_lin)
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class HelzbergApiFunctionalAcknoledgment(HelzbergApi):

    orders = []

    def __init__(self, settings, orders):
        super(HelzbergApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            order = yaml_obj.deserialize(_data=order_yaml['xml'])
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'

                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak5 = 'AK5*[1]%(acknowledgement_code)s*[2]%(error_code)s*[3]%(error_code)s'
                segments.append(self.insertToStr(ak5, {
                    'acknowledgement_code': 'A',
                    'error_code': ''
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class HelzbergOpenerp(ApiOpenerp):

    def __init__(self):

        super(HelzbergOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line.get('name', ''),
            'customer_sku': line['customer_sku'],
            'customer_id_delmar': line['customer_id_delmar'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'cost': line['cost'],
            'qty': line['qty'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = {}
        field_list = ['customer_sku', 'customer_id_delmar']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[
                                                                                          field],
                                                                                      ('customer_sku', 'customer_id_delmar')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False

                    if not line.get('name', False):
                        line_obj['name'] = product.name

                    # product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    # if product_cost:
                    #     line['cost'] = product_cost
                    #     break
                    # else:
                    #     line['cost'] = False
                    #     line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])
                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        order_lines = []
        for order_line in sale_order.order_line:
            line = {}
            price = order_line.price_unit
            if float(price).is_integer():
                price = int(price)
            
            line['qty'] = int(order_line.product_uom_qty)
            line['unit_price'] = price
            line['merchantSKU'] = order_line.merchantSKU
            line['vendorSku'] = order_line.vendorSku
            order_lines.append(line)

        result = {
            'po_number': sale_order['po_number'],
            'external_customer_order_id': sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR',
            'lines': order_lines,
        }

        return result
