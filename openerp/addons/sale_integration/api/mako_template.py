from mako.template import Template
from mako.runtime import Context
from StringIO import StringIO
from os import access, F_OK, R_OK
import logging

_logger = logging.getLogger(__name__)


class MakoTemplate(object):

    def __init__(self, tpl=None, type_tpl=None):
        super(MakoTemplate, self).__init__()
        if(type_tpl is not None):
            if(tpl is not None):
                if(type_tpl == "file"):
                    if(access(tpl, F_OK)):
                        if(access(tpl, R_OK)):
                            self.tpl = tpl
                        else:
                            _logger.error("No read permission to template: %s" % tpl)
                            tpl = None
                            type_tpl = None
                    else:
                        _logger.error("Template: %s not found" % tpl)
                        tpl = None
                        type_tpl = None
                elif(type_tpl == "string"):
                    if(isinstance(tpl, str) or isinstance(tpl, unicode)):
                        self.tpl = tpl
                    else:
                        _logger.error("Template not a string!(tpl: %s, type: %s)" % (tpl, type(tpl)))
                        tpl = None
                        type_tpl = None
                else:
                    _logger.error("Unknown template type: %s" % type_tpl)
                    tpl = None
                    type_tpl = None
            else:
                _logger.error("Template if empty!")
                tpl = None
                type_tpl = None
        else:
            _logger.error("Type template is empty!")
            tpl = None
            type_tpl = None
        self.tpl = tpl
        self.type_tpl = type_tpl

    def render_data(self, **kwargs):
        if((self.tpl is not None) and (self.type_tpl is not None)):
            if(self.type_tpl == 'file'):
                with open(self.tpl) as template_xml:
                    tpl_data = template_xml.read()
            elif(self.type_tpl == 'string'):
                tpl_data = self.tpl
            tpl = Template(tpl_data)
            buf = StringIO()
            ctx = Context(buf, **kwargs)
            tpl.render_context(ctx)
            data = buf.getvalue()
            if data:
                result = data
            else:
                result = None
        else:
            result = None
        return result
