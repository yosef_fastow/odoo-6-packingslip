# -*- coding: utf-8 -*-
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import time
from utils import feedutils as feed_utils
from customer_parsers import wayfair_txt_parser
from customer_parsers import wayfair_xml_parser
from configparser import ConfigParser
import os
import logging

_logger = logging.getLogger(__name__)


class WayfairApi(ApiClient):
    def __init__(self, settings):
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/wayfair_settings.ini' % (os.path.dirname(__file__)))
        self.saveFlag = settings["saveFlag"] if 'saveFlag' in settings else False
        self.customer = settings["customer"]
        self.multi_ftp_settings = settings["multi_ftp_settings"]
        self._log = []

    def get_ftp_setting(self, ftp_settings, setting_name):
        current_setting = False
        ftp_path = False
        for setting in ftp_settings:
            if setting.action == setting_name:
                current_setting = setting.ftp_setting_id
                ftp_path = setting['ftp_path']
                break
        return current_setting, ftp_path


class WayfairApiClient(AbsApiClient):
    def __init__(self, settings_variables):
        super(WayfairApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = WayfairApiGetOrdersXML

    def loadOrders(self, saveInFolder=False):
        self.settings["saveFlag"] = saveInFolder and saveInFolder or False
        ordersApi = WayfairApiGetOrdersXML(self.settings)
        orders = ordersApi.send()
        self.extend_log(ordersApi)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = WayfairApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        confirmApi = WayfairApiConfirmOrders(self.settings, lines)
        confirmApi.send()
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = WayfairApiInvoiceOrders(self.settings, lines)
            invoiceApi.send()
            self.extend_log(invoiceApi)

        return True

    def updateQTY(self, lines, mode=None):
        updateApi = WayfairApiUpdateQTY(self.settings, lines)
        if updateApi.cur_ftp_setting:
            updateApi.send()
            self.check_and_set_filename_inventory(updateApi)
        else:
            _logger.error("Wayfair not found ftp_setting (inventory)")
        self.extend_log(updateApi)
        return updateApi.revision_lines


class WayfairApiGetOrdersXML(WayfairApi):
    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(WayfairApiGetOrdersXML, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = \
            self.get_ftp_setting(self.multi_ftp_settings, 'load_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"],
                True)
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_local_dir': '%s' % str(os.path.join(settings['input_local_path'], "wayfair", "orders")),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "wayfair", "orders", "now")
            }
            self.set_ftp_method('get')
            self.set_path_list(path_list)

    def parse_response(self, response):
        txt_parser = wayfair_txt_parser.WayfairApiGetOrdersXML()
        ordersList = txt_parser.parse_response(response)
        return ordersList


class WayfairApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        xml_parser = wayfair_xml_parser.WayfairApiGetOrderObj()
        ordersObj = xml_parser.getOrderObj(self.xml)
        return ordersObj


class WayfairApiConfirmOrders(WayfairApi):

    delimetr = "|"
    filename = ""
    confirmLines = []
    name = "ConfirmOrders"

    def __init__(self, settings, lines):
        super(WayfairApiConfirmOrders, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = \
            self.get_ftp_setting(self.multi_ftp_settings, 'confirm_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "wayfair", "confirm", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        po_number = ''
        for line in self.confirmLines:
            if line['po_number']:
                po_number = line['po_number']
                break
        self.filename = "WFR%s-%s.txt" % (po_number, datetime.now().strftime("%Y%m%d-%H%M%S"))

    def upload_data(self):
        # WARN: self.confirmLines[0] - baaaad thing... please make better or add checking
        data = ''
        try:
            for line in self.confirmLines:
                if line['carrier']:
                    carrier = line['carrier'].split(',')
                    if (len(carrier) == 2):
                        line['carrier'] = carrier[1].strip()
                if line['po_date']:
                    if ('-' not in line['po_date']):
                        po_date = line['po_date']
                        po_date = "%s-%s-%s" % (po_date[0:4], po_date[4:6], po_date[6:8])
                        line['po_date'] = po_date
                dt = datetime.now()
                if (line['shp_date']):
                    dt = datetime.strptime(line['shp_date'], "%Y-%m-%d %H:%M:%S")
                line['date'] = dt.strftime("%Y-%m-%d")
            data = feed_utils.FeedUtills(self.config['ShippingConfirmationHeader'].items(), self.confirmLines[0]).create_csv_line(self.delimetr)
            for line in self.confirmLines:
                line['int_product_qty'] = int(line['product_qty'])
                data += feed_utils.FeedUtills(self.config['ShippingConfirmationDetail'].items(), line).create_csv_line(self.delimetr)
            return data
        except Exception, e:
            _logger.error("error method confirmShipment (Wayfair): %s" % e.message)
            self._log.append({
                    'title': 'error method confirmShipment (Wayfair)',
                    'msg': e,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

class WayfairApiInvoiceOrders(WayfairApi):

    delimetr = "|"
    filename = ""
    confirmLines = []
    name = "InvoiceOrders"

    def __init__(self, settings, lines):
        super(WayfairApiInvoiceOrders, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = \
            self.get_ftp_setting(self.multi_ftp_settings, 'invoice_orders') #TODO: not sure about 'invoices' value
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "wayfair", "invoices", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        po_number = ''
        for line in self.confirmLines:
            if line['po_number']:
                po_number = line['po_number']
                break
        self.filename = "INV%s-%s.txt" % (po_number, datetime.now().strftime("%Y%m%d-%H%M%S"))

    # Just coppied from confirmation need to be changed
    def upload_data(self):
        # WARN: self.confirmLines[0] - baaaad thing... please make better or add checking
        data = ''
        try:
            for line in self.confirmLines:
                if line['po_date']:
                    if ('-' not in line['po_date']):
                        po_date = line['po_date']
                        po_date = "%s-%s-%s" % (po_date[0:4], po_date[4:6], po_date[6:8])
                        line['po_date'] = po_date
                dt = datetime.now()
                if (line['shp_date']):
                    dt = datetime.strptime(line['shp_date'], "%Y-%m-%d %H:%M:%S")
                line['date'] = dt.strftime("%Y-%m-%d")
            data = feed_utils.FeedUtills(self.config['InvoiceHeader'].items(), self.confirmLines[0]).create_csv_line(self.delimetr)
            for line in self.confirmLines:
                line['int_product_qty'] = int(line['product_qty'])
                data += feed_utils.FeedUtills(self.config['InvoiceDetail'].items(), line).create_csv_line(self.delimetr)
            return data
        except Exception, e:
            _logger.error("error method confirmShipment (Wayfair):  %s" % e.message)
            self._log.append({
                    'title': 'error method confirmShipment (Wayfair)',
                    'msg': e,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })


class WayfairApiUpdateQTY(WayfairApi):
    filename = ""
    confirmLines = []
    name = "UpdateQTY"
    cur_ftp_setting = ""

    def __init__(self, settings, lines):
        super(WayfairApiUpdateQTY, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = \
            self.get_ftp_setting(self.multi_ftp_settings, 'inventory')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "wayfair", "inventory", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        self.filename = "WFR%s.txt" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):

        delimetr = "|"
        data = []

        header = feed_utils.FeedUtills(self.config['InventorySetting'].items(), None).create_csv_header(delimetr)
        data.append(header)

        for line in self.confirmLines:
            # if product don't have a customer_sku -> continue
            try:
                if not (line.get('eta', False) or False):
                    line['eta'] = ''
                else:
                    line['eta'] = datetime.strptime(line['eta'], "%m/%d/%Y")
            except:
                line['eta'] = ''

            if not line.get("customer_sku", False):
                self.append_to_revision_lines(line, 'bad')
                continue

            if not line["dnr_flag"]:
                line["dnr_flag"] = 0

            if line["base_description"] is None:
                line["base_description"] = line["description"]

            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line(delimetr)
            if SkuRecord:
                data.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')

        return "".join(data)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
