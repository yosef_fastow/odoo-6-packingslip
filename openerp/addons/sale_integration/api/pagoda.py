# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from customer_parsers import pagoda_csv_parser, pagoda_xml_parser
from utils import feedutils as feed_utils
from configparser import ConfigParser
from apiopenerp import ApiOpenerp
import os
import logging
from datetime import datetime

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class PagodaApi(FtpClient):

    def __init__(self, settings):
        super(PagodaApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/pagoda_settings.ini' % (os.path.dirname(__file__)))

    def set_decrypt_data(self, data):
        gpg = self.get_gnupg(self.gnupghome)
        res = gpg.decrypt(str(data), passphrase=self.passphrase).data
        return res


class PagodaApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(PagodaApiClient, self).__init__(
            settings_variables, PagodaOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = PagodaApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = PagodaApiGetOrderObj(xml, self.settings['customer'])
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = PagodaApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        return True


class PagodaApiGetOrdersXML(PagodaApi):

    def __init__(self, settings):
        super(PagodaApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')

    def parse_response(self, response):
        csv_parser = pagoda_csv_parser.PagodaApiGetOrdersXML()
        ordersList = csv_parser.parse_response(response)
        return ordersList


class PagodaApiGetOrderObj():

    def __init__(self, xml, customer):
        self.xml = xml
        self.customer = customer

    def getOrderObj(self):
        xml_parser = pagoda_xml_parser.PagodaApiGetOrderObj()
        ordersObj = xml_parser.getOrderObj(self.xml)
        ordersObj.update({'partner_id': 'pagoda'})

        return ordersObj


class PagodaApiUpdateQTY(PagodaApi):
    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(PagodaApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.confirmLines = lines
        self.filename = "inv.txt"
        self.filename_local = "inv(%s).txt" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        data = "Host SKU\tVendor SKU\tHost Item\tDescription\tQty On Hand\tPrice\n"
        for line in self.confirmLines:
            if line['customer_price']:
                line['customer_price'] = "%.2f" % float(line['customer_price'])
            SkuRecord = feed_utils.FeedUtills(self.config['InventorySetting'].items(), line).create_csv_line('\t', True)
            if SkuRecord:
                data += SkuRecord
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return data[:-2]


class PagodaOpenerp(ApiOpenerp):

    def __init__(self):
        super(PagodaOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'additional_fields': [
                (0, 0, {'name': 'store', 'label': 'Store',
                        'value': line.get('store', '')}),
            ]
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if size and size.id:
                    line_obj['size_id'] = size.id
                elif line.get('size', False):
                    try:
                        size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                    except Exception:
                        size_ids = []
                    if len(size_ids) > 0:
                        line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                        line['size'] = False
                else:
                    line_obj['size_id'] = False
                if product:
                    line_obj["product_id"] = product.id
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj["notes"] = "Can't find product cost.\n"

        return line_obj
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
