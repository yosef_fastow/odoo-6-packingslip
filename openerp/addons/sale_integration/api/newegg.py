# -*- coding: utf-8 -*-
from lxml import etree
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
from apiopenerp import ApiOpenerp
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from pf_utils.utils.re_utils import f_d


class NewEggApi(ApiClient):
    host = ""
    merchantKey = ""
    authenticationKey = ""
    api_url = ""
    name = ""
    method = ""

    def __init__(self, settings):
        self.host = settings["host"]
        self.merchantKey = settings["merchantKey"]
        self.authenticationKey = settings["authenticationKey"]
        self.secret_key = settings["secret_key"]
        self.backup_local_path = settings['backup_local_path']
        self.api_url = self.api_url.replace('{host}', self.host)
        self.api_url = self.api_url.replace('{sellerid}', self.merchantKey)
        self._log = []

    def prepare_data(self, data=''):
        self._log.append({
            'title': "Send %s NewEgg" % self.name,
            'msg': data + ' ' + self.api_url,
            'type': 'send',
            'create_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        })
        return ' '.join(data.split())

    def get_method(self):
        res = False
        if(self.method == 'PUT'):
            res = 'PUT'
        return res

    def get_request_headers(self):
        headers = {
            "Authorization": self.authenticationKey,
            "SecretKey": self.secret_key,
            "Content-Type": 'application/xml',
            "Accept": 'application/xml'
        }
        return headers

    def prepare_headers(self, headers, data):
        headers["Content-Length"] = len(data)
        headers["Content-Type"] = "application/xml"
        return headers

    def check_response(self, response):
        res = True
        if(not response):
            return False

        tree = etree.XML(response)
        log_msg = {
            'title': "Recive %s NewEgg" % self.name,
            'msg': response,
            'type': 'recive',
            'create_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

        if(tree.find('Error') is not None):
            error_code = tree.findtext('Error/Code')
            # SO011:  # Only unshipped orders can be shipped. The order status is currently Closed
            if error_code != 'SO011':
                error_type = tree.findtext('Error/Message')
                log_msg.update({'title': "Recive %s ERROR %s NewEgg" % (self.name, error_type)})
                res = False

        elif (tree.find('IsSuccess') is not None):
            is_success = tree.findtext('IsSuccess').lower()
            if is_success != 'true':
                log_msg.update({'msg': response})
                res = False

            elif(tree.find('Result/Shipment') is not None):
                is_processed = (tree.findtext('Result/Shipment/PackageList/Package/ProcessStatus') or '').lower()
                if is_processed != 'true':
                    log_msg.update({'title': "Recive %s ERROR Process Error NewEgg" % (self.name)})
                    res = False

        self._log.append(log_msg)
        return res


class NewEggApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(NewEggApiClient, self).__init__(
            settings_variables, NewEggOpenerp, False, False)
        self.load_orders_api = NewEggApiGetOrdersXML

    def loadOrders(self):
        orders = self.load_orders_api.send()
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = NewEggApiGetOrderObj(xml, self.settings['customer'])
        order = ordersApi.getOrderObj()
        return order

    def orderConfirmation(self, order_id):
        orderConfirmationApi = NewEggApiOrderConfirmation(self.settings, order_id)
        orderConfirmationApi.send()
        self.extend_log(orderConfirmationApi)
        return True

    def confirmShipment(self, order):
        self.settings["response"] = ""
        confirmApi = NewEggApiConfirmShipment(self.settings, order)
        res = confirmApi.send()
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        self.settings["response"] = ""
        updateApi = NewEggApiUpdateQTY(self.settings, lines)
        updateApi.send()
        self.extend_log(updateApi)
        self.check_and_set_filename_inventory(updateApi)
        return updateApi.revision_lines

    def cancelOrder(self, lines):
        orderApi = NewEggApiOrderCancelByLine(self.settings, lines)
        res = orderApi.send()
        self.extend_log(orderApi)
        return res


class NewEggApiGetOrdersXML(NewEggApi):

    name = "GetOrderInfoRequest"
    api_url = "{host}/ordermgmt/order/orderinfo?sellerid={sellerid}&version=304"
    method = "PUT"

    def __init__(self, settings):
        super(NewEggApiGetOrdersXML, self).__init__(settings)
        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            settings['customer'],
            'load_orders',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'newegg'),
            datetime.utcnow()
        )

    def get_request_data(self):
        data = """
            <NeweggAPIRequest>
                <OperationType>GetOrderInfoRequest</OperationType>
                <RequestBody>
                    <PageIndex>1</PageIndex>
                    <PageSize>100</PageSize>
                    <RequestCriteria>
                        <Status>0</Status>
                        <OrderDownloaded>1</OrderDownloaded>
                        <Type>0</Type>
                    </RequestCriteria>
                </RequestBody>
            </NeweggAPIRequest>"""
        return data

    def parse_response(self, response):
        if(not response):
            return []
        tree = etree.XML(response)
        ordersList = []
        if(tree is not None and tree.findtext('IsSuccess') == 'true'):
            root = tree.find('ResponseBody')
            #if int(root.find('PageInfo/PageSize').text) > 0:
            orders = root.findall('OrderInfoList/OrderInfo')
            if(len(orders) > 0):
                for order in orders:
                    ordersObj = {}
                    ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                    ordersObj['name'] = order.findtext('OrderNumber')
                    ordersObj['order_id'] = order.findtext('OrderNumber')
                    ordersObj['OrderStatus'] = order.findtext('OrderStatus')
                    #0 - Unshipped
                    #1 - Partially Shipped
                    #2 - Shipped
                    #3 - Invoiced
                    #4 - Voided
                    if bool(ordersObj['OrderStatus']) and int(ordersObj['OrderStatus']) == 4:
                        ordersObj['name'] = 'CANCEL ' + ordersObj['name']
                    ordersList.append(ordersObj)
        return ordersList


class NewEggApiGetOrderObj():

    def __init__(self, xml, customer):
        self.xml = xml
        self.customer = customer

    def getOrderObj(self):
        if(self.xml == ""):
            return {}
        tree = etree.XML(self.xml)

        ordersObj = {}
        if(tree is not None and bool(tree.findtext('OrderNumber'))):
            ordersObj['order_id'] = tree.findtext('OrderNumber')
            ordersObj['partner_id'] = self.customer
            if(ordersObj['order_id']):
                ordersObj['lines'] = []
                ordersObj['OrderStatus'] = tree.findtext('OrderStatus')
                #0 - Unshipped
                #1 - Partially Shipped
                #2 - Shipped
                #3 - Invoiced
                #4 - Voided
                if bool(ordersObj['OrderStatus']) and int(ordersObj['OrderStatus']) == 4:
                    ordersObj['external_customer_order_id'] = ordersObj['order_id']
                    for line in tree.find('ItemInfoList'):
                        # 1 - Unshipped
                        # 2 - Shipped
                        # 3 - Cancelled
                        if int(line.findtext('Status')) == 3:
                            ordersObj['lines'].append(line.findtext('NeweggItemNumber'))

                    return ordersObj

                ordersObj['SellerID'] = tree.findtext('SellerID')
                ordersObj['InvoiceNumber'] = tree.findtext('InvoiceNumber')
                dt = datetime.strptime(tree.findtext('OrderDate'), "%m/%d/%Y %H:%M:%S")
                ordersObj['external_date_order'] = dt.strftime('%Y/%m/%d %H:%M:%S')

                ordersObj['OrderStatusDescription'] = tree.findtext('OrderStatusDescription')
                ordersObj['address'] = {}
                ordersObj['address']['ship'] = {}
                ordersObj['address']['ship']['name'] = tree.findtext('CustomerName')
                ordersObj['address']['ship']['email'] = tree.findtext('CustomerEmailAddress')
                ordersObj['address']['ship']['address1'] = tree.findtext('ShipToAddress1')
                ordersObj['address']['ship']['address2'] = tree.findtext('ShipToAddress2')
                ordersObj['address']['ship']['city'] = tree.findtext('ShipToCityName')
                ordersObj['address']['ship']['state'] = tree.findtext('ShipToStateCode')
                ordersObj['address']['ship']['zip'] = tree.findtext('ShipToZipCode')
                ordersObj['address']['ship']['country'] = tree.findtext('ShipToCountryCode')
                ordersObj['address']['ship']['phone'] = tree.findtext('CustomerPhoneNumber')
                ordersObj['address']['ship']['company'] = tree.findtext('ShipToCompany')
                ordersObj['carrier'] = tree.findtext('ShipService')
                ordersObj['OrderStatus'] = tree.findtext('OrderStatus')
                ordersObj['OrderItemAmount'] = tree.findtext('OrderItemAmount')
                ordersObj['shipCost'] = tree.findtext('ShippingAmount')
                ordersObj['DiscountAmount'] = tree.findtext('DiscountAmount')
                ordersObj['GSTorHSTAmount'] = tree.findtext('GSTorHSTAmount')
                ordersObj['PSTorQSTAmount'] = tree.findtext('PSTorQSTAmount')
                ordersObj['RefundAmount'] = tree.findtext('RefundAmount')
                ordersObj['OrderTotalAmount'] = tree.findtext('OrderTotalAmount')
                ordersObj['OrderQty'] = tree.findtext('OrderQty')
                ordersObj['IsAutoVoid'] = tree.findtext('IsAutoVoid')
                i = 0
                for line in tree.find('ItemInfoList'):
                    i += 1
                    lineObj = {'id': i}
                    sku = line.findtext('SellerPartNumber')
                    if sku.find("/") == -1:
                        lineObj['sku'] = sku
                        lineObj['size'] = False
                    else:
                        lineObj['sku'] = sku[:sku.find("/")]
                        lineObj['size'] = sku[sku.find("/") + 1:]
                    lineObj['SellerPartNumber'] = sku
                    lineObj['merchantSKU'] = line.findtext('NeweggItemNumber')
                    lineObj['manufacturerSKU'] = line.findtext('MfrPartNumber')
                    lineObj['upc'] = line.findtext('UPCCode')
                    lineObj['name'] = line.findtext('Description')
                    lineObj['qty'] = line.findtext('OrderedQty')
                    lineObj['ShippedQty'] = line.findtext('ShippedQty')
                    lineObj['cost'] = line.findtext('UnitPrice')
                    lineObj['ExtendUnitPrice'] = line.findtext('ExtendUnitPrice')
                    lineObj['UnitShippingCharge'] = line.findtext('UnitShippingCharge')
                    lineObj['ExtendShippingCharge'] = line.findtext('ExtendShippingCharge')
                    lineObj['Status'] = line.findtext('Status')
                    lineObj['StatusDescription'] = line.findtext('StatusDescription')
                    ordersObj['lines'].append(lineObj)

        return ordersObj


class NewEggApiOrderConfirmation(NewEggApi):

    name = "OrderConfirmationRequest"
    api_url = "{host}/ordermgmt/orderstatus/orders/confirmation?sellerid={sellerid}"
    method = "POST"

    def __init__(self, settings, order_id):
        super(NewEggApiOrderConfirmation, self).__init__(settings)
        self.order_id = order_id
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "confirm_load",
            "now")
        self.filename = "load(%s).xml" % str(order_id)

    def get_request_data(self):
        data = """
            <NeweggAPIRequest>
                <OperationType>OrderConfirmationRequest</OperationType>
                <RequestBody>
                    <DownloadedOrderList>
                        <OrderNumber>%s</OrderNumber>
                    </DownloadedOrderList>
                </RequestBody>
            </NeweggAPIRequest>""" % self.order_id

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(data), 'wb').create()
        return data

    def parse_response(self, response):
        if not response:
            return False
        return True


class NewEggApiConfirmShipment(NewEggApi):

    name = "UpdateOrderStatus"
    api_url = "{host}/ordermgmt/orderstatus/orders/{ordernumber}?sellerid={sellerid}&version=304"
    method = "PUT"
    lines = []

    def __init__(self, settings, lines):
        super(NewEggApiConfirmShipment, self).__init__(settings)
        self.lines = lines
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "confirm_shipment",
            "now")
        self.filename = "ship(%s).xml" % str(lines[0]['origin'])

    def get_request_data(self):
        lineObj = self.lines[0]
        self.api_url = self.api_url.replace('{ordernumber}', lineObj['external_customer_order_id'])
        dt = datetime.strptime(lineObj["shp_date"], "%Y-%m-%d %H:%M:%S")
        items = ""
        for line in self.lines:
            items += """<Item>
                        <SellerPartNumber>%s</SellerPartNumber>
                        <ShippedQty>%s</ShippedQty>
                    </Item>""" % (line['vendorSku'], int(line['product_qty'])) # FIXME! Should be SellerPartNumber and not MfrPartNumber (vendorSku)
        data = """
            <UpdateOrderStatus>
                <Action>2</Action>
                <Value>
                    <![CDATA[
                        <Shipment>
                            <Header>
                                <SellerID>%s</SellerID>
                                <SONumber>%s</SONumber>
                            </Header>
                            <PackageList>
                                <Package>
                                    <TrackingNumber>%s</TrackingNumber>
                                    <ShipDate>%s</ShipDate>
                                    <ShipFromAddress>%s</ShipFromAddress>
                                    <ShipFromAddress2>%s</ShipFromAddress2>
                                    <ShipFromCity>%s</ShipFromCity>
                                    <ShipFromCountry>%s</ShipFromCountry>
                                    <ShipFromState>%s</ShipFromState>
                                    <ShipFromZipcode>%s</ShipFromZipcode>
                                    <ShipCarrier>%s</ShipCarrier>
                                    <ShipService>%s</ShipService>
                                    <ItemList>
                                        %s
                                    </ItemList>
                                </Package>
                            </PackageList>
                        </Shipment>
                    ]]>
                </Value>
            </UpdateOrderStatus>""" % (self.merchantKey,
                                       lineObj['external_customer_order_id'],
                                       lineObj['tracking_number'],
                                       dt.strftime("%Y-%m-%d"),
                                       lineObj['shipFrom']['street'],
                                       lineObj['shipFrom']['street2'],
                                       lineObj['shipFrom']['city'],
                                       lineObj['shipFrom']['country'],
                                       lineObj['shipFrom']['state'],
                                       lineObj['shipFrom']['zip'],
                                       lineObj['carrier'],
                                       lineObj['carrier'],
                                       items)

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(data), 'wb').create()
        return data

    def parse_response(self, response):
        if(not response):
            return False
        return True


class NewEggApiUpdateQTY(NewEggApi):

    api_url = "{host}/datafeedmgmt/feeds/submitfeed?sellerid={sellerid}&requesttype=INVENTORY_AND_PRICE_DATA"

    name = "Inventory"
    updateLines = []
    method = "POST"

    def __init__(self, settings, lines):
        super(NewEggApiUpdateQTY, self).__init__(settings)
        self.updateLines = lines
        self.customer = settings["customer"]
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "inventory",
            "now")
        self.filename = "inv(%s).xml" % datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def get_request_data(self):
        data = """<NeweggEnvelope xsi:noNamespaceSchemaLocation="Inventory.xsd"
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <Header>
                        <DocumentVersion>1.0</DocumentVersion>
                    </Header>
                    <MessageType>Inventory</MessageType>
                    <Overwrite>No</Overwrite>
                    <Message>
                        <Inventory>
                            %s
                        </Inventory>
                    </Message>
                    </NeweggEnvelope>"""
        lines = []
        if self.customer == 'newegg_ca':
            source_field_name = 'upc'
        else:
            source_field_name = 'customer_id_delmar'
        for line in self.updateLines:
            if bool(line[source_field_name]):
                if line['customer_price']:
                    try:
                        line['customer_price'] = "%.2f" % float(line['customer_price'])
                        line['msrp'] = "%.2f" % (1.4 * float(line['customer_price']))
                        lines.append("""
                            <Item>
                                <SellerPartNumber>%s</SellerPartNumber>
                                <SellingPrice>%s</SellingPrice>
                                <Inventory>%s</Inventory>
                                <FulfillmentOption>Merchant</FulfillmentOption>
                                <ActivationMark>%s</ActivationMark>
                                <MSRP>%s</MSRP>
                            </Item>
                        """ % (
                            line[source_field_name],
                            line['customer_price'],
                            int(line['qty']),
                            str(bool(int(line['qty']))),
                            line['msrp'])
                        )
                        self.append_to_revision_lines(line, 'good')
                    except:
                        self.append_to_revision_lines(line, 'bad')
                else:
                    self.append_to_revision_lines(line, 'bad')
            else:
                self.append_to_revision_lines(line, 'bad')

        send_data = data % "".join(lines)

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(send_data), 'wb').create()

        return send_data

    def parse_response(self, response):
        if(not response):
            return False
        return True


class NewEggApiOrderCancel(NewEggApi):
    name = "UpdateOrderStatus"
    api_url = "{host}/ordermgmt/orderstatus/orders/{ordernumber}?sellerid={sellerid}&version=304"
    method = "PUT"
    cancel_code = ''

    def __init__(self, settings, order_id, cancel_code='72'):
        super(NewEggApiOrderCancel, self).__init__(settings)
        self.order_id = order_id
        self.cancel_code = cancel_code
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "cancel",
            "now")
        self.filename = "cancel(%s).xml" % str(order_id)

    def get_request_data(self):
        self.api_url = self.api_url.replace('{ordernumber}', self.order_id)
        #24. OutOfStock
        #72. Customer Requested to Cancel
        #73. PriceError
        #74. Unable to Fulfill Order

        data = """
            <UpdateOrderStatus>
                <Action>1</Action>
                <Value>%s</Value>
            </UpdateOrderStatus>""" % self.cancel_code
        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(data), 'wb').create()
        return data

    def parse_response(self, response):
        if not response:
            return False
        return True


class NewEggApiOrderCancelByLine(NewEggApi):
    name = "KillItemRequest"
    api_url = "{host}/ordermgmt/orderstatus/orders/{ordernumber}?sellerid={sellerid}"
    method = "PUT"
    lines = []

    def __init__(self, settings, lines):
        super(NewEggApiOrderCancelByLine, self).__init__(settings)
        self.lines = lines
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "cancel_by_line",
            "now")
        self.filename = "cancel(%s).xml" % str(lines[0]['external_customer_order_id'])

    def get_request_data(self):
        self.api_url = self.api_url.replace('{ordernumber}', self.lines[0]['external_customer_order_id'])
        lineStr = ""
        for line in self.lines:
            lineStr += "<Item><SellerPartNumber>%s</SellerPartNumber></Item>" % line['merchantSKU']

        data = """
            <NeweggAPIRequest>
                <OperationType>KillItemRequest</OperationType>
                <RequestBody>
                    <KillItem>
                        <Order>
                            <ItemList>
                                %s
                            </ItemList>
                        </Order>
                    </KillItem>
                </RequestBody>
            </NeweggAPIRequest>""" % lineStr

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(data), 'wb').create()
        return data

    def parse_response(self, response):
        if not response:
            return False
        return True


class NewEggSubmitRMA(NewEggApi):
    name = "Submit RMA"
    api_url = "{host}/servicemgmt/rma/newrma?sellerid={sellerid}"
    method = "POST"
    lines = []

    def __init__(self, settings, lines):
        super(NewEggSubmitRMA, self).__init__(settings)
        self.lines = lines
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(
            settings['backup_local_path'],
            str(settings["customer_root_dir"]),
            "submit_rma",
            "now")
        self.filename = "rma(%s).xml" % str(lines[0]['external_customer_order_id'])

    def get_request_data(self):
        lineStr = ""
        for line in self.lines:
            lineStr += """<RMATransaction>
                                <SellerPartNumber>%s</SellerPartNumber>
                                <ReturnQuantity>%s</ReturnQuantity>
                                <ReturnUnitPrice>%s</ReturnUnitPrice>
                                <RefundShippingPrice>%s</RefundShippingPrice>
                                <RMAReason>%s</RMAReason>
                            </RMATransaction>""" % (line['customer_sku'], str(line['qty']), line['ItemPrice'], line['shipCost'], line['return_reason'])

        data = """
            <NeweggAPIRequest>
                <OperationType>IssueRMARequest</OperationType>
                <RequestBody>
                    <IssueRMA>
                        <RMAType>2</RMAType>
                        <SourceSONumber>%s</SourceSONumber>
                        <AutoReceiveMark>1</AutoReceiveMark>
                        <RMANote>This is a test RMA</RMANote>
                        <RMATransactionList>
                            %s
                        </RMATransactionList>
                    </IssueRMA>
                </RequestBody>
            </NeweggAPIRequest>""" % (self.lines[0]['external_customer_order_id'], lineStr)

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(data), 'wb').create()
        return data

    def parse_response(self, response):
        if not response:
            return False
        return True


class NewEggOpenerp(ApiOpenerp):

    def __init__(self):
        super(NewEggOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        if context is None:
            context = {}
        additional_fields = []

        if order.get('shipCost', None) is not None:
            additional_fields.append((0, 0, {'name': 'shipping_amount',
                                             'label': 'Shipping Amount',
                                             'value': order.get('shipCost', '0')}))

        if order.get('DiscountAmount', None) is not None:
            additional_fields.append((0, 0, {'name': 'discount_amount',
                                             'label': 'Discount Amount',
                                             'value': order.get('DiscountAmount', '0')}))

        if order.get('GSTorHSTAmount', None) is not None:
            additional_fields.append((0, 0, {'name': 'gst_or_hst_amount',
                                             'label': 'GSTorHSTAmount',
                                             'value': order.get('GSTorHSTAmount', '0')}))

        if order.get('PSTorQSTAmount', None) is not None:
            additional_fields.append((0, 0, {'name': 'pst_or_qst_amount',
                                             'label': 'PSTorQSTAmount',
                                             'value': order.get('PSTorQSTAmount', '0')}))

        if order.get('RefundAmount', None) is not None:
            additional_fields.append((0, 0, {'name': 'refund_amount',
                                             'label': 'Refund Amount',
                                             'value': order.get('RefundAmount', '0')}))

        if order.get('OrderTotalAmount', None) is not None:
            additional_fields.append((0, 0, {'name': 'order_total_amount',
                                             'label': 'OrderTotalAmount',
                                             'value': order.get('OrderTotalAmount', '0')}))

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['SellerPartNumber']
        }

        #line['ExtendUnitPrice']
        #line['UnitShippingCharge']
        #line['ExtendShippingCharge']
        product = False
        field_list = ['sku', 'merchantSKU', 'upc', 'manufacturerSKU', 'SellerPartNumber']
        for field in field_list:
            if line.get(field, False):
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'],
                                                                                      line[field],
                                                                                      ('default_code', 'customer_sku',
                                                                                       'upc', 'customer_id_delmar'))
                if product:
                    if size and size.id:
                        line_obj["size_id"] = size.id
                        break
        if (product):
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_additional_invoice_information(self, cr, uid, sale_order_id, invoice_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        #Need only for canada Bug #14968
        if sale_order.partner_id.external_customer_id == 'newegg_ca':
            additional_fields = sale_order.additional_fields
            for field in additional_fields:
                if field.name == 'shipping_amount':
                    res['Shipping_Handling'] = float(field.value)
                elif field.name == 'discount_amount':
                    res['Discount'] = float(field.value)
                elif field.name == 'gst_or_hst_amount':
                    res['GST'] = float(field.value)
                elif field.name == 'pst_or_qst_amount':
                    res['PST'] = float(field.value)
                elif field.name == 'refund_amount':
                    pass
                elif field.name == 'order_total_amount':
                    res['Total_Ship_Cost'] = float(field.value)

        return res
