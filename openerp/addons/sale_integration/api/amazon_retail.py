import os
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import time
import xmltodict
import logging
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


class AmazonRetailApi(ApiClient):

    name = ''
    code = ''
    functional_identifier_code = ''
    order_name = ''
    filename = ''

    keys = {
        'ST' : {
            'transaction_set_identifier_code' : 1,
            'transaction_set_control_number' : 2
        },
        'BEG' : {
            'transaction_set_purpose_code' : 1,
            'order_type' : 2,
            'order_number' : 3,
            'order_date' : 5
        },
        'REF' : {
            'reference_number_qualifier' : 1,
            'reference_number' : 2
        },
        'FOB' : {
            'shipment_method_of_payment' : 1,
            'transportation_terms_qualifier_code' : 4,
            'transportation_terms_code' : 5,
            'location_qualifier' : 6,
            'description' : 7
        },
        'CSH' : {
            'sales_requirement_code' : 1
            },
        'DTM' : {
            'qualifier' : 1,
            'date' : 2
        },
        'PKG' : {
            'item_description_type' : 1,
            'packaging_characteristic_code' : 2,
            'description' : 5
            },
        'N9' : {
            'qualifier' : 1,
            'description' : 2
        },
        'MSG' : {
            'text' : 1
        },
        'N1' : {
            'entity_identifier_code' : 1,
            'identifier_code_qualifier' : 3,
            'identifier_code' : 4
        },
        'PO1' : {
            'assigned_identification' : 1,
            'quantity_ordered' : 2,
            'unit_of_measurement_code' : 3,
            'unit_price' : 4,
            'basis_of_unit_price_code' : 5,
            'product_qualifier_6' : 6,
            's_7_digit_sku' : 7
        },
        'CTT' : {
            'number_of_lines_items' : 1,
            'hash_total' : 2
        }
    }

    def __init__(self, settings):
        self.multi_ftp_settings = settings.get("multi_ftp_settings")
        self.saveFlag = settings["saveFlag"] if 'saveFlag' in settings else False
        self.current_dir = os.path.dirname(__file__)
        self._log = []
        self.customer = 'Amazon Retail'
        self.date = datetime.now().strftime("%Y%m%d.%H%M%S")
        self.filename = self.name + "_" + self.order_name + '_' + self.date.replace('.', '_') + '.edi'

    def get_ftp_setting(self, ftp_settings, setting_name):
        current_setting = False
        for setting in ftp_settings:
            if setting.action == setting_name:
                current_setting = setting.ftp_setting_id
                ftp_path = setting['ftp_path']
                break
        if not current_setting:
            return False, False
        return current_setting, ftp_path

    def prepare_data(self, data, transaction_number):

        str_number = str(transaction_number)
        if (len(str_number) < 9):
            str_number = '0'*(9 - len(str_number))+str_number

        str_number1 = str(transaction_number)
        if (len(str_number1) < 9):
            str_number1 = str_number1 + '0'*(9 - len(str_number1))

        # date_full = datetime.now().strftime("%Y%m%d.%H%M%S")
        date = datetime.strftime(datetime.now(), '%Y%m%d')
        mdate = datetime.strftime(datetime.now(), '%y%m%d')
        time_hm = datetime.strftime(datetime.now(), '%H%M')
        # rand = random.randint(100000, 999999)
        header = "ISA*00*          *00*          *ZZ*DELMAR         *ZZ*AMAZON         *" + mdate + "*" + time_hm + "*U*00400*" + str_number + "*0*T*>~\n"
        header += "GS*"+self.functional_identifier_code+"*DELMAR*AMAZON*" + date + "*" + time_hm + "*" + transaction_number + "*X*004010~\n"
        header += "ST*%s*%s~\n" % (self.code, str_number)

        footer = "SE*%s*%s~\n" % (str(data.count('~')+2), str_number)
        footer += "GE*1*%s~\n" % str(transaction_number)
        footer += "IEA*1*%s~" % str(str_number)

        # self.filename = self.name + "_" + self.vendor_id + '_' + date.replace('.', '_') + '_' + str(rand) + '.xml'
        self.filename = self.name + "_" + self.order_name + '_' + self.date.replace('.', '_') + '.edi'

        self._log.append({
            'title': "Send %s To Amazon Retail" % self.name,
            'msg': header + data + footer,
            'type': 'send',
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })
        return header + data + footer

    def insert_to_str(self, target, source):
        for key in source.keys():
            target = target.replace('{' + key + '}', str(source[key]))
        return target


class AmazonRetailApiClient(AbsApiClient):

    settings = {
        "multi_ftp_settings": "",
        "saveFlag": ''
    }

    def __init__(self, settings_variables):
        super(AmazonRetailApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = AmazonRetailApiGetOrdersXML

    def loadOrders(self, saveInFolder=False):
        orders = []
        self.settings["saveFlag"] = saveInFolder and saveInFolder or False
        ordersApi = AmazonRetailApiGetOrdersXML(self.settings)
        if ordersApi.cur_ftp_setting:
            orders = ordersApi.send()
        else:
            _logger.error("Amazon Retail not found ftp_setting (load_orders)")
        self.extend_log(ordersApi)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = AmazonRetailApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def Acknowledgement(self, order, transaction_number, state='accepted'):
        acknowledgementApi = AmazonRetailApiAcknowledgement(self.settings, order, transaction_number, state)
        acknowledgementApi.send()
        self.extend_log(acknowledgementApi)
        return True

    def changeAcknowledgement(self, order, transaction_number):
        changeAcknowledgementApi = AmazonRetailApiChangeAcknowledgement(self.settings, order, transaction_number)
        changeAcknowledgementApi.send()
        self.extend_log(changeAcknowledgementApi)
        return True

    def routingRequest(self, order, transaction_number, routing_request_number):
        routingRequestApi = AmazonRetailApiRoutingRequest(self.settings, order, transaction_number, routing_request_number)
        routingRequestApi.send()
        self.extend_log(routingRequestApi)
        return True

    def confirmShipment(self, order):
        confirmApi = AmazonRetailApiConfirmShipment(self.settings, order)
        confirmApi.send()
        self.extend_log(confirmApi)
        return True

    def returnOrders(self, lines):
        return True

    def searchProducts(self, products):
        return True


class AmazonRetailApiGetOrdersXML(AmazonRetailApi):

    def __init__(self, settings):
        super(AmazonRetailApiGetOrdersXML, self).__init__(settings)
        # if(settings['order_type'] == 'order'):
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'load_orders')
        # else:
            # self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'load_cancel_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"], True)
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_local_dir': '%s' % self.create_local_dir(settings['input_local_path'], "amazon_retail", "orders"),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "amazon_retail", "orders", "now")
            }
            self.set_ftp_method('get')
            self.set_path_list(path_list)
            self.filename = f_d('amazon_retail_orders_%Y%m%d.txt', datetime.utcnow())

    def parse_edi(self, edi):
        orders = []
        skip_for_next = False
        order_line_index = ''
        line = []
        loop = ('N1', 'PO1')
        for edi_order in edi:
            for edi_row in edi_order.split('~'):
                row = edi_row.split('*')
                index = row[0]
                if (index == 'ST'):
                    if (row[1] == '850'):
                        order_line_index = 'PO1'
                    elif (row[1] == '860'):
                        order_line_index = 'POC'
                    else:
                        break
                    # order1={}
                    # order1['root']={}
                    order = {}
                    # order_line_index = list()
                    for item in loop:
                        order[item] = list()
                    continue
                if (index != 'SE' and skip_for_next):
                    continue

                if (index == 'SE'):
                    if(not skip_for_next):
                        # if(line):
                        #     order[order_line_index].append(line)

                        orders.append(order)
                        order = list()
                        # order = order1
                        line = list()

                    else:
                        skip_for_next = False

                    continue

                # if (index == order_line_index or index in loop):
                #     if (line):
                #         order[index].append(line)

                    # line = array('PID' => array());
                try:
                    if(self.keys[index]):
                        rebuild = False
                        tmp = {}
                        copy_array = {}

                        # self.keys[index]

                        if(order.get(index+"_"+self.keys[index].iterkeys().next(), False)):
                            rebuild = True;

                        for key,value in self.keys[index].items():
                            if(not row[value]):
                                continue
                            tmp[index+"_"+key] = row[value]
                            if(rebuild):
                                copy_array[index+"_"+key] = order[index+"_"+key]
                                order[index+"_"+key]=None
                        if(rebuild):
                            order[index] = list()
                            order[index].append(copy_array)
                        if(order and order.get(index) and index not in loop):
                            order[index].append(tmp)

                        # // else if(!empty(line) and index == 'PID'){
                        # //     line['PID'][] = tmp;
                        # // }
                        elif (index in loop):
                            line = tmp
                            order[index].append(line)
                        else:
                            order.update(tmp)
                except Exception:
                    continue
        return orders

    def parse_response(self, response):
        orders = self.parse_edi(response)
        ordersList = []
        for order in orders:
            ordersObj = {}
            sale_order = {}
            ordersObjXML = {}
            # timezone = new DateTimeZone('UTC');
            date_order = datetime.strptime(order['BEG_order_date'], '%Y%m%d')
            sale_order['orderDate'] = date_order.strftime('%Y.%m.%d 00:00:00 %Z')
            sale_order['poNumber'] = order['BEG_order_number'].replace('SO','')
            sale_order['orderId'] = sale_order['poNumber'];
            sale_order['OrderType'] = order['BEG_order_type'];
            sale_order['Price'] = 0;
            for n1 in order['N1']:
                if (n1['N1_entity_identifier_code'] == 'ST'):
                    # // sale_order->ShipTo['ShipName'] = n1['N1_name'];
                    # // sale_order->MerchRefOne = 'sterling_special';
                    sale_order['MerchRefOneQualifier'] = n1['N1_identifier_code_qualifier']
                    sale_order['MerchRefOne'] = n1['N1_identifier_code'];
                    sale_order['address'] = {}
                    # if (sale_order['MerchRefOne' == 15):
                        # ordersObj['address']['name'] =
                        # ordersObj['address']['address1'] =
                        # ordersObj['address']['address2'] =
                        # ordersObj['address']['city'] =
                        # ordersObj['address']['state'] =
                        # ordersObj['address']['zip'] =
                        # ordersObj['address']['country'] =
                        # ordersObj['address']['phone'] =
                    break
            sale_order['requestedShipDate'] = sale_order['orderDate']
            if(order['DTM'] and isinstance(order['DTM'], list)):
                for dtm in order['DTM']:
                    if(dtm['DTM_qualifier'] == '064'):
                        sale_order['earliest_ship_date_order'] = dtm['DTM_date']
                    if(dtm['DTM_qualifier'] == '063'):
                        sale_order['latest_ship_date_order'] = dtm['DTM_date']

            sale_order['lines'] = list()
            line_index = sale_order['lines']
            line_count = 1
            for line in order['PO1']:
                sale_order_line = dict()
                sale_order_line['lineId'] = line_count
                sale_order_line['LinePrice'] = float(line['PO1_quantity_ordered']) * float(line['PO1_unit_price'])
                sale_order['Price'] += sale_order_line['LinePrice']
                # sale_order_line['LineNum'] = key + 1
                sale_order_line['Product'] = {}
                sale_order_line['Product']['unitPrice'] = line['PO1_unit_price']
                sale_order_line['Product']['prodID'] = line['PO1_s_7_digit_sku']
                sale_order_line['Product']['qty'] = line['PO1_quantity_ordered']
                sale_order['lines'].append(sale_order_line)
                line_count = line_count + 1
            ordersObj['root'] = sale_order
            ordersObjXML['xml'] = xmltodict.unparse(ordersObj)
            ordersObjXML['name'] = sale_order['poNumber']
            ordersList.append(ordersObjXML)

        return ordersList


class AmazonRetailApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml
        self._log = []

    def getOrderObj(self):
        ordersObj = {}

        if(self.xml == ""):
            return ordersObj
        # order = etree.XML(self.xml)
        root = xmltodict.parse(self.xml)
        order = root['root']
        if(order is not None):
            ordersObj['partner_id'] = order.get('store', 'Amazon Retail')
            ordersObj['order_id'] = order.get('orderId', False)
            ordersObj['poNumber'] = order.get('poNumber', False)
            ordersObj['external_date_order'] = order.get('orderDate', False)
            ordersObj['earliest_ship_date_order'] = order.get('earliest_ship_date_order', False)
            ordersObj['latest_ship_date_order'] = order.get('latest_ship_date_order', False)
            ordersObj["Comments"] = []
            commentobj = {}
            commentobj["CommentType"] = 'OL'
            commentobj["Text"] = ''
            ordersObj["Comments"].append(commentobj)
            # ordersObj['carrier'] = order.ship.get('ship_via', {}).get('value', '')
            ordersObj['address'] = {}
            if (order.get('MerchRefOneQualifier', False) == '15'):
                ordersObj['address']['ship'] = {}
                ordersObj['address']['ship']['address_id'] = order.get('MerchRefOne', False)
                ordersObj['lines'] = []
            lines = order.get('lines', {}) if (isinstance(order.get('lines', {}), list)) else [order.get('lines', {})]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('lineId', False)
                lineItemObj['sku'] = lineItem.get('Product',{}).get('prodID', False)
                lineItemObj['merchantSKU'] = lineItem.get('Product',{}).get('prodID', False)
                lineItemObj['size'] = lineItem.get('Product', {}).get('size', False)
                lineItemObj['qty'] = lineItem.get('Product', {}).get('qty', False)
                lineItemObj['name'] = lineItem.get('Product', {}).get('desc', False)
                lineItemObj['cost'] = lineItem.get('Product', {}).get('unitPrice', False)
                # lineItemObj['customerCost'] = lineItem.get('Product', {}).get('unitPrice', False)
                ordersObj['lines'].append(lineItemObj)

        return ordersObj


class AmazonRetailApiAcknowledgement(AmazonRetailApi):
    # method = "put"
    filename = ""
    name = "AMZRTL_Acknowledgement"
    # customer_root_dir = ""

    def __init__(self, settings, order, transaction_number, state):
        super(AmazonRetailApiAcknowledgement, self).__init__(settings)
        self.order = order
        self.code = self.order['code']
        self.functional_identifier_code = 'PR'
        self.transaction_number = transaction_number
        self.order_name = self.order['sale_order']['name']
        self.state = state
        self.date = datetime.now().strftime("%Y%m%d.%H%M%S")
        # self.filename = self.name + "_" + self.order['name'] + '_' + date.replace('.', '_') + '.edi'
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'confirm_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                # 'path_to_backup_local_dir': '/orders_temp/amazon_retail/confirm_line/'
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "amazon_retail", "confirm_line", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)

    def upload_data(self):
        acknowledgement = self.order
        order_list = []
        # for acknowledgement in acknowledgements:

        bak = {
        'transaction_set_purpose_code' : '00',
        'acknowledgement_type': 'AC',
        'po_order_name' : acknowledgement['sale_order']['external_customer_order_id'],
        'date' : acknowledgement['sale_order']['date_order'].replace('-',''),
        }
        bak_s = "BAK*{transaction_set_purpose_code}*{acknowledgement_type}*{po_order_name}*{date}~\n"
        request = self.insert_to_str(bak_s, bak)
        line_number = 0
        sum_po = 0
        for line in acknowledgement['lines']:
            line_number += 1
            po1 = {
                'assigned_identification' : line['sale_order_line']['external_customer_line_id'],
                'quantity_ordered' : int(line['sale_order_line']['product_uom_qty']),
                'unit_of_measurement_code' : 'EA',
                'unit_price' : line['sale_order_line']['price_unit'],
                'basis_of_unit_price_code' : 'NT',
                'product_qualifier_6' : 'BP',
                's_7_digit_sku' : line['customer_sku'],
            }
            po1_s = "PO1*{assigned_identification}*{quantity_ordered}*{unit_of_measurement_code}*{unit_price}*{basis_of_unit_price_code}*{product_qualifier_6}*{s_7_digit_sku}~\n"
            request += self.insert_to_str(po1_s, po1)

            ctp = {
                # 'class_of_trade_code' : line['class_of_trade_code'] or '',
                'class_of_trade_code' : '',
                'price_identifier_code' : 'LPR',
                'unit_price' : line['sale_order_line']['price_unit'],
                'quantity' : int(line['sale_order_line']['product_uom_qty']),
                # 'compose_unit_of_measure'
                'unit_basis_of_measurement_code' : 'EA',
                'price_multiplier_qualifier' : 'DIS',
                'multiplier' : '1',
            }
            ctp_s = "CTP*{class_of_trade_code}*{price_identifier_code}*{unit_price}*{quantity}*{unit_basis_of_measurement_code}*{price_multiplier_qualifier}*{multiplier}~\n"
            request += self.insert_to_str(ctp_s, ctp)

            ack = {
                'line_item_status_code' : line['state'],
                'quantity' : int(line['sale_order_line']['product_uom_qty']),
                'unit_basis_of_measurement_code' : 'EA',
                'date_qualifier' : '068',
                'date' : acknowledgement['sale_order']['latest_ship_date_order'].replace('-',''),
            }
            ack_s = "ACK*{line_item_status_code}*{quantity}*{unit_basis_of_measurement_code}*{date_qualifier}*{date}~\n"
            request += self.insert_to_str(ack_s, ack)
            sum_po = sum_po + line['sale_order_line']['product_uom_qty']

        dtm = {
        'qualifier' : '067',
        'date' : acknowledgement['sale_order']['latest_ship_date_order'].replace('-',''),
        }
        dtm_s = "DTM*{qualifier}*{date}~\n"
        request += self.insert_to_str(dtm_s, dtm)

        ctt = {
        'number_of_lines_items' : line_number,
        'hash_total' : sum_po,
        }
        ctt_s = "CTT*{number_of_lines_items}*{hash_total}~\n"
        request += self.insert_to_str(ctt_s, ctt)

        return self.prepare_data(request, self.transaction_number)

class AmazonRetailApiChangeAcknowledgement(AmazonRetailApi):
    name = "AMZP_ChangeAcknowledgement"

    def __init__(self, settings, order, transaction_number):
        super(AmazonRetailApiChangeAcknowledgement, self).__init__(settings)
        self.order = order
        self.code = self.order['code']
        self.functional_identifier_code = 'CA'
        self.transaction_number = transaction_number
        self.order_name = self.order['name']
        self.date = datetime.now().strftime("%Y%m%d.%H%M%S")
        # self.filename = self.name + "_" + self.order['name'] + '_' + date.replace('.', '_') + '.edi'
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'confirm_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                # 'path_to_backup_local_dir': '/orders_temp/amazon_retail/confirm_line/'
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "amazon_retail", "reconfirm_line", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)

    def upload_data(self):
        date = datetime.strftime(datetime.now(),'%Y%m%d')

        change_acknowledgement = self.order

        bca = {
        'transaction_set_purpose_code' : '00',
        'acknowledgment_type' : 'AC',
        'purchase_order_number' : change_acknowledgement['external_customer_order_id'],
        'change_order_sequence_number': str(self.transaction_number),
        'date': str(date),
        }

        bca_s = "BCA*{transaction_set_purpose_code}*{acknowledgment_type}*{purchase_order_number}**{change_order_sequence_number}*{date}~\n"
        request = self.insert_to_str(bca_s, bca)

        # """ Currency is optional field """
        # cur = {
        # 'entity_identifier_code',
        # 'currency_code',
        # }

        n1_su = {
        'entity_identifier_code': 'SU',
        'name': change_acknowledgement['partner_order'],
        'identification_code_qualifier': '15',
        'identification_code': change_acknowledgement['partner_order_san'],
        }

        n1_s_su = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_s_su, n1_su)

        n1_st = {
        'entity_identifier_code': 'ST',
        'name': change_acknowledgement['partner_shipping'],
        'identification_code_qualifier': '15',
        'identification_code': change_acknowledgement['partner_shipping_san'],
        }

        n1_s_st = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_s_st, n1_st)

        line_count = 0
        sum_products = 0
        for line in change_acknowledgement['lines']:
            poc = {
            'assigned_identification': line['external_customer_line_id'],
            'change_or_response_type_code': line['change_code'],
            'quantity_ordered': int(line['product_qty']),
            'unit_or_basis_for_measurement_code': 'EA',
            'unit_price': line['price_unit'],
            'basis_of_unit_price_code': 'PE',
            'product_qualifier': 'BP',
            'product_id': line['customer_sku'],
            }

            poc_s = "POC*{assigned_identification}*{change_or_response_type_code}*{quantity_ordered}**{unit_or_basis_for_measurement_code}*{unit_price}*{basis_of_unit_price_code}*{product_qualifier}*{product_id}~\n"
            request += self.insert_to_str(poc_s, poc)

            # """ CTP is optional field """
            # ctp = {
            # 'price_identifier_code',
            # 'unit_price',
            # 'multiplier',
            # }

            ack = {
            'line_item_status_code': line['item_status_code'],
            'quantity': int(line['product_qty']),
            'unit_or_basis_for_measurement_code': 'EA',
            'date_qualifier': '068',
            'date': change_acknowledgement['earliest_ship_date_order'].replace('-',''),
            }

            ack_s = "ACK*{line_item_status_code}*{quantity}*{unit_or_basis_for_measurement_code}*{date_qualifier}*{date}~\n"
            request += self.insert_to_str(ack_s, ack)

            dtm = {
            'date_qualifier': '067',
            'date': change_acknowledgement['latest_ship_date_order'].replace('-',''),
            }

            dtm_s = "DTM*{date_qualifier}*{date}~\n"
            request += self.insert_to_str(dtm_s, dtm)

            line_count = line_count + 1
            sum_products = sum_products + line['product_qty']

        ctt = {
        'number_of_line_items': line_count,
        'hash_total': sum_products,
        }

        ctt_s = "CTT*{number_of_line_items}*{hash_total}~\n"
        request += self.insert_to_str(ctt_s, ctt)
        return self.prepare_data(request, self.transaction_number)

class AmazonRetailApiRoutingRequest(AmazonRetailApi):
    name = "AMZRTL_RoutingRequest"

    def __init__(self, settings, order, transaction_number, routing_request_number):
        super(AmazonRetailApiRoutingRequest, self).__init__(settings)
        self.code = '753'
        self.functional_identifier_code = 'RF'
        self.routing_request_number = routing_request_number
        self.transaction_number = transaction_number
        self.order = order
        self.order_name = self.order.sale_id['po_number']
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'routing_request')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                # 'path_to_backup_local_dir': '/orders_temp/amazon_retail/routing_request/'
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "amazon_retail", "routing_request", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)

    def upload_data(self):
        routing_order = self.order
        # address_type = {'ShipFrom' : 'SF', 'ShipTo' : 'ST'}

        # generate = self.GenerateAmazonRetail()

        # unique_number = str(generate.get_routing_request_number())
        if (len(self.routing_request_number) < 9):
            self.routing_request_number = '0'*(9 - len(self.routing_request_number))+self.routing_request_number

        bgn = {
            'transaction_set_purpose_code' : '00',
            'reference_identification' : self.routing_request_number,
            'date' : datetime.strftime(datetime.now(),'%y%m%d'),
            'time' : datetime.strftime(datetime.now(),'%H%M'),
            # 'Time Code' : 'UTC',
            # 'Reference Identification' : '',
            # 'Transaction Type Code' : '',
            # 'Action Code' : '',
            # 'Security Level Code' : ''
        }
        bgn_s = "BGN*{transaction_set_purpose_code}*{reference_identification}*{date}*{time}~\n"
        request = self.insert_to_str(bgn_s, bgn)

        per = {
            'contact_function_code' : 'IC',
            'name' : routing_order['address_id'].company or '',
            'communication_number_qualifier' : 'TE',
            'communication_number' : routing_order['address_id'].phone or '',
            # 'Communication Number Qualifier' : '',
            # 'Communication Number' : '',
            # 'Communication Number Qualifier' : '',
            # 'Communication Number' : '',
            # 'Contact Inquiry Reference' : ''
        }
        per_s = "PER*{contact_function_code}*{name}~\n"
        if per['communication_number'] != '':
            per_s.replace("~\n", "*{communication_number_qualifier}*{communication_number}~\n")
        request += self.insert_to_str(per_s, per)

        # for address in address_type.keys():
        n1_sf = {
            'entity_identifier_code' : 'SF',
            'name' : routing_order['address_id'].company or '',
            'identification_code_qualifier' : 15,
            'identification_code' : routing_order['address_id'].title,
            # 'Entity Relationship Code' : '',
            # 'Entity Identifier Code' : ''
        }
        n1_sf_s = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_sf_s, n1_sf)

        # n2 = {
        #     'Name' : '',
        #     'Name' : ''
        # }

        n3 = {
            'address_information' : routing_order['address_id'].street,
            # 'Address Information' : ''
        }
        n3_s = "N3*{address_information}~\n"
        request += self.insert_to_str(n3_s, n3)

        # Information about Ship from
        n4 = {
            'city_name' : routing_order['address_id'].city,
            'state_or_province_code' : routing_order['address_id'].state_id.code,
            'postal_code' : routing_order['address_id'].zip or '',
            'country_code' : routing_order['address_id'].country_id.code,
            # 'Location Qualifier' : routing_order['partner_shipping_id'],
            # 'Location Identifier' : routing_order['partner_shipping_id'],
            # 'Country Subdivision Code' : routing_order['partner_shipping_id']
        }
        n4_s = "N4*{city_name}*{state_or_province_code}*{postal_code}*{country_code}~\n"
        request += self.insert_to_str(n4_s, n4)

        # if (address == 'ShipFrom'):
        lx = {
            'assigned_number' : 1
        }
        lx_s = "LX*{assigned_number}~"
        request += self.insert_to_str(lx_s, lx)

        n1_st = {
            'entity_identifier_code' : 'ST',
            'name' : routing_order['address_id'].company or '',
            'identification_code_qualifier' : 15,
            'identification_code' : routing_order['address_id'].title,
            # 'Entity Relationship Code' : '',
            # 'Entity Identifier Code' : ''
        }
        n1_st_s = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_st_s, n1_st)

        # l11 = {
        #     'Reference Identification' : '',
        #     'Reference Identification Qualifier' : '',
        #     'Description' : '',
        #     'Date' : '',
        #     'Yes/No Condition or Response Code' : ''

        # }

        g62 = {
            'date_qualifier' : 'EP',
            'date' : datetime.strftime(datetime.now(),'%y%m%d'),
            'time_qualifier' : 'EP',
            'time' : datetime.strftime(datetime.now(),'%H%M'),
            'time_code' : 'CT'
        }
        g62_s = "G62*{date_qualifier}*{date}*{time_qualifier}*{time}*{time_code}~\n"
        request += self.insert_to_str(g62_s, g62)

        usi = {
            'quantity' : 1,
            'packaging_form_code' : 'PKG',
            'yes_no_condition_or_response_code' : 'N'
        }
        usi_s = "USI*{quantity}*{packaging_form_code}*{yes_no_condition_or_response_code}~\n"
        request += self.insert_to_str(usi_s, usi)

        # for line in sale_order:
        oid = {
            'reference_identification_1' : '',
            'purchase_order_number' : routing_order['sale_id'].external_customer_order_id,
            'reference_identification_2' : '',
            'packaging_form_code' : 'PKG',
            'quantity' : routing_order['number_of_packages'],
            'weight_unit_code' : 'G',
            'weight' : routing_order['weight'],
            'volume_unit_qualifier' : 'X',
            'volume' : routing_order['volume'],
            # 'Application Error Condition Code' : '',
            # 'Reference Identification' : ''
        }
        oid_s = "OID*{reference_identification_1}*{purchase_order_number}*{reference_identification_2}*{packaging_form_code}*{quantity}*{weight_unit_code}*{weight}*{volume_unit_qualifier}*{volume}~\n"
        request += self.insert_to_str(oid_s, oid)

        cmc = {
            'commodity_code' : 'Unspecified',
            'freight_class_code': 77.5
        }
        cmc_s = "CMC*{commodity_code}*{freight_class_code}~\n"
        request += self.insert_to_str(cmc_s, cmc)

        return self.prepare_data(request, self.transaction_number)

class AmazonRetailApiConfirmShipment(AmazonRetailApi):

    name = "AMZRTL_Confirm"
    # code = '856'
    _file_type = 'FOS'
    confirmLines = []

    def __init__(self, settings, order):
        super(AmazonRetailApiConfirmShipment, self).__init__(settings)
        self.code = '856'
        self.functional_identifier_code = 'SH'
        self.transaction_number = order[0].get('transaction_number', None)
        self.order = order
        self.order_name = self.order[0]['origin']
        date = datetime.now().strftime("%Y%m%d.%H%M%S")
        # self.confirmLines = order
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(self.multi_ftp_settings, 'confirm_shipment')
        if self.cur_ftp_setting:
            self.set_ftp_settings(self.cur_ftp_setting["ftp_host"], self.cur_ftp_setting["ftp_user"], self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                # 'path_to_backup_local_dir': '/orders_temp/amazon_retail/confirm_ship/'
                'path_to_backup_local_dir': '%s' % self.create_local_dir(settings['backup_local_path'], "amazon_retail", "confirm_ship", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)

    def upload_data(self):
        shipment_order = self.order[0]
        number = '00001'

        bsn = {
            'transaction_set_purpose_code' : '00',
            'shipment_identification' : shipment_order['invoice'],
            'date' : datetime.strftime(datetime.now(),'%Y%m%d'),
            'time' : datetime.strftime(datetime.now(),'%H%M%S'),
            'hierarchical_structure_code' : '0004'
        }
        bsn_s = "BSN*{transaction_set_purpose_code}*{shipment_identification}*{date}*{time}*{hierarchical_structure_code}~\n"
        request = self.insert_to_str(bsn_s, bsn)

        dtm = {
            'date_qualifier' : '017',
            'date' : shipment_order['latest_ship_date_order'].replace('-',''),
            'time' : '0900',
            'time_code' : 'UT'
        }
        dtm_s = "DTM*{date_qualifier}*{date}*{time}*{time_code}~\n"
        request += self.insert_to_str(dtm_s, dtm)

        # Loop Hierarchical Level - Shipment
        loop_iter = 1
        # for line in shipment_order['lines']:
        hl = {
            'hierarchical_id_number' : 1,
            'hierarchical_level_code' : 'S',
            'hierarchical_child_code' : 1
        }
        # hl_s = "HL*{hierarchical_id_number}*{hierarchical_level_code}*{hierarchical_child_code}\n"
        hl_s = "HL*{hierarchical_id_number}**{hierarchical_level_code}~\n"
        request += self.insert_to_str(hl_s, hl)

        td1_9_str = {
            'packaging_code' : 'CTN',
            'lading_quantity' : 1,
            'commodity_code' : '',
            'lading_description' : '',
            'weight_qualifier' : 'G',
            'weight' : str(shipment_order['weight']),
            'unit_or_basis_for_measurement_code_weight' : 'GR'
            # 'volume' : str(shipment_order['volume']) or '0',
            # 'unit_or_basis_for_measurement_code_volume' : 'CR'
        }
        td1_9_str_s = "TD1*{packaging_code}*{lading_quantity}**{commodity_code}*{lading_description}*{weight_qualifier}*{weight}*{unit_or_basis_for_measurement_code_weight}~\n"
        request += self.insert_to_str(td1_9_str_s, td1_9_str)

        # td1_2_str = {
        #     'packaging_code' : 'PLT',
        #     'lading_quantity' : 1,
        # }
        # td1_2_str_s = "TD1*{packaging_code}*{lading_quantity}\n"
        # request += self.insert_to_str(td1_2_str_s, td1_2_str)

        td5 = {
            'identification_code_qualifier' : 2,
            'identification_code' : shipment_order['carrier_code']
        }
        td5_s = "TD5**{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(td5_s, td5)

        # td3 = {
        #     'equipment_description_code' : '',
        #     'weight_qualifier' : '',
        #     'weight' : '',
        #     'unit_or_basis_for_measurement_code' : ''
        # }
        # td3_s = "TD3*{equipment_description_code}*{weight_qualifier}*{weight}*{unit_or_basis_for_measurement_code}\n"
        # request += self.insert_to_str(td3_s, td3)

        # ref_bill = {
        #     'reference_identification_qualifier' : 'BL',
        #     'reference_identification' : ''
        # }
        # ref_bill_s = "REF*{reference_identification_qualifier}*{reference_identification}\n"
        # request += self.insert_to_str(ref_bill_s, ref_bill)

        # ref_number = {
        #     'reference_identification_qualifier' : 'CN',
        #     'reference_identification' : shipment_order["tracking_number"]
        # }
        # ref_number_s = "REF*{reference_identification_qualifier}*{reference_identification}\n"
        # request += self.insert_to_str(ref_number_s, ref_number)

        # ref_seal = {
        #     'reference identification qualifier' : '',
        #     'reference identification' : ''
        # }
        # ref_seal_s = "REF*{reference_identification_qualifier}*{reference_identification}\n"
        # request += self.insert_to_str(ref_seal_s, ref_seal)

        # ref_amazon = {
        #     'reference identification qualifier' : '',
        #     'reference identification' : ''
        # }
        # ref_amazon_s = "REF*{reference_identification_qualifier}*{reference_identification}\n"
        # request += self.insert_to_str(ref_amazon_s, ref_amazon)
        shp_date = datetime.strptime(shipment_order["shp_date"], '%Y-%m-%d %H:%M:%S')
        dtm_ship_date = {
            'date_qualifier' : '011',
            'date' : datetime.strftime(shp_date, '%Y%m%d'),
            'time' : datetime.strftime(shp_date, '%H%M'),
            'time_code' : 'UT'
        }
        dtm_ship_date_s = "DTM*{date_qualifier}*{date}*{time}*{time_code}~\n"
        request += self.insert_to_str(dtm_ship_date_s, dtm_ship_date)

        fob = {
            'shipment_method_of_payment' : 'PO'
        }
        fob_s = "FOB*{shipment_method_of_payment}~\n"
        request += self.insert_to_str(fob_s, fob)

        # loop_iter = loop_iter + 1
        # Ship from
        n1 = {
            'entity_identifier_code' : 'SF',
            'name' : shipment_order["remit_to_name_1"],
            'identification_code_qualifier' : 15,
            'identification_code' : shipment_order['remit_to_san_address']
        }
        n1_s = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_s, n1)

        # n3 = {
        #     'address_information' : '',
        #     'address_information' : ''
        # }
        # n3_s = "N3*{address_information}*{address_information}\n"
        # request += self.insert_to_str(n3_s, n3)

        n4 = {
            'city_name' : shipment_order["remit_to_city"],
            'state_or_province_code' : shipment_order["remit_to_state"],
            'postal_code' : shipment_order["remit_to_postal_code"],
            'country_code' : shipment_order["remit_to_country"]

        }
        n4_s = "N4*{city_name}*{state_or_province_code}*{postal_code}*{country_code}~\n"
        request += self.insert_to_str(n4_s, n4)

        # Ship To
        n1 = {
            'entity_identifier_code' : 'ST',
            'name' : shipment_order['ship_to_name_1'],
            'identification_code_qualifier' : 15,
            'identification_code' : shipment_order['ship_to_san_address']
        }
        n1_s = "N1*{entity_identifier_code}*{name}*{identification_code_qualifier}*{identification_code}~\n"
        request += self.insert_to_str(n1_s, n1)

        n4 = {
            'city_name' : shipment_order["ship_to_city"],
            'state_or_province_code' : shipment_order["ship_to_state"],
            'postal_code' : shipment_order["ship_to_postal_code"],
            'country_code' : shipment_order["ship_to_country"]

        }
        n4_s = "N4*{city_name}*{state_or_province_code}*{postal_code}*{country_code}~\n"
        request += self.insert_to_str(n4_s, n4)

        # Loop vessel
        # v1 = {
        #     'vessel code' : '',
        #     'vessel name' : '',
        #     'country code' : '',
        #     'flight/voyage number' : '',
        #     'standard carrier alpha code' : '',
        #     'vessel requirement code' : '',
        #     'vessel type code' : '',
        #     'vessel code qualifier' : '',
        #     'transportation method/type code' : ''
        # }

        # r4 = {
        #     'port or terminal function code' : '',
        #     'location qualifier' : '',
        #     'location identifier' : '',
        #     'port name' : '',
        #     'country code' : '',
        #     'terminal name' : '',
        #     'pier number' : '',
        #     'state or province code' : ''
        # }

        # dtm = {
        #     'date/time qualifier' : '',
        #     'date' : '',
        #     'time' : '',
        #     'time code' : ''
        # }

        # Loop Hierarchical Level - Order
        loop_iter+=1
        hl_o = {
            'hierarchical_id_number' : loop_iter,
            'hierarchical_parent_id_number' : 1,
            'hierarchical_level_code' : 'O',
            'hierarchical child code' : ''
        }
        hl_o_s = "HL*{hierarchical_id_number}*{hierarchical_parent_id_number}*{hierarchical_level_code}~\n"
        request += self.insert_to_str(hl_o_s, hl_o)

        prf_o = {
            'purchase_order_number' : shipment_order["external_customer_order_id"]

        }
        prf_o_s = "PRF*{purchase_order_number}~\n"
        request += self.insert_to_str(prf_o_s, prf_o)

        loop_package_id = 0
        # Loop Hierarchical Level - Package

        loop_iter+=1
        hl_p = {
            'hierarchical_id_number' : loop_iter,
            'hierarchical_parent_id_number' : 2,
            'hierarchical_level_code' : 'P',
            'hierarchical child code' : ''
        }
        hl_p_s = "HL*{hierarchical_id_number}*{hierarchical_parent_id_number}*{hierarchical_level_code}~\n"
        request += self.insert_to_str(hl_p_s, hl_p)

        # td1_p = {
        #     'packaging_code' : '',
        #     'lading_quantity' : '',
        #     'commodity_code_qualifier' : '',
        #     'commodity_code' : '',
        #     'lading_description' : '',
        #     'weight_qualifier' : '',
        #     'weight' : '',
        #     'unit_or_basis_for_measurement_code' : ''
        # }
        # td1_p_s = "TD1*{packaging_code}*{lading_quantity}*{commodity}*{description}*{weight_qualifier}*{weight}*{unit_or_basis_for_measurement_code}\n"
        # request += self.insert_to_str(td1_p_s, td1_p)

        ref_number_p = {
            'reference_identification_qualifier' : 'CN',
            'reference_identification' : shipment_order["tracking_number"]
        }
        ref_number_p_s = "REF*{reference_identification_qualifier}*{reference_identification}~\n"
        request += self.insert_to_str(ref_number_p_s, ref_number_p)

        man = {
            'marks_and_numbers_qualifier' : 'GM',
            'marks_and_numbers' : shipment_order["tracking_number"]
        }
        man_s = "MAN*{marks_and_numbers_qualifier}*{marks_and_numbers}~\n"
        request += self.insert_to_str(man_s, man)

        loop_item_id = 0
        sum_products = 0
        for item in self.order:
        # Loop Hierarchical Level - Item
            loop_iter+=1
            hl_p = {
                'hierarchical_id_number' : loop_iter,
                'hierarchical_parent_id_number' : 3,
                'hierarchical_level_code' : 'I',
                'hierarchical_child_code' : ''
            }
            hl_p_s = "HL*{hierarchical_id_number}*{hierarchical_parent_id_number}*{hierarchical_level_code}~\n"
            request += self.insert_to_str(hl_p_s, hl_p)

            lin = {
                'assigned_identification' : item['external_customer_line_id'],
                'product_id_qualifier' : 'BP',
                'product_id' : item['customer_sku'] or item['product_item']
            }
            lin_s = "LIN*{assigned_identification}*{product_id_qualifier}*{product_id}~\n"
            request += self.insert_to_str(lin_s, lin)

            sn = {
                # 'assigned_identification' : item['external_customer_line_id'],
                'assigned_identification' : item['external_customer_line_id'],
                'number_of_units_shipped' : int(item['product_qty']),
                'unit_or_basis_for_measurement_code' : 'EA'
            }
            sn_s = "SN1*{assigned_identification}*{number_of_units_shipped}*{unit_or_basis_for_measurement_code}~\n"
            request += self.insert_to_str(sn_s, sn)

            sum_products += item['product_qty']

        ctt = {
            'number_of_line_items' : loop_iter,
            'hash_total' : sum_products
        }
        ctt_s = "CTT*{number_of_line_items}*{hash_total}~\n"
        request += self.insert_to_str(ctt_s, ctt)

        return self.prepare_data(request, self.transaction_number)