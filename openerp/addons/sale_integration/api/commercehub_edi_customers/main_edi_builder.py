# -*- coding: utf-8 -*-
from ..edi_objects import BaseEDIClient


class CommercehubEdiMainGenerator(BaseEDIClient):

    def __init__(self, parent):
        super(CommercehubEdiMainGenerator, self).__init__(settings=self.parent)
        self.parent = parent

    def upload_data(self):
        return False


class CommercehubEdiInvoiceGenerator(CommercehubEdiMainGenerator):

    def __init__(self, parent):
        super(CommercehubEdiInvoiceGenerator, self).__init__(parent)
        self.lines = self.parent.lines
        self.edi_type = '810'
