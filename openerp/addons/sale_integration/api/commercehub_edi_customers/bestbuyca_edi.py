# -*- coding: utf-8 -*-
from datetime import datetime
import random
from ..apiopenerp import ApiOpenerp
from ..utils.ediparser import EdiParser
from ..edi_objects import BaseEDIClient


SETTINGS_FIELDS = (
    ('vendor_number',           'Vendor number',            '6044358223FCD'),
    ('vendor_name',             'Vendor name',              'BestbuyCA'),
    ('sender_id',               'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',     'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',             'Receiver Id',              '6044358223FCD'),
    ('receiver_id_qualifier',   'Receiver Id Qualifier',    '12'),
    ('edi_x12_version',         'EDI X12 Version',          '4010'),
    ('filename_format',         'Filename Format',          'DEL{edi_type}_{date}.out'),
    ('line_terminator',         'Line Terminator',          r'~\n'),
    ('repetition_separator',    'Repetition Separator',     '}'),
    ('environment_mode',        'Environment Mode',         'P'),
    ('gst_number',              'GST Registration Number'   'R883554891'),
)


class Parser(object):

    customer = ""

    def __init__(self, customer):
        super(Parser, self).__init__()
        self.customer = customer

    def getOrderObj(self, edi):
        if edi == '':
            return []

        message = EdiParser(edi)
        return_orders = []

        for segment in message:
            elements = segment.split(message.delimiters[1])

            # GS - Functional Group Header
            if elements[0] == 'GS':
                self.ack_control_number = elements[6]
                self.functional_group = elements[1]

            # ST – Transaction Set Header
            elif elements[0] == 'ST':
                ordersObj = {
                    'address': {},
                    'partner_id': self.customer,
                    'lines': [],
                    'format': 'edi',
                    'Cancellation': False,
                    'Change': False,
                    'additional_fields': [],
                }
                address_type = ''
                if elements[1] == '850':  # Order
                    pass
                elif elements[1] == '860':  # Change
                    pass
                elif elements[1] == '997':  # Aknowlegment skip this file
                    return False
            elif elements[0] == 'BCH':   # Change
                if elements[1] == '01':  # Cancellation
                    ordersObj['Cancellation'] = True
                elif elements[1] == '04':  # Normal it's Change, but for BBY it's a Cancel
                    ordersObj['Cancellation'] = True
                ordersObj['poNumber'] = elements[3]
                ordersObj['external_date_order'] = elements[6]

            # BEG – Beginning Segment for Purchase Order
            elif elements[0] == 'BEG':
                ordersObj['poNumber'] = elements[3]
                ordersObj['order_id'] = elements[3]
                ordersObj['external_date_order'] = elements[5]

            # CUR - Currency
            elif elements[0] == 'CUR':
                if elements[1] == 'CQ':  # Buying Party (Purchaser)
                    ordersObj['additional_fields'].append((0, 0, {
                        'name': 'currency',
                        'label': 'Currency',
                        'value': elements[2]
                    }))

            # REF – Reference Numbers
            elif elements[0] == 'REF':
                if elements[1] == 'ZS':  # Application Number
                    ordersObj['Division'] = elements[2]  # Division name for packing slip
            elif elements[0] == 'SAC':
                if elements[1] == 'C' and elements[2] == 'G830':  # Customer’s shipping and handling
                    ordersObj['shipping_tax'] = elements[5]

            # FOB – FOB Related Instructions
            elif elements[0] == 'FOB':
                ordersObj['carrier'] = elements[1]

            # DTM – Date Time Reference
            elif elements[0] == 'DTM':
                if len(elements) > 3 and elements[2] and elements[3]:
                    dt = datetime.strptime(elements[2] + elements[3], "%Y%m%d%H%M")
                    dt_str = datetime.strftime(dt, "%d/%m/%Y %H:%M:00")
                    if elements[1] == '071':
                        ordersObj['additional_fields'].append((0, 0, {
                            'name': 'requested_date',
                            'label': 'Requested for Delivery',
                            'value': dt_str
                        }))
                    elif elements[1] == '038':
                        ordersObj['additional_fields'].append((0, 0, {
                            'name': 'deadline',
                            'label': 'Ship No Later',
                            'value': dt_str
                        }))

            # AMT – Dollar Amount
            elif elements[0] == 'AMT':
                if elements[1] == '2':
                    ordersObj['amount_total'] = elements[2]

            # N1 – Name
            elif elements[0] == 'N1':
                if elements[1] == 'RI':  # Bill-to-Party, Vendor
                    address_type = ''
                elif elements[1] == 'BT':  # Sold To If Different From ShipTo
                    address_type = 'order'
                    ordersObj['address']['order'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['order']['name'] = elements[2]
                elif elements[1] == 'ST':  # Ship To
                    address_type = 'ship'
                    ordersObj['address']['ship'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['ship']['name'] = elements[2]

            # N2 –
            elif elements[0] == 'N2' and address_type != '':
                ordersObj['address'][address_type]['name2'] = elements[1]

            # N3 – Address Information
            elif elements[0] == 'N3' and address_type != '':
                ordersObj['address'][address_type]['address1'] = elements[1]
                if len(elements) > 2:
                    ordersObj['address'][address_type]['address2'] = elements[2]

            # N4 – Geographic Location
            elif elements[0] == 'N4' and address_type != '':
                ordersObj['address'][address_type]['city'] = elements[1]
                ordersObj['address'][address_type]['state'] = elements[2]
                ordersObj['address'][address_type]['zip'] = elements[3]
                if len(elements) > 4:
                    country = elements[4]
                    # FIXME: Hardcode because BBY EDI sent CAN code instead of CA.
                    if country == "CAN":
                        country = "CA"
                    ordersObj['address'][address_type]['country'] = country

            # PO1 – Base-line Item Data
            elif elements[0] == 'PO1':
                lineObj = {
                    'id': elements[1],
                    'qty': elements[2],
                    'cost': elements[4],
                    'sku': elements[7],
                    'vendorSku': elements[7],
                    'UPC': elements[9],
                    'merchantSKU': elements[9],
                    'customer_sku': elements[11],
                    'optionSku': elements[11],
                    'name': elements[11],
                }
                if len(elements) > 15 and elements[7] and elements[15] and elements[7].endswith('J'+str(elements[15])):
                    lineObj['size'] = elements[15]
                ordersObj['lines'].append(lineObj)

            # PID – Product Item Description
            elif elements[0] == 'PID':
                if len(elements) > 9 and elements[9] == 'EN':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['name'] = elements[5]
            # Segment for Purchase Order Change
            elif elements[0] == 'POC':  # Change
                if elements[2] == 'DI':  # Delete Item(s)
                    ordersObj['lines'].append(elements[1])
                elif elements[2] == 'RZ':
                    lineObj = {
                        'id': elements[1],
                        'qty': elements[3],
                        'qty_left': elements[4],  # Quantity Left to Receive
                    }
                    for i in (10, 12, 14, 16):
                        if len(elements) > i:
                            if elements[i] == 'UP':
                                lineObj['upc'] = elements[i + 1]
                            elif elements[i] == 'VP':
                                lineObj['vendor_sku'] = elements[i + 1]
                            elif elements[i] == 'VE':
                                lineObj['supplier_color'] = elements[i + 1]
                            elif elements[i] == 'SZ':
                                lineObj['size'] = elements[i + 1]
                        else:
                            break
                    ordersObj['lines'].append(lineObj)
            elif elements[0] == 'GE':
                self.number_of_transaction = elements[1]
            elif elements[0] == 'SE':
                return_orders.append(ordersObj)

        return return_orders


class CommercehubEdiInvoiceGenerator(BaseEDIClient):

    def __init__(self, parent):
        self.parent = parent
        super(CommercehubEdiInvoiceGenerator, self).__init__(self.parent.settings, None)
        self.lines = self.parent.lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        # SEGMENT ST - TRANSACTION SET HEADER
        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': self.edi_type,
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        # SEGMENT BIG - BEGINNING SEGMENT FOR STANDARD INVOICES
        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]*[4]%(po_number)s*[5]*[6]*[7]%(invoice_type)s'
        invoice_date = datetime.now()
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.lines[0]['invoice'],
            'po_number': self.lines[0]['po_number'],
            # BIG 07: Invoice Type
            # Available values DR (Debit) and CR (credit).
            # Best Buy/Future Shop Systems currently CANNOT process Credit Invoices (Code CR) via EDI.
            'invoice_type': 'DR',
        }))

        nte = 'NTE*INV*[1]%(text_message)s'
        segments.append(self.insertToStr(nte, {
            'text_message': 'Invoice {0} for order {1}'.format(
                self.lines[0]['invoice'],
                self.lines[0]['po_number']
            ),
        }))

        # Indicates which currency is in use on the invoice.
        # MUST be the same as the currency listed on the Purchase Order
        cur = 'CUR*[1]%(currency)s*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'currency': 'CQ',
            'currency_code': self.lines[0]['order_additional_fields'].get('currency', None) or 'USD',  # Correct for all?
        }))

        ref_pk = 'REF*PK*[1]%(tracking_number)s'
        segments.append(self.insertToStr(ref_pk, {
            'tracking_number': self.lines[0]['tracking_number'],
        }))

        ref_vn = 'REF*VN*[1]%(vendor_number)s'
        segments.append(self.insertToStr(ref_vn, {
            'vendor_number': self.lines[0]['po_number'],
        }))

        if self.lines[0]['gst']:
            ref_vn = 'REF*GT*[1]%(gst_number)s'
            segments.append(self.insertToStr(ref_vn, {
                'gst_number': self.gst_number,
            }))

        address_services = self.lines[0]['address_services']
        for asn in address_services:
            current_service = address_services[asn]

            n1 = 'N1*[1]%(edi_code)s*[2]%(remit_to_name)s'
            segments.append(self.insertToStr(n1, {
                'edi_code': current_service['edi_code'],
                'remit_to_name': current_service['name'],
            }))

            n3 = 'N3*[1]%(address)s'
            segments.append(self.insertToStr(n3, {
                'address': current_service['address1'],
            }))

            n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[4]%(country)s'
            segments.append(self.insertToStr(n4, {
                'city': current_service['city'],
                'state': current_service['state'],
                'zip': current_service['zip'],
                'country': current_service['country'],
            }))

        # SEGMENT ITD – TERMS OF SALE
        itd = 'ITD*[1]%(terms_type_code)s*[2]%(terms_date_code)s*[3]*[4]*[5]*[6]*[7]%(numbers_of_days)s'
        segments.append(self.insertToStr(itd, {
            'terms_type_code': '01',
            'terms_date_code': '3',  # (1=shipdate, 2=delivery date, 3=invoice date)
            'numbers_of_days': '5',
        }))

        # SEGMENT DTM - DATE/TIME REFERENCE
        dtm = 'DTM*[1]011*[2]%(shipping_date)s'
        segments.append(self.insertToStr(dtm, {
            'shipping_date': invoice_date.strftime('%Y%m%d'),
        }))

        # SEGMENT IT1 - BASELINE ITEM DETAIL (INVOICE)
        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]%(basis_of_unit_price_code)s*[6]%(product_qualifier)s*[7]%(vendorSku)s*[8]*[9]*UP*[10]%(upc)s'

        # SEGMENT PID - PRODUCT/ITEM DESCRIPTION
        pid = 'PID*[1]F*[2]*[3]*[4]*[5]%(description)s'

        # ctp = 'CTP*[1]*[2]*[3]*[4]*[5]*[6]*[7]*[8]%(line_price)s'
        line_number = 0
        for line in self.lines:
            line_number += 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(int(line['product_qty'])),
                'unit_price': str("{0:.2f}".format(line['price_unit'])),
                'basis_of_unit_price_code': 'CP',  # CP - Current Price
                'product_qualifier': 'BP',  # [6] “ BP “ VendorSku
                'vendorSku': line['vendorSku'],
                'upc': line['merchantSKU'],
            }))

            segments.append(self.insertToStr(pid, {
                'description': line['name'],
            }))

        tds = 'TDS*[1]%(total_amount)s*[2]%(sub_total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': str("{0:.2f}".format(
                self.lines[0]['amount_untaxed'] + self.lines[0]['gst'] + self.lines[0]['pst'] + self.lines[0]['qst'] + self.lines[0]['hst'])
            ).replace('.', ''),
            'sub_total_amount': str("{0:.2f}".format(
                self.lines[0]['amount_untaxed'])
            ).replace('.', ''),
        }))

        tax_identifiers = {
            'gst': 'CG',
            'pst': 'SP',
            'qst': 'ST',
            'hst': 'VA',
        }
        for name_tax in ['gst', 'pst', 'qst', 'hst']:
            txi = 'TXI*[1]%(tax_identifier)s*[2]%(amount_tax)s'
            segments.append(self.insertToStr(txi, {
                'tax_identifier': tax_identifiers[name_tax],
                'amount_tax': str("{0:.2f}".format(self.lines[0][name_tax])),
            }))

        ctt = 'CTT*[1]%(line_number)s'
        segments.append(self.insertToStr(ctt, {
            'line_number': line_number
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))
        return self.wrap(segments, {'group': 'IN'})


class CommercehubApiEdiOpenerp(ApiOpenerp):

    def __init__(self):
        super(CommercehubApiEdiOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        if order['additional_fields']:
                additional_fields = order['additional_fields']

        order_obj = {
            'additional_fields': additional_fields
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if (context is None):
            context = {}
        line_obj = {
            "notes": "",
            'id': line['id'],
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'optionSku': line['optionSku'],
            'vendorSku': line['vendorSku'],
            'customer_sku': line['customer_sku'],
            'UPC': line['UPC'],
            'qty': line['qty']
        }
        if (line.get('size', False) and line.get('sku', False)):
            try:
                float(line['size'])
            except Exception:
                line['sku'] += '/' + line['size']
                line['size'] = False

        product = False

        field_list = ['UPC', 'merchantSKU', 'vendorSku', 'customer_sku']
        for field in field_list:
            if (line.get(field, False)):
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr, uid, context['customer_id'], line[field])
                if (product):
                    if (size and size.id):
                        line_obj["size_id"] = size.id
                    elif (line.get('size', False)):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if (len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break
        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by upc {0}.\n".format(line['UPC'])
            line_obj["product_id"] = 0

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        line = {
            'address_services': {
                'remmit_to': {
                    'edi_code': 'RI',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'bill_to': {
                    'edi_code': 'BT',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
            }
        }
        if (sale_obj.partner_invoice_id):
            line['address_services']['remmit_to'].update({
                'name': 'FIRST CANADIAN DIAMOND CUTTING WORKS',
                'address1': sale_obj.partner_invoice_id.street or '',
                'address2': sale_obj.partner_invoice_id.street2 or '',
                'city': sale_obj.partner_invoice_id.city or '',
                'state': sale_obj.partner_invoice_id.state_id.code or '',
                'country': sale_obj.partner_invoice_id.country_id.code or '',
                'zip': sale_obj.partner_invoice_id.zip or '',
            })
        if (sale_obj.partner_order_id):
            line['address_services']['bill_to'].update({
                'name': sale_obj.partner_order_id.name or '',
                'address1': sale_obj.partner_order_id.street or '',
                'address2': sale_obj.partner_order_id.street2 or '',
                'city': sale_obj.partner_order_id.city or '',
                'state': sale_obj.partner_order_id.state_id.code or '',
                'country': sale_obj.partner_order_id.country_id.code or '',
                'zip': sale_obj.partner_order_id.zip or '',
            })
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })
        return line

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = ",".join([str(x.id) for x in settings.customer_ids])

        cr.execute(
            """SELECT
                    so.name AS name,
                    sp.id AS picking_id
                FROM
                    sale_order so
                    INNER JOIN stock_picking sp ON so.id = sp.sale_id
                WHERE
                    so.po_number = '{po_number}' AND
                    so.partner_id IN ({cutomer_ids})
            """.format(
            po_number=cancel_obj['poNumber'],
            cutomer_ids=customer_ids
        ))

        return cr.dictfetchall()
