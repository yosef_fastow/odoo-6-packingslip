# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from datetime import datetime
from utils import feedutils as feed_utils
from configparser import ConfigParser
from os.path import dirname
import logging
from customer_parsers.ebay_input_xml_parser import XML_parser
import requests
import json

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}

SETTINGS_FIELDS = (
    ('site_id',               'Site ID',            ''),  # Fake Data
    ('country',               'Country',            ''),  # Fake Data
    ('currency',              'Currency',           ''),  # Fake Data
    ('version',               'Version',            ''),  # Fake Data
    ('cc',                    'CC',                 ''),  # Fake Data

)


class EbayApi(FtpClient):

    def __init__(self, settings):
        super(EbayApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/ebay_settings.ini' % (dirname(__file__)))

        # Inventory settings
        self.line_sku_name = "vendor_id"
        self.line_size_name = "size"
        self.line_size_str_name = "size_str"
        self.line_ring_sizes_name = "ring_sizes"
        self.line_qty_name = "qty"
        self.relationshipdetails_str = "Size="
        self.csv_delimeter = ","
        title_settings = {x: settings.get(x) for x in ['site_id', 'country', 'currency', 'version', 'cc']}
        self.title_str = "Action(SiteID={site_id}|Country={country}|Currency={currency}|Version={version}|CC={cc}),ItemID,Relationship,RelationshipDetails,Quantity\n".format(**title_settings)

        # Always filling sizes
        self.mandatory_sizes_list = []
        if settings.get('from_size', False) and settings.get('to_size', False):
            try:
                from_size = float(settings['from_size'])
                to_size = float(settings['to_size'])
                self.mandatory_sizes_list = [str(x * 0.5) for x in range(int(from_size * 2), int(to_size * 2 + 1))]
            except Exception:
                pass

    def fill_inventory_line(self, line, config_name, size=False, qty=None):
        line_qty = self.line_qty_name
        line_size = self.line_size_name
        line_size_str = self.line_size_str_name
        line_ring_sizes = self.line_ring_sizes_name
        if str(line[self.line_sku_name]).strip() in ['321367501353', '321367501595']:
            relationshipdetails_str = 'RingSize='
        else:
            relationshipdetails_str = self.relationshipdetails_str
        delimeter = self.csv_delimeter

        if size:
            line_to_fill = line.copy()
            line_to_fill[line_size] = size
        else:
            line_to_fill = line

        if line_to_fill[line_size] and config_name == 'VariationWithSized':
            line_to_fill[line_size_str] = relationshipdetails_str + str(line_to_fill[line_size]).replace(".0", "")
        elif config_name == 'FirstWithSized':
            sizes_str = ';'.join(size).replace('.0', '')
            line_to_fill[line_ring_sizes] = relationshipdetails_str + sizes_str

        if qty is not None:
            line_to_fill[line_qty] = qty

        return feed_utils.FeedUtills(self.config.items(config_name), line_to_fill).create_csv_line(delimeter, True)

    def get_attach_data(self, lines=None):
        if lines is None:
            lines = self.confirmLines
        else:
            self.confirmLines = lines

        mandatory_sizes_list = self.mandatory_sizes_list
        current_size_index = 0
        prev_line = {}

        line_sku = self.line_sku_name
        line_size = self.line_size_name

        sku_record = ""

        data = self.title_str
        # data_block stores records with the same current sku
        data_block = []
        # Current sizes list may include not only mandatories. Usually they are equal to each other
        current_sizes_list = mandatory_sizes_list[:]

        for line in self.confirmLines:

            # Check if sizes list comes in lines prioritize it
            # else take from product settings min and max size
            current_sizes_list = line.get('RelationshipDetails') and \
                                 line.get('RelationshipDetails').lstrip('Size=').split(',') or \
                                 current_sizes_list

            # if product don't have a customer_sku -> continue
            if not line.get(line_sku, False):
                self.append_to_revision_lines(line, 'bad')
                continue

            size_f = 0.0
            if line.get(line_size, False):
                try:
                    size_f = float(line[line_size])
                except Exception:
                    self.append_to_revision_lines(line, 'bad')

                if not (size_f and str(size_f) in mandatory_sizes_list):
                    continue

            # Fill big-sized lines for previous line_sku
            # Add data blok for previos sku into result
            if prev_line.get(line_sku, False) != line[line_sku]:
                if prev_line.get(line_size, False):
                    for size in mandatory_sizes_list[current_size_index:]:
                        sku_record = self.fill_inventory_line(prev_line, 'VariationWithSized', size=size, qty=0)
                        data_block.append(sku_record)
                        self.append_to_revision_lines(line, 'good')
                    # Re-sort if current_sizes_list was changed
                    if len(current_sizes_list) != len(mandatory_sizes_list):
                        current_sizes_list = [str(i) for i in sorted([float(e) for e in current_sizes_list])]
                    sku_record = self.fill_inventory_line(prev_line, 'FirstWithSized', size=current_sizes_list)
                    data += sku_record

                for record in data_block:
                    data += record or ''

                current_size_index = 0
                if len(current_sizes_list) != len(mandatory_sizes_list):
                    current_sizes_list = mandatory_sizes_list[:]
                data_block = []

            # Fill if sized
            if size_f:
                # Fill small-sized (or just less than current size) for current line_sku
                for size in mandatory_sizes_list[current_size_index:]:
                    if size_f > float(size):
                        sku_record = self.fill_inventory_line(line, 'VariationWithSized', size=size, qty=0)
                        data_block.append(sku_record)
                        self.append_to_revision_lines(line, 'good')
                        current_size_index += 1
                    else:
                        break

                # Adding not mandatory size
                # if line[line_size] not in current_sizes_list:
                #     current_sizes_list.append(line[line_size])

                sku_record = self.fill_inventory_line(line, 'VariationWithSized')
                data_block.append(sku_record)
                self.append_to_revision_lines(line, 'good')

                if mandatory_sizes_list and len(mandatory_sizes_list) > current_size_index and mandatory_sizes_list[current_size_index] == line[line_size]:
                    current_size_index += 1
            # Fill if non-sized
            else:
                sku_record = self.fill_inventory_line(line, 'NonSized')
                data_block.append(sku_record)
                self.append_to_revision_lines(line, 'good')

            prev_line = line

        # Add the last block
        if prev_line.get(line_size, False):
            for size in mandatory_sizes_list[current_size_index:]:
                sku_record = self.fill_inventory_line(prev_line, 'VariationWithSized', size=size, qty=0)
                data_block.append(sku_record)
                self.append_to_revision_lines(line, 'good')
            sku_record = self.fill_inventory_line(prev_line, 'FirstWithSized', size=current_sizes_list)
            data += sku_record
        for record in data_block:
            data += record or ''

        return data[:-2]

    def process_request(self):
        json_data = self.get_json()
        response = requests.post("{0}/shipping".format(self.url), data=json_data)
        print(response.content)
        result = {
            'json_data': json_data,
            'response': response.content,
            'status': False,
        }
        self._log.append({
            'title': 'Response from {0}/shipping'.format(self.url),
            'msg': response.content,
            'create_date': "{:%Y-%m-%d %H:%M:%S}".format(datetime.now())
        })
        if(response.status_code == 200):
            response_status = json.loads(response.content)[0]['result']['Ack']
            if(str(response_status).strip().lower() == 'success'):
                result.update({'status': True})
        self._log.append({
            'title': 'Result confirmation eBay',
            'msg': str(json.dumps(result)),
            'create_date': "{:%Y-%m-%d %H:%M:%S}".format(datetime.now())
        })
        return result


class EbayApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(EbayApiClient, self).__init__(
            settings_variables, EbayOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = EbayGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def updateQTY(self, lines, mode=None):
        updateApi = EbayApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def getAttachData(self, lines):
        csvApi = EbayApi(self.settings)
        data = csvApi.get_attach_data(lines)

        return data

    def confirmShipment(self, lines):
        res = True
        confirmApi = EbayApiConfirmShipment(self.settings, lines)
        res_confirm = confirmApi.process_request()
        self.extend_log(confirmApi)
        res = res_confirm['status']
        return res


class EbayGetOrdersXML(EbayApi):

    def __init__(self, settings):
        super(EbayGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))
        self.parser = XML_parser(external_customer_id=self.customer)


class EbayApiUpdateQTY(EbayApi):
    filename = ""
    confirmLines = []

    def __init__(self, settings, lines):
        super(EbayApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory', self.get_root_dir(settings))
        self.confirmLines = lines
        self.filename = "{:EBAYREVISEINVENTORY%Y%m%d%H%M.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        return self.get_attach_data()


class EbayApiConfirmShipment(EbayApi):

    def __init__(self, settings, lines):
        super(EbayApiConfirmShipment, self).__init__(settings)
        self.lines = lines

    def get_json(self):
        lines = self.lines
        confirm_list = []
        for line in lines:
            additional_fields = line.get('additional_fields', {})
            confirm_obj = {
                "customer_login": str(additional_fields.get('customer_login', False)),
                "order_line_item_id": str(additional_fields.get('order_line_item_id', False)),
                "shipment": {
                    "tracking_number": line["tracking_number"],
                    "carrier": line["carrier_code"],
                },
                "order_id": line["po_number"],
            }
            confirm_list.append(confirm_obj)
        result = json.dumps(confirm_list)
        return result


class EbayOpenerp(ApiOpenerp):

    def __init__(self):
        super(EbayOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}

        additional_fields = []

        line_obj = {
            'id': line['id'],
            'customer_sku': line['customer_sku'],
            'sku': line['sku'],
            'name': line['name'],
            'size': line['size'],
            'qty': line['qty'],
            'cost': line['price_unit'],
            'price_unit': line['price_unit'],
            'shipCost': line['ship_cost'],
            'notes': "",
        }

        if(line.get('order_line_item_id', False)):
            additional_fields.append((0, 0, {
                'name': 'order_line_item_id',
                'label': 'Order Line Item ID',
                'value': line['order_line_item_id']
            }))
        if(line.get('customer_login', False)):
            additional_fields.append((0, 0, {
                'name': 'customer_login',
                'label': 'Customer Login',
                'value': line['customer_login']
            }))

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['customer_sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = False

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                        'customer_id_delmar',
                        'vendor_id',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if(product):
            line_obj["product_id"] = product.id
            # # DELMAR-186
            # if line_obj["name"] == None or line_obj["name"] == "empty product description in order":
            #     line_obj["name"] = product.name
            # # END DELMAR-186
            if(not line_obj.get('price_unit', False)):
                price_unit = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if(price_unit):
                    line_obj['price_unit'] = price_unit
                    line_obj['cost'] = price_unit
                else:
                    line_obj['notes'] += "Can't find price unit for name %s" % (line['name'])
        else:
            line_obj["notes"] = "Can't find product by sku {0}.\n".format(line['sku'])
            line_obj["product_id"] = 0
        line_obj.update({'additional_fields': additional_fields})
        return line_obj

    def fill_order(self, cr, uid, settings, order, context=None):
        return {
            'cust_order_number': order.get('cust_order_number', None)
        }

