# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from customer_parsers.gdm_input_csv_parser import CSVParser
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class GdmApi(FtpClient):

    def __init__(self, settings):
        super(GdmApi, self).__init__(settings)

    def upload_data(self):
        return {'lines': self.lines}


class GdmApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(GdmApiClient, self).__init__(
            settings_variables, GdmOpenerp, False, DEFAULT_VALUES)
        self.settings["order_type"] = 'normal'
        self.load_orders_api = GdmApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='normal'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        self.settings["order_type"] = order_type
        api = GdmApiGetOrdersXML(self.settings)
        if save_flag:
            orders = api.process('load')
        else:
            orders = api.process('read_new')
        self.extend_log(api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = GdmApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = GdmApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class GdmApiGetOrdersXML(GdmApi):

    def __init__(self, settings):
        super(GdmApiGetOrdersXML, self).__init__(settings)
        self.order_type = settings['order_type']
        self.use_ftp_settings('load_orders')
        if(self.order_type == 'normal'):
            self.use_ftp_settings('load_orders')
        else:
            self.use_ftp_settings('load_cancel_orders')
        self.parser = CSVParser(self.customer, ',', '"')


class GdmApiUpdateQTY(GdmApi):

    def __init__(self, settings, lines):
        super(GdmApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "inv.csv"
        self.filename_local = "{:GDMINVENTORY%Y%m%d%H%M.csv}".format(
            datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def get_sku_and_size_str(self, line):
        _size_str = ''
        _sku = line.get('sku', '')
        if('/' in _sku):
            try:
                _size_str = str(
                    float(_sku[_sku.find("/") + 1:])).replace('.0', '')
                _sku = _sku[:_sku.find("/")]
            except ValueError:
                pass
        return _sku, _size_str

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if((not line.get('sku', False)) and line.get('sku', False) == ''):
                self.append_to_revision_lines(line, 'bad')
                continue
            line['sku_str'], line['size_str'] = self.get_sku_and_size_str(line)
            if(line['customer_price'] is None):
                line['customer_price'] = ''
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class GdmApiConfirmOrders(GdmApi):

    def __init__(self, settings, lines):
        super(GdmApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "{:Confirm%Y%m%d-%H%M.csv}".format(datetime.now())

    def upload_data(self):
        lines = self.lines
        for line in lines:
            amount_line = 0.0
            if(line.get('price_unit', False) and line.get('product_qty', False)):
                price_unit = 0.0
                product_qty = 0
                try:
                    price_unit = float(line['price_unit'])
                except ValueError:
                    pass
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
                if(price_unit and product_qty):
                    amount_line = price_unit * product_qty
            line['amount_line'] = amount_line
            date_now = datetime.now()
            if line.get('shp_date', False):
                date_now = datetime.strptime(
                    line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y %I:%M:%S %p")
            line['product_qty'] = int(line['product_qty'])
        return {'lines': lines}


class GdmOpenerp(ApiOpenerp):

    def __init__(self):
        super(GdmOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': '0.0',
            'customerCost': line['price_unit'],
            'customer_sku': line['customer_sku'],
            'vendorSku': line['sku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': line['sku'],
            'size': line['size'],
            'id': line['id'],
        }

        if('/' in line['sku']):
            try:
                _sku = line['sku']
                _size = float(_sku[_sku.find("/") + 1:])
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
