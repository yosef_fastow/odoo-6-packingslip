# -*- coding: utf-8 -*-
import csv
import logging
from lxml import etree
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
import configparser
from os.path import join as join_path
import xlwt
import cStringIO
from io import BytesIO
from datetime import datetime, timedelta
from dateutil import tz
from commercehub_customers import (
    bedbath, belk, bestbuyca,
    bluestem, bonton, groupongoods,
    drugstore, fmj, jcpenney, kmart,
    kohls, main_parser, meijer,
    ppower, qvc, sears,
    searscanada, stagestores, costco, snbc, walmartca,
    signetjewelers, wish, lord, bjs, macys, sterling
)

from commercehub_invoices import main_xml_builder, other_xml_builder, canada_xml_builder
from openerp.addons.pf_utils.utils.decorators import return_ascii
from openerp.addons.pf_utils.utils.csv_utils import UnicodeWriter
from pf_utils.utils.re_utils import f_d
from utils import feedutils as feed_utils
import re
import json
import time

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}

SETTINGS_FIELDS = None


class CommercehubApi(FtpClient):

    def __init__(self, settings):
        super(CommercehubApi, self).__init__(settings)

    def check_response(self, response):
        if (not response.ok):
            _logger.error("CommerceHub api error: message - %s" % (response.stderr))
            raise Exception("CommerceHub api error: message - %s" % (response.stderr))

    def create_xls_line(self, sheet, row, columns_total=45, datas=None):
        if not datas:
            datas = []
        for data in datas:
            for i in xrange(0, columns_total):
                sheet.write(row, i, data.get('cell_' + str(i)))
            row += 1
        return row

    def create_xls_data_line(self, sheet, row, columns_total, config, datas=[]):
        for src_line in datas:
            result = feed_utils.FeedUtills(config, src_line)._prepare_line_data(skip_invalid_line=False)
            if result:
                keys = [str(dKey) for dKey, dVal in config]
                values = []
                for el in result:
                    if isinstance(el, int):
                        el = int(el)
                    elif isinstance(el, float):
                        el = float(el)
                    else:
                        el = re.sub('[\"\'$]', '', el.encode('ascii', 'ignore')).strip()
                    values.append(el)
                result = dict(zip(keys, values))
                row = self.create_xls_line(sheet, row, columns_total, [result])
        
        return row

    def create_delimited_line(self, params, line, skip_invalid_line=False, csv_flag=False):
        result = feed_utils.FeedUtills(params, line)._prepare_line_data(skip_invalid_line=skip_invalid_line)
        if not result:
            return False

        result = [unicode(el) for el in result]

        if csv_flag:
            output = cStringIO.StringIO()
            writer = UnicodeWriter(output, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(result)
            return output.getvalue()
        return "|".join(result) + "\n"

    def set_decrypt_data(self, data):
        gpg = self.get_gnupg(self.gnupghome)
        decrypt_data = gpg.decrypt(data, passphrase=self.passphrase)
        return decrypt_data

    def set_encrypt_data(self, input_data):
        gpg = self.get_gnupg(self.gnupghome)
        encrypt_flag = True
        encrypt_data = gpg.encrypt(input_data, '898BC765D65F967F44FC2202DD5D9E135439741D', always_trust=True)
        self.check_response(encrypt_data)
        return encrypt_data, encrypt_flag

    def get_root_dir(self, settings):
        if (settings["customer_root_dir"]):
            path = str(settings["customer_root_dir"])
        else:
            path = str(self.customer)
        root_dir = "commercehub/{0}".format(path)
        return root_dir

    def log(self, log_line=None):
        if (log_line):
            title = log_line.get('title', False)
            msg_type = log_line.get('type', False)
            message = log_line.get('message', '')
            if (msg_type):
                self.append_to_logger(msg_type, message)
            if (title):
                if (msg_type):
                    title = '{0} method: {1} for {2}: {3}'.format(
                        str(msg_type).capitalize(),
                        self.name,
                        self.customer,
                        title
                    )
                else:
                    title = 'INFO method: {0} for {1}: {2}'.format(
                        self.name,
                        self.customer,
                        title
                    )
                self.append_to_log(title, message)
        return True

    def check_email_sending(self, ch_settings):
        # Check send or not information about order action to customer and chouse way to send
        # For Acknowledgement, Confirm Shipment, Return Order, Invoice
        if 'manual_order' in ch_settings and 'use_mail_instead_of_ftp' in ch_settings and 'send_email_list' in ch_settings:
            if ch_settings['manual_order'] and ch_settings['use_mail_instead_of_ftp'] and ch_settings[
                'send_email_list']:
                return ch_settings['send_email_list']

        return False


class CommercehubApiClient(AbsApiClient):
    use_local_folder = True
    __apierp_map = {
        'bedbath': bedbath.CommercehubApiBedBathOpenerp,
        'belk': belk.CommercehubApiBelkOpenerp,
        'bluestem': bluestem.CommercehubApiBluestemOpenerp,
        'bonton': bonton.CommercehubApiBontonOpenerp,
        'kohls': kohls.CommercehubApiKohlsOpenerp,
        'groupongoods': groupongoods.CommercehubApiGrouponGoodsOpenerp,
        'iqvc': qvc.CommercehubApiQVCOpenerp,
        'kmart': kmart.CommercehubApiKmartOpenerp,
        'meijer': meijer.CommercehubApiMeijerOpenerp,
        'sears': sears.CommercehubApiSearsOpenerp,
        'searscanada': searscanada.CommercehubApiSearsCanadaOpenerp,
        'costco': costco.CommercehubApiCostcoOpenerp,
        'snbc': snbc.CommercehubApiSnbcOpenerp,
        'walmartca': walmartca.CommercehubApiWalmartcaOpenerp,
        'signetjewelers': signetjewelers.CommercehubApiSignetJewelersOpenerp,
        'wish': wish.CommercehubApiWishOpenerp,
        'hbc': lord.CommercehubApiLordOpenerp,
        'bjs': bjs.CommercehubApiBjsOpenerp,
        'macys': macys.CommercehubApiMacysOpenerp,
        'signetkj': sterling.CommercehubApiSterlingOpenerp
    }

    def __init__(self, settings_variables):
        apierp = self.__apierp_map.get(settings_variables['customer'], False)
        setting_fields = False
        try:
            exec (
                'from commercehub_customers.{customer} import SETTINGS_FIELDS as CUSTOMER_SETTINGS_FIELDS'.format(
                    customer=settings_variables['customer']
                )
            )
            setting_fields = CUSTOMER_SETTINGS_FIELDS
        except (NameError, SyntaxError, ImportError):
            setting_fields = SETTINGS_FIELDS

        super(CommercehubApiClient, self).__init__(
            settings_variables, apierp, setting_fields, DEFAULT_VALUES)
        self.load_orders_api = CommercehubApiGetOrdersXML

    @property
    def customers_with_invoice(self):
        return ['drugstore', 'sears', 'bedbath', 'bestbuyca', 'fmj', 'ppower', 'snbc', 'walmartca', 'signetjewelers',
                'hbc', 'bjs', 'macys']

    @customers_with_invoice.setter
    def customers_with_invoice(self, value):
        raise Exception('You can\'t set property customers_with_invoice')

    def loadOrders(self, save_flag=False):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        time.sleep(3)
        ordersApi = CommercehubApiGetOrderObj(xml, self.settings['customer'])
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = CommercehubApiUpdateQTY(self.settings, lines)
        updateApi.process('send')

        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def Acknowledgement(self, order):
        time.sleep(3)
        AcknowledgementApi = CommercehubApiAcknowledgement(self.settings, order)
        res = AcknowledgementApi.process('send')
        self.extend_log(AcknowledgementApi)
        return res

    def returnOrders(self, lines):
        if self._settings.get('skip_return', '').lower().strip() not in ['1', 'true', 'yes']:
            ReturnApi = CommercehubApiReturnOrders(self.settings, lines)
            ReturnApi.process('send')
            self.extend_log(ReturnApi)
        return True

    def confirmShipment(self, lines, ch_settings=None, context=None):
        if not ch_settings:
            ch_settings = {}
        if (context is None):
            context = {}
        if self.settings:
            self.settings.update({
                '_emails': ch_settings.get('send_email_list') or False,
                '_email_title': 'Confirm Shipment',
            })
        confirmApi = CommercehubApiConfirmOrders(self.settings, lines)
        emails = confirmApi.check_email_sending(ch_settings)
        # send confirm by email if manual and has flag
        time.sleep(3)
        if emails:
            res_confirm = bool(confirmApi.process('send_mail'))
        else:
            res_confirm = bool(confirmApi.process('send'))
        self.extend_log(confirmApi)
        res_invoice = None
        if (self.is_invoice_required and self.settings['customer'] in self.customers_with_invoice and context.get(
                'invoice', True)):
            if res_confirm:
                invoiceApi = CommercehubApiInvoiceOrders(self.settings, lines)
                time.sleep(3)
                if emails:
                    if self.settings:
                        self.settings.update({'_email_title': 'Invoice'})
                    res_invoice = bool(invoiceApi.process('send_mail'))
                else:
                    res_invoice = bool(invoiceApi.process('send'))
                self.extend_log(invoiceApi)
            else:
                res_invoice = False
        else:
            res_invoice = True
        return res_confirm and res_invoice


class CommercehubApiGetOrderObj():
    name = "GetOrderObj"

    def __init__(self, xml, customer):
        self.xml = xml
        self.customer = customer

    def getOrderObj(self):

        ordersObj = None
        try:
            ordersObj = json.loads(self.xml)
        except Exception:
            pass

        parsers_map = {
            'bedbath': bedbath.CommercehubApiGetOrderObjBedBath,
            'belk': belk.CommercehubApiGetOrderObjBelk,
            'bestbuyca': bestbuyca.CommercehubApiGetOrderObjBestBuyCa,
            'bluestem': bluestem.CommercehubApiGetOrderObjBluestem,
            'bonton': bonton.CommercehubApiGetOrderObjBonton,
            'groupongoods': groupongoods.CommercehubApiGetOrderObjGrouponGoods,
            'drugstore': drugstore.CommercehubApiGetOrderObjDrugstore,
            'fmj': fmj.CommercehubApiGetOrderObjFMJ,
            'iqvc': qvc.CommercehubApiGetOrderObjQVC,
            'jcpenney': jcpenney.CommercehubApiGetOrderObjJcpenney,
            'kmart': kmart.CommercehubApiGetOrderObjKmart,
            'kohls': kohls.CommercehubApiGetOrderObjKohls,
            'meijer': meijer.CommercehubApiGetOrderObjMeijer,
            'ppower': ppower.CommercehubApiGetOrderObjPPower,
            'sears': sears.CommercehubApiGetOrderObjSears,
            'searscanada': searscanada.CommercehubApiGetOrderObjSearsCanada,
            'stagestores': stagestores.CommercehubApiGetOrderObjStageStores,
            'costco': costco.CommercehubApiGetOrderObjCostco,
            'snbc': snbc.CommercehubApiGetOrderObjSnbc,
            'walmartca': walmartca.CommercehubApiGetOrderObjWalmartca,
            'signetjewelers': signetjewelers.CommercehubApiGetOrderObjSignetJewelers,
            'wish': wish.CommercehubApiGetOrderObjWish,
            'hbc': lord.CommercehubApiGetOrderObjLord,
            'bjs': bjs.CommercehubApiGetOrderObjBjs,
            'macys': macys.CommercehubApiGetOrderObjMacys,
            'signetkj': sterling.CommercehubApiGetOrderObjSterling,
        }


        if ordersObj is None:
            if self.customer in parsers_map:
                parser = parsers_map[self.customer]()
            else:
                parser = main_parser.CommercehubApiGetOrderObjMain()

            ordersObj = parser.getOrderObj(self.xml)
        return ordersObj


class CommercehubApiGetOrdersXML(CommercehubApi):
    name = "GetOrdersFromLocalFolder"
    customer = ""

    def __init__(self, settings):
        super(CommercehubApiGetOrdersXML, self).__init__(settings)
        self.customer = str(settings.get("customer", ""))
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))

    def parse_response(self, response):
        if (not response):
            return []
        ordersList = []
        for data in response:
            data = str(data)
            root = etree.XML(data)
            partnerID = root.findtext('partnerID')
            for order in root.findall('hubOrder'):
                ordersObj = {}
                ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                ordersObj['name'] = order.findtext('poNumber')
                ordersObj['partner_id'] = partnerID
                ordersList.append(ordersObj)
        return ordersList


class CommercehubApiAcknowledgement(CommercehubApi):
    name = "Acknowledgement"

    def __init__(self, settings, orders_list):
        super(CommercehubApiAcknowledgement, self).__init__(settings)
        self.order = orders_list
        self.use_ftp_settings('acknowledgement', self.get_root_dir(settings))
        self.filename = orders_list['name']

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, order_name):
        if not self.raised_errors:
            if self._use_encrypt:
                self._filename = f_d('{order_name}_acknowledgement_%d%m%y_%H%M%S.pgp'.format(order_name=order_name))
            else:
                self._filename = f_d('{order_name}_acknowledgement_%d%m%y_%H%M%S.txt'.format(order_name=order_name))

    @return_ascii
    def upload_data(self):
        customer_setting = '%s/commercehub_settings/%s/acknowledgement.ini' % (self.current_dir, self.customer)
        data = ""
        config = configparser.ConfigParser()
        config.read(customer_setting)
        data += self.create_delimited_line(config['HeaderRecord'].items(), self.order)
        data += self.create_delimited_line(config['DetailRecord'].items(), self.order)
        data += self.create_delimited_line(config['TrailerRecord'].items(), self.order)

        return data


class CommercehubApiUpdateQTY(CommercehubApi):
    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(CommercehubApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory', self.get_root_dir(settings))
        self.confirmLines = lines
        self.filename = self.customer
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.partner = None
        if len(settings.get('multi_ftp_settings')) > 0:
            self.partner = settings.get('multi_ftp_settings')[0] and settings.get('multi_ftp_settings')[0].notification_for_customer or None

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, customer):
        if not self.raised_errors:
            if self._use_encrypt:
                self._filename = f_d('{customer}_inventory_%d%m%y_%H%M%S.pgp'.format(customer=customer))
            else:
                self._filename = f_d('{customer}_inventory_%d%m%y_%H%M%S.txt'.format(customer=customer))
            self.local_filename_for_not_encrypted_file = f_d(
                '{customer}_inventory_%d%m%y_%H%M_not_encrypted.txt'.format(customer=customer)
            )

    @property
    def full_filename(self):
        if not self.raised_errors:
            if self._use_encrypt:
                return join_path(self._path_to_backup_local_dir, self.local_filename_for_not_encrypted_file)
            else:
                return join_path(self._path_to_backup_local_dir, self.actual_local_filename)

    @full_filename.setter
    def full_filename(self, order_name):
        raise Exception('You can not set property full_filename!')

    @return_ascii
    def upload_data(self):
        data = []
        ###################################
        # prepare lines for params export #
        ###################################
        for line in self.confirmLines:
            # delete coma from description field for inventory feed
            description = line.get('description', None)
            if description:
                line['description'] = description.replace(',', ' ')

            line['deactivated'] = 'No'
            if (line.get('dnr_flag', None) in (1, '1') and line['qty'] in (0, '0')):
                line['deactivated'] = 'Yes'
            line['eta_date'] = ''
            line['eta_qty'] = ''

            if (line.get('eta', None) and line['qty'] in (0, '0')):
                try:
                    if int(line['eta'][-4:]) >= 1900:
                        line['eta_date'] = datetime.strptime(line['eta'], "%m/%d/%Y").strftime("%Y%m%d")
                        line['eta_qty'] = 1
                    else:
                        raise ValueError('Too old ETA')
                except ValueError:
                    _logger.error("""Wrong ETA date: {0}""".format(line['eta']))

            elif (line['qty'] in (0, '0') and self.customer == 'bestbuyca'):
                line['eta_date'] = (datetime.now().date() + timedelta(days=7 * 8)).strftime("%Y%m%d")
                line['eta_qty'] = 1
            line['available'] = 'Yes'

            if line['qty'] in (0, '0') and not line['eta_date']:
                line['available'] = 'No'

            if (self.customer in ['searscanada']):
                if line.get("merchant_department", False) and line.get("customer_sku", False):
                    line["customer_sku_with_dept"] = line["merchant_department"] + '_' + line["customer_sku"]
                    merchant_size = line.get('merchant_size', False)
                    if merchant_size:
                        line["customer_sku_with_dept"] += '_' + merchant_size
                else:
                    line["customer_sku_with_dept"] = line["customer_sku"]
            line['vendor_sku'] = None
            if line.get("upc", False):
                line['vendor_sku'] = ('0' * 13 + line['upc'])[-13:]
            line['manufacturer_sku'] = line.get("manufacturer_sku", '')

            # DLMR-1204 REMOVE LEADING ZERO's FROM UPC
            if self.customer in ['hbc'] and 'upc' in line and line.get('upc'):
                line['upc'] = str(int(line.get('upc', '')))

            line['discontinued_date'] = line.get("discontinued_date", '')

        # Get export file type from partner
        export_type = self.partner and self.partner.si_ftype_updateQTY or None

        ##############
        # xls export #
        ##############
        if export_type == 'xls':
            # parse ini config
            customer_setting = '%s/commercehub_settings/%s/inventory_xls.ini' % (self.current_dir, self.customer)
            config = configparser.ConfigParser()
            config.read(customer_setting)

            # customize data for customer
            customers_map = {
                'costco': costco.CommercehubApiUpdateQTYFileCostco,
                'walmartca': walmartca.CommercehubApiUpdateQTYFileWalmartca,
                'signetjewelers': signetjewelers.CommercehubApiUpdateQTYFileSignetJewelers,
            }
            if self.customer in customers_map:
                cApi = customers_map[self.customer]()
                self.confirmLines = cApi.prepare_xls_data(self)

            # create xls buffer
            xls = xlwt.Workbook()
            sheet = xls.add_sheet("inventory")
            text_rows_total = 0
            columns_total = 0

            # get sheet data
            if config.has_section('XLSSettings'):
                for cKey, cVal in config['XLSSettings'].items():
                    if cKey == 'rows_before_data':
                        text_rows_total = int(float(cVal)) or 0
                    if cKey == 'columns_total':
                        columns_total = int(cVal) or len(self.confirmLines) or 0

            # Begin static text writing
            row = 0
            for i in xrange(0, text_rows_total):
                row_sect = config.has_section("row_{}".format(i))
                if row_sect:
                    line_vars = {}
                    for rKey, rVal in config["row_{}".format(i)].items():
                        line_vars.update({rKey: str(rVal).strip('\'').strip('"')})
                    row = self.create_xls_line(sheet=sheet, row=row, columns_total=columns_total, datas=[line_vars])

            # Write data to xls
            for line in self.confirmLines:
                row = self.create_xls_data_line(sheet, row, columns_total, config['data'].items(), [line])

            # set filename
            self._filename = f_d('{}_inventory_%d%m%y_%H%M%S.xls'.format(self.customer))
            self.local_filename_for_not_encrypted_file = f_d(
                '{}_inventory_%d%m%y_%H%M_not_encrypted.xls'.format(self.customer))

            # create Bytes buffer and return
            buf = BytesIO()
            xls.save(buf)
            return buf

        #########################
        # csv export by default #
        #########################
        else:
            customer_setting = '%s/commercehub_settings/%s/inventory.ini' % (self.current_dir, self.customer)
            config = configparser.ConfigParser()
            config.read(customer_setting)
            for line in self.confirmLines:
                SkuRecord = self.create_delimited_line(config['SkuRecord'].items(), line, True, True)
                if SkuRecord:
                    data.append(SkuRecord)
                    if 'WarehouseRecord' in config.keys():
                        WarehouseRecord = self.create_delimited_line(config['WarehouseRecord'].items(), line, True,
                                                                     True)
                        data.append(WarehouseRecord)
                    self.append_to_revision_lines(line, 'good')
                else:
                    self.append_to_revision_lines(line, 'bad')
            # return comma-separated data
            return "".join(data)

        # return empty by default
        return ""


class CommercehubApiConfirmOrders(CommercehubApi):
    confirmLines = []
    name = "ConfirmOrders"

    def __init__(self, settings, lines):
        super(CommercehubApiConfirmOrders, self).__init__(settings)
        self.confirmLines = lines
        self.customer_variables = settings
        self.use_ftp_settings('confirm_shipment', self.get_root_dir(settings))
        self.filename = str(lines[0]['origin'])

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, origin):
        if not self.raised_errors:
            if self._use_encrypt:
                self._filename = f_d('{origin}_confirm_%d%m%y_%H%M%S.pgp'.format(origin=origin))
            else:
                self._filename = f_d('{origin}_confirm_%d%m%y_%H%M%S.txt'.format(origin=origin))

    def get_last_digit(self, upc):
        try:
            upc_odd_sum = 0
            upc_even_sum = 0
            for upc_index in range(len(upc)):
                if upc_index % 2 == 0:
                    upc_odd_sum += int(upc[upc_index])
                else:
                    upc_even_sum += int(upc[upc_index])
            last_digit = (upc_even_sum + (upc_odd_sum * 3)) % 10
            last_digit = 10 - last_digit
            last_digit = str(last_digit)[-1]
        except ValueError:
            return False
        return last_digit

    def _get_amount_total(self, lines):
        amounts_lines = [int(line['product_qty']) * float(line.get('unit_cost', 0.0)) for line in lines]
        return float(sum(amounts_lines))

    @return_ascii
    def upload_data(self):
        data = []
        if self.confirmLines:
            default_ship_values = {}
            if self.customer in ('costco'):
                default_ship_values['vendor_warehouse_id'] = self.customer_variables.get('vendor_warehouse_id', '')

            if self.confirmLines[0]['action'] == 'v_ship':
                if self.customer in ('kmart', 'sears'):
                    if 'sscc_id' in self.confirmLines[0] and self.confirmLines[0]['sscc_id']:
                        # This field will contain a value of ‘GM’ to indicate
                        # that an SSCC ID (Serial shipping Container Code)
                        # with application code prefix is used.
                        default_ship_values['container_label_type'] = 'GM'
                        # This field will contain the SSCC ID from GS1-128
                        # style container label (data length = 20 with
                        # application code).
                        default_ship_values['container_label_id'] = self.confirmLines[0]['sscc_id']
                    else:
                        default_ship_values['container_label_type'] = ''
                        default_ship_values['container_label_id'] = ''

                elif self.customer in ('kohls', 'bluestem'):
                    default_ship_values['vendor_warehouse'] = self.confirmLines[0].get('order_additional_fields',
                                                                                       {}).get('vendor_warehouse') or ''
            amount_total = self._get_amount_total(self.confirmLines)
            for line in self.confirmLines:
                from_zone = tz.gettz('UTC')
                to_zone = tz.gettz('EST')
                if (line.get('shp_date', False)):
                    dt = datetime.now()
                    utc = dt.replace(tzinfo=from_zone)
                    dt_est = utc.astimezone(to_zone)
                else:
                    dt = datetime.now()
                    utc = dt.replace(tzinfo=from_zone)
                    dt_est = datetime.now(to_zone)

                customer_setting = '%s/commercehub_settings/%s/confirm.ini' % (self.current_dir, self.customer)
                line['date'] = dt_est.strftime("%Y%m%d")
                line['int_product_qty'] = int(line['product_qty'])
                line['amount_total'] = "%.2f" % amount_total
                line['unit_cost'] = "%.2f" % float(line.get('unit_cost', 0.0))

                if not data:
                    config = configparser.ConfigParser()
                    config.read(customer_setting)
                    data.append(self.create_delimited_line(config['HeaderRecord'].items(), line))

                if (line['action'] == 'v_ship'):
                    if default_ship_values:
                        line.update(default_ship_values)

                    config.read(customer_setting)
                    data.append(self.create_delimited_line(config['MessageDetailShip'].items(), line))
                    data.append(self.create_delimited_line(config['PackageDetail'].items(), line))

                elif (line['action'] == 'v_cancel'):
                    if default_ship_values:
                        line.update(default_ship_values)

                    config.read(customer_setting)
                    data.append(self.create_delimited_line(config['MessageDetailCancel'].items(), line))

        return "".join(data)


class CommercehubApiInvoiceOrders(CommercehubApi):
    confirmLines = []
    name = "InvoiceOrders"

    def __init__(self, settings, lines):
        super(CommercehubApiInvoiceOrders, self).__init__(settings)
        self.confirmLines = lines
        self.use_ftp_settings('invoice_orders', self.get_root_dir(settings))
        self.filename = str(lines[0]['origin'])
        self.settings = settings

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, origin):
        if not self.raised_errors:
            if self._use_encrypt:
                self._filename = f_d('{origin}_invoice_%d%m%y_%H%M%S.pgp'.format(origin=origin))
            else:
                self._filename = f_d('{origin}_invoice_%d%m%y_%H%M%S.txt'.format(origin=origin))

    @return_ascii
    def upload_data(self):
        data = ""
        if self.settings['customer'] not in (
        'bedbath', 'bestbuyca', 'fmj', 'sears', 'snbc', 'walmartca', 'signetjewelers', 'hbc', 'bjs', 'macys'):
            data = []
            for line in self.confirmLines:

                from_zone = tz.gettz('UTC')
                to_zone = tz.gettz('EST')
                if (line['shp_date']):
                    dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                    utc = dt.replace(tzinfo=from_zone)
                    dt_est = utc.astimezone(to_zone)
                else:
                    dt = datetime.now()
                    utc = dt.replace(tzinfo=from_zone)
                    dt_est = datetime.now(to_zone)

                customer_setting = '%s/commercehub_settings/%s/invoice.ini' % (self.current_dir, self.customer)
                line['date'] = dt_est.strftime("%Y%m%d")
                line['int_product_qty'] = int(line['product_qty'])
                line['amount_total'] = "%.2f" % float(line["amount_total"])
                line['unit_cost'] = "%.2f" % float(line['unit_cost'])
                line['line_amount'] = "%.2f" % (float(line['unit_cost']) * int(line['product_qty']))
                line['gst'] = "%.2f" % float(line['gst'])
                line['pst'] = "%.2f" % float(line['pst'])
                line['hst'] = "%.2f" % float(line['hst'])
                line['qst'] = "%.2f" % float(line['qst'])
                if not data:
                    config = configparser.ConfigParser()
                    config.read(customer_setting)
                    data.append(self.create_delimited_line(config['HeaderRecord'].items(), line))
                if (line['action'] == 'v_ship'):
                    config.read(customer_setting)
                    data.append(self.create_delimited_line(config['DetailRecord'].items(), line))
            data = "".join(data)
        else:
            if self.settings['customer'] in ('walmartca', 'signetjewelers', 'hbc', 'bjs', 'macys'):
                parser = canada_xml_builder.CommercehubCreateInvoiceXmlMain()
            elif self.settings['customer'] in ('fmj', 'sears'):
                parser = other_xml_builder.CommercehubCreateInvoiceXmlMain()
            else:
                parser = main_xml_builder.CommercehubCreateInvoiceXmlMain()

            invoiceXml = parser.createXmlInvoice(self.confirmLines, self.customer, self.current_dir, self.settings)
            xml_parser = etree.XMLParser(remove_blank_text=True)
            elem = etree.XML(str(invoiceXml), parser=xml_parser)
            data = etree.tostring(elem)
        return data


class CommercehubApiReturnOrders(CommercehubApi):
    name = "ReturnOrders"
    lines = []

    def __init__(self, settings, lines):
        super(CommercehubApiReturnOrders, self).__init__(settings)
        self.lines = lines
        self.use_ftp_settings('return_orders', self.get_root_dir(settings))
        self.filename = str(lines[0]['po_number'])

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, origin):
        if not self.raised_errors:
            if self._use_encrypt:
                self._filename = f_d('{origin}_return_%d%m%y_%H%M%S.pgp'.format(origin=origin))
            else:
                self._filename = f_d('{origin}_return_%d%m%y_%H%M%S.txt'.format(origin=origin))

    @return_ascii
    def upload_data(self):
        data = []
        now_date = datetime.now().date().strftime("%Y%m%d")
        if self.lines:
            customer_setting = '%s/commercehub_settings/%s/return.ini' % (self.current_dir, self.customer)
            config = configparser.ConfigParser()
            config.read(customer_setting)
            self.lines[0]['date'] = now_date
            if self.customer in ('snbc', 'ppower'):
                adjustment_amount = 0
                for line in self.lines:
                    adjustment_amount += line['ItemPrice'] * int(line['product_uos_qty'])
                self.lines[0]['adjustment_amount'] = adjustment_amount * -1
                self.lines[0]['ret_inv_no'] = 'RET' + line['po_number']
            data.append(self.create_delimited_line(config['MessageHeader'].items(), self.lines[0]))
        for line in self.lines:
            if (not line['rma_number']):
                line['rma_number'] = "1234"
            if not line.get('return_reason', False):
                line['return_reason'] = 'other'
            line['date'] = now_date
            line['int_product_qty'] = int(line['product_uos_qty'])
            if self.customer in ('snbc', 'ppower'):
                line['line_unit_cost'] = line['ItemPrice'] * -1
                line['adjustment_amount'] = line['line_unit_cost'] * line['int_product_qty']

            data.append(self.create_delimited_line(config['MessageDetail'].items(), line))
        return "".join(data)
