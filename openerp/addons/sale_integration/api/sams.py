# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.sams_input_edi_parser import EDIParser, REF_MAP, REF_MAP_WA, SHIPMENT_METHOD_OF_PAYMENT_MAP
import random
from datetime import datetime, timedelta
from apiopenerp import ApiOpenerp
import logging
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from dateutil import tz

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '057932620'),
    ('vendor_name',                 'Vendor name',              'SAMS CLUB'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '925485USSM'),
    ('receiver_id_810',             'Receiver Id 810',          '925485US00'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    'ZZ'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '5010'),
    ('filename_format',             'Filename Format',          'DEL{edi_type}_{date}.txt'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('GLN',                         'Global Location Number',   '0605388000002'),
    ('repetition_separator',        'Repetition Separator',     '*'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      ':'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class SamsApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class SamsApiConfirmation(SamsApi):
    def __init__(self, settings):
        super(SamsApiConfirmation, self).__init__(settings)

    def _parse_carrier_code(self, carrier_code):
        carrier_code = carrier_code.strip()
        scac_code = ''
        service_level_code = ''
        if ' ' in carrier_code:
            carrier_parts = carrier_code.split(' ')
            scac_code = carrier_parts[0]
            service_level_code = carrier_parts[1]
        else:
            raise NotImplementedError('service_level_code is empty, please check outgoing code')

        if not scac_code:
            raise NotImplementedError('Carrier is empty')
        if len(scac_code) > 4:
            raise NotImplementedError('Max length for carrier is 4')
        return (scac_code, service_level_code)


class SamsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(SamsApiClient, self).__init__(
            settings_variables, SamsOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = SamsApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = SamsApiAcknowledgementOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmLoad(self, orders):
        api = SamsApiFunctionalAcknowledegment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        confirmApi = SamsApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            if lines[0]['ship2store']:
                invoiceApi = SamsApiInvoiceS2SOrders(self.settings, lines)
            else:
                invoiceApi = SamsApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = SamsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class SamsApiGetOrders(SamsApi):
    """EDI/V5010 X12/850: SamsClub.com DSV Purchase Orders"""

    def __init__(self, settings):
        super(SamsApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'



class SamsApiAcknowledgementOrders(SamsApi):
    """EDI/V5010 X12/855: 855 Purchase Order Acknowledgement"""

    def __init__(self, settings, lines, state):
        super(SamsApiAcknowledgementOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel'):
            return False

        self.edi_type = '855'

        statuscode = 'AC'
        if (self.state == 'accepted'):
            # PO1/ACK segments are not required when using this code.
            # This code indicates to Bealls that you accept all items.
            statuscode = 'AC'
        elif (self.state == 'accepted_with_details'):
            # PO1/ACK segment usage required when using this code.
            # All ACK01 values must contain IA.
            statuscode = 'AD'

        segments = []
        api_customer = self.processingLines.get('new_type_api_id', False)
        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s*[5]*[6]*[7]*[8]*[9]%(ack_date)s'

        now_date = datetime.utcnow().strftime('%Y%m%d')
        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = now_date

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
            'ack_date': now_date,  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        ref_codes = [
            'DP',  # 5-digit Walmart Department Number.
            'MR',  # Merchandise Type Code
            'IA',  # Internal Vendor Number
            'CO',  # Customer Order Number
            'AN',  # Associated Purchase Orders
        ]

        additional_fields = self.processingLines['order_additional_fields']

        for ref_code in ref_codes:
            ref_value = REF_MAP.get(ref_code, ref_code)
            if api_customer == 'Walmart Fulfilled (FBW)':
                ref_value = REF_MAP_WA.get(ref_code, ref_code)
            if ref_value in additional_fields:
                segments.append(self.insertToStr(ref, {
                    'code': ref_code,
                    'order_number': additional_fields[ref_value],
                }))

        # PID Product / item descriptiojn

        # no n9 in walmart
        if api_customer != 'Walmart Fulfilled (FBW)':
            n9_zz = 'N9*[1]ZZ*[2]%(country_code)s'
            segments.append(self.insertToStr(n9_zz, {
                'country_code': self.processingLines['ship_to_address']['country'],
            }))

            sams_brand_code = '4U'
            n9_4u = 'N9*[1]%(sams_brand_code)s*[2]%(sams_brand_name)s'
            ref_value = REF_MAP.get(sams_brand_code, sams_brand_code)
            segments.append(self.insertToStr(n9_4u, {
                'sams_brand_code': sams_brand_code,
                'sams_brand_name': additional_fields[ref_value],
            }))

        n1_by = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Buying Party (Purchaser)
        n1_code = "BY"
        if api_customer == 'Walmart Fulfilled (FBW)':
            n1_code = 'ST'
        identification_code = self.processingLines['ship_to_address']['identification_code']
        segments.append(self.insertToStr(n1_by, {
            'code': n1_code,
            'name': self.processingLines['ship_to_address']['name'],
            'identification_code_qualifier': identification_code and 'UL' or '',  # Global Location Number (GLN)
            'identification_code': identification_code or '',
        }))

        #NI BT - not in example
        n1_su = 'N1*[1]SU*[2]%(name)s'  # Supplier

        
        segments.append(self.insertToStr(n1_su, {
            'name': 'DELMAR MANUFACTURING',
        }))

        po1 = 'PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(customer_sku_qualifier)s*[7]%(customer_sku)s*[8]%(upc_qualifier)s*[9]%(upc)s*[10]%(vendorSku_qualifier)s*[11]%(vendorSku)s*[12]%(merchantSku_qualifier)s*[13]%(merchantSku)s'

        i = 0
        for line in self.processingLines['order_lines']:
            i += 1
            qty = line['product_qty']
            template = po1
            customer_sku = line['customer_sku'] or ''
            customer_sku_qualifier = customer_sku and 'IN' or ''
            upc = line['upc'] or ''
            upc_qualifier = upc and 'UP' or ''
            merchantSku = line['merchantSku'] or ''
            merchantSku_qualifier = merchantSku and 'UK' or ''
            vendorSku = line['vendorSku'] or ''
            vendorSku_qualifier = vendorSku and 'VN' or ''
            insert_obj = {
                'line_number': str(i),
                'qty': qty,
                'unit_price': line['price_unit'],
                'customer_sku_qualifier': customer_sku_qualifier,
                'customer_sku': customer_sku,
                'upc_qualifier': upc_qualifier,
                'upc': upc,
                'vendorSku_qualifier': vendorSku_qualifier,
                'vendorSku': vendorSku,
                'merchantSku_qualifier': merchantSku_qualifier,
                'merchantSku': merchantSku,
            }

            segments.append(self.insertToStr(template, insert_obj))
            
            # SDQ -destination quantity
            ack = 'ACK*[1]IA*[2]%(qty)s*[3]EA'
            segments.append(self.insertToStr(ack, {
                'qty': qty,
            }))

            dtm = 'DTM*[1]%(code)s*[2]%(date)s'
            segments.append(self.insertToStr(dtm, {
                'code': '068',
                'date': now_date,
            }))

        ctt = 'CTT*[1]%(count_lines)s'
        segments.append(self.insertToStr(ctt, {
            'count_lines': i,
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})


class SamsApiFunctionalAcknowledegment(SamsApi):

    orders = []

    def __init__(self, settings, orders):
        super(SamsApiFunctionalAcknowledegment, self).__init__(settings)
        self.use_ftp_settings('functional_acknowledgement')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            # TODO: need to fix it, wrote number_of_transaction only to latest order in parser
            if order['ack_control_number'] not in numbers and 'number_of_transaction' in order:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))

                ak1 = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak1, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak5 = 'AK5*[1]%(acknowledgement_code)s*[2]%(error_code)s*[3]%(error_code)s'
                segments.append(self.insertToStr(ak5, {
                    # A - Accepted
                    # E - Accepted But Errors Were Noted
                    # M - Rejected, Message Authentication Code (MAC) Failed
                    # R - Rejected
                    # W - Rejected, Assurance Failed Validity Tests
                    # X - Rejected, Content After Decryption Could Not Be Analyzed
                    'acknowledgement_code': 'A',
                    # 1 - Transaction Set Not Supported
                    # 2 - Transaction Set Trailer Missing
                    # 3 - Transaction Set Control Number in Header and Trailer Do Not Match
                    # 4 - Number of Included Segments Does Not Match Actual Count
                    # 5 - One or More Segments in Error
                    # 6 - Missing or Invalid Transaction Set Identifier
                    # 7 - Missing or Invalid Transaction Set Control Number
                    # 8 - Authentication Key Name Unknown
                    # 9 - Encryption Key Name Unknown
                    # 10 - Requested Service (Authentication or Encrypted) Not Available
                    # 11 - Unknown Security Recipient
                    # 12 - Incorrect Message Length (Encryption Only)
                    # 13 - Message Authentication Code Failed
                    # 15 - Unknown Security Originator
                    # 16 - Syntax Error in Decrypted Text
                    # 17 - Security Not Supported
                    # 18 - Transaction Set not in Functional Group
                    # 19 - Invalid Transaction Set Implementation Convention Reference
                    # 23 - Transaction Set Control Number Not Unique within the Functional Group
                    # 24 - S3E Security End Segment Missing for S3S Security Start Segment
                    # 25 - S3S Security Start Segment Missing for S3E Security End Segment
                    # 26 - S4E Security End Segment Missing for S4S Security Start Segment
                    # 27 - S4S Security Start Segment Missing for S4E Security End Segment
                    'error_code': '',
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class SamsApiConfirmShipment(SamsApiConfirmation):
    """EDI/V5010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(SamsApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):

        api_customer = self.confirmLines[0]['api_customer']
        order_additional_fields = self.confirmLines[0]['order_additional_fields']
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S')  # Self-testing says: DTM segment, element 03, Time field must be present and must be in HHMMSS format
        if len(shp_time) > 6:
            shp_time = shp_time[:6]

        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        # PATCH FOR DLMR-2074
        ref_map_dict = REF_MAP
        if api_customer == 'Walmart Fulfilled (FBW)':
            ref_map_dict = REF_MAP_WA
        # END PATCH

        hl_number = 1
        hl_number_prev = ''
        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'
        if api_customer == 'Walmart Fulfilled (FBW)':
            hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s*[4]1'
        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))
        # if api_customer == 'Walmart Fulfilled (FBW)':
        #     td1 = 'TD1*[1]%(packaging_code)s*[2]%(lading_quantity)s*[3]*[4]*[5]*[6]G*[7]%(weight)s*[8]LB*[9]%(volume)s*[10]CF'
            
        #     segments.append(self.insertToStr(td1, {
        #         'packaging_code': 'CTL', #options are AAA (pallet, returnable), CTL (carton), SLP(slip sheet)
        #         'lading_quantity': 1, #self.confirmLines[0]['product_qty'], # number of units (pieced) in lading commodidity
        #         'weight': self.confirmLines[0]['weight'],
        #         'volume': self.confrimLines[0]['volume'],
        #     }))

        td5 = 'TD5*[1]B*[2]2*[3]%(carrier)s*[4]*[5]*[6]*[7]*[8]*[9]*[10]*[11]*[12]%(service_level_code)s'
        if api_customer == 'Walmart Fulfilled (FBW)':
            td5 = 'TD5*[1]*[2]2*[3]%(carrier)s*[4]%(service_level_code)s'
        if self.confirmLines[0]['customer_id'] == 'fbw':
            scac_code = self.confirmLines[0]['carrier_code']
            service_level_code = ''
        else:
            (scac_code, service_level_code) = self._parse_carrier_code(self.confirmLines[0]['carrier_code'])

        td5_data = {
            'carrier': scac_code,
            'service_level_code': service_level_code or 'U',
        }
        segments.append(self.insertToStr(td5, td5_data))
        if api_customer == 'Walmart Fulfilled (FBW)':
            td3 = 'TD3*[1]%(name)s*[2]%(initials)s*[3]%(number)s*[4]*[5]*[6]*[7]*[8]*[9]%(seal_number)s'
            # segments.append(self.insertToStr(td3,{
            #     'name': 'TL',
            #     'initials': 'ABCD',
            #     'number': '07213567',
            #     'seal_number': '30394938483234'
            # }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Bill of Lading Number
        #     'data': 'NEED_DATA'
        # }))
        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Master Bill of Lading (Common Carrier shipments only)
        #     'data': 'NEED_DATA'
        # }))
        #walmart has AO - Appointment number, CN - carrier refernce number, CR - customer number,
        # and UCB -Vics Bill of lading number
        if api_customer == 'Walmart Fulfilled (FBW)':
            ref_codes = [
                'AO', # Appointment Number
                'CN', # Carrier's Reference Number (PRO/Invoice)
                'CR', # Customer Reference Number
                'UCB', # VICS Bill of Lading Number (17 Digits)
            ]
            for ref_code in ref_codes:
                ref_value = REF_MAP_WA.get(ref_code, ref_code)
                order_number = self.confirmLines[0]['order_additional_fields'].get(ref_value, '')
                if order_number:
                    segments.append(self.insertToStr(ref, {
                        'code': ref_code,
                        'order_number': order_number,
                    }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
            'time': shp_time,
        }))

        if api_customer == 'Walmart Fulfilled (FBW)':
            fob = 'FOB*%(shipment_method_of_payment)s'
            shipment_method_of_payment_code = ''
            shipment_method_of_payment_name = order_additional_fields['shipment_method_of_payment']
            for code, name in SHIPMENT_METHOD_OF_PAYMENT_MAP.iteritems():
                if name == shipment_method_of_payment_name:
                    shipment_method_of_payment_code = code
            segments.append(self.insertToStr(fob, {
                'shipment_method_of_payment': shipment_method_of_payment_code or 'CC',
            }))

        n1_by = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Buying Party (Purchaser)
        n1_code = "BY"
        if api_customer == 'Walmart Fulfilled (FBW)':
            n1_code = 'ST'
        identification_code = self.confirmLines[0]['remit_to_san_address']
        segments.append(self.insertToStr(n1_by, {
            'code': n1_code,
            'name': self.confirmLines[0]['remit_to_name_1'],
            'identification_code_qualifier': identification_code and 'UL' or '',  # Global Location Number (GLN)
            'identification_code': identification_code or '',
        }))

        n1_sf = 'N1*[1]SF*[2]%(name)s'  # Supplier

        segments.append(self.insertToStr(n1_sf, {
            'name': 'DELMAR MANUFACTURING',
        }))

        if api_customer == 'Walmart Fulfilled (FBW)':
            n3_st = 'N3*[1]%(address1)s'
            segments.append(self.insertToStr(n3_st, {
                'address1': self.confirmLines[0]['remit_to_address_line_1']
            }))

            n4_st = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'
            segments.append(self.insertToStr(n4_st, {
                'city': self.confirmLines[0]['remit_to_city'],
                'state': self.confirmLines[0]['remit_to_state'],
                'postal_code': self.confirmLines[0]['remit_to_postal_code'],
                'country': self.confirmLines[0]['remit_to_country'],
            }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))
        

        prf = 'PRF*[1]%(po_number)s'
        if api_customer == 'Walmart Fulfilled (FBW)':
            prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]%(date)s'
            segments.append(self.insertToStr(prf, {
                'po_number': self.confirmLines[0]['po_number'],
                'date': self.confirmLines[0]['external_date_order'].replace("-", "")
            }))
        else:
            segments.append(self.insertToStr(prf, {
                'po_number': self.confirmLines[0]['po_number']
            }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        ref_codes = [
            'IA',  # Internal Vendor Number
            'CO',  # Customer Order Number.
            'AN',  # Associated Purchase Orders
        ]
        ref_map = REF_MAP
        if api_customer == 'Walmart Fulfilled (FBW)':
            ref_codes = [
                'IA', # Internal Vendor Number
                'IV', # Seller's Invoice number
            ]

        for ref_code in ref_codes:
            ref_value = ref_map_dict.get(ref_code, ref_code)
            order_number = self.confirmLines[0]['order_additional_fields'].get(ref_value, '')
            if order_number:
                segments.append(self.insertToStr(ref, {
                    'code': ref_code,
                    'order_number': order_number,
                }))
        if api_customer == 'Walmart Fulfilled (FBW)':
            n1_by = 'N1*[1]BY*[2]*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Buying Party (Purchaser)
            identification_code = self.confirmLines[0]['remit_to_san_address']
            segments.append(self.insertToStr(n1_by, {
                'code': n1_code,
                'identification_code_qualifier': identification_code and 'UL' or '',  # Global Location Number (GLN)
                'identification_code': identification_code or '',
            }))
        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number), # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'P'  # Pack
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        lin = 'LIN*[1]*[2]%(customer_sku_qualifier)s*[3]%(customer_sku)s*[4]%(upc_qualifier)s*[5]%(upc)s*[6]%(vendorSku_qualifier)s*[7]%(vendorSku)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'
        pid = 'PID*[1]%(description_type)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(data)s'
        
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            if api_customer == 'Walmart Fulfilled (FBW)':
                hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s*[4]0'
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item (code for walmart fulfilled and sams club is S,O,P,I)
            }))

            customer_sku = line['additional_fields'].get('customer_sku', '') or line['customer_sku'] or ''
            customer_sku_qualifier = customer_sku and 'IN' or ''
            upc = line['additional_fields'].get('upc', '') or ''
            upc_qualifier = upc and 'UP' or ''
            vendorSku = line['vendorSku'] or ''
            vendorSku_qualifier = vendorSku and 'VN' or ''

            segments.append(self.insertToStr(lin, {
                'customer_sku': customer_sku,
                'customer_sku_qualifier': customer_sku_qualifier,
                'upc': upc,
                'upc_qualifier': upc_qualifier,
                'vendorSku': vendorSku,
                'vendorSku_qualifier': vendorSku_qualifier,
            }))
            segments.append(self.insertToStr(sn1, {
                'qty': int(line['product_qty'])
            }))
            if api_customer == 'Sams Club':
                segments.append(self.insertToStr(pid, {
                    'description_type': 'F',  # Free-form
                    'characteristic_code': '08',
                    # 08 Product
                    # 75 Buyer's Color Description
                    # 91 Buyer's Item Size Description
                    'data': line['name'],
                }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))
        
        return self.wrap(segments, {'group': 'SH'})

class SamsApiInvoiceS2SOrders(SamsApiConfirmation):
    """EDI/V5010 X12/810: 810 Invoice -  invoices for orders that will be shipped to store"""

    def __init__(self, settings, lines):
        super(SamsApiInvoiceS2SOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        api_customer = self.invoiceLines[0]['api_customer']
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        order_additional_fields = self.invoiceLines[0]['order_additional_fields']
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        ref_codes = [
            'IA',  # Internal Vendor Number
            'DP',  # 5-digit Walmart Department Number.
            'MR',  # Merchandise Type Code
        ]
        for ref_code in ref_codes:
            ref_value = REF_MAP.get(ref_code, ref_code)
            segments.append(self.insertToStr(ref, {
                'code': ref_code,
                'order_number': order_additional_fields[ref_value],
            }))

        n1_su = 'N1*[1]SU*[2]%(name)s*[3]UL*[4]%(gln)s'  # Supplier
        segments.append(self.insertToStr(n1_su, {
            'name': 'DELMAR MANUFACTURING',
            'gln': self.GLN,
        }))

        n1_st = 'N1*[1]ST*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Buying Party (Purchaser)
        identification_code = self.invoiceLines[0]['remit_to_san_address']
        segments.append(self.insertToStr(n1_st, {
            'name': self.invoiceLines[0]['remit_to_name_1'],
            'identification_code_qualifier': identification_code and 'UL' or '',  # Global Location Number (GLN)
            'identification_code': identification_code or '',
        }))

        n3_st = 'N3*[1]%(address1)s'
        segments.append(self.insertToStr(n3_st, {
            'address1': self.invoiceLines[0]['remit_to_address_line_1']
        }))

        n4_st = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s'
        n4_dict = {
            'city': self.invoiceLines[0]['remit_to_city'],
            'state': self.invoiceLines[0]['remit_to_state'],
            'postal_code': self.invoiceLines[0]['remit_to_postal_code'],
        }
        if api_customer == 'Walmart Fulfilled (FBW)':
            n4_st = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'
            n4_dict['country'] = self.invoiceLines[0]['remit_to_country']
        
        segments.append(self.insertToStr(n4_st, n4_dict))
        terms_net_days = 5  # C&P value from bealls
        terms_net_due_date = ''
        if (external_date_order):
            terms_net_due_date = external_date_order + timedelta(days=terms_net_days)
        else:
            terms_net_due_date = datetime.utcnow() + timedelta(days=terms_net_days)
        terms_net_due_date = terms_net_due_date.strftime('%Y%m%d')
        itd = 'ITD*[1]%(code)s*[2]%(terms_basis_date_code)s*[3]*[4]*[5]*[6]*[7]%(terms_net_days)s'
        segments.append(self.insertToStr(itd, {
            'code': '05',  # Discount Not Applicable
            # 08 - Basic Discount Offered
            # 12 - 10 Days After End of Month (10 EOM)
            # 14 - Previously agreed upon
            'terms_basis_date_code': 3,
            # 3 Invoice Date
            'terms_net_days': str(terms_net_days),
        }))

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        fob = 'FOB*%(shipment_method_of_payment)s'
        shipment_method_of_payment_code = ''
        shipment_method_of_payment_name = order_additional_fields['shipment_method_of_payment']
        for code, name in SHIPMENT_METHOD_OF_PAYMENT_MAP.iteritems():
            if name == shipment_method_of_payment_name:
                shipment_method_of_payment_code = code
        segments.append(self.insertToStr(fob, {
            'shipment_method_of_payment': shipment_method_of_payment_code or 'CC',
        }))

        it1 = 'IT1*[1]*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(customer_sku_qualifier)s*[7]%(customer_sku)s*[8]%(upc_qualifier)s*[9]%(upc)s'
        pid = 'PID*[1]%(item_discription_type)s*[2]%(item_characteristic_code)s*[3]%(agency_qualifier_code)s*[4]%(product_description_code)s*[5]%(data)s'
        #PO4 - item physical details (Required if IT103 = 'CA' DO NOT send if IT103 = 'EA')
        # sac - sevice, promotion, allowance or charge information
        if api_customer == 'Sams Club':
            it1 += '*[10]%(merchantSku_qualifier)s*[11]%(merchantSku)s'

        line_number = 0
        total_qty_invoiced = 0
        calculated_amount = 0.0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            customer_sku = line['additional_fields'].get('customer_sku', '') or ''
            customer_sku_qualifier = customer_sku and 'IN' or ''
            upc = line['additional_fields'].get('upc', '') or ''
            upc_qualifier = upc and 'UP' or ''
            merchantSku = line['merchantSKU'] or ''
            merchantSku_qualifier = merchantSku and 'UK' or ''
            it1_data = {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['price_unit'],
                'customer_sku_qualifier': customer_sku_qualifier,
                'customer_sku': customer_sku,
                'upc_qualifier': upc_qualifier,
                'upc': upc,
            }

            if api_customer == 'Sams Club':
                it1_data['merchantSku_qualifier'] = merchantSku_qualifier
                it1_data['merchantSku'] = merchantSku
                
            segments.append(self.insertToStr(it1, it1_data))

            pid_field_names = {
                'item_discription_type',
                'item_characteristic_code',
                'agency_qualifier_code',
                'product_description_code',
            }
            pid_data = {name: order_additional_fields.get(name, '') for name in pid_field_names}
            pid_data.update({'data': line['name']})
            if api_customer == 'Walmart Fulfilled (FBW)':
                pid_data['item_characteristic_code'] = ''
                if pid_data['item_discription_type'] == '' or pid_data['agency_qualifier_code'] == '':
                    pid_data['item_discription_type'] = 'F'
                    pid_data['agency_qualifier_code'] = ''
            else:
                segments.append(self.insertToStr(pid, pid_data))

            total_qty_invoiced += qty
            calculated_amount += qty * float(line['price_unit'])
        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(calculated_amount)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))
        #txi Tax information

        cad = 'CAD*[1]T*[2]*[3]*[4]%(carrier)s*[5]%(carrier_code)s*[6]*[7]%(reference_identification_qualifier)s*[8]%(reference_identification)s'

        if self.invoiceLines[0]['customer_id'] == 'fbw':
            carrier = self.invoiceLines[0]['carrier_code']
        else:
            (carrier, service_level_code) = self._parse_carrier_code(self.invoiceLines[0]['carrier_code'])

        segments.append(self.insertToStr(cad, {
            'carrier': carrier,
            'carrier_code': self.invoiceLines[0]['carrier_code'],
            # 2I - Tracking Number
            # BM - Bill of Lading Number
            # CN - Carrier's Reference Number (PRO/Invoice)
            'reference_identification_qualifier': '2I',
            'reference_identification': self.invoiceLines[0]['tracking_number'],
        }))

        # SAC Service, Promotion, Allowance, or Charge information

        # INvoice shipment summary
        iss = 'ISS*[1]%(total_qty_invoiced)s*[2]EA*[3]%(weight)s*[4]%(weight_uom)s'
        weight = self.invoiceLines[0]['weight'] or ''
        segments.append(self.insertToStr(iss, {
            'total_qty_invoiced': total_qty_invoiced,
            'weight_uom': weight and 'KG' or '',  # Kilogram
            'weight': weight,
        }))

        ctt = 'CTT*[1]%(items_count)s'
        # transaction totals
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'IN'})


class SamsApiInvoiceOrders(SamsApiConfirmation):
    """EDI/V5010 X12/810: 810 Invoice -  orders shipped to customer"""

    def __init__(self, settings, lines):
        super(SamsApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        api_customer = self.invoiceLines[0]['api_customer']
        segments = []

        st = 'ST*[1]810*[2]%(st_number)s'
        # transaction set header
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        #Biginner segment for invoice
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        order_additional_fields = self.invoiceLines[0]['order_additional_fields']
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        # refrernce information
        ref_codes = [
            'IA',  # Internal Vendor Number
            'DP',  # 5-digit Walmart Department Number.
            'MR',  # Merchandise Type Code
        ]
        for ref_code in ref_codes:
            ref_value = REF_MAP.get(ref_code, ref_code)
            segments.append(self.insertToStr(ref, {
                'code': ref_code,
                'order_number': order_additional_fields[ref_value],
            }))

        n1_su = 'N1*[1]SU*[2]%(name)s*[3]UL*[4]%(gln)s'  # Supplier
        segments.append(self.insertToStr(n1_su, {
            'name': 'DELMAR MANUFACTURING',
            'gln': self.GLN,
        }))

        n1_st = 'N1*[1]ST*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Buying Party (Purchaser)
        identification_code = self.invoiceLines[0]['remit_to_san_address']
        segments.append(self.insertToStr(n1_st, {
            'name': self.invoiceLines[0]['remit_to_name_1'],
            'identification_code_qualifier': identification_code and 'UL' or '',  # Global Location Number (GLN)
            'identification_code': identification_code or '',
        }))

        n3_st = 'N3*[1]%(address1)s'
        # party location
        segments.append(self.insertToStr(n3_st, {
            'address1': self.invoiceLines[0]['remit_to_address_line_1']
        }))

        n4_st = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s'
        # geographic location
        n4_dict = {
            'city': self.invoiceLines[0]['remit_to_city'],
            'state': self.invoiceLines[0]['remit_to_state'],
            'postal_code': self.invoiceLines[0]['remit_to_postal_code'],
        }
        if api_customer == 'Walmart Fulfilled (FBW)':
            n4_st = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'
            n4_dict['country'] = self.invoiceLines[0]['remit_to_country']
        
        segments.append(self.insertToStr(n4_st, n4_dict))

        # ITD Terms of Sale/Deferred Terms of Sale
        terms_net_days = 5  # C&P value from bealls
        terms_net_due_date = ''
        if (external_date_order):
            terms_net_due_date = external_date_order + timedelta(days=terms_net_days)
        else:
            terms_net_due_date = datetime.utcnow() + timedelta(days=terms_net_days)
        terms_net_due_date = terms_net_due_date.strftime('%Y%m%d')
        itd = 'ITD*[1]%(code)s*[2]%(terms_basis_date_code)s*[3]*[4]*[5]*[6]*[7]%(terms_net_days)s'
        segments.append(self.insertToStr(itd, {
            'code': '05',  # Discount Not Applicable
            # 02 - End of Month (EOM)
            # 08 - Basic Discount Offered
            # 12 - 10 Days After End of Month (10 EOM)
            # 14 - Previously agreed upon
            'terms_basis_date_code': 3,
            # 1 - ship date
            # 3 Invoice Date
            # 7 - effective date
            # 15 receipt of goods
            'terms_net_days': str(terms_net_days),
        }))

        # date time reference
        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 007 - effctive
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        fob = 'FOB*%(shipment_method_of_payment)s'
        # F.O.B. Related instructions
        shipment_method_of_payment_code = ''
        shipment_method_of_payment_name = order_additional_fields['shipment_method_of_payment']
        for code, name in SHIPMENT_METHOD_OF_PAYMENT_MAP.iteritems():
            if name == shipment_method_of_payment_name:
                shipment_method_of_payment_code = code
        segments.append(self.insertToStr(fob, {
            'shipment_method_of_payment': shipment_method_of_payment_code or 'CC',
        }))

        # base line item data (invoice)
        it1 = 'IT1*[1]*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(customer_sku_qualifier)s*[7]%(customer_sku)s*[8]%(upc_qualifier)s*[9]%(upc)s'
        if api_customer == 'Sams Club':
            it1 += '*[10]%(merchantSku_qualifier)s*[11]%(merchantSku)s'
        
        line_number = 0
        total_qty_invoiced = 0
        calculated_amount = 0.0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            customer_sku = line['additional_fields'].get('customer_sku', '') or ''
            customer_sku_qualifier = customer_sku and 'IN' or ''
            upc = line['additional_fields'].get('upc', '') or ''
            upc_qualifier = upc and 'UP' or ''
            merchantSku = line['merchantSKU'] or ''
            merchantSku_qualifier = merchantSku and 'UK' or ''
            it1_data = {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['price_unit'],
                'customer_sku_qualifier': customer_sku_qualifier,
                'customer_sku': customer_sku,
                'upc_qualifier': upc_qualifier,
                'upc': upc,
            }
            if api_customer == 'Sams Club':
                it1_data['merchantSku_qualifier'] = merchantSku_qualifier
                it1_data['merchantSku'] = merchantSku

            segments.append(self.insertToStr(it1, it1_data))

            total_qty_invoiced += qty
            calculated_amount += qty * float(line['price_unit'])
        # PID - Porudct/Item Description (exists in method SamsApiInvoiceS2SOrders)
        #PO4 - item physical details (Required if IT103 = 'CA' DO NOT send if IT103 = 'EA')
        # sac - sevice, promotion, allowance or charge information


        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(calculated_amount)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        #txi - Tax information (seems like we need it)

        #CAD is required for all backhaul shipments and is recommended for all shipments
        cad = 'CAD*[1]T*[2]*[3]*[4]%(carrier)s*[5]%(carrier_code)s*[6]*[7]%(reference_identification_qualifier)s*[8]%(reference_identification)s'

        if self.invoiceLines[0]['customer_id'] == 'fbw':
            carrier = self.invoiceLines[0]['carrier_code']
        else:
            (carrier, service_level_code) = self._parse_carrier_code(self.invoiceLines[0]['carrier_code'])

        #2I is not in the walmart doc. says CN is required for all backhaul orders
        segments.append(self.insertToStr(cad, {
            'carrier': carrier,
            'carrier_code': self.invoiceLines[0]['carrier_code'],
            # 2I - Tracking Number
            # BM - Bill of Lading Number
            # CN - Carrier's Reference Number (PRO/Invoice)
            'reference_identification_qualifier': '2I',
            'reference_identification': self.invoiceLines[0]['tracking_number'],
        }))

        # SAC - Sevice, Promotion, Allowance, or Charge information

        iss = 'ISS*[1]%(total_qty_invoiced)s*[2]EA*[3]%(weight)s*[4]%(weight_uom)s'
        weight = self.invoiceLines[0]['weight'] or ''
        segments.append(self.insertToStr(iss, {
            'total_qty_invoiced': total_qty_invoiced,
            'weight_uom': weight and 'KG' or '',  # Kilogram
            'weight': weight,
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'IN'})



class SamsApiUpdateQTY(SamsApi):
    """EDI/V5010 X12/846: SamsClub.com DSV \"On Hand\" Inventory & Adjustments"""

    def __init__(self, settings, lines):
        super(SamsApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]%(purpose_code)s*[2]%(report_type_code)s*[3]%(inventory_feed_qualifier)s*[4]%(date)s*[5]%(time)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))
        segments.append(self.insertToStr(bia, {
            'purpose_code': '00',  # Original
            'report_type_code': 'SI',  # Seller Inventory Report
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'date': date.strftime('%Y%m%d'),
            'time': date.strftime('%H%M'),
        }))

        ref = 'REF*[1]%(reference_identification_qualifier)s*[2]%(reference_identification)s'
        segments.append(self.insertToStr(ref, {
            'reference_identification_qualifier': 'IA',  # Internal Vendor Number
            'reference_identification': self.vendor_number,
        }))

        n1 = 'N1*[1]%(entity_identifier_code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'
        segments.append(self.insertToStr(n1, {
            'entity_identifier_code': 'TO',  # Message To
            'name': self.vendor_name,
            'identification_code_qualifier': 'UL',  # Global Location Number (GLN)
            'identification_code': self.GLN,
        }))

        n1_su = 'N1*[1]%(entity_identifier_code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'  # Supplier
        segments.append(self.insertToStr(n1_su, {
            'entity_identifier_code': 'SU',
            'name': self.sender_id,
            # Assigned by Seller or Seller's Agent
            'identification_code_qualifier': '91',
            # This field will contain the Vendor's "Ship Node" Identification Code.
            # This field is required by Wal-Mart Stores, Inc.
            'identification_code': '01',
        }))

        n4 = 'N4*[1]*[2]*[3]*[4]%(country)s'
        segments.append(self.insertToStr(n4, {
            'country': 'CA',
        }))

        lin = 'LIN*[1]%(iter)s*[2]%(product_qualifier)s*[3]%(product_identifying_number)s*[4]%(upc_qualifier)s*[5]%(upc)s*[6]%(vendor_qualifier)s*[7]%(vendor_sku)s'
        ref_prt = 'REF*[1]%(product_type_identificator)s*[2]%(product_type)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        i = 0
        illegal_values = [None, 'None', False, 'False']
        for line in self.updateLines:
            if line['upc'] not in illegal_values:
                i += 1
                vendor_sku = line.get('customer_id_delmar') or ''
                vendor_qualifier = vendor_sku and 'VN' or ''
                # FIXME: Very strange magic. e have this value in Customer SKU. But Erel....
                # Buyer's Item Number
                # SamsClub 9-digit assigned Item Number.
                # This element is required by Wal-Mart Stores, Inc.
                # item_no = len(line['upc']) > 6 and line['upc'][-7:-1] or line['upc']
                # item_no = item_no and '63' + item_no + '9'
                # http://redmine.pfrus.com/attachments/download/9157/846%20Information.docx
                item_no = line.get('customer_sku') or ''
                segments.append(self.insertToStr(lin, {
                    'product_qualifier': 'IN',  # Buyer's Item Number
                    'product_identifying_number': item_no,
                    'upc_qualifier': 'UP',  # UCC – 12
                    'upc': line['upc'],
                    'vendor_qualifier': vendor_qualifier,  # Vendor's (Seller's) Item Number
                    'vendor_sku': vendor_sku,
                    'iter': str(i).rjust(6, '0'),
                }))

                segments.append(self.insertToStr(ref_prt, {
                    'product_type_identificator': 'PRT',
                    'product_type': 'MAINSITE',
                }))

                segments.append(self.insertToStr(qty, {
                    # 17 - Quantity on Hand
                    # A5 - Adjusted Quantity
                    'code': '17',
                    'qty': str(line['qty']),  # Product
                    'uof_code': 'EA'  # Each
                }))

                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class SamsOpenerp(ApiOpenerp):

    def __init__(self):

        super(SamsOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def generate_duplicate_xml_email(self, cr, uid, xml_name, duplicate_xml_ids):
        return False

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        #line['cut_sku'] = line['customer_sku'][2:-1] if line['customer_sku'] and len(line['customer_sku']) > 3 else False
        line['cut_sku'] = line['customer_sku']
        line_obj = {
            "notes": "",
            "name": line['name'],
            'sku': line['sku'],
            'customer_sku': line['customer_sku'],
            # Stupid logic, need to remove prefix (usually 62 or 63) and suffix (9)
            'cut_sku': line['cut_sku'],
            'upc': line['upc'],
            'vendorSku': line['vendor_number'],
            'cost': line['cost'],
            'customerCost': line.get('retail_cost', False),
            'qty': line['qty'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}
        field_list = ['sku', 'customer_sku', 'upc', 'vendor_number']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field], search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
            # # DELMAR-186
            # if line_obj["name"] == None or line_obj["name"] == "empty product description in order":
            #     line_obj["name"] = product.name
            # # END DELMAR-186
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        order_additional_fields = {x.name: x.value for x in sale_order.additional_fields or []}
        result = {
            'po_number': sale_order.po_number,
            'date': date.strftime('%Y%m%d'),
            'order_additional_fields': order_additional_fields,
            'ship_to_address': {
                'edi_code': 'ST',
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'country': '',
                'zip': '',
                'identification_code': '',
            },
        }

        ship_to_address = sale_order.partner_shipping_id
        if ship_to_address:
            if not (ship_to_address.name) and sale_order.partner_order_id.name:
                ship_to_address = sale_order.partner_order_id
            result['ship_to_address'].update({
                'name': ship_to_address.name or '',
                'address1': ship_to_address.street or '',
                'address2': ship_to_address.street2 or '',
                'city': ship_to_address.city or '',
                'state': ship_to_address.state_id.code or '',
                'country': ship_to_address.country_id.code or '',
                'zip': ship_to_address.zip or '',
                'identification_code': ship_to_address.san_address or '',
            })
        order_lines = []
        for line in sale_order.order_line:
            line_additional_fields = {x.name: x.value for x in line.additional_fields or []}
            order_lines.append({
                'vendorSku': line.vendorSku,
                'merchantSku': line.merchantSKU,
                'customer_sku': line_additional_fields.get('customer_sku', ''),
                'upc': line_additional_fields.get('upc', ''),
                'product_qty': int(line.product_uom_qty),
                'price_unit': line.price_unit,
            })
        result.update({
            'order_lines': order_lines,
        })

        return result

    def get_additional_confirm_shipment_information(
            self, cr, uid, sale_obj, deliveri_id, lines, context=None
    ):
        line = {
            'ship2store': False,
        }
        if sale_obj.ship2store:
            line.update({
                'ship2store': True,
            })
        return line
