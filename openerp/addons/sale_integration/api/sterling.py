# -*- coding: utf-8 -*-
from lxml import etree
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
import os
import logging

_logger = logging.getLogger(__name__)


class SterlingApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(SterlingApiClient, self).__init__(
            settings_variables, False, False, False)
        self.processQueue = settings_variables['processQueue']

    def getOrdersObj(self, xml):
        ordersApi = SterlingApiGetOrderObj(xml, self.processQueue)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        return True

    def processingOrderLines(self, lines, state="accept"):
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = SterlingApiUpdateQTY(lines)
        res = updateApi.getXml()
        return res

    def generateInventoryFile(self, data):
        generateIFApi = SterlingGenerateInventoryFile(self.settings, data)
        generateIFApi.create_inv_file()
        self.check_and_set_filename_inventory(generateIFApi)
        return True
    


class SterlingApiGetOrderObj():

    def __init__(self, xml, processQueue=False):
        self.xml = xml
        self.processQueue = processQueue

    def getOrderObj(self):
        if(self.xml == ""):
            return {}
        tree = etree.XML(self.xml)

        if(tree is not None and tree.find('Order') is not None):
            returnObj = {}
            returnObj["MerchantID"] = tree.findtext('MerchantID')
            returnObj["UserID"] = tree.findtext('UserID')
            returnObj["Password"] = tree.findtext('Password')
            returnObj["TransactionType"] = tree.findtext('TransactionType')
            returnObj["AutoConfirm"] = tree.findtext('AutoConfirm')

            root = tree.find('Order')

            returnObj = {}
            if(root.find("SalesSlipNum") is not None):
                returnObj["order_id"] = root.findtext('OrderID')
                returnObj["orderType"] = root.findtext('OrderType')
                returnObj["poNumber"] = root.findtext('PONum')
                returnObj["lines"] = []
                for line in root.findall('SalesSlipNum'):
                    returnObj["lines"].append(line.text)
                return returnObj

            returnObj["order_id"] = root.findtext('OrderID')
            returnObj["orderType"] = root.findtext('OrderType')
            returnObj["poNumber"] = root.findtext('PONum')
            returnObj["total"] = root.findtext('Price')
            returnObj["ShipComplete"] = root.findtext('ShipComplete')
            returnObj["BackorderAllowed"] = root.findtext('BackorderAllowed')
            returnObj["priorityEDI"] = root.findtext('priorityEDI')

            returnObj['address'] = {}
            returnObj['address']["order"] = {}
            returnObj['address']["order"]["name"] = root.findtext('SoldTo/SoldName')
            returnObj['address']["order"]["address1"] = root.findtext('SoldTo/Addr1')
            returnObj['address']["order"]["address2"] = root.findtext('SoldTo/Addr2')
            returnObj['address']["order"]["address3"] = root.findtext('SoldTo/Addr3')
            returnObj['address']["order"]["city"] = root.findtext('SoldTo/City')
            returnObj['address']["order"]["state"] = root.findtext('SoldTo/Region')
            returnObj['address']["order"]["country"] = root.findtext('SoldTo/Country')
            returnObj['address']["order"]["zip"] = root.findtext('SoldTo/PostalCode')
            returnObj['address']["order"]["email"] = root.findtext('SoldTo/EMail')
            returnObj['address']["order"]["phone"] = root.findtext('SoldTo/Phone')
            returnObj['address']["order"]["fax"] = root.findtext('SoldTo/Fax')

            returnObj["carrier"] = root.findtext('Carrier') + "_" + root.findtext('ShipmentMethod')
            returnObj["Service"] = root.findtext('Service')
            returnObj["merchantreference1"] = root.findtext('MerchRefOne')
            returnObj["merchantreference2"] = root.findtext('MerchRefTwo')
            returnObj["merchantreference3"] = root.findtext('MerchRefThree')
            returnObj["ProductTotal"] = root.findtext('ProductTotal')
            returnObj["external_date_order"] = root.findtext('OrderDate')
            returnObj["requestedShipDate"] = root.findtext('RequestedShipDate')
            returnObj["amount_tax"] = root.findtext('TotalTax')

            returnObj["partner_id"] = returnObj["merchantreference1"]

            returnObj['address']["ship"] = {}
            returnObj['address']["ship"]["name"] = root.findtext('ShipTo/ShipName')
            returnObj['address']["ship"]["address1"] = root.findtext('ShipTo/Addr1')
            returnObj['address']["ship"]["address2"] = root.findtext('ShipTo/Addr2')
            returnObj['address']["ship"]["address3"] = root.findtext('ShipTo/Addr3')
            returnObj['address']["ship"]["city"] = root.findtext('ShipTo/City')
            returnObj['address']["ship"]["state"] = root.findtext('ShipTo/Region')
            returnObj['address']["ship"]["country"] = root.findtext('ShipTo/Country')
            returnObj['address']["ship"]["zip"] = root.findtext('ShipTo/PostalCode')
            returnObj['address']["ship"]["EMail"] = root.findtext('ShipTo/EMail')
            returnObj['address']["ship"]["phone"] = root.findtext('ShipTo/Phone')
            returnObj['address']["ship"]["Fax"] = root.findtext('ShipTo/Fax')

            returnObj["lines"] = []
            for line in root.findall('Line'):
                lineobj = {}
                lineobj["id"] = line.findtext('LineNum')

                lineobj["tax"] = line.findtext('Tax')
                lineobj["linePrice"] = line.findtext('LinePrice')
                lineobj["customerCost"] = line.findtext('LinePrice')
                lineobj["size"] = line.findtext('LineRef4')
                lineobj["lineRef1"] = line.findtext('LineRef1')
                lineobj["lineRef2"] = line.findtext('LineRef2')
                lineobj["lineRef3"] = line.findtext('LineRef3')
                lineobj["lineRef4"] = line.findtext('LineRef4')

                lineobj['gift'] = line.findtext('GiftWrapID')

                lineobj["sku"] = line.findtext('Product/ProdID')
                lineobj["vendorSku"] = line.findtext('Product/ProdID')
                lineobj["merchantSKU"] = lineobj["sku"]
                lineobj["name"] = ''
                lineobj["qty"] = line.findtext('Product/Quantity')
                lineobj["cost"] = False

                comments = root.find("Line")
                lineobj["Comments"] = []
                for comment in comments.findall('Comment'):
                    commentobj = {}
                    commentobj["CommentType"] = comment.findtext('CommentType')
                    commentobj["Text"] = comment.findtext('Text') or ''
                    lineobj["Comments"].append(commentobj)
                    if commentobj["CommentType"] == 'WD1':
                        lineobj["name"] = commentobj["Text"] or ''

                if lineobj["lineRef3"] == 'JRP':
                    lineobj["Comments"].append({'CommentType': 'JRP', 'Text': ''})
                else:
                    try:
                        if lineobj["lineRef3"] and float(lineobj["lineRef3"]) > 0:
                            lineobj["Comments"].append({'CommentType': 'ESP', 'Text': ''})
                    except (ValueError, TypeError):
                        pass
                returnObj["lines"].append(lineobj)

            returnObj["Comments"] = []

            if self.processQueue is True and returnObj["order_id"].find('EDI') != -1:
                returnObj["processQueue"] = True
                returnObj["Comments"].append({
                    "CommentType": 'INV',
                    "Text": ''
                })
            for comment in root.findall('Comment'):
                commentobj = {}
                commentobj["CommentType"] = comment.findtext('CommentType')
                commentobj["Text"] = comment.findtext('Text') or ''
                returnObj["Comments"].append(commentobj)

            return returnObj


class SterlingApiUpdateQTY():

    name = "UpdateQuantity"
    updateLines = []

    def __init__(self, lines):
        self.updateLines = lines

    def getXml(self):
        res = []
        for line in self.updateLines:
            if not line['customer_sku']:
                continue
            xml = self.getLine(line)
            res.append({'xml': xml, 'line': line})

        return res

    def getLine(self, line):
        data = '<?xml version="1.0" encoding="UTF-8" ?><Transaction><UserID>XMLSterling</UserID><Password>st3rl1ng</Password><MerchantID>16654</MerchantID><TransactionType>VendorProductUpdate</TransactionType><VendorProductUpdate>%s</VendorProductUpdate></Transaction>'
        lines = ""
        if line['qty'] > 0:
            status = 1
        else:
            status = 0
        lines = """<VendorProduct><ProductID>%s</ProductID><ProductStatus>%s</ProductStatus></VendorProduct>""" % (line['customer_sku'], status)
        return data % lines


class SterlingGenerateInventoryFile():

    def __init__(self, settings, data):
        self.data = data or ""
        localpath_api = ApiClient()
        self._path_to_backup_local_dir = '%s' % localpath_api.create_local_dir(settings['backup_local_path'], "sterling", "inventory", "now")
        self.filename = "inv(%s).xml" % datetime.now().strftime("%Y%m%d%H%M")
        self.full_filename = os.path.join(self._path_to_backup_local_dir, self.filename)

    def create_inv_file(self):
        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(self.data), 'wb').create()
