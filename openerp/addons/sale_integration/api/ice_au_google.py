# -*- coding: utf-8 -*-
from httpauthapiclient import HTTPWithAuthApiClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from apiopenerp import ApiOpenerp
from customer_parsers.ice_au_input_csv_parser import CSVParser
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from json import loads as json_loads
from json import dumps as json_dumps
from datetime import datetime
import traceback
import logging
from openerp.addons.pf_utils.utils.decorators import return_ascii

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('bulk_carriers_list',       'Bulk Carrier List',       "['Landmark']"),
)

DEFAULT_VALUES = {
    'use_http': True
}


class IceAUGoogleApi(HTTPWithAuthApiClient):

    def __init__(self, settings):
        super(IceAUGoogleApi, self).__init__(settings)

    @property
    def sub_request_kwargs(self):
        request = {
            'headers': {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + str(self.get_api_key)
            },
        }
        return request

    @property
    def get_api_key(self):
        """Uses a pickled google object to get/refresh token. Token is used in header for all google http requests."""
        return self.get_google_token()


class IceAUGoogleApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        multi_http_settings_dict = {}
        if 'multi_http_settings' in settings_variables:
            for mhs in settings_variables['multi_http_settings']:
                multi_http_settings_dict.update(mhs.to_dict())
        settings_variables['multi_http_settings_dict'] = multi_http_settings_dict
        super(IceAUGoogleApiClient, self).__init__(
            settings_variables, IceAUGoogleOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.settings['order_type'] = 'normal'
        

        # self.load_orders_api = IceAUGoogleApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='normal'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: normal or cancel
        @return:
        """
        orders = []
        self.settings["order_type"] = order_type
        
        if save_flag:
            get_orders_api = IceAUGoogleApiGetOrdersID(self.settings) # gets all file ids from the orders folder
            orders_file_id = get_orders_api.process('only_result')
            if orders_file_id:
                for order in orders_file_id: # loops through each file
                    download_order_api = IceAUGoogleApiDownloadOrders(self.settings, order) # downloads the file
                    download_order_api.process('load')
                    orders.extend(download_order_api.orders_list)
                    if download_order_api.orders_list:
                        delete_file_api = IceAUGoogleApiDeleteFile(self.settings, order) # deletes the file in google drive if permitted
                        delete_file_api.process('only_result')
                    self.extend_log(download_order_api)
        else:
            load_orders_api = IceAUGoogleApiLoadOrders(self.settings)
            orders = load_orders_api.process('read_new')
            self.extend_log(load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = IceAUGoogleApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        confirmApi = IceAUGoogleApiConfirmOrders(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = IceAUGoogleApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines




class IceAUGoogleApiGetOrdersID(IceAUGoogleApi):

    def __init__(self, settings):
        super(IceAUGoogleApiGetOrdersID, self).__init__(settings)
        self.settings = settings
        self.order_type = settings['order_type']
        self.use_http_settings('get_orders_id')
        self.fieldnames = ["kind", "incompleteSearch", "files"]

    def process_response(self, response):
        response_data = super(IceAUGoogleApiGetOrdersID, self).process_response(response)
        orders_list = []
        if response and response_data:
            response_data = json_loads(response_data)
            lines = response_data.get('files', [])
            for line in lines:
                file_id = line.get('id', '')
                file_name = line.get('name', '')
                if file_id and file_name:
                    line_dict = {'id': file_id, 'filename': file_name}
                    orders_list.append(line_dict)
        return list(orders_list)


class IceAUGoogleApiLoadOrders(IceAUGoogleApi):
    def __init__(self, settings):
        super(IceAUGoogleApiLoadOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.parser = CSVParser(self.customer, ',', '"', self.bulk_carriers_list)

class IceAUGoogleApiDownloadOrders(IceAUGoogleApi):

    def __init__(self, settings, order_id):
        super(IceAUGoogleApiDownloadOrders, self).__init__(settings)
        self.order_type = settings['order_type']
        self.use_http_settings('load_orders')
        self.sub_url += "{}?alt=media".format(str(order_id['id']))
        self.filename = str(order_id['filename'])
        self.orders_list = []

    def process_response(self, response):
        response_data = super(IceAUGoogleApiDownloadOrders, self).process_response(response)
        orders_list = []
        self.orders_list.append(response_data)
        orders_list.append(response_data)
        return orders_list

class IceAUGoogleApiDeleteFile(IceAUGoogleApi):

    def __init__(self, settings, order_id):
        super(IceAUGoogleApiDeleteFile, self).__init__(settings)
        self.use_http_settings('delete_file')
        self.sub_url += str(order_id['id'])

    def check_response(self, response):
        try:
            super(IceAUGoogleApiDeleteFile, self).check_response(response)
        except Exception:
            pass
            if response and response.status_code == 403:
                resp = json_loads(response.text)
                logger.info("Permission Denied. Cusotmer doesnt not give us permission to delete this file. Order is loaded though.")
                return True
            else:
                return False
        return True


class IceAUGoogleApiGetOrderObj(YamlOrder):

    def getOrderObj(self):
        order_obj = {}

        if(self.serialized_order == ""):
            return order_obj

        yaml_obj = YamlObject()
        try:
            order = yaml_obj.deserialize(_data=self.serialized_order)
            if(order is not None):
                order_obj['partner_id'] = order.get('partner_id', False)
                order_obj['order_id'] = order.get('order_id', False)
                order_obj['carrier'] = order.get('carrier', False)
                order_obj['external_date_order'] = order.get('external_date_order', False)
                order_obj['address'] = {}
                order_obj['address']['ship'] = {}
                order_obj['address']['ship']['name'] = order['address']['ship'].get('name', '')
                order_obj['address']['ship']['address1'] = order['address']['ship'].get('address1', '')
                order_obj['address']['ship']['address2'] = order['address']['ship'].get('address2', '')
                order_obj['address']['ship']['city'] = order['address']['ship'].get('city', '')
                order_obj['address']['ship']['state'] = order['address']['ship'].get('state', '')
                order_obj['address']['ship']['zip'] = order['address']['ship'].get('zip', '')
                order_obj['address']['ship']['country'] = order['address']['ship'].get('country', '')
                order_obj['address']['ship']['phone'] = order['address']['ship'].get('phone', '')
                order_obj['bulk_transfer'] = order.get('bulk_transfer', False)

                order_obj['shipping'] = 0.0
                order_obj['tax'] = 0.0
                order_obj['discount_total'] = 0.0

                order_obj['ice_total_due'] = order.get('total_due', 0)
                order_obj['ice_tax_percentage'] = order.get('tax_percentage', 0)

                for i in ('shipping_fee', 'shipping_tax'):
                    order_obj['shipping'] += float(order.get(i, 0.0))
                    order_obj[i] = order.get(i, 0)

                order_obj['lines'] = []
                if(not isinstance(order['lines'], list)):
                    order['lines'] = [order['lines']]
                for line_item in order['lines']:
                    line_item_obj = {}
                    line_item_obj['id'] = line_item.get('id', False)
                    line_item_obj['vendorSku'] = line_item.get('vendor_sku', '')
                    line_item_obj['originvendorSku'] = line_item.get('vendor_sku', False)
                    line_item_obj['merchantSKU'] = line_item.get('merchant_sku', False)
                    line_item_obj['size'] = line_item.get('size', False)
                    line_item_obj['qty'] = line_item.get('qty', False)
                    line_item_obj['name'] = line_item.get('name', False)
                    line_item_obj['customerCost'] = line_item.get('cost', False)
                    line_item_obj['cost'] = line_item.get('cost', False)
                    line_item_obj['shipCost'] = 0
                    line_item_obj['gift_message'] = line_item.get('gift_message', '')

                    line_item_obj['ice_sub_total'] = line_item.get('sub_total', 0.0)
                    line_item_obj['ice_row_total'] = line_item.get('row_total', 0.0)
                    line_item_obj['ice_tax_amount'] = line_item.get('tax_amount', 0.0)
                    line_item_obj['ice_discount'] = line_item.get('discount', 0.0)

                    order_obj['tax'] += float(line_item.get('tax_amount', 0.0))
                    order_obj['discount_total'] += float(line_item.get('discount', 0.0))

                    if('/' in line_item_obj['vendorSku']):
                        try:
                            sku = line_item_obj['vendorSku']
                            if float(sku[sku.find("/") + 1:]) == float(line_item_obj['size']):
                                line_item_obj['vendorSku'] = sku[:sku.find("/")]
                        except Exception:
                            pass
                    line_item_obj['sku'] = line_item_obj['vendorSku']

                    order_obj['lines'].append(line_item_obj)

                order_obj['tax'] = str(order_obj['tax'])
                order_obj['shipping'] = str(order_obj['shipping'])
        except Exception:
            _title_msg = 'error for order: %s' % order.get('order_id', False)
            _msg = traceback.format_exc()
            self._log.append({
                'title': "Deserialize order error: %s" % self.customer,
                'msg': '\n'.join([_title_msg, _msg]),
                'type': 'deserialize error',
                'create_date': "{:'%Y-%m-%d %H:%M:%S}".format(datetime.now())
            })
        return order_obj


class IceAUGoogleApiUpdateQTY(IceAUGoogleApi):

    def __init__(self, settings, lines):
        super(IceAUGoogleApiUpdateQTY, self).__init__(settings)
        self.use_http_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.attachment = ''
        self.filename = "{:ICEAUINVENTORY%Y%m%d%H%M%f.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def prepare_feed(self):
        lines = self.lines
        for line in lines:
            if(line.get('sku', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        data = {'lines': self.revision_lines['good']}
        return self.prepare_data(data) # to use mako template

    @property
    def sub_request_kwargs(self):
        request = super(IceAUGoogleApiUpdateQTY, self).sub_request_kwargs
        self.attachment = self.prepare_feed()
        meta_data = {
            'parents': [str(self.update_qty_folder_id)],
            'name': self.filename,
            'mimeType': 'text/csv'
        }

        headers = request.get('headers', {})
        body, content_type = self.encode_multipart_related(
            json_dumps(meta_data),
            self.attachment,
            'text/csv'
        )
        headers.update({
            'Content-Type': content_type,
            'Content-Length': str(len(self.attachment.encode('utf-8')))
        })
        request.update({
            "data": body
        })
        return request

    def data_to_save(self, response):
        return self.attachment


class IceAUGoogleApiConfirmOrders(IceAUGoogleApi):

    def __init__(self, settings, lines):
        super(IceAUGoogleApiConfirmOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.attachment = ''
        self.filename = "{:ICEAUSHIPPED%Y%m%d%H%M%f.csv}".format(datetime.now())

    def prepare_feed(self):
        lines = self.lines
        for line in lines:
            date_now = datetime.now()
            if line.get('shp_date', False):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
                line['date'] = date_now.strftime("%d/%m/%Y")
                line['product_qty'] = int(line['product_qty'])
            else:
                line['date'] = ''
                line['product_qty'] = ''
        return self.prepare_data({'lines': lines})

    @property
    def sub_request_kwargs(self):
        request = super(IceAUGoogleApiConfirmOrders, self).sub_request_kwargs
        self.attachment = self.prepare_feed()
        meta_data = {
            'parents': [str(self.confirm_shipment_folder_id)],
            'name': self.filename,
            'mimeType': 'text/csv'
        }

        headers = request.get('headers', {})
        body, content_type = self.encode_multipart_related(
            json_dumps(meta_data),
            self.attachment,
            'text/csv'
        )
        headers.update({
            'Content-Type': content_type,
            'Content-Length': str(len(self.attachment.encode('utf-8')))
        })
        request.update({
            "data": body
        })
        return request

    def data_to_save(self, response):
        return self.attachment


class IceAUGoogleOpenerp(ApiOpenerp):

    def __init__(self):

        super(IceAUGoogleOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'customerCost': line['cost'] or False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': False,
            'size': line['size'],
        }

        if('/' in line['sku']):
            try:
                _sku = line['sku']
                _size = float(_sku[_sku.find("/") + 1:])
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except Exception:
                pass

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        field_list = ['sku', 'merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj
