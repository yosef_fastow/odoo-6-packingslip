# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from apiopenerp import ApiOpenerp
from datetime import datetime
import logging
from customer_parsers.smb_input_csv_parser import CSVParser

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


class SmbApi(FtpClient):

    def __init__(self, settings):
        super(SmbApi, self).__init__(settings)


class SmbApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(SmbApiClient, self).__init__(
            settings_variables, SmbOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = SmbApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        res = []
        confirmApi = SmbApiConfirmOrders(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        res.append('Confirm result:\n{0}\n'.format(str(res_confirm)))

        if self.is_invoice_required:
            invoiceApi = SmbApiInvoiceOrders(self.settings, lines)
            res_invoice = invoiceApi.process('send')
            self.extend_log(invoiceApi)
            res.append('Invoice result:\n{0}'.format(str(res_invoice)))

        return "\n\n".join(res)

    def updateQTY(self, lines, mode=None):
        updateApi = SmbApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class SmbApiGetOrdersXML(SmbApi):

    def __init__(self, settings):
        super(SmbApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSVParser(self.customer, ',', '"')


class SmbApiConfirmOrders(SmbApi):

    def __init__(self, settings, lines):
        super(SmbApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = "tracking-{0}.csv"

    def upload_data(self):
        lines = self.lines
        for line in lines:
            self.filename = self.filename.format(str(line['po_number']).replace('SMB', ''))
            product_qty = 0
            if line.get('product_qty', False):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
            date_now = datetime.now()
            if(line.get('shp_date', False)):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y")
            line['product_qty'] = product_qty
        return {'lines': lines}


class SmbApiInvoiceOrders(SmbApi):

    def __init__(self, settings, lines):
        super(SmbApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.type_tpl = "string"
        self.use_mako_templates('invoice')
        self.lines = lines
        self.filename = "{:invoice%m%d%Y%H%M.csv}".format(datetime.now())

    def upload_data(self):
        lines = self.lines
        for line in lines:
            product_qty = 0
            if(line.get('product_qty', False)):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
            date_now = datetime.now()
            if(line.get('shp_date', False)):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y")
            line['product_qty'] = product_qty
            additional_fields = line.get('additional_fields', {})
            line['batch_po_number'] = str(additional_fields.get('batch_po_number', False))
        return {'lines': lines}


class SmbApiUpdateQTY(SmbApi):

    def __init__(self, settings, lines):
        super(SmbApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "{:inventory%m%d%Y%H%M.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            _customer_sku = line.get('customer_id_delmar', False)
            if((not _customer_sku) or (_customer_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class SmbOpenerp(ApiOpenerp):

    def __init__(self):
        super(SmbOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': line['price_unit'],
            'customerCost': line['price_unit'],
            'customer_sku': line['customer_sku'],
            'merchantSKU': line['customer_sku'],
            'vendorSku': line['line_vendor_sku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': line['sku'],
            'size': line['size'],
            'id': line['id'],
            'additional_fields':
            [(0, 0, {
                'name': 'batch_po_number',
                'label': 'Batch PO Number',
                'value': line.get('batch_po_number', ''),
            }), ],
        }

        if('/' in line['sku']):
            try:
                _sku = line['sku']
                _size = float(_sku[_sku.find("/") + 1:])
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        product = False

        field_list = ['sku', 'customer_sku', 'line_vendor_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'customer_sku',
                        'customer_id_delmar'
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            if not line_obj['price_unit']:
                product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if product_cost:
                    line_obj['price_unit'] = product_cost
                else:
                    line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
