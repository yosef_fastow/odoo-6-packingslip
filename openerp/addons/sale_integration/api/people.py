# -*- coding: utf-8 -*-
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from customer_parsers import people_csv_parser, people_xml_parser
from utils import feedutils as feed_utils
from configparser import ConfigParser
import os
import logging
from datetime import datetime

_logger = logging.getLogger(__name__)


class PeopleApi(ApiClient):

    def __init__(self, settings):
        super(PeopleApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/people_settings.ini' % (
            os.path.dirname(__file__)))
        self.saveFlag = settings["saveFlag"] \
            if 'saveFlag' in settings else False
        self.customer = settings["customer"]
        self.passphrase = settings["passphrase"]
        self.gnupghome = settings["gnupghome"]
        self.multi_ftp_settings = settings.get("multi_ftp_settings", {})
        self._log = []

    def get_ftp_setting(self, ftp_settings, setting_name):
        current_setting = False
        ftp_path = False
        for setting in ftp_settings:
            if setting.action == setting_name:
                current_setting = setting.ftp_setting_id
                ftp_path = setting['ftp_path']
                break
        return current_setting, ftp_path

    def set_decrypt_data(self, data):
        res = ""
        if(self.customer == 'people'):
            gpg = self.get_gnupg(self.gnupghome)
            res = gpg.decrypt(str(data), passphrase=self.passphrase).data
        else:
            res = data
        return res


class PeopleApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(PeopleApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = PeopleApiGetOrdersXML

    def loadOrders(self, saveInFolder=False):
        orders = []
        self.settings["saveFlag"] = saveInFolder and saveInFolder or False
        ordersApi = PeopleApiGetOrdersXML(self.settings)
        if ordersApi.cur_ftp_setting:
            orders = ordersApi.send()
        else:
            _logger.error("People not found ftp_setting (load_orders)")
        self.extend_log(ordersApi)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = PeopleApiGetOrderObj(xml, self.settings['customer'])
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = PeopleApiUpdateQTY(self.settings, lines)
        if updateApi.cur_ftp_setting:
            updateApi.send()
            self.check_and_set_filename_inventory(updateApi)
        else:
            _logger.error("People not found ftp_setting (inventory)")
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        return True


class PeopleApiGetOrdersXML(PeopleApi):
    cur_ftp_setting = ""

    def __init__(self, settings):
        super(PeopleApiGetOrdersXML, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(
            self.multi_ftp_settings, 'load_orders')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"], True)
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_local_dir': '%s' % self.create_local_dir(
                    settings['input_local_path'], "people", "orders"),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(
                    settings['backup_local_path'], "people", "orders", "now")
            }
            self.set_ftp_method('get')
            self.set_path_list(path_list)

    def parse_response(self, response):
        csv_parser = people_csv_parser.PeopleApiGetOrdersXML()
        ordersList = csv_parser.parse_response(response)
        return ordersList


class PeopleApiGetOrderObj():

    def __init__(self, xml, customer):
        self.xml = xml
        self.customer = customer

    def getOrderObj(self):
        xml_parser = people_xml_parser.PeopleApiGetOrderObj()
        ordersObj = xml_parser.getOrderObj(self.xml)

        return ordersObj


class PeopleApiUpdateQTY(PeopleApi):
    filename = "inv.txt"
    filename_local = ""
    confirmLines = []
    name = "UpdateQTY"
    cur_ftp_setting = ""

    def __init__(self, settings, lines):
        super(PeopleApiUpdateQTY, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(
            self.multi_ftp_settings, 'inventory')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(
                    settings['backup_local_path'],
                    "people", "inventory", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        self.filename_local = "inv(%s).txt" %\
            datetime.now().strftime("%Y%m%d%H%M")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        data = ["Item Number\tQuantity Orderable\tUnit Cost\n"]
        for line in self.confirmLines:
            if line['customer_price']:
                line['customer_price'] = "%.2f" % float(line['customer_price'])
            try:
                items = self.config['InventorySetting'].items()
            except AttributeError:
                items = self.config.items('InventorySetting')
            SkuRecord = feed_utils.FeedUtills(
                items, line).create_csv_line('\t', True)
            if SkuRecord:
                data.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(data)[:-2]


class PeopleApiConfirmShipment(PeopleApi):

    name = "ConfirmShipment"
    confirmLines = []

    def __init__(self, settings, lines):
        super(PeopleApiConfirmShipment, self).__init__(settings)
        self.cur_ftp_setting, self.ftp_path = self.get_ftp_setting(
            self.multi_ftp_settings, 'confirm_shipment')
        if self.cur_ftp_setting:
            self.set_ftp_settings(
                self.cur_ftp_setting["ftp_host"],
                self.cur_ftp_setting["ftp_user"],
                self.cur_ftp_setting["ftp_pass"])
            path_list = {
                'path_to_ftp_dir': '%s' % str(self.ftp_path),
                'path_to_backup_local_dir': '%s' % self.create_local_dir(
                    settings['backup_local_path'], "people", "ship", "now")
            }
            self.set_ftp_method('put')
            self.set_path_list(path_list)
        self.confirmLines = lines
        date = datetime.now().strftime("%Y%m%d%H%M")
        self.filename = 'SHIPPED%s.csv' % str(date)

    def upload_data(self):
        data = ["PO Number\tFulfiller Order Number\tFulfiller Sku\tQuantity\tItem Status\tShip to Name\tShip Date\tShipping Carrier\tShipping Method\tTracking Number\n"]
        for line in self.confirmLines:
            dt = datetime.now()
            if(line['shp_date']):
                dt = datetime.strptime(
                    line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = dt.strftime("%d/%m/%Y")
            line['int_product_qty'] = int(line['product_qty'])
            data.append(feed_utils.FeedUtills(
                self.config['ShippingConfirmation'].items(),
                line).create_csv_line('\t', True))
        return "".join(data)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
