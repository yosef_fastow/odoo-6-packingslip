import os
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from customer_parsers.jomashop_csv_parser2 import CSV_parser
from utils import feedutils as feed_utils
from configparser import ConfigParser
from pf_utils.utils.re_utils import f_d
import logging
from datetime import datetime
from apiopenerp import ApiOpenerp
from json import loads as json_loads

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


def normalize_f_json(string):
    return string.replace("'", '"').replace(" None,", ' "",')


class JomashopApi(FtpClient):

    def __init__(self, settings):
        super(JomashopApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/jomashop_settings2.ini' % os.path.dirname(__file__))


class JomashopApiClient2(AbsApiClient):
    use_local_folder = True

    def __init__(self, settings_variables):
        super(JomashopApiClient2, self).__init__(
            settings_variables, JomashopOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = JomashopApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def updateQTY(self, lines, mode=None):
        updateApi = JomashopApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        ordersApi = JomashopApiConfirmShipment(self.settings, lines)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def processingOrderLines(self, lines, state='accepted'):
        res = False
        processed_lines = []
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            ordersApi = JomashopApiCancelOrders(self.settings, lines)
            res_send = ordersApi.send()
            res &= res_send
            if res_send:
                processed_lines.append(lines)
            self.extend_log(ordersApi)
        else:
            res = True
        return res


class JomashopApiGetOrdersXML(JomashopApi):

    def __init__(self, settings):
        super(JomashopApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSV_parser(customer=self.customer)


class JomashopApiUpdateQTY(JomashopApi):
    updateLines = []

    def __init__(self, settings, lines):
        super(JomashopApiUpdateQTY, self).__init__(settings)

        self.use_ftp_settings('inventory')
        self.lines = lines
        self.filename = 'inv.csv'
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.revision_lines = {
            'bad': [],
            'good': [],
        }

    def upload_data(self):
        for line in self.lines:
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class JomashopApiConfirmShipment(JomashopApi):
    confirmLines = []

    def __init__(self, settings, lines):
        super(JomashopApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.confirmLines = lines
        self.filename = "Confirm%s.csv" % datetime.now().strftime('%Y%m%d-%H%M')

    def upload_data(self):

        data = '"UniqueID", "Tracking", "Vendor Invoice", "Order Qty", "SO #" ,"JomaShop Item #","SKU","Ship Method","Ship Full Name","Ship Address","Ship Address2","Ship City","Ship State","Ship Zip","Ship Country","Phone","SentToShipDate","ShipDate","Cost"\n'
        for line in self.confirmLines:
            if (line['shp_date']):
                line['shp_date'] = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S").strftime(
                    "%m/%d/%Y %H:%M")
            if (line['date']):
                line['date'] = datetime.strptime(line.get('date'), "%Y-%m-%d %H:%M:%S").strftime("%m/%d/%Y %H:%M")

            if (line.get('order_additional_fields', False)):
                line['billphone'] = line.get('order_additional_fields').get('billphone', '')
                line['billfullname'] = line.get('order_additional_fields').get('billfullname', '')
                line['shipphone'] = line.get('order_additional_fields').get('shipphone', '')
                line['ship_service'] = line.get('order_additional_fields').get('ship_service', '')
                line['name'] = line.get('order_additional_fields').get('ship_full_name', '')
            data += feed_utils.FeedUtills(self.config['ShippingConfirmation'].items(), line).create_csv_line(quote=True)
        return data


class JomashopOpenerp(ApiOpenerp):

    def __init__(self):
        super(JomashopOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = order.get('additional_fields')
        if additional_fields:
            wrapped_fields = self.wrap_additional_fields(additional_fields)
            return {'additional_fields': wrapped_fields}
        else:
            return {}

    def fill_line(self, cr, uid, settings, line, context=None):


        line_obj = {
            "notes": "",
            "name": line['name'],
            "price_unit": line['cost'],
            "customerCost": line['customerCost'],
            "merchantSKU": line['merchantSKU'],
            "vendorSku": line['vendorSku'],
            'gift_message': line['gift_message'],
            "qty": line['qty'],
            'product_id': False,
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]

        }

        product = {}
        product_obj = self.pool.get('product.product')
        field_list = ['vendorSku', 'merchantSKU', 'zuck_item']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not product and 'AMR-' in line[field]:
                    sku = line[field].replace('AMR-', '')
                    product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], sku)
                if product:
                    line_obj['name'] = product.name
                    line_obj['sku'] = product.default_code
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid,
                                                                         [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if size_ids:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
        else:
            line_obj['notes'] = "Can't find product by sku %s.\n" % (line['merchantSKU'])
        additional_fields = {
            'additional_fields': [
                (0, 0, {'name': 'zuck_order', 'label': 'ZuckOrder', 'value': line.get('zuck_order', False)}),
                (0, 0, {'name': 'zuck_item', 'label': 'ZuckItem', 'value': line.get('zuck_item', False)})
            ]
        }
        line_obj.update(additional_fields)

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        tax_obj = self.pool.get("delmar.sale.taxes")
        raw_taxes = {}
        line = {
            'original_addresses': {},
        }
        shipping_address = sale_obj and sale_obj.partner_shipping_id
        country_id = shipping_address.country_id.id
        state_id = shipping_address.state_id.id
        if country_id and state_id:
            raw_taxes = tax_obj.get_raw_taxes(cr, uid, country_id, [state_id])
        line.update({'raw_taxes': raw_taxes})

        order_additional_fields = {x.name: x.value for x in sale_obj.additional_fields or []}
        addresses = order_additional_fields.get('addresses', {})
        if addresses:
            try:
                addresses = json_loads(addresses)
            except ValueError:
                normalized_json_str = normalize_f_json(addresses)
                addresses = json_loads(normalized_json_str)
            revert_address_type_map = {value: key for key, value in address_type_map.items()}
            for type_address, value in addresses.items():
                line['original_addresses'].update({
                    revert_address_type_map.get(type_address, 'unknown'): value
                })
        return line

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):

        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_line_id': order_line.external_customer_line_id,
            'product_qty': order_line.product_uom_qty,

        }

        return line

    def get_additional_confirm_shipment_information_for_line(
            self, cr, uid,
            partner_id, order, order_line,
            context=None):

        line = {
            'customerCost': order_line.customerCost,
            "gift_message": order_line.gift_message
        }

        return line


class JomashopApiCancelOrders(JomashopApi):

    def __init__(self, settings, lines):
        super(JomashopApiCancelOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.cancelLines = lines
        self.filename = "Cancel%s.csv" % datetime.now().strftime('%Y%m%d-%H%M ')

    def upload_data(self):
        data = '"UniqueID", "Tracking", "Vendor Invoice", "Order Qty", "SO #" ,"JomaShop Item #","SKU","Ship Method","Ship Full Name","Ship Address","Ship Address2","Ship City","Ship State","Ship Zip","Ship Country","Phone","SentToShipDate","ShipDate","Cost"\n'

        for line in self.cancelLines:
            line['date'] = line['shp_date'] = datetime.now().strftime("%m/%d/%Y %H:%M")
            if (line.get('ship', False)):
                line['ship_to_address_line_1'] = line.get('ship', False).get('address1', '')
                line['ship_to_address_line_2'] = line.get('ship', False).get('address2', '')
                line['ship_to_postal_code'] = line.get('ship', False).get('zip', '')
                line['shipphone'] = line.get('ship', False).get('phone', '')
                line['remit_to_country'] = line.get('ship', False).get('country', '')
                line['ship_to_city'] = line.get('ship', False).get('city', '')
                line['ship_to_state'] = line.get('ship', False).get('state', '')
                line['name'] = line.get('ship', False).get('name', '')

            if (line.get('order_additional_fields', False)):
                line['billphone'] = line.get('order_additional_fields').get('billphone', '')
                line['billfullname'] = line.get('order_additional_fields').get('billfullname', '')
                line['shipphone'] = line.get('order_additional_fields').get('shipphone', '')
                line['ship_service'] = line.get('order_additional_fields').get('ship_service', '')
                line['name'] = line.get('order_additional_fields').get('ship_full_name', '')
            line['tracking_number'] = 'NIS'
            data += feed_utils.FeedUtills(self.config['ShippingConfirmation'].items(), line).create_csv_line(
                quote=True)

        return data
