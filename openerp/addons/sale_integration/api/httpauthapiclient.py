# -*- coding: utf-8 -*-
from baseapiclient import BaseApiClient
import codecs
import pickle
import requests
import logging
import traceback
from openerp.addons.pf_utils.utils.backup_file import backup_file_open
from os.path import join as os_path_join
from datetime import datetime, timedelta
from google.auth.transport.requests import Request
from requests.exceptions import ConnectionError
from urllib import urlencode as urllib_urlencode
from urllib3.filepost import encode_multipart_formdata, choose_boundary
from urllib3.fields import RequestField
from urlparse import urlsplit, urlunsplit
from requests import HTTPError
from openerp.addons.pf_utils.utils.decorators import timethis_to_log, to_ascii
from openerp.addons.pf_utils.utils.str_utils import str_encode


_logger = logging.getLogger(__name__)


def url_path_join(parts):
    """Normalize url parts and join them with a slash."""
    schemes, netlocs, paths, queries, fragments = zip(*(urlsplit(part) for part in parts))
    scheme = first(schemes)
    netloc = first(netlocs)
    path = '/'.join(x.strip('/') for x in paths if x)
    query = first(queries)
    fragment = first(fragments)
    return urlunsplit((scheme, netloc, path, query, fragment))


def first(sequence, default=''):
    return next((x for x in sequence if x), default)


class HTTPWithAuthApiClient(BaseApiClient):
    """Base class for creating HTTP API Client

    with [HTTPBasicAuth, HTTPProxyAuth, HTTPDigestAuth]
    authentication

    """
    def __init__(self, settings):
        """Constructs new :class: `HTTPWithAuthApiClient <HTTPWithAuthApiClient>`

        :param settings: dict, should be contains http_setting_ids

        Usage::
            from httpauthapiclient import HTTPWithAuthApiClient


            class MyHTTPWithAuthApiClient(HTTPWithAuthApiClient):
            def __init__(self, settings):
                super(MyHTTPWithAuthApiClient, self).__init__(settings)

        """
        super(HTTPWithAuthApiClient, self).__init__()
        if 'url' in settings:
            del settings['url']
        self.copy_attributes_from_settings(settings)
        self._log = []
        self.logger = _logger

    @property
    def auth_method(self):
        """Property returns autodetection auth_method from `authentication` :attribute:"""
        authentication = getattr(self, 'authentication', False)
        if authentication:
            if authentication == 'no_auth':
                self._auth_method = self._no_auth_method
            elif authentication == 'base':
                self._auth_method = self._base_auth_method
            elif authentication == 'HTTPBasicAuth':
                self._auth_method = requests.auth.HTTPBasicAuth
            elif authentication == 'HTTPProxyAuth':
                self._auth_method = requests.auth.HTTPProxyAuth
            elif authentication == 'HTTPDigestAuth':
                self._auth_method = requests.auth.HTTPDigestAuth
            else:
                raise NotImplementedError('Unknow Auth method name: {}'.format(authentication))
        else:
            raise NotImplementedError('Authentication can\'t be empty!')
        return self._auth_method

    @auth_method.setter
    def auth_method(self, authentication):
        raise AttributeError('You can\'t set auth_method property, please set authentication attribute')

    def _no_auth_method(self, username, password):
        return None

    def _base_auth_method(self, username, password):
        """Return a tuple for base authentication with requests module"""
        return (username, password)

    def auth(self):
        """Wrapper for self._auth_method, returns headers for selected authentication"""
        return self.auth_method(self.username, self.password)

    def encode_multipart_media(self, metadata, media, media_content_type):
        """Encodes Metadata and content for a multi-part post request"""
        rf1 = RequestField(name='placeholder', data=metadata, headers={'Content-Type': 'application/json; charset=UTF-8'})
        rf2 = RequestField(name='placeholder2', data=media, headers={'Content-Type': media_content_type})
        return [rf1, rf2]

    def get_google_token(self):
        """Uses a pickled google object to get/refresh token. Token is used in header for all google http requests."""
        creds = pickle.loads(codecs.decode(self.pickled_token.encode(), "base64")) # gets credentials from settings_variables
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request()) # gets refreshed token
            else:
                return False # needs authorisation in browser to run
        return creds.token

    def encode_multipart_related(self, metadata, media, media_content_type, boundary=None):
        """Creates the body and content_type (in header) for a multipart/related post request """
        fields = self.encode_multipart_media(metadata, media, media_content_type)
        if boundary is None:
            boundary = choose_boundary()
        body, _ = encode_multipart_formdata(fields, boundary)
        content_type = str('multipart/related; boundary=%s' % boundary)
        return body, content_type

    def process(self, method, skip=None):
        """Base method for execute selected action

        :param method: method to execute, should be ['load', 'read', 'read_new', 'send']

        Usage::
            # USE ONLY IN CLASS INHERITED FROM AbsApiClient
            my_orders_api = MyGetOrdersApi(settings)  # inherited from you base API class
            order_list = my_api.process('load')

        """
        result = None
        try:
            if method == 'load':
                result = self.load()
            elif method == 'read':
                result = self.read()
            elif method == 'read_new':
                result = self.read_new()
            elif method == 'send':
                result = self.send()
            elif method == 'only_result':
                result = self.only_result()
            else:
                raise NotImplementedError('Unknown method: {}'.format(method))
        except ConnectionError as connection_err:
            msg = traceback.format_exc()
            print connection_err
            if hasattr(connection_err, 'response'):
                msg += '\n\nERROR: {err}\n\nURL: {url}\n\nMETHOD: {method}\n\nHEADERS: {headers}\n\nBODY:{body}'.format(
                    err=str(connection_err),
                    url=str(connection_err.response.url),
                    method=str(connection_err.response.method),
                    headers=str(connection_err.response.headers),
                    body=self.response and self.response.request and self.response.request.body or ''
                )
            self.log({
                'title': msg,
                'type': 'error',
                'message': msg,
            })
            result = False
        except HTTPError as http_error:
            status_code = http_error.response.status_code
            if skip and status_code in skip:
                pass
            else:
                raise
        except Exception:
            msg = traceback.format_exc()
            self.log({
                'title': msg,
                'type': 'error',
                'message': msg,
            })
            result = False

        return result

    @property
    def exec_method(self):
        """:property: parse and returns method from `method` attribute"""
        method = getattr(self, 'method', 'GET').upper()
        if method not in ['GET', 'POST', 'PUT', 'DELETE']:
            raise NotImplementedError('Unknow HTTP method: {}'.format(method))
        if method == 'GET':
            self._method = self.get
        elif method == 'POST':
            self._method = self.post
        elif method == 'PUT':
            self._method = self.put
        elif method == 'DELETE':
            self._method = self.delete
        else:
            pass
        return self._method

    @exec_method.setter
    def exec_method(self, value):
        raise AttributeError('You can\'t set exec_method property, please set http_method attribute')

    @property
    def sub_request_kwargs(self):
        """:property: returns your custom parameters for request

        Usage::
        class MyGetOrdersApi(MyApiClient):

                ...

            @property
            def sub_request_kwargs(self):
                return {'data': self.get_request_data()}

        """
        return {}

    @sub_request_kwargs.setter
    def sub_request_kwargs(self, value):
        raise AttributeError('You can\'t set sub_request_kwargs property, please inherit sub_request_kwargs in your class')

    @property
    def request_kwargs(self):
        """:property: returns a base authentication parameters"""
        request_kwargs = {
            'auth': self.auth(),
            'timeout': self.timeout,
            'verify': self.verify,
        }
        request_kwargs.update(self.sub_request_kwargs)
        return request_kwargs

    @request_kwargs.setter
    def request_kwargs(self, value):
        raise AttributeError('You can\'t set request_kwargs property, please use sub_request_kwargs in your class')

    @property
    def url(self):
        url_parts = [self.base_url]
        if self.sub_url:
            url_parts.append(self.sub_url)
        if self.params:
            url_parts.append(self.params)
        url = url_path_join(url_parts)
        return url

    @url.setter
    def url(self, value):
        raise AttributeError('You can\'t set url property')

    @property
    def params(self):
        if hasattr(self, '_params') and self._params:
            return '?' + self._params
        else:
            return None

    @params.setter
    def params(self, input_params=None):
        self._params = ''
        params = {}
        if input_params is None:
            input_params = []
        for param_dict in input_params:
            key = param_dict.name
            value = param_dict.value
            if value and '{' and '}' in value:
                type_value, date_format, value = value[1: -1].split('|')
                if type_value == 'date':
                    if value == 'today':
                        value = datetime.utcnow().strftime(date_format)
                    elif 'date_delta' in value:
                        temp, days = value.split('=')
                        value = (
                            datetime.utcnow() - timedelta(days=int(days))
                        ).strftime(date_format)
                    else:
                        raise NotImplementedError('Unknown {}'.format(value))
                else:
                    raise NotImplementedError('Unknown type value {}'.format(type_value))
            else:
                pass
            params.update({key: value})
        self._params = urllib_urlencode(params)

    @property
    def response(self):
        if hasattr(self, '_response') and self._response:
            return self._response
        else:
            return None

    @response.setter
    def response(self, value):
        raise AttributeError('You can\'t set auth_method property, it setting automatically in \'get\' or \'post\' methods')

    def get(self):
        """:method: for send HTTP request using `GET` method"""
        self._response = requests.get(self.url, **self.request_kwargs)
        return self._response

    def post(self):
        """:method: for send HTTP request using `POST` method"""
        self._response = requests.post(self.url, **self.request_kwargs)
        return self._response

    def put(self):
        """:method: for send HTTP request using `PUT` method"""
        self._response = requests.put(self.url, **self.request_kwargs)
        return self._response

    def delete(self):
        """:method: for send HTTP request using `DELETE` method"""
        self._response = requests.delete(self.url, **self.request_kwargs)
        return self._response

    def check_response(self, response):
        """:method: for checking response from server(only `200` returned status is correct)"""
        if not response.status_code == requests.codes.ok:
            response.raise_for_status()
        return True

    def process_response(self, response):
        return response.text


    @timethis_to_log
    def load(self):
        """:method: `load` for using only in `process` wrapper :method:"""
        result = False
        response = self.exec_method()
        self.check_response(response)
        write_data = self.process_response(response)
        if write_data:
            i = 0
            result = True
            if not isinstance(write_data, (list, tuple)):
                write_data = [write_data]
            for data in write_data:
                filename = "{base_name}_{index}".format(base_name=self.filename, index=i)
                self.save_file(filename, data)
                self.backup_file(filename, data)

                i += 1
                result &= True
            self.log({
                'title': 'LOAD',
                'type': 'debug',
                'message': 'LOADED {} FILE/FILES'.format(len(write_data)),
            })
        else:
            result &= False
            self.log({
                'title': 'LOAD',
                'type': 'debug',
                'message': 'NOTHING LOAD',
            })
        return result

    @timethis_to_log
    def read(self):
        """:method: `read` for using only in `process` wrapper :method:"""
        pass

    def data_to_save(self, response):
        return response.request.body

    @timethis_to_log
    def send(self):
        """:method: `send` for using only in `process` wrapper :method:"""
        response = self.exec_method()
        self.check_response(response)
        local_filename = self.actual_local_filename
        self.backup_file(local_filename, self.data_to_save(response))
        response = self.process_response(response)
        return response

    def only_result(self):
        """:method: `only_result` for using only in `process` wrapper :method:"""
        response = self.exec_method()
        self.check_response(response)
        response = self.process_response(response)
        return response

    def save_file(self, filename, data):
        """:method: saving data to file to `input local folder`, using only in `load` or `send` :methods:"""
        full_filename = os_path_join(self.path_to_local_dir, filename)
        data = to_ascii(data)
        with backup_file_open(full_filename, 'wb') as f:
            f.write(data)
        self.log({
            'title': 'SAVE FILE',
            'type': 'debug',
            'message': 'SAVED DATA: {data}\n\nTO FILE: {filename}'.format(
                data=data,
                filename=full_filename
            ),
        })

        return full_filename

    def backup_file(self, filename, data):
        """:method: saving data to file to `backup local folder`, using only in `load` or `send` :methods:"""
        full_filename = os_path_join(self.path_to_backup_local_dir, filename)
        data = to_ascii(data)
        with backup_file_open(full_filename, 'wb') as f:
            f.write(data)
        self.log({
            'title': 'BACKUP FILE',
            'type': 'debug',
            'message': 'BACKUP DATA: {data}\n\nTO FILE: {filename}'.format(
                data=data,
                filename=full_filename
            ),
        })

        return full_filename

    def use_http_settings(self, setting_name, root_dir=None):
        """:method: for setting http settings by setting_name

        :param setting_name: string like `load_orders`, `inventory`
        :param root_dir: string for customize path_to_local_dir and path_to_backup_local_dir

        Usage::
            self.use_http_settings('load_orders')

        """
        if not self.customer and not root_dir:
            raise AttributeError('Not installed root_dir!')
        root_dir = root_dir or self.customer
        current_setting = {}
        for setting in self.multi_http_settings:
            if setting.action == setting_name:
                for key in setting.http_setting_id._columns.keys():
                    current_setting.update({
                        str(key): setting.http_setting_id[key],
                    })
                current_setting.update({
                    'setting_name': setting_name,
                    'root_dir': root_dir,
                    'method': setting.method,
                    'sub_url': setting.sub_url,
                    'params': setting.params,
                })
                break
        if current_setting:
            self.set_http_settings(current_setting)
        else:
            raise NotImplementedError('Couldn\'t found HTTP settings for {}'.format(setting_name))

    def set_http_settings(self, settings):
        """:method: for setup http connection settings from use_http_settings :method:"""
        self.path_to_local_dir = self.create_local_dir(
            self.input_local_path,
            settings['root_dir'],
            settings['setting_name']
        )
        self.path_to_backup_local_dir = self.create_local_dir(
            self.backup_local_path,
            settings['root_dir'],
            settings['setting_name'],
            "now"
        )
        self.copy_attributes_from_settings(settings)
