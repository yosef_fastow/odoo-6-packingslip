# -*- coding: utf-8 -*-
from mako_template import MakoTemplate
from os.path import join as os_path_join
from os.path import basename as os_path_basename
from datetime import datetime
from openerp.addons.pf_utils.utils.backuplocalfileutils import get_list_files
from os import remove as os_remove
from os import access, F_OK, R_OK
from openerp.addons.pf_utils.utils.re_utils import date_range_string
from os import symlink as os_symlink
import traceback
import logging
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.pf_utils.utils.decorators import to_ascii
import re
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


class BaseApiClient(object):

    def __init__(self):
        self.logger = _logger

    def copy_attributes_from_settings(self, settings):
        for attribute_name in settings:
            if attribute_name:
                exec('self.{attr_name} = settings["{attr_name}"]'.format(attr_name=attribute_name))

    def get_root_dir(self, settings):
        if (settings.get("customer_root_dir")):
            root_dir = str(settings["customer_root_dir"])
        else:
            root_dir = str(self.customer)
        return root_dir

    def get_gnupg(self, gnupghome):
        import gnupg
        try:
            gpg = gnupg.GPG(gnupghome=gnupghome)
        except TypeError:
            gpg = gnupg.GPG(homedir=gnupghome)
        finally:
            return gpg

    def use_mako_templates(self, name):
        if(self.type_tpl == 'file'):
            self.tpl = \
                os_path_join(
                    self.current_dir,
                    "mako_templates",
                    self.customer,
                    "%s.mako" % name
                )
        current_mako_tpl = {}
        for mako_tpl in self.mako_templates:
            if(mako_tpl.action == name):
                current_mako_tpl = mako_tpl.tpl
                break
        if(current_mako_tpl):
            self.tpl = current_mako_tpl
        else:
            self.tpl = None
        if(isinstance(self.tpl, unicode)):
            self.tpl = str(self.tpl)
        self._mako_template = MakoTemplate(self.tpl, self.type_tpl)

    def append_to_revision_lines(self, line, type_of_line):
        if(not hasattr(self, 'revision_lines')):
            self.revision_lines = {
                'bad': [],
                'good': []
            }
        else:
            if(type_of_line in ['bad', 'good']):
                self.revision_lines[type_of_line].append(line)
            else:
                self.log({
                    'title': "append_to_revision_lines {0}".format(self.customer),
                    'type': 'debug',
                    'message': 'bad argument type_of_line: {0}'.format(type_of_line),
                })

    def append_to_logger(self, msg_type, message):
        result = None
        msg_type = str(msg_type).lower()
        if(msg_type in ['debug', 'warning', 'error']):
            if(msg_type == 'debug'):
                self.logger.debug(message)
            elif(msg_type == 'warning'):
                self.logger.warning(message)
            elif(msg_type == 'error'):
                self.logger.error(message)
            result = True
        else:
            result = False
        return result

    def append_to_log(self, title, message):
        self._log.append({
            'title': title,
            'msg': message,
            'create_date': "{:'%Y-%m-%d %H:%M:%S}".format(datetime.now())
        })
        return True

    def log(self, log_line=None):
        if(log_line):
            title = log_line.get('title', False)
            msg_type = log_line.get('type', False)
            message = log_line.get('message', '')
            if(msg_type):
                self.append_to_logger(msg_type, message)
            if(title):
                self.append_to_log(title, message)
        return True

    def create_local_dir(self, init_local_path=None, path_customer=None, path_action=None, when=None):
        to_join = [
            init_local_path or "temp",
            path_customer or "none_customer",
            path_action or "none_action",
        ]
        if when:
            to_join.append(datetime.now().strftime("%Y_%m_%d"))
        return os_path_join(*to_join)

    def set_decrypt_data(self, data):
        """Return decrypt download data"""
        return data

    def set_encrypt_data(self, data):
        # Return encrypt upload data
        # If encrypt_flag == True in backup_local_dir create decrypt file
        encrypt_flag = False
        return data, encrypt_flag

    def prepare_data(self, data):
        """
            Prepare data before send
        """
        if(self._mako_template):
            data = self._mako_template.render_data(**data)
        return data

    def read_new(self, check_ftp=False):
        orders_list = []
        filenames = get_list_files(self.path_to_local_dir)
        if(filenames):
            response = self.parse_files(filenames)
            orders_list = response.get('orders_list', [])
            _error_files = response.get('error_files', [])
            _errors = response.get('errors', {})
            processed_files = list(set(filenames) - set(_error_files))
            if (orders_list or processed_files):
                for filename in processed_files:
                    os_remove(os_path_join(self.path_to_local_dir, filename))
            for filename in _error_files:
                if re.compile('^(.+)~824~OUT-(.+)$').match(filename):
                    os_remove(os_path_join(self.path_to_local_dir, filename))
                _errors_for_file = _errors.get(filename, None)
                for _msg in _errors_for_file:
                    _title_msg = 'error for file: {0} ERROR!'.format(filename)
                    self.log({
                        'title': "GetFilesFromLocalFolder {0} ERROR!".format(self.customer),
                        'type': 'debug',
                        'message': '\n'.join([_title_msg, _msg]),
                    })
            return orders_list
        else:
            self.log({
                'title': "GetFilesFromLocalFolder {0} files in local folder not found".format(self.customer),
                'type': 'debug',
                'message': 'Files in local folder: {0} not found!'.format(self.path_to_local_dir),
            })

    def _add_error(self, _result, _filename, _error):
        if(_filename not in _result['error_files']):
            _result['error_files'].append(_filename)
        _errors_list = _result['errors'].get(_filename, None)
        if(_errors_list is None):
            _result['errors'][_filename] = []
        _result['errors'][_filename].append(_error)
        return _result

    @property
    def use_decrypt(self):
        if hasattr(self, '_use_decrypt'):
            return self._use_decrypt
        else:
            return False

    def parse_files(self, filenames=None):
        if(filenames is None):
            filenames = []
        parser = self.parser
        result = {
            'orders_list': [],
            'error_files': [],
            'errors': {}
        }
        msg = ''
        for filename in filenames:
            if(access(filename, F_OK)):
                if(access(filename, R_OK)):
                    _data = None
                    with open(filename, 'r') as fh:
                        _data = fh.read()
                    if (self.use_decrypt):
                        _data = self.set_decrypt_data(_data)
                        self.check_response(_data)
                    msg += ''.join(
                        [
                            'filename: ', str(filename),
                            '\ndata: \n', str(_data), '\n'
                        ]
                    )
                    if _data:
                        if (hasattr(_data, 'data') and _data.data):
                            _data = _data.data
                    if((_data is not None) and (_data != '')):
                        curr_result = parser.parse_data(_data)
                        curr_orders_list = curr_result['orders_list']
                        curr_errors = curr_result['errors']
                        if(len(curr_orders_list) > 0):
                            result['orders_list'].extend(curr_orders_list)
                        if(len(curr_errors) > 0):
                            for _err in curr_errors:
                                result = self._add_error(result, filename, _err)
                    else:
                        self.log({
                            'title': 'Empty file {0}!'.format(filename),
                            'type': 'warning',
                            'message': 'Empty file {0}!'.format(filename),
                        })
                else:
                    result = self._add_error(result, filename, "No access reading file!")
            else:
                result = self._add_error(result, filename, 'Not find file!')
        self.log({
            'title': "GetFilesFromLocalFolder {0}".format(self.customer),
            'type': 'debug',
            'message': 'files in local folder: \n{0}'.format(msg),
        })
        return result

    @property
    def path_to_local_dir(self):
        return self._path_to_local_dir

    @path_to_local_dir.setter
    def path_to_local_dir(self, value):
        self._path_to_local_dir = value

    @property
    def path_to_backup_local_dir(self):
        return self._path_to_backup_local_dir

    @path_to_backup_local_dir.setter
    def path_to_backup_local_dir(self, value):
        self._path_to_backup_local_dir = value

    @property
    def full_log(self):
        full_log = self._log[:]
        self._log = []
        return full_log

    @full_log.setter
    def full_log(self, full_log):
        raise Exception("You can't set property full_log, please use 'log' function instead")

    def getLog(self):
        self.log({
            'type': 'debug',
            'message': 'Function getLog deprecated please use full_log property',
        })
        return self.full_log

    @property
    def actual_local_filename(self):
        if (
            hasattr(self, 'filename_local') and
            self.filename_local
        ):
            result = self.filename_local
        else:
            result = self.filename
        if (not result):
            raise AttributeError("Not installed attribute filename!")
        return result

    @actual_local_filename.setter
    def actual_local_filename(self, value):
        raise AttributeError("You can't set property actual_local_filename!")

    @property
    def full_filename(self):
        if (hasattr(self, 'filename_local') and self.filename_local):
            return os_path_join(self._path_to_backup_local_dir, self.filename_local)
        else:
            return os_path_join(self._path_to_backup_local_dir, self.filename)

    @full_filename.setter
    def full_filename(self, value):
        raise Exception('You can not set property full_filename')

    # DELMAR-7
    # add a readable file for inventory
    # getter
    @property
    def full_readable_filename(self):
        if hasattr(self, 'filename_readable') and self.filename_readable:
            return os_path_join(self._path_to_backup_local_dir, self.filename_readable)
        else:
            return os_path_join(self._path_to_backup_local_dir, f_d('Readable-Inventory-%Y%m%d%H%M.csv', datetime.utcnow()))

    # setter
    @full_readable_filename.setter
    def full_readable_filename(self, value):
        raise Exception('You can not set property full_filename')
    # END DELMAR-7

    @property
    def reload_orders_dates(self):
        self._reload_orders_dates = date_range_string(
            self.reload_date_start,
            self.reload_date_end,
            input_fmt='%Y-%m-%d',
            output_fmt='%Y_%m_%d'
        )
        return self._reload_orders_dates

    @reload_orders_dates.setter
    def reload_orders_dates(self, value):
        raise AttributeError(
            '''
            You can\'t set property reload_orders_dates!
            Please fill reload_date_start and reload_date_end
            '''
        )

    @property
    def real_root_dir(self):
        if (
            hasattr(self, '_root_dir') and
            self._root_dir
        ):
            return self._root_dir
        else:
            if (
                hasattr(self, 'customer_root_dir') and
                self.customer_root_dir
            ):
                return self.customer_root_dir
            elif self.customer:
                return self.customer
            else:
                raise AttributeError('Not installed root_dir!')

    @real_root_dir.setter
    def real_root_dir(self, value):
        raise AttributeError('You can\'t set property real_root_dir!')

    def prepareUpdateQTYLines(self, lines):
        new_lines = []
        for line in lines:
            new_line = line
            if 'customer_price' in new_line:
                try:
                    new_line['customer_price'] = _try_parse(to_ascii(new_line['customer_price']), 'float')
                except Exception:
                    pass
            new_lines.append(new_line)
        return new_lines

    def reload_orders(self):
        reloaded_order_files = []
        not_reloaded_order_files = []
        dr = self.reload_orders_dates
        filenames = []
        load_orders_path = None
        if (
            hasattr(self, '_setting_action') and
            self._setting_action
        ):
            load_orders_path = self._setting_action
        else:
            load_orders_path = 'orders'
        base_backup_path = os_path_join(
            self.backup_local_path,
            self.real_root_dir,
            load_orders_path,
        )
        for date_backup_path in dr:
            backup_path = os_path_join(base_backup_path, date_backup_path)
            filenames.extend(get_list_files(backup_path))
        for src_filename in filenames:
            dst_filename = os_path_join(self._path_to_local_dir, os_path_basename(src_filename))[:-14]
            try:
                os_symlink(src_filename, dst_filename)
                reloaded_order_files.append(
                    '{src} -> {dst}'.format(
                        src=src_filename,
                        dst=dst_filename
                    )
                )
            except Exception:
                not_reloaded_order_files.append(
                    'Error create symlink: {src} -> {dst}\n{err}'.format(
                        src=src_filename,
                        dst=dst_filename,
                        err=str(traceback.format_exc())
                    )
                )
        self.log({
            'title': 'RELOAD ORDERS REPORT',
            'type': 'debug',
            'message':
            '\nRELOADED:\n' +
            '\n'.join(reloaded_order_files) +
            '\nNOT RELOADED:\n' +
            '\n'.join(not_reloaded_order_files),
        })
