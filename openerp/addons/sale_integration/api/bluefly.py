# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from customer_parsers import bluefly_input_xml_parser
from customer_parsers import bluefly_xml_parser
import logging
from datetime import datetime
from utils.configutils import Config
from utils.ediparser import check_none


_logger = logging.getLogger(__name__)


class BlueflyApi(FtpClient):

    def __init__(self, settings, section_name=None):
        super(BlueflyApi, self).__init__(settings)
        if section_name:
            _config = Config('%s/customers_settings/bluefly_settings.yaml' % self.current_dir)
            all_namespaces = _config.read()
            self.namespaces = all_namespaces[section_name]
            self.namespaces["customer_name"] = all_namespaces["customer_name"]

    def upload_data(self):
        return {'lines': self.lines}


class BlueflyApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(BlueflyApiClient, self).__init__(
            settings_variables, False, False, False)
        self.settings["order_type"] = 'order'
        self.load_orders_api = BlueflyApiGetOrdersXML

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        @return:
        """
        self.settings["order_type"] = order_type
        api = BlueflyApiGetOrdersXML(self.settings)
        if save_flag:
            orders = api.process('load')
        else:
            orders = api.process('read')
        self.extend_log(api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = BlueflyApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = BlueflyApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        confirmApi = BlueflyApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res

    def orderAcknowledgement(self, lines, status=None):
        if not status:
            return False
        if status == "cancel":
            orderApi = BlueflyApiAcknowledgement(self.settings, lines, status)
            res = orderApi.process('send')
            self.extend_log(orderApi)
            return res
        return False

    def returnOrders(self, lines):
        returnApi = BlueflyApiReturnOrders(self.settings, lines)
        returnApi.process('send')
        self.extend_log(returnApi)
        return True


class BlueflyApiGetOrdersXML(BlueflyApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(BlueflyApiGetOrdersXML, self).__init__(settings, "get_orders_xml")
        self.order_type = settings['order_type']
        self.use_ftp_settings('load_orders')
        if(self.order_type == 'order'):
            self.use_ftp_settings('load_orders')
        else:
            self.use_ftp_settings('load_cancel_orders')
        self.options = self.namespaces["options"]

    def parse_response(self, response):
        input_xml_parser = \
            bluefly_input_xml_parser.BlueflyApiGetOrdersXML(self.order_type, self.options)
        ordersList = input_xml_parser.parse_response(response)
        return ordersList


class BlueflyApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        xml_parser = bluefly_xml_parser.BlueflyApiGetOrderObj()
        ordersObj = xml_parser.getOrderObj(self.xml)
        return ordersObj


class BlueflyApiConfirmShipment(BlueflyApi):

    def __init__(self, settings, lines):
        super(BlueflyApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.lines = lines
        self.filename = "{:ProductShippingOverrides-%Y-%m-%d-%H%M%S.xml}".format(datetime.now())
        self.type_tpl = "string"
        self.use_mako_templates('confirm')


class BlueflyApiUpdateQTY(BlueflyApi):

    def __init__(self, settings, lines):
        super(BlueflyApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.lines = lines
        self.filename = "{:ProductQuantities-%Y-%m-%d-%H%M%S.xml}".format(datetime.now())
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if check_none(line.get('customer_id_delmar', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
                continue
        return {'lines': self.revision_lines['good']}


class BlueflyApiAcknowledgement(BlueflyApi):

    def __init__(self, settings, lines, status):
        super(BlueflyApiAcknowledgement, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.lines = lines
        self.filename = "{:OrderStatusUpdate-%Y-%m-%d-%H%M%S.xml}".format(datetime.now())
        self.type_tpl = "string"
        if(status == "cancel"):
            self.use_mako_templates("cancel")
        else:
            self.use_mako_templates("acknowledgement")


class BlueflyApiReturnOrders(BlueflyApi):

    def __init__(self, settings, lines):
        super(BlueflyApiReturnOrders, self).__init__(settings)
        self.use_ftp_settings('return_orders')
        self.lines = lines
        self.filename = "{:NewRMA-%Y-%m-%d-%H%M%S.xml}".format(datetime.now())
        self.type_tpl = "string"
        self.use_mako_templates("return")
