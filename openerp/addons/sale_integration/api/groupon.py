# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from customer_parsers import groupon_csv_parser
from apiopenerp import ApiOpenerp
import logging
from openerp.addons.pf_utils.utils.re_utils import f_d
from datetime import datetime

_logger = logging.getLogger(__name__)


class GrouponApi(FtpClient):

    def __init__(self, settings):
        super(GrouponApi, self).__init__(settings)

    def check_response(self, response):
        if (hasattr(response, 'ok')):
            if (not response.ok):
                raise Exception(str(response.stderr))

    def set_decrypt_data(self, data):
        res = data
        return res


class GrouponApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):

        super(GrouponApiClient, self).__init__(
            settings_variables, GrouponOpenerp, False, False)
        self.load_orders_api = GrouponApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if (save_flag):
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, serialized_order):
        ordersApi = YamlOrder(serialized_order)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        return []

    def confirmShipment(self, lines):
        confirmApi = GrouponApiConfirmOrders(self.settings, lines)
        confirmApi.process('send')
        self.extend_log(confirmApi)
        return True


class GrouponApiGetOrdersXML(GrouponApi):

    def __init__(self, settings):
        super(GrouponApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = groupon_csv_parser.GrouponApiGetOrdersXML(self)

    def parse_response(self, response):
        csv_parser = groupon_csv_parser.GrouponApiGetOrdersXML(self)
        ordersList = csv_parser.parse_response(response)
        return ordersList

class GrouponApiConfirmOrders(GrouponApi):

    def __init__(self, settings, lines):
        super(GrouponApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("GROUPONSHIPPED%Y%m%d%H%M%f.csv")

    def upload_data(self):
        for line in self.lines:
            dt = datetime.now()
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = dt.strftime("%d/%m/%Y")
            line['int_product_qty'] = int(line['product_qty'])
        return {'lines': self.lines}


class GrouponOpenerp(ApiOpenerp):

    def __init__(self):
        super(GrouponOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'customerCost': line['customerCost'],
            "product_id": False,
            "size_id": False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'additional_fields': [
            ]
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not line_obj.get('size_id', False):
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif(line.get('size', False)):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if(len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]
                        else:
                            line_obj['size_id'] = False
                            line['size'] = False
                    else:
                        line_obj['size_id'] = False
                if not line_obj.get('product_id', False):
                    if product:
                        line_obj["product_id"] = product.id
                        line_obj['cost'] = line.get('cost', False)
                        if not line_obj['cost'] or line_obj['cost'] == 0:
                            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                             context['customer_id'],
                                                                                             'Customer Price',
                                                                                             line_obj['size_id'])
                            if product_cost:
                                line['cost'] = product_cost
                                break
                            else:
                                line['cost'] = False
                                line_obj["notes"] = "Can't find product cost.\n"

        return line_obj

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        order_obj = {
            'additional_fields': additional_fields,
        }
        order_obj['latest_ship_date_order'] = None
        return order_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        res = {
            'Ref1': sale_order.po_number or '',
            'Ref2': ''
        }

        return res

