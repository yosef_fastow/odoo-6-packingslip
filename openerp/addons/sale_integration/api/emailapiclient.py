# -*- coding: utf-8 -*-
import traceback
import time
import logging
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from os.path import dirname as os_path_dirname
from baseapiclient import BaseApiClient


_logger = logging.getLogger(__name__)


class EmailApiClient(BaseApiClient):

    _log = []
    _mako_template = False

    _path_to_backup_local_dir = ''
    _path_to_local_dir = ''


    def __init__(self, settings=None):
        self.current_dir = os_path_dirname(__file__)
        if settings is not None:
            self.copy_attributes_from_settings(settings)
        self._log = []

    def process(self, method):
        res = ''
        try:
            if(method == 'send'):
                res = self.send()

        except Exception, e:
            msg = traceback.format_exc()
            _logger.error('Error method %s (%s): %s' % (method, self.customer, msg))
            self._log.append({
                'title': 'Error method %s (%s) error: %s' % (method, self.customer, str(e)),
                'msg': msg,
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })

        return res

    def send(self):
        file_name = self.filename
        upload_data = self.upload_data()
        if(self._mako_template):
            upload_data = self._mako_template.render_data(**upload_data)
        if(self._path_to_backup_local_dir):
            if(self.filename):
                blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, upload_data, 'w').create()

        self._log.append({
            'title': "SendFilesToEmail %s" % self.customer,
            'msg': 'filename: \n %s\n data: \n%s\n' % (str(file_name), str(upload_data)),
            'type': 'send',
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })
        if(upload_data):
            result = upload_data
        else:
            result = False
        return result
