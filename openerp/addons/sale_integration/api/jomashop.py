import os
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from customer_parsers.jomashop_csv_parser import CSV_parser
from utils import feedutils as feed_utils
from configparser import ConfigParser
from pf_utils.utils.re_utils import f_d
import logging
from datetime import datetime
from apiopenerp import ApiOpenerp

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class JomashopApi(FtpClient):

    def __init__(self, settings):
        super(JomashopApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/jomashop_settings.ini' % os.path.dirname(__file__))


class JomashopApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(JomashopApiClient, self).__init__(
            settings_variables, JomashopOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = JomashopApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def updateQTY(self, lines, mode=None):
        updateApi = JomashopApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        ordersApi = JomashopApiConfirmShipment(self.settings, lines)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

class JomashopApiGetOrdersXML(JomashopApi):

    def __init__(self, settings):
        super(JomashopApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSV_parser(customer=self.customer)


class JomashopApiUpdateQTY(JomashopApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(JomashopApiUpdateQTY, self).__init__(settings)

        self.use_ftp_settings('inventory')
        self.lines = lines
        self.filename = 'inv.csv'
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.revision_lines = {
            'bad': [],
            'good': [],
        }

    def upload_data(self):
        for line in self.lines:
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class JomashopApiConfirmShipment(JomashopApi):

    confirmLines = []

    def __init__(self, settings, lines):
        super(JomashopApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.confirmLines = lines
        self.filename = "Confirm%s.csv" % datetime.now().strftime('%Y%m%d-%H%M%S')

    def upload_data(self):
        data = '"OrderNumber","ShipDate","Product","Quantity","ShipMethod","Tracking#","Amount","ZukOrder","ZukItem","Invoice"\n'
        for line in self.confirmLines:
            dt = datetime.now()
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = dt.strftime("%m/%d/%Y %I:%M:%S %p")
            line['int_product_qty'] = int(line['product_qty'])
            line['amount_item'] = line['price_unit'] * line['int_product_qty']
            line['zuck_order'] = line.get('additional_fields', {}).get('zuck_order', '') or ''
            line['zuck_item'] = line.get('additional_fields', {}).get('zuck_item', '') or ''
            data += feed_utils.FeedUtills(self.config['ShippingConfirmation'].items(), line).create_csv_line(quote=True)
        return data


class JomashopOpenerp(ApiOpenerp):

    def __init__(self):
        super(JomashopOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = {
            "notes": "",
            "name": line['name'],
            "price_unit": line['cost'],
            "customerCost": line['customerCost'],
            "merchantSKU": line['merchantSKU'],
            "vendorSku": line['vendorSku'],
            "qty": line['qty'],
            'product_id': False,
        }
        product = {}
        product_obj = self.pool.get('product.product')
        field_list = ['vendorSku', 'merchantSKU', 'zuck_item']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not product and 'AMR-' in line[field]:
                    sku = line[field].replace('AMR-', '')
                    product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], sku)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if size_ids:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
        else:
            line_obj['notes'] = "Can't find product by sku %s.\n" % (line['merchantSKU'])
        additional_fields = {
            'additional_fields': [
            (0, 0, {'name': 'zuck_order', 'label': 'ZuckOrder', 'value': line.get('zuck_order', False)}),
            (0, 0, {'name': 'zuck_item', 'label': 'ZuckItem', 'value': line.get('zuck_item', False)})
            ]
        }
        line_obj.update(additional_fields)

        return line_obj

    def check_address(self, cr, uid, address_obj):

        return True if address_obj.get('country') in ['US'] else False
