<%!
    def get_date1(date):
        return date.strftime("%Y%m%d.%H%M%S")

    def get_date2(date):
        date_part = date.strftime("%Y-%m-%dT%H:%M:%S")
        zone_part = date.strftime("%z")
        zone_part = zone_part[:3] + ':' + zone_part[3:]

        return date_part + zone_part

    def escape_data(data):
        result = ''
        from xml.sax.saxutils import escape
        if(data):
            result = escape(str(data))
        return result
%><?xml version="1.0" encoding="UTF-8"?>
<ns:wmi xmlns:ns="http://www.walmart.com/2010/XMLSchema/MTEP/fulfillment/InventorySummary/version1.0">
    <ns:wmiheader>
        <fileid>${additional_info['vendor_id']}.${get_date1(additional_info['date'])}.${additional_info['rand']}</fileid>
        <version>5.0.1</version>
        <enterprisecode>CAN</enterprisecode>
        <messagetype>inventory_summary</messagetype>
        <gendate>${get_date2(additional_info['date'])}</gendate>
        <to>
            <toid>${additional_info['external_customer_id']}</toid>
            <toname>Walmart Canada</toname>
        </to>
        <from>
            <fromid>${additional_info['vendor_id']}</fromid>
            <fromname>Delmar International</fromname>
            <contactinfo>
                <contactname>Sam Cohen</contactname>
                <contactphone>
                    <phonenumber>800-738-8463</phonenumber>
                </contactphone>
                <emailaddress>
                    <primaryemail>wmit@delmarintl.ca</primaryemail>
                </emailaddress>
            </contactinfo>
        </from>
    </ns:wmiheader>
    <ns:wmiiteminventory>
        % for line in lines:
        % if line['customer_id_delmar'] and line['upc'] and line['customer_sku']:
        <ns:wininventory>
            <win>${escape_data(line['customer_id_delmar'])}</win>
            <upc>${escape_data(line['upc'])}</upc>
            <sku>${escape_data(line['customer_sku'])}</sku>
            <facilityid>${additional_info['vendor_id']}</facilityid>
            <datetime>${get_date2(additional_info['date'])}</datetime>
            <description>${escape_data(line['base_description'])}</description>
            <availability>
                <availabilitycode>${line['code_to_send']}</availabilitycode>
                <uom>EA</uom>
                <ats>${escape_data(line['qty']) or '0'}</ats>
                <trbl>0</trbl>
                <rip>0</rip>
                <wip>0</wip>
                <oip>0</oip>
                <eip>0</eip>
                <por>0</por>
                <poe>0</poe>
                <startdate>${get_date2(additional_info['date'])}</startdate>
                <mindays>0</mindays>
                <maxdays>1</maxdays>
            </availability>
        </ns:wininventory>
        % endif
        % endfor
    </ns:wmiiteminventory>
</ns:wmi>