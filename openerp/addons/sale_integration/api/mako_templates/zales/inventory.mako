Item Number Quantity Orderable  Unit Cost
% for line in lines:
${check_none(line['customer_id_delmar'])}   ${line['qty']}  ${line['customer_price']}
% endfor
<%!
    def check_none(value):
        return '' if value in [None, False, 'None', 'False'] else value.strip()
%>