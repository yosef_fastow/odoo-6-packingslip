<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns1:quantityFeed xmlns:ns1="http://production.jewelrytelevision.ws/vendor/quantity_feed.xsd">
    <ns1:vendorId>222</ns1:vendorId>
    <ns1:dateGenerated>${get_date()}</ns1:dateGenerated>
% for line in lines:
% if line.get('customer_id_delmar', False):
<ns1:productsWithQuantities>
    <ns1:vendorItemId>${line['customer_id_delmar']}</ns1:vendorItemId>
    <ns1:quantity>${line['qty']}</ns1:quantity>
    <ns1:price>${line['customer_price']}</ns1:price>
</ns1:productsWithQuantities>
% endif
% endfor
</ns1:quantityFeed>
<%!
    def get_date():
        import time
        from random import randrange

        return time.strftime("%Y-%m-%dT%I:%M:%S."+str(randrange(100,999)))
%>