"Brand","Style","Code#","Gender","MSRP","Wholesale","Code","O/H","Approx ETA","Price Change Date","New MSRP","New Wholesale Cost"
% for line in lines:
"Delmar Jewelry","${prepare_description(line)}","${line['customer_sku']}",,,,"${get_code(line)}","${line['qty']}","${convert_eta(line)}",,,
% endfor
<%!
    from datetime import datetime
    def convert_eta(line):
        eta_date = ''
        if line.get('eta', False) and line.get('qty', 0) in (0, '0'):
            try:
                if int(line['eta'][-4:]) >= 1900:
                    eta_date = datetime.strptime(line['eta'], "%m/%d/%Y").strftime("%Y-%m-%d")
            except:
                pass
        return eta_date
    def get_code(line):
        return line.get('dnr_flag', 0) and "Discontinued" or "Active"
    def prepare_description(line):
        description = line['description'] or ''
        return description.replace(',', ' ')
%>