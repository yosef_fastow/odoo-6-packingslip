OrderNo, VendorPartNo, Qty, ShipAgent, ShipMethod, TrackingNumber, ShipDate
% for line in lines:
${line['po_number'] or ''},${line['vendorSku']},${line['product_qty']},${line['carrier'].split(', ')[-1]},${line['shp_service']},${line['tracking_number']},${get_shp_date(line['shp_date'])}
% endfor
<%!
    def get_shp_date(input_date):
        from datetime import datetime
        shp_date = input_date
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        return shp_date.strftime('%m/%d/%Y')
%>