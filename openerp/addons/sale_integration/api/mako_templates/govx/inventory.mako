"SKU","UPC","Product Name","Color","Size","Weight","Stock Quantity","Wholesale Price","MSRP","Shipping Cost","MAP Price"
% for line in lines:
"${trim_length(line['customer_id_delmar'], 30)}","${trim_length(line['upc'], 21)}","${trim_length(replace_inside_double_quotes(line['description']), 60)}","","${trim_length(line['size'], 30)}","","${trim_length(line['qty'], 21, 0)}","${trim_length(line['customer_price'], 21)}","","",""
% endfor
<%!
def trim_length(string, length=21, default=""):
    if not string and default != "":
        return default
    if string in ['False', False, 'None', None]:
        string = ''
    res = str(string)
    if len(res) > length:
        try:
            res = res[:length]
        except Exception as ex:
            res = str(ex)
    return res

def replace_inside_double_quotes(string):
    if string:
        string = string.replace('"', '\'\'').replace(',', ' ')
    return string
%>