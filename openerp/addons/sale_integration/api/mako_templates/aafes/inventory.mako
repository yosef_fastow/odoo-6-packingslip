Type	Identifier	OnHandQty	OnHandDate	ExpectedQty	ExpectedDate	Discontinued
% for line in lines:
% if line.get('upc', False):
G	${line['upc']}	${line['qty']}	${line['feed_date']}	${get_eta_qty(line)}	${get_eta_date(line)}	${get_discontinued(line)}
% endif
% endfor
<%!
	def get_discontinued(line):
		discontinued = ''
		if (line['dnr_flag'] in (1, '1') and line['qty'] in (0, '0')):
			discontinued = 'Discontinued'
		return discontinued

	def get_eta_date(line):
		from datetime import datetime
		eta_date = ''
		if (line['eta'] and line['qty'] in (0, '0') and line['ordered_qty'] not in (0, '0')):
			try:
				if int(line['eta'][-4:]) >= 1900:
					eta_date = datetime.strptime(line['eta'], "%m/%d/%Y").strftime("%Y%m%d")
				else:
					raise ValueError('Too old ETA')
			except ValueError:
				pass
		return eta_date

	def get_eta_qty(line):
		from datetime import datetime
		eta_qty = ''
		if (line['eta'] and line['qty'] in (0, '0') and line['ordered_qty'] not in (0, '0')):
			try:
				if int(line['eta'][-4:]) >= 1900:
					eta_date = datetime.strptime(line['eta'], "%m/%d/%Y").strftime("%Y%m%d")
				else:
					raise ValueError('Too old ETA')
				eta_qty = line['ordered_qty']
			except ValueError:
				pass
		return eta_qty
%>