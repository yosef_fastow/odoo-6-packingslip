"sku","qty"
% for line in lines:
% if line.get('customer_sku', False) not in [None, 'None', False] and avaiable_size(line['size']) :
"${line['customer_sku']}","${line['qty']}"
% endif
% endfor
<%!
  def avaiable_size(size):
      try:
          size = float(size or 0.0)
      except (ValueError, TypeError):
          size = 0.0
      except:
          return None
      return not (size % 1.0)
%>
