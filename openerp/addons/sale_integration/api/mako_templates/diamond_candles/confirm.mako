"PO Number","Fulfiller Order Number","Fulfiller Sku","Quantity","Item Status","Ship to Name","Ship Date","Shipping Carrier","Shipping Method","Tracking Number"
% for line in lines:
"${line['po_number']}","${line['po_number']}","${line['vendorSku']}","${line['product_qty']}","SHIPPED","${line['ship_to_name']}","${line['date']}","${line['carrier']}","${line['carrier_code']}","${line['tracking_number']}"
% endfor