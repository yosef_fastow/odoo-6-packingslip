<Message VendorID="${vendor_number}" CreateDate="${date}">
    <Products>
% for line in lines:
% if line.get('customer_id_delmar', False):
        <Product>
            <VendorProduct_ID>${line['customer_id_delmar']}</VendorProduct_ID>
            <VendorProductP_ID>${get_parent_code(line['customer_id_delmar'])}</VendorProductP_ID>
            <QtyAvailable>${line['qty']}</QtyAvailable>
            <OrdersProcessed>N</OrdersProcessed>
        </Product>
% endif
% endfor
    </Products>
</Message>
<%!
    def get_parent_code(code):
        import re
        match = re.search(r"^(.*)[-\/]\d+$", code)
        return match.group(1) if match else code
%>