"customer id delmar","size","qty"
% for line in lines:
% if get_field(line['customer_id_delmar']):
"${line['customer_id_delmar']}","${get_field(line['size'])}","${line['qty']}"
% endif
% endfor
<%!
    def get_field(field):
        result = False
        if((field is None) or (field == 'None')):
            result = ''
        else:
            result = field
        return result
%>