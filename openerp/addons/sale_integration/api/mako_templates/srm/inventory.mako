<%! from datetime import datetime; now_date = "{:%Y/%m/%d %H:%M:%S %Z}".format(datetime.now()) %>"date","delmar_id","desc","price","qty"
% for line in lines:
% if line.get('customer_price', False):
"${now_date}","${line['sku']}","${line['description']}","${line['customer_price']}",${line['qty']}
% endif
% endfor