"PO_ID","InvoiceNo","Sku","FullName","ShippingWhse","CarrierName","OrderQty","Items","OrderCost","Tax","Total_Ship_Cost"
% for line in lines:
"${line['po_number']}","${line['invoice']}","${line['merchantSKU'] or ''}","${line['ship_to_name_1']}","${line['warehouse']}","${line['shp_service']}","${line['product_qty']}","${line['lineCount']}","${line['unit_cost']}","${amount_tax(line)}","${amount_total(line['unit_cost'], amount_tax(line))}"
% endfor
<%!

    def amount_tax(line):
        fmt = "{:.2f}"
        all_tax_value = 0.0
        for name_tax in ['gst', 'pst', 'qst', 'hst']:
            try:
                all_tax_value += float(line[name_tax])
            except Exception:
                pass
        return fmt.format(all_tax_value)

    def amount_total(unit_cost, amount_tax):
        res = 0.0
        fmt = "{:.2f}"
        try:
            res = float(unit_cost) + float(amount_tax)
        except ValueError:
            pass
        return fmt.format(res)
%>