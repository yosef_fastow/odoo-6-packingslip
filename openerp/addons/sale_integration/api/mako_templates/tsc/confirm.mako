"OrderNo","SkuNo","Quantity","TrackingNo","SMdesc","ShipDate"
% for line in lines:
"${line['external_customer_order_id']}","${line['merchantSKU']}","${line['product_qty']}","${line['tracking_number']}","${line['carrier']}","${convert_shp_date(line['shp_date'])}"
% endfor
<%!
    def convert_shp_date(date_str):
        from datetime import datetime
        try:
            _date = datetime.strptime(date_str, "%m/%d/%Y %I:%M:%S %p")
        except ValueError:
            _date = datetime.now()
        _date = _date.strftime("%Y%m%d")
        return _date
%>