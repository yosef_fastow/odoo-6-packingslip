"sku","qty"
% for line in lines:
% if line.get('customer_sku', False) not in [None, 'None', False]:
"${line['customer_sku']}","${line['qty']}"
% endif
% endfor