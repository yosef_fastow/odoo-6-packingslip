"sku","vendor_sku","qty"
% for line in lines:
% if get_field(line['model']) and  get_field(line['customer_sku']):
"${line['customer_sku']}","${get_field(line['model'])}","${line['qty']}"
% endif
% endfor
<%!
    def get_field(field):
        result = False
        if((field is None) or (field == 'None')):
            result = ''
        else:
            result = field
        return result
%>