"LBHSKU","VENDORSKU","AVAILABLE","EXPECTED"
% for line in lines:
% if line.get('customer_sku') not in [None, 'None', False] and line.get('customer_id_delmar') not in [None, 'None', False]:
"${line['customer_sku']}","${line['customer_id_delmar']}","${get_available_flag(line)}","${convert_eta(line)}"
% endif
% endfor
<%!
    from datetime import datetime
    def convert_eta(line):
        result = ''
        if line.get('po_eta') and line.get('qty', 0) in (0, '0'):
            try:
                eta = datetime.strptime(line['po_eta'], "%Y-%m-%d")
                if eta > datetime.now():
                    result = eta.strftime("%Y-%m-%d")
            except:
                pass
        return result
    def get_available_flag(line):
        return 'N' if line.get('qty', 0) in (0, '0') else 'Y'
%>