Order Id,Package Id,Tracking Number,Shipping Carrier,Shipping Date,Package Status
% for line in lines:
${line['po_number'] or ''},${get_package_id(line)},${line['tracking_number'] or ''},${line['shp_service'] or ''},${line['date']},Shipped
% endfor
<%!
    def get_package_id(line):
        return line.get('additional_fields', {}).get('package_id', '') or ''
%>
