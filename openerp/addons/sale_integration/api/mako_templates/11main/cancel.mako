Order Id,Package Id,Tracking Number,Shipping Carrier,Shipping Date,Package Status
% for line in lines:
${line['po_number'] or ''},${line['additional_fields']['package_id'] or ''},${line['tracking_number'] or ''},${line['shp_service'] or ''},${get_cancel_date()},Cancelled
% endfor
<%!
    def get_cancel_date():
        import time
        cancel_date = time.strftime('%m/%d/%y', time.gmtime())

        return cancel_date
%>