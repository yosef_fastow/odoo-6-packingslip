ItemID,Title,Description,Size,Quantity,Price,Image1,GroupProductID <% flines = result_filter(lines) %>
% for index, line in enumerate(flines):
    % if line['size'] != '' and insert_parent_line(line, flines[index-1 if index > 0 else 0]):
        ${get_group_product_id(line)},"${line['name']}","${line['description']}",YES,${line['qty']},${line['customer_price_formula']},${'http://delmarimage.com/images/'+decrease_size(line['sku'])+'.jpg'},
    % endif
    ${line['customer_id_delmar']},"${line['name']}","${line['description']}",${line['size']},${line['qty']},${line['customer_price_formula']},${'http://delmarimage.com/images/'+decrease_size(line['sku'])+'.jpg'},${get_group_product_id(line)}
% endfor
<%!
    def decrease_size(id_delmar):
        _index = id_delmar.rfind('/')
        if _index > 0 and '-' not in id_delmar[_index:] and '_' not in id_delmar[_index:]:
            id_delmar = id_delmar[0:_index]

        return id_delmar

    def get_group_product_id(line):
        if line['size'] != '':
            line_rfind = line['customer_id_delmar'].rfind('/')
            if line_rfind > 0:
                return line['customer_id_delmar'][:line_rfind]
        return ''

    def insert_parent_line(line, prev_line):
        if prev_line['customer_id_delmar'] == line['customer_id_delmar']:
            return True

        if prev_line['customer_id_delmar'] and line['customer_id_delmar']:
            line_rfind = line['customer_id_delmar'].rfind('/')
            prev_line_rfind = prev_line['customer_id_delmar'].rfind('/')
            if prev_line_rfind > 0 and line_rfind > 0:
                return line['customer_id_delmar'][:line_rfind] != prev_line['customer_id_delmar'][:prev_line_rfind]

        return True

    def result_filter(lines):
        result = []
        for line in lines:
            if (line['size'] == '' or (float(line['size']) >= 5 and float(line['size']) <= 9)) and line['customer_id_delmar']:
                if line['sku'] not in [None, 'None'] and len(line['customer_price_formula'] or '') > 0:
                    result.append(line)

        return result
%>
