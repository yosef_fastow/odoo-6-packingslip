<?xml version="1.0" encoding="UTF-8"?>
<order-adjustment-feed xmlns="http://seller.marketplace.sears.com/oms/v2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://seller.marketplace.sears.com/oms/v2 order-adjustment.xsd ">
  <date-time-stamp>${lines[0]['date-time-stamp']}</date-time-stamp>
  <order-adjustment>
    <oa-header>
      <po-number>${lines[0]['po-number']}</po-number>
      <po-date>${lines[0]['po-date']}</po-date>
    </oa-header>
    <oa-detail>
      %for line in lines:
      <sale-adjustment>
        <line-number>${line['line-number']}</line-number>
        <item-id>${line['item-id']}</item-id>
        <return>
        <return-unique-id>${line['return-unique-id']}</return-unique-id>
          <return-amount>
            <amount-in-dollars>${line['shipCost']}</amount-in-dollars>
          </return-amount>
          <return-reason>${line['return-reason']}</return-reason>
          <return-date>${line['return-date']}</return-date>
          <quantity>${line['quantity']}</quantity>
          <memo>
            <internal-memo>${line['return-reason']}</internal-memo>
            <external-memo>${line['return-reason']}</external-memo>
          </memo>
        </return>
      </sale-adjustment>
      % endfor
    </oa-detail>
  </order-adjustment>
</order-adjustment-feed>