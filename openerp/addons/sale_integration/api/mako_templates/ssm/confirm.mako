<?xml version="1.0" encoding="UTF-8"?>
<shipment-feed xmlns="http://seller.marketplace.sears.com/oms/v7"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://seller.marketplace.sears.com/oms/v7 asn.xsd ">
    %for line in lines:
    <shipment>
        <header>
            <asn-number>${line['asn-number']}</asn-number>
            <po-number>${line['po-number']}</po-number>
            <po-date>${line['po-date']}</po-date>
        </header>
        <detail>
            <tracking-number>${line['tracking-number']}</tracking-number>
            <ship-date>${line['ship-date']}</ship-date>
            % if line.get('shipping-carrier', False):
            <shipping-carrier>${line['shipping-carrier']}</shipping-carrier>
            % endif
            % if line.get('shipping-method', False):
            <shipping-method>${line['shipping-method']}</shipping-method>
            % endif
            <package-detail>
                <line-number>${line['line-number']}</line-number>
                <item-id>${line['item-id']}</item-id>
                <quantity>${line['quantity']}</quantity>
            </package-detail>
        </detail>
    </shipment>
    % endfor
</shipment-feed>