<?xml version="1.0" encoding="UTF-8"?>
<order-cancel-feed xmlns="http://seller.marketplace.sears.com/oms/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://seller.marketplace.sears.com/oms/v1 order-cancel.xsd ">
  <order-cancel>
    <header>
      <po-number>${lines[0]['po-number']}</po-number>
      <po-date>${lines[0]['po-date']}</po-date>
    </header>
    <detail>
      <line-number>${lines[0]['line-number']}</line-number>
      <item-id>${lines[0]['item-id']}</item-id>
      <cancel>
        <line-status>${lines[0]['line-status']}</line-status>
        <cancel-reason>${lines[0]['cancel-reason']}</cancel-reason>
      </cancel>
    </detail>
  </order-cancel>
</order-cancel-feed>
