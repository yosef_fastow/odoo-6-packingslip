<?xml version="1.0" encoding="UTF-8"?>
<store-inventory
        xmlns="http://seller.marketplace.sears.com/catalog/v7"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://seller.marketplace.sears.com/catalog/v7 ../../../../../rest/inventory/import/v7/store-inventory.xsd">
    % for line in lines:
        % if line['customer_id_delmar'] and line['customer_id_delmar'] != 'None':
    <item item-id="${line['customer_id_delmar']}">
        <locations>
            <location location-id="${line['location_id']}">
                <quantity>${line['qty']}</quantity>
                <low-inventory-threshold>${line['low_inventory_threshold']}</low-inventory-threshold>
                <pick-up-now-eligible>${line['pick_up_now_eligible']}</pick-up-now-eligible>
                <inventory-timestamp>${line['inventory_timestamp']}</inventory-timestamp>
            </location>
        </locations>
    </item>
        % endif
    % endfor
</store-inventory>