% for line in lines:
<supplierInventory>
    <partnerSku>${line['customer_id_delmar']}</partnerSku>
    <supplierWarehouseQuantity>
        <warehouseName>
            <code>Delmar IFC</code>
        </warehouseName>
        <quantity>${line['qty']}</quantity>
        <timestamp>${line['timestamp']}</timestamp>
        % if line['backorderTimestamp'] and line['backorderTimestamp'] is not None and line['backorderTimestamp'] != '':
        <backorderTimestamp>${line['backorderTimestamp']}</backorderTimestamp>
        % endif
    </supplierWarehouseQuantity>
</supplierInventory>
% endfor