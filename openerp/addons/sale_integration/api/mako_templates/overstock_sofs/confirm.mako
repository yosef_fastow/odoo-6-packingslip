<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<supplierShipmentMessage xmlns="api.supplieroasis.com">
    % for line in lines:
    <supplierShipment>
        <salesChannelName>${line['order_additional_fields'].get('salesChannelName', '')}</salesChannelName>
        <salesChannelOrderNumber>${line['order_additional_fields'].get('salesChannelOrderNumber', '')}</salesChannelOrderNumber>
        <salesChannelLineNumber>${line['additional_fields'].get('salesChannelLineNumber', '')}</salesChannelLineNumber>
        <warehouse>
            <code>Delmar IFC</code>
        </warehouse>
        <supplierShipConfirmation>
            <quantity>${line['product_qty']}</quantity>
            <carrier>
                <code>${line['carrier_code']}</code>
            </carrier>
            <trackingNumber>${line['tracking_number']}</trackingNumber>
            <shipDate>${line['timestamp']}</shipDate>
            <serviceLevel>
                <code>${line['servicelevel']}</code>
            </serviceLevel>
        </supplierShipConfirmation>
    </supplierShipment>
    % endfor
</supplierShipmentMessage>