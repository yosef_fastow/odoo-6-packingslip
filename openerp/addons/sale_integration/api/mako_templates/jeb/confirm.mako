<?xml version="1.0" encoding="utf-8"?>
<ShipmentStatusUpdateRequest version="1.00">
    <UDIParameter>
        <Parameter key="HTTPBizID">NwHaZjrYxMZiSgmvGGPRnyIUmrPtkgAEPAgpbfCiiispkJDneTkVIMixgBRTmQDljCKXEZrwnXqjjkUtRSNeKghBfgEoBgjUlcbDaeOwVsmhWYiioaIraUBnUUADIgXGiKGajFEpcgDAwPixIetquTubfdgsnUiRKTKiMqQqePGQrQOhYBsVvdIfxwwDdipOtcKQfulReNSSmhcYqQhLNOcRAvmVRoLLohiEkUYelFGrVxfyawLHhNLnkDVdffj</Parameter>
        <Parameter key="NotShippedFlag">True</Parameter>
        <Parameter key="AppendTrackingNumber">False</Parameter>
    </UDIParameter>
% for line in lines:
    <ShipmentConfirmation shipmentNumber="${line['po_number']}" type="ShipmentNumber">
        <Date>${line['date_now']}</Date>
        <TrackingNumber>${line['tracking_number']}   </TrackingNumber>
        <OverwriteSHCode>${line['shp_code']}</OverwriteSHCode>
    </ShipmentConfirmation>
% endfor
</ShipmentStatusUpdateRequest>