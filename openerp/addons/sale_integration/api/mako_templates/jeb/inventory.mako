"Delmar ID","Size","Stock"
% for line in lines:
"${get_sku_str(line)}","${get_size_str(line)}","${line['qty']}"
% endfor
<%!
    def get_sku_str(line):
        result = False
        if(line.get('sku_str', False)):
            result = line['sku_str']
        else:
            _sku = line['sku']
            if('/' in _sku):
                try:
                    _sku = _sku[:_sku.find("/")]
                except ValueError:
                    pass
            result = _sku
        return result
    def get_size_str(line):
        result = False
        if(line.get('size_str', False)):
            result = line['size_str']
        else:
            _sku = line['sku']
            _size_str = ''
            if('/' in _sku):
                try:
                    _size_str = str(float(_sku[_sku.find("/") + 1:])).replace('.0', '')
                except ValueError:
                    pass
            result = _size_str
        return result
%>