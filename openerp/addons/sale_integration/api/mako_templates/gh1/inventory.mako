"Customer ID Delmar","Size","Qty","Location"
% for line in lines:
    % if line.get('customer_id_delmar', False):
"${line['customer_id_delmar']}","${line['size']}","${line['qty']}","${get_location(line['uschp_qty'],line['qty'])}","${line['uschp_qty']}"
    % endif
% endfor
<%!
    def get_location(uschp_qty, total_qty):
        location = 'MTL'
        if uschp_qty >= 2 and total_qty >= 2:
            location = 'IFC'
        elif total_qty <= 0:
            location = ''
        return location
%>