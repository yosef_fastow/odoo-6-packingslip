"ProductID","Vendor name","PhysicalInventoryQty","InventoryDate"
% for line in lines:
% if line.get('sku', False):
"${line['sku']}","Delmar MFG.","${line['qty']}","${line['inventory_date']}"
% endif
% endfor