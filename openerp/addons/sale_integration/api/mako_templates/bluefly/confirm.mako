<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.ixtens.com/xml/mp/override/R1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ixtens.com/xml/mp/override/R1.1 http://support.ixtens.com/mp/R1.1/product/override.xsd">
% for line in lines:
  <product>
    <SKU>${line['vendorSku']}</SKU>
    <overrides>
      <override>
        <ShippingOptionName>shipping_option_currency</ShippingOptionName>
        <Fee currency="USD">
          <price>${line['gst']}</price>
        </Fee>
      </override>
    </overrides>
  </product>
% endfor
</feed>