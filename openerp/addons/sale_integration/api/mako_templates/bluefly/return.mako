<%!
from datetime import datetime
now_date = "{:%Y/%m/%d %H:%M:%S %Z}".format(datetime.now())
%>
<?xml version="1.0" encoding="UTF-8" ?>
 <RMAs xmlns="http://www.ixtens.com/xml/mp/RMA/R1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ixtens.com/xml/mp/RMA/R1.1 http://support.ixtens.com/mp/R1.1/order/RMAs.xsd">
% for line in lines:
  <RMA>
   <OrderID>${line['external_customer_order_id']}</OrderID>
   <RMA>${line['rma_number']}</RMA>
   <RMADate>${now_date}</RMADate>
   <ReturnReason>${line['return_reason']}</ReturnReason>
   <RMAItem>
    <OrderItemCode>${line['external_customer_line_id']}</OrderItemCode>
    <Quantity>${line['ordered_qty']}</Quantity>
   </RMAItem>
  </RMA>
% endfor
 </RMAs>