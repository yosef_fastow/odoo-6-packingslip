<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.ixtens.com/xml/mp/inventory/R1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ixtens.com/xml/mp/inventory/R1.1 http://support.ixtens.com/mp/R1.1/product/inventory.xsd">
% for line in lines:
% if line.get('customer_sku', False):
<product>
    <SKU>${line['customer_sku']}</SKU>
    <inventory>
      <quantity>${line['qty']}</quantity>
    </inventory>
</product>
% endif
% endfor
</feed>