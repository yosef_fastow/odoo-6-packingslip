<?xml version="1.0" encoding="UTF-8"?>
<OrderStatusUpdates xmlns="http://www.ixtens.com/xml/mp/orderack/R1.8" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ixtens.com/xml/mp/orderack/R1.8 http://support.ixtens.com/mp/R1.8/order/OrderStatusUpdate.xsd">
  <OrderStatusUpdate>
% for line in lines:
    <OrderID>${line['external_customer_order_id']}</OrderID>
    <Item>
        <OrderItemCode>${line['external_customer_line_id']}</OrderItemCode>
    <ItemStatusCode>Canceled</ItemStatusCode>
    </Item>
  </OrderStatusUpdate>
% endfor
</OrderStatusUpdates>