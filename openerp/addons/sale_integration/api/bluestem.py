# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from ftplib import FTP
import time
import cStringIO
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from abstract_apiclient import AbsApiClient
from datetime import datetime
from dateutil import tz
import logging
from utils.ediparser import EdiParser
import random
import json
from apiopenerp import ApiOpenerp
from customer_parsers.base_edi import BadSenderError

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',           'Vendor number',            '042132'),
    ('vendor_name',             'Vendor name',              'DELMAR'),
    ('sender_id',               'Sender Id',                '5148752800'),
    ('sender_id_qualifier',     'Sender Id Qualifier',      '12'),
    ('receiver_id',             'Receiver Id',              'TST1BLUESTEM'),
    ('receiver_id_qualifier',   'Receiver Id Qualifier',    'ZZ'),
    ('contact_name',            'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',           'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',         'EDI X12 Version',          '4010'),
    ('filename_format',         'Filename Format',          'DEL{edi_type}_{date}.out'),
    ('line_terminator',         'Line Terminator',          r'\n'),
    ('repetition_separator',    'Repetition Separator',     '}'),
    ('environment_mode',        'Environment Mode',         'P'),
)
# name: value
DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class BluestemApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, EdiParser)


class BluestemApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(BluestemApiClient, self).__init__(
            settings_variables, BluestemOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = BluestemApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        @return:
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = BluestemApiConfirmLoad(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def getOrdersObj(self, order_json):
        ordersApi = BluestemApiGetOrderObj(order_json)
        order = ordersApi.getOrderObj()
        return order

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = BluestemApiProcessingOrderLines(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        ordersApi = BluestemApiConfirmShipment(self.settings, lines)
        res = ordersApi.process('send')
        self.extend_log(ordersApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = BluestemApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class BluestemApiGetOrders(BluestemApi):

    number_of_transaction = ''
    ack_control_number = ''

    def __init__(self, settings):
        super(BluestemApiGetOrders, self).__init__(settings)

        self.use_ftp_settings('load_orders')
        self.functional_group = ''
        self.ack_control_number = ''

    def parse_response(self, data=None):
        if bool(data) is False:
            return []

        orders = []
        for edi in data:
            parse_orders = self.getOrderObj(edi)
            if parse_orders is not False:
                for order in parse_orders:
                    orderObj = {
                        'name': order['poNumber'],
                        'ack_control_number': self.ack_control_number,
                        'number_of_transaction': self.number_of_transaction,
                        'functional_group': self.functional_group
                    }
                    if order['Cancellation'] is True:
                        orderObj['name'] = 'CANCEL ' + orderObj['name']

                    orderObj['xml'] = json.dumps(order)
                    orders.append(orderObj)
        return orders

    def check_sender_is_correct(self, edi_data):
        result = True
        if (not edi_data):
            result = False
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    break
        except BadSenderError:
            result = False
        except Exception:
            result = None
        return result

    def getOrderObj(self, edi):
        if (not edi):
            return {}

        message = EdiParser(edi)
        return_orders = []

        for segment in message:
            elements = segment.split(message.delimiters[1])
            if elements[0] == 'GS':
                self.ack_control_number = elements[6]
                self.functional_group = elements[1]
            elif elements[0] == 'ST':
                ordersObj = {'address': {}, 'partner_id': 'Fingerhut', 'lines': [], 'Cancellation': False}
                address_type = ''
                if elements[1] == '850':  # Order
                    pass
                elif elements[1] == '860':  # Change
                    pass
                elif elements[1] == '997':  # Aknowlegment skip this file
                    return False
            elif elements[0] == 'BCH':   # Change
                if elements[1] == '01':  # Cancellation
                    ordersObj['Cancellation'] = True
                elif elements[1] == '04':  # Change
                    pass
                ordersObj['poNumber'] = elements[3]
                ordersObj['external_date_order'] = elements[5]
            elif elements[0] == 'BEG':
                ordersObj['poNumber'] = elements[3]
                ordersObj['external_date_order'] = elements[5]
            elif elements[0] == 'REF':
                if elements[1] == 'ST':  # Store Number
                    ordersObj['Division'] = elements[2]  # Division name for packing slip
                    ordersObj['partner_id'] = elements[2]  # Division means customer too
                elif elements[1] == 'CO':  # Customer Order Number
                    ordersObj['order_id'] = elements[2]
            elif elements[0] == 'SAC':
                if elements[1] == 'C' and elements[2] == 'G830':  # Customer’s shipping and handling
                    ordersObj['shipping_tax'] = elements[5]
            elif elements[0] == 'TD5':
                if len(elements) >= 6:
                    ordersObj['carrier'] = elements[5]
                elif len(elements) >= 4:
                    ordersObj['carrier'] = elements[3]
            elif elements[0] == 'AMT':
                if elements[1] == 'OJ':  # Merchandise
                    ordersObj['merchandise_amount'] = elements[2]
                elif elements[1] == 'TT':  # Total Transaction Amount
                    ordersObj['amount_total'] = elements[2]
            elif elements[0] == 'N1':
                if elements[1] in ['BT', 'VN']:  # Bill-to-Party, Vendor
                    address_type = ''
                elif elements[1] == 'SO':  # Sold To If Different From ShipTo
                    address_type = 'order'
                    ordersObj['address']['order'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['order']['name'] = elements[2]
                elif elements[1] == 'ST':  # Ship To
                    address_type = 'ship'
                    ordersObj['address']['ship'] = {
                        'name': '',
                        'name2': '',
                        'address1': '',
                        'address2': '',
                        'city': '',
                        'state': '',
                        'zip': '',
                        'country': '',
                        'phone': '',
                        'email': ''
                    }
                    ordersObj['address']['ship']['name'] = elements[2]
            elif elements[0] == 'N2' and address_type != '':
                ordersObj['address'][address_type]['name2'] = elements[1]
            elif elements[0] == 'N3' and address_type != '':
                ordersObj['address'][address_type]['address1'] = elements[1]
                if len(elements) > 2:
                    ordersObj['address'][address_type]['address2'] = elements[2]
            elif elements[0] == 'N4' and address_type != '':
                ordersObj['address'][address_type]['city'] = elements[1]
                ordersObj['address'][address_type]['state'] = elements[2]
                ordersObj['address'][address_type]['zip'] = elements[3]
                ordersObj['address'][address_type]['country'] = 'US'
                if len(elements) == 5:
                    ordersObj['address'][address_type]['country'] = elements[4]
            elif elements[0] == 'PO1':
                lineObj = {
                    'id': elements[1],
                    'qty': elements[2],
                    'cost': elements[4],
                    'sku': elements[7],
                    'vendorSku': elements[7],
                    'upc': elements[9],
                    'merchantSKU': elements[13],
                    'customer_sku': elements[11],
                    'optionSku': elements[11],
                    'name': elements[11]
                }
                if len(elements) > 15 and elements[7] and elements[15] and elements[7].endswith('J'+str(elements[15])):
                    lineObj['size'] = elements[15]
                ordersObj['lines'].append(lineObj)
            elif elements[0] == 'PID':
                if elements[2] == '74':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['size'] = elements[3]
            elif elements[0] == 'POC':  # Change
                if elements[2] == 'DI':  # Delete Item(s)
                    ordersObj['lines'].append(elements[1])
            elif elements[0] == 'GE':
                self.number_of_transaction = elements[1]
            elif elements[0] == 'SE':
                return_orders.append(ordersObj)

        return return_orders


class BluestemApiConfirmLoad(BluestemApi):

    orders = []

    def __init__(self, settings, orders):
        super(BluestemApiConfirmLoad, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def send(self):
        upload_data = self.upload_data()

        ftp = FTP(self._ftp_host)
        ftp.login(self._ftp_user, self._ftp_passwd)
        ftp.cwd(self._path_to_ftp_dir)

        for data in upload_data:
            data = self.prepare_data(data)
            IO = cStringIO.StringIO(str(data))
            file_name = self.filename
            if getattr(self, 'filename_local', ''):
                file_name = self.filename_local

            blf_utils.file_for_writing(self._path_to_backup_local_dir, file_name, str(IO.getvalue()), 'w').create()

            ftp.storbinary('STOR ' + self.filename, IO)

            self._log.append({
                'title': "SendFilesToFTP %s" % self.customer,
                'msg': 'filename: \n %s\n data: \n%s\nencrypt: %s\n' % (
                    str(file_name), str(upload_data), str(upload_data)),
                'type': 'send',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })

        try:
            ftp.quit()
        except EOFError:
            ftp.close()

        return True

    def upload_data(self):

        numbers = []
        data = []
        for order in self.orders:
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'

                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class BluestemApiGetOrderObj():

    order_json = ''

    def __init__(self, order_json):
        self.order_json = order_json

    def getOrderObj(self):
        if bool(self.order_json) is False:
            return {}

        return json.loads(self.order_json)


class BluestemApiProcessingOrderLines(BluestemApi):

    processingLines = []

    def __init__(self, settings, lines, state):
        super(BluestemApiProcessingOrderLines, self).__init__(settings)
        self.use_ftp_settings('confirm_orders')
        self.processingLines = lines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel'):
            return self.accept_cancel()

        self.edi_type = '855'

        lines = []
        statuscode = 'AK'
        if self.state == 'accepted':
            statuscode = 'AK'  # conveys that you are acknowledging the entire purchase order and there are no changes.
        elif self.state == 'cancel_line':
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.
        elif self.state == 'cancel':
            lines = self.processingLines
            self.processingLines = self.processingLines[0]['accept_information']
            statuscode = 'AC'  # conveys that you are acknowledging the order but changes need to be made to the order.
            #statuscode = 'RJ'  # conveys that you are acknowledging the order but changes need to be made to the order.

        segments = []

        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s*[5]*[6]*[7]*[8]*[9]%(ackn_date)s*DS'

        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.utcnow().strftime('%Y%m%d')

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
            'ackn_date': self.processingLines['date'],  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'CO',  # Customer Order Number
            'order_number': self.processingLines['external_customer_order_id'],
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        n1 = 'N1*[1]VN*[2]%(vendor_name)s'  # VN Vendor
        segments.append(self.insertToStr(n1, {
            'vendor_name': self.vendor_name,
        }))

        n3 = 'N3*[1]%(address1)s'

        address1 = self.processingLines['ship_to_address']['address1']
        if (not address1):
            raise Exception('Ship to address is required for 855(Purchase Order Acknowledgment)!')
        segments.append(self.insertToStr(n3, {
            'address1': address1,
        }))

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        segments.append(self.insertToStr(n4, {
            'city': self.processingLines.get('city', ''),
            'state': self.processingLines.get('state', ''),
            'zip': self.processingLines.get('zip', ''),
            'country': self.processingLines.get('country', ''),
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(optionSku_qualifier)s*[7]%(optionSku)s*[8]%(merchantSKU_qualifier)s*[9]%(merchantSKU)s*[10]%(optionSku_qualifier)s*[11]%(optionSku)s"
        po1_size = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(optionSku_qualifier)s*[7]%(optionSku)s*[8]%(merchantSKU_qualifier)s*[9]%(merchantSKU)s*[10]%(optionSku_qualifier)s*[11]%(optionSku)s*[12]*[13]*[14]SM*[15]%(size)s"
        ack = "ACK*[1]%(line_status)s*[2]%(qty)s*[3]EA*[4]068*[5]%(date)s"
        i = 1
        date_now = datetime.utcnow()
        for line in lines:
            template = po1
            vendorSku = line['vendorSku'] or ''
            vendorSku_qualifier = vendorSku and 'VP' or ''
            merchantSKU = line['merchantSKU'] or ''
            merchantSKU_qualifier = merchantSKU and 'UK' or ''
            optionSku = line['optionSku'] or ''
            optionSku_qualifier = optionSku and 'SK' or ''
            insert_obj = {
                'line_number': str(i),
                'qty': line['product_qty'],
                'unit_price': line['price_unit'],
                'vendorSku_qualifier': vendorSku_qualifier,
                'vendorSku': vendorSku,
                'merchantSKU_qualifier': merchantSKU_qualifier,
                'merchantSKU': merchantSKU,
                'optionSku': optionSku,
                'optionSku_qualifier': optionSku_qualifier,
            }

            if line['size'] is not False:
                line['size'] = line['size'].replace('.', '')
                template = po1_size
                insert_obj['size'] = ('0' * (4 - len(line['size']))) + line['size']

            segments.append(self.insertToStr(template, insert_obj))
            segments.append(self.insertToStr(ack, {
                'line_status': 'IR',  # Item Rejected
                'qty': line['product_qty'],
                'date': date_now.strftime('%Y%m%d'),  # CCYYMMDD
            }))
            i += 1

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '865'

        segments = []

        st = 'ST*[1]865*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA*[1]01*[2]%(statuscode)s*[3]%(po_number)s*[4]*[5]*[6]%(po_date)s'

        date = datetime.strptime(self.processingLines[0]['create_date'], '%Y-%m-%d %H:%M:%S.%f')

        statuscode = 'AT'
        if self.state == 'accept_cancel':
            statuscode = 'AT'
        elif self.state == 'rejected_cancel':
            statuscode = 'RJ'

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
            #'ackn_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CA'})


class BluestemApiConfirmShipment(BluestemApi):

    confirmLines = []

    def __init__(self, settings, lines):
        super(BluestemApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '06',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]*[2]2*[3]%(carrier)s*[4]*[5]%(carrier)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier_code']
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(data)s'

        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Bill of Lading Number
        #     'data': 'NEED_DATA'
        # }))
        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Master Bill of Lading (Common Carrier shipments only)
        #     'data': 'NEED_DATA'
        # }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            'code': '011',  # ???
            'date': shp_date.strftime('%Y%m%d')
        }))

        n1 = 'N1*[1]%(code)s*[2]%(data)s'
        segments.append(self.insertToStr(n1, {
            'code': 'SF',  # Ship From
            'data':  self.confirmLines[0]['shipFrom']['name']
        }))

        n3 = 'N3*[1]%(address1)s'

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['shipFrom']['street']
        }))

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['shipFrom']['city'],
            'state': self.confirmLines[0]['shipFrom']['state'],
            'zip': self.confirmLines[0]['shipFrom']['zip'],
            'country': self.confirmLines[0]['shipFrom']['country']
        }))

        segments.append(self.insertToStr(n1, {
            'code': 'ST',  # Ship To
            'data': self.confirmLines[0]['ship_to_name_1']
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['ship_to_address_line_1']
        }))

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['ship_to_city'],
            'state': self.confirmLines[0]['ship_to_state'],
            'zip': self.confirmLines[0]['ship_to_postal_code'],
            'country': self.confirmLines[0]['ship_to_country']
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'data': self.vendor_number
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'IK',  # Invoice Number
            'data': self.confirmLines[0]['invoice']
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'CO',  # Customer Order Number
            'data': self.confirmLines[0]['external_customer_order_id']
        }))

        segments.append(self.insertToStr(dtm, {
            'code': '003',
            'date': shp_date.strftime('%Y%m%d')
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'P'  # Pack
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        lin = 'LIN*[1]%(line_id)s*[2]%(code_SK)s*[3]%(sku)s*[4]%(code_UP)s*[5]%(upc)s*[6]%(code_VP)s*[7]%(optionSku)s'
        sn = 'SN1*[1]*[2]%(qty)s*[3]EA'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))
            segments.append(self.insertToStr(lin, {
                'line_id': line['external_customer_line_id'],
                'code_SK': bool(line['optionSku']) and 'SK' or '',
                'sku': line['vendorSku'],
                'code_UP': bool(line['merchantSKU']) and 'UK' or '',
                'upc': line['merchantSKU'],
                'code_VP': bool(line['vendorSku']) and 'VP' or '',
                'optionSku': line['optionSku']
            }))
            segments.append(self.insertToStr(sn, {
                'qty': line['product_qty']
            }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class BluestemApiUpdateQTY(BluestemApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(BluestemApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bia = 'BIA*[1]00*[2]MB*[3]%(reference_identification)s*[4]%(date)s'
        shp_date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'reference_identification': '5148754800',
            'date': shp_date.strftime('%Y%m%d')
        }))

        ref = 'REF*[1]IA*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'data': self.vendor_number,
        }))

        per = 'PER*[1]%(code)s*[2]%(name)s*[3]%(com_number_code)s*[4]%(com_number)s'
        segments.append(self.insertToStr(per, {
            'code': 'IC',  # Information Contact
            'name': self.contact_name,
            'com_number_code': 'TE',  # Telephone
            'com_number': self.contact_phone
        }))

        n1 = 'N1*[1]%(code)s*[2]%(name)s'
        segments.append(self.insertToStr(n1, {
            'code': 'VN',  # Vendor
            'name': 'DELMAR'
        }))

        lin = 'LIN*[1]*[2]SK*[3]%(customer_sku)s*[4]UP*[5]%(upc)s*[6]UK*[7]%(customer_id_delmar)s'
        pid = 'PID*[1]%(code)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(desc)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        for line in self.updateLines:
            if not (line['upc'] and line['customer_id_delmar'] and line['customer_sku']):
                self.append_to_revision_lines(line, 'bad')
                continue

            eta_date = False
            try:
                if line.get('eta', False):
                    tmp_date = datetime.strptime(line['eta'], "%m/%d/%Y").replace(tzinfo=self.tz)
                    if tmp_date > shp_date:
                        eta_date = tmp_date
            except Exception:
                pass

            self.append_to_revision_lines(line, 'good')

            segments.append(self.insertToStr(lin, {
                'upc': '0'*(12 - len(line['upc'])) + line['upc'][-12:],
                'customer_id_delmar': '0'*(14 - len(line['customer_id_delmar'])) + line['customer_id_delmar'][-14:],
                'customer_sku': line['customer_sku'],
            }))

            if line.get('size'):
                # 08 - Product
                # 74 - Vendor size description
                # 75 - Buyer's Color Description
                segments.append(self.insertToStr(pid, {
                    'code': 'F',  # Free-form
                    'characteristic_code': '74',
                    'desc': line['size']
                }))

            segments.append(self.insertToStr(qty, {
                'code': '33',  # Quantity Available for Sale (stock quantity)
                'qty': str(line['qty']),  # Product
                'uof_code': 'EA'  # Each
            }))

            # When DTM01=018
            # This date should be "today's date", unless the Qty Available for Sale is zero. In that case the
            # Availability Date must be past Today's Date indicating when that product will be available
            # again.
            # Date the product is being discontinued if applicable(DTM01=036)
            dtm_code = '018'  # Available
            dtm_date = shp_date
            if line['qty'] in (0, '0'):
                if (line.get('dnr_flag', '0') in (1, '1')):
                    dtm_code = '036'  # Discontinued
                if eta_date:
                    dtm_date = eta_date

            segments.append(self.insertToStr(dtm, {
                'code': dtm_code,
                'date': dtm_date.strftime('%Y%m%d')
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class BluestemApiInvoice(BluestemApi):

    invoiceLines = []

    def __init__(self, settings, lines):
        super(BluestemApiInvoice, self).__init__(settings)
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bia = 'BIG*[1]date*[2]%(invoice_number)s*[3]*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        segments.append(self.insertToStr(bia, {
            'date': invoice_date.strftime('%Y%m%d'),
            'po_number': self.invoiceLines[0]['po_number']  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
        }))

        ref = 'REF*[1]IA*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'data': self.vendor_number
        }))

        itd = 'ITD*[1]*[2]*[3]%(terms_discount_percent)s*[4]*[5]%(terms_discount_days_due)s*[6]*[7]%(terms_net_days)s*[8]%(terms_discount_amount)s*[9]*[10]*[11]*[12]%(description)s'
        segments.append(self.insertToStr(itd, {
            'terms_discount_percent': 'NEED_DATA',  # Terms discount percentage, expressed as a percent, available to the purchaser if an invoice is paid on or before the Terms Discount Due Date
            'terms_discount_days_due': 'NEED_DATA',  # Number of days in the terms discount period by which payment is due if terms discount is earned
            'terms_net_days': 'NEED_DATA',  # Number of days until total invoice amount is due (discount not applicable)
            'terms_discount_amount': 'NEED_DATA',  # Total amount of terms discount
            'description': 'NEED_DATA'  # A free-form description to clarify the related data elements and their content
        }))

        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]SK*[7]%(vendorSku)s*[8]*[9]*[10]VP*[11]%(merchantSKU)s*[12]UK*[13]%(optionSku)s*[14]%(size_code)s*[15]%(size)s'
        ctp = 'CTP*[1]*[2]*[3]*[4]*[5]*[6]*[7]*[8]%(line_price)s'
        line_number = 0
        for line in self.invoiceLines:
            line_number += 1

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(line['qty']),
                'unit_price': line['unit_price'],
                'vendorSku': line['vendorSku'],
                'merchantSKU': line['merchantSKU'],
                'optionSku': line['optionSku'],
                'size_code': 'SM',  # National Retail Merchants Association Size Code
                'size': line['size']
            }))

            segments.append(self.insertToStr(ctp, {
                'line_price': line['line_price']
            }))

        tds = 'TDS*[1]%(total_amount)s*[2]%(total_amount_with_descount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': self.invoiceLines[0]['total_amount'],  # Total Invoice Amount (including charges, less allowances)
            'total_amount_with_descount': self.invoiceLines[0]['total_amount']  # Amount subject to Terms Discount (Merchandise Total)
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number)  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class BluestemOpenerp(ApiOpenerp):

    def __init__(self):
        super(BluestemOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields': [
                (0, 0, {
                    'name': 'merchandise_amount',
                    'label': 'Merchandise amount',
                    'value': order.get('merchandise_amount', '0')
                })
            ]
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'merchantSKU': line['merchantSKU'],
            'optionSku': line['optionSku'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty']

        }
        product = {}
        field_list = ['vendorSku', 'merchantSKU', 'optionSku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[
                                                                                          field],
                                                                                      ('default_code', 'customer_sku',
                                                                                       'upc')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        result = {
            'po_number': sale_order['po_number'],
            'external_customer_order_id': sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR',
            'address1': '',
            'city': '',
            'state': '',
            'zip': '',
            'country': '',
            'ship_to_address': {
                'edi_code': 'ST',
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'country': '',
                'zip': '',
            },
        }
        if vendor_address:
            result['address1'] = vendor_address.street
            result['city'] = vendor_address.city
            result['state'] = vendor_address.state_id.code
            result['zip'] = vendor_address.zip
            result['country'] = vendor_address.country_id.code

        if (sale_order.partner_shipping_id):
            result['ship_to_address'].update({
                'name': sale_order.partner_shipping_id.name or '',
                'address1': sale_order.partner_shipping_id.street or '',
                'address2': sale_order.partner_shipping_id.street2 or '',
                'city': sale_order.partner_shipping_id.city or '',
                'state': sale_order.partner_shipping_id.state_id.code or '',
                'country': sale_order.partner_shipping_id.country_id.code or '',
                'zip': sale_order.partner_shipping_id.zip or '',
            })

        return result

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['poNumber'], tuple(customer_ids)))

        return cr.dictfetchall()

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        #Bluestem "going live" post action - add customer sku to ref2
        #Add customer sku to ref2

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        customer_skus = [line.vendorSku for line in sale_order.order_line if line.vendorSku]
        customer_skus_str = ' ,'.join(customer_skus)
        res = {
            'Ref2': customer_skus_str[:30] or ''
        }
        return res
