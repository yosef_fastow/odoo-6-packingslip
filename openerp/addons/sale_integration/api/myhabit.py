import logging
import random
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from edi_objects import BaseEDIClient
from ftpapiclient import FtpClient
from customer_parsers.myhabit_input_edi_parser import EDIParser
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


_logger = logging.getLogger(__name__)


DATE_FORMAT = '%Y%m%d'
TIME_FORMAT = '%H%M%S'


SETTINGS_FIELDS = (
    ('repetition_separator',         'Repetition Separator',         '^'),
    ('line_terminator',              'Line Terminator',              r'~'),

    ('sender_id',                    'SenderId',                     'DELMAR'),
    ('sender_id_qualifier',          'SenderIdQualifier',            'ZZ'),

    ('receiver_id',                  'ReceiverId',                   'AMAZONDS'),
    ('receiver_id_qualifier',        'ReceiverIdQualifier',          'ZZ'),

    ('edi_x12_version',              'EDIX12Version',                '5010'),
    ('environment_mode',             'EnvironmentMode',              'P'),

    ('vendor_payee_code',            'Vendor Payee Code',            'ABCD1'),

    ('terms_net_days',               'Terms Net Days',               '5'),
    ('terms_description',            'Terms Description',            'Free-form description to clarity the terms'),

    ('inventory_warehouse',          'Inventory Warehouse',          'ABCD'),
    ('field_name_to_send_inventory', 'Field Name To Send Inventory', 'customer_sku'),
)


DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}


class MyHabitApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class MyHabitApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(MyHabitApiClient, self).__init__(
            settings_variables, MyHabitOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = MyHabitApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')

        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = MyHabitApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def processingOrderLines(self, data, state='accepted'):
        orders_api = MyHabitApiAcknowledgementOrders(self.settings, data, state)
        orders_api.process('send')
        self.extend_log(orders_api)
        return True

    def confirmShipment(self, lines):

        confirm_api = MyHabitApiConfirmShipment(self.settings, lines)
        confirm_api.process('send')
        self.extend_log(confirm_api)

        if self.is_invoice_required:
            invoice_api = MyHabitApiInvoiceOrders(self.settings, lines)
            invoice_api.process('send')
            self.extend_log(invoice_api)

        return True

    def updateQTY(self, lines, mode=None):
        update_api = MyHabitApiUpdateQty(self.settings, lines, partial=False)
        update_api.process('send')

        self.check_and_set_filename_inventory(update_api)

        self.extend_log(update_api)

        return update_api.revision_lines

    def updateQTYPartial(self, lines, mode=None):
        return self.updateQTY(lines, mode=mode)


class MyHabitApiGetOrders(MyHabitApi):

    def __init__(self, settings):
        super(MyHabitApiGetOrders, self).__init__(settings)
        self.edi_type = '850'
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)


class MyHabitApiFunctionalAcknoledgment(MyHabitApi):

    orders = []

    def __init__(self, settings, orders):
        super(MyHabitApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):
        numbers = []
        data = []
        yaml_obj = YamlObject()

        for order_yaml in self.orders:
            order = yaml_obj.deserialize(_data=order_yaml['xml'])

            if order['ack_control_number'] in numbers:
                continue

            numbers.append(order['ack_control_number'])

            segments = []

            st_number = random.randrange(10000, 99999)

            st = self.repetition_separator.join([
                'ST',
                '%(edi_type)s',
                '%(st_number)s',
            ])
            segments.append(self.insertToStr(st, {
                'edi_type': self.edi_type,
                'st_number': st_number
            }))

            ak = self.repetition_separator.join([
                'AK1',
                '%(group)s',
                '%(ack_control_number)s',
            ])

            segments.append(self.insertToStr(ak, {
                'group': order['functional_group'],
                'ack_control_number': order['ack_control_number']
            }))

            ak9 = self.repetition_separator.join([
                'AK9',
                'A',
                '%(number_of_transaction)s',
                '%(number_of_transaction)s',
                '%(number_of_transaction)s',
            ])
            segments.append(self.insertToStr(ak9, {
                'number_of_transaction': order['number_of_transaction']
            }))

            se = self.repetition_separator.join([
                'SE',
                '%(segment_count)s',
                '%(st_number)s',
            ])
            segments.append(self.insertToStr(se, {
                'segment_count': len(segments) + 1,
                'st_number': st_number
            }))

            data.append(self.wrap(segments, {'group': 'FA'}))

        return data


class MyHabitApiAcknowledgementOrders(MyHabitApi):

    def __init__(self, settings, data, state):
        super(MyHabitApiAcknowledgementOrders, self).__init__(settings)
        self.edi_type = '855'
        self.use_ftp_settings('acknowledgement')
        self.data = data
        self.state = state

    def upload_data(self):
        if self.state == 'accepted':
            statuscode = 'AT'
        else:
            statuscode = 'RD'

        segments = []

        st_number = random.randrange(10000, 99999)

        st = self.repetition_separator.join([
            'ST',
            '%(edi_type)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(st, {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }))

        bak_elements = [
            'BAK',
            '00',
            '%(statuscode)s',
            '%(po_number)s',
            '%(order_confirmation_date)s',
            '',
            '',
            '',
            '[8]%(vendor_order_ref)s',
        ]

        if self.state == 'accepted':
            vendor_order_ref = self.data['name']
        else:
            vendor_order_ref = ''

        segments.append(self.insertToStr(self.repetition_separator.join(bak_elements), {
            'statuscode': statuscode,
            'po_number': self.data['po_number'],
            'order_confirmation_date': datetime.utcnow().strftime(DATE_FORMAT),
            'vendor_order_ref': vendor_order_ref,
        }))

        n1 = self.repetition_separator.join([
            'N1',
            '%(vendor_warehouse)s',
            '92',
            '%(vendor_warehouse)s',
        ])
        segments.append(self.insertToStr(n1, {
            'vendor_warehouse': self.data['vendor_warehouse'],
        }))

        for line in self.data['lines']:
            po1 = self.repetition_separator.join([
                'PO1',
                '%(line_item_id)s',
                '%(quantity_ordered)s',
                'EA',
                'SK',
                '%(product_id)s',
            ])
            segments.append(self.insertToStr(po1, {
                'line_item_id': line['external_customer_line_id'],
                'quantity_ordered': int(line['product_qty']),
                'product_id': line['vendorSku'],
            }))

            if self.state == 'accepted':
                industry_code = '00'
            else:
                industry_code = line['cancel_code']

            ack = self.repetition_separator.join([
                'ACK',
                '%(line_status_code)s',
                '%(quantity)s',
                'EA',
            ] + ([''] * 27) + ['[29]%(industry_code)s'])
            segments.append(self.insertToStr(ack, {
                'line_status_code': 'IA' if self.state == 'accepted' else 'IR',
                'quantity': int(line['product_qty']),
                'industry_code': industry_code,
            }))

        ctt = self.repetition_separator.join(['CTT', '%(number_of_line_items)s'])
        segments.append(self.insertToStr(ctt, {
            'number_of_line_items': len(self.data['lines']),
        }))

        se = self.repetition_separator.join([
            'SE',
            '%(num_segments)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(se, {
            'num_segments': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'PR'})


class MyHabitApiConfirmShipment(MyHabitApi):

    def __init__(self, settings, lines):
        super(MyHabitApiConfirmShipment, self).__init__(settings)
        self.edi_type = '856'
        self.use_ftp_settings('confirm_shipment')
        self.lines = lines

    def upload_data(self):
        segments = []

        st_number = random.randrange(10000, 99999)

        st = self.repetition_separator.join([
            'ST',
            '%(edi_type)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(st, {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }))

        shp_date = datetime.strptime(
            self.lines[0]['shp_date'], DEFAULT_SERVER_DATETIME_FORMAT)

        bsn = self.repetition_separator.join([
            'BSN',
            '00',
            '%(shipment_identification)s',
            '%(shp_date)s',
            '%(shp_time)s',
            'ZZZZ',
            'AS',
            'NOR',
        ])
        segments.append(self.insertToStr(bsn, {
            'purpose_code': '00',
            'shipment_identification': self.lines[0]['tracking_number'],
            'shp_date': shp_date.strftime(DATE_FORMAT),
            'shp_time': shp_date.strftime(TIME_FORMAT),
        }))

        hierarchical_number = 1

        hl_order = self.repetition_separator.join(
            ['HL', '%(hierarchical_number)s', '', 'O'])

        segments.append(self.insertToStr(hl_order, {
            'hierarchical_number': hierarchical_number,
        }))

        external_date_order = datetime.strptime(
            self.lines[0]['external_date_order'], DEFAULT_SERVER_DATE_FORMAT)
        external_date_order = external_date_order.strftime(DATE_FORMAT)

        prf = self.repetition_separator.join([
            'PRF',
            '%(po_number)s',
            '',
            '',
            '%(date)s',
            '',
            '%(vendor_order_ref)s',
        ])
        segments.append(self.insertToStr(prf, {
            'po_number': self.lines[0]['po_number'],
            'date': shp_date.strftime(DATE_FORMAT),
            'vendor_order_ref': self.lines[0]['merchant_order_id']
        }))

        vendor_warehouse = self.lines[0]['order_additional_fields']['vendor_warehouse']

        n1 = self.repetition_separator.join([
            'N1',
            'SF',
            '%(vendor_warehouse)s',
            '92',
            '%(vendor_warehouse)s',
        ])
        segments.append(self.insertToStr(n1, {'vendor_warehouse': vendor_warehouse}))

        for line in self.lines:
            hierarchical_number += 1

            hl_item = self.repetition_separator.join([
                'HL', '%(hierarchical_number)s', '', 'I'])

            segments.append(self.insertToStr(hl_item, {
                'hierarchical_number': hierarchical_number,
            }))

            lin = self.repetition_separator.join([
                'LIN',
                '%(id)s',
                'SK',
                '%(product_id)s',
            ])
            segments.append(self.insertToStr(lin, {
                'id': line['external_customer_line_id'],
                'product_id': line['vendorSku'],
            }))

            sn1 = self.repetition_separator.join([
                'SN1',
                '%(id)s',
                '%(shipped_qty)s',
                'EA',
                '',
                '%(ordered_qty)s',
                'EA',
                '',
                'IA',
            ])
            segments.append(self.insertToStr(sn1, {
                'id': line['external_customer_line_id'],
                'shipped_qty': int(line['product_qty']),
                'ordered_qty': int(line['ordered_qty']),
            }))

        hierarchical_number += 1
        hl_pack = self.repetition_separator.join([
            'HL', '%(hierarchical_number)s', '', 'P'])
        segments.append(self.insertToStr(hl_pack, {
            'hierarchical_number': hierarchical_number,
        }))

        td5 = self.repetition_separator.join([
            'TD5',
            '',
            '92',
            '%(carrier_code)s',
        ])
        segments.append(self.insertToStr(td5, {
            'carrier_code': self.lines[0]['carrier_code'],
        }))

        ctt = self.repetition_separator.join([
            'CTT',
            '%(num_lines)s',
            '%(total_shipped_qty)s',
        ])
        segments.append(self.insertToStr(ctt, {
            'num_lines': len(self.lines),
            'total_shipped_qty': int(sum([line['product_qty'] for line in self.lines])),
        }))

        se = self.repetition_separator.join([
            'SE',
            '%(segment_count)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class MyHabitApiInvoiceOrders(MyHabitApi):

    def __init__(self, settings, lines):
        super(MyHabitApiInvoiceOrders, self).__init__(settings)
        self.edi_type = '810'
        self.use_ftp_settings('invoice_orders')
        self.lines = lines

    def upload_data(self):
        segments = []

        st_number = random.randrange(10000, 99999)

        st = self.repetition_separator.join([
            'ST',
            '%(edi_type)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(st, {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }))

        shp_date = datetime.strptime(
            self.lines[0]['shp_date'], DEFAULT_SERVER_DATETIME_FORMAT)

        big = self.repetition_separator.join([
            'BIG',
            '%(date)s',
            '%(invoice_number)s',
            '%(date)s',
            '',
            '',
            '',
            'CI',
        ])
        segments.append(self.insertToStr(big, {
            'date': shp_date.strftime(DATE_FORMAT),
            'invoice_number': self.lines[0]['invoice'],
        }))

        cur = self.repetition_separator.join([
            'CUR',
            'BT',
            '%(currency)s',
        ])
        segments.append(self.insertToStr(cur, {
            'currency': self.lines[0]['order_additional_fields']['currency'],
        }))

        n1_ri = self.repetition_separator.join([
            'N1',
            'RI',
            '%(name)s',
            '92',
            '%(vendor_payee_code)s',
        ])
        segments.append(self.insertToStr(n1_ri, {
            'name': self.lines[0]['remit_to_name_1'],
            'vendor_payee_code': self.vendor_payee_code,
        }))

        n4_ri = self.repetition_separator.join([
            'N4',
            '%(city)s',
            '%(state_code)s',
            '%(postal_code)s',
            '%(country_code)s',
        ])
        segments.append(self.insertToStr(n4_ri, {
            'city': self.lines[0]['remit_to_city'],
            'state_code': self.lines[0]['remit_to_state'],
            'postal_code': self.lines[0]['remit_to_postal_code'],
            'country_code': self.lines[0]['remit_to_country'],
        }))

        vendor_warehouse = self.lines[0]['order_additional_fields']['vendor_warehouse']
        n1_sf = self.repetition_separator.join([
            'N1',
            'SF',
            '%(vendor_warehouse)s',
            '92',
            '%(vendor_warehouse)s',
        ])
        segments.append(self.insertToStr(n1_sf, {
            'vendor_warehouse': vendor_warehouse,
        }))

        itd = self.repetition_separator.join([
            'ITD',
            '01',
            '3',
            '',
            '',
            '',
            '',
            '%(terms_net_days)s',
            '',
            '',
            '',
            '',
            '%(terms_description)s',
        ])
        segments.append(self.insertToStr(itd, {
            'terms_net_days': self.terms_net_days,
            'terms_description': self.terms_description,
        }))

        for line in self.lines:
            it1 = self.repetition_separator.join([
                'IT1',
                '%(id)s',
                '%(qty_invoiced)s',
                'EA',
                '%(unit_price)s',
                'NT',
                'SK',
                '%(product_id)s',
                'PO',
                '%(po)s',
                '',
                '',
                '',
                '',
            ])
            segments.append(self.insertToStr(it1, {
                'id': line['external_customer_line_id'],
                'qty_invoiced': int(line['product_qty']),
                'unit_price': '{:.2f}'.format(line['price_unit'], 2),
                'product_id': line['vendorSku'],
                'po': line['po_number'],
            }))

        invoice_total = sum([round(line['price_unit'], 2) for line in self.lines])
        tds = self.repetition_separator.join([
            'TDS',
            '%(invoice_total)s',
        ])
        segments.append(self.insertToStr(tds, {
            'invoice_total': '{:.2f}'.format(invoice_total).replace('.', ''),
        }))

        se = self.repetition_separator.join([
            'SE',
            '%(segment_count)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class MyHabitApiUpdateQty(MyHabitApi):

    def __init__(self, settings, lines, partial=False):
        super(MyHabitApiUpdateQty, self).__init__(settings)
        self.edi_type = '846'
        self.use_ftp_settings('inventory')
        self.lines = lines

        self.partial = partial

        self.revision_lines = {
            'bad': [],
            'good': [],
        }

    def upload_data(self):
        segments = []
        st_number = random.randrange(10000, 99999)

        st = self.repetition_separator.join([
            'ST',
            '%(edi_type)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(st, {
            'edi_type': self.edi_type,
            'st_number': st_number,
        }))

        inventory_feed_qualifier = random.randrange(10000, 99999)
        datetime_now = datetime.utcnow()
        bia = self.repetition_separator.join([
            'BIA',
            '%(feed_type)s',
            'DD',
            '%(inventory_feed_qualifier)s',
            '%(date)s',
            '%(time)s',
        ])
        segments.append(self.insertToStr(bia, {
            'feed_type': '25' if self.partial else '00',
            'inventory_feed_qualifier': inventory_feed_qualifier,
            'date': datetime_now.strftime(DATE_FORMAT),
            'time': datetime_now.strftime('%H%M'),
        }))

        dtm = self.repetition_separator.join([
            'DTM',
            '166',
            '%(date)s',
            '%(time)s',
        ])
        segments.append(self.insertToStr(dtm, {
            'date': datetime_now.strftime(DATE_FORMAT),
            'time': datetime_now.strftime(TIME_FORMAT),
        }))

        n1 = self.repetition_separator.join([
            'N1',
            'ZZ',
            '%(inventory_warehouse)s',
            '92',
            '%(inventory_warehouse)s',
        ])
        segments.append(self.insertToStr(n1, {
            'inventory_warehouse': self.inventory_warehouse,
        }))

        for line_id, line in enumerate(self.lines, 1):
            if not line.get(self.field_name_to_send_inventory):
                self.append_to_revision_lines(line, 'bad')
                continue

            lin = self.repetition_separator.join([
                'LIN',
                '%(line_id)s',
                'SK',
                '%(sku)s',
            ])
            segments.append(self.insertToStr(lin, {
                'line_id': line_id,
                'sku': line[self.field_name_to_send_inventory],
            }))

            sdq = self.repetition_separator.join([
                'SDQ',
                'EA',
                '92',
                '%(inventory_warehouse)s',
                '%(quantity)s',
            ])
            segments.append(self.insertToStr(sdq, {
                'inventory_warehouse': self.inventory_warehouse,
                'quantity': int(line['qty']),
            }))

            self.append_to_revision_lines(line, 'good')

        ctt = self.repetition_separator.join([
            'CTT',
            '%(num_lines)s',
            '',
        ])
        segments.append(self.insertToStr(ctt, {
            'num_lines': len(self.revision_lines['good']),
        }))

        se = self.repetition_separator.join([
            'SE',
            '%(segment_count)s',
            '%(st_number)s',
        ])
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class MyHabitOpenerp(ApiOpenerp):

    def __init__(self):
        super(MyHabitOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []

        gift = False
        for key, value in order.get('additional_fields', {}).iteritems():
            additional_field = (0, 0, {
                'name': unicode(key),
                'label': unicode(key).capitalize(),
                'value': value,
            })
            additional_fields.append(additional_field)

            if key == 'po_type_code' and value == 'ZZ':
                gift = True

        return {
            'additional_fields': additional_fields,
            'gift': gift,
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        line.update({
            'notes': '',
            'customerCost': line['retail_price'],
            'additional_fields': [
                (0, 0, {
                    'name': unicode(key),
                    'label': unicode(key).capitalize(),
                    'value': value
                })
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        product_model = self.pool.get('product.product')

        search_by = ('default_code', 'customer_sku', 'upc')
        product, size = product_model.search_product_by_id(
            cr, uid, context['customer_id'], line['sku'], search_by)

        line['size_id'] = size.id if size else False

        if product:
            line['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line['size_id'])
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line['notes'] = "Can't find product cost.\n"
        else:
            line['notes'] = 'Can\'t find product by sku {}.\n'.format(line['sku'])
            line['product_id'] = False

        return line

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        additional_fields = {additional_field.name: additional_field.value
                             for additional_field in sale_order.additional_fields or []}

        return {
            'po_number': sale_order.po_number,
            'name': sale_order.name,
            'vendor_warehouse': additional_fields.get('vendor_warehouse', False),
            'lines': map(self.get_accept_information_line, sale_order.order_line),
        }

    def get_accept_information_line(self, line):
        return {
            'external_customer_line_id': line.external_customer_line_id,
            'product_qty': line.product_uos_qty,
            'vendorSku': line.vendorSku,
        }

    def get_additional_confirm_shipment_information_for_line(
            self, cr, uid,
            partner_id, order, order_line, context=None):

        return {'ordered_qty': order_line.product_uos_qty}
