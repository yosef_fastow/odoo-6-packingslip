# -*- coding: utf-8 -*-
from lxml import etree
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import time
import re
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from pf_utils.utils.re_utils import f_d


class OverstockApi(ApiClient):
    merchantKey = ""
    authenticationKey = ""
    api_url = ""

    nm = "http://www.overstock.com/shoppingApi"

    def __init__(self, settings):
        self.api_url = settings["api_url"]
        self.merchantKey = settings["merchantKey"]
        self.authenticationKey = settings["authenticationKey"]
        self.response = ' '.join(settings["response"].split())

        self._log = []

    def prepare_data(self, data, store_log=True):
        wrap = """<?xml version="1.0" encoding="ISO-8859-1"?>
            <Request xmlns="%s">
                <MerchantKey>%s</MerchantKey>
                <AuthenticationKey>%s</AuthenticationKey>
                %s
            </Request>"""

        data = wrap % (self.nm, self.merchantKey, self.authenticationKey, data)
        if store_log:
            self._log.append({
                'title': "Send %s Overstock" % self.name,
                'msg': data,
                'type': 'send',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        return ' '.join(data.split())

    def get_request_headers(self):
        headers = {"SapiMethodName": self.name}
        return headers

    def prepare_headers(self, headers, data):
        headers["Content-Length"] = len(data)
        headers["Content-Type"] = "application/xml"
        return headers

    def check_response(self, response):
        res = True
        if(not response):
            return False

        tree = etree.XML(response)
        if(tree.find('{%s}Failure'.replace("%s", self.nm)) is not None):
            #error_message = tree.findtext('{%s}Failure/{%s}Error/{%s}Message'.replace("%s", self.nm))
            error_type = tree.findtext('{%s}Failure/{%s}Error/{%s}Type'.replace("%s", self.nm))
            self._log.append({
                'title': "Recive %s ERROR %s Overstock" % (self.name, error_type),
                'msg': response,
                'type': 'recive',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            res = False
            #raise Exception("Overstock api error: type - %s, message - %s" % (error_type, error_message))

        elif(tree.find('{%s}PartialFailure'.replace("%s", self.nm)) is not None):
            error_type = tree.findtext('{%s}PartialFailure/{%s}Error/{%s}Type'.replace("%s", self.nm))
            #error_message = tree.findtext('{%s}Failure/{%s}Error/{%s}Message'.replace("%s", self.nm))
            if(tree.find('{%s}%sResponse' % (self.nm, self.name)) is not None):
                self._log.append({
                    'title': "Recive %s Overstock PartialFailure %s" % (self.name, error_type),
                    'msg': response,
                    'type': 'recive',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            else:
                self._log.append({
                    'title': "Recive %s PartialFailure ERROR %s Overstock" % (self.name, error_type),
                    'msg': response,
                    'type': 'recive',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                res = False
            #raise Exception("Overstock api error: type - %s, message - %s" % (error_type, error_message))
        else:
            self._log.append({
                'title': "Recive %s Overstock" % self.name,
                'msg': response,
                'type': 'recive',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })

        return res


class OverstockApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(OverstockApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = OverstockApiGetOrdersXML

    def loadOrders(self):
        orders = self.load_orders_api.send()
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = OverstockApiGetOrderObj(self.settings, xml)
        order = ordersApi.getOrderObj()
        if(order.get('order_id', False)):
            lines = order['lines']
            productApi = OverstockApiGetProductsInfo(self.settings, lines)
            products = productApi.send()
            for product in products:
                order['lines'][product['line_index']] = product
                order['lines'][product['line_index']]['customerCost'] = product['product']['Pricing']['SuggestedSellingPrice']
                order['lines'][product['line_index']]['syncPrice'] = True
        return order

    def getProductsInfo(self, lines):
        self.settings["response"] = ""
        productApi = OverstockApiGetProductsInfo(self.settings, lines)
        products = productApi.send()
        self.extend_log(productApi)
        return products

    def getReturns(self):
        returnApi = OverstockApiGetReturns(self.settings)
        returnLine = returnApi.send()
        self.extend_log(returnApi)
        return returnLine

    def returnOrders(self, lines):
        ReturnApi = OverstockApiReturnOrders(self.settings, lines)
        ReturnApi.send()
        self.extend_log(ReturnApi)
        return True

    def processingReturns(self, processLines):
        processReturnApi = OverstockApiProcessingReturns(self.settings, processLines)
        res = processReturnApi.send()
        self.extend_log(processReturnApi)
        return res

    def processingOrderLines(self, lines, state='accepted'):
        self.settings["response"] = ""
        processingApi = OverstockApiProcessingOrderLines(self.settings, lines, state)
        res = processingApi.send()
        self.extend_log(processingApi)
        return res

    def confirmShipment(self, lines):
        self.settings["response"] = ""
        confirmApi = OverstockApiConfirmShipment(self.settings, lines)
        res = confirmApi.send()
        self.extend_log(confirmApi)
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = OverstockApiUpdateQTY(lines, self.settings)
        res = updateApi.getXml()
        self.extend_log(updateApi)
        self.check_and_set_filename_inventory(updateApi)
        return res

    def generateInventoryFile(self, data):
        generateIFApi = OverstockGenerateInventoryFile(self.settings, data)
        generateIFApi.create_inv_file()
        self.check_and_set_filename_inventory(generateIFApi)
        return True

    def getProductsStatus(self, lines):
        self.settings["response"] = ""
        statusApi = OverstockApiGetProductsStatus(self.settings, lines)
        lines = statusApi.send()
        self.extend_log(statusApi)
        return lines


class OverstockApiProcessingOrderLines(OverstockApi):

    name = "ProcessingOrders"
    processingLines = []
    #rejected
    #accepted

    def __init__(self, settings, lines, state):
        super(OverstockApiProcessingOrderLines, self).__init__(settings)
        self.processingLines = lines
        self.state = state

    def get_request_data(self):
        data = "<ProcessingOrders>%s</ProcessingOrders>"
        lines = []
        for line in self.processingLines:
            lines.append('<InvoiceLineId acknowledgement="%s">%s</InvoiceLineId>' % (self.state, line["external_customer_line_id"]))
        return data % "".join(lines)

    def parse_response(self, response):
        if(not response):
            return False
        return True


class OverstockApiGetOrdersXML(OverstockApi):

    name = "GetOrders2"

    def __init__(self, settings):
        super(OverstockApiGetOrdersXML, self).__init__(settings)
        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            settings['customer'],
            'load_orders',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'overstock'),
            datetime.utcnow()
        )

    def get_request_data(self):
        data = "<GetOrders2 />"
        return data

    def parse_response(self, response):
        if(not response):
            return []
        tree = etree.XML(response)
        ordersList = []
        if(tree is not None and tree.find('{%s}GetOrders2Response'.replace("%s", self.nm)) is not None and tree.find('{%s}GetOrders2Response/{%s}Order'.replace("%s", self.nm)) is not None):
            root = tree.find('{%s}GetOrders2Response'.replace("%s", self.nm))
            orders = root.findall('{%s}Order'.replace("%s", self.nm))
            if(len(orders) > 0):
                for order in orders:
                    ordersObj = {}
                    ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                    ordersObj['name'] = order.findtext('{%s}Id'.replace("%s", self.nm))
                    if(not ordersObj['name']):
                        ordersObj['name'] = "CANCEL "
                        for invoce in order.findall('{%s}InvoiceLine'.replace("%s", self.nm)):
                            ordersObj['name'] = ordersObj['name'] + " " + invoce.findtext('{%s}Id'.replace("%s", self.nm))
                    ordersList.append(ordersObj)
        return ordersList


class OverstockApiGetOrderObj():

    nm = "http://www.overstock.com/shoppingApi"

    def __init__(self, settings,  xml):
        self.xml = xml
        self.settings = settings

    def getOrderObj(self):
        if(self.xml == ""):
            return {}
        tree = etree.XML(self.xml)

        if(tree is not None):
            ordersObj = {}
            ordersObj['order_id'] = tree.findtext('{%s}Id'.replace("%s", self.nm))
            if(ordersObj['order_id']):
                ordersObj['external_date_order'] = tree.findtext('{%s}Date'.replace("%s", self.nm))
                ordersObj['partner_id'] = (self.settings or {}).get('customer', 'overstock')
                ordersObj['address'] = {}
                ordersObj['address']['ship'] = {}
                ordersObj['address']['ship']['name'] = tree.findtext('{%s}Address/{%s}Name'.replace("%s", self.nm))
                ordersObj['address']['ship']['address1'] = tree.findtext('{%s}Address/{%s}Address1'.replace("%s", self.nm))
                ordersObj['address']['ship']['address2'] = tree.findtext('{%s}Address/{%s}Address2'.replace("%s", self.nm))
                ordersObj['address']['ship']['city'] = tree.findtext('{%s}Address/{%s}City'.replace("%s", self.nm))
                ordersObj['address']['ship']['state'] = tree.findtext('{%s}Address/{%s}State'.replace("%s", self.nm))
                ordersObj['address']['ship']['zip'] = tree.findtext('{%s}Address/{%s}ZipCode'.replace("%s", self.nm))
                ordersObj['address']['ship']['country'] = tree.findtext('{%s}Address/{%s}CountryCode'.replace("%s", self.nm))
                ordersObj['address']['ship']['phone'] = tree.findtext('{%s}Address/{%s}Phone'.replace("%s", self.nm))
                ordersObj['carrier'] = tree.findtext('{%s}Carrier'.replace("%s", self.nm))
                ordersObj['carrier'] += tree.findtext('{%s}ShipMethod'.replace("%s", self.nm))
                ordersObj['lines'] = []
                for invoce in tree.findall('{%s}InvoiceLine'.replace("%s", self.nm)):
                    invoceObj = {}
                    invoceObj['line_index'] = len(ordersObj['lines'])
                    invoceObj['id'] = invoce.findtext('{%s}Id'.replace("%s", self.nm))
                    invoceObj['qty'] = invoce.findtext('{%s}Quantity'.replace("%s", self.nm))
                    invoceObj['optionSku'] = invoce.findtext('{%s}OptionSku'.replace("%s", self.nm))
                    invoceObj['name'] = invoce.findtext('{%s}Name'.replace("%s", self.nm))
                    invoceObj['shipCost'] = invoce.findtext('{%s}ShipCost'.replace("%s", self.nm))
                    invoceObj['cost'] = invoce.findtext('{%s}Cost'.replace("%s", self.nm))
                    sku = invoce.findtext('{%s}VendorSku'.replace("%s", self.nm))
                    match_CP = re.search('^CP(\d+)_(.*)', sku)
                    match_MERGED = re.search('^(.*)-MERGED', sku)
                    if match_CP:
                        invoceObj['qty'] = int(match_CP.group(1)) * int(invoceObj['qty'])
                        sku = match_CP.group(2)
                    if match_MERGED:
                        sku = match_MERGED.group(1)
                    invoceObj['vendorSku'] = sku
                    if(sku.find("/") == -1):
                        invoceObj['sku'] = sku
                        invoceObj['size'] = False
                    else:
                        invoceObj['sku'] = sku[:sku.find("/")]
                        invoceObj['size'] = sku[sku.find("/") + 1:]
                    invoceObj['merchantSKU'] = invoceObj['sku']
                    invoceObj['Message'] = invoce.findtext('{%s}GiftMessage'.replace("%s", self.nm))
                    ordersObj['lines'].append(invoceObj)
            else:
                ordersObj['lines'] = []
                for invoce in tree.findall('{%s}InvoiceLine'.replace("%s", self.nm)):
                    invoceObj = {}
                    invoceObj['id'] = invoce.findtext('{%s}Id'.replace("%s", self.nm))
                    ordersObj['lines'].append(invoceObj)

        return ordersObj


class OverstockApiGetReturns(OverstockApi):

    name = "GetReturns"

    def get_request_data(self):
        data = "<GetReturns />"
        return data

    def parse_response(self, response):
        if(not response):
            return []
        tree = etree.XML(response)
        returnList = []
        if(tree is not None and tree.find('{%s}GetReturnsResponse'.replace("%s", self.nm)) is not None and tree.find('{%s}GetReturnsResponse/{%s}Return'.replace("%s", self.nm)) is not None):
            root = tree.find('{%s}GetReturnsResponse'.replace("%s", self.nm))

            for returnLine in root.findall('{%s}Return'.replace("%s", self.nm)):
                returnObj = {}
                returnObj['xml'] = etree.tostring(returnLine, pretty_print=True)
                returnObj["id"] = returnLine.findtext('{%s}Id'.replace("%s", self.nm))
                returnObj["lineId"] = returnLine.findtext('{%s}InvoiceLineId'.replace("%s", self.nm))
                returnObj["qty"] = returnLine.findtext('{%s}Quantity'.replace("%s", self.nm))
                returnObj["date"] = returnLine.findtext('{%s}Date'.replace("%s", self.nm))

                returnObj["return_code"] = returnLine.findtext('{%s}ReturnReason/{%s}Code'.replace("%s", self.nm))
                returnObj["return_reason"] = returnLine.findtext('{%s}ReturnReason/{%s}Notes'.replace("%s", self.nm))

                returnObj["Closure"] = {}
                returnObj["Closure"]["Action"] = returnLine.findtext('{%s}Closure/{%s}Action'.replace("%s", self.nm))
                returnObj["Closure"]["Date"] = returnLine.findtext('{%s}Closure/{%s}Date'.replace("%s", self.nm))
                returnObj["Closure"]["TrackingNumber"] = returnLine.findtext('{%s}Closure/{%s}TrackingNumber'.replace("%s", self.nm))
                returnObj["Closure"]["Type"] = returnLine.findtext('{%s}Closure/{%s}Type'.replace("%s", self.nm))
                returnObj["Closure"]["Action"] = returnLine.findtext('{%s}Closure/{%s}Action'.replace("%s", self.nm))

                returnObj["Rma"] = {}
                returnObj["Rma"]["Number"] = returnLine.findtext('{%s}Rma/{%s}Number'.replace("%s", self.nm))
                returnObj["Rma"]["Date"] = returnLine.findtext('{%s}Rma/{%s}Date'.replace("%s", self.nm))
                returnObj["Rma"]["ArsIssued"] = returnLine.findtext('{%s}Rma/{%s}ArsIssued'.replace("%s", self.nm))

                if not returnObj["Rma"]["Number"] and returnObj["return_reason"]:
                    preg = re.compile('RMA: (.*?) ')
                    match = preg.findall(returnObj["return_reason"])
                    if match:
                        returnObj["Rma"]["Number"] = match[0]

                returnList.append(returnObj)

        return returnList


class OverstockApiProcessingReturns(OverstockApi):

    name = "ProcessingReturns"
    processLines = []

    def __init__(self, settings, processLines):
        super(OverstockApiProcessingReturns, self).__init__(settings)
        self.processLines = processLines

    def get_request_data(self):
        data = "<ProcessingReturns>%s</ProcessingReturns>"
        lines = ""
        for line in self.processLines:
            lines += "<ReturnId>%s</ReturnId>" % line
        return data % lines

    def parse_response(self, response):
        if(not response):
            return False
        return True


class OverstockApiConfirmShipment(OverstockApi):

    name = "ConfirmShipment"
    confirmLines = []

    def __init__(self, settings, lines):
        super(OverstockApiConfirmShipment, self).__init__(settings)
        self.confirmLines = lines

    def send(self):
        from urllib2 import Request, urlopen
        import traceback
        data = self.get_request_data()
        data = self.prepare_data(data)

        headers = self.get_request_headers()
        headers = self.prepare_headers(headers, data)

        response = ""
        try:
            conn = Request(url=self.api_url, data=data, headers=headers)
            method = self.get_method()
            if method == 'PUT':
                conn.get_method = lambda: 'PUT'
            f = urlopen(conn, timeout=self._http_timeout)
            response = f.read()
        except Exception, e:

            msg = ""
            if hasattr(e, 'reason'):
                msg += 'Could not reach the server, reason: %s' % e.reason
            elif hasattr(e, 'code'):
                msg += 'Could not fulfill the request, code: %d' % e.code
            else:
                msg += e.message
            if bool(getattr(e, 'read', False)):
                msg += e.read()
            print msg

            msg += '  ' + traceback.format_exc()

            self._log.append({
                'title': "Send %s Error" % self.name,
                'msg': msg,
                'type': 'send',
                'create_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            })

            response = False

        return self.check_response(response)

    def get_request_data(self):
        data = "<ConfirmShipment>%s</ConfirmShipment>"
        lines = []
        for line in self.confirmLines:
            dt = datetime.strptime(line["shp_date"], "%Y-%m-%d %H:%M:%S")
            lines.append("""
                <InvoiceLine>
                  <Id>%s</Id>
                  <Quantity>%s</Quantity>
                  <Shipment>
                    <CarrierCode>%s</CarrierCode>
                    <ShipDate>%s</ShipDate>
                    <TrackingNumber>%s</TrackingNumber>
                    <PartnerInvoiceNumber>%s</PartnerInvoiceNumber>
                  </Shipment>
                </InvoiceLine>
            """ % (line['external_customer_line_id'],
                   int(line['product_qty']),
                   line['carrier_code'],
                   dt.strftime("%Y-%m-%d"),
                   line['tracking_number'],
                   line['external_customer_order_id'])
            )
        return data % "".join(lines)

    def parse_response(self, response):
        if(not response):
            return False
        return True


class OverstockApiUpdateQTY(OverstockApi):

    name = "UpdateQuantity"
    updateLines = []

    def __init__(self, lines, settings):
        self.updateLines = lines
        self._path_to_backup_local_dir = '%s' % self.create_local_dir(settings['backup_local_path'],
                                                                               "overstock", "inventory", "now")
        self.filename = "inv(%s).xml" % datetime.now().strftime("%Y%m%d%H%M")
        self._log = []

    def getXml(self):
        res = []
        send_data = []
        for line in self.updateLines:
            if not line['customer_sku']:
                continue
            if line['qty_tolerance'] > 3:
                line['qty'] = line['qty'] - int(line['qty_tolerance']) + 3
            xml = self.getLine(line)
            send_data.append(xml)
            xml = self.prepare_data("<UpdateQuantity>%s</UpdateQuantity>" % xml, store_log=False)
            res.append({'xml': xml, 'line': line})

        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, self.prepare_data("<UpdateQuantity>%s</UpdateQuantity>" % "".join(send_data)), 'wb').create()

        return res

    def getLine(self, line):

        line_ = """
                    <Option>
                      <OptionSku>%s</OptionSku>
                      <Quantity>%s</Quantity>
                    </Option>
                """ % (line['customer_sku'], line['qty'])

        return line_

    def parse_response(self, response):
        if(not response):
            return False
        return True


class OverstockApiGetProductsInfo(OverstockApi):

    name = "GetProduct2"
    products = []

    def __init__(self, settings, products):
        super(OverstockApiGetProductsInfo, self).__init__(settings)
        self.products = products

    def get_request_data(self):
        lines = ""
        for line in self.products:
            if(line.get('optionSku', False)):
                sku = line['optionSku']
                if(line['optionSku'].find('-') != -1):
                    sku = sku[:sku.find('-')]
                lines += "<ProductSku>%s</ProductSku>" % sku
        return "<GetProduct2>%s</GetProduct2>" % lines

    def parse_response(self, response):
        if(not response):
            return []
        tree = etree.XML(response)
        returnList = []
        if(tree is not None and tree.find('{%s}GetProduct2Response'.replace("%s", self.nm)) is not None and tree.find('{%s}GetProduct2Response/{%s}Product'.replace("%s", self.nm)) is not None):
            root = tree.find('{%s}GetProduct2Response'.replace("%s", self.nm))

            for returnLine in root.findall('{%s}Product'.replace("%s", self.nm)):
                returnObj = {}
                returnObj['xml'] = etree.tostring(returnLine, pretty_print=True)
                returnObj["ProductSku"] = returnLine.findtext('{%s}ProductSku'.replace("%s", self.nm))
                returnObj["Name"] = returnLine.findtext('{%s}Name'.replace("%s", self.nm))
                returnObj["ShortName"] = returnLine.findtext('{%s}ShortName'.replace("%s", self.nm))
                returnObj["Summary"] = returnLine.findtext('{%s}Summary'.replace("%s", self.nm))
                returnObj["Description"] = returnLine.findtext('{%s}Description'.replace("%s", self.nm))
                returnObj["Status"] = returnLine.findtext('{%s}Status'.replace("%s", self.nm))
                returnObj["CategoryId"] = returnLine.findtext('{%s}CategoryId'.replace("%s", self.nm))
                returnObj["ISBN"] = returnLine.findtext('{%s}ISBN'.replace("%s", self.nm))
                returnObj["ReleaseDate"] = returnLine.findtext('{%s}ReleaseDate'.replace("%s", self.nm))
                returnObj["ProductDimensions"] = returnLine.findtext('{%s}ProductDimensions'.replace("%s", self.nm))
                returnObj["LTL"] = returnLine.findtext('{%s}LTL'.replace("%s", self.nm))
                returnObj["ShipAlone"] = returnLine.findtext('{%s}ShipAlone'.replace("%s", self.nm))
                returnObj["SourceZipCode"] = returnLine.findtext('{%s}SourceZipCode'.replace("%s", self.nm))
                returnObj["CountryOfOrigin"] = returnLine.findtext('{%s}CountryOfOrigin'.replace("%s", self.nm))
                returnObj["Condition"] = returnLine.findtext('{%s}Condition'.replace("%s", self.nm))
                returnObj["Brand"] = returnLine.findtext('{%s}Brand'.replace("%s", self.nm))

                returnObj["Pricing"] = {}
                returnObj["Pricing"]["MSRP"] = returnLine.findtext('{%s}Pricing/{%s}MSRP'.replace("%s", self.nm))
                returnObj["Pricing"]["MSRPSalesLocation"] = returnLine.findtext('{%s}Pricing/{%s}MSRPSalesLocation'.replace("%s", self.nm))
                returnObj["Pricing"]["StreetPrice"] = returnLine.findtext('{%s}Pricing/{%s}StreetPrice'.replace("%s", self.nm))
                returnObj["Pricing"]["NormalWholesalePrice"] = returnLine.findtext('{%s}Pricing/{%s}NormalWholesalePrice'.replace("%s", self.nm))
                returnObj["Pricing"]["SuggestedSellingPrice"] = returnLine.findtext('{%s}Pricing/{%s}SuggestedSellingPrice'.replace("%s", self.nm))

                returnObj["PackageDimensions"] = {}
                returnObj["PackageDimensions"]["Length"] = returnLine.findtext('{%s}PackageDimensions/{%s}Length'.replace("%s", self.nm))
                returnObj["PackageDimensions"]["Width"] = returnLine.findtext('{%s}PackageDimensions/{%s}Width'.replace("%s", self.nm))
                returnObj["PackageDimensions"]["Height"] = returnLine.findtext('{%s}PackageDimensions/{%s}Height'.replace("%s", self.nm))

                options = returnLine.find('{%s}Options'.replace("%s", self.nm))
                returnObj["Options"] = []
                if(len(options)):
                    for option in options.findall('{%s}Option'.replace("%s", self.nm)):
                        optionObj = {}
                        optionObj['VendorSku'] = option.findtext('{%s}VendorSku'.replace("%s", self.nm))
                        optionObj['OptionSku'] = option.findtext('{%s}OptionSku'.replace("%s", self.nm))
                        optionObj['Description'] = option.findtext('{%s}Description'.replace("%s", self.nm))
                        optionObj['UPC'] = option.findtext('{%s}UPC'.replace("%s", self.nm))
                        optionObj['Cost'] = option.findtext('{%s}Cost'.replace("%s", self.nm))
                        optionObj['Quantity'] = option.findtext('{%s}Quantity'.replace("%s", self.nm))
                        optionObj['Weight'] = option.findtext('{%s}Weight'.replace("%s", self.nm))
                        returnObj["Options"].append(optionObj)

                returnObj["Manufacturer"] = {}
                returnObj["Manufacturer"]["Name"] = returnLine.findtext('{%s}Manufacturer/{%s}Name'.replace("%s", self.nm))
                returnObj["Manufacturer"]["PartNumber"] = returnLine.findtext('{%s}Manufacturer/{%s}PartNumber'.replace("%s", self.nm))
                returnObj["Manufacturer"]["ModelNumber"] = returnLine.findtext('{%s}Manufacturer/{%s}ModelNumber'.replace("%s", self.nm))

                returnObj["Warranty"] = {}
                returnObj["Warranty"]["Provider"] = returnLine.findtext('{%s}Warranty/{%s}Provider'.replace("%s", self.nm))
                returnObj["Warranty"]["Description"] = returnLine.findtext('{%s}Warranty/{%s}Description'.replace("%s", self.nm))
                returnObj["Warranty"]["ContactPhoneNumber"] = returnLine.findtext('{%s}Warranty/{%s}ContactPhoneNumber'.replace("%s", self.nm))

                returnObj["Specifications"] = {}
                returnObj["Specifications"]["Materials"] = returnLine.findtext('{%s}Specifications/{%s}Materials'.replace("%s", self.nm))

                returnObj["Images"] = {}
                returnObj["Images"]["Thumbnail"] = returnLine.findtext('{%s}Images/{%s}Thumbnail'.replace("%s", self.nm))
                returnObj["Images"]["Main"] = returnLine.findtext('{%s}Images/{%s}Main'.replace("%s", self.nm))
                returnObj["Images"]["Large"] = returnLine.findtext('{%s}Images/{%s}Large'.replace("%s", self.nm))

                for i in xrange(0, len(self.products)):
                    if(self.products[i].get('optionSku', False)):
                        if((self.products[i]['optionSku'][:self.products[i]['optionSku'].find('-')] == returnObj['ProductSku']) or
                            (self.products[i]['optionSku'] == returnObj['ProductSku'])):
                            self.products[i]['product'] = returnObj
                            returnList.append(self.products[i])
                            break

        return returnList


class OverstockApiGetProductsStatus(OverstockApi):

    name = "GetProductStatus2"
    statusLines = []

    def __init__(self, settings, lines):
        super(OverstockApiGetProductsStatus, self).__init__(settings)
        self.statusLines = lines

    def get_request_data(self):
        data = "<GetProductStatus2>%s</GetProductStatus2>"
        lines = ""
        for line in self.statusLines:
            if(line.get('optionSku', False)):
                sku = line['optionSku']
                if(line['optionSku'].find('-') != -1):
                    sku = sku[:sku.find('-')]
                lines += "<ProductSku>%s</ProductSku>" % sku
            else:
                lines += "<VendorSku>%s</VendorSku>" % line['vendorSku']

        return data % lines

    def parse_response(self, response):
        if(not response):
            return []
        tree = etree.XML(response)
        returnList = []
        if(tree is not None and tree.find('{%s}GetProductStatus2Response'.replace("%s", self.nm)) is not None and tree.find('{%s}GetProductStatus2Response/{%s}Product'.replace("%s", self.nm)) is not None):
            root = tree.find('{%s}GetProductStatus2Response'.replace("%s", self.nm))
            for returnLine in root.findall('{%s}Product'.replace("%s", self.nm)):
                returnObj = {}

                returnObj['ProductSku'] = returnLine.findtext('{%s}ProductSku'.replace("%s", self.nm))
                returnObj['Status'] = returnLine.findtext('{%s}Status'.replace("%s", self.nm))

                returnObj["Options"] = []
                options = returnLine.find('{%s}Options'.replace("%s", self.nm))
                if(options is not None):
                    for option in options.findall('{%s}Option'.replace("%s", self.nm)):
                            optionObj = {}
                            optionObj['VendorSku'] = option.findtext('{%s}VendorSku'.replace("%s", self.nm))
                            optionObj['OptionSku'] = option.findtext('{%s}OptionSku'.replace("%s", self.nm))
                            returnObj["Options"].append(optionObj)

                for i in xrange(0, len(self.statusLines)):
                    found = False
                    if(self.statusLines[i].get('optionSku', False) and
                        (
                            (
                                self.statusLines[i]['optionSku'].find('-') != -1 and
                                self.statusLines[i]['optionSku'][:self.statusLines[i]['optionSku'].find('-')] == returnObj['ProductSku']
                            ) or
                            (
                                self.statusLines[i]['optionSku'] == returnObj['ProductSku']
                            )
                        )):
                        self.statusLines[i]['product'] = returnObj
                        found = True
                    else:
                        for option in returnObj['Options']:
                            if(self.statusLines[i]['vendorSku'] == option['VendorSku']):
                                self.statusLines[i]['product'] = returnObj
                                found = True
                                break
                    if(found):
                        returnList.append(self.statusLines[i])
                        break

        return returnList


class OverstockApiReturnOrders(OverstockApi):

    name = "SubmitReturnRma"
    returnLines = []

    def __init__(self, settings, lines):
        super(OverstockApiReturnOrders, self).__init__(settings)
        self.returnLines = lines

    def get_request_data(self):
        data = "<SubmitReturnRma>%s</SubmitReturnRma>"
        dt = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        lines = []
        for line in self.returnLines:
            lines.append("""<Return>
                <Id>%s</Id>
                <Rma>
                    <Number>%s</Number>
                    <Date>%s</Date>
                    <ArsIssued>%s</ArsIssued>
                </Rma>
            </Return>
            """ % (line['external_customer_line_id'], line['rma_number'] or 'No RMA number', dt, str(line['ars_issued']).lower())
            )
        return data % "".join(lines)

    def parse_response(self, response):
        if(not response):
            return False
        return True

if __name__ == '__main__':
    over = OverstockApiClient()
    over.getOrders()


class OverstockGenerateInventoryFile():

    def __init__(self, settings, data):
        self.data = data or ""
        localpath_api = ApiClient()
        self._path_to_backup_local_dir = '%s' % localpath_api.create_local_dir(settings['backup_local_path'], "overstock", "inventory", "now")
        self.filename = "inv(%s).xml" % datetime.now().strftime("%Y%m%d%H%M%f")

    def create_inv_file(self):
        blf_utils.file_for_writing(self._path_to_backup_local_dir, self.filename, str(self.data), 'wb').create()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: