# -*- coding: utf-8 -*-
from httpauthapiclient import HTTPWithAuthApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
from openerp.addons.pf_utils.utils.re_utils import f_d
from openerp.addons.pf_utils.utils.str_utils import str_encode
from json import loads as json_loads
from json import dumps as json_dumps
from openerp.addons.pf_utils.utils.re_utils import str2date
from openerp.addons.pf_utils.utils.re_utils import str2float
from openerp.addons.pf_utils.utils.re_utils import pretty_float
from openerp.addons.pf_utils.utils.helper import get_image_url
from customer_parsers.bluefly_input_json_parser import Parser
from apiopenerp import ApiOpenerp
import csv
import cStringIO
from utils.ediparser import check_none
import requests
import logging

logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_http': True,
}


class BlueflyRESTApi(HTTPWithAuthApiClient):
    def __init__(self, settings):
        super(BlueflyRESTApi, self).__init__(settings)

    @property
    def sub_request_kwargs(self):
        request = {
            'headers': {
                'Accept': 'application/json',
                'Authorization': self.get_api_key
            },
        }
        return request

    @property
    def get_api_key(self):
        return self.api_key


class BlueflyRESTApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        multi_http_settings_dict = {}
        if 'multi_http_settings' in settings_variables:
            for mhs in settings_variables['multi_http_settings']:
                multi_http_settings_dict.update(mhs.to_dict())
        settings_variables['multi_http_settings_dict'] = multi_http_settings_dict
        super(BlueflyRESTApiClient, self).__init__(
            settings_variables, BlueflyRESTOpenerp, False, False)

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        Function for loading orders from BlueflyREST.API
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        order_api = BlueflyRESTApiGetOrders(self.settings)
        if save_flag:
            orders = order_api.process('load')
        else:
            orders = order_api.process('read_new')
        self.extend_log(order_api)
        return orders

    def updateQTY(self, lines, mode=None):
        self.settings['main_product_field'] = 'customer_id_delmar'
        self.checkAttributes()
        update_qty_api = BlueflyRESTApiUpdateQTY(self.settings, lines)
        update_qty_api.process('only_result')
        self.extend_log(update_qty_api)
        self.check_and_set_filename_inventory(update_qty_api)
        return update_qty_api.revision_lines

    def confirmShipment(self, lines, context=None):
        confirm_api = BlueflyRESTApiConfirmOrders(self.settings, lines)
        res_confirm = confirm_api.process('only_result')
        self.extend_log(confirm_api)
        logger.info('Confirm result:\n{0}'.format(str(res_confirm)))
        return res_confirm

    def processingOrderLines(self, lines, state='accepted'):
        res = True
        if state == 'cancel' and len(lines) > 0 and lines[0]['external_customer_order_id']:  # in ('accept_cancel', 'rejected_cancel', 'cancel')
            cancel_api = BlueflyRESTApiCancelOrders(self.settings, lines[0]['external_customer_order_id'])
            res = cancel_api.process('only_result')
            self.extend_log(cancel_api)
        return res

    def checkAttributes(self):
        try:
            self.settings['ring_size_attribute_id'] = int(self.settings.get('ring_size_attribute_id'))
        except:
            raise Exception("BlueflyREST inventory wrong `ring_size_attribute_id`")


class BlueflyRESTApiGetOrders(BlueflyRESTApi):
    def __init__(self, settings):
        super(BlueflyRESTApiGetOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')
        self.parser = Parser(self)
        self.filename = f_d('ORDERS_%Y%m%d_%H%M%S_%f', datetime.utcnow())

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiGetOrders, self).sub_request_kwargs
        # make a cheat due to fucking API
        # get awaiting_acceptance orders
        awaiting = BlueflyRESTApiGetAwaitingOrders(self.settings).process('only_result')
        if awaiting and len(awaiting) > 0:
            # accept each awaiting order
            for order in awaiting:
                accept_res = BlueflyRESTApiAcceptOrder(self.settings, order).process('only_result')
            awaiting_ids = [x.get('order_id') for x in awaiting]
            request.update({'params': {'order_ids': ",".join(awaiting_ids)}})
        return request

    def process_response(self, response):
        # get accepted orders
        response_data = json_loads(super(BlueflyRESTApiGetOrders, self).process_response(response))
        # final with full info
        write_data = [json_dumps(x) for x in response_data.get('orders') if 'orders' in response_data]
        return write_data


class BlueflyRESTApiGetAwaitingOrders(BlueflyRESTApi):
    def __init__(self, settings):
        super(BlueflyRESTApiGetAwaitingOrders, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('load_orders')

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiGetAwaitingOrders, self).sub_request_kwargs
        request.update({'params': {'order_state_codes': 'WAITING_ACCEPTANCE'}})
        logger.info("Awaiting orders request args: %s" % request)
        return request

    def process_response(self, response):
        # get accepted orders
        response_data = json_loads(super(BlueflyRESTApiGetAwaitingOrders, self).process_response(response))
        # final with full info
        orders = [x for x in response_data.get('orders') if 'orders' in response_data]
        return orders


class BlueflyRESTApiAcceptOrder(BlueflyRESTApi):
    def __init__(self, settings, order=None):
        super(BlueflyRESTApiAcceptOrder, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('confirm_shipment')
        self.bluefly_order = order
        if hasattr(self, 'sub_url'):
            self.sub_url = self.sub_url.format(command='accept', order_id=order.get('order_id'))
            logger.info("Accept order url: %s" % self.sub_url)

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiAcceptOrder, self).sub_request_kwargs
        headers = request.get('headers', {})
        headers.update({'Content-Type': 'application/json'})
        order_lines = [{'accepted': bool(1), 'id': x.get('order_line_id')} for x in self.bluefly_order.get('order_lines')]
        request.update({'data': json_dumps({'order_lines': order_lines})})
        logger.info("Accept order request args: %s" % request)
        return request

    def process_response(self, response):
        # process
        res = super(BlueflyRESTApiAcceptOrder, self).process_response(response)
        logger.info("Accepting result: %s" % res)
        return True


class BlueflyRESTApiConfirmOrders(BlueflyRESTApi):

    def __init__(self, settings, lines):
        super(BlueflyRESTApiConfirmOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.lines = lines
        self.settings = settings
        # self.filename = "{:ShipConfirm_%Y%m%d%H%M.xml}".format(datetime.now())
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(command='ship', order_id=self.lines[0]['external_customer_order_id'])

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiConfirmOrders, self).sub_request_kwargs
        BlueflyRESTApiUpdateTracking(self.settings, self.lines).process('only_result')
        return request

    def check_response(self, response):
        try:
            super(BlueflyRESTApiConfirmOrders, self).check_response(response)
        except Exception, e:
            if response.status_code in [400, 404] and response.text:
                resp = json_loads(response.text)
                logger.warn("Order was not confirmed: %s" % resp.get('message'))
                if 'The order does not have the expected status' in resp.get('message'):
                    return True
            raise e
        return True

    def process_response(self, response):
        return True


class BlueflyRESTApiUpdateTracking(BlueflyRESTApi):
    def __init__(self, settings, lines):
        super(BlueflyRESTApiUpdateTracking, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.lines = lines
        if self.lines:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(command='tracking', order_id=self.lines[0]['external_customer_order_id'])

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiUpdateTracking, self).sub_request_kwargs
        if self.lines:
            line = self.lines[0]
            data = {
                "carrier_code": line.get('carrier_code', ''),
                "carrier_name": line.get('shp_service', ''),
                "carrier_url": '',
                "tracking_number": line.get('tracking_number', '')
            }
            headers = request.get('headers', {})
            headers.update({'Content-Type': 'application/json'})
            request.update({'data': json_dumps(data)})
        return request

    def check_response(self, response):
        try:
            super(BlueflyRESTApiUpdateTracking, self).check_response(response)
        except Exception, e:
            logger.info("Update tracking exception: %s, response: %s" % (e.message, response.text))
            pass
        return True


class BlueflyRESTApiCancelOrders(BlueflyRESTApi):
    def __init__(self, settings, order_id=None):
        super(BlueflyRESTApiCancelOrders, self).__init__(settings)
        self.use_http_settings('confirm_shipment')
        self.settings = settings
        if order_id:
            if hasattr(self, 'sub_url'):
                self.sub_url = self.sub_url.format(command='cancel', order_id=order_id)
        logger.info("Canceling bluefly order: %s" % order_id)

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiCancelOrders, self).sub_request_kwargs
        return request

    def check_response(self, response):
        try:
            super(BlueflyRESTApiCancelOrders, self).check_response(response)
        except Exception, e:
            if response.status_code == 400 and response.text:
                resp = json_loads(response.text)
                logger.warn("Order was not confirmed: %s" % resp.get('message'))
                # return True
            raise e
        return True

    def process_response(self, response):
        return True


class BlueflyRESTApiUploadFile(BlueflyRESTApi):
    def __init__(self, settings):
        super(BlueflyRESTApiUploadFile, self).__init__(settings)
        self.settings = settings
        self.use_http_settings('inventory')
        self.history = {}

    def _write_csv_lines(self, lines, context=None):
        if not lines:
            lines = []
        quote_style = csv.QUOTE_ALL
        output = cStringIO.StringIO()
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
        writer = csv.writer(output, dialect='csv', quoting=quote_style)
        for line in lines:
            writer.writerow([isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in line])
        return output

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiUploadFile, self).sub_request_kwargs
        return request

    def check_response(self, response):
        # logger.info("Run bluefly check_response: %s" % response.text)
        super(BlueflyRESTApiUploadFile, self).check_response(response)
        if response and response.text:
            self.history['response'] = json_loads(response.text)
            self.backup_file(self.filename_local, json_dumps(self.history))
        return True


class BlueflyRESTApiUpdateQTY(BlueflyRESTApiUploadFile):
    def __init__(self, settings, lines):
        super(BlueflyRESTApiUpdateQTY, self).__init__(settings)
        self.settings = settings
        self.file_type = 'Inventory'
        self.lines = lines
        self.filename_local = f_d('bluefly_offer_import_%Y_%m_%d_%H_%M.csv', datetime.utcnow())
        self.revision_lines = {
            'good': [],
            'bad': [],
        }
        self.variations = {}

    @property
    def sub_request_kwargs(self):
        request = super(BlueflyRESTApiUpdateQTY, self).sub_request_kwargs
        attachment = self.prepare_feed()
        headers = request.get('headers', {})
        headers.update({'cache-control': "no-cache"})
        request.update({'files': attachment})
        request.update({'data': {'import_mode': 'NORMAL', 'with_products': False}})
        return request

    def prepare_feed(self):
        feed = list()
        # init feed csv header line
        feed.append([
            "sku",
            "product-id",
            "product-id-type",
            "description",
            "internal-description",
            #"discount-price",
            "price-additional-info",
            "quantity",
            "state",
            "offer-return-policy"
        ])
        main_field = self.settings['main_product_field']
        # iterate over inventory lines
        for line in self.lines:
            price = line.get('customer_price', None)
            # check customer_id_delmar and skip if not
            if not line.get(main_field, False):
                self.append_to_revision_lines(line, 'bad')
                continue

            descr = check_none(line.get('long_description'))
            if not descr:
                descr = check_none(line.get('description'))

            if descr:
                descr = str_encode(descr[:1999])

            # format requested by rest api docs:
            # line.get('customer_id_delmar', line.get('sku', line.get('default_code'))),
            feed_line = [
                line.get('customer_id_delmar', line.get('sku', line.get('default_code'))),
                line.get('customer_id_delmar', line.get('sku', line.get('default_code'))),
                'SHOP_SKU',
                descr,
                descr,
                #price or '',
                '',
                line['qty'],
                1,
                'Standard'
            ]
            # possible additional columns
            """
            "min-quantity-alert": '',
            "state": '',
            "available-start-date": '',
            "available-end-date": '',
            "discount-price": '',
            "discount-start-date": '',
            "discount-end-date": '',
            "discount-ranges": '',
            "allow-quote-requests": '',
            "leadtime-to-ship": '',
            "min-order-quantity": '',
            "max-order-quantity": '',
            "package-quantity": '',
            "update-delete": '',
            "product-tax-code": '',
            "price-ranges": ''
            """

            feed.append(feed_line)
            self.append_to_revision_lines(line, 'good')

        # generate csv file from feed lines
        # returns cStringIO.StringIO()
        feed_file = self._write_csv_lines(feed)
        attachment = {'file': (self.filename_local, feed_file.getvalue(), 'multipart/form-data', {'Expires': '0'})}
        self.history.update({'feed': feed_file.getvalue()})
        # return attachment dict
        return attachment


class BlueflyRESTOpenerp(ApiOpenerp):

    def __init__(self):
        super(BlueflyRESTOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        carrier = None

        if order['additional_fields']['order_detail']:
            order_detail = json_loads(order['additional_fields']['order_detail'])
            carrier = order_detail.get('request_shipping_method', None) or "{service}__{level}".format(
                service=order_detail.get('request_shipping_carrier', None),
                level=order_detail.get('request_service_level', None)
            )

        return {
            'carrier': carrier
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'merchantSKU': line['merchantSKU'],
            'additional_fields': [],
            "qty": line['request_order_quantity'],
            'price_unit': line['price_unit'],
            'external_customer_line_id': line['external_customer_line_id'],
        }

        for field, value in line.get('additional_fields', {}).iteritems():
            field_obj = (
                0, 0, {
                    'name': field.lower().replace(' ', '_'),
                    'label': field,
                    'value': value,
                    }
                )
            line_obj['additional_fields'].append(field_obj)

        product = False
        field_list = ['merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if size and size.id:
                    line_obj['size_id'] = size.id
                else:
                    line_obj['size_id'] = False

                if product:
                    line_obj["product_id"] = product.id
                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price')
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj["notes"] = "Can't find product cost.\n"

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):
        lines = [{
            'order_item_id': x.external_customer_line_id,
            'order_item_acknowledgement_status': 'fulfillable',
        } for x in sale_order.order_line]
        orderObj = {
            'id': sale_order['po_number'],
            'order_items': lines,
        }

        return orderObj
