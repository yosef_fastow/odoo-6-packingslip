# -*- coding: utf-8 -*-
import re
from pyvirtualdisplay import Display
from selenium import webdriver
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class CommerceHubRemoteAPIClient(object):

    def __init__(self, settings):
        super(CommerceHubRemoteAPIClient, self).__init__()
        self.settings = settings

    def get_orders_needed_feeds(self, type_orders='critical', color='red'):
        display = Display(visible=0, size=(800, 600))
        display.start()
        browser = webdriver.Firefox()
        chrapi = CommerceHubRemoteAPI(browser, self.settings)
        chrapi.login()
        orders = {}
        try:
            if type_orders == 'critical':
                orders = chrapi.get_critical(color=color)
            else:
                orders = chrapi.get_non_critical()
        except:
            pass
        finally:
            browser.quit()
            display.stop()
        return orders

    def get_order_info(self, po_number):
        status = {'errors': []}
        try:
            display = Display(visible=0, size=(800, 600))
            log.info('Starting virtual display')
            display.start()
            log.info('Opening firefox browser')
            browser = webdriver.Firefox()
            chrapi_order_status = CommerceHubRemoteAPIOrderStatus(browser, self.settings, po_number)
            log.info('Log to CommerceHub')
            chrapi_order_status.login(url=chrapi_order_status.po_url)
            log.info('Getting status')
            status.update(chrapi_order_status.get_order_info())
        except Exception as ex:
            status['errors'].append(unicode(ex))
        finally:
            log.info('Closing browser')
            browser.quit()
            log.info('Closing virtual display')
            display.stop()
        return status


class AbstractCommerceHubRemoteAPI(object):

    def __init__(self, browser, settings):
        super(AbstractCommerceHubRemoteAPI, self).__init__()
        self.browser = browser
        self.username = settings['connection']['username']
        self.password = settings['connection']['password']
        self.timeout = settings['connection']['timeout']
        self.partner_map = settings['partner_map']
        self.mapping_type_orders = settings['mapping_type_orders']

    def wait(self):
        self.browser.implicitly_wait(self.timeout)

    def login(self, url=None):
        default_url = 'https://apps.commercehub.com/account/login?service=https://dsm.commercehub.com/dsm/shiro-cas'
        if not url:
            start_url = default_url
        else:
            start_url = url
        self.browser.get(start_url)
        username_column = self.browser.find_element_by_id("username")
        username_column.send_keys(self.username)

        password_column = self.browser.find_element_by_id("password")
        password_column.send_keys(self.password)

        login_button = self.browser.find_element_by_id('loginButton')
        login_button.click()


class CommerceHubRemoteAPIOrderStatus(AbstractCommerceHubRemoteAPI):

    def __init__(self, browser, settings, po_number):
        super(CommerceHubRemoteAPIOrderStatus, self).__init__(browser, settings)
        self.po_number = po_number
        self.id_vendorName = 'cell.summary-left.order{}.vendorName'
        self.id_merchantName = 'cell.summary-left.order{}.merchantName'
        self.id_poNumber = 'cell.summary-left.order{}.poNumber'
        self.id_shippingMethod = 'cell.summary-left.order{}.shippingMethod'
        self.id_customerOrderNumber = 'cell.summary-left.order{}.customerOrderNumber'
        self.id_orderDate = 'cell.summary-right.order{}.orderDate'
        self.id_orderStatus = 'cell.summary-right.order{}.orderStatus'
        self.id_orderSubStatus = 'cell.summary-right.order{}.orderSubStatus'
        self.id_salesDivision = 'cell.summary-right.order{}.salesDivision'

    @property
    def po_url(self):
        if not hasattr(self, '_po_url'):
            return None
        else:
            return self._po_url

    @po_url.setter
    def po_url(self, po_url):
        self._po_url = po_url

    @property
    def po_number(self):
        if not hasattr(self, '_po_number'):
            return None
        else:
            return self._po_number

    @po_number.setter
    def po_number(self, po_number):
        self._po_number = po_number
        if po_number and isinstance(po_number, (str, unicode)):
            self.po_url = '''https://dsm.commercehub.com/dsm/gotoOrderDetail.do?Hub_PO={po_number}'''.format(
                po_number=po_number)
            log.info('Set url: {}'.format(self.po_url))
        else:
            self.po_url = False

    def get_order_info(self):
        order_info = {
            'vendorName': '',
            'merchantName': '',
            'poNumber': '',
            'shippingMethod': '',
            'customerOrderNumber': '',
            'orderDate': '',
            'orderStatus': '',
            'orderSubStatus': '',
            'salesDivision': '',
            'states': [],
            'addresses': {
                'ship_to': '',
                'bill_to': '',
            },
            'lines': [],
        }
        if self.po_url:
            self.wait()
            states = self.browser.find_elements_by_xpath('//div[@id="pageRoot"]/div[@id="body-content"]/div[@id="blockfive"]/div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]/table[@class="fw_widget_table"]/tbody/tr[@class="fw_widget_tablerow-odd" or @class="fw_widget_tablerow-even"]')
            for state in states:
                elements = state.find_elements_by_xpath('td')
                status = None
                description = None
                date = None
                time = None
                for i, elem in enumerate(elements):
                    if i == 0:
                        if re.findall(r'<a.*>.*</a>', elem.get_attribute('innerHTML').strip()):
                            status = elem.find_element_by_xpath('a').get_attribute('innerHTML').strip()
                        else:
                            status = elem.get_attribute('innerHTML').strip()
                    elif i == 1:
                        description = elem.get_attribute('innerHTML').strip()
                    elif i == 2:
                        date = elem.get_attribute('innerHTML').strip()
                    elif i == 3:
                        time = elem.get_attribute('innerHTML').strip()
                if status:
                    order_info['states'].append({
                        'state': status,
                        'description': description,
                        'date': date,
                        'time': time,
                    })
            first_tr = self.browser.find_element_by_xpath('//div[@id="pageRoot"]/div[@id="body-content"]/div[@id="blocktwo"]/div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]/table[@class="or-window-interior"]/tbody/tr/td[@class="genericDetailSide"]/table/tbody/tr')
            if first_tr:
                raw_id = first_tr.get_attribute('id').strip()
                math_ids = re.findall(r'\(\d*\)', raw_id)
                if math_ids:
                    id_ = math_ids[0]
                    attrs = vars(self)
                    for item in attrs.items():
                        if item[0].startswith('id_') and item[1]:
                            node = self.browser.find_element_by_id(item[1].format(id_))
                            if node:
                                order_info.update({
                                    item[0].replace('id_', ''): node.get_attribute('innerHTML').strip()
                                })
                blockfour = self.browser.find_element_by_id('blockfour')
                ship_to_node = blockfour.find_element_by_xpath('div[@class="framework_fiftyfifty_container"]/div[@class="framework_fiftyfifty_left"]')
                bill_to_node = blockfour.find_element_by_xpath('div[@class="framework_fiftyfifty_container"]/div[@class="framework_fiftyfifty_right"]')
                if ship_to_node:
                    ship_to_body = ship_to_node.find_element_by_xpath('div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]')
                    if ship_to_body:
                        order_info['addresses']['ship_to'] = ship_to_body.get_attribute('innerHTML').strip()
                if bill_to_node:
                    bill_to_body = bill_to_node.find_element_by_xpath('div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]')
                    if bill_to_body:
                        order_info['addresses']['bill_to'] = bill_to_body.get_attribute('innerHTML').strip()
                lines = self.browser.find_element_by_id("lineItemSummaryColumnHeaders").find_element_by_xpath("..").find_elements_by_xpath("tr[contains(@id, 'row.lines.order{}')]".format(id_))
                for line in lines:
                    line_obj = {
                        'lineNumber': line.find_element_by_id('cell.lines.order{}.lineNumber'.format(id_)).get_attribute('innerHTML').strip(),
                        'merchantSku': line.find_element_by_id('cell.lines.order{}.merchantSku'.format(id_)).get_attribute('innerHTML').strip(),
                        'vendorSku': line.find_element_by_id('cell.lines.order{}.vendorSku'.format(id_)).get_attribute('innerHTML').strip(),
                        'description': line.find_element_by_id('cell.lines.order{}.description'.format(id_)).get_attribute('innerHTML').strip(),
                        'qtyOrdered': line.find_element_by_id('cell.lines.order{}.qtyOrdered'.format(id_)).get_attribute('innerHTML').strip(),
                        'qty': line.find_element_by_id('cell.lines.order{}.qty'.format(id_)).get_attribute('innerHTML').strip(),
                        'status': line.find_element_by_id('cell.lines.order{}.status'.format(id_)).get_attribute('innerHTML').strip(),
                        'expectedShipDate': line.find_element_by_id('cell.lines.order{}.expectedShipDate'.format(id_)).get_attribute('innerHTML').strip(),
                    }
                    order_info['lines'].append(line_obj)
        # import pdb; pdb.set_trace()
        return order_info


class CommerceHubRemoteAPI(AbstractCommerceHubRemoteAPI):

    def __init__(self, browser, settings):
        super(CommerceHubRemoteAPI, self).__init__(browser, settings)

    def get_priority_links(self, color='all'):
        res = []
        self.wait()
        xpath = '//div[@id="VendorOrderExceptionsSection"]/table[@class="window"]/tbody/tr/td[@class="windowpane"]/div[@id="VendorOrderExceptionsSectionInner"]/table/tbody/tr/td/table[@class="dashboardbox"]/tbody/tr[@class="oddrow" or @class="evenrow"]/td[@class="numericdata"]/a'
        if color in ['red', 'orange', 'gold']:
            xpath += '[@style="color: {color}"]'.format(color=color)
        links = self.browser.find_elements_by_xpath(xpath)
        for link_obj in links:
            res.append(link_obj.get_attribute('href').strip())
        return res

    def get_priority_order_links(self, links):
        res = {}
        for href in links:
            self.browser.get(href)
            self.wait()
            tds = self.browser.find_elements_by_xpath('//table[@id="pageParent"]/tbody/tr/td/table[@class="page"]/tbody/tr/td[@class="pagecell"]/table[@class="window"]/tbody/tr/td[@class="windowpane"]/table[@class="plain"]/tbody/tr/td[@class="fieldvalue"]')
            partner_raw = tds[0].get_attribute('innerHTML').strip()
            partner = self.partner_map[partner_raw]
            orders_links = self.browser.find_elements_by_xpath('//table[@id="pageParent"]/tbody/tr/td/table[@class="page"]/tbody/tr/td[@class="pagecell"]/table[@class="window"]/tbody/tr/td[@class="windowpane"]/table[@class="linedata"]/tbody/tr[@class="oddrow" or @class="evenrow"]/td[@class="numericdata"]/a')
            for main_orders_link in orders_links:
                if partner not in res:
                    res[partner] = {}
                order_name = main_orders_link.get_attribute('innerHTML').strip()
                order_link = main_orders_link.get_attribute('href').strip()
                log.info('append order: {}'.format(order_name))
                res[partner].update({order_name: order_link})
        return res

    def get_main_links(self):
        self.wait()
        self.browser.get('https://dsm.commercehub.com/dsm/gotoOrderSummary.do')
        self.wait()
        parent = self.browser.find_element_by_id('pageParent')
        res = {}
        link_objs = parent.find_elements_by_xpath('//tbody/tr/td/div/table[@class="page"]/tbody/tr/td[@class="pagecell"]/table[@class="window"]/tbody/tr/td[@class="windowpane"]/table[@class="noborders"]/tbody/tr[@class="oddrow" or @class="evenrow"]/td/a')
        for link_obj in link_objs:
            partner_name = link_obj.get_attribute('innerHTML').strip()
            res[self.partner_map[partner_name]] = {
                'href': link_obj.get_attribute('href').strip(),
            }
        for key, value in res.iteritems():
            href = value['href']
            self.browser.get(href)
            self.wait()
            parent = self.browser.find_element_by_id('pageParent')
            rows = parent.find_elements_by_xpath('//tbody/tr/td/table[@class="page"]/tbody/tr/td[@class="pagecell"]/table[@class="window"]/tbody/tr/td[@class="windowpane"]/table[@class="noborders"]/tbody/tr[@class="oddrow" or @class="evenrow"]')
            for row in rows:
                columns = row.find_elements_by_xpath('td[@class="label" or @class="value"]')
                name = columns[0].find_element_by_xpath('a').get_attribute('innerHTML').strip()
                href = columns[0].find_element_by_xpath('a').get_attribute('href').strip()
                value = columns[1].get_attribute('innerHTML').strip()
                if 'exceptions' not in res[key]:
                    res[key]['exceptions'] = {}
                res[key]['exceptions'].update({
                    self.mapping_type_orders[name]: {'value': value, 'href': href}
                })
        return res

    def get_orders_links_from_page(self):
        sub_res = {}
        main_orders_links = self.browser.find_elements_by_xpath('//div[@id="pageRoot"]/div[@id="body-content"]/form[@id="primaryForm"]/div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]/div[@class="fw_widget_windowtag_topbar"]/div[@class="framework_fiftyfifty_left_justify"]/span[@class="no_emphasis_label"]/a[@class="simple_link"]')
        for main_orders_link in main_orders_links:
            order_name = main_orders_link.get_attribute('innerHTML').strip()
            order_link = main_orders_link.get_attribute('href').strip()
            log.info('append order: {}'.format(order_name))
            sub_res.update({order_name: order_link})
        return sub_res

    def get_order_links(self, main_links):
        res = {}
        for partner, link_obj in main_links.iteritems():
            for type_orders, link in link_obj['exceptions'].iteritems():
                if type_orders in ['no_activity', 'needs_invoicing']:
                    if partner not in res:
                        res[partner] = {}
                    href = link['href']
                    self.browser.get(href)
                    self.wait()
                    count_elem = self.browser.find_element_by_xpath('//div[@id="pageRoot"]/div[@id="body-content"]/form[@id="primaryForm"]/span[@class="fw_widget_pageselector_text" and @style="float:right"]')
                    count_elem_text = count_elem.get_attribute('innerHTML').strip()
                    count_raws = re.findall('> of \d*', count_elem_text)
                    count_raw = count_raws and count_raws[0]
                    count = int(count_raw.replace('> of ', ''))
                    # current_page = int(count_elem.find_element_by_xpath('input[@id="pageSelector"]').get_attribute('value').strip())
                    res[partner].update(self.get_orders_links_from_page())
                    if count != 1:
                        another_pages = [i + 2 for i in xrange(count - 1)]
                        for count_page in another_pages:
                            self.browser.get(href)
                            self.wait()
                            page_selector = self.browser.find_element_by_id('pageSelector')
                            page_selector.clear()
                            page_selector.send_keys(count_page)
                            go_button = page_selector.find_element_by_xpath('..').find_element_by_xpath('input[@value="Go"]')
                            go_button.click()
                            self.wait()
                            res[partner].update(self.get_orders_links_from_page())
                else:
                    pass
        return res

    def grab_order_statuses(self, _order_links):
        res = {}
        count = 0
        for partner, order_links in _order_links.iteritems():
            all_count = len(order_links)
            for order_name, order_href in order_links.iteritems():
                count += 1
                log.info('{0}/{1}|Grabbed states for \'{2}\', url: {3}'.format(count, all_count, order_name, order_href))
                if partner not in res:
                    res[partner] = {}
                res[partner][order_name] = {
                    'states': [],
                }
                self.browser.get(order_href)
                self.wait()
                states = self.browser.find_elements_by_xpath('//div[@id="pageRoot"]/div[@id="body-content"]/div[@id="blockfive"]/div[@class="fw_widget_windowtag"]/div[@class="fw_widget_windowtag_body"]/table[@class="fw_widget_table"]/tbody/tr[@class="fw_widget_tablerow-odd" or @class="fw_widget_tablerow-even"]')
                for state in states:
                    elements = state.find_elements_by_xpath('td')
                    status = None
                    description = None
                    date = None
                    time = None
                    for i, elem in enumerate(elements):
                        if i == 0:
                            if re.findall(r'<a.*>.*</a>', elem.get_attribute('innerHTML').strip()):
                                status = elem.find_element_by_xpath('a').get_attribute('innerHTML').strip()
                            else:
                                status = elem.get_attribute('innerHTML').strip()
                        elif i == 1:
                            description = elem.get_attribute('innerHTML').strip()
                        elif i == 2:
                            date = elem.get_attribute('innerHTML').strip()
                        elif i == 3:
                            time = elem.get_attribute('innerHTML').strip()
                    if status:
                        res[partner][order_name]['states'].append({
                            'state': status,
                            'description': description,
                            'date': date,
                            'time': time,
                        })
        return res

    def get_critical(self, color='all'):
        log.info('get all priority links')
        priority_links = self.get_priority_links(color=color)
        log.info('get all priority orders links')
        priority_orders_links = self.get_priority_order_links(priority_links)
        log.info('grab states for orders, count {}'.format(len(priority_orders_links)))
        priority_orders_with_statuses = self.grab_order_statuses(priority_orders_links)
        return priority_orders_with_statuses

    def get_non_critical(self, color='all'):
        log.info('get all main links')
        main_links = self.get_main_links()
        log.info('get all orders links')
        order_links = self.get_order_links(main_links)
        log.info('grab states for orders, count {}'.format(len(order_links)))
        orders_with_statuses = self.grab_order_statuses(order_links)
        return orders_with_statuses
