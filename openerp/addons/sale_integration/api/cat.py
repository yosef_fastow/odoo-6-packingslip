# -*- coding: utf-8 -*-
from emailapiclient import EmailApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging
from pf_utils.utils.re_utils import f_d
import base64


_logger = logging.getLogger(__name__)

SETTINGS_FIELDS = (
    ('emails_for_sending_inventory',     'email list',           ''),
    ('emails_for_sending_confirm',       'email list(confirm)',  ''),
)


class CatApi(EmailApiClient):

    def __init__(self, settings):
        super(CatApi, self).__init__(settings)
        self.logger = _logger

    def upload_data(self):
        return {'lines': self.lines}


class CatApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(CatApiClient, self).__init__(
            settings_variables, False, SETTINGS_FIELDS, False)

        self._mail_messages = []

    def updateQTY(self, lines, mode=None):
        updateApi = CatApiUpdateQTY(self.settings, lines)
        res = updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        if(res):
            self._mail_messages.append({
                'email_to_list': self.settings['emails_for_sending_inventory'],
                'subject': 'Cat Inventory Report',
                'attachments': {
                    updateApi.filename: base64.b64encode(res),
                },
            })
            self._mail_messages = self.validate_mail_messages(
                self._mail_messages)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class CatApiUpdateQTY(CatApi):

    def __init__(self, settings, lines):
        super(CatApiUpdateQTY, self).__init__(settings)
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.filename = f_d(
            'cat_inventory-%Y-%m-%d-%H%M%S.csv', datetime.utcnow())
        self._path_to_backup_local_dir = self.create_local_dir(
            self.backup_local_path, "Cat", "inventory", "now")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            line['inventory_date'] = f_d(
                '%Y/%m/%d', datetime.utcnow())
            if(line.get('customer_id_delmar', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')

        return {'lines': self.revision_lines['good']}
