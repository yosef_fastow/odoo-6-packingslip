# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.saks_edi_parser import EDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime, timedelta
import logging
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import random
from json import dumps as json_dumps
from json import loads as json_loads
from math import ceil
from pf_utils.utils.re_utils import f_d
#import tz
from dateutil import tz

_logger = logging.getLogger(__name__)

SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '7011358'),
    ('vendor_name',                 'Vendor name',              'Saks'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('receiver_id_qualifier_810', 'Sender Id Qualifier Invoice', 'ZZ'),
    ('receiver_id_qualifier_856', 'Sender Id Qualifier Shipment', 'ZZ'),
    ('receiver_id_qualifier_850', 'Sender Id Qualifier 850', '01'),
    ('receiver_id_qualifier_997', 'Sender Id Qualifier FA', '01'),
    ('receiver_id_810', 'Receiver Id Invoice', '4142785783T'),
    ('receiver_id_856', 'Receiver Id Shipment', '6092785376T'),
    ('receiver_id_997', 'Receiver Id FA', '006989567T'),
    # ('receiver_id_qualifier',       'Receiver Id Qualifier',    '01'),
    ('contact_name',                'Contact name',             ''),
    ('contact_phone',               'Contact phone',            ''),
    ('edi_x12_version',             'EDI X12 Version',          '4050'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.edi'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      'U'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class SaksApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class SaksApiClient(AbsApiClient):
    use_local_folder = True

    def __init__(self, settings_variables):
        super(SaksApiClient, self).__init__(
            settings_variables, SaksOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = SaksApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = SaksApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        confirmApi = SaksApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        if self.is_invoice_required and lines[0]['action'] == 'v_ship':
            invoiceApi = SaksApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = SaksApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def returnOrders(self, lines):
        ReturnApi = SaksApiReturnOrders(self.settings, lines)
        ReturnApi.process('send')
        self.extend_log(ReturnApi)
        return True


class SaksApiGetOrders(SaksApi):
    """EDI/V5010 X12/850: AAFESS2S Purchase Orders"""

    def __init__(self, settings):
        super(SaksApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class SaksOpenerp(ApiOpenerp):
    number_of_transaction = ''

    def __init__(self):

        super(SaksOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def write_unique_fields_for_orders(self, cr, uid, picking):
        if picking and picking.sale_id and picking.sale_id.sale_integration_xml_id:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            serial_reference_number_obj = \
                uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id)
            if not serial_reference_number_obj:
                raise ValueError('Not Available any serial_reference_number')
            serial_reference_number_obj.update({
                'sscc_id': uffo_obj.generate_sscc_id(
                    cr, uid,
                    [serial_reference_number_obj['id']],
                    application_identifier='00',
                    extension_digit='0'
                ).get(serial_reference_number_obj['id'], '')
            })
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if srn_row_ids:
                sale_order_fields_obj.write(
                    cr, uid, srn_row_ids, {
                        'name': 'serial_reference_number',
                        'label': 'serial_reference_number',
                        'value': json_dumps(serial_reference_number_obj)
                    }
                )
            else:
                self.pool.get('sale.order').write(
                    cr, uid, picking.sale_id.id, {
                        'additional_fields': [(0, 0, {
                            'name': 'serial_reference_number',
                            'label': 'serial_reference_number',
                            'value': json_dumps(serial_reference_number_obj)
                        })]
                    }
                )
            uffo_obj.use_for(cr, uid, serial_reference_number_obj['id'], picking.id)
        return True

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
                [
                    (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                    for key, value in order.get('additional_fields', {}).iteritems()
                ],
        }

        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line
        additional_fields = []
        line_obj.update({
            'notes': '',
            'sku': line['vendorSku'],
            'vendorSku': line['vendorSku'],
            'cost': line['cost'],
            'upc': line['upc'],
            'merchantSKU': line['upc'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        if line.get('poLineData').get('prodColor'):
            additional_fields.append((0, 0, {
                'name': 'prod_color',
                'label': 'Product Color',
                'value': line.get('poLineData').get('prodColor', '')
            }))

        if line.get('poLineData').get('prodSize'):
            additional_fields.append((0, 0, {
                'name': 'prod_size',
                'label': 'Product Size',
                'value': line.get('poLineData').get('prodSize', '')
            }))

        if line.get('poLineData').get('UP'):
            additional_fields.append((0, 0, {
                'name': 'UP',
                'label': 'UP',
                'value': line.get('poLineData').get('UP', '')
            }))

        if line.get('poLineData').get('store_number'):
            additional_fields.append((0, 0, {
                'name': 'store_number',
                'label': 'store_number',
                'value': line.get('poLineData').get('store_number', '')
            }))

        line_obj.update({'additional_fields': additional_fields})

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}

        additional_fields = {x[2]['name']: x[2]['value'] for x in line.get('additional_fields', []) or [] if x and x[2]}
        field_list = ['vendorSku', 'sku', 'upc', 'merchantSKU']

        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field],
                                                                 search_by)
                if product:
                    line_obj['size_id'] = size and size.id or False
                    break
        if product:
            line_obj['product_id'] = product.id
            if not line.get('cost'):
                product_cost = self.pool.get('product.product').get_val_by_label(
                    cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                if product_cost:
                    line['cost'] = product_cost
                else:
                    line['cost'] = False
                    line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(
                ','.join(
                    [
                        additional_fields.get(x, None)
                        for x in field_list if additional_fields.get(x, None)
                    ]
                )
            )
            line_obj['product_id'] = False

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, lines, context=None):
        line = {
            'serial_reference_number': '',
            'serial_reference_number_id': False,
            'sscc_id': False,
            'address_services': {
                'remmit_to': {
                    'edi_code': 'RI',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
            }
        }
        if (sale_obj.partner_invoice_id):
            line['address_services']['remmit_to'].update({
                'name': 'FIRST CANADIAN DIAMOND CUTTING WORKS',
                'address1': sale_obj.partner_invoice_id.street or '',
                'address2': sale_obj.partner_invoice_id.street2 or '',
                'city': sale_obj.partner_invoice_id.city or '',
                'state': sale_obj.partner_invoice_id.state_id.code or '',
                'country': sale_obj.partner_invoice_id.country_id.code or '',
                'zip': sale_obj.partner_invoice_id.zip or '',
            })
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })

            serial_reference_number_obj = {'id': False, 'value': False}
            order_additional_fields = lines[0]['order_additional_fields']
            if 'serial_reference_number' in order_additional_fields:
                serial_reference_number_obj = json_loads(order_additional_fields['serial_reference_number'])
            else:
                raise ValueError('Not Available any serial_reference_number')
            line.update({
                'serial_reference_number': serial_reference_number_obj['value'],
                'serial_reference_number_id': serial_reference_number_obj['id'],
                'sscc_id': serial_reference_number_obj['sscc_id'],
            })

        return line

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):
        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_line_id': order_line.external_customer_line_id,
            'product_qty': order_line.product_uom_qty,

        }

        return line


class SaksApiConfirmShipment(SaksApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(SaksApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]%(str_code)s'

        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        str_code = '0001'
        if self.confirmLines[0]['action'] == 'v_cancel':
            str_code = '0004'
        bsn_data = {
            'purpose_code': '00',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time,
            'str_code': str_code,
        }

        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),
            'hl_number_prev': str(''),
            'code': 'S'

        }
        segments.append(self.insertToStr(hl, hl_data))

        td1 = 'TD1*' \
              '[1]%(packing_code)s*' \
              '[2]%(number_of_cartons)s*' \
              '[3]*' \
              '[4]*' \
              '[5]*' \
              '[6]G*' \
              '[7]%(weight)s*' \
              '[8]LB'

        weight =  int(ceil(self.confirmLines[0].get('weight', 1) * 0.0022))
        if weight < 1:
            weight = 0.0022

        segments.append(self.insertToStr(td1, {
            'packing_code': 'CTN25',
            'number_of_cartons': 1,
            'weight': weight   # Order
        }))

        td5 = 'TD5*' \
              '[1]*' \
              '[2]2*' \
              '[3]%(carrier_code)s*' \
              '[4]%(type_code)s*' \
              '[5]%(carrier_name)s*' \
              '[6]%(status)s'

        td5_data = {
            'carrier_code': self.confirmLines[0]['carrier_code'],
            'type_code': 'A',
            'carrier_name': self.confirmLines[0]['carrier'],
            'status': 'CC'
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(detail)s'
        segments.append(self.insertToStr(ref, {
            'code': 'CN',
            'detail': self.confirmLines[0]['tracking_number']
        }))
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))
        # n1 = 'N1*[1]SF*[2]%(company)s'
        # segments.append(self.insertToStr(n1, {
        #     'company': 'company',
        # }))
        # n1 = 'N1*[1]ST*[2]*[3]92*[4]9999'
        # segments.append(self.insertToStr(n1, {}))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))

        segments.append(self.insertToStr(td1, {
            'packing_code': 'CTN25',
            'number_of_cartons': 1,
            'weight': weight  # Order
        }))

        ref = 'REF*[1]%(code)s*[2]%(detail)s'

        segments.append(self.insertToStr(ref, {
            'code': str("DP"),
            'detail': self.confirmLines[0]['order_additional_fields'].get('DP', '')
        }))

        ref = 'REF*[1]%(code)s[2]%(detail)s'
        segments.append(self.insertToStr(ref, {
            'code': str("IV"),
            'detail': self.confirmLines[0]['invoice']
        }))

        n1 = 'N1*[1]BY*[2]*[3]92*[4]%(store_number)s'
        segments.append(self.insertToStr(n1, {
            'store_number': self.confirmLines[0]['additional_fields'].get('store_number',''),
        }))

        hl_number_prev = hl_number
        hl_number += 1
        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'
        # segments.append(self.insertToStr(hl, {
        #     'hl_number': str(hl_number),
        #     'hl_number_prev': str(hl_number_prev),
        #     'code': 'P'  # Order
        # }))

        man = 'MAN*[1]GM*[2]%(carton_number)s'
        segments.append(self.insertToStr(man, {
            'carton_number': self.confirmLines[0]['sscc_id'],
        }))

        lin = 'LIN*' \
              '[1]*' \
              '[2]UP*' \
              '[3]%(upc)s'

        item_statuses = ['AC', 'ID']

        # sn1 = 'SN1*' \
        #       '[1]*' \
        #       '[2]%(qty)s*' \
        #       '[3]EA*' \
        #       '[4]*' \
        #       '[5]%(qty_ordered)s*' \
        #       '[6]%(man_code)s*' \
        #       '[7]*' \
        #       '[8]%(item_status)s'
        #
        # if self.confirmLines[0]['action'] == 'v_cancel':
        sn1 = 'SN1*' \
              '[1]*' \
              '[2]%(qty)s*' \
              '[3]EA'

        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            segments.append(self.insertToStr(lin, {
                'upc': line.get('merchantSKU',''),
                'sku': line['vendorSku'],
            }))

            if line['action'] == 'v_cancel':
                segments.append(self.insertToStr(sn1, {
                    # 'qty': 0,
                    'qty': int(line['product_qty']),
                    # 'man_code': 'EA',
                    # 'item_status': 'ID',
                }))
            else:
                segments.append(self.insertToStr(sn1, {
                    'qty': int(line['product_qty']),
                    # 'qty_ordered': '',
                    # 'man_code': '',
                    # 'item_status': 'AC',
                }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(hl_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        isa_control_number = str(random.randrange(100000000, 999999999))
        gs_control_number = str(random.randrange(1000, 99999999))
        date_now = datetime.utcnow()
        receiver_id = self.receiver_id_856
        if hasattr(self, 'repetition_separator'):
            repetition_separator = self.repetition_separator
        else:
            repetition_separator = '*'

        if hasattr(self, 'standard_identifier'):
            standard_identifier = self.standard_identifier
        else:
            standard_identifier = 'U'

        params = {
            'group': 'SH',
            'isa_data': {
                'sender_id_qualifier': self.sender_id_qualifier,
                'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                'receiver_id_qualifier': self.receiver_id_qualifier_856,
                'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                'edi_x12_version_isa': self.edi_x12_version_isa,
                'isa_control_number': isa_control_number,
                'date': date_now.strftime('%y%m%d'),
                'time': date_now.strftime('%H%M'),
                'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                'repetition_separator': repetition_separator,
                'standard_identifier': standard_identifier,
            },
            # 'gs_data': {
            #     'group': 'SH',
            #     'sender_id': self.sender_id,
            #     'receiver_id': receiver_id,
            #     'date': date_now.strftime('%Y%m%d'),
            #     'time': date_now.strftime('%H%M'),
            #     'gs_control_number': gs_control_number,
            #     'edi_x12_version_gs': '00' + self.edi_x12_version + 'VICS',
            # }
        }

        return self.wrap(segments, params)


class SaksApiUpdateQTY(SaksApi):
    """EDI/V4030 X12/846: 846 Inventory Inquiry"""

    def __init__(self, settings, lines):
        super(SaksApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]00*[2]SI*[3]%(inventory_feed_qualifier)s*[4]%(date)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'date': date.strftime('%Y%m%d'),
            'inventory_feed_qualifier': inventory_feed_qualifier,
        }))

        ref = 'REF*[1]IO*[2]Saks'
        segments.append(self.insertToStr(ref, {
            'data': self.vendor_number,
        }))

        lin = 'LIN*' \
              '[1]%(product_qualifier)s*' \
              '[2]VN*' \
              '[3]%(product_identifying_number)s*' \
              '[4]UP*' \
              '[5]%(sku)s'


        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        ctp = 'CTP*[1]*[2]UCP*[3]%(price)s'
        qty = 'QTY*[1]%(qty_q)s*[2]%(qty)s'
        for line in self.updateLines:
            if line.get('upc', False) in (None, False) or line.get('customer_id_delmar', False) in (None, False):
                continue
            eta_date = False
            try:
                if line.get('eta', False):
                    tmp_date = datetime.strptime(line['eta'], "%m/%d/%Y").replace(tzinfo=self.tz)
                    if tmp_date > date:
                        eta_date = tmp_date
            except Exception:
                pass
            segments.append(self.insertToStr(lin, {
                'product_qualifier': '1',
                'product_identifying_number': line.get('customer_id_delmar', False) or '',
                'sku': str(line['upc']),
                'customer_id_delmar': line.get('customer_id_delmar', False) or ''
            }))

            dtm_code = '036'  # Available
            dtm_date = date

            segments.append(self.insertToStr(dtm, {
                'code': dtm_code,
                'date': dtm_date.strftime('%Y%m%d')
            }))

            # todo: no unit_price here!
            segments.append(self.insertToStr(ctp, {
                'price': str(line.get('unit_price', ''))
            }))

            segments.append(self.insertToStr(qty, {
                'qty': str(line['qty']),
                'qty_q': str(61)
            }))
            self.append_to_revision_lines(line, 'good')
        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))
        return self.wrap(segments, {'group': 'IB'})


class SaksApiInvoiceOrders(SaksApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(SaksApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.now()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        ref = 'REF*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(ref, {
            'code': 'MA',
            'number': self.invoiceLines[0]['tracking_number']
        }))
        segments.append(self.insertToStr(ref, {
            'code': 'DP',
            'number': self.invoiceLines[0]['order_additional_fields'].get('DP', '')
        }))

        n1 = 'N1*[1]%(code)s*[2]*[3]92*[4]%(number)s'
        segments.append(self.insertToStr(n1, {
            'code': 'ST',
            'number': '9999'
        }))
        # segments.append(self.insertToStr(n1, {
        #     'code': 'RI',
        #     'number': self.invoiceLines[0]['additional_fields'].get('remit_o_duns_number','')
        # }))
        segments.append(self.insertToStr(n1, {
            'code': 'BY',
            'number': self.invoiceLines[0]['additional_fields'].get('store_number','')
        }))

        import calendar
        from calendar import monthrange
        now = datetime.now()
        itd = 'ITD*[1]01*[2]2*[3]%(term_discount_percent)s*[4]*[5]%(term_discount_day_off)s*[6]*[7]%(term_net_days)s*[8]*[9]*[10]*[11]*[12]*[13]%(count_day_od_mounth)s'
        # segments.append(self.insertToStr(itd, {
        #     'term_discount_percent': '',
        #     'term_discount_day_off': '',
        #     'term_net_days': '',
        #     'count_day_od_mounth': calendar.monthrange(now.year, now.month)[1]
        # }))

        dmt = 'DMT*[1]011*[2]%(date)s'
        # segments.append(self.insertToStr(dmt, {
        #     'date': invoice_date
        # }))

        fob = 'FOB*[1]CC*[2]*[3]*[4]*[5]*[6]AC*[7]%(city_and_state)s'
        # segments.append(self.insertToStr(fob, {
        #      'city_and_state': self.invoiceLines[0]['remit_to_city'] + '/' + self.invoiceLines[0]['remit_to_state']
        # }))

        it1 = 'IT1*' \
              '[1]*' \
              '[2]%(qty)s*' \
              '[3]EA*' \
              '[4]%(unit_price)s*' \
              '[5]QT*' \
              '[6]UP*' \
              '[7]%(product_identifying_number)s*' \
              '[8]UP*' \
              '[9]%(product_identifying_number)s'
        pid = 'PID*[1]F*[2]08*[3]*[4]*[5]%(description)s'
        line_number = 0
        total_qty_invoiced = 0
        total_price_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = line['product_qty']
            except ValueError:
                qty = 1

            product_qualifier = None
            product_identifying_number = ''
            for p_qualifier in product_qualifiers:
                if (line['additional_fields'].get(p_qualifier, False)):
                    product_qualifier = p_qualifier
                    product_identifying_number = line['additional_fields'][p_qualifier]
                    break

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': qty,
                'unit_price': line['unit_cost'],
                'product_identifying_number': line.get('merchantSKU')
            }))
            total_qty_invoiced += qty
            total_price_invoiced += qty * float(line['unit_cost'])

            # segments.append(self.insertToStr(pid, {
            #      'description':  line['name']
            # }))

        tds = 'TDS*[1]%(total_amount)s*[2]%(amount_discount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(total_price_invoiced or 0.0)).replace('.', ''),
            'amount_discount': ''
        }))

        cad = 'CAD*[1]*[2]*[3]*[4]*[5]%(carrier)s*[6]*[7]BM*[8]%(number)s'
        # segments.append(self.insertToStr(cad, {
        #     'carrier': self.invoiceLines[0]['carrier_code'],
        #     'number': self.invoiceLines[0]['tracking_number']
        # }))

        sac = 'SAC*[1]%(code)s*[2]%(name)s*[3]*[4]*[5]%(value)s*[6]*[7]*[8]*[9]*[10]*[11]*[12]02*[13]*[14]*[15]%(reason)s'
        # segments.append(self.insertToStr(sac, {
        #     'code': 'A',
        #     'name': 'Service',
        #     'value': 'Dollar Value',
        #     'reason': 'Reason for Allowance'
        # }))

        iss = 'ISS*[1]%(number_of_cartons)s*[2]CA*[3]%(weight)s*[4]LB'
        segments.append(self.insertToStr(iss, {
            'number_of_cartons': 1,
            'weight': self.invoiceLines[0]['weight'] or 0
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        isa_control_number = str(random.randrange(100000000, 999999999))
        date_now = datetime.utcnow()
        receiver_id = self.receiver_id_810
        if hasattr(self, 'repetition_separator'):
            repetition_separator = self.repetition_separator
        else:
            repetition_separator = '*'

        if hasattr(self, 'standard_identifier'):
            standard_identifier = self.standard_identifier
        else:
            standard_identifier = 'U'

        params = {
            'group': 'IN',
            'isa_data': {
                'sender_id_qualifier': self.sender_id_qualifier,
                'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                'receiver_id_qualifier': self.receiver_id_qualifier_810,
                'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                'edi_x12_version_isa': self.edi_x12_version_isa,
                'isa_control_number': isa_control_number,
                'date': date_now.strftime('%y%m%d'),
                'time': date_now.strftime('%H%M'),
                'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                'repetition_separator': repetition_separator,
                'standard_identifier': standard_identifier,
            }
        }

        return self.wrap(segments, params)


class SaksApiReturnOrders(SaksApi):
    """EDI/V4030 X12/180: 180 Return"""
    lines = []

    def __init__(self, settings, lines):
        super(SaksApiReturnOrders, self).__init__(settings)
        self.lines = lines
        self.use_ftp_settings('return_orders')
        self.edi_type = '180'

    def upload_data(self):
        segments = []
        date_now = datetime.now()

        st = 'ST*[1]%(edi_type)s*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'edi_type': '180',
            'st_number': st_number,
        }
        segments.append(self.insertToStr(st, st_data))
        bgn = 'BGN*[1]00*[2]%(ref)s*[3]%(date)s'

        bgn_data = {
            'ref': self.lines[0]['order_additional_fields'].get('REF02', None),
            'date': date_now.strftime('%Y%m%d'),
        }
        segments.append(self.insertToStr(bgn, bgn_data))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.lines[0]['po_number'],
        }))

        bli = 'BLI*[1]VN*[2]%(indef_num1)s*[3]%(qty)s*[4]*[5]*[6]*[7]*[8]UP*[9]%(indef_num2)s'
        n9 = 'N9*[1]RZ*%(number_of_transaction)s'
        rdr = 'RDR*[1]*[2]SP'
        prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]*[5]%(assigned_identification)s'
        dtm = 'DTM*[1]050*[2]%(date)s'
        for line in self.lines:
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(bli, {
                'indef_num1': str(line.get('customer_id_delmar', '')),  # line['vendorSku'],
                'qty': qty,
                'indef_num2': str(line.get('upc', ''))
            }))

            segments.append(self.insertToStr(n9, {
                'number_of_transaction': line['number_of_transaction'],
            }))

            segments.append(self.insertToStr(rdr, {}))

            segments.append(self.insertToStr(prf, {
                'po_number': line['po_number'],
                'assigned_identification': line['number_of_transaction'],
            }))

            segments.append(self.insertToStr(dtm, {
                'date': date_now.strftime('%Y%m%d'),
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'AN'})

class SaksApiFunctionalAcknoledgment(SaksApi):
    orders = []

    def __init__(self, settings, orders):
        super(SaksApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        k = 0
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                k += 1
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak2 = 'AK2*[1]850*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak2, {
                    'ack_control_number': "%04d" % int(order['ack_control_number'])
                }))
                ak5 = 'AK5*[1]A*[2]'
                segments.append(self.insertToStr(ak5, {
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))

                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                isa_control_number = str(random.randrange(100000000, 999999999))
                date_now = datetime.utcnow()
                receiver_id = self.receiver_id_997
                if hasattr(self, 'repetition_separator'):
                    repetition_separator = self.repetition_separator
                else:
                    repetition_separator = '*'

                if hasattr(self, 'standard_identifier'):
                    standard_identifier = self.standard_identifier
                else:
                    standard_identifier = 'U'


                params = {
                    'group': 'FA',
                    'isa_data': {
                        'sender_id_qualifier': self.sender_id_qualifier,
                        'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
                        'receiver_id_qualifier': self.receiver_id_qualifier_997,
                        'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
                        'edi_x12_version_isa': self.edi_x12_version_isa,
                        'isa_control_number': isa_control_number,
                        'date': date_now.strftime('%y%m%d'),
                        'time': date_now.strftime('%H%M'),
                        'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
                        'repetition_separator': repetition_separator,
                        'standard_identifier': standard_identifier
                    }
                }


                data.append(self.wrap(segments, params))
        return data

