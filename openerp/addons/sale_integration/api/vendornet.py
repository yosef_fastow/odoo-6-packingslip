# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from customer_parsers import vendornet_csv_parser
from apiopenerp import ApiOpenerp
import logging
from openerp.addons.pf_utils.utils.re_utils import f_d
from datetime import datetime

_logger = logging.getLogger(__name__)


class VendornetApi(FtpClient):

    def __init__(self, settings):
        super(VendornetApi, self).__init__(settings)

    def check_response(self, response):
        if (hasattr(response, 'ok')):
            if (not response.ok):
                raise Exception(str(response.stderr))

    def set_decrypt_data(self, data):
        res = ""
        if(self.customer == 'Zales'):
            gpg = self.get_gnupg(self.gnupghome)
            res = gpg.decrypt(data, passphrase=self.passphrase).data
        else:
            res = data
        return res


class VendornetApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(VendornetApiClient, self).__init__(
            settings_variables, VendornetOpenerp, False, False)
        self.load_orders_api = VendornetApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if (save_flag):
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, serialized_order):
        ordersApi = YamlOrder(serialized_order)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = VendornetApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        return True


class VendornetApiGetOrdersXML(VendornetApi):

    def __init__(self, settings):
        super(VendornetApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = vendornet_csv_parser.VendornetApiGetOrdersXML(self)

    def parse_response(self, response):
        csv_parser = vendornet_csv_parser.VendornetApiGetOrdersXML(self)
        ordersList = csv_parser.parse_response(response)
        return ordersList


class VendornetApiUpdateQTY(VendornetApi):
    filename = "inv.txt"
    lines = []
    name = "UpdateQTY"
    cur_ftp_setting = ""
    date_format = "%Y%m%d"

    def __init__(self, settings, lines):
        super(VendornetApiUpdateQTY, self).__init__(settings)
        self.filename_local = f_d("inv(%Y%m%d%H%M%f).csv")
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates('inventory')
        self.lines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        self.lines = self.prepareUpdateQTYLines(self.lines)

        feed_date = datetime.now().strftime(self.date_format)

        for line in self.lines:

            line['feed_date'] = feed_date

            if (line.get('customer_price', False)):
                line['customer_price'] = "%.2f" % line['customer_price']
            if (
                line.get('qty', False) is not False and
                line.get('customer_price', False) and
                line.get('customer_id_delmar')
            ):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}


class VendornetOpenerp(ApiOpenerp):

    def __init__(self):
        super(VendornetOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'customerCost': line['customerCost'],
            "product_id": False,
            "size_id": False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'additional_fields': [
                (0, 0, {'name': 'store', 'label': 'Store',
                        'value': line.get('store', '')}),
            ]
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not line_obj.get('size_id', False):
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif(line.get('size', False)):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if(len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]
                        else:
                            line_obj['size_id'] = False
                            line['size'] = False
                    else:
                        line_obj['size_id'] = False
                if not line_obj.get('product_id', False):
                    if product:
                        line_obj["product_id"] = product.id
                        line_obj['cost'] = line.get('cost', False)
                        if not line_obj['cost'] or line_obj['cost'] == 0:
                            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                             context['customer_id'],
                                                                                             'Customer Price',
                                                                                             line_obj['size_id'])
                            if product_cost:
                                line['cost'] = product_cost
                                break
                            else:
                                line['cost'] = False
                                line_obj["notes"] = "Can't find product cost.\n"

        return line_obj
