# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.direct_buy_edi_parser import DirectBuyEDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime
import logging
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import random
from json import dumps as json_dumps
from json import loads as json_loads
from math import ceil
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '7823'),
    ('vendor_name',                 'Vendor name',              'DirectBuy'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '2197361100'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    '12'),
    ('contact_name',                'Contact name',             ''),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '4010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.x12'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      '^'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}

SEPARATOR = '*'

class DirectBuyApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)

class DirectBuyApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(DirectBuyApiClient, self).__init__(
            settings_variables, DirectBuyOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = DirectBuyApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = DirectBuyApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        manual_order = lines[0]['manual_order']
        if manual_order:
            return True

        confirmApi = DirectBuyApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)
        if self.is_invoice_required:
            invoiceApi = DirectBuyApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = DirectBuyApiAcknowledgementOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True


    def updateQTY(self, lines, mode=None):
        updateApi = DirectBuyApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class DirectBuyApiUpdateQTY(DirectBuyApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(DirectBuyApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.revision_lines = {
            'bad': [],
            'good': [],
        }

    @property
    def filename(self):
        if (
            hasattr(self, '_filename') and
            self._filename
        ):
            pass
        else:
            self._filename = "{:inv-%Y-%m-%d-%H%M%S.csv}".format(datetime.now())
        return self._filename

    def upload_data(self):
        for line in self.lines:
            self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class DirectBuyApiGetOrders(DirectBuyApi):
    """EDI/V5010 X12/850: AAFESS2S Purchase Orders"""

    def __init__(self, settings):
        super(DirectBuyApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = DirectBuyEDIParser(self)
        self.edi_type = '850'

class DirectBuyApiFunctionalAcknoledgment(DirectBuyApi):

    orders = []

    def __init__(self, settings, orders):
        super(DirectBuyApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        k=0
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                k +=1
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST'+SEPARATOR+'[1]997'+SEPARATOR+'[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak2 = 'AK2*[1]850*[2]%(ack_control_number)s'
                segments.append(self.insertToStr(ak2, {
                    'ack_control_number': order['ack_control_number']+"%04d" % (int(k))
                }))
                ak5 = 'AK5*A'
                segments.append(self.insertToStr(ak5, {
                }))

                ak9 = 'AK9*[1]A*' \
                      '[2]%(number_of_transaction)s*' \
                      '[3]%(number_of_transaction)s*' \
                      '[4]%(number_of_transaction)s'

                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))

                se = 'SE'+SEPARATOR+'%(segment_count)s'+SEPARATOR+'%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))
                data.append(self.wrap(segments, {'group': 'FA'}))
        return data

class DirectBuyApiConfirmShipment(DirectBuyApi):
    """EDI/V5010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(DirectBuyApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'
        self._default_weight = 1

    def upload_data(self):
        segments = []
        st = 'ST'+SEPARATOR+'[1]856'+SEPARATOR+'[2]%(st_number)s'
        st_number = '0001'
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN'+SEPARATOR+'[1]%(purpose_code)s'+SEPARATOR+'[2]%(shipment_identification)s'+SEPARATOR+'[3]%(shp_date)s'+SEPARATOR+'[4]%(shp_time)s'+SEPARATOR+'[5]0004'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        shp_time = shp_date.strftime('%H%M')

        bsn_data = {
            'purpose_code': '00',
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL'+SEPARATOR+'[1]%(hl_number)s'+SEPARATOR+'[2]%(hl_number_prev)s'+SEPARATOR+'[3]%(code)s'+SEPARATOR+'[4]%(hierarchical_child_code)s'

        hl_data = {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'S',  # Shipment,
            'hierarchical_child_code': '',

        }
        segments.append(self.insertToStr(hl, hl_data))

        td1 = 'TD1'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(qty)s'+SEPARATOR+'[3]'+SEPARATOR+'[4]'+SEPARATOR+'[5]'+SEPARATOR+'[6]%(weight_qualifier)s'+SEPARATOR+'[7]%(weight)s'+SEPARATOR+'[8]%(weight_uom)s'
        weight = self.confirmLines[0]['weight']
        try:
            weight = float(weight)
            if not weight:
                weight = self._default_weight
        except:
            weight = self._default_weight

        segments.append(self.insertToStr(td1, {
            'code': '',
            'qty': sum([int(x['product_qty']) for x in self.confirmLines if int(x['product_qty'])]),
            'weight': int(ceil(weight * 0.0022)),
            'weight_uom': 'LB',
            'weight_qualifier': 'G',
        }))


        td5 = 'TD5'+SEPARATOR+'[1]O'+SEPARATOR+'[2]2'+SEPARATOR+'[3]SCAC'+SEPARATOR+'[4]O'+SEPARATOR+'[5]%(carrier)s'
        transportationMethod = {
            'A': 'Air',
            'M': 'Common Carrier',
            'O': 'Containerized Ocean'
        }
        td5_data = {
            'carrier': self.confirmLines[0]['carrier'],
            'carrier_code': self.confirmLines[0]['carrier_code']
        }
        segments.append(self.insertToStr(td5, td5_data))



        ref = 'REF'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(data)s'

        segments.append(self.insertToStr(ref, {
            'code': 'CN',
            'data': self.confirmLines[0]['tracking_number'],
        }))
        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Vendor ID Number
            'data': self.confirmLines[0]['order_additional_fields']['IA']
        }))

        dtm = 'DTM'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        fob = 'FOB'+SEPARATOR+'[1]%(shipment_method_of_payment)s'
        segments.append(self.insertToStr(fob, {
            'shipment_method_of_payment': 'PP',
        }))

        # loop N1
        n1 = 'N1'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(data)s'+SEPARATOR+'[3]%(identification_code_qualifier)s'+SEPARATOR+'[4]%(identification_code)s'
        n3 = 'N3'+SEPARATOR+'[1]%(address1)s'
        n4 = 'N4'+SEPARATOR+'[1]%(city)s'+SEPARATOR+'[2]%(state)s'+SEPARATOR+'[3]%(zip)s'+SEPARATOR+'[1]%(country)s'

        # N1 SF
        duns = self.confirmLines[0]['order_additional_fields'].get('DUNS') or ''

        # N1 ST
        original_addresses = self.confirmLines[0]['order_additional_fields'].get('addresses', {})
        if isinstance(original_addresses, (str, unicode)):
            original_addresses = json_loads(original_addresses)
        ship = original_addresses.get('ship', {})
        segments.append(self.insertToStr(n1, {
            'code': 'ST',  # Ship To
            'data': self.confirmLines[0]['ship_to_name_1'],
            'identification_code_qualifier': '92',
            'identification_code': ship.get('facility_number',''),
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['ship_to_address_line_1']
        }))

        country_code = self.confirmLines[0].get('order_additional_fields', {}).get('ship_to_country')
        if not country_code or country_code.lower() in ['null', 'none', 'false']:
            country_code = self.confirmLines[0]['ship_to_country']
        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['ship_to_city'],
            'state': self.confirmLines[0]['ship_to_state'],
            'zip': self.confirmLines[0]['ship_to_postal_code'],
            'country': country_code
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': '',
            'code': 'O',  # Order
            'hierarchical_child_code': '',
        }))

        prf = 'PRF'+SEPARATOR+'[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number'],
        }))

        original_addresses = self.confirmLines[0]['order_additional_fields'].get('addresses', {})

        td5 = 'TD5' + SEPARATOR +\
              '[1]' + SEPARATOR +\
              '[2]' + SEPARATOR +\
              '[3]' + SEPARATOR +\
              '[4]' + SEPARATOR + \
              '[5]%(carrier)s' + SEPARATOR +\
              '[6]%(carrier_code)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier'],
            'carrier_code': 'CC',
        }

        segments.append(self.insertToStr(td5, td5_data))

        lin = 'LIN'+SEPARATOR+\
              '[1]%(number)s'+SEPARATOR+\
              '[2]SK' + SEPARATOR + \
              '[3]%(item_name_identificator)s'+SEPARATOR+\
              '[4]VN'+SEPARATOR+ \
              '[5]%(purchase_item_code)s'

        sn1 = 'SN1'+SEPARATOR+'[1]'+SEPARATOR+'[2]%(qty)s'+SEPARATOR+'[3]EA'+SEPARATOR+'[4]'+SEPARATOR+'[5]%(qty)s'+SEPARATOR+'[6]EA'
        # po4 = 'PO4'+SEPARATOR+'[1]%(qty)s'
        hl_number_prev = hl_number
        number_line = 0
        for line in self.confirmLines:
            hl_number += 1
            number_line += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': '',
                'code': 'I',  # Item
                'hierarchical_child_code': '',
            }))

            item_name_identificator = 'UP'
            purchase_item_code = line['additional_fields'].get(item_name_identificator, '')
            customerSku = str(line.get('customer_sku', ''))
            vendorSku = str(line.get('vendorSku', False))
            if not vendorSku or vendorSku == 'False':
                vendorSku = customerSku
            segments.append(self.insertToStr(lin, {
                'number': number_line,
                'item_name_identificator': customerSku,
                'purchase_item_code': vendorSku,#
            }))

            qty = line['product_qty']
            try:
                qty = int(qty)
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(sn1, {
                'qty': qty,
            }))


        ctt = 'CTT'+SEPARATOR+'[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': len(self.confirmLines)
        }))

        se = 'SE'+SEPARATOR+'%(segment_count)s'+SEPARATOR+'%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class DirectBuyApiInvoiceOrders(DirectBuyApi):
    """EDI/V5010 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(DirectBuyApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'


    def upload_data(self):
        segments = []

        st = 'ST'+SEPARATOR+'[1]810'+SEPARATOR+'[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG'+SEPARATOR+\
              '[1]%(date)s'+SEPARATOR+\
              '[2]%(invoice_number)s'+SEPARATOR+\
              '[3]%(po_date)s'+SEPARATOR+\
              '[4]%(po_number)s'
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        currency_code = self.invoiceLines[0]['order_additional_fields'].get('currency', 'USD')
        if currency_code == '[]' or not currency_code:
            currency_code = 'USD'

        cur = 'CUR'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'code': 'SE',
            'currency_code': currency_code,
        }))


        ref = 'REF'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(order_number)s'
        ref_codes = [
            'IA',  # Internal Vendor Number
            'BM',  # Bill of Lading Number
        ]

        for ref_code in ref_codes:
            ref_value = self.invoiceLines[0]['order_additional_fields'].get(ref_code, False)
            if ref_value:
                segments.append(self.insertToStr(ref, {
                    'code': ref_code,
                    'order_number': ref_value,
                }))


        original_addresses = self.invoiceLines[0]['order_additional_fields'].get('addresses', {})
        if isinstance(original_addresses, (str, unicode)):
            original_addresses = json_loads(original_addresses)
        for address_type in ['ST']:
            address = original_addresses.get(address_type_map.get(address_type, ''), {})
            if address:
                # if address_type in ['ST', 'BY']:
                ship = original_addresses.get('ship', {})
                n1 = 'N1'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(name)s'+SEPARATOR+'[3]%(identification_code_qualifier)s'+SEPARATOR+'[4]%(facility_number)s'
                segments.append(self.insertToStr(n1, {
                    'code': address_type,
                    'name': check_none(address['name']),
                    'identification_code_qualifier': '92',
                    'facility_number': check_none(self.invoiceLines[0]['order_additional_fields'].get('facility_number', '')),
                }))

                country_code = self.invoiceLines[0].get('order_additional_fields', {}).get('ship_to_country')
                if not country_code or country_code.lower() in ['null', 'none', 'false']:
                    country_code = self.invoiceLines[0]['ship_to_country']

                n3 = 'N3' + SEPARATOR + '[1]%(adress_info)s'
                adres_info = self.invoiceLines[0]['return_address_line_1'] or self.invoiceLines[0]['ship_to_address_line_1']
                segments.append(self.insertToStr(n3, {
                    'adress_info': adres_info
                }))

                n4 = 'N4'+SEPARATOR+'[1]%(city)s'+SEPARATOR+'[2]%(state)s'+SEPARATOR+'[3]%(postal_code)s'+SEPARATOR+'[4]%(country)s'
                segments.append(self.insertToStr(n4, {
                    'city': address['city'],
                    'state': address['state'],
                    'postal_code': address['zip'],
                    'country': country_code,
                }))

        duns = self.invoiceLines[0]['order_additional_fields'].get('DUNS') or ''
        invoice =  original_addresses.get('invoice',{})
        n1 = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(duns_number)s'
        segments.append(self.insertToStr(n1, {
            'code': 'RI',
            'identification_code_qualifier': '92',
            'name': invoice.get('name',''),
            'duns_number': duns,
        }))
        n3 = 'N3' + SEPARATOR + '[1]%(adress_info)s'

        segments.append(self.insertToStr(n3, {
            'adress_info':invoice.get('address1', '')
        }))

        n4 = 'N4' + SEPARATOR + '[1]%(city)s' + SEPARATOR + '[2]%(state)s' + SEPARATOR + '[3]%(postal_code)s' + SEPARATOR + '[4]%(country)s'
        country = invoice.get('country', '')
        if not country:
            country = ''
        segments.append(self.insertToStr(n4, {
            'city': invoice.get('city', ''),
            'state': invoice.get('state', ''),
            'postal_code': invoice.get('zip', ''),
            'country': country,
        }))

        terms_of_sale = json_loads(self.invoiceLines[0]['order_additional_fields'].get('terms_of_sale', '{}'))

        itd = 'ITD'+SEPARATOR+'[1]%(terms_type_code)s'+SEPARATOR+'[2]03'+SEPARATOR+'[3]%(terms_discount_percent)s'+SEPARATOR+'[4]%(terms_discount_due_date)s'+SEPARATOR+'[5]%(terms_discount_days_due)s'+SEPARATOR+'[6]%(terms_net_due_date)s'+SEPARATOR+'[7]%(terms_net_days)s'+SEPARATOR+'[8]'+SEPARATOR+'[9]'+SEPARATOR+'[10]'+SEPARATOR+'[11]'+SEPARATOR+'[12]%(description)s'

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        segments.append(self.insertToStr(itd, {
            'terms_type_code': '01',
            'terms_discount_percent': check_none(str(terms_of_sale.get('terms_discount_percent'))),
            'terms_discount_due_date': check_none(str(terms_of_sale.get('terms_discount_due_date'))),
            'terms_discount_days_due': check_none(str(terms_of_sale.get('terms_discount_days_due'))),
            'terms_net_due_date': shp_date.strftime('%Y%m%d'),
            'terms_net_days': 1,
            'description': '',
        }))

        dtm = 'DTM'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        it1 = 'IT1'+SEPARATOR+\
              '[1]%(line_number)s'+SEPARATOR+\
              '[2]%(qty)s'+SEPARATOR+\
              '[3]EA'+SEPARATOR+\
              '[4]%(unit_price)s'+SEPARATOR+\
              '[5]'+SEPARATOR+\
              '[6]%(upc_qualifier)s'+SEPARATOR+\
              '[7]%(customer_sku)s'+SEPARATOR+\
              '[8]VC'+SEPARATOR+\
              '[9]%(vendor_sku)s'

        ctp = 'CTP'+SEPARATOR+'[1]'+SEPARATOR+'[2]%(code)s'+SEPARATOR+'[3]%(value)s'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            upc = line['additional_fields'].get('UP', '') or ''
            upc_qualifier = upc and 'UP' or 'SK'
            vc = line['additional_fields'].get('VC', '') or ''

            customer = str(line.get('customer_sku', ''))
            vendor = str(line.get('vendorSku', False))
            if not vendor or  vendor == 'False':
                vendor = customer

            segments.append(self.insertToStr(it1, {
                'qty': str(qty),
                'unit_price': '{0:.2f}'.format(float(line['price_unit'])),
                'upc_qualifier': upc_qualifier,
                'upc': vc,
                'line_number': line_number,
                'customer_sku': customer,
                'vendor_sku': vendor,
            }))

            RPP = line['additional_fields'].get('RPP', '') or ''
            if RPP:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RPP)),
                }))
            RTL = line['additional_fields'].get('RTL', '') or ''
            if RTL:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RTL)),
                }))


            total_qty_invoiced += qty
        tds = 'TDS'+SEPARATOR+'[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total_shipping'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        sac = 'SAC'+SEPARATOR+\
              '[1]C'+SEPARATOR+\
              '[2]G830'+SEPARATOR+\
              '[3]'+SEPARATOR+\
              '[4]'+SEPARATOR+\
              '[5]%(amount)s'+SEPARATOR+\
              '[6]'+SEPARATOR+\
              '[7]'
        segments.append(self.insertToStr(sac, {
            'amount': '{0:.2f}'.format(float(self.invoiceLines[0]['shp_handling'] or 0.0)).replace('.', ''),
        }))

        ctt = 'CTT'+SEPARATOR+'[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))


        return self.wrap(segments, {'group': 'IN'})


class DirectBuyApiAcknowledgementOrders(DirectBuyApi):
    """EDI/V4030 X12/855: 855 Purchase Order Acknowledgment"""

    def __init__(self, settings, lines, state):
        super(DirectBuyApiAcknowledgementOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        print self.processingLines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel'):
            return self.accept_cancel()

        self.edi_type = '855'

        lines = []
        #Acknowledge - With Detail and Change
        statuscode = 'AC'
        if (self.state == 'accepted'):
            # Acknowledge - With Detail, No Change
            statuscode = 'AD'

        segments = []

        # ------------------------------ST----------------------------
        st = 'ST'+SEPARATOR+'[1]855'+SEPARATOR+'[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))
        #------------------------------BAK----------------------------
        bak = 'BAK'+SEPARATOR+'[1]06'+SEPARATOR+'[2]%(statuscode)s'+SEPARATOR+'[3]%(po_number)s'+SEPARATOR+'[4]%(po_date)s'+SEPARATOR+'[5]'+SEPARATOR+'[6]'+SEPARATOR+'[7]'+SEPARATOR+'[8]'+SEPARATOR+'[9]%(ackn_date)s'

        now_dtm = datetime.utcnow()
        now_date = now_dtm.strftime('%Y%m%d')
        now_time = now_dtm.strftime('%H%M')

        processing_date = self.processingLines[0].get('date', False) or now_date

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines[0]['po_number'],
            'po_date': processing_date,  # CCYYMMDD
            'ackn_date': processing_date,  # CCYYMMDD
        }
        segments.append(self.insertToStr(bak, bak_data))
        # ------------------------------REF----------------------------
        ref_field = self.processingLines[0].get('order_additional_fields',{})

        ref = 'REF' + SEPARATOR + '[1]%(code)s' + SEPARATOR + '[2]%(order_number)s'
        segments.append(self.insertToStr(ref, {
            'code': ref_field.get('VN','IA'),
            'order_number': ref_field.get('IA','')
        }))

      # ------------------------------PO1----------------------------
        po1 = 'PO1'+SEPARATOR+\
              '[1]%(line_number)s'+SEPARATOR+\
              '[2]%(qty)s'+SEPARATOR+\
              '[3]EA'+SEPARATOR+\
              '[4]%(unit_price)s'+SEPARATOR+\
              '[5]'+SEPARATOR+\
              '[6]SK'+SEPARATOR+ \
              '[7]%(customerSku)s' + SEPARATOR + \
              '[8]VC' + SEPARATOR + \
              '[9]%(vendorSku)s'

        po1_size = 'PO1'+SEPARATOR+\
                   '[1]%(line_number)s'+SEPARATOR+\
                   '[2]%(qty)s'+SEPARATOR+\
                   '[3]EA'+SEPARATOR+\
                   '[4]%(unit_price)s'+SEPARATOR+\
                   '[5]'+SEPARATOR+\
                   '[6]%(optionSku_qualifier)s'+SEPARATOR+\
                   '[7]%(optionSku)s'+SEPARATOR+\
                   '[8]%(merchantSKU_qualifier)s'+SEPARATOR+\
                   '[9]%(merchantSKU)s'+SEPARATOR+\
                   '[10]%(optionSku_qualifier)s'+SEPARATOR+\
                   '[11]%(optionSku)s'+SEPARATOR+\
                   '[12]'+SEPARATOR+\
                   '[13]'+SEPARATOR+\
                   '[14]SM'+SEPARATOR+\
                   '[15]%(size)s'

        ack = 'ACK'+SEPARATOR+\
              '[1]%(line_status)s'+SEPARATOR+\
              '[2]'+SEPARATOR+\
              '[3]'+SEPARATOR+\
              '[4]086'+SEPARATOR+\
              '[5]%(date)s'+SEPARATOR+ \
              '[6]%(po_number)s'
        date_now = datetime.utcnow()
        i = 1

        for line in self.processingLines:
            template = po1
            vendorSku = line.get('vendorSku', False) or ''
            vendorSku_qualifier = vendorSku and 'VP' or ''
            merchantSKU = line.get('merchantSKU', False) or ''
            merchantSKU_qualifier = merchantSKU and 'UK' or ''
            optionSku = line.get('optionSku', False) or ''
            price_unit = line.get('additional_fields', '') and line.get('additional_fields', False).get('unit_price_code', '') or ''
            line_number = format(i, '03')
            qty = line.get('product_qty', '')

            customerSku = str(line.get('customer_sku', False))
            vendorSku = str(line.get('vendorSku', ''))
            if not customerSku or customerSku == 'False':
                customerSku = vendorSku
            if qty:
                qty = int(qty)
            insert_obj = {
                'line_number': line_number,
                'qty': qty,
                'unit_price': price_unit,
                'identifier': vendorSku,
                'customerSku': customerSku,
                'vendorSku': vendorSku
            }

            if line.get('size', False) is not False:
                line['size'] = line['size'].replace('.', '')
                template = po1_size
                insert_obj['size'] = ('0' * (4 - len(line['size']))) + line['size']

            segments.append(self.insertToStr(template, insert_obj))
            segments.append(self.insertToStr(ack, {
                'line_status': 'IA',
                # 'qty': line.get('product_qty', ""),
                'date': date_now.strftime('%Y%m%d'),
                'po_number': self.processingLines[0]['po_number']
            }))
            i += 1

        ctt = 'CTT'+SEPARATOR+'%(number_line_items)s'
        segments.append(self.insertToStr(ctt, {
            'number_line_items': len(self.processingLines)
        }))

        se = 'SE'+SEPARATOR+'%(segment_count)s'+SEPARATOR+'%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '865'
        segments = []

        st = 'ST'+SEPARATOR+'[1]865'+SEPARATOR+'[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA'+SEPARATOR+'[1]01'+SEPARATOR+'[2]%(statuscode)s'+SEPARATOR+'[3]%(po_number)s'+SEPARATOR+'[4]'+SEPARATOR+'[5]'+SEPARATOR+'[6]%(po_date)s'

        date = datetime.strptime(self.processingLines[0]['create_date'], '%Y-%m-%d %H:%M:%S.%f')

        statuscode = 'AT'
        if self.state == 'accept_cancel':
            statuscode = 'AT'
        elif self.state == 'rejected_cancel':
            statuscode = 'RJ'

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))
        # REF
        ref = 'REF'+SEPARATOR+'[1]%(code)s'+SEPARATOR+'[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        se = 'SE'+SEPARATOR+'%(segment_count)s'+SEPARATOR+'%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))
        # CA
        return self.wrap(segments, {'group': ''})



class DirectBuyOpenerp(ApiOpenerp):

    def __init__(self):

        super(DirectBuyOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def write_unique_fields_for_orders(self, cr, uid, picking):
        if picking and picking.sale_id and picking.sale_id.sale_integration_xml_id:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            serial_reference_number_obj = \
                uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id)
            if not serial_reference_number_obj:
                raise ValueError('Not Available any serial_reference_number')
            serial_reference_number_obj.update({
                'sscc_id': uffo_obj.generate_sscc_id(
                    cr, uid,
                    [serial_reference_number_obj['id']],
                    application_identifier='00',
                    extension_digit='0'
                ).get(serial_reference_number_obj['id'], '')
            })
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if srn_row_ids:
                sale_order_fields_obj.write(
                    cr, uid, srn_row_ids, {
                        'name': 'serial_reference_number',
                        'label': 'serial_reference_number',
                        'value': json_dumps(serial_reference_number_obj)
                    }
                )
            else:
                self.pool.get('sale.order').write(
                    cr, uid, picking.sale_id.id, {
                        'additional_fields': [(0, 0, {
                            'name': 'serial_reference_number',
                            'label': 'serial_reference_number',
                            'value': json_dumps(serial_reference_number_obj)
                        })]
                    }
                )
            uffo_obj.use_for(cr, uid, serial_reference_number_obj['id'], picking.id)
        return True

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line

        line_obj.update({
            'notes': '',
            'sku': line['vendorSKU'],
            'vendorSku': line['vendorSKU'],
            'customerCost': line['retail_price'] or line['cost'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}

        additional_fields = {x[2]['name']: x[2]['value'] for x in line.get('additional_fields', []) or [] if x and x[2]}
        field_list = ['vendorSku', 'sku','upc']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], line[field], search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break
        if product:
            line_obj['product_id'] = product.id
            if not line.get('cost'):
                product_cost = self.pool.get('product.product').get_val_by_label(
                    cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
                if product_cost:
                    line['cost'] = product_cost
                else:
                    line['cost'] = False
                    line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(
                ','.join(
                    [
                        additional_fields.get(x, None)
                        for x in field_list if additional_fields.get(x, None)
                    ]
                )
            )
            line_obj['product_id'] = False

        return line_obj

    def get_additional_confirm_shipment_information(self, cr, uid, sale_obj, deliver_id, ship_data, context=None):
        line = {
            'address_services': {
                'remmit_to': {
                    'edi_code': 'RI',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
                'ship_to': {
                    'edi_code': 'ST',
                    'name': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'country': '',
                    'zip': '',
                },
            }
        }
        if (sale_obj.partner_invoice_id):
            line['address_services']['remmit_to'].update({
                'name': 'FIRST CANADIAN DIAMOND CUTTING WORKS',
                'address1': sale_obj.partner_invoice_id.street or '',
                'address2': sale_obj.partner_invoice_id.street2 or '',
                'city': sale_obj.partner_invoice_id.city or '',
                'state': sale_obj.partner_invoice_id.state_id.code or '',
                'country': sale_obj.partner_invoice_id.country_id.code or '',
                'zip': sale_obj.partner_invoice_id.zip or '',
            })
        if (sale_obj.partner_shipping_id):
            line['address_services']['ship_to'].update({
                'name': sale_obj.partner_shipping_id.name or '',
                'address1': sale_obj.partner_shipping_id.street or '',
                'address2': sale_obj.partner_shipping_id.street2 or '',
                'city': sale_obj.partner_shipping_id.city or '',
                'state': sale_obj.partner_shipping_id.state_id.code or '',
                'country': sale_obj.partner_shipping_id.country_id.code or '',
                'zip': sale_obj.partner_shipping_id.zip or '',
            })
        return line

    def after_correct_confirm_shipment_callback(self, cr, uid, sale_obj, picking_obj, lines):
        if 'serial_reference_number_id'in lines[0] and lines[0]['serial_reference_number_id']:
            self.pool.get('unique.fields.for.orders').use_for(
                cr, uid, lines[0]['serial_reference_number_id'], picking_obj.id)
        return True

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['po_number'], tuple(customer_ids)))

        return cr.dictfetchall()

    # def get_accept_information(self, cr, uid, settings, sale_order, context=None):
    #
    #     vendor_address = False
    #
    #     for addr in sale_order.partner_id.address:
    #         if addr.type == 'invoice':
    #             vendor_address = addr
    #             break
    #     date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
    #
    #     result = {
    #         'po_number': sale_order['po_number'],
    #         'external_customer_order_id': sale_order.external_customer_order_id,
    #         'date': date.strftime('%Y%m%d'),
    #         'order_number': sale_order.name,
    #         'vendor_name': 'DELMAR',
    #         'address1': '',
    #         'city': '',
    #         'state': '',
    #         'zip': '',
    #         'country': '',
    #         'ship_to_address': {
    #             'edi_code': 'ST',
    #             'name': '',
    #             'address1': '',
    #             'address2': '',
    #             'city': '',
    #             'state': '',
    #             'country': '',
    #             'zip': '',
    #         },
    #     }
    #     if vendor_address:
    #         result['address1'] = vendor_address.street
    #         result['city'] = vendor_address.city
    #         result['state'] = vendor_address.state_id.code
    #         result['zip'] = vendor_address.zip
    #         result['country'] = vendor_address.country_id.code
    #
    #     if (sale_order.partner_shipping_id):
    #         result['ship_to_address'].update({
    #             'name': sale_order.partner_shipping_id.name or '',
    #             'address1': sale_order.partner_shipping_id.street or '',
    #             'address2': sale_order.partner_shipping_id.street2 or '',
    #             'city': sale_order.partner_shipping_id.city or '',
    #             'state': sale_order.partner_shipping_id.state_id.code or '',
    #             'country': sale_order.partner_shipping_id.country_id.code or '',
    #             'zip': sale_order.partner_shipping_id.zip or '',
    #         })
    #
    #     return [result]

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):

        line = {
            'po_number': sale_obj.po_number,
            'order_additional_fields': {x.name: x.value for x in sale_obj.additional_fields or []},
            'additional_fields': {x.name: x.value for x in order_line.additional_fields or []},
            'external_customer_order_id': sale_obj.external_customer_order_id,
            'product_qty': order_line.product_uom_qty,
            'external_customer_line_id': order_line.external_customer_line_id,
            'vendorSku': order_line.vendorSku,
        }

        return line
