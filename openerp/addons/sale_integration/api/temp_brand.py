# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from datetime import datetime
import logging
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class TempBrandApi(FtpClient):

    def __init__(self, settings):
        super(TempBrandApi, self).__init__(settings)


class TempBrandApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(TempBrandApiClient, self).__init__(settings_variables, None, None, None)

    def updateQTY(self, lines, mode=None):
        updateApi = TempBrandApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        confirmApi = TempBrandApiConfirmOrders(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res_confirm


class TempBrandApiUpdateQTY(TempBrandApi):

    def __init__(self, settings, lines):
        super(TempBrandApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "inventory.csv"
        self.filename_local = f_d("inventory_%Y%m%d%_H%M%S.csv")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            _sku = line.get('customer_id_delmar', False)
            if((not _sku) or (_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class TempBrandApiConfirmOrders(TempBrandApi):

    def __init__(self, settings, lines):
        super(TempBrandApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("confirm_{order_name}_%d%m%y_%H%M%S.csv".format(order_name=str(lines[0]['origin'])))

    def upload_data(self):
        lines = self.lines
        for line in lines:
            product_qty = 0
            if(line.get('product_qty', False)):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
            date_now = datetime.now()
            if(line.get('shp_date', False)):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y")
            line['product_qty'] = product_qty
        return {'lines': lines}
