# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.bealls_input_edi_parser import EDIParser
import random
from datetime import datetime, timedelta
from apiopenerp import ApiOpenerp
from dateutil import tz
import logging
from openerp.addons.pf_utils.utils.yamlutils import YamlObject

_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            1143),
    ('vendor_name',                 'Vendor name',              "Beall's"),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              'BEALLSINC'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    'ZZ'),
    ('sender_van',                  'Sender VAN',               'AS2'),
    ('receiver_van',                'Receiver VAN',             'Liaison Technologies (Nubridges)'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '4030'),
    ('filename_format',             'Filename Format',          'DEL{edi_type}_{date}.out'),
    ('line_terminator',             'Line Terminator',          r'~\n'),
    ('repetition_separator',        'Repetition Separator',     '}'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('duns',                        'DUNS',                     '153114348'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class BeallsApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, EDIParser)


class BeallsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(BeallsApiClient, self).__init__(
            settings_variables, BeallsOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = BeallsApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = BeallsApiAcknowledgmentOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):

        confirmApi = BeallsApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = BeallsApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = BeallsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmLoad(self, orders):
        api = BeallsApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True


class BeallsApiGetOrders(BeallsApi):
    """EDI/V4030 X12/850: 850 Purchase Order"""

    def __init__(self, settings):
        super(BeallsApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = EDIParser(self)
        self.edi_type = '850'


class BeallsApiChangeOrders(BeallsApi):
    """EDI/V4030 X12/860: 860 Purchase Order Change"""

    def __init__(self, settings):
        super(BeallsApiChangeOrders, self).__init__(settings)
        self.edi_type = '860'


class BeallsApiInvoiceOrders(BeallsApi):
    """EDI/V4030 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(BeallsApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []
        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]%(po_date)s*[4]%(po_number)s'
        invoice_date = datetime.utcnow()

        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break

        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
            'po_date': external_date_order and external_date_order.strftime("%Y%m%d") or '',
        }))

        ref = 'REF*[1]%(code)s*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            # CO - Customer Order Number
            'code': 'CO',
            'data': self.invoiceLines[0]['order_additional_fields'].get('customer_order_number', None)
        }))

        segments.append(self.insertToStr(ref, {
            # IA - Internal Vendor Number
            'code': 'IA',
            'data': self.invoiceLines[0]['order_additional_fields'] and
            self.invoiceLines[0]['order_additional_fields'].get('internal_vendor_number', None) or
            self.vendor_number
        }))

        n1_st = 'N1*[1]ST*[2]%(name)s'
        segments.append(self.insertToStr(n1_st, {
            'name':  self.invoiceLines[0]['ship_to_name_1'],
        }))

        n3_st = 'N3*[1]%(address1)s'
        segments.append(self.insertToStr(n3_st, {
            'address1': self.invoiceLines[0]['ship_to_address_line_1']
        }))

        n1_vn = 'N1*[1]VN*[2]%(name)s*[3]1*[4]%(duns)s'
        segments.append(self.insertToStr(n1_vn, {
            'name': self.invoiceLines[0]['remit_to_name_1'],
            'duns': self.duns,
        }))

        n3_vn = 'N3*[1]%(address1)s'
        segments.append(self.insertToStr(n3_vn, {
            'address1': self.invoiceLines[0]['remit_to_address_line_1']
        }))

        n4_vn = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[1]%(country)s'
        segments.append(self.insertToStr(n4_vn, {
            'city': self.invoiceLines[0]['remit_to_city'],
            'state': self.invoiceLines[0]['remit_to_state'],
            'postal_code': self.invoiceLines[0]['remit_to_postal_code'],
            'country': self.invoiceLines[0]['remit_to_country'],
        }))

        terms_net_days = 5
        terms_net_due_date = ''
        if (external_date_order):
            terms_net_due_date = external_date_order + timedelta(days=terms_net_days)
        else:
            terms_net_due_date = datetime.utcnow() + timedelta(days=terms_net_days)
        terms_net_due_date = terms_net_due_date.strftime('%Y%m%d')

        itd = 'ITD*[1]%(code)s*[2]%(terms_basis_date_code)s*[3]*[4]*[5]*[6]%(terms_net_due_date)s*[7]%(terms_net_days)s*[8]*[9]*[10]*[11]*[12]%(terms_net_days_str)s'
        segments.append(self.insertToStr(itd, {
            'code': '05',  # Discount Not Applicable
            # 08 - Basic Discount Offered
            # 12 - 10 Days After End of Month (10 EOM)
            # 14 - Previously agreed upon
            'terms_basis_date_code': 3,
            # 3 Invoice Date
            'terms_net_due_date': terms_net_due_date,
            'terms_net_days': str(terms_net_days),
            'terms_net_days_str': 'Net {0} Days'.format(terms_net_days),
        }))

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        it1 = 'IT1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(product_qualifier)s*[7]%(product_identifying_number)s*[8]VA*[9]%(sku)s*[10]CB*[11]%(customer_id_delmar)s*[12]PL*[13]%(merchantSKU)s'
        pid = 'PID*[1]%(description_type)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(data)s'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            product_qualifier = None
            product_identifying_number = None
            for p_qualifier in product_qualifiers:
                if (line['additional_fields'].get(p_qualifier, False)):
                    product_qualifier = p_qualifier
                    product_identifying_number = line['additional_fields'][p_qualifier]
                    break

            if ((not product_qualifier) or (not product_identifying_number)):
                raise Exception('Required product_qualifier and product_identifying_number')

            segments.append(self.insertToStr(it1, {
                'line_number': str(line_number),
                'qty': str(qty),
                'unit_price': line['unit_cost'],
                'product_qualifier': product_qualifier,
                'product_identifying_number': product_identifying_number,
                'sku': line['vendorSku'],
                'customer_id_delmar': line['customer_id_delmar'],
                'merchantSKU': line['merchantSKU'],
            }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'characteristic_code': '08',
                # 08 Product
                # 75 Buyer's Color Description
                # 91 Buyer's Item Size Description
                'data': line['name'],
            }))

            total_qty_invoiced += qty

        tds = 'TDS*[1]%(total_amount)s*[2]%(total_amount)s*[3]%(total_amount)s*[4]000'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        cad = 'CAD*[1]*[2]*[3]*[4]*[5]%(carrier)s*[6]*[7]%(reference_identification_qualifier)s*[8]%(reference_identification)s'
        segments.append(self.insertToStr(cad, {
            'carrier': self.invoiceLines[0]['carrier_code'],
            # 2I - Tracking Number
            # BM - Bill of Lading Number
            # CN - Carrier's Reference Number (PRO/Invoice)
            'reference_identification_qualifier': '2I',
            'reference_identification': self.invoiceLines[0]['tracking_number'],
        }))

        iss = 'ISS*[1]%(total_qty_invoiced)s*[2]EA'
        segments.append(self.insertToStr(iss, {
            'total_qty_invoiced': total_qty_invoiced,
        }))

        ctt = 'CTT*[1]%(items_count)s*[2]%(total_qty_invoiced)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
            'total_qty_invoiced': total_qty_invoiced,
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IN'})


class BeallsApiAcknowledgmentOrders(BeallsApi):
    """EDI/V4030 X12/855: 855 Purchase Order Acknowledgment"""

    def __init__(self, settings, lines, state):
        super(BeallsApiAcknowledgmentOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.processingLines = lines
        self.state = state

    def upload_data(self):
        if self.state in ('accept_cancel', 'rejected_cancel'):
            return self.accept_cancel()

        self.edi_type = '855'

        lines = []
        statuscode = 'AK'
        if (self.state == 'accepted'):
            # PO1/ACK segments are not required when using this code.
            # This code indicates to Bealls that you accept all items.
            statuscode = 'AK'
        elif (self.state == 'accepted_with_details'):
            # PO1/ACK segment usage required when using this code.
            # All ACK01 values must contain IA.
            statuscode = 'AD'
        elif self.state == 'reject':
            # PO1/ACK segments are not required when using this code.
            # This code indicates to Bealls that you reject all items.
            statuscode = 'RJ'
        elif self.state == 'reject_with_details':
            # PO1/ACK segment usage required when using this code.
            # All ACK01 values must contain IR.
            statuscode = 'RD'

        segments = []

        st = 'ST*[1]855*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bak = 'BAK*[1]00*[2]%(statuscode)s*[3]%(po_number)s*[4]%(po_date)s*[5]*[6]*[7]*[8]*[9]%(ackn_date)s*DS'

        if self.processingLines.get('date', False) is False:
            self.processingLines['date'] = datetime.utcnow().strftime('%Y%m%d')

        bak_data = {
            'statuscode': statuscode,
            'po_number': self.processingLines['po_number'],
            'po_date': self.processingLines['date'],  # CCYYMMDD
            'ackn_date': self.processingLines['date'],  # CCYYMMDD
        }

        segments.append(self.insertToStr(bak, bak_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'CO',  # Customer Order Number
            'order_number': self.processingLines['external_customer_order_id'],
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        n1 = 'N1*[1]VN*[2]%(vendor_name)s'  # VN Vendor
        segments.append(self.insertToStr(n1, {
            'vendor_name': self.vendor_name,
        }))

        n3 = 'N3*[1]%(address1)s'

        address1 = self.processingLines['ship_to_address']['address1']
        if (not address1):
            raise Exception('Ship to address is required for 855(Purchase Order Acknowledgment)!')
        segments.append(self.insertToStr(n3, {
            'address1': address1,
        }))

        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        segments.append(self.insertToStr(n4, {
            'city': self.processingLines.get('city', ''),
            'state': self.processingLines.get('state', ''),
            'zip': self.processingLines.get('zip', ''),
            'country': self.processingLines.get('country', ''),
        }))

        po1 = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(optionSku_qualifier)s*[7]%(optionSku)s*[8]%(merchantSKU_qualifier)s*[9]%(merchantSKU)s*[10]%(optionSku_qualifier)s*[11]%(optionSku)s"
        po1_size = "PO1*[1]%(line_number)s*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(optionSku_qualifier)s*[7]%(optionSku)s*[8]%(merchantSKU_qualifier)s*[9]%(merchantSKU)s*[10]%(optionSku_qualifier)s*[11]%(optionSku)s*[12]*[13]*[14]SM*[15]%(size)s"
        ack = "ACK*[1]%(line_status)s*[2]%(qty)s*[3]EA*[4]068*[5]%(date)s"
        i = 1
        date_now = datetime.utcnow()
        for line in lines:
            template = po1
            vendorSku = line['vendorSku'] or ''
            vendorSku_qualifier = vendorSku and 'VP' or ''
            merchantSKU = line['merchantSKU'] or ''
            merchantSKU_qualifier = merchantSKU and 'UK' or ''
            optionSku = line['optionSku'] or ''
            optionSku_qualifier = optionSku and 'SK' or ''
            insert_obj = {
                'line_number': str(i),
                'qty': line['product_qty'],
                'unit_price': line['price_unit'],
                'vendorSku_qualifier': vendorSku_qualifier,
                'vendorSku': vendorSku,
                'merchantSKU_qualifier': merchantSKU_qualifier,
                'merchantSKU': merchantSKU,
                'optionSku': optionSku,
                'optionSku_qualifier': optionSku_qualifier,
            }

            if line['size'] is not False:
                line['size'] = line['size'].replace('.', '')
                template = po1_size
                insert_obj['size'] = ('0' * (4 - len(line['size']))) + line['size']

            segments.append(self.insertToStr(template, insert_obj))
            segments.append(self.insertToStr(ack, {
                'line_status': 'IR',  # Item Rejected
                'qty': line['product_qty'],
                'date': date_now.strftime('%Y%m%d'),  # CCYYMMDD
            }))
            i += 1

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'PR'})

    def accept_cancel(self):
        self.edi_type = '865'
        segments = []

        st = 'ST*[1]865*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bca = 'BCA*[1]01*[2]%(statuscode)s*[3]%(po_number)s*[4]*[5]*[6]%(po_date)s'

        date = datetime.strptime(self.processingLines[0]['create_date'], '%Y-%m-%d %H:%M:%S.%f')

        statuscode = 'AT'
        if self.state == 'accept_cancel':
            statuscode = 'AT'
        elif self.state == 'rejected_cancel':
            statuscode = 'RJ'

        bca_data = {
            'statuscode': statuscode,  # confirms that you are able to cancel the PO
            'po_number': self.processingLines[0]['po_number'],
            'po_date': date.strftime('%Y%m%d'),
            #'ackn_date': date.strftime('%Y%m%d'),
        }

        segments.append(self.insertToStr(bca, bca_data))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'

        segments.append(self.insertToStr(ref, {
            'code': 'IA',  # Internal Vendor Number
            'order_number': self.vendor_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'CA'})


class BeallsApiConfirmShipment(BeallsApi):
    """EDI/V4030 X12/856: 856 Ship Notice"""

    def __init__(self, settings, lines):
        super(BeallsApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        shp_time = shp_date.strftime('%H%M%S%f')
        if len(shp_time) > 8:
            shp_time = shp_time[:8]

        bsn_data = {
            'purpose_code': '06',  # 00 Original (Warehouse ASN), 06 Confirmation (Dropship ASN)
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S'  # Shipment

        }
        segments.append(self.insertToStr(hl, hl_data))

        td5 = 'TD5*[1]*[2]2*[3]SCAC*[4]*[5]%(carrier)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier_code']
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(data)s'

        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Bill of Lading Number
        #     'data': 'NEED_DATA'
        # }))
        # segments.append(self.insertToStr(ref, {
        #     'code': 'BM',  # Master Bill of Lading (Common Carrier shipments only)
        #     'data': 'NEED_DATA'
        # }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d')
        }))

        # loop N1
        n1 = 'N1*[1]%(code)s*[2]%(data)s'
        n3 = 'N3*[1]%(address1)s'
        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        # N1 SF
        segments.append(self.insertToStr(n1, {
            # SF - Ship From
            # ST - Ship To
            'code': 'SF',  # Ship From
            'data':  self.confirmLines[0]['shipFrom']['name']
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['shipFrom']['street']
        }))

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['shipFrom']['city'],
            'state': self.confirmLines[0]['shipFrom']['state'],
            'zip': self.confirmLines[0]['shipFrom']['zip'],
            'country': self.confirmLines[0]['shipFrom']['country']
        }))

        # N1 ST
        segments.append(self.insertToStr(n1, {
            # SF - Ship From
            # ST - Ship To
            'code': 'ST',  # Ship From
            'data':  self.confirmLines[0]['ship_to_name_1']
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['ship_to_address_line_1']
        }))

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['ship_to_city'],
            'state': self.confirmLines[0]['ship_to_state'],
            'zip': self.confirmLines[0]['ship_to_postal_code'],
            'country': self.confirmLines[0]['ship_to_country']
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'O'  # Order
        }))

        prf = 'PRF*[1]%(po_number)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number']
        }))

        segments.append(self.insertToStr(ref, {
            # CO - Customer Order Number
            'code': 'CO',
            'data': self.confirmLines[0]['order_additional_fields'].get('customer_order_number', None)
        }))

        segments.append(self.insertToStr(ref, {
            # IA - Internal Vendor Number
            'code': 'IA',
            'data': self.confirmLines[0]['order_additional_fields'] and
            self.confirmLines[0]['order_additional_fields'].get('internal_vendor_number', None) or
            self.vendor_number
        }))

        segments.append(self.insertToStr(dtm, {
            'code': '003',
            'date': shp_date.strftime('%Y%m%d')
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),
            'hl_number_prev': str(hl_number_prev),
            'code': 'P'  # Pack
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'CP',  # Carrier-Assigned Package ID Number
            'number': self.confirmLines[0]['tracking_number']
        }))

        product_qualifiers = ['EN', 'UK', 'UP']

        lin = 'LIN*[1]%(line_id)s*[2]%(product_qualifier)s*[3]%(product_identifying_number)s*[4]VA*[5]%(sku)s*[6]CB*[7]%(customer_id_delmar)s*[8]PL*[9]%(merchantSKU)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'
        pid = 'PID*[1]%(description_type)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(data)s'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I'  # Item
            }))

            product_qualifier = None
            product_identifying_number = None
            for p_qualifier in product_qualifiers:
                if (line['additional_fields'].get(p_qualifier, False)):
                    product_qualifier = p_qualifier
                    product_identifying_number = line['additional_fields'][p_qualifier]
                    break

            if ((not product_qualifier) or (not product_identifying_number)):
                raise Exception('Required product_qualifier and product_identifying_number')

            segments.append(self.insertToStr(lin, {
                'line_id': line['external_customer_line_id'],
                'product_qualifier': product_qualifier,
                'product_identifying_number': product_identifying_number,
                'sku': line['vendorSku'],
                'customer_id_delmar': line['customer_id_delmar'],
                'merchantSKU': line['merchantSKU'],
            }))
            segments.append(self.insertToStr(sn1, {
                'qty': line['product_qty']
            }))
            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'characteristic_code': '08',
                # 08 Product
                # 75 Buyer's Color Description
                # 91 Buyer's Item Size Description
                'data': line['name'],
            }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class BeallsApiUpdateQTY(BeallsApi):
    """EDI/V4030 X12/846: 846 Inventory Inquiry"""

    def __init__(self, settings, lines):
        super(BeallsApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.updateLines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }
        self.tz = tz.gettz(CUSTOMER_PARAMS['timezone'])
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        inventory_feed_qualifier = str(random.randrange(1000000000000, 9999999999999))
        bia = 'BIA*[1]00*[2]MB*[3]%(inventory_feed_qualifier)s*[4]%(date)s'
        date = self.tz.fromutc(datetime.utcnow().replace(tzinfo=self.tz))

        segments.append(self.insertToStr(bia, {
            'date': date.strftime('%Y%m%d'),
            'inventory_feed_qualifier': inventory_feed_qualifier,
        }))

        ref = 'REF*[1]IA*[2]%(data)s'
        segments.append(self.insertToStr(ref, {
            'data': self.vendor_number,
        }))

        per = 'PER*[1]%(code)s*[2]%(name)s*[3]%(com_number_code)s*[4]%(com_number)s'
        segments.append(self.insertToStr(per, {
            'code': 'IC',  # Information Contact
            'name': self.contact_name,
            'com_number_code': 'TE',  # Telephone
            'com_number': self.contact_phone
        }))

        n1 = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(inventory_feed_qualifier)s*'
        segments.append(self.insertToStr(n1, {
            'code': 'VN',  # Vendor
            'name': self.sender_id,
            'identification_code_qualifier': '91',  # Assigned by Seller or Seller's Agent
            'inventory_feed_qualifier': inventory_feed_qualifier,
        }))

        product_identifying_mapping = {
            'EN': ('sku', 13),
            'UK': ('merchantSKU', 14),
            'UP': ('upc', 12),
        }

        lin = 'LIN*[1]*[2]%(product_qualifier)s*[3]%(product_identifying_number)s*[4]VA*[5]%(sku)s*[6]CB*[7]%(customer_id_delmar)s'
        pid = 'PID*[1]%(code)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(desc)s'
        qty = 'QTY*[1]%(code)s*[2]%(qty)s*[3]%(uof_code)s'
        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        for line in self.updateLines:
            product_qualifier = 'UP'
            if (not line.get(product_identifying_mapping[product_qualifier][0], False)):
                self.append_to_revision_lines(line, 'bad')
                continue

            eta_date = False
            try:
                if line.get('eta', False):
                    tmp_date = datetime.strptime(line['eta'], "%m/%d/%Y").replace(tzinfo=self.tz)
                    if tmp_date > date:
                        eta_date = tmp_date
            except Exception:
                pass

            self.append_to_revision_lines(line, 'good')

            segments.append(self.insertToStr(lin, {
                # EN EAN/UCC - 13-digit EAN
                # UK EAN/UCC - 14-digit GTIN
                # UCC - 12-digit Vendor UPC
                'product_qualifier': product_qualifier,
                'product_identifying_number': line[
                    product_identifying_mapping[product_qualifier][0]
                ].rjust(product_identifying_mapping[product_qualifier][1], '0'),
                'sku': line['sku'],
                'customer_id_delmar': line['customer_id_delmar'],
            }))

            if line.get('size'):
                # 91 - Vendor size description
                segments.append(self.insertToStr(pid, {
                    'code': 'F',  # Free-form
                    'characteristic_code': '91',
                    'desc': line['size']
                }))

            segments.append(self.insertToStr(pid, {
                'code': 'F',  # Free-form
                'characteristic_code': '08',  # Product
                'desc': str(line['description'])[0:80]
            }))

            segments.append(self.insertToStr(qty, {
                'code': '33',  # Quantity Available for Sale (stock quantity)
                'qty': str(line['qty']),  # Product
                'uof_code': 'EA'  # Each
            }))

            # When DTM01=018
            # This date should be "today's date", unless the Qty Available for Sale is zero. In that case the
            # Availability Date must be past Today's Date indicating when that product will be available
            # again.
            # Date the product is being discontinued if applicable(DTM01=036)
            dtm_code = '018'  # Available
            dtm_date = date
            if line['qty'] in (0, '0'):
                if (line.get('dnr_flag', '0') in (1, '1')):
                    dtm_code = '036'  # Discontinued
                if eta_date:
                    dtm_date = eta_date

            segments.append(self.insertToStr(dtm, {
                'code': dtm_code,
                'date': dtm_date.strftime('%Y%m%d')
            }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'IB'})


class BeallsApiFunctionalAcknoledgment(BeallsApi):

    orders = []

    def __init__(self, settings, orders):
        super(BeallsApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            order = yaml_obj.deserialize(_data=order_yaml['xml'])
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'

                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))

                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class BeallsOpenerp(ApiOpenerp):

    def __init__(self):

        super(BeallsOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        line_obj = {
            "notes": "",
            "name": line['name'],
            'merchantSKU': line['merchantSKU'],
            'optionSku': line['optionSku'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = {}
        field_list = ['vendorSku', 'merchantSKU', 'optionSku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(cr,
                                                                                      uid,
                                                                                      context['customer_id'],
                                                                                      line[
                                                                                          field],
                                                                                      ('default_code', 'customer_sku',
                                                                                       'upc')
                                                                                      )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False

                    product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
                    if product_cost:
                        line['cost'] = product_cost
                        break
                    else:
                        line['cost'] = False
                        line_obj['notes'] += "Can't find product cost for  name %s.\n" % (line['name'])

                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        return line_obj

    def get_accept_information(self, cr, uid, settings, sale_order, context=None):

        vendor_address = False

        for addr in sale_order.partner_id.address:
            if addr.type == 'invoice':
                vendor_address = addr
                break
        date = datetime.strptime(sale_order.create_date, '%Y-%m-%d %H:%M:%S')
        result = {
            'po_number': sale_order['po_number'],
            'external_customer_order_id': sale_order.external_customer_order_id,
            'date': date.strftime('%Y%m%d'),
            'order_number': sale_order.name,
            'vendor_name': 'DELMAR',
            'address1': '',
            'city': '',
            'state': '',
            'zip': '',
            'country': '',
            'ship_to_address': {
                'edi_code': 'ST',
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'country': '',
                'zip': '',
            },
        }
        if vendor_address:
            result['address1'] = vendor_address.street
            result['city'] = vendor_address.city
            result['state'] = vendor_address.state_id.code
            result['zip'] = vendor_address.zip
            result['country'] = vendor_address.country_id.code

        if (sale_order.partner_shipping_id):
            result['ship_to_address'].update({
                'name': sale_order.partner_shipping_id.name or '',
                'address1': sale_order.partner_shipping_id.street or '',
                'address2': sale_order.partner_shipping_id.street2 or '',
                'city': sale_order.partner_shipping_id.city or '',
                'state': sale_order.partner_shipping_id.state_id.code or '',
                'country': sale_order.partner_shipping_id.country_id.code or '',
                'zip': sale_order.partner_shipping_id.zip or '',
            })

        return result

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['poNumber'], tuple(customer_ids)))

        return cr.dictfetchall()

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        customer_skus = [line.vendorSku for line in sale_order.order_line if line.vendorSku]
        customer_skus_str = ' ,'.join(customer_skus)
        res = {
            'Ref2': customer_skus_str[:30] or ''
        }
        return res
