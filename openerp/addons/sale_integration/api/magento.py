# -*- coding: utf-8 -*-
from utils import xmlutils as xml_utils
from apiclient import ApiClient
from abstract_apiclient import AbsApiClient
import time
import xmlrpclib
import netsvc
import logging
from openerp.osv.osv import except_osv
from pf_utils.utils.re_utils import f_d

_logger = logging.getLogger(__name__)


class MagentoApi(ApiClient):

    location = ""
    username = ""
    password = ""
    debug = True

    def __init__(self, settings):
        self.location = settings["location"]
        self.username = settings["username"]
        self.password = settings["password"]
        self.logger = _logger
        self._log = []

    def connect(self):
        self.debug = True
        if not self.location[-1] == '/':
            self.location += '/'
        if self.debug:
            self.logger.info("Attempting connection with Settings:%s,%s,%s" % (self.location, self.username, self.password))
        self.ser = xmlrpclib.ServerProxy(self.location)
        for sleep_time in [1, 3, 6]:
            try:
                self.session = self.ser.login(self.username, self.password)
                if self.debug:
                    self.logger.info("Login Successful")
                return True
            except IOError, e:
                self.logger.error("Error in connecting:%s" % e)
                self.logger.warning("Webservice Failure, sleeping %s second before next attempt" % sleep_time)
                time.sleep(sleep_time)
            except Exception, e:
                self.logger.error("Magento Connection" + netsvc.LOG_ERROR + "Error in connecting:%s" % e)
                self.logger.warning("Webservice Failure, sleeping %s second before next attempt" % sleep_time)
                time.sleep(sleep_time)
        raise except_osv(('User Error'), ('Error when try to connect to magento, are your sure that your login is right? Did openerp can access to your magento?'))

    def call(self, method, *arguments):
        if arguments:
            arguments = list(arguments)[0]
        else:
            arguments = []
        for sleep_time in [1, 3, 6]:
            try:
                if self.debug:
                    self.logger.info(("Calling Method:%s,Arguments:%s") % (method, arguments))
                res = self.ser.call(self.session, method, arguments)
                if self.debug:
                    if method == 'catalog_product.list':
                        # the response of the method catalog_product.list can be very very long so it's better to see it only if debug log is activate
                        self.logger.debug(("Query Returned:%s") % (res))
                    else:
                        self.logger.info(("Query Returned:%s") % (res))
                return res
            except IOError, e:
                self.logger.error(("Method: %s\nArguments:%s\nError:%s") % (method, arguments, e))
                self.logger.warning(("Webservice Failure, sleeping %s second before next attempt") % (sleep_time))
                time.sleep(sleep_time)
        raise

    def send(self):
        data = self.get_request_data()
        self.connect()
        if (self.name == 'loadOrders'):
            orders = self.call(data['method'], data['args'])
            order_list = []
            for item in orders:
                order = self.call('sales_order.info', [item['increment_id']])
                order_list.append(order)
            self._log.append({
                'title': "Response data, method: %s" % (self.name),
                'msg': 'Response data: \n %s' % (str(orders)),
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            return self.parse_response(order_list)

        if (self.name == 'ConfirmOrders'):
            try:

                return_id = self.call('order_shipment.create', [str(data['ext_cust_ord_id'])])
                params = (str(return_id), 'ups', 'tracking title', str(data['track_no']))
                response = self.call('sales_order_shipment.addTrack', params)
                self._log.append({
                    'title': "Response data, method: %s" % (self.name),
                    'msg': 'Response data: \n %s' % (response),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

            except Exception, e:
                print 'error method ConfirmOrders (magento): %s' % e

        if (self.name == 'UpdateQTY'):
            for product in data:
                try:
                    self.call('product_stock.update', (str(product.get('sku', '')), {'qty': product['qty']}))
                except Exception, e:
                    print 'error method UpdateQTY (magento): %s' % e
            self._log.append({
                'title': "Response data, method: %s" % (self.name),
                'msg': 'Request data: \n %s' % (str(data)),
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })

        if (self.name == 'Acknowledgement'):
            status = self.call(data['method'], data['args'])
            self._log.append({
                'title': "Response data, method: %s" % (self.name),
                'msg': 'Response data: \n %s' % (str(status)),
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })


class MagentoApiClient(AbsApiClient):

    settings = {
        "location": "",
        "corelocation": "",
        "username": "",
        "password": "",
    }

    def __init__(self, settings_variables):
        super(MagentoApiClient, self).__init__(
            settings_variables, False, False, False)
        self.load_orders_api = MagentoApiGetOrdersXML

    def loadOrders(self):
        orders = self.load_orders_api.send()
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, xml):
        ordersApi = MagentoApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def Acknowledgement(self, order):
        AcknowledgementApi = MagentoApiAcknowledgement(self.settings, order)
        AcknowledgementApi.send()
        self.extend_log(AcknowledgementApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = MagentoApiUpdateQTY(self.settings, lines)
        updateApi.send()
        self.extend_log(updateApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = MagentoApiConfirmOrders(self.settings, lines)
        confirmApi.send()
        self.extend_log(confirmApi)
        return True


class MagentoApiGetOrdersXML(MagentoApi):

    name = "loadOrders"

    def __init__(self, settings):
        super(MagentoApiGetOrdersXML, self).__init__(settings)
        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            settings['customer'],
            'load_orders',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'magento'),
            datetime.utcnow()
        )

    def get_request_data(self):
        # data = {'method': 'order.list', 'args': [{'created_at': {'is_greater_than':'2013-02-01%'}}]}
        data = {'method': 'order.list', 'args': [{'status': 'Pending'}]}
        # data = {'method': 'order.list'}
        return data

    def parse_response(self, response):
        if(not response):
            return []
        ordersList = []

        for order in response:
            ordersObj = {}
            ordersObj['xml'] = xml_utils.XmlUtills(order, True).to_xml()
            ordersObj['name'] = order['increment_id']
            ordersList.append(ordersObj)
        return ordersList

class MagentoApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml

    def getOrderObj(self):
        ordersObj = {}

        if(self.xml == ""):
            return ordersObj
        order = xml_utils.XmlUtills(self.xml).to_obj()
        if(order is not None):
            ordersObj['partner_id'] = 'magento'
            ordersObj['order_id'] = order.get('increment_id', {}).get('value', False)
            ordersObj['carrier'] = order.get('shipping_method', {}).get('value', False)
            ordersObj['external_date_order'] = order.get('created_at', {}).get('value', False)
            ordersObj['shipCost'] = order.get('shipping_amount', {}).get('value', False)
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['name'] = order.get('shipping_address', {}).get('firstname', {}).get('value', "")
            ordersObj['address']['ship']['address1'] = order.get('shipping_address', {}).get('street', {}).get('value', "")
            ordersObj['address']['ship']['address2'] = order.get('shipping_address', {}).get('street', {}).get('value', "")
            ordersObj['address']['ship']['city'] = order.get('shipping_address', {}).get('city', {}).get('value', "")
            ordersObj['address']['ship']['state'] = order.get('shipping_address', {}).get('region', {}).get('value', "")
            ordersObj['address']['ship']['zip'] = order.get('shipping_address', {}).get('postcode', {}).get('value', "")
            ordersObj['address']['ship']['country'] = order.get('shipping_address', {}).get('country_id', {}).get('value', "")
            ordersObj['address']['ship']['phone'] = order.get('shipping_address', {}).get('telephone', {}).get('value', "")
            ordersObj['address']['order'] = {}
            ordersObj['address']['order']['name'] = order.get('billing_address', {}).get('firstname', {}).get('value', "")
            ordersObj['address']['order']['address1'] = order.get('billing_address', {}).get('street', {}).get('value', "")
            ordersObj['address']['order']['address2'] = order.get('billing_address', {}).get('street', {}).get('value', "")
            ordersObj['address']['order']['city'] = order.get('billing_address', {}).get('city', {}).get('value', "")
            ordersObj['address']['order']['state'] = order.get('billing_address', {}).get('region', {}).get('value', "")
            ordersObj['address']['order']['zip'] = order.get('billing_address', {}).get('postcode', {}).get('value', "")
            ordersObj['address']['order']['country'] = order.get('billing_address', {}).get('country_id', {}).get('value', "")
            ordersObj['address']['order']['phone'] = order.get('billing_address', {}).get('telephone', {}).get('value', "")
            ordersObj['lines'] = []
            if (isinstance(order.get('items', {}), list)):
                lines = order.get('items', {})
            else:
                lines = [order.get('items', {})]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('product_id', {}).get('value', False)
                lineItemObj['sku'] = lineItem.get('sku', {}).get('value', False)
                lineItemObj['merchantSKU'] = lineItemObj['sku']
                # sku = lineItem.get('sku', {}).get('value', False)
                # if(sku.find("/") == -1):
                #     lineItemObj['sku'] = sku
                #     lineItemObj['size'] = False
                # else:
                #     lineItemObj['sku'] = sku[:sku.find("/")]
                #     lineItemObj['size'] = sku[sku.find("/") + 1:]
                lineItemObj['qty'] = lineItem.get('qty_ordered', {}).get('value', False)
                lineItemObj['name'] = lineItem.get('name', {}).get('value', False)
                lineItemObj['cost'] = lineItem.get('price', {}).get('value', False)
                ordersObj['lines'].append(lineItemObj)
        return ordersObj


class MagentoApiConfirmOrders(MagentoApi):

    confirmLines = []
    name = "ConfirmOrders"

    def __init__(self, settings, lines):
        super(MagentoApiConfirmOrders, self).__init__(settings)
        self.confirmLines = lines

    def get_request_data(self):
        for line in self.confirmLines:
            data = {'track_no': line["tracking_number"], 'ext_cust_ord_id': line['external_customer_order_id']}
            break
        return data


class MagentoApiAcknowledgement(MagentoApi):
    confirmLines = []
    name = "Acknowledgement"

    def __init__(self, settings, lines):
        super(MagentoApiAcknowledgement, self).__init__(settings)
        self.confirmLines = lines

    def get_request_data(self):
        comment = 'The order was successfully downloaded'
        data = {'method': 'sales_order.addComment',
                'args': [str(self.confirmLines['order_id']), 'fulfilment', comment, True]}
        return data


class MagentoApiUpdateQTY(MagentoApi):

    confirmLines = []
    name = "UpdateQTY"

    def __init__(self, settings, lines):
        super(MagentoApiUpdateQTY, self).__init__(settings)
        self.confirmLines = lines

    def get_request_data(self):
        data = []
        for line in self.confirmLines:
            product = {}
            product['qty'] = line['qty']
            product['sku'] = line['sku']
            if (product['sku'] and product["sku"] != 'None'):
                data.append(product)
        return data

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
