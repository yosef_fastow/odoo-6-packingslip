# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from datetime import datetime
import logging
from openerp.addons.pf_utils.utils.re_utils import f_d
from customer_parsers.staples_input_csv_parser import CSVParser
from configparser import ConfigParser
from openerp.addons.pf_utils.utils.decorators import return_ascii
from utils import feedutils as feed_utils
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}

SETTINGS_FIELDS = (
    ('vendor_number',                           'Vendor number',                         ''),
    ('vendor_facility_code',                    'Vendor Facility code',                  ''),
    ('lead_time_min_days',                      'Lead time Minimum days',                '3'),
    ('lead_time_max_days',                      'Lead time Maximum days',                '7'),
    ('default_next_available_qty',              'Default nex available qty',             '1'),
    ('default_next_available_interval_unit',    'Default nex available interval unit',   'months'),
    ('default_next_available_interval_number',  'Default nex available interval number', '2'),
)


class StaplesApi(FtpClient):

    def __init__(self, settings):
        super(StaplesApi, self).__init__(settings)
        self.customer_setting = '%s/customers_settings/staples_settings.ini' % (self.current_dir)


class StaplesApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(StaplesApiClient, self).__init__(
            settings_variables, StaplesOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = StaplesApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = StaplesApiAcknowledgmentOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True

    def confirmShipment(self, lines):
        confirmApi = StaplesApiConfirmOrders(self.settings, lines)
        confirmApi.process('send')
        self.extend_log(confirmApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = StaplesApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class StaplesApiGetOrdersXML(StaplesApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(StaplesApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))
        self.parser = CSVParser(self)


class StaplesApiAcknowledgmentOrders(StaplesApi):
    name = "Acknowledgement"

    def __init__(self, settings, lines, state):
        super(StaplesApiAcknowledgmentOrders, self).__init__(settings)
        self.is_cancel_feed = state == 'cancel'
        ftp_setting = 'confirm_cancel' if self.is_cancel_feed else 'acknowledgement'
        self.use_ftp_settings(ftp_setting)
        self.processingLines = lines
        self.state = state

        self.config = ConfigParser()
        self.config.read(self.customer_setting)

        self.filename = f_d('po_cancelres_%Y%m%d_%H%M.csv') if self.is_cancel_feed else f_d('po_res_%Y%m%d_%H%M.csv')

    @return_ascii
    def upload_data(self):

        feed_items = self.config.items('AckRecord')

        data = [
            feed_utils.FeedUtills(feed_items, {}).create_csv_header()
        ]

        code = "ACCEPT"
        for line in self.processingLines:
            line['code'] = code
            if code == 'ACCEPT':
                line['cancel_code'] = None
            feed_line = feed_utils.FeedUtills(feed_items, line).create_csv_line(skip_invalid_line=False)
            data.append(feed_line)

        return "".join(data)


class StaplesApiUpdateQTY(StaplesApi):

    lines = []

    def __init__(self, settings, lines):
        super(StaplesApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory', self.get_root_dir(settings))
        self.lines = lines
        self.config = ConfigParser()
        self.config.read(self.customer_setting)
        self.feed_date = datetime.now()

        self.date_format = "%Y-%m-%d"

        self.filename = f_d("{vendor_number}_inventory_%Y%m%d_%H%M%S.csv".format(vendor_number=self.vendor_number), date_time=self.feed_date)

        try:
            self.default_qty = int(self.default_next_available_qty)
        except:
            self.default_qty = 1

        try:
            params = {
                self.default_next_available_interval_unit: int(self.default_next_available_interval_number)
            }
            self.default_eta_date = self.feed_date + relativedelta(**params)

        except:
            self.default_eta_date = self.feed_date + relativedelta(months=2)

        self.default_eta = self.default_eta_date.strftime(self.date_format)

        self.conditions = {
            'new': 'NEW',
            'used': 'USED',
            'refurbished': 'REFURBISHED',
        }

        self.available_status = {
            'available': 'Available',
            'out_of_stock': 'OUT OF STOCK',
            'guaranteed': 'Guaranteed',
            'discontinued': 'Discontinued',
        }

        self.revision_lines = {
            'bad': [],
            'good': []
        }

    @return_ascii
    def upload_data(self):
        data = []

        feed_items = self.config.items('SkuRecord')

        for line in self.lines:

            line['vendor_number'] = self.vendor_number
            line['vendor_facility_code'] = self.vendor_facility_code
            line['lead_time_min_days'] = self.lead_time_min_days
            line['lead_time_max_days'] = self.lead_time_max_days
            line['available_end_date'] = None

            line_status = 'available'
            line_condition = 'new'
            line = feed_utils.LineUtils(line).validate_eta(time_format=self.date_format, field='po_eta')

            if line['qty'] in (0, '0'):

                if line['dnr_flag'] in (1, '1'):
                    line_status = 'discontinued'
                    line['available_end_date'] = self.feed_date.strftime(self.date_format)

                else:
                    line_status = 'out_of_stock'
                    if not line['eta_date']:
                        line['eta_date'] = self.default_eta
                        line['ordered_qty'] = self.default_qty
                    else:
                        line['ordered_qty'] = int(line['ordered_qty'])

            if line_status in ('available', 'discontinued'):
                line['eta_date'] = ''
                line['ordered_qty'] = ''

            line['vendor_number'] = self.vendor_number
            line['vendor_facility_code'] = self.vendor_facility_code

            line['condition'] = self.conditions[line_condition]
            line['available_status'] = self.available_status[line_status]

            SkuRecord = feed_utils.FeedUtills(feed_items, line).create_csv_line(skip_invalid_line=True)
            if SkuRecord:
                data.append(SkuRecord)
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')

        return "".join(data)


class StaplesApiConfirmOrders(StaplesApi):

    def __init__(self, settings, lines):
        super(StaplesApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.lines = lines

        self.config = ConfigParser()
        self.config.read(self.customer_setting)

        self.filename = f_d('po_asn_%Y%m%d_%H%M.csv')

    def upload_data(self):

        feed_items = self.config.items('AsnRecord')

        data = [
            feed_utils.FeedUtills(feed_items, {}).create_csv_header()
        ]

        for line in self.lines:
            line['product_qty'] = int(line.get('product_qty', 0))
            data.append(
                feed_utils.FeedUtills(feed_items, line).create_csv_line(skip_invalid_line=False)
            )
        return "".join(data)


class StaplesOpenerp(ApiOpenerp):

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'cust_order_number': order.get('cust_order_number'),
            'additional_fields': self.wrap_additional_fields(order.get('additional_fields', {}))
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line.copy()
        line_obj.update({
            'notes': '',
            'additional_fields': self.wrap_additional_fields(line.get('additional_fields', {}))
        })

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc', 'customer_id_delmar')
        product = {}
        field_list = ['sku', 'upc', 'merchantSKU', 'vendorSku']
        for field_name in field_list:
            field = line.get(field_name, None)
            if field:
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], field, search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(line['sku'])
            line_obj['product_id'] = False

        return line_obj

    def get_additional_processing_so_information(
            self, cr, uid, settigs, sale_obj, order_line, context=None
    ):
        line = {
            'po_number': sale_obj.po_number,
            'vendorSku': order_line.vendorSku,
            'merchantLineNumber': order_line.merchantLineNumber,
        }

        return line
