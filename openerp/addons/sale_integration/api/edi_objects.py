# -*- coding: utf-8 -*-
from re import sub as re_sub
from re import findall as re_findall
from random import randrange
from datetime import datetime
from pf_utils.utils.re_utils import f_d
import logging
import unicodedata

_logger = logging.getLogger(__name__)


DEFAULT_REPETITION_SEPARATOR = '*'


class BaseEDIClient(object):
    def __init__(self, settings, edi_parser=None, separator = DEFAULT_REPETITION_SEPARATOR):
        super(BaseEDIClient, self).__init__()
        if settings is not None:
            self.copy_attributes_from_settings(settings)
        self.edi_parser = edi_parser
        self.separator = separator

    def copy_attributes_from_settings(self, settings):
        for attribute_name in settings:
            if attribute_name:
                try:
                    exec('self.{attr_name} = settings["{attr_name}"]'.format(attr_name=attribute_name))
                except (SyntaxError, KeyError):
                    pass

    @property
    def edi_parser(self):
        return self._edi_parser

    @edi_parser.setter
    def edi_parser(self, edi_parser):
        if edi_parser is None:
            _logger.warning(
                'Not installed edi_parser for {} and cannot define check_sender_is_correct function'.format(
                    self.customer
                )
            )
            self._edi_parser = None
        else:
            self._edi_parser = edi_parser

    @property
    def edi_x12_version(self):
        return self._edi_x12_version

    @property
    def edi_x12_version_isa(self):
        return self._edi_x12_version_isa

    @property
    def edi_x12_version_gs(self):
        return self._edi_x12_version_gs

    @edi_x12_version.setter
    def edi_x12_version(self, version):
        if version in ['4010', 4010]:
            self._edi_x12_version = '4010'
            self._edi_x12_version_isa = '00401'
            self._edi_x12_version_gs = '004010'
        elif version in ['4030', 4030]:
            self._edi_x12_version = '4030'
            self._edi_x12_version_isa = '00403'
            self._edi_x12_version_gs = '004030'
        elif version in ['4050', 4050]:
            self._edi_x12_version = '4050'
            self._edi_x12_version_isa = '00405'
            self._edi_x12_version_gs = '004050VICS'
        elif version in ['5010', 5010]:
            self._edi_x12_version = '5010'
            self._edi_x12_version_isa = '00501'
            self._edi_x12_version_gs = '005010'
        else:
            raise Exception('Unsupported EDI X12 version: {}'.format(version))

    @property
    def filename_format(self):
        result = None
        if (
            hasattr(self, '_filename_format') and
            self._filename_format
        ):
            result = self._filename_format
        else:
            result = "DELMAR_{edi_type}_{date}.edi"
        return result

    @filename_format.setter
    def filename_format(self, filename_format):
        availaible_variables = set(['{edi_type}', '{date}'])
        variables = set(re_findall(r"{[a-zA-Z_]+[0-9]*}", filename_format))
        not_availaible_variables = list(variables - availaible_variables)
        if not_availaible_variables:
            raise Exception(
                "You can't use variables: {} in filename_format".format(
                    ','.join(not_availaible_variables)
                )
            )
        self._filename_format = filename_format

    @property
    def filename(self):
        if (
            hasattr(self, '_filename') and
            self._filename
        ):
            pass
        else:
            self._filename = self.filename_format.format(
                edi_type=self.edi_type,
                date=f_d('%Y%m%d_%H%M%S_%f', datetime.utcnow()),
            )
        return self._filename

    @filename.setter
    def filename(self, value):
        raise AttributeError("You can't set property filename!")

    @property
    def edi_type(self):
        if not hasattr(self, '_edi_type'):
            raise AttributeError("Not installed attribute _edi_type!")
        if not self._edi_type:
            raise AttributeError("Attribute _edi_type is empty!")
        return self._edi_type

    @edi_type.setter
    def edi_type(self, value):
        self._edi_type = value

    @property
    def line_terminator(self):
        if (
            hasattr(self, '_line_terminator') and
            self._line_terminator
        ):
            result = self._line_terminator
        else:
            result = '\n'
        result = result.replace('\\n', '\n')
        return result

    @line_terminator.setter
    def line_terminator(self, value):
        self._line_terminator = value

    def insertToStr(self, line, data):
        line = re_sub('\[\d+\]', '', line)
        return line % data

    def wrap(self, segments, params, header=False):

        isa_control_number =  params.get('isa_data', {}).get('isa_control_number', False) or str(randrange(100000000, 999999999))
        date_now = datetime.utcnow()

        head = []

        receiver_id = self.receiver_id
        type_receiver_id = 'receiver_id_' + self.edi_type
        if hasattr(self, type_receiver_id):
            receiver_id = getattr(self, type_receiver_id)

        if hasattr(self, 'repetition_separator'):
            repetition_separator = self.repetition_separator
        else:
            repetition_separator = DEFAULT_REPETITION_SEPARATOR

        if hasattr(self, 'standard_identifier'):
            standard_identifier = self.standard_identifier
        else:
            standard_identifier = 'U'

        isa = self.separator.join([
            'ISA',
            '[1]00',
            '[2]          ',
            '[3]00',
            '[4]          ',
            '[5]%(sender_id_qualifier)s',
            '[6]%(sender_id)s',
            '[7]%(receiver_id_qualifier)s',
            '[8]%(receiver_id)s',
            '[9]%(date)s',
            '[10]%(time)s',
            '[11]%(standard_identifier)s',
            '[12]%(edi_x12_version_isa)s',
            '[13]%(isa_control_number)s',
            '[14]0',
            '[15]%(environment_mode)s',
            '[16]%(repetition_separator)s',
        ])

        isa_data = params.get('isa_data', False) or {
            'sender_id_qualifier': self.sender_id_qualifier,
            'sender_id': self.sender_id + ' ' * (15 - len(self.sender_id)),
            'receiver_id_qualifier': self.receiver_id_qualifier,
            'receiver_id': receiver_id + ' ' * (15 - len(receiver_id)),
            'edi_x12_version_isa': self.edi_x12_version_isa,
            'isa_control_number': isa_control_number,
            'date': date_now.strftime('%y%m%d'),
            'time': date_now.strftime('%H%M'),
            'environment_mode': self.environment_mode,  # Modes: p - prod, T - test
            'repetition_separator': repetition_separator,
            'standard_identifier': standard_identifier,
        }

        head.append(self.insertToStr(isa, isa_data))

        gs_control_number = str(randrange(1000, 99999999))

        gs = self.separator.join([
            'GS',
            '[1]%(group)s',
            '[2]%(sender_id)s',
            '[3]%(receiver_id)s',
            '[4]%(date)s',
            '[5]%(time)s',
            '[6]%(gs_control_number)s',
            '[7]X',
            '[8]%(edi_x12_version_gs)s',
        ])

        gs_data = params.get('gs_data', False) or {
            'group': params['group'],
            'sender_id': self.sender_id,
            'receiver_id': receiver_id,
            'date': date_now.strftime('%Y%m%d'),
            'time': date_now.strftime('%H%M'),
            'gs_control_number': gs_control_number,
            'edi_x12_version_gs': self.edi_x12_version_gs,
        }

        head.append(self.insertToStr(gs, gs_data))

        segments = head + segments

        ge = self.separator.join([
            'GE',
            '%(ge_code)s',
            '%(gs_control_number)s',
        ])

        segments.append(self.insertToStr(ge, {
            'ge_code': '1',
            'gs_control_number': gs_control_number
        }))

        iea = self.separator.join([
            'IEA',
            '%(iea_code)s',
            '%(isa_control_number)s',
        ])

        segments.append(self.insertToStr(iea, {
            'iea_code': '1',
            'isa_control_number': isa_control_number
        }))

        segments = [unicodedata.normalize('NFD', x) if isinstance(x, unicode) else str(x) for x in segments]
        result = self.line_terminator.join(segments) + self.line_terminator
        if header:
            result = header + '\n' + result
        return result

    def check_sender_is_correct(self, data):
        if self.edi_parser:
            parser = self.edi_parser(self)
            return parser.check_sender_is_correct(data)
        else:
            return True
