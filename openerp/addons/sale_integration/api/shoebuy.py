# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from datetime import datetime
import logging
from openerp.addons.pf_utils.utils.decorators import return_ascii
from openerp.addons.pf_utils.utils.re_utils import f_d
from customer_parsers.shoebuy_input_csv_parser import CSVParser
from utils import feedutils as feed_utils
from configparser import ConfigParser
import os

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class ShoebuyApi(FtpClient):

    def __init__(self, settings):
        super(ShoebuyApi, self).__init__(settings)
        self.delimiter = '|'
        self.quoting = '"'
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/shoebuy_settings.ini' % (os.path.dirname(__file__)))


class ShoebuyApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(ShoebuyApiClient, self).__init__(
            settings_variables, ShoebuyOpenerp, False, DEFAULT_VALUES
        )
        self.load_orders_api = ShoebuyApiGetOrdersCSV

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = ShoebuyApiConfirmOrders(self.settings, lines)
        confirmApi.process('send')
        self.extend_log(confirmApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = ShoebuyApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class ShoebuyApiGetOrdersCSV(ShoebuyApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(ShoebuyApiGetOrdersCSV, self).__init__(settings)
        self.delimiter = "|"
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))
        self.parser = CSVParser(self.customer, self.delimiter, self.quoting)


class ShoebuyApiUpdateQTY(ShoebuyApi):

    filename = ""
    lines = []

    def __init__(self, settings, lines):
        super(ShoebuyApiUpdateQTY, self).__init__(settings)
        self.delimiter = ","
        self.use_ftp_settings('inventory', self.get_root_dir(settings))
        self.lines = lines
        self.filename = "SHOEBUY_INV_7769.txt"
        self.filename_local = f_d("Inv_7769_%Y%m%d_%H%M%S_%f.csv")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    @return_ascii
    def upload_data(self):
        data = []
        lines = self.lines
        feed_date = datetime.utcnow()
        feed_date = feed_date.strftime('%Y%m%d')

        feed_items = self.config['InventorySetting'].items()

        for line in lines:
            SkuRecord = None
            if(line.get('customer_sku', False) and line.get('upc', False) and line.get('customer_price')):

                line['date'] = feed_date
                line['discontinued'] = 'N'
                line['discontinued_date'] = ''
                if (line['dnr_flag'] in (1, '1') and line['qty'] in (0, '0')):
                    line['discontinued'] = 'Y'
                    line['discontinued_date'] = feed_date

                line['closed'] = 'N'
                line['eta_date'] = ''
                if line['qty'] in (0, '0'):
                    try:
                        if line['eta'] and int(line['eta'][-4:]) >= 1900:
                            line['eta_date'] = datetime.strptime(line['eta'], "%m/%d/%Y").strftime("%Y%m%d")
                        else:
                            raise ValueError('Too old ETA')
                    except ValueError:
                        line['eta_date'] = ''
                    if not line['eta_date']:
                        line['closed'] = 'Y'

                SkuRecord = feed_utils.FeedUtills(
                        feed_items, line
                    ).create_csv_line(
                        delimiter=self.delimiter,
                        skip_invalid_line=False,
                        quote=False
                    )
            if SkuRecord:
                self.append_to_revision_lines(line, 'good')
                data.append(SkuRecord)
            else:
                self.append_to_revision_lines(line, 'bad')
        return "".join(data)


class ShoebuyApiConfirmOrders(ShoebuyApi):

    def __init__(self, settings, lines):
        super(ShoebuyApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.lines = lines
        self.filename = f_d("Confirm_7769_%Y%m%d_%H%M%S_%f.txt")

    def upload_data(self):
        data = []
        dt = datetime.now()
        feed_items = self.config['ShippingConfirmation'].items()

        for line in self.lines:
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = dt.strftime("%Y%m%d")
            line['int_product_qty'] = int(line['product_qty'])
            line['upc'] = line.get('additional_fields', {}).get('upc', False)

            feed_line = feed_utils.FeedUtills(
                feed_items, line
            ).create_csv_line(
                delimiter=self.delimiter,
                skip_invalid_line=False,
                quote=False
            )
            data.append(feed_line)

        return "".join(data)


class ShoebuyOpenerp(ApiOpenerp):

    def __init__(self):

        super(ShoebuyOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': 0.0,
            'customerCost': line['customerCost'],
            'customer_sku': line['customer_sku'],
            'vendorSku': line['sku'],
            'qty': line['qty'],
            'size': line['size'],
            'id': line['id'],
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ],
        }

        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        product = False

        field_list = ['sku', 'upc']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                        'upc',
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
