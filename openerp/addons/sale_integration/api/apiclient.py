# -*- coding: utf-8 -*-
from baseapiclient import BaseApiClient
from urllib2 import Request, urlopen, HTTPError
import time
import os
import ftplib
from ftplib import FTP
import cStringIO
import logging
import csv
from openerp.addons.pf_utils.utils.backup_file import backup_file_open
from datetime import datetime
import traceback
import re


_logger = logging.getLogger(__name__)


class ApiClient(BaseApiClient):

    _use_ftp_send = False
    _ftp_host = ''
    _ftp_user = ''
    _http_timeout = 600
    _ftp_timeout = 10
    _ftp_file_mask = None
    _method = ''
    _ftp_passwd = ''
    _path_to_local_dir = ''
    _path_to_backup_local_dir = ''
    _path_to_ftp_dir = ''
    _remove_flag = False

    def __init__(self, settings=None):
        super(ApiClient, self).__init__()
        self._log = []

    def send(self):
        if(not self._use_ftp_send):
            if (not getattr(self, 'response', False)):
                """
                    Send request to server
                """
                data = self.get_request_data()
                data = self.prepare_data(data)

                headers = self.get_request_headers()
                headers = self.prepare_headers(headers, data)

                print data
                response = ""
                try:
                    conn = Request(url=self.api_url, data=data, headers=headers)
                    method = self.get_method()
                    if method == 'PUT':
                        conn.get_method = lambda: 'PUT'

                    try:
                        f = urlopen(conn, timeout=self._http_timeout)
                        response = f.read()
                    except HTTPError, e:
                        if e.code == 400:
                            if bool(getattr(e, 'read', False)):
                                response = e.read()
                        if not response:
                            raise e

                except Exception, e:

                    msg = ""
                    if hasattr(e, 'reason'):
                        msg += 'Could not reach the server, reason: %s' % e.reason
                    elif hasattr(e, 'code'):
                        msg += 'Could not fulfill the request, code: %d' % e.code
                    else:
                        msg += e.message
                    if bool(getattr(e, 'read', False)):
                        msg += e.read()
                    print msg

                    msg += '  ' + traceback.format_exc()

                    self._log.append({
                        'title': "Send %s Error" % self.name,
                        'msg': msg,
                        'type': 'send',
                        'create_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    })

                    response = False

                if (not self.check_response(response)):
                    response = False
            else:
                response = self.response

            if hasattr(self, 'filename'):
                with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename + ".resp"), 'w') as bf:
                    bf.write(str(response))

            return self.parse_response(response)

        elif(self._use_ftp_send):
            """
                SendFTP request to server (method: get/put)
            """

            now = time.strftime('%Y%m%d_%H%M', time.gmtime())
            msg = ""
            if(self._method == "get"):
                response = ""
                try:
                    if(self.saveFlag):
                        ftp = FTP(self._ftp_host, self._ftp_user, self._ftp_passwd, timeout=self._ftp_timeout)
                        ftp.set_debuglevel(2)
                        ftp.cwd(self._path_to_ftp_dir)
                        filenames = ftp.nlst()
                        if(filenames):
                            count_files = 0
                            file_mask = re.compile(self._ftp_file_mask) if self._ftp_file_mask else None
                            for filename in filenames:
                                if not filename:
                                    continue
                                if filename[0] == '.':
                                    continue
                                if file_mask and not file_mask.match(file_mask):
                                    continue
                                IO = cStringIO.StringIO()
                                try:
                                    ftp.retrbinary('RETR ' + filename, IO.write)
                                    count_files += 1
                                except ftplib.error_perm:
                                    continue
                                write_data = IO.getvalue()
                                if write_data:
                                    with backup_file_open(
                                        os.path.join(
                                            self._path_to_local_dir, filename + '_' + now
                                        ), 'wb'
                                    ) as f1, backup_file_open(
                                        os.path.join(
                                            self._path_to_backup_local_dir, filename + '_' + now
                                        ), 'wb'
                                    ) as f2:
                                        f1.write(str(write_data))
                                        f2.write(str(write_data))
                                    msg += 'filename: ' + self._path_to_ftp_dir + str(filename) + '\ndata: \n' + str(write_data) + '\n'

                                    if self._remove_flag:
                                        ftp.delete(filename)
                                else:
                                    _logger.error('error method send get (%s): %s ' % (self.customer, 'data is empty'))
                                    msg += 'error: ' + 'write data is empty'
                            try:
                                ftp.quit()
                            except EOFError:
                                ftp.close()
                            self._log.append({
                                'title': "GetFilesFromFTP [count files: %s] method: %s (%s)" % (str(count_files), self._method, self.customer),
                                'msg': 'get files from ftp: \n' + msg,
                                'type': 'send',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                        else:
                            print "GetFilesFromFTP method: %s (%s) files on ftp not found" % (self._method, self.customer)

                    else:
                        data = []
                        filenames = os.listdir(self._path_to_local_dir)
                        if(filenames):
                            for filename in filenames:
                                fh = open(os.path.join(self._path_to_local_dir, filename), 'r')
                                response = self.set_decrypt_data(str(fh.read()))
                                fh.close()
                                self.check_response(response)
                                if response:
                                    data.append(response)
                                msg += 'filename: ' + str(filename) + '\ndata: \n ' + str(response) + '\n'
                            ret = self.parse_response(data)
                            self._log.append({
                                'title': "GetFilesFromLocalFolder method: %s (%s)" % (self._method, self.customer),
                                'msg': 'files in local folder: \n' + msg,
                                'type': 'send',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            if(ret):
                                for filename in filenames:
                                    os.remove(os.path.join(self._path_to_local_dir, filename))
                                return ret
                        else:
                            print "GetFilesFromLocalFolder method: %s (%s) files in local folder is not found" % (
                                self._method,
                                self.customer
                            )

                except Exception, e:
                    print 'error method send get: %s  (%s)' % (e, self.customer)
                    msg = traceback.format_exc()
                    self._log.append({
                        'title': 'error method send get (%s)' % (self.customer),
                        'msg': msg,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

            elif(self._method == "put"):
                response = ""
                try:
                    upload_data = self.upload_data()
                    response, encrypt_flag = self.set_encrypt_data(upload_data)
                    IO = cStringIO.StringIO(str(response))
                    if not hasattr(self, 'filename_local'):
                        with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename), 'w') as bf:
                            bf.write(str(IO.getvalue()))
                    else:
                        with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename_local), 'w') as bf:
                            bf.write(str(IO.getvalue()))

                    if encrypt_flag:
                        if not hasattr(self, 'filename_local'):
                            with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename + '.txt'), 'w') as bf:
                                bf.write(str(upload_data))
                        else:
                            with backup_file_open(os.path.join(self._path_to_backup_local_dir, self.filename_local + '.txt'), 'w') as bf:
                                bf.write(str(upload_data))

                    ftp = FTP(self._ftp_host)
                    ftp.login(self._ftp_user, self._ftp_passwd)
                    ftp.cwd(self._path_to_ftp_dir)
                    ftp.storbinary('STOR ' + self.filename, IO)

                    self._log.append({
                        'title': "%s method: %s (%s)" % (self.name, self._method, self.customer),
                        'msg': 'filename: \n %s\nresponse: \n%s\nencrypt responce: %s\n' % (str(self.filename), str(upload_data), str(response)),
                        'type': 'send',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

                    try:
                        ftp.quit()
                    except EOFError:
                        ftp.close()

                except Exception, e:
                    print 'error method send put: %s, (%s)' % (e, self.customer)
                    msg = traceback.format_exc()
                    self._log.append({
                        'title': 'error method send put',
                        'msg': msg,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

    def check_response(self, response):
        """
            Check response on error
        """
        pass

    def prepare_headers(self, headers, data=None):
        """
            Prepare headers before send
        """
        return headers

    def get_request_headers(self):
        return {}

    def set_ftp_settings(self, host, user, password, remove_flag=False, file_mask=None):
        self._use_ftp_send = True
        self._ftp_host = host
        self._ftp_user = user
        self._ftp_passwd = password
        self._ftp_file_mask = file_mask
        self._remove_flag = remove_flag

    def set_path_list(self, path_list):
        """
            path_list={
                'path_to_ftp_dir': '',
                'path_to_local_dir': '',
                'path_to_backup_local_dir': ''
            }
        """
        for path in path_list:
            if not path[-1] == '/':
                path += '/'
        self._path_to_ftp_dir = path_list.get('path_to_ftp_dir', '/')
        self._path_to_local_dir = path_list.get('path_to_local_dir', '/')
        self._path_to_backup_local_dir = path_list.get('path_to_backup_local_dir', '/tmp/')

    def set_ftp_method(self, method):
        """ method: (put/get)"""
        self._method = method

    def upload_data(self):
        """Return upload data on server"""
        pass

    def get_attach_data(self, lines):

        cols = ['sku', 'qty']
        output = cStringIO.StringIO()
        writer = csv.writer(output, delimiter=",", quoting=csv.QUOTE_MINIMAL)

        for line in lines:
            feed_line = []
            for col in cols:
                el = line.get(col, False)
                el = type(el) is unicode and el.encode('utf-8') or str(el).encode('utf-8')
                feed_line.append(el)
            writer.writerow(feed_line)

        data = output.getvalue()

        return data

    def get_method(self):
        return ''

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
