# -*- coding: utf-8 -*-
from apiclient import ApiClient
from datetime import datetime
import netsvc
import base64
from os.path import exists as os_path_exists
from ftpapiclient import BaseFTPApiGetOrders
import logging
_logger = logging.getLogger('openerp')


INIT_SETTINGS_FIELDS = tuple()
INIT_DEFAULT_VALUES = {}


class AbsApiClient(object):

    _filename_inventory = ""
    _readable_inventory_filename = ""
    is_invoice_required = True

    def __init__(self, settings_variables, oapi=None, SETTINGS_FIELDS=None, DEFAULT_VALUES=None, invoice_message_manager=None):
        self.settings = settings_variables
        self.apierp = oapi
        self.setting_fields = SETTINGS_FIELDS or tuple(INIT_SETTINGS_FIELDS)
        self.default_values = DEFAULT_VALUES or dict(INIT_DEFAULT_VALUES)
        self._log = []

        self._invoice_message_manager = invoice_message_manager or InvoiceMessageManager

    # Use load file to local folder
    use_local_folder = False

    def apierp_is_available(self, function_name):
        available = False
        if (
            self.apierp is not False and
            function_name in self.apierp.available_methods
        ):
            available = True
        return available

    @property
    def apierp(self):
        return self._apierp

    @apierp.setter
    def apierp(self, oapi):
        if oapi:
            self._apierp = oapi()
            self._apierp.apierp_class = oapi
        else:
            self._apierp = False

    @property
    def settings(self):
        return self._settings

    @settings.setter
    def settings(self, settings_variables):
        if settings_variables is None:
            settings_variables = {}
        self._settings = settings_variables

    def GetOrdersInLocalFolder(self, order_type='order'):
        using_ftp_for_loading_orders = None
        if (
            'multi_ftp_settings' in self.settings and
            isinstance(self.settings['multi_ftp_settings'], (list, set))
        ):
            using_ftp_for_loading_orders = False
            for setting in self.settings:
                if setting.action == 'load_orders':
                    using_ftp_for_loading_orders = True
                    break
        if using_ftp_for_loading_orders is not None:
            if using_ftp_for_loading_orders:
                api = BaseFTPApiGetOrders(self.settings)
                orders = api.process('load')
                self.append_to_log(api.full_log)
                return orders
            else:
                raise Exception("Couldn't finding setting for loading orders from ftp")
        else:
            raise Exception('GetOrdersInLocalFolder does not work now for ApiClient')

    def getAttachData(self, lines):
        csvApi = ApiClient()
        data = csvApi.get_attach_data(lines)

        return data

    def returnOrders(self, lines):
        return False

    def processingOrderLines(self, lines, state):
        return True

    def changeAcknowledgement(self, order, transaction_number):
        return False

    def confirmShipment(self, lines):
        return True

    def updateQTY(self, lines, mode=None):
        return []

    def updateQTYPartial(self, lines, mode=None):
        return self.updateQTY(lines, mode=mode)

    def create_local_dir(self, init_local_path=None, path_customer=None, path_action=None, when=None):
        api = ApiClient()
        local_dir = api.create_local_dir(init_local_path, path_customer, path_action, when)
        return local_dir

    def validate_mail_messages(self, base_messages):
        resended_messages = []
        if(not isinstance(base_messages, list)):
            base_messages = [base_messages]
        for base_message in base_messages:
            email_from = base_message.get('email_from', 'erp.delmar@progforce.com')
            email_to_list = base_message.get('email_to_list', [])
            try:
                email_to_list = [mail.strip() for mail in (email_to_list or '').split(',') if mail and mail.strip()]
            except KeyError:
                email_to_list = ['delmar@isddesign.com']
            subject = base_message.get('subject', 'ERP Delmar Message')
            body = base_message.get('body', '')
            attachments = base_message.get('attachments', {})
            mail_message = {
                'email_from': email_from,
                'emailto_list': email_to_list,
                'subject': subject,
                'body': body,
                'attachments': attachments,
            }
            resended_messages.append(mail_message)
        return resended_messages

    def generate_invoice_mail_message(self, _settings):

        if(not hasattr(self, '_mail_messages')):
            self._mail_messages = []

        manager = self._invoice_message_manager(_settings)
        mail_message = manager.generate_email()
        if mail_message:
            self._mail_messages.append(mail_message)

    def getOrdersObj(self, xml):
        ordersApi = YamlOrder(xml)
        order = ordersApi.getOrderObj()
        # # DELMAR-186
        # if (order['address']):
        #     encode_address_fields = {}
        #     for key, value in order['address'].iteritems():
        #         if (isinstance(value, (dict))):
        #             key_fields = {}
        #             for k, val in value.iteritems():
        #                 if (isinstance(val, (dict))):
        #                     inner_key_fields = {}
        #                     for i_k, i_val in val.iteritems():
        #                         if (i_val == None or i_val == False or i_val == True or isinstance(i_val,
        #                                                                                            (long, int)) == True):
        #                             inner_key_fields.update({
        #                                 i_k: i_val
        #                             })
        #                         else:
        #                             inner_key_fields.update({
        #                                 i_k: i_val.encode('utf-8')
        #                             })
        #                     key_fields.update({k: inner_key_fields})
        #                 else:
        #                     if (val == None or val == False or val == True or isinstance(val, (long, int)) == True):
        #                         key_fields.update({
        #                             k: val
        #                         })
        #                     else:
        #                         key_fields.update({
        #                             k: val.encode('utf-8')
        #                         })
        #             encode_address_fields.update({key: key_fields})
        #         else:
        #             if (value == None or value == False or value == True or isinstance(value, (long, int)) == True):
        #                 encode_address_fields.update({
        #                     key: value
        #                 })
        #             else:
        #                 encode_address_fields.update({
        #                     key: value.encode('utf-8')
        #                 })
        #     order['address'] = encode_address_fields
        #     # END DELMAR-186
        self._log.extend(ordersApi.getLog())
        return order

    def check_and_set_filename_inventory(self, obj):
        if not hasattr(self, '_filename_inventory'):
            self._filename_inventory = ""
        if not os_path_exists(obj.full_filename):
            msg = 'Inventory file for: {0} not backup {1}!'.format(obj.customer, obj.full_filename)
            obj.log({
                'title': msg,
                'type': 'warning',
                'message': msg,
            })
            self._filename_inventory = ""
        else:
            self._filename_inventory = obj.full_filename
            # DELMAR-7
            # add readable inventory file
            self._readable_inventory_filename = obj.full_readable_filename
            # END DELMAR-7
        return True

    @property
    def setting_fields(self):
        result = None
        if (
            hasattr(self, '_setting_fields') and
            self._setting_fields
        ):
            result = self._setting_fields
        else:
            result = False
        return result

    @setting_fields.setter
    def setting_fields(self, value):
        self._setting_fields = value

    @property
    def default_values(self):
        result = None
        if (
            hasattr(self, '_default_values') and
            self._default_values
        ):
            result = self._default_values
        else:
            result = False
        return result

    @default_values.setter
    def default_values(self, value):
        self._default_values = value

    @property
    def load_orders_api(self):
        if (
            hasattr(self, '_load_orders_api') and
            self._load_orders_api
        ):
            return self._load_orders_api
        else:
            return None

    @load_orders_api.setter
    def load_orders_api(self, load_orders_api):
        self._load_orders_api = load_orders_api(self.settings)

    def reloadOrders(self):
        if self.load_orders_api is not None:
            self.load_orders_api.copy_attributes_from_settings(self.settings)
            self.load_orders_api.reload_orders()
            self.extend_log(self.load_orders_api)
            return True
        else:
            return False

    def extend_log(self, obj):
        self._log.extend(obj.full_log)
        return True

    @property
    def log(self):
        log = self._log[:]
        self._log = []
        return log

    @log.setter
    def log(self, log):
        raise Exception("You can't set property log, please use extend_log function instead")

    def getLog(self):
        tmp = self._log[:]
        self._log = []
        return tmp


class YamlOrder(object):

    def __init__(self, serialized_order=None):
        super(YamlOrder, self).__init__()
        self.serialized_order = serialized_order
        self._log = []

    def getOrderObj(self):
        import traceback
        from datetime import datetime
        from openerp.addons.pf_utils.utils.yamlutils import YamlObject
        order_obj = {}

        if(self.serialized_order == ""):
            return order_obj

        yaml_obj = YamlObject()
        try:
            order_obj = yaml_obj.deserialize(_data=self.serialized_order)
        except Exception:
            _title_msg = 'error for order: {0}'.format(order_obj.get('order_no', False))
            _msg = traceback.format_exc()
            self._log.append({
                'title': "Deserialize order error: {0}".format(self.customer),
                'msg': '\n'.join([_title_msg, _msg]),
                'type': 'deserialize error',
                'create_date': "{:'%Y-%m-%d %H:%M:%S}".format(datetime.now())
            })
            order_obj = {}
        return order_obj

    def getLog(self):
        tmp = self._log[:]
        self._log = []
        return tmp


class InvoiceMessageManager(object):

    def __init__(self, _settings=None):

        self.__file = None
        self.__file_name = None
        self.__delivery_ids = None

        if _settings is None:
            _settings = {}

        self.cr = _settings.get('cr', False)
        self.uid = _settings.get('uid', False)
        self.delivery_ids = _settings.get('delivery_ids', False)
        self.email_to_list = _settings.get('emails_to', False)
        self.customer_name = _settings.get('customer_name', False)
        self.report_service = _settings.get('report_service', False)
        self.subject = _settings.get('subject', False)
        self.shp_date = datetime.utcnow()
        self.file_name_template = _settings.get('file_name', False)
        self.file_ext = _settings.get('file_ext', False)

    @property
    def file(self):
        return self.__file

    @property
    def file_name(self):
        return self.__file_name

    @property
    def delivery_ids(self):
        return self.__delivery_ids

    @delivery_ids.setter
    def delivery_ids(self, ids):

        delivery_ids = None
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]

            self.cr.execute("""
                SELECT array_agg(distinct sp.id)
                FROM stock_picking sp
                    LEFT JOIN stock_picking_params pp on pp.picking_id = sp.id and pp.name = 'skip_customer_invoice'
                WHERE sp.id in %(ids)s
                    and coalesce(pp.value, '0') != '1'
            """, {
                'ids': tuple(ids),
            })
            res = self.cr.fetchone()
            delivery_ids = res and res[0] or None

        self.__delivery_ids = delivery_ids

    def _get_subject(self):
        if not self.subject:
            invoice_no = ''
            if len(self.delivery_ids) == 1:
                self.cr.execute("""
                    SELECT CASE WHEN state = 'returned' then ret_invoice_no else invoice_no end
                    FROM stock_picking
                    where id = %s
                """, [self.delivery_ids[0]]
                )
                res = self.cr.fetchone()
                invoice_no = res and res[0] or ''

            self.subject = "{customer} Invoice Report {invoice_no} {date}".format(
                customer=self.customer_name,
                date=self.shp_date.strftime("%Y-%m-%d %H:%M:%S"),
                invoice_no=invoice_no
            )

        return self.subject

    def _get_emailto_list(self):
        try:
            email_to_list = [mail.strip() for mail in (self.email_to_list or '').split(',') if mail and mail.strip()]
        except KeyError:
            email_to_list = ['delmar@isddesign.com']

        return email_to_list

    def _get_emailfrom_list(self):
        return 'erp.delmar@progforce.com'

    def _get_body(self):
        return ''

    def _get_file_name(self):

        if not self.file_name_template:
            self.file_name_template = '{:Invoice%Y-%m-%d %H:%M:%S}'

        if not self.file_ext:
            self.file_ext = 'pdf'

        file_name = (self.file_name_template + '.' + self.file_ext).format(self.shp_date)

        return file_name

    def _get_file(self):
        service = netsvc.LocalService(self.report_service)
        (pdf_file, format) = service.create(self.cr, self.uid, self.delivery_ids, data={})

        return pdf_file

    def _get_attachments(self):

        _attachments = {}

        self.__file_name = self._get_file_name()
        self.__file = base64.b64encode(self._get_file())

        _attachments[self.__file_name] = self.__file

        return _attachments

    def generate_email(self):
        if not self.delivery_ids:
            return False

        mail_message = {
            'email_from': self._get_emailfrom_list(),
            'emailto_list': self._get_emailto_list(),
            'subject': self._get_subject(),
            'body': self._get_body(),
            'attachments': self._get_attachments(),
        }

        return mail_message
