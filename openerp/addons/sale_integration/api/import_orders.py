import logging
import traceback
import time
from cStringIO import StringIO as IO
from csv import DictReader

from apiopenerp import ApiOpenerp
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_ids
from openerp.addons.pf_utils.utils.import_orders_normalize import import_orders_normalize
from customer_parsers.import_orders_input_csv_parser import CSVPparser, CSVLineError
from openerp.addons.pf_utils.utils.import_orders_normalize import get_starting_chunk
from openerp.addons.pf_utils.utils.import_orders_normalize import is_binary_string
from openerp.addons.pf_utils.utils.xls_to_csv import xls_data_to_csv


_logger = logging.getLogger(__name__)


class ImportOrdersApi(FtpClient):

    path_to_local_dir = ''
    _path_to_backup_local_dir = ''
    import_settings = None
    customer = None

    default_template = """---
    !!map {
      ? !!str "main"
      : !!map {
        ? !!str "ship_code"
        : !!str "shipping code",
        ? !!str "line_cost"
        : !!str "cost",
        ? !!str "line_description"
        : !!str "Description",
        ? !!str "line_delmar_id"
        : !!str "DelmarID",
        ? !!str "order_id"
        : !!str "po#",
        ? !!str "ship_address1"
        : !!str "shipto address",
        ? !!str "ship_address2"
        : !!str "shipto address2",
        ? !!str "ship_city"
        : !!str "ship city",
        ? !!str "ship_country"
        : !!str "ship country",
        ? !!str "ship_name"
        : !!str "shipto name",
        ? !!str "ship_phone"
        : !!str "ship phone",
        ? !!str "ship_state"
        : !!str "ship state",
        ? !!str "ship_zip"
        : !!str "ship zip",
        ? !!str "line_size"
        : !!str "size",
        ? !!str "line_qty"
        : !!str "qty",
        ? !!str "line_tax"
        : !!str "tax",
        ? !!str "line_retail"
        : !!str "retail",
        ? !!str "line_customer_sku"
        : !!str "CustomerID",
        ? !!str "line_ship_cost"
        : !!str "shippingcost",
        ? !!str "order_comment"
        : !!str "order_comment",
        ? !!str "combine"
        : !!str "combine",
        ? !!str "gift_message"
        : !!str "gift message",
      },
    }
    """

    def __init__(self, settings):
        super(ImportOrdersApi, self).__init__(settings)
        self.all_import_settings = settings['import_settings']

    def _get_import_files_from_ftp(self):
        self.not_to_upload_the_same_files = True
        ftp_ids = self.get_ftp_settings_ids()
        for _id in ftp_ids:
            self.use_ftp_settings('load_orders', 'import_orders', by_id=_id)
            self.process('load')
        return True

    def get_ftp_settings_ids(self):
        ids = []
        for setting in self.multi_ftp_settings:
            if(setting.action == 'load_orders'):
                ids.append(setting.id)
        return ids

    def read_new(self, check_ftp):
        result = False
        if(check_ftp):
            # Check new files on ftp
            result = self._get_import_files_from_ftp()
        else:
            data = []
            input_data = self.input_data
            for record in input_data:
                filename = record['filename']
                with open(filename, 'r') as _file:
                    _data = _file.read()
                if(_data):
                    data_obj = {}
                    data_obj['filename'] = filename
                    data_obj['data'] = _data
                    data_obj['dialect'] = detect_dialect(_data)
                    data.append(data_obj)
            result = self.parse_data(data)

        return result

    def set_import_settings(self):
        yamlObj = YamlObject()
        curr_import_settings = None
        for import_settings in self.all_import_settings:
            if import_settings.partner_id.external_customer_id == self.customer:
                curr_import_settings = import_settings
                break
        if curr_import_settings:
            yaml_setting = curr_import_settings.yaml_setting
        else:
            yaml_setting = self.default_template
            _logger.info('Using default import settings')
        self.import_settings = yamlObj.deserialize(yaml_setting)

    def process_file_data(self, data):
        result = data
        chunk = get_starting_chunk(data=data)
        if(is_binary_string(chunk)):
            try:
                csv_data = xls_data_to_csv(data, raise_exceptions=True)
            except Exception:
                msg = traceback.format_exc()
                self._log.append({
                    'title': "Warning GetImportFilesFromFTP Converting XLS/XLSX files",
                    'msg': "Couldn't convert to csv\n{0}".format(msg),
                    'type': 'get',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                csv_data = False
            if(csv_data):
                data = csv_data
        result_normalize = import_orders_normalize(data=data)
        if(result_normalize['normalize_ok'] and result_normalize['csv_data']):
            result = result_normalize['csv_data']
        return result


class ImportOrdersApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(ImportOrdersApiClient, self).__init__(
            settings_variables, ImportOrdersOpenerp, False, False)
        # NEED FIX: FAST DISABLE RELOAD ORDERS
        # self.load_orders_api = ImportOrdersApiGetOrdersXML

    def loadOrders(self, resent_data=None, check_ftp=False, cursor=False, pool=False):
        api = ImportOrdersApiGetOrdersXML(self.settings, resent_data, cursor=cursor, pool=pool)
        result = api.process('read_new', check_ftp=check_ftp)
        self.extend_log(api)
        return result

    def getOrdersObj(self, xml):
        ordersApi = ImportOrdersApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        return True


class ParseFileError(Exception):
    """Raised except global errors"""


class ImportOrdersApiGetOrdersXML(ImportOrdersApi):

    def __init__(self, settings, input_data, cursor, pool):
        self.settings = settings
        self.input_data = input_data
        self.customer = None
        self.is_import_orders = True
        self.pool = pool
        self.cursor = cursor
        super(ImportOrdersApiGetOrdersXML, self).__init__(self.settings)

    def _get_main_header_data(self, _header_csv, _dialect):
        result = {
            'customercode': '',
            'sotype': '',
            'fromaddressexpected': ''
        }
        _header_csv = strip_lower_del_spaces_from_header(_header_csv, _dialect)
        dr = DictReader(IO(_header_csv), dialect=_dialect)
        for row in dr:
            result['customercode'] = row['customercode']
            result['sotype'] = str(row['sotype']).strip().lower().replace(' ', '').replace('_', '')
            result['fromaddressexpected'] = row['fromaddressexpected']
            break
        return result

    def _get_header_and_data(self, csv_file_data):
        all_data = csv_file_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        header_csv = "\r\n".join(all_data[:2])
        main_csv = "\r\n".join(all_data[2:])
        return (header_csv, main_csv)

    def get_customer_by_customer_code(self, customer_code):
        customer_code = customer_code and customer_code.strip().lower()
        if not customer_code:
            return False
        search_order = ['ref', 'name', 'external_customer_id']
        customer_ids = []
        base_sql = """SELECT external_customer_id FROM res_partner WHERE lower(trim({search_field})) = %s"""
        sql_params = (customer_code, )
        for search_field in search_order:
            sql = base_sql.format(search_field=search_field, )
            self.cursor.execute(sql, sql_params)
            customer_ids = self.cursor.fetchone()
            if customer_ids:
                break
        return customer_ids and customer_ids[0] or False

    def parse_file(self, data_obj):
        result = {
            'errors': {
                'critical': [],
                'bad_format': [],
                'bad_lines': {},
            },
            'orders_list': []
        }
        try:
            csv_data = data_obj['data']
            csv_dialect = data_obj['dialect']
            if(not csv_dialect):
                raise ParseFileError('DialectError: Unknown Dialect')
            if(not csv_data):
                raise ParseFileError('Not find data')
            (header_csv, main_csv) = self._get_header_and_data(csv_data)
            if(not header_csv):
                raise ParseFileError('Bad Header in file')
            if(not main_csv):
                raise ParseFileError('Bad main data in file')
            main_header_data = self._get_main_header_data(header_csv, csv_dialect)
            self.customer = self.get_customer_by_customer_code(main_header_data['customercode'])
            if (
                not self.customer or
                self.customer in ['None', 'False', None, False, ''] or
                not str(self.customer).strip()
            ):
                raise CSVLineError(
                    2,
                    """Customer not find in customer list
                    or couldn't find import settings!
                    Please First Check External Customer ID""")
            self.set_import_settings()
            if(not self.import_settings):
                raise ParseFileError('Not find import settings!')
            main_header_data.update({'customer': self.customer})
            ids = self.import_settings['main']
            ids = strip_lower_del_spaces_from_ids(ids)
            parser = CSVPparser(csv_dialect, ids, main_header_data)
            main_csv = strip_lower_del_spaces_from_header(main_csv, csv_dialect)
            parser_result = parser.parse_file(main_csv)
            if(len(parser_result['errors']['bad_format']) > 0):
                result['errors']['bad_format'].extend(parser_result['errors']['bad_format'])
            if(len(parser_result['errors']['bad_lines']) > 0):
                result['errors']['bad_lines'].update(parser_result['errors']['bad_lines'])
            if(len(parser_result['orders_list']) > 0):
                result['orders_list'] = parser_result['orders_list']
            result['load_for_partner'] = self.customer
            result['load_for_partner_ref'] = main_header_data['customercode']
        except ParseFileError as non_crit_ex:
            result['errors']['bad_format'].append(non_crit_ex.message)
        except CSVLineError as csv_line_err:
            if(not result['errors']['bad_lines'].get(csv_line_err.line_count, False)):
                result['errors']['bad_lines'][csv_line_err.line_count] = []
            result['errors']['bad_lines'][csv_line_err.line_count].append(csv_line_err.message)
        except Exception as crit_ex:
            _msg = 'Exception: {0}\n{1}'.format(crit_ex.message, traceback.format_exc())
            result['errors']['critical'].append(_msg)
        return result

    def parse_data(self, data):
        result = {}
        for elem in data:
            result[elem['filename']] = self.parse_file(elem)
        return result


class ImportOrdersApiGetOrderObj(object):

    def __init__(self, serialized_order):
        self.serialized_order = serialized_order

    def getOrderObj(self):
        yaml_obj = YamlObject()
        result = yaml_obj.deserialize(_data=self.serialized_order)

        gift_message = ''
        for elem in result['lines']:
            if 'gift_message' in elem:
                if elem['gift_message'] != '':
                    gift_message = elem['gift_message']
                else:
                    gift_message = ''
                    break

        if (gift_message != '') and (gift_message != False):
            result['poHdrData'] = {}
            result['poHdrData']['giftIndicator'] = "y"
            result['poHdrData']['giftMessage'] = gift_message

        return result


class ImportOrdersOpenerp(ApiOpenerp):

    def __init__(self):
        super(ImportOrdersOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = {
            'notes': '',
            'name': line['name'],
            'customerCost': line['retail_cost'],
            'cost': line['cost'],
            'product_uom_qty': False,
            'product_uom': 1,
            'qty': line['qty'],
            'price_unit': '',
            'external_customer_line_id': line['id'],
            'state': 'draft',
            'type': 'make_to_stock',
            'vendorSku': line['customer_sku'],
            'merchantSKU': line['customer_sku'],
            'sku': line['sku'],
            'additional_fields':
            [
                (0, 0, {
                    'name': 'retail_cost',
                    'label': 'Retail Cost',
                    'value': line.get('retail_cost', False),
                }),
            ],
            'shipCost': line['ship_cost'],
            'gift_message': line['gift_message'] if 'gift_message' in line else '',
            'poLineData': {'giftMessage': line['gift_message'] if 'gift_message' in line else ''},
        }

        if (line.get('size', False) and line.get('sku', False)):
            try:
                float(line['size'])
            except Exception:
                line['sku'] += '/' + line['size']
                line['size'] = False

        product = False
        size = False
        field_list = ['delmar_id', 'sku', 'customer_sku']
        search_field = ('upc', 'os_product_sku', 'customer_sku', 'customer_id_delmar', 'default_code', 'model')
        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')

        for field in field_list:
            if not line.get(field):
                continue
            line['sku'] = line[field]
            product, size = product_obj.search_product_by_id(
                cr, uid,
                context['customer_id'],
                line[field],
                search_field=search_field
                )
            if product:
                break

        line_obj['size_id'] = False
        if product:
            if size:
                line_obj['size_id'] = size.id
            elif not product.sizeable:
                line['size'] = False
            elif line.get('size'):
                try:
                    args = [('name', '=', str(float(line['size'])))]
                    size_ids = size_obj.search(cr, uid, args)
                except Exception:
                    size_ids = []
                if size_ids:
                    line_obj['size_id'] = size_ids[0]

            try:
                _cost = line.get('cost') or product_obj.get_val_by_label(
                        cr,
                        uid,
                        product.id,
                        context['customer_id'],
                        'Customer Price',
                        line_obj.get('size_id', False)
                    )
                if not _cost:
                    _cost = ''
                    line_obj["notes"] += "Can't find cost! product_id: %s" % product.id

                line_obj['price_unit'] = _cost

            except Exception:
                line_obj['price_unit'] = ''
            line_obj["product_id"] = product.id
            if not line['name']:
                line_obj['name'] = product.name
                line['name'] = product.name
        else:
            line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
            line_obj["product_id"] = False
        return line_obj
