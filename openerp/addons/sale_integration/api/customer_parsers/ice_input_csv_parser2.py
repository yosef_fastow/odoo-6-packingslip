import csv
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import cStringIO
import traceback
from openerp.addons.pf_utils.utils.import_orders_normalize import fix_line_terminators


class CSVParser(object):

    def __init__(self, parent):
        self.parent = parent

    def parse_data(self, data):
        data = fix_line_terminators(data)
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        if (not data):
            return []
        obj_ordersList = []
        try:
            obj = list(csv.reader(cStringIO.StringIO(data), delimiter=',', quotechar='"', lineterminator='\n'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)
            order_id_old = ""
            ordersObj = {}

            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get('po#')].strip()
                elif (ordersObj.get('order_no', False) and order_id_old != row[idx.get('po#')].strip()):
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get('po#')].strip()
                if (ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False)):
                    ordersObj['partner_id'] = self.parent
                    ordersObj['external_date_order'] = row[idx.get('Date')].strip()
                    ordersObj['po_no'] = row[idx.get('po#')].strip()
                    ordersObj['poNumber'] = ordersObj['po_no']
                    ordersObj['DelmarID'] = row[idx.get('DelmarID')].strip()
                    ordersObj['size'] = False
                    ordersObj['qty'] = row[idx.get('qty')].strip()
                    ordersObj['cost'] = row[idx.get('cost')].strip()
                    ordersObj['customerCost'] = row[idx.get('retail')].strip()
                    ordersObj['order_id'] = ordersObj['po_no']

                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['carrier'] = row[idx.get('shipping code')].strip()
                    ordersObj['address']['ship']['zip'] = row[idx.get('shipto zip')].strip()
                    shipto_name = row[idx.get('shipto name')] or ''
                    shipto_name2 = row[idx.get('shipto name2')] or ''
                    ordersObj['address']['ship']['name'] = shipto_name + ' ' + shipto_name2
                    ordersObj['address']['ship']['address1'] = row[idx.get('shipto address')].strip()
                    ordersObj['address']['ship']['street2'] = ordersObj['address']['ship']['address2'] = row[
                        idx.get('shipto address2')].strip()
                    ordersObj['address']['ship']['city'] = row[idx.get('shipto city')].strip()
                    ordersObj['address']['ship']['country'] = row[idx.get('shipto country')].strip()
                    ordersObj['address']['ship']['state'] = row[idx.get('shipto state')].strip()
                    ordersObj['address']['ship']['phone'] = row[idx.get('shipto phone')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []
                itemsObj = {}
                itemsObj['id'] = row[idx.get('DelmarID')].strip()
                itemsObj['size'] = False
                itemsObj['qty'] = row[idx.get('qty')].strip()
                itemsObj['cost'] = row[idx.get('cost')].strip()
                itemsObj['customerCost'] = row[idx.get('retail')].strip()
                itemsObj['unitprice'] = row[idx.get('cost')].strip()
                itemsObj['name'] = row[idx.get('Description')].strip()
                itemsObj['sku'] = itemsObj['vendorSku'] = row[idx.get('DelmarID')].strip()
                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        for order in obj_ordersList:
            yaml_obj = YamlObject()
            ordersObj = {}
            ordersObj['xml'] = yaml_obj.serialize(_data=order)
            ordersObj['name'] = order['po_no']
            result['orders_list'].append(ordersObj)

        return result
