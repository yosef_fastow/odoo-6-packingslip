# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from element_with_xpath import CSVElementWithXpath, xpathRule


class BaseCSVParser(BaseParser):

    def __init__(self, parent):
        super(BaseCSVParser, self).__init__(parent)

    def split_csv_by_order(self, csv_data, identity):
        identity = identity.strip().lower().replace(' ', '')
        csv_data = strip_lower_del_spaces_from_header(csv_data, 'excel')
        lines = []
        _pseudo_file = IO(csv_data)
        dr = DictReader(_pseudo_file, dialect='excel')
        for row in dr:
            lines.append(row)
        orders = {}
        for line in lines:
            if line[identity] not in orders:
                orders[line[identity]] = []
            orders[line[identity]].append(line)
        return orders


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        xpath = xpath.strip().lower().replace(' ', '')
        keys = xpath.split('|')
        return [Line([{x: y for x, y in line.items() if x in keys}]).dict for line in res]


class Line(CSVElementWithXpath):

    xpaths = {
        'id': ('str', ''),
        'external_customer_line_id': ('str', 'po_li_id'),
        'sku': ('str', 'customer_item_id'),
        'upc': ('str', 'upc_id'),
        'merchantSKU': ('str', 'staples_sku'),
        'vendorSku': ('str', 'supplier_item_id'),
        'name': ('str', 'product_name'),
        'qty': ('int', 'open_quantity'),
        'merchantLineNumber': ('str', 'po_line_number'),
        'additional_fields': {
            'status': ('str', 'status'),
            'item_unit_cost': ('str', 'item_unit_cost'),
            'shipping_cost': ('str', 'shipping_cost'),
            'ext_line_amount': ('str', 'ext_line_amount'),
        }
    }


class Order(CSVElementWithXpath):

    # Staples use one sale_order_number for several PO.
    # To not pass orders we will check uniqueness by PO
    # and store original `sale_order_number` in `cust_order_number` field.

    xpaths = {
        'order_id': ('str', 'po_number'),
        'poNumber': ('str', 'po_number'),
        'cust_order_number': ('str', 'sales_order_number'),
        'external_date_order': ('datetime', 'po_expected_shipdate'),
        'address': {
            'ship': {
                'name': ('str', 'ship_to_name'),
                'company': ('str', 'ship_to_company_name'),
                'address1': ('str', 'ship_to_line1'),
                'address2': ('str', 'ship_to_line2'),
                'address3': ('str', 'ship_to_line3'),
                'city': ('str', 'ship_to_city'),
                'state': ('str', 'ship_to_state'),
                'zip': ('str', 'ship_to_postalcode'),
                'country': ('str', 'ship_to_country_code'),
                'phone': ('str', 'ship_to_telephone'),
                'email': ('str', 'ship_to_email'),
            },
            'order': {
                'name': ('str', 'bill_to_party_full_name'),
                'company': ('str', 'bill_to_party_company_name'),
                'address1': ('str', 'bill_to_party_address_line1'),
                'address2': ('str', 'bill_to_party_address_line2'),
                'address3': ('str', 'bill_to_party_address_line3'),
                'city': ('str', 'bill_to_city_name'),
                'state': ('str', 'bill_to_state_name'),
                'zip': ('str', 'bill_to_party_postal_code'),
                'country': ('str', 'bill_to_party_country_code'),
                'phone': ('str', 'bill_to_party_day_phone'),
                'email': ('str', 'bill_to_party_email'),
            },
        },
        'gift': ('str', 'gift_wrap'),
        'additional_fields': {
            'customer_note': ('str', 'notes_description'),
            'promise_date': ('datetime', 'promised_deliver_timestamp'),
            'requested_ship_date': ('datetime', 'requested_ship_timestamp'),
            'total_po_cost': ('float', 'total_po_cost'),
            'total_number_of_lines': ('int', 'total_number_of_lines'),
            'reject_duplicates': ('str', 'reject_duplicates'),
            'split_shipments': ('str', 'split_shipments'),
            'future_open': ('str', 'future_open'),
            'split_line': ('str', 'split_line'),
            'po_resend': ('str', 'po_resend'),
            'allow_substitutes': ('str', 'allow_substitutes'),
            'priced_packing_slip': ('str', 'priced_packing_slip'),
            'supplier_ship_from_facility_override': ('str', 'supplier_ship_from_facility_override'),
            'source_app': ('str', 'source_app'),
        },
        'lines': ('Line', 'po_li_id|staples_sku|customer_item_id|supplier_item_id|upc_id|product_name|open_quantity|open_quantity_unit|item_unit_cost|ext_line_amount|shipping_cost|status|po_line_number'),
    }

    def __init__(self, csv_lines):
        super(Order, self).__init__(csv_lines)
        self.custom_xpath_rules = {
            'Line': LineXpathRule,
        }
        self.inject_custom_xpath_rules()

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res['gift'] = True if res['gift'] == 'Y' else 'N'
        for address in res['address']:
            if res['address'][address].get('country', '').lower() == 'can':
                res['address'][address]['country'] = 'CA'
        for i, line in enumerate(res['lines']):
            line['id'] = line['external_customer_line_id'] = i + 1
        return res


class CSVParser(BaseCSVParser):

    def __init__(self, parent):
        super(CSVParser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, csv_data):
        orders = self.split_csv_by_order(csv_data, 'sales_order_number')
        for order_name, order_data in orders.items():
            self.orders.append(Order(order_data).dict)
        return True
