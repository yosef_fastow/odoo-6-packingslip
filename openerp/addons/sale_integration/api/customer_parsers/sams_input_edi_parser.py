# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BaseEDIParser
from base_edi import BadEDIFile
import dateutil.parser
import logging

_logger = logging.getLogger(__name__)

# REF01 must be:
# 1) DP - Department Number(5-digit Walmart Department Number)
# 2) MR - Merchandise Type Code(4-digit Walmart Order Type Number)
# 3) KK - Delivery Reference(This will always contain the literal text "Home Delivery" or "Site to Club".)
# 4) IA - Internal Vendor Number(9-digit Walmart assigned Vendor Number)
# 5) 4U - Sam's Brand Name(This will always contain the literal text "SAMSCOM", indicating that this is a SamsClub.com DSV Order.)
# 6) EVI - Event Identification(This will contain the text "Online", indicating that this is an Online Order.)
# 7) CO - Customer Order Number(Reference information as defined for a particular Transaction Set or as specified by the Reference Identification Qualifier)
# 8) AN - Associated Purchase Orders(SamsClub OMS purchase order number)
REF_MAP = {
    'DP': 'department_number',
    'MR': 'merchandise_type_code',
    'KK': 'delivery_reference',
    'IA': 'internal_vendor_number',
    '4U': 'sams_brand_name',
    'EVI': 'event_identification',
    'CO': 'customer_order_number',
    'AN': 'associated_purchase_orders',
}

# ref map for walmart fufilled integration
REF_MAP_WA = {
    '19': 'division_identifier',
    'AN': 'associated_purchase_orders',
    'AO': 'appointment_number',
    'DP': 'department_number',
    'IA': 'internal_vendor_number',
    'MR': 'merchandise_type_code',
    'PD': 'promtion_number'
}

SHIPMENT_METHOD_OF_PAYMENT_MAP = {
    'CC': 'collect',
    'PP': 'prepaid_by_seller'
}

TECHNICAL_ERROR_DESCRIPTION = {
    'P': 'Missing or Invalid Item Quantity',
    'Q': 'Missing or Invalid Item Identification',
    'T': 'Unauthorized Transaction Set Purpose Code',
    'U': 'Missing or Unauthorized Transaction Type Code',
    'V': 'Missing or Unauthorized Action Code',
    'MB': 'Missing or Invalid Purchase Order Number',
    'MF': 'Missing or Invalid Internal Vendor Number',
    'MI': 'Missing or Invalid SCAC',
    '006': 'Duplicate',
    '007': 'Missing Data',
    '009': 'Invalid Date',
    '011': 'Not Matching',
    '012': 'Invalid Combination',
    '024': 'Other Unlisted Reason Error',  # The reason for the application error condition cannot be described using any other code on the standard code list
    '107': 'Missing or Invalid Location',
    '110': 'Missing Marking Identification at Pack Level',
    '133': 'Item Not Found On Purchase Order',
    'IID': 'Invalid Identification Code',
    'IQT': 'Invalid Quantity',
    'MID': 'Missing Identification Code',
    'OTH': 'Unspecified application error',  # Other
    'POI': 'Purchase Order Number Invalid',
}

TECHNICAL_ERROR_DESCRIPTION_WA = {
    'J': 'Missing or Invalid Purpose Code',
    'T': 'Unauthorized Transaction Set Purpose Code',
    'MB': 'Missing or Invalid Purchase Order Number',
    'MK': 'Missing or Invalid Ship Date Location',
    '006': 'Duplicate',
    '007': 'Missing Data',
    '008': 'Out of Range',
    '012': 'Invalid Combination',
    '024': 'Other Unlisted Reason',
    '107': 'Missing or Invalid Location',
    '134': 'Missing or Invalid Invoice Number',
    '140': 'Missing or Invalid Ship-To Location',
    '803': 'Missing or Invalid Payment Method Code',
    'DTE': 'Incorrect Date',
    'IID': 'Invalid Identification Code',
    'INF': 'Invalid Status Code',
    'IQT': 'Invalid Quantity',
    'IWT': 'Invalid Weight',
    'MID': 'Missing Identification Code',
    'OTH': 'Other',
    'POI': 'Purchase Order Number Invalid',
    'SCA': 'Invalid SCAC Standard Carrier Alpha Code)',
    'ZZZ': 'Mutually Defined ',
}


MAINTENANCE_TYPE_CODES = {
    '001': 'Change',
    '002': 'Delete',
    '021': 'Addition'
}


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)

    def _get_main_order_obj(self):
        return {
            'address': {
                'ship': {
                    'name': '',
                    'name2': '',
                    'address1': '',
                    'address2': '',
                    'city': '',
                    'state': '',
                    'zip': '',
                    'country': '',
                    'phone': '',
                    'email': '',
                }
            },
            'partner_id': self.parent.customer,
            'lines': [],
            'cancellation': False,
            'change': False,
            'additional_fields': {},
            'ack_control_number': self.ack_control_number,
            'functional_group': self.functional_group,
        }

    # DLMR-556
    def parse_997_acknowledgement(self, message):
        try:
            correct_notif = False
            rejected = False
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'AK2':
                    if elements[1] in ['810', '846']:
                        correct_notif = True
                elif elements[0] == 'AK5':
                    if elements[1] in ['M', 'R', 'W', 'X']:
                        rejected = True

            if rejected and correct_notif:
                _logger.warn("Reject found in 997, need to send notification!")
                return True
            else:
                return False

        except Exception, e:
            _logger.warn("Error: %s" % e.message)

        return False

    def parse_order_data(self, message, api_customer):
        orders, text_messages = [], []
        try:
            po1_loop = False
            country = 'US'
            ordersObj = self._get_main_order_obj()
            address_type = ''
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'ST':
                    # Transaction set header. Contains identifier code and set control number
                    ordersObj = self._get_main_order_obj()
                    address_type = ''
                elif elements[0] == 'BCH':   # Change
                    if elements[1] == '01':  # Cancellation
                        ordersObj['cancellation'] = True
                    elif elements[1] == '04':  # Change
                        ordersObj['change'] = True
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'BEG':
                    # Beginning Segment.
                    if elements[1] != '00':
                        # 00 original, 22 information copy, 38 No financial value
                        raise BadEDIFile(
                            'BEG01 must be have only "00", now: {code}'.format(
                                code=str(elements[1])))
                    if elements[2] != 'SA':
                        # SA stand alone, RL Release or delivery Order, BE blanket order
                        raise BadEDIFile(
                            'BEG02 must be have only "SA"(Stand-alone Order), now: {code}'.format(
                                code=str(elements[2])))
                    ordersObj['order_id'] = elements[3]
                    # retailers original purchase order
                    ordersObj['poNumber'] = elements[3]
                    # retailers original purchase order
                    # element[4] is the release number. not used by a stand alone order
                    ordersObj['external_date_order'] = dateutil.parser.parse(elements[5])
                    # date in YYYYMMDD
                elif elements[0] == 'CUR':
                    # currency used
                    if elements[1] == 'BY':
                        # Buying Party (Purchaser) only one
                        ordersObj['additional_fields'].update({
                            'currency': elements[2],
                        })
                elif elements[0] == 'REF': # reference information
                    if api_customer == 'Walmart Fulfilled (FBW)':
                        ordersObj['additional_fields'].update({
                            REF_MAP_WA.get(elements[1], elements[1]): elements[2],
                        })
                    else:
                        ordersObj['additional_fields'].update({
                            REF_MAP.get(elements[1], elements[1]): elements[2],
                        })

                    if elements[1] == 'CO':  # Customer Order Number
                        ordersObj['order_id'] = elements[2]
                elif elements[0] == 'FOB':
                    # transportation instruction
                    ordersObj['additional_fields'].update({
                        'shipment_method_of_payment': SHIPMENT_METHOD_OF_PAYMENT_MAP.get(elements[1], elements[1]),
                    })
                    if len(elements) >2 and  elements[2] == 'OR':
                        ordersObj['additional_fields'].update({
                            'ship_node_location': elements[3],
                        })
                elif elements[0] == 'ITD':
                    # terms of sale
                    terms_type_code = {
                        '02': 'End of Month (EOM)',
                        '05': 'Discount Not Applicable',
                        '08': 'Basic Discount Offered',
                    }
                    terms_basis_date_code = {
                        '3': 'Invoice Date',
                        '7': 'Effective Date',
                        '15': 'Receipt of Goods',
                    }
                    ordersObj['additional_fields'].update({
                        'terms_type_code': terms_type_code.get(elements[1], elements[1]),
                        'terms_basis_date_code': terms_basis_date_code.get(elements[2], elements[2]),
                        'terms_discount_percent': elements[3],
                        'terms_Discount_days_due': elements[5],
                        'terms_net_days': elements[7],
                    })
                elif elements[0] == 'DTM':
                     # date time reference
                    if elements[1] == '002':  # Delivery Requested
                        ordersObj['additional_fields'].update({
                            'customer_delivery_request_date': dateutil.parser.parse(elements[2])
                        })
                    elif (api_customer == 'Sams Club' and elements[1] == '004') or (api_customer == 'Walmart Fulfilled (FBW)' and elements[1] == '007'):  # Purchase Order
                        dtm = elements[2]
                        if len(elements) == 4:
                            dtm += elements[3]
                        ordersObj['additional_fields'].update({
                            'customer_delivery_request_date': dateutil.parser.parse(dtm)
                        })
                    elif elements[1] == '001' or (elements[1] == '037' and api_customer == 'Walmart Fulfilled (FBW)'):  # Cancel After
                        # If the order has not been shipped by this date, the order is considered canceled
                        #37 Do not deliver after
                        ordersObj['additional_fields'].update({
                            'cancel_after': dateutil.parser.parse(elements[2])
                        })
                    elif elements[1] == '037' and api_customer == 'Walmart Fulfilled (FBW)':
                        ordersObj['additional_fields'].update({
                            'ship_not_before': dateutil.parser.parse(elements[2])
                        })

                    elif elements[1] == '038' and api_customer == 'Walmart Fulfilled (FBW)':
                        ordersObj['additional_fields'].update({
                            'ship_not_later': dateutil.parser.parse(elements[2])
                        })

                    elif elements[1] == '037' and api_customer == 'Walmart Fulfilled (FBW)':
                        ordersObj['additional_fields'].update({
                            'cancel_after': dateutil.parser.parse(elements[2])
                        })

                    elif (api_customer == 'Sams Club' and elements[1] == '830') or (api_customer == 'Walmart Fulfilled (FBW)' and elements[1] == '704'):  # Schedule
                        ordersObj['additional_fields'].update({
                            'schedule_date': dateutil.parser.parse(elements[2])
                        })
                elif elements[0] == 'TD5':
                    # ROuting sequence transit time
                    if elements[1] == 'O' and api_customer == 'Walmart Fulfilled (FBW)':
                        ordersObj['additional_fields'].update({
                            'routing_description': elements[3]
                        })
                    if elements[2] == 'LE':
                        ordersObj['additional_fields'].update({
                            'language': elements[3],
                        })
                    elif elements[1] == 'B' and po1_loop and len(elements) > 11:  # Routing Sequence Code
                        ordersObj['carrier'] = elements[3] + ' ' + elements[12]
                        ordersObj['additional_fields'].update({
                            'scac_code': elements[3],
                            'service_level_code': elements[12],
                        })
                    elif po1_loop and len(elements) > 11:
                        if elements[12] == 'GT':
                            ordersObj['additional_fields'].update({
                                'tracking': 'required',
                            })
                        if elements[12] == 'ET':
                            ordersObj['additional_fields'].update({
                                'signature': 'required,'
                            })
                #N9*L1*SPECIAL INSTRUCTIONS
                elif elements[0] == 'MTX' and api_customer == 'Walmart Fulfilled (FBW)':
                    ordersObj['additional_fields'].update({
                        'mtx': elements[2]
                    })
                    #No Preticket
                # elif elements[0] == 'SDQ': # dESTINATION qUANTITY
                # elif elements[0] == 'SLN': # SUBLINE ITEM DETAILS
                elif elements[0] == 'N9' and api_customer == 'Sams Club':
                    # extended reference information
                    country = elements[1]
                elif elements[0] == 'SAC':
                    if elements[1] == 'C' and elements[2] == 'G830':
                        # Customer’s shipping and handling
                        ordersObj['shipping_tax'] = elements[5]
                    if len(elements) > 7:
                        ordersObj['additional_fields'].update({
                            'sac_name': elements[1],
                            'sac_code': elements[2],
                            'sac_amount': elements[5],
                            'sac_percent': elements[7]
                        })
                    if len(elements) > 12:
                        ordersObj['additional_fields'].update({
                            'sac_handling_code': elements[12]
                        })

                elif elements[0] == 'N1':
                    # party identification
                    if elements[1] == 'BY':  # Buying Party (Purchaser)
                        address_type = 'order'
                        ordersObj['address']['order'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': '',
                            'san_address': '',
                        }
                        ordersObj['address']['order']['name'] = elements[2]
                        ordersObj['address']['order']['san_address'] = elements[4]
                        ordersObj['additional_fields'].update({
                            'location_number': elements[4]
                        })
                    elif elements[1] == 'BT':  # Bill To
                        address_type = 'invoice'
                        ordersObj['address']['invoice'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['invoice']['name'] = elements[2]
                        if len(elements) > 3 and elements[3] == 'MI':
                            ordersObj['additional_fields'].update({
                                'membership': elements[4]
                            })
                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                    #elif elements[1] == 'SU' $ always DELMAR
                        ordersObj['address']['ship']['name'] = elements[2]
                elif elements[0] == 'N2' and address_type != '':
                    ordersObj['address'][address_type]['name2'] = elements[1]
                elif elements[0] == 'N3' and address_type != '':
                    # party location
                    # HTO FXI: For some reason there are two N3 segments, the 2nd one is with city.
                    if not ordersObj['address'][address_type]['address1']:
                        ordersObj['address'][address_type]['address1'] = elements[1]
                        if len(elements) > 2:
                            ordersObj['address'][address_type]['address2'] = elements[2]
                elif elements[0] == 'N4' and address_type != '':
                    # geographic location
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    ordersObj['address'][address_type]['country'] = country
                    if len(elements) >= 5:
                        ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PER' and api_customer == 'Sams Club':
                    addr_types_map = {
                        'OC': 'invoice',  # Order Contact
                        'RE': 'ship',  # Receiving Contact
                    }
                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        if addr:
                            addr.update({
                                'name': elements[2],
                                'phone': elements[4] if len(elements) > 3 else '',
                                'email': elements[8] if len(elements) > 7 else '',
                            })
                elif elements[0] == 'PO1':
                    # Baseline item data
                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        # 'retail_cost': elements[4],
                        'name': None,
                        'size': None,
                        'sku': None,
                        'customer_sku': None,
                        'upc': None,
                        'vendor_number': None,
                        'additional_fields': {},
                    }
                    if elements[6] == 'IN':
                        # buyers item number
                        lineObj.update({
                            'sku': elements[7],
                            'customer_sku': elements[7],
                        })
                        lineObj['additional_fields'].update({
                            'customer_sku': elements[7],
                        })
                    if elements[8] == 'UP':
                        lineObj.update({
                            'upc': elements[9],
                        })
                        lineObj['additional_fields'].update({
                            'upc': elements[9],
                        })
                    if elements[10] == 'VN':
                        lineObj.update({
                            'vendor_number': elements[11],
                        })
                        if api_customer == 'Walmart Fulfilled (FBW)':
                            lineObj['name'] = elements[11]
                    if api_customer == 'Walmart Fulfilled (FBW)' and len(elements) > 20:
                        if elements[14] == 'IZ':
                            try:
                                size = int(elements[15])
                                lineObj['additional_fields'].update({
                                    'size': size,
                                })
                            except ValueError:
                                pass
                        if elements[22] == 'UK':
                            lineObj['additional_fields'].update({
                                'GTIN-14': elements[23],
                            })

                    ordersObj['lines'].append(lineObj)
                    po1_loop = True
                elif elements[0] == 'CTP':
                    # Pricing Information
                    if elements[2] == 'RTL' or elements[2] == 'RES':
                        # Retail Price
                        ordersObj['lines'][len(ordersObj['lines']) - 1].update({
                            'retail_cost': elements[3]
                        })
                elif elements[0] == 'PO4': # item physical details
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['additional_fields'].update({
                        #  The number of inner containers, or number of eaches if there are no inner containers, per outer container
                        'item_pack_amount': elements[1],
                    })
                elif elements[0] == 'PAM' and api_customer == 'Sams Club':
                    if elements[4] == '8':
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['additional_fields'].update({
                            'item_discount_amount': elements[5],
                        })
                elif elements[0] == 'PID':
                    if elements[1] == 'F':  # Product Description
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['name'] = elements[5]
                    elif elements[1] == 'S':
                        ordersObj['additional_fields'].update({
                            'item_discription_type': elements[1],
                            'item_characteristic_code': elements[2],
                            'agency_qualifier_code': elements[3],
                            'product_description_code': elements[4],
                        })
                elif elements[0] == 'AMT':
                    # Monetary amount information
                    if elements[1] == '1' and po1_loop:
                        # Line Item Total (code to qualify amount)
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['additional_fields'].update({
                            # monetary amount of PO LINE(quantity * unit price)
                            'total': elements[2],
                        })
                        if api_customer == 'Sams Club':
                            ordersObj['lines'][len(ordersObj['lines']) - 1]['retail_cost'] = elements[2]
                    if elements[1] == 'GV':
                        ordersObj['additional_fields'].update({
                            'total_modetary_amount': elements[1]
                        })
                elif (elements[0] == 'GE' and api_customer == 'Sams Club') or (elements[0] == 'CTT' and api_customer == 'Walmart Fulfilled (FBW)'):
                    #Transactions total
                    self.number_of_transaction = elements[1]
                    ordersObj['number_of_transaction'] = self.number_of_transaction
                elif elements[0] == 'SE':
                    # transaction set trailer
                    orders.append(ordersObj)
                    # Ship to Store/Warehouse order
                    if not ordersObj['address'].get('ship'):
                        po_num = ordersObj['poNumber']
                        textObj = ordersObj.copy()
                        textObj.update({
                            'header': 'Received Ship to Store/Warehouse order',
                            'message_text': ['ERP could not process order {0}. Please check Sale Integration XML'.format(po_num)],
                            'message_id': po_num,
                        })
                        text_messages.append(textObj)
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        return orders, text_messages


    def parse_text_message_data(self, message, api_customer):
        text_messages = []
        try:
            text_messageObj = {
                'header': '',
                'invoice_no': '',
                'message_id': '',
                'message_subject': '',
                'message_text': [],
                'ack_control_number': self.ack_control_number,
                'functional_group': self.functional_group,
            }
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'ST':
                    text_messageObj = {
                        'header': '',
                        'invoice_no': '',
                        'message_id': '',
                        'message_subject': '',
                        'message_text': [],
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                    }
                elif elements[0] == 'BMG':
                    text_messageObj['header'] = elements[2]
                elif elements[0] == 'REF':
                    if elements[1] == 'IA':
                        text_messageObj['message_id'] = elements[2]
                    elif elements[1] == 'IV':
                        text_messageObj['invoice_no'] = elements[2]
                elif elements[0] == 'MIT':
                    text_messageObj['message_subject'] = elements[1]
                elif elements[0] == 'MSG':
                    text_messageObj['message_text'].append(elements[1])
                elif elements[0] == 'SE':
                    text_messages.append(text_messageObj)
                elif elements[0] == 'GE':
                    self.number_of_transaction = elements[1]
                    text_messageObj['number_of_transaction'] = self.number_of_transaction
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        return text_messages

    def parse_application_advice_data(self, message, api_customer):
        text_messages = []
        try:
            text_messageObj = {
                'header': '',
                'trans_no': '',
                'message_id': '',
                'message_subject': '',
                'message_text': [],
                'trans_code': '',
                'ack_control_number': self.ack_control_number,
                'functional_group': self.functional_group,
            }
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'ST':
                    text_messageObj = {
                        'header': '',
                        'trans_no': '',
                        'message_id': '',
                        'message_subject': '',
                        'message_text': [],
                        'trans_code': '',
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                    }
                elif (api_customer == 'Sams Club' and elements[0] == 'BMG') or (api_customer == 'Walmart Fulfilled (FBW)' and elements[0] == 'BGN'):
                    text_messageObj['header'] = elements[2]

                elif elements[0] == 'REF':
                    if elements[1] == 'IA':
                        # code quantity IA = internal vendor number
                        text_messageObj['message_id'] = elements[2]
                elif elements[0] == 'OTI':
                    # original transaction identification
                    text_messageObj['trans_no'] = elements[3]
                    if len(elements) > 10:
                        text_messageObj['trans_code'] = elements[10]
                elif elements[0] == 'TED':
                    # technical error description
                    ted_code = elements[1]
                    if api_customer == 'Sams Club':
                        text_messageObj['message_subject'] = TECHNICAL_ERROR_DESCRIPTION.get(ted_code, 'Error')
                    else:
                        text_messageObj['message_subject'] = TECHNICAL_ERROR_DESCRIPTION_WA.get(ted_code, 'Error')
                    if 2 < len(elements) <= 7:
                        tech_error = elements[2]
                        # free form message
                    elif len(elements) > 7:
                        tech_error = elements[2] + ': ' + elements[7]
                        # copy of bad element data
                    text_messageObj['message_text'].append(tech_error)
                    # DLMR-556
                    self._append_error("INVALID_DATA_824:{}:{}".format(text_messageObj['trans_no'], tech_error))
                elif elements[0] == 'NTE':
                    # note /special instruction
                    text_messageObj['message_text'].append(elements[2]) # description
                elif elements[0] == 'SE':
                    # transactions et trailer
                    text_messages.append(text_messageObj) # end message
                elif elements[0] == 'GE':
                    # functional group trailer
                    self.number_of_transaction = elements[1]
                    text_messageObj['number_of_transaction'] = self.number_of_transaction
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        return text_messages

    def parse_organizational_relationship_data(self, message, api_customer):
        text_messages = []
        try:
            text_messageObj = {
                'trans_no': '',
                'message_id': '',
                'message_subject': 'Organizational Relationship',
                'message_text': [],
                'structure': {},
                'order_structure': [],
                'ack_control_number': self.ack_control_number,
                'functional_group': self.functional_group,
            }
            address_fields = [
                'Address1',
                'Address2',
                'City',
                'State/Province',
                'Postal Code',
                'Country',
            ]
            subdivision_fields = [
                'Level',
                'Type',
                'Subdivision of',
                'Name',
                'Global Location Number',
                'Address Info',
                'D-U-N-S',
                'Distribution Center Number',
                'Store Number',
                'Contact Name',
                'Contact Phone',
                'Date',
                'Action',
            ]
            subdivision_template = {el: '' for el in subdivision_fields}
            address_template = {el: '' for el in address_fields}
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'ST':
                    text_messageObj = {
                        'trans_no': '',
                        'message_id': '',
                        'message_subject': 'Organizational Relationship',
                        'message_text': [],
                        'structure': {},
                        'order_structure': [],
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                    }
                elif elements[0] == 'BHT':
                    text_messageObj['message_id'] = elements[3]
                    if elements[2] == '00':
                        text_messageObj['message_subject'] += ' (Original)'
                    elif elements[2] == '04':
                        text_messageObj['message_subject'] += ' (Change)'
                elif elements[0] == 'HL':
                    subdivision = subdivision_template.copy()
                    address = address_template.copy()
                    subdivision['Address Info'] = address
                    id_number = elements[1]
                    text_messageObj['structure'].update({
                        id_number: subdivision,
                    })
                    text_messageObj['order_structure'].append(id_number)
                    parent_id_number = elements[2]
                    if parent_id_number:
                        subdivision['Subdivision of'] = text_messageObj['structure'].get(parent_id_number, {}).get('Name', '')
                    if elements[3] == '35':
                        subdivision['Level'] = 'Company/Corporation'
                    elif elements[3] == '36':
                        subdivision['Level'] = 'Operating Unit'
                elif elements[0] == 'N1' and elements[1] != 'FR':
                    if elements[1] == 'CQ':
                        subdivision['Type'] = 'Corporate Office'
                    elif elements[1] == 'SN':
                        subdivision['Type'] = 'Store'
                    subdivision['Name'] = elements[2]
                    if len(elements) > 4:
                        subdivision['Global Location Number'] = elements[4]
                elif elements[0] == 'N3':
                    address['Address1'] = elements[1]
                    if len(elements) > 2:
                        address['Address2'] = elements[2]
                elif elements[0] == 'N4':
                    address['City'] = elements[1]
                    address['State/Province'] = elements[2]
                    address['Postal Code'] = elements[3]
                    address['Country'] = elements[4]
                elif elements[0] == 'REF':
                    if elements[1] == 'DNS':
                        subdivision['D-U-N-S'] = elements[2]
                    elif elements[1] == 'AEM':
                        subdivision['Distribution Center Number'] = elements[2]
                    elif elements[1] == 'ST':
                        subdivision['Store Number'] = elements[2]
                elif elements[0] == 'PER' and elements[1] == 'IC':
                    subdivision['Contact Name'] = elements[2]
                    if len(elements) > 3 and elements[3] == 'TE':
                        subdivision['Contact Phone'] = elements[4]
                elif elements[0] == 'DTM':
                    subdivision['Date'] = elements[2]
                elif elements[0] == 'ASI':
                    subdivision['Action'] = MAINTENANCE_TYPE_CODES.get(elements[2], '')
                elif elements[0] == 'SE':
                    for id_number in text_messageObj['order_structure']:
                        subdivision_data = text_messageObj['structure'].get(id_number, None)
                        if subdivision_data:
                            for subdivision_field in subdivision_fields:
                                subdivision_field_value = subdivision_data.get(subdivision_field)
                                if subdivision_field == 'Address Info':
                                    text_messageObj['message_text'].append(subdivision_field)
                                    text_messageObj['message_text'].extend(
                                        ['  ' + el + ': ' + subdivision_field_value.get(el) for el in address_fields if subdivision_field_value.get(el)]
                                    )
                                elif subdivision_field_value:
                                    text_messageObj['message_text'].append(subdivision_field + ': ' + subdivision_field_value)
                            text_messageObj['message_text'].append(' ')
                    text_messages.append(text_messageObj)
                elif elements[0] == 'GE':
                    self.number_of_transaction = elements[1]
                    text_messageObj['number_of_transaction'] = self.number_of_transaction

        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        return text_messages

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result

        orders = []
        text_messages = []
        str_message = str(edi_data)
        api_customer = ''
        if str_message.find('SAM') >= 0:
            api_customer = 'Sams Club'
        else:
            api_customer = 'Walmart Fulfilled (FBW)'
        # else:
        #     api_customer = 'Sams Club'
        try:
            message = EdiParser(edi_data)

            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'GS':
                    # functional group header
                    self.ack_control_number = elements[6]
                    # group control number
                    self.functional_group = elements[1]
                    # functional identifier code
                    self.sender = elements[2]
                    # functional identifier code
                    self.receiver = elements[3]
                    # application reciever code
                    # if (self.sender != self.parent.receiver_id):
                    #     raise BadSenderError('Bad file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    # if (self.receiver != self.parent.sender_id):
                    #     raise BadSenderError('Bad file! Correct receiver_id: {0}'.format(self.parent.sender_id))
                elif elements[0] == 'ST':
                    if elements[1] in ('850', '860'):  # 850 - Order, 860 - Change # DELMAR-186
                        orders, text_messages = self.parse_order_data(message, api_customer)
                    elif elements[1] == '864':  # Text Message
                        text_messages = self.parse_text_message_data(message, api_customer)
                    elif elements[1] == '824':  # Application Advice
                        text_messages = self.parse_application_advice_data(message, api_customer)
                    elif elements[1] == '816':  # Organizational Relationship
                        text_messages = self.parse_organizational_relationship_data(message, api_customer)
                    elif elements[1] == '997':  # Functional Acknowlegement, skip this file
                        # DLMR-556 check is Rejected, not skip file
                        # self.result.update({'is_rejected': self.parse_997_acknowledgement(message)})
                        return self.result
                    else:
                        return self.result
                    break
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if api_customer == 'Walmart Fulfilled (FBW)':
                    order_obj['partner_id'] = 'fbw'
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                # # DELMAR-186
                # if (order_obj['lines']):
                #     serialize_line_fields = {}
                #     for key, value in order_obj['lines'][0].iteritems():
                #         if (isinstance(value, (tuple, list))):
                #             if key == 'name' and (value == None or value == ''):
                #                 value = 'empty product description in order'
                #             serialize_line_fields.update({
                #                 key: json.dumps(value)
                #             })
                #         else:
                #             if key == 'name' and (value == None or value == ''):
                #                 value = 'empty product description in order'
                #             serialize_line_fields.update({
                #                 key: value
                #             })
                #     order_obj['lines'][0] = serialize_line_fields
                # # END DELMAR-186
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                returned_order['new_type_api_id'] = api_customer
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        for text_message in text_messages:
            returned_message = {}
            try:
                returned_message['name'] = 'TEXTMESSAGE{0}'.format(text_message['message_id'])
                message_data = [text_message.get('header', ''), text_message.get('message_subject', '')]
                if text_message.get('invoice_no'):
                    message_data.append('INVOICE # {0}'.format(text_message.get('invoice_no')))
                elif text_message.get('trans_no'):
                    message_data.append('TRANSACTION ({1}) # {0}'.format(text_message.get('trans_no'), text_message.get('trans_code', 'Unspecified code')))
                message_data.extend(text_message.get('message_text', ''))
                returned_message['message_data'] = '\n'.join([el for el in message_data if el])
                returned_message['xml'] = str(text_message)
                returned_message['ack_control_number'] = text_message.get('ack_control_number', '')
                returned_message['functional_group'] = text_message.get('functional_group', '')
                returned_message['number_of_transaction'] = text_message.get('number_of_transaction', '')
                returned_message['new_type_api_id'] = api_customer
                self.result['orders_list'].append(returned_message)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
