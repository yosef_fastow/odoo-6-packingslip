# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import dateutil.parser


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        try:
            message = EdiParser(edi_data)
            line_id = 1
            n9_message = 'ZZ'
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'poHdrData': {
                            'discountTerms': {}
                        },
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':
                    if elements[1] == 'CO':  # Customer Order Number
                        ordersObj['additional_fields'].update({
                            'customer_order_number': elements[2]
                        })
                        #ordersObj['order_id'] = elements[2]
                        # ordersObj['poNumber'] = elements[2]
                    elif elements[1] == 'IA':  # Internal Vendor Number
                        ordersObj['additional_fields'].update({
                            'internal_vendor_number': elements[2]
                        })
                    elif elements[1] == 'IL':  # Internal Order Number
                        ordersObj['additional_fields'].update({
                            'internal_order_number': elements[2]
                        })
                    elif elements[1] == 'IT': # Internal Customer Number
                        ordersObj['additional_fields'].update({
                            'internal_customer_number': elements[2]
                        })
                elif elements[0] == 'PER':
                    addr = ordersObj.get('address', {}).get('ship')
                    if addr:
                        addr.update({
                            'name': elements[2],
                            'phone': elements[4] if len(elements) > 3 else '',
                            'email': elements[6] if len(elements) > 7 else '',
                        })
                elif elements[0] == 'CSH':
                    sales_requirements = {
                        'N': 'No Backorder',
                        'P4': 'Do Not Preship',
                        'SC': 'Ship Complete'
                    }
                    if elements[1] in sales_requirements:
                        ordersObj['additional_fields'].update({
                            'sales_requirements': sales_requirements[elements[1]]
                        })
                elif elements[0] == 'SAC':
                    if elements[1] == 'A' and elements[2] == 'A260':  # Advertizement allowance
                        ordersObj['poHdrData']['discountTerms']['discTypeCode'] = elements[2]
                        ordersObj['poHdrData']['discountTerms']['discPercent'] = elements[7]
                elif elements[0] == 'ITD':
                    ordersObj['poHdrData']['discountTerms']['discDateCode'] = elements[1]
                    ordersObj['poHdrData']['discountTerms']['discDaysDue'] = elements[5]
                    ordersObj['poHdrData']['discountTerms']['netDaysDue'] = elements[7]
                    ordersObj['poHdrData']['discountTerms']['text'] = elements[12]

                elif elements[0] == 'DTM':
                    if elements[1] == '001':
                        ordersObj['additional_fields'].update({
                            'cancel_after': dateutil.parser.parse(elements[2])
                        })
                    elif elements[1] == '002':
                        ordersObj['additional_fields'].update({
                            'customer_delivery_request_date': dateutil.parser.parse(elements[2])
                        })
                #CTB = Restriction / Conditions
                #N9 = Refrence identification
                elif elements[0] == 'N9':
                    n9_message = elements[1]
                elif elements[0] == 'MTX':
                    mtx_options = {
                        'QS': 'lcp_contract_number',
                        'SH': 'gift_message',
                        'ZZ': 'MTX',
                    }
                    if n9_message in mtx_options:
                        ordersObj['additional_fields'].update({
                            mtx_options[n9_message]: elements[2]
                        })
                    else:
                        ordersObj['additional_fields'].update({
                            'MTX': ordersObj['additional_fields'].get('MTX', '')+' '+elements[2]
                        })

                elif elements[0] == 'N1':
                    if elements[1] == 'BT':  # Bill-to-Party, Vendor
                        address_type = 'invoice'
                        ordersObj['address']['invoice'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['invoice']['name'] = elements[2]
                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address']['ship'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['ship']['name'] = elements[2]
                elif elements[0] == 'N3' and address_type != '':
                    ordersObj['address'][address_type]['address1'] = elements[1]
                    if len(elements) > 2:
                        ordersObj['address'][address_type]['address2'] = elements[2]
                elif elements[0] == 'N4' and address_type != '':
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    ordersObj['address'][address_type]['country'] = 'US'
                    if len(elements) == 5:
                        ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PO1':
                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'sku': elements[7],
                        'customer_sku': elements[7],
                        'customer_id_delmar': elements[9],
                        'vendorSku': elements[7],
                        'merchantSKU': elements[9],
                        'additional_fields': {},
                    }
                    if not lineObj['id']:
                        lineObj['id'] = line_id
                        line_id += 1
                    if len(elements) > 11:
                        try:
                            lineObj['size'] = '{0:.1f}'.format(float(elements[11]))
                        except:
                            pass
                    ordersObj['lines'].append(lineObj)

                elif elements[0] == 'PID':
                    if elements[1] == 'F' and elements[2] == '08':
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['name'] = elements[5]
                elif elements[0] == 'GE':
                    self.number_of_transaction = elements[1]
                    ordersObj['number_of_transaction'] = self.number_of_transaction
                elif elements[0] == 'SE':
                    orders.append(ordersObj)
        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
