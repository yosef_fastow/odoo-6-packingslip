# -*- coding: utf-8 -*-

#  Copyright (c) 2020
#  Project: delmar_openerp
#  File: hautelook_dsco_edi_parser.py
#  Author: snake@SOLVVE
#  Updated: 3/30/20, 1:18 PM

from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import logging

_logger = logging.getLogger(__name__)


class EDIParser(BaseEDIParser):

    ack_control_number = None
    functional_group = None
    # a list of incoming orders
    orders = {}
    _current_order = None
    # a list of addresses
    addresses = {}
    _current_address = None
    # a list of lines
    lines = {}
    _current_line = None

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)

    @property
    def order(self):
        """
        Current iterated order
        :rtype: dict
        """
        if self._current_order and self._current_order in self.orders:
            return self.orders[self._current_order]
        return None

    @order.setter
    def order(self, val):
        """
        String of order unique number in current parsing iteration
        :param string val: Uniq num of iterated order
        """
        self._current_order = val

    @property
    def address(self):
        """
        Current iterated address
        :rtype: dict
        """
        if self._current_address and self._current_address in self.addresses:
            return self.addresses[self._current_address]
        return  None

    @address.setter
    def address(self, val):
        """
        String of address type in current parsing iteration
        :param string val: Uniq type of address
        """
        self._current_address = val

    @property
    def line(self):
        """
        Current iterated po-line
        :rtype: dict
        """
        if self._current_line and self._current_line in self.lines:
            return self.lines[self._current_line]
        return  None

    @line.setter
    def line(self, val):
        """
        String of line id in current parsing iteration
        :param string val: Uniq line number
        """
        self._current_line = val

    def parse_data(self, edi_data):
        if not edi_data:
            return self.result
        # start parsing EDI file
        try:
            message = EdiParser(edi_data)
            # iterating lines
            for segment in message:
                elements = segment.split(message.delimiters[1])
                _logger.debug("Parsing segment elements: %s" % elements)
                # header
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                # order header
                if elements[0] == 'ST':
                    # define order we are currently parsing
                    self.order = elements[2]
                    # and create new order in a list of orders
                    self.orders.update({
                        elements[2]: {
                            'address': {},
                            'lines': [],
                            'cancellation': False,
                            'change': False,
                            'additional_fields': {},
                            'poHdrData': {},
                            'partner_id': self.parent.customer,
                            'ack_control_number': self.ack_control_number,
                            'functional_group': self.functional_group,
                        }
                    })
                    # Acknowledgement - skip this file
                    if elements[1] == '997':
                        return self.result
                # order data
                if elements[0] == 'BEG':
                    self.order.update({
                        'order_id': elements[3],
                        'poNumber': elements[3],
                        'external_date_order': elements[5]
                    })
                # order additional fields and some order's data
                if elements[0] == 'REF':
                    additional_fields_data = {}
                    po_hdr_data = {}
                    order_data = {}
                    # trading partner id
                    if elements[1] == 'IA':
                        additional_fields_data.update({'trading_partner_id': elements[2]})
                    # default REFs
                    if len(elements) == 4:
                        additional_fields_data.update({elements[3]: elements[2]})
                        # receipt
                        if elements[3] == 'receipt_id':
                            order_data.update({'receipt_id': elements[2]})
                        # gift flag
                        if elements[3] == 'gift_flag':
                            po_hdr_data.update({'giftIndicator': elements[2]})
                        # force warehouse
                        if elements[3] == 'dsco_warehouse_code':
                            additional_fields_data.update({'vendor_warehouse': elements[2]})
                            order_data.update({'vendor_warehouse': elements[2]})
                        # S2S
                        if elements[3] == 'ship_store_number':
                            order_data.update({'store_number': elements[2]})
                        # order id
                        if elements[3] == 'dsco_order_id':
                            order_data.update({
                                'order_id': elements[2],
                                'external_customer_order_id': elements[2]
                            })
                    # update fields
                    self.order.update(order_data)
                    self.order['additional_fields'].update(additional_fields_data)
                    self.order['poHdrData'].update(po_hdr_data)
                # taxes and shipping handling
                if elements[0] == 'SAC':
                    if elements[1] == 'C' and elements[2] == 'D230':
                        self.order.update({
                            'shipping_tax': elements[5],
                            'amount_tax': elements[5]
                        })
                # order created date
                if elements[0] == 'DTM' and elements[1] == '004':
                    self.order.update({'external_date_order': elements[2]})
                # shipping carrier details
                if elements[0] == 'TD5':
                    self.order['additional_fields'].update({
                        'dsco_ship_carrier': elements[3],
                        'dsco_ship_method': elements[5],
                        'dsco_shipping_service_level_code': len(elements) > 8 and elements[8],
                    })
                    self.order.update({
                        'ship_service': elements[3],
                        'ship_method': elements[5],
                        'carrier': len(elements) > 8 and elements[8]
                    })
                # signature required
                if elements[0] == 'TD4' and elements[4] == 'signature_required_flag':
                    self.order.update({'signature': elements[5]})
                # customer (consumer) order number
                if elements[0] == 'N9':
                    # for order
                    if elements[1] == 'CO':
                        self.order['additional_fields'].update({'consumer_order_number': elements[2]})
                        self.order['poHdrData'].update({'custOrderNumber': elements[2]})
                    # for letter or note
                    elif elements[1] == 'L1':
                        self.order['additional_fields'].update({'notes': elements[2]})
                # gift message
                if elements[0] == 'MTX' and elements[1] == 'EAJ':
                    self.order['additional_fields'].update({'gift_message': elements[2]})
                    self.order['poHdrData'].update({
                        'gift_message': elements[2],
                        'giftIndicator': 'y'
                    })
                # address started
                if elements[0] == 'N1':
                    # gift data
                    if elements[1] in ['GIR', 'BY']:
                        self.order['additional_fields'].update({
                            'gift_recipient_name': elements[2],
                            'gift_sender_name': elements[2]
                        })
                    # ship-to or bill-to address started
                    if elements[1] in ['ST', 'BT']:
                        self.address = 'ship' if elements[1] == 'ST' else 'order'
                        self.addresses.update({
                            'ship': {
                                'name': elements[2],
                                'name2': '',
                                'address1': '',
                                'address2': '',
                                'city': '',
                                'state': '',
                                'zip': '',
                                'country': '',
                                'phone': '',
                                'email': ''
                            }
                        })
                        if elements[1] == 'ST':
                            self.address.update({
                                'personPlaceData': {
                                    'ReceiptID': self.order.get('receipt_id', '')
                                },
                                'carrier': self.order.get('carrier', ''),
                                'ship_service': self.order.get('ship_service')
                            })
                # address line 1 and line 2
                if elements[0] == 'N3':
                    self.address.update({'address1': elements[1]})
                    if len(elements) > 2:
                        self.address.update({'address2': elements[2]})
                # city, state, zip, country
                if elements[0] == 'N4':
                    self.address.update({
                        'city': elements[1],
                        'state': elements[2],
                        'zip': elements[3],
                        'country': elements[4]
                    })
                # contact phone / email
                if elements[0] == 'PER':
                    addr_types_map = {
                        'OC': 'order',  # Order Contact
                        'IC': 'ship',   # Information
                    }
                    if elements[1] in addr_types_map:
                        addr = self.addresses.get(addr_types_map.get(elements[1]))
                        if addr:
                            addr.update({
                                'phone': elements[4] if len(elements) > 4 else '',
                                'email': elements[6] if len(elements) > 6 else '',
                            })
                # base PO line
                if elements[0] == 'PO1':
                    self.line = elements[1]
                    self.lines.update({
                        elements[1]: {
                            'id': elements[1],
                            'qty': elements[2],
                            'cost': elements[4],
                            'customerCost': 0,
                            'retail_cost': 0,
                            'additional_fields': {},
                            'poLineData': {
                                'lineNote2': self.order.get('receipt_id', ''),

                            }
                        }
                    })
                    # check existed data in PO1 segment
                    for i in range(6, 22, 2):
                        if len(elements) > i:
                            if elements[i] == 'SK':
                                self.line.update({
                                    'sku': elements[i + 1],
                                    'merchantSKU': elements[i + 1],
                                })
                            if elements[i] == 'UP':
                                self.line.update({'upc': elements[i + 1]})
                            if elements[i] == 'PD':
                                self.line.update({'name': elements[i + 1]})
                            if elements[i] == 'BP':
                                self.line.update({'vendorSku': elements[i + 1]})
                        else:
                            break
                # line data
                if elements[0] == 'LIN':
                    if len(elements) > 2 and elements[2] == 'SK':
                        self.line.update({
                            'sku': elements[3],
                        })
                    if len(elements) > 4 and elements[4] == 'UP':
                        self.line.update({'upc': elements[5]})
                # consumer and retail price
                if elements[0] == 'CTP':
                    if elements[2] == 'PUR':
                        self.line.update({'customerCost': elements[3]})
                    if elements[2] == 'RTL':
                        self.line.update({'retail_cost': elements[3]})
                # end of order block
                elif elements[0] == 'SE':
                    tax_type = "{} = {}%".format(
                        self.order.get('additional_fields').get('tax_type_code_1', ''),
                        self.order.get('additional_fields').get('tax_percentage_1', '')
                    ) if self.order.get('additional_fields').get('tax_type_code_1', False) else ''
                    self.order.update({
                        'paymentMethod': "{} {}".format(
                            self.order.get('additional_fields').get('payment_card_type_1', ''),
                            self.order.get('additional_fields').get('payment_card_last_four_1', '')
                        ).strip(),
                        'tax': tax_type,
                    })
                    self.order['poHdrData'].update({
                        'taxType': tax_type
                    })
                    # append order lines and addresses
                    self.order.update({
                        'lines': self.lines.values(),
                        'address': self.addresses
                    })
                    # clear current states
                    self._current_line = None
                    self._current_address = None
                    self._current_order = None

        # except BadSenderError:
        #   return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)

        # generate result
        yaml_obj = YamlObject()
        for k, order_obj in self.orders.iteritems():
            returned_order = {}
            try:
                if order_obj['cancellation'] is True:
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif order_obj['change'] is True:
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if order_obj['additional_fields']:
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if isinstance(value, (tuple, list)):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
