# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from pf_utils.utils.re_utils import str2date
from lxml.etree import XPathEvalError


class xpathRule(object):

    __metaclass__ = ABCMeta

    def __init__(self):
        super(xpathRule, self).__init__()

    @abstractmethod
    def process_res(self, res, xpath):
        return res


class staticXpathRule(xpathRule):
    __type_res__ = 'static'

    def process_res(self, res, xpath):
        return xpath


class AbstractElementWithXpath(object):

    __metaclass__ = ABCMeta

    xpath_rules = {
        'static': staticXpathRule,
    }

    xpaths = {}

    def __init__(self, input_obj):
        super(AbstractElementWithXpath, self).__init__()
        self.input_obj = input_obj
        self.inject_custom_xpath_rules()

    def __getdict__(self, obj):
        result = {}
        if isinstance(obj, dict):
            for key, value in obj.iteritems():
                if not isinstance(value, dict):
                    result.update({
                        key: self.__get_elem__(
                            type_res=value[0],
                            xpath=value[1]
                        )
                    })
                else:
                    result.update({
                        key: self.__getdict__(value)
                    })
        else:
            raise NotImplementedError('obj not a dict!')
        return result

    def __getattr__(self, name):
        result = None
        if name in self.xpaths:
            if isinstance(self.xpaths[name], dict):
                result = self.__getdict__(self.xpaths[name])
            elif isinstance(self.xpaths[name], tuple):
                result = self.__get_elem__(name=name)
            else:
                raise NotImplementedError(
                    'In xpaths you can use dict or tuple, {} not available'.format(
                        str(type(self.xpaths[name]))
                    )
                )
        else:
            result = None
        return result

    def to_dict(self):
        result = {}
        for name in self.xpaths:
            result.update({name: self.__getattr__(name)})
        return result

    @property
    def dict(self):
        return self.to_dict()

    @dict.setter
    def dict(self, value):
        raise AttributeError('You can\'t set property dict!')

    @abstractmethod
    def xpath_get(self, xpath):
        pass

    @property
    def available_types_for_result(self):
        return [y.__type_res__ for x, y in self.xpath_rules.items()]

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            res = self.xpath_get(xpath)
            if type_res not in self.available_types_for_result:
                raise NotImplementedError(
                    'Unknown {} type_res, available types: {}'.format(
                        type_res,
                        ','.join([str(x) for x in self.available_types_for_result])
                    )
                )
            rule = self.xpath_rules.get(type_res, None)
            if rule is not None and res:
                result = rule().process_res(res, xpath)
            else:
                result = None
        except Exception:
            result = False
        return result

    def inject_custom_xpath_rules(self):
        if (
            hasattr(self, 'custom_xpath_rules') and
            self.custom_xpath_rules and
            isinstance(self.custom_xpath_rules, dict)
        ):
            for key, value in self.custom_xpath_rules.items():
                self.xpath_rules.update({key: value})


class LxmlStrXpathRule(xpathRule):
    __type_res__ = 'str'

    def process_res(self, res, xpath):
        return str(res[0].text) if len(res) > 0 else res


class LxmlDatetimeXpathRule(xpathRule):
    __type_res__ = 'datetime'

    def process_res(self, res, xpath):
        return str2date(res[0].text) if len(res) > 0 else res


class LxmlBoolXpathRule(xpathRule):
    __type_res__ = 'bool'

    def process_res(self, res, xpath):
        map = {
            'true': True,
            'false': False
        }
        value = res[0].text if len(res) > 0 else None
        return map.get(value, None)


class LxmlIntXpathRule(xpathRule):
    __type_res__ = 'int'

    def process_res(self, res, xpath):
        value = res[0].text if len(res) > 0 else None
        return _try_parse(value, 'int') if value else res


class LxmlFloatXpathRule(xpathRule):
    __type_res__ = 'float'

    def process_res(self, res, xpath):
        value = res[0].text if len(res) > 0 else None
        return _try_parse(value, 'float') if value else res


class lxmlElementWithXpath(AbstractElementWithXpath):

    custom_xpath_rules = {
        'str': LxmlStrXpathRule,
        'datetime': LxmlDatetimeXpathRule,
        'bool': LxmlBoolXpathRule,
        'int': LxmlIntXpathRule,
        'float': LxmlFloatXpathRule,
    }

    def __init__(self, lxml_etree_Element):
        super(lxmlElementWithXpath, self).__init__(lxml_etree_Element)

    def xpath_get(self, xpath):
        elem = self.input_obj
        result = None
        try:
            result = elem.xpath(xpath)
        except XPathEvalError:
            result = False
        return result


class CSVStrXpathRule(xpathRule):
    __type_res__ = 'str'

    def process_res(self, res, xpath):
        return res


class CSVDatetimeXpathRule(xpathRule):
    __type_res__ = 'datetime'

    def process_res(self, res, xpath):
        return str2date(res)


class CSVBoolXpathRule(xpathRule):
    __type_res__ = 'bool'

    def process_res(self, res, xpath):
        map = {
            'true': True,
            'false': False
        }
        return map.get(res, None)


class CSVIntXpathRule(xpathRule):
    __type_res__ = 'int'

    def process_res(self, res, xpath):
        return _try_parse(res, 'int') if res else res


class CSVFloatXpathRule(xpathRule):
    __type_res__ = 'float'

    def process_res(self, res, xpath):
        return _try_parse(res, 'float') if res else res


class CSVElementWithXpath(AbstractElementWithXpath):

    custom_xpath_rules = {
        'str': CSVStrXpathRule,
        'datetime': CSVDatetimeXpathRule,
        'bool': CSVBoolXpathRule,
        'int': CSVIntXpathRule,
        'float': CSVFloatXpathRule,
    }

    def __init__(self, csv_lines):
        super(CSVElementWithXpath, self).__init__(csv_lines)

    def xpath_get(self, xpath):
        elem = self.input_obj
        result = None
        try:
            xpath = xpath.strip().lower().replace(' ', '')
            if '|' in xpath:
                result = elem
            else:
                result = elem[0].get(xpath, None)
        except Exception:
            result = False
        return result
