import csv
from ..utils import xmlutils as xml_utils
import cStringIO

class PagodaApiGetOrdersXML(object):

    def parse_response(self, response):
        if(not response):
            return []
        obj_ordersList = []
        ordersList = []
        for item in response:
            obj = list(csv.reader(cStringIO.StringIO(item), delimiter=',', quotechar='"', lineterminator='\r\n'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                col_ = col.strip().lower().replace(' ', '_')
                idx[col_] = head_row.index(col)
            order_id_old = ""
            ordersObj = {}
            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get('order_no')].strip()
                elif(ordersObj.get('order_no', False) and order_id_old != row[idx.get('order_no')].strip()):
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get('order_no')].strip()
                if(ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False)):
                    ordersObj['sold'] = {}
                    ordersObj['sold']['soldcity'] = idx.get('soldcity', False) and row[idx.get('soldcity')].strip() or False
                    ordersObj['sold']['soldstate'] = idx.get('soldstate', False) and row[idx.get('soldstate')].strip() or False
                    ordersObj['sold']['soldzip'] = idx.get('soldzip', False) and row[idx.get('soldzip')].strip() or False
                    ordersObj['sold']['soldphone'] = idx.get('soldphone', False) and row[idx.get('soldphone')].strip() or False
                    ordersObj['ship'] = {}
                    ordersObj['ship']['shipname'] = row[idx.get('shipname')].strip()
                    ordersObj['ship']['shipadd1'] = row[idx.get('shipadd1')].strip()
                    ordersObj['ship']['shipadd2'] = row[idx.get('shipadd2')].strip()
                    ordersObj['ship']['shipcity'] = row[idx.get('shipcity')].strip()
                    ordersObj['ship']['shipstate'] = row[idx.get('shipstate')].strip()
                    ordersObj['ship']['shipzip'] = row[idx.get('shipzip')].strip()
                    ordersObj['ship']['shipphone'] = row[idx.get('shipphone')].strip()
                    ordersObj['ship']['ship_via'] = row[idx.get('ship_via')].strip()
                    ordersObj['date'] = row[idx.get('m/d/yyyy')].strip()
                    ordersObj['po_no'] = row[idx.get('po_no')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []
                itemsObj = {}
                itemsObj['item'] = row[idx.get('item')].strip()
                itemsObj['desc'] = row[idx.get('desc')].strip()
                itemsObj['qty_shippe'] = row[idx.get('qty_shippe')].strip()
                itemsObj['vendorSku'] = row[idx.get('vendor_sku')].strip()
                itemsObj['store'] = row[idx.get('store')].strip()
                itemsObj['size'] = row[idx.get('size')].strip()
                itemsObj['qty'] = row[idx.get('qty')].strip()
                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        for order in obj_ordersList:
            ordersObj = {}
            ordersObj['xml'] = xml_utils.XmlUtills(order, True).to_xml()
            ordersObj['name'] = order['order_no']
            ordersList.append(ordersObj)
        return ordersList