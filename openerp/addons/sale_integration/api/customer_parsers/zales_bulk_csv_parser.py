import csv
from ..utils import xmlutils as xml_utils
import cStringIO

class ZalesApiGetOrdersXML(object):

    def parse_response(self, response):
        if(not response):
            return []
        obj_ordersList = []
        ordersList = []
        for item in response:
            obj = list(csv.reader(cStringIO.StringIO(item), delimiter=',', quotechar='"'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)
            idx['po_number'] = 1
            idx['Order Number'] = 23
            order_id_old = ""
            ordersObj = {}
            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get('Order Number')].strip()
                    ordersObj['po_no'] = row[idx.get('po_number')].strip()
                elif(ordersObj.get('order_no', False) and order_id_old != row[idx.get('Order Number')].strip()):
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get('Order Number')].strip()
                if(ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False)):
                    ordersObj['merchant_id'] = row[idx.get('Merchant ID')].strip()
                    ordersObj['date'] = row[idx.get('Order Date')].strip()
                    ordersObj['ship'] = {}
                    ordersObj['ship']['name'] = row[idx.get('Name')].strip()
                    ordersObj['ship']['address1'] = row[idx.get('Address1')].strip()
                    ordersObj['ship']['address2'] = row[idx.get('Address2')].strip()
                    ordersObj['ship']['city'] = row[idx.get('City')].strip()
                    ordersObj['ship']['state'] = row[idx.get('State')].strip()
                    ordersObj['ship']['country'] = row[idx.get('Country')].strip()
                    ordersObj['ship']['zip'] = row[idx.get('Zip')].strip()
                    ordersObj['ship']['phone'] = row[idx.get('Phone')].strip()
                    ordersObj['ship']['store_number'] = row[idx.get('Store Number')].strip()
                    ordersObj['ship']['ship_method'] = row[idx.get('Ship Method')].strip()
                    ordersObj['ship']['service_type'] = row[idx.get('Service Type')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []
                itemsObj = {}
                itemsObj['item_line_number'] = row[idx.get('Item Line Number')].strip()
                itemsObj['description'] = row[idx.get('Description')].strip()
                itemsObj['vendorSku'] = row[idx.get('Vendor SKU')].strip()
                itemsObj['merchantSKU'] = row[idx.get('Merchant SKU')].strip()
                itemsObj['size'] = row[idx.get('Size')].strip()
                itemsObj['unit_price'] = row[idx.get('Unit Price')].strip()
                itemsObj['unit_cost'] = row[idx.get('Unit Cost')].strip()
                itemsObj['qty'] = row[idx.get('Quantity')].strip()
                itemsObj['item_count'] = row[idx.get('Item Count')].strip()
                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        for order in obj_ordersList:
            ordersObj = {}
            ordersObj['xml'] = xml_utils.XmlUtills(order, True).to_xml()
            ordersObj['name'] = order['order_no']
            ordersList.append(ordersObj)
        return ordersList