# -*- coding: utf-8 -*-
from base_parser import ElementWithXpath
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
from base_edi import BaseEDIParser
from pf_utils.utils.re_utils import str2date
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from json import dumps as json_dumps


class map_type(object):

    def __init__(self, map_dict):
        super(map_type, self).__init__()
        self.map_dict = map_dict


class map_type_with_description(map_type):
    pass


class ediElementWithXpath(ElementWithXpath):

    def __init__(self, input_segments):
        super(ediElementWithXpath, self).__init__()
        self.input_obj = input_segments

    def xpath_get(self, xpath):
        elem = self.input_obj
        number_segment = None
        if '.' in xpath:
            xpaths = xpath.split('.')
            xpath = xpaths[0]
            try:
                number_segment = int(xpaths[1])
            except ValueError:
                pass
        elem_number = None
        if '->' in xpath:
            xpaths = xpath.split('->')
            xpath = xpaths[0]
            try:
                elem_number = int(xpaths[1])
            except ValueError:
                pass
        try:
            res = elem[xpath]
            if res:
                if number_segment:
                    if elem_number:
                        res = res[elem_number][number_segment]
                    else:
                        res = res.first()[number_segment]
                else:
                    if elem_number:
                        res = res[elem_number]
        except:
            res = False
        return res

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            res = self.xpath_get(xpath)
            if type_res == str and res:
                result = str(res)
            elif type_res == 'date' and res:
                result = str2date(res)
            elif type_res == 'flag':
                if res:
                    if res in ['y', 'Y']:
                        result = True
                    else:
                        result = False
                else:
                    result = None
            elif type_res == bool:
                result = res
            elif type_res == int and res:
                result = _try_parse(res, 'int')
            elif type_res == float and res:
                result = _try_parse(res, 'float')
            elif isinstance(type_res, map_type):
                if type(type_res) == map_type_with_description:
                    result = {'original': res, 'description': type_res.map_dict.get(res, None)}
                else:
                    result = type_res.map_dict.get(res, None)
            elif type_res == N9Msg:
                result = {
                    str(N9Msg(msg).dict['order_status_message_number']):
                    N9Msg(msg).dict['order_status_message'] for msg in res
                }
            elif type_res == Address:
                result = Address(res).dict
            elif type_res == AddressAdditionalName:
                result = AddressAdditionalName(res).dict['additional_name']
            elif type_res == Line:
                result = [Line(line).dict for line in res]
            else:
                result = None
        except Exception as e:
            print(e)
            result = False

        return result


class N9Msg(ediElementWithXpath):
    xpaths = {
        'order_status_message_number': (int, 'N9.2'),
        'order_status_message': (str, 'MSG.1'),
    }


class Address(ediElementWithXpath):
    xpaths = {
        'name': (str, 'N1.2'),
        'address3': (str, 'N2.2'),

        'address1': (str, 'N3.1'),
        'address2': (str, 'N3.2'),

        'city': (str, 'N4.1'),
        'state': (str, 'N4.2'),
        'zip': (str, 'N4.3'),
        'country': (str, 'N4.4'),
        'country_name': (str, 'N4.6'),

        'email': (str, 'PER.4'),
        'phone': (str, 'PER.6'),
    }


class AddressAdditionalName(ediElementWithXpath):
    xpaths = {
        'additional_name': (str, 'N2.1'),
    }


class Line(ediElementWithXpath):
    price_code_mapping = {
        'NT': 'Net',
    }

    uom_mapping = {
        'EA': 'Each',
    }

    xpaths = {
        'id': (str, 'PO1.1'),
        'qty': (int, 'PO1.2'),
        'name': (str, 'MSG->0.1'),
        'gift_message': (str, 'MSG->1.1'),

        'cost': (float, 'PO1.4'),
        'retail_price': (float, 'CTP.3'),

        'sku': (str, 'PO1.7'),
        'vendorSku': (str, 'PO1.7'),

        'additional_fields': {
            'uom': (map_type(uom_mapping), 'PO1.3'),
            'unit_price_code': (map_type(price_code_mapping), 'PO1.5'),

            'author_name': (str, 'PO1.9'),
            'product_type': (str, 'PO1.11'),
            'merchant_branding': (str, 'PO1.13'),
            'seller_of_record': (str, 'PO1.15'),
        },
    }


class Order(ediElementWithXpath):

    xpaths = {
        'poNumber': (str, 'BEG.3'),
        'order_id': (str, 'REF:OQ.2'),
        'external_date_order': ('date', 'BEG.5'),
        'carrier': (str, 'TD5.3'),

        'address': {
            'ship': (Address, 'N1<-TD5(N2,N3,N4)|2'),
            'invoice': (Address, 'N1<-PER(N2,N3,N4)|3'),
        },

        'lines': (Line, 'PO1<-@(MSG,CTP)'),

        'additional_fields': {
            'po_type_code': (str, 'BEG.2'),
            'status_messages': (N9Msg, 'N9<-MSG'),
            'bill_to_location': (str, 'N1:BT.2'),
            'vendor_warehouse': (str, 'N1:SF.2'),
            'currency': (str, 'CUR.2'),
            'ship_additional_name': (AddressAdditionalName, 'N1<-TD5(N2,N3,N4)|2'),
            'invoice_additional_name': (AddressAdditionalName, 'N1<-PER(N2,N3,N4)|3'),
        },
    }


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)
        self.Order = Order
