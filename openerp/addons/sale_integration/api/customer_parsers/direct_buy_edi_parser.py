from ..utils.ediparser import Segments
from element_with_xpath import xpathRule
from base_edi import (
    BaseEDIParser,
    ediElementWithXpath,
    AddressEdiElementWithXpath,
    address_type_map,
)

class map_type(object):

    def __init__(self, map_dict):
        super(map_type, self).__init__()
        self.map_dict = map_dict

class map_type_with_description(map_type):
    pass

class AddressXpathRule(xpathRule):
    __type_res__ = 'Address'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).dict)
        return result

class OriginalAddressesXpathRule(xpathRule):
    __type_res__ = 'original_addresses'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).get_original_dict())
        return result


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        return [Line(line).dict for line in res]

class PKGXpathRule(xpathRule):
    __type_res__ = 'PKG'

    def process_res(self, res, xpath):
        return [PKG(Segments([line])).description for line in res]


# class MTXXpathRule(xpathRule):
#     __type_res__ = 'MTX'
#
#     def process_res(self, res, xpath):
#         return [MTX(Segments([line])).data for line in res]


class AdviceAddressXpathRule(xpathRule):
    __type_res__ = 'AdviceAddress'

    def process_res(self, res, xpath):
        addresses = {}
        for line in res:
            address = AdviceAddress(Segments([line])).to_dict()
            addresses[address['target']] = address
        return addresses

class CustomEdiElementWithXpath(ediElementWithXpath):

    def __init__(self, segments):
        super(CustomEdiElementWithXpath, self).__init__(segments)
        self.custom_xpath_rules = {
            'Address': AddressXpathRule,
            'original_addresses': OriginalAddressesXpathRule,
            'Line': LineXpathRule,
            'PKG': PKGXpathRule,
            # 'MTX': MTXXpathRule,
            'AdviceAddress': AdviceAddressXpathRule,
        }
        self.inject_custom_xpath_rules()

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = super(CustomEdiElementWithXpath, self).__get_elem__(name, type_res, xpath)

        xpath = xpath or self.xpaths[name][1]
        type_res = type_res or self.xpaths[name][0]

        try:
            if isinstance(type_res, map_type):
                res = self.xpath_get(xpath)
                if type(type_res) == map_type_with_description:
                    result = {'original': res, 'description': type_res.map_dict.get(res, None)}
                else:
                    result = type_res.map_dict.get(res, None)
        except Exception:
            result = False
        return result


class Line(ediElementWithXpath):
    xpaths = {
        # Alphanumeric characters assigned for differentiation within a transaction set
        # This element will contain the purchase order item line number.
        'id': ('str', 'PO1.1'),
        # Numeric value of quantity
        'qty': ('int', 'PO1.2'),
        # Price per unit of product, service, commodity, etc.
        # If code in PO103 is "EA" unit price will be in eaches.
        # If code in PO103 is "CA" unit price will be in cases.
        'cost': ('float', 'PO1.4'),
        'merchantSKU': ('str', 'PO1.7'),
        'vendorSKU': ('str', 'PO1.7'),
        'vendorSku:': ('str', 'PO1:*:*:*:*:*:*:*:*:*:VA.11'),
        'upc': ('str', 'PO1.7'),
        'name': ('str', 'PID.2'),
        'retail_price': ('float', 'PO1.4'),
        'additional_fields': {
            'uom': ('str', 'PO1.3'),
            'unit_price_code': ('str', 'PO1.4'),
            # Code identifying the type/source of the descriptive number used in
            # Product/Service ID (234)
            # EN - EAN/UCC - 13
            # 'EN': ('str', 'PO1:*:*:*:*:*:EN.7'),
            # # Data structure for the 13 digit EAN.UCC (EAN
            # # International.Uniform Code Council) Global Trade
            # # Identification Number (GTIN)
            # # EO - EAN/UCC - 8
            # 'EO': ('str', 'PO1:*:*:*:*:*:EO.7'),
            # # Data structure for the 8 digit EAN.UCC (EAN
            # # International.Uniform Code Council) Global Trade
            # # Identification Number (GTIN)
            # # IB - International Standard Book Number (ISBN)
            # 'IB': ('str', 'PO1:|:|:|:|:|:IB.7'),
            # # UA - U.P.C./EAN Case Code (2-5-5)
            # 'UA': ('str', 'PO1:|:|:|:|:|:UA.7'),
            # # UK - GTIN 14-digit Data Structure
            # 'UK': ('str', 'PO1:|:|:|:|:|:UK.7'),
            # # Data structure for the 14 digit EAN.UCC (EAN
            # # International.Uniform Code Council) Global Trade Item
            # # Number (GTIN)
            # # UP - UCC - 12
            # # Data structure for the 12 digit EAN.UCC (EAN
            # # International.Uniform Code Council) Global Trade
            # # Identification Number (GTIN). Also known as the
            # # Universal Product Code (U.P.C.)
             'UP': ('str', 'PO1:*:*:*:*:*:UP.7'),
            # # Code identifying the type/source of the descriptive number used in
            # # Product/Service ID (234)
            # # PI - Purchaser's Item Code
            # 'PI': ('str', 'PO1:|:|:|:|:|:|:|:PI.9'),
            # # Code identifying the type/source of the descriptive number used in
            # # Product/Service ID (234)
            # # VA - Vendor's Style Number
            # 'VA': ('str', 'PO1:|:|:|:|:|:|:|:|:|:VA.11'),
            # # Code identifying the type/source of the descriptive number used in
            # # Product/Service ID (234)
            # # SK - Stock Keeping Unit (SKU)
            # 'SK': ('str', 'PO1:|:|:|:|:|:|:|:|:|:|:|:SK.13'),
            # # Code identifying the type/source of the descriptive number used in
            # # Product/Service ID (234)
            # # OT - Internal Number
            # 'OT': ('str', 'PO1:|:|:|:|:|:|:|:|:|:|:|:|:|:OT.15'),
            # # Promotional price
            # 'RPP': ('float', 'CTP:PRP.3'),
            # # Retail
            # 'RTL': ('float', 'CTP:RTL.3'),
            # The number of inner containers, or number of eaches if there are no inner
            # containers, per outer container
            'pack': ('int', 'PO4.1'),
            'VC': ('str', 'PO1:*:*:*:*:*:PO1.7'),
        },
    }


class Address(AddressEdiElementWithXpath):

    xpaths = {
        'type': ('str', 'N1.1'),
        'name': ('str', 'N1.2'),
        'number': ('str', 'N1.3'),
        'last_name': ('str', 'N2.1'),
        'middle_name': ('str', 'N2.2'),
        'address1': ('str', 'N3->0.1'),
        'address2': ('str', 'N3->0.2'),
        'address3': ('str', 'N3->1.1'),
        'address4': ('str', 'N3->1.2'),
        'city': ('str', 'N4.1'),
        'state': ('str', 'N4.2'),
        'zip': ('str', 'N4.3'),
        'country': ('str', 'N4.4'),
        'phone': ('str', 'PER:RE:*:TE.4'),
        'email': ('str', 'PER:RE:*:TE:*:EM.8'),
        # D-U-N-S+4, D-U-N-S Number with Four Character
        # Suffix
        ########'DUNS': ('str', 'N1:|:|:9.4'),
        # Assigned by Buyer or Buyer's Agent
        # AAFES 4 digit alpha/numeric facility number or 7 digit
        # numeric facility number
        'facility_number': ('str', 'N1:*:*:92.4'),
        # Global Location Number (GLN)
        # A globally unique 13 digit code for the identification of a
        # legal, functional or physical location within the Uniform
        # Code Council (UCC) and International Article Number
        # Association (EAN) numbering system
        'GLN': ('str', 'N1:*:*:UL.4'),
     }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res_type = address_type_map.get(res['type'], 'order')
        del res['type']
        country = res['country'] or 'US'
        if country == 'GE':
            country = 'DE'

        result = {
            res_type: {
                'name': ' '.join(
                    [
                        value for key, value in res.items()
                        if key in ['name', 'last_name', 'middle_name'] and value
                    ]
                ),
                'address1': res['address1'],
                'address2': res['address2'],
                # TODO: need to store it or use?
                # 'address3': res['address3'],
                # 'address4': res['address4'],
                'city': res['city'],
                'state': res['state'],
                'zip': res['zip'],
                'country': country,
                'phone': res['phone'],
                'email': res['email'],
            }
        }
        return result


class PKG(ediElementWithXpath):

    xpaths = {
        'description': ('str', 'PKG.5'),
    }


# class MTX(ediElementWithXpath):
#
#     xpaths = {
#         'data': ('str', 'MTX.2'),
#     }


class AdviceAddress(ediElementWithXpath):

    xpaths = {
        'target': ('str', 'N1.1'),
        'contact': ('str', 'N1:*.2'),
        'duns': ('str', 'N1:FR:*:1.4'),
        'duns_4': ('str', 'N1:FR:*:9.4'),
        'buyer_code': ('str', 'N1:FR:*:92.4'),
    }


class Order(CustomEdiElementWithXpath):

    sales_requirements_map = {
        'N': 'No Back Order',
        'Y': 'Back Order if Out of Stock',
    }
    #
    terms_type_code_map = {
        '01': 'Basic',
        '02': 'End of Month (EOM)',
        '03': 'Fixed Date',
        '04': 'Deferred or Installment',
        '05': 'Discount Not Applicable',
        '06': 'Mixed',
        '07': 'Extended',
        '08': 'Basic Discount Offered',
        '09': 'Proximo',
        '11': 'Elective',
        '12': '10 Days After End of Month (10 EOM)',
        '14': 'Previously agreed upon'
    }

    # Transportation Method/Type Code
    transportation_type_code_map = {
        'A': 'Air',
        'AF': 'Air Freight',
        'B': 'Barge',
        'BP': 'Book Postal',
        'C': 'Consolidation',
        'D': 'Parcel Post',
        'E': 'Expedited Truck',
        'FL': 'Motor (Flatbed)',
        'H': 'Customer Pickup',
        'I': 'Common Irregular Carrier',
        'L': 'Contract Carrier',
        'LT': 'Less Than Trailer Load (LTL)',
        'M': 'Motor (Common Carrier)',
        'O': 'Containerized Ocean',
        'P': 'Private Carrier',
        'Q': 'Conventional Ocean',
        'R': 'Rail',
        'S': 'Ocean',
        'SE': 'Sea/Air',
        'T': 'Best Way (Shippers Option)',
        'U': 'Private Parcel Service',
        'X': 'Intermodal (Piggyback)',
        'ZZ': 'Mutually defined'
    }
    identificier_code_organixation = {
        'BT': 'Bill-to-Party',
        'ST': 'Ship To'
    }


    xpaths = {
        # Purchase Order Number
        'poNumber': ('str', 'BEG:00:DS.3'),
        'order_id': ('str', 'BEG:00:DS.3'),
        # Date expressed as CCYYMMDD where CC represents the first two digits of
        # the calendar year
        'external_date_order': ('datetime', 'BEG:00:DS.5'),
        'address': ('Address', 'N1<-N4(N2,N3)'),
        'carrier': ('static', 'TD5.2'),
        'additional_fields': {
            # Internal Vendor Number
            'IA': ('str', 'REF:IA.2'),
            # # Vendor Order Number
            'VN': ('str', 'REF.1'),#REF:VN.2
            # Reference information as defined for a particular Transaction Set or as
            # specified by the Reference Identification Qualifier
            'AH': ('str', 'N9.1'),#N9:AH.2
            # Code (Standard ISO) for country in whose currency the charges are specified
            'currency': ('str', 'CUR:SE.2'),#CUR:SE.2
            'administrative_communications_contact': {
                'name': ('str', 'PER.2'),#PER:BD.2
                'phone': ('str', 'PER.4'),#PER:BD:|:TE.4
            },
            'sales_requirements': ('str', 'CSH.1'),#CSH.1 #map_type(sales_requirements_map)
            # Terms of Sale/Deferred Terms of Sale
            'terms_of_sale': {
                # Code identifying type of payment terms
                'terms_type_code': (map_type_with_description(terms_type_code_map), 'ITD.1'),
                # Terms discount percentage, expressed as a percent, available to the purchaser if
                # an invoice is paid on or before the Terms Discount Due Date
                'terms_discount_percent': ('float', 'ITD.3'),#
                # Date payment is due if discount is to be earned expressed in format
                # CCYYMMDD where CC represents the first two digits of the calendar year
                'terms_discount_due_date': ('datetime', 'ITD.4'),#
                # Number of days in the terms discount period by which payment is due if terms
                # discount is earned
                'terms_discount_days_due': ('datetime', 'ITD.5'),#
                # Date when total invoice amount becomes due expressed in format
                # CCYYMMDD where CC represents the first two digits of the calendar year
                'terms_net_due_date': ('datetime', 'ITD.6'),#
                # Number of days until total invoice amount is due (discount not applicable)
                'terms_net_days': ('int', 'ITD.7'),#
                # A free-form description to clarify the related data elements and their content
                'description': ('str', 'ITD.12'),#
            },

            'date_time_reference': {
                'delivery_requested': ('datetime', 'DTM:002.2'),#
                'requested_ship': ('datetime', 'DTM:010.2'),#
                'promotion_order_start': ('datetime', 'DTM:023.2'),#
                'ship_no_later': ('datetime', 'DTM:038.2'),#
                'requested_for_delivery_week_of': ('datetime', 'DTM:077.2'),#
            },
            'PKG': ('PKG', 'PKG:F:10'),#
            'carrier_details': {
                 'identeficate_code_qualifer': ('str', 'TD5.2'),#
                 'commodity_code': ('str', 'TD5.3'),
                 'transportation_type_code': ('str', 'TD5.4'),#'transportation_type_code': (map_type(transportation_type_code_map), 'TD5.4'),
                 'routing': ('str', 'TD5.5'),
            },
            'MTX': ('MTX', 'MTX'),
            'addresses': ('original_addresses', 'N1<-N4(N2,N3)'),#N1:ST:|:92.4
            'facility_number': ('str', 'BEG:00:DS.3'),#N1:ST:|:92.4
            'DUNS': ('str', 'BEG:00:DS.3'),#N1:|:|:9.4
            'ship_to_country': ('str', 'N4.4'),
            'number_of_included_functional_group':('str', 'IEA.1'),
            'interchange_control_number': ('str', 'IEA.2'),
        },
        'lines': ('Line', 'PO1<-TC2(CTP,PID,PO4,SDQ)'),
    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        cd = res['additional_fields']['carrier_details']
        res.update({
            'carrier': ','.join(
                [
                    cd['commodity_code'] or '',
                    cd['transportation_type_code'] or '',
                    cd['routing'] or ''
                ]
            )
        })
        return res


class CancelOrder(Order):

    cancellation_map = {
        '01': True,
    }

    change_map = {
        '04': True,
    }

    po_type_code_map = {
        'PR': 'Promotion',
        'RL': 'Release or Delivery Order',
        'SA': 'Stand-alone Order',
        'ZZ': 'Mutually Defined',
}

    def __init__(self, input_segments):
        super(CancelOrder, self).__init__(input_segments)
        self.xpaths.update({
            'order_id': ('str', 'BCH.3'),
            'cancellation': (map_type(self.cancellation_map), 'BCH.1'),
            'change': (map_type(self.change_map), 'BCH.1'),
            'po_type_code': (map_type(self.po_type_code_map), 'BCH.2'),
            'po_number': ('str', 'BCH.3'),
            # Date of original purchase order number issued by AAFES.
            'original_date': ('datetime', 'BCH.6'),
            # Date of change to purchase order issued by AAFES.
            'change_date': ('datetime', 'BCH.11'),
            # 'IA': {
            #     'internal_vendor_number': ('str', 'REF:IA.2'),
            #     'description': ('str', 'REF:IA.3'),
            # },
            # 'administrative_communications_contact': {
            #     'name': ('str', 'PER:BD.2'),
            #     'phone': ('str', 'PER:BD:|:TE.4'),
            # },
        })


class AppAdvice(CustomEdiElementWithXpath):

    result_codes = {
        'IR': 'Item Reject',
        'TA': 'Transaction Set Accept',
        'TE': 'Transaction Set Accept with Error',
        'TR': 'Transaction Set Reject'
    }

    reference_codes = {
        '08': 'Carrier Assigned Package Identification Number',
        'A': 'Reference number assigned by a carrier to uniquely identify a single package',
        '1X': 'Credit or Debit Adjustment Number',
        '2F': 'Consolidated Invoice Number',
        'BM': 'Bill of Lading Number',
        'CN': 'Carrier\'s Reference Number(PRO / Invoice)',
        'IV': 'Seller\'s Invoice Number',
    }

    error_condition_codes = {
        '007': 'Missing Data',
        'POI': 'Purchase Order Number Invalid',
        'MF': 'Missing or Invalid Internal Vendor Number',
        '848': 'Incorrect Data',
        '012': 'Invalid Combination',
        '010': 'Total Out Of Balance',
    }

    xpaths = {

        'message_id': ('str', 'BGN:00.2'),  # Transaction ref
        'date': ('str', 'BGN:00:*.3'),
        'address': ('AdviceAddress', 'N1'),
        'result': (map_type(result_codes), 'OTI.1'),
        'ref_obj': (map_type(reference_codes), 'OTI.2'),
        'ref_code': ('str', 'OTI.3'),
        'transaction_set_identifier_code': ('str', 'OTI.10'),  # Transaction set identifier code
        'error_code': (map_type(error_condition_codes), 'TED.1'),
        'error_details': ('str', 'TED.2'),

    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res.update({
            'functional_group': 'AG',  # FIXME: Need parse this vlue from file
            'message_text': ' '.join(filter(
                None, [
                    res['result'],
                    res['ref_obj'],
                    res['ref_code'],
                ]
            ))
        })
        return res


class DirectBuyEDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(DirectBuyEDIParser, self).__init__(parent)
        self.Order = Order
        self.CancelOrder = CancelOrder
        self.AppAdvice = AppAdvice
