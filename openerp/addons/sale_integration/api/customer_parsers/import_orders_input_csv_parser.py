# -*- coding: utf-8 -*-
import logging
from csv import DictReader
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.sale_integration.api.customer_parsers.exceptions import CSVPparserError, CSVLineError

_logger = logging.getLogger(__name__)


class CSVPparser():

    def __init__(self, csv_dialect, ids, main_header_data):
        self.ids = ids
        self._collect_fieldnames()
        self.csv_dialect = csv_dialect
        self.main_header_data = main_header_data
        so_type = main_header_data.get('sotype', '').strip().lower()
        if(so_type in ['flash', 'bulk']):
            self.so_type = so_type
        else:
            self.so_type = 'regular'

    def _collect_fieldnames(self):
        self.fieldnames = []
        for name_id in self.ids:
            self.fieldnames.append(self.ids[name_id])
        return True

    def _get_value(self, line, name_column, default_value=False):
        result = line.get(name_column, default_value)
        if(isinstance(result, (unicode, str))):
            result = result.strip()
        return result

    def _get_order_obj(self, line):
        order_obj = {
            'order_id': self._get_value(line, self.ids['order_id'], False),
            'carrier': self._get_value(line, self.ids['ship_code'], False),
            'partner_id': self.main_header_data['customer'],
            'so_type': self.so_type,
            'flash': False,
            'bulk_transfer': False,
            'address':
            {
                'ship':
                    {
                        'name': self._get_value(line, self.ids['ship_name'], ''),
                        'address1': self._get_value(line, self.ids['ship_address1'], ''),
                        'address2': self._get_value(line, self.ids['ship_address2'], ''),
                        'city': self._get_value(line, self.ids['ship_city'], ''),
                        'state': self._get_value(line, self.ids['ship_state'], ''),
                        'zip': self._get_value(line, self.ids['ship_zip'], ''),
                        'country': self._get_value(line, self.ids['ship_country'], ''),
                        'phone': self._get_value(line, self.ids['ship_phone'], ''),
                    },
            },
            'lines': [],
        }

        if 'order_comment' in self.ids:
            order_obj['order_comment'] = self._get_value(line, self.ids['order_comment'], '')
        if(order_obj['so_type'] == 'flash'):
            order_obj['flash'] = True
        elif(order_obj['so_type'] == 'bulk'):
            order_obj['bulk_transfer'] = True
        return order_obj

    def _get_line_obj(self, line):

        line_obj = {
            'id': self._get_value(line, self.ids.get('line_item_number', False), False),
            'delmar_id': self._get_value(line, self.ids['line_delmar_id'], False),
            'customer_sku': self._get_value(line, self.ids['line_customer_sku'], False),
            'name': self._get_value(line, self.ids['line_description'], False),
            'cost':  self._get_value(line, self.ids['line_cost'], False),
            'qty': self._get_value(line, self.ids['line_qty'], 1),
            'size': self._get_value(line, self.ids['line_size'], False),
            'sku': False,
            'retail_cost': self._get_value(line, self.ids['line_retail'], 0),
            'ship_cost': self._get_value(line, self.ids['line_ship_cost'], False),
            'combine': self._get_value(line, self.ids.get('combine', 'combine'), True),
            'gift_message': self._get_value(line, self.ids.get('gift_message','gift_message'), False),
        }

        line_delmar_id_without_size = self.ids.get('line_delmar_id_without_size', False)
        if (line_delmar_id_without_size):
            delmar_id_without_size = self._get_value(line, line_delmar_id_without_size, False)
            if (delmar_id_without_size):
                line_obj.update({'delmar_id': delmar_id_without_size})

        for name_value in ['cost', 'retail_cost', 'ship_cost']:
            if(line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'float', default=None)

        for name_value in ['id', 'qty']:
            if(line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'int')

        if(
            line_obj['delmar_id'] and
            ('/' in line_obj['delmar_id']) and
            not(line_obj['size'])
            and not line_delmar_id_without_size
        ):
            try:
                _sku = line_obj['delmar_id']
                _size = _try_parse(_sku[_sku.find("/") + 1:], 'float')
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass
        else:
            line_obj['sku'] = line_obj['delmar_id']
        return line_obj

    def _add_line_to_order_obj(self, order_obj, _line):
        line_obj = self._get_line_obj(_line)
        add_new_line = True

        if line_obj['combine']:

            for line in order_obj['lines']:

                if line.get('combine'):
                    if (isinstance(line['combine'], (str, unicode)) and line['combine'].lower() in ('0', 'false', 'n')) or line['combine']:
                        continue

                condition = False
                if(not line['delmar_id']):
                    if(line['customer_sku']):
                        condition = (line['customer_sku'] == line_obj['customer_sku'])
                        if(line['size']):
                            condition = (condition and(line['size'] == line_obj['size']))
                else:
                    condition = (line['delmar_id'] == line_obj['delmar_id'])
                    if(line['size']):
                        condition = (condition and(line['size'] == line_obj['size']))
                if(condition):
                    if(isinstance(line_obj['qty'], int)):
                        line['qty'] += line_obj['qty']
                    else:
                        line['qty'] += 1
                    add_new_line = False
                    break
        if(add_new_line):
            if(not line_obj['id']):
                line_obj['id'] = len(order_obj['lines']) + 1
                order_obj['lines'].append(line_obj)
        return order_obj

    def _get_lines_from_csv(self, _data):
        lines = []
        _pseudo_file = IO(_data)
        dr = DictReader(_pseudo_file, dialect=self.csv_dialect)
        # next(dr)
        for row in dr:
            if bool([v for v in row.values() if v]):
                lines.append(row)
        return lines

    def parse_file(self, csv_data):
        result = {
            'errors': {
                'bad_format': [],
                'bad_lines': {},
            },
            'orders_list': []
        }
        ids = self.ids
        list_of_orders_obj = {}
        try:
            csv_lines = self._get_lines_from_csv(csv_data)
            _orders = {}
            _count = 3
            # collective csv lines for all orders
            for _line in csv_lines:
                _count += 1
                _order_id = self._get_value(_line, ids['order_id'], False)
                if(not _order_id):
                    raise CSVLineError(_count, 'CSVPparserError: bad line: {0}, not find order id'. format(_count))
                if(not _orders.get(_order_id, False)):
                    _orders[_order_id] = []
                _orders[_order_id].append(_line)
            # parse orders
            for order_id in _orders:
                for line in _orders[order_id]:
                    if(not list_of_orders_obj.get(order_id, False)):
                        list_of_orders_obj[order_id] = self._get_order_obj(line)
                    list_of_orders_obj[order_id] = self._add_line_to_order_obj(list_of_orders_obj[order_id], line)
            bad_format = result['errors']['bad_format']
            # collect xml orders list
            if(not bad_format):
                yaml_obj = YamlObject()
                for order_id in list_of_orders_obj:
                    if(not result['errors']['bad_lines']):
                        result_order_obj = {
                            'name': order_id,
                            'xml': yaml_obj.serialize(_data=list_of_orders_obj[order_id])
                        }
                        result['orders_list'].append(result_order_obj)
        except CSVLineError as csv_line_err:
            if(not result['errors']['bad_lines'].get(csv_line_err.line_count, False)):
                result['errors']['bad_lines'][csv_line_err.line_count] = []
            result['errors']['bad_lines'][csv_line_err.line_count].append(csv_line_err.message)
        except CSVPparserError as csv_parser_err:
            result['errors']['bad_format'].append(csv_parser_err.message)

        return result
