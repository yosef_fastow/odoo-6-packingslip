# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import dateutil.parser

TECHNICAL_ERROR_DESCRIPTION = {
    'P': 'Missing or Invalid Item Quantity',
    'Q': 'Missing or Invalid Item Identification',
    'T': 'Unauthorized Transaction Set Purpose Code',
    'U': 'Missing or Unauthorized Transaction Type Code',
    'V': 'Missing or Unauthorized Action Code',
    'MB': 'Missing or Invalid Purchase Order Number',
    'MF': 'Missing or Invalid Internal Vendor Number',
    'MI': 'Missing or Invalid SCAC',
    '006': 'Duplicate',
    '007': 'Missing Data',
    '009': 'Invalid Date',
    '011': 'Not Matching',
    '012': 'Invalid Combination',
    '024': 'Other Unlisted Reason Error',  # The reason for the application error condition cannot be described using any other code on the standard code list
    '107': 'Missing or Invalid Location',
    '110': 'Missing Marking Identification at Pack Level',
    '133': 'Item Not Found On Purchase Order',
    'IID': 'Invalid Identification Code',
    'IQT': 'Invalid Quantity',
    'MID': 'Missing Identification Code',
    'OTH': 'Unspecified application error',  # Other
    'POI': 'Purchase Order Number Invalid',
}

class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        self.delimiters = '*'
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(self.delimiters)
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {}
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '824':  # Application Advice
                        text_messages = self.parse_application_advice_data(message)
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':
                    ordersObj['additional_fields'].update({
                        elements[1]: elements[2]
                    })
                elif elements[0] == 'ITD':
                    terms_type_code = {
                        '02': 'End of Month (EOM)',
                        '05': 'Discount Not Applicable',
                        '08': 'Basic Discount Offered',
                    }
                    terms_basis_date_code = {
                        '3': 'Invoice Date',
                        '7': 'Effective Date',
                        '15': 'Receipt of Goods',
                    }
                    ordersObj['additional_fields'].update({
                        'terms_type_code': terms_type_code.get(elements[1], elements[1]),
                        'terms_basis_date_code': terms_basis_date_code.get(elements[2], elements[2]),
                        'terms_discount_percent': elements[3],
                        'terms_Discount_days_due': elements[5],
                        'terms_net_days': elements[7],
                    })
                elif elements[0] == 'DTM':
                    if elements[1] == '001':  # Delivery Requested
                        ordersObj['additional_fields'].update({
                            'cancel_after': elements[2]
                        })
                    elif elements[1] == '010':  # Schedule
                        ordersObj['additional_fields'].update({
                            'schip_date': elements[2]
                        })

                elif elements[0] == 'MTX':
                    ordersObj['additional_fields'].update({
                        'MTX': ordersObj['additional_fields'].get('MTX', '')+' '+elements[2]
                    })


                elif elements[0] == 'PO1':

                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'unit_price': elements[4],
                        'sku': elements[9],
                        'vendorSku': elements[9],
                        'customer_id_delmar': elements[9],
                        'upc': elements[7],
                        'merchantSKU': elements[7],
                        'customer_sku': elements[9],
                        'optionSku': elements[9],
                        'name': elements[9],
                        'poLineData': {},
                        'additional_fields': {}
                    }

                    if (elements[9]):
                        lineObj['additional_fields'].update({
                            elements[8]: elements[9]
                        })
                        lineObj['poLineData'][elements[8]] = elements[9]
                    if (elements[6]):
                        lineObj['additional_fields'].update({
                            elements[6]: elements[7]
                        })
                    ordersObj['lines'].append(lineObj)
                elif elements[0] == 'CTP':
                    ordersObj['lines'][len(ordersObj['lines']) - 1].update({
                        'retail_cost': elements[3],
                        'customerCost': elements[3],
                    })
                elif elements[0] == 'PID':
                    if elements[2] == '08':
                        lineObj['name'] = elements[5]
                    if elements[2] == '75':
                        lineObj['color'] = elements[5]
                        lineObj['poLineData']['prodColor'] = elements[5]
                    if elements[2] == '91':
                        lineObj['description'] = elements[5]
                        lineObj['poLineData']['prodSize'] = elements[5]
                elif elements[0] == 'SAC':
                    ordersObj['poHdrData'].update({
                        'gift_message': elements[15] if 15 < len(elements) else '',
                        'giftIndicator': 'y'
                    })

                elif elements[0] == 'SDQ':
                    ordersObj['poHdrData'].update({
                        'store_number': elements[3],
                        'qty_ordered': elements[4]
                    })
                    lineObj['poLineData']['store_number'] = elements[3]
                    lineObj['poLineData']['qty_ordered'] = elements[4]

                elif elements[0] == 'N1':

                    if elements[1] in ['BT']:  # Bill-to-Party, Vendor
                        address_type = 'order'
                        ordersObj['address']['order'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['order']['name'] = elements[2]

                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address']['ship'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }

                        ordersObj['address']['ship']['name'] = elements[2]

                elif elements[0] == 'SE':
                    orders.append(ordersObj)
                elif elements[0] == 'GE':
                    ordersObj['number_of_transaction'] = self.number_of_transaction = elements[1]
                elif elements[0] == 'IEA':
                    ordersObj['additional_fields'].update({
                        'number_of_included_functional_group': elements[1],
                        'interchange_control_number': elements[2],
                    })

        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result

    def parse_application_advice_data(self, message):
        text_messages = []
        try:
            text_messageObj = {
                'header': '',
                'trans_no': '',
                'message_id': '',
                'message_subject': '',
                'message_text': [],
                'trans_code': '',
                'ack_control_number': self.ack_control_number,
                'functional_group': self.functional_group,
            }
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'ST':
                    text_messageObj = {
                        'header': '',
                        'trans_no': '',
                        'message_id': '',
                        'message_subject': '',
                        'message_text': [],
                        'trans_code': '',
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                    }
                elif elements[0] == 'BMG':
                    text_messageObj['header'] = elements[2]
                elif elements[0] == 'REF':
                    if elements[1] == 'IA':
                        text_messageObj['message_id'] = elements[2]
                elif elements[0] == 'OTI':
                    text_messageObj['trans_no'] = elements[3]
                    if len(elements) > 10:
                        text_messageObj['trans_code'] = elements[10]
                elif elements[0] == 'TED':
                    ted_code = elements[1]
                    text_messageObj['message_subject'] = TECHNICAL_ERROR_DESCRIPTION.get(ted_code, 'Error')
                    if 2 < len(elements) <= 7:
                        tech_error = elements[2]
                    elif len(elements) > 7:
                        tech_error = elements[2] + ': ' + elements[7]
                    text_messageObj['message_text'].append(tech_error)
                    # DLMR-556
                    self._append_error("INVALID_DATA_824:{}:{}".format(text_messageObj['trans_no'], tech_error))
                elif elements[0] == 'NTE':
                    text_messageObj['message_text'].append(elements[2])
                elif elements[0] == 'SE':
                    text_messages.append(text_messageObj)
                elif elements[0] == 'GE':
                    self.number_of_transaction = elements[1]
                    text_messageObj['number_of_transaction'] = self.number_of_transaction
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        return text_messages