from ..utils import xmlutils as xml_utils


class ZalesApiGetOrderObj(object):

    def getOrderObj(self, xml):
        ordersObj = {}

        if(xml == ""):
            return ordersObj
        order = xml_utils.XmlUtills(xml).to_obj()
        if(order is not None):
            ordersObj['partner_id'] = order.get('merchant_id', {}).get('value', '')
            ordersObj['order_id'] = order.get('order_no', {}).get('value', False)
            ordersObj['poNumber'] = order.get('po_no', {}).get('value', False)
            ordersObj['Comments'] = [{'CommentType': 'OL', 'Text': ''}]
            ordersObj['store_number'] = order.ship.get('store_number', {}).get('value', False)
            ordersObj['carrier'] = order.ship.get('ship_method', {}).get('value', '')
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['name'] = order.ship.get('name', {}).get('value', '')
            ordersObj['address']['ship']['address1'] = order.ship.get('shipadd1', {}).get('value', '')
            ordersObj['address']['ship']['address2'] = order.ship.get('shipadd2', {}).get('value', '')
            ordersObj['address']['ship']['city'] = order.ship.get('city', {}).get('value', '')
            ordersObj['address']['ship']['state'] = order.ship.get('state', {}).get('value', '')
            ordersObj['address']['ship']['zip'] = order.ship.get('zip', {}).get('value', '')
            ordersObj['address']['ship']['country'] = order.ship.get('country', {}).get('value', '') and order.ship.get('country', {}).get('value')[0:2]
            ordersObj['address']['ship']['phone'] = order.ship.get('phone', {}).get('value', '')
            ordersObj['lines'] = []
            lines = order.lines if (isinstance(order.lines, list)) else [order.lines]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('item_line_number', {}).get('value', False)
                lineItemObj['vendorSku'] = lineItem.get('vendor_sku', {}).get('value', False)
                if not lineItemObj['vendorSku']:
                    lineItemObj['vendorSku'] = lineItem.get('vendorSku', {}).get('value', False)
                lineItemObj['merchantSKU'] = lineItem.get('merchant_sku', {}).get('value', False)
                lineItemObj['size'] = lineItem.get('size').get('value', False)
                lineItemObj['qty'] = lineItem.get('item_count', {}).get('value', False)
                lineItemObj['name'] = lineItem.get('description', {}).get('value', False)
                lineItemObj['cost'] = lineItem.get('unit_cost', {}).get('value', False)
                ordersObj['lines'].append(lineItemObj)
        return ordersObj
