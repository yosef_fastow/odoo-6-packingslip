from ..utils import xmlutils as xml_utils
import re
class WayfairApiGetOrdersXML(object):
    
    def parse_response(self, response):
        if(not response):
            return []
        obj_ordersList = []
        ordersList = []
        for obj in response:
            order_id_old = ""
            ordersObj = {}
            line_id = 0
            item = re.sub("\n$", "", obj).split('\n')
            for line in item:
                row = line.split('|')
                if row[0] == 'IH':
                    if not ordersObj.get('order_id', False):
                        ordersObj['order_id'] = row[1][2:]
                    elif(ordersObj.get('order_id', False) and order_id_old != row[1][2:]):
                        order_id_old = ""
                        obj_ordersList.append(ordersObj)
                        ordersObj = {}
                        ordersObj['order_id'] = row[1][2:]

                    if(ordersObj.get('order_id', False) and order_id_old != ordersObj.get('order_id', False)):
                        
                        ordersObj['po_number'] = row[1]
                        ordersObj['external_date_order'] = row[2]
                        ordersObj['address'] = {}
                        ordersObj['address']['ship'] = {}
                        ordersObj['address']['ship']['name'] = row[3]
                        ordersObj['address']['ship']['address1'] = row[4]
                        ordersObj['address']['ship']['address2'] = row[5]
                        ordersObj['address']['ship']['city'] = row[6]
                        ordersObj['address']['ship']['state'] = row[7]
                        ordersObj['address']['ship']['zip'] = row[8]
                        ordersObj['address']['ship']['country'] = row[9]
                        ordersObj['address']['ship']['phone'] = row[10]
        
                        order_id_old = ordersObj['order_id']
                
                if row[0] == 'ID':
                    ordersObj['lines'] = []
                    itemsObj = {}
                    line_id += 1
                    itemsObj['id'] = line_id
                    itemsObj['qty'] = row[2]
                    itemsObj['cost'] = row[3]
                    itemsObj['name'] = row[4]
                    ordersObj['carrier'] = row[5]
                    itemsObj['custom_comment'] = row[6] or False
                    itemsObj['customer_sku'] = row[1]
                    if len(row[1].split('/')) == 2:
                        itemsObj['size'] = row[1].split('/')[1]
                    ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        for order in obj_ordersList:
            ordersObj = {}
            ordersObj['xml'] = xml_utils.XmlUtills(order, True).to_xml()
            ordersObj['name'] = order['order_id']
            ordersList.append(ordersObj)
        return ordersList