# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from element_with_xpath import CSVElementWithXpath, xpathRule


class BaseCSVParser(BaseParser):

    def __init__(self, parent):
        super(BaseCSVParser, self).__init__(parent)

    def split_csv_by_order(self, csv_data, identity):
        identity = identity.strip().lower().replace(' ', '')
        csv_data = strip_lower_del_spaces_from_header(csv_data, 'excel-tab')
        lines = []
        _pseudo_file = IO(csv_data)
        dr = DictReader(_pseudo_file, dialect='excel-tab')
        for row in dr:
            lines.append(row)
        orders = {}

        for line in lines:
            if line[identity] not in orders:
                orders[line[identity]] = []
            orders[line[identity]].append(line)
        return orders


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        xpath = xpath.strip().lower().replace(' ', '')
        keys = xpath.split('|')
        return [Line([{x: y for x, y in line.items() if x in keys}]).dict for line in res]


class Line(CSVElementWithXpath):

    xpaths = {
        'id': ('str', ''),
        'external_customer_line_id': ('str', ''),
        'sku': ('str', 'SKU'),
        'merchantSKU': ('str', 'SKU'),
        'vendorSku': ('str', 'VENDOR_SKU'),
        'name': ('str', 'PRODUCT_TITLE'),
        'qty': ('int', 'QUANTITY'),
        'customerCost': ('float', 'RETAIL_PRICE'),
        'additional_fields': {
            'retai_tax': ('float', 'RETAIL_TAX'),
            'promo_discount': ('float', 'PROMO_DISCOUNT'),
        }
    }


class Order(CSVElementWithXpath):

    xpaths = {
        'order_id': ('str', 'ORDER_NUMBER'),
        'po_number': ('str', 'ORDER_NUMBER'),
        'address': {
            'ship': {
                'first_name': ('str', 'SHIPPING_FIRSTNAME'),
                'last_name': ('str', 'SHIPPING_LASTNAME'),
                'company': ('str', 'SHIPPING_COMPANY'),
                'address1': ('str', 'SHIPPING_ADDRESS1'),
                'address2': ('str', 'SHIPPING_ADDRESS2'),
                'city': ('str', 'SHIPPING_CITY'),
                'state': ('str', 'SHIPPING_STATE'),
                'zip': ('str', 'SHIPPING_ZIP'),
                'country': ('str', 'SHIPPING_COUNTRY'),
                'email': ('str', 'EMAIL_ADDRESS'),
            },
        },
        'carrier': ('str', 'SHIPPING_CODE'),
        'additional_fields': {
            'billing_firstname': ('str', 'BILLING_FIRSTNAME'),
            'billing_lastname': ('str', 'BILLING_LASTNAME'),
            'billing_phone': ('datetime', 'BILLING_PHONE'),
            'trans_id': ('str', 'TRANS_ID'),
        },
        'lines': ('Line', 'SKU|QUANTITY|RETAIL_PRICE|RETAIL_TAX|PROMO_DISCOUNT|VENDOR_SKU|PRODUCT_TITLE'),
    }

    def __init__(self, csv_lines):
        super(Order, self).__init__(csv_lines)
        self.custom_xpath_rules = {
            'Line': LineXpathRule,
        }
        self.inject_custom_xpath_rules()

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        ship_addr = res['address']['ship']
        ship_addr['name'] = ship_addr['first_name'] or ''
        if ship_addr['last_name']:
            ship_addr['name'] += ' ' + ship_addr['last_name']
        del ship_addr['first_name']
        del ship_addr['last_name']
        del ship_addr['company']
        for i, line in enumerate(res['lines']):
            line['id'] = line['external_customer_line_id'] = i + 1
        return res


class CSVParser(BaseCSVParser):

    def __init__(self, parent):
        super(CSVParser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, csv_data):
        orders = self.split_csv_by_order(csv_data, 'ORDER_NUMBER')
        for order_name, order_data in orders.items():
            self.orders.append(Order(order_data).dict)
        return True
