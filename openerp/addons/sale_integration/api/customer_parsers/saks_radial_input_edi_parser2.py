# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        type_n1 = ''
        gift_message = ''
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(message.delimiters[1])
                print elements
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    self.sender = 'saks'
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {}
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['po_number'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':
                    if (elements[1] in ['19','4N','IA','CO','PD','W9']):
                        if elements[1] == '19':
                            ordersObj['additional_fields']['division_identifier'] = elements[2]
                            if elements[2] == 'SAKS':
                                ordersObj['additional_fields']['brand'] = 'Saks 5th Avenue order'
                            if elements[2] == 'OFF5':
                                ordersObj['additional_fields']['brand'] = 'Off 5th order'
                        elif (elements[1] == '4N'):
                            ordersObj['additional_fields']['special_payment_reference_number'] = elements[2]
                        elif (elements[1] == 'IA'):
                            ordersObj['additional_fields']['internal_vendor_number'] = elements[2]
                        elif (elements[1] == 'CO'):
                            ordersObj['order_id'] = ordersObj['external_customer_order_id'] = elements[2]
                        elif (elements[1] == 'PD'):
                            ordersObj['additional_fields']['promotion_number'] = elements[2]
                        elif (elements[1] == 'W9'):
                            gift_map = {
                                '01': 'y',
                                '02': 'n',
                                '03': 'n'
                            }
                            #TODO “03” identifies an international order
                            ordersObj['poHdrData'].update({
                                'giftIndicator': gift_map[elements[2]]
                            })
                    else:
                        ordersObj['additional_fields'].update({
                            elements[1]: elements[2]
                        })
                elif elements[0] == 'DTM':
                    if elements[1] == '010':
                        ordersObj['external_date_order'] = elements[2]
                        ordersObj['additional_fields']['vendor_guide'] = elements[2]
                elif elements[0] == 'TD5':
                    ship_method_codes = {
                        'FED1': 'FedEx Overnight',
                        'FEDH': 'FedEx Home',
                        'FED2': 'FedEx 2 Day',
                        'FEDG': 'FedEx Ground',
                        'USPP': 'USPS Priority',
                        'FEDS': 'FedEx Saturday',
                        'SPEC': 'Special Freight',
                        '2': 'Standard'
                    }
                    ordersObj['carrier'] = ship_method_codes[elements[3]]
                    ordersObj['ship_service'] = elements[5]
                    ordersObj['ship_method'] = elements[5]
                    ordersObj['additional_fields']['carrier_code'] = elements[3]
                elif elements[0] == 'N9':
                    if elements[1] == 'E9':
                        ordersObj['additional_fields']['attachment_code'] = False
                        ordersObj['additional_fields']['readable_transaction_id'] = False
                    if elements[1] == 'LA':
                        ordersObj['additional_fields']['shipping_label_serial_number '] = False
                        ordersObj['additional_fields']['transaction_id_number'] = False
                    if elements[1] == 'TOC':
                        gift_message = elements[2]
                    type_n1 = elements[1]
                elif elements[0] == 'MSG':
                    if gift_message and type_n1 == 'TOC':
                        gift_message = gift_message + ' ' + elements[1]
                    if type_n1 == 'E9' and not ordersObj['additional_fields'].get('readable_transaction_id'):
                        ordersObj['additional_fields']['readable_transaction_id'] = elements[1]
                    if type_n1 == 'LA' and not ordersObj['additional_fields'].get('transaction_id_number'):
                        ordersObj['additional_fields']['transaction_id_number'] = elements[1]

                elif elements[0] == 'N1':
                    if elements[1] == 'BT':
                        address_type = 'order'
                        ordersObj['address'][address_type] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address'][address_type]['name'] = elements[2]
                    if elements[1] == 'ST':
                        address_type = 'ship'
                        ordersObj['address'][address_type] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address'][address_type]['carrier'] = ordersObj['carrier']
                        ordersObj['address'][address_type]['ship_service'] = ordersObj['ship_method']
                        ordersObj['address'][address_type]['name'] = elements[2]
                elif elements[0] == 'N2':
                    ordersObj['address'][address_type]['company'] = elements[1]
                elif elements[0] == 'N3':
                    if ordersObj['address'][address_type]['address1']:
                        ordersObj['address'][address_type]['address2'] = elements[1]
                    else:
                        ordersObj['address'][address_type]['address1'] = elements[1]
                        if len(elements) > 2:
                            ordersObj['address'][address_type]['address1'] = elements[1] + ' ' + elements[2]
                elif elements[0] == 'N4':
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PER':
                    addr_types_map = {
                        'OC': 'invoice',  # Order Contact
                        'RE': 'ship',  # Receiving Contact
                    }
                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        if addr:
                            addr.update({
                                'name': elements[2],
                                'phone': elements[4]
                            })
                elif elements[0] == 'PO1':

                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'customerCost': 0,
                        'retail_cost': 0,
                        'additional_fields': {},
                        'size': ''
                        # 'poLineData': {}
                    }
                    for i in (6, 8, 10):
                        if len(elements) > i:
                            if elements[i] == 'SK':
                                lineObj['merchantSKU'] = lineObj['customer_sku'] = lineObj['sku'] = elements[i + 1]
                                lineObj['additional_fields'].update({'SK': elements[i + 1]})
                            if elements[i] == 'VN':
                                lineObj['vendor_sku'] = elements[i + 1]
                                lineObj['additional_fields'].update({'VN': elements[i + 1]})
                            if elements[i] == 'UP':
                                lineObj['optionSku'] = lineObj['upc'] = elements[i + 1]
                                lineObj['additional_fields'].update({'UP': elements[i + 1]})
                            if elements[i] == 'PD':
                                lineObj['name'] = elements[i + 1]
                                lineObj['additional_fields'].update({'PD': elements[i + 1]})
                        else:
                            break
                    ordersObj['lines'].append(lineObj)
                elif elements[0] == 'CTP':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['retail_cost'] = elements[3]
                elif elements[0] == 'PID':
                    #
                    if elements[2] == '08':
                        lineObj.update({
                            'name': elements[5],
                        })
                    elif elements[2] == '35':
                        lineObj['additional_fields'].update({'color': elements[5]})
                        lineObj.update({
                            'color': elements[5],
                        })
                    elif elements[2] == '85':
                        lineObj['additional_fields'].update({'style': elements[5]})
                    elif elements[2] == 'MB':
                        lineObj['additional_fields'].update({'marking': elements[5]})
                    elif elements[2] == 'ZZ':
                        lineObj['additional_fields'].update({'return_flag': elements[5]})
                elif elements[0] == 'AMT':
                    if elements[1] == 'F7':
                        lineObj['additional_fields'].update({'sales_tax': elements[2]})
                    elif elements[1] == 'OH':
                        lineObj['additional_fields'].update({'handling_charges': elements[2]})
                        lineObj['additional_fields'].update({'gift_message': gift_message})
                # elif elements[0] == 'CTT':
                    # ordersObj['lines'][len(ordersObj['lines']) - 1]['size'] = elements[1]
                elif elements[0] == 'SE':
                    orders.append(ordersObj)

        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
