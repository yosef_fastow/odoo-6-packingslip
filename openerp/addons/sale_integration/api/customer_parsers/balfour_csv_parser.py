import csv
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import cStringIO
import traceback
from openerp.addons.pf_utils.utils.import_orders_normalize import fix_line_terminators


class BalfourApiGetOrdersXML(object):

    def __init__(self, parent):
        self.parent = parent

    def parse_data(self, data):
        data = fix_line_terminators(data)
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        if(not data):
            return []
        obj_ordersList = []
        try:
            obj = list(csv.reader(cStringIO.StringIO(data), delimiter=',', quotechar='"', lineterminator='\n'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)

                _order_number = 'po_no'
                _po_number    = 'order_no'

            order_id_old = ""
            ordersObj = {}
            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                elif ordersObj.get('order_no', False) and order_id_old != row[idx.get(_order_number)].strip():
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                if ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False):
                    ordersObj['sold'] = {}
                    ordersObj['sold']['soldcity'] = row[idx.get('soldcity')].strip()
                    ordersObj['sold']['soldstate'] = row[idx.get('soldstate')].strip()
                    ordersObj['sold']['soldzip'] = row[idx.get('soldzip')].strip()
                    ordersObj['sold']['soldphone'] = row[idx.get('soldphone')].strip()
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['name'] = row[idx.get('shipname')].strip()
                    ordersObj['address']['ship']['address1'] = row[idx.get('shipadd1')].strip()
                    ordersObj['address']['ship']['address2'] = row[idx.get('shipadd2')].strip()
                    ordersObj['address']['ship']['city'] = row[idx.get('shipcity')].strip()
                    ordersObj['address']['ship']['state'] = row[idx.get('shipstate')].strip()
                    ordersObj['address']['ship']['zip'] = row[idx.get('shipzip')].strip()
                    ordersObj['address']['ship']['country'] = ''
                    ordersObj['address']['ship']['phone'] = row[idx.get('shipphone')].strip()

                    ordersObj['returnname'] = row[idx.get('returnname')].strip()
                    ordersObj['returnadd1'] = row[idx.get('returnadd1')].strip()
                    ordersObj['returnadd2'] = row[idx.get('returnadd2')].strip()
                    ordersObj['returncity'] = row[idx.get('returncity')].strip()
                    ordersObj['returnstate'] = row[idx.get('returnstate')].strip()
                    ordersObj['returnzip'] = row[idx.get('returnzip')].strip()

                    if (self.parent.customer == 'balfour'):
                        ordersObj['address']['order'] = {}
                        ordersObj['address']['order']['name'] = row[idx.get('soldname')].strip()
                        ordersObj['address']['order']['address1'] = row[idx.get('soldadd1')].strip()
                        ordersObj['address']['order']['address2'] = row[idx.get('soldadd2')].strip()
                        ordersObj['address']['order']['city'] = row[idx.get('soldcity')].strip()
                        ordersObj['address']['order']['state'] = row[idx.get('soldstate')].strip()
                        ordersObj['address']['order']['zip'] = row[idx.get('soldzip')].strip()
                        ordersObj['address']['order']['country'] = ''
                        ordersObj['address']['order']['phone'] = row[idx.get('soldphone')].strip()

                    ordersObj['ship_via'] = row[idx.get('ship_via')].strip()
                    ordersObj['external_date_order'] = ordersObj['date'] = row[idx.get('m/d/yyyy')].strip()
                    ordersObj['po_no'] = row[idx.get(_po_number)].strip()
                    ordersObj['partner_id'] = self.parent.customer
                    ordersObj['order_id'] = ordersObj['order_no']
                    ordersObj['poNumber'] = ordersObj['po_no']
                    ordersObj['carrier'] = row[idx.get('ship_via')].strip()
                    ordersObj['store_number'] = row[idx.get('store')].strip()
                    # ordersObj['payment_method'] = row[idx.get('payment_reference')].strip()
                    # ordersObj['shipping'] = ordersObj['shipping_fee'] = row[idx.get('total_freight')].strip()
                    # ordersObj['amount_tax'] = ordersObj['tax'] = row[idx.get('tax')].strip()

                    # ordersObj['web_order'] = row[idx.get('web_order')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []

                itemsObj = {}
                itemsObj['id'] = row[idx.get('item')].strip()
                itemsObj['desc'] = row[idx.get('desc')].strip()
                itemsObj['name'] = itemsObj['desc']
                itemsObj['qty_shippe'] = row[idx.get('qty_shippe')].strip()
                itemsObj['vendorSku'] = row[idx.get('vendor_sku')].strip()
                # itemsObj['merchantSKU'] = row[idx.get('retailer_sku')].strip()
                #temsObj['store'] = row[idx.get('store')].strip()
                # itemsObj['size'] = row[idx.get('size')].strip()
                itemsObj['qty'] = row[idx.get('qty')].strip()
                itemsObj['cost'] = False
                # itemsObj['customerCost'] = row[idx.get('unit_price')].strip()
                itemsObj['customerCost'] = 0.0

                if (self.parent.customer == 'balfour'):
                    itemsObj['merchantSKU'] = row[idx.get('item')].strip()
                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        for order in obj_ordersList:
            yaml_obj = YamlObject()
            ordersObj = {}
            ordersObj['xml'] = yaml_obj.serialize(_data=order)
            ordersObj['name'] = order[_order_number]
            result['orders_list'].append(ordersObj)
        return result
