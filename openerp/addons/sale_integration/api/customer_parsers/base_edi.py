# -*- coding: utf-8 -*-
from ..utils.ediparser import EdiParser
from abc import ABCMeta, abstractmethod
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from pf_utils.utils.re_utils import str2date
from element_with_xpath import AbstractElementWithXpath, xpathRule
from json import dumps as json_dumps
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import traceback


def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


address_type_map = {
    'SF': 'order',
    'Z7': 'order',
    'ST': 'ship',
    'BT': 'invoice',
    'VN': 'invoice',
}


class BadEDIFile(Exception):
    def __init__(self, message):
        super(BadEDIFile, self).__init__()
        self.message = message


class BadSenderError(Exception):
    def __init__(self, message):
        super(BadSenderError, self).__init__()
        self.message = message


class BaseEDIParser(object):

    def __init__(self, parent):
        super(BaseEDIParser, self).__init__()
        self.result = {
            'orders_list': [],  # Orders received via [850, 860] feeds + Advices [824
            'errors': [],       # Processing errors
        }
        self.parent = parent

    def check_sender_is_correct(self, edi_data):
        result = True
        if (not edi_data):
            result = False
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(message.delimiters[1])
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    break
        except BadSenderError:
            result = False
        except Exception:
            result = None
        return result

    def _append_error(self, _msg):
        self.result["errors"].append(_msg)
        return True

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        try:
            main_envelope = EdiParser(edi_data)
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
            main_envelope = None
        orders = []
        text_messages = []
        if main_envelope:
            for document in main_envelope['GS<-GE']:
                GS = document['GS'].first()
                ST = document['ST'].first()

                self.ack_control_number = GS[6]
                self.functional_group = GS[1]
                self.sender = GS[2]
                self.receiver = GS[3]
                extra_edi_info = {
                    'ack_control_number': self.ack_control_number,
                    'functional_group': self.functional_group,
                    'partner_id': self.parent.customer,
                    'number_of_transaction': main_envelope['GE'].first()[1],
                }
                if ST[1] == '850' and hasattr(self, 'Order'):
                    orders_documents = main_envelope['BEG<-CTT']
                    for order_document in orders_documents:
                        order_dict = self.Order(order_document).dict
                        order_dict.update(extra_edi_info.copy())
                        orders.append(order_dict)
                elif ST[1] == '860' and hasattr(self, 'CancelOrder'):
                    orders_documents = main_envelope['BCH<-CTT']
                    for order_document in orders_documents:
                        order_dict = self.CancelOrder(order_document).dict
                        order_dict.update(extra_edi_info.copy())
                        orders.append(order_dict)
                elif ST[1] == '864':  # Text Message
                    text_messages.append(self.parse_text_message_data(document))
                elif ST[1] == '997':  # Functional Acknowledgement, skip this file
                    return self.result
                elif ST[1] == '824' and hasattr(self, 'AppAdvice'):  # Application Advice
                    idx = 1
                    advice_documents = main_envelope['BGN<-TED']
                    for advice_document in advice_documents:
                        error_dict = self.AppAdvice(advice_document).dict
                        error_dict.update(extra_edi_info.copy())
                        text_messages.append(error_dict)
                        idx += 1

        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj.get('cancellation', False) is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj.get('change', False) is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if order_obj.get('additional_fields', {}):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list, dict))):
                            serialize_additional_fields.update({
                                key: json_dumps(value, default=date_handler)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        for text_message in text_messages:
            returned_message = {}
            try:
                returned_message['name'] = 'TEXTMESSAGE{0}'.format(text_message['message_id'])
                message_data = [text_message.get('header', ''), text_message.get('message_subject', '')]
                if text_message.get('invoice_no'):
                    message_data.append('INVOICE # {0}'.format(text_message.get('invoice_no')))
                message_data.append(text_message.get('message_text', ''))
                returned_message['message_data'] = '\n'.join([el for el in message_data if el])
                returned_message['xml'] = str(text_message)
                returned_message['ack_control_number'] = text_message.get('ack_control_number', '')
                returned_message['functional_group'] = text_message.get('functional_group', '')
                returned_message['number_of_transaction'] = text_message.get('number_of_transaction', '')
                self.result['orders_list'].append(returned_message)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result


class strXpathRule(xpathRule):
    __type_res__ = 'str'

    def process_res(self, res, xpath):
        return str(res) if res else res


class datetimeXpathRule(xpathRule):
    __type_res__ = 'datetime'

    def process_res(self, res, xpath):
        return str2date(res) if res else res


class flagXpathRule(xpathRule):
    __type_res__ = 'flag'

    def process_res(self, res, xpath):
        result = None
        if res:
            if res in ['y', 'Y']:
                result = True
            else:
                result = False
        return result


class boolXpathRule(xpathRule):
    __type_res__ = 'bool'

    def process_res(self, res, xpath):
        return res


class intXpathRule(xpathRule):
    __type_res__ = 'int'

    def process_res(self, res, xpath):
        return _try_parse(res, 'int')


class floatXpathRule(xpathRule):
    __type_res__ = 'float'

    def process_res(self, res, xpath):
        return _try_parse(res, 'float')


class ediElementWithXpath(AbstractElementWithXpath):

    custom_xpath_rules = {
        'str': strXpathRule,
        'datetime': datetimeXpathRule,
        'flag': flagXpathRule,
        'bool': boolXpathRule,
        'int': intXpathRule,
        'float': floatXpathRule,
    }

    def xpath_get(self, xpath):
        elem = self.input_obj
        number_segment = None
        if '.' in xpath:
            xpaths = xpath.split('.')
            xpath = xpaths[0]
            try:
                number_segment = int(xpaths[1])
            except ValueError:
                pass
        elem_number = None
        if '->' in xpath:
            xpaths = xpath.split('->')
            xpath = xpaths[0]
            try:
                elem_number = int(xpaths[1])
            except ValueError:
                pass
        try:
            res = elem[xpath]
            if res:
                if number_segment:
                    if elem_number:
                        res = res[elem_number][number_segment]
                    else:
                        res = res.first()[number_segment]
                else:
                    if elem_number:
                        res = res[elem_number]
        except:
            res = False
        return res


class AddressEdiElementWithXpath(ediElementWithXpath):

    __metaclass__ = ABCMeta

    def get_original_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res_type = address_type_map.get(res['type'], res['type'])
        del res['type']
        result = {res_type: res}
        return result

    @abstractmethod
    def to_dict(self):
        pass
