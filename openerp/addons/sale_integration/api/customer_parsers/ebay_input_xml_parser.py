# -*- coding: utf-8 -*-
from lxml.etree import parse, XMLParser, XPathEvalError
from lxml import objectify
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from datetime import datetime
import traceback
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from xml.sax.saxutils import unescape
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError, DelmarIDError


class XML_parser(object):

    ids = {
        "orders": "Order",
        "lines": "TransactionArray/Transaction",
        "order_id": "ShippingDetails/SellingManagerSalesRecordNumber",
        "external_date_order": "CreatedTime",
        "order_status": "OrderStatus",
        "cust_order_number": "ExtendedOrderID",
        "ship_method": "CheckoutStatus/PaymentMethod",
        "ship_service": "ShippingServiceSelected/ShippingService",
        "line_external_customer_line_id": "ShippingDetails/SellingManagerSalesRecordNumber",
        "line_delmar_id": "DelmarID",
        "line_customer_sku": "Item/SKU",
        "line_alternative_customer_sku": "Variation/SKU",
        "line_description": "Item/Title",
        "line_price_unit": "TransactionPrice",
        "line_ship_cost": "ActualShippingCost",
        "line_qty": "QuantityPurchased",
        "line_id": "Item/ItemID",
        "line_trans_with_item": "OrderLineItemID",
        "line_variation": "Variation",
        "line_variation_specifics": "VariationSpecifics",
        "line_variation_specifics_name_value_list": "NameValueList",
        "line_full_sku": "SKU",
        "line_name_size": "Name",
        "line_value_size": "Value",
        "customer_login": "RecipientUserID",
        "is_multi_leg_shipping": "IsMultiLegShipping",
    }

    shipping_address_priority = {
        "ship_name": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/Name",
        "ship_address1": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/Street1",
        "ship_address2": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/Street2",
        "ship_city": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/CityName",
        "ship_state": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/StateOrProvince",
        "ship_country": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/Country",
        "ship_zip": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/PostalCode",
        "ship_phone": "MultiLegShippingDetails/SellerShipmentToLogisticsProvider/ShipToAddress/Phone",
    }

    shipping_address = {
        "ship_name": "ShippingAddress/Name",
        "ship_address1": "ShippingAddress/Street1",
        "ship_address2": "ShippingAddress/Street2",
        "ship_city": "ShippingAddress/CityName",
        "ship_state": "ShippingAddress/StateOrProvince",
        "ship_country": "ShippingAddress/Country",
        "ship_zip": "ShippingAddress/PostalCode",
        "ship_phone": "ShippingAddress/Phone",
    }

    def __init__(self, external_customer_id):
        super(XML_parser, self).__init__()
        self.result = {
            'orders_list': [],
            'errors': [],
        }
        self.external_customer_id = external_customer_id

    def _get_value(self, parent, name, type_result='list'):
        result = None
        _xpath = ""
        try:
            _xpath = self.ids[name]
        except KeyError:
            _xpath = ""
        finally:
            try:
                _result = parent.xpath(_xpath)
                if((type_result == 'text') and (len(_result) == 1)):
                    result = _result[0].text
                elif((type_result == 'first_in_list') and (isinstance(_result, list)) and (len(_result) >= 1)):
                    result = _result[0]
                else:
                    result = _result
            except XPathEvalError:
                result = False
            except AttributeError:
                result = parent.xpath(_xpath)
            finally:
                return result

    def _get_lines(self, parent):
        return self._get_value(parent, "lines", "list")

    def _get_order_obj(self, parent):
        is_multi_leg_shipping = self._get_value(parent, 'is_multi_leg_shipping', "text")
        if(str(is_multi_leg_shipping).strip().lower() == "true"):
            self.ids.update(self.shipping_address_priority)
        else:
            self.ids.update(self.shipping_address)
        fmt = '%Y-%m-%dT%H:%M:%S'
        _external_date_order = self._get_value(parent, 'external_date_order', "text")
        try:
            external_date_order = datetime.strptime(_external_date_order[:-5], fmt).date().isoformat()
        except ValueError:
            external_date_order = _external_date_order
        order_obj = {
            'order_id': self._get_value(parent, 'order_id', "text"),
            'external_date_order': external_date_order,
            'order_status': self._get_value(parent, 'order_status', "text"),
            'partner_id': self.external_customer_id,
            'cust_order_number': self._get_value(parent, 'cust_order_number', "text"),
            'ship_method': self._get_value(parent, 'ship_method', "text"),
            'ship_service': self._get_value(parent, 'ship_service', "text"),
            'address':
            {
                'ship':
                    {
                        'name': self._get_value(parent, 'ship_name', "text"),
                        'address1': self._get_value(parent, 'ship_address1', "text"),
                        'address2': self._get_value(parent, 'ship_address2', "text"),
                        'city': self._get_value(parent, 'ship_city', "text"),
                        'state': self._get_value(parent, 'ship_state', "text"),
                        'zip': self._get_value(parent, 'ship_zip', "text"),
                        'country': self._get_value(parent, 'ship_country', "text"),
                        'phone': self._get_value(parent, 'ship_phone', "text"),
                    },
            },
            'lines': [],
            'customer_login': self._get_value(parent, 'customer_login', "text"),
        }
        _carrier = '_'.join(x for x in [order_obj['ship_service'], order_obj['ship_method']] if x)
        order_obj.update({'carrier': _carrier})
        return order_obj

    def try_get_size(self, parent):
        size = None
        variation = self._get_value(parent, "line_variation", "first_in_list")
        if(variation):
            variation_specifics = self._get_value(variation, "line_variation_specifics", "first_in_list")
            value_list = self._get_value(variation_specifics, "line_variation_specifics_name_value_list", "list")
            for item in value_list:
                name_variation = self._get_value(item, "line_name_size", "text")
                if(name_variation == "Size"):
                    size = _try_parse(self._get_value(item, "line_value_size", "text"), 'float')
                    break
            if(not size):
                full_sku = self._get_value(variation, "line_full_sku", "text")
                if(full_sku):
                    if('/' in full_sku):
                        size = _try_parse(full_sku[full_sku.find("/") + 1:], 'float')
                    else:
                        size = False
                else:
                    size = False
        else:
            size = False
        return size

    def _get_line_obj(self, parent):
        _customer_sku = self._get_value(parent, 'line_customer_sku', "text")
        if(not _customer_sku):
            _customer_sku = self._get_value(parent, 'line_alternative_customer_sku', "text")
        line_obj = {
            'id': self._get_value(parent, 'line_external_customer_line_id', "text"),
            'customer_sku': _customer_sku,
            'sku': False,
            'name': self._get_value(parent, 'line_description', "text"),
            'size': False,
            'qty': self._get_value(parent, 'line_qty', "text"),
            'ship_cost': self._get_value(parent, 'line_ship_cost', "text"),
            'price_unit': self._get_value(parent, 'line_price_unit', "text"),
            'line_id': self._get_value(parent, 'line_id', "text"),
            'order_line_item_id': self._get_value(parent, 'line_trans_with_item', "text"),
            'notes': ''
        }
        line_obj['qty'] = _try_parse(line_obj['qty'], 'int')

        for name_value in ['ship_cost', 'price_unit', 'size']:
            line_obj[name_value] = _try_parse(line_obj[name_value], 'float')

        if('/' in line_obj['customer_sku']):
            try:
                _sku = line_obj['customer_sku']
                _size = _try_parse(_sku[_sku.find("/") + 1:], 'float')
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass
        else:
            line_obj['sku'] = line_obj['customer_sku']

        if(not line_obj['size']):
            line_obj['size'] = self.try_get_size(parent)

        line_obj['notes'] = 'id: {0}\nQTY: {1}\nProduct: {2}\nSize: {3}\n'.format(
            *tuple(
                [line_obj.get(x, 'None') for x in ['id', 'qty', 'sku', 'size']]
            )
        )
        return line_obj

    def _add_line_to_order_obj(self, order_obj, line_xml):
        line_obj = self._get_line_obj(line_xml)

        if(line_obj['id'] is None):
            line_obj['id'] = len(order_obj['lines']) + 1
        order_obj['lines'].append(line_obj)

        return order_obj

    def _append_error(self, _msg):
        self.result["errors"].append(_msg)
        return True

    def parse_data(self, xml_data):
        order_obj = {}
        try:
            if '&apos;' in xml_data or '&quot;' in xml_data:
                xml_data = unescape(xml_data, {"&apos;": "'", "&quot;": '"'})
            parser = XMLParser(remove_blank_text=True)
            root_obj = parse(IO(xml_data), parser)
            if(root_obj):
                for elem in root_obj.getiterator():
                    if not hasattr(elem.tag, 'find'):
                        continue
                    i = elem.tag.find('}')
                    if i >= 0:
                        elem.tag = elem.tag[i+1:]
                objectify.deannotate(root_obj, cleanup_namespaces=True)
            if(not root_obj):
                raise Exception("Couldn't find root_obj in xml: {0}".format(xml_data))
            order_obj = self._get_order_obj(root_obj)
            lines_xml = self._get_lines(root_obj)
            for line_xml in lines_xml:
                order_obj = self._add_line_to_order_obj(order_obj, line_xml)
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)

        if order_obj:
            # # DELMAR-186
            # if (order_obj['lines']):
            #     serialize_line_fields = {}
            #     for key, value in order_obj['lines'][0].iteritems():
            #         if (isinstance(value, (tuple, list))):
            #             if key == 'name' and (value == None or value == ''):
            #                 value = 'empty product description in order'
            #             serialize_line_fields.update({
            #                 key: json.dumps(value)
            #             })
            #         else:
            #             if key == 'name' and (value == None or value == ''):
            #                 value = 'empty product description in order'
            #             serialize_line_fields.update({
            #                 key: value
            #             })
            #     order_obj['lines'][0] = serialize_line_fields
            # # END DELMAR-186
            for _line in order_obj['lines']:
                _line['customer_login'] = order_obj['customer_login']
            yaml_obj = YamlObject()
            returned_order = {}
            try:
                returned_order['name'] = order_obj['order_id']
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
