# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result, ElementWithXpath
from openerp.addons.pf_utils.utils.xml_utils import clean_namespaces
from lxml.etree import parse
from io import BytesIO
from lxml.etree import XPathEvalError
import dateutil.parser
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.pf_utils.utils.str_utils import str_encode


class lxmlElementWithXpath(ElementWithXpath):

    def __init__(self, lxml_etree_Element):
        super(lxmlElementWithXpath, self).__init__()
        self.el = lxml_etree_Element

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            res = self.el.xpath(xpath)
            if type_res == str and res:
                result = str_encode(res[0].text)
            elif type_res == 'date' and res:
                result = dateutil.parser.parse(res[0].text)
            elif type_res == bool and res:
                result = True if res[0].text.strip().lower() == 'true' else False
            elif type_res == int and res:
                result = _try_parse(res[0].text, 'int')
            elif type_res == float and res:
                result = _try_parse(res[0].text, 'float')
            elif type_res == Line:
                result = []
                for line in res:
                    result.append(Line(line).dict)
            else:
                result = None
        except XPathEvalError:
            result = False
        return result


class Line(lxmlElementWithXpath):

    xpaths = {
        'id': (str, 'salesChannelLineId'),
        'external_customer_line_id': (str, 'lineId'),
        'sku': (str, 'barcode'),
        'customer_sku': (str, 'partnerSKU'),
        'name': (str, 'itemName'),
        'qty': (int, 'quantity'),
        'price_unit': (float, 'firstCost'),
        'cost': (str, 'itemPrice'),
        'shipCost': (float, 'unitCost'),
        'additional_fields': {
            'salesChannelLineNumber': (str, 'salesChannelLineNumber'),
            'salesChannelSKU': (str, 'salesChannelSKU'),
            'itemId': (str, 'itemId'),
            'lineStatus': (str, 'lineStatus'),
        }
    }

    def __init__(self, lxml_etree_Element):
        super(Line, self).__init__(lxml_etree_Element)


class Order(lxmlElementWithXpath):

    xpaths = {
        'order_id': (str, 'orderId'),
        'poNumber': (str, 'retailOrderNumber'),
        'external_date_order': ('date', 'orderDate'),
        'carrier': (str, 'shippingSpecifications/smallParcelShipment/shippingServiceLevel/code'),
        'address': {
            'ship': {
                'name': (str, 'shipToAddress/contactName'),
                'address1': (str, 'shipToAddress/address1'),
                'address2': (str, 'shipToAddress/address2'),
                'city': (str, 'shipToAddress/city'),
                'state': (str, 'shipToAddress/stateOrProvince'),
                'zip': (str, 'shipToAddress/postalCode'),
                'country': (str, 'shipToAddress/countryCode'),
                'phone': (str, 'shipToAddress/phone'),
            },
            'return': {
                'name': (str, 'returnAddress/contactName'),
                'address1': (str, 'returnAddress/address1'),
                'address2': (str, 'shipToAddress/address2'),
                'city': (str, 'returnAddress/city'),
                'state': (str, 'returnAddress/stateOrProvince'),
                'zip': (str, 'returnAddress/postalCode'),
                'country': (str, 'returnAddress/countryCode'),
                'phone': (str, 'returnAddress/phone'),
            },
        },
        'additional_fields': {
            'salesChannelOrderNumber': (str, 'salesChannelOrderNumber'),
            'salesChannelName': (str, 'salesChannelName'),
            'sofsCreatedDate': ('date', 'sofsCreatedDate'),
            'warehouseName': (str, 'warehouseName/code'),
            'shippingSpecifications': {
                'isThirdPartyBilling': (bool, 'shippingSpecifications/isThirdPartyBilling'),
                'isSignatureRequired': (bool, 'shippingSpecifications/isSignatureRequired'),
                'isDeclaredValueRequired': (bool, 'shippingSpecifications/isDeclaredValueRequired'),
                'smallParcelShipment': (str, 'shippingSpecifications/smallParcelShipment/shippingServiceLevel/code'),
                'isExport': (bool, 'shippingSpecifications/isExport'),
            },
            'orderFulfillment': (str, 'orderFulfillment'),
            'status': (str, 'status'),
            'retailChannelCode': (str, 'retailChannelCode'),
            'actionRequired': (bool, 'actionRequired'),
        },
        'lines': (Line, 'processedSalesOrderLine'),
    }

    def __init__(self, lxml_etree_Element):
        super(Order, self).__init__(lxml_etree_Element)


class XMLParser(BaseParser):
    """:class: for parsing input XML data to Yaml object"""
    def __init__(self, parent):
        super(XMLParser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, xml_data):
        xml_data = clean_namespaces(xml_data)
        processedSalesOrderMessage = parse(BytesIO(xml_data))
        for processedSalesOrder in processedSalesOrderMessage.findall('list'):
            self.orders.append(Order(processedSalesOrder).dict)
        return True
