# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.import_orders_normalize import normalize_csv
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import traceback
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError


class CSV_parser():
    ids = {
        'order_id': 'so#',
        'shipname': 'Ship Full Name',
        'shipadd1': 'Ship Address',
        'shipadd2': 'Ship Address2',
        'shipcity': 'Ship City',
        'shipstate': 'Ship State',
        'shipzip': 'Ship Zip',
        'shipcountry': 'Ship Country',
        'shipphone': 'phone',
        'ship_via': 'Ship Method',
        'ship_service': 'Ship Method',
        'gift_message': 'giftmessage',
        'tax': 'tax',
        'date': 'orderdate',
        'line_number': 'itemlinenumber',
        'desc': 'description',
        'merchantSku': 'SKU',
        'vendorSku': 'JomaShop Item #',  #
        'size': 'size',
        'qty': 'Order Qty',
        'cost': 'Cost',
        'amount_total': 'CustCost',
        'external_customer_line_id': 'external_customer_line_id'
    }

    def __init__(self, customer=None, ids=None):
        self.external_customer_id = customer
        if (ids):
            self.ids = ids

    def _get_lines_from_csv(self, _data):
        lines = []
        _pseudo_file = IO(_data)
        csv_dialect = detect_dialect(_data)
        dr = DictReader(_pseudo_file, dialect=csv_dialect)
        for row in dr:
            lines.append(row)
        return lines

    def _get_value(self, line, name, default_value=False):
        name_column = None
        try:
            name_column = self.ids[name]
        except KeyError:
            name_column = False
        finally:
            if (name_column):
                result = line.get(name_column, default_value)
                if (isinstance(result, (unicode, str))):
                    result = result.strip()
            else:
                result = default_value
            return result

    def _get_order_obj(self, line):
        result = {
            'partner_id': self._get_value(line, 'partner_id', self.external_customer_id),
            'order_id': line.get('so#', False).rstrip(),
            'poNumber': line.get('so#', False).rstrip(),
            'merchantSku': line.get('jomashopitem#', False),
            'vendorSku': line.get('sku', False),
            'carrier': line.get('shipmethod', False).rstrip(),
            'ship_service': self._get_value(line, 'ship_service'),  #
            'address': {
                'ship': {
                    'name': self._get_value(line, 'shipname'),
                    'address1': line.get('shipaddress', '').rstrip(),
                    'address2': line.get('shipaddress2', '').rstrip(),
                    'city': line.get('shipcity', False).rstrip(),
                    'state': line.get('shipstate', False).rstrip(),
                    'zip': line.get('shipzip', False).rstrip(),
                    'country': line.get('shipcountry', False).rstrip(),
                    'phone': line.get('phone', False).rstrip(),
                },
            },
            'additional_fields': {
                'billfullname': line.get('billfullname', False),
                'billphone': line.get('billphone', False),
                'shipphone': line.get('phone', False),
                'ship_service': line.get('shipmethod', False),
                'gift_message': line.get('giftmessage', False),
                'ship_full_name': line.get('shipfullname', False),
            },
            'gift_message': line.get('giftmessage', False),
            'poHdrData': {
                'gift_message': line.get('giftmessage', False),
                'gift': bool(line.get('giftmessage', False)),
                'giftIndicator': 'y' if bool(line.get('giftmessage', False)) else 'n',
            },
            'billname': line.get('billfullname', False),
            'billphone': line.get('billphone', False),
            'sent_ship_date': line.get('senttoshipdate', False),
            'ship_date': line.get('shipdate', False),
            'ship_full_name':  line.get('shipfullname', False),
            'tax': line.get('tax', False),
            'date': line.get('date', False),
            'lines': []
        }

        return result

    def _get_line_obj(self, line):

        line_obj = {
            'id': False,
            'line_number': line.get('uniqueid'),
            'name': '',
            'size': line.get('size'),
            'qty': line.get('orderqty', 1),
            'cost': line.get('cost'),
            'amount_total': line.get('custcost'),
            'customerCost': self._get_value(line, 'custcost'),
            'gift_message': line.get('giftmessage', False),
            'zuck_order': line.get('zuck_order'),
            'zuck_item': line.get('zuck_item'),
            'partner_id': self._get_value(line, 'partner_id', self.external_customer_id),
            'order_id': line.get('so#', False),
            'poNumber': line.get('so#', False),
            'merchantSKU': line.get('jomashopitem#', False),
            'id': line.get('uniqueid'),
            'vendorSku': line.get('sku', False),
            'sku': line.get('sku', False),
        }

        for name_value in ['cost', 'customerCost', 'amount_total', 'size']:
            if (line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'float')
        for name_value in ['qty']:
            if (line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'int')
        return line_obj

    def _add_line_to_order_obj(self, order_obj, _line):
        line_obj = self._get_line_obj(_line)

        add_new_line = True
        curr_line_id = 1

        for line in order_obj['lines']:
            curr_line_id += 1
            condition = ((line['merchantSKU'] == line_obj['merchantSKU']) and (line['size'] == line_obj['size']))
            if (condition):
                if (isinstance(line_obj['qty'], int)):
                    line['qty'] += line_obj['qty']
                else:
                    line['qty'] += 1
                add_new_line = False
                break
        if (add_new_line):
            if (not line_obj['id']):
                line_obj['id'] = curr_line_id
            order_obj['lines'].append(line_obj)
        return order_obj

    def parse_data(self, csv_data):
        result = {
            'errors': [],
            'orders_list': [],
        }
        result_normalize = normalize_csv(data=csv_data)
        if (not result_normalize['normalize_ok']):
            raise Exception("Couldn't normalize csv file data :\n{0}".format(csv_data))
        csv_data = result_normalize['csv_data']
        csv_data = strip_lower_del_spaces_from_header(csv_data)
        list_of_orders_obj = {}
        try:

            for line in self._get_lines_from_csv(csv_data):

                order_id = self._get_value(line, 'order_id')
                if (not order_id):
                    raise OrderIDError("Order ID not find in line: {0}".format(str(line)))
                if (not list_of_orders_obj.get(order_id, False)):
                    list_of_orders_obj[order_id] = self._get_order_obj(line)
                list_of_orders_obj[order_id] = self._add_line_to_order_obj(list_of_orders_obj[order_id], line)
            yaml_obj = YamlObject()
            if (not result['errors']):
                for order_id in list_of_orders_obj:
                    result_order_obj = {
                        'name': order_id,
                        'xml': yaml_obj.serialize(_data=list_of_orders_obj[order_id])
                    }
                    result['orders_list'].append(result_order_obj)
        except Exception:
            _msg = traceback.format_exc()
            result['errors'].append(_msg)
        finally:
            return result
