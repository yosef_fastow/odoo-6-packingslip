# -*- coding: utf-8 -*-
import logging
import csv
from cStringIO import StringIO
from datetime import datetime
import traceback
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError, DelmarIDError

_logger = logging.getLogger(__name__)


class ParseLineObj(object):

    def __init__(self, ids, line):
        self.ids = ids
        self.line = line

    def get_value(self, name, default=-1):
        result = None
        try:
            result = self.line[self.ids[name]]
        except KeyError:
            if(default != -1):
                result = default
            else:
                result = self.ids[name]
        finally:
            return result

    def get_order_obj(self, external_customer_id):
        date_order = None
        try:
            dt = self.get_value('order_date', False)
            if dt:
                date_order = datetime.strptime(dt, '%m/%d/%Y').date().isoformat()
        except:
            date_order = None

        order_obj = {
            'order_id': self.get_value('order_id', False),
            'merchant_id': self.get_value('merchant_id', False),
            'partner_id': external_customer_id,
            'external_date_order': date_order,
            'carrier': self.get_value('ship_method', False),
            'ship_method': self.get_value('ship_method', False),
            'lines': [],
            'additional_fields': {
                'supplier_id': self.get_value('supplier_id', False)
            },
        }

        order_obj['address'] = {}
        order_obj['address']['ship'] = {}
        order_obj['address']['ship']['name'] = self.get_value('ship_name', False)
        order_obj['address']['ship']['address1'] = self.get_value('ship_address1', False)
        order_obj['address']['ship']['address2'] = self.get_value('ship_address2', False)
        order_obj['address']['ship']['city'] = self.get_value('ship_city', False)
        order_obj['address']['ship']['state'] = self.get_value('ship_state', False)
        order_obj['address']['ship']['zip'] = self.get_value('ship_zip', False)
        order_obj['address']['ship']['country'] = self.get_value('ship_country', False)
        order_obj['address']['ship']['phone'] = self.get_value('ship_phone', False)
        return order_obj

    def get_line_obj(self):
        line_obj = {
            'id': self.get_value('external_customer_line_id'),
            'sku': self.get_value('line_customer_sku'),
            'customer_sku': self.get_value('line_customer_sku'),
            'upc': self.get_value('line_upc'),
            'qty': self.get_value('line_qty'),
            'size': self.get_value('line_size'),
            'name': self.get_value('line_description'),
            'customerCost': self.get_value('line_price_unit'),
            'additional_fields': {
                'upc': self.get_value('line_upc'),
                'color': self.get_value('line_color'),
                'width': self.get_value('line_width'),
            }
        }

        if str(line_obj['size']).strip().lower() == 'one size':
            line_obj['size'] = False

        return line_obj


class CSVParser():

    ids = {
        'order_id': 0,
        'external_customer_line_id': 1,
        'partner_id': 2,
        'supplier_id': 3,
        'order_date': 4,  # check date
        'location_code': 5,
        'ship_name': 6,
        'ship_address1': 7,
        'ship_address2': 8,
        'ship_city': 9,
        'ship_state': 10,
        'ship_zip': 11,
        'ship_country': 12,
        'ship_phone': 13,
        'ship_method': 14,
        'line_qty': 15,
        'line_customer_sku': 16,
        'line_upc': 17,
        'line_description': 18,
        'line_size': 19,
        'line_width': 20,
        'line_color': 21,
        'line_price_unit': 22
    }

    def __init__(self, external_customer_id, delimiter=',', quotechar='"', ids=None):
        if(ids is not None):
            self.ids = ids
        self.external_customer_id = external_customer_id
        self.delimiter = delimiter
        self.quotechar = quotechar

    def parse_data(self, csv_data):
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        _order_obj = {}
        order_id_old = ""
        obj_orders_list = []
        curr_line_id = 0
        try:
            rdr = csv.reader(StringIO(csv_data), delimiter=self.delimiter, quotechar=self.quotechar)
            for rec in rdr:
                rec_obj = ParseLineObj(self.ids, rec)
                if(not _order_obj.get('order_id', False) or order_id_old != rec_obj.get_value('order_id', False)):
                    _order_obj = rec_obj.get_order_obj(self.external_customer_id)
                if(not _order_obj['order_id']):
                    raise OrderIDError("Order ID not find in line: {0}".format(str(rec_obj.line)))
                if(_order_obj.get('order_id', False) and order_id_old != rec_obj.get_value('order_id', False)):
                    obj_orders_list.append(_order_obj)
                    order_id_old = _order_obj['order_id']
                _line_obj = rec_obj.get_line_obj()
                if(not _line_obj['sku']):
                    raise DelmarIDError("Not find Delmar ID in line: {0}".format(str(rec_obj.line)))
                if(_line_obj['qty']):
                    try:
                        _line_obj['qty'] = int(_line_obj['qty'])
                    except ValueError:
                        _line_obj['qty'] = False
                    curr_line_id += 1
                    if(not _line_obj['id']):
                        _line_obj['id'] = curr_line_id
                    _order_obj['lines'].append(_line_obj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        yaml_obj = YamlObject()
        for order in obj_orders_list:
            returned_order = {}
            try:
                returned_order['name'] = order['order_id']
                returned_order['xml'] = yaml_obj.serialize(_data=order)
                result['orders_list'].append(returned_order)
            except Exception:
                msg = traceback.format_exc()
                result['errors'].append(msg)
        return result
