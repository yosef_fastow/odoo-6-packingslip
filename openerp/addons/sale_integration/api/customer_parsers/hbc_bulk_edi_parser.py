# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import dateutil.parser
import datetime

# TODO: update Technical_error_Descripition with HBC Bulk error description
TECHNICAL_ERROR_DESCRIPTION = {
    'P': 'Missing or Invalid Item Quantity',
    'Q': 'Missing or Invalid Item Identification',
    'T': 'Unauthorized Transaction Set Purpose Code',
    'U': 'Missing or Unauthorized Transaction Type Code',
    'V': 'Missing or Unauthorized Action Code',
    'MB': 'Missing or Invalid Purchase Order Number',
    'MF': 'Missing or Invalid Internal Vendor Number',
    'MI': 'Missing or Invalid SCAC',
    '006': 'Duplicate',
    '007': 'Missing Data',
    '009': 'Invalid Date',
    '011': 'Not Matching',
    '012': 'Invalid Combination',
    '024': 'Other Unlisted Reason Error',  # The reason for the application error condition cannot be described using any other code on the standard code list
    '107': 'Missing or Invalid Location',
    '110': 'Missing Marking Identification at Pack Level',
    '133': 'Item Not Found On Purchase Order',
    'IID': 'Invalid Identification Code',
    'IQT': 'Invalid Quantity',
    'MID': 'Missing Identification Code',
    'OTH': 'Unspecified application error',  # Other
    'POI': 'Purchase Order Number Invalid',
}

class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        self.delimiters = '*'
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(self.delimiters)
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {
                            'store_numbers': [],
                        },
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {}
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                    #DELMAR-100 start
                        order_number = ''
                        for edi_line in message.segments:
                            if edi_line[0] == 'BCH':
                                order_number = edi_line[3]
                        filename = str(datetime.datetime.now()).split(' ')
                        filename = 'hbc_860_' + order_number + '_' + filename[0] + '_' + filename[1][:8]
                        self.parent.si_xml_obj.create(self.parent.cursor, self.parent.user_id, {
                                    'xml': str(message.edi),
                                    'name': filename,
                                    'type_api_id': self.parent.type_api_id,
                                    'load': True,
                                })
                        sender = 'techspawn2016.tester@gmail.com'
                        receiver = ['delmar_support@techspawn.com']
                        subject = 'SUPPORT :: Received 860 order from hbc_bulk for number ' + order_number
                        body = " Please find 860 order for hbc_bulk. Order number :: " + order_number + "\n\n" + str(message.edi)
                        _mail_message = {
                            'email_from': sender,
                            'emailto_list': receiver,
                            'subject': subject,
                            'body': body,
                            'attachments': {},
                        }
                        self.parent.send_mail.integrationApiSendEmail(self.parent.cursor, self.parent.user_id, _mail_message)
                        break
                        #DELMAR-100 end
                    elif elements[1] == '824':  # Application Advice
                        return self.result
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':
                    if elements[1] in ('DP', 'MR'):
                        ordersObj['additional_fields'].update({
                            elements[1]: elements[3]
                        })
                    else:
                        ordersObj['additional_fields'].update({
                            elements[1]: elements[2]
                        })
                elif elements[0] == 'ITD':
                    terms_type_code = {
                        '01': 'Only Includes Net Days',
                        '02': 'First Following Day',
                        '08': 'Basic Discount Offered',
                    }
                    ordersObj['additional_fields'].update({
                        'terms_type_code': terms_type_code.get(elements[1], elements[1]),
                        'terms_basis_date_code': elements[2],
                        'terms_discount_percent': elements[3],
                        'terms_Discount_days_due': elements[5],
                        'terms_net_days': elements[7],
                    })
                    if len(elements) > 13:
                        ordersObj['additional_fields'].update({
                            'Day_of_month_FF': elements[13]
                        })
                elif elements[0] == 'DTM':
                    if elements[1] == '064':
                        # Do Not Deliver Before
                        ordersObj['additional_fields'].update({
                            'ship_date': elements[2]
                        })
                    elif elements[1] == '063':
                        # No not deliver After
                        ordersObj['additional_fields'].update({
                            'cancel_after': elements[2]
                        })

                # TD5 - relevant carriers. Routing Sequence, carrier id qualifier, carrier name/code
                elif elements[0] == 'TD5':
                    if len(elements) > 3:
                        ordersObj['carrier'] = elements[3]
                # N9 - To transmit identifying information as specified by the Reference Identification.
                # same number in BEG[3]
                
                elif elements[0] == 'MSG':
                    ordersObj['additional_fields'].update({
                        'MSG': ordersObj['additional_fields'].get('MSG', '')+' '+elements[1]
                    })


                elif elements[0] == 'N1':
                    if elements[1] in ['CS']:  # Bill-to-Party, Vendor
                        address_type = 'order'
                        ordersObj['address']['order'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['order']['name'] = elements[2]
                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address']['ship'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['ship']['name'] = elements[4]

                elif elements[0] == 'PO1':

                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'sku': elements[9],
                        'vendorSku': elements[11],
                        'customer_id_delmar': elements[11],
                        'upc': elements[7],
                        'merchantSKU': elements[7],
                        'customer_sku': elements[9],
                        'optionSku': elements[9],
                        'name': elements[9],
                        'poLineData': {},
                        'poPackData': [],
                        'additional_fields': {}
                    }

                    if (elements[9]):
                        lineObj['additional_fields'].update({
                            elements[8]: elements[9]
                        })
                        lineObj['poLineData'][elements[8]] = elements[9]
                    if (elements[6]):
                        lineObj['additional_fields'].update({
                            elements[6]: elements[7]
                        })
                    if elements[13]:
                        lineObj['color'] = elements[13]
                        lineObj['poLineData']['prodColor'] = elements[13]
                    if elements[15]:
                        lineObj['poLineData']['prodSize'] = elements[15]

                    ordersObj['lines'].append(lineObj)
                elif elements[0] == 'CTP':
                    if elements[2] == 'UCP':
                        ordersObj['lines'][len(ordersObj['lines']) - 1].update({
                            'cost': elements[3],
                            'unit_price': elements[3],
                        })
                    elif elements[2] == 'RES':
                        ordersObj['lines'][len(ordersObj['lines']) - 1].update({
                            'retail_cost': elements[3],
                            'customerCost': elements[3],
                        })

                # po4 INNER PACK INFROMATION

                # P04 INNER PACK INFORMATION

                elif elements[0] == 'SAC':
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['additional_fields'].update({
                        'vics_charge_code': elements[4]
                    })

                elif elements[0] == 'SDQ':
                    ordersObj['poHdrData'].update({
                        'store_number': elements[3],
                        'qty_ordered': elements[4]
                    })
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['poLineData']['store_number'] = elements[3]
                    ordersObj['lines'][len(ordersObj['lines']) - 1]['poLineData']['qty_ordered'] = elements[4]
                    for i in range(2, 12):
                        element_index = (i * 2)
                        if len(elements) >= element_index:
                            ordersObj['lines'][len(ordersObj['lines']) - 1]['poPackData'].append({
                                'store_number': elements[element_index-1],
                                'qty_ordered': elements[element_index]
                            });
                            if elements[element_index-1] not in ordersObj['additional_fields']['store_numbers']:
                                ordersObj['additional_fields']['store_numbers'].append(elements[element_index-1])
                        else:
                            break
                    

                elif elements[0] == 'SE':
                    if not ordersObj.get('carrier', False):
                        ordersObj['carrier'] = 'CANP'
                    orders.append(ordersObj)
                elif elements[0] == 'GE':
                    ordersObj['number_of_transaction'] = self.number_of_transaction = elements[1]
                elif elements[0] == 'IEA':
                    ordersObj['additional_fields'].update({
                        'number_of_included_functional_group': elements[1],
                        'interchange_control_number': elements[2],
                    })

        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result

