class OrderIDError(Exception):
    """Raised when not find order id."""


class DelmarIDError(Exception):
    """Raised when not find Delmar ID."""


class CSVPparserError(Exception):
    def __init__(self, message):
        super(CSVPparserError, self).__init__()
        self.message = message


class CSVLineError(Exception):
    """raised if bad csv line"""
    def __init__(self, line_count, message):
        super(CSVLineError, self).__init__()
        self.line_count = line_count
        self.message = message
