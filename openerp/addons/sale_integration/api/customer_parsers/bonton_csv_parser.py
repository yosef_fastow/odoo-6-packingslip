import csv
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import cStringIO
import traceback
from openerp.addons.pf_utils.utils.import_orders_normalize import fix_line_terminators
from ..utils import xmlutils as xml_utils


class CSVParser(object):


    def __init__(self, customer=None, ids=None):
        self.external_customer_id = customer
        if (ids):
            self.ids = ids

    def parse_data(self, response):
        if(not response):
            return []
        obj_ordersList = []
        result = {}
        result['orders_list'] = []
        result['errors'] = []

        obj = list(csv.reader(cStringIO.StringIO(response), delimiter=',', quotechar='"', lineterminator='\r\n'))
        head_row = obj[0]
        idx = {}
        for col in head_row:
            idx[col] = head_row.index(col)
        order_id_old = ""
        ordersObj = {}
        try:
            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get('po_number')].strip()
                elif(ordersObj.get('order_no', False) and order_id_old != row[idx.get('po_number')].strip()):
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get('po_number')].strip()
                if(ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False)):
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['first_name'] = row[idx.get('ship_first_name')].strip()
                    ordersObj['address']['ship']['last_name'] = row[idx.get('ship_last_name')].strip()
                    ordersObj['address']['ship']['name'] = ordersObj['address']['ship']['first_name'] + " " + ordersObj['address']['ship']['last_name']
                    ordersObj['address']['ship']['company'] = row[idx.get('ship_company')].strip()
                    ordersObj['address']['ship']['address1'] = row[idx.get('ship_address_1')].strip()
                    ordersObj['address']['ship']['address2'] = row[idx.get('ship_address_2')].strip()
                    ordersObj['address']['ship']['city'] = row[idx.get('ship_city')].strip()
                    ordersObj['address']['ship']['state'] = row[idx.get('ship_region')].strip()
                    ordersObj['address']['ship']['zip'] = row[idx.get('ship_postal')].strip()
                    ordersObj['address']['ship']['phone'] = row[idx.get('ship_phone')].strip()
                    ordersObj['address']['ship']['country'] = row[idx.get('ship_country')].strip()
                    ordersObj['address']['ship']['phone'] = row[idx.get('ship_phone')].strip()
                    ordersObj['address']['ship']['email'] = row[idx.get('ship_email')].strip()
                    ordersObj['address']['ship']['store_number'] = row[idx.get('ship_store_number')].strip()
                    ordersObj['address']['ship']['carrier'] = row[idx.get('ship_carrier')].strip()
                    ordersObj['address']['ship']['method'] = row[idx.get('ship_method')].strip()
                    ordersObj['address']['ship']['attention'] = row[idx.get('ship_attention')].strip()

                    ordersObj['address']['order'] = {}
                    ordersObj['address']['order']['attention'] = row[idx.get('bill_to_attention')].strip()
                    ordersObj['address']['order']['first_name'] = row[idx.get('bill_to_first_name')].strip()
                    ordersObj['address']['order']['last_name'] = row[idx.get('bill_to_last_name')].strip()
                    ordersObj['address']['order']['name'] = ordersObj['address']['order']['first_name'] + " " + ordersObj['address']['order']['last_name']
                    ordersObj['address']['order']['company'] = row[idx.get('bill_to_company')].strip()
                    ordersObj['address']['order']['address1'] = row[idx.get('bill_to_address_1')].strip()
                    ordersObj['address']['order']['address2'] = row[idx.get('bill_to_address_2')].strip()
                    ordersObj['address']['order']['city'] = row[idx.get('bill_to_city')].strip()
                    ordersObj['address']['order']['state'] = row[idx.get('bill_to_region')].strip()
                    ordersObj['address']['order']['zip'] = row[idx.get('bill_to_postal')].strip()
                    ordersObj['address']['order']['country'] = row[idx.get('bill_to_country')].strip()
                    ordersObj['address']['order']['phone'] = row[idx.get('bill_to_phone')].strip()
                    ordersObj['address']['order']['email'] = row[idx.get('bill_to_email')].strip()

#todo:get   unit_price, customer_cost, carrier, customer_id_delmar
                    ordersObj['additional_fields'] = {}
                    ordersObj['additional_fields']['shipping_service_level_code'] = row[
                        idx.get('shipping_service_level_code')].strip()
                    ordersObj['additional_fields']['expected_delivery_date'] = row[
                        idx.get('expected_delivery_date')].strip()
                    ordersObj['additional_fields']['required_delivery_date'] = row[
                        idx.get('required_delivery_date')].strip()
                    ordersObj['additional_fields']['requested_warehouse_code'] = row[
                        idx.get('requested_warehouse_code')].strip()
                    ordersObj['additional_fields']['requested_warehouse_retailer_code'] = row[
                        idx.get('requested_warehouse_retailer_code')].strip()
                    ordersObj['additional_fields']['requested_warehouse_dsco_id'] = row[
                        idx.get('requested_warehouse_dsco_id')].strip()
                    ordersObj['additional_fields']['dsco_warehouse_code'] = row[
                        idx.get('dsco_warehouse_code')].strip()
                    ordersObj['additional_fields']['dsco_warehouse_retailer_code'] = row[
                        idx.get('dsco_warehouse_retailer_code')].strip()
                    ordersObj['additional_fields']['dsco_warehouse_dsco_id'] = row[
                        idx.get('dsco_warehouse_dsco_id')].strip()
                    ordersObj['additional_fields']['ship_warehouse_code'] = row[
                        idx.get('ship_warehouse_code')].strip()
                    ordersObj['additional_fields']['ship_warehouse_retailer_code'] = row[
                        idx.get('ship_warehouse_retailer_code')].strip()
                    ordersObj['additional_fields']['ship_warehouse_dsco_id'] = row[
                        idx.get('ship_warehouse_dsco_id')].strip()
                    ordersObj['additional_fields']['signature_required_flag'] = row[
                        idx.get('signature_required_flag')].strip()
                    ordersObj['additional_fields']['retailer_create_date'] = row[
                        idx.get('retailer_create_date')].strip()
                    ordersObj['additional_fields']['channel'] = row[
                        idx.get('channel')].strip()
                    ordersObj['additional_fields']['test_flag'] = row[
                        idx.get('test_flag')].strip()
                    ordersObj['additional_fields']['consumer_order_number'] = row[
                        idx.get('consumer_order_number')].strip()
                    ordersObj['additional_fields']['currency_code'] = row[
                        idx.get('currency_code')].strip()
                    ordersObj['additional_fields']['marketing_message'] = row[
                        idx.get('marketing_message')].strip()
                    ordersObj['additional_fields']['packing_slip_message'] = row[
                        idx.get('packing_slip_message')].strip()
                    ordersObj['additional_fields']['packing_instructions'] = row[
                        idx.get('packing_instructions')].strip()
                    ordersObj['additional_fields']['returns_message'] = row[
                        idx.get('returns_message')].strip()
                    ordersObj['additional_fields']['message'] = row[
                        idx.get('message')].strip()
                    ordersObj['additional_fields']['receipt_id'] = row[
                        idx.get('receipt_id')].strip()
                    ordersObj['additional_fields']['shipping_surcharge'] = row[
                        idx.get('shipping_surcharge')].strip()
                    # ordersObj['additional_fields']['tax_percentage_x'] = row[
                    #     idx.get('tax_percentage_x')].strip()
                    # ordersObj['additional_fields']['tax_type_code_x'] = row[
                    #     idx.get('tax_type_code_x')].strip()
                    # ordersObj['additional_fields']['coupon_amount_x'] = row[
                    #     idx.get('coupon_amount_x')].strip()
                    # ordersObj['additional_fields']['coupon_percentage_x'] = row[
                    #     idx.get('coupon_percentage_x')].strip()
                    # ordersObj['additional_fields']['payment_card_type_x'] = row[
                    #     idx.get('payment_card_type_x')].strip()
                    # ordersObj['additional_fields']['payment_card_last_four_x'] = row[
                    #     idx.get('payment_card_last_four_x')].strip()
                    # ordersObj['additional_fields']['acknowledge_by_date'] = row[
                    #     idx.get('acknowledge_by_date')].strip()
                    # ordersObj['additional_fields']['cancel_after_date'] = row[
                    #     idx.get('cancel_after_date')].strip()
                    # ordersObj['additional_fields']['ship_by_date'] = row[
                    #     idx.get('ship_by_date')].strip()
                    # ordersObj['additional_fields']['invoice_by_date'] = row[
                    #     idx.get('invoice_by_date')].strip()
                    # ordersObj['additional_fields']['dsco_ship_carrier'] = row[
                    #     idx.get('dsco_ship_carrier')].strip()
                    # ordersObj['additional_fields']['dsco_ship_method'] = row[
                    #     idx.get('dsco_ship_method')].strip()
                    # ordersObj['additional_fields']['dsco_shipping_service_level_code'] = row[
                    #     idx.get('dsco_shipping_service_level_code')].strip()
                    # ordersObj['additional_fields']['retailer_ship_carrier'] = row[
                    #     idx.get('retailer_ship_carrier')].strip()
                    # ordersObj['additional_fields']['retailer_ship_method'] = row[
                    #     idx.get('retailer_ship_method')].strip()
                    # ordersObj['additional_fields']['retailer_shipping_service_level_code'] = row[
                    #     idx.get('retailer_shipping_service_level_code')].strip()
                    # ordersObj['additional_fields']['dsco_order_id'] = row[
                    #     idx.get('dsco_order_id')].strip()
                    # ordersObj['additional_fields']['dsco_order_status'] = row[
                    #     idx.get('dsco_order_status')].strip()
                    # ordersObj['additional_fields']['dsco_item_id'] = row[
                    #     idx.get('dsco_item_id')].strip()
                    # ordersObj['additional_fields']['dsco_supplier_id'] = row[
                    #     idx.get('dsco_supplier_id')].strip()
                    # ordersObj['additional_fields']['dsco_supplier_name'] = row[
                    #     idx.get('dsco_supplier_name')].strip()
                    # ordersObj['additional_fields']['dsco_retailer_id'] = row[
                    #     idx.get('dsco_retailer_id')].strip()
                    # ordersObj['additional_fields']['dsco_retailer_name'] = row[
                    #     idx.get('dsco_retailer_name')].strip()
                    ordersObj['additional_fields']['dsco_trading_partner_id'] = row[
                        idx.get('dsco_trading_partner_id')].strip()
                    ordersObj['additional_fields']['dsco_trading_partner_name'] = row[
                        idx.get('dsco_trading_partner_name')].strip()
                    # ordersObj['additional_fields']['dsco_create_date'] = row[
                    #     idx.get('dsco_create_date')].strip()
                    # ordersObj['additional_fields']['dsco_last_update_date'] = row[
                    #     idx.get('dsco_last_update_date')].strip()

                    ordersObj['poHdrData'] = {}
                    ordersObj['poHdrData']['gift_flag'] = row[idx.get('gift_flag')].strip()
                    ordersObj['poHdrData']['gift_message'] = row[idx.get('gift_message')].strip()
                    ordersObj['poHdrData']['gift_to_name'] = row[idx.get('gift_to_name')].strip()
                    ordersObj['poHdrData']['gift_from_name'] = row[idx.get('gift_from_name')].strip()
                    ordersObj['poHdrData']['gift_receipt_id'] = row[idx.get('gift_receipt_id')].strip()



                    # ordersObj['date'] = row[idx.get('m/d/yyyy')].strip()
                    ordersObj['po_no'] = row[idx.get('po_number')].strip()
                    ordersObj['ship_via'] = row[idx.get('shipping_service_level_code')].strip()
                    ordersObj['carrier'] = row[idx.get('shipping_service_level_code')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []
                    ordersObj['order_id'] = ordersObj['order_no']
                    ordersObj['partner_id'] = self.external_customer_id
                itemsObj = {}
                itemsObj['id'] = row[idx.get('line_item_line_number')].strip()
                itemsObj['item'] = row[idx.get('line_item_sku')].strip()
                itemsObj['upc'] = row[idx.get('line_item_upc')].strip()
                itemsObj['ean'] = row[idx.get('line_item_ean')].strip()
                itemsObj['partner_sku'] = row[idx.get('line_item_partner_sku')].strip()
                itemsObj['retailer_item_id'] = row[idx.get('line_item_retailer_item_id_1')].strip()
                itemsObj['qty'] = row[idx.get('line_item_quantity')].strip()
                itemsObj['name'] = row[idx.get('line_item_title')].strip()
                itemsObj['color'] = row[idx.get('line_item_color')].strip()
                itemsObj['size'] = row[idx.get('line_item_size')].strip()
                itemsObj['cost'] = row[idx.get('line_item_expected_cost')].strip()
                itemsObj['customerCost'] = row[idx.get('line_item_consumer_price')].strip()
                itemsObj['consumer_price'] = row[idx.get('line_item_consumer_price')].strip()
                itemsObj['retail_price'] = row[idx.get('line_item_retail_price')].strip()
                itemsObj['personalization'] = row[idx.get('line_item_personalization')].strip()
                itemsObj['warehouse_code'] = row[idx.get('line_item_warehouse_code')].strip()
                itemsObj['warehouse_retailer_code'] = row[idx.get('line_item_warehouse_retailer_code')].strip()
                itemsObj['warehouse_dsco_id'] = row[idx.get('line_item_warehouse_dsco_id')].strip()


                itemsObj['poHdrData'] = {}
                itemsObj['poHdrData']['gift_flag'] = row[idx.get('line_item_gift_flag')].strip()
                itemsObj['poHdrData']['gift_receipt_id'] = row[idx.get('line_item_gift_receipt_id')].strip()
                itemsObj['poHdrData']['gift_to_name'] = row[idx.get('line_item_gift_to_name')].strip()
                itemsObj['poHdrData']['gift_from_name'] = row[idx.get('line_item_gift_from_name')].strip()
                itemsObj['poHdrData']['gift_message'] = row[idx.get('line_item_gift_message')].strip()

                itemsObj['additional_fields'] = {}
                itemsObj['additional_fields']['line_item_shipping_surcharge'] = row[idx.get('line_item_shipping_surcharge')].strip()
                itemsObj['additional_fields']['line_item_packing_slip_message'] = row[
                    idx.get('line_item_packing_slip_message')].strip()
                itemsObj['additional_fields']['packing_instructions'] = row[
                    idx.get('line_item_packing_instructions')].strip()
                itemsObj['additional_fields']['returns_message'] = row[
                    idx.get('line_item_returns_message')].strip()
                itemsObj['additional_fields']['ship_instructions'] = row[
                    idx.get('line_item_ship_instructions')].strip()
                itemsObj['additional_fields']['message'] = row[idx.get('line_item_message')].strip()
                itemsObj['additional_fields']['bogo_flag'] = row[idx.get('line_item_bogo_flag')].strip()
                itemsObj['additional_fields']['bogo_instructions'] = row[idx.get('line_item_bogo_instructions')].strip()
                # itemsObj['additional_fields']['line_item_tax_percentage_x'] = row[idx.get('line_item_tax_percentage_x')].strip()
                # itemsObj['additional_fields']['line_item_tax_type_code_x'] = row[idx.get('line_item_tax_type_code_x')].strip()
                itemsObj['additional_fields']['department_id'] = row[idx.get('line_item_department_id')].strip()
                itemsObj['additional_fields']['department_name'] = row[idx.get('line_item_department_name')].strip()
                itemsObj['additional_fields']['merchandising_account_id'] = row[idx.get('line_item_merchandising_account_id')].strip()
                itemsObj['additional_fields']['merchandising_account_name'] = row[
                    idx.get('line_item_merchandising_account_name')].strip()
                itemsObj['additional_fields']['receipt_id'] = row[idx.get('line_item_receipt_id')].strip()

                ordersObj['lines'].append(itemsObj)
                obj_ordersList.append(ordersObj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)

        for order in obj_ordersList:
            yaml_obj = YamlObject()
            ordersObj = {}
            ordersObj['xml'] = yaml_obj.serialize(_data=order)
            ordersObj['name'] = order['order_no']


            result['orders_list'].append(ordersObj)
        return result
