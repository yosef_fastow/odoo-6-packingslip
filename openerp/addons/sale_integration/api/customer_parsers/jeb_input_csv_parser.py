# -*- coding: utf-8 -*-
import logging
from csv import DictReader
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import traceback
from csv import register_dialect
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from datetime import datetime
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.sale_integration.api.customer_parsers.exceptions import CSVPparserError, CSVLineError

_logger = logging.getLogger(__name__)


class CSV_parser(object):

    ids = {
        'order_id': 'shipmentid',
        'order_date': 'orderdate',
        'carrier': 'ship_method_code',
        'bill_name': 'bill_name',
        'bill_address1': 'bill_address1',
        'bill_address2': 'bill_address2',
        'bill_city': 'bill_city',
        'bill_state': 'bill_state',
        'bill_zip': 'bill_postal_code',
        'bill_country': 'bill_country',
        'bill_phone': 'bill_phone',
        'ship_name': 'ship_name',
        'ship_address1': 'ship_address_1',
        'ship_address2': 'ship_address_2',
        'ship_city': 'ship_city',
        'ship_state': 'ship_state',
        'ship_zip': 'ship_postal',
        'ship_country': 'ship_country',
        'ship_phone': 'ship_phone',
        'line_item_number': 'linenumber',
        'line_delmar_id': 'itemnumber',
        'line_description': 'description',
        'line_qty': 'quantity',
        'line_size': 'size',
        'line_gift_message': 'message',
        'line_cost': 'cost',
        'line_price_unit': 'price_unit',
    }

    def __init__(self, external_customer_id):
        super(CSV_parser, self).__init__()
        self.external_customer_id = external_customer_id

    def _get_order_obj(self, line):
        order_obj = {
            'order_id': line.get(self.ids['order_id'], False),
            'external_date_order': line.get(self.ids['order_date'], False),
            'carrier': line.get(self.ids['carrier'], False),
            'partner_id': self.external_customer_id,
            'address':
            {
                'order': {
                    'name': line.get(self.ids['bill_name'], ''),
                    'address1': line.get(self.ids['bill_address1'], ''),
                    'address2': line.get(self.ids['bill_address2'], ''),
                    'city': line.get(self.ids['bill_city'], ''),
                    'state': line.get(self.ids['bill_state'], ''),
                    'zip': line.get(self.ids['bill_zip'], ''),
                    'country': line.get(self.ids['bill_country'], ''),
                    'phone': line.get(self.ids['bill_phone'], ''),
                },
                'ship': {
                    'name': line.get(self.ids['ship_name'], ''),
                    'address1': line.get(self.ids['ship_address1'], ''),
                    'address2': line.get(self.ids['ship_address2'], ''),
                    'city': line.get(self.ids['ship_city'], ''),
                    'state': line.get(self.ids['ship_state'], ''),
                    'zip': line.get(self.ids['ship_zip'], ''),
                    'country': line.get(self.ids['ship_country'], ''),
                    'phone': line.get(self.ids['ship_phone'], ''),
                },
            },
            'lines': [],
        }
        if(order_obj['external_date_order']):
            try:
                order_obj['external_date_order'] = datetime.strptime(order_obj['external_date_order'], '%Y-%m-%d').date().isoformat()
            except Exception:
                pass
        return order_obj

    def _get_line_obj(self, line):
        line_obj = {
            'id': str(line.get(self.ids['line_item_number'])),
            'delmar_id': line.get(self.ids['line_delmar_id'], False),
            'name': line.get(self.ids['line_description'], False),
            'qty': line.get(self.ids['line_qty'], 1),
            'size': line.get(self.ids['line_size'], False),
            'sku': False,
            'gift_message': line.get(self.ids['line_gift_message'], ''),
            'cost': line.get(self.ids['line_cost'], False),
            'price_unit': line.get(self.ids['line_price_unit'], False),
        }
        for name_value in ['qty']:
            if(line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'int')

            if(line_obj['size']):
                line_obj['size'] = _try_parse(line_obj['size'], 'float')

        if(('/' in line_obj['delmar_id']) and not(line_obj['size'])):
            try:
                _sku = line_obj['delmar_id']
                _size = _try_parse(_sku[_sku.find("/") + 1:], 'float')
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass
        else:
            line_obj['sku'] = line_obj['delmar_id']
        return line_obj

    def _add_line_to_order_obj(self, order_obj, _line):
        line_obj = self._get_line_obj(_line)
        add_new_line = True
        curr_line_id = 1
        for line in order_obj['lines']:
            curr_line_id += 1
            condition = (line['delmar_id'] == line_obj['delmar_id'])
            if(line['size']):
                condition = (condition and(line['size'] == line_obj['size']))
            if(condition):
                if(isinstance(line_obj['qty'], int)):
                    line['qty'] += line_obj['qty']
                else:
                    line['qty'] += 1
                add_new_line = False
                break
        if(add_new_line):
            if(line_obj['id'] is None):
                line_obj['id'] = curr_line_id
            order_obj['lines'].append(line_obj)
        return order_obj

    def _get_lines_from_csv(self, _data):
        lines = []
        _pseudo_file = IO(_data)
        dr = DictReader(_pseudo_file, dialect=self.dialect)
        for row in dr:
            lines.append(row)
        return lines

    def parse_data(self, csv_data):
        result = {
            'errors': [],
            'orders_list': [],
        }
        # ids = self.ids
        list_of_orders_obj = {}
        try:
            register_dialect('pipes', delimiter='|')
            self.dialect = 'pipes'
            if(not self.dialect):
                raise CSVPparserError('DialectError: Unknown Dialect')
            csv_data = strip_lower_del_spaces_from_header(csv_data, self.dialect)
            csv_lines = self._get_lines_from_csv(csv_data)
            _orders = {}
            for count, _line in enumerate(csv_lines):
                _order_id = _line.get(self.ids['order_id'], False)
                if(not _order_id):
                    raise CSVLineError(count, 'CSVLineError: bad line: {0}, not find order id'. format(count))
                if(not _orders.get(_order_id, False)):
                    _orders[_order_id] = []
                _orders[_order_id].append(_line)
            # parse orders
            for order_id in _orders:
                for line in _orders[order_id]:
                    if(not list_of_orders_obj.get(order_id, False)):
                        list_of_orders_obj[order_id] = self._get_order_obj(line)
                    list_of_orders_obj[order_id] = self._add_line_to_order_obj(list_of_orders_obj[order_id], line)
            # collect xml orders list
            yaml_obj = YamlObject()
            for order_id in list_of_orders_obj:
                if(not result['errors']):
                    result_order_obj = {
                        'name': order_id,
                        'xml': yaml_obj.serialize(_data=list_of_orders_obj[order_id])
                    }
                    result['orders_list'].append(result_order_obj)
        except (CSVLineError, CSVPparserError) as csv_err:
            result['errors'].append(csv_err.message)
        except Exception:
            _msg = traceback.format_exc()
            result['errors'].append(_msg)
        return result
