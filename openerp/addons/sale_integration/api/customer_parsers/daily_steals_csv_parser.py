# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from element_with_xpath import CSVElementWithXpath, xpathRule


class BaseCSVParser(BaseParser):

    def __init__(self, parent):
        super(BaseCSVParser, self).__init__(parent)

    def split_csv_by_order(self, csv_data, identity):
        identity = identity.strip().lower().replace(' ', '')
        csv_data = strip_lower_del_spaces_from_header(csv_data, 'excel')
        lines = []
        _pseudo_file = IO(csv_data)
        dr = DictReader(_pseudo_file, dialect='excel')
        for row in dr:
            lines.append(row)
        orders = {}
        for line in lines:
            if line[identity] not in orders:
                orders[line[identity]] = []
            orders[line[identity]].append(line)
        return orders


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        xpath = xpath.strip().lower().replace(' ', '')
        keys = xpath.split('|')
        return [Line([{x: y for x, y in line.items() if x in keys}]).dict for line in res]


class Line(CSVElementWithXpath):

    xpaths = {
        'id': ('str', ''),
        'external_customer_line_id': ('str', ''),
        'sku': ('str', 'VendorPartNo'),
        'name': ('str', 'Description'),
        'qty': ('int', 'Qty'),
        'customerCost': ('float', 'Item Wholesale'),
        'additional_fields': {
            'item_status': ('str', 'Item Status'),
        }
    }


class Order(CSVElementWithXpath):

    xpaths = {
        'order_id': ('str', 'OrderNo'),
        'po_number': ('str', 'OrderNo'),
        'external_date_order': ('datetime', 'PO Date'),
        'address': {
            'ship': {
                'first_name': ('str', 'ShipToName'),
                'last_name': ('str', 'Shipping Last Name'),
                'company': ('str', 'Shipping Company'),
                'address1': ('str', 'ShipToAddress'),
                'address2': ('str', 'ShipToAddress2'),
                'city': ('str', 'ShipToCity'),
                'state': ('str', 'ShipToState'),
                'zip': ('str', 'ShipToZip'),
                'country': ('str', 'Shipping Country'),
                'phone': ('str', 'Shipping Phone'),
            },
        },
        'carrier': ('str', 'Shipping Method'),
        'additional_fields': {
            'po_sent': ('datetime', 'PO Sent'),
            'po_confirmation_time': ('datetime', 'PO Confirmation Time'),
            'retailer': ('str', 'Retailer'),
            'gift_note': ('str', 'Gift note'),
            'customer_note': ('str', 'Customer Note'),
            'promise_date': ('datetime', 'Promise Date'),
            'ship_date': ('datetime', 'Ship Date'),
            'original_address': {
                'ship': {
                    'first_name': ('str', 'Shipping First Name'),
                    'last_name': ('str', 'Shipping Last Name'),
                    'company': ('str', 'Shipping Company'),
                    'address1': ('str', 'Shipping Address 1'),
                    'address2': ('str', 'Shipping Address 2'),
                    'city': ('str', 'Shipping City'),
                    'state': ('str', 'Shipping State or Province'),
                    'zip': ('str', 'Shipping Post Code'),
                    'country': ('str', 'Shipping Country'),
                    'phone': ('str', 'Shipping Phone'),
                },
            },
        },
        'lines': ('Line', 'Vendor SKU|Item Name|Item Quantity|Item Wholesale|Item Status'),
    }

    def __init__(self, csv_lines):
        super(Order, self).__init__(csv_lines)
        self.custom_xpath_rules = {
            'Line': LineXpathRule,
        }
        self.inject_custom_xpath_rules()

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        ship_addr = res['address']['ship']
        ship_addr['name'] = ship_addr['first_name'] or ''
        if ship_addr['last_name']:
            ship_addr['name'] += ' ' + ship_addr['last_name']
        del ship_addr['first_name']
        del ship_addr['last_name']
        del ship_addr['company']
        for i, line in enumerate(res['lines']):
            line['id'] = line['external_customer_line_id'] = i + 1
        return res


class CSVParser(BaseCSVParser):

    def __init__(self, parent):
        super(CSVParser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, csv_data):
        orders = self.split_csv_by_order(csv_data, 'OrderNo')
        for order_name, order_data in orders.items():
            self.orders.append(Order(order_data).dict)
        return True
