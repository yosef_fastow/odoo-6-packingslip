# -*- coding: utf-8 -*-
from element_with_xpath import xpathRule
from pf_utils.utils.re_utils import str2date
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from base_edi import (
    BaseEDIParser,
    ediElementWithXpath,
    AddressEdiElementWithXpath,
    address_type_map,
)
import re

class AddressXpathRule(xpathRule):
    __type_res__ = 'Address'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).dict)
        return result


class OriginalAddressesXpathRule(xpathRule):
    __type_res__ = 'original_addresses'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).get_original_dict())
        return result


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        return [Line(line).dict for line in res]


class Line(ediElementWithXpath):

    xpaths = {
        'id': ('str', 'PO1.1'),
        'qty': ('int', 'PO1.2'),
        'cost': ('float', 'PO1.4'),
        'retail_cost': ('float', 'CTP:RS:RTL.3'),
        'customer_sku': ('str', 'PO1:*:*:*:*:*:SK.7'),
        'sku': ('str', 'PO1:*:*:*:*:*:SK.7'),
        'vendor_sku': ('str', 'PO1:*:*:*:*:*:*:*:VN.9'),
        'upc': ('str', 'PO1:*:*:*:*:*:*:*:*:*:UP.11'),
        'optionSku': ('str', 'PO1:*:*:*:*:*:*:*:*:*:UP.11'),
        'size': ('str', 'PID:F:35.5'),
        'name': ('str', 'PID:F:08.5'),
        'additional_fields': {
            'color': ('str', 'PID:F:35:ZZ:.5'),
            'appearance': ('str', 'PID:F:85:ZZ::.5'),
            'marking': ('str', 'PID:F:MB:ZZ:.5'),
            'mutually_defined': ('str', 'PID:F:ZZ:ZZ:.5'),
        },
    }


class Address(AddressEdiElementWithXpath):

    xpaths = {
        'type': ('str', 'N1.1'),
        'name': ('str', 'N1.2'),
        'last_name': ('str', 'N2.1'),
        'address1': ('str', 'N3->0.1'),
        'address2': ('str', 'N3->0.2'),
        'address3': ('str', 'N3->1.1'),
        'address4': ('str', 'N3->1.2'),
        'city': ('str', 'N4.1'),
        'state': ('str', 'N4.2'),
        'zip': ('str', 'N4.3'),
        'country': ('str', 'N4.4'),
        'contact_data': {
            'name': ('str', 'PER:OC.2'),
            'phone': ('str', 'PER:OC:*:TE.4'),
        },
    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res_type = address_type_map.get(res['type'], 'order')
        del res['type']
        # BAD decision, but it is better than US for all orders
        alphanumeric_regex = re.compile('^[0-9a-zA-Z\s]+$')
        numeric_regex = re.compile('^[0-9\s]+$')
        if bool(numeric_regex.match(res['zip'])):
            country = 'US'
        elif bool(alphanumeric_regex.match(res['zip'])):
            country = 'CA'
        else:
            country = res['country']
        result = {
            res_type: {
                'name': ' '.join(
                    [
                        value for key, value in res.items()
                        if key in ['name', 'last_name', 'middle_name'] and value
                    ]
                ),
                'address1': res['address1'],
                'address2': res['address2'],
                # TODO: need to store it or use?
                # 'address3': res['address3'],
                # 'address4': res['address4'],
                'city': res['city'],
                'state': res['state'],
                'zip': res['zip'],
                'country': country,
                'phone': res.get('contact_data', {}).get('phone', None),
            }
        }
        return result


class Order(ediElementWithXpath):

    xpaths = {
        'poNumber': ('str', 'BEG:00:DS.3'),
        'order_id': ('str', 'BEG:00:DS.3'),
        'external_date_order': ('datetime', 'BEG:00:DS.5'),
        'external_customer_order_id': ('str', 'REF:CO.2'),
        'address': ('Address', 'N1<-PER(N2,N3,N4)'),
        'carrier': ('str', 'TD5:*:92.3'),
        'ship_service': ('str', 'TD5:*:92:*:*.5'),
        'ship_method': ('str', 'TD5:*:92:*:*.5'),
        'additional_fields': {
            'division_identifier': ('str', 'REF:19.2'),
            'special_payment_reference_number': ('str', 'REF:4N.2'),
            'internal_vendor_number': ('str', 'REF:IA.2'),
            'promotion_number': ('str', 'REF:PD.2'),
            'special_packaging_instruction_number': ('str', 'REF:W9.2'),
            'sales_tax': ('str', 'AMT:F7.2'),
            'handling_charges ': ('str', 'AMT:OH.2'),
            'attachment_code': ('str', 'REF:E9.2'),
            'shipping_label_serial_number ': ('str', 'REF:LA.2'),
            'text_information': ('str', 'N9:TOC.2'),
            'msg1': ('str', 'MSG.1'),
            'ship_service': ('str', 'TD5:*:92:*:*.5'),
            'ship_method': ('str', 'TD5:*:92:*:*.5'),
            'vendor_guide': ('datetime', 'DTM:010.2'),
            'store_number': ('datetime', 'ST:850:.2'),
            # 'status_messages': (N9Msg, 'N9<-MSG'),
        },
        'lines': ('Line', 'PO1<-TC2(CTP,PID,PO4,SDQ)'),
        'poHdrData': {
            'gift_message': ('str', 'MSG.1'),

            'gift_flag': (bool, 'REF:W9.2'),
        },
        'number_of_transaction': ('str', 'GE:*:2'),
        'number_of_included_functional_group': ('str', 'IEA.1'),
    }
    #todo gift_flag
    # todo gitf message
    def __init__(self, lxml_etree_Element):
        super(Order, self).__init__(lxml_etree_Element)
        self.custom_xpath_rules = {
            'Address': AddressXpathRule,
            'original_addresses': OriginalAddressesXpathRule,
            'Line': LineXpathRule,
        }

        self.inject_custom_xpath_rules()


class SaksRadialEDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(SaksRadialEDIParser, self).__init__(parent)
        self.Order = Order
