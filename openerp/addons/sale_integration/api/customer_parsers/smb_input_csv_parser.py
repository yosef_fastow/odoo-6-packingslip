# -*- coding: utf-8 -*-
import logging
from csv import DictReader as csv_reader
from cStringIO import StringIO
from datetime import datetime
import traceback
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.pf_utils.utils.import_orders_normalize import get_starting_chunk
from openerp.addons.pf_utils.utils.import_orders_normalize import is_binary_string
from openerp.addons.pf_utils.utils.xls_to_csv import xls_data_to_csv
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError, DelmarIDError

_logger = logging.getLogger(__name__)


class ParseLineObj(object):

    def __init__(self, ids, line):
        self.ids = ids
        self.line = line

    def get_qty(self):
        qty = 1
        raw_qty = self.get_value('line_qty', False)
        if(not raw_qty):
            qty = 1
        else:
            raw_qty = str(raw_qty)
            if(('.' in raw_qty) or (',' in raw_qty)):
                raw_qty = raw_qty.replace(',', '.')
            try:
                qty = int(float(raw_qty))
            except ValueError:
                qty = 1
        return qty

    def get_value(self, name, default=-1):
        result = None
        try:
            result = self.line[self.ids[name]]
        except KeyError:
            if(default != -1):
                result = default
            else:
                result = self.ids[name]
        finally:
            return result

    def get_order_obj(self, external_customer_id):
        order_obj = {
            'order_id': self.get_value('order_id', False),
            'batch_po_number': self.get_value('batch_po_number', False),
            'merchant_id': self.get_value('merchant_id', False),
            'partner_id': external_customer_id,
            'external_date_order': datetime.strptime(self.get_value('order_date', False), '%m/%d/%Y').date().isoformat(),
            'carrier': self.get_value('carrier', False),
            'ship_method': self.get_value('ship_method', False),
            'tax': self.get_value('tax', False),
            'lines': [],
        }
        order_obj['address'] = {}
        order_obj['address']['ship'] = {}
        order_obj['address']['ship']['name'] = self.get_value('name', False)
        order_obj['address']['ship']['address1'] = self.get_value('address1', False)
        order_obj['address']['ship']['address2'] = self.get_value('address2', False)
        order_obj['address']['ship']['city'] = self.get_value('city', False)
        order_obj['address']['ship']['state'] = self.get_value('state', False)
        order_obj['address']['ship']['zip'] = self.get_value('zip', False)
        order_obj['address']['ship']['country'] = self.get_value('country', False)
        order_obj['address']['ship']['phone'] = self.get_value('phone', False)
        return order_obj

    def get_line_obj(self):
        line_obj = {
            'id': self.get_value('line_item_number', False),
            'sku': self.get_value('sku', False),
            'customer_sku': self.get_value('line_customer_sku', False),
            'name': self.get_value('line_description', False),
            'size': self.get_value('line_size', ""),
            'qty': self.get_qty(),
            'price_unit': self.get_value('line_price_unit', False),
            'unit_cost': self.get_value('line_unit_cost', False),
            'cost': self.get_value('line_price_unit', False),
            'item_count': self.get_value('line_item_count', False),
            'gift_message': self.get_value('gift_message', False),
            'line_declared_value': self.get_value('line_declared_value', False),
            'line_vendor_sku': self.get_value('line_vendor_sku', False),
            'batch_po_number': self.get_value('batch_po_number', False),
        }
        for name_value in ['id', 'qty', 'item_count']:
            try:
                temp = int(line_obj[name_value])
            except ValueError:
                temp = line_obj[name_value]
            line_obj[name_value] = temp
        for name_value in ['price_unit', 'unit_cost', 'cost']:
            try:
                temp = float(line_obj[name_value])
            except ValueError:
                temp = line_obj[name_value]
            line_obj[name_value] = temp
        line_obj['notes'] = 'id: {0}\nQTY: {1}\nProduct: {2}\nSize: {3}\n'.format(
            *tuple(
                [line_obj.get(x, 'None') for x in ['id', 'qty', 'sku', 'size']]
            )
        )
        return line_obj


class CSVParser():

    ids = {
        'ship_from': 'ShipFrom',
        'line_vendor_sku': 'SKU',
        'batch_po_number': 'BatchPONumber',
        'line_declared_value': 'DeclaredValue',
        'ship_company': 'Shipping_Company',
        'merchant_id': 'MerchantID',
        'address1': 'Shipping_Address1',
        'address2': 'Shipping_Address2',
        'city': 'Shipping_City',
        'country': 'Shipping_Country',
        'line_description': 'Description',
        'line_gift_message': 'GiftMessage',
        'partner_id': 'Merchant ID',
        'line_customer_sku': 'VendorSKU',
        'name': 'Shipping_Name',
        'order_date': 'Batch Date',
        'order_id': 'Order_id',
        'phone': 'Phone',
        'line_qty': 'Quantity',
        'carrier': 'Shipping_Code',
        'ship_method': 'Ship Method',
        'line_size': 'Size',
        'state': 'Shipping_State',
        'line_tax': 'Tax',
        'line_price_unit': 'Price',
        'line_unit_cost': 'Unit Cost',
        'sku': 'VendorSKU',
        'zip': 'Shipping_Zip',
        'line_item_number': 'Item Line Number',
        'tax': 'Tax',
        'line_item_count': 'Item Count',
    }

    def __init__(self, external_customer_id, delimiter=',', quotechar='"', ids=None):
        if(ids is not None):
            self.ids = ids
        self.external_customer_id = external_customer_id
        self.delimiter = delimiter
        self.quotechar = quotechar

    def parse_data(self, data):
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        chunk = get_starting_chunk(data=data)
        if(is_binary_string(chunk)):
            try:
                csv_data = xls_data_to_csv(data, raise_exceptions=True)
            except Exception:
                msg = traceback.format_exc()
                result['errors'].append(msg)
                return result
        else:
            csv_data = data
        _order_obj = {}
        order_id_old = ""
        obj_orders_list = []
        curr_line_id = 0
        try:
            rdr = csv_reader(StringIO(csv_data), delimiter=self.delimiter, quotechar=self.quotechar)
            for rec in rdr:
                rec_obj = ParseLineObj(self.ids, rec)
                if(not _order_obj.get('order_id', False) or order_id_old != rec_obj.get_value('order_id', False)):
                    _order_obj = rec_obj.get_order_obj(self.external_customer_id)
                if(not _order_obj['order_id']):
                    raise OrderIDError("Order ID not find in line: {0}".format(str(rec_obj.line)))
                if(_order_obj.get('order_id', False) and order_id_old != rec_obj.get_value('order_id', False)):
                    obj_orders_list.append(_order_obj)
                    order_id_old = _order_obj['order_id']
                add_new_line = True
                _line_obj = rec_obj.get_line_obj()
                if(not _line_obj['sku']):
                    raise DelmarIDError("Not find Delmar ID in line: {0}".format(str(rec_obj.line)))
                if(_line_obj['qty']):
                    try:
                        _line_obj['qty'] = int(_line_obj['qty'])
                    except ValueError:
                        _line_obj['qty'] = False
                for line in _order_obj['lines']:
                    if((line['sku'] == _line_obj['sku']) and _line_obj['size'] and (_line_obj['size'] == line['size'])):
                        if((_line_obj['qty'] is not False) and (_line_obj['qty'] != 1) and (isinstance(_line_obj['qty'], int))):
                            line['qty'] += _line_obj['qty']
                        else:
                            line['qty'] += 1
                        line["notes"] = "id: %s\nQTY: %s\nProduct: %s" % tuple(
                            [line.get(str(x), 'None') for x in ['id', 'qty', 'sku']]
                        )
                        add_new_line = False
                        break
                if(add_new_line):
                    curr_line_id += 1
                    if(not _line_obj['id']):
                        _line_obj['id'] = curr_line_id
                    _order_obj['lines'].append(_line_obj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        yaml_obj = YamlObject()
        for order in obj_orders_list:
            returned_order = {}
            try:
                returned_order['name'] = order['order_id']
                returned_order['xml'] = yaml_obj.serialize(_data=order)
                result['orders_list'].append(returned_order)
            except Exception:
                msg = traceback.format_exc()
                result['errors'].append(msg)
        return result
