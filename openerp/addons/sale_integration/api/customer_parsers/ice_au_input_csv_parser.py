# -*- coding: utf-8 -*-
import logging
from csv import DictReader as csv_reader
from cStringIO import StringIO
from datetime import datetime
import traceback
from openerp.addons.pf_utils.utils.yamlutils import YamlObject

_logger = logging.getLogger(__name__)


class ParseLineObj(object):

    def __init__(self, ids, line):
        self.ids = ids
        self.line = line

    def get_value(self, name, default=-1):
        result = None
        try:
            result = self.line[self.ids[name]]
        except KeyError:
            if(default != -1):
                result = default
            else:
                result = self.ids[name]
        finally:
            return result


class CSVParser():

    ids = {
        'address1': 'Address1',
        'address2': 'Address2',
        'city': 'City',
        'country': 'Country',
        'line_description': 'Description',
        'line_gift_message': 'GiftMessage',
        'partner_id': 'Merchant ID',
        'line_customer_sku': 'Merchant SKU',
        'name': 'Name',
        'order_date': 'Order Date',
        'order_id': 'Order Number',
        'phone': 'Phone',
        'line_qty': 'Quantity',
        'carrier': 'Service Type',
        'line_size': 'Size',
        'state': 'State',
        'line_tax': 'Tax',
        'line_price_unit': 'Unit Price',
        'line_delmar_id': 'Vendor SKU',
        'zip': 'Zip',
    }

    def __init__(self, external_customer_id, delimiter=',', quotechar='"', bulk_carriers_list=None, ids=None):
        if(ids is not None):
            self.ids = ids
        if(bulk_carriers_list is None):
            bulk_carriers_list = []
        self.external_customer_id = external_customer_id
        self.delimiter = delimiter
        self.quotechar = quotechar
        self.bulk_carriers_list = bulk_carriers_list

    def parse_data(self, csv_data):
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        order_obj = {}
        obj_orders_list = []
        order_id_old = ""
        line_id = 0
        try:
            rdr = csv_reader(StringIO(csv_data), delimiter=self.delimiter, quotechar=self.quotechar)
            for rec in rdr:
                rec_obj = ParseLineObj(self.ids, rec)
                if(not order_obj.get('order_id', False)):
                    order_obj['order_id'] = rec_obj.get_value('order_id', False)
                elif(order_obj.get('order_id', False) and order_id_old != rec_obj.get_value('order_id', False)):
                    order_id_old = ""
                    obj_orders_list.append(order_obj)
                    order_obj = {}
                    order_obj['order_id'] = rec_obj.get_value('order_id')

                if(order_obj.get('order_id', False) and order_id_old != order_obj.get('order_id', False)):
                    order_obj['partner_id'] = self.external_customer_id
                    order_obj['external_date_order'] = datetime.strptime(rec_obj.get_value('order_date'), '%d/%m/%Y').date().isoformat()
                    order_obj['address'] = {}
                    order_obj['address']['ship'] = {}
                    order_obj['address']['ship']['name'] = rec_obj.get_value('name')
                    order_obj['address']['ship']['address1'] = rec_obj.get_value('address1')
                    order_obj['address']['ship']['address2'] = rec_obj.get_value('address2')
                    order_obj['address']['ship']['city'] = rec_obj.get_value('city')
                    order_obj['address']['ship']['state'] = rec_obj.get_value('state')
                    order_obj['address']['ship']['zip'] = rec_obj.get_value('zip')
                    order_obj['address']['ship']['country'] = rec_obj.get_value('country')
                    order_obj['address']['ship']['phone'] = rec_obj.get_value('phone')
                    order_obj['carrier'] = rec_obj.get_value('carrier').replace(' ', '')
                    if(order_obj['carrier'] == self.ids['carrier']):
                        order_obj['carrier'] = 'Landmark'
                    if((order_obj['carrier'] in self.bulk_carriers_list) or (order_obj['carrier'] == '')):
                        order_obj['bulk_transfer'] = True
                    else:
                        order_obj['bulk_transfer'] = False
                    if(rec_obj.get_value('tax_percentage', False)):
                        order_obj['tax_percentage'] = rec_obj.get_value('tax_percentage')
                        order_obj['shipping_fee'] = rec_obj.get_value('shipping_fee')
                        order_obj['shipping_tax'] = rec_obj.get_value('shipping_tax')
                        order_obj['total_due'] = rec_obj.get_value('total_due')
                    order_id_old = order_obj['order_id']
                    order_obj['lines'] = []
                items_obj = {}
                line_id += 1
                items_obj['id'] = line_id
                items_obj['merchant_sku'] = str(rec_obj.get_value('line_customer_sku'))
                items_obj['qty'] = int(str(rec_obj.get_value('line_qty')).replace('.0000', ''))
                if(not items_obj['qty']):
                    _logger.error('Cannot find "Quantity" column in csv (order_id: %s)' % (str(items_obj['id'])))
                items_obj['vendor_sku'] = rec_obj.get_value('line_delmar_id')
                items_obj['size'] = rec_obj.get_value('line_size')
                items_obj['name'] = rec_obj.get_value('line_description')
                items_obj['cost'] = rec_obj.get_value('line_price_unit')

                if(rec_obj.get_value('line_subtotal', False)):
                    items_obj['sub_total'] = rec_obj.get_value('line_subtotal')
                    items_obj['tax_amount'] = rec_obj.get_value('line_tax_amount')
                    items_obj['discount'] = rec_obj.get_value('line_discount_tax')
                    items_obj['row_total'] = rec_obj.get_value('line_row_total')

                items_obj['gift_message'] = rec_obj.get_value('line_gift_message')
                order_obj['lines'].append(items_obj)
            obj_orders_list.append(order_obj)

        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)

        yaml_obj = YamlObject()
        for order in obj_orders_list:
            returned_order = {}
            try:
                returned_order['name'] = order['order_id']
                returned_order['xml'] = yaml_obj.serialize(_data=order)
                result['orders_list'].append(returned_order)
            except Exception:
                msg = traceback.format_exc()
                result['errors'].append(msg)

        return result
