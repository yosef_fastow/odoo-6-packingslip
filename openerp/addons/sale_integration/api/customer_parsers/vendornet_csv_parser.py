import csv
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import cStringIO
import traceback
from openerp.addons.pf_utils.utils.import_orders_normalize import fix_line_terminators


class VendornetApiGetOrdersXML(object):

    def __init__(self, parent):
        self.parent = parent

    def parse_data(self, data):
        data = fix_line_terminators(data)
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        if(not data):
            return []
        obj_ordersList = []
        try:
            obj = list(csv.reader(cStringIO.StringIO(data), delimiter=',', quotechar='"', lineterminator='\n'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)

            if(self.parent.customer == 'aafes'):
                _order_number = 'po_no'
                _po_number    = 'order_no'
            else:
                _order_number = 'order_no'
                _po_number    = 'po_no'

            po_number_old = ""
            order_id_old = ""
            ordersObj = {}

            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()

                if(ordersObj.get('order_no', False) and order_id_old != row[idx.get(_order_number)].strip() and self.parent.customer != 'aafes') :
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                    ordersObj['po_no'] = row[idx.get(_po_number)].strip()

                if not ordersObj.get('po_no', False):
                    ordersObj['po_no'] = row[idx.get(_po_number)].strip()
                elif (ordersObj.get('po_no', False) and po_number_old != row[idx.get(_po_number)].strip() and self.parent.customer == 'aafes'):
                    po_number_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                    ordersObj['po_no'] = row[idx.get(_po_number)].strip()

                if(ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False) and self.parent.customer != 'aafes') or (ordersObj.get('po_no', False) and po_number_old != row[idx.get(_po_number)].strip() and self.parent.customer == 'aafes'):

                    ordersObj['sold'] = {}
                    ordersObj['sold']['soldcity'] = row[idx.get('soldcity')].strip()
                    ordersObj['sold']['soldstate'] = row[idx.get('soldstate')].strip()
                    ordersObj['sold']['soldzip'] = row[idx.get('soldzip')].strip()
                    ordersObj['sold']['soldphone'] = row[idx.get('soldphone')].strip()
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['name'] = row[idx.get('shipname')].strip()
                    ordersObj['address']['ship']['address1'] = row[idx.get('shipadd1')].strip()
                    if ordersObj['address']['ship']['address1']:
                        ordersObj['address']['ship']['address2'] = row[idx.get('shipadd2')].strip()
                    else:
                        ordersObj['address']['ship']['address1'] = row[idx.get('shipadd2')].strip()
                        ordersObj['address']['ship']['address2'] = row[idx.get('shipadd1')].strip()
                    ordersObj['address']['ship']['city'] = row[idx.get('shipcity')].strip()
                    ordersObj['address']['ship']['state'] = row[idx.get('shipstate')].strip()
                    ordersObj['address']['ship']['zip'] = row[idx.get('shipzip')].strip()
                    ordersObj['address']['ship']['country'] = ''
                    ordersObj['address']['ship']['phone'] = row[idx.get('shipphone')].strip()

                    if (self.parent.customer == 'aafes'):
                        ordersObj['address']['order'] = {}
                        ordersObj['address']['order']['name'] = row[idx.get('soldname')].strip()
                        ordersObj['address']['order']['address1'] = row[idx.get('soldadd1')].strip()
                        ordersObj['address']['order']['address2'] = row[idx.get('soldadd2')].strip()
                        ordersObj['address']['order']['city'] = row[idx.get('soldcity')].strip()
                        ordersObj['address']['order']['state'] = row[idx.get('soldstate')].strip()
                        ordersObj['address']['order']['zip'] = row[idx.get('soldzip')].strip()
                        ordersObj['address']['order']['country'] = ''
                        ordersObj['address']['order']['phone'] = row[idx.get('soldphone')].strip()

                    ordersObj['ship_via'] = row[idx.get('ship_via')].strip()
                    ordersObj['date'] = row[idx.get('m/d/yyyy')].strip()
                    # ordersObj['po_no'] = row[idx.get(_po_number)].strip()
                    po_number_old = ordersObj['po_no'].strip()
                    ordersObj['partner_id'] = self.parent.customer
                    ordersObj['order_id'] = ordersObj['order_no']
                    ordersObj['poNumber'] = ordersObj['po_no']
                    ordersObj['carrier'] = row[idx.get('ship_via')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []

                itemsObj = {}
                itemsObj['id'] = row[idx.get('item')].strip()
                itemsObj['desc'] = row[idx.get('desc')].strip()
                itemsObj['name'] = itemsObj['desc']
                itemsObj['qty_shippe'] = row[idx.get('qty_shippe')].strip()
                itemsObj['vendorSku'] = row[idx.get('vendor_sku')].strip()
                itemsObj['merchantSKU'] = itemsObj['vendorSku']
                itemsObj['store'] = row[idx.get('store')].strip()
                itemsObj['size'] = row[idx.get('size')].strip()
                itemsObj['qty'] = row[idx.get('qty')].strip()
                itemsObj['cost'] = False
                itemsObj['customerCost'] = 0.0

                itemsObj['poHdrData'] = {}
                itemsObj['poHdrData']['giftIndicator'] = True if row[idx.get('GIFT_INDICATOR')].strip()=='Yes' else False
                itemsObj['poHdrData']['gift_message'] = row[idx.get('LINE_COMMENT3')].strip()
                itemsObj['additional_fields'] = {}
                itemsObj['additional_fields']['giftIndicator'] = True if row[idx.get('GIFT_INDICATOR')].strip()=='Yes' else False
                itemsObj['additional_fields']['LINE_COMMENT3'] = row[idx.get('LINE_COMMENT3')].strip()
                itemsObj['additional_fields']['LINE_COMMENT1'] = row[idx.get('LINE_COMMENT1')].strip()
                itemsObj['additional_fields']['LINE_COMMENT2'] = row[idx.get('LINE_COMMENT2')].strip()
                itemsObj['additional_fields']['LINE_COMMENT4'] = row[idx.get('LINE_COMMENT4')].strip()
                itemsObj['additional_fields']['GIFT_WRAP_XREF'] = row[idx.get('GIFT_WRAP_XREF')].strip()

                if (self.parent.customer == 'aafes'):
                    itemsObj['merchantSKU'] = row[idx.get('item')].strip()
                    itemsObj['customerCost'] = row[idx.get('retail_price')].strip()
                    itemsObj['cost'] = row[idx.get('unit_price')].strip()
                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        for order in obj_ordersList:
            yaml_obj = YamlObject()
            ordersObj = {}
            ordersObj['xml'] = yaml_obj.serialize(_data=order)
            ordersObj['name'] = order[_order_number]
            result['orders_list'].append(ordersObj)
        return result
