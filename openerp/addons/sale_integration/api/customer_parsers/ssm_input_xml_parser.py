# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result
from element_with_xpath import lxmlElementWithXpath, xpathRule
from openerp.addons.pf_utils.utils.xml_utils import clean_namespaces
from lxml.etree import parse
from io import BytesIO


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        return [Line(line).dict for line in res]


class Line(lxmlElementWithXpath):

    xpaths = {
        'id': ('str', 'po-line-header/line-number'),
        'external_customer_line_id': ('str', 'po-line-header/item-id'),
        'sku': ('str', 'po-line-header/item-id'),
        'customer_sku': ('str', 'po-line-detail/partnerSKU'),
        'name': ('str', 'po-line-header/item-name'),
        'qty': ('int', 'po-line-detail/quantity'),
        'price_unit': ('float', 'po-line-header/selling-price-each'),
        'shipCost': ('float', 'po-line-header/unit-cost'),
        'additional_fields': {
            'line-number': ('str', 'po-line-header/line-number'),
            'item-id': ('str', 'po-line-header/item-id'),
            'commission': ('float', 'po-line-header/commission'),
            'order-quantity': ('int', 'po-line-header/order-quantity'),
            'shipping-and-handling': ('float', 'po-line-header/shipping-and-handling'),
            'customer-cancellation-warning': ('str', 'po-line-header/customer-cancellation-warning'),
            'po-line-status': ('str', 'po-line-detail/po-line-status'),
        }
    }

    def __init__(self, lxml_etree_Element):
        super(Line, self).__init__(lxml_etree_Element)


class Order(lxmlElementWithXpath):

    xpaths = {
        'order_id': ('str', 'po-number'),
        'external_customer_order_id': ('str', 'customer-order-confirmation-number'),
        'external_date_order': ('datetime', 'po-date'),
        'address': {
            'ship': {
                'name': ('str', 'shipping-detail/ship-to-name'),
                'address1': ('str', 'shipping-detail/address'),
                'address2': ('str', 'shipping-detail/address2'),
                'city': ('str', 'shipping-detail/city'),
                'state': ('str', 'shipping-detail/state'),
                'zip': ('str', 'shipping-detail/zipcode'),
                'country': ('str', 'shipping-detail/country'),
                'phone': ('str', 'shipping-detail/phone'),
            },
        },
        'carrier': ('static', ''),
        'additional_fields': {
            'customer-email': ('str', 'customer-email'),
            'po-date': ('str', 'po-date'),
            'po-time': ('str', 'po-time'),
            'po-number-with-date': ('str', 'po-number-with-date'),
            'unit': ('str', 'unit'),
            'site': ('str', 'site'),
            'channel': ('str', 'channel'),
            'location-id': ('str', 'location-id'),
            'expected-ship-date': ('datetime', 'expected-ship-date'),
            'customer-name': ('str', 'customer-name'),
            'shipping-detail': {
                'shipping-method': ('str', 'shipping-detail/shipping-method'),
                'carrier': ('str', 'shipping-detail/carrier'),
            },
            'order-total-sell-price': ('float', 'order-total-sell-price'),
            'total-commission': ('float', 'total-commission'),
            'total-shipping-handling': ('float', 'total-shipping-handling'),
            'balance-due': ('float', 'balance-due'),
            'sales-tax': ('float', 'sales-tax'),
            'po-status': ('str', 'po-status'),
        },
        'lines': ('Line', 'po-line'),
    }

    def __init__(self, lxml_etree_Element):
        super(Order, self).__init__(lxml_etree_Element)
        self.custom_xpath_rules = {
            'Line': LineXpathRule,
        }
        self.inject_custom_xpath_rules()

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        sd = res['additional_fields']['shipping-detail']
        res.update({
            'carrier': '|'.join(
                [
                    sd['shipping-method'] or '',
                    sd['carrier'] or ''
                ]
            )
        })
        return res


class XML_parser(BaseParser):
    """:class: for parsing input XML data to Yaml object"""
    def __init__(self, parent):
        super(XML_parser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, xml_data):
        xml_data = clean_namespaces(xml_data)
        response = parse(BytesIO(xml_data))
        for purchase_order in response.findall('purchase-order'):
            self.orders.append(Order(purchase_order).dict)
        return True
