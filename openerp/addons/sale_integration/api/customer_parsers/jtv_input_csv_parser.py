# -*- coding: utf-8 -*-
import logging
from osv import osv
from csv import DictReader as csv_reader
from cStringIO import StringIO
import json
import re
import traceback
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError, DelmarIDError

_logger = logging.getLogger(__name__)


class ParseLineObj(object):

    def __init__(self):
        pass

    def get_value(self, name, default=-1):
        result = None
        try:
            result = self.line[self.ids[name]]
        except KeyError:
            if(default != -1):
                result = default
            else:
                result = self.ids[name]
        finally:
            return result

    def get_partial_order_obj(self, order_matrix, _map):
        order_data = {}

        for key in _map.keys():
            line_num = _map[key]['line']
            cell_num = _map[key]['cell']
            fields_list = _map[key].get('fields', False)
            separator = _map[key].get('separator', False)
            cell_value = order_matrix[line_num][cell_num].strip()

            if fields_list and separator:
                # If value from csv has few order fields
                cell_value = re.sub(' +', ' ', cell_value)
                fields_list = fields_list.split(',')
                value_list = cell_value.split(separator)
                for i in range(0,len(fields_list)):
                    order_data.update({fields_list[i]:value_list[i].strip(', .')})
            else:
                order_data.update({key:cell_value.strip(', .')})

        return order_data

    def get_order_obj(self, order_matrix, _map):
        order_map = _map['order_info']
        address_map = _map['address']
        additional_fields = _map['order_additional_fields']
        order_data = {
            'additional_fields':{},
            'address': {'ship': {}},
            'lines': []
        }

        order_data.update(self.get_partial_order_obj(order_matrix, order_map))
        order_data['additional_fields'].update(self.get_partial_order_obj(order_matrix, additional_fields))
        order_data['address']['ship'].update(self.get_partial_order_obj(order_matrix, address_map))

        return order_data

    def get_line_obj(self, order_matrix, order_map):
        order_line_data = []
        item_end_pos = order_map['item_end_pos']
        shift_index = order_map['shift_index']
        _map = order_map['item_info']
        
        line_counter = 0
        # Get last position and count lines
        for i in range(0,100):
            line_counter = i
            shift = i * shift_index
            line_pos = item_end_pos['line'] + shift
            cell_pos = item_end_pos['cell']
            if order_matrix[line_pos][cell_pos] == item_end_pos['text']:
                break

        # Get items info
        for i in range(0,line_counter+1):
            shift = i * shift_index
            line_data = {
                'additional_fields':{}
            }
            for key in _map.keys():
                line_num = _map[key]['line'] + shift
                cell_num = _map[key]['cell']
                _type = _map[key].get('type')
                is_additional = _map[key].get('additional')

                cell_value = order_matrix[line_num][cell_num].strip()
                if _type:
                    try:
                        if _type == 'float':
                            cell_value = float(cell_value or 0)
                        elif _type == 'int':
                            cell_value = int(cell_value or 0)
                    except ValueError:
                        raise osv.except_osv(('ERROR !'), ("The %s value of %s column must be %s" % (cell_value, key, _type)))
                if is_additional:
                    line_data['additional_fields'].update({key:cell_value})
                else:
                    line_data.update({key:cell_value})

            order_line_data.append(line_data)

        return order_line_data


class CSVParser():

    def __init__(self, external_customer_id, settings):
        self.external_customer_id = external_customer_id
        self.dialect = None
        self.order_map = settings.get('order_map', False)

    def insert_column_num(self, csv_data, dialect):
        lineterminator = "\n"
        delimiter = dialect.delimiter
        first_line = csv_data[0:csv_data.index(lineterminator)]
        first_line_len = len(first_line)
        new_first_line = ''
        new_first_line = [str(i) for i in range(0,first_line_len+1)]
        new_first_line = delimiter.join(new_first_line)

        new_csv_data = new_first_line + csv_data[csv_data.index(lineterminator):]

        return new_csv_data

    def parse_data(self, csv_data):
        # Parse this shit...
        # Order looks like xml page for comfortable printing but not to easy parse
        if not self.order_map:
            raise osv.except_osv(('ERROR !'), ("The mandatory 'order_map' Sale Integration variable doesn't exist!"))
        self.dialect = detect_dialect(csv_data)
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        obj_order = []
        order_matrix = []
        try:
            order_map = json.loads(self.order_map)
        except ValueError:
            raise osv.except_osv(('ERROR !'), ("Please, check the 'order_map' Sale Integration variable!"))

        parser = ParseLineObj()

        csv_data = self.insert_column_num(csv_data, self.dialect)
        try:
            rdr = csv_reader(StringIO(csv_data), dialect=self.dialect)
            # First step - create matrix from csv
            for rec in rdr:
                order_matrix.append(rec)

            # Second step: get data from matrix
            obj_order = parser.get_order_obj(order_matrix, order_map)
            obj_order['lines'] = parser.get_line_obj(order_matrix, order_map)
            obj_order['partner_id'] = self.external_customer_id

        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        yaml_obj = YamlObject()
        returned_order = {}
        try:
            returned_order['name'] = obj_order['order_id']
            returned_order['xml'] = yaml_obj.serialize(_data=obj_order)
            result['orders_list'].append(returned_order)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)

        return result
