# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import dateutil.parser

class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        self.delimiter = '*'
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        try:
            # message = EdiParser(edi_data)
            message = edi_data.split('~')
            for segment in message:
                elements = segment.split('*')
                print elements
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    self.sender = 'BTS-SENDER'
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {
                            'giftIndicator': 'n',
                            'gift_message': ''
                        }
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['po_number'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':
                    ordersObj['additional_fields'].update({
                        elements[1]: elements[2]
                    })
                elif elements[0] == 'PER':
                    addr_types_map = {
                        'BD': 'ship',  # Order Contact
                        'IC': 'invoice',  # Receiving Contact
                    }

                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        name = False
                        if len(elements[2]) > 2:
                            name = elements[2]
                        if addr:
                            addr.update({
                                'phone': elements[4] if len(elements) > 3 else '',
                                'email': elements[6] if len(elements) > 6 else '',
                            })
                        else:
                            ordersObj['address'][addr_types_map[elements[1]]] = {
                                'phone': elements[4] if len(elements) > 3 else '',
                                'email': elements[6] if len(elements) > 6 else '',
                            }
                       # if name and not ordersObj['address'][addr_types_map[elements[1]]].get('name', False):
                       #     ordersObj['address'][addr_types_map[elements[1]]]['name'] = name


                elif elements[0] == 'SAC':
                    if elements[1] == 'N' and len(elements) > 14:
                        if elements[2] == 'F030':
                            ordersObj['additional_fields'].update({
                                'gift_to': elements[15]
                            })
                            ordersObj['poHdrData'].update({
                                'gift_to': elements[15]
                            })
                        elif elements[2] == 'F040':
                            ordersObj['additional_fields'].update({
                                'gift_from': elements[15]
                            })
                            ordersObj['poHdrData'].update({
                                'gift_from': elements[15]
                            })
                        elif elements[2] == 'F050':
                            gift_message = ordersObj.get('additional_fields',{}).get('gift_message','')
                            ordersObj['additional_fields'].update({
                                'gift_message': gift_message + elements[15]+'\n',
                            })
                            ordersObj['poHdrData'].update({
                                'gift_message': gift_message + elements[15]+'\n',
                                'giftIndicator': 'y'
                            })
                elif elements[0] == 'DTM':
                    if elements[1] == '101':  # Delivery Requested
                        ordersObj['external_date_order'] = elements[2]
                elif elements[0] == 'TD5':
                    ordersObj['carrier'] = elements[5]
                    service_level_code = 'UPS Ground'
                    service_map = {
                        'UPSN': 'UPS (service level unspecified)',
                        'UPSN-ND': 'UPS Next Day Air',
                        'UPSN-SC': 'UPS 2nd Day Air',
                        'UPSN-3D': 'UPS 3 Day Select',
                        'UPSN-CG': 'UPS Ground'
                    }

                    if elements[5] in service_map:
                        service_level_code = service_map[elements[5]]

                    ordersObj['additional_fields'].update({
                            'service_level_code': service_level_code,
                    })
                elif elements[0] == 'N1':
                    n1 = elements[1]
                    if n1 == 'BY':
                        address_type = 'invoice'
                        ordersObj['address']['invoice'] = {
                            'name': elements[2],
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            # 'phone': '',
                            # 'email': ''
                        }
                    elif n1 == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address']['ship'] = {
                            'name': elements[2],
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            # 'phone': '',
                            # 'email': ''
                        }
                elif elements[0] == 'N2' and address_type != '' and n1 != 'BT':
                    # ordersObj['address'][address_type]['company'] = ordersObj['address'][address_type]['name']
                    ordersObj['address'][address_type]['company'] = elements[2]
                elif elements[0] == 'N3' and address_type != '' and n1 != 'BT':
                    ordersObj['address'][address_type]['address1'] = elements[1]
                    if len(elements) > 2:
                        ordersObj['address'][address_type]['address2'] = elements[2]
                elif elements[0] == 'N4' and address_type != '' and n1 != 'BT':
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    # ordersObj['address'][address_type]['country'] = elements[4]
                    if len(elements) == 5:
                        ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PO1':
                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'sku': elements[7],
                        'vendorSku': elements[7],
                        'upc': elements[11],
                        'customer_sku': elements[11],
                        'optionSku': elements[11],
                        'name': elements[11],
                        'additional_fields': {},
                    }
                    ordersObj['lines'].append(lineObj)
                # elif elements[0] == 'CTP':
                # unit_price * qty

                elif elements[0] == 'PID' and len(elements)>=5:
                    if elements[2] == '08':
                        lineObj['name'] = elements[5]
                    elif elements[2] == '74':
                        lineObj['size'] = elements[5]
                        lineObj['additional_fields'].update({
                            'size': elements[5],
                        })
                    elif elements[2] == '73':
                        lineObj['color'] = elements[5]
                        lineObj['additional_fields'].update({
                            'color': elements[5],
                        })
                # elif elements[0] == 'AMT':
                # unit_price * qty
                elif elements[0] == 'SE':
                    orders.append(ordersObj)
                elif elements[0] == 'GE':
                    ordersObj['number_of_transaction'] = self.number_of_transaction = elements[1]

        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
