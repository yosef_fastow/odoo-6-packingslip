# -*- coding: utf-8 -*-
from lxml.etree import XML, tostring, parse, XMLParser, Element, SubElement, XMLSyntaxError
from lxml import objectify
from StringIO import StringIO
import time
import re


class BlueflyApiGetOrdersXML():

    def __init__(self, order_type, options=None):
        self.order_type = order_type
        self.options = options
        self._log = []

    def delete_namespaces(self, element=None, xml=None, step=None):
        """
            1) If step is "first":
        required: element(<type 'lxml.etree._ElementTree'>), step
        returned: element with a remove namespaces(<type 'lxml.etree._ElementTree'>)
        or None
            2) if step is "second":
        required: xml(<type 'str'>), step
        returned: string with a remove namespaces(like xmlns:xsi="...")
        or empty string
            3) else return None
        """
        if step == "first":
            if element:
                try:
                    root = element.getroot()
                    for elem in root.getiterator():
                        try:
                            i = elem.tag.find('}')
                        except AttributeError:
                            continue
                        if i >= 0:
                            elem.tag = elem.tag[i+1:]
                    objectify.deannotate(root, cleanup_namespaces=True)
                    result = element
                except Exception, e:
                    print "Can't remove namespaces(1st step)"
                    response = tostring(element, pretty_print=True)
                    self._log.append({
                        'title': 'Error: %s' % e,
                        'msg': response,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    result = None
                finally:
                    return result
            else:
                return None

        elif step == "second":
            if xml:
                result = re.sub(''' xmlns:xsi=".*"''', "", xml)
                result = re.sub(''' xmlns=".*"''', "", result)
                return result
            else:
                return u""
        elif step == "replace_version":
            if xml:
                result = re.sub('''version="2.0"''', '''version="1.0"''', xml)
                return result
            else:
                return None
        else:
            return None

    def parse_response(self, response):
        if(not response):
            return []

        order_type = self.order_type
        ordersList = []

        for xml in response:
            parser = XMLParser(remove_blank_text=True)
            tree = None
            try:
                tree = parse(StringIO(xml), parser)
            except XMLSyntaxError:
                xml = self.delete_namespaces(xml=xml, step="replace_version")
                tree = parse(StringIO(xml), parser)
            finally:
                if tree is None:
                    print "Can't find XML tree"
                    self._log.append({
                        'title': 'Error: Not find XML tree',
                        'msg': "input xml: %s" % xml,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    break
            tree = self.delete_namespaces(element=tree, step="first")
            if not tree:
                continue
            detect_cancel_order = tree.find('.//OrderCancellation')
            if detect_cancel_order is not None:
                order_type = "cancel"
            ordersObj = {}
            # <--- Normal orders
            if order_type == "order":
                try:
                    orders = {}
                    Orders = tree.findall('Order')
                    for Order in Orders:
                        order_id = Order.find('OrderID', False)
                        if order_id is not None:
                            order_id = order_id.text
                            Type = Element("type")
                            Type.text = "normal"
                            Order.append(Type)
                            order_xml = tostring(Order, pretty_print=True)
                            order_xml = self.delete_namespaces(xml=order_xml, step="second")
                            orders.update({order_id: order_xml})
                    for key in orders:
                        ordersObj = {}
                        ordersObj['lines'] = []
                        ordersObj['xml'] = orders[key]
                        ordersObj['name'] = key
                        StorefrontOrderID = XML(ordersObj['xml']).find('StorefrontOrderID')
                        if StorefrontOrderID is not None:
                            ordersObj['external_customer_order_id'] = StorefrontOrderID.text
                        else:
                            ordersObj['external_customer_order_id'] = u"None"
                        items = XML(ordersObj['xml']).find('Items')
                        if items is not None:
                            for item in items:
                                OrderItemCode = item.find('OrderItemCode')
                                if OrderItemCode is not None:
                                    OrderItemCode = OrderItemCode.text
                                    ordersObj['lines'].append({'external_customer_line_id': OrderItemCode})
                        ordersList.append(ordersObj)
                except Exception, e:
                    print "Can't parse file(normal orders)"
                    self._log.append({
                        'title': 'Error: %s' % e,
                        'msg': response,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
            # <--- Cancel orders
            elif order_type == "cancel":
                try:
                    Header = tree.find("Header")
                    OrderStatus = tree.find("OrderStatus")
                    orders = OrderStatus.findall("OrderStatus")
                    if orders:
                        for order in orders:
                            messageID = order.attrib["MessageID"]
                            if messageID not in self.options["message_ids"]["cancelation"]:
                                continue
                            else:
                                ordersObj = {}
                                global_order = Element("Order")
                                if not self.options["remove_header"]:
                                    global_order.append(Header)
                                Type = SubElement(global_order, "type")
                                Type.text = "cancel"
                                global_order.append(order)
                                global_order = tostring(global_order, pretty_print=True)
                                global_order = self.delete_namespaces(xml=global_order, step="second")
                                ordersObj['xml'] = global_order
                                order_id = order.find("OrderID")
                                if order_id is not None:
                                    order_id = unicode(order_id.text)
                                else:
                                    order_id = u""
                                ordersObj['name'] = u"CANCEL" + order_id
                                ordersObj['external_customer_order_id'] = order_id
                                OrderCancellation = order.find("OrderCancellation")
                                Items = OrderCancellation.findall("CancellationItem")
                                if Items:
                                    ordersObj['lines'] = []
                                    for item in Items:
                                        OrderItemCode = item.find('OrderItemCode')
                                        if OrderItemCode is not None:
                                            OrderItemCode = OrderItemCode.text
                                            ordersObj['lines'].append({'external_customer_line_id': OrderItemCode})
                                ordersList.append(ordersObj)
                except Exception, e:
                    print "Can't parse file(cancel orders)"
                    self._log.append({
                        'title': 'Error: %s' % e,
                        'msg': response,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
        return ordersList

    def get_log(self):
        tmp = self._log[:]
        self._log = []
        return tmp
