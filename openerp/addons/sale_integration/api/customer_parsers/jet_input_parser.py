# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result, ElementWithXpath
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from json import loads as json_loads
from pf_utils.utils.re_utils import str2date


class dictElementWithXpath(ElementWithXpath):

    def __init__(self, input_order_obj_str):
        super(dictElementWithXpath, self).__init__()
        self.input_obj = json_loads(input_order_obj_str)

    def xpath_get(self, path):
        elem = self.input_obj
        try:
            for x in path.strip(".").split("."):
                elem = elem.get(x)
        except:
            pass
        return elem

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            res = self.xpath_get(xpath)
            if type_res == str and res:
                result = str(res)
            elif type_res == 'date' and res:
                result = str2date(res)
            elif type_res == bool:
                result = res
            elif type_res == int and res:
                result = _try_parse(res, 'int')
            elif type_res == float and res:
                result = _try_parse(res, 'float')
            elif type_res == Line:
                result = []
                for line in res:
                    result.append(Line(line).dict)
            else:
                result = None
        except Exception:
            result = False
        return result


class Line(dictElementWithXpath):

    xpaths = {
        'additional_fields': {
            'base_price': (float, 'item_price.base_price'),
            'item_shipping_cost': (float, 'item_price.item_shipping_cost'),
            'item_shipping_tax': (float, 'item_price.item_shipping_tax'),
            'item_tax': (float, 'item_price.item_tax'),
            'item_tax_code': (str, 'item_tax_code'),
            'url': (str, 'url'),
        },
        'qty': (int, 'request_order_quantity'),
        'request_order_cancel_qty': (int, 'request_order_cancel_qty'),
        'request_order_quantity': (int, 'request_order_quantity'),
        'merchantSKU': (str, 'merchant_sku'),
        'id': (str, 'order_item_id'),
        'name': (str, 'product_title'),
    }

    def __init__(self, dict_line):
        super(ElementWithXpath, self).__init__()
        self.input_obj = dict_line


class Order(dictElementWithXpath):

    xpaths = {
        'poNumber': (str, 'reference_order_id'),
        'order_id': (str, 'merchant_order_id'),
        'external_date_order': ('date', 'order_transmission_date'),
        'address': {
            'ship': {
                'name': (str, 'shipping_to.recipient.name'),
                'address1': (str, 'shipping_to.address.address1'),
                'address2': (str, 'shipping_to.address.address2'),
                'city': (str, 'shipping_to.address.city'),
                'state': (str, 'shipping_to.address.state'),
                'zip': (str, 'shipping_to.address.zip_code'),
                'country': (str, 'shipping_to.address.country'),
                'phone': (str, 'shipping_to.recipient.phone_number'),
            },
        },
        'additional_fields': {
            'buyer': {
                'name': (bool, 'buyer.name'),
                'phone_number': (bool, 'buyer.phone_number'),
            },
            'fulfillment_node': (str, 'fulfillment_node'),
            'has_shipments': (bool, 'has_shipments'),
            'jet_request_directed_cancel': (bool, 'jet_request_directed_cancel'),
            'order_detail': {
                'request_delivery_by': ('date', 'order_detail.request_delivery_by'),
                'request_service_level': (str, 'order_detail.request_service_level'),
                'request_ship_by': ('date', 'order_detail.request_ship_by'),
                'request_shipping_carrier': (str, 'order_detail.request_shipping_carrier'),
                'request_shipping_method': (str, 'order_detail.request_shipping_method'),
            },
            'order_placed_date': ('date', 'order_placed_date'),
            'order_totals': {
                'item_price': {
                    'base_price': (float, 'order_totals.item_price.base_price'),
                    'item_shipping_cost': (float, 'order_totals.item_price.item_shipping_cost'),
                    'item_shipping_tax': (float, 'order_totals.item_price.item_shipping_tax'),
                    'item_tax': (float, 'order_totals.item_price.item_tax'),
                }
            },
            'status': (str, 'status'),
            'urls': {
                'base_url': (str, 'urls.base_url'),
                'sub_url': (str, 'urls.sub_url'),
                'full_url': (str, 'urls.full_url'),
            }
        },
        'lines': (Line, 'order_items'),
    }


class Parser(BaseParser):
    """:class: for parsing input XML data to Yaml object"""
    def __init__(self, parent):
        super(Parser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, json_data):
        self.orders.append(Order(json_data).dict)
        return True
