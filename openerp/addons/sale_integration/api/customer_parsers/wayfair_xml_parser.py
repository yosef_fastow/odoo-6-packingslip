from ..utils import xmlutils as xml_utils

class WayfairApiGetOrderObj(object):

    def getOrderObj(self, xml):
        ordersObj = {}
        if(xml == ""):
            return ordersObj
        order = xml_utils.XmlUtills(xml).to_obj()
        if(order is not None):
            ordersObj['partner_id'] = 'Wayfair'
            ordersObj['order_id'] = order.get('order_id', {}).get('value', False)
            ordersObj['poNumber'] = order.get('po_number', {}).get('value', False)
            ordersObj['carrier'] = order.get('carrier', {}).get('value', False)
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['name'] = order.address.ship.get('name', {}).get('value', '')
            ordersObj['address']['ship']['address1'] = order.address.ship.get('address1', {}).get('value', '')
            ordersObj['address']['ship']['address2'] = order.address.ship.get('address2', {}).get('value', '')
            ordersObj['address']['ship']['city'] = order.address.ship.get('city', {}).get('value', '')
            ordersObj['address']['ship']['state'] = order.address.ship.get('state', {}).get('value', '')
            ordersObj['address']['ship']['zip'] = order.address.ship.get('zip', {}).get('value', '')
            ordersObj['address']['ship']['country'] = order.address.ship.get('country', {}).get('value', '') and order.address.ship.get('country', {}).get('value')[0:2]
            ordersObj['address']['ship']['phone'] = order.address.ship.get('phone', {}).get('value', '')
            ordersObj['lines'] = []
            lines = order.lines if (isinstance(order.lines, list)) else [order.lines]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('id', {}).get('value', False)
                lineItemObj['customer_sku'] = lineItem.get('customer_sku', {}).get('value', False)
                if lineItem.get('size'):
                    lineItemObj['size'] = lineItem.get('size').get('value', False)
                lineItemObj['merchantSKU'] = lineItemObj['customer_sku']
                lineItemObj['qty'] = lineItem.get('qty', {}).get('value', False)
                lineItemObj['name'] = lineItem.get('name', {}).get('value', False)
                lineItemObj['cost'] = lineItem.get('cost', {}).get('value', False)
                ordersObj['lines'].append(lineItemObj)
        return ordersObj