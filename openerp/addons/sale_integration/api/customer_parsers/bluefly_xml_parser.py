# -*- coding: utf-8 -*-
from lxml.etree import XML
import time


class BlueflyApiGetOrderObj():
    def __init__(self):
        self._log = []

    def getOrderObj(self, xml):
        result = None
        if(xml != ""):
            order = XML(xml)
            if(order is not None):
                order_type = order.find("type")
                if order_type is not None:
                    order_type = order_type.text
                    print order_type
                    # <--- normal orders
                    if order_type == "normal":
                        ordersObj = {}
                        ordersObj['partner_id'] = 'bluefly'
                        ordersObj['address'] = {}
                        ordersObj['address']['ship'] = {}
                        ordersObj['address']['order'] = {}
                        ordersObj['lines'] = []
                        ordersObj['order_id'] = order.findtext('OrderID', False)
                        ordersObj['poNumber'] = order.findtext('OrderID', False)
                        ordersObj['external_date_order'] = order.findtext('OrderDate', False)
                        ordersObj['tax'] = order.find(".//ItemPrices")[0].text or False
                        ordersObj['shipping'] = order.findtext('ShippingCharge', False)
                        ordersObj['carrier'] = 'CPCPE'

                        ordersObj['address']['order'] = {
                            'name':  False,
                            'phone':  False,
                            'email':  False,
                            'address1':  False,
                            'address2':  False,
                            'city':  False,
                            'state':  False,
                            'zip':  False,
                            'country':  False,
                        }

                        billing_data = order.find("BillingData")
                        if billing_data:
                            billing_address = billing_data.find("BillingAddress")
                            if billing_address:
                                ordersObj['address']['order']['name'] = billing_address.findtext("Name")
                                ordersObj['address']['order']['phone'] = billing_address.findtext("PhoneNumber", False)
                                ordersObj['address']['order']['email'] = billing_data.findtext("BuyerEmailAddress", False)
                                ordersObj['address']['order']['address1'] = billing_address.findtext("AddressFieldOne", False)
                                ordersObj['address']['order']['address2'] = billing_address.findtext("AddressFieldTwo", False)
                                ordersObj['address']['order']['city'] = billing_address.findtext("City", False)
                                ordersObj['address']['order']['state'] = billing_address.findtext("StateOrRegion", False)
                                ordersObj['address']['order']['zip'] = billing_address.findtext("PostalCode", False)
                                ordersObj['address']['order']['country'] = billing_address.findtext("CountryCode", False)

                        ordersObj['address']['ship'] = {
                            'name':  False,
                            'phone':  False,
                            'email':  False,
                            'address1':  False,
                            'address2':  False,
                            'city':  False,
                            'state':  False,
                            'zip':  False,
                            'country':  False,
                        }

                        ship_data = order.find("FulfillmentData")
                        if ship_data:
                            ship_address = ship_data.find("FulfillmentAddress")
                            if ship_address:
                                ordersObj['address']['ship']['name'] = ship_address.findtext("Name", False)
                                ordersObj['address']['ship']['phone'] = ship_address.findtext("PhoneNumber", False)
                                ordersObj['address']['ship']['email'] = ship_address.findtext("PhoneNumber", False)
                                ordersObj['address']['ship']['address1'] = ship_address.findtext("AddressFieldOne", False)
                                ordersObj['address']['ship']['address2'] = ship_address.findtext("AddressFieldTwo", False)
                                ordersObj['address']['ship']['city'] = ship_address.findtext("City", False)
                                ordersObj['address']['ship']['state'] = ship_address.findtext("StateOrRegion", False)
                                ordersObj['address']['ship']['zip'] = ship_address.findtext("PostalCode", False)
                                ordersObj['address']['ship']['country'] = ship_address.findtext("CountryCode", False)

                        order_items = order.find('Items')
                        for item in order_items:
                            lineObj = {}
                            item_prices = item.find("ItemPrices")
                            lineObj['qty'] = item.findtext('Quantity', False)
                            for pr_item in item_prices:
                                if pr_item.attrib['type'] == "ItemPrice":
                                    lineObj['linePrice'] = pr_item.text or False
                                    lineObj['cost'] = pr_item.text or False
                                    try:
                                        lineObj['cost'] = float(lineObj['cost']) / float(lineObj['qty'])
                                    except:
                                        pass
                                elif pr_item.attrib['type'] == "ItemTax":
                                    lineObj['tax'] = pr_item.text or False
                            lineObj['id'] = item.findtext('OrderItemCode', False)
                            lineObj['balance_due'] = item.findtext('item_discount', False)
                            lineObj['shipCost'] = item.findtext('ItemFees', False)
                            lineObj['sku'] = item.findtext('SKU', False)
                            lineObj['merchantSKU'] = lineObj['sku']
                            lineObj['name'] = item.findtext('Title', False)
                            lineObj['ItemStatus'] = item.findtext('ItemStatus', False)
                            ordersObj['lines'].append(lineObj)
                        result = ordersObj
                    # <-- cancel order
                    elif order_type == "cancel":
                        order_header = order.find("Header")
                        order_status = order.find("OrderStatus")
                        try:
                            order_id = order_status.find("OrderID").text
                        except AttributeError:
                            print("Couldn't find order ID: bluefly %s order" % order_type)
                            self._log.append({
                                'title': "Error: Couldn't find order ID: bluefly %s " % order_type,
                                'msg': "xml: %s" % xml,
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            result = {}
                            return result
                        ordersObj = {}
                        ordersObj['partner_id'] = 'bluefly'
                        ordersObj['external_customer_order_id'] = order_id
                        order_cancellation = order_status.find("OrderCancellation")
                        cancellation_items = order_cancellation.findall("CancellationItem")
                        ordersObj['lines'] = []
                        ordersObj['reasons'] = {}
                        for item in cancellation_items:
                            item_code = item.find("OrderItemCode")
                            if item_code is not None:
                                ordersObj['lines'].append(item_code.text)
                                cancel_reason = item.find("CancelReason")
                                if cancel_reason is not None:
                                    ordersObj['reasons'][item_code.text] = cancel_reason.text
                        result = ordersObj
                    else:
                        print("Unknown order type: %s" % order_type)
                        self._log.append({
                            'title': 'Error: Unknown order type: %s' % order_type,
                            'msg': "xml: %s" % xml,
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                        result = {}
                else:
                    print("Order type is not found from xml.")
                    self._log.append({
                        'title': 'Error: Order type is not found from xml.',
                        'msg': "xml: %s" % xml,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    result = {}
            else:
                print("Couldn't generate lxml.etree._ElementTree object")
                self._log.append({
                    'title': "Error: Couldn't generate lxml.etree._ElementTree object",
                    'msg': "xml: %s" % xml,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                result = {}
        else:
            print("xml is empty!")
            self._log.append({
                'title': "Error: xml is empty!",
                'msg': "customer: Bluefly",
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            result = {}
        return result

    def get_log(self):
        tmp = self._log[:]
        self._log = []
        return tmp
