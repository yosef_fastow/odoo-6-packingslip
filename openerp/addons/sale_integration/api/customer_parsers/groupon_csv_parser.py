import csv
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import cStringIO
import traceback
from openerp.addons.pf_utils.utils.import_orders_normalize import fix_line_terminators


class GrouponApiGetOrdersXML(object):

    def __init__(self, parent):
        self.parent = parent

    def parse_data(self, data):
        data = fix_line_terminators(data)
        result = {}
        result['orders_list'] = []
        result['errors'] = []
        if(not data):
            return []
        obj_ordersList = []
        try:
            obj = list(csv.reader(cStringIO.StringIO(data.decode("utf-8-sig").encode("utf-8")), delimiter=',', quotechar='"', lineterminator='\n'))
            head_row = obj[0]
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)

                _order_number = 'parent_order_id'
                _po_number    = 'groupon_number'

            order_id_old = ""
            ordersObj = {}
            print obj
            for row in obj[1:]:
                if not ordersObj.get('order_no', False):
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                elif ordersObj.get('order_no', False) and order_id_old != row[idx.get(_order_number)].strip():
                    order_id_old = ""
                    obj_ordersList.append(ordersObj)
                    ordersObj = {}
                    ordersObj['order_no'] = row[idx.get(_order_number)].strip()
                if ordersObj.get('order_no', False) and order_id_old != ordersObj.get('order_no', False):
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['name'] = row[idx.get('shipment_address_name')].strip()
                    ordersObj['address']['ship']['address1'] = row[idx.get('shipment_address_street')].strip()
                    ordersObj['address']['ship']['address2'] = row[idx.get('shipment_address_street_2')].strip()
                    ordersObj['address']['ship']['city'] = row[idx.get('shipment_address_city')].strip()
                    ordersObj['address']['ship']['state'] = row[idx.get('shipment_address_stat')].strip()
                    ordersObj['address']['ship']['zip'] = row[idx.get('shipment_address_postal_code')].strip()
                    ordersObj['address']['ship']['country'] = row[idx.get('shipment_address_country')].strip()
                    ordersObj['address']['ship']['phone'] = row[idx.get('customer_phone')].strip()

                    ordersObj['address']['order'] = {}
                    ordersObj['address']['order']['name'] = row[idx.get('billing_address_name')].strip()
                    ordersObj['address']['order']['address1'] = row[idx.get('billing_address_street')].strip()
                    ordersObj['address']['order']['address2'] = ''
                    ordersObj['address']['order']['city'] = row[idx.get('billing_address_city')].strip()
                    ordersObj['address']['order']['state'] = row[idx.get('billing_address_stat')].strip()
                    ordersObj['address']['order']['zip'] = row[idx.get('billing_address_postal_code')].strip()
                    ordersObj['address']['order']['country'] = row[idx.get('billing_address_country')].strip()
                    ordersObj['address']['order']['phone'] = row[idx.get('customer_phone')].strip()

                    ordersObj['ship_via'] = row[idx.get('shipment_method_requested')].strip()
                    ordersObj['external_date_order'] = ordersObj['date'] = row[idx.get('order_date')].strip()
                    ordersObj['po_no'] = row[idx.get('groupon_number')].strip()
                    ordersObj['partner_id'] = self.parent.customer
                    ordersObj['order_id'] = ordersObj['po_no'].strip()
                    ordersObj['poNumber'] = ordersObj['order_no'].strip()
                    ordersObj['external_customer_order_id'] = ordersObj['order_id'].strip()
                    ordersObj['carrier'] = row[idx.get('shipment_method_requested')].strip()
                    #ordersObj['store_number'] = row[idx.get('store')].strip()

                    ordersObj['external_date_order'] = row[idx.get('order_date')].strip()
                    ordersObj['gift'] = row[idx.get('gift')].strip()
                    ordersObj['gift_message'] = row[idx.get('gift_message')].strip()
                    ordersObj['latest_ship_date_order'] = row[idx.get('ship_date')].strip()
                    order_id_old = ordersObj['order_no'].strip()
                    ordersObj['lines'] = []

                itemsObj = {}
                itemsObj['external_customer_line_id'] = row[idx.get('fulfillment_line_item_id')].strip()
                itemsObj['id'] = row[idx.get('fulfillment_line_item_id')].strip()
                itemsObj['name'] = row[idx.get('item_name')].strip()
                itemsObj['vendorSku'] = row[idx.get('merchant_sku_item')].strip()
                itemsObj['merchantSKU'] = row[idx.get('groupon_sku')].strip()
                # itemsObj['size'] = row[idx.get('size')].strip()
                itemsObj['qty'] = row[idx.get('quantity_requested')].strip()
                itemsObj['cost'] = row[idx.get('groupon_cost')].strip()
                #itemsObj['unit_price'] = row[idx.get('groupon_cost')].strip()
                itemsObj['customerCost'] = row[idx.get('sell_price')].strip()

                ordersObj['lines'].append(itemsObj)
            obj_ordersList.append(ordersObj)
        except Exception:
            msg = traceback.format_exc()
            result['errors'].append(msg)
        print obj_ordersList
        for order in obj_ordersList:
            yaml_obj = YamlObject()
            ordersObj = {}
            ordersObj['xml'] = yaml_obj.serialize(_data=order)
            ordersObj['name'] = order['poNumber']
            result['orders_list'].append(ordersObj)
        return result
