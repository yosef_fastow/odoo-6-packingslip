from ..utils import xmlutils as xml_utils


class PeopleApiGetOrderObj(object):

    def getOrderObj(self, xml):
        ordersObj = {}

        if(xml == ""):
            return ordersObj
        order = xml_utils.XmlUtills(xml).to_obj()
        if(order is not None):
            ordersObj['partner_id'] = order.get('store', {}).get('value', 'people')
            ordersObj['order_id'] = order.get('order_no', {}).get('value', False)
            ordersObj['poNumber'] = order.get('po_no', {}).get('value', False)
            ordersObj['carrier'] = order.ship.get('ship_via', {}).get('value', '')
            ordersObj['address'] = {}
            ordersObj['address']['ship'] = {}
            ordersObj['address']['ship']['name'] = order.ship.get('shipname', {}).get('value', '')
            ordersObj['address']['ship']['address1'] = order.ship.get('shipadd1', {}).get('value', '')
            ordersObj['address']['ship']['address2'] = order.ship.get('shipadd2', {}).get('value', '')
            ordersObj['address']['ship']['city'] = order.ship.get('shipcity', {}).get('value', '')
            ordersObj['address']['ship']['state'] = order.ship.get('shipstate', {}).get('value', '')
            ordersObj['address']['ship']['zip'] = order.ship.get('shipzip', {}).get('value', '')
            ordersObj['address']['ship']['country'] = 'Canada'
            ordersObj['address']['ship']['phone'] = order.ship.get('shipphone', {}).get('value', '')
            ordersObj['lines'] = []
            lines = order.lines if (isinstance(order.lines, list)) else [order.lines]
            for lineItem in lines:
                lineItemObj = {}
                lineItemObj['id'] = lineItem.get('item', {}).get('value', False)
                lineItemObj['vendorSku'] = lineItem.get('vendor_sku', {}).get('value', False)
                if not lineItemObj['vendorSku']:
                    lineItemObj['vendorSku'] = lineItem.get('vendorSku', {}).get('value', False)
                lineItemObj['merchantSKU'] = lineItemObj['vendorSku']
                lineItemObj['size'] = lineItem.get('size').get('value', False)
                lineItemObj['qty'] = lineItem.get('qty', {}).get('value', False)
                lineItemObj['name'] = lineItem.get('desc', {}).get('value', False)
                lineItemObj['cost'] = False
                ordersObj['lines'].append(lineItemObj)

        return ordersObj
