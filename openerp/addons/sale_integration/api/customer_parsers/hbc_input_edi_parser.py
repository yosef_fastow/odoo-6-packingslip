# -*- coding: utf-8 -*-
from element_with_xpath import xpathRule
from base_edi import (
    BaseEDIParser,
    ediElementWithXpath,
    AddressEdiElementWithXpath,
    address_type_map,
)
import re


class AddressXpathRule(xpathRule):
    __type_res__ = 'Address'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).dict)
        return result


class OriginalAddressesXpathRule(xpathRule):
    __type_res__ = 'original_addresses'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).get_original_dict())
        return result


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        return [Line(line).dict for line in res]


class Line(ediElementWithXpath):

    xpaths = {
        'id': ('str', 'PO1.1'),
        'qty': ('int', 'PO1.2'),
        'cost': ('float', 'PO1.4'),
        'retail_cost': ('float', 'CTP:RS:RTL.3'),
        'customer_sku': ('str', 'PO1:*:*:*:*:*:SK.7'),
        'sku': ('str', 'PO1:*:*:*:*:*:SK.7'),
        'upc': ('str', 'PO1:*:*:*:*:*:*:*:UP.9'),
        'optionSku': ('str', 'PO1:*:*:*:*:*:*:*:UP.9'),
        'size': ('str', 'PID:F:91.2'),
        'name': ('str', 'PID:F:08.5'),
        'additional_fields': {
            'color': ('str', 'PID:F:35.5'),
            'size_description': ('str', 'PID:F:91.5'),
        },
    }


class Address(AddressEdiElementWithXpath):

    xpaths = {
        'type': ('str', 'N1.1'),
        'name': ('str', 'N1.2'),
        'last_name': ('str', 'N2.1'),
        'middle_name': ('str', 'N2.2'),
        'address1': ('str', 'N3->0.1'),
        'address2': ('str', 'N3->0.2'),
        'address3': ('str', 'N3->1.1'),
        'address4': ('str', 'N3->1.2'),
        'city': ('str', 'N4.1'),
        'state': ('str', 'N4.2'),
        'zip': ('str', 'N4.3'),
        'country': ('str', 'N4.4'),
        'contact_data': {
            'name': ('str', 'PER:OC.2'),
            'phone': ('str', 'PER:OC:*:TE.4'),
        },
    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res_type = address_type_map.get(res['type'], 'order')
        del res['type']
        # BAD decision, but it is better than US for all orders
        alphanumeric_regex = re.compile('^[0-9a-zA-Z\s]+$')
        numeric_regex = re.compile('^[0-9\s]+$')
        if bool(numeric_regex.match(res['zip'])):
            country = 'US'
        elif bool(alphanumeric_regex.match(res['zip'])):
            country = 'CA'
        else:
            country = res['country']
        result = {
            res_type: {
                'name': ' '.join(
                    [
                        value for key, value in res.items()
                        if key in ['name', 'last_name', 'middle_name'] and value
                    ]
                ),
                'address1': res['address1'],
                'address2': res['address2'],
                # TODO: need to store it or use?
                # 'address3': res['address3'],
                # 'address4': res['address4'],
                'city': res['city'],
                'state': res['state'],
                'zip': res['zip'],
                'country': country,
                'phone': res.get('contact_data', {}).get('phone', None),
            }
        }
        return result


class Order(ediElementWithXpath):

    xpaths = {
        'poNumber': ('str', 'BEG:00:DS.3'),
        'order_id': ('str', 'BEG:00:DS.3'),
        'external_date_order': ('datetime', 'BEG:00:DS.5'),
        'external_customer_order_id': ('str', 'REF:CO.2'),
        'address': ('Address', 'N1<-PER(N2,N3,N4)'),
        'carrier': ('str', 'TD5:*:92.3'),
        'additional_fields': {
            'customer#': ('str', 'BEG:00:DS.3'),
            'customer_order_number': ('str', 'REF:IA.2'),
            'internal_vendor_number': ('str', 'REF:CO.2'),
            'mutually_defined': ('str', 'REF:ZZ.2'),
            'addresses': ('original_addresses', 'N1<-PER(N2,N3,N4)'),
            'goods_and_services_tax': ('str', 'AMT:GT.2'),
            'canadian_goods_and_services_or_Quebec_sales_tax_reference_number': ('str', 'REF:4O.2'),
            'transaction_reference_number': ('str', 'REF:TN.2'),
        },
        'lines': ('Line', 'PO1<-TC2(CTP,PID,PO4,SDQ)'),
        'cancellation': ('static', False),
        'change': ('static', False),

    }

    def __init__(self, lxml_etree_Element):
        super(Order, self).__init__(lxml_etree_Element)
        self.custom_xpath_rules = {
            'Address': AddressXpathRule,
            'original_addresses': OriginalAddressesXpathRule,
            'Line': LineXpathRule,
        }

        self.inject_custom_xpath_rules()


class HBCEDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(HBCEDIParser, self).__init__(parent)
        self.Order = Order
