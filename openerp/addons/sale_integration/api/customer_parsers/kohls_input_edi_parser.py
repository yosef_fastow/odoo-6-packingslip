# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []

        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(message.delimiters[1])

                print elements

                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                    self.sender = 'kohls'
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {}
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    # elements[1] '00'
                    #elements[2] 'RE’	for	Reorder;‘SA'	for	Stand-alone	Order
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['po_number'] = elements[3]

                    ordersObj['external_date_order'] = elements[5]
                    
                elif elements[0] == 'REF':
                    if len(elements) < 4:
                        ordersObj['additional_fields'].update({
                            elements[2]: elements[1]
                        })
                    elif (elements[3] in ['receipt_id',
                                          'gift_flag',
                                          'dsco_warehouse_code',
                                          'ship_store_number',
                                          'dsco_order_id',
                                          'dsco_shipping_service_level_code',
                                          'dsco_ship_method',
                                          'dsco_ship_carrier'
                                          ]):
                        if elements[3] == 'receipt_id':
                            ordersObj['receipt_id'] = elements[2]
                        elif(elements[3] == 'gift_flag'):
                            ordersObj['poHdrData']['giftIndicator'] = elements[2]
                        elif (elements[3] == 'dsco_warehouse_code'):
                            ordersObj['additional_fields']['vendor_warehouse'] = elements[2]
                            ordersObj['vendor_warehouse'] = elements[2]
                        elif (elements[3] == 'ship_store_number'):
                            ordersObj['store_number'] = elements[2]
                        elif (elements[3] == 'dsco_order_id'):
                            ordersObj['order_id'] =  ordersObj['external_customer_order_id'] = elements[2]
                    else:
                        ordersObj['additional_fields'].update({
                            elements[3]: elements[2]
                        })

                elif elements[0] == 'PER':
                    addr_types_map = {
                        'OC': 'invoice',  # Order Contact
                        'RE': 'ship',  # Receiving Contact
                    }
                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        if addr:
                            addr.update({
                                'name': elements[2],
                                'phone': elements[4] if len(elements) > 3 else '',
                                'email': elements[6] if len(elements) > 7 else '',
                            })
                elif elements[0] == 'SAC':
                    if elements[1] == 'C' and elements[2] == 'D230':  # Customer’s shipping and handling
                        ordersObj['shipping_tax'] = elements[5]
                        ordersObj['amount_tax'] = elements[5]

                elif elements[0] == 'DMT':
                    if elements[1] == '004':
                        ordersObj['external_date_order'] = elements[2]
                elif elements[0] == 'TD5':
                    ordersObj['additional_fields'].update({
                        'dsco_ship_carrier': elements[2],
                        'dsco_ship_method': elements[5],
                        'dsco_shipping_service_level_code': elements[8],
                    })
                    ordersObj['ship_service'] = elements[5]
                    ordersObj['ship_method'] = elements[5]
                    ordersObj['transaction_set_identifier_code'] = elements[8]
                    ordersObj['carrier'] = elements[8]
                elif elements[0] == 'TD4' and elements[4] == 'signature_required_flag':
                    ordersObj['additional_fields'].update({
                        'signature': elements[5]
                    })
                elif elements[0] == 'N9':
                    if elements[1] == 'CO':
                        ordersObj['additional_fields'].update({
                            'consumer_order_number': elements[2]
                        })
                        ordersObj['cust_order_number'] = elements[2]
                        ordersObj['poHdrData'].update({
                            'custOrderNumber': elements[2],
                        })

                    elif elements[1] == 'L1':
                        ordersObj['additional_fields'].update({
                            'notes': elements[2]
                        })
                elif elements[0] == 'MTX' and elements[1] == 'EAJ':
                    ordersObj['additional_fields'].update({
                        'gift_message': elements[2],
                    })

                    ordersObj['poHdrData'].update({
                        'gift_message': elements[2],
                        'giftIndicator': 'y'
                    })

                #how get 'ST' ? 'BT
                elif elements[0] == 'N1':  # Ship To
                    if elements[1] in ['BT']:  # Bill-to-Party, Vendor
                        address_type = 'order'
                        ordersObj['address'][address_type] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address'][address_type]['name'] = elements[2]
                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address'][address_type] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': '',
                            'personPlaceData': {
                                'ReceiptID': ordersObj.get('receipt_id', '')
                            }
                        }
                        ordersObj['address'][address_type]['carrier'] = ordersObj['carrier']
                        ordersObj['address'][address_type]['ship_service'] = ordersObj['ship_method']
                        ordersObj['address'][address_type]['name'] = elements[2]

                elif elements[0] == 'N3':
                    ordersObj['address'][address_type]['address1'] = elements[1]
                    if len(elements)>2:
                        ordersObj['address'][address_type]['address2'] = elements[2]
                elif elements[0] == 'N4':
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PER' and elements[1] == 'OC':
                    addr_types_map = {
                        'OC': 'invoice',  # Order Contact
                        'RE': 'ship',  # Receiving Contact
                    }
                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        if addr:
                            addr.update({
                                'name': elements[2],
                                'phone': elements[4],
                                'email': elements[6],
                            })
                elif elements[0] == 'PO1':
                    # lineObj = {
                    #     'id': elements[1],
                    #     'qty': elements[2],
                    #     'cost': elements[4],
                    #     'sku': elements[7],
                    #     'vendorSku': elements[7],
                    #     'upc': elements[9],
                    #     'merchantSKU': elements[13],
                    #     'customer_sku': elements[11],
                    #     'optionSku': elements[11],
                    #     'name': elements[11],
                    #     'additional_fields': {},
                    # }

                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'customerCost': 0,
                        'retail_cost': 0,
                        'additional_fields': {},
                        'poLineData': {
                            'lineNote2': ordersObj.get('receipt_id', ''),

                        }
                    }
                    for i in (6, 8, 10, 12, 14, 16, 18, 20):
                        if len(elements) > i:
                            if elements[i] == 'SK':
                                lineObj['merchantSKU'] = elements[i + 1]
                            if elements[i] == 'UP':
                                lineObj['upc'] = elements[i + 1]
                            if elements[i] == 'PD':
                                lineObj['name'] = elements[i + 1]
                            if elements[i] == 'BP':
                                lineObj['vendor_sku'] = elements[i + 1]

                            if elements[i] == 'EN':
                                elements[i + 1]
                            if elements[i] == 'MG':
                                elements[i + 1]

                            if elements[i] == 'EM':
                                elements[i + 1]

                            if elements[i] == 'CB':
                                elements[i + 1]
                            if elements[i] == 'DV':
                                elements[i + 1]
                        else:
                            break

                    ordersObj['lines'].append(lineObj)

                elif elements[0] == 'LIN':
                    lineObj.update({
                        'sku': elements[3],
                        'customer_sku': elements[15],
                        'color': elements[17],
                        'poLineData': {
                            'color': elements[17],
                            'size': elements[19],
                            'taxType': ordersObj['additional_fields'].get('payment_card_type_1','')
                        }
                    })
                    additional_fields = {}

                    if elements[5] and not lineObj['upc']:
                        lineObj['upc'] = elements[5]

                    if len(elements)>=7:
                        additional_fields.update({
                            'ean': elements[7], #european_article_number
                        })
                    if len(elements)>=21:
                        additional_fields.update({
                            'line_item_gift_flag': elements[21],
                        })
                    if len(elements)>=25:
                        additional_fields.update({
                            'line_item_receipt_id': elements[25],
                        })
                    if len(elements)>=27:
                        additional_fields.update({
                            'line_item_bogo_flag': elements[27],
                        })
                    if len(elements)>=29:
                        additional_fields.update({
                            'line_item_bogo_instructions': elements[29],
                        })
                    lineObj['additional_fields'] = additional_fields
                elif elements[0] == 'CTP':
                    if 'PUR' == elements[2]:
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['customerCost'] = elements[3]
                    if 'RTL' == elements[2]:
                        ordersObj['lines'][len(ordersObj['lines']) - 1]['retail_cost'] = elements[3]

                # elif elements[0] == 'CTT':
                    # ordersObj['lines'][len(ordersObj['lines']) - 1]['size'] = elements[1]
                elif elements[0] == 'SE':

                    ordersObj['paymentMethod'] = ordersObj['additional_fields'].get('payment_card_type_1','') + ' ' + ordersObj['additional_fields'].get('payment_card_last_four_1','')
                    ordersObj['poHdrData']['taxType'] = ordersObj['tax'] =  ordersObj['additional_fields'].get('tax_type_code_1','') + ' = ' + ordersObj['additional_fields'].get('tax_percentage_1','') + "%"
                    orders.append(ordersObj)
        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']
                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
