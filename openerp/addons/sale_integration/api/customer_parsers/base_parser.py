# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import traceback
import json
from functools import wraps
import logging

_logger = logging.getLogger(__name__)


def orders_list_result(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        func_self = args[0]
        func(*args, **kwargs)
        return func_self.result
    return wrapper


def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


class BaseParser(object):
    """Base :class: for parsing data to YAML orders

    Usage::
        from base_parser import BaseParser

        class MyParser(BaseParser):
            def __init__(self, parent):
                super(MyParser, self).__init__(parent)

    """
    def __init__(self, parent):
        super(BaseParser, self).__init__()
        self.parent = parent
        self.orders = []
        self.errors = []

    @property
    def result(self):
        """fore return result please use decorator @orders_list_result"""
        result = {
            'orders_list': self.yaml_orders_list,
            'errors': self.errors
        }
        return result

    @result.setter
    def result(self, value):
        raise AttributeError('You can\'t set up result property')

    @property
    def yaml_orders_list(self):
        """:property: using in self.result

        returns result like this:
        [
            {
                'name': 'POw345w4',
                'xml': 'SERIALIZED `YamlObject <YamlObject>`',
            },
            ...
        ]

        """
        self._yaml_orders_list = []
        yaml_obj = YamlObject()
        for order_obj in self.orders:
            order_obj.update({
                'partner_id': self.parent.customer,
            })
            returned_order = {}
            try:
                returned_order['name'] = order_obj['order_id']
                if order_obj.get('additional_fields'):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list, dict))):
                            # TODO: What a reason of this serialization?
                            serialize_additional_fields.update({
                                key: json.dumps(value, default=date_handler)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                if 'lines' in order_obj:
                    for line_obj in order_obj['lines']:
                        if line_obj.get('additional_fields'):
                            serialize_additional_fields = {}
                            for key, value in line_obj['additional_fields'].iteritems():
                                if (isinstance(value, (tuple, list, dict))):
                                    serialize_additional_fields.update({
                                        key: json.dumps(value, default=date_handler)
                                    })
                                else:
                                    serialize_additional_fields.update({
                                        key: value
                                    })
                            line_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self._yaml_orders_list.append(returned_order)
            except Exception:
                self.errors.append(traceback.format_exc())
        return self._yaml_orders_list

    @yaml_orders_list.setter
    def yaml_orders_list(self, value):
        raise AttributeError('You can\'t set up yaml_orders_list property')


class ElementWithXpath(object):

    xpaths = {}

    def __init__(self):
        super(ElementWithXpath, self).__init__()
        _logger.info('ElementWithXpath is deprecated, please use AbstractElementWithXpath from element_with_xpath')

    def __getdict__(self, obj):
        result = {}
        if isinstance(obj, dict):
            for key, value in obj.iteritems():
                if not isinstance(value, dict):
                    result.update({
                        key: self.__get_elem__(
                            type_res=value[0],
                            xpath=value[1]
                        )
                    })
                else:
                    result.update({
                        key: self.__getdict__(value)
                    })
        else:
            raise NotImplementedError('obj not a dict!')
        return result

    def __getattr__(self, name):
        result = None
        if name in self.xpaths:
            if isinstance(self.xpaths[name], dict):
                result = self.__getdict__(self.xpaths[name])
            elif isinstance(self.xpaths[name], tuple):
                result = self.__get_elem__(name=name)
            else:
                raise NotImplementedError(
                    'In xpaths you can use dict or tuple, {} not available'.format(
                        str(type(self.xpaths[name]))
                    )
                )
        else:
            result = None
        return result

    def to_dict(self):
        result = {}
        for name in self.xpaths:
            result.update({name: self.__getattr__(name)})
        return result

    @property
    def dict(self):
        return self.to_dict()

    @dict.setter
    def dict(self, value):
        raise AttributeError('You can\'t set property dict!')
