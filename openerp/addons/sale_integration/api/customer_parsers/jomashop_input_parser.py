# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result, ElementWithXpath
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from json import loads as json_loads
from pf_utils.utils.re_utils import str2date


class dictElementWithXpath(ElementWithXpath):

    def __init__(self, input_order_obj_str):
        super(dictElementWithXpath, self).__init__()
        self.input_obj = json_loads(input_order_obj_str)

    def xpath_get(self, path):
        elem = self.input_obj
        try:
            for x in path.strip(".").split("."):
                elem = elem.get(x)
        except:
            pass
        return elem

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            res = self.xpath_get(xpath)
            if type_res == str and res:
                result = str(res)
            elif type_res == 'date' and res:
                result = str2date(res)
            elif type_res == bool:
                result = res
            elif type_res == int and res:
                result = _try_parse(res, 'int')
            elif type_res == float and res:
                result = _try_parse(res, 'float')
            elif type_res == Line:
                result = []
                for line in res:
                    result.append(Line(line).dict)
            else:
                result = None
        except Exception:
            result = False
        return result


class Line(dictElementWithXpath):

    xpaths = {
        'additional_fields': {
        },
        'qty': (int, 'quantity'),
        'merchantSKU': (str, 'sku'),
        'vendorSku': (str, 'jomashop_sku'),
        'id': (str, 'jomashop_sku'),
        'name': (str, 'product_name'),
        'cost': (float, 'price'),
    }

    def __init__(self, dict_line):
        super(ElementWithXpath, self).__init__()
        self.input_obj = dict_line


class Order(dictElementWithXpath):
    xpaths = {
        'poNumber': (str, 'sales_order_number'),
        'order_id': (str, 'sales_order_number'),
        'carrier': (str, 'ship_method'),
        'address': {
            'ship': {
                'name': (str, 'ship_full_name'),
                'address1': (str, 'ship_address'),
                'address2': (str, 'ship_address2'),
                'city': (str, 'ship_city'),
                'state': (str, 'ship_state'),
                'zip': (str, 'ship_zip'),
                'country': (str, 'ship_country'),
                'phone': (str, 'phone'),
            },
        },
        'additional_fields': {
            'order_detail': {
                'request_shipping_method': (str, 'ship_method'),
                'billphone': (str, 'billing_phone'),
            },
            'order_placed_date': ('date', 'placed_at'),
           'status': (str, 'status'),
        },
        'gift_message': (str, 'gift_message'),
        'poHdrData': {
            'giftMessage': (str, 'gift_message'),
        },
        'lines': (Line, 'products'),
    }

class Parser(BaseParser):
    """:class: for parsing input XML data to Yaml object"""
    def __init__(self, parent):
        super(Parser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, json_data):
        self.orders.append(Order(json_data).dict)
        return True
