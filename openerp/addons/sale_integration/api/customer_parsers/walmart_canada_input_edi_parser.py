# -*- coding: utf-8 -*-
from element_with_xpath import xpathRule
from base_edi import (
    BaseEDIParser,
    ediElementWithXpath,
    AddressEdiElementWithXpath,
    address_type_map,
)
from pf_utils.utils.re_utils import str2date


class AddressXpathRule(xpathRule):
    __type_res__ = 'Address'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).dict)
        return result


class OriginalAddressesXpathRule(xpathRule):
    __type_res__ = 'original_addresses'

    def process_res(self, res, xpath):
        result = {}
        for address in res:
            result.update(Address(address).get_original_dict())
        return result


class LineXpathRule(xpathRule):
    __type_res__ = 'Line'

    def process_res(self, res, xpath):
        return [Line(line).dict for line in res]


class CustomEdiElementWithXpath(ediElementWithXpath):

    custom_xpath_rules = {
        'Address': AddressXpathRule,
        'original_addresses': OriginalAddressesXpathRule,
        'Line': LineXpathRule,
    }


class Line(ediElementWithXpath):

    xpaths = {
        # Order Line Number
        # Line number unique to this order request.
        # PO line number. Required for DSV orders and must be blank for 3PL orders
        # Max 5 digits.
        'id': ('str', 'PO1.1'),
        # Item Quantity
        # Quantity of  item ordered. Max 4 decimals.
        'qty': ('int', 'PO1.2'),
        # Item Cost
        # Cost of the item in the line. More specifically, this is the cost of the WIN
        # associated with the SKU-DSV combination at the time of PO creation.
        # Assumption: There will only be 1 WIN for each UPC against a SKU for a single DSV.

        # Item Cost is now Optional(if there more than one active WIN for SKU-DSV
        # combination, then system can’t decide which cost to pick. In that case we wouldn’t send Item Cost.
        # Max 2 decimals.
        # Default  to zero
        'cost': ('float', 'PO1.4'),
        'merchantSKU': ('str', 'PO1.7'),
        'external_customer_line_id': ('str', 'PO1.1'),
        'vendorSKU': ('str', 'PO1:*:*:*:*:*:*:*:*:*:VN.11'),
        'walmart_ca_sku_id': ('str', 'SLN:*:*:*:*:*:*:*:*:*:*:*:*:SK.14'),
        'name': ('str', 'PID:F:*:*:1.5'),
        # SAC*N*D430 Goods and Services Charge
        # Retail Price for each item in this order line
        'retail_price': ('float', 'SAC:N:D340:ZZ.8'),
        'additional_fields': {
            'sequence_number': ('str', 'SLN.1'),
            # Unit of measure( EA, KG, LB) Canada 1.3 will only use EA
            'uom': ('str', 'PO1.3'),
            # Basis of Unit Price Code
            # Code identifying the type of unit price for an item
            # QE - Quoted Price per Each
            'unit_price_code': ('str', 'PO1.5'),
            'language_descriptions': {
                'language1': ('str', 'PID:F:*:*:1.5'),
                'language2': ('str', 'PID:F:*:*:2.5'),
            },
            # Ship Alone
            # This flag indicates that this item should not ship in the same box
            # as anything else, but in its own shipbox. Generally this flag is just
            # a cross-check because ‘shipalone’ SKUs are put in their own
            # Order Parts (POs/releases) by Yantra
            'ship_alone': ('str', 'REF:SS.2'),
            # Ship in original box
            # This flag indicates that this item should ship in the original
            # vendor box and not be boxed at the fulfiller
            'ship_in_original_box': ('str', 'REF:CB.2'),
            # Service, Promotion, Allowance, or Charge Information
            # SAC*N*G830 Shipping and Handling
            'shipping_charge': ('float', 'SAC:N:G830:ZZ.8'),
            # SAC*N*F050 Misc Charges
            'misc_charge': {
                'value': ('float', 'SAC:N:F050:ZZ.8'),
                'misc_charge_name_language_1': ('str', 'SAC:N:F050:ZZ.13'),
                'misc_charge_name_language_2': ('str', 'SAC:N:F050:ZZ.15'),
            },
            # SAC*N*C310 Discount
            'amounts': {
                'discount_1': {
                    # The word “Percent” or “Amount
                    'type': ('str', 'SAC:N:C310->0.4'),
                    # Discount sequence
                    'sequence': ('str', 'SAC:N:C310->0.5'),
                    # The amount off or percentage off.
                    # “Amount”-type discounts are per-unit, not per line
                    'value': ('float', 'SAC:N:C310->0.8'),
                    'name_lang_1': ('str', 'SAC:N:C310->0.13'),
                    'name_lang_3': ('str', 'SAC:N:C310->0.15'),
                },
                'discount_2': {
                    # The word “Percent” or “Amount
                    'type': ('str', 'SAC:N:C310->1.4'),
                    # Discount sequence
                    'sequence': ('str', 'SAC:N:C310->1.5'),
                    # The amount off or percentage off.
                    # “Amount”-type discounts are per-unit, not per line
                    'value': ('float', 'SAC:N:C310->1.8'),
                    'name_lang_1': ('str', 'SAC:N:C310->1.13'),
                    'name_lang_3': ('str', 'SAC:N:C310->1.15'),
                },
                # Tax Information
                # Tax Rate (e.g., 0.0 through 1.0 represents 0% through 100%)
                # Canada Tax types: HST, GST, GST/PST and GST/QST.
                'tax_information': {
                    'hst': {
                        'amount': ('float', 'TXI:GS:*:*:*:*:*:*:*:HST.2'),
                        'rate': ('float', 'TXI:GS:*:*:*:*:*:*:*:HST.3'),
                    },
                    'gst': {
                        'amount': ('float', 'TXI:GS:*:*:*:*:*:*:*:GST.2'),
                        'rate': ('float', 'TXI:GS:*:*:*:*:*:*:*:GST.3'),
                    },
                    'gst_pst': {
                        'amount': ('float', 'TXI:GS:*:*:*:*:*:*:*:PST.2'),
                        'rate': ('float', 'TXI:GS:*:*:*:*:*:*:*:PST.3'),
                    },
                    'gst_qst': {
                        'amount': ('float', 'TXI:GS:*:*:*:*:*:*:*:QST.2'),
                        'rate': ('float', 'TXI:GS:*:*:*:*:*:*:*:QST.3'),
                    },
                },
            },
        },
    }


class Address(AddressEdiElementWithXpath):

    xpaths = {
        'type': ('str', 'N1.1'),
        'name': ('str', 'N1.2'),
        'last_name': ('str', 'N2.1'),
        'middle_name': ('str', 'N2.2'),
        'address1': ('str', 'N3->0.1'),
        'address2': ('str', 'N3->0.2'),
        'address3': ('str', 'N3->1.1'),
        'address4': ('str', 'N3->1.2'),
        'city': ('str', 'N4.1'),
        'state': ('str', 'N4.2'),
        'zip': ('str', 'N4.3'),
        'country': ('str', 'N4.4'),
        'phone': ('str', 'PER:RE:*:TE.4'),
        'email': ('str', 'PER:RE:*:TE:*:EM.6'),
    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        res_type = address_type_map.get(res['type'], 'order')
        del res['type']
        result = {
            res_type: {
                'name': ' '.join(
                    [
                        value for key, value in res.items()
                        if key in ['name', 'last_name', 'middle_name'] and value
                    ]
                ),
                'address1': res['address1'],
                'address2': res['address2'],
                'address3': res['address3'],
                # TODO: need to store it or use?
                # 'address4': res['address4'],
                'city': res['city'],
                'state': res['state'],
                'zip': res['zip'],
                'country': res['country'],
                'phone': res['phone'],
                'email': res['email'],
            }
        }
        return result


class Order(CustomEdiElementWithXpath):

    xpaths = {
        # PO Number
        'poNumber': ('str', 'BEG:00:DS.3'),
        'order_id': ('str', 'BEG:00:DS.3'),
        # The date/time the order was created. EST time zone
        'external_date_order': ('date', 'BEG:00:DS.5'),
        # Customer’s website order number. Note: Only for printing on the packing slip.
        # Max 13 digits
        'external_customer_order_id': ('str', 'REF:CO.2'),
        'address': ('Address', 'N1<-PER(N2,N3,N4)'),
        'carrier': ('static', ''),
        'additional_fields': {
            'addresses': ('original_addresses', 'N1<-PER(N2,N3,N4)'),
            'LU': ('str', 'REF:LU.2'),
            'VR': ('str', 'REF:VR.2'),
            # Code (Standard ISO) for country in whose currency the charges are specified
            'currency': ('str', 'CUR:BY.2'),
            # Country Code
            # Max 2 characters
            'location_number': ('str', 'REF:LU.2'),
            # Order Type
            # SHIPTOHOME, SITETOSTORE. (Currently default to SHIPTOHOME)
            # Max 40 characters
            'order_type': ('str', 'REF:4C.2'),
            'vendor_id_number': {
                # 6 digit DSV Id + 3 digit location ID all created in MTEP and shared with DSV.
                # Max 9 digits.
                'ship_node':  ('str', 'REF:VR.2'),
                # DSV Name
                'vendor_name': ('str', 'REF:VR.3'),
            },
            'languages': {
                # Container for list of up to 3 languages for use in printing the packing slip.
                # Default if this tag is not present is “eng_USA”
                'language1': ('str', 'REF:YY.2'),
                'language2': ('str', 'REF:YY.3'),
            },
            'paynment_type': {
                # “Credit Card” or “Gift Card. Max 40 chars
                'payment_type': ('str', 'REF:4N.2'),
                # Printable identifier to distinguish this payment instrument.
                # This is the string to print on the packing slip. Max 40 chars
                'printableid': ('str', 'REF:4N.3'),
            },
            # if ‘Y’, use a version of the packing slip without prices
            'show_no_prices': ('flag', 'REF:PI.2'),
            # Is address a PO Box?
            'po_box_flag': ('flag', 'REF:MBX.2'),
            'order_dates': {
                # DTM*004 - DTM:004
                # 004 - Purchase Order
                'po_datetime_expressed': {
                    'date': ('str', 'DTM:004.2'),  # Purchase Order Date expressed as CCYYMMDD
                    'time': ('str', 'DTM:004.3'),  # Time expressed in 24-hour clock time as follows: HHMM
                    # Code identifying the time.
                    # In accordance with International Standards Organization standard 8601,
                    # time can be specified by a + or - and an indication in hours in relation
                    # to Universal Time Coordinate (UTC) time;
                    # since + is a restricted character, + and - are substituted by P and M in the codes that follow
                    'code': ('str', 'DTM:004.4'),  # GM - Greenwich Mean Time
                },
                # DTM*168 - DTM:168
                # 168 - Release
                'release_datetime_expressed': {
                    'date': ('str', 'DTM:168.2'),  # Release Date expressed as CCYYMMDD
                    'time': ('str', 'DTM:168.3'),  # Time expressed in 24-hour clock time as follows: HHMM
                    # Code identifying the time.
                    # In accordance with International Standards Organization standard 8601,
                    # time can be specified by a + or - and an indication in hours in relation
                    # to Universal Time Coordinate (UTC) time;
                    # since + is a restricted character, + and - are substituted by P and M in the codes that follow
                    'code': ('str', 'DTM:168.4'),  # GM - Greenwich Mean Time
                },
                # DTM*010 - DTM:010
                # 010 - Requested Ship
                'ship_datetime_expressed': {
                    'date': ('str', 'DTM:010.2'),  # Ship Date expressed as CCYYMMDD
                    'time': ('str', 'DTM:010.3'),  # Ship Time expressed in 24-hour clock time as follows: HHMM
                    # Code identifying the time.
                    # In accordance with International Standards Organization standard 8601,
                    # time can be specified by a + or - and an indication in hours in relation
                    # to Universal Time Coordinate (UTC) time;
                    # since + is a restricted character, + and - are substituted by P and M in the codes that follow
                    'code': ('str', 'DTM:010.4'),  # GM - Greenwich Mean Time
                },
            },
            # TD5*O*93 - 'TD5:O:93'
            # O - Origin Carrier (Air, Motor, or Ocean)
            # 93 - Code designating the system/method of code structure used for Identification Code (67)
            'carrier_details': {
                # See Appendix B below. Max 3 digits
                'carrier_method_code': ('str', 'TD5:O:93.3'),
                # Carrier Name Code. See Appendix A below. Max 3 digits.
                'carrier_name_code': ('str', 'TD5:O:93.5'),
                # Code indicating the level of transportation service
                # or the billing service offered by the transportation carrier
                # CX - Express Service
                # R5 - Manifest Freight
                # SG - Standard Ground
                # DT - Electronic Delivery
                'level_code': ('str', 'TD5:O:93.12'),
            },
        },
        'lines': ('Line', 'PO1<-SLN(PID,REF,SAC,TXI)'),
        'number_of_transaction': ('int', 'GE.1')
    }

    def to_dict(self):
        res = {}
        for name in self.xpaths:
            res.update({name: self.__getattr__(name)})
        order_dates = res.get('additional_fields', {}).get('order_dates', {})
        if order_dates:
            for order_date_name, order_date_dict in order_dates.items():
                order_date_dict.update({
                    'datetime': str2date(order_date_dict['date'] + order_date_dict['time'])
                })
        cd = res['additional_fields']['carrier_details']
        res.update({
            'carrier': ','.join(
                [
                    cd['carrier_method_code'],
                    cd['carrier_name_code'],
                    cd['level_code']
                ]
            )
        })
        return res


class WalmartCanadaEDIParser(BaseEDIParser):

    def __init__(self, parent):
        super(WalmartCanadaEDIParser, self).__init__(parent)
        self.Order = Order
