# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.import_orders_normalize import normalize_csv
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from datetime import datetime
import traceback
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError


class CSVParser():

    ids = {
        'partner_id': 'merchantid',
        'order_id': 'ordernumber',
        'carrier': 'servicetype',
        'ship_name': 'name',
        'ship_address1': 'address1',
        'ship_address2': 'address2',
        'ship_city': 'city',
        'ship_state': 'state',
        'ship_zip': 'zip',
        'ship_country': 'country',
        'ship_phone': 'phone',
        'order_date': 'orderdate',
        'line_customer_sku': 'merchantsku',
        'line_qty': 'quantity',
        'line_delmar_id': 'vendorsku',
        'line_size': 'size',
        'line_description': 'description',
        'line_gift_message': 'giftmessage',
        'line_tax': 'tax',
        'line_price_unit': 'unitprice',
        # for order
        'tax_percentage': 'taxpercentage',
        'shipping_fee': 'shippingfee',
        'shipping_tax': 'shippingtax',
        'total_due': 'totaldue',
        # for line
        'line_subtotal': 'subtotal',
        'line_tax_amount': 'taxamount',
        'line_discount_amount': 'discountamount',
        'line_row_total': 'rowtotal',
    }

    def __init__(self, customer=None, ids=None):
        self.external_customer_id = customer
        if(ids):
            self.ids = ids

    def _get_lines_from_csv(self, _data):
        lines = []
        _pseudo_file = IO(_data)
        csv_dialect = detect_dialect(_data)
        if csv_dialect:
            if csv_dialect.delimiter == ' ' or str(csv_dialect.delimiter).isalpha():
                csv_dialect.delimiter = ','
            if csv_dialect.quotechar == "'":
                csv_dialect.quotechar = '"'
        dr = DictReader(_pseudo_file, dialect=csv_dialect)
        for row in dr:
            lines.append(row)
        return lines

    def _get_value(self, line, name, default_value=False):
        name_column = None
        try:
            name_column = self.ids[name]
        except KeyError:
            name_column = False
        finally:
            if(name_column):
                result = line.get(name_column, default_value)
                if(isinstance(result, (unicode, str))):
                    result = result.strip()
            else:
                result = default_value
            return result

    def _get_order_obj(self, line):
        result = {
            'partner_id': self._get_value(line, 'partner_id', self.external_customer_id),
            'order_id': self._get_value(line, 'order_id'),
            'poNumber': self._get_value(line, 'order_id'),
            'carrier': self._get_value(line, 'carrier'),
            'address': {
                'ship': {
                    'name': self._get_value(line, 'ship_name'),
                    'address1': self._get_value(line, 'ship_address1'),
                    'address2': self._get_value(line, 'ship_address2'),
                    'city': self._get_value(line, 'ship_city'),
                    'state': self._get_value(line, 'ship_state'),
                    'zip': self._get_value(line, 'ship_zip'),
                    'country': self._get_value(line, 'ship_country'),
                    'phone': self._get_value(line, 'ship_phone'),
                },
            },
            'lines': [],
            'tax': 0.0,
            'discount_total': 0.0,
        }
        external_date_order = datetime.now()
        date_in_file = self._get_value(line, 'order_date')
        if date_in_file:
            try:
                external_date_order = datetime.strptime(date_in_file, '%d/%m/%Y')
            except ValueError:
                pass
        result.update({
            'external_date_order': external_date_order.date().isoformat(),
        })
        if self._get_value(line, 'tax_percentage'):
            result.update({
                'ice_tax_percentage': self._get_value(line, 'tax_percentage'),
                'shipping_fee': self._get_value(line, 'shipping_fee', 0),
                'shipping_tax': self._get_value(line, 'shipping_tax', 0),
                'ice_total_due': self._get_value(line, 'total_due', 0),
                'shipping': self._get_value(line, 'shipping_fee', 0) + self._get_value(line, 'shipping_tax', 0),
            })
        return result

    def _get_line_obj(self, line):
        line_obj = {
            'id': False,
            'merchantSKU': self._get_value(line, 'line_customer_sku'),
            'qty': self._get_value(line, 'line_qty'),
            'vendorSku': self._get_value(line, 'line_delmar_id'),
            'originvendorSku': self._get_value(line, 'line_delmar_id'),
            'size': self._get_value(line, 'line_size'),
            'name': self._get_value(line, 'line_description'),
            'shipCost': 0,
            'customerCost': self._get_value(line, 'line_price_unit'),
            'Message': self._get_value(line, 'line_gift_message'),
        }
        if self._get_value(line, 'line_subtotal'):
            line_obj.update({
                'ice_sub_total': self._get_value(line, 'line_subtotal', 0),
                'ice_tax_amount': self._get_value(line, 'line_tax_amount', 0),
                'ice_discount': self._get_value(line, 'line_discount_amount', 0),
                'ice_row_total': self._get_value(line, 'line_row_total', 0),
            })
        else:
            line_obj.update({
                'ice_sub_total': 0,
                'ice_tax_amount': 0,
                'ice_discount': 0,
                'ice_row_total': 0,
            })
        return line_obj

    def _add_line_to_order_obj(self, order_obj, _line):
        line_obj = self._get_line_obj(_line)
        add_new_line = True
        curr_line_id = 1
        for line in order_obj['lines']:
            curr_line_id += 1
            condition = ((line['merchantSKU'] == line_obj['merchantSKU']) and(line['size'] == line_obj['size']))
            if(condition):
                if(isinstance(line_obj['qty'], int)):
                    line['qty'] += line_obj['qty']
                else:
                    line['qty'] += 1
                add_new_line = False
                break
        if(add_new_line):
            if(not line_obj['id']):
                line_obj['id'] = curr_line_id
                order_obj['lines'].append(line_obj)
        order_obj['tax'] += float(_try_parse(line_obj['ice_tax_amount'], 'float', 0))
        order_obj['discount_total'] += float(_try_parse(line_obj['ice_discount'], 'float', 0))
        return order_obj

    def parse_data(self, csv_data):
        result = {
            'errors': [],
            'orders_list': [],
        }
        result_normalize = normalize_csv(data=csv_data)
        if(not result_normalize['normalize_ok']):
            raise Exception("Couldn't normalize csv file data :\n{0}".format(csv_data))
        csv_data = result_normalize['csv_data']
        csv_data = strip_lower_del_spaces_from_header(csv_data)
        list_of_orders_obj = {}
        try:
            for line in self._get_lines_from_csv(csv_data):
                order_id = self._get_value(line, 'order_id')
                if(not order_id):
                    raise OrderIDError("Order ID not find in line: {0}".format(str(line)))
                if(not list_of_orders_obj.get(order_id, False)):
                    list_of_orders_obj[order_id] = self._get_order_obj(line)
                list_of_orders_obj[order_id] = self._add_line_to_order_obj(list_of_orders_obj[order_id], line)
            yaml_obj = YamlObject()
            if(not result['errors']):
                for order_id in list_of_orders_obj:
                    result_order_obj = {
                        'name': order_id,
                        'xml': yaml_obj.serialize(_data=list_of_orders_obj[order_id])
                    }
                    result['orders_list'].append(result_order_obj)
        except Exception:
            _msg = traceback.format_exc()
            result['errors'].append(_msg)
        finally:
            return result
