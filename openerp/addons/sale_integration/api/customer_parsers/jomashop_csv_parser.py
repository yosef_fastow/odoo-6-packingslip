# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.import_orders_normalize import normalize_csv
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from openerp.addons.pf_utils.utils.csv_utils import strip_lower_del_spaces_from_header
from csv import DictReader
from cStringIO import StringIO as IO
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import traceback
from openerp.addons.sale_integration.api.customer_parsers.exceptions import OrderIDError
import re

class CSV_parser():
    ids = {
        'order_id': 'ordernumber',
        'partner_id': 'store',
        'shipname': 'name',
        'shipadd1': 'address',
        'shipadd2': 'address2',
        'shipcity': 'city',
        'shipstate': 'state',
        'shipzip': 'zip',
        'shipcountry': 'country',
        'shipphone': 'phone',
        'ship_via': 'servicetype',
        'ship_service': 'shipmethod',
        'gift': 'giftmessage',
        'tax': 'tax',
        'date': 'orderdate',
        'line_number': 'itemlinenumber',
        'desc': 'description',
        'merchantSku': 'merchantsku',
        'vendorSku': 'vendorsku',
        'size': 'size',
        'qty': 'quantity',
        'cost': 'unitcost',
        'customer_cost': 'unitprice',
        'zuck_order': 'zuckorder',
        'zuck_item': 'zuckitem',
    }

    def __init__(self, customer=None, ids=None):
        self.external_customer_id = customer
        if(ids):
            self.ids = ids

    def _get_lines_from_csv(self, _data):
        lines = []
        _pseudo_file = IO(_data)
        csv_dialect = detect_dialect(_data)
        dr = DictReader(_pseudo_file, dialect=csv_dialect)
        for row in dr:
            lines.append(row)
        return lines

    def _get_value(self, line, name, default_value=False):
        name_column = None
        try:
            name_column = self.ids[name]
        except KeyError:
            name_column = False
        finally:
            if(name_column):
                result = line.get(name_column, default_value)
                if(isinstance(result, (unicode, str))):
                    result = result.strip()
            else:
                result = default_value
            return result

    def _get_order_obj(self, line):
        result = {
            'partner_id': self._get_value(line, 'partner_id', self.external_customer_id),
            'order_id': self._get_value(line, 'order_id'),
            'poNumber': self._get_value(line, 'order_id'),
            'carrier': self._get_value(line, 'ship_via', 'Jomashop'),
            'ship_service': self._get_value(line, 'ship_service'),
            'address': {
                'ship': {
                    'name': self._get_value(line, 'shipname'),
                    'address1': self._get_value(line, 'shipadd1'),
                    'address2': self._get_value(line, 'shipadd2'),
                    'city': self._get_value(line, 'shipcity'),
                    'state': self._get_value(line, 'shipstate'),
                    'zip': self._get_value(line, 'shipzip'),
                    'country': self._get_value(line, 'shipcountry'),
                    'phone': re.sub("\D", "", self._get_value(line, 'shipphone')) if self._get_value(line, 'shipcountry') in ['CA', 'US', 'PR'] else self._get_value(line, 'shipphone')
                },
            },
            'gift': self._get_value(line, 'gift'),
            'tax': self._get_value(line, 'tax'),
            'date': self._get_value(line, 'date'),
            'lines': [],
            'bulk_transfer': True if self._get_value(line, 'shipcountry') not in ['US'] else False
        }
        return result

    def _get_line_obj(self, line):
        line_obj = {
            'id': False,
            'line_number': self._get_value(line, 'line_number'),
            'name': self._get_value(line, 'desc'),
            'merchantSKU': self._get_value(line, 'merchantSku'),
            'vendorSku': self._get_value(line, 'vendorSku'),
            'size': self._get_value(line, 'size'),
            'qty': self._get_value(line, 'qty'),
            'cost': self._get_value(line, 'cost'),
            'customerCost': self._get_value(line, 'customer_cost'),
            'zuck_order': self._get_value(line, 'zuck_order'),
            'zuck_item': self._get_value(line, 'zuck_item'),
        }
        for name_value in ['cost', 'customerCost', 'size']:
            if(line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'float')
        for name_value in ['qty']:
            if(line_obj[name_value]):
                line_obj[name_value] = _try_parse(line_obj[name_value], 'int')
        return line_obj

    def _add_line_to_order_obj(self, order_obj, _line):
        line_obj = self._get_line_obj(_line)
        add_new_line = True
        curr_line_id = 1
        for line in order_obj['lines']:
            curr_line_id += 1
            condition = ((line['merchantSKU'] == line_obj['merchantSKU']) and(line['size'] == line_obj['size']))
            if(condition):
                if(isinstance(line_obj['qty'], int)):
                    line['qty'] += line_obj['qty']
                else:
                    line['qty'] += 1
                add_new_line = False
                break
        if(add_new_line):
            if(not line_obj['id']):
                line_obj['id'] = curr_line_id
                order_obj['lines'].append(line_obj)
        return order_obj

    def parse_data(self, csv_data):
        result = {
            'errors': [],
            'orders_list': [],
        }
        result_normalize = normalize_csv(data=csv_data)
        if(not result_normalize['normalize_ok']):
            raise Exception("Couldn't normalize csv file data :\n{0}".format(csv_data))
        csv_data = result_normalize['csv_data']
        csv_data = strip_lower_del_spaces_from_header(csv_data)
        list_of_orders_obj = {}
        try:
            for line in self._get_lines_from_csv(csv_data):
                order_id = self._get_value(line, 'order_id')
                if(not order_id):
                    raise OrderIDError("Order ID not find in line: {0}".format(str(line)))
                if(not list_of_orders_obj.get(order_id, False)):
                    list_of_orders_obj[order_id] = self._get_order_obj(line)
                list_of_orders_obj[order_id] = self._add_line_to_order_obj(list_of_orders_obj[order_id], line)
            yaml_obj = YamlObject()
            if(not result['errors']):
                for order_id in list_of_orders_obj:
                    result_order_obj = {
                        'name': order_id,
                        'xml': yaml_obj.serialize(_data=list_of_orders_obj[order_id])
                    }
                    result['orders_list'].append(result_order_obj)
        except Exception:
            _msg = traceback.format_exc()
            result['errors'].append(_msg)
        finally:
            return result
