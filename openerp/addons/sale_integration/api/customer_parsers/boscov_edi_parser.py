# -*- coding: utf-8 -*-
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from ..utils.ediparser import EdiParser
import traceback
import json
from base_edi import BadSenderError
from base_edi import BaseEDIParser
import dateutil.parser


class EDIParser(BaseEDIParser):

    def __init__(self, parent):
        self.delimiters = '*'
        super(EDIParser, self).__init__(parent)

    def parse_data(self, edi_data):
        if (not edi_data):
            return self.result
        orders = []
        try:
            message = EdiParser(edi_data)
            for segment in message:
                elements = segment.split(self.delimiters)
                if elements[0] == 'GS':
                    self.ack_control_number = elements[6]
                    self.functional_group = elements[1]
                    self.sender = elements[2]
                    self.receiver = elements[3]
                    if (self.sender != self.parent.receiver_id):
                        raise BadSenderError('Bad sender file! Correct sender_id: {0}'.format(self.parent.receiver_id))
                elif elements[0] == 'ST':
                    ordersObj = {
                        'address': {},
                        'partner_id': self.parent.customer,
                        'lines': [],
                        'cancellation': False,
                        'change': False,
                        'additional_fields': {},
                        'ack_control_number': self.ack_control_number,
                        'functional_group': self.functional_group,
                        'poHdrData': {}
                    }
                    address_type = ''
                    if elements[1] == '850':  # Order
                        pass
                    elif elements[1] == '860':  # Change
                        pass
                    elif elements[1] == '997':  # Aknowlegment skip this file
                        return self.result
                elif elements[0] == 'BEG':
                    ordersObj['order_id'] = elements[3]
                    ordersObj['poNumber'] = elements[3]
                    ordersObj['external_date_order'] = elements[5]
                elif elements[0] == 'REF':

                    ordersObj['additional_fields'].update({
                        'REF02': elements[1]
                    })

                    if elements[1] == 'CO':  # Customer Order Number
                        ordersObj['additional_fields'].update({
                            'customer_order_number': elements[2]
                        })
                        ordersObj['external_customer_order_id'] = elements[2]
                    elif elements[1] == 'IA':  # Internal Vendor Number
                        ordersObj['additional_fields'].update({
                            'internal_vendor_number': elements[2]
                        })
                elif elements[0] == 'PER':
                    addr_types_map = {
                        'OC': 'invoice',  # Order Contact
                        'RE': 'ship',  # Receiving Contact
                    }
                    if elements[1] in addr_types_map:
                        addr = ordersObj.get('address', {}).get(addr_types_map[elements[1]])
                        if addr:
                            addr.update({
                                'name': elements[2],
                                'phone': elements[4] if len(elements) > 3 else '',
                                'email': elements[6] if len(elements) > 7 else '',
                            })
                elif elements[0] == 'DTM':
                    if elements[1] == '002':  # Delivery Requested
                        ordersObj['additional_fields'].update({
                            'customer_delivery_request_date': dateutil.parser.parse(elements[2])
                        })
                    elif elements[1] == '004':  # Purchase Order
                        dtm = elements[2]
                        if len(elements) == 4:
                            dtm += elements[3]
                        ordersObj['additional_fields'].update({
                            'customer_delivery_request_date': dateutil.parser.parse(dtm)
                        })
                    elif elements[1] == '001':  # Cancel After
                        # If the order has not been shipped by this date, the order is considered canceled
                        ordersObj['additional_fields'].update({
                            'cancel_after': dateutil.parser.parse(elements[2])
                        })
                    elif elements[1] == '830':  # Schedule
                        ordersObj['additional_fields'].update({
                            'schedule_date': dateutil.parser.parse(elements[2])
                        })

                # todo: PWK*R5****UR*customer-service*boscovs.com~

                elif elements[0] == 'TD5':
                    if len(elements) >= 6:
                        ordersObj['carrier'] = elements[5]
                    elif len(elements) >= 4:
                        ordersObj['carrier'] = elements[3]
                elif elements[0] == 'N1':

                    if elements[1] in ['BT']:  # Bill-to-Party, Vendor
                        address_type = 'order'
                        ordersObj['address']['order'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }
                        ordersObj['address']['order']['name'] = elements[2]

                    elif elements[1] == 'ST':  # Ship To
                        address_type = 'ship'
                        ordersObj['address']['ship'] = {
                            'name': '',
                            'name2': '',
                            'address1': '',
                            'address2': '',
                            'city': '',
                            'state': '',
                            'zip': '',
                            'country': '',
                            'phone': '',
                            'email': ''
                        }

                        ordersObj['address']['ship']['name'] = elements[2]

                elif elements[0] == 'N2' and address_type != '':
                    ordersObj['address'][address_type]['name2'] = elements[1]
                elif elements[0] == 'N3' and address_type != '':
                    ordersObj['address'][address_type]['address1'] = elements[1]
                    if len(elements) > 2:
                        ordersObj['address'][address_type]['address2'] = elements[2]
                elif elements[0] == 'N4' and address_type != '':
                    ordersObj['address'][address_type]['city'] = elements[1]
                    ordersObj['address'][address_type]['state'] = elements[2]
                    ordersObj['address'][address_type]['zip'] = elements[3]
                    ordersObj['address'][address_type]['country'] = 'US'
                    if len(elements) == 5:
                        ordersObj['address'][address_type]['country'] = elements[4]
                elif elements[0] == 'PO1':

                    lineObj = {
                        'id': elements[1],
                        'qty': elements[2],
                        'cost': elements[4],
                        'sku': elements[13],
                        'vendorSku': elements[13],
                        'customer_id_delmar': elements[9],
                        'upc': elements[13],
                        'customer_sku': elements[11],
                        'optionSku': elements[11],
                        'name': elements[11],
                        'poLineData': {},
                        'additional_fields': {}
                    }

                    if (elements[9]):
                        lineObj['additional_fields'].update({
                            elements[12]: elements[13]
                        })
                        lineObj['poLineData'][elements[12]] = elements[13]
                    if (elements[6]):
                        lineObj['additional_fields'].update({
                            elements[6]: elements[7]
                        })
                    if (elements[12] == 'PL'):
                        lineObj['additional_fields'].update({
                            'po_line_number': elements[13]
                        })
                    if len(elements) > 15 and elements[7] and elements[15] and elements[7].endswith(
                            'J' + str(elements[15])):
                        lineObj['size'] = elements[15]
                    ordersObj['lines'].append(lineObj)
                elif elements[0] == 'PID':
                    if elements[2] == '08':
                        lineObj['name'] = elements[5]
                    if elements[2] == '35':
                        lineObj['color'] = elements[5]
                        lineObj['poLineData']['prodColor'] = elements[5]
                    if elements[2] == '91':
                        lineObj['description'] = elements[5]
                        lineObj['poLineData']['prodSize'] = elements[5]
                elif elements[0] == 'SAC':
                    if elements[1] == 'C' and elements[2] == 'G830':  # Customer’s shipping and handling
                        ordersObj['shipping_tax'] = elements[5]

                    if elements[1] == 'F':
                        if elements[2] == '08':
                            pass
                        if elements[2] == '35':
                            pass
                        if elements[2] == '91':
                            pass
                    if elements[2] == 'H151':
                        ordersObj['additional_fields'].update({
                            'gift_message': elements[15] if 15 < len(elements) else '',
                        })

                        ordersObj['poHdrData'].update({
                            'gift_message': elements[15] if 15 < len(elements) else '',
                            'giftIndicator': 'y'
                        })
                    if elements[2] == 'D340':
                        try:
                            lineObj['customerCost'] = float(float(elements[5]) / 100) / int(lineObj['qty'])
                        except:
                            lineObj['customerCost'] = 0
                        ordersObj['additional_fields'].update({
                            'goods_and_services_charge': elements[13] if 13 < len(elements) else '',
                        })
                    if elements[2] == 'F150':
                        ordersObj['additional_fields'].update({
                            'gift_registry': elements[13] if 13 < len(elements) else '',
                        })
                elif elements[0] == 'SE':
                    orders.append(ordersObj)
                elif elements[0] == 'GE':
                    self.number_of_transaction = elements[1]
                elif elements[0] == 'IEA':
                    ordersObj['additional_fields'].update({
                        'number_of_included_functional_group': elements[1],
                        'interchange_control_number': elements[2],
                    })
                elif elements[0] == 'SE':
                    orders.append(ordersObj)


        except BadSenderError:
            return self.result
        except Exception:
            _msg = traceback.format_exc()
            self._append_error(_msg)
        yaml_obj = YamlObject()
        for order_obj in orders:
            returned_order = {}
            try:
                order_obj['number_of_transaction'] = self.number_of_transaction
                if (order_obj['cancellation'] is True):
                    returned_order['name'] = 'CANCEL{0}'.format(order_obj['order_id'])
                elif (order_obj['change'] is True):
                    returned_order['name'] = 'CHANGE{0}'.format(order_obj['order_id'])
                else:
                    returned_order['name'] = order_obj['order_id']

                if (order_obj['additional_fields']):
                    serialize_additional_fields = {}
                    for key, value in order_obj['additional_fields'].iteritems():
                        if (isinstance(value, (tuple, list))):
                            serialize_additional_fields.update({
                                key: json.dumps(value)
                            })
                        else:
                            serialize_additional_fields.update({
                                key: value
                            })
                    order_obj['additional_fields'] = serialize_additional_fields
                returned_order['xml'] = yaml_obj.serialize(_data=order_obj)
                self.result['orders_list'].append(returned_order)
            except Exception:
                _msg = traceback.format_exc()
                self._append_error(_msg)
        return self.result
