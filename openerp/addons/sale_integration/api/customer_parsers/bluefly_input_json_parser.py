# -*- coding: utf-8 -*-
from base_parser import BaseParser, orders_list_result, ElementWithXpath
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from json import loads as json_loads
from pf_utils.utils.re_utils import str2date


class dictElementWithXpath(ElementWithXpath):

    def __init__(self, input_order_obj_str):
        super(dictElementWithXpath, self).__init__()
        self.input_obj = json_loads(input_order_obj_str)

    def xpath_get(self, path):
        elem = self.input_obj
        try:
            for x in path.strip(".").split("."):
                elem = elem.get(x)
        except:
            pass
        return elem

    def __get_elem__(self, name=None, type_res=None, xpath=None):
        result = None
        try:
            xpath = xpath or self.xpaths[name][1]
            type_res = type_res or self.xpaths[name][0]
            if type(xpath) == list:
                items = []
                for xp in xpath:
                    items.append(str(self.xpath_get(xp)))
                res = str(" ".join(items))
            # found first existed and not 0
            elif type(xpath) == tuple:
                for item in xpath:
                    res = self.xpath_get(item)
                    if res:
                        break
            else:
                res = self.xpath_get(xpath)
            if type_res == str and res:
                result = str(res)
            elif type_res == 'date' and res:
                result = str2date(res)
            elif type_res == bool:
                result = res
            elif type_res == int and res:
                result = _try_parse(res, 'int')
            elif type_res == float and res:
                result = _try_parse(res, 'float')
            elif type_res == Line:
                result = []
                for line in res:
                    result.append(Line(line).dict)
            else:
                result = None
        except Exception:
            result = False
        return result


class Line(dictElementWithXpath):

    xpaths = {
        'additional_fields': {
            'base_price': (float, 'price_unit'),
            'item_shipping_cost': (float, 'shipping_price'),
            #'item_shipping_tax': (float, 'item_price.item_shipping_tax'),
            #'item_tax': (float, 'total_commission'),
            #'item_tax_code': (str, 'commission_taxes.code'),
            #'url': (str, 'url'),
        },
        'external_customer_line_id': (int, 'order_line_id'),
        'qty': (int, 'quantity'),
        'request_order_cancel_qty': (int, 'cancelations.quantity'),
        'request_order_quantity': (int, 'quantity'),
        'merchantSKU': (str, 'offer_sku'),
        'price_unit': (float, ('discount_price', 'price',)),
        'id': (str, 'order_line_id'),
        'name': (str, 'product_title'),
    }

    def __init__(self, dict_line):
        super(ElementWithXpath, self).__init__()
        self.input_obj = dict_line


class Order(dictElementWithXpath):

    xpaths = {
        'poNumber': (str, 'commercial_id'),
        'order_id': (str, 'order_id'),
        'external_customer_order_id': (str, 'order_id'),
        'external_date_order': ('date', 'created_date'),
        'address': {
            'ship': {
                'name': (str, ['customer.shipping_address.firstname', 'customer.shipping_address.lastname']),
                'address1': (str, 'customer.shipping_address.street_1'),
                'address2': (str, 'customer.shipping_address.street_2'),
                'city': (str, 'customer.shipping_address.city'),
                'state': (str, 'customer.shipping_address.state'),
                'zip': (str, 'customer.shipping_address.zip_code'),
                'country': (str, 'customer.shipping_address.country'),
                'phone': (str, 'customer.shipping_address.phone'),
            },
            'order': {
                'name': (str, ['customer.billing_address.firstname', 'customer.billing_address.lastname']),
                'address1': (str, 'customer.billing_address.street_1'),
                'address2': (str, 'customer.billing_address.street_2'),
                'city': (str, 'customer.billing_address.city'),
                'state': (str, 'customer.billing_address.state'),
                'zip': (str, 'customer.billing_address.zip_code'),
                'country': (str, 'customer.billing_address.country'),
                'phone': (str, 'customer.billing_address.phone'),
            }
        },
        'additional_fields': {
            'buyer': {
                'firstname': (bool, 'customer.firstname'),
                'lastname': (bool, 'customer.lastname'),
                #'phone_number': (bool, 'buyer.phone_number'),
            },
            #'fulfillment_node': (str, 'fulfillment_node'),
            #'has_shipments': (bool, 'has_shipments'),
            #'bluefly_request_directed_cancel': (bool, 'bluefly_request_directed_cancel'),
            'order_detail': {
                #'request_delivery_by': ('date', 'shipping_company'),
                'request_service_level': (str, 'shipping_company'),
                #'request_ship_by': ('date', 'order_detail.request_ship_by'),
                'request_shipping_carrier': (str, 'shipping_carrier_code'),
                'request_shipping_method': (str, 'shipping_type_code'),
            },
            'order_placed_date': ('date', 'created_date'),
            #'order_totals': {
            #    'item_price': {
            #        'base_price': (float, 'order_totals.item_price.base_price'),
            #        'item_shipping_cost': (float, 'shipping_price'),
            #        'item_shipping_tax': (float, 'order_totals.item_price.item_shipping_tax'),
            #        'item_tax': (float, 'order_totals.item_price.item_tax'),
            #    }
            #},
            'status': (str, 'order_state'),
            #'urls': {
            #    'base_url': (str, 'urls.base_url'),
            #    'sub_url': (str, 'urls.sub_url'),
            #    'full_url': (str, 'urls.full_url'),
            #}
        },
        'lines': (Line, 'order_lines'),
    }


class Parser(BaseParser):
    """:class: for parsing input XML data to Yaml object"""
    def __init__(self, parent):
        super(Parser, self).__init__(parent)

    @orders_list_result
    def parse_data(self, json_data):
        self.orders.append(Order(json_data).dict)
        return True
