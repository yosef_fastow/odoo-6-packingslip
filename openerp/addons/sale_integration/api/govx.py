#-*- coding: utf-8 -*-
from lxml import etree
from apiclient import ApiClient
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient, InvoiceMessageManager
from datetime import datetime
import time
from pf_utils.utils.re_utils import f_d
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.xml_utils import clean_namespaces
from utils import feedutils as feed_utils
import copy


SETTINGS_FIELDS = (
    ('username',            'User name',       ''),
    ('password',            'Password',        ''),
)

nm = 'http://GovX.Service.Order3'


class GovXApi(ApiClient):

    def __init__(self, settings):
        self.settings = settings
        self.api_url = settings["url"]
        self.soapenv = "http://schemas.xmlsoap.org/soap/envelope/"
        self.soap_action = "http://GovX.Service.Order3/IOrderWebService/"
        self.nm = nm
        self._log = []
        self.method = ""

    def prepare_data(self, data, store_log=True):
        body = """<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope xmlns:soapenv="{soapenv}" xmlns:govx="{govx}">
                <soapenv:Header/>
                <soapenv:Body>
                    <govx:{action}>
                        {data}
                    </govx:{action}>
                </soapenv:Body>
            </soapenv:Envelope>
            """.format(
                soapenv=self.soapenv,
                govx=self.nm,
                action=self.name,
                data=data
            )
        if store_log:
            self._log.append({
                'title': "Send %s GovX" % self.name,
                'msg': body,
                'type': 'send',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        return ' '.join(body.split())

    def prepare_headers(self, headers, data):
        if headers is None:
            headers = {}

        headers.update({
            "operation": self.name,
            "SOAPAction": "{prefic}{action}".format(prefic=self.soap_action, action=self.name),
            "Content-Length": len(data),
            "Content-Type": "text/xml;charset=UTF-8",
            "Connection": "Keep-Alive",
        })

        return headers

    def check_response(self, response):
        res = True
        if(not response):
            return False

        # TODO: Parse response for edditional checking
        # tree = etree.XML(response)
        return res

    def get_method(self):
        return self.method or "POST"


class GovXFtpApi(FtpClient):

    def __init__(self, settings):
        super(GovXFtpApi, self).__init__(settings)


class GovXApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(GovXApiClient, self).__init__(
            settings_variables, GovXOpenerp, SETTINGS_FIELDS, False, GovXInvoiceMessageManager)

    def loadOrders(self, save_flag=False):
        loadApi = GovXApiGetOrdersXML(self.settings)
        orders = loadApi.send()
        self.extend_log(loadApi)
        return orders

    def confirmLoad(self, orders):
        confirmed_orders = []
        if orders:
            for order in orders:
                ordersApi = GovXApiConfirmLoad(self.settings, order)
                if ordersApi.send():
                    confirmed_orders.append(order)
                self.extend_log(ordersApi)
        return confirmed_orders

    def getOrdersObj(self, xml):
        ordersApi = GovXApiGetOrderObj(xml)
        order = ordersApi.getOrderObj()
        return order

    def confirmShipment(self, lines):
        self.settings["response"] = ""
        confirmed_lines = []
        for line in lines:
            confirmApi = GovXApiConfirmShipment(self.settings, line)
            if confirmApi.send():
                confirmed_lines.append(line)
            self.extend_log(confirmApi)

        return confirmed_lines and len(confirmed_lines) == len(lines)

    def returnOrders(self, lines):
        returned_lines = []
        for line in lines:
            returnApi = GovXApiReturnOrders(self.settings, line)
            if returnApi.send():
                returned_lines.append(line)
            self.extend_log(returnApi)
        return returned_lines and len(returned_lines) == len(lines)

    def processingOrderLines(self, lines, state='accepted'):
        res = False
        processed_lines = []
        if state in ('accept_cancel', 'rejected_cancel', 'cancel'):
            for line in lines:
                ordersApi = GovXApiCancelOrders(self.settings, line)
                res_send = ordersApi.send()
                res &= res_send
                if res_send:
                    processed_lines.append(line)
                self.extend_log(ordersApi)
        else:
            res = True
        return res

    def updateQTY(self, lines, mode=None):
        updateApi = GovXApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class GovXApiGetOrdersXML(GovXApi):

    name = "GetOrders"
    method = "POST"

    def __init__(self, settings):
        super(GovXApiGetOrdersXML, self).__init__(settings)
        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            'govx',
            'load_orders',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'govx'),
            datetime.utcnow()
        )

    def get_request_data(self):
        """
        OrderStatus:
            New : will return all the new orders
            Open : will return all the open orders which need status updates
        """
        data = """
                <govx:username>{username}</govx:username>
                <govx:password>{password}</govx:password>
                <govx:orderStatus>New</govx:orderStatus>
            """.format(
            username=self.settings.get('username', ''),
            password=self.settings.get('password', '')
        )
        return data

    def parse_response(self, response):
        if not response:
            return []
        tree = etree.XML(response)
        ordersList = []
        root_xpath = './/{%s}GetOrdersResponse/{%s}GetOrdersResult/{%s}Orders'.replace("%s", self.nm)
        if(tree is not None and tree.find(root_xpath) is not None):
            root = tree.find(root_xpath)
            orders = root.findall('{%s}Order'.replace("%s", self.nm))
            if(len(orders) > 0):
                for order in orders:
                    ordersObj = {}
                    ordersObj['xml'] = etree.tostring(order, pretty_print=True)
                    ordersObj['name'] = order.findtext('{%s}OrderId'.replace("%s", self.nm))
                    ordersList.append(ordersObj)
        return ordersList


class GovXApiConfirmLoad(GovXApi):

    name = "SetOrderResponse"
    method = "POST"

    def __init__(self, settings, order):
        super(GovXApiConfirmLoad, self).__init__(settings)
        self.order = order

        self._path_to_backup_local_dir = self.create_local_dir(
            settings['backup_local_path'],
            'govx',
            'confirm_load',
            'now'
        )
        self.filename = f_d(
            '{}_%Y%m%d.txt'.format(settings['customer'] or 'govx'),
            datetime.utcnow()
        )

    def get_request_data(self):
        order_xml = ""
        if self.order:

            """
            Load status:
                000 – Successful
                001 – Invalid Order Header format
                002 – Invalid Order Item format
                003 – Ship-to Address Error
                004 – Material Error-Invalid items on the order
                005 – Discontinued / Blocked items on the order.
                006 – Order already exists
                999 - Others
            """

            order_xml = """
                <govx:orderSetResponseRequest>
                    <govx:Username>{username}</govx:Username>
                    <govx:Password>{password}</govx:Password>
                    <govx:OrderId>{order_id}</govx:OrderId>
                    <govx:LoadStatus>000</govx:LoadStatus>
                    <govx:VendorOrderNumber>{vendor_name}</govx:VendorOrderNumber>
                    <govx:Remarks>Loaded Sucessfully</govx:Remarks>
                </govx:orderSetResponseRequest>
            """.format(
                order_id=self.order['name'],
                vendor_name=self.order['name'],
                username=self.settings.get('username', ''),
                password=self.settings.get('password', '')
            )

        return order_xml

    def parse_response(self, response):
        success = False
        if not response:
            return success

        log_msg = {
            'title': "Recive %s GovX" % self.name,
            'msg': response,
            'type': 'recive',
            'create_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

        tree = etree.XML(response)
        if tree:
            status_xpath = './/{%s}SetOrderResponseResponse/{%s}SetOrderResponseResult/{%s}ResultStatus'.replace("%s", nm)
            status = tree.findtext(status_xpath)
            if status == 'Success':
                success = True

            else:
                log_msg['title'] += ' ERROR'

        self._log.append(log_msg)

        return success


class GovXApiGetOrderObj():

    def __init__(self, xml):
        self.xml = xml
        self.nm = nm

    def getOrderObj(self):
        ordersObj = {}

        if(self.xml == ""):
            return ordersObj

        tree = etree.XML(self.xml)

        if(tree is not None):
            ordersObj['order_id'] = tree.findtext('{%s}OrderId'.replace("%s", self.nm))
            if(ordersObj['order_id']):
                ordersObj['external_date_order'] = tree.findtext('{%s}OrderDate'.replace("%s", self.nm))
                ordersObj['partner_id'] = 'GovX'
                ordersObj['address'] = {}
                ordersObj['address']['ship'] = {}
                ordersObj['address']['ship']['name'] = tree.findtext('{%s}CustomerName'.replace("%s", self.nm)) or tree.findtext('{%s}ShipToName'.replace("%s", self.nm))
                ordersObj['address']['ship']['address1'] = tree.findtext('{%s}ShipToAddress1'.replace("%s", self.nm))
                ordersObj['address']['ship']['address2'] = tree.findtext('{%s}ShipToAddress2'.replace("%s", self.nm))
                ordersObj['address']['ship']['city'] = tree.findtext('{%s}ShipToCity'.replace("%s", self.nm))
                ordersObj['address']['ship']['state'] = tree.findtext('{%s}ShipToState'.replace("%s", self.nm))
                ordersObj['address']['ship']['zip'] = tree.findtext('{%s}ShipToZipCode'.replace("%s", self.nm))
                ordersObj['address']['ship']['country'] = tree.findtext('{%s}ShipToCountry'.replace("%s", self.nm)) or 'US'
                ordersObj['address']['ship']['phone'] = tree.findtext('{%s}ShipToPhoneNumber'.replace("%s", self.nm))
                ordersObj['carrier'] = tree.findtext('{%s}ShipmentMethod'.replace("%s", self.nm))

                ordersObj['shipping_and_handling'] = tree.findtext('{%s}OrderTotalAmount'.replace("%s", self.nm))
                ordersObj['shipping_total'] = tree.findtext('{%s}SubTotal'.replace("%s", self.nm))
                ordersObj['shipping_tax'] = tree.findtext('{%s}TaxAmount'.replace("%s", self.nm))

                ordersObj['additional_fields'] = {}
                ordersObj['additional_fields']['freight'] = tree.findtext('{%s}Freight'.replace("%s", self.nm))

                ordersObj['lines'] = []
#                global_signature = '';
                for line in tree.findall('{%s}OrderItems/{%s}OrderItem'.replace("%s", self.nm)):
                    lineObj = {}
                    lineObj['line_index'] = len(ordersObj['lines'])
                    lineObj['id'] = line.findtext('{%s}OrderItemId'.replace("%s", self.nm))
                    lineObj['qty'] = line.findtext('{%s}Quantity'.replace("%s", self.nm))
                    lineObj['sku'] = line.findtext('{%s}ItemNumber'.replace("%s", self.nm))
                    lineObj['vendorSku'] = line.findtext('{%s}ItemNumber'.replace("%s", self.nm))
                    lineObj['name'] = line.findtext('{%s}ItemDescription'.replace("%s", self.nm))
                    lineObj['vendor_cost'] = line.findtext('{%s}UnitPrice'.replace("%s", self.nm))
                    lineObj['vendor_extend_price'] = line.findtext('{%s}ExtendedPrice'.replace("%s", self.nm))
                    lineObj['tax'] = line.findtext('{%s}TaxAmount'.replace("%s", self.nm))
#                   lineObj['signature'] = line.findtext('{%s}SignatureRequired'.replace("%s", self.nm))
#                   if(global_signature != 'true'):
#                       global_signature = lineObj['signature']

                    ordersObj['lines'].append(lineObj)

#                ordersObj['additional_fields']['signature'] = global_signature

        return ordersObj


class GovXApiSetOrderStatus(GovXApi):
    name = "SetOrderStatus"
    method = "POST"

    def __init__(self, settings):
        super(GovXApiSetOrderStatus, self).__init__(settings)

    def parse_response(self, response):
        success = False

        if not response:
            return success

        response = clean_namespaces(response)
        status = etree.XML(response).xpath('Body/SetOrderStatusResponse/SetOrderStatusResult/ResultStatus')
        if status and status[0] is not None and (status[0].text).lower() == 'success':
            success = True

        return success


class GovXApiConfirmShipment(GovXApiSetOrderStatus):

    confirmLines = []

    def __init__(self, settings, line):
        super(GovXApiConfirmShipment, self).__init__(settings)
        self.confirmLine = line

    def get_request_data(self):
        line = ""
        if self.confirmLine:

            dt = datetime.strptime(self.confirmLine["shp_date"], "%Y-%m-%d %H:%M:%S")
            shp_date = dt.strftime("%Y-%m-%dT%H:%M:%S.636")
            line = """
                <govx:orderSetStatusRequest>
                    <govx:Username>{username}</govx:Username>
                    <govx:Password>{password}</govx:Password>
                    <govx:OrderId>{order_id}</govx:OrderId>
                    <govx:VendorOrderNumber>{vendor_order_name}</govx:VendorOrderNumber>
                    <govx:Status>Shipped</govx:Status>
                    <govx:ItemNumber>{item}</govx:ItemNumber>
                    <govx:Quantity>{qty}</govx:Quantity>
                    <govx:ShipDate>{shp_date}</govx:ShipDate>
                    <govx:ShipMethod>{carrier_code}</govx:ShipMethod>
                    <govx:Freight>{freight}</govx:Freight>
                    <govx:TrackingNumber>{tracking_number}</govx:TrackingNumber>
                    <govx:IsHashed>false</govx:IsHashed>
               </govx:orderSetStatusRequest>
            """.format(
                    freight=1,#self.confirmLine['additional_fields']['freight'] if 'freight' in self.confirmLine['additional_fields'] else 1,  # need to find a way to receive correct value
                    order_id=self.confirmLine['po_number'],
                    vendor_order_name=self.confirmLine['po_number'],
                    qty=int(self.confirmLine['product_qty']),
                    carrier_code=self.confirmLine['carrier_code'],
                    shp_date=shp_date,
                    tracking_number=self.confirmLine['tracking_number'],
                    item=self.confirmLine['vendorSku'],
                    username=self.settings.get('username', ''),
                    password=self.settings.get('password', '')
                )

        return line


class GovXApiCancelOrders(GovXApiSetOrderStatus):

    confirmLines = []

    def __init__(self, settings, line):
        super(GovXApiCancelOrders, self).__init__(settings)
        self.confirmLine = line

    def get_request_data(self):

        line = ""

        if self.confirmLine:
            line = """
                <govx:orderSetStatusRequest>
                    <govx:Username>{username}</govx:Username>
                    <govx:Password>{password}</govx:Password>
                    <govx:OrderId>{order_id}</govx:OrderId>
                    <govx:VendorOrderNumber>{vendor_order_name}</govx:VendorOrderNumber>
                    <govx:Status>Cancelled</govx:Status>
                    <govx:ItemNumber>{item}</govx:ItemNumber>
                    <govx:Quantity>{qty}</govx:Quantity>
                    <govx:Freight>{freight}</govx:Freight>
                    <govx:IsHashed>false</govx:IsHashed>
               </govx:orderSetStatusRequest>
            """.format(
                    freight=1,  # need to find a way to receive correct value
                    order_id=self.confirmLine['po_number'],
                    vendor_order_name=self.confirmLine['po_number'],
                    qty=int(self.confirmLine['product_qty']),
                    item=self.confirmLine['vendorSku'],
                    username=self.settings.get('username', ''),
                    password=self.settings.get('password', '')
                )
        return line


class GovXApiReturnOrders(GovXApiSetOrderStatus):

    confirmLines = []

    def __init__(self, settings, line):
        super(GovXApiReturnOrders, self).__init__(settings)
        self.confirmLine = line

    def get_request_data(self):

        line = ""
        if self.confirmLine:
            line = """
                <govx:orderSetStatusRequest>
                    <govx:Username>{username}</govx:Username>
                    <govx:Password>{password}</govx:Password>
                    <govx:OrderId>{order_id}</govx:OrderId>
                    <govx:VendorOrderNumber>{vendor_order_name}</govx:VendorOrderNumber>
                    <govx:Status>Returned</govx:Status>
                    <govx:ItemNumber>{item}</govx:ItemNumber>
                    <govx:Quantity>{qty}</govx:Quantity>
                    <govx:Freight>{freight}</govx:Freight>
                    <govx:IsHashed>false</govx:IsHashed>
               </govx:orderSetStatusRequest>
            """.format(
                freight=1,  # need to find a way to receive correct value
                order_id=self.confirmLine['po_number'],
                vendor_order_name=self.confirmLine['po_number'],
                qty=int(self.confirmLine['product_qty']),
                item=self.confirmLine['vendorSku'],
                username=self.settings.get('username', ''),
                password=self.settings.get('password', '')
            )

        return line


class GovXApiUpdateQTY(GovXFtpApi):

    def __init__(self, settings, lines):
        super(GovXApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "INVENTORY.csv"
        self.filename_local = f_d("INVENTORY%Y%m%d%H%M.csv")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if (
                check_none(line.get('customer_sku', False)) and
                check_none(line.get('customer_price', False))
            ):
                # DLMR-1364
                # Duplicate line if it has GWP data
                if (
                    check_none(line.get('customer_price_gwp', False)) and
                    check_none(line.get('customer_sku_gwp', False))
                ):
                    line_gwp = copy.deepcopy(line)
                    line_gwp['customer_sku'] = line_gwp.get('customer_sku_gwp')
                    line_gwp['customer_price'] = line_gwp.get('customer_price_gwp')
                    self.append_to_revision_lines(line_gwp, 'good')
                # END DLMR-1364

                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
                continue
        return {'lines': self.revision_lines['good']}


class GovXOpenerp(ApiOpenerp):

    def __init__(self):
        super(GovXOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):

        additional_fields = []

        line_obj = {
            "notes": "",
            "cost": 0.0,
            "name": line['name'],
            "tax": line['tax'],
            'vendorSku': line['vendorSku'],
            'qty': line['qty'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        }
        product = {}
        field_list = ['vendorSku', 'sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr, uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code', 'customer_sku',
                        'upc', 'customer_id_delmar'
                    )
                )
                # DLMR-1364
                # Try to find by GWP field
                if not product:
                    product, size = self.pool.get('product.product').search_product_by_id(
                        cr, uid,
                        context['customer_id'],
                        line[field],
                        ['customer_sku_gwp']
                    )
                    if product:
                        line_obj['product_gwp'] = True
                # END DLMR-1364
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False

                    line['cost'] = line.get('vendor_cost', 0.0)

                    break

        if product:
            line_obj["product_id"] = product.id
        else:
            line_obj["notes"] += "Can't find product by sku %s.\n" % (line['sku'])
            line_obj["product_id"] = 0

        if line.get('vendor_cost'):
            additional_fields.append((0, 0, {
                'name': 'vendor_cost',
                'label': 'Vendor price Unit',
                'value': line.get('vendor_cost', '')
                }))

        if line.get('vendor_extend_price'):
            additional_fields.append((0, 0, {
                'name': 'vendor_extend_price',
                'label': 'Vendor priceExtendedPrice Unit',
                'value': line.get('vendor_extend_price', '')
                }))

#        if line.get('signature'):
#            additional_fields.append((0, 0, {
#                'name': 'signature',
#                'label': 'Signature',
#                'value': line.get('signature', '')
#                }))

        line_obj.update({'additional_fields': additional_fields})

        return line_obj


class GovXInvoiceMessageManager(InvoiceMessageManager):

    def __init__(self, _settings=None):
        from configparser import ConfigParser
        from os.path import dirname

        if _settings is None:
            _settings = {}

        _settings['file_ext'] = 'csv'

        super(GovXInvoiceMessageManager, self).__init__(_settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/govx_settings.ini' % (dirname(__file__)))
        self.config_name = 'InvoiceMail'
        self.delimiter = ','

    def _get_subject(self):

        if not self.subject:
            import pooler
            pool = pooler.get_pool(self.cr.dbname)

            self._return = bool(
                len(self.delivery_ids) == 1 and pool.get('stock.picking').search(self.cr, self.uid, [('id', 'in', self.delivery_ids), ('state', '=', 'returned')])
            )

            self.subject = "{customer} {type} Report {ts}".format(
                customer=self.customer_name,
                type="Returns" if self._return else "Invoice",
                ts=self.shp_date.strftime("%Y-%m-%d %H:%M:%S")
                )

        return self.subject

    def __get_orders_info(self):
        self.cr.execute("""
            select
                so.po_number as po_number,
                sp.tracking_ref as tracking,
                'DEL01' as vendor_code,
                sp.shp_date as shp_date,
                case when sm.is_set_component is true then (ol.price_unit * ol.product_uom_qty::integer) else ol.price_unit end as price,
                sm.product_qty as qty,
                (case when sp.state <> 'returned' and rp.invoice_without_ship_cost is false
                    then 0 else 1 end) as returned,
                sp.shp_handling as shipping,
                COALESCE(sp.ret_invoice_no, sp.invoice_no) as invoice_number,
                '0' as sales_tax,
                ol.id as sale_order_id,
                pmcf.value as customer_id_delmar
            from stock_picking sp
                left join sale_order so on sp.sale_id = so.id
                left join stock_move sm on sp.id = sm.picking_id
                left join sale_order_line ol on sm.sale_line_id = ol.id
                left join res_partner rp on rp.id = so.partner_id
                left join product_multi_customer_fields_name pmcfn on pmcfn.customer_id=rp.id and label='Customer ID Delmar'
                left join product_multi_customer_fields pmcf on (pmcf.partner_id=rp.id and pmcf.product_id=sm.product_id and pmcf.field_name_id=pmcfn.id and coalesce(sm.size_id,0)=coalesce(pmcf.size_id,0))
            where sp.id in %(ids)s
            order by po_number
        """, {
            'ids': tuple(self.delivery_ids)
        })

        return self.cr.dictfetchall()

    def _get_file(self):
        csv_file = None

        data = self.__get_orders_info()
        data_to_po = {}
        for item in data:
            if not item.get('po_number') in data_to_po:
                data_to_po[item.get('po_number')] = {}
                data_to_po[item.get('po_number')]['data'] = {
                    'po_number': item.get('po_number'),
                    'tracking': item.get('tracking'),
                    'vendor_code': item.get('vendor_code'),
                    'date': item.get('shp_date'),
                    'invoice_number': item.get('invoice_number'),
                    'po': item.get('po_number'),
                    'merchandise': 0.0,
                    'sales_tax': item.get('sales_tax'),
                    'shipping': item.get('shipping') if item.get('returned') == 0 else 0,
                    'drop_ship_handling': 0.0,
                    'total': item.get('shipping') if item.get('returned') == 0 else 0
                }
            if not item.get('sale_order_id') in data_to_po[item.get('po_number')]:
                data_to_po[item.get('po_number')][item.get('sale_order_id')] = {}
            merchandise = (item.get('qty') * item.get('price')) if item.get('returned') == 0 else 0
            shipping = item.get('shipping') if item.get('returned') == 0 else 0
            data_to_po[item.get('po_number')][item.get('sale_order_id')] = {
                'po_number': " ",
                'tracking': " ",
                'vendor_code': " ",
                'date': item.get('qty'),
                'invoice_number': item.get('customer_id_delmar'),
                'po': item.get('po_number'),
                'merchandise': item.get('price'),
                'sales_tax': " ",
                'shipping': " ",
                'drop_ship_handling': " ",
                'total': merchandise
            }
            data_to_po[item.get('po_number')]['data']['merchandise'] += merchandise
            data_to_po[item.get('po_number')]['data']['total'] += merchandise
#            data_to_po[item.get('po_number')]['data']['shipping'] += shipping

        if data_to_po:
            result = []
            for po in data_to_po:
                result.append(data_to_po[po].pop('data'))
                for invoice in data_to_po[po]:
                    result.append(data_to_po[po][invoice])

            csv_lines = [feed_utils.FeedUtills(self.config.items(self.config_name + 'Header'), {}).create_csv_line(self.delimiter, quote=True)]
            csv_lines += [feed_utils.FeedUtills(self.config.items(self.config_name), line).create_csv_line(self.delimiter) for line in result]
            csv_file = "".join(csv_lines)

        return csv_file
