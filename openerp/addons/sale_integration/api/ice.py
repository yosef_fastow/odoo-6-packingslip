# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from apiopenerp import ApiOpenerp
from datetime import datetime
import logging
from openerp.addons.pf_utils.utils.re_utils import f_d
from openerp.addons.pf_utils.utils.xml_utils import _try_parse
from customer_parsers.ice_input_csv_parser import CSVParser

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


class IceApi(FtpClient):

    def __init__(self, settings):
        super(IceApi, self).__init__(settings)


class IceApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(IceApiClient, self).__init__(
            settings_variables, IceOpenerp, False, DEFAULT_VALUES)
        self.load_orders_api = IceApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = IceApiConfirmOrders(self.settings, lines)
        confirmApi.process('send')
        self.extend_log(confirmApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = IceApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class IceApiGetOrdersXML(IceApi):

    method = "get"
    name = "GetOrdersFromLocalFolder"

    def __init__(self, settings):
        super(IceApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders', self.get_root_dir(settings))
        self.parser = CSVParser(self.customer)


class IceApiUpdateQTY(IceApi):

    filename = ""
    lines = []

    def __init__(self, settings, lines):
        super(IceApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory', self.get_root_dir(settings))
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.filename = f_d("ICEUSINVENTORY%Y%m%d%H%M%f.csv")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if(line.get('sku', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}


class IceApiConfirmOrders(IceApi):

    def __init__(self, settings, lines):
        super(IceApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("ICEUSSHIPPED%Y%m%d%H%M%f.csv")

    def upload_data(self):
        for line in self.lines:
            dt = datetime.now()
            if(line['shp_date']):
                dt = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = dt.strftime("%d/%m/%Y")
            line['int_product_qty'] = int(line['product_qty'])
        return {'lines': self.lines}


class IceOpenerp(ApiOpenerp):

    def __init__(self):

        super(IceOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def fill_line(self, cr, uid, settings, line, context=None):
        if (context is None):
            context = {}
        line_obj = {
            'id': line['id'],
            "name": line['name'],
            'customerCost': line['customerCost'],
            'merchantSKU': line['merchantSKU'],
            'shipCost': line['shipCost'],
            'vendorSku': line['originvendorSku'],
            'ice_sub_total': line['ice_sub_total'],
            'ice_row_total': line['ice_row_total'],
            'ice_tax_amount': line['ice_tax_amount'],
            'ice_discount': line['ice_discount'],
            'qty': line['qty'],
            'price_unit': False,
            'size': line['size'],
            'notes': '',
        }

        product = False

        field_list = ['vendorSku', 'merchantSKU']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr, uid, context['customer_id'], line[field])
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif(line.get('size', False)):
                        _size = _try_parse(line['size'], 'float', False)
                        if(not _size):
                            line_obj['size_id'] = False
                        else:
                            size_ids = self.pool.get('ring.size').search(
                                cr, uid, [('name', '=', str(_size))])
                            if(len(size_ids) > 0):
                                line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost:
                line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])
        else:
            line_obj["notes"] += "Can't find product by sku {0}.\n".format(line['sku'])
            line_obj["product_id"] = 0

        return line_obj
