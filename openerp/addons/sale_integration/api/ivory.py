# -*- coding: utf-8 -*-

import base64
from emailapiclient import EmailApiClient
from abstract_apiclient import AbsApiClient
from datetime import datetime
import logging


_logger = logging.getLogger(__name__)

SETTINGS_FIELDS = (
    ('emails_for_sending_inventory',     'email list',              ''),
    ('emails_for_sending_confirm',       'email list(confirm)',     ''),
)


class IvoryApi(EmailApiClient):

    def __init__(self, settings):
        super(IvoryApi, self).__init__(settings)

    def upload_data(self):
        return {'lines': self.lines}


class IvoryApiClient(AbsApiClient):

    def __init__(self, settings_variables):
        super(IvoryApiClient, self).__init__(
            settings_variables, False, SETTINGS_FIELDS, False)
        self._mail_messages = []

    def updateQTY(self, lines, mode=None):
        updateApi = IvoryApiUpdateQTY(self.settings, lines)
        res = updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        if(res):
            self._mail_messages.append({
                'email_to_list': self.settings['emails_for_sending_inventory'],
                'subject': 'Ivory Inventory Report',
                'attachments': {
                    updateApi.filename: base64.b64encode(res),
                },
            })
            self._mail_messages = self.validate_mail_messages(self._mail_messages)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        res = False
        confirmApi = IvoryApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        if(res):
            self._mail_messages.append({
                'email_to_list': self.settings['emails_for_sending_confirm'],
                'subject': 'Ivory Confirm Shipment Report',
                'attachments': {
                    confirmApi.filename: base64.b64encode(res),
                },
            })
            self._mail_messages = self.validate_mail_messages(self._mail_messages)
        self.extend_log(confirmApi)
        return res


class IvoryApiUpdateQTY(IvoryApi):

    def __init__(self, settings, lines):
        super(IvoryApiUpdateQTY, self).__init__(settings)
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.filename = "{:ivory_inventory-%Y-%m-%d-%H%M%S.csv}".format(datetime.now())
        self._path_to_backup_local_dir = self.create_local_dir(self.backup_local_path, "ivory", "inventory", "now")
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            if(line.get('customer_sku', False)):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}


class IvoryApiConfirmShipment(IvoryApi):

    def __init__(self, settings, lines):
        super(IvoryApiConfirmShipment, self).__init__(settings)
        self.lines = lines
        self.type_tpl = "string"
        self.use_mako_templates("confirm")
        self.filename = "{:ivory_shipment-%Y-%M-%d-%H%M%S.csv}".format(datetime.now())
        self._path_to_backup_local_dir = self.create_local_dir(self.backup_local_path, "ivory", "ship", "now")
