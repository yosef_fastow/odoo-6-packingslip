# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient, InvoiceMessageManager
import netsvc
import base64
from ftpapiclient import FtpClient
from datetime import datetime
import logging
from pf_utils.utils.re_utils import f_d
from utils import feedutils as feed_utils

_logger = logging.getLogger(__name__)

DEFAULT_VALUES = {
    'use_ftp': True
}


class SamuelsApi(FtpClient):

    def __init__(self, settings):
        super(SamuelsApi, self).__init__(settings)


class SamuelsApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(SamuelsApiClient, self).__init__(settings_variables, None, None, DEFAULT_VALUES, SamuelsInvoiceMessageManager)
        _logger.warn('Init samuels Api')

    def updateQTY(self, lines, mode=None):
        updateApi = SamuelsApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        confirmApi = SamuelsApiConfirmOrders(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        return res_confirm

    def processingOrderLines(self, lines, state='accepted'):
        ordersApi = SamuelsApiAcknowledgementOrders(self.settings, lines, state)
        ordersApi.process('send')
        self.extend_log(ordersApi)
        return True


class SamuelsApiUpdateQTY(SamuelsApi):

    def __init__(self, settings, lines):
        super(SamuelsApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "samuels_inventory.csv"
        self.filename_local = "{:samuels_inventory_%Y%m%d%_H%M%S.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            _sku = line.get('sku', False)
            if((not _sku) or (_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class SamuelsApiAcknowledgementOrders(SamuelsApi):

    def __init__(self, settings, lines, state):
        super(SamuelsApiAcknowledgementOrders, self).__init__(settings)
        self.use_ftp_settings('acknowledgement')
        self.type_tpl = "string"
        self.use_mako_templates('acknowledgement')
        self.lines = lines
        self.filename = f_d('ACKNOWLEDGEMENT%Y%m%d-%H%M%S.csv', datetime.utcnow())
        self.state = state

    def upload_data(self):
        lines = self.lines
        return {'lines': lines}


class SamuelsApiConfirmOrders(SamuelsApi):

    def __init__(self, settings, lines):
        super(SamuelsApiConfirmOrders, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        self.use_mako_templates('confirm')
        self.lines = lines
        self.filename = f_d("confirm_{order_name}_%d%m%y_%H%M%S.csv".format(order_name=str(lines[0]['origin'])))

    def upload_data(self):
        lines = self.lines
        for line in lines:
            product_qty = 0
            if(line.get('product_qty', False)):
                try:
                    product_qty = int(line['product_qty'])
                except ValueError:
                    pass
            date_now = datetime.now()
            if(line.get('shp_date', False)):
                date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%m/%d/%Y")
            line['product_qty'] = product_qty
        return {'lines': lines}


class SamuelsInvoiceMessageManager(InvoiceMessageManager):

    def __init__(self, _settings=None):
        from configparser import ConfigParser
        from os.path import dirname

        if _settings is None:
            _settings = {}

        _settings['file_ext'] = 'csv'

        super(SamuelsInvoiceMessageManager, self).__init__(_settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/samuels_settings.ini' % (dirname(__file__)))
        self.config_name = 'InvoiceMail'
        self.delimiter = ','

    def _get_subject(self):
        _logger.warn("Samuels Api get subject")

        if not self.subject:
            import pooler
            pool = pooler.get_pool(self.cr.dbname)

            self._return = bool(
                len(self.delivery_ids) == 1 and pool.get('stock.picking').search(self.cr, self.uid, [('id', 'in', self.delivery_ids), ('state', '=', 'returned')])
            )

            self.subject = "{customer} {type} Report {ts}".format(
                customer=self.customer_name,
                type="Returns" if self._return else "Invoice",
                ts=self.shp_date.strftime("%Y-%m-%d %H:%M:%S")
                )

        return self.subject

    def __get_orders_info(self):
        self.cr.execute("""
            select
                so.po_number as po_number,
                sp.tracking_ref as tracking,
                'DEL01' as vendor_code,
                sp.shp_date as shp_date,
                case when sm.is_set_component is true then (ol.price_unit * ol.product_uom_qty::integer) else ol.price_unit end as price,
                sm.product_qty as qty,
                (case when sp.state <> 'returned' and rp.invoice_without_ship_cost is false
                    then 0 else 1 end) as returned,
                sp.shp_handling as shipping,
                COALESCE(sp.ret_invoice_no, sp.invoice_no) as invoice_number,
                '0' as sales_tax,
                ol.id as sale_order_id,
                pmcf.value as customer_id_delmar,
                coalesce(ol.size_id, 0) as size,
                sp.carrier as carrier
            from stock_picking sp
                left join sale_order so on sp.sale_id = so.id
                left join stock_move sm on sp.id = sm.picking_id
                left join sale_order_line ol on sm.sale_line_id = ol.id
                left join res_partner rp on rp.id = so.partner_id
                left join product_multi_customer_fields_name pmcfn on pmcfn.customer_id=rp.id and label='Customer ID Delmar'
                left join product_multi_customer_fields pmcf on (pmcf.partner_id=rp.id and pmcf.product_id=sm.product_id and pmcf.field_name_id=pmcfn.id and coalesce(sm.size_id,0)=coalesce(pmcf.size_id,0))
            where sp.id in %(ids)s
            order by po_number
        """, {
            'ids': tuple(self.delivery_ids)
        })

        return self.cr.dictfetchall()

    def _get_attachments(self):
        _logger.warn('Samuels Api get attachments')

        _attachments = {}
        # add csv-file
        self.__file_name = self._get_file_name()
        self.__file = base64.b64encode(self._get_file())
        _attachments[self.__file_name] = self.__file

        _attachments[self._get_pdf_file_name()] = base64.b64encode(self._get_pdf_file())
        _logger.warn("Samuels Api attachments list: %s" % _attachments)

        return _attachments

    def _get_pdf_file_name(self):

        if not self.file_name_template:
            self.file_name_template = '{:Invoice%Y-%m-%d %H:%M:%S}'

        file_name = (self.file_name_template + '.pdf').format(self.shp_date)

        return file_name

    def _get_pdf_file(self):
        service = netsvc.LocalService(self.report_service)
        (pdf_file, format) = service.create(self.cr, self.uid, self.delivery_ids, data={})

        return pdf_file

    def _get_file(self):
        csv_file = None
        data = self.__get_orders_info()
        invoice_lines = []
        for item in data:
            shipping = item.get('shipping') if item.get('returned') == 0 and item.get('shipping') is not None else 0
            line_data = {
                'invoice_number': item.get('invoice_number'),
                'doctype': 'Invoice',
                'date': item.get('shp_date'),
                'po_number': item.get('po_number'),
                'style': item.get('customer_id_delmar'),
                'qty': item.get('qty'),
                'unit_price': item.get('price'),
                'total_price': (item.get('qty') * item.get('price')) if item.get('returned') == 0 else 0,
                'size': item.get('size'),
                'shipping': shipping,
                'carrier': item.get('carrier'),
                'tracking': item.get('tracking'),
            }
            invoice_lines.append(line_data)

        _logger.warn("Invoice lines: %s" % invoice_lines)

        if invoice_lines:
            csv_lines = [feed_utils.FeedUtills(self.config.items(self.config_name + 'Header'), {}).create_csv_line(self.delimiter, quote=True)]
            csv_lines += [feed_utils.FeedUtills(self.config.items(self.config_name), line).create_csv_line(self.delimiter) for line in invoice_lines]
            csv_file = "".join(csv_lines)

        return csv_file
