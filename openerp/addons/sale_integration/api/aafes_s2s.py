# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from edi_objects import BaseEDIClient
from abstract_apiclient import AbsApiClient
from customer_parsers.aafes_s2s_input_edi_parser import AAFESS2SEDIParser
from customer_parsers.base_edi import address_type_map
from datetime import datetime
import logging
from apiopenerp import ApiOpenerp
from utils.ediparser import check_none
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
import random
from json import dumps as json_dumps
from json import loads as json_loads
from math import ceil


_logger = logging.getLogger(__name__)


SETTINGS_FIELDS = (
    ('vendor_number',               'Vendor number',            '057932'),
    ('vendor_name',                 'Vendor name',              'AAFESS2S'),
    ('sender_id',                   'Sender Id',                'DELMAR'),
    ('sender_id_qualifier',         'Sender Id Qualifier',      'ZZ'),
    ('receiver_id',                 'Receiver Id',              '001695568GT'),
    ('receiver_id_qualifier',       'Receiver Id Qualifier',    '14'),
    ('contact_name',                'Contact name',             'Erel Shlisenberg'),
    ('contact_phone',               'Contact phone',            '(514) 875-4800'),
    ('edi_x12_version',             'EDI X12 Version',          '5010'),
    ('filename_format',             'Filename Format',          'DEL_{edi_type}_{date}.x12'),
    ('line_terminator',             'Line Terminator',          r'~'),
    ('repetition_separator',        'Repetition Separator',     '>'),
    ('environment_mode',            'Environment Mode',         'P'),
    ('standard_identifier',         'Standard Identifier',      '^'),
)

DEFAULT_VALUES = {
    'use_ftp': True,
    'send_functional_acknowledgement': True,
}

CUSTOMER_PARAMS = {
    'timezone': 'EST',
}


class AAFESS2SApi(FtpClient, BaseEDIClient):

    def __init__(self, settings):
        FtpClient.__init__(self, settings)
        BaseEDIClient.__init__(self, settings, None)


class AAFESS2SApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(AAFESS2SApiClient, self).__init__(
            settings_variables, AAFESS2SOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = AAFESS2SApiGetOrders

    def loadOrders(self, save_flag=False, order_type='order'):
        """
        @param save_flag: save to local folder or read local folder
        @param order_type: order or cancel
        """
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmLoad(self, orders):
        api = AAFESS2SApiFunctionalAcknoledgment(self.settings, orders)
        api.process('send')
        self.extend_log(api)
        return True

    def confirmShipment(self, lines):
        manual_order = lines[0]['manual_order']
        if manual_order:
            return True

        confirmApi = AAFESS2SApiConfirmShipment(self.settings, lines)
        res = confirmApi.process('send')
        self.extend_log(confirmApi)

        if self.is_invoice_required:
            invoiceApi = AAFESS2SApiInvoiceOrders(self.settings, lines)
            invoiceApi.process('send')
            self.extend_log(invoiceApi)

        return res

    def updateQTY(self, lines, mode=None):
        updateApi = AAFESS2SApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class AAFESS2SApiGetOrders(AAFESS2SApi):
    """EDI/V5010 X12/850: AAFESS2S Purchase Orders"""

    def __init__(self, settings):
        super(AAFESS2SApiGetOrders, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = AAFESS2SEDIParser(self)
        self.edi_type = '850'


class AAFESS2SApiFunctionalAcknoledgment(AAFESS2SApi):

    orders = []

    def __init__(self, settings, orders):
        super(AAFESS2SApiFunctionalAcknoledgment, self).__init__(settings)
        self.use_ftp_settings('confirm_load')
        self.orders = orders
        self.edi_type = '997'

    def upload_data(self):

        numbers = []
        data = []
        yaml_obj = YamlObject()
        for order_yaml in self.orders:
            if order_yaml['name'].find('TEXTMESSAGE') == -1:
                order = yaml_obj.deserialize(_data=order_yaml['xml'])
            else:
                order = order_yaml
            if order['ack_control_number'] not in numbers:
                numbers.append(order['ack_control_number'])
                segments = []
                st = 'ST*[1]997*[2]%(st_number)s'
                st_number = str(random.randrange(10000, 99999))
                st_data = {
                    'st_number': st_number
                }
                segments.append(self.insertToStr(st, st_data))
                ak = 'AK1*[1]%(group)s*[2]%(ack_control_number)s'

                segments.append(self.insertToStr(ak, {
                    'group': order['functional_group'],
                    'ack_control_number': order['ack_control_number']
                }))

                ak9 = 'AK9*[1]A*[2]%(number_of_transaction)s*[3]%(number_of_transaction)s*[4]%(number_of_transaction)s'
                segments.append(self.insertToStr(ak9, {
                    'number_of_transaction': order['number_of_transaction']
                    # FIXME: Why I not see assignment / parsing of number_of_transaction
                }))
                se = 'SE*%(segment_count)s*%(st_number)s'
                segments.append(self.insertToStr(se, {
                    'segment_count': len(segments) + 1,
                    'st_number': st_number
                }))
                data.append(self.wrap(segments, {'group': 'FA'}))
        return data


class AAFESS2SApiConfirmShipment(AAFESS2SApi):
    """EDI/V5010 X12/856: 856 Ship Notice/Manifest"""

    def __init__(self, settings, lines):
        super(AAFESS2SApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.confirmLines = lines
        self.edi_type = '856'
        self._default_weight = 1

    def upload_data(self):
        segments = []
        st = 'ST*[1]856*[2]%(st_number)s'
        st_number = '0001'
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bsn = 'BSN*[1]%(purpose_code)s*[2]%(shipment_identification)s*[3]%(shp_date)s*[4]%(shp_time)s*[5]0001'
        shp_date = self.confirmLines[0]['shp_date']
        if shp_date:
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()
        shp_time = shp_date.strftime('%H%M')

        bsn_data = {
            # 00 - Original
            # 01 - Cancellation
            # 17 - Cancel, to be Reissued
            'purpose_code': '00',
            'shipment_identification': self.confirmLines[0]['tracking_number'],
            'shp_date': shp_date.strftime('%Y%m%d'),
            'shp_time': shp_time
        }
        segments.append(self.insertToStr(bsn, bsn_data))

        # now_dtm = datetime.utcnow()
        # now_date = now_dtm.strftime('%Y%m%d')
        # now_time = now_dtm.strftime('%H%M')
        # dtm = 'DTM*[1]%(code)s*[2]%(date)s*[3]%(time)s*[4]%(time_code)s'
        # segments.append(self.insertToStr(dtm, {
        #     'code': '205',  # Transmitted
        #     'date': now_date,  # Date the feed is generated
        #     'time': now_time,  # Time the feed is generated
        #     # Code identifying the time.
        #     # In accordance with International Standards Organization standard 8601,
        #     # time can be specified by a + or - and an indication in hours
        #     # in relation to Universal Time Coordinate (UTC) time; since + is a restricted character,
        #     # + and - are substituted by P and M in the codes that follow
        #     'time_code': 'GM',  # Greenwich Mean Time
        # }))

        hl_number = 1
        hl_number_prev = ''

        hl = 'HL*[1]%(hl_number)s*[2]%(hl_number_prev)s*[3]%(code)s*[4]%(hierarchical_child_code)s'

        hl_data = {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'S',  # Shipment,
            'hierarchical_child_code': '1',

        }
        segments.append(self.insertToStr(hl, hl_data))

        td1 = 'TD1*[1]%(code)s*[2]%(qty)s*[3]*[4]*[5]*[6]%(weight_qualifier)s*[7]%(weight)s*[8]%(weight_uom)s'
        weight = self.confirmLines[0]['weight']
        try:
            weight = float(weight)
            if not weight:
                weight = self._default_weight
        except:
            weight = self._default_weight

        # 01 - Actual Pounds
        # 50 - Actual Kilograms
        # LB  -Pound
        segments.append(self.insertToStr(td1, {
            'code': 'BOX',
            'qty': sum([int(x['product_qty']) for x in self.confirmLines if int(x['product_qty'])]),
            'weight': int(ceil(weight * 0.0022)),  # Convert to LB, but "Stacy H.: Is there a way that you can round to 1 lbs"
            'weight_uom': 'LB',  # Stacy Comment: There is nothing wrong with sending 50 as the UOM.  We do not see that often….. However, we prefer LB.
            'weight_qualifier': 'G',  # Stacy Comment:  This is something that you should know being that you are shipping the items to us.  Normally, TD106 = G – gross weight, then TD108 should be LB.
        }))

        td5 = 'TD5*[1]*[2]2*[3]SCAC*[4]%(carrier_code)s*[5]%(carrier)s'
        td5_data = {
            'carrier': self.confirmLines[0]['carrier'],
            'carrier_code': self.confirmLines[0]['carrier_code'],
        }
        segments.append(self.insertToStr(td5, td5_data))

        ref = 'REF*[1]%(code)s*[2]%(data)s'

        # REF*2I (2I) qualifier is only used for catalog drop shipment orders
        # segments.append(self.insertToStr(ref, {
        #     'code': '2I',
        #     'data': self.confirmLines[0]['tracking_number']
        # }))

        segments.append(self.insertToStr(ref, {
            'code': 'CN',
            'data': self.confirmLines[0]['tracking_number'],
        }))

        segments.append(self.insertToStr(ref, {
            'code': 'BM',
            'data': self.confirmLines[0]['tracking_number'],
        }))

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            # 017 - Estimated Delivery
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        fob = 'FOB*[1]%(shipment_method_of_payment)s'
        segments.append(self.insertToStr(fob, {
            # BP - Paid by Buyer
            # The buyer agrees to the transportation payment term
            # requiring the buyer to pay transportation charges to a
            # specified location (origin or destination location)
            # CC - Collect
            # DE - Per Contract
            # Destination with exceptions as agreed between buyer and
            # seller
            # PB - Customer Pickup/Backhaul
            # PC - Prepaid but Charged to Customer
            # PP - Prepaid (by Seller)
            # PS - Paid by Seller
            # The seller agrees to the transportation payment term
            # requiring the seller to pay transportation charges to a
            # specified location (origin or destination location)
            'shipment_method_of_payment': 'PP',
        }))

        # loop N1
        n1 = 'N1*[1]%(code)s*[2]%(data)s*[3]%(identification_code_qualifier)s*[4]%(identification_code)s'
        n3 = 'N3*[1]%(address1)s'
        n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(zip)s*[1]%(country)s'

        # N1 SF
        duns = self.confirmLines[0]['order_additional_fields'].get('DUNS') or ''
        segments.append(self.insertToStr(n1, {
            'code': 'SF',  # Ship From
            'data': self.confirmLines[0]['return_name'],
            'identification_code_qualifier': '1',
            'identification_code': duns[:9],
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['return_address_line_1']
        }))

        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['return_city'],
            'state': self.confirmLines[0]['return_state'],
            'zip': self.confirmLines[0]['return_postal_code'],
            'country': self.confirmLines[0]['return_country']
        }))

        # N1 ST
        segments.append(self.insertToStr(n1, {
            'code': 'ST',  # Ship To
            'data':  self.confirmLines[0]['ship_to_name_1'],
            'identification_code_qualifier': '92',
            'identification_code': self.confirmLines[0]['order_additional_fields']['facility_number'],
        }))

        segments.append(self.insertToStr(n3, {
            'address1': self.confirmLines[0]['ship_to_address_line_1']
        }))

        country_code = self.confirmLines[0].get('order_additional_fields', {}).get('ship_to_country')
        if not country_code or country_code.lower() in ['null', 'none', 'false']:
            country_code = self.confirmLines[0]['ship_to_country']
        segments.append(self.insertToStr(n4, {
            'city': self.confirmLines[0]['ship_to_city'],
            'state': self.confirmLines[0]['ship_to_state'],
            'zip': self.confirmLines[0]['ship_to_postal_code'],
            'country': country_code
        }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'O',  # Order
            'hierarchical_child_code': '',
        }))

        prf = 'PRF*[1]%(po_number)s*[2]*[3]*[4]%(date_expressed)s'
        segments.append(self.insertToStr(prf, {
            'po_number': self.confirmLines[0]['po_number'],
            'date_expressed': shp_date.strftime('%Y%m%d'),
        }))

        original_addresses = self.confirmLines[0]['order_additional_fields'].get('addresses', {})
        if isinstance(original_addresses, (str, unicode)):
            original_addresses = json_loads(original_addresses)
        for address_type in ['Z7']:
            address = original_addresses.get(address_type_map.get(address_type, ''), {})
            if address:
                segments.append(self.insertToStr(n1, {
                    'code': address_type,
                    'data': check_none(address['name']),
                    'identification_code_qualifier': '92',
                    'identification_code': self.confirmLines[0]['order_additional_fields']['facility_number'],
                }))
            else:
                segments.append(self.insertToStr(n1, {
                    'code': 'BY',
                    'data':  self.confirmLines[0]['ship_to_name_1'],
                    'identification_code_qualifier': '92',
                    'identification_code': self.confirmLines[0]['order_additional_fields']['facility_number'],
                }))

        # Stacy Comment: Please remove TARE
        # hl_number_prev = hl_number
        # hl_number += 1
        # segments.append(self.insertToStr(hl, {
        #     'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
        #     'hl_number_prev': str(hl_number_prev),
        #     'code': 'T',  # Shipping Tare
        #     'hierarchical_child_code': '',
        # }))

        hl_number_prev = hl_number
        hl_number += 1
        segments.append(self.insertToStr(hl, {
            'hl_number': str(hl_number),  # A unique number assigned by the sender to identify a particular data segment in a hierarchical structure
            'hl_number_prev': str(hl_number_prev),
            'code': 'P',  # Pack
            'hierarchical_child_code': '',
        }))

        man = 'MAN*[1]%(code)s*[2]%(number)s'
        segments.append(self.insertToStr(man, {
            'code': 'GM',  # EAN.UCC Serial Shipping Container Code (SSCC) and Application Identifier
            'number': self.confirmLines[0]['sscc_id']
        }))

        lin = 'LIN*[1]*[2]%(item_name_identificator)s*[3]%(purchase_item_code)s'
        sn1 = 'SN1*[1]*[2]%(qty)s*[3]EA'
        po4 = 'PO4*[1]%(qty)s'
        hl_number_prev = hl_number
        for line in self.confirmLines:
            hl_number += 1
            segments.append(self.insertToStr(hl, {
                'hl_number': str(hl_number),
                'hl_number_prev': str(hl_number_prev),
                'code': 'I',  # Item
                'hierarchical_child_code': '',
            }))

            item_name_identificator = 'UP'
            purchase_item_code = line['additional_fields'].get(item_name_identificator, '')
            segments.append(self.insertToStr(lin, {
                'item_name_identificator': item_name_identificator,
                'purchase_item_code': purchase_item_code,
            }))

            qty = line['product_qty']
            try:
                qty = int(qty)
            except ValueError:
                qty = 1

            segments.append(self.insertToStr(sn1, {
                'qty': qty,
            }))

            segments.append(self.insertToStr(po4, {
                'qty': qty,
            }))

        ctt = 'CTT*[1]%(count_hl)s'
        segments.append(self.insertToStr(ctt, {
            'count_hl': hl_number
        }))

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'SH'})


class AAFESS2SApiInvoiceOrders(AAFESS2SApi):
    """EDI/V5010 X12/810: 810 Invoice"""

    def __init__(self, settings, lines):
        super(AAFESS2SApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.invoiceLines = lines
        self.edi_type = '810'

    def upload_data(self):
        segments = []

        st = 'ST*[1]810*[2]%(st_number)s'
        st_number = '0001'  # the first transaction set control number will be 0001 and incremented by one for each additional transaction set within the group
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        big = 'BIG*[1]%(date)s*[2]%(invoice_number)s*[3]*[4]%(po_number)s'
        invoice_date = datetime.utcnow()
        external_date_order_str = self.invoiceLines[0]['external_date_order'] or ''
        external_date_order = None
        if (external_date_order_str):
            for pattern in ["%m/%d/%Y", "%Y-%m-%d"]:
                try:
                    external_date_order = datetime.strptime(external_date_order_str, pattern)
                except Exception:
                    pass

                if external_date_order:
                    break
        segments.append(self.insertToStr(big, {
            'date': invoice_date.strftime('%Y%m%d'),
            'invoice_number': self.invoiceLines[0]['invoice'],
            'po_number': self.invoiceLines[0]['po_number'],  # Must be 14 characters (First characters are PO with the rest being a number and leading zeros)
        }))

        cur = 'CUR*[1]%(code)s*[2]%(currency_code)s'
        segments.append(self.insertToStr(cur, {
            'code': 'SE',
            'currency_code': check_none(self.invoiceLines[0]['order_additional_fields'].get('currency')) or 'USD',
        }))

        ref = 'REF*[1]%(code)s*[2]%(order_number)s'
        ref_codes = [
            'IA',  # Internal Vendor Number
            'BM',  # Bill of Lading Number
        ]

        for ref_code in ref_codes:
            ref_value = self.invoiceLines[0]['order_additional_fields'].get(ref_code, False)
            if ref_value:
                segments.append(self.insertToStr(ref, {
                    'code': ref_code,
                    'order_number': ref_value,
                }))

        original_addresses = self.invoiceLines[0]['order_additional_fields'].get('addresses', {})
        if isinstance(original_addresses, (str, unicode)):
            original_addresses = json_loads(original_addresses)
        for address_type in ['ST']:
            address = original_addresses.get(address_type_map.get(address_type, ''), {})
            if address:
                if address_type in ['ST', 'BY']:
                    n1 = 'N1*[1]%(code)s*[2]%(name)s*[3]%(identification_code_qualifier)s*[4]%(facility_number)s'
                    segments.append(self.insertToStr(n1, {
                        'code': address_type,
                        'name': check_none(address['name']),
                        'identification_code_qualifier': '92',
                        'facility_number': check_none(self.invoiceLines[0]['order_additional_fields'].get('facility_number', '')),
                    }))

                    # n3 = 'N3*[1]%(address1)s*[2]%(address2)s'
                    # segments.append(self.insertToStr(n3, {
                    #     'address1': check_none(address['address1']),
                    #     'address2': check_none(address['address2']),
                    # }))

                    country_code = self.invoiceLines[0].get('order_additional_fields', {}).get('ship_to_country')
                    if not country_code or country_code.lower() in ['null', 'none', 'false']:
                        country_code = self.invoiceLines[0]['ship_to_country']

                    n4 = 'N4*[1]%(city)s*[2]%(state)s*[3]%(postal_code)s*[4]%(country)s'
                    segments.append(self.insertToStr(n4, {
                        'city': address['city'],
                        'state': address['state'],
                        'postal_code': address['zip'],
                        'country': country_code,
                    }))

        duns = self.invoiceLines[0]['order_additional_fields'].get('DUNS') or ''
        for address_type in ['RI']:
            n1 = 'N1*[1]%(code)s*[2]*[3]%(identification_code_qualifier)s*[4]%(duns_number)s'
            segments.append(self.insertToStr(n1, {
                'code': 'RI',
                'identification_code_qualifier': '1',
                'duns_number': duns[:9],
            }))

        terms_of_sale = json_loads(self.invoiceLines[0]['order_additional_fields'].get('terms_of_sale', '{}'))
        itd = 'ITD*[1]%(terms_type_code)s*[2]*[3]%(terms_discount_percent)s*[4]%(terms_discount_due_date)s*[5]%(terms_discount_days_due)s*[6]%(terms_net_due_date)s*[7]%(terms_net_days)s*[8]*[9]*[10]*[11]*[12]%(description)s'
        segments.append(self.insertToStr(itd, {
            'terms_type_code': check_none(str(terms_of_sale.get('terms_type_code', {}).get('original', ''))),
            'terms_discount_percent': check_none(str(terms_of_sale.get('terms_discount_percent'))),
            'terms_discount_due_date': check_none(str(terms_of_sale.get('terms_discount_due_date'))),
            'terms_discount_days_due': check_none(str(terms_of_sale.get('terms_discount_days_due'))),
            'terms_net_due_date': check_none(str(terms_of_sale.get('terms_net_due_date'))),
            'terms_net_days': check_none(str(terms_of_sale.get('terms_net_days'))),
            'description': check_none(str(terms_of_sale.get('description'))),
        }))

        shp_date = self.invoiceLines[0]['shp_date']
        if (shp_date):
            shp_date = datetime.strptime(shp_date, "%Y-%m-%d %H:%M:%S")
        else:
            shp_date = datetime.utcnow()

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'
        segments.append(self.insertToStr(dtm, {
            # 011 - Shipped
            'code': '011',
            'date': shp_date.strftime('%Y%m%d'),
        }))

        it1 = 'IT1*[1]*[2]%(qty)s*[3]EA*[4]%(unit_price)s*[5]*[6]%(upc_qualifier)s*[7]%(upc)s'
        ctp = 'CTP*[1]*[2]%(code)s*[3]%(value)s'
        pid = 'PID*[1]%(description_type)s*[2]*[3]*[4]*[5]%(data)s'
        po4 = 'PO4*[1]%(qty)s'
        line_number = 0
        total_qty_invoiced = 0
        for line in self.invoiceLines:
            line_number += 1
            try:
                qty = int(line['product_qty'])
            except ValueError:
                qty = 1

            upc = line['additional_fields'].get('UP', '') or ''
            upc_qualifier = upc and 'UP' or ''

            segments.append(self.insertToStr(it1, {
                'qty': str(qty),
                'unit_price': line['price_unit'],
                'upc_qualifier': upc_qualifier,
                'upc': upc,
            }))

            RPP = line['additional_fields'].get('RPP', '') or ''
            if RPP:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RPP)),
                }))
            RTL = line['additional_fields'].get('RTL', '') or ''
            if RTL:
                segments.append(self.insertToStr(ctp, {
                    'qty': str(qty),
                    'value': check_none(str(RTL)),
                }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'data': line['name'],
            }))

            segments.append(self.insertToStr(po4, {
                'qty': qty,
            }))

            total_qty_invoiced += qty

        tds = 'TDS*[1]%(total_amount)s'
        segments.append(self.insertToStr(tds, {
            'total_amount': '{0:.2f}'.format(float(self.invoiceLines[0]['amount_total'] or 0.0)).replace('.', ''),  # Total Invoice Amount (including charges, less allowances)
        }))

        ctt = 'CTT*[1]%(items_count)s'
        segments.append(self.insertToStr(ctt, {
            'items_count': str(line_number),  # Total number of line items in the transaction set
        }))

        se = 'SE*[1]%(segment_count)s*[2]%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number,
        }))

        return self.wrap(segments, {'group': 'IN'})


class AAFESS2SApiUpdateQTY(AAFESS2SApi):

    updateLines = []

    def __init__(self, settings, lines):
        super(AAFESS2SApiUpdateQTY, self).__init__(settings)
        self.updateLines = lines
        self.use_ftp_settings('inventory')
        self.edi_type = '846'

    def upload_data(self):
        segments = []
        st = 'ST*[1]846*[2]%(st_number)s'
        st_number = str(random.randrange(10000, 99999))
        st_data = {
            'st_number': st_number
        }
        segments.append(self.insertToStr(st, st_data))

        bia = 'BIA*[1]04*[2]TC*[3]%(reference_identification)s*[4]%(date)s'
        feed_date = datetime.now()

        bia_number = str(random.randrange(100000, 999999))

        segments.append(self.insertToStr(bia, {
            'reference_identification': bia_number,  # unique number identifying this transaction
            'date': feed_date.strftime('%Y%m%d')  # CCYYMMDD
        }))

        lin = 'LIN*[1]%(line_number)s*[2]%(code_UP)s*[3]%(upc)s*[4]%(code_VA)s*[5]%(customer_id_delmar)s'

        pid = 'PID*[1]%(description_type)s*[2]%(characteristic_code)s*[3]*[4]*[5]%(description)s'

        QTY_TYPE = {
            'on_hand': 17,
            'on_order': 63
        }
        qty = 'QTY*[1]%(qty_type)s*[2]%(qty)s*[3]EA'

        dtm = 'DTM*[1]%(code)s*[2]%(date)s'

        i = 0
        for (i, line) in enumerate(self.updateLines):
            if not bool(line['upc']) or not bool(line['customer_id_delmar']):
                self.append_to_revision_lines(line, 'bad')
                continue

            segments.append(self.insertToStr(lin, {
                'line_number': str(line.get('customer_sku', False) and line['customer_sku'][-4:]),
                'code_UP': bool(line['upc']) and 'UP' or '',
                'upc': line['upc'],
                'code_VA': bool(line['customer_id_delmar']) and 'VA' or '',
                'customer_id_delmar': line['customer_id_delmar'],
            }))

            segments.append(self.insertToStr(pid, {
                'description_type': 'F',  # Free-form
                'characteristic_code': '08',  # 08 Product
                'description': line['description'],
            }))

            segments.append(self.insertToStr(qty, {
                'qty_type': QTY_TYPE['on_hand'],
                'qty': str(line['qty']),
            }))

            if not line['qty'] and line.get('eta'):
                try:
                    eta_date = datetime.strptime(line['eta'], '%m/%d/%Y')
                except:
                    eta_date = False
                if eta_date and line['ordered_qty']:

                    segments.append(self.insertToStr(qty, {
                        'qty_type': QTY_TYPE['on_order'],
                        'qty': str(line['ordered_qty']),
                    }))

                    segments.append(self.insertToStr(dtm, {
                        # 139 - Estimates
                        'code': '139',
                        'date': eta_date.strftime('%Y%m%d'),
                    }))

            self.append_to_revision_lines(line, 'good')

        se = 'SE*%(segment_count)s*%(st_number)s'
        segments.append(self.insertToStr(se, {
            'segment_count': len(segments) + 1,
            'st_number': st_number
        }))

        return self.wrap(segments, {'group': 'GE'})

    def parse_response(self, response):
        if not response:
            return False
        return True


class AAFESS2SOpenerp(ApiOpenerp):

    def __init__(self):

        super(AAFESS2SOpenerp, self).__init__()
        self.confirm_fields_map = {
            'customer_sku': 'Customer SKU',
            'upc': 'UPC',
            'customer_id_delmar': 'Customer ID Delmar',
        }

    def write_unique_fields_for_orders(self, cr, uid, picking):
        if picking and picking.sale_id and picking.sale_id.sale_integration_xml_id:
            uffo_obj = self.pool.get('unique.fields.for.orders')
            serial_reference_number_obj = \
                uffo_obj.fetch_one_unused(cr, uid, 'serial_reference_number', picking.id)
            if not serial_reference_number_obj:
                raise ValueError('Not Available any serial_reference_number')
            serial_reference_number_obj.update({
                'sscc_id': uffo_obj.generate_sscc_id(
                    cr, uid,
                    [serial_reference_number_obj['id']],
                    application_identifier='00',
                    extension_digit='0'
                ).get(serial_reference_number_obj['id'], '')
            })
            sale_order_fields_obj = self.pool.get('sale.order.fields')
            srn_row_ids = sale_order_fields_obj.search(
                cr, uid, [
                    '&',
                    ('order_id', '=', picking.sale_id.id),
                    ('name', '=', 'serial_reference_number')
                ]
            )
            if srn_row_ids:
                sale_order_fields_obj.write(
                    cr, uid, srn_row_ids, {
                        'name': 'serial_reference_number',
                        'label': 'serial_reference_number',
                        'value': json_dumps(serial_reference_number_obj)
                    }
                )
            else:
                self.pool.get('sale.order').write(
                    cr, uid, picking.sale_id.id, {
                        'additional_fields': [(0, 0, {
                            'name': 'serial_reference_number',
                            'label': 'serial_reference_number',
                            'value': json_dumps(serial_reference_number_obj)
                        })]
                    }
                )
            uffo_obj.use_for(cr, uid, serial_reference_number_obj['id'], picking.id)
        return True

    def fill_order(self, cr, uid, settings, order, context=None):
        order_obj = {
            'additional_fields':
            [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in order.get('additional_fields', {}).iteritems()
            ],
        }
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        line_obj = line
        line_obj.update({
            'notes': '',
            'sku': line['vendorSKU'],
            'customerCost': line['retail_price'] or line['cost'],
            'additional_fields': [
                (0, 0, {'name': unicode(key), 'label': unicode(key).capitalize(), 'value': value})
                for key, value in line.get('additional_fields', {}).iteritems()
            ]
        })

        product_obj = self.pool.get('product.product')
        size_obj = self.pool.get('ring.size')
        search_by = ('default_code', 'customer_sku', 'upc')
        product = {}
        field_list = ['EN', 'EO', 'IB', 'UA', 'UK', 'UP', 'PI', 'VA', 'SK', 'OT']
        additional_fields = {x[2]['name']: x[2]['value'] for x in line.get('additional_fields', []) or [] if x and x[2]}
        for field_name in field_list:
            field = additional_fields.get(field_name, None)
            if field:
                line_obj['sku'] = field
                product, size = product_obj.search_product_by_id(cr, uid, context['customer_id'], field, search_by)
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_str = str(float(line['size']))
                            size_ids = size_obj.search(cr, uid, [('name', '=', size_str)])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj['product_id'] = product.id
            product_cost = self.pool.get('product.product').get_val_by_label(
                cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj['size_id'])
            if product_cost:
                line['cost'] = product_cost
            else:
                line['cost'] = False
                line_obj["notes"] = "Can't find product cost.\n"
        else:
            line_obj['notes'] = 'Can\'t find product by any sku {}.\n'.format(
                ','.join(
                    [
                        additional_fields.get(x, None)
                        for x in field_list if additional_fields.get(x, None)
                    ]
                )
            )
            line_obj['product_id'] = False

        return line_obj

    def get_additional_confirm_shipment_information(
            self, cr, uid, sale_obj, deliveri_id, lines, context=None
    ):
        line = {
            'serial_reference_number': '',
            'serial_reference_number_id': False,
            'sscc_id': False,
        }
        serial_reference_number_obj = {'id': False, 'value': False}
        order_additional_fields = lines[0]['order_additional_fields']
        if 'serial_reference_number' in order_additional_fields:
            serial_reference_number_obj = json_loads(order_additional_fields['serial_reference_number'])
        else:
            raise ValueError('Not Available any serial_reference_number')
        line.update({
            'serial_reference_number': serial_reference_number_obj['value'],
            'serial_reference_number_id': serial_reference_number_obj['id'],
            'sscc_id': serial_reference_number_obj['sscc_id'],
        })
        return line

    def after_correct_confirm_shipment_callback(self, cr, uid, sale_obj, picking_obj, lines):
        if 'serial_reference_number_id'in lines[0] and lines[0]['serial_reference_number_id']:
            self.pool.get('unique.fields.for.orders').use_for(
                cr, uid, lines[0]['serial_reference_number_id'], picking_obj.id)
        return True

    def get_cancel_accept_information(self, cr, uid, settings, cancel_obj, context=None):
        customer_ids = [str(x.id) for x in settings.customer_ids]
        cr.execute("""SELECT
                        DISTINCT ON (st.id)
                        so.create_date,
                        so.name,
                        so.id as order_id,
                        st.id as picking_id,
                        so.po_number,
                        so.external_customer_order_id,
                        ra.street as address1,
                        ra.street2 as address2,
                        ra.city as city,
                        rs.code as state,
                        ra.zip as zip,
                        rc.code as country
                    FROM
                        sale_order so
                        INNER JOIN stock_picking st ON so.id = st.sale_id
                        LEFT JOIN res_partner_address ra on
                            so.partner_id = ra.partner_id
                            AND ra.type = 'return'
                            AND (ra.default_return = True or ra.warehouse_id = st.warehouses)
                        LEFT JOIN res_country rc on ra.country_id = rc.id
                        LEFT JOIN res_country_state rs on ra.state_id = rs.id
                    WHERE
                        so.po_number = %s AND
                        so.partner_id IN %s
                    ORDER BY
                        st.id,
                        ra.default_return""", (cancel_obj['po_number'], tuple(customer_ids)))

        return cr.dictfetchall()
