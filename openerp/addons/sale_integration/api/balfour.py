# -*- coding: utf-8 -*-
from ftpapiclient import FtpClient
from abstract_apiclient import AbsApiClient
from abstract_apiclient import YamlOrder
from configparser import ConfigParser
from customer_parsers import balfour_csv_parser
from apiopenerp import ApiOpenerp
import logging
import os
from openerp.addons.pf_utils.utils.re_utils import f_d
from datetime import datetime
from utils import feedutils as feed_utils

_logger = logging.getLogger(__name__)


class BalfourApi(FtpClient):

    def __init__(self, settings):
        super(BalfourApi, self).__init__(settings)
        self.config = ConfigParser()
        self.config.read('%s/customers_settings/balfour_settings.ini' % os.path.dirname(__file__))


    def check_response(self, response):
        if (hasattr(response, 'ok')):
            if (not response.ok):
                raise Exception(str(response.stderr))

    def set_decrypt_data(self, data):
        res = ""
        if(self.customer == 'Zales'):
            gpg = self.get_gnupg(self.gnupghome)
            res = gpg.decrypt(data, passphrase=self.passphrase).data
        else:
            res = data
        return res


class BalfourApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):

        super(BalfourApiClient, self).__init__(
            settings_variables, BalfourOpenerp, False, False)
        self.load_orders_api = BalfourApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        orders = []
        if (save_flag):
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def getOrdersObj(self, serialized_order):
        ordersApi = YamlOrder(serialized_order)
        order = ordersApi.getOrderObj()
        return order

    def updateQTY(self, lines, mode=None):
        updateApi = BalfourApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines

    def confirmShipment(self, lines):
        res = []
        confirmApi = BalfourApiConfirmShipment(self.settings, lines)
        res_confirm = confirmApi.process('send')
        self.extend_log(confirmApi)
        res.append('Confirm result:\n{0}\n'.format(str(res_confirm)))

        if self.is_invoice_required:
            invoiceApi = BalfourApiInvoiceOrders(self.settings, lines)
            res_invoice = invoiceApi.process('send')
            self.extend_log(invoiceApi)
            res.append('Invoice result:\n{0}'.format(str(res_invoice)))

        return "\n\n".join(res)


class BalfourApiGetOrdersXML(BalfourApi):

    def __init__(self, settings):
        super(BalfourApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = balfour_csv_parser.BalfourApiGetOrdersXML(self)

    def parse_response(self, response):
        csv_parser = balfour_csv_parser.BalfourApiGetOrdersXML(self)
        ordersList = csv_parser.parse_response(response)
        return ordersList


class BalfourApiUpdateQTY(BalfourApi):
    filename = "inv.txt"
    lines = []
    name = "UpdateQTY"
    cur_ftp_setting = ""
    date_format = "%Y%m%d"

    def __init__(self, settings, lines):
        super(BalfourApiUpdateQTY, self).__init__(settings)
        self.filename_local = f_d("inv(%Y%m%d%H%M%f).csv")
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates('inventory')
        self.lines = lines
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        self.lines = self.prepareUpdateQTYLines(self.lines)

        feed_date = datetime.now().strftime(self.date_format)

        for line in self.lines:

            line['feed_date'] = feed_date

            if (line.get('customer_price', False)):
                line['customer_price'] = "%.2f" % line['customer_price']
            if (
                line.get('qty', False) is not False and
                line.get('customer_price', False) and
                line.get('customer_id_delmar')
            ):
                self.append_to_revision_lines(line, 'good')
            else:
                self.append_to_revision_lines(line, 'bad')
        return {'lines': self.revision_lines['good']}



class BalfourApiConfirmShipment(BalfourApi):
    confirmLines = []

    def __init__(self, settings, lines):
        super(BalfourApiConfirmShipment, self).__init__(settings)
        self.use_ftp_settings('confirm_shipment')
        self.type_tpl = "string"
        # self.use_mako_templates('confirm')
        self.confirmLines = lines
        self.filename = "SHIPPED%s.txt" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        # sale_order.po_number, NULL, sale_order_line.vendorSku, 
        #  stock_picking.shp_date, stock_picking.carrier,
        # stock_picking.carrier_code, stock_picking.tracking_ref (tracking_number)

        # SUM(sm.product_qty)::numeric(32,0), res_partner_address.name,
        data_list = ['PO Number', 'Fulfiller Order Number', 'Fulfiller Sku', 'Quantity',
                'Item Status', 'Ship To Name', 'Ship Date', 'Shipping Carrier', 'Shipping Method', 'Tracking Number']
        data = '\t'.join(data_list) + '\n'
        lines = self.confirmLines
        for line in lines:
            try:
                line['product_qty'] =  int(line['product_qty'])
            except:
                continue
            #line['product_qty'] = 'Empty' # sm.product_qty
            if (line['shp_date']):
                line['shp_date_confirm'] = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S").strftime(
                    "%m/%d/%Y")
            else:
                line['shp_date_confirm'] = datetime.now().strftime("%m/%d/%Y")
            data += feed_utils.FeedUtills(self.config['ShippingConfirmation'].items(), line).create_csv_line('\t', quote=False)
        # return {'lines': lines}
        return data


class BalfourApiInvoiceOrders(BalfourApi):
    def __init__(self, settings, lines):
        super(BalfourApiInvoiceOrders, self).__init__(settings)
        self.use_ftp_settings('invoice_orders')
        self.type_tpl = "string"
        # self.use_mako_templates('invoice')
        self.lines = lines
        self.filename = "Balfour_Invoice_FTP_%s.csv" % datetime.now().strftime('%Y%m%d%H%M%S')

    def upload_data(self):
        #sale_order.po_number, stock_picking.invoice_no = invoice, stock_picking.shp_date
        # sale_order_line.vendorSku, sale_order_line.product_uom_qty = product_qty,
        # sale_order_line.price_unit = unit_cost, stock_picking_carrier ''
        lines = self.lines
        invoice_total_amount = float(lines[0].get('amount_total', 0.0))
        data = "PO#,Invoice#,Invoice Date,Item#,Quantity,UnitCost,TotalFreight,Handling \n"
        for line in self.lines:
            if (line['shp_date']):
                line['shp_date_invoice'] = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S").strftime(
                    "%Y-%m-%d")
            else:
                line['shp_date_invoice'] = datetime.now().strftime("%Y-%m-%d")
            try:
                line['product_uom_qty'] =  int(line['product_uom_qty'])
                line['unit_cost'] = '{:,.2f}'.format(line['unit_cost'])
            except:
                continue

            carrier_num = '0'
            carrier = line['carrier'].lower()
            if 'usps' in carrier:
                carrier_num = '4'
            line['carrier_num'] = carrier_num
            data += feed_utils.FeedUtills(self.config['Invoice'].items(), line).create_csv_line()
        return data




class BalfourOpenerp(ApiOpenerp):

    def __init__(self):
        super(BalfourOpenerp, self).__init__()

    def fill_line(self, cr, uid, settings, line, context=None):
        if context is None:
            context = {}

        line_obj = {
            "notes": "",
            "name": line['name'],
            'cost': line['cost'],
            'customerCost': line['customerCost'],
            "product_id": False,
            "size_id": False,
            'merchantSKU': line['merchantSKU'],
            'vendorSku': line['vendorSku'],
            'additional_fields': [
                (0, 0, {'name': 'store', 'label': 'Store',
                        'value': line.get('store', '')}),
            ]
        }

        product = False
        field_list = ['merchantSKU', 'vendorSku']
        for field in field_list:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(cr, uid, context['customer_id'], line[field])
                if not line_obj.get('size_id', False):
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif(line.get('size', False)):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                        except Exception:
                            size_ids = []
                        if(len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]
                        else:
                            line_obj['size_id'] = False
                            line['size'] = False
                    else:
                        line_obj['size_id'] = False
                if not line_obj.get('product_id', False):
                    if product:
                        line_obj["product_id"] = product.id
                        line_obj['cost'] = line.get('cost', False)
                        if not line_obj['cost'] or line_obj['cost'] == 0:
                            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id,
                                                                                             context['customer_id'],
                                                                                             'Customer Price',
                                                                                             line_obj['size_id'])
                            if product_cost:
                                line['cost'] = product_cost
                                break
                            else:
                                line['cost'] = False
                                line_obj["notes"] = "Can't find product cost.\n"

        return line_obj

    def fill_order(self, cr, uid, settings, order, context=None):

        additional_fields = []

        web_order = order.get('web_order', False)
        store_transaction_code = order.get('order_no')
        if store_transaction_code:
            store_transaction_code = store_transaction_code[:len(store_transaction_code) - 4]

        additional_fields.append((0, 0, {
            'name': 'web_order',
            'label': 'web order',
            'value': web_order,
        }))
        additional_fields.append((0, 0, {
            'name': 'store_transaction_code',
            'label': 'store_transaction_code',
            'value': store_transaction_code,
        }))

        additional_fields.append((0, 0, {
            'name': 'returnname',
            'label': 'returnname',
            'value': order.get('returnname', False),
        }))
        additional_fields.append((0, 0, {
            'name': 'returnadd1',
            'label': 'returnadd1',
            'value': order.get('returnadd1', False),
        }))
        additional_fields.append((0, 0, {
            'name': 'returnadd2',
            'label': 'returnadd2',
            'value': order.get('returnadd2', False),
        }))
        additional_fields.append((0, 0, {
            'name': 'returncity',
            'label': 'returncity',
            'value': order.get('returncity', False),
        }))
        additional_fields.append((0, 0, {
            'name': 'returnstate',
            'label': 'returnstate',
            'value': order.get('returnstate', False),
        }))
        additional_fields.append((0, 0, {
            'name': 'returnzip',
            'label': 'returnzip',
            'value': order.get('returnzip', False),
        }))
        order_obj = {
            'additional_fields': additional_fields
        }
        return order_obj

    def get_additional_shipping_information(self, cr, uid, sale_order_id, ship_data, context=None):
        res = {}

        if context is None:
            context = {}

        sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)
        res = {
            'Ref1': sale_order.po_number or '',
            'Ref2': '922'
        }

        return res

