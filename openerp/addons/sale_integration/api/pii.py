# -*- coding: utf-8 -*-
from abstract_apiclient import AbsApiClient
from ftpapiclient import FtpClient
from apiopenerp import ApiOpenerp
from datetime import datetime
from xlwt import Workbook as xls_workbook
from xlwt import Font as xls_font
from xlwt import XFStyle as xls_style
from cStringIO import StringIO as pseudo_file
import logging
from customer_parsers.pii_input_csv_parser import CSVParser
import base64

_logger = logging.getLogger(__name__)


DEFAULT_VALUES = {
    'use_ftp': True
}


SETTINGS_FIELDS = (
    ('emails_for_sending_invoice',     'email list(invoice)',     ''),
    ('emails_for_sending_confirm',     'email list(confirm)',     ''),
)


class PiiApi(FtpClient):

    def __init__(self, settings):
        super(PiiApi, self).__init__(settings)

    def _write_line_to_ws(self, ws, line, y, style=None):
        for x, cell in enumerate(line):
            if(style):
                ws.write(y, x, cell, style)
            else:
                ws.write(y, x, cell)
        return ws

    def _get_line_data(self, line):
        result = []
        try:
            for name_field in self.body_fields:
                result.append(line.get(name_field, False))
        except Exception:
            result = False
        finally:
            return result

    def _get_raw_template(self, name):
        raw_inventory_tpl = None
        for mako_tpl in self.mako_templates:
            if mako_tpl.action == name:
                raw_inventory_tpl = mako_tpl.tpl
                break
        if(not raw_inventory_tpl):
            raw_inventory_tpl = self.default_template
        return raw_inventory_tpl

    def _create_blank_xls_file(self):
        wb = xls_workbook()
        ws = wb.add_sheet('Sheet1')
        font_header = xls_font()
        font_header.bold = True
        style_header = xls_style()
        style_header.font = font_header
        ws = self._write_line_to_ws(ws, self.header_fields, 0, style_header)
        return wb, ws

    def _prepare_line(self, line):
        return line

    def _create_mail_message(self, type_message, data):
        if(not hasattr(self, '_mail_messages')):
            self._mail_messages = []
        _subject = "PI Incentives Report"
        _attachments = {}
        _email_to = []
        if(type_message == 'confirm'):
            _email_to = self.emails_for_sending_confirm
            _subject = "PI Incentives Confirm Shipment Report"
            _attachments['{:SHIPPED%Y%m%d-%H%m.xls}'.format(datetime.now())] = base64.b64encode(data)
        elif(type_message == 'invoice'):
            _email_to = self.emails_for_sending_invoice
            shp_date = datetime.utcnow()
            _subject = "PI Incentives Invoice Report {:%Y-%m-%d %H:%M:%S}".format(shp_date)
            _attachments['{:PIIInv%Y%m%d-%H%m.pdf}'.format(datetime.now())] = data
        if _email_to:
            mail_message = {
                'email_from': 'erp.delmar@progforce.com',
                'emailto_list': _email_to,
                'subject': _subject,
                'body': '',
                'attachments': _attachments,
            }
            self._mail_messages.append(mail_message)


class PiiApiClient(AbsApiClient):

    use_local_folder = True

    def __init__(self, settings_variables):
        super(PiiApiClient, self).__init__(
            settings_variables, PiiOpenerp, SETTINGS_FIELDS, DEFAULT_VALUES)
        self.load_orders_api = PiiApiGetOrdersXML

    def loadOrders(self, save_flag=False):
        if save_flag:
            orders = self.load_orders_api.process('load')
        else:
            orders = self.load_orders_api.process('read_new')
        self.extend_log(self.load_orders_api)
        return orders

    def confirmShipment(self, lines):
        confirmApi = PiiApiConfirmOrders(self.settings, lines)
        confirmApi.upload_data()
        if(hasattr(confirmApi, '_mail_messages')):
            self._mail_messages = confirmApi._mail_messages
        self.extend_log(confirmApi)
        return True

    def updateQTY(self, lines, mode=None):
        updateApi = PiiApiUpdateQTY(self.settings, lines)
        updateApi.process('send')
        self.check_and_set_filename_inventory(updateApi)
        self.extend_log(updateApi)
        return updateApi.revision_lines


class PiiApiGetOrdersXML(PiiApi):

    def __init__(self, settings):
        super(PiiApiGetOrdersXML, self).__init__(settings)
        self.use_ftp_settings('load_orders')
        self.parser = CSVParser(self.customer, ',', '"')


class PiiApiConfirmOrders(PiiApi):

    def __init__(self, settings, lines):
        super(PiiApiConfirmOrders, self).__init__(settings)
        self.lines = lines
        self.default_template = """["PO_ID", "ShipDate", "MerchantSKU", "Quantity", "TrackingNo", "InvoiceNo", "CarrierName", "ServiceTypeName"]|["po_number", "shp_date", "merchantSKU", "product_qty", "tracking_number", "invoice", "carrier", "shp_service"]"""

    def _prepare_line(self, line):
        result = False
        date_now = datetime.now()
        if line.get('shp_date', False):
            date_now = datetime.strptime(line.get('shp_date'), "%Y-%m-%d %H:%M:%S")
            line['date'] = date_now.strftime("%d/%m/%Y")
        else:
            line['date'] = date_now
        result = line
        return result

    def upload_data(self):
        raw_inventory_tpl = self._get_raw_template("confirm")
        confirm_tpl = raw_inventory_tpl.split("|")
        self.header_fields = eval(confirm_tpl[0])
        self.body_fields = eval(confirm_tpl[1])

        data = ""
        lines = self.lines
        (wb, ws) = self._create_blank_xls_file()
        _y = 1
        for line in lines:
            line = self._prepare_line(line)
            if(not line):
                continue
            _line = self._get_line_data(line)
            if(_line):
                ws = self._write_line_to_ws(ws, _line, _y)
                _y += 1
            else:
                continue
        buf = pseudo_file()
        wb.save(buf)
        data = buf.getvalue()
        buf.close()
        self._create_mail_message("confirm", data)
        return data


class PiiApiUpdateQTY(PiiApi):

    def __init__(self, settings, lines):
        super(PiiApiUpdateQTY, self).__init__(settings)
        self.use_ftp_settings('inventory')
        self.type_tpl = "string"
        self.use_mako_templates("inventory")
        self.lines = lines
        self.filename = "PII_Inv.csv"
        self.filename_local = "{:PIIINVENTORY%Y%m%d%H%M.csv}".format(datetime.now())
        self.revision_lines = {
            'bad': [],
            'good': []
        }

    def upload_data(self):
        lines = self.lines
        for line in lines:
            _customer_sku = line.get('customer_sku', False)
            if((not _customer_sku) or (_customer_sku == "None")):
                self.append_to_revision_lines(line, 'bad')
                continue
            else:
                self.append_to_revision_lines(line, 'good')
        return {'lines': self.revision_lines['good']}


class PiiOpenerp(ApiOpenerp):

    def __init__(self):
        super(PiiOpenerp, self).__init__()

    def fill_order(self, cr, uid, settings, order, context=None):
        additional_fields = []
        if(order.get('account_number', False)):
            additional_fields.append((0, 0, {
                'name': 'account_number',
                'label': 'Account Number',
                'value': order.get('account_number', '')
            }))
        if(order.get('customer_po', False)):
            additional_fields.append((0, 0, {
                'name': 'customer_po',
                'label': 'Customer PO',
                'value': order.get('customer_po', '')
            }))
        order_obj = {'additional_fields': additional_fields}
        return order_obj

    def fill_line(self, cr, uid, settings, line, context=None):
        if(context is None):
            context = {}
        line_obj = {
            "notes": "",
            "name": line['name'],
            'price_unit': '0.0',
            'customerCost': line['price_unit'],
            'customer_sku': line['customer_sku'],
            'merchantSKU': line['customer_sku'],
            'vendorSku': line['sku'],
            'qty': line['qty'],
            'gift_message': line['gift_message'],
            'sku': line['sku'],
            'size': line['size'],
            'id': line['id'],
        }

        if('/' in line['sku']):
            try:
                _sku = line['sku']
                _size = float(_sku[_sku.find("/") + 1:])
                if(_size):
                    line_obj['sku'] = _sku[:_sku.find("/")]
                    line_obj['size'] = _size
            except:
                pass

        if(line_obj.get('sku', False)):
            line['sku'] = line_obj['sku']
        if(line_obj.get('size', False)):
            _size = line_obj['size']
        else:
            _size = line['size']

        product = False

        field_list = ['sku', 'customer_sku']
        for field in field_list:
            if line.get(field, False):
                line_obj['sku'] = line[field]
                product, size = self.pool.get('product.product').search_product_by_id(
                    cr,
                    uid,
                    context['customer_id'],
                    line[field],
                    (
                        'default_code',
                        'customer_sku',
                        'customer_id_delmar'
                    )
                )
                if product:
                    if size and size.id:
                        line_obj['size_id'] = size.id
                    elif line.get('size', False):
                        try:
                            size_ids = self.pool.get('ring.size').search(cr, uid, [
                                ('name', '=', str(float(_size)))])
                        except Exception:
                            size_ids = []
                        if len(size_ids) > 0:
                            line_obj['size_id'] = size_ids[0]
                    else:
                        line_obj['size_id'] = False
                    break

        if product:
            line_obj["product_id"] = product.id
            size_id = line_obj.get('size_id', None)
            if not size_id:
                size_id = None

            customer_ids = settings.customer_ids
            customer_id = None
            if customer_ids:
                customer_id = customer_ids[0].id

            product_cost = self.pool.get('product.product').get_val_by_label(cr, uid, product.id, context['customer_id'], 'Customer Price', line_obj.get('size_id', False))
            if product_cost and customer_id:
                # gets discounted price for price unit
                product_discount = self.pool.get('product.discount').get_result_price(cr, uid, product.id,
                   size_id=size_id, partner_id=customer_id, price=product_cost)
                if product_discount:
                    line_obj['price_unit'] = product_discount
                else:
                    line_obj['price_unit'] = product_cost
            else:
                line_obj['notes'] += "Can't find product cost for name %s" % (line['name'])

        else:
            line_obj["notes"] = "Can't find product by sku %s.\n" % (
                line['sku'])
            line_obj["product_id"] = 0

        return line_obj
