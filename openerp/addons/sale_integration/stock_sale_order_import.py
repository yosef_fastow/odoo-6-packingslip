# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from base64 import decodestring as decode_base64_string
from os import access, R_OK
from os import remove as remove_file
from os.path import join, exists, isfile, getsize, dirname, basename, split
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from openerp.addons.pf_utils.utils.binary_files_from_local_folder import _get_file, _diff_files
from datetime import datetime
from openerp.addons.pf_utils.utils.csv_utils import detect_dialect
from cStringIO import StringIO as IO
from csv import DictReader
from mako.template import Template
from openerp.addons.pf_utils.utils.yamlutils import YamlObject
from openerp.addons.pf_utils.utils.import_orders_normalize import import_orders_normalize
from openerp.addons.pf_utils.utils.import_orders_normalize import data_to_ascii
from openerp.addons.pf_utils.utils.import_orders_normalize import get_starting_chunk
from openerp.addons.pf_utils.utils.import_orders_normalize import is_binary_string
from openerp.addons.pf_utils.utils.xls_to_csv import xls_data_to_csv
import traceback


class OSVException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)
        raise osv.except_osv(_('Warning'), _(message))


class stock_sale_order_import(osv.osv_memory):
    _name = "stock.sale.order.import"

    _columns = {
        'file': fields.binary(string="Input file", ),
        'file_type': fields.char(string="Content Type", size=128, ),
        'file_name': fields.char(string="File Name", size=128, ),
        'size': fields.char(string="File Size", size=128, ),
        'confirm_import_button': fields.boolean(string="Confirm", ),
    }

    def _is_supported_format(self, cr, uid, file_type):
        param_obj = self.pool.get('ir.config_parameter')
        param = param_obj.get_param(cr, uid, 'supported_filetype_GIO')
        supported_import_formats = eval(param) if param else ['text/csv']
        result = file_type in supported_import_formats
        return result

    def _get_file_data(self, import_file, file_name):
        errors = []
        file_data = decode_base64_string(import_file)
        chunk = get_starting_chunk(data=file_data)
        if is_binary_string(chunk):
            try:
                csv_data = xls_data_to_csv(file_data, raise_exceptions=True)
            except Exception:
                errors.append(tuple(["error", unicode(traceback.format_exc())]))
                csv_data = False
            if csv_data:
                file_data = csv_data
        file_data = data_to_ascii(file_data)
        if not file_data:
            error_msg = "Couldn't encode file: {0}".format(str(file_name))
            raise OSVException(error_msg)
        result_normalize = import_orders_normalize(data=file_data)
        if result_normalize['normalize_ok'] and result_normalize['csv_data']:
            file_data = result_normalize['csv_data']
        return file_data, errors

    def _backup_file(self, cr, uid, file_name, file_data):
        api_obj = self.pool.get('sale.integration')
        (settings, api) = api_obj.getApi(cr, uid, "Import Orders")
        today_backup_dir = api.create_local_dir(settings.backup_local_path,
                                                'import_orders', 'orders',
                                                when=True)
        backup_dir = join(settings.backup_local_path, 'import_orders', 'orders')
        today_filenames = blf_utils.get_list_files(today_backup_dir)
        for today_filename in today_filenames:
            if file_name == split(today_filename)[-1]:
                error_msg = "File with the same name has already been imported today!"
                raise OSVException(error_msg)
        filenames = blf_utils.get_list_files(backup_dir)
        for filename in filenames:
            with open(filename) as f:
                backup_file_data = f.read()
            if file_data == backup_file_data:
                error_msg = "File with the same data has already been imported!"
                raise OSVException(error_msg)
        file_writer = blf_utils.file_for_writing(today_backup_dir, file_name,
                                                 file_data, 'w')
        file_writer.create()
        full_filename = join(today_backup_dir, file_name)
        if not exists(full_filename):
            raise OSVException("""File not imported!""")
        return full_filename

    def confirm_import(self, cr, uid, ids, context=None):
        if(context is None):
            context = {}
        ssoih = self.pool.get('stock.sale.order.import.history')
        file_type = str(context.get('file_type', '')).strip()
        if not self._is_supported_format(cr, uid, file_type):
            error_msg = "Unsupported file type: {0}".format(str(file_type))
            raise OSVException(error_msg)
        file_name = str(context.get('file_name', '')).strip()
        file_size = str(context.get('file_size', '')).strip()
        import_file = context.get('file', False)
        if not import_file:
            raise OSVException("Please Select file!")
        file_data, log_messages = self._get_file_data(import_file, file_name)
        full_filename = self._backup_file(cr, uid, file_name, file_data)
        _id = ssoih.create(cr, uid, {
            'full_filename': full_filename,
            'filename': file_name,
            'file_type': file_type,
            'size': file_size,
            'log': '',
            'load_method': 'manual',
            'state': 'draft',
        })
        _msg = "File {0} imported.".format(file_name)
        ssoih.to_log(cr, uid, _id, _msg, "info")
        for type_message, log_message in log_messages:
            ssoih.to_log(cr, uid, _id, log_message, type_message)
        title = "Complete Importing File : {0}!".format(file_name)
        result = self._stock_sale_order_import_info_form(cr, uid, title=title)
        return result

    def _stock_sale_order_import_info_form(self, cr, uid, title="INFO", message="MESSAGE", context=None):
        if(context is None):
            context = {}
        obj_model = self.pool.get('ir.model.data')
        model_data_ids = obj_model.search(
            cr,
            uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'stock_sale_order_import_info_form')
            ]
        )
        resource_id = obj_model.read(cr, uid, model_data_ids, fields=['res_id'])[0]['res_id']
        return {
            'name': _(str(title)),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.sale.order.import.info',
            'views': [(False, 'form'), (resource_id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
        }

stock_sale_order_import()


class stock_sale_order_import_info(osv.osv_memory):
    _name = 'stock.sale.order.import.info'

    def return_to_import(self, cr, uid, ids, context=None):
        if(context is None):
            context = {}
        obj_model = self.pool.get('ir.model.data')
        model_data_ids = obj_model.search(
            cr,
            uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'stock_sale_order_import_form')
            ]
        )
        resource_id = obj_model.read(cr, uid, model_data_ids, fields=['res_id'])[0]['res_id']
        return {
            'name': 'New Import Sale Orders',
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'stock.sale.order.import',
            'views': [(False, 'form'), (resource_id, 'form')],
            'target': 'new',
            'type': 'ir.actions.act_window',
            'context': context,
        }

    def to_new_import_managment(self, cr, uid, ids, context=None):
        if(context is None):
            context = {}
        obj_model = self.pool.get('ir.model.data')
        tree_data_ids = obj_model.search(
            cr, uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'view_stock_sale_order_import_history_tree')
            ]
        )
        form_data_ids = obj_model.search(
            cr, uid,
            [
                ('model', '=', 'ir.ui.view'),
                ('name', '=', 'view_stock_sale_order_import_history_form')
            ]
        )
        tree_resource_id = obj_model.read(cr, uid, tree_data_ids, fields=['res_id'])[0]['res_id']
        form_resource_id = obj_model.read(cr, uid, form_data_ids, fields=['res_id'])[0]['res_id']
        return {
            'name': 'New Sale Order Import History',
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'stock.sale.order.import.history',
            'views': [(tree_resource_id, 'tree'), (form_resource_id, 'form')],
            'type': 'ir.actions.act_window',
            'context': context,
        }

stock_sale_order_import_info()


class stock_sale_order_import_history(osv.osv):
    _name = "stock.sale.order.import.history"

    def _get_html(self, cr, uid, ids, *args, **kwargs):
        if isinstance(ids, (int, long)):
            ids = [ids]
        _tpl_table = \
            '''
            <table border="1">
                % for i, line in enumerate(lines):
                <tr id="${i}_line">
                    % for j, elem in enumerate(line):
                        ${generate_td(i, j, elem, bad_format, bad_lines)}
                    % endfor
                </tr>
                % endfor
            </table>
            <%!
                def generate_td(i, j, elem, bad_format, bad_lines):
                    td = "<td"
                    if(i in [0, 2]):
                        td += ' class="table_header" '
                    else:
                        td += ' class="table_data" '
                    msg_tooltip = ""
                    if(bad_format):
                        for message in bad_format:
                            msg_tooltip += message
                    elif(bad_lines):
                        for _id in bad_lines:
                            if(_id == (i+1)):
                                for message in bad_lines[_id]:
                                    msg_tooltip += message
                                td += 'bgcolor="red"'
                    if(msg_tooltip):
                        td += 'title="{0}"'.format(msg_tooltip)
                    if(elem is None):
                        elem = ''
                    td = td + " >" + elem + "</td>"
                    return td
            %>
            '''

        result = {}
        for _id in ids:
            result[_id] = ''
        for _id in ids:
            file_data = self.read_local_file(self.browse(cr, uid, _id).full_filename)
            if(file_data and (file_data not in ["Couldn't read file!", "File not Found!"])):
                dialect = detect_dialect(file_data)
                all_data = file_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
                header_csv = "\r\n".join(all_data[:2])
                main_csv = "\r\n".join(all_data[2:])
                bad_format = self.browse(cr, uid, _id).bad_format
                bad_lines = self.browse(cr, uid, _id).bad_lines
                if(bad_format):
                    bad_format = YamlObject().deserialize(_data=bad_format)
                if(bad_lines):
                    bad_lines = YamlObject().deserialize(_data=bad_lines)
                lines = []
                tpl_table = Template(_tpl_table)
                _pseudo_file = IO(header_csv)
                dr = DictReader(_pseudo_file, dialect=dialect)
                first_header = dr.fieldnames
                lines.append(first_header)
                for _row in dr:
                    row = []
                    for elem in first_header:
                        row.append(_row.get(elem, ''))
                    lines.append(row)

                _pseudo_file = IO(main_csv)
                dr = DictReader(_pseudo_file, dialect=dialect)
                second_header = dr.fieldnames
                lines.append(second_header)
                for _row in dr:
                    row = []
                    for elem in second_header:
                        row.append(_row.get(elem, ''))
                    lines.append(row)
                for i, line in enumerate(lines):
                    if(i == 0):
                        err_msg = 'ERRORS'
                    elif((isinstance(bad_lines, dict)) and (bad_lines.get((i+1), False))):
                        err_msg = ','.join(bad_lines[i+1])
                    else:
                        err_msg = ''
                    line.insert(0, err_msg)
                result[_id] = tpl_table.render(lines=lines, bad_format=bad_format, bad_lines=bad_lines)
            else:
                if(file_data):
                    result[_id] = file_data
                else:
                    result[_id] = ''
        return result

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'create_uid': fields.many2one('res.users', 'User', readonly=True, ),
        'create_date': fields.datetime('Import date', readonly=True, ),
        'state': fields.selection(
            [
                ('draft', 'Draft'),
                ('complete_import', 'Complete import'),
                ('error', 'Error'),
                ('bad_format', 'Bad Format'),
                ('bad_lines', 'Bad Lines'),
                ('created_xmls', 'In Progress'),
                ('canceled', 'Canceled'),
            ],
            'State', select=True, required=True, readonly=True),
        'download': fields.function(_get_file, type='binary2', method=True, string='Download', ),
        'full_filename': fields.char(string='Full filename', size=256, readonly=True, ),
        'filename': fields.char(string='Filename', size=256, readonly=True, ),
        'size': fields.char(string='Size', size=128, readonly=True, ),
        'log': fields.text('Log', type='log_list', readonly=True, ),
        'file_type': fields.char('File type', size=256, readonly=True, ),
        'load_order_ids': fields.text(string="Order ID'S", readonly=True, ),
        'load_for_partner': fields.char(string="Raw partners", size=256, readonly=True, ),
        'load_method': fields.selection(
            [
                ('manual', 'MANUAL'),
                ('from_ftp', 'FROM FTP'),
            ],
            'Load method', select=True, required=True, readonly=True),
        'editor': fields.text('Editor', readonly=False),
        'gen_html': fields.function(_get_html, method=True, string='HTML', ),
        'bad_lines': fields.text('Bad Lines', ),
        'bad_format': fields.text('Bad Format', ),
        'mfs': fields.many2one('multi.ftp.settings', 'Multi FTP Settings', ),
        'xml_ids': fields.one2many('sale.integration.xml', 'import_id', "XML's"),
    }

    _defaults = {
        'load_method': 'manual',
        'editor': '',
        'mfs': None,
    }

    _order = 'create_date desc'

    def read_local_file(self, PATH):
        data_to_editor = ""
        if(exists(PATH) and isfile(PATH) and access(PATH, R_OK)):
            try:
                data_to_editor = data_to_ascii(from_file=PATH)
            except Exception:
                data_to_editor = "Couldn't read file!"
        else:
            data_to_editor = "File not Found!"
        return data_to_editor

    def read(self, cr, uid, ids, fields_to_read=None, context=None, load='_classic_read'):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        read_local_file_to_editor = False
        if(fields_to_read):
            if('editor' in fields_to_read):
                read_local_file_to_editor = True
        result = super(stock_sale_order_import_history, self).read(cr, uid, ids, fields_to_read, context, load)
        if((read_local_file_to_editor) and (len(ids) == 1) and (load == '_classic_read')):
            for result_record in result:
                _data = self.read_local_file(result_record["full_filename"])
                result_record.update({'editor': _data})
        return result

    def write(self, cr, uid, ids, vals, context=None):
        result = False
        if context is None:
            context = {}
        from_to_log = context.get('from_to_log', False)
        if isinstance(ids, (int, long)):
            ids = [ids]
        if((len(ids) == 1) and (not from_to_log)):
            record_id = False
            if(vals.get('editor', False)):
                record = self.browse(cr, uid, ids[0])
                data_to_file = str(vals.get('editor', ''))
                PATH = record.full_filename
                if(exists(PATH) and isfile(PATH) and access(PATH, R_OK)):
                    data_from_file = self.read_local_file(PATH)
                    blf_utils.file_for_writing(dirname(PATH), basename(PATH), str(data_to_file), 'wb').create()
                    _diff = False
                    if(data_from_file and (data_from_file not in ["Couldn't read file!", "File not Found!"])):
                        _diff = _diff_files(data_from_file, data_to_file)
                    if(_diff):
                        _msg = "File edited:\n{0}".format(_diff)
                        self.to_log(cr, uid, record.id, _msg, "info")
                record_id = record.id
            if(vals.get('editor', None) is not None):
                vals['editor'] = ''
            result = super(stock_sale_order_import_history, self).write(cr, uid, ids, vals, context=context)
            if(result and record_id):
                self.load_orders(cr, uid, record_id, context=context)
        else:
            result = super(stock_sale_order_import_history, self).write(cr, uid, ids, vals, context=context)
        return result

    def get_xml_ids(self, cr, uid, _id, *args, **kwargs):
        return [xml.id for xml in self.browse(cr, uid, _id).xml_ids]

    def recalculate_size(self, cr, uid, ids, context=None):
        if(not isinstance(ids, list)):
            ids = [ids]

        records = self.browse(cr, uid, ids)
        for r in records:
            PATH = str(r.full_filename)
            if(exists(PATH) and isfile(PATH) and access(PATH, R_OK)):
                size = getsize(PATH)
                self.write(cr, uid, r.id, {'size': str(size)})
            else:
                self.write(cr, uid, r.id, {'size': "File not found!"})
        return True

    def unlink(self, cr, uid, ids, context=None):
        if(not isinstance(ids, list)):
            ids = [ids]
        res = True
        records = self.browse(cr, uid, ids)
        for r in records:
            if(not self.check_sale_order_ids(cr, uid, r.id, context=context)):
                PATH = str(r.full_filename)
                if(exists(PATH) and isfile(PATH) and access(PATH, R_OK)):
                    try:
                        remove_file(PATH)
                    except Exception as ex:
                        _msg = "\n{0}".format(ex.message)
                        _log = self.browse(cr, uid, r.id).log
                        _log.append(_msg)
                        self.write(cr, uid, r.id, {'errors': _log})
                    finally:
                        self.delete_xmls(cr, uid, r.id)
                        res = super(stock_sale_order_import_history, self).unlink(cr, uid, r.id, context=context)
                else:
                    self.delete_xmls(cr, uid, r.id)
                    res = super(stock_sale_order_import_history, self).unlink(cr, uid, r.id, context=context)
            else:
                _msg = "You cannot delete record! Please delete all sale_orders first!"
                self.to_log(cr, uid, r.id, _msg, "info")
        return res

    def check_sale_order_ids(self, cr, uid, _id, context=None):
        status = None
        record = self.browse(cr, uid, _id)
        sale_order_obj = self.pool.get('sale.order')
        xml_ids = self.get_xml_ids(cr, uid, record.id)
        for xml_id in xml_ids:
            so_ids = sale_order_obj.search(cr, uid, [('sale_integration_xml_id', '=', int(xml_id))])
            if(so_ids):
                status = True
                break
        else:
            status = False
        return status

    def delete_xmls(self, cr, uid, _id, context=None):
        if(context is None):
            context = []
        for xml_id in self.get_xml_ids(cr, uid, _id):
            xml = self.pool.get('sale.integration.xml').browse(cr, uid, xml_id)
            _msg = "Delete XML[id: {0}, name:{1}]".format(xml.id, xml.name)
            self.pool.get('sale.integration.xml').unlink(cr, uid, xml.id)
            self.to_log(cr, uid, _id, _msg, "info")
        return True

    def back_to_draft(self, cr, uid, ids, context=None):
        if(context is None):
            context = []
        if(not isinstance(ids, list)):
            ids = [ids]
        for record in self.browse(cr, uid, ids, context=context):
            if(not self.check_sale_order_ids(cr, uid, record.id, context=context)):
                self.delete_xmls(cr, uid, record.id)
                self.write(cr, uid, ids, {'state': 'draft', 'load_for_partner': '', 'load_order_ids': ''})
                _msg = "Back to draft"
                self.to_log(cr, uid, record.id, _msg, "info")
            else:
                raise OSVException("You cannot back to draft, please delete sale order's first!")
        return True

    def load_orders(self, cr, uid, ids, context=None):
        if(context is None):
            context = {}
        if ids:
            if isinstance(ids, (int, long)):
                _id = ids
            else:
                assert len(ids) == 1, 'Load order may only be done one at a time'
                _id = ids[0]
            if(_id):
                self.back_to_draft(cr, uid, _id)
                self.pool.get('sale.integration').integrationApiLoadOrders(
                    cr,
                    uid,
                    "Import Orders",
                    _id
                )
        return True

    def create_orders(self, cr, uid, ids, context=None):
        if(context is None):
            context = {}
        if ids:
            if isinstance(ids, (int, long)):
                _id = ids
            else:
                assert len(ids) == 1, 'Create order may only be done one at a time'
                _id = ids[0]

            xml_obj = self.pool.get('sale.integration.xml')
            api_obj = self.pool.get('sale.integration')
            if(_id and (self.browse(cr, uid, _id).state == 'created_xmls')):
                xml_ids = self.get_xml_ids(cr, uid, _id)
                to_create_xml_ids = xml_obj.search(cr, uid, [('id', 'in', xml_ids), ('load', '=', False)])
                still_need_create_ids = []

                cr.execute('select xml_id from _rabbitmq_create_order_queue')
                rabbit_ids = map(lambda x: x[0], cr.fetchall() or [])

                if to_create_xml_ids and all(map(lambda x, rids=rabbit_ids: x not in rids, to_create_xml_ids)):
                    api_obj.integrationApiCreateOrders(
                        cr,
                        uid,
                        "Import Orders",
                        xml_id=to_create_xml_ids
                    )
                    still_need_create_ids = xml_obj.search(cr, uid, [('id', 'in', xml_ids), ('load', '=', False)])
                if not still_need_create_ids:
                    self.write(cr, uid, _id, {'state': 'complete_import', 'bad_lines': '', 'bad_format': ''})

        return True

    def _update_completed(self, cr, uid, ids):
        xml_obj = self.pool.get('sale.integration.xml')
        completed_ids = []
        for import_id in ids:
            xml_ids = self.get_xml_ids(cr, uid, import_id)
            not_loaded_ids = xml_obj.search(cr, uid, [
                ('id', 'in', xml_ids),
                ('load', '=', False),
            ])
            if not not_loaded_ids:
                completed_ids.append(import_id)
        self.write(cr, uid, completed_ids, {'state': 'complete_import'})

    def finish_import(self, cr, uid, xml_ids):
        xml_ids = [xml_ids] if isinstance(xml_ids, (int, long)) else xml_ids
        xml_obj = self.pool.get('sale.integration.xml')
        xmls = xml_obj.browse(cr, uid, xml_ids)
        import_ids = [xml.import_id.id for xml in xmls if xml.import_id]
        self._update_completed(cr, uid, import_ids)

    def to_log(self, cr, uid, _id, message, type_message, context=None):
        if(context is None):
            context = {}
        date_now = "{:%Y-%m-%d %H:%M:%S}".format(datetime.now())
        _log = self.browse(cr, uid, _id).log
        _msg = None
        if(message and type_message in ['info', 'error', 'warning', 'bad format', 'bad lines']):
            uid_name = self.pool.get('res.users').browse(cr, uid, uid).name
            _msg = "{0}[{1}][{2}] : {3}\n".format(uid_name, date_now, type_message.upper(), message)
        if(_msg):
            _log = _log + _msg
            context.update({'from_to_log': True})
            self.write(cr, uid, _id, {'log': _log}, context=context)
        return True

    def _log_errors(self, cr, uid, _id, state, error):
        if not error:
            return
        for err_msg in error:
            self.to_log(cr, uid, _id, err_msg, state.replace('_', ' '))

    def add_errors(self, cr, uid, _id, errors, context=None):
        import_instance = self.browse(cr, uid, _id)
        result = False
        write_args = {}
        for error_type in errors:
            error = errors[error_type]
            if not error and error_type == 'critical':
                continue
            result |= bool(error)
            state = 'error' if error_type == 'critical' else error_type
            self._log_errors(cr, uid, _id, state, error)
            if error_type != 'critical':
                write_args.update({error_type: YamlObject().serialize(_data=error)})
            if not error:
                continue
            write_args['state'] = state
            subject = ''
            if error_type == 'bad_lines':
                subject = 'Bad Lines in '
            elif error_type == 'bad_format':
                subject = 'Bad format'
            else:
                continue
            subject += 'file: {0}'.format(import_instance.filename)
            self.send_notification_to_email(cr, uid, _id, subject)
        if write_args:
            self.write(cr, uid, _id, write_args)
        return result

    def send_notification_to_email(self, cr, uid, _id, subject, context=None):
        record = self.browse(cr, uid, _id)
        if(record.mfs and record.mfs.emails_list):
            email_list_str = record.mfs.emails_list
            emails_list = [mail.strip() for mail in (email_list_str or '').split(',') if mail and mail.strip()]
            mail_messages = [{
                'email_from': 'erp.delmar@progforce.com',
                'emailto_list': emails_list,
                'subject': subject,
                'body': '',
                'attachments': [],
            }]
            self.pool.get('sale.integration').integrationApiSendEmail(cr, uid, mail_messages)
            _msg = 'Email sended to {0}'.format(email_list_str)
            self.to_log(cr, uid, _id, _msg, "info")
        else:
            _msg = 'Not found setting for sending notifications to email, email not sent!'
            self.to_log(cr, uid, _id, _msg, "info")

stock_sale_order_import_history()


# This class is deprecated.
class change_type_api_history(osv.osv):
    _name = "change.type.api.history"

    def _get_order_name(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for row in self.browse(cr, uid, ids):
            order_name = 'Sale Integration XML not found'
            try:
                order_name = row and row.sale_integration_xml_id and row.sale_integration_xml_id.name or 'Sale Integration XML not found'
            except AttributeError:
                pass
            res[row.id] = order_name
        return res

    _columns = {
        'sale_integration_xml_id': fields.many2one('sale.integration.xml', 'Sale Integration XML', ),
        'order_name': fields.function(
            _get_order_name,
            type='char',
            method=True,
            string='Order Name',
            readonly=True,
        ),
        'unique_order_name': fields.char('Unique Order Name', size=512, ),
        'input_type_api_id': fields.many2one('sale.integration', 'Input type_api_id', readonly=True, ),
        'new_type_api_id': fields.many2one('sale.integration', 'type_api_id for Sale Order', ),
        'changed': fields.boolean('Changed', readonly=True, ),
    }

    _defaults = {
        'changed': False,
    }

change_type_api_history()
