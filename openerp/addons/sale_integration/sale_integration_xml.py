from osv import fields, osv


class sale_integration_xml(osv.osv):
    _name = "sale.integration.xml"

    _columns = {
        'name': fields.char('Order ID', size=512),
        'type_api_id': fields.many2one('sale.integration', 'Service provider'),
        'new_type_api_id': fields.many2one('sale.integration', 'New Service provider'),
        'load': fields.boolean('Load in system'),
        'xml': fields.text('XML'),
        'create_date': fields.datetime('Creation Date', readonly=True),
        'import_id': fields.many2one('stock.sale.order.import.history', 'Import history'),
    }

    _order = "create_date DESC"

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if not args:
            return []
        new_args = []
        for arg in args:
            if isinstance(arg, (tuple, list)) and arg[0] == 'type_api_id':
                new_api_arg = type(arg)(['new_type_api_id']) + arg[1:]
                is_negative_operator = arg[1] == '!=' or 'not' in arg[1]
                # use union for positive and intersection for negative operators
                term_operator = '&' if is_negative_operator else '|'
                new_args.extend([term_operator, arg, new_api_arg])
            else:
                new_args.append(arg)
        res = super(sale_integration_xml, self).search(
            cr, user, new_args, offset=offset, limit=limit, order=order,
            context=context, count=count)
        return res

    def write(self, cr, uid, ids, vals, context=None):
        res = super(sale_integration_xml, self).write(cr, uid, ids, vals,
                                                      context=context)
        if vals.get('load'):
            import_obj = self.pool.get('stock.sale.order.import.history')
            import_obj.finish_import(cr, uid, ids)
        return res


class sale_integration_sterling_xml(osv.osv):
    _name = 'sale.integration.sterling.xml'
    _columns = {
        'send': fields.integer('Send'),
        'send_date': fields.datetime('Send Date', readonly=True),
        'customer_sku': fields.char('Sku', size=250),
        'size': fields.char('size', size=250),
        'phantom_inventory': fields.integer('Phantom inventory'),
        'force_zero': fields.integer('Force zero'),
        'status': fields.integer('Status'),
        'qty': fields.integer('Qty'),
        'actual_qty': fields.integer('Actual qty'),
        'delmar_id': fields.char('delmar_id', size=250),
        'xml': fields.text('XML', required=False),
        'response': fields.text('Response', required=False),
        'sale_integration_id': fields.integer('Sale Integration Id'),
        'product_id': fields.integer('product_id')
    }

    _defaults = {
        'send': 0,
        'force_zero': 0,
        'phantom_inventory': 0,
        'qty': 0
    }

sale_integration_sterling_xml()


class sale_integration_overstock_xml(osv.osv):
    _name = 'sale.integration.overstock.xml'
    _columns = {
        'send': fields.integer('Send'),
        'send_date': fields.datetime('Send Date', readonly=True),
        'customer_sku': fields.char('Sku', size=250),
        'size': fields.char('size', size=250),
        'phantom_inventory': fields.integer('Phantom inventory'),
        'force_zero': fields.integer('Force zero'),
        'status': fields.integer('Status'),
        'qty': fields.integer('Qty'),
        'actual_qty': fields.integer('Actual qty'),
        'delmar_id': fields.char('delmar_id', size=250),
        'xml': fields.text('XML', required=False),
        'response': fields.text('Response', required=False),
        'sale_integration_id': fields.integer('Sale Integration Id'),
        'product_id': fields.integer('product_id')
    }

    _defaults = {
        'send': 0,
        'force_zero': 0,
        'phantom_inventory': 0,
        'qty': 0
    }

sale_integration_overstock_xml()


class sale_integration_overstock_sofs_xml(osv.osv):
    _name = 'sale.integration.overstock.sofs.xml'
    _columns = {
        'send': fields.integer('Send'),
        'send_date': fields.datetime('Send Date', readonly=True),
        'customer_sku': fields.char('Sku', size=250),
        'size': fields.char('size', size=250),
        'phantom_inventory': fields.integer('Phantom inventory'),
        'force_zero': fields.integer('Force zero'),
        'status': fields.integer('Status'),
        'qty': fields.integer('Qty'),
        'actual_qty': fields.integer('Actual qty'),
        'delmar_id': fields.char('delmar_id', size=250),
        'xml': fields.text('XML', required=False),
        'response': fields.text('Response', required=False),
        'sale_integration_id': fields.integer('Sale Integration Id'),
        'product_id': fields.integer('product_id'),
        # 'previous_qty': fields.integer('previous qty'),
    }

    _defaults = {
        'send': 0,
        'force_zero': 0,
        'phantom_inventory': 0,
        'qty': 0,
    }

sale_integration_overstock_sofs_xml()


class sale_integration_overstock_full(osv.osv):
    _name = 'sale.integration.overstock.full'
    _columns = {
        'send': fields.integer('Send'),
        'customer_sku': fields.char('Sku', size=250),
        'size': fields.char('size', size=250),
        'phantom_inventory': fields.integer('Phantom inventory'),
        'force_zero': fields.integer('Force zero'),
        'qty_tolerance': fields.integer('Qty Tolerance'),
        'qty': fields.integer('Qty'),
        'actual_qty': fields.integer('Actual qty'),
        'delmar_id': fields.char('delmar_id', size=250),
    }

    _defaults = {
        'send': 0,
        'force_zero': 0,
        'phantom_inventory': 0,
        'qty': 0
    }

sale_integration_overstock_full()
