# -*- coding: utf-8 -*-
from osv import fields, osv
import netsvc
from openerp.addons.pf_utils.utils.decorators import timethis
import time
from datetime import datetime, timedelta
import logging
from openerp.addons.product_catalog_helper.catalog import catalog_request


_logger = logging.getLogger(__name__)


class flash_from_stock_line(osv.osv):
    _name = "flash.from.stock.line"
    _columns = {
        "line_id": fields.char("ID", size=256, required=True, ),
        "ext_id": fields.float("Ext ID", ),
        "warehouse": fields.char('Warehouse', size=100, required=True, ),
        "stockid": fields.char('Stockid', size=256, ),
        "product_id": fields.many2one("product.product", "Product", required=True),
        "size_id": fields.many2one("ring.size", "Size", ),
        "bin": fields.char('Bin', size=256, ),
        "qty": fields.integer('Qty', ),
        "flash_id": fields.many2one("flash.from.stock", "Order", ondelete='cascade', ),
        "sale_id": fields.related("flash_id", "sale_id", string="Order", type="many2one", relation="sale.order", ),
    }

    _order = "warehouse, stockid, product_id, size_id"

    def approve(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if context.get('flash_id'):
            self.write(cr, uid, ids, {'flash_id': context['flash_id']})
        return True

    def reject(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {"flash_id": False})

        return True

flash_from_stock_line()


class flash_from_stock(osv.osv):
    _name = "flash.from.stock"
    _rec_name = "po_number"

    def _get_search_ids(self, cr, uid, ids, name, args, context=None):
        res = {x: False for x in ids}
        return res

    _columns = {
        'create_date': fields.datetime('Creation Date', readonly=True, select=True),
        "sale_id": fields.many2one("sale.order", "Order", ),
        "po_id": fields.many2one("purchase.order", "Purchase Order", ),
        "po_number": fields.char('PO#', size=256, required=True, ),
        "partner_id": fields.many2one("res.partner", "Customer", required=True, ),
        "address_id": fields.many2one("res.partner.address", "Address", required=True, ),
        "carrier": fields.char("Carrier", size=256),
        "price_type": fields.selection([
            ('zero', 'Zero'),
            ('gta', 'GTA'),
            ('cm_price', 'Customer Price'),
            ('cm_price_formula', 'Customer Price Formula')
        ], "Price", required=True, ),
        "warehouse_id": fields.many2one("stock.warehouse", "Warehouse", ),
        "source_id": fields.many2one(
            "stock.location", "Source location",
            required=False, domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', ' / Stock')]
        ),
        'search_ids': fields.function(
            _get_search_ids,
            string='Sesrch',
            type='one2many',
            obj="flash.from.stock.line",
            method=True,
            ),
        "line_ids": fields.one2many(
            "flash.from.stock.line",
            "flash_id", "Order"
        ),
        "ext_id": fields.float("Hash", ),
        "location_ids": fields.many2many(
            "stock.location",
            "flash_from_stock_loc_rel", "flash_id", "loc_id",
            string='Locations', readonly=True,
        ),
        "refresh_btn": fields.boolean("Refresh"),
        "approve_all_btn": fields.boolean("Approve All"),
        "reject_all_btn": fields.boolean("Reject All"),
        'button_update_po_on_pc': fields.boolean('Refresh PO on P/C', ),

    }

    _defaults = {
        "ext_id": lambda self, cr, uid, ctx: int(time.time() * 1000)
    }

    def change_partner_id(self, cr, uid, ids, partner_id, context=None):
        value = {
            "partner_id": partner_id,
            "address_id": False
        }

        FALSE_DOMAIN = [('id', '=', False)]

        domain = {
            "address_id": [('partner_id', '=', partner_id)] if partner_id else FALSE_DOMAIN
        }

        return {"value": value, "domain": domain}

    @timethis
    def change_source_id(self, cr, uid, ids, ext_id, source_id, context=None):
        value = {}
        warning = {}
        line_ids = []
        if source_id:
            location_obj = self.pool.get("stock.location")
            wh, locs = location_obj.get_wh_loc(cr, uid, source_id)
            if (
                wh and
                self.get_approved_line_ids(cr, uid, ids, ext_id, limit=1, domain=[("warehouse", "!=", wh)])
            ):
                value["source_id"] = False
                warning = {
                    "title": "Configuration Error !",
                    "message": "You can't select items from different warehouses"
                }
            else:
                line_ids = self.get_search_line_ids(cr, uid, ids, ext_id, wh, locs)

        value["search_ids"] = line_ids

        return {"value": value, "warning": warning}

    def prepare_lines(self, cr, uid, ids, context=None):
        line_obj = self.pool.get("flash.from.stock.line")
        prepare_lines = """
            INSERT INTO flash_from_stock_line (
                  id
                , create_uid
                , create_date
                , product_id
                , size_id
                , warehouse
                , stockid
                , bin
                , qty
                , line_id
                , ext_id
            )
            SELECT
                  distinct on (fs.id)
                  nextval(%(sequence)s) as id
                , %(uid)s as create_uid
                , (now() at time zone 'UTC') as create_date
                , pp.id as product_id
                , rs.id as size_id
                , fs.warehouse
                , fs.stockid
                , fs.bin
                , fs.qty
                , fs.id as line_id
                , fs.ext_id
            from _tmp_flash_stock fs
                left join product_product pp on pp.default_code = fs.id_delmar and pp.type = 'product'
                left join ring_size rs on (fs.size::float / 100.0) = CASE WHEN coalesce(rs.name ~ '^[\.0-9]+$') THEN rs.name::float ELSE 0.0 END
            where pp.id is not null
            order by fs.id, pp.id desc

        """
        cr.execute(prepare_lines, {'uid': uid, 'sequence': line_obj._sequence})
        return True

    def fill_temp_data(self, cr, uid, ext_id, warehouse, stockids=None, line_ext_ids=None, store=None, context=None):
        server_obj = self.pool.get('fetchdb.server')
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            raise osv.except_osv('Warning', 'Remote server is not available.')

        ext_id = int(ext_id)
        params = [
            warehouse,
        ]

        if line_ext_ids is None:
            line_ext_ids = ''

        def chunks(l, n):
            for i in xrange(0, len(l), n):
                yield l[i:i+n]

        prepare_sqls = []
        select_sqls = []

        w_stockid = ''
        if stockids:
            if not isinstance(stockids, (list, tuple)):
                stockids = [stockids]
            w_stockid = "AND tr.stockid in ({stock})".format(stock=",".join(['?'] * len(stockids)))
            params += stockids

        stock_sql = """
            SELECT
                inv.id,
                inv.warehouse,
                inv.stockid,
                inv.bin,
                inv.id_delmar,
                inv.size,
                inv.qty,
                {s_invredid} as invredid,
                {use} coalesce(ti.Reserved, 0) as reserved,
                {use} ti.ITEMDESCRIPTION as itemdescription,
                {ext_id} as ext_id
            FROM (
                SELECT
                    CONVERT(NVARCHAR(32), HashBytes(
                        'MD5',
                        CONCAT(
                            tr.WAREHOUSE, '||',
                            tr.STOCKID, '||',
                            COALESCE(tr.BIN, ''), '||',
                            tr.ID_DELMAR, '||',
                            CASE WHEN tr."SIZE" IS NULL OR tr."SIZE" = '' THEN '0000' ELSE tr."SIZE" END
                        )
                    ), 2) as id,
                    tr.WAREHOUSE as warehouse,
                    tr.STOCKID as stockid,
                    COALESCE(tr.BIN, '') as bin,
                    tr.ID_DELMAR as id_delmar,
                    CASE WHEN tr."SIZE" IS NULL OR tr."SIZE" = '' THEN '0000' ELSE tr."SIZE" END AS "size",
                    SUM(tr.ADJUSTED_QTY) as qty,
                    max(tr.id) as last_id
                FROM TRANSACTIONS tr LEFT JOIN prg_sets ps on tr.id_delmar = ps.id_delmar
                WHERE STATUS != 'Posted'
                    AND ps.id is null
                    AND tr.warehouse = ? {w_stockid}
                GROUP BY
                    tr.WAREHOUSE,
                    tr.STOCKID,
                    COALESCE(tr.BIN, ''),
                    tr.ID_DELMAR,
                    CASE WHEN tr."SIZE" IS NULL OR tr."SIZE" = '' THEN '0000' ELSE tr."SIZE" END
                HAVING SUM(ADJUSTED_QTY) > 0
            ) as inv
                {use} left join transactions ti on ti.id = inv.last_id
            WHERE 1=1 {w_line_ext_ids}
        """

        for line_ids in chunks(line_ext_ids.split(','), 3000):
            line_ids = ",".join(line_ids)
            w_line_ext_ids = "AND inv.id in (%s)" % (line_ids) if line_ids else ''

            select_sqls.append((
                stock_sql.format(
                    w_line_ext_ids=w_line_ext_ids,
                    w_stockid=w_stockid,
                    ext_id=ext_id,
                    use='' if store else '--',
                    s_invredid='ti.invredid' if store else 'null'
                ),
                params
                )
            )

        if store:
            store_sql = """
                INSERT INTO _prg_flash_inv (
                    id,
                    warehouse,
                    stockid,
                    bin,
                    id_delmar,
                    size,
                    qty,
                    invredid,
                    reserved,
                    itemdescription,
                    ext_id
                ) {stock_sql};
            """
            prepare_sqls = [
                ("""DELETE FROM _prg_flash_inv WHERE ext_id = {ext_id};""".format(ext_id=ext_id), None)
            ]

            prepare_sqls += [(store_sql.format(stock_sql=x), y) for x, y in select_sqls]

            select_sqls = [("""
                SELECT
                    inv.id,
                    inv.warehouse,
                    inv.stockid,
                    inv.bin,
                    inv.id_delmar,
                    inv.size,
                    inv.qty,
                    inv.invredid,
                    inv.reserved,
                    inv.itemdescription,
                    inv.ext_id
                FROM _prg_flash_inv inv
                WHERE ext_id = {ext_id};
            """.format(
                ext_id=ext_id
            ), None
            )]

        for sql, p in prepare_sqls:
            status = server_obj.make_query(cr, uid, server_id, sql, params=p, select=False, commit=True)
            if status is None:
                raise osv.except_osv('Warning', 'Inventory table is not available.')

        stock_res = []
        for sql, p in select_sqls:
            stock = server_obj.make_query(cr, uid, server_id, sql, params=p, select=True, commit=False)
            if stock is None:
                raise osv.except_osv('Warning', 'Inventory table is not available.')
            stock_res += stock

        create_tmp_sql = """
            DROP TABLE IF EXISTS _tmp_flash_stock;
            CREATE TEMPORARY TABLE _tmp_flash_stock (
                id varchar not null,
                warehouse varchar not null,
                stockid varchar not null,
                bin varchar not null,
                id_delmar varchar not null,
                size varchar not null,
                qty integer not null,
                invredid varchar(128),
                ext_id float
            )
        """
        cr.execute(create_tmp_sql)
        insert_tmp_sql = """
            insert into _tmp_flash_stock (
                id, warehouse, stockid, bin,
                id_delmar, size, qty, invredid,
                ext_id
            ) VALUES (
                %(id)s, %(warehouse)s, %(stockid)s, %(bin)s,
                %(id_delmar)s, %(size)s, %(qty)s, %(invredid)s,
                %(ext_id)s
                )
        """
        keys = [
            'id', 'warehouse', 'stockid', 'bin',
            'id_delmar', 'size', 'qty',
            'reserved', 'invredid', 'itemdescription',
            'ext_id'
        ]

        if not store:
            for x in ['reserved', 'itemdescription']:
                keys.remove(x)

        stock_params = [dict(zip(keys, row)) for row in stock_res]
        cr.executemany(insert_tmp_sql, stock_params)

    def get_approved_line_ids(self, cr, uid, ids, ext_id, warehouse=None, stockids=None, domain=None, limit=None, context=None):
        if domain is None:
            domain = []
        domain += [
            ('ext_id', '=', ext_id),
            ('flash_id', 'in', ids)
        ]
        if warehouse:
            domain.append(('warehouse', '=', warehouse))

        if stockids:
            domain.append(('stockid', 'in', stockids))
        approved_ids = self.pool.get('flash.from.stock.line').search(cr, uid, domain, limit=limit)
        return approved_ids

    def get_search_line_ids(self, cr, uid, ids, ext_id, warehouse, stockids, domain=None, context=None):
        if domain is None:
            domain = []
        domain += [
            ('warehouse', '=', warehouse),
            ('stockid', 'in', stockids),
            ('ext_id', '=', ext_id),
            ('flash_id', '=', False),
        ]

        line_ids = []

        if warehouse and stockids:
            line_obj = self.pool.get("flash.from.stock.line")

            not_used = list(set(stockids) - set(self.get_locations_from_temp_data(cr, uid, ext_id)))

            if not_used:
                self.fill_temp_data(
                    cr, uid,
                    ext_id,
                    warehouse,
                    stockids=not_used
                )

                self.prepare_lines(cr, uid, ids)

            line_ids = line_obj.search(cr, uid, domain)
        return line_ids

    def get_locations_from_temp_data(self, cr, uid, ext_id, context=None):
        res = []
        cr.execute("""
            select distinct stockid
            from flash_from_stock_line
            where ext_id = %(ext_id)s
            """, {'ext_id': ext_id}
        )

        for line in cr.fetchall() or []:
            res.append(line[0])

        return res

    def approve_all_btn(self, cr, uid, ids, ext_id, source_id, context=None):
        if source_id and ids:
            location_obj = self.pool.get("stock.location")
            line_obj = self.pool.get("flash.from.stock.line")
            wh, locs = location_obj.get_wh_loc(cr, uid, source_id)
            line_ids = self.get_search_line_ids(cr, uid, ids, ext_id, wh, locs)
            if line_ids:
                line_obj.write(cr, uid, line_ids, {"flash_id": ids[0]})

        return {
            'value': {
                'line_ids': self.get_approved_line_ids(cr, uid, ids, ext_id),
                'search_ids': [],
            }
        }

    def reject_all_btn(self, cr, uid, ids, ext_id, source_id, context=None):
        location_obj = self.pool.get("stock.location")
        line_obj = self.pool.get("flash.from.stock.line")
        wh, locs = location_obj.get_wh_loc(cr, uid, source_id)
        line_ids = self.get_approved_line_ids(cr, uid, ids, ext_id, warehouse=wh, stockids=locs)
        if line_ids:
            line_obj.write(cr, uid, line_ids, {"flash_id": False})

        return {
            'value': {
                'line_ids': self.get_approved_line_ids(cr, uid, ids, ext_id, wh, locs),
                'search_ids': self.get_search_line_ids(cr, uid, ids, ext_id, wh, locs),
            }
        }

    def update_approved(self, cr, uid, ids, ext_id, context=None):
        return {'value': {'line_ids': self.get_approved_line_ids(cr, uid, ids, ext_id)}}

    def fill_order(self, cr, uid, ids, context=None):
        return True

    def get_location_ids(self, cr, uid, ids, context=None):
        loc_ids = []
        if ids:
            cr.execute("""
                select distinct sl.id
                from flash_from_stock_line fl
                    left join stock_location sl on sl.complete_name ilike concat('Physical Locations / ', fl.warehouse, ' / Stock / ', fl.stockid)
                where fl.flash_id in %(flash_ids)s
                    and sl.id is not null
                """, {'flash_ids': (tuple(ids), )})
            loc_ids = [x[0] for x in cr.fetchall() or []]
        return loc_ids

    def get_line_ext_ids(self, cr, uid, ids, context=None):
        ext_ids = {}
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]
            cr.execute("""
                select fs.id, string_agg(concat('''', fl.line_id, ''''), ',') as line_ids
                from flash_from_stock fs
                    left join flash_from_stock_line fl on fs.id = fl.flash_id
                where fs.id in %(flash_ids)s
                group by fs.id
                """, {'flash_ids': (tuple(ids), )}
            )
            for line in cr.fetchall() or []:
                ext_ids[line[0]] = line[1]
        return ext_ids

    @timethis
    def confirm(self, cr, uid, ids, context=None):

        if ids and len(ids) == 1:
            sale_obj = self.pool.get("sale.order")
            server_obj = self.pool.get('fetchdb.server')
            location_obj = self.pool.get("stock.location")
            warehouse_obj = self.pool.get("stock.warehouse")

            server_id = server_obj.get_sale_servers(cr, uid, single=True)
            if not server_id:
                raise osv.except_osv('Warning', 'Remote server is not available.')

            builder = self.browse(cr, uid, ids[0])
            ext_id = float(builder.ext_id)

            if not builder.line_ids:
                raise osv.except_osv('Warning', 'You should select at least 1 item to create order.')

            force_location_ids = self.get_location_ids(cr, uid, ids)
            if not force_location_ids:
                raise osv.except_osv('Warning', "List of locations can't be empty.")
            wh_id = location_obj.get_warehouse(cr, uid, force_location_ids[0])
            wh_name = warehouse_obj.read(cr, uid, wh_id, ['name'])['name']

            sale_values = {
                "flash": True,
                "po_number": builder.po_number,
                "external_customer_order_id": builder.po_number,
                "partner_id": builder.partner_id.id,
                "carrier": builder.carrier,
                "force_location_ids": [(6, 0, force_location_ids)],
                "flash_force_id": ext_id
            }

            partner_vals = sale_obj.onchange_partner_id(cr, uid, None, builder.partner_id.id)
            sale_values.update(partner_vals.get('value', {}))
            sale_values.update({'partner_shipping_id': builder.address_id.id})

            sale_id = sale_obj.create(cr, uid, sale_values)
            if not sale_id:
                raise osv.except_osv('Warning', 'Orde was not created.')
            else:
                builder.write({'sale_id': sale_id, 'warehouse_id': wh_id})

            line_ext_ids = self.get_line_ext_ids(cr, uid, builder.id)[builder.id]

            self.fill_temp_data(
                cr, uid, ext_id,
                wh_name, stockids=[x['name'] for x in location_obj.read(cr, uid, force_location_ids, ['name'])],
                line_ext_ids=line_ext_ids,
                store=True, context=None
            )

            self.fill_sale_lines(
                cr, uid,
                sale_id, builder.partner_id.id
            )

            if builder.price_type == 'gta':
                sale_obj.update_price_from_gta(cr, uid, sale_id)
            elif builder.price_type == 'cm_price':
                sale_obj.update_price_from_cf(cr, uid, sale_id, 'Customer Price')
            elif builder.price_type == 'cm_price_formula':
                self.update_price_formula(cr, uid, sale_id)
                sale_obj.update_price_from_cf(cr, uid, sale_id, 'Customer Price Formula')

            self.recalculate_stored_fields(cr, uid, [sale_id])

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'sale.order', sale_id, 'order_confirm', cr)

            return True

        else:
            raise osv.except_osv('Warning', 'You can create only one order at time.')

    @timethis
    def fill_sale_lines(self, cr, uid, sale_id, partner_id, context=None):
        # # Better way, but there is a problems specific values for lines
        # import types
        # line_ctx = {}
        # line_defaults = {}
        # for key in po_line_obj._defaults:
        #     value = None
        #     if isinstance(po_line_obj._defaults[key], types.FunctionType):
        #         value = po_line_obj._defaults[key](cr, uid, line_ctx)
        #     else:
        #         value = po_line_obj._defaults[key]
        #     line_defaults[key] = value

        line_defaults = {
            'product_uom': 1,
            'product_uos': 1,
            'sequence': 10,
            'price_unit': 0.0,
            'state': 'draft',
            'delay': 0,
            'pack_depth': 0,
            'type': 'make_to_stock',
            'create_uid': uid,
            'order_id': sale_id,
            'order_partner_id': partner_id,
        }

        default_fields = line_defaults.keys()
        default_fields_value = ['%({key})s as {key}'.format(key=key) for key in default_fields]

        prepare_lines = """
            insert into sale_order_line (
                {default_fields}
                , create_date
                , product_uos_qty
                , product_uom_qty
                , size_id
                , product_id
                , name
                , external_customer_line_id
            )
            select
                distinct on (fs.id)
                {default_fields_value}
                , (now() at time zone 'UTC') as create_date
                , fs.qty as product_uos_qty
                , fs.qty as product_uom_qty
                , rs.id as size_id
                , pp.id as product_id
                , coalesce(pp.short_description, pt.name) as name
                , fs.id as external_customer_line_id
            from _tmp_flash_stock fs
                left join product_product pp on pp.default_code = fs.id_delmar and pp.type = 'product'
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join ring_size rs on (fs.size::float / 100.0) = rs.name::float
            where pp.id is not null
            order by fs.id, pp.id desc
        """.format(
            default_fields=",\n".join(default_fields),
            default_fields_value=",\n".join(default_fields_value)
        )
        cr.execute(prepare_lines, line_defaults)

    def update_price_formula(self, cr, uid, order_id, context=None):
        pricelist_callbacks_obj = self.pool.get('pricelist.callbacks')

        sql = """
            select
                ol.product_id,
                array_remove(array_agg(distinct ol.size_id), null) as size_id
            from sale_order_line ol
            where ol.order_id = %(order_id)s
            group by ol.product_id
        """
        cr.execute(sql, {'order_id': order_id})
        lines = cr.fetchall() or []

        data = {}

        for product_id, size_ids in lines:
            data[product_id] = size_ids or None

        return pricelist_callbacks_obj.calculate_pricelists(
            cr, uid,
            context={
                'profile_it': False,
                'store_to_customer_fields': True,
                'overwrite': True,
                'data': data,
            }
        )

    def _clean_garbage(self, cr, uid, context=None):

        delta_days = 1

        cr.execute("""
            DELETE FROM flash_from_stock
            WHERE sale_id is null
                AND create_date < (now() at time zone 'UTC') - interval '%(delta)s days'
        """, {'delta': delta_days})

        ext_id = int(time.mktime((datetime.today() - timedelta(days=delta_days)).timetuple()) * 1000)

        server_obj = self.pool.get("fetchdb.server")
        server_id = server_obj.get_sale_servers(cr, uid, single=True)
        if not server_id:
            _logger.error('Remote server is not available.')
            return False

        sql = """
            delete from _prg_flash_inv
            where ext_id < %(ext_id)s;

            delete from _prg_flash_data
            where ext_id < %(ext_id)s;
        """

        params = {
            'ext_id': ext_id,
        }

        server_obj.make_query(cr, uid, server_id, sql, params=params, select=False, commit=True)

    def create_po(self, cr, uid, ids, context=None):
        po_obj = self.pool.get('purchase.order')
        partner_obj = self.pool.get("res.partner")

        conf_obj = self.pool.get('ir.config_parameter')

        # Getting supplier data
        supplier_ref = conf_obj.get_param(cr, uid, 'supplier_ref', default=False)
        if not supplier_ref:
            raise osv.except_osv('Error', 'Invalid system parameter \'supplier_ref\'.')
        supplier_id = partner_obj.search(cr, uid, [('ref', '=', supplier_ref), ('supplier', '=', True)])
        if not supplier_id:
            raise osv.except_osv('Error', 'Can\'t find supplier by system parameter \'supplier_ref\'.')
        supplier_id = supplier_id[0]
        supplier_address = partner_obj.address_get(cr, uid, [supplier_id], ['default', 'contact'])

        assert len(ids) == 1, 'Create PO may only be done for one order at a time'

        builder_id = ids[0]
        builder = self.browse(cr, uid, builder_id)

        if not (builder.sale_id and builder.sale_id.id or False):
            raise osv.except_osv('Error', 'Sale order is not created yet.')

        if (builder.po_id and builder.po_id or False):
            raise osv.except_osv('Error', 'PO order is already created.')

        location_id = builder.warehouse_id.lot_input_id and builder.warehouse_id.lot_input_id.id or False
        if not location_id:
            location_id = builder.partner_id.property_stock_customer.id

        new_po_vals = {
            "name": "PO" + builder.po_number,
            "date_order": builder.create_date,
            "partner_address_id": supplier_address.get('contact', False) or supplier_address.get('default', False),
            "warehouse_id": builder.warehouse_id.id,
            "location_id": location_id,
            "invoice_method": "order",
            "partner_ref": supplier_ref,
            "partner_id": supplier_id,
            "reserved": False,
            "pricelist_id": builder.partner_id.property_product_pricelist_purchase.id,
        }

        new_po_id = po_obj.create(cr, uid, new_po_vals)
        if not new_po_id:
            raise osv.except_osv('Warning', 'PO was not created.')

        builder.write({'po_id': new_po_id})
        line_defaults = {
            'create_uid': uid,
            'state': 'draft',
            'order_id': new_po_id,
            'partner_id': supplier_id,
            'status': 'Repair',
            'company_id': builder.sale_id.company_id.id or False,
        }

        default_fields = line_defaults.keys()
        default_fields_value = ['%({key})s as {key}'.format(key=key) for key in default_fields]

        prepare_lines = """
            insert into purchase_order_line (
                {default_fields}
                , create_date
                , date_planned
                , product_uom
                , product_qty
                , name
                , product_id
                , size_id
            )
            select
                {default_fields_value}
                , (now() at time zone 'UTC') as create_date
                , (now() at time zone 'UTC') as date_planned
                , max(ol.product_uom) as product_uom
                , sum(ol.product_uom_qty) * coalesce(psc.qty, 1)
                , max(ol.name)
                , coalesce(psc.cmp_product_id, ol.product_id) as product_id
                , case
                    when psc.id is not null and cm_pc.sizeable is not true then null
                    else ol.size_id
                  end
            from sale_order_line ol
                left join product_product pp on ol.product_id = pp.id
                left join product_set_components psc on pp.id = psc.parent_product_id
                left join product_product cm_pp  on psc.cmp_product_id = cm_pp.id
                left join product_template cm_pt on cm_pp.product_tmpl_id = cm_pt.id
                left join product_category cm_pc on cm_pt.categ_id = cm_pc.id
            where ol.order_id = {sale_id}
            group by ol.product_id, ol.size_id, psc.id, cm_pc.id
            returning id
        """.format(
            sale_id=builder.sale_id.id,
            default_fields=",\n".join(default_fields),
            default_fields_value=",\n".join(default_fields_value)
        )
        cr.execute(prepare_lines, line_defaults)

        # Insert the records corresponding Purchase Order Lines
        purchase_order_line_ids = cr.fetchall()
        customer_id = builder.partner_id.id
        cr.executemany("""
            insert into purchase_order_line_partner
                values (%s, {customer_id})
        """.format(customer_id=customer_id), purchase_order_line_ids)

        return True

    def refresh_on_pc(self, cr, uid, ids, context=None):

        if not ids:
            raise osv.except_osv('ERROR !', 'You should select one PO#')

        if isinstance(ids, (int, long)):
            ids = [ids]

        message = "Done"

        req_obj = catalog_request.Request(cr, uid, self.pool)
        try:
            for flash in self.read(cr, uid, ids, ['po_id']):
                if not flash['po_id']:
                    continue

                raw_response = req_obj.send('api/purchaseOrders/import/' + flash['po_id'][1])
                response = raw_response.read()
                print "Update {po}\nResponse: {res}".format(po=flash['po_id'][1], res=response)

        except Exception:
            message = "Fail, can't update PO"

        return {
            'warning': {
                'title': 'Info!',
                'message': message
            }
        }

flash_from_stock()
