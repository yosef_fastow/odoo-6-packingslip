from osv import fields, osv
import re


class stock_location(osv.osv):
    _inherit = 'stock.location'

    def get_warehouse(self, cr, uid, loc_id, context=None):
        wh_id = False
        try:
            wh_ids = self.pool.get('stock.warehouse').search(cr, uid, [('lot_stock_id', '=', loc_id)])
            if len(wh_ids) > 0:
                wh_id = wh_ids[0]
            else:
                loc = self.browse(cr, uid, loc_id)
                if loc.location_id:
                    wh_id = self.get_warehouse(cr, uid, loc.location_id.id)
        except:
            wh_id = False

        return wh_id

    def _warehouse(self, cr, uid, ids, name, args, context=None):
        res = {}
        for loc_id in ids:
            res[loc_id] = self.get_warehouse(cr, uid, loc_id) or None
        return res

    def _auto_init(self, cr, context=None):
        super(stock_location, self)._auto_init(cr, context)
        cr.execute('SELECT indexname FROM pg_indexes WHERE indexname = \'stock_location_warehouse_id\'')
        if not cr.fetchone():
            cr.execute('CREATE INDEX stock_location_warehouse_id ON stock_location (warehouse_id)')

    _columns = {
        'warehouse_id': fields.function(_warehouse, string="Warehouse", type='many2one', relation='stock.warehouse', method=True, store={
            'stock.location': (lambda self, cr, uid, ids, c={}: self.search(cr, uid, [('id', 'child_of', ids)], context=c), ['location_id', 'direct_warehouse_ids'], 10),
        }),
        'direct_warehouse_ids': fields.one2many('stock.warehouse', 'lot_stock_id', 'Warehouses On This Stock'),
    }

    def get_wh_loc(self, cr, uid, loc_id, context=None):
        wh = None
        locs = None
        if loc_id:
            source = self.browse(cr, uid, loc_id)
            child_loc_ids = self.search(cr, uid, [('location_id', 'child_of', source.id), ('id', '!=', source.id)])
            if child_loc_ids:
                locs = [x['name'] for x in self.read(cr, uid, child_loc_ids, ['name'])]
            else:
                locs = [source.name]
            if not source.warehouse_id:
                self.clear_caches()
                self.recalculate_stored_fields(cr, uid, [loc_id])
                source = self.browse(cr, uid, loc_id)

            if source.warehouse_id:
                wh = source.warehouse_id.name

            else:
                p = re.compile(ur'Physical Locations / (\w+) / Stock.*', re.IGNORECASE)
                m = re.match(p, source.complete_name)
                groups = m.groups()
                if groups:
                    wh = groups[0]

        return wh, locs

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):

        if args is None:
            args = []

        new_args = []

        if context is None:
            context = {}

        if context.get('complete_name', False):
            for i, x in enumerate(args):
                if x and isinstance(x, (list, tuple)) and x[0] == 'name':
                    new_args = args[:i] + ['|', ('complete_name', x[1], x[2])] + args[i:]
                    break
        if new_args:
            args = new_args

        return super(stock_location, self).search(
            cr, uid,
            args=args, offset=offset, limit=limit, order=order,
            context=context, count=count
        )


stock_location()
