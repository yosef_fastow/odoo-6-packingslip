# -*- coding: utf-8 -*-

import netsvc
from osv import fields, osv
from datetime import datetime, timedelta
from dateutil import parser
import socket
import inspect
import logging
import time
import re
import os
import base64
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from openerp.addons.pf_utils.utils.backup_file import backup_file_open
from openerp.addons.pf_utils.utils.backuplocalfileutils import get_list_files
from json import (
    dumps as json_dumps,
    loads as json_loads
)
from openerp.addons.delmar_pricing.stock_product_pricelist import pricelist_fn as price_list_field_name
from openerp.osv.orm import browse_null
import requests
from openerp.addons.pf_utils.utils.re_utils import str2float
import csv
# import API's
from api.aafes_ds import AAFESDSApiClient
from api.aafes_s2s import AAFESS2SApiClient
from api.elmain import ElMainApiClient
from api.amazon import AmazonApiClient
from api.amazon_retail import AmazonRetailApiClient
from api.bealls import BeallsApiClient
from api.bluefly import BlueflyApiClient
from api.bluefly_rest import BlueflyRESTApiClient
from api.bluefly_txt import BlueflyTxtApiClient
from api.bluestem import BluestemApiClient
from api.cat import CatApiClient
from api.commercehub import CommercehubApiClient
from api.commercehub_edi import CommercehubApiEdiClient
from api.daily_steals import DailyStealsApiClient
from api.ebay import EbayApiClient
from api.gdm import GdmApiClient
from api.hautelook import HautelookApiClient
from api.hbc import HBCApiClient
from api.hbc_bulk import HBCBulkApiClient
from api.helzberg import HelzbergApiClient
from api.hsn import HsnApiClient
from api.ice import IceApiClient
from api.ice2 import IceApi2Client
from api.staples import StaplesApiClient
from api.ice_au import IceAUApiClient
from api.ice_au_google import IceAUGoogleApiClient
from api.import_orders import ImportOrdersApiClient
from api.ivory import IvoryApiClient
from api.jeb import JebApiClient
from api.jomashop import JomashopApiClient
from api.jomashop2 import JomashopApiClient2
from api.jomashop3 import Jomashop3RESTApiClient
from api.jtv import JTVApiClient
from api.magento import MagentoApiClient
from api.mjb import MjbApiClient
from api.myhabit import MyHabitApiClient
from api.netjewels import NetJewelsApiClient
from api.newegg import NewEggApiClient
from api.abstract_apiclient import AbsApiClient, InvoiceMessageManager
from api.olve import OlveApiClient
from api.overstock import OverstockApiClient
from api.overstock_sofs import OverstockSOFSApiClient
from api.pagoda import PagodaApiClient
from api.people import PeopleApiClient
from api.pii import PiiApiClient
from api.reeds import ReedsApiClient
from api.samuels import SamuelsApiClient
from api.shop import ShopApiClient
from api.shopify import ShopifyApiClient
from api.smb import SmbApiClient
from api.ssm import SsmApiClient
from api.sterling import SterlingApiClient
from api.target import TargetApiClient
from api.tsc import TSCApiClient
from api.walmart import WalmartApiClient
from api.walmartcanada import WalmartCanadaApiClient
from api.wayfair import WayfairApiClient
from api.vendornet import VendornetApiClient
from api.ch_checker import CommerceHubRemoteAPIClient
from api.sams import SamsApiClient
from api.jet import JetApiClient
from api.govx import GovXApiClient
from api.shoebuy import ShoebuyApiClient
from api.ross import RossSimonsApiClient
from api.generic_ftp_inv import GenericFtpInvApiClient
from api.temp_brand import TempBrandApiClient
from openerp import SUPERUSER_ID
from api.steinmart import SteinmartApiClient
from api.balfour import BalfourApiClient
from api.direct_buy import DirectBuyApiClient
from api.boscov import BoscovApiClient
from api.saks import SaksApiClient
from api.kohls import KohlsApiClient
from api.saks import SaksApiClient
from api.bonton import BontonApiClient
from api.saks_radial import SaksRadialApiClient
from api.groupon import GrouponApiClient
from api.rhd import RhdApiClient

import copy
from openerp.addons.sale_integration.lib.product.product_factory import ProductFactory
from openerp.addons.import_fields.lib.retail_price import RetailPrice

logger = logging.getLogger('sale.integration')


def send_mails_from_result(function_to_decorate):
    def wrapper(*args, **kwargs):
        _args = list(args)
        self = _args[0]
        cr = _args[1]
        uid = _args[2]
        return_result = True
        result = function_to_decorate(*args, **kwargs)
        if isinstance(result, dict):
            settings = result.get('settings', None)
            if (result.get('object')):
                if(hasattr(result['object'], '_mail_messages')):
                    if(result['object']._mail_messages):
                        if (self and cr and uid):
                            self.integrationApiSendEmail(cr, uid, result['object']._mail_messages, settings)
            if (result.get('return_result', -1) != -1):
                return_result = result['return_result']
            return return_result
        else:
            return result
    return wrapper


def get_mapping_apis(mode):
    mapping_apis = {
        'aafes_ds': ('AAFES DS', AAFESDSApiClient),
        'aafes_s2s': ('AAFES S2S', AAFESS2SApiClient),
        'elmain': ('11Main', ElMainApiClient),
        'amazon': ('Amazon', AmazonApiClient),
        'amazon_retail': ('Amazon Retail', AmazonRetailApiClient),
        'bealls': ("Beall's", BeallsApiClient),
        'bluefly': ('Bluefly', BlueflyApiClient),
        'bluefly_rest': ('BlueflyRest', BlueflyRESTApiClient),
        'bluefly_txt': ('BlueflyTxt', BlueflyTxtApiClient),
        'bluestem': ('Bluestem', BluestemApiClient),
        'cat': ('Catherine Malandrino', CatApiClient),
        'commercehub': ('CommerceHub', CommercehubApiClient),
        'commercehub_edi': ('CommerceHubEdi', CommercehubApiEdiClient),
        'daily_steals': ('Daily Steals', DailyStealsApiClient),
        'direct_buy': ('Direct Buy', DirectBuyApiClient),
        'ebay': ('eBay', EbayApiClient),
        'generic_ftp_inv': ('Generic FTP INV', GenericFtpInvApiClient),
        'gdm': ('GDM', GdmApiClient),
        'govx': ('GovX', GovXApiClient),
        'hautelook': ('HautelookDSCO', HautelookApiClient),
        'hbc': ("Hudson’s Bay Company", HBCApiClient),
        'hbc_bulk': ("HBC Bulk", HBCBulkApiClient),
        'helzberg': ("Helzberg", HelzbergApiClient),
        'hsn': ('Home Shopping Network', HsnApiClient),
        'ice': ('Ice', IceApiClient),
        'ice2': ('Ice2', IceApi2Client),
        'ice_au': ('Ice AU (OLD)', IceAUApiClient),
        'ice_au_google': ('ICE AU Google (NEW)', IceAUGoogleApiClient),
        'import_orders': ('Import Orders', ImportOrdersApiClient),
        'ivory': ('Ivory', IvoryApiClient),
        'jeb': ('Jewelry Basket', JebApiClient),
        'jet': ('Jet Partner', JetApiClient),
        'jomashop': ('Jomashop', JomashopApiClient),
        'jomashop2': ('Jomashop2', JomashopApiClient2),
        'jomashop3': ('Jomashop3', Jomashop3RESTApiClient),
        'jtv': ('JTV', JTVApiClient),
        'magento': ('Magento', MagentoApiClient),
        'mjb': ('MyJewelryBox', MjbApiClient),
        'myhabit': ('My Habit', MyHabitApiClient),
        'newegg': ('NewEgg', NewEggApiClient),
        'netjewels': ('Net Jewels', NetJewelsApiClient),
        'no_api': ('Not Use API', AbsApiClient),
        'olve': ('Olve', OlveApiClient),
        'overstock': ('Overstock', OverstockApiClient),
        'overstock_sofs': ('Overstock SOFS', OverstockSOFSApiClient),
        'pagoda': ('Pagoda', PagodaApiClient),
        'people': ('People', PeopleApiClient),
        'pii': ('PI Incentives', PiiApiClient),
        'reeds': ('Reeds Jewelers', ReedsApiClient),
        'samuels': ('Samuels', SamuelsApiClient),
        'sams': ('Sams Club', SamsApiClient),
        'shop': ('Shop', ShopApiClient),
        'shopify': ('Shopify', ShopifyApiClient),
        'shoebuy': ('Shoebuy', ShoebuyApiClient),
        'staples': ('Staples', StaplesApiClient),
        'ross': ('Ross Simons', RossSimonsApiClient),
        'smb': ('Smart Bargains', SmbApiClient),
        'ssm': ('Sears market place', SsmApiClient),
        'sterling': ('Sterling', SterlingApiClient),
        'target': ('Target', TargetApiClient),
        'temp_brand': ('Temp Brand', TempBrandApiClient),
        'tsc': ('The Shopping Channel', TSCApiClient),
        'walmart': ('Walmart', WalmartApiClient),
        'walmartcanada': ('Walmart Canada', WalmartCanadaApiClient),
        'wayfair': ('Wayfair', WayfairApiClient),
        'vendornet': ('Vendornet', VendornetApiClient),
        'steinmart': ('Steinmart', SteinmartApiClient),
        'balfour': ('Balfour', BalfourApiClient),
        'boscov': ('Boscov', BoscovApiClient),
        'kohls': ('Kohls', KohlsApiClient),
        'saks': ('Saks', SaksApiClient),
        'saks_radial': ('SaksRadial', SaksRadialApiClient),
        'bonton': ('Bonton', BontonApiClient),
        'groupon': ('Groupon', GrouponApiClient),
        'rhd': ('Roger and Holland', RhdApiClient),
    }
    result = None
    if mode == 'tuple':
        result = [(key, value[0]) for key, value in mapping_apis.iteritems()]
    elif mode == 'classes':
        result = {key: value[1] for key, value in mapping_apis.iteritems()}
    else:
        raise Exception('Unknown mode: {}'.format(mode))
    return result


def _get_type_api(self, cr, uid, context=None):
    apis = get_mapping_apis('tuple')
    return sorted(apis, key=lambda r: r[1])


def _get_test_method(self, cr, uid, context=None):
    return (
        ('integrationApiLoadOrders', 'LoadOrders'),
        ('integrationApiLoadOrdersFBA', 'LoadOrdersFBA'),
        ('integrationApiCreateOrders', 'CreateOrders'),
        ('integrationApiGetOrdersInLocalFolder', 'GetOrdersInFolder'),
        ('integrationApiGetCancelOrdersInLocalFolder', 'GetCancelOrdersInFolder'),
        ('integrationApiGetReturns', 'GetReturns'),
        ('integrationApiUpdateQTY', 'UpdateQTY'),
        ('integrationApiSyncAllProductAmazon', 'Sync All Product Amazon'),
        ('integrationApiGetRMA', 'Get RMA'),
        ('integrationApiProcessCancelXML', 'Process Cancel XML'),
        ('TestAmazonRetail', 'Test AmazonRetail'),
        ('integrationApiGetGIftDR', 'Get Generic Invoices for the Date Range'),
        ('SendOneEmailInvoiceForAllDoneOrders', 'Send One Email Invoice For All Done Orders'),
        ('integrationApiReloadOrders', 'ReloadOrders'),
        ('fillImageURLsToCF', 'fillImageURLsToCF'),
        ('integrationApiUploadFeed', 'UploadFeed'),
    )


def _get_si_type(self, cr, uid, context=None):
    return(
        ('main', 'Main'),
        ('virtual', 'Virtual'),
        ('other', 'Other'),
        ('edi', 'EDI'),
    )


class sale_integration(osv.osv):
    _name = "sale.integration"
    _columns = {
        'name': fields.char('Name', size=128),
        'url': fields.char('Url', size=128),
        'merchantKey': fields.char('Merchant Key', size=128),
        'authenticationKey': fields.char('Authentication Key', size=128),
        'aws_access_key_id': fields.char('AWS Access Key ID', size=128),
        'merchant_id': fields.char('Merchant ID:', size=128),
        'marketplace_id': fields.char('Marketplace ID', size=128),
        'secret_key': fields.char('Secret Key', size=128),
        'auth_token': fields.char('MWS Auth Token', size=128),
        'customer_ids': fields.many2many('res.partner', 'sale_integration_customer', 'integration_id', 'customer_id', 'Customer'),
        'customer_field_ids': fields.many2many('product.multi.customer.fields.name', 'sale_integration_customer_field', 'integration_id', 'customer_field_id', 'Customer Field'),
        'type_api': fields.selection(_get_type_api, 'Api services'),
        'tag': fields.many2one('tagging.tags', 'Tag'),
        'syncTag': fields.many2one('tagging.tags', 'Sync Tag'),
        'commercehub_login': fields.char('Login', size=128),
        'commercehub_pass': fields.char('Password', size=128),
        'gnupghome': fields.char('Gnupg home', size=128),
        'passphrase': fields.char('Passphrase', size=128),
        'customer_root_dir': fields.char('CustomerRoot directory', size=128),
        'test_method':  fields.selection(_get_test_method, 'Test Method'),
        'test_xml': fields.text('Test XML'),
        'process_limit': fields.integer('Process limit'),
        'select_limit': fields.integer('Select Limit'),
        'mode': fields.char('Mode', size=256, ),
        'process_canceled': fields.boolean('Process Canceled'),
        'load_twice_confirm': fields.boolean('Load Twice Confirm'),
        'update_sec_locations': fields.boolean('Update QTY with secondary location'),
        'location_ids': fields.many2many('stock.location', 'sale_integration_stock_location', 'integration_id', 'location_id', 'Locations'),
        'customer_field': fields.char('Name customer field', size=128),
        'newegg_sellerid': fields.char('SellerId', size=128),
        'newegg_authorization': fields.char('Authorization', size=128),
        'newegg_secret_key': fields.char('SecretKey', size=128),
        'vendor_id': fields.char('Vendor Id', size=128),  # Walmart
        'ftp_settings_ids': fields.many2many('multi.ftp.settings', 'sale_integration_ftp_setting', 'integration_id', 'ftp_setting_id', 'Multi Ftp Settings'),
        'http_settings_ids': fields.many2many('multi.http.settings', 'sale_integration_http_setting', 'integration_id', 'http_setting_id', 'Multi HTTP Settings'),
        'last_id': fields.integer('Last Inventory ID'),
        'inventory_product_usage': fields.boolean('Send inventory update by product usage'),
        'processQueue': fields.boolean('Process Queue'),  # Stertling
        'inventory_setting': fields.char('Inventory Setting', size=128),
        'history_sent_inventory': fields.one2many('history.sent.inventory', 'type_api_id', 'History'),  # History sent inventory
        'backup_local_path': fields.char("""Local path to save backup's files""", size=128),
        'input_local_path': fields.char('Local path to save input files', size=128),
        'shop_supplier_id': fields.char('Supplier Id', size=128, required=False),
        'shop_store_name': fields.char('Store name', size=128, required=False),
        'settings': fields.one2many('sale.integration.settings', 'type_api_id', 'Settings'),
        'use_ftp': fields.boolean('Use FTP'),
        'use_http': fields.boolean('Use HTTP'),
        'auto_confirm': fields.boolean('Auto Confirm Sale Order'),
        'flash': fields.boolean('Use Orders on Flash Screen'),
        'mako_templates': fields.one2many('stock.mako.templates', 'type_api_id', 'Mako Templates', ),
        'use_mako_templates': fields.boolean('Use Mako Templates', ),
        'import_settings': fields.one2many('stock.import.settings', 'type_api_id', 'Import Settings', ),
        'from_size': fields.many2one('ring.size', 'From Ring Size'),
        'to_size': fields.many2one('ring.size', 'To Ring Size'),
        'si_type': fields.selection(_get_si_type, 'Sale Integration Type', ),
        'default_country': fields.many2one('res.country', 'Country by default', ),
        'validate_tracking': fields.boolean('Validate Tracking', ),
        'mi_from_date': fields.datetime('Manual Invoice From Date', ),
        'mi_to_date': fields.datetime('Manual Invoice To Date', ),
        'mi_filename': fields.char('MI Filename', size=128, ),
        'mi_file': fields.binary('Manual Invoice File', ),
        'old_get_external_customer_id': fields.boolean('Use old method for getting customer', ),
        'deny_partial_inventory': fields.boolean('Deny partial inventory'),
        'send_functional_acknowledgement': fields.boolean('Send Functional Acknowledgement'),
        'reload_date_start': fields.date('Date Start', ),
        'reload_date_end': fields.date('Date End', ),
        'mi_send_by_email': fields.boolean('Send invoice to Email'),
        'use_discounts': fields.boolean('Use Discounts in Inventory Feed'),
        'use_set_size_tolerance': fields.boolean('Use Sets Size tolerance'),
        'order_list': fields.text('Order ids list'),
        'use_mail_instead_of_ftp': fields.boolean('Use Email Instead of FTP', ),
        'confirm_all_orders': fields.boolean('Confirm all orders', help='System will also send confirmation for orders created by Generic Import or manually.', ),
        # DELMAR-170
        'generate_readable_inventory': fields.boolean('Generate Readable inventory file'),
    }
    _order = "name ASC"

    _defaults = {
        'last_id': 0,
        'inventory_product_usage': False,
        'update_sec_locations': 1,
        'type_api': 'no_api',
        'use_ftp': False,
        'auto_confirm': True,
        'flash': False,
        'use_mako_templates': False,
        'si_type': 'main',
        'validate_tracking': False,
        'old_get_external_customer_id': True,
        'send_functional_acknowledgement': False,
        'mi_send_by_email': False,
        'use_mail_instead_of_ftp': False,
        'use_discounts': False,
        'use_set_size_tolerance': True,
        # DELMAR-170
        'generate_readable_inventory': False,
    }

    is_test = False

    def init(self, cr):
        # Create base table for si_inv
        cr.execute("""
            CREATE OR REPLACE FUNCTION create_si_last_inventory()
            RETURNS VARCHAR AS $$
            BEGIN
                IF NOT EXISTS(
                    SELECT table_name
                    from information_schema.tables
                    where 1=1
                            and table_type = 'BASE TABLE'
                            and table_name = 'si_last_inv'
                )
                THEN

                    CREATE TABLE si_last_inv(
                        id BIGSERIAL PRIMARY KEY,
                        product_id integer,
                        default_code varchar(50),
                        size_id integer,
                        size varchar(50),
                        qty integer,
                        phantom_inventory integer,
                        force_zero integer,
                        actual_qty integer,
                        qty_tolerance integer,
                        send boolean,
                        create_date timestamp,
                        by_product_usage boolean default False
                    );

                    CREATE INDEX si_last_inv_create_date_idx ON public.si_last_inv (create_date);

                    CREATE INDEX si_last_inv_default_code_idx ON public.si_last_inv (default_code,size);

                    CREATE INDEX si_last_inv_product_id_idx ON public.si_last_inv (product_id,size_id);

                END IF;
            RETURN 'OK';
            END;
            $$
            LANGUAGE plpgsql;

            SELECT create_si_last_inventory();
        """)

        # Create functions for partitioning of inventory history
        cr.execute("""
            CREATE OR REPLACE FUNCTION si_last_inv_trigger()
            RETURNS TRIGGER AS $$
            DECLARE
                range VARCHAR;
                master_table varchar;
                section_table varchar;
            BEGIN
                range := to_char(NEW.create_date, 'YYYY_MM');
                master_table := TG_TABLE_NAME;
                section_table := concat(master_table, '_', range);
                execute 'create table if not exists ' || quote_ident(section_table) || ' (LIKE ' || quote_ident(master_table) || ' INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES, check (to_char(create_date, ''YYYY_MM'') = ''' || range || ''')) inherits ( ' || quote_ident(master_table) || ');';
                execute format('INSERT INTO %I VALUES ($1.*)', section_table) USING NEW;
                RETURN NULL;
            END;
            $$
            LANGUAGE plpgsql;


            CREATE OR REPLACE FUNCTION drop_trigger_for_si_last_inventory(master_table varchar)
            RETURNS VARCHAR AS
            $BODY$
            DECLARE
                trigger_name VARCHAR;
            BEGIN
                trigger_name := concat('insert_', master_table, '_trigger');
                execute format('DROP TRIGGER IF EXISTS %I ON %I;', trigger_name, master_table);
            RETURN 'OK';
            END;
            $BODY$ LANGUAGE plpgsql;


            CREATE OR REPLACE FUNCTION create_trigger_for_si_last_inventory(master_table varchar)
            RETURNS VARCHAR AS
            $BODY$
            DECLARE
                trigger_name VARCHAR;
            BEGIN
                trigger_name := concat('insert_', master_table, '_trigger');
                execute format('CREATE TRIGGER  %I BEFORE INSERT ON %I FOR EACH ROW EXECUTE PROCEDURE si_last_inv_trigger();', trigger_name, master_table);
            RETURN 'OK';
            END;
            $BODY$ LANGUAGE plpgsql;


            CREATE OR REPLACE FUNCTION recreate_si_last_inventory(
                master_table VARCHAR,
                drop_master_tables BOOLEAN DEFAULT FALSE,
                drop_section_tables BOOLEAN DEFAULT FALSE
            )
            RETURNS VARCHAR AS
            $BODY$
            DECLARE
                section_table varchar;
            BEGIN
                IF (drop_section_tables = true)
                THEN
                    FOR section_table in
                        SELECT table_name
                        from information_schema.tables
                        where 1=1
                            and table_type = 'BASE TABLE'
                            and table_name ~ concat(master_table, '_\d\d\d\d_\d\d$')
                    LOOP
                        execute format('DROP TABLE  %I ;', section_table);
                    END LOOP;
                END IF;

                IF (drop_master_tables = true)
                THEN
                    execute format('DROP TABLE  %I ;', master_table);
                    execute format('CREATE TABLE  %I  (LIKE si_last_inv INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES);', master_table);
                END IF;
            RETURN 'OK';
            END;
            $BODY$ LANGUAGE plpgsql;


            CREATE OR REPLACE FUNCTION update_all_triggers_for_si_last_inventory(
                drop_master_tables boolean default false,
                drop_section_tables boolean default false
            )
            RETURNS VARCHAR AS
            $BODY$
            DECLARE
                master_table VARCHAR;
            BEGIN
                FOR master_table in
                    SELECT table_name
                    from information_schema.tables
                    where 1=1
                        and table_type = 'BASE TABLE'
                        and table_name like 'si_last_inv_%'
                        and table_name  !~ '_\d\d\d\d_\d\d$'
                LOOP
                    execute 'select drop_trigger_for_si_last_inventory(''' || master_table || ''');';
                    execute 'select recreate_si_last_inventory(''' || master_table || ''', drop_master_tables := ' || drop_master_tables || ', drop_section_tables := ' || drop_section_tables || ');';
                    execute 'select create_trigger_for_si_last_inventory(''' || master_table || ''');';
                END LOOP;
            RETURN 'OK';
            END;
            $BODY$ LANGUAGE plpgsql;

        """)

        # Force allocate
        cr.execute("""
            CREATE OR REPLACE FUNCTION force_allocate_flash(picking_id integer)
            RETURNS VARCHAR AS
            $BODY$
            BEGIN
                IF EXISTS (
                    SELECT 1
                    FROM pg_catalog.pg_class
                    WHERE relkind = 'r'
                        AND relname = '_tmp_flash_stock'
                        AND pg_catalog.pg_table_is_visible(oid)
                    LIMIT 1
                )
                THEN
                    UPDATE stock_move sm
                    SET location_id = _new.location_id,
                        old_location_id = _new.old_location_id,
                        bin = _new.bin,
                        invredid = _new.invredid,
                        state = 'assigned'
                    FROM (
                        SELECT
                            sm.id,
                            sl.id as location_id,
                            sm.location_id as old_location_id,
                            ts.bin as bin,
                            ts.invredid as invredid,
                            (select id from stock_bin where name = ts.bin limit 1) as bin_id
                        FROM stock_move sm
                            left join sale_order_line ol on sm.sale_line_id = ol.id
                            left join _tmp_flash_stock ts on ts.id = ol.external_customer_line_id
                            left join stock_location sl on sl.complete_name ilike concat('Physical Locations / ', ts.warehouse, ' / Stock / ', ts.stockid)
                        WHERE sm.picking_id = $1
                            and ts.id is not null
                    ) _new
                    WHERE _new.id = sm.id;

                END IF;
            RETURN 'OK';
            END;
            $BODY$ LANGUAGE plpgsql;
        """)

    def get_SETTINGS_FIELDS(self, cr, uid, type_api, name=None):
        setting_fields = None
        api_class = get_mapping_apis('classes').get(type_api, False)
        if api_class:
            try:
                setting_fields = api_class({}).setting_fields
            except Exception:
                if name is not None:
                    (settings, api) = self.getApi(cr, uid, name)
                    if api:
                        setting_fields = api.setting_fields
                    else:
                        setting_fields = False
        else:
            setting_fields = False

        if setting_fields:
            if not isinstance(setting_fields[0], (tuple, list)):
                setting_fields = (setting_fields,)

        return setting_fields

    def get_DEFAULT_VALUES(self, type_api):
        default_values = None
        api_class = get_mapping_apis('classes').get(type_api, False)
        if api_class:
            try:
                default_values = api_class({}).default_values
            except Exception:
                default_values = False
        else:
            default_values = False
        return default_values

    def create(self, cr, uid, vals, context=None):
        if vals and vals.get('type_api') != 'no_api':
            type_api = vals.get('type_api')
            if type_api:
                api_settings = self.get_SETTINGS_FIELDS(cr, uid, type_api)
                if api_settings:
                    vals['settings'] = []
                    if not isinstance(api_settings, (list, tuple)):
                        api_settings = [api_settings]
                    for setting in api_settings:
                        vals['settings'].append((0, 0, {'name': setting[0], 'label': setting[1], 'value': setting[2]}))
                default_values = self.get_DEFAULT_VALUES(type_api)
                if default_values:
                    vals.update(default_values)

        return super(sale_integration, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        res = super(sale_integration, self).write(cr, uid, ids, vals, context=context)
        if ids and isinstance(ids, (list, tuple)) and len(ids) == 1:
            id_ = ids[0]

            # FIXME: Need remove old SETTINGS_FIELDS if type_api changed

            extra_vals = {}
            row = self.browse(cr, uid, id_)
            type_api = row.type_api
            name = row.name
            if type_api != 'no_api':
                api_settings = self.get_SETTINGS_FIELDS(cr, uid, type_api, name)
                if api_settings:
                    available_settings = [x.name for x in row.settings if x.name]
                    if api_settings:
                        extra_vals['settings'] = []
                        if not isinstance(api_settings, (list, tuple)):
                            api_settings = [api_settings]

                        for setting in api_settings:
                            if setting[0] not in available_settings:
                                extra_vals['settings'].append((0, 0, {'name': setting[0], 'label': setting[1], 'value': setting[2]}))
            if extra_vals:
                super(sale_integration, self).write(cr, uid, ids, extra_vals, context=context)

        return res

    def test_cron(self, cr, uid, ids, context):
        self.is_test = True
        sale_int = self.browse(cr, uid, ids[0])
        test_method = sale_int.test_method

        method = getattr(self, test_method, None)

        if(method):
            arg_spec = inspect.getargspec(method)
            if 'context' in arg_spec[0]:
                method(cr, uid, sale_int.name, context=context)
            else:
                method(cr, uid, sale_int.name)

        return True

    def search_type_api_id_by_customer(self, cr, uid, _ext_cust_id=None, only_main=True, _ref=False):
        result = None
        if(_ext_cust_id is not None):
            if(only_main):
                sale_integration_ids = self.search(cr, uid, [('si_type', 'in', ['main', 'edi'])])
            else:
                si_types = [x[0] for x in _get_si_type(self, cr, uid)]
                sale_integration_ids = self.search(cr, uid, [('si_type', 'in', si_types)])
            if(len(sale_integration_ids) > 0):
                for si_id in sale_integration_ids:
                    curr_obj = self.browse(cr, uid, si_id)
                    for cust_obj in curr_obj.customer_ids:
                        if(cust_obj.external_customer_id == _ext_cust_id):
                            if _ref and cust_obj.ref != _ref:
                                continue
                            result = curr_obj.id
                            break
            else:
                result = None
        else:
            result = None
        return result

    def search_main_si_by_partner(self, cr, uid, partner_id):
        cr.execute('''SELECT
                        si.id
                    FROM
                        sale_integration_customer AS sic
                    LEFT JOIN
                        sale_integration AS si
                            ON sic.integration_id = si.id
                    WHERE 1=1
                        AND si.si_type IN ('main', 'edi')
                        AND sic.customer_id = %s''', (partner_id, ))
        si_ids = [x[0] for x in cr.fetchall()]
        return si_ids and si_ids[0] or None

    # Get price from acc_price table
    def get_acc_price_price(self, cr, uid, product_id, partner):
        res = 0.0
        default_code = self.pool.get('product.product').read(cr, uid, product_id, ['default_code']).get('default_code', False)
        acc_price_code = partner and (partner.acc_price_code or partner.ref)
        if acc_price_code and default_code:
            sql = """SELECT "PRICE"
                FROM acc_price
                WHERE "ITEM_" = '%s'
                    AND "CODE" = '%s'
                """ % (default_code, acc_price_code)
            cr.execute(sql)
            sql_res = cr.fetchone()
            res = sql_res and sql_res[0] and float(sql_res[0]) or 0.0
        return res

    # Get price from MS CustomerPrice DB
    def get_ms_customer_price(self, cr, uid, product_id, partner):
        res = False
        prod_obj = self.pool.get('product.product')
        server_obj = self.pool.get('fetchdb.server')
        prod_data = prod_obj.read(cr, uid, product_id, ['default_code'])
        default_code = prod_data.get('default_code', False)
        server_id = server_obj.get_price_servers(cr, uid, single=True)
        if not server_id:
            return res
        query = """SELECT top 1 Price FROM CustomerPrice
            WHERE DelmarID = ?
                AND Customer = ?
            ORDER BY Date DESC
        """
        params = [default_code, partner.ref]
        query_res = server_obj.make_query(
            cr, uid, server_id,
            query, params=params, select=True
        )
        if query_res:
            try:
                cust_price = re.sub(r'[^\d.]', '', query_res[0][0])
                float(cust_price)
                res = cust_price
            except:
                pass
        return res

    def CheckCHOrdersNeededFeeds(self, cr, uid, ch_account='yuri', type_orders='critical', color='red'):
        conf_obj = self.pool.get('ir.config_parameter')
        ch_accounts = conf_obj.get_param(cr, uid, 'sale.integrarion.ch_accounts', False)
        partner_map = conf_obj.get_param(cr, uid, 'sale.integrarion.partner_map', False)
        mapping_type_orders = conf_obj.get_param(cr, uid, 'sale.integrarion.mapping_type_orders', False)
        ch_states_order_without_invoice = [
            'Order Received', 'Order Delivered', 'Fulfillment Update', 'Order Closed']
        ch_states_order_with_invoice = [
            'Order Received', 'Order Delivered', 'Fulfillment Update', 'Order Closed', 'Invoice']
        needed_ivoice_partners = ['drugstore', 'sears', 'bedbath', 'bestbuyca', 'fmj', 'ppower']
        if ch_accounts:
            ch_accounts = json_loads(ch_accounts)
            partner_map = json_loads(partner_map)
            mapping_type_orders = json_loads(mapping_type_orders)
            ch_account_settings = ch_accounts.get(ch_account, False)
            if ch_account_settings:
                ch_settings = {
                    'connection': ch_account_settings,
                    'partner_map': partner_map,
                    'mapping_type_orders': mapping_type_orders,
                }
                api = CommerceHubRemoteAPIClient(ch_settings)
                orders = api.get_orders_needed_feeds(type_orders=type_orders, color=color)
                need_resend_acknowledgement = []
                need_resend_confirm = []
                for external_customer_id, orders_raw in orders.iteritems():
                    for order_name, data in orders_raw.iteritems():
                        states_on_ch = [x['state'] for x in data['states']]
                        all_states = []
                        if external_customer_id in needed_ivoice_partners:
                            all_states = ch_states_order_with_invoice
                        else:
                            all_states = ch_states_order_without_invoice
                        for st in states_on_ch:
                            if st in all_states:
                                all_states.remove(st)
                        if 'Order Delivered' in all_states:
                            sql = '''SELECT
                                        so.id AS id,
                                        so.name AS name
                                    FROM
                                        sale_order AS so
                                    LEFT JOIN
                                        res_partner AS rp
                                            ON so.partner_id = rp.id
                                    WHERE 1=1
                                        AND so.po_number = '{order_name}'
                                        AND rp.external_customer_id = '{external_customer_id}'
                            '''.format(order_name=order_name, external_customer_id=external_customer_id)
                            cr.execute(sql)
                            ids = cr.dictfetchall()
                            if ids:
                                order = ids[0]
                                need_resend_acknowledgement.append(order['name'])
                        if 'Order Closed' in all_states or 'Invoice' in all_states:
                            sql = """SELECT
                                        sp.id AS id,
                                        sp.name AS name
                                    FROM
                                        stock_picking AS sp
                                    LEFT JOIN
                                        sale_order AS so
                                            ON so.id = sp.sale_id
                                    LEFT JOIN
                                        res_partner AS rp
                                            ON sp.real_partner_id = rp.id
                                    WHERE 1=1
                                        AND sp.state = 'done'
                                        AND so.po_number = '{order_name}'
                                        AND rp.external_customer_id = '{external_customer_id}'
                            """.format(order_name=order_name, external_customer_id=external_customer_id)
                            cr.execute(sql)
                            ids = cr.dictfetchall()
                            if ids:
                                order = ids[0]
                                need_resend_confirm.append(order['name'])
                msg = ''
                if need_resend_acknowledgement:
                    msg += 'Need Resend Acknowledgement for : {orders}'.format(
                        orders=','.join([a for a in need_resend_acknowledgement])
                    )
                if need_resend_confirm:
                    msg += 'Need Resend Confirm for : {orders}'.format(
                        orders=','.join([a for a in need_resend_confirm])
                    )
                if msg:
                    mail_message = {
                        'email_from': 'delmar.report@progforce.com',
                        'emailto_list': ['delmar@isddesign.com'],
                        'subject': 'Need resend feeds for CH',
                        'body': msg,
                        'attachments': '',
                    }
                    self.integrationApiSendEmail(cr, uid, mail_message)
        return True

    def fillImageURLsToCF(self, cr, uid, name="", context=None):
        (settings, api) = self.getApi(cr, uid, name)
        if settings:
            tag_id = settings.tag.id
            partner_ids = settings.customer_ids
            if partner_ids:
                pmcfn_ids_sql = '''SELECT
                                        pmcfn.customer_id AS partner_id,
                                        pmcfn.id AS pmcfn_id
                                    FROM
                                        product_multi_customer_fields_name AS pmcfn
                                    LEFT JOIN
                                        product_multi_customer_fields_name_type AS pmcfnt
                                            ON pmcfn.type_id = pmcfnt.id
                                    WHERE 1=1
                                        AND pmcfnt.name = \'image_urls\'
                                        AND pmcfn.customer_id in (%s)'''
                cr.execute(pmcfn_ids_sql, tuple([x.id for x in partner_ids]))
                pmcfn_ids = {a['partner_id']: a['pmcfn_id'] for a in cr.dictfetchall()}
                partner_ids = [partner_id.id for partner_id in partner_ids if partner_id.id in pmcfn_ids]
                full_sql = '                            '
                for partner_id in partner_ids:
                    sql = '''SELECT
                                pp.default_code AS default_code,
                                pmcf.product_id AS product_id,
                                pmcf.id AS pmcf_id,
                                pmcf."value" AS value
                            FROM
                                product_multi_customer_fields AS pmcf
                            LEFT JOIN
                                product_product AS pp
                                    ON pmcf.product_id = pp.id
                            LEFT JOIN
                                taggings_product AS tp
                                    ON pp.id = tp.product_id
                            WHERE 1=1
                                AND pmcf.partner_id = %s
                                and pmcf.field_name_id = %s
                                AND pmcf.size_id IS NULL
                                AND tp.tag_id = %s'''
                    cr.execute(
                        sql, (partner_id, pmcfn_ids[partner_id], tag_id))
                    availaible_in_db = {
                        str(x['default_code']).strip().lower():
                        {
                            'product_id': x['product_id'],
                            'pmcf_id': x['pmcf_id'],
                            'value': x['value']
                        } for x in cr.dictfetchall()
                    }
                    tag_sql = '''SELECT
                                    pp.default_code AS default_code,
                                    pp.id AS product_id
                                FROM
                                    taggings_product AS tp
                                LEFT JOIN
                                    product_product AS pp
                                        ON tp.product_id = pp.id
                                WHERE tag_id = %s'''
                    cr.execute(tag_sql, (tag_id, ))
                    default_codes = {
                        str(dc['default_code']).strip().lower():
                        dc['product_id'] for dc in cr.dictfetchall()
                    }
                    response = requests.post(
                        'http://erp.delmarintl.ca/get_openerp_link/get_images_for_products',
                        data=json_dumps({'default_codes': [x for x, y in default_codes.iteritems()]}),
                        headers={'Content-type': 'application/json', 'Accept': 'text/plain'}
                    )
                    if not response.status_code == requests.codes.ok:
                        response.raise_for_status()
                    image_urls = json_loads(response.text)
                    update_sql = ''
                    for default_code, sub_data in availaible_in_db.iteritems():
                        if (
                            default_code in image_urls and
                            json_dumps(image_urls[default_code]) != sub_data['value']
                        ):
                            sub_update_sql = """UPDATE
                                                    product_multi_customer_fields
                                                SET value = '{value}',
                                                    write_uid = {uid},
                                                    write_date = (now() at time zone 'UTC')
                                                WHERE id = {field_id};\n
                            """.format(
                                uid=uid,
                                value=json_dumps(image_urls[default_code]),
                                field_id=sub_data['pmcf_id']
                            )
                            update_sql += sub_update_sql
                        del image_urls[default_code]
                    full_sql += update_sql
                    insert_sql = ''
                    for default_code, sub_image_urls in image_urls.iteritems():
                        if default_code in default_codes:
                            sub_insert_sql = """INSERT INTO
                                                    product_multi_customer_fields
                                                        (create_uid, create_date, field_name_id, partner_id, product_id, value)
                                                VALUES
                                                    ({uid}, (now() at time zone 'UTC'), {field_name_id}, {partner_id}, {product_id}, '{value}');\n
                            """.format(
                                uid=uid,
                                field_name_id=pmcfn_ids[partner_id],
                                partner_id=partner_id,
                                product_id=default_codes[default_code],
                                value=json_dumps(image_urls[default_code])
                            )
                            insert_sql += sub_insert_sql
                    full_sql += insert_sql
                if full_sql.strip():
                    cr.execute(full_sql)
        return True

    def integrationApiReloadOrders(self, cr, uid, name="", date_start=None, date_end=None, context=None):
        (settings, api) = self.getApi(cr, uid, name)
        date_start = date_start or settings.reload_date_start or False
        date_end = date_end or settings.reload_date_end or False
        if not date_start or not date_end:
            return False
        else:
            api.settings.update({
                'reload_date_start': date_start,
                'reload_date_end': date_end,
            })
            api.reloadOrders()
            log = api.log
            if log and settings:
                self.log(cr, uid, settings, log)
            return True

    def getInvoiceDomain(self, cr, uid, ids=None, partner=None, start_date=None, end_date=None, send_returns=None, context=None):
        domain = []

        if partner:
            domain.append(('real_partner_id', '=', partner.id))

        if ids:
            domain.append(('id', 'in', ids))

        def append_date_domain(field, start_date=None, end_date=None, domain=None):
            if not domain:
                domain = []

            date_domain = []
            if start_date:
                date_domain.append((field, '>=', start_date))
            if end_date:
                date_domain.append((field, '<=', end_date))

            if date_domain:
                lengs = len(date_domain) if domain else len(date_domain) - 1
                domain = ['&'] * lengs + domain + date_domain

            return domain

        done_domain = append_date_domain('date_done', start_date, end_date, [('state', '=', 'done')])
        ret_domain = append_date_domain('ret_date', start_date, end_date, [('state', '=', 'returned')])

        state_domain = done_domain if not send_returns else ['|'] + done_domain + ret_domain

        return domain + state_domain

    # DLMR-556
    def send_reject_824_notification(self, cr, uid, name=None, context=None):
        log_obj = self.pool.get('sale.integration.log')
        (settings, api) = self.getApi(cr, uid, name)
        if not settings or not api:
            return False

        log_search_domain = [
            ('create_date', '>', (datetime.utcnow() - timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')),
            ('type_api', '=', settings.type_api),
            ('debug_information', 'like', '%INVALID_DATA_824%')
        ]
        log_ids = log_obj.search(cr, uid, log_search_domain)
        if log_ids and len(log_ids) > 0:
            for logrec in log_obj.browse(cr, uid, log_ids):
                l0 = logrec.debug_information.split("INVALID_DATA_824:")
                try:
                    trn, err = l0[1].split(":", 1)
                except:
                    pass
                    return True
                # trying to find order
                if trn and err:
                    pick_obj = self.pool.get('stock.picking')

                    pick_ids = pick_obj.search(cr, uid, [('state', 'not in', ('shipped', 'done', 'cancel')), ('tracking_ref', '=', trn.strip())])
                    if pick_ids and len(pick_ids) > 0:
                        pick_data = pick_obj.read(cr, uid, pick_ids, ['name'])
                        message_body = ''
                        for pick in pick_data:
                            message_body = "{}Order {} was rejected with error: {}\n".format(message_body, pick.get('name'), err)

                        message = {
                            'email_from': 'erp.delmar@gmail.com',
                            'email_to': ['avi@delmarintl.ca', 'kim@delmarintl.com'],
                            'subject': "Rejected orders by {}".format(settings.type_api.upper()),
                            'body': message_body,
                            'attachments': None,
                        }
                        self.pool.get('mail.message').schedule_with_attach(
                            cr, uid,
                            message.get('email_from'), message.get('email_to'), message.get('subject'), message.get('body'),
                            subtype='plain', attachments=message.get('attachments'), context=None
                        )

        return True

    @send_mails_from_result
    def SendOneEmailInvoiceForAllDoneOrders(self, cr, uid, name="", days=1, context=None):
        result = {
            'object': None,
            'return_result': False,
            'settings': None,
        }
        log_msg = 'Starting SendOneEmailInvoiceForAllDoneOrders\n'
        (settings, api) = self.getApi(cr, uid, name)
        deliver_obj = self.pool.get("stock.picking")
        try:
            days = int(days)
        except ValueError:
            days = 1
        if(api):
            log_msg += 'Getting API: {0}\n'.format(
                api.__class__.__name__ or name
            )
            _emails_to = False
            _send_returns = False
            customer = False
            if(settings.settings):
                customer = settings.customer_ids[0]
                for item in settings.settings:
                    item_name = str(item.name).strip()
                    if(item_name == 'emails_for_sending_invoice_24_hours'):
                        if(item.value):
                            _emails_to = item.value
                    if item_name == 'send_return_invoice' and (item.value or '').strip().lower() in ['1', 'y', 'true']:
                        _send_returns = True
            log_msg += 'emails_to: {0}\n'.format(_emails_to)
            delivery_ids = []
            if(customer):
                start_date = (datetime.utcnow() - timedelta(days=days)).strftime('%Y-%m-%d %H:%M:%S')
                end_date = (datetime.utcnow() + timedelta(days=days)).strftime('%Y-%m-%d')
                log_msg += 'start_date: {0}, end_date: {1}\n'.format(start_date, end_date)

                condition = self.getInvoiceDomain(
                    cr, uid,
                    partner=customer,
                    start_date=start_date,
                    end_date=end_date,
                    send_returns=_send_returns
                )

                log_msg += 'condition for searching Done/Return orders: {0}\n'.format(condition)
                delivery_ids = deliver_obj.search(cr, uid, condition)
                if (delivery_ids):
                    log_msg += 'Founded orders: {0}\n'.format(','.join([x.name for x in deliver_obj.browse(cr, uid, delivery_ids)]))
            else:
                log_msg += 'Not found customer in Sale Integration\n'
            if(_emails_to and customer and delivery_ids):
                shp_date = datetime.utcnow()
                if (days == 1):
                    subject = "{} Invoice Report for last 24 hours {:%Y-%m-%d %H:%M:%S}".format(customer.name, shp_date)
                else:
                    subject = "{} Invoice Report for last {} days {:%Y-%m-%d %H:%M:%S}".format(customer.name, days, shp_date)
                _settings_invoice = {
                    'cr': cr,
                    'uid': uid,
                    'delivery_ids': delivery_ids,
                    'emails_to': _emails_to,
                    'customer_name': customer.name,
                    'report_service': 'report.stock.picking.invoice',
                    'subject': subject,
                }
                api.generate_invoice_mail_message(_settings_invoice)
                generated_messages = api._mail_messages or []
                log_msg += 'Generated messages: {0}'.format(len(generated_messages))
            else:
                log_msg += '''
                    insufficient data
                    _emails_to: {0}, customer: {1}, delivery_ids: {2}
                '''.format(_emails_to, customer, delivery_ids)
            result.update({'object': api, 'settings': settings, 'return_result': True})
        logger.info(log_msg)
        if (settings):
            self.log(cr, uid, settings, [{'title': 'SendOneEmailInvoiceForAllDoneOrders', 'msg': log_msg}])
        return result

    def integrationApiGetGIftDR(self, cr, uid, name="", context=None):
        (settings, api) = self.getApi(cr, uid, name)
        deliver_obj = self.pool.get("stock.picking")

        if(api):
            _send_returns = False
            customer = False
            if(settings):
                customer = settings.customer_ids[0]
                start_date = settings.mi_from_date or (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
                end_date = settings.mi_to_date or (datetime.now()).strftime('%Y-%m-%d')
                for item in settings.settings:
                    if (item.name or '').strip().lower() == 'send_return_invoice':
                        if (item.value or '').strip().lower() in ['1', 'y', 'true']:
                            _send_returns = True
                            break
            else:
                start_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
                end_date = (datetime.now()).strftime('%Y-%m-%d')
            delivery_ids = []

            domain = []
            if settings.order_list:
                ids_order_list = settings.order_list
                ids_list = ids_order_list.split(',')
                order_ids = []
                for ids in ids_list:
                    try:
                        order_ids.append(int(ids.strip()))
                    except ValueError:
                        pass
                domain = self.getInvoiceDomain(cr, uid, partner=None, ids=order_ids, start_date=settings.mi_from_date, end_date=settings.mi_to_date, send_returns=_send_returns)
            else:
                if(customer):
                    domain = self.getInvoiceDomain(cr, uid, partner=customer, ids=None, start_date=start_date, end_date=end_date, send_returns=_send_returns)

            if domain:
                delivery_ids = deliver_obj.search(cr, uid, domain)

            if(customer and delivery_ids):

                Manager = InvoiceMessageManager
                if hasattr(api, '_invoice_message_manager'):
                    Manager = api._invoice_message_manager

                mail_params = {
                    'uid': uid,
                    'report_service': 'report.stock.picking.invoice',
                    'cr': cr,
                    'delivery_ids': delivery_ids,
                    'customer_name': customer.name,
                    'file_name': '{:ManualInvoice%Y_%m_%d_%H_%M_%S}',
                    'subject': 'Invoice for {customer_name}'.format(customer_name=customer.name) if customer else 'Invoice'
                }

                for sett in settings.settings:
                    if sett.name == 'emails_for_sending_invoice':
                        mail_params['emails_to'] = sett.value

                _manager = Manager(mail_params)
                mail_message = _manager.generate_email()
                if mail_message:
                    self.write(cr, uid, settings.id, {
                        'mi_file': _manager.file,
                        'mi_filename': _manager.file_name,
                    })

                    if settings.mi_send_by_email:
                        self.integrationApiSendEmail(cr, uid, mail_message)

        return True

    def integrationApiSendEmail(self, cr, uid, mail_messages, settings=None):
        log = []
        if (not isinstance(mail_messages, list)):
            mail_messages = [mail_messages]
        try:
            for mail_message in mail_messages:
                if not isinstance(mail_message["emailto_list"], (list, tuple)):
                    mail_message["emailto_list"] = [mail_message["emailto_list"]]
                _email_from = mail_message["email_from"]
                _emailto_list = mail_message["emailto_list"]
                _subject = mail_message["subject"]
                _body = mail_message["body"]
                _attachments = mail_message["attachments"]
                _subtype = mail_message.get("subtype", 'plain')
                self.pool.get('mail.message').schedule_with_attach(
                    cr, uid,
                    _email_from, _emailto_list, _subject, _body,
                    subtype=_subtype, attachments=_attachments, context=None
                )
        except Exception as ex:
            log.append({
                'title': 'Error sending email!',
                'msg': ex,
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        if(log and settings):
            self.log(cr, uid, settings, log)
        return True

    def getApi(self, cr, uid, name):
        conf_obj = self.pool.get('ir.config_parameter')
        settings_obj = conf_obj.get_param(cr, uid, 'sale.integrarion.allowed.hosts')
        local_path_ids_obj = conf_obj.get_param(cr, uid, 'sale.integration.local.path')
        local_path_ids = eval(local_path_ids_obj) if local_path_ids_obj else ('/orders_temp', '/orders')
        api = None
        settings = None
        mapping_apis = get_mapping_apis('classes')

        if name:
            settings_id = self.search(cr, uid, [('name', '=', name)])
            if(len(settings_id) > 0):
                settings = self.browse(cr, uid, settings_id[0])

                if not settings.backup_local_path:
                    settings.backup_local_path = local_path_ids[0]
                if not settings.input_local_path:
                    settings.input_local_path = local_path_ids[1]

                settings_variables = {}

                settings_variables_keys = []
                if settings.settings:
                    for item in settings.settings:
                        settings_variables[item.name] = item.value
                        settings_variables_keys.append(item.name)
                settings_variables['settings_variables_keys'] = settings_variables_keys

                if(settings.use_mako_templates):
                    if(settings.mako_templates):
                        settings_variables['mako_templates'] = settings.mako_templates

                if settings.use_http:
                    settings_variables['multi_http_settings'] = settings.http_settings_ids

                if settings.use_ftp:
                    settings_variables['multi_ftp_settings'] = settings.ftp_settings_ids

                settings_variables['customer'] = self.get_external_customer_id(settings)
                settings_variables['backup_local_path'] = settings.backup_local_path
                settings_variables['input_local_path'] = settings.input_local_path
                settings_variables['passphrase'] = settings.passphrase
                settings_variables['gnupghome'] = settings.gnupghome
                settings_variables['url'] = settings.url
                settings_variables['customer_root_dir'] = settings.customer_root_dir

                if(settings.type_api == "amazon"):
                    settings_variables.update({
                        'host': settings.url,
                        'aws_access_key_id': settings.aws_access_key_id,
                        'secret_key': settings.secret_key,
                        'merchant_id': settings.merchant_id,
                        'marketplace_id': settings.marketplace_id,
                        'auth_token': settings.auth_token,
                        'customer': settings.customer_ids[0].ref,
                    })

                elif(settings.type_api == "ebay"):
                    settings_variables.update({
                        'from_size': settings.from_size and settings.from_size.name or False,
                        'to_size': settings.to_size and settings.to_size.name or False,
                    })

                elif(settings.type_api == "import_orders"):
                    settings_variables.update({
                        'import_settings': settings.import_settings,
                    })

                elif(settings.type_api == "magento"):
                    location = settings.url
                    if not location[-1] == '/':
                        location += '/'
                    settings_variables.update({
                        'corelocation': location,
                        'location': str(location + "index.php/api/xmlrpc"),
                        'username': settings.commercehub_login,
                        'password': settings.commercehub_pass,
                    })

                elif(settings.type_api == "newegg"):
                    settings_variables.update({
                        "host": settings.url,
                        'merchantKey': settings.newegg_sellerid,
                        "authenticationKey": settings.newegg_authorization,
                        "secret_key": settings.newegg_secret_key,
                        "response": "",
                    })

                elif(settings.type_api == "overstock"):
                    test_xml = settings.test_xml
                    if(settings.test_method not in ['integrationApiLoadOrders'] or not self.is_test):
                        test_xml = ""
                    test_xml = test_xml and test_xml.encode('iso-8859-1') or ""
                    settings_variables.update({
                        'api_url': settings.url,
                        'merchantKey': settings.merchantKey,
                        'authenticationKey': settings.authenticationKey,
                        'response': test_xml,
                    })

                elif(settings.type_api == "shop"):
                    settings_variables.update({
                        'shop_supplier_id': settings.shop_supplier_id,
                        'shop_store_name': settings.shop_store_name,
                    })

                elif(settings.type_api == "sterling"):
                    settings_variables.update({
                        'processQueue': settings.processQueue
                    })

                elif(settings.type_api in ["walmart", "walmartcanada"]):
                    settings_variables.update({
                        'vendor_id': settings.vendor_id,
                    })
                #DELMAR-100 start
                elif (settings.type_api == "hbc_bulk"):
                    sale_integration_xml_obj = self.pool.get('sale.integration.xml')
                    settings_variables['si_xml_obj'] = sale_integration_xml_obj
                    settings_variables['type_api_id'] = settings.id
                    settings_variables['send_mail'] = self
                    settings_variables['cursor'] = cr
                    settings_variables['user_id'] = uid
                #DELMAR-100 end    
                logger.info("TYPE API: %s" % settings.type_api)
                api_class = mapping_apis.get(settings.type_api, False)
                if api_class:
                    api = api_class(settings_variables)
                else:
                    api = None


        if settings_obj:
            allowed = False
            allowed_hosts = eval(settings_obj)
            for host in allowed_hosts:
                if host == socket.gethostname():
                    allowed = True
            if not allowed:
                return settings, None
        return settings, api

    def get_external_customer_id(self, settings):
        external_customer_id = None
        if settings and settings.customer_ids:
            for customer in settings.customer_ids:
                if settings.old_get_external_customer_id:
                    external_customer_id = customer.external_customer_id
                    break
                if customer.title and customer.title.shortcut:
                    external_customer_id = customer.title.shortcut
                    break
                if customer.sale_integration_main:
                    external_customer_id = customer.external_customer_id
                    break
            if not external_customer_id:
                external_customer_id = settings.customer_ids[0].external_customer_id
        return external_customer_id

    def get_main_customer_id(self, settings, single_is_main=False):
        main_customer_id = False
        if settings.type_api != 'import_orders':
            if len(settings.customer_ids) > 1:
                for customer in settings.customer_ids:
                    if customer.sale_integration_main:
                        main_customer_id = customer.id
                        break
            elif single_is_main and settings.customer_ids:
                main_customer_id = settings.customer_ids[0].id
        return main_customer_id

    def integrationApiGetOrdersInLocalFolder(self, cr, uid, name=""):
        (settings, api) = self.getApi(cr, uid, name)
        if(api):
            log = []
            if api.use_local_folder is True or (settings.type_api in ("commercehub", "ice", "vendornet", "walmart", "people", "amazon_retail", "shop", "wayfair", "shoebuy", "bluefly_txt", "steinmart", "balfour", "aafes_ds","jomashop2")):
                api.loadOrders(True)
                log = api.getLog()
            if(len(log) > 0):
                self.log(cr, uid, settings, log)
        return True

    def integrationApiGetCancelOrdersInLocalFolder(self, cr, uid, name=""):
        (settings, api) = self.getApi(cr, uid, name)
        if(api):
            log = []
            if api.use_local_folder is True or (settings.type_api in ("commercehub", "ice", "vendornet", "walmart", "people", "amazon_retail", "shop", "wayfair", "bluefly_txt", "steinmart", "aafes_ds", "balfour","jomashop2")):
                api.loadOrders(True, 'cancel')
                log = api.getLog()
            if(len(log) > 0):
                self.log(cr, uid, settings, log)
        return True

    def create_rows_for_IM(self, cr, uid, api, settings):
        ssoih_obj = self.pool.get('stock.sale.order.import.history')
        filenames_loaded_from_ftp = get_list_files(os.path.join(settings.input_local_path, 'import_orders', 'load_orders'))
        _path_to_backup_local_dir = api.create_local_dir(settings.backup_local_path, 'import_orders', 'orders', "now")
        if(filenames_loaded_from_ftp):
            for _ftp_filename in filenames_loaded_from_ftp:
                customer_ref = re.search("""<ftp\[.*\]>""", _ftp_filename)
                if(customer_ref):
                    file_name = re.sub("""<ftp\[.*\]>""", "", os.path.basename(_ftp_filename))
                    customer_ref = str(customer_ref.group()).replace('<ftp[', '').replace(']>', '')
                else:
                    file_name = os.path.basename(_ftp_filename)
                with open(_ftp_filename) as _f:
                    _file_data = _f.read()
                file_exist = False
                filenames_backup = get_list_files(os.path.join(settings.backup_local_path, 'import_orders', 'orders'))
                for filename in filenames_backup:
                    with open(filename) as f:
                        backup_file_data = f.read()
                    if(_file_data == backup_file_data):
                        availaible_ssoih_ids = ssoih_obj.search(cr, uid, [('full_filename', '=', filename)])
                        if (len(availaible_ssoih_ids) > 0):
                            file_exist = True
                            break
                if(not file_exist):
                    blf_utils.file_for_writing(
                        _path_to_backup_local_dir,
                        file_name,
                        _file_data,
                        'w'
                    ).create()
                    full_filename = os.path.join(_path_to_backup_local_dir, file_name)
                    mfs_id = False
                    if(customer_ref):
                        customer = self.pool.get('res.partner').search(cr, uid, [('ref', '=', customer_ref)])
                        mfs_obj = self.pool.get('multi.ftp.settings')
                        if(customer):
                            for mfs_row in mfs_obj.browse(cr, uid, settings.ftp_settings_ids):
                                if(mfs_row.id.action == 'load_orders'):
                                    if(mfs_row.id.notification_for_customer.id == customer[0]):
                                        mfs_id = mfs_row.id.id
                                        break
                    if(os.path.exists(full_filename)):
                        _id = ssoih_obj.create(cr, uid, {
                            'full_filename': full_filename,
                            'filename': file_name,
                            'file_type': False,
                            'size': False,
                            'log': '',
                            'load_method': 'from_ftp',
                            'state': 'draft',
                            'mfs': mfs_id,
                        })
                        if(_id):
                            _msg = "File {0} imported from ftp".format(file_name)
                            ssoih_obj.to_log(cr, uid, _id, _msg, "info")
                            ssoih_obj.recalculate_size(cr, uid, _id)
                            # ssoih_obj.recheck_filetype(cr, uid, _id)
                        os.remove(_ftp_filename)
                else:
                    os.remove(_ftp_filename)

    def integrationApiLoadImportOrders(self, cr, uid, api, settings, _id=False):
        orders_list = []
        ssoih_obj = self.pool.get('stock.sale.order.import.history')

        resent_data = []
        if(_id):
            orders_filenames_ids = ssoih_obj.search(cr, uid, [('id', '=', _id)])
        else:
            api.loadOrders(check_ftp=True, cursor=cr, pool=self.pool)
            self.create_rows_for_IM(cr, uid, api, settings)
            orders_filenames_ids = ssoih_obj.search(cr, uid, [('state', '=', 'draft')])
        for o_id in orders_filenames_ids:
            import_instance = ssoih_obj.browse(cr, uid, o_id)
            if os.path.exists(import_instance.full_filename):
                resent_data.append({
                    'filename': import_instance.full_filename,
                    'file_format': import_instance.file_type,
                    'import_id': o_id,
                })
        if not resent_data:
            return orders_list
        result = api.loadOrders(resent_data, cursor=cr, pool=self.pool)
        for finish_filename in result:
            import_id = ssoih_obj.search(cr, uid, [('full_filename', '=', finish_filename)])[0]
            if not import_id:
                continue
            errors = result[finish_filename]['errors']
            has_errors = ssoih_obj.add_errors(cr, uid, import_id, errors)
            if has_errors:
                continue
            orders_list.extend(result[finish_filename]['orders_list'])

            ssoih_obj.write(cr, uid, import_id, {
                'state': 'created_xmls',
                'load_for_partner': result[finish_filename]['load_for_partner'],
                'load_order_ids': ','.join(order['name'] for order in result[finish_filename]['orders_list'] if order['name'])
            })
            ssoih_obj.to_log(cr, uid, import_id, 'Complete Import', "info")
            new_type_api_id = self.checkAvailaibleNewTypeApi(cr, uid, result[finish_filename]['load_for_partner'], result[finish_filename]['load_for_partner_ref'])
            if(new_type_api_id):
                for order in result[finish_filename]['orders_list']:
                    order['import_id'] = import_id
                    order['new_type_api_id'] = new_type_api_id
            else:
                ssoih_obj.to_log(cr, uid, import_id, "Couldn't find main Sale Integration", "warning")
        return orders_list

    def checkAvailaibleNewTypeApi(self, cr, uid, load_for_partner,load_for_partner_ref):
        change_api_ok = False
        _type_api_id = False
        _new_type_api_id = None
        _type_api_id = self.search_type_api_id_by_customer(cr, uid, load_for_partner,True,load_for_partner_ref)
        if(_type_api_id is not None):
            change_api_ok = True
        else:
            change_api_ok = False
        if((change_api_ok is not False) and (_type_api_id is not False)):
            if(isinstance(_type_api_id, int)):
                _new_type_api_id = _type_api_id
            else:
                _new_type_api_id = None
        return _new_type_api_id

    @send_mails_from_result
    def integrationApiLoadOrders(self, cr, uid, name="", _id=False):
        result = {
            'object': None,
            'return_result': False,
            'settings': None,
        }
        (settings, api) = self.getApi(cr, uid, name)
        logger.info("Get API: %s, SETTINGS: %s, NAME: %s" % (api, settings, name))
        orders_list = []
        log = []
        mail_message = self.pool.get('mail.message')
        sale_integration_xml_obj = self.pool.get('sale.integration.xml')
        if api:
            if settings.type_api == 'import_orders':
                orders_list = self.integrationApiLoadImportOrders(cr, uid, api, settings, _id)
            else:
                orders_list = api.loadOrders()
            logger.info("Got orders: %s" % orders_list)
            log.extend(api.getLog())
            create_xml = 0
            if orders_list:
                original_type_api_id = settings.id
                for i in xrange(0, len(orders_list)):
                    import_id = orders_list[i].get('import_id'),
                    new_type_api_id = orders_list[i].get('new_type_api_id', False)
                    if new_type_api_id in ('Sams Club', 'Walmart Fulfilled (FBW)'):
                        sams_api_id = self.search(cr, uid, [('name', '=', new_type_api_id)])
                        sams_api = False
                        if sams_api_id:
                            sams_api = self.browse(cr, uid, sams_api_id[0])
                        new_type_api_id = sams_api and sams_api.id or None
                        original_type_api_id = sams_api and sams_api.id or None
                    real_type_api_id = new_type_api_id or original_type_api_id
                    search_args = [
                        ('name', '=', orders_list[i]['name']),
                        '|',
                            ('type_api_id', '=', real_type_api_id),
                            ('new_type_api_id', '=', real_type_api_id),
                    ]
                    xml_ids = sale_integration_xml_obj.search(cr, uid, search_args)
                    if not xml_ids:

                        si_xml_id = sale_integration_xml_obj.create(cr, uid, {
                            'xml': orders_list[i]['xml'],
                            'name': orders_list[i]['name'],
                            'type_api_id': original_type_api_id,
                            'import_id': import_id,
                            'new_type_api_id': new_type_api_id,
                        })
                        create_xml += 1

                        if settings.type_api == 'walmart':
                            lines = []
                            for xml_line in orders_list[i]['lines']:
                                lineObj = {}
                                lineObj["external_customer_line_id"] = xml_line['external_customer_line_id']
                                lineObj["external_customer_order_id"] = orders_list[i]['external_customer_order_id']
                                lines.append(lineObj)
                            state = 'accepted'
                            if 'cancel' in orders_list[i]['name'].lower():
                                emails_list = ['moshe@delmarintl.ca', 'delmar@isddesign.com', 'leab@delmarintl.ca']
                                mail_message.schedule_with_attach(cr, uid, 'erp.delmar@gmail.com', emails_list, 'Walmart Load ' + orders_list[i]['name'], orders_list[i]['xml'])
                            else:
                                api.processingOrderLines(lines, state)
                            log.extend(api.getLog())
                            log.append({
                                'title': 'Accepted Sale Order %s ' % orders_list[i]['external_customer_order_id'],
                                'msg': 'Accepted Sale Order',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                        elif settings.type_api == 'newegg':
                            api.orderConfirmation(orders_list[i]['order_id'])
                            log.extend(api.getLog())
                        elif settings.type_api in('sams', 'aafes_s2s'):
                            if 'textmessage' in orders_list[i]['name'].lower():

                                attachments = {}
                                order_xml = orders_list[i]['xml']

                                if order_xml:
                                    attachments['Data.txt'] = base64.b64encode(order_xml)

                                message_data = orders_list[i].get('message_data')
                                mail_message.schedule_with_attach(
                                    cr, uid,
                                    'erp.delmar@gmail.com',
                                    ['delmar@isddesign.com'],
                                    settings.name + ' ' + orders_list[i]['name'],
                                    message_data,
                                    attachments=attachments
                                )
                                sale_integration_xml_obj.write(cr, uid, si_xml_id, {'load': True})
                    elif api.apierp_is_available('generate_duplicate_xml_email'):
                        api.apierp.pool = self.pool
                        email_message = api.apierp.generate_duplicate_xml_email(cr, uid, orders_list[i]['name'], xml_ids)
                        if email_message:
                            self.integrationApiSendEmail(cr, uid, email_message)

                if settings.send_functional_acknowledgement:
                    api.confirmLoad(orders_list)
                    log.extend(api.getLog())

                log.append({
                    'title': 'Finish loading orders %s count %d' % (name, create_xml),
                    'msg': '',
                    'type': 'finish',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            if(len(log) > 0):
                self.log(cr, uid, settings, log)
            result.update({'object': api, 'settings': settings, 'return_result': True})
        return result

    def integrationApiResendFunctionalAcknoledgment(self, cr, uid, sale_order, context=None):
        log = []
        si = sale_order.type_api_id
        if not isinstance(si, browse_null):
            name = si.name
            (settings, api) = self.getApi(cr, uid, name)
            if api:
                if settings:
                    if settings.send_functional_acknowledgement:
                        sale_integration_xml = sale_order.sale_integration_xml_id
                        if sale_integration_xml and sale_integration_xml.xml:
                            order_obj = {
                                'name': sale_integration_xml.name,
                                'xml': sale_integration_xml.xml
                            }
                            if settings.name in ('Sams Club', 'Walmart Fulfilled (FBW)'):
                                order_obj['new_type_api_id'] = settings.name
                            api.confirmLoad([order_obj])
                            log.append({
                                'title': 'Resent Functional Acknoledgment for Sale Order: {0}'.format(sale_order.name),
                                'msg': '',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            log.extend(api.getLog())
                        else:
                            log.append({
                                'title': 'Couldn\'t found XML for Sale Order: {0}'.format(sale_order.name),
                                'msg': '',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                    else:
                        log.append({
                            'title': 'Please change flag \'Send Functional Acknowledgement\' to True for {}'.format(
                                settings.name),
                            'msg': '',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                else:
                    log.append({
                        'title': 'Couldn\'t found settings for {0}'.format(name),
                        'msg': '',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
            else:
                log.append({
                    'title': 'Couldn\'t getting api for Sale Order: {0} by name {1}'.format(
                        sale_order.name, name
                    ),
                    'msg': '',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
        else:
            log.append({
                'title': 'Couldn\'t found type_api_id for Sale Order: {0}'.format(sale_order.name),
                'msg': '',
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        if log and settings:
            self.log(cr, uid, settings, log)
        return True

    def integrationApiCreateOrders(self, cr, uid, name="", limit=-10, xml_id=None):
        # xml_id also can be an array

        xml_ids = []
        sale_model = self.pool.get('sale.order')
        xml_model = self.pool.get('sale.integration.xml')
        discount_model = self.pool.get('product.discount')
        helper = self.pool.get('stock.picking.helper')
        prod_obj = self.pool.get('product.product')

        if xml_id:
            if isinstance(xml_id, (int, long)):
                xml_id = [xml_id]
            for in_xml in xml_model.browse(cr, uid, xml_id):
                if in_xml and in_xml.type_api_id and in_xml.type_api_id.name and not in_xml.load:
                    name = in_xml.type_api_id.name
                    xml_ids.append(in_xml.id)

        logger.info("RUN ApiCreateOrder, IDS: %s" % xml_ids)

        (settings, api) = self.getApi(cr, uid, name)
        log = []
        if(limit == -10):
            limit = settings.process_limit
        if(api):
            logger.info("API FOUND")
            main_customer_id = self.get_main_customer_id(settings)

            if(len(xml_ids) == 0):
                condition = [
                    ('load', '=', False),
                    ('type_api_id', '=', settings.id),
                    ('name', 'not like', 'CANCEL'),
                    ('name', 'not like', 'CHANGE'),
                    ('name', 'not like', 'TEXTMESSAGE'),
                ]
                if(limit != -1):
                    xml_ids = xml_model.search(cr, uid, condition, limit=limit)
                else:
                    xml_ids = xml_model.search(cr, uid, condition)

            create_orders = []
            confirm_lines = []
            if (len(xml_ids) == 0):
                log.append({
                    'title': 'Xml list empty',
                    'msg': '',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

            for xml_obj in xml_model.browse(cr, uid, xml_ids):
                if(xml_obj.type_api_id and xml_obj.type_api_id.type_api == 'sterling' and (datetime.utcnow() < parser.parse(xml_obj.create_date or ''))):
                    continue

                logger.info("START PROCESSING XML")

                default_address = {
                    'country_id': settings.default_country and settings.default_country.id or False
                }

                _type_api_id = xml_obj.new_type_api_id.id or settings.id
                orders_obj = api.getOrdersObj(xml_obj.xml)

                if settings.name == 'CommerceHubSterling' and orders_obj.get('salesDivision', False):
                    if orders_obj.get('salesDivision', False) in  ['2000','2032']:
                        if orders_obj.get('salesDivision', False) == '2000':
                            name = 'CommerceHubSterlingKay'
                        elif orders_obj.get('salesDivision', False) == '2032':
                            name = 'CommerceHubSterlingKayOutlet'
                        (settings, api) = self.getApi(cr, uid, name)
                        _type_api_id =  settings.id
                        main_customer_id = self.get_main_customer_id(settings)

                log.extend(api.getLog())
                if(orders_obj):
                    logger.info("GOT ORDERS OBJECT")
                    if(orders_obj['order_id']):
                        search_args = [('type_api_id', '=', _type_api_id)]

                        if settings.type_api == 'sterling' and orders_obj.get('poNumber', False):
                            search_args.extend(['|', ("external_customer_order_id", "=", orders_obj['order_id']), ("po_number", "=", orders_obj['poNumber'])])
                        elif (settings.type_api in ('aafes_ds','balfour')):
                            search_args.append(("po_number", "=", orders_obj['poNumber']))
                        else:
                            search_args.append(("external_customer_order_id", "=", orders_obj['order_id']))

                        order_ids = self.pool.get("sale.order").search(cr, uid, search_args)

                        if (len(order_ids) == 0 and settings.type_api == 'overstock_sofs'):
                            search_args = [('type_api_id', '=', _type_api_id), ("po_number", "=", orders_obj['poNumber'])]
                            po_ids = self.pool.get("sale.order").search(cr, uid, search_args)
                            if po_ids:
                                salesChannelOrderNumber = orders_obj.get('additional_fields',{}).get('salesChannelOrderNumber', False)
                                num = '_1'
                                if salesChannelOrderNumber:
                                    res = re.split('-', salesChannelOrderNumber)
                                    num = '_' +res[len(res) - 1]
                                orders_obj['poNumber'] = orders_obj['poNumber'] + num

                        # salesChannelOrderNumber
                        if(len(order_ids) == 0):
                            logger.info("START CREATING NEW SALE ORDER")
                            sales_order = {}
                            orders_obj.update({'ref': self.getCustRef(cr, uid, xml_obj.id)})
                            customer = self.getPartner(cr, uid, settings, orders_obj)
                            customer_address = self.pool.get('res.partner').address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])

                            logger.info("GOR PARTNER AND ADDRESS")

                            sales_order['partner_id'] = customer.id

                            sales_order['type_api'] = settings.type_api
                            if (settings.si_type == 'edi'):
                                sales_order['format'] = 'edi'
                            sales_order['type_api_id'] = _type_api_id
                            sales_order['sale_integration_xml_id'] = xml_obj.id
                            sales_order['note'] = orders_obj.get('note', False)
                            sales_order['flash'] = settings.flash
                            if(orders_obj.get('flash', False)):
                                sales_order['flash'] = True

                            shop = self.getShop(cr, uid, orders_obj)

                            logger.info("GOT SHOP")

                            sales_order['external_customer_order_id'] = orders_obj['order_id']
                            if orders_obj.get('external_customer_order_id', False):
                                sales_order['external_customer_order_id'] = orders_obj['external_customer_order_id']
                            sales_order['original_order_number'] = orders_obj.get('original_order_number', False)
                            sales_order['shop_id'] = shop.id
                            sales_order['pricelist_id'] = shop.pricelist_id.id

                            sales_order['fulfillment_channels'] = orders_obj.get('fulfillment_channels', "")

                            sales_order['po_number'] = orders_obj.get('poNumber', False)
                            if not sales_order['po_number']:
                                sales_order['po_number'] = sales_order['external_customer_order_id']
                            sales_order['bulk_transfer'] = orders_obj.get('bulk_transfer', False) or customer.bulk_transfer or False
                            sales_order['payment_method'] = orders_obj.get('paymentMethod', False)
                            sales_order['order_comment'] = orders_obj.get('order_comment', False)
                            sales_order['merchandise_cost'] = orders_obj.get('merchandiseCost', False)
                            sales_order['sales_division'] = orders_obj.get('salesDivision', '')
                            sales_order['tax'] = orders_obj.get('tax', False)
                            sales_order['credits'] = orders_obj.get('credits', False)
                            sales_order['shipping'] = orders_obj.get('shipping', False)
                            sales_order['handling'] = orders_obj.get('handling', False)
                            sales_order['sub_total'] = orders_obj.get('sub_total', False)
                            sales_order['total'] = orders_obj.get('total', False)
                            if (orders_obj.get('fulfillment_channels', "") == "AFN" and settings.type_api == 'amazon'):
                                sales_order['date_order'] = orders_obj.get('external_date_order', False)

                            sales_order['packslip_message'] = orders_obj.get('poHdrData', {}).get('packListData', {}).get('packslipMessage', "")
                            sales_order['cust_order_number'] = orders_obj.get('poHdrData', {}).get('custOrderNumber', {}) or orders_obj.get('ERPCustOrderNumber', False)
                            sales_order['credit_type'] = orders_obj.get('poHdrData', {}).get('creditType', '')
                            sales_order['tax_type'] = orders_obj.get('poHdrData', {}).get('taxType', '')

                            sales_order['discount_available'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('text', 0)
                            sales_order['discount_type'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('discTypeCode', "")
                            sales_order['discount_date_indicator'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('discDateCode', "")
                            sales_order['discount_percentage'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('discPercent', "")
                            sales_order['discount_due_days'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('discDaysDue', "")
                            sales_order['net_due_days'] = orders_obj.get('poHdrData', {}).get('discountTerms', {}).get('netDaysDue', "")
                            sales_order['ship2store'] = self.getShip2Store(cr, uid, orders_obj)
                            logger.info("GOT S2S")

                            if(orders_obj.get('poHdrData', {}).get('giftIndicator') == 'y'):
                                sales_order['gift'] = True
                            if orders_obj.get('lines'):
                                sales_order['receipt_id'] = orders_obj.get('lines')[0].get('poLineData', {}).get('lineNote2', "")
                                if not sales_order['receipt_id']:
                                    sales_order['receipt_id'] = orders_obj.get('address', {}).get('ship', {}).get('personPlaceData', {}).get('ReceiptID', "")

                            for i in ('amount_tax',
                                      'carrier',
                                      'external_date_order',
                                      'orderType',
                                      'merchantreference1',
                                      'merchantreference2',
                                      'merchantreference3',
                                      'requestedShipDate',
                                      'store_number',
                                      'tcnumber',
                                      'asnnumber_prefix',
                                      'processQueue',
                                      'priorityEDI',
                                      'discount_total',
                                      'ice_total_due',
                                      'ice_tax_percentage',
                                      'shipping_fee',
                                      'shipping_tax',
                                      'earliest_ship_date_order',
                                      'latest_ship_date_order',
                                      'signature'):
                                sales_order[i] = orders_obj.get(i, False)
                            for type_address in orders_obj['address']:
                                if type_address in ['shipFrom']:
                                    continue
                                address_id = self.getAddress(cr, uid, type_address, orders_obj, customer, default_address, settings.type_api)
                                logger.info("GOT ADDRESS_ID")

                                if(type_address == "ship"):
                                    sales_order['partner_shipping_id'] = address_id
                                elif(type_address == "order"):
                                    sales_order['partner_order_id'] = address_id
                                elif(type_address == "invoice"):
                                    sales_order['partner_invoice_id'] = address_id

                            if(not sales_order.get('partner_order_id', False)):
                                sales_order['partner_order_id'] = customer_address['contact']

                            if(not sales_order.get('partner_invoice_id', False)):
                                sales_order['partner_invoice_id'] = customer_address['invoice']

                            sales_order['state'] = 'draft'
                            sales_order['company_id'] = customer.company_id.id or False

                            if api.apierp_is_available('fill_order'):
                                logger.info("RUN API FILL_ORDER")
                                api.apierp.pool = self.pool
                                api_context = {
                                    'customer_id': customer.id
                                }
                                api_sales_order = api.apierp.fill_order(cr, uid, settings, orders_obj, api_context)
                                sales_order.update(api_sales_order)
                                logger.info("COMPLETE FILL SALES ORDER")
                            elif settings.type_api == 'import_orders':
                                logger.info("RUN IMPORT_ORDERS API")
                                additional_fields = []
                                gift_message = orders_obj.get('poHdrData', {}).get('giftMessage', False) or orders_obj.get('gift_message', '')

                                additional_fields.append((0, 0, {
                                    'name': 'gift_message',
                                    'label': 'Gift Message',
                                    'value': gift_message,
                                }))
                                order_obj_fields = {
                                    'additional_fields': additional_fields
                                }
                                sales_order.update(order_obj_fields)
                                logger.info("COMPLETE FILL IMPORT_ORDER")

                            order_id = sale_model.create(cr, uid, sales_order)
                            logger.info("SALE ORDER CREATED, ID: %s" % order_id)

                            if(settings.type_api in ('sterling', 'walmart', 'amazon_retail', 'commercehub') and orders_obj.get('Comments', False) and len(orders_obj['Comments']) > 0):
                                insert_comment = []
                                for comment in orders_obj['Comments']:
                                    insert_comment.append((0, 0, {
                                        'type': comment['CommentType'],
                                        'text': comment['Text']
                                    }))
                                sale_model.write(cr, uid, order_id, {'sterling_comments': insert_comment})
                            self.pool.get("order.ext").create(cr, uid, {'ext_xml': str(orders_obj), 'sale_id': order_id})

                            # self.clear_transactions_for_reentered_order(cr, uid, orders_obj['order_id'])

                            new_sale_order = sale_model.browse(cr, uid, order_id)
                            order_name = str(new_sale_order.name)
                            log.append({
                                'title': 'Create sale order %s , order_name %s' % (name, order_name),
                                'msg': 'from: \n %s sale order id = %s' % (str(orders_obj), order_name),
                                'type': 'create',
                                'model': 'sale.order',
                                'id': order_id,
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            logger.info("SI LOG WROTE")
                            # all lines list
                            line_ids = []
                            # set lines list
                            set_line_ids = []

                            note = ""
                            ret_p_object = RetailPrice(cr, uid, self.pool)
                            logger.info("GOT RETAIL PRICE OBJ")
                            for line in orders_obj['lines']:
                                logger.info("START LINE CYCLE")
                                line_obj = {
                                    'notes': '',
                                    'external_customer_line_id': line.get('id', False),
                                }
                                if 'additional_fields' in line:
                                    line_obj.update({
                                        'additional_fields': line.get('additional_fields', {})
                                    })

                                if not line.get('qty', False):
                                    note += "Qty should be positive value %s" % (line['name'])
                                    line_obj["notes"] += "Qty should be positive value.\n"

                                factory_type = settings.type_api
                                if customer.parent_partner_id and customer.parent_partner_id.name == 'Denny':
                                    factory_type = 'dennys_customers'
                                elif settings.name == 'Dennys Customers':
                                    factory_type = 'dennys_customers'  # double check
                                elif 'ledpax' in settings.name.lower():  # For Amazon LedPax use own price calc.
                                    logger.info('Amazon LedPax customised calc price called!')
                                    factory_type = 'amazon_ledpax'
                                elif customer.virtual:
                                    factory_type = 'virtual_customer'
                                elif customer.ref in ('ZLS', 'PPL'):
                                    factory_type = 'zales'

                                customer_product = ProductFactory.factory(factory_type, cr, uid, self.pool)

                                if api.apierp_is_available('fill_line'):
                                    api.apierp.pool = self.pool
                                    api_context = {
                                        'customer_id': main_customer_id or customer.id
                                    }
                                    api_line = api.apierp.fill_line(cr, uid, settings, line, api_context)
                                    line_obj.update(api_line)
                                    note += api_line['notes']
                                # custom fill_line in main code >>> START <<<
                                # TODO: NEED TO TRANSFER TO OAPI CLASS TO SAME API

                                else:
                                    if (line.get('size', False) and line.get('sku', False)):
                                        try:
                                            float(line['size'])
                                        except Exception:
                                            line['sku'] += '/' + line['size']
                                            line['size'] = False

                                    cur_customer_id = main_customer_id or customer.id
                                    product, size = customer_product.get_product_and_size(line, cur_customer_id)

                                    if size and size.id:
                                        line_obj["size_id"] = size.id

                                    if settings.type_api == 'walmart':
                                        if product:
                                            line_obj["size_id"] = size and size.id or False
                                    elif settings.type_api == 'people':
                                        line_obj['size_id'] = customer_product.size_id


                                    if(product):
                                        line_obj["product_id"] = product.id
                                    else:
                                        if(settings.type_api != 'commercehub'):
                                            note += "Can't find product by sku %s, name %s" % (line['sku'], line['name'])
                                            line_obj["notes"] = "Can't find product by sku %s.\n" % (line['sku'])
                                        else:
                                            note += "Can't find product by upc %s, name %s" % (line['UPC'], line['name'])
                                            line_obj["notes"] += "Can't find product by upc %s.\n" % (line['UPC'])
                                        line_obj["product_id"] = 0

                                    line_obj["gift_message"] = line.get('poLineData', {}).get('giftMessage', "")
                                    line_obj["gift_wrap_indicator"] = line.get('poLineData', {}).get('giftWrapIndicator', "")
                                    line_obj["tax_type"] = line.get('poLineData', {}).get('taxType', "")
                                    line_obj["tax"] = line.get('poLineData', {}).get('taxBreakout', "")
                                    line_obj["credit_amount"] = line.get('poLineData', {}).get('creditAmount', "")
                                    line_obj["balance_due"] = line.get('lineBalanceDue', "")

                                    line_obj["line_note1"] = line.get('poLineData', {}).get('lineNote1', "")
                                    line_obj["full_retail"] = line.get('poLineData', {}).get('fullRetail', "")

                                    line_obj["expected_ship_date"] = line.get('expectedShipDate', "")
                                    line_obj["long_description"] = line.get('description2', "")
                                    line_obj["manufacturer_sku"] = line.get('manufacturerSKU', "")
                                    line_obj["bond"] = line.get('poLineData', {}).get('prodDescription3', "")

                                    for i in ('size',
                                              'tax',
                                              'customerCost',
                                              'merchantLineNumber',
                                              'merchantSKU',
                                              'optionSku',
                                              'shipCost',
                                              'syncPrice',
                                              'vendorSku',
                                              'linePrice',
                                              'lineRef1',
                                              'lineRef2',
                                              'lineRef3',
                                              'lineRef4',
                                              'ice_sub_total',
                                              'ice_row_total',
                                              'ice_tax_amount',
                                              'ice_discount'):
                                        line_obj[i] = line.get(i, False)

                                    if not line_obj["tax"] and line.get('lineTax', False):
                                        line_obj["tax"] = line.get('lineTax')
                                # custom fill_line in main code >>> END <<<

                                _size = line.get('size', False)
                                if(_size):
                                    try:
                                        _size = float(str(_size).replace(',', '.'))
                                    except ValueError:
                                        bad_size_note = "[Bad Size: {0}]".format(_size)
                                        line_obj['notes'] += bad_size_note
                                        note += bad_size_note
                                        _size = False
                                line['size'] = _size
                                if line.get('size', False) or False:
                                    size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', str(float(line['size'])))])
                                    if size_ids:
                                        if line_obj.get('size_id', False):
                                            if line_obj['size_id'] != size_ids[0]:
                                                variants = self.pool.get('ring.size').browse(cr, uid, [size_ids[0], line_obj['size_id']])
                                                size_note = "Please double-check size. Need to select %s or %s.\n" % (variants[0].name, variants[1].name,)
                                                note += size_note
                                                line_obj['notes'] += size_note
                                                line_obj['size_id'] = False
                                        else:
                                            line_obj['size_id'] = size_ids[0]

                                line_obj['type_api'] = settings.type_api
                                line["name"] = line_obj.get('name', '')
                                # DELMAR - 186
                                line_obj["name"] = line.get('name', '')
                                # END DELMAR- 186
                                line_obj["order_id"] = order_id
                                line_obj["product_uom_qty"] = line['qty']
                                line_obj["product_uom"] = 1

                                # DLMR-735 - check is imported in MANUAL
                                line_obj["price_unit"] = line_obj.get('price_unit', 0.0)
                                if (xml_obj.import_id and xml_obj.type_api_id.id == 69 and xml_obj.import_id.load_method == 'manual') or customer.virtual or customer.ref == 'BLF':
                                    logger.info("IMPORT ORDER FROM FILE, USE COMMON PRICE FROM FILE: %s" % line_obj.get('price_unit', 0.0))
                                else:
                                    logger.info("TRYING TO CALCULATE PRICE_UNIT")
                                    # price
                                    parameters = {
                                        'customer': customer,
                                        'line': line,
                                        'customer_id': main_customer_id or customer.id,
                                        'product_id': line_obj.get("product_id", False),
                                        'size_id': line_obj.get('size_id', False),
                                        'price_from_file': line_obj.get('price_unit', 0.0),
                                        'type_api': settings.type_api,
                                        'product_gwp': line_obj.get('product_gwp', False)
                                    }

                                    # Pagoda
                                    if customer.ref == "ZLS" and orders_obj.get('salesDivision', '') == '0010':
                                        parameters.update({'customer_id': self.pool.get('res.partner').search(cr, uid, [('ref', '=', 'PRP')])[0]})
                                    if customer.ref == "STJ" and orders_obj.get('salesDivision', '') != '':
                                        ref = 'STK'
                                        if orders_obj.get('salesDivision', '') == '2001':
                                            ref = 'STJ'
                                        elif orders_obj.get('salesDivision', '') == '2032':
                                            ref = 'STO'
                                        parameters.update({'customer_id': self.pool.get('res.partner').search(cr, uid, [('ref', '=', ref)])[0]})

                                    if line_obj.get("product_id", False):
                                        line_obj['price_unit'], price_note = customer_product.calculate_price(
                                            parameters
                                        )

                                        note += price_note
                                        if not line_obj['price_unit']:
                                            line_obj["notes"] += "Can't find product cost.\n"

                                if not line_obj['price_unit']:
                                    line_obj["price_unit"] = line_obj.get('price_unit', 0.0)

                                price_fields = ['price_unit', 'cost']
                                for _field in price_fields:
                                    if line_obj.get(_field):
                                        line_obj[_field] = str2float(line_obj[_field])

                                # DLMR-996 cf_price_unit
                                cf_price_unit = None
                                if not customer.virtual:
                                    line_obj['cf_price_unit'] = line_obj.get('price_unit', 0.0)
                                    try:
                                        cf_price_unit = customer_product.calculate_discounts(
                                            parameters.get("product_id", False),
                                            parameters.get('size_id', False),
                                            parameters.get('customer_id', False),
                                            customer_product.get_price_from_customer_field(
                                                parameters.get('customer_id', False),
                                                parameters.get("product_id", False),
                                                parameters.get('size_id', False)
                                            )
                                        ) or None
                                    except:
                                        pass

                                if cf_price_unit and cf_price_unit is not None:
                                    line_obj['cf_price_unit'] = str(cf_price_unit).replace('$','').replace(',','')

                                # Check if retail_cost column exists in line and can be converted to float value
                                if customer.virtual:
                                    logger.info("NO NEED RETAIL PRICE")
                                    line_obj["customerCost"] = None
                                else:
                                    logger.info("GET RETAIL PRICE")
                                    cur_customer_id = main_customer_id or customer.id
                                    customer_ref = customer.ref

                                    price = self.get_retail_price(cr, uid, line_obj, customer) or None
                                    line_obj["customerCost"] = price

                                    # if all(map(lambda x: x in line, ('optionSku', 'sku'))):
                                    #    product, _ = customer_product.get_product_and_size(line, cur_customer_id)
                                    #else:
                                    #    product = prod_obj.browse(cr, uid, prod_obj.search(cr, uid, [('default_code', '=', line.get('vendorSku') or line.get('merchantSKU') or line.get('sku'))]))
                                    #delmar_id = product and (product[0] if isinstance(product, list) else product).default_code or None
                                    if line_obj.get("product_id"):
                                        product = prod_obj.browse(cr, uid, prod_obj.search(cr, uid, [('id', '=',int(line_obj.get("product_id", 0)))]))
                                        delmar_id = product and (product[0] if isinstance(product, list) else product).default_code or None
                                        if price and delmar_id:
                                            if (line_obj['type_api'] != 'sams'):
                                                ret_p_object.insert_or_update_retail_price(customer_ref, ((delmar_id, price,),))
                                line_obj['state'] = 'draft'
                                line_obj['type'] = 'make_to_stock'
                                line_obj['size'] = line['size']
                                if settings.flash:
                                    line_obj["product_order_qty"] = line_obj['product_uom_qty']

                                if line_obj['notes']:
                                    line_obj['notes'] += 'Debug info: %s' % (str(line))

                                product_is_set = helper.check_product_is_set(cr, uid, product_id=line_obj.get("product_id", False))

                                if new_sale_order.partner_id.auto_split_sets:
                                    if product_is_set:
                                        line_obj["is_set"] = True
                                        line_obj["set_parent_order_id"] = order_id
                                        line_obj["order_id"] = order_id
                                        line_obj["set_product_id"] = line_obj["product_id"]
                                        if line_obj.get("product_uos_qty", False) == False:
                                            line_obj["product_uos_qty"] = line_obj.get("product_uom_qty", 1)

                                    if (line_obj.get("size_id", False) == False and line_obj.get("size", False) and  size_ids):
                                        line_obj["size_id"] = size_ids[0]

                                # create sale line
                                line_id = self.pool.get("sale.order.line").create(cr, uid, line_obj)
                                line_ids.append(line_id)

                                if (settings.type_api in ['sterling', 'commercehub'] and line.get('Comments', False) and len(
                                        line['Comments']) > 0):
                                    insert_comment = []
                                    for comment in line['Comments']:
                                        insert_comment.append((0, 0, {
                                            'type': comment['CommentType'],
                                            'text': comment['Text']
                                        }))
                                    self.pool.get("sale.order.line").write(cr, uid, line_id,
                                                                           {'sterling_comments': insert_comment})

                                if new_sale_order.partner_id.auto_split_sets and product_is_set:
                                    set_line_ids.append(line_id)
                                    self.pool.get("sale.order").write(cr, uid, order_id, {"is_set": True})

                                logger.info("DONE CYCLE")
                                confirm_lines.append({'external_customer_line_id': line['id']})

                            # Update sale order lines contains sets
                            logger.info("Update sale order lines contains sets")
                            set_hidden_lines = self.pool.get("sale.order.line").split_set_line_to_components(cr, uid, set_line_ids, settings=settings, context=None)
                            if set_hidden_lines and set_hidden_lines != []:
                                line_ids.append(set_hidden_lines)
                            logger.info("Update sale order lines contains sets DONE")

                            if(note != ""):
                                if new_sale_order.note:
                                    note = new_sale_order.note + '\n\n' + note
                                self.pool.get("sale.order").write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                                if api.apierp_is_available('generate_fail_creation_order_email'):
                                    api.apierp.pool = self.pool
                                    email_message = api.apierp.generate_fail_creation_order_email(cr, uid, order_id)
                                    if email_message:
                                        self.integrationApiSendEmail(cr, uid, email_message)
                            elif settings.auto_confirm:
                                wf_service = netsvc.LocalService("workflow")
                                wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)

                            logger.info("DONE Write sale order notes")

                            cur_env = self.pool.get('ir.config_parameter').get_param(cr, uid, 'system.environment')
                            if(len(confirm_lines) > 0 and settings.type_api in ["overstock"] and cur_env != 'production'):
                                logger.info("RUN Overstock")
                                created_sale_order = self.pool.get('sale.order').browse(cr, uid, order_id)
                                if not created_sale_order.sale_integration_proccessed:
                                    api.settings['response'] = ""
                                    if api.apierp_is_available('get_accept_information'):
                                        api.apierp.pool = self.pool
                                        confirm_lines = api.apierp.get_accept_information(cr, uid, settings, created_sale_order)

                                    confirm_log = api.getLog()
                                    sale_vals = {}
                                    if(api.processingOrderLines(confirm_lines)):
                                        sale_vals.update({"sale_integration_proccessed": True})
                                    else:
                                        sale_vals.update({"note": str(confirm_log)})
                                    sale_model.write(cr, uid, order_id, sale_vals)

                                    confirm_lines = []
                                    log.extend(confirm_log)
                                    logger.info("DONE Overstock")

                            create_orders.append(order_name)
                            xml_model.write(cr, uid, xml_obj.id, {'load': True})
                            #print order_id
                            logger.info("DONE ORDER!!!")
                        else:
                            logger.info("ORDER EXISTS, Sending notifications")
                            ext_order_id_list = [x.external_customer_order_id for x in self.pool.get('sale.order').browse(cr, uid, order_ids) or []]
                            if(orders_obj['order_id'] not in ext_order_id_list):
                                sale_model.write(cr, uid, order_ids, {
                                    'external_customer_related_order_id': orders_obj['order_id'],
                                })

                            if(settings.load_twice_confirm):
                                self.integrationApiProcessingOrder(cr, uid, order_ids)
                                log.append({
                                    'title': 'Resend confirm for external_customer_order_id = %s' % (orders_obj['order_id']),
                                    'msg': 'from: \n %s \n external_customer_order_id = %s' % (str(orders_obj), orders_obj['order_id']),
                                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                                })
                            xml_model.write(cr, uid, xml_obj.id, {'load': True})
                            log.append({
                                'title': 'Found order by external_customer_order_id = %s, not create' % (orders_obj['order_id']),
                                'msg': 'from: \n %s \n external_customer_order_id = %s' % (str(orders_obj), orders_obj['order_id']),
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            if api.apierp_is_available('generate_duplicate_order_email'):
                                api.apierp.pool = self.pool
                                email_message = api.apierp.generate_duplicate_order_email(cr, uid, xml_obj, orders_obj['order_id'])
                                if email_message:
                                    self.integrationApiSendEmail(cr, uid, email_message)

            if(settings.process_canceled):
                self.integrationApiProcessCancelXML(cr, uid, name, limit=10)

            if(len(create_orders) > 0):
                logger.info("Write complete orders to log: %s" % create_orders)
                msg = "Creating orders: "
                for i in create_orders:
                    msg += str(i) + " "
                log.append({
                    'title': 'Finish creating orders %s count %d' % (name, len(create_orders)),
                    'msg': msg,
                    'type': 'finish',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })

            if(len(log) > 0):
                self.log(cr, uid, settings, log)

            return True

    def _check_prod_size(self, cr, uid, component_id, size_id):
        component_size = ''
        if size_id:
            sql = """select rel.* FROM product_product as pp
                LEFT JOIN product_template as pt on pt.id = pp.product_tmpl_id
                LEFT JOIN product_ring_size_default_rel as rel on rel.product_id = pt.id
                where pp.id = %(product_id)s
                and size_id = %(size_id)s
                """
            cr.execute(sql, {
                'product_id': component_id,
                'size_id': size_id
            })
            products_data = cr.fetchall()
            if products_data:
                component_size = size_id

        return component_size

    def get_retail_price(self, cr, uid, line, customer):

        if customer.use_price_unit_as_retail:
            return line['price_unit']

        if 'product_id' not in line or not line["product_id"]:
            return None

        customerCost = 0
        if line.get("customerCost"):
            try:
                customerCost = float(line.get("customerCost", 0))
            except:
                customerCost = False
                pass

        if customerCost <= 0:
            customerCost = False

        from_xml = customerCost
        from_ms_sql = self.get_ms_customer_price(cr, uid, line["product_id"], customer)
        from_last_order = self.get_previous_retail(cr, uid, line, customer)

        sale_obj = self.pool.get("sale.order").browse(cr, uid, line['order_id'])
        # set default Retail price
        retail_price = from_xml or from_ms_sql or from_last_order or None
        # Hardcode for sterling EDI
        if sale_obj.type_api == "sterling" and 'EDI' in sale_obj.external_customer_order_id:
            retail_price = from_ms_sql or from_xml or from_last_order or None
        # DLMR-2235 Hardcoded deny retail for Balfour and
        elif customer.ref in ['DRB', 'BAL']:
            retail_price = None
        # END DLMR-2235
        return retail_price

    def get_previous_retail(self, cr, uid, line, customer):
        cr.execute("""
            select str_to_float(ol."customerCost")
            from sale_order_line ol
                left join sale_order so on so.id = ol.order_id
            where ol.product_id = %(product_id)s
                and coalesce(ol.size_id, 0) = %(size_id)s
                and so.partner_id = %(partner_id)s
                and str_to_float(ol."customerCost") != 0
            order by ol.id desc
            limit 1
        """, {
            'product_id': line['product_id'],
            'size_id': line.get('size_id') or 0,
            'partner_id': customer.id,
        })
        res = cr.fetchone()
        return res and res[0] or None

    def integrationApiLoadOrdersFBA(self, cr, uid, name="", limit=-10):
        (settings, api) = self.getApi(cr, uid, name)
        orders_list = {}
        log = []
        if(limit == -10):
            limit = settings.process_limit
        if(api):
            prefix = ""
            if (name != "Amazon_FBA"):
                prefix = "." + name.lower()
            next_token_param = 'sale.integration{prefix}.next_token_param'.format(prefix=prefix)
            orders_get_param = 'sale.integration{prefix}.orders_get'.format(prefix=prefix)

            token_system_param = self.pool.get('ir.config_parameter')
            token_system_param_this = token_system_param.get_param(cr, uid, next_token_param)
            time_delta_system_param_this = token_system_param.get_param(cr, uid, 'sale.integration.time_delta_system_param')
            id_list_system_param = token_system_param.get_param(cr, uid, 'sale.integration.id_list_system_param')
            if (token_system_param_this == "False"):
                token_system_param.set_param(cr, uid, orders_get_param, "All")
                token_system_param.set_param(cr, uid, next_token_param, "token")
                token_system_param_this = token_system_param.get_param(cr, uid, next_token_param)
            orders_list = api.loadOrdersFBA(token_system_param_this, id_list_system_param, time_delta_system_param_this, limit)
            log.extend(api.getLog())
            create_xml = 0
            if(orders_list):
                for i in xrange(0, len(orders_list)):
                    xml_ids = self.pool.get('sale.integration.xml').search(cr, uid, [('name', '=', orders_list[i]['name'])])
                    if(len(xml_ids) == 0):
                        orders_list[i]['type_api_id'] = settings.id
                        self.pool.get("sale.integration.xml").create(cr, uid, orders_list[i])
                        create_xml += 1

                log.append({
                    'title': 'Finish loading orders %s count %d' % (name, create_xml),
                    'msg': '',
                    'type': 'finish',
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            if(len(log) > 0):
                self.log(cr, uid, settings, log)
            token_system_param = self.pool.get('ir.config_parameter')
            token_system_param.set_param(cr, uid, next_token_param, str(api.next_token_Amazon))

    def search_id_delmar_by_OS_sku(self, cr, uid, os_sku):
        id_delmar = False

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)
        if server_id:
            search_sql = """
                        SELECT TOP 1
                            ltrim(rtrim(DelmarProductID))
                        FROM
                            dbo.CustomerProductIDs
                        WHERE
                            CustomerID like 'OS'
                        and (CustomerProductID = '{0}' or CustomerProductID + 'C' = '{0}')
                        ORDER BY CustomerProductID """.format(os_sku)

            result = server_data.make_query(cr, uid, server_id, search_sql, select=True, commit=False)

            id_delmar = result and result[0] and result[0][0] or False

        return id_delmar

    def getShop(self, cr, uid, order):
        shop_ids = self.pool.get("sale.shop").search(cr, uid, [])
        return self.pool.get("sale.shop").browse(cr, uid, shop_ids[0])

    def getPartner(self, cr, uid, settings, order):
        res_partner_obj = self.pool.get("res.partner")
        import_orders = (settings.name == 'Import Orders')
        where_id = ''
        or_external = ''
        sql = """WITH split_partners as (
                SELECT external_customer_id, trim(unnest(string_to_array(external_customer_id, ','))) AS part_id, id
                FROM res_partner
                {where_id}
            )
            SELECT id
            FROM split_partners
            WHERE part_id = %s
            {or_external}
        """
        if import_orders:
            if order.get('ref', False):
                res = res_partner_obj.search(cr, uid, [('ref', '=', order.get('ref', False))])
                if res:
                    return res_partner_obj.browse(cr, uid, res[0])

            or_external = 'OR external_customer_id = %s'
            params = (order['partner_id'], order['partner_id'])
        else:
            customer_ids = [customer.id for customer in settings.customer_ids]
            where_id = 'WHERE id in %s'
            params = (tuple(customer_ids), order['partner_id'])

        if order['partner_id'] in ('signetkj') and order.get('salesDivision', False):
            cust = res_partner_obj.search(cr, uid, [('sale_division', '=', order.get('salesDivision', False))])
            return cust and res_partner_obj.browse(cr, uid, cust[0]) or None

        sql = sql.format(where_id=where_id, or_external=or_external)
        cr.execute(sql, params)
        res = cr.fetchone()
        return res and res_partner_obj.browse(cr, uid, res[0]) or None

    def getCustRef(self, cr, uid, xmlid):
        sql = """
        select ref from sale_integration_xml six
        left join sale_integration_customer sic on sic.integration_id=six.new_type_api_id
        left join res_partner rp on rp.id=sic.customer_id 
        where 
        not exists (select 1 from sale_integration_customer sic2 where sic2.integration_id=sic.integration_id and sic.customer_id<>sic2.customer_id)
        and  six.id = {xml_id}
        """.format(**{'xml_id': xmlid})
        cr.execute(sql)
        res = cr.fetchone()

        return res and res[0] or None

    def getAddress(self, cr, uid, type_address, order, customer, defaults=None, type_api=False):
        logger.info("RUN getAddress")
        address = order['address'][type_address]
        address_id = address.get('address_id', False)
        address_obj = self.pool.get("res.partner.address")
        if address_id:
            if isinstance(address_id, (str, unicode)):

                if (customer.name == 'Amazon Retail'):
                    res_address_id = address_obj.search(cr, uid,
                                                        [('san_address', '=', address_id), ('type', '=', 'delivery')])
                    address_id = res_address_id and res_address_id[0] or False
                else:
                    module, str_id = address_id.split('.')
                    data_obj = self.pool.get('ir.model.data')
                    logger.info("RUN SEARCH ADDRESS OBJECT REF")
                    res_address_id = data_obj.get_object_reference(cr, uid, module, str_id)
                    address_id = res_address_id and res_address_id[1] or False
                    logger.info("END SEARCH ADDRESS OBJECT REF")

            if address_id and address_obj.browse(cr, uid, address_id):
                return address_id

        values = defaults or {}

        values['type'] = type_address
        if (type_address == "ship"):
            values['type'] = 'delivery'
        if (type_address == "order"):
            values['type'] = 'contact'

        # Denny's customers orders should take address from the customer
        is_empty_address = not (address['address1'] or address['address2'])
        if (type_api == 'import_orders' and values['type'] == 'delivery'
                and is_empty_address and customer.parent_partner_id
        ):
            for cust_address in customer.address:
                if cust_address.type == values['type']:
                    defaults = {
                        'partner_id': None,
                        'stock_picking_ids': [],
                    }
                    return address_obj.copy(cr, uid, cust_address.id, defaults)

        if (type_api == 'hbc_bulk' and values['type'] == 'delivery'):
            for customer_address in customer.address:
                warehouse_id = customer_address.function
                if len(warehouse_id) > 5:
                    warehouse_id = warehouse_id[2:]
                if address['name'] == warehouse_id:
                    return customer_address.id

        values['name'] = address['name']
        values['street'] = address['address1']
        values['street2'] = address['address2']

        values['street3'] = address.get('address3', None)
        values['zip'] = address['zip']
        values['city'] = address['city']
        values['phone'] = address.get('phone', False)
        values['email'] = address.get('email', False)
        values['fax'] = address.get('fax', False)
        values['san_address'] = address.get('san_address', False)
        values['company'] = address.get('company','')
        person_place_data = address.get('personPlaceData')
        if isinstance(person_place_data, dict):
            if person_place_data.get('VcdId'):
                values['vcdid'] = person_place_data['VcdId']
            if person_place_data.get('s_company'):
                values['s_company'] = person_place_data['s_company']
        values['outgoind_partner_id'] = customer.id
        values['external_customer_order_id'] = order['order_id']

        # Get Country
        country = address.get('country', False)
        if not country:
            country = "US"
        elif country in ('USA', 'CAN'):
            country = country[:2]

        if country == 'US':
            zip_preg = re.compile('^\d{9}$')
            address_zip = address['zip'] or ''
            if zip_preg.match(address_zip):
                values['zip'] = address_zip[:5] + '-' + address_zip[5:]

        if not values.get('country_id', False) and country:
            ids_name = self.pool.get("res.country").name_search(cr, uid, country)
            if ids_name:
                values["country_id"] = ids_name[0][0]

        # Get State
        state_name = address.get('state')

        if order['partner_id'] in ("overstock_canada"):
            state_obj = self.pool.get("res.country.state")
            state_ids = state_obj.search(cr, uid, [('code', '=', address.get('state', False))])
            state = state_obj.browse(cr, uid, state_ids)
            if state:
                state_name = state[0].name
                values["country_id"] = state[0].country_id.id

        if state_name:
            state_args = []
            if values.get('country_id', False):
                state_args = [('country_id', '=', values["country_id"])]
            ids_name = self.pool.get("res.country.state").name_search(cr, uid, name=state_name, args=state_args)
            if ids_name:
                full_match_ids = [id_name[0] for id_name in ids_name if id_name[1].lower() == state_name.lower()]
                values["state_id"] = full_match_ids and full_match_ids[0] or ids_name[0][0]

        # Stop creation addresses for partner(like AAFES)
        values['partner_id'] = None
        logger.info("RUN ADDRESS CREATE NEW")
        address_id = address_obj.create(cr, uid, values, {'partner_name': customer.name})
        logger.info("END ADDRESS CREATE NEW")
        return address_id

    def getShip2Store(self, cr, uid, orders_obj={}):
        ship2store = False
        if orders_obj:
            if orders_obj.get('poHdrData', {}).get('merchandiseTypeCode') in ('D2S', 'X2S'):
                ship2store = True
            shp_address1 = orders_obj.get('address', {}).get('ship', {}).get('address1', {})

            if shp_address1 and re.findall(r'PEO\d+|ZJC\d{5}|ZO\d{5}|GOR\d+|PAG\d+', shp_address1):
                ship2store = True

            if orders_obj.get('orderType', '') == 'SS':
                ship2store = True

        return ship2store

    # DLMR-1070
    # initiated for Jet first
    def integrationApiGrantReturnOrder(self, cr, uid, picking_id=None):
        name = None
        order = self.pool.get("stock.picking").browse(cr, uid, picking_id)
        settings_id = self.search(cr, uid, [('customer_ids', '=', order.sale_id.partner_id.id)])
        if settings_id:
            name = self.browse(cr, uid, settings_id[0]).name
        (settings, api) = self.getApi(cr, uid, name)
        # run check if checking method exists
        if settings and api and callable(getattr(api, 'grantReturnOrder', None)):
            grant_return = api.grantReturnOrder(order)
            logger.debug("Check grant return for Order# %s in Api %s: %s" % (order.name, settings.type_api, grant_return))
            return grant_return
        # grant by default
        return True

    def integrationApiGetRMA(self, cr, uid, name="", picking_id=""):
        log = []
        no_rma_str = "No RMA number"
        if picking_id:
            delivery_obj = self.pool.get("stock.picking").browse(cr, uid, picking_id)
            settings_id = self.search(cr, uid, [('customer_ids', '=', delivery_obj.sale_id.partner_id.id)])

            if settings_id:
                name = self.browse(cr, uid, settings_id[0]).name

        (settings, api) = self.getApi(cr, uid, name)
        if not settings or not api:
            return True

        if(api):
            if(settings.type_api == "overstock"):
                returnLines = api.getReturns()
                log = api.getLog()
                log.extend(api.getLog())
                print [(x['lineId'], x['Rma']['Number']) for x in returnLines]
                if picking_id:
                    delivery_obj = self.pool.get("stock.picking").browse(cr, uid, picking_id)
                    reason = delivery_obj.return_reason or ""
                    new_reason = ""
                    if delivery_obj.sale_id.partner_id.ref == "OS":
                        for move_line in delivery_obj.move_lines:
                            for returnLine in returnLines:
                                if(move_line.external_customer_line_id == returnLine['lineId']):
                                    if (not move_line.rma_number or move_line.rma_number in [no_rma_str]):
                                        new_reason += "\nExternal customer line id:  %s ; \nreturn code : %s ; \nreturn reason:  %s" % (returnLine['lineId'], returnLine["return_code"], returnLine["return_reason"])
                                        vals = {
                                            "rma_number": returnLine['Rma']['Number'] or no_rma_str,
                                            "ars_issued": str(returnLine["Rma"]["ArsIssued"]),
                                        }
                                        self.pool.get("stock.move").write(cr, uid, move_line.id, vals)
                                    break
                        if new_reason:
                            self.pool.get("stock.picking").write(cr, uid, picking_id, {"return_reason": reason + new_reason})
                else:
                    for returnLine in returnLines:
                        new_reason = "\nExternal customer line id:  %s ; \nreturn code : %s ; \nreturn reason:  %s" % (returnLine['lineId'], returnLine["return_code"], returnLine["return_reason"])
                        move_ids = self.pool.get("stock.move").search(cr, uid, [('state', '=', 'done'), ('external_customer_line_id', '=', returnLine['lineId'])])
                        update_pickings = []

                        if(len(move_ids) > 0):
                            for move_obj in self.pool.get("stock.move").browse(cr, uid, move_ids):
                                ref = move_obj.sale_line_id.order_id.partner_id.ref
                                if ref == "OS":
                                    if(not move_obj.rma_number or move_obj.rma_number in [no_rma_str]):
                                        vals = {
                                            "rma_number": returnLine['Rma']['Number'] or no_rma_str,
                                            "ars_issued": str(returnLine["Rma"]["ArsIssued"])
                                        }
                                        self.pool.get("stock.move").write(cr, uid, move_obj.id, vals)
                                        if move_obj.picking_id and move_obj.picking_id not in update_pickings:
                                            update_pickings.append(move_obj.picking_id)
                            for pick in update_pickings:
                                self.pool.get('stock.picking').write(cr, uid, pick.id, {'return_reason': (pick.return_reason or '') + new_reason})

            if(len(log) > 0):
                self.log(cr, uid, settings, log)

        return True

    def integrationApiGetReturns(self, cr, uid, name=""):
        (settings, api) = self.getApi(cr, uid, name)
        if(api):
            returnLines = api.getReturns()
            log = api.getLog()
            wf_service = netsvc.LocalService("workflow")
            processLine = []
            picking_id_store = {}
            for returnLine in returnLines:
                print returnLine
                move_id = self.pool.get("stock.move").search(cr, uid, [('external_customer_line_id', '=', returnLine['lineId'])])
                if(len(move_id) == 1):
                    move_id = move_id[0]
                    move_obj = self.pool.get("stock.move").browse(cr, uid, move_id)
                    pick_obj = move_obj.picking_id

                    if(picking_id_store.get(pick_obj.id, False)):
                        new_picking = picking_id_store[pick_obj.id]
                    else:
                        picking_ids = self.pool.get('stock.picking').search(cr, uid, [('name', '=', '%s-return' % pick_obj.name)])

                        if(len(picking_ids) > 0):
                            new_picking = picking_ids[0]
                        else:
                            type_mapping = {
                                'out': 'in',
                                'in': 'out',
                            }
                            new_type = type_mapping.get(pick_obj.type)
                            new_picking = self.pool.get('stock.picking').copy(cr, uid, pick_obj.id, {
                                'name': '%s-return' % pick_obj.name,
                                'move_lines': [], 'state': 'draft', 'type': new_type,
                                'date': returnLine['date'], 'invoice_state': 'none',
                            })
                            log.append({
                                'title': 'Create return order %s, name %s' % (name, '%s-return' % pick_obj.name),
                                'msg': 'From %s return order id = %s' % (returnLine.get('xml', ''), new_picking),
                                'type': 'create',
                                'model': 'stock.picking',
                                'id': new_picking,
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })

                        picking_id_store[pick_obj.id] = new_picking

                    new_move = self.pool.get('stock.move').copy(cr, uid, move_obj.id, {
                        'product_qty': returnLine["qty"],
                        'product_uos_qty': self.pool.get('product.uom')._compute_qty(cr, uid, move_obj.product_uom.id, int(returnLine["qty"]), move_obj.product_uom.id),
                        'picking_id': new_picking,
                        'state': 'draft',
                        'location_id': move_obj.location_dest_id.id,
                        'location_dest_id': move_obj.location_id.id,
                        'date': returnLine['date'],
                        'return_code': returnLine['return_code'],
                        'return_reason': returnLine['return_reason'],
                        'external_customer_return_id': returnLine["id"]
                    })

                    log.append({
                        'title': 'Create return stock move %s, name %s' % (name, pick_obj.name),
                        'msg': 'From %s return stock move id = %s' % (returnLine.get('xml', ''), new_move),
                        'model': 'stock.move',
                        'id': new_move,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

                    self.pool.get("stock.move").write(cr, uid, [move_obj.id], {'move_history_ids2': [(4, new_move)]})

                    wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)

                    self.pool.get('stock.picking').force_assign(cr, uid, [new_picking])

                    processLine.append(returnLine["id"])
                break
            if(len(log) > 0):
                self.log(cr, uid, settings, log)
            if(len(processLine) > 0):
                print processLine
                api.processingReturns(processLine)

        return True

    # @prodonly(return_value=True)
    def integrationApiProcessingOrder(self, cr, uid, ids=None, state="accepted"):
        if not ids:
            ids = []
        res = True
        for order_id in ids:

            sale_obj = self.pool.get("sale.order")
            sale = sale_obj.browse(cr, uid, order_id)


            if(sale.external_customer_order_id):
                if not sale.type_api_id:
                    settings_id = self.search_main_si_by_partner(cr, uid, sale.partner_id.id)
                else:
                    settings_id = sale.type_api_id.id

                if settings_id:
                    name = self.browse(cr, uid, settings_id).name
                    (settings, api) = self.getApi(cr, uid, name)

                    if(not api):
                        return True

                    lines = []
                    if api.apierp_is_available('get_additional_processing_so_information'):
                        api.apierp.pool = self.pool
                        for order_line in sale.order_line:
                            lineObj = api.apierp.get_additional_processing_so_information(cr, uid, settings, sale, order_line)
                            lineObj['new_type_api_id'] = name
                            lines.append(lineObj)

                    else:
                        # @TODO: these variables can be added dynamic in for-logic, but the first problem - embedded object (etc. order_line.order_id)
                        for order_line in sale.order_line:
                            lineObj = {}
                            lineObj["external_customer_line_id"] = order_line.external_customer_line_id
                            lineObj["po_number"] = order_line.order_id.po_number
                            lineObj["external_customer_order_id"] = order_line.order_id.external_customer_order_id
                            lineObj["cust_order_number"] = order_line.order_id.cust_order_number
                            lineObj['new_type_api_id'] = name
                            lines.append(lineObj)

                    res = api.processingOrderLines(lines, state)

                    log = api.getLog()
                    vals = {}
                    if not res and sale.flash:
                        res = True
                        note = str(log) + '\nSkipping acknowledgement fail for flash order'
                        vals.update({"note": note})
                    if res:
                        vals.update({"sale_integration_proccessed": True})
                    else:
                        vals.update({"note": str(log)})
                    sale_obj.write(cr, uid, order_id, vals)

                    if log:
                        for i in xrange(0, len(log)):
                            log[i]['title'] += "  %s" % sale.name

                        self.log(cr, uid, settings, log)
        return res

    def integrationApiRoutingRequest(self, cr, uid, delivery_id):
        deliver_obj = self.pool.get("stock.picking").browse(cr, uid, delivery_id)
        tracking_number = deliver_obj.tracking_ref
        sale_obj = deliver_obj.sale_id
        customer_obj = sale_obj.partner_id
        res = True
        if(sale_obj.external_customer_order_id):
            settings_id = self.search(cr, uid, [('customer_ids', '=', customer_obj.id)])
            if(len(settings_id) > 0):
                name = self.browse(cr, uid, settings_id[0]).name
                (settings, api) = self.getApi(cr, uid, name)

                if(not api):
                    return True

                if(settings.type_api != "amazon_retail"):
                    return True
                transaction_number = self.get_transaction_number(cr, uid)
                routing_request_number = self.get_routing_request_number(cr, uid)
                res = api.routingRequest(deliver_obj, transaction_number, routing_request_number)
                new_transaction_number = int(transaction_number) + 1
                self.set_transaction_number(cr, uid, new_transaction_number)
                new_routing_request_number = int(routing_request_number) + 1
                self.set_routing_request_number(cr, uid, new_routing_request_number)

                if(res):
                    log = api.getLog()
                    # self.pool.get("stock.picking").write(cr, uid, delivery_id, {'sale_integration_ship_confirmed': True}, {'note': ''})
                else:
                    log = api.getLog()
                    self.pool.get("stock.picking").write(cr, uid, delivery_id, {'note': str(log)})

                log.append({
                    'title': 'RoutingRequest %s' % str(res),
                    'msg': 'delivery order id: %s , tracking number: %s' % (delivery_id, tracking_number),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                if(len(log) > 0):
                    for i in xrange(0, len(log)):
                        log[i]['title'] += "  %s" % deliver_obj.name
                    self.log(cr, uid, settings, log)

        return res

    def get_transaction_number(self, cr, uid):
        current_number = self.pool.get('ir.config_parameter').get_param(cr, uid, 'amazon_retail.interchange.control.number')
        # current_number = self.transaction_number
        return current_number

    def set_transaction_number(self, cr, uid, number):
        new_number = self.pool.get('ir.config_parameter').set_param(cr, uid, 'amazon_retail.interchange.control.number', str(number))
        # new_number = self.transaction_number + 1
        # self.write(cr, uid, settings.id, {'transaction_number' : str(new_number)})
        return new_number

    def get_routing_request_number(self, cr, uid):
        current_number = self.pool.get('ir.config_parameter').get_param(cr, uid, 'amazon_retail.interchange.control.routing_request_number')
        # current_number = self.routing_request_number
        return current_number

    def set_routing_request_number(self, cr, uid, number):
        new_number = self.pool.get('ir.config_parameter').set_param(cr, uid, 'amazon_retail.interchange.control.routing_request_number', str(number))
        # new_number = self.write(cr, uid, settings.id, {'routing_request_number' : str(new_number)})
        return new_number

    def try_validate_tracking(self, cr, uid, delivery_id, context=None):
        if(context is None):
            context = {}
        tracking_is_valid = None
        tracking_validation_obj = self.pool.get('tracking.validation')
        res_shp_service_obj = self.pool.get('res.shipping.service')
        stock_picking_obj = self.pool.get("stock.picking")
        delivery_obj = stock_picking_obj.browse(cr, uid, delivery_id)
        tracking_number = delivery_obj.tracking_ref
        service_ids = res_shp_service_obj.search(cr, uid, [('name', '=', delivery_obj.shp_service)])
        msg_to_note = None
        if(service_ids):
            service = res_shp_service_obj.browse(cr, uid, service_ids[0])
            condition = [('service_id', '=', service.id), ('active', '=', True)]
            tracking_validation_ids = tracking_validation_obj.search(cr, uid, condition)
            if(tracking_validation_ids):
                tracking_validation_id = tracking_validation_ids[0]
                tracking_validators = []
                for validator in tracking_validation_obj.browse(cr, uid, tracking_validation_id).validators:
                    if(validator.len_tracking == len(tracking_number)):
                        tracking_validators.append(validator)
                if(tracking_validators):
                    result_validation = {'match': None}
                    for tracking_validator in tracking_validators:
                        result_validation = tracking_validator.validate(tracking_number)
                        if(result_validation['match'] is not None):
                            break
                    if(result_validation['match'] is not None):
                        tracking_is_valid = True
                    else:
                        tracking_is_valid = False
                        msg_to_note = 'Tracking "{0}" not valid for service "{1}"'.format(tracking_number, service.name)
                else:
                    tracking_is_valid = False
                    msg_to_note = 'Not found validator for Tracking "{0}", length = {1}, service "{2}"'.format(
                        tracking_number, len(tracking_number), service.name)
            else:
                tracking_is_valid = False
                msg_to_note = 'Not found validation for service "{0}"'.format(service.name)
        else:
            tracking_is_valid = False
            msg_to_note = 'Not found service: "{0}"'.format(delivery_obj.shp_service)
        if(msg_to_note):
            stock_picking_obj.add_message_to_note(cr, uid, delivery_id, msg_to_note)
        if(not tracking_is_valid):
            stock_picking_obj.action_bad_format_tracking(cr, uid, delivery_id, context)
        return tracking_is_valid

    # Replacement for set lines.
    def _update_set_line(self, cr, uid, line):
        sale_line_obj = self.pool.get("sale.order.line")
        sale_line_ids = [line.get('sale_line_id')[0]]

        sale_order_lines = sale_line_obj.read(cr, uid, sale_line_ids)

        if sale_order_lines:
            line['product_id'] = sale_order_lines[0].get('product_id')
            line['product_item'] = sale_order_lines[0].get('default_sku')
            line['product_uos_qty'] = sale_order_lines[0].get('product_uos_qty')
            line['product_qty'] = sale_order_lines[0].get('product_uos_qty')

        return line

    def _send_invoice_for_order(self, cr, uid, delivery_id, api, settings, customer_name):
        if not (api and settings and delivery_id):
            return
        if not self.pool.get("stock.picking").search(cr, uid, [('id', '=', delivery_id)]):
            return
        invoice_emails_to = False
        for item in settings.settings:
            item_name = str(item.name).strip()
            if item_name == 'emails_for_sending_invoice' and item.value:
                invoice_emails_to = item.value
                break
        if invoice_emails_to:
            settings_invoice = {
                'cr': cr,
                'uid': uid,
                'delivery_ids': delivery_id,
                'emails_to': invoice_emails_to,
                'customer_name': customer_name,
                'report_service': 'report.stock.picking.invoice',
            }
            api.generate_invoice_mail_message(settings_invoice)

    @send_mails_from_result
    def integrationApiSendInvoice(self, cr, uid, delivery_id):
        result = {}
        delivery_order = self.pool.get("stock.picking").browse(cr, uid, delivery_id)
        sale_order = delivery_order.sale_id
        customer = sale_order.partner_id
        si_type = 'edi' if sale_order.format == 'edi' else 'main'
        settings_ids = self.search(cr, uid, [
            ('customer_ids', '=', customer.id),
            ('si_type', '=', si_type),
        ])
        if settings_ids:
            name = self.browse(cr, uid, settings_ids[0]).name
            settings, api = self.getApi(cr, uid, name)
            self._send_invoice_for_order(cr, uid, delivery_id, api, settings, customer.name)
            result.update({'object': api, 'settings': settings})
        return result

    def _switch_set_lines(self, cr, uid, picking, type='to_done'):
        move_obj = self.pool.get('stock.move')
        _order_line = self.pool.get("sale.order.line")
        _order_history = self.pool.get("order.history")
        sale_obj = picking.sale_id

        if picking.is_set and picking.state  not in ('done', 'final_cancel'):
            move_line_ids = [x.id for x in picking.move_lines if x.is_set]
            set_parent_move_line_ids = [x.id for x in picking.set_parent_move_lines]
            if (type in ['to_done', 'to_return', 'to_cancel'] and len(move_line_ids) >= len(set_parent_move_line_ids)) or type == 'to_rollback':
                so_line_ids = [x.sale_line_id.id for x in picking.move_lines if x.is_set]
                so_real_line_ids = [x.sale_line_id.id for x in picking.set_parent_move_lines]
                so_line_components = [x for x in sale_obj.order_line if x.is_set]
                if type == 'to_done':
                    move_obj.write(cr, uid, set_parent_move_line_ids, {'state': 'done'})

                if type == 'to_rollback':
                    self.pool.get("sale.order").write(cr, uid, sale_obj.id, {'sale_integration_ship_confirmed': False})

                move_obj.write(cr, uid, move_line_ids,
                               {'picking_id': '', 'set_parent_order_id': picking.id, 'is_set_component': True,
                                'is_set': True})
                move_obj.write(cr, uid, set_parent_move_line_ids,
                               {'picking_id': picking.id, 'set_parent_order_id': ''})

                oh_ids = self.pool.get('order.history').search(cr, uid, [('sm_id', 'in', set_parent_move_line_ids)])
                if not oh_ids:
                    for set_line in set_parent_move_line_ids:
                        oh_vals = {'type': 'stock_move', 'sm_id': set_line}
                        _order_history.create(cr, uid, oh_vals)
                _order_history.write(cr, uid, oh_ids,
                                     {'parent_sale_order': sale_obj.id})
                #for splited orders
                if type == 'to_done' and\
                        len(so_line_ids) == len(move_line_ids) and\
                        len(set_parent_move_line_ids) == len(so_real_line_ids):
                    _order_line.write(cr, uid, so_line_ids,
                                      {'order_id': '', 'set_parent_order_id': sale_obj.id})
                    _order_line.write(cr, uid, so_real_line_ids,
                                      {'order_id': sale_obj.id, 'set_parent_order_id': ''})

    @send_mails_from_result
    def integrationApiConfirmShipment(self, cr, uid, delivery_id):
        result = {
            'object': None,
            'return_result': False,
            'settings': None,
        }
        _move = self.pool.get("stock.move")
        _picking = self.pool.get("stock.picking")
        _order_line = self.pool.get("sale.order.line")
        _partner_address = self.pool.get('res.partner.address')
        deliver_obj = _picking.browse(cr, uid, delivery_id)
        tracking_number = deliver_obj.tracking_ref
        sale_obj = deliver_obj.sale_id
        customer_obj = sale_obj.partner_id
        res = True
        manual_order = False

        if(sale_obj.external_customer_order_id):
            feed_format = 'edi' if sale_obj.format == 'edi' else 'main'
            settings_id = self.search(cr, uid, [('customer_ids', '=', customer_obj.id), ('si_type', '=', feed_format)])
            if(len(settings_id) > 0):
                name = self.browse(cr, uid, settings_id[0]).name
                if((name == 'Import Orders') and len(settings_id) > 1):
                    name = self.browse(cr, uid, settings_id[1]).name
                (settings, api) = self.getApi(cr, uid, name)

                if(not api):
                    return True

                if(settings.validate_tracking):
                    tracking_is_valid = self.try_validate_tracking(cr, uid, delivery_id)
                    if(not tracking_is_valid):
                        return False

                move_line_ids = [x.id for x in deliver_obj.move_lines]

                if deliver_obj.is_set and  deliver_obj.state != 'done':
                    set_parent_move_line_ids = [x.id for x in deliver_obj.set_parent_move_lines]
                    self._switch_set_lines(cr, uid, deliver_obj)
                    self.pool.get("stock.move").write(cr, uid, set_parent_move_line_ids, {'state': 'done'})
                    move_line_ids = set_parent_move_line_ids
                    no_sets_line_ids = [x.id for x in deliver_obj.move_lines if not x.is_set]
                    if no_sets_line_ids:
                        move_line_ids = set_parent_move_line_ids + no_sets_line_ids

                if sale_obj.flash:
                    _picking.write(cr, uid, delivery_id, {'sale_integration_ship_confirmed': True})
                    return True

                if settings.type_api == "sterling" and 'EDI' in sale_obj.external_customer_order_id and not sale_obj.external_customer_related_order_id:
                    _picking.write(cr, uid, delivery_id, {'sale_integration_ship_confirmed': True})
                    return True

                self.write_unique_fields_for_orders(cr, uid, deliver_obj, api=api)

                manual_order = (
                    name == 'Import Orders' or
                    not (
                        sale_obj and
                        sale_obj.sale_integration_xml_id and
                        sale_obj.sale_integration_xml_id.id or
                        False) or
                    (
                        sale_obj and
                        sale_obj.sale_integration_xml_id and
                        sale_obj.sale_integration_xml_id.type_api_id and
                        sale_obj.sale_integration_xml_id.type_api_id.name == 'Import Orders' or
                        False)
                )

                order_params = _picking.get_order_params(
                    cr, uid, deliver_obj.id,
                    ['skip_customer_invoice']
                )[deliver_obj.id]
                api.is_invoice_required = order_params.get('skip_customer_invoice') != '1'

                fmt = "{:.2f}"
                main_customer_id = self.get_main_customer_id(settings) or customer_obj.id
                move_line_ids = [x.id for x in deliver_obj.move_lines]


                if deliver_obj.is_set and  deliver_obj.state != 'done':
                    set_parent_move_line_ids = [x.id for x in deliver_obj.set_parent_move_lines]
                    self._switch_set_lines(cr, uid, deliver_obj)
                    self.pool.get("stock.move").write(cr, uid, set_parent_move_line_ids, {'state': 'done'})
                    move_line_ids = set_parent_move_line_ids
                    no_sets_line_ids = [x.id for x in deliver_obj.move_lines if not x.is_set]
                    if no_sets_line_ids:
                        move_line_ids = set_parent_move_line_ids + no_sets_line_ids

                lines = _move.read(cr, uid, move_line_ids)

                # common fields
                order_additional_fields = {x.name: x.value for x in sale_obj.additional_fields or []}
                line_count = len(sale_obj.order_line)
                amount_total_shipping = fmt.format(float(sale_obj.amount_total + float(deliver_obj.shp_handling or 0.0)))
                sale_taxes = self.pool.get("delmar.sale.taxes").compute_stock_picking_taxes(cr, uid, delivery_id, context=None)
                delivery_gst = fmt.format(float(sale_taxes.get('gst', 0.0)))
                delivery_pst = fmt.format(float(sale_taxes.get('pst', 0.0)))
                delivery_qst = fmt.format(float(sale_taxes.get('qst', 0.0)))
                delivery_hst = fmt.format(float(sale_taxes.get('hst', 0.0)))

                ship_addrs = _partner_address.search(
                    cr, uid, [
                        ('warehouse_id', '=', deliver_obj.warehouses.id),
                        ('partner_id', '=', customer_obj.id),
                        ('type', '=', 'delivery')
                    ])
                shipFrom = _partner_address.get_address(cr, uid, ship_addrs[0]) if ship_addrs else None

                set_lines = {}
                filtered_lines = []

                for line in lines:
                    if line.get('is_set_component'):
                        product_id = line.get('set_product_id') and line.get('set_product_id')[0]
                        if not set_lines.get(product_id):
                            set_lines[product_id] = True
                        filtered_lines.append(self._update_set_line(cr, uid, line))
                    else:
                        filtered_lines.append(line)

                lines = filtered_lines

                for idx, line in enumerate(lines):
                    moveline_taxes = self.pool.get("delmar.sale.taxes").compute_moveline_taxes(cr, uid, line.get('id', False), context=None)
                    location_id = line['location_id'] and line['location_id'][0] or None
                    order_line = _order_line.browse(cr, uid, line['sale_line_id'][0])
                    line['size'] = order_line.size_id and order_line.size_id.name or None
                    line['action'] = "v_ship"
                    line["lineCount"] = line_count
                    line["shipFrom"] = shipFrom
                    line['delmar_id'] = order_line.product_id.default_code
                    line['merchantSKU'] = str(order_line.merchantSKU)
                    line['vendorSku'] = str(order_line.vendorSku)
                    line['optionSku'] = str(order_line.optionSku)
                    line['merchantLineNumber'] = str(order_line.merchantLineNumber)
                    line['external_customer_line_id'] = str(order_line.external_customer_line_id)
                    line['po_number'] = sale_obj.po_number
                    line['discount_available'] = sale_obj.discount_available
                    line['discount_type'] = sale_obj.discount_type
                    line['discount_date_indicator'] = sale_obj.discount_date_indicator
                    line['discount_percentage'] = sale_obj.discount_percentage and int(float(sale_obj.discount_percentage)) or ''
                    line['discount_due_days'] = sale_obj.discount_due_days
                    line['net_due_days'] = sale_obj.net_due_days or ''
                    if line['discount_due_days']:
                        discount_due_date = datetime.now().date() + timedelta(days=int(line['discount_due_days']))
                        line['discount_due_date'] = discount_due_date.strftime("%Y%m%d")
                    else:
                        line['discount_due_date'] = ''
                    if line['net_due_days']:
                        net_due_date = datetime.now().date() + timedelta(days=int(line['net_due_days']))
                        line['net_due_date'] = net_due_date.strftime("%Y%m%d")
                    else:
                        line['net_due_date'] = ''

                    line["tracking_number"] = tracking_number
                    line["date_expected"] = deliver_obj.date_expected
                    line["carrier_code"] = deliver_obj.carrier_code
                    line["shp_date"] = deliver_obj.shp_date
                    line["shp_service"] = deliver_obj.shp_service
                    line["shp_code"] = deliver_obj.shp_code
                    line["warehouse"] = deliver_obj.warehouses and deliver_obj.warehouses.name or ''
                    line['warehouse_name'] = _move.get_warehouse_by_move_location(cr, uid, location_id) or ''

                    line["ship_to_name_1"] = deliver_obj.address_id.name
                    line["ship_to_address_line_1"] = deliver_obj.address_id.street or ''
                    line["ship_to_address_line_2"] = deliver_obj.address_id.street2 or ''
                    line["ship_to_city"] = deliver_obj.address_id.city or ''
                    line["ship_to_state"] = deliver_obj.address_id.state_id and deliver_obj.address_id.state_id.code or ''
                    line["ship_to_country"] = deliver_obj.address_id.country_id and deliver_obj.address_id.country_id.code or ''
                    line["ship_to_postal_code"] = deliver_obj.address_id.zip or ''
                    line["ship_to_san_address"] = deliver_obj.address_id.san_address or ''
                    line["ship_to_company"] = deliver_obj.address_id.company or ''
                    line["ship_to_s_company"] = deliver_obj.address_id.s_company or ''

                    if not idx:
                        line["shp_handling"] = fmt.format(float(deliver_obj.shp_handling or 0.0))
                    else:
                        line["shp_handling"] = fmt.format(0.0)
                    line["weight"] = deliver_obj.shp_weight

                    if(settings.type_api in ('wayfair', 'amazon_retail', 'tsc')):
                        line['customer_sku'] = self.pool.get('product.product').get_val_by_label(
                            cr,
                            uid,
                            order_line.product_id.id,
                            main_customer_id or order_line.order_partner_id.id,
                            'Customer SKU',
                            order_line.size_id.id or False,
                            )
                    if (settings.type_api in ['sams']):
                        line["weight"] = deliver_obj.weight or 1
                        line["volume"] = deliver_obj.volume or 1
                    if(settings.type_api in ["amazon_retail", "commercehub_edi"]):
                        line["latest_ship_date_order"] = sale_obj.latest_ship_date_order or ''
                        line["weight"] = deliver_obj.weight or 1
                        line["volume"] = deliver_obj.volume
                        if sale_obj.partner_shipping_id:
                            line["ship_to_name_1"] = sale_obj.partner_shipping_id.name or ''
                            line["ship_to_address_line_1"] = sale_obj.partner_shipping_id.street or ''
                            line["ship_to_city"] = sale_obj.partner_shipping_id.city or ''
                            line["ship_to_state"] = sale_obj.partner_shipping_id.state_id and sale_obj.partner_shipping_id.state_id.code or ''
                            line["ship_to_country"] = sale_obj.partner_shipping_id.country_id and sale_obj.partner_shipping_id.country_id.code or ''
                            line["ship_to_postal_code"] = sale_obj.partner_shipping_id.zip or ''
                            line["ship_to_san_address"] = sale_obj.partner_shipping_id.san_address or ''
                        else:
                            line["ship_to_name_1"] = ''
                            line["ship_to_address_line_1"] = ''
                            line["ship_to_city"] = ''
                            line["ship_to_state"] = ''
                            line["ship_to_postal_code"] = ''
                            line["ship_to_san_address"] = ''
                            line["ship_to_company"] = ''

                    line["external_customer_order_id"] = sale_obj.external_customer_order_id
                    line["external_customer_id"] = str(customer_obj.external_customer_id)
                    line["merchant_order_id"] = sale_obj.name
                    line["carrier"] = deliver_obj.carrier
                    line["invoice"] = str(deliver_obj.invoice_no)

                    line["amount_total"] = sale_obj.amount_total
                    line["amount_total_shipping"] = amount_total_shipping
                    line["amount_tax"] = sale_obj.amount_tax
                    line["amount_untaxed"] = sale_obj.amount_untaxed

                    line["gst"] = moveline_taxes.get('gst', 0)
                    line["pst"] = moveline_taxes.get('pst', 0)
                    line["qst"] = moveline_taxes.get('qst', 0)
                    line["hst"] = moveline_taxes.get('hst', 0)

                    line["customer_id"] = str(customer_obj.external_customer_id)
                    line['additional_fields'] = {x.name: x.value for x in order_line.additional_fields or []}
                    line['order_additional_fields'] = order_additional_fields
                    line['order_date'] = sale_obj.create_date

                    line['unit_cost'] = order_line.price_unit
                    line['product_uom_qty'] = order_line.product_uom_qty

                    # invoices fields
                    line["po_date"] = sale_obj.create_date and len(sale_obj.create_date) > 9 and sale_obj.create_date[:4]+sale_obj.create_date[5:7]+sale_obj.create_date[8:10] or ''
                    line["external_date_order"] = sale_obj.external_date_order or sale_obj.date_order
                    if sale_obj.partner_order_id:
                        line["remit_to_name_1"] = sale_obj.partner_order_id.name or ''
                        line["remit_to_address_line_1"] = sale_obj.partner_order_id.street or ''
                        line["remit_to_address_line_2"] = sale_obj.partner_order_id.street2 or ''
                        line["remit_to_city"] = sale_obj.partner_order_id.city or ''
                        line["remit_to_state"] = sale_obj.partner_order_id.state_id and sale_obj.partner_order_id.state_id.code or ''
                        line["remit_to_country"] = sale_obj.partner_order_id.country_id and sale_obj.partner_order_id.country_id.code or ''
                        line["remit_to_postal_code"] = sale_obj.partner_order_id.zip or ''
                        line["remit_to_san_address"] = sale_obj.partner_order_id.san_address or ''
                    else:
                        line["remit_to_name_1"] = ''
                        line["remit_to_address_line_1"] = ''
                        line["remit_to_address_line_2"] = ''
                        line["remit_to_city"] = ''
                        line["remit_to_state"] = ''
                        line["remit_to_country"] = ''
                        line["remit_to_postal_code"] = ''
                        line["remit_to_san_address"] = ''

                    line["return_name"] = ''
                    line["return_address_line_1"] = ''
                    line["return_address_line_2"] = ''
                    line["return_city"] = ''
                    line["return_state"] = ''
                    line["return_country"] = ''
                    line["return_postal_code"] = ''

                    if(settings.type_api in ('wayfair', 'newegg', 'aafes_s2s')):
                        if sale_obj.partner_id.address:
                            for address in sale_obj.partner_id.address:
                                if address.type == "return":
                                    line["return_name"] = address.name or ''
                                    line["return_address_line_1"] = address.street or ''
                                    line["return_address_line_2"] = address.street2 or ''
                                    line["return_city"] = address.city or ''
                                    line["return_state"] = address.state_id.code or ''
                                    line["return_country"] = address.country_id and address.country_id.code or ''
                                    line["return_postal_code"] = address.zip or ''
                                    break
                    line['api_customer'] = settings.name
                    line['manual_order'] = manual_order

                    if api.apierp_is_available('get_confirm_fields'):
                        api.apierp.pool = self.pool
                        sub_line = api.apierp.get_confirm_fields(cr, uid, main_customer_id, order_line)
                        line.update(sub_line)

                    if api.apierp_is_available('get_additional_confirm_shipment_information_for_line'):
                        api.apierp.pool = self.pool
                        sub_line = api.apierp.get_additional_confirm_shipment_information_for_line(
                            cr, uid,
                            main_customer_id,
                            deliver_obj,
                            order_line
                        )
                        line.update(sub_line)

                if(settings.type_api == "amazon_retail"):
                    transaction_number = self.get_transaction_number(cr, uid)
                    res = api.confirmShipment(lines, transaction_number)
                    new_transaction_number = int(transaction_number) + 1
                    self.set_transaction_number(cr, uid, new_transaction_number)
                else:
                    if api.apierp_is_available('get_additional_confirm_shipment_information'):
                        api.apierp.pool = self.pool
                        add_fields = api.apierp.get_additional_confirm_shipment_information(cr, uid, sale_obj, delivery_id, lines)
                        lines[0].update(add_fields)
                    send_email_list = False
                    for item in settings.settings:
                        item_name = str(item.name).strip()
                        if item_name == 'send_email_list' and item.value:
                            send_email_list = item.value
                            break

                    if(settings.type_api == 'commercehub'):
                        ch_settings = {
                            'manual_order': manual_order,
                            'use_mail_instead_of_ftp': settings.use_mail_instead_of_ftp,
                            'send_email_list': send_email_list,
                        }
                        res = api.confirmShipment(lines, ch_settings=ch_settings)
                    else:
                        res = api.confirmShipment(lines)
                    self._send_invoice_for_order(cr, uid, delivery_id, api, settings, customer_obj.name)
                    result.update({'object': api, 'settings': settings})
                log = api.getLog()
                check_sterling = ('EDI' in sale_obj.external_customer_order_id and not sale_obj.external_customer_related_order_id)
                if(res and (settings.type_api != "sterling" or check_sterling)):
                    self.pool.get("stock.picking").write(cr, uid, delivery_id, {'sale_integration_ship_confirmed': True}, {'note': ''})
                    if api.apierp_is_available('after_correct_confirm_shipment_callback'):
                        api.apierp.pool = self.pool
                        api.apierp.after_correct_confirm_shipment_callback(cr, uid, sale_obj, deliver_obj, lines)
                else:
                    _picking.write(cr, uid, delivery_id, {'note': str(log)})

                log.append({
                    'title': 'ConfirmShipment %s' % str(res),
                    'msg': 'delivery order id: %s , tracking number: %s' % (delivery_id, tracking_number),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                if(len(log) > 0):
                    for i in xrange(0, len(log)):
                        log[i]['title'] += "  %s" % deliver_obj.name
                    self.log(cr, uid, settings, log)
        result.update({'return_result': res})
        return result

    def integrationApiConfirmShipmentFlash(self, cr, uid, sale_id, tracking_number, flash_carrier_code=None):
        sale_obj = self.pool.get('sale.order').browse(cr, uid, sale_id)
        customer_obj = sale_obj.partner_id
        res = True
        if(sale_obj.external_customer_order_id):
            settings_id = self.search(cr, uid, [('customer_ids', '=', customer_obj.id)])
            if(len(settings_id) > 0):
                name = self.browse(cr, uid, settings_id[0]).name
                (settings, api) = self.getApi(cr, uid, name)

                if(not api):
                    return True

                sale_line_ids = []
                for order_line in sale_obj.order_line:
                    sale_line_ids.append(order_line.id)
                lines = self.pool.get("sale.order.line").read(cr, uid, sale_line_ids)

                transaction_number = self.get_transaction_number(cr, uid)
                for line in lines:
                    #order
                    line['origin'] = sale_obj.name
                    line["invoice"] = 'AMZP' + str(transaction_number)
                    line["latest_ship_date_order"] = sale_obj.latest_ship_date_order or ''
                    line["weight"] = 1
                    # line["volume"] = deliver_obj.volume

                    #Carrier
                    if(flash_carrier_code is None):
                        if(settings.type_api == 'amazon_retail'):
                            flash_carrier_code = 'UPSD'
                        else:
                            if(sale_obj.partner_id.flash_carrier_code):
                                flash_carrier_code = sale_obj.partner_id.flash_carrier_code
                            else:
                                flash_carrier_code = ''

                    line["carrier_code"] = flash_carrier_code
                    line["shp_date"] = sale_obj.shp_date

                    # Ship from
                    line["remit_to_name_1"] = sale_obj.partner_order_id.name or ''
                    line["remit_to_san_address"] = sale_obj.partner_order_id.san_address or ''
                    line["remit_to_city"] = sale_obj.partner_order_id.city or ''
                    line["remit_to_state"] = sale_obj.partner_order_id.state_id and sale_obj.partner_order_id.state_id.code or ''
                    line["remit_to_country"] = sale_obj.partner_order_id.country_id and sale_obj.partner_order_id.country_id.code or ''
                    line["remit_to_postal_code"] = sale_obj.partner_order_id.zip or ''

                    # Ship to
                    line["ship_to_name_1"] = sale_obj.partner_shipping_id.name or ''
                    line["ship_to_city"] = sale_obj.partner_shipping_id.city or ''
                    line["ship_to_state"] = sale_obj.partner_shipping_id.state_id and sale_obj.partner_shipping_id.state_id.code or ''
                    line["ship_to_country"] = sale_obj.partner_shipping_id.country_id and sale_obj.partner_shipping_id.country_id.code or ''
                    line["ship_to_postal_code"] = sale_obj.partner_shipping_id.zip or ''
                    line["ship_to_san_address"] = sale_obj.partner_shipping_id.san_address or ''

                    line["external_customer_order_id"] = sale_obj.external_customer_order_id
                    line["tracking_number"] = tracking_number

                    line['customer_sku'] = self.pool.get('product.product').get_val_by_label(cr, uid, line['product_id'][0], line['order_partner_id'][0], 'Customer SKU', line['size_id'] and line['size_id'][0] or False)
                    line['product_qty'] = line['product_uom_qty']
                    line['api_customer'] = settings.name

                lines[0]['transaction_number'] = transaction_number

                res = api.confirmShipment(lines)


                new_transaction_number = int(transaction_number) + 1
                self.set_transaction_number(cr, uid, new_transaction_number)


                log = api.getLog()
                if (res):
                    self.pool.get("sale.order").write(cr, uid, sale_obj.id, {'flash_processed': True})
                else:
                    note = sale_obj.note
                    if bool(note) is False:
                        note = ''
                    self.pool.get("sale.order").write(cr, uid, sale_obj.id, {'note': note + str(log)})

                log.append({
                    'title': 'ConfirmShipmentFlash %s' % str(res),
                    'msg': 'sale order id: %s , tracking number: %s' % (sale_id, tracking_number),
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                if(len(log) > 0):
                    for i in xrange(0, len(log)):
                        log[i]['title'] += "  %s" % sale_obj.name
                    self.log(cr, uid, settings, log)
        return res

    def integrationApiSwitchProduct(self, cr, uid, name=None, tag_fba=False, product_ids_null=None,
                                    product_ids_positive=None, select_limit=500, send_feed=False, context=None):
        if not name: name = ""
        if not product_ids_null: product_ids_null = {}
        if not product_ids_positive: product_ids_positive = []

        (settings, api) = self.getApi(cr, uid, name)
        if(api):
            log = []
            qty_res = []
            qty_res_list = []
            if product_ids_null:
                qty_res = self.integrationApiGetQTY(cr, uid, name=name, product_ids=product_ids_null.keys(), select_limit=select_limit, context={'is_gui_call': True})
                # qty_res = self.integrationApiGetQTY(cr, uid, name=name, product_ids=product_ids_null,select_limit=select_limit, context={'is_gui_call': True})
                if qty_res:
                    qty_res_list = self.integrationApiGetSkuMFN(cr, uid, qty_res=qty_res, product_ids=product_ids_null, context=None)
            sku_fba_res = self.integrationApiGetSkuFBA(cr, uid, tag_fba, product_ids=product_ids_positive, context=context)
            if qty_res_list or sku_fba_res:
                # conf_obj = self.pool.get('ir.config_parameter')
                updateqty_res = api.SwitchProduct(qty_res_list, sku_fba_res, send_feed)
                log += api.getLog()
            if log:
                self.log(cr, uid, settings, log)

    def integrationApiGetSkuFBA(self, cr, uid, tag_fba, product_ids, context=None):
        # (settings, api) = self.getApi(cr, uid, name)
        # tag_id = settings.tag_fba.id
        lines = []
        product_ids_list = []
        if (product_ids and len(product_ids) > 0):
            for item in product_ids:
                product_ids_list.append(item.product_id.id)
            products_list = ','.join([str(i) for i in product_ids_list])

            sql = """select pp.default_code as default_code, rs.name as size, pp.id as pp_product_id
            from product_multi_customer_fields as pmcf
            left join product_multi_customer_fields_name as pmcfn on pmcf.field_name_id = pmcfn.id
            left join product_product as pp on pmcf.product_id = pp.id
            left join taggings_product as tp on tp.product_id = pp.id
            left join tagging_tags as tt on tp.tag_id = tt.id
            left join product_ring_size_default_rel as prs on (pp.product_tmpl_id = prs.product_id and pmcf.size_id = prs.size_id)
            left join ring_size as rs on prs.size_id = rs.id
            where tt.id = %s and pp.id in (%s)
            and pmcfn.label = 'Fullfilment Notes'
            and pmcf.value = 'AFN'
            group by default_code, size, pp_product_id
            """ % (tag_fba, products_list)
            cr.execute(sql)
            # fba_skus = [i[0] for i in cr.fetchall()]
            products = cr.fetchall()
            if products:
                for product in products:
                    for item in product_ids:
                        if (item.product_id.id == product[2] and item.size_id.name == product[1]):
                            line = {}
                            # if (product.get('size')):
                            #     line['sku'] = str(product.get('default_code')) + '/' + str(product.get('size'))
                            # else:
                            #     line['sku'] = product.get('default_code')
                            if (product[1]):
                                line['sku'] = str(product[0]) + '/' + str(product[1])
                            else:
                                line['sku'] = product[0]
                            lines.append(line)
        return lines

    def integrationApiGetSkuMFN(self, cr, uid, qty_res, product_ids, context=None):
        lines = []
        for prod in product_ids.keys():
            for size_id in product_ids[prod]:
                if size_id == 'False':
                    size_id = None
                for item in qty_res:
                    if (item['product_id'] == prod and item['size_id'] == size_id):
                        lines.append(item)
        return lines

    def integrationsApiUpdateQTY_products(self, cr, uid, product_ids=False, context=None):
        if not product_ids:
            return False
        sale_integration_obj = self.pool.get('sale.integration')
        sql = """SELECT si.id, tp.product_id from sale_integration si
            LEFT JOIN taggings_product tp on si.tag = tp.tag_id
            WHERE 1 = 1
            AND si.inventory_product_usage = TRUE
            AND si.si_type = 'main'
            AND tp.product_id in (%s)
        """ % (','.join([str(i) for i in product_ids]))
        cr.execute(sql)
        res = cr.dictfetchall()

        settings = {}
        for item in res:
            setting_id = item.get('id')
            if setting_id not in settings:
                settings[setting_id] = [item.get('product_id')]
            else:
                settings[setting_id].append(item.get('product_id'))

        for setting_id in settings:
            if not settings[setting_id]:
                continue
            setting = sale_integration_obj.browse(cr, uid, setting_id)
            self.integrationApiUpdateQTY(cr, uid, setting.name, product_ids=settings[setting_id], mode=None)

    def generateDataInventory(self, cr, uid, cust_name="", settings_id=False):

        data = ""

        sql = """SELECT xml
        FROM public.sale_integration_{type_api}_xml
        WHERE send = 1 AND sale_integration_id = {sale_integration_id};
        """.format(type_api=cust_name, sale_integration_id=settings_id)

        print sql
        cr.execute(sql)
        xml_lines = cr.dictfetchall()

        if xml_lines:
            for xml_line in xml_lines:
                data += str(xml_line["xml"]) + "\n\n"
        if data:
            return data
        return False

    # DELMAR-7
    # store inventory file in csv (human readable) format
    @staticmethod
    def store_readable_inventory_file(api, good_lines):
        if not good_lines or not api._readable_inventory_filename:
            return False
        logger.debug("Storing readable file %s" % (api._readable_inventory_filename))
        try:
            with backup_file_open(api._readable_inventory_filename, 'wb') as f:
                quote_style = csv.QUOTE_ALL
                csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_ALL)
                writer = csv.writer(f, dialect='csv', quoting=quote_style)
                # write headers (dict keys)
                header_row = dict(good_lines[0]).keys()
                writer.writerow(
                    [isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in header_row])
                # iterate lines
                for line in good_lines:
                    row = dict(line).values()
                    try:
                        writer.writerow(
                            [isinstance(el, unicode) and el.encode('utf-8') or str(el).encode('utf-8') for el in row])
                    except Exception, e:
                        logger.warning("Cannot write line into readable inventory: %s" % e.message)
                        continue
        except Exception, e:
            logger.warning("Cannot open file to store readable inventory: %s" % e.message)
            pass
        return True
    # END DELMAR-7

    def store_inventory_to_db(self, cr, uid, settings, base_lines=None, lines=None, limit=1, by_product_usage=False, context=None):
        if context is None:
                context = {}
        context.update({
            'write_log': True,
        })
        queryes = {
            'query_check_exist_table': "SELECT * FROM pg_tables WHERE tablename = '{0}';",
            'query_create_table': 'CREATE TABLE {0} (LIKE si_last_inv INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES);',
            'query_add_trigger': "select create_trigger_for_si_last_inventory('{0}');",
            'query_insert_to_table': "INSERT INTO {0} (id, product_id, default_code, size_id, size, qty, phantom_inventory, force_zero, actual_qty, qty_tolerance, send, create_date, by_product_usage) VALUES (DEFAULT, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
        }
        result = None
        log = []
        name = str(settings.customer_ids[0].ref).strip().lower()
        table_name = "si_last_inv_%s" % name
        now_date = datetime.now()

        def chunks(iterable, count=1):
            result = []
            if((count == 1) or (count > len(iterable))):
                result = iterable
            else:
                curr_count = 0
                curr_iterable = []
                for row in iterable:
                    curr_count += 1
                    if((curr_count - 1) == count):
                        result.append(curr_iterable)
                        curr_iterable = []
                        curr_count = 1
                    curr_iterable.append(row)
                if(len(curr_iterable) > 0):
                    result.append(curr_iterable)
            return result

        def prepare_query(query, params):
            return query.format(params)

        try:
            _queryes = {}
            for _name in queryes:
                _queryes[_name] = prepare_query(queryes[_name], table_name)
                print(_name, _queryes[_name])
            queryes = _queryes

            cr.execute(queryes['query_check_exist_table'])
            check_result = cr.dictfetchone()
            if(not check_result):
                cr.execute(queryes['query_create_table'])
                cr.execute(queryes['query_add_trigger'])
            raw_values_to_db = []
            if(name and base_lines and lines):
                for base_line in base_lines:
                    send = False
                    for line in lines:
                        if(line == base_line):
                            send = True
                            break
                    value_list = []
                    value_list.append(base_line.get('product_id', False))
                    value_list.append(base_line.get('sku', ''))
                    value_list.append(base_line.get('size_id', False) or None)
                    value_list.append(base_line.get('size', ''))
                    value_list.append(base_line.get('qty', False) or 0)
                    value_list.append(base_line.get('phantom_inventory', False) or 0)
                    value_list.append(base_line.get('force_zero', False) or 0)
                    value_list.append(base_line.get('actual_qty', False) or 0)
                    value_list.append(base_line.get('qty_tolerance', False) or 0)
                    value_list.append(send)
                    value_list.append("{:%Y-%m-%d %H:%M:%S}".format(now_date))
                    value_list.append(bool(by_product_usage))
                    value_list = tuple(value_list)
                    if(value_list not in raw_values_to_db):
                        raw_values_to_db.append(value_list)
                values_to_db = chunks(raw_values_to_db, limit)
                values = []
                for values in values_to_db:
                    if (isinstance(values, list)):
                        cr.executemany(queryes['query_insert_to_table'], tuple(values))
                    else:
                        cr.execute(queryes['query_insert_to_table'], tuple(values))
                result = True
            else:
                result = False
        except Exception, _error:
            logger.error(_error)
            log.append({
                'title': 'store_inventory_to_db: {0}'.format(name),
                'msg': 'ERROR: {0}'.format(str(_error)),
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
        if(log):
            self.log(cr, uid, settings, log)
        return result

    def integrationApiUploadFeed(self, cr, uid, name="", limit=None, request_limit=None, context=None):
        settings, api = self.getApi(cr, uid, name)
        if not api:
            return False
        if not limit or limit < 0:
            limit = 2000
        if not request_limit or request_limit < 0:
            request_limit = 50
        cr.execute("""SELECT *
            FROM sale_integration_{type_api}_xml
            WHERE (response <> 'Success' OR response is NULL)
            AND sale_integration_id = {settings_id}
            order by id ASC
            LIMIT {limit}
        """.format(type_api=settings.type_api, settings_id=settings.id, limit=limit))
        lines = cr.dictfetchall()

        def chunks(l, n):
            """Yield successive n-sized chunks from l."""
            for i in xrange(0, len(l), n):
                yield l[i:i+n]

        for lines_chunk in chunks(lines, request_limit):
            result = api.uploadFeed(lines_chunk)
            if not result:
                continue

            ## Uncomment to store file on inventory management
            # self.save_inventory_history(cr, uid, api, settings)

            ids = [line['id'] for line in lines_chunk]
            if not ids:
                continue
            cr.execute(
                """UPDATE sale_integration_{type_api}_xml
                    SET response = 'Success'
                    WHERE id in %s
                """.format(type_api=settings.type_api,),
                (tuple(ids),)
            )
        return True

    @send_mails_from_result
    def integrationApiUpdateQTY(self, cr, uid, name="", select_limit=500, product_ids=False, mode=None, context=None):
        result = {
            'object': None,
            'return_result': False,
            'settings': None,
        }
        (settings, api) = self.getApi(cr, uid, name)
        if(api):

            if context is None:
                context = {}

            context.update({
                'write_log': True,
                'return_settings': True,
            })

            if mode is None:
                mode = settings.mode

            log = []
            if settings.deny_partial_inventory:
                product_ids = None
                select_limit = -1
            inventory = self.integrationApiGetQTY(cr, uid, name=name, select_limit=select_limit, product_ids=product_ids, context=context)

            if inventory:
                (lines, current_setting) = inventory
                lines = self.recalculateDNS(cr, uid, lines)

                if lines and "customer_price" in lines[0] and settings.use_discounts:
                    lines = self.recalculate_discounts(cr, uid, lines, settings)
                if lines:
                    if product_ids:
                        updateqty_res = api.updateQTYPartial(lines, mode=mode)
                    else:
                        updateqty_res = api.updateQTY(lines, mode=mode)

                    log += api.getLog()

                    if isinstance(updateqty_res, dict):
                        _good_lines = updateqty_res['good']
                        self.store_inventory_to_db(cr, uid, settings, lines, _good_lines, limit=100, by_product_usage=bool(product_ids))

                    if settings.type_api not in ("walmart"):
                        self.write(cr, uid, settings.id, {'inventory_setting': str(current_setting)})

                    if settings.type_api in ("sterling", "overstock", "overstock_sofs"):
                        data_inventory = self.generateDataInventory(cr, uid, settings.type_api, settings.id)
                        if data_inventory:
                            api.generateInventoryFile(data_inventory)
                        if not product_ids:
                            not_processed_row = False
                            if settings.type_api in ['overstock', 'overstock_sofs']:
                                cr.execute("""SELECT id from sale_integration_{type_api}_xml
                                    where sale_integration_id <> {sale_integration_id}
                                    and (response <> 'Success' or response is null)
                                    limit 1
                                    """.format(type_api=settings.type_api, sale_integration_id=settings.id))
                                not_processed_row = cr.fetchone()
                            if not not_processed_row:
                                cr.execute("TRUNCATE TABLE sale_integration_%s_xml;" % settings.type_api)
                            else:
                                cr.execute("""DELETE FROM sale_integration_{type_api}_xml
                                    where sale_integration_id = {sale_integration_id}
                                    """.format(type_api=settings.type_api, sale_integration_id=settings.id))
                        _good_lines = []
                        main_sql = """INSERT INTO
                                sale_integration_{type_api}_xml
                                (
                                    create_date, send, delmar_id, customer_sku, size,
                                    phantom_inventory, force_zero, status,
                                    qty, actual_qty, xml, sale_integration_id, product_id
                                )
                            VALUES\n""".format(type_api=settings.type_api)
                        sub_sqls = []
                        for updateqty_line in updateqty_res:
                            _good_lines.append(updateqty_line['line'])
                            sub_sql = """(
                                            (now() at time zone 'UTC'), {send}, '{delmar_id}', '{customer_sku}', '{size}',
                                            {phantom_inventory}, {force_zero}, {status},
                                            {qty}, {actual_qty}, '{xml}', {sale_integration_id}, {product_id}
                                        )""".format(
                                send=0,
                                delmar_id=updateqty_line['line']['sku'],
                                customer_sku=updateqty_line['line']['customer_sku'],
                                size=updateqty_line['line']['size'],
                                phantom_inventory=updateqty_line['line'].get('phantom_inventory', None) or 0,
                                force_zero=updateqty_line['line'].get('force_zero', None) or 0,
                                status=updateqty_line['line']['qty'] > 0 and 1 or 0,
                                qty=updateqty_line['line']['qty'],
                                actual_qty=updateqty_line['line']['actual_qty'],
                                xml=updateqty_line['xml'],
                                sale_integration_id=settings.id,
                                product_id=updateqty_line['line']['product_id']
                            )
                            sub_sqls.append(sub_sql)
                        main_sql += ",\n".join(sub_sqls) + ';'
                        if (updateqty_res):
                            cr.execute(main_sql)

                    # DELMAR-7
                    # store readable inventory file
                    if '_good_lines' in locals() and settings.generate_readable_inventory:
                        logger.debug("Good revision lines for common: %s" % _good_lines)
                        self.store_readable_inventory_file(api, _good_lines)
                    # END DELMAR-7

                    self.save_inventory_history(cr, uid, api, settings)
                    result.update({'object': api, 'settings': settings})

            if log:
                self.log(cr, uid, settings, log)

            result.update({'return_result': True})
            return result

    def save_inventory_history(self, cr, uid, api, settings):
        if hasattr(api, '_filename_inventory'):
            if api._filename_inventory:
                history_vals = {
                    'type_api_id': settings.id,
                    'path_to_file': os.path.dirname(api._filename_inventory),
                    'filename': os.path.basename(api._filename_inventory),
                    'full_filename': api._filename_inventory
                }
                if api._readable_inventory_filename:
                    history_vals.update({
                        'filename_readable': os.path.basename(api._readable_inventory_filename),
                        'file_readable': api._readable_inventory_filename,
                    })
                self.pool.get('history.sent.inventory').create(cr, uid, history_vals)

    def integtationApiGetCsvAtach(self, cr, uid, name="", product_ids=None, select_limit=-1, context=None):

        if context is None:
            context = {
                'write_log': False,
                'is_gui_call': True,
            }

        data = False
        (settings, api) = self.getApi(cr, uid, name)
        if(api):

            context.update({
                'return_settings': False,
                })

            lines = self.integrationApiGetQTY(cr, uid, name=name, select_limit=select_limit, product_ids=product_ids, context=context)
            if lines:
                lines = self.recalculateDNS(cr, uid, lines)
                if lines and "customer_price" in lines[0] and settings.use_discounts:
                    lines = self.recalculate_discounts(cr, uid, lines, settings)
                data = api.getAttachData(lines)
        return data

    def update_showroom_tag(self, cr, uid, tag_id):
        insert_query = """WITH sr_locs (id) AS (
                SELECT id FROM stock_location
                WHERE complete_name LIKE '% CAMTLSR %'
            )
            INSERT INTO taggings_product (product_id, tag_id)
            SELECT DISTINCT sm.product_id, {tag_id}
            FROM sr_locs
            LEFT JOIN stock_move sm
                ON sm.location_dest_id = sr_locs.id
            LEFT JOIN taggings_product tp
                ON tp.tag_id = {tag_id}
                AND tp.product_id = sm.product_id
            WHERE sm.product_id IS not null
                AND tp.product_id IS null
        """.format(tag_id=tag_id)
        cr.execute(insert_query)

    def integrationApiGetQTY(self, cr, uid, name="", select_limit=500, product_ids=False, return_only_qty=False, context=None):
        if context is None:
            context = {}

        def its_etl_time():
            import pytz
            time_now = datetime.now(pytz.timezone('Canada/Eastern'))
            if time_now.hour > 0 and time_now.hour < 7:
                return True
            if time_now.hour == 0 and time_now.minute >= 20:
                return True
            return False

        if product_ids and isinstance(product_ids, (int, long)):
            product_ids = [product_ids]

        write_log = context.get('write_log', False)
        (settings, api) = self.getApi(cr, uid, name)

        if(settings):
            test_start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())

            lines = []
            log = []

            if select_limit == 500 and settings.select_limit != 0:
                select_limit = settings.select_limit
            limit_str = ''
            if select_limit > 0:
                limit_str = 'limit %s' % (select_limit)

            tag_id = settings.tag.id

            settings_obj = settings.inventory_setting
            current_setting = {'last_id': '0', 'start_date': int(datetime.now().strftime('%s'))}
            if settings_obj and settings_obj != '0':
                current_setting = eval(settings_obj)

            for customer in settings.customer_ids:
                if len(settings.customer_ids) > 1 and not customer.sale_integration_main:
                    # For "family" customers need use one customer with main flag
                    continue

                if tag_id and customer.ref == 'SRM':
                    self.update_showroom_tag(cr, uid, tag_id)

                location_ids = [str(x.id) for x in customer.location_ids]

                if(settings.update_sec_locations):
                    if(customer.sec_location_ids):
                        location_ids += [str(x.id) for x in customer.sec_location_ids]

                if location_ids:
                    location_ids_str = ','.join(location_ids)

                    current_date = int((datetime.now() + timedelta(minutes=10)).strftime("%s"))
                    start_date = int(current_setting.get('start_date', int(datetime.now().strftime('%s'))))
                    try:
                        amazon_timedelta = int(self.pool.get('ir.config_parameter').get_param(cr, uid, 'amazon.update_qty.timedelta'))
                    except:
                        amazon_timedelta = 24
                    is_gui_call = context and context.get('is_gui_call', False)
                    if (start_date > current_date and not is_gui_call and settings.type_api in ("amazon")):
                        print "Update QTY Skip %s" % (customer.external_customer_id)
                        print "start_date: %s " % (datetime.fromtimestamp(start_date).strftime('%Y-%m-%d %H:%M:%S'))
                        print "current_date: %s " % (datetime.fromtimestamp(current_date).strftime('%Y-%m-%d %H:%M:%S'))

                        return []

                    # get last id from tag
                    if not product_ids:
                        if not tag_id:
                            log.append({
                                'title': 'UpdateQTY: %s' % name,
                                'msg': 'Tag is not specified',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                            self.log(cr, uid, settings, log)
                            return []
                        else:
                            last_id_sql = """
                                select max(product_id) max_last_id, count(product_id)
                                from taggings_product
                                where tag_id = %s
                            """ % (tag_id)
                            cr.execute(last_id_sql)
                            last_id_sql_res = cr.dictfetchone()
                            if not last_id_sql_res['max_last_id']:
                                log.append({
                                    'title': 'UpdateQTY: tag(%s)' % settings.tag.name,
                                    'msg': 'Tag is empty',
                                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                                })
                                self.log(cr, uid, settings, log)
                                return []

                    last_id = current_setting.get('last_id', '0')
                    pmcf_left_join_sql = ''
                    pmcf_select_section = ''
                    customer_field_list = []
                    customer_field_list_sort = []
                    customer_field_list_order = ['phantom_inventory', 'size_tolerance', 'min_qty', 'force_zero']
                    calculate_price_flag = False

                    if settings.customer_field_ids:
                        for pmcf in settings.customer_field_ids:
                            pmcf_data = {
                                'label': pmcf.label,
                                'name': pmcf.name_id.name,
                                'expand_ring_sizes': pmcf.expand_ring_sizes,
                                'field_name_id': pmcf.id,
                            }
                            if pmcf.name_id.name in customer_field_list_order:
                                customer_field_list_sort.append(pmcf_data)
                            else:
                                customer_field_list.append(pmcf_data)

                            if price_list_field_name == pmcf.label:
                                calculate_price_flag = True

                        customer_field_list_sort.sort(key=lambda (x): customer_field_list_order.index(x['name']))
                        customer_field_list.extend(customer_field_list_sort)
                        print customer_field_list

                    if(settings.type_api == 'magento'):
                            customer_field = settings.customer_field
                            if customer_field and customer_field != 'None':
                                customer_field_list.append({
                                    'label': customer_field,
                                    'name': customer_field.replace(' ', '_'),
                                    'expand_ring_sizes': True,
                                })

                    if is_gui_call:
                        locked_size = {
                            'label': 'Locked Size Tolerance',
                            'name': 'locked_size_tolerance',
                            'expand_ring_sizes': False,
                        }
                        if not next((x for x in customer_field_list if x['name'] == locked_size['name']), None):
                            customer_field_list.append(locked_size)

                    # Fill todo list to price formula customer fields update
                    if(calculate_price_flag):
                        price_calculate_todo = []

                        if product_ids:
                            products_sql = """SELECT pp.id, array_to_string(array_agg(rel.size_id), ',') as sizes
                            FROM product_product as pp
                            LEFT JOIN product_template as pt on pt.id = pp.product_tmpl_id
                            LEFT JOIN product_ring_size_default_rel as rel on rel.product_id = pt.id
                            WHERE pp.id in %(product_ids)s
                            GROUP BY pp.id
                            """
                            product_params = {
                                'product_ids': tuple(product_ids)
                            }
                        else:
                            products_sql = """SELECT tp.product_id, array_to_string(array_agg(rel.size_id), ',') as sizes
                                FROM taggings_product as tp
                                LEFT JOIN product_product as pp on pp.id = tp.product_id
                                LEFT JOIN product_template as pt on pt.id = pp.product_tmpl_id
                                LEFT JOIN product_ring_size_default_rel as rel on rel.product_id = pt.id
                                WHERE tp.tag_id = %(tag_id)s
                                GROUP BY tp.product_id
                            """
                            product_params = {
                                'tag_id': tag_id
                            }
                        cr.execute(products_sql, product_params)
                        products_data = cr.fetchall()
                        if products_data:
                            for line in products_data:
                                if line[1]:
                                    # If has size
                                    price_calculate_todo.append([line[0], json_loads('['+line[1]+']')])
                                else:
                                    # If have no size
                                    price_calculate_todo.append(line[0])

                        if price_calculate_todo:
                            self.pool.get('pricelist.callbacks').calculate_pricelists(
                                cr, uid,
                                context={
                                    'profile_it': False,
                                    'store_to_customer_fields': True,
                                    'overwrite': True,
                                    'return_result': False,
                                    'fill_result_step_by_step': False,
                                    'fill_profits': False,
                                    'erp_export': False,
                                    'field_name_id': False,
                                    'data': {customer.id: price_calculate_todo},
                                }
                            )

                    # generate product multi customer sql join
                    for pmc_field in customer_field_list:
                        pmcf_select_section += 'trim(pmcf_{label}.value) as {label}, '.format(label=pmc_field['name'])
                        if_expand_ring_size = '--'
                        if pmc_field['expand_ring_sizes']:
                            if_expand_ring_size = ''

                        field_name_id = pmc_field.get('field_name_id')
                        if not field_name_id:
                            field_name_id = """(
                                select fn.id
                                from product_multi_customer_fields_name fn
                                where fn.label = '{field_label}'
                                and customer_id = {customer_id}
                                limit 1
                            )""".format(
                                field_label=pmc_field['label'],
                                customer_id=customer.id
                            )
                        pmcf_left_join_sql += """left join product_multi_customer_fields pmcf_{label_in_delimeted}
                            on pmcf_{label_in_delimeted}.product_id = limit_p.product_id
                            {if_expand_ring_size} and (coalesce(pmcf_{label_in_delimeted}.size_id, 0) = coalesce(limit_p.size_id, 0))
                            and pmcf_{label_in_delimeted}.field_name_id = {field_name_id}
                        """.format(
                            label_in_delimeted=pmc_field['name'],
                            if_expand_ring_size=if_expand_ring_size,
                            field_name_id=field_name_id
                        )

                    # All products from tag (regular)
                    if not product_ids:
                        tag_sql = """ left join taggings_product tp on tp.product_id = p.id
                                where p.id > %s
                                and tp.tag_id = %s
                                order by p.id
                                %s """ % (last_id, tag_id, limit_str)
                    # Only products from order (after check availability)
                    else:
                        tag_sql = """ where p.id in (%s)
                        order by p.id """ % (','.join([str(i) for i in product_ids]))

                    # get product_id, delmar_id, size, customer_field_names, qty
                    sql = """SELECT DISTINCT
                            tmpl.name as description,
                            limit_p.product_id,
                            limit_p.default_code,
                            limit_p.size_id,
                            {pmcf_select_section}
                            coalesce(qty_p.init_qty::FLOAT8,  0) as init_qty,                 
                            coalesce(qty_p.init_qty::FLOAT8, 0) + (case when coalesce(qty_p.init_qty::FLOAT8, 0)>0 then coalesce(qty_sams.sams_qty::FLOAT8,0) else 0 end)  - case
                                when coalesce(qty_r.reduce_qty::FLOAT8, 0) > coalesce(qty_o.ordered_qty::FLOAT8, 0)
                                then coalesce(qty_r.reduce_qty::FLOAT8, 0) - coalesce(qty_o.ordered_qty::FLOAT8, 0)
                                else 0
                            end as sum_qty,
                            coalesce(qty_r.reduce_qty::FLOAT8, 0) as reduce_qty,
                            coalesce(qty_o.ordered_qty::FLOAT8, 0) as ordered_qty,
                            coalesce(qty_p.uschp_qty::FLOAT8, 0) - case
                                when coalesce(qty_r.reduce_qty::FLOAT8, 0) > coalesce(qty_o.ordered_qty_uschp::FLOAT8, 0)
                                then coalesce(qty_r.reduce_qty, 0::FLOAT8) - coalesce(qty_o.ordered_qty_uschp::FLOAT8, 0)
                                else 0
                            end as sum_qty_uschp,
                            coalesce(qty_p.uschp_qty::FLOAT8, 0) as uschp_qty,
                            qty_o.po_eta as po_eta,
                            qty_o.po_date_ordered as po_date_ordered,
                            case when rs.name ~ '^[\.0-9]+$' THEN rs.name::float else 0.0 end as size_number,
                            sets_virtual.qty as sets_virtual_qty,
                            sets_virtual2.qty as vqty
                        from (
                            SELECT p.default_code, prs.size_id, p.id as product_id, p.product_tmpl_id
                            from product_ring_size_default_rel prs
                            left join
                        (
                            SELECT p.id, p.product_tmpl_id, p.default_code
                            from product_product p
                            {tag_sql}
                        ) p on prs.product_id = p.product_tmpl_id
                        where p.product_tmpl_id is not null
                        union
                        SELECT p.default_code, prs.size_id, p.id as product_id, p.product_tmpl_id
                        from product_ring_size_default_rel prs
                        right join
                        (
                            SELECT p.id, p.product_tmpl_id, p.default_code
                            from product_product p
                            {tag_sql}
                        ) p on prs.product_id = p.product_tmpl_id
                        where prs.size_id is null
                        order by product_id
                        ) limit_p

                        left join (SELECT product_id as id, (SELECT id FROM product_product pp WHERE default_code=concat('',pmcf_alt_delmar_id.value,'')
                                   and type='product' ) alt_id from  product_multi_customer_fields pmcf_alt_delmar_id
                                   where   pmcf_alt_delmar_id.field_name_id = (
                                   SELECT fn.id
                                    from product_multi_customer_fields_name fn
                                   where fn.label = 'Alt Delmar ID'
                                   and customer_id = {customer_id}
                                  limit 1))alt_p
                        on alt_p.id=limit_p.product_id

                        {pmcf_left_join_sql}
                        left join
                        (
                            SELECT
                                a.product_id, a.size_id,
                                round(sum(a.qty)*{percente_max}) as init_qty,
                                sum(a.uschp_qty) as uschp_qty
                            FROM (
                                SELECT sm.product_id,
                                    sm.size_id,
                                    CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END as location_dest_id,
                                    sm.bin,
                                    sm.bin_id,
                                    sum(sm.product_qty * (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN -1 ELSE 1 END)) as qty,
                                    sum(sm.product_qty * (CASE WHEN sl.warehouse_id=15  THEN 1
                                    WHEN sl2.warehouse_id=15  and sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') then -1 ELSE 0 END)) as uschp_qty
                                FROM stock_move sm
                                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                                    LEFT JOIN stock_location sl2 ON sl2.id = sm.location_id
                                WHERE sm.state IN ('done', 'assigned', 'reserved')
                                    AND sm.product_id in (
                                    SELECT p.id
                                    from product_product p
                                    {tag_sql})
                                    AND (  sm.location_dest_id in ({location_ids_str}) OR  sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') )
                                GROUP BY (CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END),
                                    sm.bin,
                                    sm.bin_id,
                                    sm.product_id,
                                    sm.size_id
                                ORDER BY CASE WHEN sl.name IN ('Inventory loss', 'Customers', 'Output', 'EXCP') THEN sm.location_id ELSE sm.location_dest_id END
                            ) a
                            WHERE 1=1
                            AND a.location_dest_id in ({location_ids_str})
                            GROUP BY a.product_id, a.size_id
                            ORDER BY a.product_id
                        ) qty_p on
                        (
                            alt_p.id is null and qty_p.product_id = limit_p.product_id or
                            alt_p.id is not null and qty_p.product_id = alt_p.alt_id
                        )
                        and ( coalesce(qty_p.size_id, 0) = coalesce(limit_p.size_id, 0))
                        LEFT JOIN (
                            select smr.product_id, smr.size_id,
                                sum(smr.product_qty) as reduce_qty
                            from stock_move smr
                                left join stock_location slr on smr.location_dest_id = slr.id
                                left join stock_picking spr on smr.picking_id = spr.id and spr.type = 'out'
                                left join res_partner rpr on spr.real_partner_id = rpr.id
                            where smr.state = 'confirmed'
                                and slr.name = 'EXCP'
                                and spr.state in ('confirmed', 'back', 'waiting_split')
                                and spr.id is not null
                                and rpr.id in (
                                    select cslr.customer_id
                                    from customer_sec_location_relation cslr
                                    where cslr.location_id in ({location_ids_str})
                                    union
                                    select clr.customer_id
                                    from customer_location_relation clr
                                    where clr.location_id in ({location_ids_str})
                                )
                            group by smr.product_id, smr.size_id
                        ) qty_r on
                        (
                            alt_p.id is null and qty_r.product_id = limit_p.product_id or
                            alt_p.id is not null and qty_r.product_id = alt_p.alt_id
                        )
                        and ( coalesce(qty_r.size_id, 0) = coalesce(limit_p.size_id, 0))
                        LEFT JOIN (
                            -- this need calculate with receiving
select product_id, size_id,
sum(product_qty) as ordered_qty,
min(eta)::DATE as po_eta,
min(pol.date_ordered)::DATE as po_date_ordered,
sum(case when (sl.warehouse_id=15) then product_qty else 0 end) as ordered_qty_uschp
from purchase_order_line as pol
left outer join purchase_order po on po.id = pol.order_id
left join stock_location as sl on sl.id = po.location_id
where 
(lower(pol.status) in ('ordered','shipped','invoiced','partial') or 
(lower(pol.state)='created' and lower(pol.status)='new'))
group by product_id, size_id
                        ) qty_o on
                        (
                            alt_p.id is null and qty_o.product_id = limit_p.product_id or
                            alt_p.id is not null and qty_o.product_id = alt_p.alt_id
                        )
                        and ( coalesce(qty_o.size_id, 0) = coalesce(limit_p.size_id, 0))
                        LEFT JOIN size_tolerance_based_qty_for_sets sets_virtual
                            ON limit_p.product_id = sets_virtual.product_id
                            AND coalesce(limit_p.size_id, 0) = coalesce(sets_virtual.size_id, 0)
                            AND sets_virtual.location_id in ({location_ids_str})
                        LEFT JOIN product_template tmpl on tmpl.id = limit_p.product_tmpl_id
                        LEFT JOIN ring_size rs on rs.id = limit_p.size_id

                        LEFT JOIN sets_virtual_qty_from_all_loc sets_virtual2
                            ON limit_p.product_id = sets_virtual2.product_id
                            and  coalesce(limit_p.SIZE_ID,0) =  coalesce(sets_virtual2.SIZE_id,0)
                            and sets_virtual2.customer_id = {cust_id}
                        left join(
                                select
                                sp.product_id,
                                sp.size_id,
                                sum( sp.product_qty ) as sams_qty
                            from
                                (
                                    (
                                        select
                                            sp.real_partner_id,
                                            sp.state,
                                            sm.product_id,
                                            sm.size_id,
                                            sm.product_qty
                                        from
                                            stock_picking sp inner join stock_move sm on
                                            sm.set_parent_order_id = sp.id
                                    )
                            union all(
                                    select
                                        sp.real_partner_id,
                                        sp.state,
                                        sm.product_id,
                                        sm.size_id,
                                        sm.product_qty
                                    from
                                        stock_picking sp inner join stock_move sm on
                                        sm.picking_id = sp.id
                                )
                                ) sp left join res_partner rpr on
                                sp.real_partner_id = rpr.id
                            where
                                sp.state in(
                                    'assigned',
                                    'picking',
                                    'shipped',
                                    'incode_except',
                                    'outcode_except',
                                    'bad_format_tracking'
                                )
                                and rpr."ref" = 'SAMS'
                                and rpr.id = {cust_id}
                            group by
                                sp.product_id,
                                sp.size_id

                        ) qty_sams on
                        (
                            alt_p.id is null
                            and qty_sams.product_id = limit_p.product_id
                            or alt_p.id is not null
                            and qty_sams.product_id = alt_p.alt_id
                        )
                        and(
                            coalesce(
                                qty_sams.size_id,
                                0
                            )= coalesce(
                                limit_p.size_id,
                                0
                            )
                        )
                        ORDER BY limit_p.default_code, size_number
                    """.format(
                        pmcf_select_section=pmcf_select_section,
                        tag_sql=tag_sql,
                        customer_id=customer.id,
                        pmcf_left_join_sql=pmcf_left_join_sql,
                        location_ids_str=location_ids_str,
                        cust_id=customer.id,
                        percente_max=customer.max_inv*0.01 or 1
                    )
                    logger.debug(sql)

                    #if 1 == uid and not its_etl_time():
                    #    server_obj = self.pool.get('fetchdb.server')
                    #    server_id = server_obj.get_mirror_servers(cr, uid, single=True)
                    #    if not server_id:
                    #        raise osv.except_osv('Warning', 'Remote server is not available.')
                    #    pssql_conn = server_obj.connect(cr, uid, server_id)
#                        curs = pssql_conn.cursor()
                    #    res = server_obj.make_query(
                    #        cr,
                    #        uid,
                    #        server_id,
                    #        sql,
                    #        params={},
                    #        select=True,
                    #        commit=False,
                    #        connection=False,
                    #        cursor=False,
                    #        multi=False,
                    #        return_dict=True
                    #    )
                    #    pssql_conn.close()
 #                      curs.close()
	     	    #else:
                    cr.execute(sql)
                    res = cr.dictfetchall()

                    if res:
                        size_map = self.pool.get("ring.size").get_map_id_name(cr, uid)
                        send_without_size = {}
                        res_length = len(res)

                        prev_qty = {}
                        if api.apierp_is_available('get_prev_qtys'):
                            prev_qty = api.apierp.get_prev_qtys(cr, uid, settings)

                        for index, item in enumerate(res):
                            if (settings.name.lower() in ("amazon_ds", "amazon_di")):
                                if (item.get('fulfillment_notes', False)):
                                    fullfilment = item.get('fulfillment_notes', False)
                                    if fullfilment and fullfilment.lower() != 'mfn':
                                        continue
                                else:
                                    continue
                            send_without_size_skip = False
                            line = {}
                            size_id = item.get('size_id', False)
                            size = ""
                            default_code = item.get('default_code')
                            if size_id:
                                size = size_map[size_id]
                                if customer.external_customer_id in ['sears', 'searscanada']:
                                    merchant_size = size
                                    try:
                                        merchant_size = '{0:.1f}'.format(float(merchant_size))
                                    except:
                                        pass
                                    merchant_size = merchant_size.replace('.', '')
                                    line['merchant_size'] = ('000' + merchant_size)[-3:]

                                if (settings.type_api == "commercehub" and size.find('.0') != -1):
                                    size = size[:size.find('.0')]
                                delimiter = '/'
                                if customer.external_customer_id == 'searscanada':
                                    delimiter = '-'
                                line["sku"] = str(default_code) + delimiter + str(size)
                            else:
                                line["sku"] = default_code
                            line["size"] = size

                            line['default_code'] = default_code
                            line["description"] = item.get('description', 'empty name')
                            if(not isinstance(item.get('description'), unicode)):
                                line["description"] = str(item.get('description', 'empty name'))
                            line["qty"] = 0 if int(item.get('sum_qty')) < 0 else int(item.get('sum_qty'))
                            line["reduce_qty"] = item["reduce_qty"]
                            line["ordered_qty"] = item["ordered_qty"]
                            line["uschp_qty"] = item["uschp_qty"]
                            line["po_eta"] = item["po_eta"]
                            line["po_date_ordered"] = item["po_date_ordered"]
                            line["actual_qty"] = line["qty"]
                            line["init_qty"] = item["init_qty"]
                            line["partner_id"] = customer.id
                            line["customer_id"] = str(customer.external_customer_id)
                            line["product_id"] = item["product_id"]
                            line["size_id"] = size_id or False

                            line["previous_qty"] = prev_qty.get(item.get('customer_id_delmar')) or 0
                            # if it's a set with virtual qty by tolerance take it as base
                            if settings.use_set_size_tolerance and item["sets_virtual_qty"] and line["qty"] == 0:
                                line["qty"] += int(item["sets_virtual_qty"])
                                line["qty_tolerance"] = int(item["sets_virtual_qty"])
                            else:
                                line["qty_tolerance"] = 0

                            if item.get('vqty'):
                                line["qty"] += item.get('vqty')

                            for pmc_field in customer_field_list:
                                label_in_delimited = pmc_field['name']

                                line[label_in_delimited] = item.get(label_in_delimited, '')

                                if (label_in_delimited == 'phantom_inventory' and line['phantom_inventory']):
                                    line['qty'] = line['qty'] + int(line['phantom_inventory'])
                                if (label_in_delimited == 'min_qty' and line['min_qty'] and int(float(line['min_qty'])) and line['qty'] > 0 and line['qty'] < int(float(line['min_qty']))):
                                    line['qty'] = int(float(line['min_qty']))
                                if (label_in_delimited == 'force_zero' and line['force_zero'] and int(line['force_zero'])):
                                    line['qty'] = 0
                                    line["qty_tolerance"] = 0
                                if (line['size_id'] and label_in_delimited == 'size_tolerance' and line['size_tolerance']):
                                    size_tolerance = 0.0
                                    base_size = item.get('size_number', False)

                                    if not (line.get('send_without_size', False) and int(line['send_without_size']) == 1):
                                        try:
                                            size_tolerance = float(line['size_tolerance'].replace(',', '.'))
                                        except:
                                            pass

                                    line['size_tolerance'] = size_tolerance
                                    if base_size and size_tolerance and line['qty'] == 0:
                                        min_size = base_size - size_tolerance
                                        max_size = base_size + size_tolerance
                                        # i = index - 1
                                        used_size_tolerance = []
                                        size_tolerance_array = range(0, res_length)
                                        size_tolerance_array.remove(index)
                                        # i = size_tolerance_array[0] THIS IS BUG!!!!
                                        for i in size_tolerance_array:
                                            if res[i]['product_id'] == line['product_id'] and min_size <= res[i]['size_number'] and max_size >= res[i]['size_number'] \
                                                    and res[i]['size_number'] != item['size_number'] and res[i]['size_number'] not in used_size_tolerance:
                                                used_size_tolerance.append(res[i]['size_number'])
                                                if res[i].get('force_zero', False) and res[i]['force_zero'] == 1:
                                                    continue
                                                line['qty'] += 0 if int(res[i].get('sum_qty')) < 0 else int(
                                                    res[i].get('sum_qty'))
                                                line["qty_tolerance"] += 0 if int(res[i].get('sum_qty')) < 0 else int(
                                                    res[i].get('sum_qty'))
                                                if line['qty'] > 0:
                                                    line['qty'] = 1
                                                    line['qty_tolerance'] = 1
                                                    break
                                            # i -= 1
                                        # if line['qty'] == 0:
                                        # i = index + 1
                                        # used_size_tolerance = []
                                        # while i < res_length and res[i]['product_id'] == line['product_id'] and min_size <= res[i]['size_number'] and max_size >= res[i]['size_number']:
                                        #     if res[i]['size_number'] != item['size_number'] and res[i]['size_number'] not in used_size_tolerance:
                                        #         used_size_tolerance.append(res[i]['size_number'])
                                        #         if res[i].get('force_zero', False) and res[i]['force_zero'] == 1:
                                        #             continue
                                        #         line['qty'] += 0 if int(res[i].get('sum_qty')) < 0 else int(
                                        #             res[i].get('sum_qty'))
                                        #         line["qty_tolerance"] += 0 if int(res[i].get('sum_qty')) < 0 else int(
                                        #             res[i].get('sum_qty'))
                                        #     i += 1
                                if settings.type_api == "magento" and customer_field and customer_field != 'None':
                                    if(label_in_delimited == customer_field.replace(' ', '_')):
                                        line["sku"] = item.get(label_in_delimited.lower(), '')

                            if (line.get('send_without_size', False) and int(line['send_without_size']) == 1):
                                if(item.get('default_code') not in send_without_size):
                                    send_without_size[item.get('default_code')] = line
                                    send_without_size[item.get('default_code')]['eta_dates'] = []
                                else:
                                    send_without_size[item.get('default_code')]['qty'] = int(send_without_size[item.get('default_code')]['qty']) + int(line["qty"])
                                    send_without_size[item.get('default_code')]['actual_qty'] = int(send_without_size[item.get('default_code')]['actual_qty']) + int(line["actual_qty"])
                                if line.get('eta', False):
                                    try:
                                        send_without_size[item.get('default_code')]['eta_dates'].append(datetime.strptime(line['eta'], "%m/%d/%Y"))
                                    except:
                                        logger.error("Wrong ETA format: %s" % (line['eta']))
                                send_without_size_skip = True

                            # DELMAR-162
                            # Global feed zero by parameter
                            try:
                                force_zero = bool(int(self.pool.get('ir.config_parameter').get_param(cr, uid, 'global.feed.zero', 0)))
                                if force_zero or customer.force_feed_zero:
                                    line['qty'] = 0
                            except Exception, e:
                                logger.warning("Can't set feed.zero globally or for customer: %s" % e.message)
                                pass
                            # END DELMAR-162

                            if send_without_size_skip:
                                continue

                            lines.append(line)

                        if bool(send_without_size):
                            for i in send_without_size:
                                line = send_without_size[i].copy()
                                line['size_id'] = False
                                line['size_number'] = None
                                eta_dates = line.get('eta_dates', False) or []
                                line['eta'] = eta_dates and min(eta_dates).strftime("%m/%d/%Y") or False
                                lines.append(line)

                        test_end_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        if not product_ids:
                            product_ids = [i['product_id'] for i in res]
                            if (last_id_sql_res['max_last_id'] != max(product_ids)):
                                current_setting['last_id'] = max(product_ids)
                            else:
                                current_setting['last_id'] = '0'
                                current_setting['start_date'] = (datetime.now() + timedelta(hours=amazon_timedelta)).strftime("%s")

                            log.append({
                                'title': 'UpdateQTY: tag(%s)' % settings.tag.name,
                                'msg': 'count products: %s \n select limit: %s \n current_last_id: %s \n max_last id: %s \n count found products: %s \n is_gui_call: %s \n start: %s \n end: %s' % (
                                    last_id_sql_res['count'],
                                    select_limit,
                                    last_id,
                                    last_id_sql_res['max_last_id'],
                                    len(res),
                                    is_gui_call,
                                    test_start_time,
                                    test_end_time
                                ),
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                        else:
                            log.append({
                                'title': 'UpdateQTY: tag(%s) partial' % settings.tag.name,
                                'msg': 'count products: %s \n  count found products: %s \n is_gui_call: %s \n start: %s \n end: %s' % (
                                    len(product_ids),
                                    len(res),
                                    is_gui_call,
                                    test_start_time,
                                    test_end_time
                                ),
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                else:
                    log.append({
                        'title': 'UpdateQTY: tag(%s)' % settings.tag.name,
                        'msg': 'location ids empty !!!',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

            if write_log and log:
                self.log(cr, uid, settings, log)

            if return_only_qty:
                lines = [{'product_id': x['product_id'], 'qty': x['qty'], 'actual_qty': x['actual_qty'], 'size_id': x['size_id']} for x in lines]

            if context.get('return_settings', False):
                return (lines, current_setting)
            else:
                return lines

        return False

    def recalculateDNS(self, cr, uid, lines=None, context=None):
        if not lines:
            lines = []
        if not context:
            context = {}
        recalculated = []
        to_decrement_ids = set()
        send0_count = int(self.pool.get('ir.config_parameter').get_param(cr, uid, 'before_dns_send0_count')) or 1
        if lines:
            for line in lines:
                if line.has_key('dns_flag') and line.has_key('rdns_flag'):
                    to_decrement_ids.add(line['product_id'])
                    if line.get('dns_flag', 0) in (1, '1'):
                        try:
                            rdns = int(line.get('rdns_flag', 0) or 0)
                            if rdns == 1:
                                # print "DNS skip product: %s \n" % (line)
                                continue
                            else:
                                line['qty'] = 0
                        except ValueError:
                            print "There is NaN in the RDNS field for product: %s \n" % (line)
                            continue
                recalculated.append(line)

            get_customer_field_name_id_sql = """SELECT id
                FROM product_multi_customer_fields_name
                WHERE 1=1
                AND customer_id = {0}
                AND label = 'DNS'
            """.format(line['partner_id'])
            cr.execute(get_customer_field_name_id_sql)
            dns_field_name_id = cr.fetchone()

            get_customer_field_name_id_sql = """SELECT id
                FROM product_multi_customer_fields_name
                WHERE 1=1
                AND customer_id = {0}
                AND label = 'Real DNS'
            """.format(line['partner_id'])
            cr.execute(get_customer_field_name_id_sql)
            rdns_field_name_id = cr.fetchone()

            if dns_field_name_id and rdns_field_name_id:
                # For formatting SQL queries below
                format_dict = {
                    'uid': uid,
                    'partner': line['partner_id'],
                    'repeat': send0_count,
                    'dns': dns_field_name_id[0],
                    'rdns': rdns_field_name_id[0],
                }

                # Insert missing RDNS with repeat qty for products are having DNS up
                insert_missing_rdns_sql = """INSERT INTO product_multi_customer_fields (
                        create_uid, create_date, product_id, size_id, partner_id, field_name_id, "value"
                    )
                    SELECT
                        {uid}, (now() at time zone 'UTC'), dns.product_id, dns.size_id, {partner}, {rdns}, '{repeat}'
                    FROM product_multi_customer_fields dns
                    LEFT JOIN product_multi_customer_fields rdns ON 1>0
                        and rdns.field_name_id = {rdns}
                        and dns.product_id = rdns.product_id
                        and (dns.size_id = rdns.size_id or (dns.size_id is NULL and rdns.size_id is NULL))
                    WHERE 1>0
                    and dns.field_name_id = {dns}
                    and dns."value" = '1'
                    and rdns.id is NULL
                """.format(**format_dict)

                # Fix wrong RDNS values
                update_invalid_rdns_sql = """UPDATE product_multi_customer_fields
                    SET "value" = {repeat},
                        write_uid = {uid},
                        write_date = (now() at time zone 'UTC')
                    WHERE id in (
                        SELECT rdns.id
                        FROM product_multi_customer_fields dns
                        LEFT JOIN product_multi_customer_fields rdns on 1>0
                            and rdns.field_name_id = {rdns}
                            and dns.product_id = rdns.product_id
                            and (dns.size_id = rdns.size_id or (dns.size_id is NULL and rdns.size_id is NULL))
                        where 1>0
                        and dns.field_name_id = {dns}
                        and dns."value" = '1'
                        and rdns.id is not NULL
                        and CASE WHEN rdns."value"~E'^\\\\d+$' THEN cast(rdns."value" as int) ELSE 0 END not between 1 and {repeat}
                    )
                """.format(**format_dict)

                # Decrement valid RDNS values > 1
                decrement_rdns_sql = """UPDATE product_multi_customer_fields
                    SET "value" = temp."rep" - 1,
                        write_uid = {uid},
                        write_date = (now() at time zone 'UTC')
                    from (
                        SELECT rdns.id as id, cast(rdns."value" as int) as rep
                        FROM product_multi_customer_fields dns
                        LEFT JOIN product_multi_customer_fields rdns on 1>0
                            and rdns.field_name_id = {rdns}
                            and dns.product_id = rdns.product_id
                            and (dns.size_id = rdns.size_id or (dns.size_id is NULL and rdns.size_id is NULL))
                        where 1>0
                        and dns.field_name_id = {dns}
                        and dns."value" = '1'
                        and rdns.id is not NULL
                        and CASE WHEN rdns."value"~E'^\\\\d+$' THEN cast(rdns."value" as int) ELSE 0 END between 2 and {repeat}
                        and dns.product_id in %s
                    ) temp
                    where temp.id = product_multi_customer_fields.id
                """.format(**format_dict)

                # Delete all RDNS that do not have DNS
                delete_rdns_sql = """DELETE FROM product_multi_customer_fields
                    WHERE id in (
                        SELECT rdns.id
                        FROM product_multi_customer_fields dns
                        RIGHT JOIN product_multi_customer_fields rdns on 1>0
                            and dns.field_name_id = {dns}
                            and dns.product_id = rdns.product_id
                            and (dns.size_id = rdns.size_id or (dns.size_id is NULL and rdns.size_id is NULL))
                        WHERE 1>0
                        and rdns.field_name_id = {rdns}
                        and (dns.id is NULL or CASE WHEN dns."value"~E'^\\\\d+$' THEN cast(dns."value" as int) ELSE 0 END < 1)
                    )
                """.format(**format_dict)

                cr.execute(delete_rdns_sql)
                cr.execute(update_invalid_rdns_sql)
                if to_decrement_ids:
                    cr.execute(decrement_rdns_sql, (tuple(to_decrement_ids), ))
                cr.execute(insert_missing_rdns_sql)

        return recalculated

    def recalculate_discounts(self, cr, uid, lines=None, settings=None):
        if not lines:
            lines = []
        customer_id = self.get_main_customer_id(settings, single_is_main=True)
        if lines and customer_id:
            sql = """SELECT product_id, size_id, percent
                FROM product_discount
                WHERE date_from <= now()
                    AND date_to >= cast(now() as date)
                    AND customer_id = {}
            """.format(customer_id)
            cr.execute(sql)
            discount_data = cr.dictfetchall()
            discount_data = {
                (
                    (row['product_id'], row['size_id']) if row['size_id']
                    else row['product_id']
                ): row['percent'] for row in discount_data}
            force_price_sql = '''
                SELECT product_id, size_id, force_price
                FROM product_discount
                WHERE date_from <= now()
                    AND date_to >= cast(now() as date)
                    AND customer_id = {}
                    AND force_price IS NOT NULL
            '''.format(customer_id)
            cr.execute(force_price_sql)
            force_prices = cr.dictfetchall()
            force_prices = {
                (
                    (row['product_id'], row['size_id']) if row['size_id']
                    else row['product_id']
                ): row['force_price'] for row in force_prices}
            if force_prices or discount_data:
                for line in lines:
                    if not line.get('customer_price'):
                        continue
                    # Apply Forced Price
                    forced_price = None
                    if line.get('size_id'):
                        forced_price = force_prices.get(
                            (line['product_id'], line['size_id']))
                    if not forced_price:
                        forced_price = force_prices.get(line['product_id'])
                    if forced_price:
                        try:
                            line['customer_price'] = \
                                unicode(round(forced_price, 2))
                            continue
                        except ValueError:
                            pass
                    # Apply Discounts
                    discount = None
                    if line.get('size_id'):
                        discount = discount_data.get(
                            (line['product_id'], line['size_id']))
                    if not discount:
                        discount = discount_data.get(line['product_id'])
                    if discount:
                        # sometimes there are values like -0.05 instead or 5
                        discount = abs(discount)
                        if discount >= 1:
                            discount /= 100.0
                        try:
                            price = float(line['customer_price'].strip())
                            new_price = price * (1.0 - discount)
                            line['customer_price'] = \
                                unicode(round(new_price, 2))
                        except ValueError:
                            pass
        return lines

    def integrationApiSyncAllProductAmazon(self, cr, uid, name):
        (settings, api) = self.getApi(cr, uid, name)
        log = []
        if(api):
            conf_obj = self.pool.get('ir.config_parameter')
            settings_obj = conf_obj.get_param(cr, uid, 'amz_sync.last.updated.product.id')
            if len(settings_obj) > 0 and settings_obj != '0':
                config_params = eval(settings_obj)
            else:
                config_params = [{'setting_name': settings.name, 'last_id': '0'}]

            current_setting = ""
            for setting in config_params:
                if (setting['setting_name'] == settings.name):
                        current_setting = setting
            if not current_setting:
                current_setting = {'setting_name': settings.name, 'last_id': '0'}
                config_params.append(current_setting)
            last_id = current_setting.get('last_id', '0')
            sql = """
                SELECT id product_id, default_code
                FROM product_product
                WHERE type = 'product'
                AND id > %s
                ORDER BY id
                LIMIT 5000
            """ % (last_id)

            cr.execute(sql)
            res = cr.dictfetchall()
            if res:
                products = [str(i['default_code']).strip() for i in res]
                amz_found_sku = api.searchProducts(products)
                log = api.getLog()

                for setting in config_params:
                    if (setting['setting_name'] == settings.name):
                        setting['last_id'] = res[-1].get('product_id', '0')
                conf_obj.set_param(cr, uid, 'amz_sync.last.updated.product.id', str(config_params))

                if amz_found_sku:
                    sql_search_id = """
                                SELECT id
                                FROM product_product
                                WHERE type = 'product'
                                AND default_code in ('%s')
                    """ % ("','".join(amz_found_sku))
                    cr.execute(sql_search_id)
                    amz_found_ids = [i[0] for i in cr.fetchall()]
                    tag_ids = self.pool.get('product.product').search(cr, uid, [('id', 'in', [i['product_id'] for i in res]), ('tagging_ids', 'in', settings.tag.id)])
                    tag_ids_all = self.pool.get("tagging.tags").read(cr, uid, settings.tag.id, ['product_ids'])
                    append_ids = list(set(amz_found_ids) - set(tag_ids_all['product_ids']))
                    remove_ids = list(set(tag_ids) - set(amz_found_ids))
                    if(len(append_ids) > 0):
                        for prod_id in append_ids:
                            self.pool.get("tagging.tags").write(cr, uid, settings.tag.id, {'product_ids': [(4, prod_id)]})

                    if(len(remove_ids) > 0):
                        for prod_id in remove_ids:
                            self.pool.get("tagging.tags").write(cr, uid, settings.tag.id, {'product_ids': [(3, prod_id)]})

                    log.append({
                        'title': 'SyncAllProductAmazon FINISH find %d , not_find %d' % (len(append_ids), len(remove_ids)),
                        'msg': 'amazon found ids: %s \n count old tag: %s \n count append: %s \n count remove: %s' % (
                            len(amz_found_ids),
                            len(tag_ids_all['product_ids']),
                            len(append_ids),
                            len(remove_ids)),
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                else:
                    log.append({
                        'title': 'SyncAllProductAmazon FINISH find 0',
                        'msg': 'amazon found ids: 0',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
            else:
                for setting in config_params:
                    if (setting['setting_name'] == settings.name):
                        setting['last_id'] = '0'
                conf_obj.set_param(cr, uid, 'amz_sync.last.updated.product.id', str(config_params))
        if(len(log) > 0):
            self.log(cr, uid, settings, log)

    def integrationApiSyncPrice(self, cr, uid, ids=None):
        if not ids: ids = []
        for order_id in ids:

            sale_obj = self.pool.get("sale.order").browse(cr, uid, order_id)

            if(sale_obj.external_customer_order_id):
                settings_id = self.search(cr, uid, [('customer_ids', '=', sale_obj.partner_id.id)])
                if(len(settings_id) > 0):
                    name = self.browse(cr, uid, settings_id[0]).name
                    (settings, api) = self.getApi(cr, uid, name)

                    if(not api):
                        return True
                    if(settings.type_api != "overstock"):
                        continue
                    lines = []
                    for order_line in sale_obj.order_line:
                        if(not order_line.syncPrice and order_line.optionSku):
                            lineObj = {}
                            lineObj["external_customer_line_id"] = order_line.external_customer_line_id
                            lineObj["optionSku"] = order_line.optionSku
                            lineObj["syncPrice"] = order_line.syncPrice
                            lineObj["id"] = order_line.id
                            lines.append(lineObj)
                    if(len(lines) > 0):
                        products = api.getProductsInfo(lines)
                        for product in products:
                            self.pool.get("sale.order.line").write(cr, uid, product['id'], {'customerCost': product['product']['Pricing']['SuggestedSellingPrice'], 'syncPrice': True})

                        log = api.getLog()
                        if(len(log) > 0):
                            for i in xrange(0, len(log)):
                                log[i]['title'] += "  %s" % sale_obj.name

                            self.log(cr, uid, settings, log)
        return True

    def integrationApiRejectSaleOrder(self, cr, uid, ids=None):
        if not ids: ids = []
        log = []
        for order_id in ids:

            sale_obj = self.pool.get("sale.order").browse(cr, uid, order_id)

            if(sale_obj.external_customer_order_id):
                settings_id = self.search(cr, uid, [('customer_ids', '=', sale_obj.partner_id.id)])
                if(len(settings_id) > 0):
                    name = self.browse(cr, uid, settings_id[0]).name
                    (settings, api) = self.getApi(cr, uid, name)

                    if(not api):
                        return True

                    if(settings.type_api in ("overstock", "staples")):
                        self.integrationApiProcessingOrder(cr, uid, [order_id], state="rejected")
                    elif(settings.type_api == "commercehub"):
                        orders_list = {}
                        orders_list["lineCount"] = len(sale_obj.order_line)
                        orders_list["action"] = 'R'
                        orders_list["order_id"] = sale_obj.po_number
                        orders_list["name"] = str(sale_obj.name)
                        orders_list["customer_id"] = str(sale_obj.partner_id.external_customer_id)
                        api.Acknowledgement(orders_list)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Rejected Sale Order %s ' % sale_obj.name,
                            'msg': 'Rejected Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    elif(settings.type_api == "amazon"):
                        orders_list = {}
                        orders_list["state"] = 'Failure'
                        orders_list["order_id"] = sale_obj.external_customer_order_id
                        orders_list["merchant_order_id"] = order_id
                        orders_list["lines"] = []
                        for order_line in sale_obj.order_line:
                            lineObj = {}
                            lineObj["order_item_id"] = order_line.external_customer_line_id
                            orders_list["lines"].append(lineObj)
                        api.Acknowledgement(orders_list)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Accepted Sale Order %s ' % sale_obj.name,
                            'msg': 'Accepted Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    elif(settings.type_api == "amazon_retail"):
                        orders_list = {}
                        orders_list["state"] = 'CC'
                        orders_list["code"] = '865'
                        orders_list["func_ident_code"] = '865'
                        orders_list["sale_order"] = sale_obj
                        api.Acknowledgement(orders_list)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Rejected Sale Order %s ' % sale_obj.name,
                            'msg': 'Rejected Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })

                    if log:
                        self.log(cr, uid, settings, log)

    # @prodonly(return_value=True)
    def integrationApiAcceptSaleOrder(self, cr, uid, ids=None, context=None):
        if not ids:
            ids = []
        if not context:
            context = {}

        res = True

        if(not isinstance(ids, list)):
            ids = [ids]
        for order_id in ids:

            log = []
            settings = None

            sale_obj = self.pool.get("sale.order")
            sale = sale_obj.browse(cr, uid, order_id)

            if(sale.external_customer_order_id):
                if not sale.type_api_id:
                    settings_id = self.search_main_si_by_partner(cr, uid, sale.partner_id.id)
                else:
                    settings_id = sale.type_api_id.id

                if settings_id:
                    name = self.browse(cr, uid, settings_id).name
                    (settings, api) = self.getApi(cr, uid, name)

                    if(not api):
                        return True

                    main_customer_id = self.get_main_customer_id(settings)

                    if(settings.type_api in ['overstock', 'walmartcanada', 'staples', 'direct_buy', 'samuels']):
                        res = self.integrationApiProcessingOrder(cr, uid, [order_id], state="accepted")
                        self.integrationApiSyncPrice(cr, uid, [order_id])
                    elif(settings.type_api == "commercehub"):
                        orders_list = {}
                        orders_list["lineCount"] = len(sale.order_line)
                        orders_list["action"] = 'A'
                        orders_list["order_id"] = sale.external_customer_order_id
                        orders_list["name"] = str(sale.name)
                        orders_list["customer_id"] = str(sale.partner_id.external_customer_id)
                        res = api.Acknowledgement(orders_list)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Accepted Sale Order %s ' % sale.name,
                            'msg': 'Accepted Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    elif(settings.type_api == "amazon"):
                        orders_list = {}
                        orders_list["state"] = 'Success'
                        orders_list["order_id"] = sale.external_customer_order_id
                        orders_list["merchant_order_id"] = order_id
                        orders_list["lines"] = []
                        for order_line in sale.order_line:
                            lineObj = {}
                            lineObj["order_item_id"] = order_line.external_customer_line_id
                            lineObj["order_line_item_id"] = order_line.id
                            orders_list["lines"].append(lineObj)
                        res = api.Acknowledgement(orders_list)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Accepted Sale Order %s ' % sale.name,
                            'msg': 'Accepted Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    elif(settings.type_api == "amazon_retail"):
                        orders_list = {}
                        orders_list["state"] = 'IA'
                        orders_list["code"] = '855'
                        orders_list["func_ident_code"] = 'PR'
                        orders_list["sale_order"] = sale
                        orders_list["lines"] = []
                        for order_line in sale.order_line:
                            line = {}
                            if (order_line.product_id):

                                if (order_line.product_uom_qty > 0):
                                    line['state'] = 'IA'
                                    line['product_qty_ack'] = order_line.product_uom_qty
                                else:
                                    line['state'] = 'CA'
                                    line['product_qty_ack'] = order_line.product_order_qty or 0
                                line['customer_sku'] = self.pool.get('product.product').get_val_by_label(
                                    cr,
                                    uid,
                                    order_line.product_id.id,
                                    main_customer_id or order_line.order_partner_id.id,
                                    'Customer SKU',
                                    order_line.size_id.id or False,
                                )
                                line["sale_order_line"] = order_line

                            if (not order_line.product_id):
                                line['state'] = 'BA'
                                line["sale_order_line"] = order_line
                                line['qty_available'] = 0
                                sku_regex = re.search(r"'sku': u'(\w+\w*\W*\w+)'", order_line.notes)
                                # sku_regex = re.search(r"'sku': u'(.+)', 'name'", order_line.notes)
                                if (sku_regex):
                                    default_code = sku_regex.group(1)
                                    line['default_code'] = default_code
                            orders_list["lines"].append(line)
                        transaction_number = self.get_transaction_number(cr, uid)
                        res = api.Acknowledgement(orders_list, transaction_number)
                        new_transaction_number = int(transaction_number) + 1
                        self.set_transaction_number(cr, uid, new_transaction_number)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Accepted Sale Order %s ' % sale.name,
                            'msg': 'Accepted Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    elif(settings.type_api == "walmart" and context.get('button_call', False)):
                        lines = []
                        for order_line in sale.order_line:
                            lineObj = {}
                            lineObj["external_customer_line_id"] = order_line.external_customer_line_id
                            lineObj["external_customer_order_id"] = sale.external_customer_order_id
                            lines.append(lineObj)
                        res = api.processingOrderLines(lines)
                        log.extend(api.getLog())
                        log.append({
                            'title': 'Accepted Sale Order %s ' % sale.name,
                            'msg': 'Accepted Sale Order',
                            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                        })
                    if api.apierp_is_available('get_accept_information'):
                        api.apierp.pool = self.pool
                        lines = api.apierp.get_accept_information(cr, uid, settings, sale)
                        if bool(lines) is True:
                            if name in ['Sams Club', 'Walmart Fulfilled (FBW)']:
                                lines.update({'new_type_api_id': name})
                            res = api.processingOrderLines(lines)
                            log.extend(api.getLog())
                            log.append({
                                'title': 'Accepted Sale Order %s ' % sale.name,
                                'msg': 'Accepted Sale Order',
                                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                            })
                    if (res):
                        sale_obj.write(cr, uid, order_id, {"sale_integration_proccessed": True})
                        sale_obj.update_last_confirm_qty(cr, uid, order_id)

                    else:
                        sale_obj.write(cr, uid, order_id, {"note": str(log)})

            if log:
                for i in xrange(0, len(log)):
                    log[i]['title'] += "  %s" % sale.name
                self.log(cr, uid, settings, log)

        return res

    def integrationApiReconfirmSaleOrder(self, cr, uid, ids, context=None):
        log = []
        settings = None
        if(not isinstance(ids, list)):
            ids = [ids]
        for order_id in ids:
            sale_obj = self.pool.get("sale.order").browse(cr, uid, order_id)
            if(sale_obj.external_customer_order_id):
                settings_id = self.search(cr, uid, [('customer_ids', '=', sale_obj.partner_id.id)])
                if settings_id:
                    name = self.browse(cr, uid, settings_id[0]).name
                    (settings, api) = self.getApi(cr, uid, name)

                    if(not api):
                        return True

                    main_customer_id = self.get_main_customer_id(settings)

                    # Need to Reconfirm button
                    order = {}
                    order['code'] = '865'
                    order['name'] = sale_obj.name or ''
                    order['external_customer_order_id'] = sale_obj.external_customer_order_id or False
                    order['latest_ship_date_order'] = sale_obj.latest_ship_date_order or False
                    order['earliest_ship_date_order'] = sale_obj.earliest_ship_date_order or False

                    order['partner_order'] = sale_obj.partner_order_id.name or ''
                    order['partner_order_san'] = sale_obj.partner_order_id.san_address or ''

                    order['partner_shipping'] = sale_obj.partner_shipping_id.name or ''
                    order['partner_shipping_san'] = sale_obj.partner_shipping_id.san_address

                    order['lines'] = []
                    for order_line in sale_obj.order_line:
                        line = {}
                        line['external_customer_line_id'] = order_line.external_customer_line_id or ''
                        line['customer_sku'] = self.pool.get('product.product').get_val_by_label(
                            cr,
                            uid,
                            order_line.product_id.id,
                            main_customer_id or order_line.order_partner_id.id,
                            'Customer SKU',
                            order_line.size_id.id or False,
                        )
                        line['customer_id_delmar'] = self.pool.get('product.product').get_val_by_label(
                            cr,
                            uid,
                            order_line.product_id.id,
                            main_customer_id or order_line.order_partner_id.id,
                            'Customer ID Delmar',
                            order_line.size_id.id or False,
                        )
                        line['product_qty'] = order_line.product_uom_qty or 0
                        line['product_last_confirm_qty'] = order_line.product_last_confirm_qty or order_line.product_order_qty or 0
                        line['price_unit'] = order_line.price_unit or 0
                        if line['product_qty'] > line['product_last_confirm_qty']:
                            line['change_code'] = 'QI'
                        elif line['product_qty'] < line['product_last_confirm_qty']:
                            line['change_code'] = 'QD'
                        else:
                            line['change_code'] = 'NC'

                        if order_line.product_uom_qty > 0:
                            line['item_status_code'] = 'IA'
                        else:
                            line['item_status_code'] = 'CA'
                        order['lines'].append(line)

                    # Need to Reconfirm button
                    transaction_number = self.get_transaction_number(cr, uid)
                    res = api.changeAcknowledgement(order, transaction_number)
                    new_transaction_number = int(transaction_number) + 1
                    self.set_transaction_number(cr, uid, new_transaction_number)

                    if res:
                        self.pool.get("sale.order").update_last_confirm_qty(cr, uid, order_id)

                    log.extend(api.getLog())
                    log.append({
                        'title': 'Reconfirmed Sale Order (send to Amazon) %s' % sale_obj.name,
                        'msg': 'Reconfirmed Sale Order (send to Amazon)',
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
        if log:
            self.log(cr, uid, settings, log)
        return True

    @send_mails_from_result
    def integrationApiReturnDeliveryOrder(self, cr, uid, return_line_ids):
        result = {}
        lines = []
        log = []
        if (len(return_line_ids) > 0):
            move = self.pool.get("stock.move").browse(cr, uid, return_line_ids[0])
            pick_obj = move.picking_id
            sale_obj = pick_obj.sale_id
            customer = sale_obj.partner_id

            if not customer:
                return True

            if not sale_obj.type_api_id:
                settings_id = self.search_main_si_by_partner(cr, uid, sale_obj.partner_id.id)
            else:
                settings_id = sale_obj.type_api_id.id

            if not settings_id:
                return True

            name = self.browse(cr, uid, settings_id).name
            (settings, api) = self.getApi(cr, uid, name)

            if(not api):
                return True

            order_additional_fields = {x.name: x.value for x in pick_obj.sale_id.additional_fields or []}

            for return_line_id in return_line_ids:
                move_obj = self.pool.get("stock.move").browse(cr, uid, return_line_id)

                log = []
                line = self.pool.get("stock.move").read(cr, uid, move_obj.id)
                # commercehub
                line['merchantLineNumber'] = str(move_obj.sale_line_id.merchantLineNumber)
                line['external_customer_line_id'] = str(move_obj.sale_line_id.external_customer_line_id)
                line['external_customer_order_id'] = str(sale_obj.external_customer_order_id)
                line['po_number'] = str(sale_obj.po_number)
                line["customer_id"] = str(customer.external_customer_id)
                line["merchantSKU"] = move_obj.sale_line_id.merchantSKU
                line["optionSku"] = move_obj.sale_line_id.optionSku
                line["vendorSku"] = move_obj.sale_line_id.vendorSku
                line['ordered_qty'] = move_obj.sale_line_id.product_uom_qty
                line['left_to_receive'] = move_obj.sale_line_id.left_to_receive

                # amazon
                line['shipCost'] = move_obj.sale_line_id.shipCost
                line['external_customer_order_id'] = str(sale_obj.external_customer_order_id)
                line['ItemPrice'] = move_obj.sale_line_id.price_unit

                line['return_reason'] = move_obj.return_partner_reason and move_obj.return_partner_reason.key or ''
                line['order_additional_fields'] = order_additional_fields
                line['number_of_transaction'] = pick_obj.sale_id.tracking_ref

                line['customer_id_delmar'] = self.pool.get('product.product').get_val_by_label(
                    cr,
                    uid,
                    move_obj.product_id.id,
                    sale_obj.partner_id.id,
                    'Customer ID Delmar',
                    move_obj.size_id.id or False,
                )

                line['upc'] = self.pool.get('product.product').get_val_by_label(
                    cr,
                    uid,
                    move_obj.product_id.id,
                    sale_obj.partner_id.id,
                    'UPC',
                    move_obj.size_id.id or False,
                )

                lines.append(line)


            api.returnOrders(lines)

            for item in settings.settings:
                item_name = str(item.name).strip()
                if item_name == 'send_return_invoice' and (item.value or '').strip().lower() in ['1', 'true']:
                    self._send_invoice_for_order(cr, uid, pick_obj.id, api, settings, customer.name)
                    result.update({'object': api, 'settings': settings, 'return_result': True})
                    break

            log.extend(api.getLog())
            if log:
                self.log(cr, uid, settings, log)

        return result

    def integrationApiProcessCancelXML(self, cr, uid, name, limit=-10):
        (settings, api) = self.getApi(cr, uid, name)
        log = []
        if not api:
            return False
        if(limit == -10):
            limit = settings.process_limit

        xml_ids = []
        condition = [
            ('load', '=', False),
            ('type_api_id', '=', settings.id),
            ('name', 'like', 'CANCEL')
        ]
        xml_obj = self.pool.get('sale.integration.xml')
        if(limit != -1):
            xml_ids = xml_obj.search(cr, uid, condition, limit=limit)
        else:
            xml_ids = xml_obj.search(cr, uid, condition)
        picking_obj = self.pool.get('stock.picking')
        internal_shipment_obj = self.pool.get('internal.shipment')
        for xml_instance in xml_obj.browse(cr, uid, xml_ids):
            orders_obj = api.getOrdersObj(xml_instance.xml)
            log.extend(api.getLog())
            customer_ids = [x.id for x in settings.customer_ids]
            if not orders_obj:
                continue
            print orders_obj
            if api.apierp_is_available('get_cancel_accept_information'):
                api.apierp.pool = self.pool
                lines = api.apierp.get_cancel_accept_information(
                    cr, uid, settings, orders_obj)
            else:

                # TODO: check return format for all customers
                # to avoid such bicycle
                order_lines = []
                for line in orders_obj.get('lines', []):
                    line_id = line
                    if line and isinstance(line, dict):
                        line_id = line.get('id')
                    if line_id:
                        order_lines.append(line_id)

                if(settings.type_api == "sterling"):
                    if(orders_obj['order_id'].find('EDI') != -1):
                        query = """SELECT
                                so.name,
                                so.id as order_id,
                                st.id as picking_id,
                                so.merchantreference3
                                    as external_customer_line_id,
                                so.external_customer_order_id,
                                so.create_date as order_date
                            FROM
                                sale_order so
                            INNER JOIN stock_picking st
                                ON so.id = st.sale_id
                            WHERE
                                so.po_number = %s
                                AND so.partner_id IN %s"""
                        params = (orders_obj['poNumber'], tuple(customer_ids))
                    else:
                        query = """SELECT
                                so.name,
                                so.id as order_id,
                                st.id as picking_id,
                                so.merchantreference3
                                    as external_customer_line_id,
                                so.external_customer_order_id,
                                so.create_date as order_date
                            FROM
                                sale_order so
                            INNER JOIN stock_picking st
                                ON so.id = st.sale_id
                            WHERE
                                so.merchantreference3 IN %s
                                AND so.partner_id IN %s"""
                        params = (tuple(order_lines), tuple(customer_ids))
                elif(settings.type_api == "walmart"):
                    query = """SELECT
                                sol.id as line_id,
                                so.id as order_id,
                                st.id as picking_id,
                                sol.name as name,
                                sol.external_customer_line_id,
                                so.external_customer_order_id,
                                so.create_date as order_date
                            FROM
                                sale_order_line sol
                            INNER JOIN sale_order so
                                ON so.id = sol.order_id
                            INNER JOIN stock_picking st
                                ON so.id = st.sale_id
                            WHERE
                                sol.external_customer_line_id IN %s
                                AND so.external_customer_order_id = %s
                                AND so.partner_id IN %s"""
                    params = (
                        tuple(order_lines),
                        orders_obj['external_customer_order_id'],
                        tuple(customer_ids),
                    )
                else:
                    query = """SELECT
                            sol.id as line_id,
                            so.id as order_id,
                            st.id as picking_id,
                            sol.name as name,
                            sol.external_customer_line_id,
                            so.external_customer_order_id,
                            so.create_date as order_date
                        FROM
                            sale_order_line sol
                        INNER JOIN sale_order so
                            ON so.id = sol.order_id
                        INNER JOIN stock_picking st
                            ON so.id = st.sale_id
                        WHERE
                            sol.external_customer_line_id IN %s
                            AND so.partner_id IN %s"""
                    params = (tuple(order_lines), tuple(customer_ids))
                cr.execute(query, params)
                lines = cr.dictfetchall()
            if not lines:
                log.append({
                    'title': 'ERROR Can\'t Find move line by move_obj.name',
                    'msg': '%s' % str(orders_obj),
                    'create_date': time.strftime(
                        '%Y-%m-%d %H:%M:%S', time.gmtime())
                })
                continue
            for line in lines:
                picking_id = line['picking_id']
                stock_picking = picking_obj.browse(cr, uid, picking_id)
                if stock_picking.state in ('cancel', 'final_cancel'):
                    continue
                if stock_picking.tracking_ref:
                    self._skip_cancel_by_vendor(cr, uid, stock_picking)
                    continue
                internal_shipment_ids = internal_shipment_obj.search(
                    cr, uid, [('picking_id', '=', picking_id)])
                if internal_shipment_ids:
                    internal_shipments = internal_shipment_obj.browse(
                        cr, uid, internal_shipment_ids)
                    self._skip_cancel_by_vendor(
                        cr, uid, stock_picking, internal_shipments)
                    continue
                wf_service = netsvc.LocalService("workflow")
                wf_service.trg_validate(
                    uid, 'stock.picking', picking_id, 'button_cancel', cr)
                history = stock_picking.report_history or ''
                time_now = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                history += "\n%s CANCELED by Vendor" % time_now
                stock_picking.write({'report_history': history})
                state = "accept_cancel"
                if settings.type_api == 'overstock':
                    state = "accepted"
                api.processingOrderLines([line], state=state)

                log.extend(api.getLog())
                log.append({
                    'title': 'CANCELED move line %s' % line['name'],
                    'msg': 'XML %s ' % orders_obj,
                    'create_date': time_now,
                })
            xml_instance.write({'load': True})
        if(len(log) > 0):
            self.log(cr, uid, settings, log)

        return True

    def _skip_cancel_by_vendor(self, cr, uid, picking, internal_shipments=None):
        history = picking.report_history or ''
        history += "\n%s Skip CANCEL by Vendor, order has %s" % (
            time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
            'internal move' if internal_shipments else 'tracking number'
        )
        picking.write({'report_history': history})
        superuser = self.pool.get('res.users').browse(cr, uid, SUPERUSER_ID)
        email_to_users = [superuser]
        support_user = picking.real_partner_id.order_support_user_id
        if support_user:
            email_to_users.append(support_user)
        if internal_shipments:
            mail_body = "Order has internal movement."
            mail_body += "\nPlease process cancel to proper location manually."
            for internal_shipment in internal_shipments:
                email_to_users.extend([
                    internal_shipment.ship_user,
                    internal_shipment.receive_user,
                ])
        else:
            mail_body = "Order already has tracking number: "
            mail_body += str(picking.tracking_ref)
        email_to = list(set([
            user.user_email for user in email_to_users if user.user_email
        ]))
        if email_to:
            mail_subject = 'Skip cancel request from vendor '
            mail_subject += str(picking.name)
            email_from = 'erp.delmar@gmail.com'
            self.pool.get('mail.message').schedule_with_attach(
                cr, uid, email_from, email_to, mail_subject, mail_body)

    def integrationApiSendCancel(self, cr, uid, ids, context=None):
        log = []
        settings = None
        if context is None:
            context = {}
        if(not isinstance(ids, list)):
            ids = [ids]
        for order_id in ids:

            picking = self.pool.get('stock.picking').browse(cr, uid, order_id)
            if not picking.move_lines:
                continue

            sale_obj = picking.sale_id

            if(sale_obj.external_customer_order_id):

                if picking.is_set and picking.set_parent_move_lines != []:
                    self._switch_set_lines(cr, uid, picking,'to_cancel')
                    picking = self.pool.get('stock.picking').browse(cr, uid, order_id)

                if not sale_obj.type_api_id:
                    settings_id = self.search_main_si_by_partner(cr, uid, sale_obj.partner_id.id)
                else:
                    settings_id = sale_obj.type_api_id.id

                if settings_id:
                    name = self.browse(cr, uid, settings_id).name
                    (settings, api) = self.getApi(cr, uid, name)
                    history = picking.report_history or ''
                    if(not api):
                        return True

                    order_additional_fields = {x.name: x.value for x in picking.sale_id.additional_fields or []}

                    lines = []
                    for order_line in picking.move_lines:
                        lineObj = {}
                        lineObj["external_customer_line_id"] = order_line.sale_line_id.external_customer_line_id
                        lineObj["external_customer_order_id"] = sale_obj.external_customer_order_id
                        lineObj["product_qty"] = int(order_line.product_qty or 0)
                        lineObj["merchantLineNumber"] = order_line.sale_line_id.merchantLineNumber
                        lineObj["merchantSKU"] = order_line.sale_line_id.merchantSKU
                        lineObj["vendorSku"] = order_line.sale_line_id.vendorSku
                        lineObj["optionSku"] = order_line.sale_line_id.optionSku
                        lineObj["price_unit"] = order_line.sale_line_id.price_unit
                        lineObj["size"] = order_line.sale_line_id.size_id and order_line.sale_line_id.size_id.name or False
                        lineObj["po_number"] = sale_obj.po_number
                        lineObj["cust_order_number"] = sale_obj.cust_order_number
                        lineObj["origin"] = sale_obj.name
                        lineObj["external_customer_id"] = sale_obj.partner_id.external_customer_id
                        lineObj['action'] = 'v_cancel'
                        lineObj['invoice'] = picking.invoice_no
                        lineObj['order_date'] = sale_obj.create_date
                        lineObj['left_to_receive'] = order_line.sale_line_id.left_to_receive
                        lineObj['ordered_qty'] = order_line.sale_line_id.product_uom_qty or 0
                        lineObj['tracking_number'] = picking.tracking_ref or ''
                        lineObj['shp_service'] = picking.shp_service or ''
                        lineObj['additional_fields'] = {x.name: x.value for x in order_line.sale_line_id.additional_fields or []}
                        lineObj['order_additional_fields'] = order_additional_fields

                        lineObj['customerCost'] = order_line.sale_line_id.customerCost
                        lineObj['unit_cost'] = order_line.sale_line_id.price_unit
                        lineObj['gift_message'] = order_line.sale_line_id.gift_message
                        lineObj['ship'] = {}
                        lineObj['ship']['address1'] = picking.address_id.street
                        lineObj['ship']['address2'] = picking.address_id.street2
                        lineObj['ship']['address3'] = picking.address_id.street3
                        lineObj['ship']['zip'] = picking.address_id.zip
                        lineObj['ship']['phone'] = picking.address_id.phone
                        lineObj['ship']['country'] = picking.address_id.country_id.code
                        lineObj['ship']['city'] = picking.address_id.city
                        lineObj['ship']['state'] = picking.address_id.state_id.code
                        lineObj['ship']['name'] = order_line.sale_line_id.name
                        lineObj["shp_date"] = picking.shp_date
                        lineObj["carrier_code"] = picking.carrier_code
                        lineObj['customer_id_delmar'] = self.pool.get('product.product').get_val_by_label(
                            cr,
                            uid,
                            order_line.product_id.id,
                            sale_obj.partner_id.id,
                            'Customer ID Delmar',
                            order_line.size_id.id or False,
                        )

                        history += "\n%s CANCELED line %s with code %s" % (
                            time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                            order_line.sale_line_id.external_customer_line_id, context.get('cancel_code', False))
                        lineObj["cancel_code"] = context.get('cancel_code', False)
                        if settings.type_api == 'hbc':
                            if api.apierp_is_available('get_additional_processing_information'):
                                api.apierp.pool = self.pool
                                sub_lineObj = api.apierp.get_additional_processing_information(cr, uid, settings, picking, order_line)
                                lineObj.update(sub_lineObj)

                        lines.append(lineObj)

                    if settings.type_api == "walmart":
                        api.processingOrderLines(lines)
                    elif settings.type_api == "overstock":
                        api.processingOrderLines(lines, 'accepted')
                    elif settings.type_api == "commercehub":
                        api.confirmShipment(lines, context={'invoice': False})
                    elif settings.type_api in ('newegg', 'shop'):
                        api.cancelOrder(lines)
                    elif settings.type_api == 'bluestem':
                        if api.apierp_is_available('get_accept_information'):
                            api.apierp.pool = self.pool
                            lines_0 = api.apierp.get_accept_information(cr, uid, settings, sale_obj)
                            if bool(lines) is True:
                                lines[0]['accept_information'] = lines_0
                                api.processingOrderLines(lines, 'cancel')
                    elif settings.type_api == 'boscov':
                        api.confirmShipment(lines)
                    else:
                        api.processingOrderLines(lines, 'cancel')

                    self.pool.get('stock.picking').write(cr, uid, picking.id, {'report_history': history})
                    log.extend(api.getLog())
                    log.append({
                        'title': 'SendCancel Order %s ' % sale_obj.name,
                        'msg': 'SendCancel Order %s' % str(lines),
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })

        if log and settings:
            self.log(cr, uid, settings, log)
        return True

    def getAdditionalInvoiceInformation(self, cr, uid, sale_order_id, invoice_data, context=None):
        res = {}
        if context is None:
            context = {}
        sale_obj = self.pool.get("sale.order").browse(cr, uid, sale_order_id)
        if (sale_obj.external_customer_order_id):
            partner_id = context.get('customer_id', sale_obj.partner_id.id)
            settings_id = self.search(cr, uid, [('customer_ids', '=', partner_id)])
            if settings_id:
                name = self.browse(cr, uid, settings_id[0]).name
                (settings, api) = self.getApi(cr, uid, name)
                if api:
                    if api.apierp_is_available('get_additional_invoice_information'):
                        api.apierp.pool = self.pool
                        context['settings'] = settings
                        res = api.apierp.get_additional_invoice_information(cr, uid, sale_order_id, invoice_data, context)
        return res

    def getAdditionalShippingInformation(self, cr, uid, picking_id, shipping_data, context=None):
        res = {}
        if context is None:
            context = {}

        picking_obj = self.pool.get("stock.picking").browse(cr, uid, picking_id)
        sale_order_id = picking_obj.sale_id.id

        sale_obj = self.pool.get("sale.order").browse(cr, uid, sale_order_id)
        if (sale_obj.external_customer_order_id):
            partner_id = context.get('customer_id', sale_obj.partner_id.id)
            settings_id = self.search(cr, uid, [('customer_ids', '=', partner_id)])
            if settings_id:
                name = self.browse(cr, uid, settings_id[0]).name
                (settings, api) = self.getApi(cr, uid, name)
                if api:

                    sale_order = self.pool.get('sale.order').browse(cr, uid, sale_order_id)

                    ref1 = sale_order.po_number
                    if ref1 and sale_order.ship2store:
                        ref1 += 'SO'

                    item_arr = []
                    for m_id in picking_obj.move_lines:
                        item = m_id.sale_line_id and m_id.sale_line_id.vendorSku

                        if not item:
                            item = self.pool.get('product.product').get_val_by_label(
                                cr,
                                uid,
                                m_id.product_id.id,
                                partner_id,
                                'Customer SKU',
                                m_id.size_id.id
                            )
                        if item:
                            item_arr.append(item)

                    ref2 = ",".join(str(elem) for elem in item_arr if elem)
                    if picking_obj.shp_service and picking_obj.shp_service.upper().strip() == 'UPS' and name == 'BTR':
                        ref2 = 'null'

                    res = {
                        'Ref1': ref1,
                        'Ref2': ref2[0:30]
                    }
                    if api.apierp_is_available('get_additional_shipping_information'):
                        api.apierp.pool = self.pool
                        context['settings'] = settings
                        api_res = api.apierp.get_additional_shipping_information(cr, uid, sale_order_id, shipping_data, context)
                        res.update(api_res)
        return res

    def write_unique_fields_for_orders(self, cr, uid, picking, api=None):
        si_name = (
            picking and
            picking.sale_id and
            picking.sale_id.type_api_id and
            picking.sale_id.type_api_id.name or None
        )
        if si_name:
            if not api:
                (settings, api) = self.getApi(cr, uid, si_name)
            if api:
                if api.apierp_is_available('write_unique_fields_for_orders'):
                    api.apierp.pool = self.pool
                    api.apierp.write_unique_fields_for_orders(cr, uid, picking)
        return True

    def log(self, cr, uid, settings, logs=None):
        if logs is None:
            logs = []

        for log_line in logs:
            try:
                name = unicode(log_line['title'], errors='ignore')
            except:
                name = unicode(log_line['title'])
            try:
                tmp = unicode(log_line['msg'], errors='ignore')
            except:
                tmp = unicode(log_line['msg'])
            self.pool.get('res.log').create(cr, uid, {
                'name': name,
                'res_model': log_line.get('model', self._name),
                'res_id': log_line.get('id', 0),
                #'debug_information': tmp,
                #'log_type': 'sale_integration_log'
            })
            self.pool.get('sale.integration.log').create(cr, uid, {
                'name': log_line['title'],
                'debug_information': tmp,
                'type_api': settings.type_api,
            })

sale_integration()


class sale_integration_log(osv.osv):

    _name = "sale.integration.log"

    _columns = {
        'name': fields.char('Message', size=512),
        'debug_information': fields.text('Debug information'),
        'user_id': fields.many2one('res.users', 'User'),
        'type_api': fields.selection(_get_type_api, 'Api services', readonly=True, select=1),
        'create_date': fields.datetime('Creation Date', readonly=True, select=1)
    }

    _order = "create_date DESC"

    _defaults = {
        'user_id': lambda self, cr, uid, ctx: uid
    }

    def search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False):
        if args:
            return super(sale_integration_log, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)
        else:
            return []


sale_integration_log()


class order_ext(osv.osv):
    _name = 'order.ext'
    _columns = {
        'sale_id': fields.many2one('sale.order', 'Id'),
        'ext_xml': fields.text('External xml fields', required=False)
    }

order_ext()


class stock_mako_templates(osv.osv):
    _name = "stock.mako.templates"

    _columns = {
        'type_api_id': fields.many2one('sale.integration', 'Service provider'),
        'tpl': fields.text('Template', ),
        'action': fields.selection((
            ('inventory', 'Update QTY'),
            ('cancel', 'Cancel'),
            ('confirm', 'Confirm'),
            ('invoice', 'Invoice'),
            ('return', 'Return'),
            ('acknowledgement', 'Acknowledgement'),
            ('upload_file', 'Upload file'),
        ), 'Action', select=1, required=True,),
    }

stock_mako_templates()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
