# -*- coding: utf-8 -*-

from osv import fields, osv
import logging

logger = logging.getLogger('sale.integration')


class res_partner(osv.osv):
    _inherit = 'res.partner'

    def _get_api_id(self, cr, uid, ids, name, arg, context=None):
        res = {x: None for x in ids}

        for partner in self.pool.get('res.partner').browse(cr, uid, ids):
            type_api_id = False

            if partner.type_api_ids:
                for api in partner.type_api_ids:
                    if api.si_type == 'main':
                        type_api_id = api.id
                        break

                if not type_api_id:
                    type_api_id = partner.type_api_ids[0].id

            res[partner.id] = type_api_id

        return res

    _columns = {
        'external_customer_id': fields.char('External Customer Id', size=4096),
        'type_api_ids': fields.many2many('sale.integration', 'sale_integration_customer', 'customer_id', 'integration_id', 'Service Provider'),
        'sale_integration_main': fields.boolean('Main customer for sale integration'),
        'account_ids': fields.one2many('res.partner.account', 'partner_id', 'Shipping Accounts', ),
        'bulk_transfer': fields.boolean('Bulk Transfer', ),
        'send_acknowledgement': fields.boolean('Send Acknowledgement'),
        "is_fbc": fields.boolean('FBC', ),
        "type_api_id": fields.function(_get_api_id, "Sale Integration", ),
        'ups_account': fields.char('UPS Account', size=256),
        'usps_account': fields.char('USPS Account', size=256),
        'fedex_account': fields.char('FedEx Account', size=256),
        'use_price_unit_as_retail': fields.boolean('Use unit price as retail', ),
        'sale_division': fields.char('saleDivision', size=4096),
    }

    _defaults = {
        'bulk_transfer': False,
        'send_acknowledgement': True,
        'is_fbc': False,
    }

    def write(self, cr, uid, ids, vals, context=None):
        super(res_partner, self).write(cr, uid, ids, vals, context=context)
        self.sync_team_to_ms(cr, uid, ids, vals)
        return ids

    def create(self, cr, uid, vals, context=None):
        ids = super(res_partner, self).create(cr, uid, vals, context=context)
        self.sync_team_to_ms(cr, uid, ids, vals)
        return ids

    def sync_team_to_ms(self, cr, uid, ids, vals=None, context=None):
        if not context:
            context = {}
        if not ids:
            return False

        customer_id = isinstance(ids, list) and ids[0] or ids
        customer = self.browse(cr, uid, customer_id)
        customer_name = vals.get('name', customer.name)
        customer_ref = vals.get('ref', customer.ref)
        if not customer_name or not customer_ref:
            return False
        customer_name = customer_name.replace("'", "''")
        section_id = vals.get('section_id', customer.section_id and customer.section_id.id)
        user = vals.get('user_id') and self.pool.get('res.users').browse(cr, uid, vals['user_id']) or customer.user_id
        salesman = user.name or False
        salesman_id = user.id or False
        virtual = int(vals.get('virtual', customer.virtual))
        parent_customer_id = vals.get('parent_partner_id', customer.parent_partner_id and customer.parent_partner_id.id) or False

        group_names = []
        group_ids = []

        if salesman and salesman_id:
            group_names.append(salesman)
            group_ids.append(str(salesman_id))

        team_id = False
        team_name = False
        if section_id:
            team = self.pool.get('crm.case.section').browse(cr, uid, section_id)
            if team and team.name:
                team_id = team.id
                team_name = team.name.replace("'", "''")
                # The customer.user_id is Sales Man - should be first in list
                if team.user_id and salesman != team.user_id.name:
                    group_names.append(team.user_id.name)
                    group_ids.append(str(team.user_id.id))
        # If customer has no Sales Man or Team Lead or was removed
        group_names = group_names and ','.join(group_names).replace("'", "''") or False
        group_ids = group_ids and ','.join(group_ids) or False

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(cr, uid, single=True)

        sql = """IF EXISTS(SELECT team_id FROM prg_merchandisers WHERE customer_ref = '{customer_ref}')
                BEGIN
                    UPDATE prg_merchandisers
                    SET
                    team_id = {team_id},
                    team_name = '{team_name}',
                    customer_name = '{customer_name}',
                    customer_id = {customer_id},
                    customer_ref = '{customer_ref}',
                    group_names = '{group_names}',
                    group_ids = '{group_ids}',
                    parent_customer_id = {parent_customer_id},
                    virtual = {virtual}
                    WHERE customer_ref = '{customer_ref}'
                END
            ELSE
                BEGIN
                    INSERT INTO prg_merchandisers
                    (team_id, team_name, customer_name, customer_id, customer_ref, group_names, group_ids, parent_customer_id, virtual)
                    VALUES
                    ({team_id}, '{team_name}', '{customer_name}', {customer_id}, '{customer_ref}', '{group_names}', '{group_ids}', {parent_customer_id}, {virtual})
                END
            """.format(
                customer_ref=customer_ref,
                team_id=team_id,
                team_name=team_name,
                customer_name=customer_name,
                customer_id=customer_id,
                group_names=group_names,
                group_ids=group_ids,
                parent_customer_id=parent_customer_id,
                virtual=virtual,
            ).replace("'False'", "NULL").replace("False", "NULL")
        result = server_data.make_query(cr, uid, server_id, sql, select=False, commit=True)
        return result

    def get_main_customer(self, cr, uid, customer_id, context=None):
        sale_integration_obj = self.pool.get('sale.integration')
        try:
            rp = self.read(cr, uid, customer_id, ['sale_integration_main'])
            if rp and not rp['sale_integration_main']:
                settings_ids = sale_integration_obj.search(cr, uid, [('customer_ids', '=', customer_id)])
                if settings_ids:
                    settings = sale_integration_obj.browse(cr, uid, settings_ids)[0]
                    if settings.customer_ids and len(settings.customer_ids) > 1 and settings.name != 'Import Orders':
                        for customer in settings.customer_ids:
                            if customer.sale_integration_main:
                                return customer.id
        except:
            pass
        return customer_id


res_partner()


class res_partner_account(osv.osv):
    _name = 'res.partner.account'
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner', required=True, ),
        'country_id': fields.many2one('res.country', 'Country', ),
        'service': fields.selection([
            ('ca post', 'Canada Post'),
            ('fedex', 'Fedex'),
            ('ups', 'UPS'),
            ('usps', 'USPS'),
            ('canpar', 'Canpar'),
            ('loomis', 'Loomis'),
            ('generic', 'Generic'),
            ('generic1', 'Generic 1'),
            ('generic2', 'Generic 2'),
            ('generic3', 'Generic 3'),
            ('generic4', 'Generic 4'),
            ('purolator', 'Purolator')
            ], 'Shipping Service', required=True, ),
        'account': fields.char('Account', size=256, required=True, ),
        's2s': fields.boolean('S2S Only'),
        'billing_method': fields.selection([
            ('Third party', 'Third party'),
            ('Prepaid', 'Prepaid'),
            ('Collected', 'Collect'),
        ], 'Billing Method'),
    }

    _sql_constraints = [
        ('uniq', 'unique (country_id, partner_id, service, s2s)',
            'The settings must be unique !')
    ]

    _order = "country_id, partner_id, service, s2s"

res_partner_account()
