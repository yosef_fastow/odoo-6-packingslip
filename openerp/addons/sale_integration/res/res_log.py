# -*- coding: utf-8 -*-

from osv import fields, osv


class res_log(osv.osv):
    _inherit = 'res.log'
    _columns = {
        'debug_information': fields.text('Debug information'),
        'log_type': fields.char('log type', size=250)
    }

res_log()
