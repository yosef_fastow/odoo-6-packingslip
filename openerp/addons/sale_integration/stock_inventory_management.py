# -*- coding: utf-8 -*-
##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import base64
from datetime import datetime
from openerp.addons.pf_utils.utils.binary_files_from_local_folder import _get_file, _get_size
from netsvc import ExportService
import re


class stock_inventory_management(osv.osv_memory):
    _name = "stock.inventory.management"

    _rec_name = 'partner_id'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customer', ),
        'tag_id': fields.many2one('tagging.tags', 'Tag', readonly=True, ),
        'subtag_id': fields.many2one('tagging.tags', 'Sub-Tag'),
        'type_api_id': fields.many2one('sale.integration', 'Type API'),
        'email_to': fields.char('Send email with inventory', size=256, ),
        'line_ids': fields.one2many('stock.inventory.management.line', 'report_id', 'Items', ),
        'search_product': fields.char('Product', size=256, ),
        'recalculate': fields.boolean('Show Inventory', ),
        'send_inventory': fields.boolean('Recalculate', ),
        'email': fields.boolean('Email', ),
        'update_names_list': fields.boolean('Update', ),
        'customer_field_ids': fields.related(
            'type_api_id', 'customer_field_ids',
            type='many2many', relation='product.multi.customer.fields.name',
            string='Customer Fields', ),
        'history_sent_inventory': fields.related(
            'type_api_id', 'history_sent_inventory',
            type='many2many', relation='history.sent.inventory',
            string='History Sent Inventory', ),
        'btn_inc_size_tolerance': fields.boolean('Increment Size Tolerance', ),
        'btn_dec_size_tolerance': fields.boolean('Decrement Size Tolerance', ),
        'btn_mark_all_dns': fields.boolean('Mark all DNS', ),
        'btn_unmark_all_dns': fields.boolean('Unmark all DNS', ),
        'btn_mark_all_dnr': fields.boolean('Mark all DNR', ),
        'btn_unmark_all_dnr': fields.boolean('Unmark all DNR', ),
        'btn_mark_all_send_null': fields.boolean('Mark all Send 0', ),
        'btn_unmark_all_send_null': fields.boolean('Unmark all Send 0', ),
        'btn_lock_size_tolerance': fields.boolean('Lock Size Tolerance', ),
        'btn_unlock_size_tolerance': fields.boolean('Unlock Size Tolerance', ),
    }

    _fields_as_is = [
        'product_id',
        'size_id',
        'min_qty',
        'eta',
        'customer_id_delmar',
        'customer_sku',
        'customer_price',
        'upc',
    ]
    _fields_to_float = ['size_tolerance']
    _fields_to_int = [
        'phantom_inventory',
        'qty',
        'reduce_qty',
        'ordered_qty',
        'actual_qty',
        'init_qty',
    ]
    _fields_to_bool = [
        'force_zero',
        'dnr_flag',
        'dns_flag',
        'send_without_size',
        'locked_size_tolerance',
    ]

    def email_inventory(self, cr, uid, ids, type_api_id, email_to, line_ids=None, context=None):
        value = {}
        warning = {}

        subject = 'Inventory Report'
        email_from = 'erp.delmar@progforce.com'
        email_to = [x.strip() for x in (email_to or '').split(',') if x and x.strip()]
        body = ''
        attachments = {}
        type_api = False

        if type_api_id:
            type_api = self.pool.get('sale.integration').read(cr, uid, type_api_id, ['name'])

        if not type_api:
            warning = {
                'title': _('Warning!'),
                'message': 'API not found'
                }
        elif not email_to:
            warning = {
                'title': _('Warning!'),
                'message': 'Address is not specified'
                }
        else:

            product_ids = set([])
            for line in line_ids or []:
                if line[2] and line[2].get('product_id', False):
                    product_ids.add(line[2]['product_id'])

            output = self.pool.get('sale.integration').integtationApiGetCsvAtach(cr, uid, type_api['name'], product_ids=list(product_ids), select_limit=-1, context={'write_log': False, 'is_gui_call': True})
            if output:
                attachments['Inventory(%s)%s.csv' % (type_api['name'], datetime.now().strftime("%Y%m%d%H%M"))] = base64.b64encode(output)
                self.pool.get('mail.message').schedule_with_attach(
                    cr, uid,
                    email_from, email_to, subject, body,
                    attachments=attachments, context=None
                )

        return {'value': value, 'warning': warning}

    def on_change_partner(self, cr, uid, ids, partner_id, context=None):

        value = {'type_api_id': None}
        domain = {}

        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id)
            if partner and partner.type_api_ids:
                type_api_ids = [x.id for x in partner.type_api_ids if x.si_type != 'virtual']
                domain.update({'type_api_id': [('id', 'in', type_api_ids)]})
                if type_api_ids:
                    value['type_api_id'] = type_api_ids[0]

        return {'value': value, 'domain': domain}

    def _wrap_result(self, value, warning_msg=None):
        if warning_msg:
            warning = {'title': _('Warning!'), 'message': warning_msg}
        else:
            warning = {}
        return {
            'value': value,
            'warning': warning,
        }

    def _prepare_line_ids(self, line):
        result = {}
        methods = [
            self._add_fields_as_is,
            self._add_fields_to_float,
            self._add_fields_to_int,
            self._add_fields_to_bool,
        ]
        for method in methods:
            method(line, result)
        return result

    def _add_fields_as_is(self, line, result):
        for field in self._fields_as_is:
            result[field] = line.get(field) or False

    def _add_fields_to_float(self, line, result):
        for field in self._fields_to_float:
            result[field] = float(line.get(field) or 0.0)

    def _add_fields_to_int(self, line, result):
        for field in self._fields_to_int:
            result[field] = line.get(field) or 0

    def _add_fields_to_bool(self, line, result):
        for field in self._fields_to_bool:
            result[field] = (line.get(field) or '').strip() == '1'

    def update_list(self, cr, uid, ids, type_api_id, products_str=False, update_list=True, subtag_id=False, context=None):
        value = {
            'tag_id': False,
            'line_ids': [],
            'customer_field_ids': [],
        }
        if not type_api_id:
            value['history_sent_inventory'] = [(6, 0, [])]
            return self._wrap_result(value)

        type_api = self.pool.get('sale.integration').browse(cr, uid, type_api_id)

        lh_sort_ids = [x.id for x in type_api.history_sent_inventory if x]
        lh_sort_ids.sort(reverse=True)
        value['history_sent_inventory'] = [(6, 0, lh_sort_ids)]
        value['customer_field_ids'] = [
            (6, 0, [x.id for x in type_api.customer_field_ids if x])
        ]
        if not type_api.tag:
            return self._wrap_result(value, 'Tag is not specified')
        value['tag_id'] = type_api.tag.id

        if not update_list:
            return self._wrap_result(value)
        qty_res, warning = self._get_inventory(cr, uid, type_api, subtag_id,
                                               products_str)
        for line in qty_res:
            value['line_ids'].append((0, False, self._prepare_line_ids(line)))

        return self._wrap_result(value, warning)

    def _get_product_ids(self, cr, uid, line_ids, subtag_id):
        product_ids = set()
        if line_ids:
            product_ids = set([line[2]['product_id'] for line in line_ids
                               if line[2] and line[2].get('product_id')])
        elif subtag_id:
            tag_obj = self.pool.get('tagging.tags')
            tag = tag_obj.read(cr, uid, subtag_id, ['product_ids'])
            product_ids = tag.get('product_ids')
        return list(product_ids)

    def send_inventory(self, cr, uid, ids, type_api_id, line_ids, subtag_id, context=None):
        value = {}
        if not type_api_id:
            return self._wrap_result(value, 'Api not selected')
        product_ids = self._get_product_ids(cr, uid, line_ids, subtag_id)
        api_obj = self.pool.get('sale.integration')
        api = api_obj.read(cr, uid, type_api_id, ['name'])
        res = api_obj.integrationApiUpdateQTY(cr, uid, api['name'],
                                              product_ids=product_ids,
                                              context={'is_gui_call': True})
        history = api_obj.read(cr, uid, type_api_id, ['history_sent_inventory'])['history_sent_inventory']
        history = sorted(history, reverse=True)
        value['history_sent_inventory'] = [(6, 0, history)]
        return self._wrap_result(value)

    def update_names_list(self, cr, uid, ids, partner_id, type_api_id, customer_field_ids, products_str=False, context=None):
        res = True
        self.pool.get('sale.integration').write(cr, uid, type_api_id, {
            'customer_field_ids': customer_field_ids,
            })

        res = self.update_list(cr, uid, ids, type_api_id, products_str=products_str, context=context)
        return res

    def _get_tag_warning(self, products_str, tag_names):
        if products_str:
            if len(tag_names) == 1:
                warning = 'In the tag %s' % tag_names[0]
            else:
                warning = 'In the tags %s' % ', '.join(tag_names)
            warning += ', there is no product(s) %s.' % products_str
        else:
            warning = "Tags %s have no shared products" % ', '.join(tag_names)
        return warning

    def _search_products_by_str(self, cr, uid, products_str):
        product_ids = set()
        prod_obj = self.pool.get('product.product')
        prod_names = re.findall(r"[\w-]+", products_str)
        for prod_name in list(set(prod_names)):
            if not prod_name:
                continue
            products = prod_obj.name_search(cr, uid, name=prod_name,
                                            operator='ilike',
                                            context={'mode': 'simple'})
            product_ids |= set([x[0] for x in products])
        return product_ids

    def _get_inventory(self, cr, uid, api, subtag_id, products_str):
        lines = []
        products_str = (products_str or '').strip()
        product_ids = self._search_products_by_str(cr, uid, products_str)
        if products_str and not product_ids:
            return (lines, 'Product(s) %s not found in system.' % products_str)
        if subtag_id == api.tag.id:
            tag_ids = [subtag_id]
        else:
            tag_ids = [t for t in subtag_id, api.tag.id if t]
        tag_obj = self.pool.get('tagging.tags')
        tag_names = []
        for tag_id in tag_ids:
            tag = tag_obj.read(cr, uid, tag_id, ['name', 'product_ids'])
            tag_product_ids = tag.get('product_ids')
            if not tag_product_ids:
                return (lines, 'Tag %s is empty.' % tag['name'])
            tag_names.append(tag['name'])
            if product_ids:
                product_ids &= set(tag_product_ids)
                if not product_ids:
                    warning = self._get_tag_warning(products_str, tag_names)
                    return (lines, warning)
            elif tag_id == subtag_id:
                product_ids = set(tag_product_ids)
            else:
                return (lines, None)

        si_obj = self.pool.get('sale.integration')
        context = {'write_log': False, 'is_gui_call': True}
        lines = si_obj.integrationApiGetQTY(cr, uid, api.name, select_limit=-1,
                                            product_ids=list(product_ids),
                                            context=context)
        return (lines, None)

    def mass_change_parametres(self, cr, uid, ids, partner_id, type_api_id, line_ids, change_id, action, products_str=False, context=None):

        if not context:
            context = {}

        ignore_fields = [
            'phantom',
        ]

        if line_ids:
            cf_name = False
            force_value = None
            if change_id == "phantom":
                cf_name = 'Phantom Inventory'
                get_expand = """
                    SELECT expand_ring_sizes
                    FROM product_multi_customer_fields_name
                    WHERE customer_id = %s
                    AND label = 'Phantom Inventory';
                """ % partner_id
                cr.execute(get_expand)
                ph_inv_expand_ring = cr.fetchone()
                if not ph_inv_expand_ring[0]:
                    print 'Ignore updating Phantom Inventory, because not expanded by size'
                    line_ids = []

            elif change_id in ("dns", "dnr", "force_zero", "size_tolerance", "locked_size_tolerance"):
                if change_id == "dns":
                    cf_name = 'DNS'
                elif change_id == "dnr":
                    cf_name = 'DNR'
                elif change_id == "force_zero":
                    cf_name = 'Force Zero'
                elif change_id == "size_tolerance":
                    cf_name = 'Size Tolerance'
                elif change_id == "locked_size_tolerance":
                    cf_name = 'Locked Size Tolerance'

                if action == "mark":
                    force_value = True
                elif action == "unmark":
                    force_value = False

            if not cf_name:
                line_ids = []

            for line in line_ids:
                if change_id in ignore_fields and line[2]["send_without_size"]:
                    continue

                product_id = line[2]["product_id"]
                size_id = line[2]["size_id"]
                if change_id == "size_tolerance":
                    if action == "inc":
                        value = float(line[2]["size_tolerance"]) + 0.5
                    elif action == "dec":
                        if float(line[2]["size_tolerance"]) == 0.0:
                            value = 0.0
                        elif float(line[2]["size_tolerance"]) > 0.0:
                            value = float(line[2]["size_tolerance"]) - 0.5
                        else:
                            continue

                self.pool.get('stock.inventory.management.line').change_cf(
                    cr,
                    uid,
                    ids,
                    product_id,
                    size_id,
                    partner_id,
                    type_api_id,
                    cf_name,
                    value if force_value is None else force_value,
                    all_sizes=line[2]["send_without_size"],
                    context=context,
                )

        return self.update_list(cr, uid, ids, type_api_id, products_str=products_str, context=context)

stock_inventory_management()


class stock_inventory_management_line(osv.osv_memory):
    _name = "stock.inventory.management.line"

    _rec_name = "product_id"

    _columns = {
        'report_id': fields.many2one('stock.inventory.management', 'Report', ),
        'product_id': fields.many2one('product.product', 'Delmar ID', required=True, readonly=True, ),
        'size_id': fields.many2one('ring.size', 'Size', readonly=True, ),
        'size_tolerance': fields.float('Size Tolerance', ),
        'dns_flag': fields.boolean('DNS', ),
        'dnr_flag': fields.boolean('DNR', ),
        'customer_id_delmar': fields.char('Customer ID Delmar', size=128, ),
        'upc': fields.char('UPC', size=128, ),
        'customer_sku': fields.char('Customer SKU', size=128, ),
        'customer_price': fields.char('Customer Price', size=128, ),
        'eta': fields.char('ETA', size=128, ),
        'min_qty': fields.char('Min Qty', size=128, ),
        'phantom_inventory': fields.char('Phantom Inventory', size=128, ),
        'force_zero': fields.boolean('Send 0', ),
        'send_without_size': fields.boolean('Combined Items', ),
        'locked_size_tolerance': fields.boolean('Locked size tolerance', ),
        'init_qty': fields.char('Real Stock', size=128, readonly=True, ),
        'reduce_qty': fields.char('Required for orders qty', size=128, readonly=True, ),
        'ordered_qty': fields.char('On PO order qty', size=128, readonly=True, ),
        'actual_qty': fields.char('Available qty', size=128, readonly=True, ),
        'qty': fields.char('Feed qty', size=128, readonly=True, ),
    }

    def change_cf(self, cr, uid, ids, product_id, size_id, partner_id, type_api_id, cf_name, value, all_sizes=False, context=None):
        if not context:
            context = {}

        cm_f_obj = self.pool.get('product.multi.customer.fields')
        cm_fm_obj = self.pool.get('product.multi.customer.fields.name')


        type_api = self.pool.get('sale.integration').read(cr, uid, type_api_id, ['name', 'customer_field_ids'])
        cm_fields_name_ids = cm_fm_obj.search(cr, uid, [('label', '=', cf_name), ('id', 'in', type_api['customer_field_ids'])], context=context)

        if cm_fields_name_ids:

            if value == True:
                value = '1'
            elif value == False:
                value = '0'

            cm_fields_name = cm_fm_obj.read(cr, uid, cm_fields_name_ids[0], ['expand_ring_sizes', 'name_id'])
            if not type_api:
                raise osv.except_osv(_('Warning!'), 'API is not found')

            if not (cm_fields_name['name_id'] and cm_fields_name['name_id'][1] == 'locked size tolerance') and cm_fields_name['id'] not in type_api.get('customer_field_ids', []):
                raise osv.except_osv(_('Warning!'), 'Please add %s in to the list of customer fields for %s before edit this value' % (cf_name, type_api['name']))

            if not cm_fields_name.get('expand_ring_sizes', False):
                size_id = False

            size_ids = [size_id]
            if all_sizes and cm_fields_name.get('expand_ring_sizes', False):
                size_ids = [False] + self.pool.get('product.product').read(cr, uid, product_id, ['ring_size_ids'])['ring_size_ids']

            psswd = self.pool.get('res.users').read(cr, uid, uid, ['password']).get('password')

            for size_id in size_ids:
                cm_field_domain = [
                    ('product_id', '=', product_id),
                    ('field_name_id', '=', cm_fields_name['id']),
                    ('size_id', '=', size_id),
                    ]

                cm_field_ids = cm_f_obj.search(cr, uid, cm_field_domain)
                if cm_field_ids:
                    params = (self.pool.db.dbname, uid, psswd, cm_f_obj._name, 'write', (cm_field_ids, {'value': value}, {'uid': uid, 'tz': 'America/Montreal'}), {})
                    # cm_f_obj.write(cr, uid, cm_field_ids, {'value': value})
                else:
                    cm_f_vals = {
                        'product_id': product_id,
                        'size_id': size_id,
                        'field_name_id': cm_fields_name['id'],
                        'value': value,
                        }
                    params = (self.pool.db.dbname, uid, psswd, cm_f_obj._name, 'create', (cm_f_vals, {'uid': uid, 'tz': 'America/Montreal'}), {})
                    # cm_f_obj.create(cr, uid, cm_f_vals)
                ExportService.getService('object').dispatch('execute_kw', params)

        else:
            raise osv.except_osv(_('Warning!'), 'Not Found Multi Customer Field Name "%s" in Inventory fields list' % (cf_name))


stock_inventory_management_line()


class history_sent_inventory(osv.osv):
    _name = "history.sent.inventory"

    _columns = {
        'create_date': fields.datetime('Sent date', readonly=True, ),
        'type_api_id': fields.many2one('sale.integration', 'Type API', readonly=True, ),
        'create_uid': fields.many2one('res.users', 'User', readonly=True, ),
        'path_to_file': fields.char('Path to file', size=128, readonly=True, ),
        'filename': fields.char('File name', size=128, readonly=True, ),
        'full_filename': fields.char('Full Filename', size=128, readonly=True, ),
        'download': fields.function(_get_file, type='binary2', method=True, string='Download', readonly=True),
        'filename_readable': fields.char('Readable file name', size=128, readonly=True, ),
        'file_readable': fields.char('Readable file path', size=128, readonly=True, ),
        'download_readable': fields.function(_get_file, type='binary2', method=True, string='Download readable file', readonly=True, context={'field': 'file_readable'}),
        'size': fields.function(_get_size, type='string', method=True, string='Size', readonly=True),
    }

    _defaults = {
        'filename_readable': False,
        'file_readable': False,
        'download_readable': False,
    }


history_sent_inventory()
