# -*- coding: utf-8 -*-
from osv import fields, osv
import re
import csv
import xlrd
import base64
import netsvc
from tools.translate import _
from datetime import datetime
from cStringIO import StringIO
from api.utils.configutils import Config
from os.path import dirname

METHOD_TYPE_SELECTION = [
    ('amz', 'Amazon'),
    ('internal', 'Internal'),
    ('ebay', 'eBay'),
    ('os', 'OS'),
    ('amz_rtl', 'Amazon Retail'),
    ('bulk', 'Bulk'),
]

FILE_FORMAT_SELECTION = [
    ('csv', 'CSV'),
    ('xls', 'XLS'),
    ('xlsx', 'XLSX')
]

CSV_SEPARATOR_SELECTION = [
    (',', 'comma'),
    ('\t', 'tab'),
    (';', 'semicolon'),
]


def _get_method(self, cr, uid, context=None):
    return (METHOD_TYPE_SELECTION)


def _get_file_format(self, cr, uid, context=None):
    return (FILE_FORMAT_SELECTION)


def _get_csv_separator(self, cr, uid, context=None):
    return (CSV_SEPARATOR_SELECTION)


class sale_order_import_customer_mapping(osv.osv):
    _name = "sale.order.import.customer.mapping"
    _columns = {
        'code': fields.char('Code', size=256, required=True,),
        'method': fields.selection([
            ('amz', 'Amazon'),
            ('internal', 'Internal'),
            ('amz_rtl', 'Amazon Retail'),
        ], 'Method', required=True,),
        'customer_id': fields.many2one('res.partner', 'Customer', ondelete='cascade', required=True, domain="[('customer', '=', True)]"),
    }

    _defaults = {
        'method': 'internal',
    }

    _sql_constraints = [
        ('code_method_uniq', 'unique (code, method)', 'The combination of code and method must be unique !'),
    ]

    def create(self, cr, uid, vals, context=None):
        if 'code' in vals:
            vals['code'] = self.clear_code(cr, uid, vals['code'])

        return super(sale_order_import_customer_mapping, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if 'code' in vals:
            vals['code'] = self.clear_code(cr, uid, vals['code'])

        return super(sale_order_import_customer_mapping, self).write(cr, uid, ids, vals, context=context)

    def clear_code(self, cr, uid, code=False):
        if code:
            code = code.strip().upper()
        return code

    def get_customer_id(self, cr, uid, code=False, method='internal', context=None):
        customer_id = False

        if code:

            sql = """ SELECT customer_id FROM %s WHERE code = '%s' and method = '%s' """ % (self._table, code, method)
            cr.execute(sql)
            customer_ids = cr.fetchone()
            if customer_ids:
                customer_id = customer_ids[0]

        return customer_id

sale_order_import_customer_mapping()


class sale_order_import(osv.osv):
    _name = "sale.order.import"

    def _get_mapping(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for row in self.browse(cr, uid, ids):
            res[row.id] = self.get_notes(cr, uid, row.method)

        return res

    _columns = {
        'import_file': fields.binary(string="Input file", filters='*.csv,*.CSV', ),
        "state": fields.selection([('draft', 'Draft'), ('confirm', 'Confirmed')], 'Status', select=True, required=True, readonly=True),
        'order_id': fields.many2one('sale.order', 'Sale order', ondelete='cascade', size=128),
        "customer_id": fields.many2one('res.partner', 'Customer', ondelete='cascade', required=False,),
        'method': fields.selection(_get_method, 'Select method', required=True,),
        'file_format': fields.selection(_get_file_format, 'File format', select=True, required=True),
        'csv_separator': fields.selection(_get_csv_separator, 'Select csv separator'),
        'mapping_notes': fields.function(_get_mapping, string='Mappings', type='text', mathod=True,),
        'status_create_orders': fields.selection(
            [
                ('yes', 'YES'),
                ('no', 'NO')
            ],
            'Create orders after download file at once?',
            select=True,
            required=True,
        ),
    }

    _defaults = {
        'state': 'draft',
        'import_file': '',
        'method': 'internal',
        'csv_separator': ',',
        'file_format': 'csv',
        'status_create_orders': 'no',
    }

    def onchange_method(self, cr, uid, ids, method, context=None):

        result = {
            'method': method,
            'mapping_notes': self.get_notes(cr, uid, method),
        }

        return {'value': result}

    def get_notes(self, cr, uid, method=False, context=None):
        notes = []
        if method:
            mapping_obj = self.pool.get('sale.order.import.customer.mapping')

            sql = """   SELECT 'if ' || mp.code || ', customer ' || rp.ref
                        FROM %s mp LEFT JOIN res_partner rp on (rp.id = mp.customer_id)
                        WHERE mp.method = '%s';
            """ % (mapping_obj._table, method)
            cr.execute(sql)
            mappings = cr.fetchall()
            for mp in mappings:
                notes.append(mp[0])

        return ";\n".join(notes)

    def get_size(self, cr, uid, ids, size_str):
        size_str = size_str.replace(',', '.')
        if '.' not in size_str:
            size_str = size_str + '.0'
        size_ids = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_str)])
        if size_ids:
            return size_ids[0]
        else:
            return False

    def ship_address(self, cr, uid, ids, addr_obj):
        address = {}
        address['street'] = addr_obj.get('Address1')
        address['street2'] = addr_obj.get('Address2')
        address['zip'] = addr_obj.get('Zip')
        address['city'] = addr_obj.get('City')

        country = addr_obj.get('Country', 'US')
        ids_name = self.pool.get("res.country").name_search(cr, uid, country)
        if(len(ids_name) > 0):
            address["country_id"] = ids_name[0][0]
        if addr_obj.get('State'):
            ids_name = self.pool.get("res.country.state").name_search(cr, uid, addr_obj.get('State'))
            if(len(ids_name) > 0):
                address["state_id"] = ids_name[0][0]

        address_id = self.pool.get("res.partner.address").create(cr, uid, address)

        return address_id

    def getShop(self, cr, uid, ids):
        shop_ids = self.pool.get("sale.shop").search(cr, uid, ids)
        return self.pool.get("sale.shop").browse(cr, uid, shop_ids[0])

    def getProductBySku(self, cr, uid, sku):
        prod_ids = self.pool.get("product.product").search(cr, uid, [('default_code', '=', sku)])
        if(len(prod_ids) > 0):
            return self.pool.get("product.product").browse(cr, uid, prod_ids[0])
        else:
            return False

    def getPartner(self, cr, uid, order, customer_id):
        ids = self.pool.get("res.partner").search(cr, uid, ['|', ('external_customer_id', '=', order['partner_id']), ('id', 'in', [customer_id])])
        if(len(ids) > 0):
            return self.pool.get("res.partner").browse(cr, uid, ids[0])

    def getAddress(self, cr, uid, type_address, order, customer):
        address_id = self.pool.get('sale.integration').getAddress(cr, uid, type_address, order, customer)
        return address_id

    def parse_os_xls(self, cr, uid, xls_data):
        rb = xlrd.open_workbook(file_contents=xls_data)
        sheet = rb.sheet_by_index(0)
        idx = {}
        head_row = sheet.row_values(0)
        for head in head_row:
            idx[head] = head_row.index(head)

        required_cols = [
            'Order #',
            'Sent To Warehouse',
            'Overstock Order Date',
            'Quantity',
            'Overstock SKU',
            'Partner SKU',
            'Shipping Details',
            'Overstock Cost',
            'Shipping Cost',
            'Unit Price',
            'Ship Method',
            'Status'
        ]

        missing_cols = [x for x in required_cols if x not in head_row]
        if missing_cols:
            raise osv.except_osv(_('Warning'), _('Wrong xls format. Missing the following columns: %s.' % (', '.join(missing_cols))))

        orderObj = {}
        ordersList = []

        for rownum in range(sheet.nrows)[1:]:
            row = sheet.row_values(rownum)
            if not row[idx.get('Order #')]:
                continue

            if not orderObj.get('order_id', False) or orderObj.get('order_id', False) != row[idx.get('Order #')]:
                line_id = 0
                orderObj = {
                    'carrier': row[idx.get('Ship Method')],
                    'partner_id': 'overstock_canada',
                    'order_id': row[idx.get('Order #')],
                    'lines': [],
                }
                regexp = re.compile('^(.*?)\s{8}(.*?)\s{8}(.*?)\s{8}(.*?),\s{8}(.*?)-(.*?)$')
                match = regexp.findall(row[idx.get('Shipping Details')])
                if match:
                    orderObj['address'] = {}
                    orderObj['address']['ship'] = {}
                    orderObj['address']['ship']['name'] = match[0][0]
                    orderObj['address']['ship']['address1'] = match[0][1]
                    orderObj['address']['ship']['address2'] = match[0][2]
                    orderObj['address']['ship']['city'] = match[0][3]
                    orderObj['address']['ship']['zip'] = match[0][5]
                    orderObj['address']['ship']['country'] = 'Canada'
                    orderObj['address']['ship']['phone'] = ''
                    orderObj['address']['ship']['state'] = match[0][4]
                ordersList.append(orderObj)
            line_id += 1
            lineObj = {
                'id': line_id,
                'optionSku': row[idx.get('Partner SKU')],
                'sku': row[idx.get('Overstock SKU')],
                'merchantSKU' : row[idx.get('Overstock SKU')],
                'qty': row[idx.get('Quantity')],
                # 'cost': row[idx.get('Unit Price')] and row[idx.get('Unit Price')][1:],
                'cost': row[idx.get('Overstock Cost')] and row[idx.get('Overstock Cost')] if row[idx.get('Overstock Cost')][0].isdigit() else row[idx.get('Overstock Cost')][1:],
                'customerCost': row[idx.get('Unit Price')] and row[idx.get('Unit Price')] if row[idx.get('Unit Price')][0].isdigit() else row[idx.get('Unit Price')][1:],
            }

            lineObj["notes"] = "id: %s\nQTY: %s\nPrice: %s\nProduct: %s" % tuple(
                [lineObj.get(x, 'None') for x in ['id', 'qty', 'cost', 'product_id']]
            )

            add_new_line = True
            for line in orderObj['lines']:
                if line['product_id'] == lineObj['product_id']:
                    line["notes"] += "\n\n" + lineObj["notes"]
                    line["qty"] += lineObj["qty"]
                    if line["id"] != lineObj["id"]:
                        line["id"] += "," + lineObj["id"]
                    add_new_line = False
                    break

            if add_new_line:
                orderObj['lines'].append(lineObj)

        return ordersList

    def process_os_xls(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        if ordersList and widget:
            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')
            data_obj = self.pool.get('ir.model.data')

            for orderObj in ordersList:
                print orderObj
                if orderObj.get('order_id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['order_id'])])
                    if(len(order_ids) == 0):
                        sale_order = {}
                        shop = self.getShop(cr, uid, [])
                        sale_order['external_customer_order_id'] = orderObj['order_id']
                        sale_order['shop_id'] = shop.id
                        sale_order['pricelist_id'] = shop.pricelist_id.id
                        sale_order['po_number'] = sale_order['external_customer_order_id']
                        sale_order['type_api_id'] = data_obj.get_object_reference(cr, uid, 'sale_integration', 'ir_sale_integration_os_provider')[1] or False
                        customer = self.getPartner(cr, uid, orderObj, widget.customer_id.id)
                        main_customer_id = partner_obj.get_main_customer(cr, uid, customer.id)
                        customer_address = partner_obj.address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])
                        sale_order['partner_id'] = customer.id

                        for i in ('carrier', 'external_date_order'):
                            sale_order[i] = orderObj.get(i, False)
                        for type_address in orderObj.get('address', []):
                            address_id = self.getAddress(cr, uid, type_address, orderObj, customer)
                            if(type_address == "ship"):
                                sale_order['partner_shipping_id'] = address_id
                            elif(type_address == "order"):
                                sale_order['partner_order_id'] = address_id
                            elif(type_address == "invoice"):
                                sale_order['partner_invoice_id'] = address_id

                        if(not sale_order.get('partner_order_id', False)):
                            sale_order['partner_order_id'] = customer_address['contact']

                        if(not sale_order.get('partner_invoice_id', False)):
                            sale_order['partner_invoice_id'] = customer_address['invoice']
                        sale_order['state'] = 'draft'
                        order_id = sale_order_obj.create(cr, uid, sale_order)

                        line_ids = []
                        note = ""
                        for line in orderObj.get('lines', []):
                            line_obj = {}
                            field_list = ['sku', 'optionSku']
                            for field in field_list:
                                if line.get(field, False):
                                    product, size = product_obj.search_product_by_id(cr, uid, main_customer_id, line[field])
                                    if product:
                                        if size and size.id:
                                            line_obj["size_id"] = size.id
                                        break
                            if(product):
                                line_obj["product_id"] = product.id
                            else:
                                note += "Can't find product by sku %s" % (line['sku'])
                                line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
                                line_obj["product_id"] = 0

                            line_obj["name"] = ''
                            line_obj["order_id"] = order_id
                            line_obj["product_uom_qty"] = line['qty']
                            line_obj["product_uom"] = 1
                            line_obj["price_unit"] = line['cost']
                            line_obj["external_customer_line_id"] = line['id']

                            for i in ('size', 'customerCost', 'merchantLineNumber', 'merchantSKU', 'optionSku', 'shipCost', 'syncPrice'):
                                line_obj[i] = line.get(i, False)

                            if(line.get('size', False)):
                                size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                                if(len(size_ids) > 0):
                                    line_obj['size_id'] = size_ids[0]

                            line_obj['state'] = 'draft'
                            line_obj['type'] = 'make_to_stock'
                            line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                        if(note != ""):
                            sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                        else:
                            wf_service = netsvc.LocalService("workflow")
                            wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                        print order_id

    def parse_internal_csv(self, cr, uid, csv_data, csv_separator):
        # dt_start = datetime.now()
        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        delimiter = csv_separator
        csv_head_row = csv.reader([import_rows[0]], delimiter=str(delimiter)).next() or []
        head_row = [row.strip().lower() if row else row for row in csv_head_row]
        idx = {}
        for col in head_row:
            idx[col] = head_row.index(col)

        required_cols = [
            'order_id',
            'customer',
            'delmar_id',
            'qty',
            'size',
            'po',
            'cost/pc',
            'cdn/usa',
            'requested_by',
            'cog'
        ]

        missing_cols = [x for x in required_cols if x not in head_row]
        if missing_cols:
            raise osv.except_osv(_('Warning'), _('Wrong csv format. Missing the following columns: %s.' % (', '.join(missing_cols))))

        orderObj = {}
        ordersList = []

        for row in import_rows[1:]:
            if not row:
                continue

            row = [row for row in csv.reader([row], delimiter=str(delimiter))][0]
            row = [el if type(el) is not str else el.strip().upper() for el in row]
            # print row

            if not row[idx.get('order_id')]:
                continue

            if not orderObj.get('id', False) or orderObj.get('id', False) != row[idx.get('order_id')]:

                orderObj = {
                    'id': row[idx.get('order_id')],
                    'country': row[idx.get('cdn/usa')],
                    'lines': [],
                }

                ordersList.append(orderObj)

            if not orderObj.get('country', False) and row[idx.get('cdn/usa')]:
                orderObj['country'] = row[idx.get('cdn/usa')]

            invoiceObj = {
                'id': row[idx.get('po')],
                'product_id': row[idx.get('delmar_id')],
                'merchantSKU': row[idx.get('delmar_id')],
                'size': row[idx.get('size')],
                'qty': row[idx.get('qty')],
                'cost': row[idx.get('cost/pc')],
                'customer': (row[idx.get('customer')]),
                'country': (row[idx.get('cdn/usa')]),
                'requested_by': row[idx.get('requested_by')],
                'cog': row[idx.get('cog')],
            }

            for key in ['size', 'qty', 'cost']:
                value = invoiceObj.get(key, 0.0)
                if value:
                    if key == 'cost' and isinstance(value, (str, unicode)):
                        value = value.replace('$', '')
                    try:
                        value = float(value)
                        if key == 'size' and (value / 100.0) > 1.0:
                            value = value / 100.0
                    except:
                        value = 0.0
                invoiceObj[key] = value

            invoiceObj["notes"] = "PO Number: %s\nQTY: %s\nPrice: %s\nRequested by: %s\nCOG: %s\nCustomer: %s\nCDN/USA: %s" % tuple(
                [invoiceObj.get(x, 'None') for x in ['id', 'qty', 'cost', 'requested_by', 'cog', 'customer', 'country']]
            )

            add_new_line = True
            for line in orderObj['lines']:
                if line['product_id'] == invoiceObj['product_id'] and line['size'] == invoiceObj['size']:
                    line["notes"] += "\n\n" + invoiceObj["notes"]
                    line["qty"] += invoiceObj["qty"]
                    if line["id"] != invoiceObj["id"]:
                        line["id"] += "," + invoiceObj["id"]
                    add_new_line = False
                    break

            if add_new_line:
                orderObj['lines'].append(invoiceObj)

        # print orderList
        # print "Parsing time %s " % (datetime.now() - dt_start)
        return ordersList

    def process_internal_order(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        dt_start = datetime.now()
        if ordersList and widget:

            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')
            customer_mapping_obj = self.pool.get('sale.order.import.customer.mapping')
            data_obj = self.pool.get('ir.model.data')

            for orderObj in ordersList:
                print orderObj
                dt_start_order_process = datetime.now()

                if orderObj.get('id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['id'])])
                    if order_ids:
                        sale_order_obj.log(cr, uid, order_ids[0], "Order with \"External Customer Order ID\" = %s already exist" % (orderObj['id']))
                        continue

                sale_order = {}
                partner = False

                shop = self.getShop(cr, uid, [])

                if orderObj.get('country', False):
                    customer_name = False
                    partner_id = False

                    code = orderObj['country']
                    partner_id = customer_mapping_obj.get_customer_id(cr, uid, code)

                    if not partner_id:
                        if code == 'CDN':
                            customer_name = 'MNTL'
                        elif code == 'USA':
                            customer_name = 'IFC'

                        if customer_name:
                            partner_ids = partner_obj.search(cr, uid, [('name', '=', customer_name)], limit=1)
                            if partner_ids:
                                partner_id = partner_ids[0]

                    if partner_id:
                        partner = partner_obj.browse(cr, uid, partner_id)

                    if not partner:
                        raise osv.except_osv(_('Warning'), _('Can not create sale order. Customer for %s not found.' % (code)))

                else:
                    raise osv.except_osv(_('Warning'), _('Can not create sale order. Missing cdn/usa'))

                sale_order['state'] = 'draft'
                sale_order['carrier'] = 'Internal'
                sale_order['ship2store'] = True
                sale_order['shop_id'] = shop.id
                sale_order['pricelist_id'] = shop.pricelist_id.id
                sale_order['external_customer_order_id'] = orderObj.get('id', False)
                sale_order['po_number'] = orderObj.get('id', False)
                sale_order['type_api_id'] = data_obj.get_object_reference(cr, uid, 'sale_integration', 'ir_sale_integration_internal_provider')[1] or False

                sale_order['partner_id'] = partner.id
                addr = partner_obj.address_get(cr, uid, [partner.id], ['delivery', 'invoice', 'contact'])
                if addr:
                    sale_order['partner_invoice_id'] = addr['invoice']
                    sale_order['partner_order_id'] = addr['contact']
                    sale_order['partner_shipping_id'] = addr['delivery']

                if not sale_order.get('partner_shipping_id', False):
                    raise osv.except_osv(_('Warning'), _('Can not create sale order. Partner %s has not delivery address.' % (partner.name)))

                order_id = sale_order_obj.create(cr, uid, sale_order)

                line_ids = []
                note = ""
                for line in orderObj.get('lines', []):
                    line_obj = {}
                    product_name = False
                    dt = datetime.now()
                    line_obj["notes"] = line['notes']

                    # product, size = product_obj.search_product_by_id(cr, uid, partner and partner.id, line['product_id'])
                    # print "search_product_by_id %s " % (datetime.now() - dt)
                    # if(product):
                    #     line_obj["product_id"] = product.id
                    #     product_name = product_obj.name_get(cr, uid, [product.id])[0][1]
                    #     print "product_obj.name_get %s " % (datetime.now() - dt)
                    product_names = product_obj.name_search(cr, uid, line['product_id'], operator="=", context={'mode': 'simple'}, limit=1)
                    print "search_product %s " % (datetime.now() - dt)
                    if product_names:
                        product_id, product_name = product_names[0]
                        line_obj["product_id"] = product_id
                    else:
                        note += "Can't find product by sku %s" % (line['product_id'])
                        line_obj["notes"] += "\n\nCan't find product by sku %s , debug info %s\n" % (line['product_id'], str(line))
                        line_obj["product_id"] = 0
                        product_name = line['product_id']

                    line_obj['state'] = 'draft'
                    line_obj['type'] = 'make_to_stock'
                    line_obj["name"] = product_name
                    line_obj["order_id"] = order_id
                    line_obj["product_uom_qty"] = line['qty']
                    line_obj["product_uom"] = 1
                    line_obj["price_unit"] = line['cost']
                    line_obj["external_customer_line_id"] = line.get('id', 'External Customer Line ID')

                    for i in ('size', 'customerCost', 'merchantLineNumber', 'merchantSKU', 'optionSku', 'shipCost', 'syncPrice'):
                        line_obj[i] = line.get(i, False)

                    if(line.get('size', False)):
                        size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                        if(len(size_ids) > 0):
                            line_obj['size_id'] = size_ids[0]

                    if line.get('customer', False):
                        line_obj['sec_customer'] = line['customer']

                    line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                if note:
                    sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})

                created_order = sale_order_obj.read(cr, uid, order_id, ['name'])

                sale_order_obj.log(cr, uid, order_id, "Sale Order %s is created.%s" % (created_order['name'], note and ' With Shipping Exception.' or ''))
                # print order_id
                print "Total process time %s: %s " % (created_order['name'], datetime.now() - dt_start_order_process)

        else:
            raise osv.except_osv(_('Warning'), _('Nothing to import'))

        print "Total orders process time %s " % (datetime.now() - dt_start)
        return True

    def parse_amazon_csv(self, cr, uid, csv_data, csv_separator):

        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        delimiter = csv_separator
        csv_head_row = csv.reader([import_rows[0]], delimiter=str(delimiter)).next() or []
        head_row = [row.strip().lower() if row else row for row in csv_head_row]
        idx = {}
        for col in head_row:
            idx[col] = head_row.index(col)

        ordersList = []
        order_id_old = ""
        ordersObjTmpl = {
            'order_id': False,
            'lines': [],
            'partner_id': 'Amazon',
        }
        ordersObj = ordersObjTmpl
        for row in import_rows[1:]:
            row = [row for row in csv.reader([row], delimiter=str(delimiter))][0]
            row = [el if type(el) is not str else el.strip() for el in row]
            if(row):
                print row
                if not ordersObj.get('order_id', False):
                    ordersObj['order_id'] = row[idx.get('order-id')]
                elif(ordersObj.get('order_id', False) and order_id_old != row[idx.get('order-id')]):
                    order_id_old = ""
                    ordersList.append(ordersObj)
                    ordersObj = ordersObjTmpl
                    ordersObj['order_id'] = row[idx.get('order-id')]

                if(ordersObj.get('order_id', False) and order_id_old != ordersObj.get('order_id', False)):
                    ordersObj['external_date_order'] = row[idx.get('purchase-date')]
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['name'] = row[idx.get('recipient-name')]
                    ordersObj['address']['ship']['address1'] = row[idx.get('ship-address-1')]
                    ordersObj['address']['ship']['address2'] = row[idx.get('ship-address-2')]
                    ordersObj['address']['ship']['city'] = row[idx.get('ship-city')]
                    ordersObj['address']['ship']['state'] = row[idx.get('ship-state')]
                    ordersObj['address']['ship']['zip'] = row[idx.get('ship-postal-code')]
                    ordersObj['address']['ship']['country'] = row[idx.get('ship-country')]
                    ordersObj['address']['ship']['phone'] = row[idx.get('ship-phone-number')]
                    ordersObj['carrier'] = 'DDC' + row[idx.get('ship-service-level')]
                    order_id_old = ordersObj['order_id']
                invoceObj = {}
                invoceObj['id'] = str(int(float(row[idx.get('order-item-id')])))
                invoceObj['qty'] = row[idx.get('quantity-purchased')]
                if(not invoceObj['qty']):
                    raise osv.except_osv('Error!', 'Cannot find "Quantity" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                sku = row[idx.get('sku')]
                if(not sku):
                    raise osv.except_osv('Error!', 'Cannot find "Vendor SKU" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                if(sku.find("/") == -1):
                    invoceObj['sku'] = sku
                    invoceObj['size'] = False
                else:
                    invoceObj['sku'] = sku[:sku.find("/")]
                    invoceObj['size'] = sku[sku.find("/") + 1:]
                invoceObj['merchantSKU'] = invoceObj['sku']
                invoceObj['name'] = row[idx.get('product-name')]
                invoceObj['shipCost'] = row[idx.get('shipping-price')]
                invoceObj['cost'] = row[idx.get('item-price')]
                invoceObj['Message'] = row[idx.get('gift-message-text')]
                ordersObj['lines'].append(invoceObj)

        if ordersObj:

            if not ordersObj.get('order_id', False):
                raise osv.except_osv(_('Warning'), """Wrong csv format. "order-id" cannot be empty!""")
            else:
                ordersList.append(ordersObj)

        return ordersList

    def process_amz_csv(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        if ordersList and widget:

            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')

            for orderObj in ordersList:
                print orderObj
                if orderObj.get('order_id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['order_id'])])
                    if(len(order_ids) == 0):
                        sales_order = {}
                        shop = self.getShop(cr, uid, [])
                        sales_order['external_customer_order_id'] = orderObj['order_id']
                        sales_order['shop_id'] = shop.id
                        sales_order['pricelist_id'] = shop.pricelist_id.id
                        customer = self.getPartner(cr, uid, orderObj, widget.customer_id.id)
                        main_customer_id = partner_obj.get_main_customer(cr, uid, customer.id)
                        customer_address = partner_obj.address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])
                        sales_order['partner_id'] = customer.id

                        for i in ('carrier', 'external_date_order'):
                            sales_order[i] = orderObj.get(i, False)
                        for type_address in orderObj.get('address', []):
                            address_id = self.getAddress(cr, uid, type_address, orderObj, customer)
                            if(type_address == "ship"):
                                sales_order['partner_shipping_id'] = address_id
                            elif(type_address == "order"):
                                sales_order['partner_order_id'] = address_id
                            elif(type_address == "invoice"):
                                sales_order['partner_invoice_id'] = address_id

                        if(not sales_order.get('partner_order_id', False)):
                            sales_order['partner_order_id'] = customer_address['contact']

                        if(not sales_order.get('partner_invoice_id', False)):
                            sales_order['partner_invoice_id'] = customer_address['invoice']
                        sales_order['state'] = 'draft'
                        order_id = sale_order_obj.create(cr, uid, sales_order)

                        line_ids = []
                        note = ""
                        for line in orderObj.get('lines', []):
                            line_obj = {}
                            product, size = product_obj.search_product_by_id(cr, uid, main_customer_id, line['sku'])
                            if(product):
                                line_obj["product_id"] = product.id
                            else:
                                note += "Can't find product by sku %s, name %s" % (line['sku'], line['name'])
                                line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
                                line_obj["product_id"] = 0

                            line_obj["name"] = line['name']
                            line_obj["order_id"] = order_id
                            line_obj["product_uom_qty"] = line['qty']
                            line_obj["product_uom"] = 1
                            line_obj["price_unit"] = line['cost']
                            line_obj["external_customer_line_id"] = line['id']

                            for i in ('size', 'customerCost', 'merchantLineNumber', 'merchantSKU', 'optionSku', 'shipCost', 'syncPrice'):
                                line_obj[i] = line.get(i, False)

                            if(line.get('size', False)):
                                size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                                if(len(size_ids) > 0):
                                    line_obj['size_id'] = size_ids[0]

                            line_obj['state'] = 'draft'
                            line_obj['type'] = 'make_to_stock'
                            line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                        if(note != ""):
                            sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                        else:
                            wf_service = netsvc.LocalService("workflow")
                            wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                        print order_id

    def parse_eBay_csv(self, cr, uid, csv_data, csv_separator):

        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        delimiter = csv_separator
        csv_head_row = csv.reader([import_rows[0]], delimiter=str(delimiter)).next() or []
        head_row = [row.strip().lower() if row else row for row in csv_head_row]
        idx = {}
        for col in head_row:
            idx[col] = head_row.index(col)

        ordersList = []
        order_id_old = ""

        ordersObjTmpl = {
            'order_id': False,
            'lines': [],
            'partner_id': 'eBay',
        }
        ordersObj = ordersObjTmpl

        for row in import_rows[1:]:
            row = [row for row in csv.reader([row], delimiter=str(delimiter))][0]
            row = [el if type(el) is not str else el.strip() for el in row]
            if(row):
                print row
                if not row[idx.get('order id')]:
                    continue

                if not ordersObj.get('order_id', False):
                    ordersObj['order_id'] = row[idx.get('order id')]
                elif(ordersObj.get('order_id', False) and order_id_old != row[idx.get('order id')]):
                    order_id_old = ""
                    ordersList.append(ordersObj)
                    ordersObj = ordersObjTmpl
                    ordersObj['order_id'] = row[idx.get('order id')]

                if(ordersObj.get('order_id', False) and order_id_old != ordersObj.get('order_id', False)):
                    ordersObj['external_date_order'] = row[idx.get('sale date')]
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['name'] = row[idx.get('buyer fullname')]
                    ordersObj['address']['ship']['address1'] = row[idx.get('buyer address 1')]
                    ordersObj['address']['ship']['address2'] = row[idx.get('buyer address 2')]
                    ordersObj['address']['ship']['city'] = row[idx.get('buyer city')]
                    ordersObj['address']['ship']['state'] = row[idx.get('buyer state')]
                    ordersObj['address']['ship']['zip'] = row[idx.get('buyer zip')]
                    ordersObj['address']['ship']['country'] = row[idx.get('buyer country')]
                    ordersObj['address']['ship']['phone'] = row[idx.get('buyer phone number')]
                    ordersObj['carrier'] = row[idx.get('shipping service')]
                    ordersObj['tracking_ref'] = row[idx.get('tracking number')]
                    order_id_old = ordersObj['order_id']

                if row[idx.get('item id')]:
                    invoceObj = {}
                    invoceObj['id'] = str(int(float(row[idx.get('item id')])))
                    invoceObj['qty'] = row[idx.get('quantity')]
                    if(not invoceObj['qty']):
                        raise osv.except_osv('Error!', 'Cannot find "Quantity" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                    sku = row[idx.get('custom label')]
                    if(not sku):
                        raise osv.except_osv('Error!', 'Cannot find "Vendor SKU" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                    if(sku.find("/") == -1):
                        invoceObj['sku'] = sku
                        invoceObj['size'] = False
                    else:
                        invoceObj['sku'] = sku[:sku.find("/")]
                        invoceObj['size'] = sku[sku.find("/") + 1:]
                    invoceObj['merchantSKU'] = invoceObj['sku']
                    invoceObj['name'] = row[idx.get('item title')]
                    invoceObj['shipCost'] = row[idx.get('shipping and handling')]
                    invoceObj['shipCost'] = invoceObj['shipCost'] and invoceObj['shipCost'].replace('$', '')
                    invoceObj['cost'] = row[idx.get('sale price')]
                    invoceObj['cost'] = invoceObj['cost'] and invoceObj['cost'].replace('$', '')
                    # invoceObj['Message'] = row[idx.get('gift-message-text')]
                    invoceObj['tax'] = row[idx.get('sales tax')]
                    invoceObj['tax'] = invoceObj['tax'] and invoceObj['tax'].replace('$', '')
                    invoceObj['insurance'] = row[idx.get('insurance')]
                    ordersObj['lines'].append(invoceObj)

        if ordersObj:

            if not ordersObj.get('order_id', False):
                raise osv.except_osv(_('Warning'), """Wrong csv format. "Order ID" cannot be empty!""")
            else:
                ordersList.append(ordersObj)

        return ordersList

    def process_eBay_csv(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        if ordersList and widget:

            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')

            for orderObj in ordersList:
                print orderObj
                if orderObj.get('order_id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['order_id'])])
                    if(len(order_ids) == 0):
                        sales_order = {}
                        shop = self.getShop(cr, uid, [])
                        sales_order['external_customer_order_id'] = orderObj['order_id']
                        sales_order['po_number'] = orderObj['order_id']
                        sales_order['tracking_ref'] = orderObj['tracking_ref']
                        sales_order['shop_id'] = shop.id
                        sales_order['pricelist_id'] = shop.pricelist_id.id
                        customer = self.getPartner(cr, uid, orderObj, widget.customer_id.id)
                        main_customer_id = partner_obj.get_main_customer(cr, uid, customer.id)
                        customer_address = partner_obj.address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])
                        sales_order['partner_id'] = customer.id
                        sales_order['type_api_id'] = customer.type_api_ids and customer.type_api_ids[0].id or False

                        for i in ('carrier', 'external_date_order'):
                            sales_order[i] = orderObj.get(i, False)
                        for type_address in orderObj.get('address', []):
                            address_id = self.getAddress(cr, uid, type_address, orderObj, customer)
                            if(type_address == "ship"):
                                sales_order['partner_shipping_id'] = address_id
                            elif(type_address == "order"):
                                sales_order['partner_order_id'] = address_id
                            elif(type_address == "invoice"):
                                sales_order['partner_invoice_id'] = address_id

                        if(not sales_order.get('partner_order_id', False)):
                            sales_order['partner_order_id'] = customer_address['contact']

                        if(not sales_order.get('partner_invoice_id', False)):
                            sales_order['partner_invoice_id'] = customer_address['invoice']
                        sales_order['state'] = 'draft'
                        order_id = sale_order_obj.create(cr, uid, sales_order)

                        line_ids = []
                        note = ""
                        for line in orderObj.get('lines', []):
                            line_obj = {}
                            product, size = product_obj.search_product_by_id(cr, uid, main_customer_id, line['sku'], ('default_code', 'customer_sku', 'parent_sku'))
                            if(product):
                                line_obj["product_id"] = product.id
                            else:
                                note += "Can't find product by sku %s, name %s" % (line['sku'], line['name'])
                                line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
                                line_obj["product_id"] = 0

                            line_obj["name"] = line['name']
                            line_obj["order_id"] = order_id
                            line_obj["product_uom_qty"] = line['qty']
                            line_obj["product_uom"] = 1
                            line_obj["price_unit"] = line['cost']
                            line_obj["external_customer_line_id"] = line['id']

                            for i in ('size',
                                      'customerCost',
                                      'merchantLineNumber',
                                      'merchantSKU',
                                      'optionSku',
                                      'shipCost',
                                      'syncPrice',
                                      'tax'):
                                line_obj[i] = line.get(i, False)

                            if(line.get('size', False)):
                                size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                                if(len(size_ids) > 0):
                                    line_obj['size_id'] = size_ids[0]

                            line_obj['state'] = 'draft'
                            line_obj['type'] = 'make_to_stock'
                            line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                        if(note != ""):
                            sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                        else:
                            wf_service = netsvc.LocalService("workflow")
                            wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                        print order_id

    def parse_amazon_retail_csv(self, cr, uid, csv_data, csv_separator):
        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        delimiter = csv_separator
        csv_head_row = csv.reader([import_rows[0]], delimiter=str(delimiter)).next() or []
        head_row = [row.strip().lower() if row else row for row in csv_head_row]
        idx = {}
        for col in head_row:
            idx[col] = head_row.index(col)

        ordersList = []
        order_id_old = ""
        ordersObjTmpl = {
            'order_id': False,
            'lines': [],
            'partner_id': 'Amazon Retail',
        }
        ordersObj = ordersObjTmpl
        order_item_id = 1
        for row in import_rows[1:]:
            row = [row for row in csv.reader([row], delimiter=str(delimiter))][0]
            row = [el if type(el) is not str else el.strip() for el in row]
            if(row):
                print row
                if not ordersObj.get('order_id', False):
                    ordersObj['order_id'] = row[idx.get('po_id')]
                elif(ordersObj.get('order_id', False) and order_id_old != row[idx.get('po_id')]):
                    order_id_old = ""
                    order_item_id = 1
                    ordersList.append(ordersObj)
                    ordersObj = ordersObjTmpl
                    ordersObj['order_id'] = row[idx.get('po_id')]

                if(ordersObj.get('order_id', False) and order_id_old != ordersObj.get('order_id', False)):
                    ordersObj['external_date_order'] = row[idx.get('po_date')]
                    ordersObj['latest_ship_date_order'] = row[idx.get('end_ship')]
                    ordersObj['earliest_ship_date_order'] = row[idx.get('start_ship')]
                    # ordersObj['fulfillment_center'] = row[idx.get('FC')]
                    ordersObj['address'] = {}
                    ordersObj['address']['ship'] = {}
                    ordersObj['address']['ship']['address_id'] = row[idx.get('fc')]
                    # ordersObj['address']['ship']['name'] = row[idx.get('recipient-name')]
                    # ordersObj['address']['ship']['address1'] = row[idx.get('ship-address-1')]
                    # ordersObj['address']['ship']['address2'] = row[idx.get('ship-address-2')]
                    # ordersObj['address']['ship']['city'] = row[idx.get('ship-city')]
                    # ordersObj['address']['ship']['state'] = row[idx.get('ship-state')]
                    # ordersObj['address']['ship']['zip'] = row[idx.get('ship-postal-code')]
                    # ordersObj['address']['ship']['country'] = row[idx.get('ship-country')]
                    # ordersObj['address']['ship']['phone'] = row[idx.get('ship-phone-number')]
                    # ordersObj['carrier'] = 'DDC' + row[idx.get('ship-service-level')]
                    order_id_old = ordersObj['order_id']
                invoceObj = {}
                invoceObj['id'] = order_item_id
                invoceObj['qty'] = row[idx.get('confirm_qty')]
                if(not invoceObj['qty']):
                    raise osv.except_osv('Error!', 'Cannot find "Quantity" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                sku = row[idx.get('external_id')]
                if(not sku):
                    raise osv.except_osv('Error!', 'Cannot find "Vendor SKU" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                if(sku.find("/") == -1):
                    invoceObj['sku'] = sku
                    invoceObj['size'] = False
                else:
                    invoceObj['sku'] = sku[:sku.find("/")]
                    invoceObj['size'] = sku[sku.find("/") + 1:] if sku[sku.find("/") + 1:].isdigit() else False
                invoceObj['merchantSKU'] = invoceObj['sku']
                invoceObj['name'] = row[idx.get('title')]
                # invoceObj['shipCost'] = row[idx.get('shipping-price')]
                invoceObj['cost'] = row[idx.get('cost')]
                invoceObj['listCost'] = row[idx.get('list_price')]
                invoceObj['expected_ship_date'] = row[idx.get('esd')]
                # invoceObj['Message'] = row[idx.get('gift-message-text')]
                ordersObj['lines'].append(invoceObj)
                order_item_id += 1

        if ordersObj:

            if not ordersObj.get('order_id', False):
                raise osv.except_osv(_('Warning'), """Wrong csv format. "order-id" cannot be empty!""")
            else:
                ordersList.append(ordersObj)

        return ordersList

    def process_amazon_retail_csv(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        if ordersList and widget:

            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')

            for orderObj in ordersList:
                print orderObj
                if orderObj.get('order_id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['order_id'])])
                    if(len(order_ids) == 0):
                        sales_order = {}
                        shop = self.getShop(cr, uid, [])
                        sales_order['po_number'] = orderObj['order_id']
                        sales_order['external_customer_order_id'] = orderObj['order_id']
                        sales_order['shop_id'] = shop.id
                        sales_order['type_api'] = 'amazon_retail'
                        type_api_id = self.pool.get('sale.integration').search(cr, uid, [("type_api", "=", sales_order['type_api'])])
                        if type_api_id:
                            sales_order['type_api_id'] = type_api_id[0]
                        sales_order['pricelist_id'] = shop.pricelist_id.id
                        customer = self.getPartner(cr, uid, orderObj, widget.customer_id.id)
                        main_customer_id = partner_obj.get_main_customer(cr, uid, customer.id)
                        customer_address = partner_obj.address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])
                        sales_order['partner_id'] = customer.id
                        sales_order['date_order'] = orderObj['external_date_order']
                        sales_order['latest_ship_date_order'] = orderObj['latest_ship_date_order']
                        sales_order['earliest_ship_date_order'] = orderObj['earliest_ship_date_order']

                        for i in ('carrier', 'external_date_order'):
                            sales_order[i] = orderObj.get(i, False)
                        for type_address in orderObj.get('address', []):
                            address_id = self.getAddress(cr, uid, type_address, orderObj, customer)
                            if(type_address == "ship"):
                                sales_order['partner_shipping_id'] = address_id
                            elif(type_address == "order"):
                                sales_order['partner_order_id'] = address_id
                            elif(type_address == "invoice"):
                                sales_order['partner_invoice_id'] = address_id

                        if(not sales_order.get('partner_order_id', False)):
                            sales_order['partner_order_id'] = customer_address['contact']

                        if(not sales_order.get('partner_invoice_id', False)):
                            sales_order['partner_invoice_id'] = customer_address['invoice']
                        sales_order['state'] = 'draft'
                        order_id = sale_order_obj.create(cr, uid, sales_order)

                        line_ids = []
                        note = ""
                        for line in orderObj.get('lines', []):
                            line_obj = {}
                            product, size = product_obj.search_product_by_id(cr, uid, main_customer_id, line['sku'])
                            if(product):
                                line_obj["product_id"] = product.id
                                if size and size.id:
                                    line_obj['size_id'] = size.id
                            else:
                                note += "Can't find product by sku %s, name %s" % (line['sku'], line['name'])
                                line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
                                line_obj["product_id"] = 0


                            line_obj["name"] = line['name']
                            line_obj["order_id"] = order_id
                            line_obj["product_uom_qty"] = line['qty']
                            line_obj["product_order_qty"] = line_obj['product_uom_qty']
                            line_obj["product_uom"] = 1
                            line_obj["price_unit"] = line['cost']
                            line_obj["listCost"] = line['listCost']
                            line_obj["external_customer_line_id"] = line['id']
                            line_obj["expected_ship_date"] = line['expected_ship_date']
                            line_obj['type_api'] = 'amazon_retail'

                            for i in ('size', 'customerCost', 'merchantLineNumber', 'merchantSKU', 'optionSku', 'shipCost', 'syncPrice'):
                                line_obj[i] = line.get(i, False)

                            if(line.get('size', False)):
                                # import pdb; pdb.set_trace()
                                # size_name = str(float(line['size'])) if line['size'].isdigit() else False
                                # if size_name:
                                size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                                if(len(size_ids) > 0):
                                    line_obj['size_id'] = size_ids[0]

                            line_obj['state'] = 'draft'
                            line_obj['type'] = 'make_to_stock'
                            line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                        if(note != ""):
                            sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                        # else:
                        #     wf_service = netsvc.LocalService("workflow")
                        #     wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                        print order_id

    def parse_bulk_csv(self, cr, uid, csv_data, csv_separator=",", csv_quotechar='"'):
        _cur_dir = dirname(__file__)
        _import_config = Config('%s/sale_order_import_settings.yaml' % _cur_dir)
        _settings = _import_config.read()
        partner_obj = self.pool.get('res.partner')
        ordersList = []

        ids = _settings['bulk']['ids']

        import_rows = csv_data.replace('\r\n', '\n').replace('\r', '\n').split('\n')
        csv_head_row = csv.reader([import_rows[0]], delimiter=str(csv_separator)).next() or []
        head_row = [row.strip().lower() if row else row for row in csv_head_row]

        if csv_data:
            init_obj = list(
                csv.reader(
                    StringIO(csv_data),
                    delimiter=str(csv_separator),
                    quotechar=csv_quotechar
                )
            )
            data_list = []
            for i, _list in enumerate(init_obj):
                if(i != 0):
                    data_list.append(init_obj[i])
            idx = {}
            for col in head_row:
                idx[col] = head_row.index(col)

            def get_index(name):
                try:
                    result = idx.get(ids[name], ids[name])
                except KeyError:
                    result = "None"
                finally:
                    return result

            def get_value(order_line, index):
                if(index is None):
                    result = "None"
                elif(isinstance(index, int)):
                    result = order_line[index]
                else:
                    result = index
                return result

            for i, row in enumerate(data_list):
                if(get_value(row, get_index('item_id')) == ""):
                    continue
                _external_cust_id = get_value(row, get_index('reference')).strip()
                cust_ids = partner_obj.search(cr, uid, [('external_customer_id', '=', _external_cust_id)])
                cust_name = False
                cust_id = False
                if(cust_ids):
                    cust_id = cust_ids[0]
                    cust_name = partner_obj.browse(cr, uid, cust_ids[0]).name.lower()
                orderObjTmp = {
                    'order_id': False,
                    'lines': [],
                    'partner_id': cust_name,
                    'cust_id': cust_id,
                }
                orderObj = orderObjTmp
                if(orderObj['partner_id'] is False):
                    try:
                        raise osv.except_osv(
                            _('Warning'),
                            """Couldn't get partner ID!
                            Bad value in column: %s
                            Line: %s in input csv file.
                            Skip this line."""
                            % (
                                str(int(get_index('reference'))+1),
                                str(i+1)
                            )
                        )
                    except ValueError:
                        raise osv.except_osv(
                            _('Warning'),
                            """Bad value for reference in config file:
                            %s"""
                            % _import_config.filename
                        )
                    continue
                row = [el if type(el) is not str else el.strip() for el in row]
                if(row):
                    print(row)
                    if not orderObj.get('order_id', False):
                        orderObj['order_id'] = get_value(row, get_index('order_id'))
                    exist_orderObj = False
                    for prev_order in ordersList:
                        if(prev_order['order_id'] == orderObj['order_id']):
                            exist_orderObj = True
                            break
                    if(orderObj.get('order_id', False) and (not exist_orderObj)):
                        orderObj = orderObjTmp
                        orderObj['order_id'] = get_value(row, get_index('order_id'))
                        _date_order = get_value(row, get_index('external_date_order'))
                        try:
                            _date_order = datetime.strptime(_date_order, '%m/%d/%Y')
                        except ValueError:
                            try:
                                _date_order = datetime.strptime(_date_order, '%m-%d-%Y')
                            except ValueError:
                                _date_order = datetime.now()
                        finally:
                            _date_order = '{:%Y-%m-%d}'.format(_date_order)
                        orderObj['external_date_order'] = _date_order
                        orderObj['address'] = {}
                        orderObj['address']['ship'] = {}
                        orderObj['address']['ship']['name'] = get_value(row, get_index('name'))
                        orderObj['address']['ship']['address1'] = get_value(row, get_index('address1'))
                        orderObj['address']['ship']['address2'] = get_value(row, get_index('address2'))
                        orderObj['address']['ship']['city'] = get_value(row, get_index('city'))
                        orderObj['address']['ship']['state'] = get_value(row, get_index('state'))
                        orderObj['address']['ship']['zip'] = get_value(row, get_index('zip'))
                        orderObj['address']['ship']['country'] = get_value(row, get_index('country'))
                        orderObj['address']['ship']['phone'] = get_value(row, get_index('phone'))
                        _carrier1 = get_value(row, get_index('carrier1'))
                        _carrier2 = get_value(row, get_index('carrier2'))
                        orderObj['carrier'] = _carrier1 + '_' + _carrier2
                        orderObj['store_number'] = get_value(row, get_index('store_number'))
                        orderObj['bulk_transfer'] = True
                    invoceObj = {}
                    invoceObj['id'] = str(int(float(get_value(row, get_index('item_id')))))
                    invoceObj['qty'] = get_value(row, get_index('quantity'))
                    if(not invoceObj['qty']):
                        raise osv.except_osv('Error!', 'Cannot find "Quantity" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                    sku = get_value(row, get_index('sku'))
                    if(not sku):
                        raise osv.except_osv('Error!', 'Cannot find "Vendor SKU" column in csv (order_id: ' + str(invoceObj['id']) + ')')
                    invoceObj['sku'] = sku
                    invoceObj['size'] = get_value(row, get_index('size'))
                    invoceObj['name'] = get_value(row, get_index('description'))
                    invoceObj['shipCost'] = False
                    invoceObj['customerCost'] = get_value(row, get_index('unit_cost'))
                    invoceObj['unit_price'] = get_value(row, get_index('unit_price'))
                    invoceObj['customerCost'] = invoceObj['customerCost'] and invoceObj['customerCost'].replace('$', '')
                    invoceObj['unit_price'] = invoceObj['unit_price'] and invoceObj['unit_price'].replace('$', '')
                    invoceObj['tax'] = get_value(row, get_index('tax'))
                    invoceObj['tax'] = invoceObj['tax'] and invoceObj['tax'].replace('$', '')
                    invoceObj['insurance'] = get_value(row, get_index('insurance'))
                    invoceObj['merchantSKU'] = get_value(row, get_index('merchantSKU'))
                    if(exist_orderObj):
                        for prev_order in ordersList:
                            if(prev_order['order_id'] == orderObj['order_id']):
                                prev_order['lines'].append(invoceObj)
                                orderObj = None
                                break
                    else:
                        orderObj['lines'].append(invoceObj)
                if(orderObj):
                    if not orderObj.get('order_id', False):
                        raise osv.except_osv(_('Warning'), """Wrong csv format. "Order ID" cannot be empty!""")
                    else:
                        ordersList.append(orderObj)
        return ordersList

    def process_bulk_csv(self, cr, uid, widget, ordersList=None, context=None):
        if not ordersList: ordersList = []
        if ordersList and widget:

            partner_obj = self.pool.get('res.partner')
            sale_order_obj = self.pool.get('sale.order')
            sale_order_line_obj = self.pool.get('sale.order.line')
            product_obj = self.pool.get('product.product')
            ring_size_obj = self.pool.get('ring.size')

            for orderObj in ordersList:
                print orderObj
                if orderObj.get('order_id', False):
                    order_ids = sale_order_obj.search(cr, uid, [("external_customer_order_id", "=", orderObj['order_id'])])
                    if(len(order_ids) == 0):
                        sales_order = {}
                        shop = self.getShop(cr, uid, [])
                        sales_order['external_customer_order_id'] = orderObj['order_id']
                        sales_order['po_number'] = orderObj['order_id']
                        sales_order['shop_id'] = shop.id
                        sales_order['pricelist_id'] = shop.pricelist_id.id
                        customer = self.getPartner(cr, uid, orderObj, orderObj['cust_id'])
                        main_customer_id = partner_obj.get_main_customer(cr, uid, customer.id)
                        customer_address = partner_obj.address_get(cr, uid, [customer.id], ['default', 'invoice', 'contact'])
                        sales_order['partner_id'] = orderObj['cust_id']
                        sales_order['type_api_id'] = customer.type_api_ids and customer.type_api_ids[0].id or False
                        sales_order['bulk_transfer'] = orderObj.get('bulk_transfer', True)
                        for i in ('carrier', 'external_date_order'):
                            sales_order[i] = orderObj.get(i, False)
                        for type_address in orderObj.get('address', []):
                            address_id = self.getAddress(cr, uid, type_address, orderObj, customer)
                            if(type_address == "ship"):
                                sales_order['partner_shipping_id'] = address_id
                            elif(type_address == "order"):
                                sales_order['partner_order_id'] = address_id
                            elif(type_address == "invoice"):
                                sales_order['partner_invoice_id'] = address_id

                        if(not sales_order.get('partner_order_id', False)):
                            sales_order['partner_order_id'] = customer_address['contact']

                        if(not sales_order.get('partner_invoice_id', False)):
                            sales_order['partner_invoice_id'] = customer_address['invoice']
                        sales_order['state'] = 'draft'
                        sales_order['store_number'] = orderObj['store_number']
                        order_id = sale_order_obj.create(cr, uid, sales_order)

                        line_ids = []
                        note = ""
                        for line in orderObj.get('lines', []):
                            line_obj = {}
                            product, size = product_obj.search_product_by_id(cr, uid, main_customer_id, line['sku'])
                            if(product):
                                line_obj["product_id"] = product.id
                            else:
                                note += "Can't find product by sku %s, name %s" % (line['sku'], line['name'])
                                line_obj["notes"] = "Can't find product by sku %s , debug info %s" % (line['sku'], str(line))
                                line_obj["product_id"] = 0

                            line_obj["name"] = line['name']
                            line_obj["order_id"] = order_id
                            line_obj["product_uom_qty"] = line['qty']
                            line_obj["product_uom"] = 1
                            line_obj["price_unit"] = line['unit_price']
                            line_obj["external_customer_line_id"] = line['id']

                            for i in ('size',
                                      'customerCost',
                                      'merchantLineNumber',
                                      'merchantSKU',
                                      'optionSku',
                                      'shipCost',
                                      'syncPrice',
                                      'tax'):
                                line_obj[i] = line.get(i, False)

                            if(line.get('size', False)):
                                size_ids = ring_size_obj.search(cr, uid, [('name', '=', str(float(line['size'])))])
                                if(len(size_ids) > 0):
                                    line_obj['size_id'] = size_ids[0]

                            line_obj['state'] = 'draft'
                            line_obj['type'] = 'make_to_stock'
                            line_ids.append(sale_order_line_obj.create(cr, uid, line_obj))

                        if(note != ""):
                            sale_order_obj.write(cr, uid, order_id, {"state": "shipping_except", "note": note})
                        else:
                            wf_service = netsvc.LocalService("workflow")
                            wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                        print order_id

    def import_confirm(self, cr, uid, ids, context=None, *args):

        dt_start = datetime.now()

        ordersList = []
        cur = self.browse(cr, uid, ids[0])

        if cur.import_file:
            if (cur.method == 'internal'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_internal_csv(cr, uid, file_data, cur.csv_separator)
                self.process_internal_order(cr, uid, cur, ordersList)

            elif(cur.method == 'amz'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_amazon_csv(cr, uid, file_data, cur.csv_separator)
                self.process_amz_csv(cr, uid, cur, ordersList)

            elif(cur.method == 'os'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_os_xls(cr, uid, file_data)
                self.process_os_xls(cr, uid, cur, ordersList)

            elif(cur.method == 'ebay'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_eBay_csv(cr, uid, file_data, cur.csv_separator)
                self.process_eBay_csv(cr, uid, cur, ordersList)
            elif(cur.method == 'amz_rtl'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_amazon_retail_csv(cr, uid, file_data, cur.csv_separator)
                self.process_amazon_retail_csv(cr, uid, cur, ordersList)
            elif(cur.method == 'bulk'):
                file_data = base64.decodestring(cur.import_file)
                if file_data:
                    ordersList = self.parse_bulk_csv(cr, uid, file_data, cur.csv_separator, '"')
                self.process_bulk_csv(cr, uid, cur, ordersList)
        else:
            raise osv.except_osv('File is not selected', '')

        print "Total time %s " % (datetime.now() - dt_start)
        return False

sale_order_import()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
