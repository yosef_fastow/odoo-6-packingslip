# -*- coding: utf-8 -*-

from osv import fields, osv
import logging


class product_product(osv.osv):
    _inherit = 'product.product'
    _columns = {
        'syncDay': fields.integer('syncDay'),
        'overstockSku': fields.text('OverStock sku'),
        'overstockOptionSku': fields.text('OverStock Option sku'),
        'overstockProductStatus': fields.text('OverStock ProductStatus'),
        'overstockMSRP': fields.text('OverStock MSRP'),
        'overstockSuggestedSellingPrice': fields.text('OverStock SuggestedSellingPrice'),
        'overstockCost': fields.text('OverStock Cost'),
        'overstockDescription': fields.text('OverStock Description')

    }

    _defaults = {
        'syncDay': 0
    }

product_product()


class product_option_overstock(osv.osv):
    _name = 'product.option.overstock'

    _columns = {
        'product_id': fields.integer('Product'),
        'VendorSku': fields.text('VendorSku'),
        'OptionSku': fields.text('OptionSku'),
        'Description': fields.text('Description'),
        'Cost': fields.text('Cost'),
        'Quantity': fields.text('Quantity'),
        'Weight': fields.text('Weight'),
        'UPC': fields.text('UPC')
    }

product_option_overstock()


class product_option_overstock_xml(osv.osv):
    _name = 'product.option.overstock.xml'

    _columns = {
        'product_id': fields.integer('Product'),
        'VendorSku': fields.text('VendorSku'),
        'OptionSku': fields.text('OptionSku'),
        'Description': fields.text('Description'),
        'Cost': fields.text('Cost'),
        'Quantity': fields.text('Quantity'),
        'Weight': fields.text('Weight'),
        'UPC': fields.text('UPC'),
        'date': fields.datetime('Creation Date')
    }

product_option_overstock_xml()


class product_overstock_field_xml(osv.osv):
    _name = 'product.overstock.field.xml'

    _columns = {
        'product_id': fields.integer('Product'),
        'ProductSku': fields.text('ProductSku'),
        'Name': fields.text('Name'),
        'ShortName': fields.text('ShortName'),
        'Summary': fields.text('Summary'),
        'Description': fields.text('Description'),
        'Status': fields.text('Status'),
        'CategoryId': fields.text('CategoryId'),
        'ISBN': fields.text('ISBN'),
        'ReleaseDate': fields.text('ReleaseDate'),
        'ProductDimensions': fields.text('ProductDimensions'),
        'LTL': fields.text('LTL'),
        'ShipAlone': fields.text('ShipAlone'),
        'SourceZipCode': fields.text('SourceZipCode'),
        'CountryOfOrigin': fields.text('CountryOfOrigin'),
        'Condition': fields.text('Condition'),
        'Brand': fields.text('Brand'),
        'Pricing_MSRP': fields.text('Pricing_MSRP'),
        'Pricing_MSRPSalesLocation': fields.text('Pricing_MSRPSalesLocation'),
        'Pricing_StreetPrice': fields.text('Pricing_StreetPrice'),
        'Pricing_NormalWholesalePrice': fields.text('Pricing_NormalWholesalePrice'),
        'Pricing_SuggestedSellingPrice': fields.text('Pricing_SuggestedSellingPrice'),
        'PackageDimensions_Length': fields.text('PackageDimensions_Length'),
        'PackageDimensions_Width': fields.text('PackageDimensions_Width'),
        'PackageDimensions_Height': fields.text('PackageDimensions_Height'),
        'Manufacturer_Name': fields.text('Manufacturer_Name'),
        'Manufacturer_PartNumber': fields.text('Manufacturer_PartNumber'),
        'Manufacturer_ModelNumber': fields.text('Manufacturer_ModelNumber'),
        'Warranty_Provider': fields.text('Warranty_Provider'),
        'Warranty_Description': fields.text('Warranty_Description'),
        'Warranty_ContactPhoneNumber': fields.text('Warranty_ContactPhoneNumber'),
        'Specifications_Materials': fields.text('Specifications_Materials'),
        'Images_Thumbnail': fields.text('Images_Thumbnail'),
        'Images_Main': fields.text('Images_Main'),
        'Images_Large': fields.text('Images_Large'),
        'date': fields.datetime('Creation Date')
    }

    def init(self, cr):
        # Create functions for partitioning of inventory history
        cr.execute("""
            CREATE OR REPLACE FUNCTION product_overstock_field_xml_trigger()
            RETURNS TRIGGER AS $$
            DECLARE
                range VARCHAR;
                master_table varchar;
                section_table varchar;
            BEGIN
                range := to_char(NEW.date, 'YYYY_MM');
                master_table := TG_TABLE_NAME;
                section_table := concat(master_table, '_', range);
                execute 'create table if not exists ' || quote_ident(section_table) || ' (LIKE ' || quote_ident(master_table) || ' INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES, check (to_char(create_date, ''YYYY_MM'') = ''' || range || ''')) inherits ( ' || quote_ident(master_table) || ');';
                execute format('INSERT INTO %I VALUES ($1.*)', section_table) USING NEW;
                RETURN NULL;
            END;
            $$
            LANGUAGE plpgsql;

            DROP TRIGGER IF EXISTS insert_product_overstock_field_xml_trigger ON product_overstock_field_xml;

            CREATE TRIGGER insert_product_overstock_field_xml_trigger
            BEFORE INSERT ON product_overstock_field_xml
            FOR EACH ROW EXECUTE PROCEDURE product_overstock_field_xml_trigger();

        """)

product_overstock_field_xml()


class product_overstock_sku(osv.osv):
    _name = 'product.overstock.sku'

    _columns = {
        'DelmarProductID': fields.text('DelmarProductID'),
        'CustomerProductID': fields.text('CustomerProductID')
    }

product_overstock_sku()
