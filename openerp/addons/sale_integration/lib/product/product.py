import logging
_logger = logging.getLogger(__name__)


class Product:
    cr = None
    uid = None
    pool = None

    _xml_cost_difference_percent = 2
    _chub_cost_difference_percent = 0
    _ignore_price_difference = False

    def __init__(self, cr, uid, pool):
        self.cr = cr
        self.uid = uid
        self.pool = pool

    def get_product_and_size(self, line, customer_id):
        product, size = self.pool.get('product.product').search_product_by_id(
            self.cr, self.uid, customer_id, line['sku'])
        return product, size

    def calculate_price(self, parameters):
        customer = parameters.get('customer', None)
        if customer is None:
            raise Exception('For calculating the price you need set \'customer\'')

        line = parameters.get('line', None)
        if line is None:
            raise Exception('For calculating the price you need set \'line\'')

        customer_id = parameters.get('customer_id', None)
        if customer_id is None:
            raise Exception('For calculating the price you need set \'customer_id\'')

        product_id = parameters.get('product_id', None)
        if product_id is None:
            raise Exception('For calculating the price you need set \'product_id\'')

        size_id = parameters.get('size_id', None)
        if size_id is None:
            raise Exception('For calculating the price you need set \'size_id\'')

        note = ''
        price = False

        if not product_id:
            note = 'The price can not be set because the product is not defined. \n'
            return price, note

        if customer['invoice_cost_from_acc_price']:
            acc_price = self.get_price_from_acc_price(customer, product_id)
            if acc_price:
                return acc_price, note

        xml_cost_price = self.get_price_from_xml(line, parameters.get('price_from_file', 0.0))
        # DLMR-1364
        # Use another field to get customer price for GovX
        customer_price_field = None
        if parameters.get('product_gwp', False):
            customer_price_field = 'Customer Price GWP'
            _logger.info("GOT GWP REQUEST, using field: %s" % customer_price_field)
        # END DLMR-1364 mains

        cf_price = self.get_price_from_customer_field(customer_id=customer_id, product_id=product_id, size_id=size_id, field_name=customer_price_field)

        com_cf_price = cf_price
        if not cf_price:
            cf_price = self.get_price_from_customer_field(customer_id=customer_id, product_id=product_id, size_id=False, field_name=customer_price_field)
            size_id = False
        cf_price = self.calculate_discounts(product_id, size_id, customer_id, cf_price)
        cf_price = float(str(cf_price).replace('$', '').replace(',', '')) if cf_price else False
        if xml_cost_price and cf_price:
            _logger.info('Possible difference def: %s, CHUB: %s' % (self._xml_cost_difference_percent, self._chub_cost_difference_percent))
            percent_difference = round(100 * (1 - float(xml_cost_price) / float(cf_price)), 2)
            cent_difference = float(cf_price) - float(xml_cost_price)
            _logger.info('Calculated difference: %s' % percent_difference)

            if (parameters.get('type_api', '') == 'commercehub' and parameters.get('customer', '').external_customer_id == 'signetkj' and cf_price):
                return cf_price, note

            if (parameters.get('type_api', '') != 'commercehub' and percent_difference <= self._xml_cost_difference_percent) \
                    or (parameters.get('type_api', '') == 'commercehub' and percent_difference <= self._chub_cost_difference_percent) \
                    or (parameters.get('type_api', '') == 'commercehub' and cent_difference <= 0.01)\
                    or (self._ignore_price_difference == True):
                if customer.ref =='JMS':
                    if float(com_cf_price) > cf_price:
                        return float(cf_price), note
                return float(xml_cost_price), note
            else:
                note = "Product cost from xml is %d percent lower then " \
                       "product cost for customer field for name %s \n" % (percent_difference, line['name'])
                return False, note
        if not xml_cost_price:
            note += "UnitCost is not defined\n"
        if not cf_price:
            note += "Customer Price from Customer Price field is not defined\n"
        return price, note

    def get_price_from_xml(self, line, price_from_file=0.0):
        price = 0
        try:
            price = float(price_from_file or line.get('cost'))
        except Exception:
            pass
        return price

    def get_price_from_acc_price(self, customer, product_id):
        price = 0
        if customer and customer.invoice_cost_from_acc_price:
            price = self.get_acc_price_price(product_id, customer)
        return price

    def calculate_discounts(self, product_id, size_id, customer_id, price):
        discount_model = self.pool.get('product.discount')
        price = discount_model.get_result_price(
            self.cr,
            self.uid,
            product_id,
            size_id=size_id,
            partner_id=customer_id,
            price=price
        )
        return price

    def get_price_from_customer_field(self, customer_id, product_id,  size_id, field_name=None):
        if not field_name:
            field_name = 'Customer Price'
        price = self.pool.get('product.product').get_val_by_label(
            self.cr, self.uid, product_id, customer_id, field_name, size_id) or False
        return price

    # Get price from acc_price table
    def get_acc_price_price(self, product_id, partner):
        res = 0.0
        default_code = self.pool.get('product.product').read(self.cr, self.uid, product_id, ['default_code']).get(
            'default_code',
            False)
        acc_price_code = partner and (partner.acc_price_code or partner.ref)
        if acc_price_code and default_code:
            sql = """SELECT "PRICE"
                   FROM acc_price
                   WHERE "ITEM_" = '%s'
                       AND "CODE" = '%s'
                   """ % (default_code, acc_price_code)
            self.cr.execute(sql)
            sql_res = self.cr.fetchone()
            res = sql_res and sql_res[0] and float(sql_res[0]) or 0.0
        return res

