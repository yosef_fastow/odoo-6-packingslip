from openerp.addons.sale_integration.lib.product.product import Product


class CommercehubProduct(Product):
    search_field = ['UPC', 'merchantSKU', 'vendorSku']

    def get_product_and_size(self, line, customer_id):
        product, size = False, False
        for field in self.search_field:
            if line.get(field, False):
                product, size = self.pool.get('product.product').search_product_by_id(
                    self.cr, self.uid, customer_id, line[field])
                if product:
                    return product, size
        return product, size
