from openerp.addons.sale_integration.lib.product.product import Product


class PeopleProduct(Product):

    search_field = ['merchantSKU', 'vendorSku']
    size_id = False

    def get_product_and_size(self, line, customer_id):
        product, size = False, False
        for field in self.search_field:
            if line.get(field, False):
                line['sku'] = line[field] if not line.get('sku', False) else line['sku']
                product, size = self.pool.get('product.product').search_product_by_id(
                    self.cr, self.uid, customer_id, line[field])

                if size and size.id:
                    current_product_size_id = size.id
                elif line.get('size', False):
                    try:
                        size_ids = self.pool.get('ring.size').search(self.cr, self.uid, [('name', '=', str(float(line['size'])))])
                    except Exception:
                        size_ids = []
                    if len(size_ids) > 0:
                        current_product_size_id = size_ids[0]
                    else:
                        current_product_size_id = False
                        line['size'] = False
                else:
                    current_product_size_id = False

                # Need to transfer the size
                self.size_id = current_product_size_id

                if product:
                    return product, size
        return product, size
