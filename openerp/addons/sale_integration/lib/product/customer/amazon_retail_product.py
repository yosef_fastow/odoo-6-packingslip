from openerp.addons.sale_integration.lib.product.product import Product


class AmazonRetailProduct(Product):

    def get_product_and_size(self, line, customer_id):
        product, size = self.pool.get('product.product').search_product_by_id(
            self.cr, self.uid, customer_id, line['sku'],
            ('default_code', 'customer_sku', 'customer_id_delmar'))
        if product:
            line['name'] = product and product.name or ''
        return product, size


