from openerp.addons.sale_integration.lib.product.product import Product


class VirtualCustomerProduct(Product):

    def calculate_price(self, parameters):
        customer = parameters.get('customer', None)
        if customer is None:
            raise Exception('For calculating the price you need set \'customer\'')

        line = parameters.get('line', None)
        if line is None:
            raise Exception('For calculating the price you need set \'line\'')

        customer_id = parameters.get('customer_id', None)
        if customer_id is None:
            raise Exception('For calculating the price you need set \'customer_id\'')

        product_id = parameters.get('product_id', None)
        if product_id is None:
            raise Exception('For calculating the price you need set \'product_id\'')

        size_id = parameters.get('size_id', None)
        if size_id is None:
            raise Exception('For calculating the price you need set \'size_id\'')

        note = ''
        price = False

        if not product_id:
            note = 'The price can not be set because the product is not defined \n'
            return price, note

        if customer['invoice_cost_from_acc_price']:
            acc_price = self.get_price_from_acc_price(customer, product_id)
            if acc_price:
                return acc_price, note

        customer_field_price = self.get_price_from_customer_field(customer_id, product_id, size_id)

        if not (customer_field_price and float(customer_field_price)):
            customer_field_price = self.get_price_from_customer_field(customer_id, product_id, False)
            customer_field_price = self.calculate_discounts(product_id, False, customer_id, customer_field_price)
        else:
            customer_field_price = self.calculate_discounts(product_id, size_id, customer_id, customer_field_price)

        if customer_field_price:
            return customer_field_price, note

        xml_price = self.get_price_from_xml(line, parameters.get('price_from_file', 0.0))
        if xml_price:
            return xml_price, note

        note = "Can't find product cost for  name %s \n" % (line['name'])
        return price, note
