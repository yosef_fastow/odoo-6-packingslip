from openerp.addons.sale_integration.lib.product.product import Product

class WalmartProduct(Product):

    search_field = ['upc', 'sku', 'customer_sku']
    _ignore_price_difference = True
    def get_product_and_size(self, line, customer_id):
        product, size = False, False
        for field in [fld for fld in self.search_field if line.get(fld)]:
            product, size = self.pool.get('product.product').search_product_by_id(
                self.cr, self.uid, customer_id, line[field],
                ('default_code', 'customer_sku', 'upc', 'customer_id_delmar'))
            if product:
                return product, size
        return product, size
