from openerp.addons.sale_integration.lib.product.product import Product

class OverstockProduct(Product):

    def get_product_and_size(self, line, customer_id):
        product, size = False, False
        id_delmar_os = self.search_id_delmar_by_OS_sku(line['sku'])

        mapping_list = [
            line['optionSku'],
            id_delmar_os or line['sku']
        ]

        for field in mapping_list:
            product, size = self.pool.get('product.product').search_product_by_id(
                self.cr, self.uid, customer_id, field, ('customer_sku', 'os_product_sku', 'customer_id_delmar', 'default_code'))
            if product:
                return product, size
        return product, size

    def search_id_delmar_by_OS_sku(self, os_sku):
        id_delmar = False

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_main_servers(self.cr, self.uid, single=True)
        if server_id:
            search_sql = """
                        SELECT TOP 1
                            ltrim(rtrim(DelmarProductID))
                        FROM
                            dbo.CustomerProductIDs
                        WHERE
                            CustomerID like 'OS'
                        and (CustomerProductID = '{0}' or CustomerProductID + 'C' = '{0}')
                        ORDER BY CustomerProductID """.format(os_sku)

            result = server_data.make_query(self.cr, self.uid, server_id, search_sql, select=True, commit=False)

            id_delmar = result and result[0] and result[0][0] or False

        return id_delmar