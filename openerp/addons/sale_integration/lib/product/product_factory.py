from openerp.addons.sale_integration.lib.product.customer.overstock_product import OverstockProduct
from openerp.addons.sale_integration.lib.product.customer.wayfair_product import WayfairProduct
from openerp.addons.sale_integration.lib.product.customer.commercehub_product import CommercehubProduct
from openerp.addons.sale_integration.lib.product.customer.walmart_product import WalmartProduct
from openerp.addons.sale_integration.lib.product.customer.amazon_magento_product import AmazonMagentoProduct
from openerp.addons.sale_integration.lib.product.customer.amazon_retail_product import AmazonRetailProduct
from openerp.addons.sale_integration.lib.product.customer.people_product import PeopleProduct
from openerp.addons.sale_integration.lib.product.customer.sterling_product import SterlingProduct
from openerp.addons.sale_integration.lib.product.customer.dennys_customers_product import DennysCustomersProduct
from openerp.addons.sale_integration.lib.product.customer.amazon_ledpax_product import LedPaxCustomersProduct
from openerp.addons.sale_integration.lib.product.customer.virtual_customer_product import VirtualCustomerProduct
from openerp.addons.sale_integration.lib.product.customer.zales_product import ZalesProduct
from openerp.addons.sale_integration.lib.product.product import Product


class ProductFactory:

    def factory(type, cr, uid, pool):

        if type == 'overstock':
            return OverstockProduct(cr, uid, pool)
        elif type == 'wayfair':
            return WayfairProduct(cr, uid, pool)
        elif type == 'commercehub':
            return CommercehubProduct(cr, uid, pool)
        elif type == 'walmart':
            return WalmartProduct(cr, uid, pool)
        elif type in ('amazon', 'magento'):
            return AmazonMagentoProduct(cr, uid, pool)
        elif type == 'amazon_retail':
            return AmazonRetailProduct(cr, uid, pool)
        elif type == 'people':
            return PeopleProduct(cr, uid, pool)
        elif type == 'sterling':
            return SterlingProduct(cr, uid, pool)
        elif type == 'amazon_ledpax':
            return LedPaxCustomersProduct(cr, uid, pool)
        elif type == 'dennys_customers':
            return DennysCustomersProduct(cr, uid, pool)
        elif type == 'virtual_customer':
            return VirtualCustomerProduct(cr, uid, pool)
        elif type == 'zales':
            return ZalesProduct(cr, uid, pool)
        return Product(cr, uid, pool)

    factory = staticmethod(factory)
