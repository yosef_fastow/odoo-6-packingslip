# -*- coding: utf-8 -*-

{
    'name': 'Sale Integration',
    'version': '1.0',
    'category': 'Tools',
    'complexity': "expert",
    'description': """

        Required python-gnupg, boto

        To Install:
            sudo apt-get install gnupg
            sudo pip install python-gnupg requests

    """,
    'author': '',
    'website': '',
    'depends': [
        'sale',
        'tagging',
        'pf_utils',
        'delmar_discounts',
    ],
    'init_xml': [
    ],
    'update_xml': [
        'security/ir.model.access.csv',
        'security/base_menu.xml',
        "data/sale_integration_data.xml",
        "data/res_partner_data.xml",
        "data/sale_integration_cron.xml",
        "data/system_params.xml",
        "res/view/res_log_view.xml",
        "res/view/res_partner_view.xml",
        "view/sale_integration_settings.xml",
        "view/sale_view.xml",
        "view/stock_move_view.xml",
        "view/stock_view.xml",
        "view/sale_order_import_view.xml",
        "view/delivery_order_export_view.xml",
        "view/stock_partner_return_view.xml",
        "view/delmar_sale_taxes_view.xml",
        "view/stock_inventory_management_view.xml",
        "view/stock_sale_order_import.xml",
        "view/http_settings_view.xml",
        "view/jet_taxonomy_view.xml",
        "view/flash_from_stock_view.xml",
        "view/unique_fields_for_orders_view.xml",
        "wizard/approve_done_wizard.xml",
    ],
    'demo_xml': [],
    'installable': True,
    "active": False,
    'auto_install': False,
    "application": True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
