# -*- coding: utf-8 -*-
from osv import fields, osv
import decimal_precision as dp
import netsvc
from tools.translate import _
from datetime import datetime
import re
from pf_utils.utils.decorators import prodonly
from pf_utils.utils.re_utils import str2float
import logging

logger = logging.getLogger('sale')

class sale_order(osv.osv):

    _inherit = "sale.order"

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = super(sale_order, self)._amount_all(cr, uid, ids, field_name, arg, context=context)
        for order in self.browse(cr, uid, ids, context=context):
            amount_tax = self.pool.get("delmar.sale.taxes").compute_sale_taxes(cr, uid, order.id, context=context)
            res[order.id]['amount_tax'] += amount_tax and amount_tax['amount_tax']
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
            logger.warn("AMOUNT_TOTAL from si/saleorder: %s" % res[order.id]['amount_total'])
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()

    _columns = {
        'original_order_number': fields.char('Original Order Id', size=128, required=False, ),
        'external_customer_order_id': fields.char('External Customer Order Id', size=128, required=True),
        'external_customer_related_order_id': fields.char('Customer Related Order ids', size=512),  # Sterling
        'carrier': fields.char('Carrier', size=128),
        'type_api': fields.char('type_api', size=128),
        'po_number': fields.char('PurchaseOrderNumber', size=128, select=True),
        'external_date_order': fields.date('External Date', readonly=True),
        'latest_ship_date_order': fields.date('Latest Ship Date', readonly=True),
        'earliest_ship_date_order': fields.date('Earlist Ship Date', readonly=True),
        'sale_integration_proccessed': fields.boolean('Sale integration Processed', readonly=True),
        'type_api_id': fields.many2one('sale.integration', 'Service provider'),
        'sale_integration_xml_id': fields.many2one('sale.integration.xml', 'XML'),
        'gift': fields.boolean('Gift'),
        'receipt_id': fields.char('ReceiptID', size=128),
        'packslip_message': fields.text('PackslipMessage'),
        'tax_type': fields.char('Tax type', size=128),
        'credit_type': fields.char('Credit type', size=128),
        'payment_method': fields.char('Credit type', size=128),
        'merchandise_cost': fields.char('Merchandise Cost', size=128),
        'sales_division': fields.char('Sales Division', size=128),
        'shipping': fields.char('Shipping Cost', size=128),
        'handling': fields.char('Handling Cost', size=128),
        'tax': fields.char('Tax', size=128),
        'credits': fields.char('Credits', size=128),
        'payment_type': fields.char('Payment Type', size=128),
        'gift_message': fields.text('Gift Message'),
        'cust_order_number': fields.char('Cust Order Number', size=128),
        'store_number': fields.char('Store Number', size=128),  # Walmart
        'tcnumber': fields.char('TCNUMBER', size=128),  # Walmart
        'asnnumber_prefix': fields.char('asnnumber_prefix', size=128),  # Walmart
        'merchantreference1': fields.char('merchantreference1', size=128),  # Sterling
        'merchantreference2': fields.char('merchantreference2', size=128),  # Sterling
        'merchantreference3': fields.char('merchantreference3', size=128),  # Sterling
        'priorityEDI': fields.char('priorityEDI', size=128),  # Sterling
        'requestedShipDate': fields.date('Ship Date'),  # Sterling
        'sterling_comments': fields.one2many('sale.order.sterling.comments', 'order_id', 'Sterling Comments'),  # Sterling
        'orderType': fields.char('OrderType', size=128),  # Sterling
        'sale_integration_cancel_send': fields.boolean('sale_integration_cancel_send'),  # Sterling
        'sub_total': fields.char('Sub Total', size=128),  # Kmart
        'total': fields.char('Total', size=128),  # Kmart
        'discount_available': fields.char('Discount Available', size=128),  # fmj
        'discount_type': fields.char('Discount Type', size=128),   # fmj
        'discount_date_indicator': fields.char('Discount Date Indicator', size=128),  # fmj
        'discount_percentage': fields.char('Discount Percentage', size=128),  # fmj
        'discount_due_days': fields.char('Discount Due Days', size=128),  # fmj
        'discount_total': fields.float('Total Discount'),  # ice
        'net_due_days': fields.char('Net Due Days', size=128),  # fmj
        'flash_force_id': fields.float('Flash Force ID', ),
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Sale Price'), string='Untaxed Amount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'product_uom_qty'], 10),
            },
            multi='sums', help="The amount without tax."
        ),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Sale Price'), string='Taxes',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'product_uom_qty'], 10),
            },
            multi='sums', help="The tax amount."
        ),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Sale Price'), string='Total',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount."
        ),
        'processQueue': fields.boolean('processQueue'),
        'ice_total_due': fields.char('ice_total_due', size=128),  # ICE
        'ice_tax_percentage': fields.char('ice_tax_percentage', size=128),  # ICE
        'shipping_fee': fields.char('shipping_fee', size=128),  # ICE
        'shipping_tax': fields.char('shipping_tax', size=128),  # ICE
        'fulfillment_channels': fields.char('Fulfillment Channels', size=128),  # AmazonFBA
        'order_comment': fields.text('Order Comment'), # Denny's customers
        'additional_fields': fields.one2many('sale.order.fields', 'order_id', 'Additional fields'),
        'flash': fields.boolean('Use on Flash Screen'),
        'bulk_transfer': fields.boolean('Use on Bulk Screen', ),
        'format': fields.selection(
            [
                ('regular', 'Regular'),
                ('edi', 'EDI'),
            ], 'Format'
        ),
        'send_functional_acknowledgement': fields.related(
            'type_api_id', 'send_functional_acknowledgement',
            string='Send Functional Acknowledgement',
            readonly=True,
            type="boolean",
            relation="sale.integration",
        ),
        'force_location_ids': fields.many2many(
            "stock.location", "sale_order_force_location", "order_id", "location_id",
            string="Locations",
            domain=[('usage', '=', 'internal'), ('complete_name', 'ilike', ' / Stock / ')],
        ),
        'fba_invoice': fields.char('fba_invoice', size=128),
    }

    _defaults = {
        'gift': False,
        'discount_total': 0.0,
        'flash': False,
        'bulk_transfer': False,
        'format': 'regular',
    }

    # def action_cancel(self, cr, uid, ids, context=None):
    #     res = super(sale_order, self).action_cancel(cr, uid, ids, context=context)
    #     self.pool.get("sale.integration").integrationApiRejectSaleOrder(cr, uid, ids)
    #     return res

    def _prepare_order_line_move(self, cr, uid, order, line, picking_id, date_planned, context=None):
        line_obj = super(sale_order, self)._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned, context=context)
        line_obj['external_customer_line_id'] = line.external_customer_line_id

        return line_obj

    # @prodonly(return_value=True)
    def order_processing(self, cr, uid, ids):
        res = True
        if(not isinstance(ids, list)):
            ids = [ids]
        for order in self.browse(cr, uid, ids):

            errors = []

            for line in order.order_line:
                if not line.product_id:
                    errors.append("""Can't find product for %s""" % (line.name))
                    continue

                if not line.product_uom_qty or line.product_uom_qty < 0 or not line.product_uos_qty or line.product_uos_qty < 0:
                    errors.append("""Qty for %s/%s should be a positive value""" % (line.product_id.default_code, line.size_id and line.size_id.name or '0'))
                if not order.flash and (not line.price_unit or line.price_unit < 0):
                    errors.append("""Can't find price for %s/%s""" % (line.product_id.default_code, line.size_id and line.size_id.name or '0'))

                '''
                Task DLMR-317 
                if line.customerCost:
                    try:
                        retail = str2float(line.customerCost)
                        if retail and retail < line.price_unit:
                            # Bad hardcode needed ASAP!!!1111
                            # email from <yana@delmarintl.ca> "FW: Retail Price too low [Incident: 161127-042621]"
                            if line.product_id.default_code in ('FC13M9-96HY', 'FC13M9-BXL4') and order.name[:3] == 'KOH':
                                continue
                            errors.append("""Customer cost can't be less then Price Unit %s/%s""" % (line.product_id.default_code, line.size_id and line.size_id.name or '0'))
                    except:
                        errors.append("""Customer cost has not valid format for %s/%s: %s""" % (line.product_id.default_code, line.size_id and line.size_id.name or '0', line.customerCost))
                '''

            if errors:
                note = order.note or ''
                if note:
                    note += '\n\n'
                self.write(cr, uid, order.id, {
                    'state': 'shipping_except',
                    'note': note + "\n".join(errors),
                })
                return False

            if order.type_api == 'amazon' and order.fulfillment_channels == 'AFN':
                remote_status = self.pool.get("sale.order").fba_do_remote_done(cr, uid, [order.id])
                if remote_status:
                    self.pool.get("sale.order").write(cr, uid, order.id, {"state": "done"})
                return False

            partner_is_fbc = (
                order and
                order.partner_id and
                order.partner_id.is_fbc or
                False
            )

            if partner_is_fbc:
                server_data = self.pool.get('fetchdb.server')
                sale_server_id = server_data.get_sale_servers(cr, uid, single=True)
                for line in order.order_line:
                    self.fbc_fill_OS_Sales(cr, uid, line, sale_server_id)
                    self.fbc_fill_OS_Sales_BU(cr, uid, line, sale_server_id)
                    self.write(cr, uid, order.id, {"state": "done"})
                return False

            if(not order.sale_integration_proccessed and order.partner_id.send_acknowledgement):
                res = self.pool.get("sale.integration").integrationApiAcceptSaleOrder(cr, uid, order.id)

        return res

    def action_reject(self, cr, uid, ids, context=None):
        self.pool.get("sale.integration").integrationApiRejectSaleOrder(cr, uid, ids)

    def action_resend_acknowledgement(self, cr, uid, ids, context):
        res = True
        context = {'button_call': True}
        if not ids:
            return True
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for order_id in ids:
            res = self.pool.get('sale.integration').integrationApiAcceptSaleOrder(cr, uid, order_id, context)
        return res

    def action_resend_functional_acknowledgement(self, cr, uid, ids, context=None):
        si = self.pool.get('sale.integration')
        if context is None:
            context = {}
        if(not isinstance(ids, list)):
            ids = [ids]
        for order in self.browse(cr, uid, ids):
            si.integrationApiResendFunctionalAcknoledgment(cr, uid, order, context=context)
        return True

    def action_reconfirm_order_write(self, cr, uid, ids=None, context=None):
        res = True
        if not ids:
            return False

        sale_obj = self.browse(cr, uid, ids[0], context=context)

        if sale_obj and sale_obj.picking_ids:
            prohibited_states = ['shipped', 'done', 'back', 'returned', 'cancel', 'final_cancel']

            picking_ids_filtered = self.pool.get('stock.picking').search(cr, uid, [
                ('sale_id', '=', sale_obj.id),
                ('state', 'not in', prohibited_states)
            ])
            pickings = self.pool.get('stock.picking').browse(cr, uid, picking_ids_filtered) if picking_ids_filtered else []

            invoice_no = sale_obj.po_number
            customer_ref = sale_obj.partner_id.ref

            server_data = self.pool.get('fetchdb.server')
            server_id = server_data.get_sale_servers(cr, uid, single=True)

            check_move_ids = []
            move_obj = self.pool.get('stock.move')
            wf_service = netsvc.LocalService("workflow")
            index_list = (
                'itemid',
                'TransactionCode',
                'qty',
                'warehouse',
                'itemdescription',
                'bin',
                'stockid',
                'TransactionType',
                'id_delmar',
                'sign',
                'adjusted_qty',
                'size',
                'invredid',
                'CustomerID',
                'ProductCode',
                'InvoiceNo',
                'reserved',
                'trans_tag',
                'unit_price',
                'acc_invoice',
                'export_ref_ca',
            )
            for picking in pickings:
                for move in picking.move_lines:
                    transactions_table = False
                    sale_line = move.sale_line_id
                    new_qty = sale_line.product_uom_qty
                    old_qty = move.product_qty
                    if old_qty != new_qty:
                        if picking.state in ('picking', 'outcode_except'):
                            wf_service.trg_validate(uid, 'stock.picking', picking.id, 'action_reship', cr)
                        check_move_ids.append(move.id)
                        product = move.product_id
                        location = move.location_id
                        location_id = location and location.id or False
                        delmarid = product and product.default_code or False
                        size = move.size_id
                        size_postfix = move_obj.get_size_postfix(cr, uid, size)
                        itemid = delmarid and (delmarid + '/' + size_postfix) or False
                        warehouse = move_obj.get_warehouse_by_move_location(cr, uid, location_id)
                        stockid = location and location.name or False
                        reserved_flag = move_obj.get_reserved_flag(cr, uid, itemid, warehouse, stockid, server_id)
                        description = sale_line.name or product and product.name

                        params = {
                            'itemid': itemid or 'null',
                            'TransactionCode': 'SLE',
                            'qty': '', # filling below
                            'warehouse': warehouse or 'null',
                            'itemdescription': description or 'null',
                            'bin': move.bin or 'null',
                            'stockid': stockid or 'null',
                            'TransactionType': 'TRS',
                            'id_delmar': delmarid,
                            'sign': '', # filling below
                            'adjusted_qty': '0', # filling below
                            'size': size_postfix,
                            'invredid': move.invredid or 'null',
                            'CustomerID': customer_ref or 'null',
                            'ProductCode': delmarid,
                            'InvoiceNo': invoice_no or 'null',
                            'reserved': reserved_flag or 0,
                            'trans_tag': sale_line.id or None,
                            'unit_price': sale_line.price_unit or 0,
                        }

                        vals = {
                            'product_qty': new_qty,
                            'product_uos_qty': new_qty
                        }
                        if move.state in ('assigned'):
                            if old_qty > new_qty:
                                if new_qty > 0:
                                    transactions_table = True
                                    params['sign'] = 1
                                    params['qty'] = old_qty - new_qty
                                else:
                                    move_obj.uncheck_assign(cr, uid, [move.id], context={})
                            else:
                                #product_id, size_id, location_ids, bin
                                size_id = size and size.id
                                loc_qty = move_obj.get_product_qty_in_locations(cr, uid, product.id, size_id, location_id, move.bin)
                                if loc_qty >= new_qty:
                                    transactions_table = True
                                    params['sign'] = -1
                                    params['qty'] = new_qty - old_qty
                                else:
                                    move_obj.uncheck_assign(cr, uid, [move.id], context={})
                        move_obj.write(cr, uid, move.id, vals)

                        params['TransactionCode'] = self.get_sale_transaction_code(cr, uid, sale_obj.id, sign=params['sign'])

                        if transactions_table:
                            params['adjusted_qty'] = (params['qty'] * params['sign']) or '0'
                            insert_param = [params[index] for index in index_list]
                            tr_context = {
                                'erp_sm_id': move.id
                            }
                            move_obj.write_into_transactions_table(cr, uid, server_id, tuple(insert_param), context=tr_context)

                if check_move_ids:
                    wf_service.trg_validate(uid, 'stock.picking', picking.id, 'button_uncheck_flash', cr)
                    self.pool.get('stock.picking').action_assign(cr, uid, [picking.id], move_ids=check_move_ids)
        return res

    def action_reconfirm_order_send(self, cr, uid, ids=None, context=None):
        res = True
        if not ids:
            return False
        res = self.pool.get('sale.integration').integrationApiReconfirmSaleOrder(cr, uid, ids, context=context)
        return res

    def action_confirm_flash_order(self, cr, uid, ids=None, context=None):
        res = True
        if not ids:
            return False
        for order in self.pool.get('sale.order').read(cr, uid, ids, ['name', 'sale_integration_proccessed'], context=context):
            if order['sale_integration_proccessed']:
                res = self.pool.get('sale.integration').integrationApiReconfirmSaleOrder(cr, uid, order['id'], context=context)
            else:
                res = self.pool.get('sale.integration').integrationApiAcceptSaleOrder(cr, uid, order['id'], context)
        return res

    def back_to_draft(self, cr, uid, ids, context=None):
        _order_line = self.pool.get("sale.order.line")
        for order in self.browse(cr, uid, ids, context=context):
            if not order.picking_ids:
                self.write(cr, uid, ids, {'state': 'draft'})
                if order.is_set:
                    so_line_ids = [x.id for x in order.order_line if x.is_set]
                    so_real_line_ids = [x.id for x in order.set_parent_order_line]
                    if (so_real_line_ids):
                        delete_lines = _order_line.unlink(cr, uid, so_line_ids,{})
                        if (delete_lines):
                            _order_line.write(cr, uid, so_real_line_ids,
                                          {'order_id': order.id, 'set_parent_order_id': ''})
                            self.pool.get("sale.order").write(cr, uid, order.id, {"is_set": False})
                        else:
                            raise osv.except_osv(('Invalid action !'),
                                                 ('can\t delete lines'))
            else:
                raise osv.except_osv(('Invalid action !'), ('You cannot back to draft a sale order line that has delivery orders!'))

        return True

    def _confirm_orders(self, cr, uid, limit=10, partner_ref=False, context=None):
        wf_service = netsvc.LocalService("workflow")
        domain = [('state', '=', 'draft')]
        if partner_ref:
            domain.append(('partner_id.ref', '=', partner_ref))
        order_ids = self.search(cr, uid, domain, limit=limit)
        for order_id in order_ids:
            wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)

        return True

    def xml_rpc_confirm_orders(self, cr, uid, ids, context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        wf_service = netsvc.LocalService("workflow")
        for order in self.browse(cr, uid, ids):
            wf_service.trg_validate(uid, 'sale.order', order.id, 'order_confirm', cr)
        return True

    def onchange_partner_id(self, cr, uid, ids, part):
        type_api_id = False
        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part) or {}

        if part:
            type_api_id = self.pool.get('res.partner').browse(cr, uid, part).type_api_id

        value = res.get('value', {})
        value.update({
            'type_api_id': type_api_id
            })

        res['value'] = value

        return res

    def _get_picking_ids(self, cr, uid, ids, states=['done'], without_ext_cust_id=False, context=None):
        if context is None:
            context = {}
        result = {}
        result['picking_ids'] = []
        result['picking_ids_without_ext_cust_id'] = {}
        for sale_order in self.browse(cr, uid, ids, context=context):
            for picking in sale_order.picking_ids:
                if picking.state in states:
                    result['picking_ids'].append(picking.id)
                    if(without_ext_cust_id):
                        ext_cust_id = picking.real_partner_id.external_customer_id or False
                        if(not ext_cust_id):
                            ext_cust_id = unicode(picking.real_partner_id.name or 'undefined').lower()
                        result['picking_ids_without_ext_cust_id'][picking.id] = ext_cust_id
        return result

    def get_invoices_for_delivery_orders(self, cr, uid, ids, context=None):
        picking_report_ids = []
        if context is None:
            context = {}
        res = {'datas': {'ids': []}}
        def_so_id = context.get('def_so_id', False)
        active_ids = context.get('active_ids', False)
        if def_so_id:
            ids = [def_so_id]
        elif active_ids:
            ids = active_ids
        picking_report_ids = self._get_picking_ids(cr, uid, ids, states=['done','returned']).get('picking_ids', [])
        if not picking_report_ids:
            raise osv.except_osv(
                _('Warning !'),
                """Please select those sale orders,
                which delivery orders have the status 'Done' or 'Returned'."""
            )

        res = True
        if picking_report_ids:

            res = {
                'type': 'ir.actions.report.xml',
                'report_name': 'stock.picking.invoice',
                'datas': {
                    'model': 'stock.picking',
                    'report_type': 'pdf',
                    'ids': picking_report_ids,
                    'id': picking_report_ids[0],
                },
                'context': {
                    'active_ids': picking_report_ids,
                }
            }

        return res

    def send_invoices_for_delivery_orders_to_mail(self, cr, uid, ids, context=None):
        def _sort_by_ext_cust_id(_dict=None):
            result = {}
            if(_dict is None):
                _dict = {}
            for el in _dict:
                if(not result.get(_dict[el], False)):
                    result[_dict[el]] = []
                result[_dict[el]].append(el)
            return result
        res = {'datas': {'ids': []}}
        if context is None:
            context = {}
        active_ids = context.get('active_ids', False)
        if(active_ids):
            ids = active_ids
        picking_ids_raw = self._get_picking_ids(cr, uid, ids, states=['done'], without_ext_cust_id=True)
        picking_ids_without_ext_cust_id = picking_ids_raw.get('picking_ids_without_ext_cust_id', {})

        sort_pick_ids = _sort_by_ext_cust_id(picking_ids_without_ext_cust_id)
        si_obj = self.pool.get('sale.integration')
        picking_ids_without_si_raw = {}
        for ext_cust_id in sort_pick_ids:
            si_id = si_obj.search_type_api_id_by_customer(cr, uid, ext_cust_id, only_main=True)
            if(si_id is None):
                si_id = si_obj.search_type_api_id_by_customer(cr, uid, ext_cust_id, only_main=False)
            if(si_id is None):
                if(not picking_ids_without_si_raw.get('default', False)):
                    picking_ids_without_si_raw['default'] = []
                picking_ids_without_si_raw['default'].extend(sort_pick_ids[ext_cust_id])
            else:
                picking_ids_without_si_raw[si_id] = sort_pick_ids[ext_cust_id]

        for _id in picking_ids_without_si_raw:
            si_name = si_obj.browse(cr, uid, _id).name
            (settings, api) = si_obj.getApi(cr, uid, si_name)
            for delivery_id in picking_ids_without_si_raw[_id]:
                _emails_to = False
                if(settings.settings):
                    for item in settings.settings:
                        if(item.label == 'email list(invoice)'):
                            if(item.value):
                                _emails_to = item.value
                                break
                if(_emails_to):
                    _settings_invoice = {
                        'cr': cr,
                        'uid': uid,
                        'delivery_ids': delivery_id,
                        'emails_to': _emails_to,
                        'customer_name': self.pool.get("stock.picking").browse(cr, uid, delivery_id).sale_id.partner_id.name,
                        'report_service': 'report.stock.picking.invoice',
                    }
                    api.generate_invoice_mail_message(_settings_invoice)
                if(hasattr(api, '_mail_messages')):
                    if(api._mail_messages is not None):
                        si_obj.integrationApiSendEmail(cr, uid, settings, api._mail_messages)

        return res

    # TOREFACTOR: remove code duplication with:
    # stock_picking.fill_OS_Sales_tables, sale_order.fba_fill_OS_Sales_tables and sale_order.fbc_fill_OS_Sales_BU
    def fbc_fill_OS_Sales(self, cr, uid, line, sale_server_id):
        result = False
        server_data = self.pool.get('fetchdb.server')
        so_partner = line and line.order_id and line.order_id.partner_id and line.order_id.partner_id
        delmar_id = line.product_id and line.product_id.default_code
        prod_size = line.size_id and line.size_id.name
        four_digit_prod_size = prod_size and float(prod_size) and re.sub('\.', '', "%05.2f" % float(prod_size)) or '0000'
        price_unit = line.price_unit and float(line.price_unit) or 0

        table_name = 'OS_Sales'

        shp_date = datetime.utcnow()

        params = [
            line.product_id and line.product_id.default_code or None,    # ID_Delmar
            four_digit_prod_size or 0,    # Size,
            shp_date or None,                    # Date
            line.product_uom_qty and int(line.product_uom_qty) or None,  # Qty
            float(float(price_unit) * float(line.product_uom_qty)) or 0,  # Cost
            line and line.order_id and line.order_id.po_number or None,  # # order_ID
            None,   # track_no
            line and line.external_customer_line_id or None,            # Line_ID
            so_partner and so_partner.ref or None,  # customer_ID
            delmar_id and prod_size and (delmar_id + "/" + prod_size) or delmar_id or None,   # customer_SKU
            delmar_id or None,  # customer_CODE
            delmar_id and prod_size and (delmar_id + "/" + prod_size) or delmar_id or None,  # customer_ITEM
            0,                                      # STATUS
            None,                                 # Year
            None,                                 # Month
            delmar_id and four_digit_prod_size and (delmar_id + "/" + four_digit_prod_size) or delmar_id or None,  # ItemID
            line and line.order_id and line.order_id.po_number or None,  # InvoiceNo
            None,               # trnid
            0,                                      # ReturnQuantity
        ]

        sql = """
            INSERT INTO dbo.""" + table_name + """ (
                ID_Delmar,
                Size,
                Date,
                Qty,
                Cost,
                order_ID,
                track_no,
                Line_ID,
                customer_ID,
                customer_SKU,
                customer_CODE,
                customer_ITEM,
                status,
                Year,
                Month,
                ItemID,
                InvoiceNo,
                trnid,
                ReturnQuantity
            ) VALUES (
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            )
                        """

        # TOREFACTOR: need to use server_data.insert_record
        result = server_data.make_query(cr, uid, sale_server_id, sql, params=params, select=False, commit=True)
        return bool(result)

    # TOREFACTOR: remove code duplication with:
    # stock_picking.fill_OS_Sales_tables, sale_order.fba_fill_OS_Sales_tables and sale_order.fbc_fill_OS_Sales
    def fbc_fill_OS_Sales_BU(self, cr, uid, line, sale_server_id):
        result = False
        server_data = self.pool.get('fetchdb.server')
        so_partner = line and line.order_id and line.order_id.partner_id and line.order_id.partner_id
        delmar_id = line.product_id and line.product_id.default_code
        prod_size = line.size_id and line.size_id.name
        four_digit_prod_size = prod_size and float(prod_size) and re.sub('\.', '', "%05.2f" % float(prod_size)) or '0000'
        price_unit = line.price_unit and float(line.price_unit) or 0

        table_name = 'OS_Sales_BU'

        shp_date = datetime.utcnow()

        year = shp_date.year or None
        month = shp_date.month or None
        quarter = month and (month + 2) / 3 or None

        params = [
            line.product_id and line.product_id.default_code or None,    # ID_Delmar
            four_digit_prod_size or 0,    # Size,
            shp_date or None,                    # Date
            line.product_uom_qty and int(line.product_uom_qty) or None,  # Qty
            float(float(price_unit) * float(line.product_uom_qty)) or 0,  # Cost
            line and line.order_id and line.order_id.po_number or None,  # pick.sale_id.name, # order_ID
            None,   # track_no
            line and line.external_customer_line_id or None,            # Line_ID
            so_partner and so_partner.ref or None,  # customer_ID
            delmar_id and prod_size and (delmar_id + "/" + prod_size) or delmar_id or None,   # customer_SKU
            delmar_id or None,  # customer_CODE
            delmar_id and prod_size and (delmar_id + "/" + prod_size) or delmar_id or None,  # customer_ITEM
            # 0,                                      # STATUS
            year,                                 # Year
            month,                                 # Month
            quarter,                                 # Quarter
            delmar_id and four_digit_prod_size and (delmar_id + "/" + four_digit_prod_size) or delmar_id or None,  # ItemID
            # invoice_no,                             # InvoiceNo
            None,               # trnid
            0,                                      # ReturnQuantity
        ]

        sql = """
            INSERT INTO dbo.""" + table_name + """ (
                ID_Delmar,
                Size,
                Date,
                Qty,
                Cost,
                order_ID,
                track_no,
                Line_ID,
                Customer_ID,
                customer_SKU,
                customer_CODE,
                customer_ITEM,
                Year,
                Month,
                Quarter,
                ItemID,
                trnid,
                ReturnQuantity
            ) VALUES (
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            )
        """

        # TOREFACTOR: need to use server_data.insert_record
        result = server_data.make_query(cr, uid, sale_server_id, sql, params=params, select=False, commit=True)
        return bool(result)

    def update_price_from_gta(self, cr, uid, order_id, context=None):
        sql = """
            update sale_order_line ol
            set price_unit = a.price
            from (
                select ol.id, coalesce(pt.standard_price, 0.0) as price
                from sale_order_line ol
                    left join product_product pp on ol.product_id = pp.id
                    left join product_template pt on pp.product_tmpl_id = pt.id
                where ol.order_id = {order_id}
            ) a
            where ol.id = a.id
        """.format(order_id=order_id)
        cr.execute(sql)
        return True

    def update_price_from_cf(self, cr, uid, order_id, field_label, context=None):
        sql = """
            update sale_order_line ol
            set price_unit = a.price
            from (
                select ol.id, regexp_replace(mf.value, '[^\.0-9]+', '', 'g')::float as price
                from sale_order so
                    left join product_multi_customer_fields_name fn
                        on fn.customer_id = so.partner_id
                        and fn.label = %(label)s
                    left join sale_order_line ol on so.id = ol.order_id
                    left join product_multi_customer_fields mf
                        on mf.product_id = ol.product_id
                        and mf.field_name_id = fn.id
                        and coalesce(case when fn.expand_ring_sizes is true then ol.size_id else null end, 0) = coalesce(mf.size_id, 0)
                where so.id = %(order_id)s
                    and mf.id is not null
                    and regexp_replace(coalesce(mf.value, ''), '[^\.0-9]+', '', 'g') != ''
            ) a
            where ol.id = a.id
        """
        cr.execute(sql, {
            'label': field_label,
            'order_id': order_id
        })
        return True

    def update_last_confirm_qty(self, cr, uid, ids, context=None):
        if ids:
            if isinstance(ids, (int, long)):
                ids = [ids]

            cr.execute("""
                update sale_order_line set product_last_confirm_qty = product_uom_qty where order_id in %(order_ids)s
                """, ({'order_ids': tuple(ids,)})
            )

sale_order()


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"

    def _get_left_to_receive(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if ids:
            cr.execute("""
                SELECT
                    ol.id,
                    (ol.product_uos_qty - sum(sm.product_qty)) as left_to_receive
                FROM sale_order_line ol
                    LEFT JOIN stock_move sm on ol.id = sm.sale_line_id
                    LEFT JOIN stock_picking sp on sp.id = sm.picking_id and sp.state in ('returned', 'final_cancel')
                WHERE ol.id in %s
                GROUP BY ol.id
                """, (tuple(ids), ))
            res = dict(cr.fetchall() or [])
        return res

    def _get_default_sku_product(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        sale_lines = self.browse(cr, uid, ids)
        for line in sale_lines:
            res[line.id] = line and line.product_id and line.product_id.default_code or False
        return res

    def _auto_init(self, cursor, context=None):
        res = super(sale_order_line, self)._auto_init(cursor, context=context)
        cursor.execute('SELECT indexname \
                FROM pg_indexes \
                WHERE indexname = \'sale_order_line_set_parent_order_id_index\'')
        sale_order_line_set_parent_order_id_index = cursor.fetchone()
        if not sale_order_line_set_parent_order_id_index:
            cursor.execute('CREATE INDEX sale_order_line_set_parent_order_id_index \
                    ON sale_order_line (set_parent_order_id)')

        cursor.execute('SELECT indexname \
                FROM pg_indexes \
                WHERE indexname = \'sale_order_line_set_product_id_index\'')
        sale_order_line_set_product_id_index = cursor.fetchone()
        if not sale_order_line_set_product_id_index:
            cursor.execute('CREATE INDEX sale_order_line_set_product_id_index \
                    ON sale_order_line (set_product_id)')

        return res

    _columns = {
        'external_customer_line_id': fields.char('External Customer Line Id', size=128, required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'size': fields.float('Size', size=128),
        'merchantLineNumber': fields.char('Merchant Line Number', size=128),
        'merchantSKU': fields.char('Merchant SKU', size=128),
        'optionSku': fields.char('Option SKU', size=128),
        'type_api': fields.char('type_api', size=128),
        'shipCost': fields.char('Shipping Cost', size=128),
        'customerCost': fields.char('Customer Cost', size=128),
        'listCost': fields.char('List Cost', size=128),
        'syncPrice': fields.boolean('Sync price from OverStock'),
        'vendorSku': fields.char('VendorSku', size=128),
        'manufacturer_sku': fields.char('ManufacturerSku', size=128),
        'gift_message': fields.text('giftMessage'),
        'tax_type': fields.char('Tax type', size=128),
        'tax': fields.char('Tax', size=128),
        'credit_amount': fields.char('Credit Amount', size=128),
        'full_retail': fields.char('FullRetail', size=128),
        'line_note1': fields.char('lineNote1', size=128),
        'sec_customer': fields.char('Customer', size=256,),
        'gift_wrap_indicator': fields.char('Gift Wrap Indicator', size=128),
        'long_description': fields.char('Long Description', size=128),
        'expected_ship_date': fields.char('Expected Ship Date', size=128),
        'gift_registry': fields.char('Gift Registry', size=128),
        'prod_color': fields.char('Product Color', size=128),
        'lineRef1': fields.char('LineRef1', size=128),  # Sterling
        'lineRef2': fields.char('LineRef2', size=128),  # Sterling
        'lineRef3': fields.char('LineRef3', size=128),  # Sterling
        'lineRef4': fields.char('LineRef4', size=128),  # Sterling
        'linePrice': fields.char('LinePrice', size=128),  # Sterling
        'sterling_comments': fields.one2many('sale.order.line.sterling.comments', 'line_id', 'Sterling Comments'),  # Sterling
        'balance_due': fields.char('LineRef4', size=128),  # FMJ
        'bond': fields.char('BOND', size=128),  # FMJ
        'ice_sub_total': fields.char('ice_sub_total', size=128),  # ICE
        'ice_row_total': fields.char('ice_row_total', size=128),  # ICE
        'ice_tax_amount': fields.char('ice_tax_amount', size=128),  # ICE
        'ice_discount': fields.char('ice_discount', size=128),  # ICE
        'additional_fields': fields.one2many('sale.order.line.fields', 'line_id', 'Additional Fields'),
        'default_sku': fields.function(_get_default_sku_product, type="char", string="Sku", size=256, store=False),
        'user_notes': fields.char('User Notes', size=256),
        'product_last_confirm_qty': fields.float('Last Confirmed Qty', size=256),
        'price_unit': fields.float(
            'Unit Price',
            required=True,
            digits_compute=dp.get_precision('Sale Price'),
            readonly=True,
            states={
                'draft': [('readonly', False)],
                'confirmed': [('readonly', False)],
            }
        ),
        'cf_price_unit': fields.float(
            'Sale Time Unit Price',
            required=False,
            digits_compute=dp.get_precision('Sale Price'),
            readonly=True,
        ),
        'left_to_receive': fields.function(
            _get_left_to_receive,
            string="Delmar ids",
            type='int',
            method=True,
        ),
    }

sale_order_line()


class sale_order_sterling_comments(osv.osv):
    _name = 'sale.order.sterling.comments'

    def _file_name(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for comment in self.browse(cr, uid, ids, context=context):
            res[comment.id] = comment.order_id.name + '.zpl'
        return res

    _columns = {
        'order_id': fields.many2one('sale.order', 'Sale Order'),
        'type': fields.char('Type', size=10),
        'text': fields.text('Text'),
        'bin_data': fields.binary('File'),
        'file_name': fields.function(_file_name, string="Shelves", type='text', method=True)

    }

sale_order_sterling_comments()


class sale_order_line_sterling_comments(osv.osv):
    _name = 'sale.order.line.sterling.comments'

    _columns = {
        'line_id': fields.many2one('sale.order.line', 'Sale Order Line'),
        'type': fields.char('Type', size=10),
        'text': fields.text('Text')

    }

sale_order_line_sterling_comments()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
