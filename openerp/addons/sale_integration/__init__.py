# -*- coding: utf-8 -*-

import res
import settings
import sale_integration_xml
import sale
import product
import delivery_order_export
import sale_order_import
import stock_picking
import sale_order
import stock_partner_return
import delmar_sale_taxes
import stock_inventory_management
import sale_fields
import stock_sale_order_import
import jet_taxonomy
import stock_location
import flash_from_stock
import unique_fields_for_orders
import wizard

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
