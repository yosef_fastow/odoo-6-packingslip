# -*- coding: utf-8 -*-
import requests
import datetime
from osv import fields, osv
from lxml import etree
from api.ch_checker import CommerceHubRemoteAPIClient
from json import loads as json_loads
from mako.template import Template


class stock_picking_params(osv.osv):
    _name = 'stock.picking.params'
    _columns = {
        'name': fields.char('Name', size=256, required=True, ),
        'value': fields.text('Value', ),
        'picking_id': fields.many2one('stock.picking', 'Order', required=True, ondelete='cascade'),
    }

    _sql_constraints = [
        ('picking_params_uniq', 'unique (picking_id, name)', 'The combination of order and params must be unique !'),
    ]

    def create(self, cr, uid, vals, context=None):
        if vals and 'name' in vals:
            vals['name'] = vals['name'].strip().lower()

        return super(stock_picking_params, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if vals and 'name' in vals:
            vals['name'] = vals['name'].strip().lower()

        return super(stock_picking_params, self).write(cr, uid, ids, vals, context=context)

stock_picking_params()


class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def _check_needed_confirmation(self, cr, uid, ids, name, arg, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        sql = '''SELECT
                  sp.id AS sp_id,
                  CASE
                    WHEN 1=1
                      AND sp.state IN ('shipped', 'done','returned')
                      AND si.id IS NOT NULL
                      AND so.sale_integration_xml_id IS NOT NULL
                      AND (
                        si2."name" != 'Import Orders'
                        OR si."confirm_all_orders" = true
                        OR (
                            si.type_api = 'generic_ftp_inv'
                            AND mfs.id IS NOT NULL
                        )
                      )
                        THEN TRUE
                  ELSE FALSE END AS needed_confirmation
                FROM stock_picking sp
                LEFT JOIN sale_order so ON sp.sale_id = so.id
                LEFT JOIN res_partner rs ON sp.real_partner_id = rs.id
                LEFT JOIN sale_integration_customer sic ON sic.customer_id = rs.id
                LEFT JOIN sale_integration si ON sic.integration_id = si.id
                LEFT JOIN sale_integration_xml six ON so.sale_integration_xml_id = six.id
                LEFT JOIN sale_integration si2 ON six.type_api_id = si2.id
                LEFT JOIN sale_integration_ftp_setting sifs ON sifs.integration_id = si.id AND sifs.ftp_setting_id IN (SELECT id FROM multi_ftp_settings WHERE action = 'confirm_shipment')
                LEFT JOIN multi_ftp_settings mfs ON sifs.ftp_setting_id = mfs.id
                WHERE 1=1
                  AND si.type_api NOT IN ('no_api', 'sterling')
                  AND si.type_api IS NOT NULL
                  AND si.si_type != 'virtual'
                  AND sp.id IN ({sp_ids})
        '''.format(sp_ids=','.join([unicode(x) for x in ids]))
        cr.execute(sql)
        return {res['sp_id']: res['needed_confirmation'] for res in cr.dictfetchall()}

    _columns = {
        'origin_carrier': fields.related('sale_id', 'carrier', type='char', size=256, string='Original Carrier'),
        'origin_external_customer_order_id': fields.related('sale_id', 'external_customer_order_id', type='char', size=256, string='Original External Customer Order Id'),
        'origin_po_number': fields.related('sale_id', 'po_number', type='char', size=256, string='Original PurchaseOrderNumber'),
        'origin_ship2store': fields.related('sale_id', 'ship2store', type='boolean', string='Original Ship2store'),
        'sale_integration_ship_confirmed': fields.boolean(
            'Sale integration Ship Confirmed',
            help='Is order ship confirmed or not yet',
            readonly=True
            ),
        'sale_integration_invoice_send': fields.boolean(
            'Sale integration Invoice Send',
            help='Is order invoiced',
            readonly=True
            ),
        'order_params': fields.one2many('stock.picking.params', 'picking_id', 'Params', help="Order params in JSON format"),
        'return_reason': fields.text('Return reason'),
        'asn_number': fields.char('ASN Number', size=128),
        'needed_confirmation': fields.function(
            _check_needed_confirmation,
            string='needed_confirmation',
            type='boolean', method=True, ),
        'ch_info': fields.text(string='CH INFO', ),
        'manual_done_approved': fields.boolean("Manual Done approved", ),
    }

    def get_order_params(self, cr, uid, ids, keys=None, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = {x: {} for x in ids or []}

        if ids:
            key_where = ''
            if keys:
                key_where = "and name in ('%s')" % ("','".join([x.strip().lower() for x in keys]))

            cr.execute("""
                SELECT picking_id, name, value
                FROM stock_picking_params
                WHERE picking_id in %s {key_where}
            """.format(key_where=key_where), (tuple(ids), ))
            params = cr.fetchall() or []
            for x in params:
                res[x[0]][x[1]] = x[2]
        return res

    def set_order_params(self, cr, uid, ids, vals=None, force=False, context=None):
        if ids and vals:
            if force:
                self.write(cr, uid, ids, {'order_params': [(5)]})
                for pick_id in ids:
                    self.write(
                        cr, uid, pick_id,
                        {
                            'order_params': [
                                (0, 0, {'name': x.strip().lower(), 'value': y}) for x, y in vals.iteritems()
                            ]
                        }
                    )
            else:
                params_obj = self.pool.get('stock.picking.params')
                for picking_id in ids:
                    pick_data = []
                    for key, value in vals.iteritems():
                        key = key.strip().lower()
                        params_ids = params_obj.search(cr, uid, [('picking_id', '=', picking_id), ('name', '=', key)])
                        if params_ids:
                            for p_id in params_ids:
                                pick_data.append((1, p_id, {'value': value}))
                        else:
                            pick_data.append((0, 0, {'name': key, 'value': value}))
                    self.write(cr, uid, picking_id, {'order_params': pick_data})
        return True

    def ship_confirm(self, cr, uid, ids, reconfirm=False):
        sale_integration_obj = self.pool.get('sale.integration')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if reconfirm:
            for order in self.browse(cr, uid, ids):
                if order.needed_confirmation:
                    sale_integration_obj.integrationApiConfirmShipment(cr, uid, order.id)
                    order.add_message_to_note('Confirm Shipment Re-Sent.')
                else:
                    order.add_message_to_note('No Needed Confirm Shipment.')
            return True

        if len(ids) == 1:
            result = None
            order = self.browse(cr, uid, ids[0])
            sale_order = order.sale_id
            if sale_order and sale_order.flash and not (order.manual_done_approved or sale_order.shp_date or sale_order.state == 'done'):
                order.add_message_to_note('Flash order cannot be done now.')
                return False
            # if not order.needed_confirmation:
            #    order.add_message_to_note('No Needed Confirm Shipment.')
            #    self.pool.get('sale.integration')._switch_set_lines(cr, uid, order, 'to_done')
            #    sale_integration_obj.integrationApiSendInvoice(cr, uid, order.id)
            # elif not order.sale_integration_ship_confirmed:
            sale_integration_obj.integrationApiConfirmShipment(cr, uid, order.id)
            if order.sale_integration_ship_confirmed:
                order.add_message_to_note('Confirm Shipment Sent.')
            #else:
            #    order.add_message_to_note('Confirm Shipment already went.')
            updated_order = self.read(cr, uid, order.id, ['needed_confirmation', 'sale_integration_ship_confirmed'])
            if updated_order['sale_integration_ship_confirmed'] or not updated_order['needed_confirmation']:
                result = True
                try:
                    res_remote_done = self.do_remote_done(cr, uid, order.id, context=None)
                    if not res_remote_done:
                        order.add_message_to_note('Action "do_remote_done" return False.')
                        result = False
                except Exception:
                    order.add_message_to_note('Action "do_remote_done" failed.')
                    result = False
                    #roll back switch
                    if order.is_set and len(sale_order.set_parent_order_line) > 0:
                        self.pool.get('sale.integration')._switch_set_lines(cr, uid, order, 'to_rollback')

            if (result is None) and order.is_set and len(sale_order.set_parent_order_line) > 0:
                self.pool.get('sale.integration')._switch_set_lines(cr, uid, order, 'to_rollback')

            return result
        else:
            return False

    def action_reconfirm_ship(self, cr, uid, ids, orders_list, tries=0, context=None):
        # self.ship_confirm(cr, uid, ids, reconfirm=True)
        return self.pool.get('product.picking').get_shipping_label(cr, uid, 1)


    def get_shipping_label(self, cr, uid, ids, order_names, context=None):
        labels = []
        server_data = self.pool.get('fetchdb.server')
        server_ids = server_data.get_warehouse_servers(cr, uid, context=context)
        server_ids_2 = server_data.get_main_servers(cr, uid, context=context)
        picking_export_obj = self.pool.get('picking.export')

        fdx_hed_sql = """SELECT WhLocation, BJ_NO,  SERVICE_TP, ACCOUNT_BILLING_METHOD, ASIN,
        S_COMPANY, NAME, ADDRESS1, ADDRESS2, S_CITY, S_STATE, S_ZIP, S_COUNTRY, S_TEL,
        ACCOUNT, F_NAME, F_COMPANY, F_Address1,  F_Address2, F_CITY, F_STATE, F_ZIP, F_PHONE, F_Country,
        T_COMPANYN, T_NAME, T_Address1, T_Address2, T_City, T_State, T_Zip, T_Phone, T_COUNTRY,
        WEIGHT, DimL, DimW, DimH, shipperAccount, insurance, SIGNATURE, description
            FROM FDX_HED
            WHERE BJ_NO in ('%s');""" % ("', '".join(order_names))

        fdx_hed_list = server_data.make_query(
            cr, uid, server_ids[0],
            fdx_hed_sql,select=True
        )

        fed_shp_sql = """SELECT BJ_NO, LABEL_ID, RECORD_ID FROM delmarifctest.dbo.FED_SHP WHERE BJ_NO in ('%s');""" % ("', '".join(order_names))
        old_labels = server_data.make_query(
            cr, uid, 1,
            fed_shp_sql, select=True
        )

        done_labels = [x[0] for x in old_labels]
        created_spl = []
        counter = 0
        good_label = 0
        bad_label = 0
        error_messages = []
        # import pdb; pdb.set_trace()
        step_cr = self.pool.db.cursor()
        for fdx_hed in fdx_hed_list:
            po = fdx_hed[1]
            if po not in done_labels:
                print("\n\n\n\n\n\nPO number::::::   {}\nPO_number::: {}\n Good Labels: {}\n Bad_labels::: {}\n\n\n\n\n").format(counter, po, good_label, bad_label)
                counter += 1
                label = self.generate_shipping_label(cr, uid, ids, fdx_hed, tries=0)
                #error_list = label.split(' ')
                # if len(error_list) > 1 and label not in error_messages:
                #     error_messages.append(label)
                #     print("BAD LABEL")
                #     bad_label += 1
                if label:
                    print("LABEL CREATED")
                    good_label += 1
                    picking_export = picking_export_obj.search(cr, uid, [('po', '=', po)])
                    for export in picking_export_obj.browse(cr, uid, picking_export):
                        filename = po + '.pdf'
                        picking_export_obj.write(step_cr, uid, export.id, {'zpl_file': str(label),'zpl_filename': str(filename)})
                        step_cr.commit()
                        created_spl.append(po)
                else:
                    print("BAD LABEL")
                    bad_label += 1
        step_cr.close()
        print(error_messages)
        return labels

    def void_old_label(self, cr, uid, ids, old_labels, context=None):
        for old_label in old_labels:
            params = """xmlValue=<shipment>
            <header>
                <shipmentid>{bill_method}</shipmentid>
                <shipaction>VOID</shipaction>
            </header></shipment>""".format(bill_method=old_label[1], po=old_label[0])
            response = requests.post(url=url, headers=headers, data=params)
            print(response)
            print(response.text)
            response_xml = response.text
            response.close()

            delete_fed_shp_sql = """DELETE FROM delmarifctest.dbo.FED_SHP
            WHERE BJ_NO = '{}' AND RECORD_ID = {};""".format(order.name, old_label[2])
            
            fed_shp = server_data.make_query(
               cr, uid, 1, delete_fed_shp_sql,
                params=order, select=False, commit=True
            )

    def generate_shipping_label(self, cr, uid, ids, fdx_hed, tries=0, context=None):
        #shp_date = order.shp_date or ''
        #if not shp_date:
        url = "http://192.168.110.21:1607/"
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        server_data = self.pool.get('fetchdb.server')
        server_ids = server_data.get_warehouse_servers(cr, uid, context=context)
        server_ids_2 = server_data.get_main_servers(cr, uid, context=context)
        shp_date = datetime.datetime.now()
        shp_date = shp_date.strftime("%Y-%m-%d")
        warehouses = ['CAFER', 'USCHP']

        bill_method = str(fdx_hed[0])
        if bill_method not in warehouses:
            bill_method = warehouses[0]

        payment = 'PREPAID'
        billing_method = str(fdx_hed[3]) or ''
        if billing_method and 'collect' in billing_method.lower():
            payment = 'COLLECT'
        elif billing_method and ('3rd' in billing_method.lower() or 'third' in billing_method.lower()):
            payment = '3RDPARTY'

        payment = 'PREPAID'

        weight = fdx_hed[33]
        shipvia = fdx_hed[2]

        if shipvia in ('72') and float(weight) < 1.0:
            weight = '1.0'

        # account = str(fdx_hed[16])
        # if account == 'EXCHANGE RETURNS':
        #     account = ''

        signature = fdx_hed[39]
        signature_xml = ""
        if signature and signature != 'N':
            signature_xml = """<specialservices><service code="SR">Y</service></specialservices>"""

        asin = str(fdx_hed[4]) or ''
        asin_xml = ""
        if asin.lower() == 'null':
            asin = ''
        if asin:
            asin_xml = "<aesnumber>{}</aesnumber>".format(asin)


        
        phone_to = str(fdx_hed[13]).replace(' ', '')
        address_xml = """<address type="shipto">
                                <company>{company}</company>
                <attention>{name}</attention>
                <address1>{street}</address1>
                <address2>{street2}</address2>
                <city>{city}</city>
                <state>{state}</state>
                <zipcode>{zip}</zipcode>
                <country>{country}</country>
                <phone>{phone}</phone>
            </address>""".format( company=str(fdx_hed[5]) or '', name=str(fdx_hed[6]) or '',
                street=str(fdx_hed[7]), street2=str(fdx_hed[8]) or '',
                city=str(fdx_hed[9]), state=str(fdx_hed[10]), zip=str(fdx_hed[11]),
                country=str(fdx_hed[12]), phone=phone_to or '514-888-8888')

        billing_xml = ''

        if payment == '3RDPARTY' or payment == 'COLLECT':
            phone_from = str(fdx_hed[22]).replace(' ', '')
            account = ''
            account_number = str(fdx_hed[16])
            if len(account_number) > 6:
                account_number = account_number[:6]
            if payment == '3RDPARTY':
                "<account>{account}</account>".format(account=account_number)
            billing_xml = """<address type="billto">
                {account}
                    <company>{company}</company>
                    <attention>{name}</attention>
                    <address1>{street}</address1>
                    <address2>{street2}</address2>
                    <city>{city}</city>
                    <state>{state}</state>
                    <zipcode>{zip}</zipcode>
                    <phone>{phone}</phone>
                    <country>{country}</country>
                </address>""".format(account=account, company=str(fdx_hed[15]) or 'WALMART',
                    name=str(fdx_hed[16]), street=str(fdx_hed[17]),
                    street2=str(fdx_hed[18]) or '', city=str(fdx_hed[19]),
                    state=str(fdx_hed[20]), zip=str(fdx_hed[21]),
                    phone=phone_from or '514-888-8888', country=str(fdx_hed[23])
                )

        phone_r = str(fdx_hed[31]) .replace(' ', '')
        return_address_xml = ""
        return_zip = str(fdx_hed[30])
        return_address = str(fdx_hed[26])
        if return_zip and return_address:
            if return_zip != 'null' and return_address != 'null':
                return_address_xml = """<address type="returnto">
                    <company>{company}</company>
                    <attention>{name}</attention>
                    <address1>{street}</address1>
                    <address2>{street2}</address2>
                    <city>{city}</city>
                    <state>{state}</state>
                    <zipcode>{zip}</zipcode>
                    <phone>{phone}</phone>
                    <country>{country}</country>
                </address>""".format(company=str(fdx_hed[24]) or '', name=str(fdx_hed[25]),
                    street=str(fdx_hed[26]), street2=str(fdx_hed[27]) or '',
                    city=str(fdx_hed[28]), state=str(fdx_hed[29]), zip=str(fdx_hed[30]), phone=phone_r or '514-888-8888',
                    country=str(fdx_hed[32])
                )

        package_xml = """<packages>
        <package id="001">
            <weight>{weight}</weight>
            <length>{length}</length>
            <width>{width}</width>
            <height>{height}</height>
            {signature}
        </package>
        </packages>""".format(weight=str(weight) or '1.0', length=str(fdx_hed[34]), width=str(fdx_hed[35]),
            height=str(fdx_hed[36]), signature=signature_xml)

        params = """xmlValue=<shipment>
        <header>
            <shippingprofile>{bill_method}</shippingprofile>
            <ordernumber>{po}</ordernumber>
            <shipdate>{date}</shipdate>
            <shipvia>{carrier}</shipvia>
            <shipaction>SHIP</shipaction>
            <paymenttype>{payment}</paymenttype>
            <international>
                <print_certificate_of_origin>N</print_certificate_of_origin>
                <print_sed>N</print_sed>
            </international>
            {asin}{address}{billing_address}{return_address}
        </header>
        {package}
        </shipment>""".format(bill_method=bill_method, po=str(fdx_hed[1]),
            carrier=str(fdx_hed[2]), date=shp_date, payment=payment, asin=asin_xml, address=address_xml, billing_address=billing_xml,
            return_address=return_address_xml, package=package_xml)

        
        # import pdb; pdb.set_trace()
        # sending get request and saving the response as response object 
        response = requests.post(url=url, headers=headers, data=params)
        # print(response)
        # print(response.text)
        response_xml = response.text
        response.close()
        #response_xml = """<shipment><header><shippingprofile>USCHP</shippingprofile><ordernumber>JCP211658679_286-0</ordernumber><carrier>UPS_USCHP</carrier><servicelevel>G</servicelevel><shipvia>71</shipvia><shipdate>2020-08-31</shipdate><shipmentid>28880</shipmentid><shiprate>10.81</shiprate><shipaction>SHIP</shipaction><paymenttype>3RDPARTY</paymenttype><international><print_certificate_of_origin>N</print_certificate_of_origin><print_sed>N</print_sed></international><address type="shipto"><attention>MELADSON SMITH</attention><address1>2625 LARRY DR</address1><city>CHARLOTTE</city><state>NC</state><zipcode>28214</zipcode><phone>317-605-5349</phone><country>US</country></address><address type="billto"><account>V6W972</account><company>Jc Penney</company><attention>Jc Penney</attention><address1>1083 Main Street</address1><city>Champlain</city><state>NY</state><zipcode>12919</zipcode><phone>4018640975</phone><country>US</country></address><address type="returnto"><company>JCPenney</company><attention>JCPenney Invoice</attention><address1>6501 Legacy Dr.</address1><city>Plano</city><state>TX</state><zipcode>75024</zipcode><phone>null</phone><country>US</country></address></header><packages><package id="001"><db_id>56000</db_id><weight>0.25</weight> <length>3</length><width>3</width><height>2</height><tracknum>1Z8R9V140311132780</tracknum><rate>10.81</rate><charges><charge type="list base charge" code="list base">10.1</charge><charge type="base charge" code="base">10.1</charge><charge type="list fuel charge" code="list fuel">0.71</charge><charge type="fuel charge" code="fuel">0.71</charge></charges><label>XkNDXl5YQV5NQ1leWFpeWEFeTEgwLDBeTUQxMF5NVFReTU1UXlBPTl5QUjEyLDEyXkNGCl5GTzAwMSwyNzVeR0IyNjUsMCwwMl5GUwpeRk8yNjUsMjc1XkdCMCwyMzAsMDNeRlMKXkZPMjY1LDMzNV5HQjU3NSwwLDAyXkZTCl5GTzAwMSw1MDVeR0I4NDUsMCwwMl5GUwpeRk8wMDEsNTc1XkdCODQ1LDAsMDJeRlMKXkZPMDAxLDczNV5HQjg0NSwwLDAyXkZTCl5GTzAwMSw5MTJeR0I4MzAsMCwwMV5GUwpeRk8wNDAwLDAwMTheQUJOLDAwMzMsMDAxN15GRDEgTEJTIF5GUwpeRk8wNjUwLDAwMTheQUJOLDAwMjIsMDAxN15GRDEgT0YgMV5GUwpeRk8wNTU1LDAwNTVeQUROLDAwMTgsMDAxMF5GQjQwMCwzLDEsTCwwXkZEXkZTCl5GTzAwNDUsMDAxMF5BRE4sMDAxOCwwMDEwXkZCNTAwLDUsMCxMLDBeRkRKQ1BFTk5FWSBJTlZPSUNFXCZOVUxMXCZKQ1BFTk5FWVwmNjUwMSBMRUdBQ1kgRFIuXCZQTEFOTyBUWCA3NTAyNF5GUwpeRk8xNzAsMTUwXkEwTiwwMzAsMDQwXkZEU0hJUF5GUwpeRk8xNzAsMTc1XkEwTiwwMzAsMDQwXkZEIFRPOl5GUwpeRk8wMjcwLDAxNDVeQUZOLDAwMjAsMDAxMl5GQjgwMCwzICwtMiwsMF5GRE1FTEFEU09OIFNNSVRIXCYoMzE3KTYwNS01MzQ5XCYyNjI1IExBUlJZIERSXkZTCl5GTzAyNzAsMzAwXkFCTiwwMDMzLDAwMTdeRkRDSEFSTE9UVEUgTkMgMjgyMTReRlMKXkJZMywyLjVeRlNeRk8zNDAsNDAwLF5CQ04sMTAwLE4sTixOXkZEPjo0MjAyODIxNF5GUwpeRk8wNzYwLDA1MDVeQUROLDEwMCwwNjBeR0IwNzAsNzAsNzBeRlMKXkZPMDMyNSwwMzQwXkFETiwwMDcyLDAwMzBeRkROQyAyODIgOS0wM15GUwpeRk8wMDQwLDA1MTBeQUJOLDAwMzMsMDAxN15GRFVQUyBHUk9VTkReRlMKXkZPNDAsMjkwXkJEMl5GSF5GRDAwMzg0MDI4MjE0MDAwMFspPl8xRTAxXzFEOTYxWjExMTMyNzgwXzFEVVBTTl8xRDhSOVYxNF8xRTA3VlNMMDRfMERfMjUoOl8yMyc5LkcoMl8xREg4SjpRHDkiK0dXTVBCMkJEXzI1ODk1QzBLWV8yM1NfMERfMUUEXkZTCl5GTzA0MCw1NTBeQUJOLDAwMjIsMDAxN15GRFRSQUNLSU5HICM6XkZTCl5GTzIzNSw1NDVeQUROLDAwMzYsMDAxMF5GRDFaIDhSOSBWMTQgMDMgMTExMyAyNzgwXkZTCl5CWTMsMi41XkZTCl5GTzEzNSw1ODVeQkNOLDE0NSxOLE4sTl5GRD46MVo4UjlWMTQwMz41MTExMzI3ODBeRlMKXkZPMDM1LDc0MF5BQk4sMDAyMiwwMDVeRkRCSUxMSU5HOiAzUkQgUEFSVFleRlMKXkZPMDAzNSwwMDc2NV5BQk4sMDAyMiwwMDA1XkZCOTAwLDEsMixMLDBeRkRSRUYgMzogMDAxXkZTCl5GTzU0NSw4MjVeQTBOLDAxOCwwMTheRkRMR1MgNC40LjAgWkVCUkEgWjRNIDc1LjVWIDA0LzIwMTZeRlMKXlBRMSwwMDAwLDAwMDAsTl5YWg==</label></package></packages></shipment>"""
        root_xml = etree.fromstring(response_xml)
        error_xml = root_xml.find("errors")
        header_xml = root_xml.find("header")
        package_xml = root_xml.find("packages").find("package")

        if error_xml:
            error_xml = error_xml.findall("error")
            full_error_text = ''
            for error in error_xml:
                error_text = etree.tostring(error, method="text")
                error_text = error_text.replace("\r","").replace("\n","").replace("\t","")
                print(error_text)

                if ('API Error: Ship date can not be less than current date' in error_text or 'No Open Manifest for' in error_text) and tries == 0:
                    carrier = self.get_xml_data(etree, header_xml, "carrier")
                    service = self.get_xml_data(etree, header_xml, "servicelevel")
                    openmanifest_xml = """xmlValue=<shipment><header>
                    <shippingprofile>{bill_method}</shippingprofile>
                    <ordernumber>{po}</ordernumber>
                    <shipdate>{date}</shipdate>
                    <carrier>{carrier}</carrier>
                    <servicelevel>{service}</servicelevel>
                    <shipaction>OPENMANIFEST</shipaction>
                    </header>
                    </shipment>""".format(bill_method=str(fdx_hed[0]) or'USCHP', po=str(fdx_hed[1]),
                        carrier=carrier, service=service, date=shp_date, payment=payment)
                    response = requests.post(url=url, headers=headers, data=openmanifest_xml)
                    # print(response)
                    # print(response.text)
                    response_xml = response.text
                    response.close()
                    return self.generate_shipping_label(cr, uid, ids, fdx_hed, tries=1, context=context)
                elif ('API Error: Package Rating Service is Unavailable, Please try again.' in error_text):
                    fdx_hed[2] = '50'
                    return self.generate_shipping_label(cr, uid, ids, fdx_hed, tries=1, context=context)
                elif ('API Error: RequestedShipment Recipient contact - phoneNumber is required' in error_text):
                    import pdb; pdb.set_trace()
                    print(weight)
                # elif tries == 0:
                #     if bill_method == 'CAFER':
                #         fdx_hed[0] = 'USCHP'
                #     else:
                #         fdx_hed[0] = 'CAFER'
                #     return self.generate_shipping_label(cr, uid, ids, fdx_hed, tries=1, context=context)
                else:
                    full_error_text += ('\n' + error_text)
            return False

        fed_shp = {}

        record = self.get_xml_data(etree, header_xml, "shipmentid")
        shipdate = self.get_xml_data(etree, header_xml, "shipdate")
        carrier = self.get_xml_data(etree, header_xml, "carrier") + ', ' + self.get_xml_data(etree, header_xml, "servicelevel")

        label = self.get_xml_data(etree, package_xml, "label")
        track_no = self.get_xml_data(etree, package_xml, "tracknum")
        rate = self.get_xml_data(etree, package_xml, "rate")

        fed_shp_sql = """INSERT INTO delmarifctest.dbo.FED_SHP
            (BJ_NO, TRACK_NO, SHP_DATE, SHP_TYPE, Carrier, SHP_Cost, SHP_Weight, ZPL_LABEL, LABEL_ID, warehouse, scanner)
            VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', 'AUTOMATED');""".format(
                str(fdx_hed[1]), track_no, shipdate, str(fdx_hed[2]),
                carrier, rate, str(fdx_hed[33]), label, record, str(fdx_hed[0])
            )


        fed_shp = server_data.make_query(
            cr, uid, 1, fed_shp_sql,
            select=False, commit=True
        )
        # print(fed_shp)
        return label

    def get_xml_data(self, etree, xml_root, xml_name):
        data = etree.tostring(xml_root.find(xml_name), method="text")
        data = data.replace("\r","").replace("\n","").replace("\t","")
        return data

    def action_validate_flash_done(self, cr, uid, ids, context=None):

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        if context.get('active_ids'):
            if len(context.get('active_ids')) > 1:
                deliver_objs = self.pool.get("stock.picking").browse(cr, uid, context.get('active_ids'))
                orders = str([do.name+", " for do in deliver_objs])[1:-1]
                user = self.pool.get('res.users').browse(cr, uid, uid)
                import time

                for order in deliver_objs:
                    history = order.report_history or ''
                    history += '\n%s %s: Approved flash orders:\n%s' % (
                        time.strftime('%m-%d %H:%M:%S', time.gmtime()),
                        user.name or '',
                        orders
                    )
                    self.pool.get('stock.picking').write(cr, uid, order.id, {
                        'manual_done_approved': True,
                        'report_history': history,
                    })
                return {'type': 'ir.actions.act_window_close'}
        else:
            res_id = self.pool.get("stock.picking.done.approve.wizard").create(cr, uid, {'picking_id': ids[0]})
            act_win_res_id = data_obj._get_id(cr, uid, 'sale_integration', "action_stock_picking_done_approve")

        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], context=context)
        act_win.update(res_id=res_id)
        return act_win

    def routing_request(self, cr, uid, ids):
        res = True

        if(not isinstance(ids, list)):
            ids = [ids]
        for order_id in ids:
            if(not self.browse(cr, uid, order_id).sale_integration_ship_confirmed):
                res = self.pool.get('sale.integration').integrationApiRoutingRequest(cr, uid, order_id)
        return res

    def get_ch_info(self, cr, uid, ids, context=None):
        errors = []
        ids = ids and ids[0] if isinstance(ids, (tuple, list)) else ids or None
        if ids:
            picking = self.browse(cr, uid, ids)
            po_number = picking.sale_id.external_customer_order_id or None
            external_customer_id = (
                picking and
                picking.real_partner_id and
                picking.real_partner_id.external_customer_id or
                None
            )
            partner_name = (
                picking and
                picking.real_partner_id and
                picking.real_partner_id.name or
                ''
            )
            conf_obj = self.pool.get('ir.config_parameter')
            ch_accounts = conf_obj.get_param(cr, uid, 'sale.integrarion.ch_accounts', '{}')
            ch_accounts = json_loads(ch_accounts)
            partner_map = conf_obj.get_param(cr, uid, 'sale.integrarion.partner_map', False)
            mapping_type_orders = conf_obj.get_param(cr, uid, 'sale.integrarion.mapping_type_orders', False)
            mapping_accounts = {}
            for ch_acc_name, ch_acc_row in ch_accounts.iteritems():
                customer_ids = ch_acc_row.get('customer_ids', [])
                for ex_cust_id in customer_ids:
                    mapping_accounts.update({
                        ex_cust_id: ch_acc_name,
                    })

            ch_account_name = None
            if external_customer_id and mapping_accounts:
                ch_account_name = mapping_accounts.get(external_customer_id, False)
            else:
                ch_account_name = False
            if ch_account_name:
                if ch_accounts:
                    partner_map = json_loads(partner_map)
                    mapping_type_orders = json_loads(mapping_type_orders)
                    ch_account_settings = ch_accounts.get(ch_account_name, False)
                    if ch_account_settings:
                        ch_settings = {
                            'connection': ch_account_settings,
                            'partner_map': partner_map,
                            'mapping_type_orders': mapping_type_orders,
                        }
                    api = CommerceHubRemoteAPIClient(ch_settings)
                    order_info = api.get_order_info(po_number)
                    errors.extend(order_info['errors'])
            else:
                errors.append(
                    '{partner_name} is not CommerceHub customer or not have mapping in sale.integrarion.ch_accounts'.format(
                        partner_name=partner_name))
            res = ''
            if errors:
                tmpl = '''
                    </br>
                    Errors
                    </br>
                    <table border="1">
                        % for err in errors:
                            <tr>
                                <td>${err}</td>
                            </tr>
                        % endfor
                    </table>
                '''
                tpl_table = Template(tmpl)
                res = tpl_table.render(errors=errors)
            else:
                tmpl = '''
                    </br>
                    Main Information
                    </br>
                    <table border="1">
                        <tr>
                            <td>VENDOR</td>
                            <td>MERCHANT</td>
                            <td>PO NUMBER</td>
                            <td>SHIPPING METHOD</td>
                            <td>CUSTOMER ORDER NUMBER</td>
                            <td>ORDER DATE</td>
                            <td>STATUS</td>
                            <td>SUBSTATUS</td>
                            <td>SALES DIVISION</td>
                        </tr>
                        <tr>
                            <td>${order_info["vendorName"]}</td>
                            <td>${order_info["merchantName"]}</td>
                            <td>${order_info["poNumber"]}</td>
                            <td>${order_info["shippingMethod"]}</td>
                            <td>${order_info["customerOrderNumber"]}</td>
                            <td>${order_info["orderDate"]}</td>
                            <td>${order_info["orderStatus"]}</td>
                            <td>${order_info["orderSubStatus"]}</td>
                            <td>${order_info["salesDivision"]}</td>
                        </tr>
                    </table>
                    </br>
                    Addresses
                    </br>
                    <table border="1">
                        <tr>
                            <td>Ship To</td>
                            <td>Bill To</td>
                        </tr>
                        <tr>
                            <td>${order_info["addresses"]["ship_to"]}</td>
                            <td>${order_info["addresses"]["bill_to"]}</td>
                        </tr>
                    </table>
                    </br>
                    Order Lines
                    </br>
                    <table border="1">
                        <tr>
                            <td>LINE #</td>
                            <td>MERCHANT SKU</td>
                            <td>VENDOR SKU</td>
                            <td>DESCRIPTION</td>
                            <td>QTY ORDERED</td>
                            <td>QTY</td>
                            <td>STATUS</td>
                            <td>EXPECTED SHIP DATE</td>
                        </tr>
                        % for line in lines:
                            <tr>
                                <td>${line["lineNumber"]}</td>
                                <td>${line["merchantSku"]}</td>
                                <td>${line["vendorSku"]}</td>
                                <td>${line["description"]}</td>
                                <td>${line["qtyOrdered"]}</td>
                                <td>${line["qty"]}</td>
                                <td>${line["status"]}</td>
                                <td>${line["expectedShipDate"]}</td>
                            </tr>
                        % endfor
                    </table>
                    </br>
                    Events
                    </br>
                    <table border="1">
                        <tr>
                            <td>EVENT NAME</td>
                            <td>DESCRIPTION</td>
                            <td>DATE</td>
                            <td>TIME</td>
                        </tr>
                        % for event in states:
                            <tr>
                                <td>${event["state"]}</td>
                                <td>${event["description"]}</td>
                                <td>${event["date"]}</td>
                                <td>${event["time"]}</td>
                            </tr>
                        % endfor
                    </table>
                '''
                tpl_table = Template(tmpl)
                res = tpl_table.render(order_info=order_info, lines=order_info['lines'], states=order_info['states'])
            picking.write({
                'ch_info': res,
            })
        return True

stock_picking()


class stock_move(osv.osv):
    _inherit = 'stock.move'
    _columns = {
        'external_customer_return_id': fields.char('External Return Id', size=128),
        'external_customer_line_id': fields.char('External Customer Line Id', size=128),
        'return_code': fields.char('Return Code', size=128),
        'return_reason': fields.char('Return Reason', size=128),
        'rma_number': fields.char('RMA', size=128),
        'ars_issued': fields.char('Ars Issued', size=128),
        'return_partner_reason': fields.many2one('stock.partner.return', 'Return Reason'),
    }

stock_move()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
