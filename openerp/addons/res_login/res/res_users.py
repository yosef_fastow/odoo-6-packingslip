import logging

import pooler
from osv import fields,osv
import datetime

_logger = logging.getLogger(__name__)

class users(osv.osv):
    _inherit = "res.users"

    def authenticate(self, db, login, password, user_agent_env):
        result = super(users, self).authenticate(db, login, password, user_agent_env)

        if result:
            cr = pooler.get_db(db).cursor()
            login_obj = pooler.get_pool(cr.dbname).get('res.login')
            login_obj.create(cr, 1, {'user_id': result, 'create_date': datetime.datetime.now()})

        return result

users()