{
    "name": "res_login",
    "version": "0.1",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "depends" : [
        "base"
    ],
    "description" : """
This module allows write log of users and time when they was on site.
=============================================

    """,
    "category": "Project Management",
    "category": "Hidden",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'res/res_login_view.xml'
    ],
    "application": True,
    "active": False,
    "installable": True,
    "installable": True,
    "auto_install": True,
}