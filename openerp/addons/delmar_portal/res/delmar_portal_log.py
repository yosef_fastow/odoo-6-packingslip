import logging
from osv import fields, osv

_logger = logging.getLogger(__name__)

class delmar_portal_log(osv.osv):
    _name = 'delmar.portal.log'
    _columns = {
        'user_id': fields.many2one('res.users','User'),
        'context': fields.char('Context', size=250),
        'create_date': fields.datetime('Login Date', readonly=True, select=1),
    }
    _defaults = {
        'context': "{}",
    }
    _order = 'create_date desc'

    def create(self, cr, uid, values, context=None):
        cr.autocommit(True)
        try:
            cr.execute("""INSERT INTO delmar_portal_log(
                    user_id, create_date
                ) VALUES (%s, %s)""",
                (values['user_id'], values['create_date']))

        except Exception:
            _logger.exception("Failed to create delmar.portal.log row")
            return False

        finally:
            cr.close()

        return True

delmar_portal_log()