{
    "name" : "delmar_portal",
    "version" : "0.1",
    "author" : "Ivan Burlutskiy @ Prog-Force",
    "website" : "http://www.progforce.com/",
    "depends" : ["base", "delmar_iframe", "fetchdb"],
    "description" : """

    """,
    "category": "Tools",
    "images": [
        'images/dashboard.png',
        'images/dashboard-hover.png'
    ],
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
        'security/security_portal.xml',
        'security/ir.model.access.csv',
        'delmar_portal_view.xml',
        'res/delmar_portal_log_view.xml'
    ],
    "active": False,
    "application": True,
    "installable": True,
    'auto_install': False,
}