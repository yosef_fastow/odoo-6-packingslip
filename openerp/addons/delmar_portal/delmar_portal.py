import logging

import pooler
from osv import fields,osv
import datetime

_logger = logging.getLogger(__name__)

class delmar_portal(osv.osv):
    _name = "delmar.portal"
    _description = "Delmar Portal"

    _columns = {
        'name': fields.char('Name', size=256, required=True, readonly=True),
        'url': fields.char('Url', size=256, required=True, readonly=True),
    }


    def view_init(self, cr, uid, fields, context=None):

        cr = pooler.get_db(cr.dbname).cursor()
        log_obj = pooler.get_pool(cr.dbname).get('delmar.portal.log')
        log_obj.create(cr, 1, {'user_id': uid, 'create_date': datetime.datetime.now()})

delmar_portal()