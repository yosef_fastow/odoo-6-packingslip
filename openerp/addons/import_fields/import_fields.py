# -*- coding: utf-8 -*-

from osv import osv, fields
import csv
import base64
import datetime


class import_process(osv.osv):
    _name = "import.process"

    _columns = {
        "create_uid": fields.many2one('res.users', 'Creator'),
        "customer_id": fields.many2one('res.partner', 'Customer', ondelete='cascade', domain="[('customer', '=', True)]"),
        "template_id": fields.many2one('export.template', 'Template', ondelete='cascade'),
        "tag_id": fields.many2one('tagging.tags', 'Tag', ondelete='cascade'),
        "csv_file": fields.binary(string="Input file"),
        "state": fields.selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('done', 'Done')], 'Status', select=True, required=True, readonly=True),
        "first_row": fields.boolean("Skip first line", help="Tick this if the first line of the file contains headers"),
        "customer_field_value_ids": fields.many2many("product.multi.customer.fields", "import_process_fields", "import_process_id", "field_value_id", string="Customer Field Values"),
        "customer_field_id": fields.many2one('product.multi.customer.fields.name', 'Customer Field Name', ondelete='cascade'),
        "show_values_confirm": fields.boolean("Refresh Values"),
        "generate_ice_sku": fields.boolean("Generate ICE SKU"),
        "absent_products": fields.text("Absent Products"),
    }

    _defaults = {
        'state': 'draft',
        'csv_file': '',
        'first_row': True,
    }

    def process_import_lines(self, cr, uid, import_rows, params, context=None):
        customer_fields = self.pool.get('product.multi.customer.fields')

        product_list = []
        absent_products = []
        done_ids = []

        row_len = params['row_len']
        id_idx = params['id_idx']
        size_idx = params['size_idx']
        size_field = params['size_field']
        template_id = params['template_id']
        fields_ids = params['fields_ids']
        default_fields_ids = params['default_fields_ids']
        order = params['order']

        for row in import_rows:
            row = [el if not isinstance(el, (str, unicode)) else el.strip() for el in row]

            reiterative_id = False
            if row_len != len(row):
                raise osv.except_osv('Row length in file(' + str(len(row)) + ') is not equal number of fields in template(' + str(row_len) + ').', '')
                return False

            row_ids = [row[id_idx]]

            for row_id in row_ids:
                row[id_idx] = row_id

                # check on the contents of the size in idenify cell
                identifier = row[id_idx]
                size_id = False
                if size_idx and size_field:
                    size_str = row[size_idx]
                    try:
                        size_name = float(size_str)
                    except:
                        size_name = False
                    size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                elif '/' in row[id_idx]:
                    cell_els = row[id_idx].split('/')
                    size_str = cell_els[1]
                    try:
                        size_name = float(size_str)
                    except:
                        size_name = False
                    size_id = self.pool.get('ring.size').search(cr, uid, [('name', '=', size_name)])
                    if size_id:
                        identifier = cell_els[0].strip()

                if template_id and template_id.identify_field and template_id.identify_field.customer_id:
                    prod_field_ids = customer_fields.search(cr, uid, [
                        ('field_name_id', '=', template_id.identify_field.id),
                        ('partner_id', '=', template_id.identify_field.customer_id.id),
                        ('value', '=', identifier)
                    ])
                    product = [fie.product_id.id for fie in customer_fields.browse(cr, uid, prod_field_ids)]
                    product = list(set(product))
                    if size_id is False:
                        siz = [fie.size_id.id for fie in customer_fields.browse(cr, uid, prod_field_ids)]
                        siz = list(set(siz))
                        if len(siz) > 0:
                            size_id = siz
                else:
                    product = []
                    cr.execute("select p.id from product_product p where p.default_code = '%s' and p.type = 'product'" % (identifier,))
                    product_id = cr.fetchone()
                    if product_id and product_id[0]:
                        product = [product_id[0]]

                    if not product:
                        cr.execute("select p.id from product_product p left join product_template t on p.product_tmpl_id = t.id where t.id_delmar = '%s' and p.type = 'product'" % (identifier,))
                        product_id = cr.fetchone()
                        if product_id and product_id[0]:
                            product = [product_id[0]]

                if len(product) != 1:
                    absent_products += [identifier]
                    continue
                if identifier in done_ids:
                    reiterative_id = True
                else:
                    product_list += product
                for idx, cell in enumerate(row):
                    if idx not in (id_idx, size_idx):
                        if order[idx] in fields_ids:

                            self.save_multi_customer_value(cr, uid, order[idx], product, size_id, cell)

                        elif order[idx] in default_fields_ids and not reiterative_id:
                            try:
                                def_field = self.pool.get("product.multi.customer.fields.name").browse(cr, uid, order[idx])
                                models = ['product.product', 'product.template']
                                base_model = None
                                prod_field = None

                                for model in models:
                                    if def_field.label in self.pool.get(model)._columns:
                                        base_model = model
                                        prod_field = self.pool.get(model)._columns[def_field.label]
                                        break

                                if not base_model:
                                    # If this is not a base product field - then just update customer's field
                                    self.save_multi_customer_value(cr, uid, order[idx], product, size_id, cell)
                                    continue

                                if prod_field._type in ('char', 'text'):
                                    self.pool.get(base_model).write(cr, uid, product, {def_field.label: cell})
                                elif prod_field._type in ('many2one', 'one2one') and cell:
                                    search_obj = self.pool.get(prod_field._obj).search(cr, uid, [['name', '=', cell]])
                                    if search_obj:
                                        self.pool.get(base_model).write(cr, uid, product, {def_field.label: search_obj[0]})
                                    else:
                                        raise osv.except_osv('Wrong name "' + cell + '".', '')
                                elif prod_field._type == 'boolean':
                                    self.pool.get(base_model).write(cr, uid, product, {def_field.label: bool(cell)})
                                elif prod_field._type == 'float':
                                    try:
                                        self.pool.get(base_model).write(cr, uid, product, {def_field.label: float(cell)})
                                    except ValueError:
                                        raise osv.except_osv('Cannot convert "' + cell + '" to float', '')
                                elif prod_field._type == 'int':
                                    try:
                                        self.pool.get(base_model).write(cr, uid, product, {def_field.label: int(cell)})
                                    except ValueError:
                                        raise osv.except_osv('Cannot convert "' + cell + '" to int', '')
                            except csv.Error:
                                pass

                if not reiterative_id:
                    done_ids.append(identifier)

        return product_list, absent_products

    def save_multi_customer_value(self, cr, uid, field_name_id, product, size_id, cell):
        customer_fields = self.pool.get('product.multi.customer.fields')

        field_name = self.pool.get('product.multi.customer.fields.name').browse(cr, uid, field_name_id)
        field_args = [('field_name_id', '=', field_name_id), ('product_id', 'in', product)]
        field_vals = {'value': cell, 'field_name_id': field_name_id, 'product_id': product[0]}
        if field_name.expand_ring_sizes:
            size_param = False
            if size_id:
                size_param = size_id[0]

            field_args.append(('size_id', '=', size_param))
            field_vals.update({'size_id': size_param})

        else:
            if size_id:
                raise osv.except_osv('Can not import size data into not expanded by size Customer Field "%s"' % (field_name.label,), '')
                return False

        find_fields = customer_fields.search(cr, uid, field_args, context={})
        if find_fields:
            customer_fields.write(cr, uid, find_fields, {'value': cell})
        else:
            customer_fields.create(cr, uid, field_vals)

        return True

    def import_confirm(self, cr, uid, ids, *args):
        cur = self.browse(cr, uid, ids[0])

        if not cur.template_id and not cur.customer_field_id:
            raise osv.except_osv('"Template" or "Customer field" should be specified', '')

        if cur.csv_file:
            try:

                fields_list = []

                size_field = False
                size_idx = False
                product_list = []
                fields_ids = []
                default_fields_ids = []
                order = []
                id_idx = 0

                if cur.customer_field_id:
                    default_code_field_id = self.pool.get('product.multi.customer.fields.name').search(cr, uid, [('label', '=', 'default_code')])
                    if default_code_field_id:
                        default_code_field = self.pool.get('product.multi.customer.fields.name').browse(cr, uid, default_code_field_id)
                    if not default_code_field:
                        raise osv.except_osv('Field for identification is not found (with label = "default_code")')

                    fields_list = [default_code_field[0], cur.customer_field_id]

                    order = [x.id for x in fields_list]

                if cur.template_id:
                    if not cur.template_id.identify_field:
                        raise osv.except_osv('Field for identification is not selected in the template')

                    if cur.template_id.size_field and cur.template_id.size_field != cur.template_id.identify_field:
                        size_field = cur.template_id.size_field

                    fields_list = cur.template_id.customer_fields_ids

                    if cur.template_id.order_ids:
                        order = cur.template_id.order_ids.split(",")
                        for cntr, elem in enumerate(order):
                            order[cntr] = int(elem)
                    id_idx = order.index(cur.template_id.identify_field.id)

                    if size_field:
                        size_idx = order.index(size_field.id)

                # ! Import 'fields' from line 3 shadowed by loop variable
                for fields in fields_list:
                    if fields.expand_by_customer is False or fields.customer_id:
                        fields_ids.append(fields.id)
                    else:
                        default_fields_ids.append(fields.id)

                import_file = base64.decodestring(cur.csv_file)
                import_rows = import_file.replace('\r\n', '\n').replace('\r', '\n').split('\n')
                row_len = len(fields_list)
                if cur.first_row:
                    import_rows = import_rows[1:]
                if not import_rows[-1]:
                    import_rows = import_rows[:-1]

                import_rows = list(csv.reader(import_rows))

                params = {
                    'row_len': row_len,
                    'id_idx': id_idx,
                    'size_idx': size_idx,
                    'size_field': size_field,
                    'template_id': cur.template_id,
                    'fields_ids': fields_ids,
                    'default_fields_ids': default_fields_ids,
                    'order': order,
                }

                product_list, absent_products = self.process_import_lines(cr, uid, import_rows, params)

            except csv.Error:
                raise osv.except_osv('Cannot import this file', '')

            if cur.tag_id:
                self.pool.get('tagging.tags').write(cr, uid, cur.tag_id.id, {'product_ids': [(6, 0, product_list)]})

            self.write(cr, uid, ids, {'state': 'confirm', 'absent_products': ',\n'.join(absent_products)})

            if cur.customer_field_id:
                self.onchange_show_values(cr, uid, ids, cur.customer_field_id.id, cur.customer_id.id, context=None)

            return True

        else:
            raise osv.except_osv('File is not selected', '')
        return False

    def onchange_show_values(self, cr, uid, ids, customer_field_id, customer_id, context=None):
        search_criteria = []
        if customer_field_id:
            search_criteria.append(('field_name_id', '=', customer_field_id))
        else:
            return {}

        if customer_id:
            search_criteria.append(('partner_id', '=', customer_id))

        customer_field_value_ids = self.pool.get('product.multi.customer.fields').search(cr, uid, search_criteria, context=context)

        return {'value': {'customer_field_value_ids': customer_field_value_ids}}

    def onchange_generate_ice_sku(self, cr, uid, ids, context=None):
        seq_ids = self.pool.get('ir.sequence').search(cr, uid, [('name', '=', 'ICE SKU')])
        if seq_ids:
            sel_sql = """
                select a.id, tp.product_id
                from taggings_product tp
                left join tagging_tags tg on tg.id = tp.tag_id
                left join product_product p on tp.product_id = p.id
                left join product_template t on t.id = p.product_tmpl_id
                left join
                ( select f.id, f.product_id, f.value
                    from product_multi_customer_fields f
                    left join product_multi_customer_fields_name fn on fn.id = f.field_name_id and fn.label = 'Customer SKU'
                    left join res_partner rp on rp.id = fn.customer_id and rp.ref = 'ICE'
                    where f.size_id is null
                      and rp.id is not null
                ) a on a.product_id = tp.product_id
                where tg.name = 'ICEUS'
                    and (a.id is null or a.value = '')
            """

            cr.execute(sel_sql)
            res = cr.dictfetchall()

            for row in res:
                inv_seq = self.pool.get('ir.sequence').next_by_id(cr, uid, seq_ids[0])
                if row['id']:
                    upd_sql = """
                        update product_multi_customer_fields
                        set value = %(val)s,
                            write_uid = %(uid)s,
                            write_date = (now() at time zone 'UTC')
                        where id = %(id)s
                    """

                    cr.execute(upd_sql, {
                        'val': inv_seq,
                        'uid': uid,
                        'id': row['id']
                    })
                else:
                    ins_sql = """
                        insert into product_multi_customer_fields
                        (id, create_uid, create_date, field_name_id, product_id, size_id, value, partner_id)
                        select
                         nextval('product_multi_customer_fields_id_seq'),
                         %(uid)s,    -- create_uid
                         (now() at time zone 'UTC'), -- create_date
                         fn.id, -- field_name_id
                         %(product_id)s,    -- product_id
                         null,  -- size_id
                         %(val)s,    -- value
                         rp.id  -- partner_id
                        from product_multi_customer_fields_name fn
                        left join res_partner rp on rp.id = fn.customer_id
                        where 1=1
                            and fn.label = 'Customer SKU'
                            and rp.ref = 'ICE'
                        limit 1
                    """

                    cr.execute(ins_sql, {
                        'val': inv_seq,
                        'uid': uid,
                        'product_id': row['product_id']
                    })

            sql_del_empty_size_value = """
                delete
                from product_multi_customer_fields f
                where 1=1
                and f.value = ''
                and f.size_id is not null
                and f.field_name_id = (
                    select fn.id
                    from product_multi_customer_fields_name fn
                        left join res_partner p on fn.customer_id = p.id
                    where label = 'Customer SKU'
                        and p.ref = 'ICE'
                )
            """

            cr.execute(sql_del_empty_size_value)

            sql_size_value_generate = """
                insert into product_multi_customer_fields
                (create_uid, create_date,field_name_id, product_id, size_id, value, partner_id)
                select
                         %(uid)s as create_uid,
                         current_timestamp as create_date,
                         f.field_name_id as field_name_id,
                         p.id as product_id,
                         rs.id as size_id,
                         f.value || '_' || case when rs.name::real < 10 then '0' || regexp_replace(rs.name, '\.', '') else
                             regexp_replace(rs.name, '\.', '')
                             end as value,
                         f.partner_id
                from product_ring_size_default_rel rel
                left join product_product p on p.product_tmpl_id = rel.product_id
                left join product_multi_customer_fields f on f.product_id = p.id
                left join product_multi_customer_fields fd
                    on fd.product_id = p.id
                    and fd.size_id = rel.size_id
                    and fd.field_name_id = f.field_name_id
                left join ring_size rs on rs.id = rel.size_id
                where 1=1
                and (fd.id is null) -- or fd.value = '')
                and f.value <> ''
                and f.size_id is null
                and f.field_name_id = (
                    select fn.id
                    from product_multi_customer_fields_name fn
                        left join res_partner p on fn.customer_id = p.id
                    where label = 'Customer SKU'
                        and p.ref = 'ICE'
                )
            """

            cr.execute(sql_size_value_generate, {
                'uid': uid
            })

        return {}

    def onchange_customer_id(self, cr, uid, ids, customer_id, context=None):
        return {'value': {'customer_field_id': None}}

import_process()
