# -*- coding: utf-8 -*-

##############################################################################
#
#    Progforce
#    Copyright (C) 2012
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlrd
import csv
from osv import fields, osv
from ftplib import FTP
import cStringIO
import os
from datetime import datetime, timedelta
import logging
import time
from openerp.addons.pf_utils.utils import backuplocalfileutils as blf_utils
from xlrd.xldate import XLDateAmbiguous
import re

_logger = logging.getLogger(__name__)

FILE_TYPES = [
    ('csv', 'Csv'),
    ('xls', 'Xls'),
]

LIMIT_INSERT = 1000


class ftp_settings(osv.osv):
    _name = "ftp.settings"

    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'ftp_host': fields.char('Host', size=128, required=True),
        'ftp_user': fields.char('User', size=128, required=True),
        'ftp_pass': fields.char('Password', size=128, required=False),
        'ftp_port': fields.integer('Port', size=10, required=False),
        'ftp_security': fields.selection(
            [
                ('none', 'None'),
                ('ssh', 'SSH'),
                ('ssl', 'SSL'),
            ],
            'Security', select=True, required=True, ),
        'ftp_passive_mode': fields.boolean('Passive Mode'),
        'ftp_timeout': fields.integer('Timeout', size=10, required=False),
        'ftp_use_keys': fields.boolean('Use SSH keys'),
        'password_send_expiration': fields.boolean('Notify password expiration'),
        'password_renewed': fields.boolean('Password was renewed', help='Check if password was renewed after expiration'),
        'password_expiration_period': fields.integer('Expiration period, days', size=3, required=False),
        'password_renew_date': fields.datetime('Next renew date', required=False, readonly=True),
        'password_notification_emails': fields.char('Notification emails', size=256, required=False, help='Comma-separated list of email addresses'),
    }

    _defaults = {
        'ftp_security': 'none',
        'ftp_passive_mode': False,
        'ftp_port': 21,
        'ftp_timeout': 10,
        'password_send_expiration': False,
        'password_renewed': False,
        'password_expiration_period': 90
    }

    def write(self, cr, uid, ids, vals, context=None):
        if 'password_renewed' in vals and vals.get('password_renewed', False):
            for setting in self.browse(cr, uid, ids):
                next_expiration = setting.password_expiration_period
                if 'password_expiration_period' in vals:
                    next_expiration = vals.get('password_expiration_period', 0)
                if next_expiration > 0:
                    vals.update({
                        'password_renew_date': datetime.today() + timedelta(days=next_expiration),
                        'password_renewed': False
                    })
        return super(ftp_settings, self).write(cr, uid, ids, vals, context)

    def notify_password_expiration(self, cr, uid, recipients=[]):
        if not recipients:
            recipients = []
        ids = self.search(cr, uid, [('password_send_expiration', '=', True)])
        if len(ids) > 0:
            for setting in self.browse(cr, uid, ids):
                # recipients
                to_recipients = []
                if setting.password_notification_emails:
                    to_recipients = recipients + [r.strip() for r in setting.password_notification_emails.split(',')]
                # message templates
                mail_message = {
                    'to_send': False,
                    'subtype': 'plain',
                    'from': 'erp.delmar@gmail.com',
                    'to': [r for r in to_recipients],
                    'subject': 'Password expiration {}',
                    'body': """
                        Attention!
                        Connection password for FTP Settings "{}"
                        {}
                        Please renew password!
                    """,
                }
                # timedelta
                try:
                    daysdiff = datetime.strptime(setting.password_renew_date, "%Y-%m-%d %H:%M:%S") - datetime.now()
                except:
                    daysdiff = datetime.strptime(setting.password_renew_date, "%Y-%m-%d %H:%M:%S.%f") - datetime.now()
                    pass
                # daydelta int
                daysdelta = int(daysdiff.total_seconds()/86400)
                back_hint = "would be expired in {} day(s)!".format(daysdelta)
                # notification levels
                if daysdelta <= 1:
                    if daysdelta < 0:
                        back_hint = "was expired days {} ago".format(str(-1 * daysdelta))
                    mail_message.update({
                        'to_send': True,
                        'subject': str(mail_message.get('subject')).format('ALERT'),
                    })
                elif daysdelta <= 5:
                    mail_message.update({
                        'to_send': True,
                        'subject': str(mail_message.get('subject')).format('notification'),
                    })
                # add body to message
                mail_message.update({
                    'body': str(mail_message.get('body')).format(setting.name, back_hint)
                })
                # add message to queue
                if mail_message.get('to_send', False):
                    mid = self.pool.get('mail.message').schedule_with_attach(
                        cr, uid,
                        email_from=mail_message.get('from'),
                        email_to=mail_message.get('to'),
                        subject=mail_message.get('subject'),
                        body=mail_message.get('body'),
                        subtype=mail_message.get('subtype'), context=None)

        return True

    def onchange_security(self, cr, uid, ids, security=None):
        result = {
            'value': {
            },
        }
        if(security):
            ftp_ssh = (security == 'ssh')
            ftp_ssl = (security == 'ssl')
            if(ftp_ssh):
                result['value'].update({
                    'ftp_ssl': False,
                    'ftp_ssh': True,
                    'ftp_port': 22,
                })
            elif(ftp_ssl):
                result['value'].update({
                    'ftp_ssl': True,
                    'ftp_ssh': False,
                    'ftp_port': 990,
                })
            else:
                result['value'].update({
                    'ftp_ssl': False,
                    'ftp_ssh': False,
                    'ftp_port': 21,
                })
        return result

ftp_settings()


class import_field_temp(osv.osv):
    _name = "import.field.temp"

    _columns = {
        'id_delmar': fields.char('Delmar Id', size=128),
        'size': fields.char('Size', size=128),
        'customer_ref': fields.char('Customer', size=128),
        'value': fields.char('Value', size=128),
    }

import_field_temp()


class import_settings(osv.osv):
    _name = "import.settings"

    def _verification_fields(self, cr, uid, ids, context=None):
        for settings in self.browse(cr, uid, ids):
            if settings.exclude_customer_ref and (not re.findall(r'^[\w,]+$', settings.exclude_customer_ref) or not re.findall(r'^[\w]+', settings.exclude_customer_ref) or not re.findall(r'[^,]$', settings.exclude_customer_ref) or re.findall(r'[,]{2}', settings.exclude_customer_ref)):
                return False

        return True

    _columns = {
        'filename': fields.char('Template filename', size=128, required=True, help="Filename from ftp"),
        'file_type': fields.selection(FILE_TYPES, 'Type', required=True),
        'ftp_setting_id': fields.many2one('ftp.settings', 'FTP Setting', required=True),
        "customer_id": fields.many2one('res.partner', 'Customer'),
        "customer_field_id": fields.many2one('product.multi.customer.fields.name', 'Customer Field Name', required=True),
        "customer_ref": fields.char('Ref', size=128),
        "exclude_customer_ref": fields.char('Exclude customer ref(s)', size=256, ),
        "exclude_tag_id": fields.many2one('tagging.tags', 'Exclude Tag',),
        'tpl_id_delmar': fields.integer('ID Delmar N', required=True, help="Delmar ID col № from xls"),
        'tpl_size': fields.integer('Size N', required=True, help="Size Col № from xls"),
        'tpl_customer': fields.integer('Customer N', required=True, help="Customer ref col № from xls"),
        'tpl_value': fields.integer('Value N', required=True, help="Customer field value Col № from xls"),
        'active_setting': fields.boolean('Active'),
        'tmp_path': fields.char('Path to tmp folder', size=128),
        'ftp_path': fields.char('Ftp Path', size=128),
        'path_to_import_file': fields.char('Path to import file', size=128),
        'last_modified_date': fields.datetime('Import file date'),
        'skip_first_row': fields.boolean('Skip First Line'),
    }

    _constraints = [(_verification_fields, 'String can contain alphanumeric symbols only, Customer Refs can be separated only by comma(s) without of any spaces', ['exclude_customer_ref'])]

    _defaults = {
        'tmp_path': '/tmp/',
        'ftp_path': '/',
        'skip_first_row': True,
    }

    def change_customer_id(self, cr, uid, ids, customer_id=None, context=None):
        domain = {'customer_field_id': []}
        result = {
            'customer_id': customer_id,
            'customer_field_id': False,
        }

        if customer_id:
            domain = {'customer_field_id': [('customer_id', '=', customer_id)]}
        return {'value': result, 'domain': domain}

    def test_cron(self, cr, uid, ids, context):
        method = getattr(self, 'main', None)
        method(cr, uid, ids=ids)
        return True

    def get_active_settings(self, cr, uid, ids):
        if ids:
            settings = self.search(cr, uid, [('id', 'in', ids)])
        else:
            settings = self.search(cr, uid, [('active_setting', '=', True)])
        if not settings:
            _logger.error('Settings not found')
            return False
        return settings

    def main(self, cr, uid, ids=None):
        settings = self.get_active_settings(cr, uid, ids)
        for setting_id in settings:
            cur_setting = self.browse(cr, uid, setting_id)
            path = self.get_data_from_ftp(cr, uid, cur_setting)
            if not path:
                _logger.error('method get_data_from_ftp return empty path')
                continue
            self.prepare_data(cr, uid, path, cur_setting)
            self.start_import(cr, uid, cur_setting)
        return True

    def get_data_from_ftp(self, cr, uid, settings):
        log = []
        now_date = datetime.now().date().strftime("%Y_%m_%d")
        path_to_file = ""
        if settings.path_to_import_file:
            path_to_file = settings.path_to_import_file
        ftp_settings = settings.ftp_setting_id
        try:
            ftp = FTP(ftp_settings.ftp_host, ftp_settings.ftp_user, ftp_settings.ftp_pass)
            ftp.set_debuglevel(2)
            ftp.cwd(settings.ftp_path)
            filenames = ftp.nlst()
            if not settings.tmp_path[-1] == '/':
                settings.tmp_path += '/'
            tmp_folder_path = settings.tmp_path + now_date
            if not os.path.exists(tmp_folder_path):
                os.makedirs(tmp_folder_path)
            for filename in filenames:
                if not filename.lower() == settings.filename.lower():
                    continue
                last_modified_date = datetime.strptime(ftp.sendcmd('MDTM %s' % filename)[4:], "%Y%m%d%H%M%S").strftime("%Y-%m-%d %H:%M:%S")
                if settings.last_modified_date == last_modified_date and os.path.exists(path_to_file):
                    log.append({
                        'title': 'get_data_from_ftp filename: %s' % (filename),
                        'msg': 'ftp host: %s \nfilename: %s \nlocal path to file: %s \nftp path to file: %s \n' %
                            (ftp_settings.ftp_host, filename, path_to_file, settings.ftp_path),
                        'customer': settings.customer_id.ref,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    continue
                IO = cStringIO.StringIO()
                ftp.set_pasv(True)
                path_to_file = tmp_folder_path + '/' + filename
                ftp.retrbinary('RETR ' + filename, IO.write)
                write_data = IO.getvalue()
                blf_utils.file_for_writing(tmp_folder_path, filename, str(write_data), 'wb').create()
                self.write(cr, uid, settings.id, {
                    'path_to_import_file': path_to_file,
                    'last_modified_date': last_modified_date
                })
                log.append({
                    'title': 'get_data_from_ftp filename: %s' % (filename),
                    'msg': 'ftp host: %s \nfilename: %s \nlocal path to file: %s \nftp path to file: %s \n' %
                        (ftp_settings.ftp_host, filename, path_to_file, settings.ftp_path),
                    'customer': settings.customer_id.ref,
                    'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                })
            try:
                ftp.quit()
            except EOFError:
                ftp.close()
        except Exception, e:
            log.append({
                'title': 'error method get_data_from_ftp',
                'msg': 'error method get_data_from_ftp: %s ' % (e),
                'customer': settings.customer_id.ref,
                'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            })
            _logger.error('error method get_data_from_ftp: %s ' % (e))
            self.log(cr, uid, log)
        if(len(log) > 0):
            self.log(cr, uid, log)
        return path_to_file

    def prepare_data(self, cr, uid, path, settings):
        log = []
        data = []
        exclude_customer_refs = []
        if settings.file_type == 'csv':
            data = self.csv_parser(path, settings)
        elif settings.file_type == 'xls':
            data = self.xls_parser(cr, uid, path, settings)

        if settings.exclude_customer_ref:
            exclude_customer_refs = [x.strip().upper() for x in settings.exclude_customer_ref.split(',') if x.strip()]

        cr.execute('TRUNCATE TABLE import_field_temp')
        cr.execute("""
            select ref
            from res_partner
            where ref is not null
                and ref <> ''
            group by ref
        """)
        customer_refs_res = cr.dictfetchall() or []
        customer_refs = [i['ref'] for i in customer_refs_res]
        customer_refs = list(set(customer_refs) - set(exclude_customer_refs))
        required = ['customer_ref', 'value', 'id_delmar']
        empty_rows = 0
        necr_skip_row = 0
        vals = []
        for item in data:
            """Skip not existing customer ref"""
            if (item['customer_ref'] not in customer_refs):
                necr_skip_row += 1
                continue
            """Skip empty row"""
            if not bool([i for i in required if item[i] != '']):
                empty_rows += 1
                continue
            vals += [item['customer_ref'], item['value'], item['id_delmar'], item['size']]
            if len(vals) / 4 > LIMIT_INSERT:
                self.fill_import_field_temp(cr, uid, vals)
                vals = []
        if len(vals) / 4 > 0:
            self.fill_import_field_temp(cr, uid, vals)
        log.append({
            'title': 'method prepare_data',
            'msg': 'count rows: %s \ncount skip empty row: %s \ncount skip not existing customer ref: %s \ncount import rows: %s' %
                (len(data), empty_rows, necr_skip_row, len(data) - (empty_rows + necr_skip_row)),
            'customer': settings.customer_id.ref,
            'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
        })
        self.log(cr, uid, log)
        return True

    def fill_import_field_temp(self, cr, uid, items):
        if len(items) > 0:
            inserts = "(%s, %s, %s, %s), " * (len(items) / 4)
            sql = "INSERT INTO import_field_temp (customer_ref, value, id_delmar, size) VALUES " + inserts[:-2]
            cr.execute(sql, items)

    def xls_parser(self, cr, uid, path, settings):
        log = []
        fi = True
        # formatting_info=True not yet implemented in xlsx
        if path[-1:] == 'x':
            fi = False
        rb = xlrd.open_workbook(path, formatting_info=fi)
        sheet = rb.sheet_by_index(0)

        ref = settings.customer_id.ref
        if settings.customer_ref:
            ref = settings.customer_ref
        idx = {
            'customer_ref': settings.tpl_customer,
            'id_delmar': settings.tpl_id_delmar,
            'size': settings.tpl_size,
            'value': settings.tpl_value,
        }

        data = []
        for rownum in range(sheet.nrows)[1:]:
            row = sheet.row_values(rownum)
            if ref and not row[idx.get('customer_ref')] == ref:
                continue
            data_row = {}
            data_row['id_delmar'] = type(row[idx.get('id_delmar')]) is unicode and row[idx.get('id_delmar')].encode('utf-8').strip() or str(row[idx.get('id_delmar')]).encode('utf-8').strip()
            data_row['size'] = row[idx.get('size')]
            if sheet.cell(rownum, idx.get('value')).ctype == xlrd.XL_CELL_DATE:
                try:
                    data_row['value'] = datetime(*xlrd.xldate_as_tuple(row[idx.get('value')], rb.datemode)).strftime("%m/%d/%Y")
                except XLDateAmbiguous as ex:
                    log.append({
                        'title': 'warning xls_parser',
                        'msg': 'warning xls_parser: bad value for product: {id_delmar}{size}\nError: {err}'.format(
                            id_delmar=data_row['id_delmar'],
                            size=('/{}'.format(data_row['size']) if data_row['size'] else ''),
                            err=str(ex)
                        ),
                        'customer': settings.customer_id.ref,
                        'create_date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
                    })
                    self.log(cr, uid, log)
                    data_row['value'] = ''
            elif (row[idx.get('value')] and str(row[idx.get('value')]) and str(row[idx.get('value')]).lower() == 'yes'):
                data_row['value'] = 1
            elif (row[idx.get('value')] and str(row[idx.get('value')]) and str(row[idx.get('value')]).lower() == 'no'):
                data_row['value'] = 0
            else:
                data_row['value'] = type(row[idx.get('value')]) is unicode and row[idx.get('value')].encode('utf-8').strip() or str(row[idx.get('value')]).encode('utf-8').strip()
            if settings.customer_id.ref:
                data_row['customer_ref'] = settings.customer_id.ref
            else:
                data_row['customer_ref'] = type(row[idx.get('customer_ref')]) is unicode and row[idx.get('customer_ref')].encode('utf-8').strip() or str(row[idx.get('customer_ref')]).encode('utf-8').strip()
            data.append(data_row)
        return data

    def csv_parser(self, path, settings):
        data = []
        with open(path, 'rb') as csvfile:
            obj = list(csv.reader(csvfile, delimiter=",", quotechar='"'))

            ref = settings.customer_id.ref
            if settings.customer_ref:
                ref = settings.customer_ref
            idx = {
                'customer_ref': settings.tpl_customer,
                'id_delmar': settings.tpl_id_delmar,
                'size': settings.tpl_size,
                'value': settings.tpl_value,
            }

            first_line = 1 if settings.skip_first_row else 0

            for row in obj[first_line:]:
                if ref and not row[idx.get('customer_ref')] == ref:
                    continue
                data_row = {}
                data_row['id_delmar'] = type(row[idx.get('id_delmar')]) is unicode and row[idx.get('id_delmar')].encode('utf-8').strip() or str(row[idx.get('id_delmar')]).encode('utf-8').strip()
                data_row['size'] = row[idx.get('size')]
                # if sheet.cell(rownum, idx.get('value')).ctype == xlrd.XL_CELL_DATE:
                #     data_row['value'] = datetime(*xlrd.xldate_as_tuple(row[idx.get('value')], rb.datemode)).strftime("%m/%d/%Y")
                if (row[idx.get('value')] and str(row[idx.get('value')]) and str(row[idx.get('value')]).lower() == 'yes'):
                    data_row['value'] = 1
                elif (row[idx.get('value')] and str(row[idx.get('value')]) and str(row[idx.get('value')]).lower() == 'no'):
                    data_row['value'] = 0
                else:
                    data_row['value'] = type(row[idx.get('value')]) is unicode and row[idx.get('value')].encode('utf-8').strip() or str(row[idx.get('value')]).encode('utf-8').strip()
                if settings.customer_id.ref:
                    data_row['customer_ref'] = settings.customer_id.ref
                else:
                    data_row['customer_ref'] = type(row[idx.get('customer_ref')]) is unicode and row[idx.get('customer_ref')].encode('utf-8').strip() or str(row[idx.get('customer_ref')]).encode('utf-8').strip()
                data.append(data_row)

        return data

    def start_import(self, cr, uid, settings):
        pmcfn_label = settings.customer_field_id.label
        exclude_tag_id = settings.exclude_tag_id.id or False

        if pmcfn_label in ('DNR', 'Force Zero'):
            sql_delete = """
                delete
                from product_multi_customer_fields f
                where f.field_name_id
                    in (select fn.id
                        from product_multi_customer_fields_name fn
                        left join res_partner p on fn.customer_id = p.id
                        where label = '%s'
                        and p.ref in (
                            select distinct customer_ref
                            from import_field_temp
                        )
                    )
            """ % (pmcfn_label)

            cr.execute(sql_delete)

        sub_query = """
            (
                SELECT
                    a.*,
                    ift."value" as "value"
                FROM (
                    SELECT
                        pp.id as product_id,
                        pp.default_code as product_code,
                        pmcfn.id as field_name_id,
                        rp.id as partner_id,
                        rp.ref as partner_ref,
                        case
                            when rs.id is null and pmcfn.expand_ring_sizes = true
                            then rel.size_id else rs.id
                        end as size_id,
                        COALESCE(max(rs.name), '') as size_in_file
                    FROM import_field_temp ift
                        inner JOIN product_product pp ON pp.default_code = ift.id_delmar
                        LEFT JOIN res_partner rp ON rp.ref = ift.customer_ref
                        LEFT JOIN product_multi_customer_fields_name pmcfn ON pmcfn.customer_id = rp.id
                        LEFT JOIN ring_size rs ON ift.size != '' and cast(case when ift."size" = '' then '0' else ift."size" end as float) = cast(rs.name as float)
                        LEFT JOIN product_ring_size_default_rel rel on ift.size = '' and rel.product_id = pp.product_tmpl_id
                    WHERE pp.type = 'product'
                        AND pmcfn.label = '{labe}'
                        {tag_condition}
                    GROUP BY
                        pp.id,
                        product_code,
                        partner_ref,
                        pmcfn.id,
                        rp.id,
                        case
                            when rs.id is null and pmcfn.expand_ring_sizes = true
                            then rel.size_id else rs.id
                        end
                ) a
                    INNER JOIN import_field_temp ift on (
                        ift.customer_ref = a.partner_ref
                        and ift.id_delmar = a.product_code
                        and replace(ift.size, '.0', '') = replace(size_in_file, '.0', '')
                    )
            ) as res
        """.format(
            labe=pmcfn_label,
            tag_condition="""AND NOT EXISTS (
                select tt.product_id
                from taggings_product tt
                where tt.tag_id = %s
                    and tt.product_id = pp.id
                )""" % (exclude_tag_id) if exclude_tag_id else ''
            )

        sql_update = """
            UPDATE
                product_multi_customer_fields as pmcf
            SET
                value = res.value,
                write_uid = %s,
                write_date = (now() at time zone 'UTC')
            FROM %s
            WHERE 1 = 1
                AND pmcf.product_id = res.product_id
                AND pmcf.field_name_id = res.field_name_id
                AND pmcf.partner_id = res.partner_id
                AND ((pmcf.size_id = res.size_id) OR ((pmcf.size_id is null) AND (res.size_id is null)))
        """ % (uid, sub_query)

        sql_insert = """
            INSERT INTO product_multi_customer_fields (
                create_uid,
                create_date,
                product_id,
                field_name_id,
                partner_id,
                size_id,
                value
            )
            SELECT
                %s as create_uid,
                (now() at time zone 'UTC') as create_date,
                res.product_id as product_id,
                res.field_name_id as field_name_id,
                res.partner_id as partner_id,
                res.size_id as size_id,
                res.value
            FROM %s
                LEFT JOIN product_multi_customer_fields pmcf on
                    pmcf.product_id = res.product_id
                    AND pmcf.field_name_id = res.field_name_id
                    AND pmcf.partner_id = res.partner_id
                    AND ((pmcf.size_id = res.size_id) OR ((pmcf.size_id is null) AND (res.size_id is null)))
            WHERE 1 = 1
                AND pmcf.id is null
        """ % (uid, sub_query)
        cr.execute(sql_update)
        cr.execute(sql_insert)
        return True

    def log(self, cr, uid, logs=None):
        if not logs:
            logs = []

        for log_line in logs:
            try:
                name = unicode(log_line['title'], errors='ignore')
            except:
                name = unicode(log_line['title'])
            try:
                tmp = unicode(log_line['msg'], errors='ignore')
            except:
                tmp = unicode(log_line['msg'])
            try:
                customer = unicode(log_line['customer'], errors='ignore')
            except:
                customer = unicode(log_line['customer'])
            self.pool.get('import.settings.log').create(cr, uid, {
                'name': name,
                'debug_information': tmp,
                'customer': customer,
                'date': log_line.get('create_date', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()))
            })

import_settings()


class import_settings_log(osv.osv):

    _name = "import.settings.log"

    _columns = {
        'name': fields.char('Message', size=512),
        'debug_information': fields.text('Debug information'),
        'user_id': fields.many2one('res.users', 'User'),
        'customer': fields.char('Customer', size=512),
        'date': fields.datetime('Creation Date', readonly=True, select=1),
        'settings_id': fields.many2one('import.settings', 'Import Settings'),
    }

    _order = "date DESC"

    _defaults = {
        'user_id': lambda self, cr, uid, ctx: uid
    }

import_settings_log()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
