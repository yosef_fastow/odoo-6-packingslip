# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP Module
#
#    Copyright (C) 1996-2012 Pokémon.
#    Author Yuri Ivanenko
#
##############################################################################
{
    "name": "Import fields",
    "version": "1.0",
    "depends": [
        "base",
        "base_tools",
        "multi_customer",
        "tagging_product",
        "export_template",
        "pf_utils",
        "fetchdb",
    ],
    "author": "Yuri Ivanenko, progforce.com",
    "category": "Tools",
    "description": """
    Module for importing customer fields from '.csv' files.
    """,
    "init_xml": [],
    'update_xml': [
        "view/import_fields.xml",
        "view/import_settings.xml",
        "wizard/import_fields_wizard.xml",
    ],
    'js': [
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'qweb': [
        "static/src/xml/*.xml",
    ],
}
