import unittest


class MappingFieldsException(Exception):
    pass


class XMLContainer(object):

    def __init__(self, data_from_xml):
        self.columns = data_from_xml['columns']
        self.rows = data_from_xml['rows']

    def get_lines_by_key(self, *args):
        """
        Get line value by tuples. The first element of tuple = column id,  the second = column name.

        :param args: list of tuples (0, 'ID Delmar'), (1, 'Size')
        :return:
        """

        if not len(args):
            raise Exception('Ned to add minimum 1 pare')
        for column_id, name in args:
            if self.columns[column_id] != name:
                raise Exception('Error keys: ({},{},)'.format(column_id, name))
        for row in self.rows:
            row_result = {}
            for key in args:
                row_result[key] = row[key[0]]
            yield row_result


class MappingContainer(object):

    def __init__(self, mapping_data, xml_data):
        self.xml_data = xml_data

        self.delmar_id_name = mapping_data.delmar_id_column.name
        self.delmar_id_column_number = mapping_data.delmar_id_column.column_number

        self.size_column_name = mapping_data.size_column.name
        self.size_column_column_number = mapping_data.size_column.column_number

        self.delmar_id_key = (self.delmar_id_column_number, self.delmar_id_name)
        self.size_column_key = (self.size_column_column_number, self.size_column_name)
        self.retail_price_key = None

        self.mapping_lines = mapping_data.columns_mapping_line_ids

    def ger_retail_price_from_xml(self):
        """
        Form tuple with ( 'delmar product id', 'retail price' )
        it need for update CustomerPrice table
        :return: set of tuples ( 'delmar product id', 'retail price' )
        """
        results = set()
        for mapping_line in self.mapping_lines:
            customer_field_name = mapping_line.field_name_id.name_id.name
            xml_column_number   = mapping_line.file_column_name_id.column_number
            xml_column_name     = mapping_line.file_column_name_id.name
            if customer_field_name == 'retail_price':
                self.retail_price_key = (xml_column_number, xml_column_name)

        if self.retail_price_key:
            xml_cont = XMLContainer(self.xml_data)
            lines = xml_cont.get_lines_by_key(self.delmar_id_key, self.retail_price_key)
            for line in lines:
                try:
                    delmar_id_and_retail_price = (line[self.delmar_id_key], float(line[self.retail_price_key]))
                    results.add(delmar_id_and_retail_price)
                except ValueError:
                    raise MappingFieldsException(
                        '"Retail Price" have not correct value. Product: {} - value {}'.format(
                            line[self.delmar_id_key], line[self.retail_price_key])
                    )

        else:
            raise MappingFieldsException('There are not field "Retail Price" in mapping fields')

        return results


class TestXMLContainer(unittest.TestCase):
    """
    Test xml
    """

    data = {'rows': [[u'FC1XMT-E27A', u'5', u'SAMS01556/5', u'359', u'980045757', u'0040980045757'],
                     [u'FC1XMT-E27A', u'5.5', u'SAMS01556/5.5', u'359', u'980045758', u'0040980045758'],
                     [u'FC1XMT-E27A', u'6', u'SAMS01556/6', u'359', u'980045759', u'0040980045759'],
                     [u'FC1XMT-E27A', u'6.5', u'SAMS01556/6.5', u'359', u'980045760', u'0040980045760'],
                     [u'FC1XMT-E27A', u'7', u'SAMS01556/7', u'359', u'980045761', u'0040980045761'],
                     [u'FC1XMT-E27A', u'7.5', u'SAMS01556/7.5', u'359', u'980045762', u'0040980045762'],
                     [u'FC1XMT-E27A', u'8', u'SAMS01556/8', u'359', u'980045763', u'0040980045763'],
                     [u'FC1XMT-E27A', u'8.5', u'SAMS01556/8.5', u'359', u'980045764', u'0040980045764'],
                     [u'FC1XMT-E27A', u'9', u'SAMS01556/9', u'359', u'980045766', u'0040980045766']],
            'columns': [u'ID Delmar', u'Size', u'Customer ID Delmar', u'Customer Price', u'Customer Sku', u'UPC']}

    def test_get_lines_by_key(self):
        cont = XMLContainer(self.data)
        lines = cont.get_lines_by_key((0, 'ID Delmar'), (1, 'Size'))

        expected_result = [
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'5'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'5.5'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'6'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'6.5'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'7'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'7.5'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'8'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'8.5'},
            {(0, 'ID Delmar'): u'FC1XMT-E27A', (1, 'Size'): u'9'}]

        self.assertEqual(list(lines), expected_result)

    def test_check_exception(self):
        cont = XMLContainer(self.data)

        with self.assertRaises(Exception):
            lines = cont.get_lines_by_key()
            lines.next()

        with self.assertRaises(Exception):
            lines = cont.get_lines_by_key((0, 'ID Delmar'), (1, 'Error'))
            lines.next()


if __name__ == '__main__':
    unittest.main()
