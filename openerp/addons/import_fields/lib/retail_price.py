from osv import fields, osv

class RetailPrice(object):

    def __init__(self, cursor, uid, pool):
        self.cursor = cursor
        self.uid = uid
        self.pool = pool

    def insert_or_update_retail_price(self, customer, data):
        """
        Update or insert retail price to the CustomerPrice table
        :return:
        """

        def get_sql_for_update(customer, delmar_id, price):
            price = "${:,}".format(round(float(price), 2))

            rez_sql = """

                IF exists(SELECT * FROM CustomerPrice WHERE Customer = '{customer}' AND DelmarID = '{delmar_id}')
                  BEGIN

                  WITH temp AS
                    (SELECT *
                     FROM (VALUES
                       ('{customer}', '{delmar_id}', '{price}')
                          ) AS t(Customer, DelmarId, Price )
                    )
                    INSERT INTO CustomerPriceLog (Customer, DelmarID, DateNew, Description, PriceNew, PriceOld, DateOld)
                      SELECT
                        t.Customer,
                        t.DelmarID,
                        CONVERT(VARCHAR(19), GETDATE(), 120) AS Date,
                        concat('price is changed from ', cp.Price, '(', cp.Date, ') to ', t.Price, '(',
                               CONVERT(VARCHAR(19), GETDATE(), 120), ')'),
                        t.Price,
                        cp.Price,
                        cp.Date
                      FROM temp t
                    INNER JOIN CustomerPrice cp ON t.DelmarID = cp.DelmarID AND t.Customer = cp.Customer;

                    UPDATE CustomerPrice
                    SET Price = '{price}', Date = GETDATE()
                    WHERE Customer = '{customer}' AND DelmarID = '{delmar_id}';
                  END
                ELSE
                  BEGIN
                    INSERT INTO CustomerPrice (Customer, DelmarID, Price, Date)
                    VALUES ('{customer}', '{delmar_id}', '{price}', GETDATE());
                END;

                """.format(customer=customer, delmar_id=delmar_id, price=price)
            return rez_sql

        # check that product exist.
        list_of_delmar_id = []
        data_values = []
        for item in data:
            list_of_delmar_id.append("'{}'".format(item[0]))
            data_values.append("('{}', '{}' )".format(item[0], item[1]))

        sql = """
        WITH datas (id_delmar, price) AS ( VALUES
          {values}
        )
        SELECT id_delmar, price FROM datas
          RIGHT JOIN
              (SELECT "product_product"."default_code" FROM "product_template"
                INNER JOIN "product_product" ON product_template.id = product_product.product_tmpl_id
                WHERE ((product_product."active" = 'True') 
                AND  ((((product_template."id_delmar" in ({delmar_ids}))  
                OR  (product_product."web_id" in ({delmar_ids})))  
                OR  (product_product."default_code" in ({delmar_ids})))  
                ))) AS exist_product ON datas.id_delmar = exist_product.default_code
        """.format(values=','.join(data_values), delmar_ids=','.join(list_of_delmar_id))

        self.cursor.execute(sql)
        data = self.cursor.fetchall()

        sql_list = ["BEGIN TRAN"]
        for item in data:
            sql_list.append(get_sql_for_update(customer=customer, delmar_id=item[0], price=item[1]))
        sql_list.append("COMMIT TRAN")
        sql = '\n'.join(sql_list)

        server_data = self.pool.get('fetchdb.server')
        server_id = server_data.get_price_servers(self.cursor, self.uid, single=True)
        result = server_data.make_query(self.cursor, self.uid, server_id, sql, select=False, commit=True)
        return result
