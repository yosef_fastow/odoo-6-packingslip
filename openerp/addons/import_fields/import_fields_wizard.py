# -*- coding: utf-8 -*-

##############################################################################
#
#    Progforce
#    Copyright (C) 2014
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlrd
import csv
from osv import fields, osv
import base64
from datetime import datetime
import logging
import copy
from openerp.addons.import_fields.lib.import_xml import MappingContainer, MappingFieldsException
from openerp.addons.import_fields.lib.retail_price import RetailPrice
import time

_logger = logging.getLogger(__name__)

FILE_TYPES = [
    ('csv', 'Csv'),
    ('xls', 'Xls'),
]


class import_fields_prepare(osv.osv_memory):
    _name = "import.fields.prepare"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'customer_id': fields.many2one('res.partner', 'Customer', ondelete='cascade', domain="[('customer', '=', True)]"),
        'importing_file': fields.binary(string="Importing file"),
        'importing_file_type': fields.selection(
            [
                ('inventory_fields', 'inventory fields'),
                ('price_fields', 'price fields')
            ], 'Type of importing file', required=True,
            help="Collects customer fields in category that presented list of columns of particular file type"),
        'column_name_ids': fields.one2many('import.fields.column.name', 'import_fields_prepare_id', "Columns", help="File's columns"),
        'add_products_to_tag': fields.boolean("Auto adding product list to the customer's inventory tag"),
        'update_retail_price': fields.boolean("Add/Update Retail Prices"),
        'expand_by_size': fields.boolean("Expand by size"),
        'existed_only_update': fields.boolean("Update existed sizes only"),
        'base_line_update': fields.boolean("Update baseline record"),
        'half_size_update': fields.boolean("Include half-sizes update"),
        'size_min': fields.char('Size from', size=16, required=False, ),
        'size_max': fields.char('Size to', size=16, required=False, ),
    }

    _defaults = {
        'importing_file_type': 'inventory_fields',
        'add_products_to_tag': False,
        'expand_by_size': False,
        'existed_only_update': True,
        'base_line_update': True,
        'half_size_update': True,
    }

    def cellval(self, cell, datemode):
        if cell.ctype == xlrd.XL_CELL_DATE:
            datetuple = xlrd.xldate_as_tuple(cell.value, datemode)
            if datetuple[3:] == (0, 0, 0):
                return datetime.date(datetuple[0], datetuple[1], datetuple[2])
            return datetime.date(datetuple[0], datetuple[1], datetuple[2], datetuple[3], datetuple[4], datetuple[5])
        if cell.ctype == xlrd.XL_CELL_EMPTY:
            return None
        if cell.ctype == xlrd.XL_CELL_BOOLEAN:
            return cell.value == 1
        if cell.ctype == xlrd.XL_CELL_NUMBER and (cell.value).is_integer():
            return int(cell.value)
        return cell.value

    def process_xls_file(self, cr, uid, import_file, context=None):
        data = {
            'columns': [],
            'rows': [],
        }

        book = xlrd.open_workbook(file_contents=import_file)
        sheet = book.sheet_by_index(0)

        if sheet.nrows > 0:
            row_index = 0
            for col_index in range(sheet.ncols):
                cur_cell = sheet.cell(row_index, col_index)
                data['columns'].append(self.cellval(cur_cell, book.datemode))

        for row_index in range(1, sheet.nrows):
            row = []
            for col_index in range(sheet.ncols):
                cur_cell = sheet.cell(row_index, col_index)
                cell_val = self.cellval(cur_cell, book.datemode)
                # Prevent converting empty cell to u'None'
                if cell_val is None:
                    cell_val = ''
                row.append(unicode(cell_val).strip())
            data['rows'].append(row)

        return data

    def process_csv_file(self, cr, uid, import_file, context=None):
        data = {
            'columns': [],
            'rows': [],
        }

        import_rows = [x for x in import_file.replace('\r\n', '\n').replace('\r', '\n').split('\n') if (x and x.strip())]

        if len(import_rows) > 0:
            data['columns'] = list(csv.reader([import_rows[0]]))[0]
            data['rows'] = list(csv.reader(import_rows[1:]))

        return data

    def exctact_data_from_file(self, cr, uid, import_file, context=None):
        data = {}

        try:
            data = self.process_xls_file(cr, uid, import_file, context=context)
        except:
            try:
                data = self.process_csv_file(cr, uid, import_file, context=context)
            except:
                raise osv.except_osv('WRONG File Format! Importing file should has XLS or CSV format.', '')
                return False

        if not data:
            raise osv.except_osv('WRONG File! Can not process file.', '')
            return False

        if not data['columns']:
            raise osv.except_osv('Header row is required', '')
            return False

        if (len(data['rows']) < 1):
            raise osv.except_osv('Importing file should contain more the one row besides header (header row is required)', '')
            return False

        return data

    def open_columns_mapping_form(self, cr, uid, ids, context=None):
        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        column_obj = self.pool.get('import.fields.column.name')
        mapping_obj = self.pool.get('import.fields.columns.mapping.line')
        customer_fields_obj = self.pool.get('product.multi.customer.fields.name')
        file_label_obj = self.pool.get('product.multi.customer.file.label')

        prepare_data_id = False
        default_size_column = False
        default_delmar_id_column = False

        if ids:
            prepare_data_id = ids[0]
        else:
            raise osv.except_osv('Cannot find instance for the first step', '')
            return {'type': 'ir.actions.act_window_close'}

        prepare_data = self.browse(cr, uid, prepare_data_id, context=context)

        if not prepare_data.importing_file:
            raise osv.except_osv('Please select the import file', '')

        import_file = base64.decodestring(prepare_data.importing_file)

        data = self.exctact_data_from_file(cr, uid, import_file, context=context)

        mapping_line_ids = []
        col_dict = {}

        column_number = 0
        for col in data['columns']:
            col_name = str(col).strip()

            file_column_name_id = column_obj.create(cr, uid, {
                'name': col_name,
                'column_number': column_number,
                'import_fields_prepare_id': prepare_data_id
            })

            col_dict.update({file_column_name_id: col_name.lower()})

            column_number += 1

            if col_name.lower().replace(' ', '').replace('_', '') in ['delmarid', 'iddelmar']:
                default_delmar_id_column = file_column_name_id

            if col_name.lower().replace(' ', '').replace('_', '') in ['size']:
                default_size_column = file_column_name_id

        # search Customer Fields for chosen Customer and Importing File Type
        file_label_ids = file_label_obj.search(
            cr, uid, [('file_type', '=', prepare_data.importing_file_type)], context=context)
        file_labels = file_label_obj.browse(cr, uid, file_label_ids, context=context)
        for label in file_labels:
            cust_field_ids = customer_fields_obj.search(cr, uid, [
                ('customer_id', '=', prepare_data.customer_id and prepare_data.customer_id.id or None),
                ('id', 'in', [x.id for x in label.field_name_ids])
            ])

            if cust_field_ids:
                cust_field_id = cust_field_ids[0]

                try:
                    lower_label = label.label_in_file.lower()
                    file_column_name_id = col_dict.keys()[col_dict.values().index(lower_label)]
                except:
                    file_column_name_id = False

                mapping_line_id = mapping_obj.create(cr, uid, {
                    'prepare_id': prepare_data_id,
                    'field_name_id': cust_field_id,
                    'file_column_name_id': file_column_name_id,
                    'required': label.required,
                    'fill_value': label.fill_value,
                })

                mapping_line_ids.append([4, mapping_line_id, False])

        new_id = self.pool.get('import.fields.columns.mapping').create(
            cr, uid,
            {
                'columns_mapping_line_ids': mapping_line_ids,
                'delmar_id_column': default_delmar_id_column,
                'size_column': default_size_column,
                'prepare_step_id': prepare_data_id,
            }
        )

        # find form for the next step
        act_win_res_id = data_obj._get_id(cr, uid, 'import_fields', 'action_view_import_fields_column_mapping')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)

        act_win.update({
            'res_id': new_id,
        })

        return act_win

import_fields_prepare()


class import_fields_columns_mapping(osv.osv_memory):
    _name = "import.fields.columns.mapping"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'prepare_step_id': fields.many2one('import.fields.prepare', 'Prepare Step of Wizard', ondelete='cascade'),
        'delmar_id_column': fields.many2one('import.fields.column.name', 'Delmar ID', ondelete='cascade', help="Name of column that contains Delmar ID values", required=False),
        'size_column': fields.many2one('import.fields.column.name', 'Size', ondelete='cascade', help="Name of column that contains Size values"),
        'columns_mapping_line_ids': fields.one2many('import.fields.columns.mapping.line', 'column_mapping_wizard_id', "Mapping", help="File's columns Mapping on Customer fields"),
    }

    def import_file(self, cr, uid, ids, context=None):
        import_obj = self.pool.get('import.process')
        prepare_obj = self.pool.get('import.fields.prepare')
        column_obj = self.pool.get('import.fields.column.name')
        field_name_obj = self.pool.get('product.multi.customer.fields.name')
        name_obj = self.pool.get('product.multi.customer.names')
        size_obj = self.pool.get('ring.size')

        if not ids:
            raise osv.except_osv('Can not process import', '')
            return False

        cur_id = ids[0]
        id_idx = None
        size_idx = None
        size_field = None
        default_code_field = None

        mapping_data = self.browse(cr, uid, cur_id, context=context)

        # check if column for id delmar is selected
        if not mapping_data.delmar_id_column:
            raise osv.except_osv('Please select column that contains ID Delmar values', '')
            return False

        prepare_data = prepare_obj.browse(cr, uid, mapping_data.prepare_step_id.id, context=context)

        import_file = base64.decodestring(prepare_data.importing_file)
        data = prepare_obj.exctact_data_from_file(cr, uid, import_file, context=context)

        columns_ids = column_obj.search(
            cr, uid, [('import_fields_prepare_id', '=', mapping_data.prepare_step_id.id)], context=context)
        columns_count = len(columns_ids)

        default_code_field_ids = field_name_obj.search(cr, uid, [('label', '=', 'default_code')])
        if default_code_field_ids:
            default_code_field_id = default_code_field_ids[0]
            default_code_field = field_name_obj.browse(cr, uid, default_code_field_id)
        if not default_code_field:
            raise osv.except_osv('Field for identefication is not found (with label = "default_code")')
            return False

        name_ids = name_obj.search(cr, uid, [('label', '=', 'Ring sizes')])
        if name_ids:
            name_id = name_ids[0]
            size_field_ids = field_name_obj.search(cr, uid, [
                ('name_id', '=', name_id),
                ('customer_id', '=', prepare_data.customer_id.id)
            ])

            if size_field_ids:
                size_field_id = size_field_ids[0]
                size_field = field_name_obj.browse(cr, uid, size_field_id)

        if not size_field and mapping_data.size_column:
            raise osv.except_osv('Need to create Ring Sizes field for chosen customer', '')
            return False

        needed_column_numbers = []
        fields_ids = []
        cols_with_def_val = []
        order = []

        mapping_fields = []

        for i in range(columns_count):
            if mapping_data.delmar_id_column and mapping_data.delmar_id_column.column_number == i:
                id_idx = len(order)
                order.append(default_code_field.id)
                needed_column_numbers.append({
                    'field_id': default_code_field.id,
                    'position': i,
                })

            if mapping_data.size_column and mapping_data.size_column.column_number == i:
                size_idx = len(order)
                order.append(size_field.id)
                needed_column_numbers.append({
                    'field_id': size_field.id,
                    'position': i,
                })

            for col in mapping_data.columns_mapping_line_ids:
                # check required fields
                if col.required and not col.file_column_name_id:
                    raise osv.except_osv('Required Column Name for the mandatory Customer Field "%s"' % col.field_name_id.label, '')
                    return False

                if (col.file_column_name_id and col.file_column_name_id.column_number == i):
                    needed_column_numbers.append({
                        'field_id': col.field_name_id.id,
                        'position': col.file_column_name_id.column_number,
                    })

                    order.append(col.field_name_id.id)
                    fields_ids.append(col.field_name_id.id)
                    mapping_fields.append((col.field_name_id.label, col.file_column_name_id.name))

        # save fill value and order of Customer Fields that were not mapped on file's columns

        fillable_fields = []

        for col in mapping_data.columns_mapping_line_ids:
            if not col.file_column_name_id and col.fill_value:
                cols_with_def_val.append(col.fill_value)
                order.append(col.field_name_id.id)
                fields_ids.append(col.field_name_id.id)
                fillable_fields.append((col.field_name_id.label, col.fill_value))

        import_rows = []
        for row in data['rows']:
            import_row = []
            for needed_col in needed_column_numbers:
                import_row.append(row[needed_col['position']])

            # put values for Customer Fields that were not mapped on file's columns in the inport row
            for def_val in cols_with_def_val:
                import_row.append(def_val)

            import_rows.append(import_row)

        # DLMR-209
        # check is needed to expand items by size
        if prepare_data.expand_by_size:
            _logger.info("Expand by size requested")
            _logger.info("SIZE_IDX: %s" % size_idx)
            # Step 1. Check importing products for sizeablity
            products_to_expand = []
            existed_sizes = {}
            expand_templates = {}
            _logger.info("Going to expand %s row(s): %s" % (len(import_rows), import_rows))
            row_num_idx = 0
            row_num_to_drop = []

            # Pre-cache product ids
            cached_products = {}
            delmar_ids = []
            for row in import_rows:
                delmar_ids.append(row[0])
            product_list_ids = self.pool.get('product.product').search(cr, uid, [('default_code', 'in', delmar_ids), ('type', '=', 'product')])
            product_list = self.pool.get('product.product').browse(cr, uid, product_list_ids)
            for prod in product_list:
                cached_products.update({prod.default_code: prod.id})

            # run main
            for row in import_rows:
                # get product id from cached dict
                product_id = cached_products.get(row[0], None)
                if product_id:
                    product = self.pool.get('product.product').browse(cr, uid, product_id)
                    product_sizeable = product.sizeable or product.ring_size_ids or False
                    # if product is sizeable, add to list to check sizes
                    if product_sizeable:
                        _logger.info("Product %s defined as sizeable" % product.default_code)
                        products_to_expand.append(product)
                        # append row template if not exist
                        if product.default_code not in expand_templates:
                            expand_templates.update({product.default_code: copy.deepcopy(row)})
                        # append sizes existed in file
                        if size_idx:
                            _logger.info("Product is sizeable")
                            if product.default_code not in existed_sizes:
                                existed_sizes.update({product.default_code: []})
                            setted_sizes = existed_sizes.get(product.default_code)
                            setted_sizes.append(row[size_idx])
                            existed_sizes.update({product.default_code: setted_sizes})
                        else:
                            # remove row
                            # import_rows.remove(row)
                            row_num_to_drop.append(row_num_idx)
                    else:
                        _logger.warn("Product %s is not sizeable, skipping" % product.default_code)
                else:
                    _logger.warn("Product wasn't defined by default_code %s" % row[0])

                row_num_idx += 1

            # Drop not needed rows
            for k in reversed(row_num_to_drop):
                del import_rows[k]

            # _logger.info("PRODUCTS TO EXPAND: %s" % products_to_expand)
            # _logger.info("EXISTED SIZES: %s" % existed_sizes)
            # _logger.info("EXPAND TEMPLATES: %s" % expand_templates)

            # Step 2. Check size_idx or create it if not exist
            if not size_idx and len(products_to_expand) > 0:
                size_idx = len(order)
                order.append(size_field.id)
                # update all existed rows to add
                for row in import_rows:
                    row.append('')

            # Step 3. Check collected sizeable products
            for product in products_to_expand:
                _logger.warn("Trying to expand product %s" % product.default_code)
                tpl = expand_templates.get(product.default_code)

                # getting sizes list dependent on prepare data
                if prepare_data.existed_only_update:
                    # just get mapped sizes
                    size_list = product.ring_size_ids
                else:
                    # query for all sizes list by *.0 and *.5 masks
                    size_list = size_obj.browse(
                        cr,
                        uid,
                        size_obj.search(cr, uid, ['|', ('name', 'like', '%.0'), ('name', 'like', '%.5')])
                    )

                # check if base line requested
                if prepare_data.base_line_update:
                    tpl_row = copy.deepcopy(tpl)
                    if len(tpl_row) > size_idx:
                        tpl_row[size_idx] = ''
                    else:
                        tpl_row.append('')
                    import_rows.append(tpl_row)

                # iterating for sizes
                for size in size_list:
                    # check is fractional denied - if no half-size requested
                    if (not prepare_data.half_size_update) and (not float(size.name).is_integer()):
                        continue
                    # check if in size range
                    if (prepare_data.size_min and float(size.name) < float(prepare_data.size_min)) or (prepare_data.size_max and float(size.name) > float(prepare_data.size_max)):
                        continue
                    # check is not in list already
                    if size.name not in existed_sizes.get(product.default_code, []):
                        tpl_row = copy.deepcopy(tpl)
                        if len(tpl_row) > size_idx:
                            tpl_row[size_idx] = size.name
                        else:
                            tpl_row.append(size.name)
                        # append template to import_rows
                        import_rows.append(tpl_row)

        # END DLMR-209

        params = {
            'row_len': len(order),
            'id_idx': id_idx,
            'size_idx': size_idx,
            'size_field': size_field,
            'template_id': None,
            'fields_ids': fields_ids,
            'default_fields_ids': [],
            'order': order,
        }

        product_list = import_obj.process_import_lines(cr, uid, import_rows, params)

        if prepare_data.update_retail_price:
            try:
                mapping_container = MappingContainer(mapping_data, data)
                retail_prices = mapping_container.ger_retail_price_from_xml()
                ret_p_object = RetailPrice(cr, uid, self.pool)

                ret_p_object.insert_or_update_retail_price(
                    data=retail_prices, customer=prepare_data.customer_id.ref)

            except (MappingFieldsException,) as ex:
                raise osv.except_osv(ex.args[0], '')

        # add product list into the customer's inventory tag
        if prepare_data.add_products_to_tag:
            sale_integrations = prepare_data.customer_id.type_api_ids

            if sale_integrations:
                cur_sl_int = False
                cur_inv_tag = False
                for sl_int in sale_integrations:
                    if sl_int.si_type == 'main':
                        cur_sl_int = sl_int
                        break

                if not cur_sl_int:
                    cur_sl_int = sale_integrations[0]

                if cur_sl_int:
                    cur_inv_tag = cur_sl_int.tag

                if cur_inv_tag:
                    user_name = self.pool.get('res.users').browse(cr, uid, uid).name
                    for prod_id in product_list and product_list[0] or []:
                        cr.execute("""
                            insert into taggings_product
                                (product_id, tag_id)
                            select %s, %s
                                from (
                                    select tag_id, product_id
                                    from tagging_tags tg
                                    left join taggings_product tp on
                                        tp.tag_id = tg.id and
                                        tp.product_id = %s
                                    where 1=1
                                    and tg.id = %s
                                ) a
                                where a.tag_id is null
                        """ % (prod_id, cur_inv_tag.id, prod_id, cur_inv_tag.id,))

                        # record product tag history
                        cr.execute("""
                            insert into product_tag_update_history
                            (res_id, user_responsible, update_date, product_id, description)
                            VALUES(%s, %s, %s, %s, %s)""", (cur_inv_tag.id, user_name, time.strftime("%Y-%m-%d %H:%M:%S"), prod_id, 'Item has been added in tag.',))

        result_message = 'Import Fields for %s\nwith next mapping:\n' % (prepare_data.customer_id.name)

        for mapping_field in mapping_fields:
            result_message += '\t%s - %s\n' % (mapping_field[0], mapping_field[1])

        if fillable_fields:
            result_message += '\nand with filling:\n'

        for fillable_field in fillable_fields:
            result_message += '\t%s - %s\n' % (fillable_field[0], fillable_field[1])

        data_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        new_id = self.pool.get('import.fields.result').create(
            cr, uid,
            {
                'result_message': result_message,
            }
        )

        # find form for the next step
        act_win_res_id = data_obj._get_id(cr, uid, 'import_fields', 'action_view_import_fields_result')
        act_win_id = data_obj.read(cr, uid, act_win_res_id, ['res_id'])
        act_win = act_obj.read(cr, uid, act_win_id['res_id'], [], context=context)

        act_win.update({
            'res_id': new_id,
        })

        return act_win

import_fields_columns_mapping()


class import_fields_column_name(osv.osv_memory):
    _name = "import.fields.column.name"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        reads = self.read(cr, uid, ids, ['name'], context=context)
        res = []
        for record in reads:
            name = record['name']
            res.append((record['id'], name))
        return res

    _columns = {
        'name': fields.char('Name', size=256),
        'column_number': fields.integer('Column Number'),
        'import_fields_prepare_id': fields.many2one('import.fields.prepare', 'Prepare Step of Wizard', ondelete='cascade'),
    }

import_fields_column_name()


class import_fields_columns_mapping_line(osv.osv_memory):
    _name = "import.fields.columns.mapping.line"

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'prepare_id': fields.many2one('import.fields.prepare', 'Prepare Step of Wizard', ondelete='cascade'),
        'column_mapping_wizard_id': fields.many2one('import.fields.columns.mapping', 'Columns Mapping Wizard', ondelete='cascade'),
        'field_name_id': fields.many2one('product.multi.customer.fields.name', 'Customer Field Name', ondelete='cascade'),
        'file_column_name_id': fields.many2one('import.fields.column.name', 'Column Name', ondelete='cascade', help="Name of column that contains Customer Field values"),
        'required': fields.boolean('Required'),
        'fill_value': fields.char('Fill value', size=512, help="Value to fill Customer Field if colum of import file is not specified"),
    }

import_fields_columns_mapping_line()


class import_fields_result(osv.osv_memory):
    _name = 'import.fields.result'

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        res = []
        for cur_id in ids:
            name = str(cur_id)
            res.append((cur_id, name))
        return res

    _columns = {
        'result_message': fields.text('Result message', readonly=True),
    }

import_fields_result()
