# -*- encoding: latin-1 -*-
##############################################################################
#
# Copyright (c) 2009 �ngel �lvarez - NaN  (http://www.nan-tic.com) All Rights Reserved.
#
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import math
from osv import fields, osv


class product_pack(osv.osv):


    # def _get_prod_params (self, cr, uid, ids, name, args, context=None):

    #     print "\n\n Get Params %s" % (name)
    #     result = {}

    #     for obj_id in ids:
    #         product_obj = self.browse(cr, uid, obj_id)
    #         result[obj_id] = product_obj[name]

    #     return result


    _name = 'product.pack.line'
    _rec_name = 'product_id'


    def _get_type(self, cr, uid, ids, name, args, context=None):

        default_type = 'undefined'

        res = {}
        for obj_id in ids:
            categ = self.browse(cr, uid, obj_id).categ_id
            if categ:
                categ_obj = self.pool.get('product.category')
                res[obj_id] = categ_obj.get_prod_type(cr,uid,categ.id)
            else:
                res[obj_id] = default_type

        return res

    def _get_full_cost(self, cr, uid, ids, field_name, arg, context=None):
        res = {}

        for line in self.browse(cr, uid, ids, context=context):
            total = line.cost * line.quantity
            res[line.id] = total
        return res


    _columns = {
        'parent_product_id': fields.many2one('product.product', 'Parent Product', ondelete='cascade', required=True),
        'quantity': fields.float('Quantity', required=True),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'categ_id': fields.related('product_id', 'categ_id', type='many2one', relation='product.category', string="Product type", store=False),
        'attributes': fields.related('product_id', 'attributes', type='text', string="Attributes", store=False),
        'standard_price': fields.related('product_id', 'standard_price', type='float', string="Price", store=False),

        'prod_type' : fields.function(_get_type, method=True, type="char"),

        'partner_id': fields.char('Supplier', size=256),
        # 'partner_id': fields.many2one('res.partner', 'Supplier', required=True, change_default=True, select=True),
        'cost': fields.float('Cost'),
        'total': fields.float('Total cost'),

        'cmp_weight' : fields.related('product_id', 'cmp_weight', string="Weight", type="float", store=False),

        # diamond
        'dmd_category_id' : fields.related('product_id', 'dmd_category_id', string="Category", relation="diamond.category", type="many2one", store=False),
        'dmd_shape_id' : fields.related('product_id', 'dmd_shape_id', string="Shape", relation="diamond.shape", type="many2one", store=False),
        'dmd_finish_id' : fields.related('product_id', 'dmd_finish_id', string="Finish", relation="diamond.finish", type="many2one", store=False),
        'dmd_dimension_min' : fields.related('product_id', 'dmd_dimension_min', string="Dimension min", type="float", store=False),
        'dmd_dimension_max' : fields.related('product_id', 'dmd_dimension_max', string="Dimension max", type="float", store=False),
        'dmd_sieve_min_id' : fields.related('product_id', 'dmd_sieve_min_id', string="Sieve min", relation="diamond.sieve", type="many2one", store=False),
        'dmd_stone_fraction' : fields.related('product_id', 'dmd_stone_fraction', string="Stone fraction", relation="diamond.stone_fraction", type="many2one", store=False),
        'dmd_sieve_max_id' : fields.related('product_id', 'dmd_sieve_max_id', string="Sieve max", relation="diamond.sieve", type="many2one", store=False),
        'dmd_weight' : fields.related('product_id', 'dmd_weight', string="Weight", type="float", store=False),
        'dmd_total_weight' : fields.related('product_id', 'dmd_total_weight', string="Total Weight", type="float", store=False),
        'dmd_carat_min' : fields.related('product_id', 'dmd_carat_min', string="Carat min", type="float", store=False),
        'dmd_carat_max' : fields.related('product_id', 'dmd_carat_max', string="Carat max", type="float", store=False),
        'dmd_color_id' : fields.related('product_id', 'dmd_color_id', string="Color", relation="diamond.color", type="many2one", store=False),
        'dmd_color_categ_id' : fields.related('product_id', 'dmd_color_categ_id', string="Color category", relation="diamond.color_category", type="many2one", store=False),
        'dmd_material' : fields.related('product_id', 'dmd_material', string="Material", type="char", store=False),
        'dmd_clarity_id' : fields.related('product_id', 'dmd_clarity_id', string="Clarity", relation="diamond.clarity", type="many2one", store=False),
        'dmd_internal_code_id' : fields.related('product_id', 'dmd_internal_code_id', string="Internal code", relation="diamond.internal_code", type="many2one", store=False),
        'dmd_note_id' : fields.related('product_id', 'dmd_note_id', string="Note", relation="diamond.note", type="many2one", store=False),
        'dmd_est_price' : fields.related('product_id', 'dmd_est_price', string="Est Price", type="char", store=False),
        'dmd_certified' : fields.related('product_id', 'dmd_certified', string="Certified", type="boolean", store=False),
        'dmd_decoration_setting_type_id' : fields.related('product_id', 'dmd_decoration_setting_type_id', string="Decoration", relation="decoration.setting", type="many2one", store=False),

        # gemstone
        'gemst_sku' : fields.related('product_id', 'gemst_sku', string="SKU", type="char", store=False),
        'gemst_length' : fields.related('product_id', 'gemst_length', string="Length", type="float", store=False),
        'gemst_width' : fields.related('product_id', 'gemst_width', string="Width", type="float", store=False),
        'gemst_shape_id' : fields.related('product_id', 'gemst_shape_id', string="Shape", type="many2one", store=False, relation="gemstone.shape"),
        'gemst_type_id' : fields.related('product_id', 'gemst_type_id', string="Type", type="many2one", store=False, relation="gemstone.type"),
        'gemst_weight' : fields.related('product_id', 'gemst_weight', string="Weight", type="float", store=False),
        'gemst_total_weight' : fields.related('product_id', 'gemst_total_weight', string="Total Weight", type="float", store=False),
        'gemst_avg_weight' : fields.related('product_id', 'gemst_avg_weight', string="Avg weight", type="float", store=False),
        'gemst_quality' : fields.related('product_id', 'gemst_quality', string="Quality", type="char", store=False),
        'gemst_local_price' : fields.related('product_id', 'gemst_local_price', string="Local price", type="float", store=False),
        'gemst_local_currency_id' : fields.related('product_id', 'gemst_local_currency_id', string="Local currency", type="many2one", store=False, relation="res.currency"),
        'gemst_price_per' : fields.related('product_id', 'gemst_price_per', string="Price per", type="char", store=False),
        'gemst_decoration_setting_type_id' : fields.related('product_id', 'gemst_decoration_setting_type_id', string="Decoration setting type", type="many2one", store=False, relation="decoration.setting"),

        # finding
        'finding_id' : fields.related('product_id', 'finding_id', string="Finding", type="many2one", store=False, relation="finding.finding"),
        'finding_category_id' : fields.related('product_id', 'finding_category_id', string="Finding category", type="many2one", store=False, relation="finding.category"),
        'finding_group_id' : fields.related('product_id', 'finding_group_id', string="Finding group", type="many2one", store=False, relation="finding.group"),
        'finding_attachment_type_id' : fields.related('product_id', 'finding_attachment_type_id', string="Finding attachment type", type="many2one", store=False, relation="finding.attachment_type"),

        # pearl
        'pearl_type_id' : fields.related('product_id', 'pearl_type_id', string="Type", type="many2one", store=False, relation="pearl.type"),
        'pearl_surface_id' : fields.related('product_id', 'pearl_surface_id', string="Surface", type="many2one", store=False, relation="pearl.surface"),
        'pearl_luster' : fields.related('product_id', 'pearl_luster', string="Luster", type="char", store=False),
        'pearl_shape_id' : fields.related('product_id', 'pearl_shape_id', string="Shape", type="many2one", store=False, relation="pearl.shape"),
        'pearl_color_id' : fields.related('product_id', 'pearl_color_id', string="Color", type="many2one", store=False, relation="pearl.color"),
        'pearl_quality' : fields.related('product_id', 'pearl_quality', string="Quality", type="char", store=False),
        'pearl_class' : fields.related('product_id', 'pearl_class', string="Class", type="char", store=False),
        'pearl_size_type' : fields.related('product_id', 'pearl_size_type', string="Size type", type="char", store=False),
        'pearl_size_single_id' : fields.related('product_id', 'pearl_size_single_id', string="Single size", type="many2one", store=False, relation="pearl.size_single"),
        'pearl_size_strand_id' : fields.related('product_id', 'pearl_size_strand_id', string="Strand size", type="many2one", store=False, relation="pearl.size_strand"),
        'pearl_local_price' : fields.related('product_id', 'pearl_local_price', string="Local price", type="float", store=False),
        'pearl_local_currency_id' : fields.related('product_id', 'pearl_local_currency_id', string="Local currency", type="many2one", store=False, relation="res.currency"),
        'pearl_attachment_id' : fields.related('product_id', 'pearl_attachment_id', string="Attachment", type="many2one", store=False, relation="pearl.attachment"),

        # metal
        'metal_id' : fields.related('product_id', 'metal_id', type='many2one', string="Metal", relation="metal.metal", store=False),
        'metal_alloy_id' : fields.related('product_id', 'metal_alloy_id', type='many2one', string="Metal alloy", relation="metal.alloy", store=False),
        'metal_stamp_id' : fields.related('product_id', 'metal_stamp_id', type='many2one', string="Metal stamp", relation="metal.stamp", store=False),
        'weight_measure_id' : fields.related('product_id', 'weight_measure_id', type='many2one', string="Metal measure", relation="metal.measure", store=False),
        'metal_finish_id' : fields.related('product_id', 'metal_finish_id', type='many2one', string="Metal finish", relation="metal.finish", store=False),
        'metal_manufacturing_id' : fields.related('product_id', 'metal_manufacturing_id', type='many2one', string="Metal manufacturing", relation="metal.manufacturing", store=False),
        'metal_plating_id' : fields.related('product_id', 'metal_plating_id', type='many2one', string="Metal plating", relation="metal.plating", store=False),
        'metal_weight' : fields.related('product_id', 'metal_weight', string="Weight", store=False),

        # product
        'tmw' : fields.related('product_id', 'tmw', string="Total metals weight", type="float", store=False),
        'tgw' : fields.related('product_id', 'tgw', string="Total gemstones weight", type="float", store=False),
        'tdw' : fields.related('product_id', 'tdw', string="Total diamonds weight", type="float", store=False),
        'width' : fields.related('product_id', 'width', string="Width", type="float", store=False),
        'height' : fields.related('product_id', 'height', string="Height", type="float", store=False),
        'length' : fields.related('product_id', 'length', string="Length", type="float", store=False),
        'made_in' : fields.related('product_id', 'made_in', string="Made in", type="many2one", store=False, relation="res.country"),

        'prod_quality' : fields.related('product_id', 'prod_quality', string='Quality', type="many2one", relation='labour.quality', store=False),
        'gold_loss' : fields.related('product_id', 'gold_loss', string='Gold Loss', type="float", store=False),
        'increment' : fields.related('product_id', 'increment', string="Increment", type="float", store=False),
        'increment_multiplier' : fields.related('product_id', 'increment_multiplier', string="Increment multiplier", type="float", store=False),
        'labour' : fields.related('product_id', 'labour', string="Labour", type="float", store=False),
        'duty_mark' : fields.related('product_id', 'duty_mark', string="Duty mark", type="boolean", store=False),
        'duty_price' : fields.related('product_id', 'duty_price', string="Duty", type="float", store=False),
        'shipping_mark' : fields.related('product_id', 'shipping_mark', string="Shipping mark", type="boolean", store=False),
        'shipping_price' : fields.related('product_id', 'shipping_price', string="Shipping", type="float", store=False),
        'duty_price' : fields.related('product_id', 'duty_price', string='Duty price', type="float", store=False),
        'shipping_price' : fields.related('product_id', 'shipping_price', string='Shipping price', type="float", store=False),

        'prod_stone_type': fields.related('product_id', 'prod_stone_type', string="Stone type", type="char", store=False),
        'prod_stone_weight': fields.related('product_id', 'prod_stone_weight', string="Stone weight", type="char", store=False),
        'prod_total_stones': fields.related('product_id', 'prod_total_stones', string="Total stones", type="integer", store=False),
        'prod_stone_color': fields.related('product_id', 'prod_stone_color', string="Stone color", type="char", store=False),
        'prod_stone_clarity': fields.related('product_id', 'prod_stone_clarity', string="Stone clarity", type="char", store=False),
        'prod_metal_stamp': fields.related('product_id', 'prod_metal_stamp', string="Metal stamp", type="char", store=False),
        'prod_metal_type': fields.related('product_id', 'prod_metal_type', string="Metal type", type="char", store=False),
        'prod_stone_shape': fields.related('product_id', 'prod_stone_shape', string="Stone shape", type="char", store=False),
        'prod_stone_size': fields.related('product_id', 'prod_stone_size', string="Stone size", type="char", store=False),
        'prod_ring_style': fields.related('product_id', 'prod_ring_style', string="Ring style", type="char", store=False),

        # Metal Labour
        'met_lab_category_id' : fields.related('product_id', 'met_lab_category_id', string="Category", type="many2one", store=False, relation="labour.category"),
        'met_lab_metal_id' : fields.related('product_id', 'met_lab_metal_id', string="Metal", type="many2one", store=False, relation="metal_labour.metal"),
        'met_lab_quality' : fields.related('product_id', 'met_lab_quality', string="Quality", type="many2one", store=False, relation="labour.quality"),
        'met_lab_local_price' : fields.related('product_id', 'met_lab_local_price', string="Local price", type="float", store=False),
        'met_lab_weight_min' : fields.related('product_id', 'met_lab_weight_min', string="Weight Min", type="float", store=False),
        'met_lab_weight_max' : fields.related('product_id', 'met_lab_weight_max', string="Weight Max", type="float", store=False),
        'met_lab_links_min' : fields.related('product_id', 'met_lab_links_min', string="Links Min", type="integer", store=False),
        'met_lab_links_max' : fields.related('product_id', 'met_lab_links_max', string="Links Max", type="integer", store=False),
        'met_lab_cost_type' : fields.related('product_id', 'met_lab_cost_type', string="Cost type", type="char", store=False),
        'met_lab_local_price' : fields.related('product_id', 'met_lab_local_price', string="Local price", type="float", store=False),
        'met_lab_local_currency_id' : fields.related('product_id', 'met_lab_local_currency_id', string="Local currency", type="many2one", store=False, relation="res.currency"),
        'met_lab_local_supplier_id' : fields.related('product_id', 'met_lab_local_supplier_id', string="Supplier", type="many2one", store=False, relation="res.partner"),
        'met_lab_difficulty_id' : fields.related('product_id', 'met_lab_difficulty_id', string="Difficulty", type="many2one", store=False, relation="labour.difficulty"),


        # Plating Labour
        'plat_lab_quality' : fields.related('product_id', 'plat_lab_quality', string="Quality", type="many2one", store=False, relation="labour.quality"),
        'plat_lab_category_id' : fields.related('product_id', 'plat_lab_category_id', string="Category", type="many2one", store=False, relation="labour.category"),
        'plat_lab_metal_id' : fields.related('product_id', 'plat_lab_metal_id', string="Metal", type="many2one", store=False, relation="plating_labour.metal"),
        'plat_lab_platyng_type_id' : fields.related('product_id', 'plat_lab_platyng_type_id', string="Plating Type", type="many2one", store=False, relation="plating_labour.plating_type"),
        'plat_lab_color' : fields.related('product_id', 'plat_lab_color', string="Color", type="char", store=False),
        'plat_lab_multitone' : fields.related('product_id', 'plat_lab_multitone', string="Multitone", type="boolean", store=False),
        'plat_lab_local_price' : fields.related('product_id', 'plat_lab_local_price', string="Local price", type="float", store=False),
        'plat_lab_local_currency_id' : fields.related('product_id', 'plat_lab_local_currency_id', string="Local currency", type="many2one", store=False, relation="res.currency"),
        'plat_lab_local_supplier_id' : fields.related('product_id', 'plat_lab_local_supplier_id', string="Supplier", type="many2one", store=False, relation="res.partner"),


        # finishing labour
        'fin_lab_quality' : fields.related('product_id', 'fin_lab_quality', string="Quality", type="many2one", store=False, relation="labour.quality"),
        'fin_lab_category_id' : fields.related('product_id', 'fin_lab_category_id', string="Category", type="many2one", store=False, relation="labour.category"),
        'fin_lab_metal_id' : fields.related('product_id', 'fin_lab_metal_id', string="Metal", type="many2one", store=False, relation="finishing_labour.metal"),
        'fin_lab_finishing_id' : fields.related('product_id', 'fin_lab_finishing_id', string="Finishing", type="many2one", store=False, relation="finishing_labour.finishing"),
        'fin_lab_local_price' : fields.related('product_id', 'fin_lab_local_price', string="Local price", type="float", store=False),
        'fin_lab_local_currency_id' : fields.related('product_id', 'fin_lab_local_currency_id', string="Local currency", type="many2one", store=False, relation="res.currency"),
        'fin_lab_local_supplier_id' : fields.related('product_id', 'fin_lab_local_supplier_id', string="Supplier", type="many2one", store=False, relation="res.partner"),

        'po_requires': fields.boolean(string="Require for PO"),

        'component_supplier_sku' : fields.related(
            'product_id',
            'supplier_sku',
            string='Supplier SKU',
            type='char',
            store=False,
        ),

        'component_supplier_1' : fields.related(
            'product_id',
            'supplier_1',
            string='Supplier 1',
            type='char',
            store=False,
        ),

    }

    def onchange_product_id(self, cr, uid, ids, product_id, context=None):
        if product_id:
            prod = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            categ_obj = self.pool.get('product.category')
            prod_type = categ_obj.get_prod_type(cr,uid,prod.categ_id.id)
            return {'value': {'product_id': prod.id, 'categ_id': prod.categ_id.id, 'attributes': prod.attributes, 'standard_price': prod.standard_price, 'list_price': prod.list_price, 'prod_type': prod_type}}
        return {}

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}
        pack_lines = self.browse(cr, uid, ids)
        product_ids = []
        product_obj = self.pool.get('product.product')
        if context.get('update_components', True):
            product_ids = [
                set_relation.parent_product_id.id
                for pack_line in pack_lines
                for set_relation in pack_line.parent_product_id.set_parent_ids
                if set_relation.parent_product_id
            ]
            if vals.get('parent_product_id'):
                product_to = product_obj.browse(cr, uid,
                                                vals['parent_product_id'])
                product_ids.extend([
                    set_relation.parent_product_id.id
                    for set_relation in product_to.set_parent_ids
                    if set_relation.parent_product_id
                ])
        result = super(product_pack, self).write(
            cr, uid, ids, vals, context=context)
        for product_id in list(set(product_ids)):
            product_obj.update_set_packlines_from_components(
                cr, uid, product_id)
        if result:
            self.calculate_total_price(cr,uid,ids)
        return result

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        result = super(product_pack, self).create(
            cr, uid, vals, context=context)
        if context.get('update_components', True):
            product_obj = self.pool.get('product.product')
            product_obj.update_set_packlines_from_components(
                cr, uid, vals['parent_product_id'])
        return result

    def calculate_total_price(self, cr, uid, ids, context=None):
        vals = {}
        for pack in self.browse(cr,uid,ids):
            total = pack.quantity * pack.cost
            vals['total'] = total
            return super(product_pack, self).write(cr, uid, ids, vals, context=context)


product_pack()




class product_product(osv.osv):
    _inherit = 'product.product'
    _columns = {
        'stock_depends': fields.boolean('Stock depends of components', help='Mark if pack stock is calcualted from component stock'),
        'pack_fixed_price': fields.boolean('Pack has fixed price', help='Mark this field if the public price of the pack should be fixed. Do not mark it if the price should be calculated from the sum of the prices of the products in the pack.'),
        'pack_line_ids': fields.one2many('product.pack.line', 'parent_product_id', 'Pack Products', help='List of products that are part of this pack.'),
        'attributes' : fields.text('Attributes'),
    }


    def write(self, cr, uid, ids, vals, context=None):
        result = super(product_product, self).write(cr, uid, ids, vals, context=context)
        if result:
            self.calculate_attrs(cr,uid,ids)
        return result

    def create(self, cr, uid, vals, context=None):
        result = super(product_product, self).create(cr,uid,vals,context=context)
        if result:
            self.calculate_attrs(cr,uid,[result])
        return result

    def update_components(self, cr, uid, product_id, pack_line_data):
        product = self.browse(cr, uid, product_id)
        line_ids = []
        for pack_line in product.pack_line_ids:
            component_id = pack_line.product_id.id
            if component_id in pack_line_data:
                write_vals = pack_line_data.pop(component_id)
                # (1, ID, {vals}) - update ID without link
                line_ids.append((1, pack_line.id, write_vals))
            else:
                # (2, ID) - remove ID
                line_ids.append((2, pack_line.id))
        # (0, 0, {vals}) - insert new record
        line_ids.extend([
            (0, 0, dict({'product_id': comp_id}, **vals))
            for comp_id, vals
            in pack_line_data.items()
        ])
        self.write(cr, uid, [product_id], {'pack_line_ids': line_ids},
                   context={'update_components': False})
        return True

    def calculate_attrs(self, cr, uid, ids, context=None):
        if not ids:
            return False

        if isinstance(ids, (int, long)):
            ids = [ids]

        prod_fields = {
            'diamond': {
                'dmd_category_id' : 'Product type',
                'dmd_shape_id' : 'Shape',
                'dmd_finish_id': 'Finish',
                'dmd_dimension_min' : 'Dimension min',
                'dmd_dimension_max' : 'Dimension max',
                'dmd_sieve_min_id' : 'Sieve min',
                'dmd_sieve_max_id' : 'Sieve max',
                'dmd_weight' : 'Weight',
                'dmd_carat_min' : 'Carat min',
                'dmd_carat_max' : 'Carat max',
                'dmd_color_id' : 'Color' ,
                'dmd_color_categ_id' : 'Color categ',
                'dmd_clarity_id' : 'Clarity',
                'dmd_internal_code_id' : 'Internal code',
                'dmd_note_id' : 'Note',
                'dmd_certified' : 'Certified',
                'dmd_decoration_setting_type_id' : 'Decoration',
            },
            'gemstone': {
                'gemst_sku' : 'Sku',
                'gemst_length' : 'Length',
                'gemst_width' : 'Width',
                'gemst_shape_id' : 'Shape',
                'gemst_type_id' : 'Type',
                'gemst_weight' : 'Weight',
                'gemst_avg_weight' : 'Avg weight',
                'gemst_quality' : 'Quality',
                'gemst_local_price' : 'Local price',
                'gemst_local_currency_id' : 'Local currency',
                'gemst_price_per' : 'Price per',
                'gemst_decoration_setting_type_id' : 'Decoration setting type',
            },
            'metal_stone': {
                'metal_id' : 'Metal',
                'metal_alloy_id' : 'Alloy',
                'metal_stamp_id' : 'Stamp',
                'metal_finish_id' : 'Finish',
                'metal_manufacturing_id' : 'Manufacturing',
                'metal_plating_id' : 'Plating',
                'weight_measure_id' : 'Weight measure',
                'gemst_sku' : 'Sku',
                'gemst_length' : 'Length',
                'gemst_width' : 'Width',
                'gemst_shape_id' : 'Shape',
                'gemst_type_id' : 'Type',
                'gemst_weight' : 'Weight',
                'gemst_avg_weight' : 'Avg weight',
                'gemst_quality' : 'Quality',
                'gemst_local_price' : 'Local price',
                'gemst_local_currency_id' : 'Local currency',
                'gemst_price_per' : 'Price per',
                'gemst_decoration_setting_type_id' : 'Decoration setting type',
            },
            'pearl': {
                'pearl_type_id' : 'Type',
                'pearl_surface_id' : 'Surface',
                'pearl_luster' : 'Luster',
                'pearl_shape_id' : 'Shape',
                'pearl_color_id' : 'Color',
                'pearl_quality' : 'Quality',
                'pearl_class' : 'Class',
                'pearl_size_type' : 'Size type',
                'pearl_size_single_id' : 'Size single',
                'pearl_size_strand_id' : 'Size strand',
                'pearl_local_price' : 'Local price',
                'pearl_local_currency_id' : 'Local currency',
                'pearl_attachment_id' : 'Attachment',
            },
            'metal': {
                'metal_id' : 'Metal',
                'metal_alloy_id' : 'Alloy',
                'metal_stamp_id' : 'Stamp',
                'metal_finish_id' : 'Finish',
                'metal_manufacturing_id' : 'Manufacturing',
                'metal_plating_id' : 'Plating',
                'weight_measure_id' : 'Weight measure',
            },
            'finding': {
                'finding_id' : 'Finding',
                'finding_category_id' : 'Category',
                'finding_group_id' : 'Group',
                'finding_attachment_type_id' : 'Attachment type',
            },
            'metal_labour': {
                'met_lab_category_id' : 'Category',
                'met_lab_metal_id' : 'Metal',
                'met_lab_quality' : 'Quality',
                'met_lab_local_price' : 'Local price',
                'met_lab_weight_min' : 'Weight min',
                'met_lab_weight_max' : 'Weight max',
                'met_lab_links_min' : 'Links min',
                'met_lab_links_max' : 'Links max',
                'met_lab_cost_type' : 'Cost type',
                'met_lab_local_price' : 'Local price',
                'met_lab_local_currency_id' : 'Local currency',
                'met_lab_local_supplier_id' : 'Local supplier',
                'met_lab_difficulty_id' : 'Difficulty',
            },
            'plating_labour': {
                'plat_lab_quality' : 'Quality',
                'plat_lab_category_id' : 'Category',
                'plat_lab_metal_id' : 'Metal',
                'plat_lab_platyng_type_id' : 'Platyng type',
                'plat_lab_color' : 'Color',
                'plat_lab_multitone' : 'Multitone',
                'plat_lab_local_price' : 'Local price',
                'plat_lab_local_currency_id' : 'Local currency',
                'plat_lab_local_supplier_id' : 'Local supplier',
            },
            'finishing_labour': {
                'fin_lab_quality' : 'Quality',
                'fin_lab_category_id' : 'Category',
                'fin_lab_metal_id' : 'Metal',
                'fin_lab_finishing_id' : 'Finishing',
                'fin_lab_local_price' : 'Local price',
                'fin_lab_local_currency_id' : 'Local currency',
                'fin_lab_local_supplier_id' : 'Local supplier',
            },

        }

        vals = {}
        attrs = ""

        for product in self.browse(cr,uid,ids):

            categ_id = product.categ_id
            if categ_id:
                categ_obj = self.pool.get('product.category')
                prod_type = categ_obj.get_prod_type(cr,uid,categ_id.id)

                if prod_type in prod_fields:
                    fields = prod_fields[prod_type]

                    for field in fields:
                        val = product[field]
                        if ( val and val not in [None, 0, ''] ):
                            if hasattr(val,'name'):
                                val = val.name
                            attrs +=  "%s: %s \n" % (fields[field], val)

        vals['attributes'] = attrs
        return super(product_product, self).write(cr, uid, ids, vals, context=context)


    def get_product_available(self, cr, uid, ids, context=None):
        """ Calulate stock for packs, return  maximum stock that lets complete pack """
        result = {}
        for product in self.browse(cr, uid, ids, context=context):
            stock = super(product_product, self).get_product_available(cr, uid, [product.id], context=context)

            # Check if product stock depends on it's subproducts stock.
            if not product.stock_depends:
                result[product.id] = stock[product.id]
                continue

            first_subproduct = True
            pack_stock = 0

            # Check if the pack has subproducts
            if product.pack_line_ids:
                # Take the stock/virtual stock of all subproducts
                subproducts_stock = self.get_product_available(cr, uid, [line.product_id.id for line in product.pack_line_ids], context=context)
                # Go over all subproducts, take quantity needed for the pack and its available stock
                for subproduct in product.pack_line_ids:
                    if first_subproduct:
                        subproduct_quantity = subproduct.quantity
                        subproduct_stock = subproducts_stock[subproduct.product_id.id]
                        # Calculate real stock for current pack from the subproduct stock and needed quantity
                        pack_stock = math.floor(subproduct_stock / subproduct_quantity)
                        first_subproduct = False
                        continue
                    # Take the info of the next subproduct
                    subproduct_quantity_next = subproduct.quantity
                    subproduct_stock_next = subproducts_stock[subproduct.product_id.id]
                    pack_stock_next = math.floor(subproduct_stock_next / subproduct_quantity_next)
                    # compare the stock of a subproduct and the next subproduct
                    if pack_stock_next < pack_stock:
                        pack_stock = pack_stock_next
                # result is the minimum stock of all subproducts
                result[product.id] = pack_stock
            else:
                result[product.id] = stock[product.id]
        return result

product_product()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    _columns = {
        'pack_depth': fields.integer('Depth', required=True, help='Depth of the product if it is part of a pack.'),
        'pack_parent_line_id': fields.many2one('sale.order.line', 'Pack', help='The pack that contains this product.'),
        'pack_child_line_ids': fields.one2many('sale.order.line', 'pack_parent_line_id', 'Lines in pack', help=''),
    }
    _defaults = {
        'pack_depth': lambda *a: 0,
    }
sale_order_line()


class sale_order(osv.osv):
    _inherit = 'sale.order'

    def create(self, cr, uid, vals, context=None):
        result = super(sale_order, self).create(cr, uid, vals, context)
        # self.expand_packs(cr, uid, [result], context)
        return result

    def write(self, cr, uid, ids, vals, context=None):
        result = super(sale_order, self).write(cr, uid, ids, vals, context)
        # self.expand_packs(cr, uid, ids, context)
        return result

    def expand_packs(self, cr, uid, ids, context={}, depth=1):
        if depth == 10:
            return
        updated_orders = []
        for order in self.browse(cr, uid, ids, context):

            fiscal_position = order.fiscal_position and self.pool.get('account.fiscal.position').browse(cr, uid, order.fiscal_position, context) or False

            # The reorder variable is used to ensure lines of the same pack go right after their
            # parent.
            # What the algorithm does is check if the previous item had children. As children items
            # must go right after the parent if the line we're evaluating doesn't have a parent it
            # means it's a new item (and probably has the default 10 sequence number - unless the
            # appropiate c2c_sale_sequence module is installed). In this case we mark the item for
            # reordering and evaluate the next one. Note that as the item is not evaluated and it might
            # have to be expanded it's put on the queue for another iteration (it's simple and works well).
            # Once the next item has been evaluated the sequence of the item marked for reordering is updated
            # with the next value.
            sequence = -1
            reorder = []
            last_had_children = False
            for line in order.order_line:
                if last_had_children and not line.pack_parent_line_id:
                    reorder.append(line.id)
                    if line.product_id.pack_line_ids and not order.id in updated_orders:
                        updated_orders.append(order.id)
                    continue

                sequence += 1

                if sequence > line.sequence:
                    self.pool.get('sale.order.line').write(cr, uid, [line.id], {
                        'sequence': sequence,
                    }, context)
                else:
                    sequence = line.sequence

                if line.state != 'draft':
                    continue
                if not line.product_id:
                    continue

                # If pack was already expanded (in another create/write operation or in
                # a previous iteration) don't do it again.
                if line.pack_child_line_ids:
                    last_had_children = True
                    continue
                last_had_children = False

                for subline in line.product_id.pack_line_ids:
                    sequence += 1

                    subproduct = subline.product_id
                    quantity = subline.quantity * line.product_uom_qty

                    if line.product_id.pack_fixed_price:
                        price = 0.0
                        discount = 0.0
                    else:
                        pricelist = order.pricelist_id.id
                        price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                                        subproduct.id, quantity, order.partner_id.id, {
                            'uom': subproduct.uom_id.id,
                            'date': order.date_order,
                        })[pricelist]
                        discount = line.discount

                    # Obtain product name in partner's language
                    ctx = {'lang': order.partner_id.lang}
                    subproduct_name = self.pool.get('product.product').browse(cr, uid, subproduct.id, ctx).name

                    tax_ids = self.pool.get('account.fiscal.position').map_tax(cr, uid, fiscal_position, subproduct.taxes_id)

                    if subproduct.uos_id:
                        uos_id = subproduct.uos_id.id
                        uos_qty = quantity * subproduct.uos_coeff
                    else:
                        uos_id = False
                        uos_qty = quantity

                    vals = {
                        'order_id': order.id,
                        'name': '%s%s' % ('> ' * (line.pack_depth + 1), subproduct_name),
                        'sequence': sequence,
                        'delay': subproduct.sale_delay or 0.0,
                        'product_id': subproduct.id,
                        'procurement_id': line.procurement_id and line.procurement_id.id or False,
                        'price_unit': price,
                        'tax_id': [(6, 0, tax_ids)],
                        'type': subproduct.procure_method,
                        'property_ids': [(6, 0, [])],
                        'address_allotment_id': False,
                        'product_uom_qty': quantity,
                        'product_uom': subproduct.uom_id.id,
                        'product_uos_qty': uos_qty,
                        'product_uos': uos_id,
                        'product_packaging': False,
                        'move_ids': [(6, 0, [])],
                        'discount': discount,
                        'number_packages': False,
                        'notes': False,
                        'th_weight': False,
                        'state': 'draft',
                        'pack_parent_line_id': line.id,
                        'pack_depth': line.pack_depth + 1,
                    }

                    # It's a control for the case that the nan_external_prices was installed with the product pack
                    if 'prices_used' in line:
                        vals['prices_used'] = line.prices_used

                    self.pool.get('sale.order.line').create(cr, uid, vals, context)
                    if not order.id in updated_orders:
                        updated_orders.append(order.id)

                for id in reorder:
                    sequence += 1
                    self.pool.get('sale.order.line').write(cr, uid, [id], {
                        'sequence': sequence,
                    }, context)

        if updated_orders:
            # Try to expand again all those orders that had a pack in this iteration.
            # This way we support packs inside other packs.
            self.expand_packs(cr, uid, ids, context, depth + 1)
        return

sale_order()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
