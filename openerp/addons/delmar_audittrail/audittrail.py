# -*- coding: utf-8 -*-

from osv import osv, fields
import pooler
from openerp.addons.audittrail.audittrail import audittrail_objects_proxy
from openerp.tools.safe_eval import safe_eval as eval
import logging


_logger = logging.getLogger(__name__)


class audittrail_rule(osv.osv):
    _inherit = "audittrail.rule"

    _columns = {
        'log_domain': fields.text('Domain', required=False, ),
        'log_field_ids': fields.many2many('ir.model.fields', 'field_to_audittrail_rule_rel', 'field_id', 'rule_id', 'Fields', ),
    }

    _defaults = {
        'log_domain': '[]',
    }

    def change_model_id(self, cr, uid, ids, object_id, log_field_ids=None, context=None):
        value = {}

        field_ids = []
        if object_id and log_field_ids:
            cur_field_ids = log_field_ids[0] and log_field_ids[0][2] or []
            if field_ids:
                field_ids = self.pool.get('ir.model.fields').search(cr, uid, [('id', 'in', cur_field_ids), ('object_id', '=', object_id)])

        value['log_field_ids'] = [(6, 0, field_ids)]

        return {'value': value}

audittrail_rule()



# Extebd _exec_workflow_cr

def _log_fct(self, cr, uid_orig, model, method, fct_src, rule_id, *args):
    """
    Logging function: This function is performing the logging operation
    @param model: Object whose values are being changed
    @param method: method to log: create, read, write, unlink, action or workflow action
    @param fct_src: execute method of Object proxy

    @return: Returns result as per method of Object proxy
    """
    pool = pooler.get_pool(cr.dbname)
    resource_pool = pool.get(model)
    model_pool = pool.get('ir.model')
    model_ids = model_pool.search(cr, 1, [('model', '=', model)])
    model_id = model_ids and model_ids[0] or False
    assert model_id, _("'%s' Model does not exist..." %(model))
    model = model_pool.browse(cr, 1, model_id)

    # fields to log. currently only used by log on read()
    field_list = []
    old_values = new_values = {}

    rule = pool.get('audittrail.rule').read(cr, 1, rule_id, ['log_domain', 'log_field_ids'])
    rule_field_list = [x['name'] for x in pool.get('ir.model.fields').read(cr, 1, rule['log_field_ids'], ['name']) if x]

    # check domain
    def check_domain(cr, domain, res_ids, *args):
        dom = None
        if domain and res_ids:
            try:
                dom = eval(domain)
                if dom:
                    dom = ['&', ('id', 'in', res_ids)] + dom
                    res_ids = resource_pool.search(cr, 1, dom)
            except:
                res_ids = []

        return res_ids

    if method == 'create':
        res = fct_src(cr, uid_orig, model.model, method, *args)
        if res:
            res_ids = [res]
            res_ids = check_domain(cr, rule['log_domain'], res_ids)
            new_values = self.get_data(cr, uid_orig, pool, res_ids, model, method)
    elif method == 'read':
        res = fct_src(cr, uid_orig, model.model, method, *args)
        # build the res_ids and the old_values dict. Here we don't use get_data() to
        # avoid performing an additional read()
        res_ids = []
        for record in res:
            res_ids.append(record['id'])
            old_values[(model.id, record['id'])] = {'value': record, 'text': record}
        # log only the fields read
        res_ids = check_domain(cr, rule['log_domain'], res_ids)
        field_list = args[1]
    elif method == 'unlink':
        res_ids = args[0]
        res_ids = check_domain(cr, rule['log_domain'], res_ids)
        old_values = self.get_data(cr, uid_orig, pool, res_ids, model, method)
        res = fct_src(cr, uid_orig, model.model, method, *args)
    else:  # method is write, action or workflow action
        res_ids = []
        if args:
            res_ids = args[0]
            if isinstance(res_ids, (long, int)):
                res_ids = [res_ids]
            res_ids = check_domain(cr, rule['log_domain'], res_ids)
        if res_ids:
            # store the old values into a dictionary
            old_values = self.get_data(cr, uid_orig, pool, res_ids, model, method)
        # process the original function, workflow trigger...
        res = fct_src(cr, uid_orig, model.model, method, *args)
        if method == 'copy':
            res_ids = [res]
            res_ids = check_domain(cr, rule['log_domain'], res_ids)
        if res_ids:
            # check the new values and store them into a dictionary
            new_values = self.get_data(cr, uid_orig, pool, res_ids, model, method)
    # compare the old and new values and create audittrail log if needed

    if rule_field_list and field_list:
        field_list = list(set(rule_field_list) - set(field_list))
    elif rule_field_list:
        field_list = rule_field_list

    self.process_data(cr, uid_orig, pool, res_ids, model, method, old_values, new_values, field_list)
    return res

def _get_data(self, cr, uid, pool, res_ids, model, method, fields=None):
    """
    This function simply read all the fields of the given res_ids, and also recurisvely on
    all records of a x2m fields read that need to be logged. Then it returns the result in
    convenient structure that will be used as comparison basis.

        :param cr: the current row, from the database cursor,
        :param uid: the current user’s ID. This parameter is currently not used as every
            operation to get data is made as super admin. Though, it could be usefull later.
        :param pool: current db's pooler object.
        :param res_ids: Id's of resource to be logged/compared.
        :param model: Object whose values are being changed
        :param method: method to log: create, read, unlink, write, actions, workflow actions
        :return: dict mapping a tuple (model_id, resource_id) with its value and textual value
            { (model_id, resource_id): { 'value': ...
                                         'textual_value': ...
                                       },
            }
    """
    data = {}
    resource_pool = pool.get(model.model)
    # read all the fields of the given resources in super admin mode
    resource_names = dict(resource_pool.name_get(cr, 1, res_ids))
    for resource in resource_pool.read(cr, 1, res_ids):
        values = {}
        values_text = {}
        resource_id = resource['id']
        resource_name = resource_names.get(resource['id'], False)
        # loop on each field on the res_ids we just have read
        for field in resource:
            if field in ('__last_update', 'id'):
                continue
            values[field] = resource[field]
            # get the textual value of that field for this record
            values_text[field] = self.get_value_text(cr, 1, pool, resource_pool, method, field, resource[field])

            field_obj = resource_pool._all_columns.get(field).column
            if field_obj._type in ('one2many', 'many2many'):
                # check if an audittrail rule apply in super admin mode
                if self.check_rules(cr, 1, field_obj._obj, method):
                    # check if the model associated to a *2m field exists, in super admin mode
                    x2m_model_ids = pool.get('ir.model').search(cr, 1, [('model', '=', field_obj._obj)])
                    x2m_model_id = x2m_model_ids and x2m_model_ids[0] or False
                    assert x2m_model_id, _("'%s' Model does not exist..." % (field_obj._obj))
                    x2m_model = pool.get('ir.model').browse(cr, 1, x2m_model_id)
                    #recursive call on x2m fields that need to be checked too
                    data.update(self.get_data(cr, 1, pool, resource[field], x2m_model, method))
        data[(model.id, resource_id)] = {'text': values_text, 'value': values, 'resource_name': resource_name}
    return data

def _process_data(self, cr, uid, pool, res_ids, model, method, old_values=None, new_values=None, field_list=None):
    """
    This function processes and iterates recursively to log the difference between the old
    data (i.e before the method was executed) and the new data and creates audittrail log
    accordingly.

    :param cr: the current row, from the database cursor,
    :param uid: the current user’s ID,
    :param pool: current db's pooler object.
    :param res_ids: Id's of resource to be logged/compared.
    :param model: model object which values are being changed
    :param method: method to log: create, read, unlink, write, actions, workflow actions
    :param old_values: dict of values read before execution of the method
    :param new_values: dict of values read after execution of the method
    :param field_list: optional argument containing the list of fields to log. Currently only
        used when performing a read, it could be usefull later on if we want to log the write
        on specific fields only.
    :return: True
    """

    if old_values is None:
        old_values = {}
    if new_values is None:
        new_values = {}
    if field_list is None:
        field_list = []
    # loop on all the given ids
    for res_id in res_ids:
        # compare old and new values and get audittrail log lines accordingly
        lines = self.prepare_audittrail_log_line(cr, uid, pool, model, res_id, method, old_values, new_values, field_list)

        # if at least one modification has been found
        for model_id, resource_id in lines:
            vals = {
                'method': method,
                'object_id': model_id,
                'user_id': uid,
                'res_id': resource_id,
            }
            if (model_id, resource_id) not in old_values and method not in ('copy', 'read'):
                # the resource was not existing so we are forcing the method to 'create'
                # (because it could also come with the value 'write' if we are creating
                #  new record through a one2many field)
                method = 'create'
            if (model_id, resource_id) not in new_values and method not in ('copy', 'read'):
                # the resource is not existing anymore so we are forcing the method to 'unlink'
                # (because it could also come with the value 'write' if we are deleting the
                #  record through a one2many field)
                method = 'unlink'
            # create the audittrail log in super admin mode, only if a change has been detected
            resource_name = None
            if method == 'unlink':
                resource_name = old_values.get((model_id, resource_id), {}).get('resource_name', False)
            else:
                resource_name = new_values.get((model_id, resource_id), {}).get('resource_name', False)
            vals.update({'method': method, 'name': resource_name})
            if lines[(model_id, resource_id)]:
                log_id = pool.get('audittrail.log').create(cr, 1, vals)
                model = pool.get('ir.model').browse(cr, uid, model_id)
                self.create_log_line(cr, 1, log_id, model, lines[(model_id, resource_id)])
    return True


def _check_rules(self, cr, uid, model, method):
    """
    Checks if auditrails is installed for that db and then if one rule match
    @param cr: the current row, from the database cursor,
    @param uid: the current user’s ID,
    @param model: value of _name of the object which values are being changed
    @param method: method to log: create, read, unlink,write,actions,workflow actions
    @return: True or False
    """
    pool = pooler.get_pool(cr.dbname)
    if 'audittrail.rule' in pool.models:
        model_ids = pool.get('ir.model').search(cr, 1, [('model', '=', model)])
        model_id = model_ids and model_ids[0] or False
        if model_id:
            rule_ids = pool.get('audittrail.rule').search(cr, 1, [('object_id', '=', model_id), ('state', '=', 'subscribed')])
            for rule in pool.get('audittrail.rule').read(cr, 1, rule_ids, ['user_id', 'object_id', 'log_read','log_write','log_create','log_unlink','log_action','log_workflow']):
                if len(rule['user_id']) == 0 or uid in rule['user_id']:
                    if rule.get('log_'+method,0):
                        return rule['id']
                    elif method not in ('default_get','read','fields_view_get','fields_get','search','search_count','name_search','name_get','get','request_get', 'get_sc', 'unlink', 'write', 'create'):
                        if rule['log_action']:
                            return rule['id']


def _execute_cr(self, cr, uid, model, method, *ks, **kv):
    fct_src = super(audittrail_objects_proxy, self).execute_cr
    rule_id = self.check_rules(cr, uid, model, method)
    if rule_id:
        return self.log_fct(cr, uid, model, method, fct_src, rule_id, *ks)
    return fct_src(cr, uid, model, method, *ks, **kv)


def _exec_workflow_cr(self, cr, uid, model, method, *ks, **kv):
    fct_src = super(audittrail_objects_proxy, self).exec_workflow_cr
    rule_id = self.check_rules(cr, uid, model, 'workflow')
    if rule_id:
        return self.log_fct(cr, uid, model, method, fct_src, rule_id, *ks)
    return fct_src(cr, uid, model, method, *ks, **kv)


def _get_value_text(self, cr, uid, pool, resource_pool, method, field, value):
    field_obj = (resource_pool._all_columns.get(field)).column
    if field_obj._type in ('one2many', 'many2many'):
        try:
            data = pool.get(field_obj._obj).name_get(cr, uid, value)
            # return the modifications on x2many fields as a list of names
            res = map(lambda x: x[1], data)
        except KeyError, e:
            res = ['%s: %s' % (x, field_obj._obj) for x in value]
            _logger.warning('Can\'t find name for {model}: \n{error}'.format(model=field_obj._obj, error=e))

    elif field_obj._type == 'many2one':
        # return the modifications on a many2one field as its value returned by name_get()
        res = value and value[1] or value
    else:
        res = value
    return res


def _prepare_audittrail_log_line(self, cr, uid, pool, model, resource_id, method, old_values, new_values, field_list=None):
    """
    This function compares the old data (i.e before the method was executed) and the new data
    (after the method was executed) and returns a structure with all the needed information to
    log those differences.

    :param cr: the current row, from the database cursor,
    :param uid: the current user’s ID. This parameter is currently not used as every
        operation to get data is made as super admin. Though, it could be usefull later.
    :param pool: current db's pooler object.
    :param model: model object which values are being changed
    :param resource_id: ID of record to which values are being changed
    :param method: method to log: create, read, unlink, write, actions, workflow actions
    :param old_values: dict of values read before execution of the method
    :param new_values: dict of values read after execution of the method
    :param field_list: optional argument containing the list of fields to log. Currently only
        used when performing a read, it could be usefull later on if we want to log the write
        on specific fields only.

    :return: dictionary with
        * keys: tuples build as ID of model object to log and ID of resource to log
        * values: list of all the changes in field values for this couple (model, resource)
          return {
            (model.id, resource_id): []
          }

    The reason why the structure returned is build as above is because when modifying an existing
    record (res.partner, for example), we may have to log a change done in a x2many field (on
    res.partner.address, for example)
    """
    key = (model.id, resource_id)
    lines = {
        key: []
    }

    if field_list is None:
        field_list = []
    # loop on all the fields
    for field_name, field_definition in pool.get(model.model)._all_columns.items():
        #if the field_list param is given, skip all the fields not in that list
        if field_list and field_name not in field_list:
            continue
        if field_name in ('__last_update', 'id'):
                continue
        field_obj = field_definition.column
        if field_obj._type in ('one2many','many2many'):
            # checking if an audittrail rule apply in super admin mode
            if self.check_rules(cr, 1, field_obj._obj, method):
                # checking if the model associated to a *2m field exists, in super admin mode
                x2m_model_ids = pool.get('ir.model').search(cr, 1, [('model', '=', field_obj._obj)])
                x2m_model_id = x2m_model_ids and x2m_model_ids[0] or False
                assert x2m_model_id, _("'%s' Model does not exist..." %(field_obj._obj))
                x2m_model = pool.get('ir.model').browse(cr, 1, x2m_model_id)
                # the resource_ids that need to be checked are the sum of both old and previous values (because we
                # need to log also creation or deletion in those lists).
                x2m_old_values_ids = old_values.get(key, {'value': {}})['value'].get(field_name, [])
                x2m_new_values_ids = new_values.get(key, {'value': {}})['value'].get(field_name, [])
                # We use list(set(...)) to remove duplicates.
                res_ids = list(set(x2m_old_values_ids + x2m_new_values_ids))
                for res_id in res_ids:
                    lines.update(self.prepare_audittrail_log_line(cr, 1, pool, x2m_model, res_id, method, old_values, new_values, field_list))
        # if the value value is different than the old value: record the change
        if key not in old_values or key not in new_values or old_values[key]['value'][field_name] != new_values[key]['value'][field_name]:
            data = {
                  'name': field_name,
                  'new_value': key in new_values and new_values[key]['value'].get(field_name),
                  'old_value': key in old_values and old_values[key]['value'].get(field_name),
                  'new_value_text': key in new_values and new_values[key]['text'].get(field_name),
                  'old_value_text': key in old_values and old_values[key]['text'].get(field_name)
            }
            lines[key].append(data)
    return lines


audittrail_objects_proxy.log_fct = _log_fct
audittrail_objects_proxy.check_rules = _check_rules
audittrail_objects_proxy.process_data = _process_data
audittrail_objects_proxy.get_data = _get_data
audittrail_objects_proxy.get_value_text = _get_value_text
audittrail_objects_proxy.execute_cr = _execute_cr
audittrail_objects_proxy.exec_workflow_cr = _exec_workflow_cr
audittrail_objects_proxy.prepare_audittrail_log_line = _prepare_audittrail_log_line
