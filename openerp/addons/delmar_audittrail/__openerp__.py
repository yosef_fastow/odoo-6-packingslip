{
    "name": "delmar_audittrail",
    "version": "0.1",
    "author": "Ivan Burlutskiy @ Prog-Force",
    "website": "http://www.progforce.com/",
    "depends": [
        "audittrail",
    ],
    "description": """
Add domain (conditions) to audittrail rules to log only specific records.
For exaple, we need log only OS orders, so just need to update domain to "[('real_partner_id', '=', 'OS')]".
But there is a constraint "unique (object_id)". So we can create only 1 rule for a model.
    """,
    "category": "Project Management",
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'view/audittrail_view.xml',
    ],
    "application": True,
    "active": False,
    "installable": True,
}
