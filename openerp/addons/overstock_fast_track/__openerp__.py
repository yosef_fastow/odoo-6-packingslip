# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP Module
#
#    Copyright (C) 1996-2012 Pokémon.
#    Author Yuri Ivanenko
#
##############################################################################
{
    "name": "Overstock fast track",
    "version": "1.0",
    "depends": ["base", "base_tools", "fetchdb", "pf_utils"],
    "author": "Yuri Ivanenko, progforce.com",
    "category": "Tools",
    "description": """
    Overstock fast track.
    """,
    "init_xml": [],
    'update_xml': [
        "view/overstock_fast_track.xml",
        "data/overstock_fast_track_server_data.xml",
    ],
    'js': [
        'static/src/js/overstock_fast_track.js',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'qweb' : [
        "static/src/xml/*.xml",
    ],
}