# -*- coding: utf-8 -*-
import pyodbc
from osv import osv, fields
from tools.translate import _
import xlrd, xlwt
from cStringIO import StringIO
import base64
from ftplib import FTP
import re
import datetime

STYLES_COL = {"red":"pattern: pattern solid, fore_colour 10; font: name Arial, bold True, height 240;",
              "green":"pattern: pattern solid, fore_colour 12; font: name Arial, bold True, height 240;",
              "blue":"pattern: pattern solid, fore_colour 17; font: name Arial, bold True, height 240;",
              "purple":"pattern: pattern solid, fore_colour 25; font: name Arial, bold True, height 240;",
              "orange":"pattern: pattern solid, fore_colour 53; font: name Arial, bold True, height 240;",
              "arial":"font: name Arial, height 200;",
              "calibri":"font: name Calibri, height 220;"}

NAMES_COL = ["TYPE",
             "Option Count",
             "Product Name",
             "Supplier SKU",
             "Description",
             "Materials",
             "Option Description",
             "Supplier Cost",
             "Final Site Price",
             "Quantity on Hand",
             "UPC",
             "",
             "Cost Include Shipping?",
             "Quality",
             "Replenishable",
             "Lead Time (days)",
             "Date Available",
             "Type of Product Comparison",
             "Compare at Price (High Street Price)",
             "Compare at URL",
             "Does Compare at Price Include Shipping",
             "Low Street Price",
             "Low Street Price URL",
             "Compare at Comments",
             "Model/Style#",
             "Ship Box Weight",
             "Ship Box Length",
             "Ship Box Width",
             "Ship Box Height",
             "Ship from ZIP Code",
             "Number of Boxes",
             "Additional Ship Box Dimensions",
             "Additional Ship Box Weights",
             "",
             "",
             "",
             "Additional Ship from ZIP Codes",
             "Buyer Name",
             "Category",
             "",
             "New Note",
             "",
             "Replacement Overstock SKU",
             "Manufacturer Name",
             "Manufacturer Part No.",
             "Brand Name",
             "Country of Origin",
             "Sell Internationally",
             "Description Summary",
             "Warranty Type",
             "Warranty Company",
             "Warranty Phone",
             "Warranty Length",
             "ISBN Number",
             "Release Date",
             "Product Dimensions",
             "Product Weight",
             "Quantity on Order",
             "Estimated Arrival Date",
             "Will this item ship via LTL freight carrier to Overstock.com customers?",
             "Shipping Restrictions?"]

NAMES_OLD = {2:"Description",
             7:"OS_Cost",
             8:"cOS_SuggestedPrice",
             4:"db1_prod_note::description_ Overstock",
             48:"Summary",
             26:"box_length",
             27:"box_width",
             28:"box_height",
             55:"dimensions",
             45:"Brand_Name",
             5:"Materials",
             37:"Buyer",
             10:"UPC_Code::Prod_EAN_13digits",
             40:"New_note"}

CUSTOMS_COL = ["Custom Field 1",
               "Custom Field 2",
               "Custom Field 3",
               "Custom Field 4"]

TYPE_COL = ["",
            "Master",
            "Master",
            "Option",
            "Master",
            "Master",
            "Option",
            "Option",
            "Option",
            "Option",
            "Option",
            "",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Option",
            "Option",
            "Option",
            "Option",
            "Master",
            "Option",
            "Option",
            "Option",
            "",
            "",
            "",
            "Master",
            "Master",
            "Master",
            "",
            "Master",
            "",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Master",
            "Option",
            "Option",
            "Option",
            "Option",
            "Master"]

#prin id for all images 1-20
HTTPS_URL = ["http://www.delmarimage.com/IMAGES/"]

class overstock_fast_track(osv.osv):
    _name = "overstock.fast.track"

    _columns = {
        "create_uid": fields.many2one('res.users', 'Creator'),
        "create_date": fields.datetime('Creation date', readonly=True),
        "since_date": fields.date('Since date', readonly=True, store=False),
        "to_date": fields.date('To date', readonly=True, store=False),
        "input_file": fields.binary(string="Input file", required=True),
        "input_filename": fields.char("", size=256),
        "output_file": fields.binary(string="Output file", readonly=True),
        "output_filename": fields.char("", size=256),
        "state": fields.selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('done', 'Done')], 'Status', select=True, required=True, readonly=True),
    }

    _defaults = {
        'state': 'draft',
    }

    def make_ossku1_query(self, cr, uid, server_id, sql):
        result = []
        if ('INSERT' in sql) | ('UPDATE' in sql):
            result = self.pool.get('fetchdb.server').make_query(cr, uid, server_id, sql, False, True)
        else:
            result = self.pool.get('fetchdb.server').make_query(cr, uid, server_id, sql)
        return result

    def excel_parsing(self, cr, uid, ids, input_file, database_name):

        #####################################################
        #                                                   #
        #  Functions definitions                            #
        #                                                   #
        #####################################################

        style_red = xlwt.easyxf(STYLES_COL["red"])
        style_green = xlwt.easyxf(STYLES_COL["green"])
        style_blue = xlwt.easyxf(STYLES_COL["blue"])
        style_purple = xlwt.easyxf(STYLES_COL["purple"])
        style_orange = xlwt.easyxf(STYLES_COL["orange"])
        style_arial = xlwt.easyxf(STYLES_COL["arial"])
        style_calibri = xlwt.easyxf(STYLES_COL["calibri"])

        def copy_cell(row_index, col_index, value_cell, style):

            if (row_index==2)and(value_cell!=''):
                write_sheet.write(row_index, col_index, value_cell, style)
                return True
            elif(row_index<2)and(value_cell!=''):
                write_sheet.write(row_index, col_index, value_cell, style_arial)
                return True
            elif (value_cell==''):
                return False
            else:
                write_sheet.write(row_index, col_index, value_cell, style_calibri)
                return True

        def find_on_ftp():
            result = []
            ftp = FTP('ftp.delmarimage.com','delmarimages' ,'JdelS614' )
            ftp.cwd("Images")
            ftp.retrlines('NLST', result.append)
            return result

        def find_col_index(colname,row_find=2):
            for col_index in range(80):
                if read_sheet.cell_value(row_find, col_index)==colname:
                    return col_index
            return -1

        def find_row_index(colname):
            return_array = {}
            for row_index in xrange(3,read_sheet.nrows-1):
                if read_sheet.cell_value(row_index, find_col_index(colname))!="":
                    return_array[row_index+1] = read_sheet.cell_value(row_index, find_col_index(colname))
            return return_array

        def getProductName(row_index):
            result = read_sheet.cell_value(row_index, find_col_index("Description", 0))
            if result == '':
                result = read_sheet.cell_value(row_index, find_col_index("db1__prod::prod_description_line", 0))
            return result

        def getDescription(row_index):
            result = read_sheet.cell_value(row_index, find_col_index("db1_prod_note::description_ Overstock", 0))
            if result == '':
                result = read_sheet.cell_value(row_index, find_col_index("db1__prod::prod_description_line", 0))
            return result

        def getMaterials(row_index):
            result = read_sheet.cell_value(row_index, find_col_index("Materials", 0))
            if result == '':
                result = '*'
            return result

        def getReplenishable(row_index):
            Replenishable = read_sheet.cell_value(row_index,find_col_index("DELMAR Status", 0))
            if (Replenishable.upper()=='SHOWROOM') or ('DNR' in Replenishable.upper()):
                return "No"
            else:
                return "Yes"

        def getCategoty(row_index):
            return "Jewelry & Watches\\Jewelry\\"+read_sheet.cell_value(row_index,find_col_index("Field1", 0)).replace("/","\\")

        def getCountry(row_index):
            str = read_sheet.cell_value(row_index,find_col_index("SupplierCountry", 0)).replace("HONG KONG","HONG KONG (Special Administrative Region of China)")
            return str.replace("USA" ,"UNITED STATES")

        def getDimensions(row_index):
            result = read_sheet.cell_value(row_index,find_col_index("dimensions", 0))
            if result == '':
                result = 0
            return result

        def getSupplierCost(row_index):
            result = convertToFloat(read_sheet.cell_value(row_index, find_col_index("OS_Cost",0)))
            if result == '':
                result = 'Error'
            return result

        def convertToFloat(number):
            try:
                return round(float(number),2)
            except:
                return ''

        def getBrandName(row_index):
            result = read_sheet.cell_value(row_index+indexSetFlag, find_col_index("Brand_Name",0))
            if result == '' or result == '*':
                result = 'None'
            return result

        def getID_Delmar(row_index):
            return read_sheet.cell_value(row_index,find_col_index("OS_Vs_DelmarID", 0))

        def getOssku1(DelmarID, size = True, server = 1):
            try:
                option_object_key = DelmarID
                if(size):
                    option_object_key = DelmarID.split("-")[0]

                if(option_object_key != DelmarID):
                    option_object_key = "%s-%%" % option_object_key
                if DelmarID == "":
                    return [[('error DATABASE'),],]
                else:
                    customerProductIDs = self.make_ossku1_query(cr, uid, server, "SELECT CustomerProductID FROM dbo.CustomerProductIDs WHERE DelmarProductID LIKE '" + str(option_object_key) + "' AND CustomerID = 'OS'")
                    if len(customerProductIDs) == 0:
                        self.make_ossku1_query(cr, uid, server,"INSERT INTO dbo.CustomerProductIDs (DelmarProductID, CustomerID, CustomerProductID) VALUES ( '" + str(DelmarID) + "','OS','a')")
                        customerProductID = self.make_ossku1_query(cr, uid, server,"SELECT * FROM dbo.CustomerProductIDs where DelmarProductID = '"+str(DelmarID)+"' AND CustomerID = 'OS'")
                        customerProductID.CustomerProductID = 'OS' + str(customerProductID.PK_ID)
                        self.make_ossku1_query(cr, uid, server,"UPDATE dbo.CustomerProductIDs SET CustomerProductID = '"+str(customerProductID.CustomerProductID)+"' WHERE DelmarProductID = '" + str(DelmarID) + "' AND CustomerID = 'OS'")
                        customerProductIDs = [(customerProductID.CustomerProductID, )]
                    return customerProductIDs
            except:
                return [[('error DATABASE connection'),],]

        #####################################################
        #                                                   #
        #  Main code                                        #
        #                                                   #
        #####################################################

        # create output Excel file
        write_book = xlwt.Workbook()
        write_sheet = write_book.add_sheet('Sheet 1', cell_overwrite_ok=True)

        decode_input_file = base64.decodestring(input_file)
        read_book = xlrd.open_workbook(file_contents=decode_input_file)  # open source documents
        read_sheet = read_book.sheet_by_index(0)  # read from first list

        col_index = 0
        copy_cell(0, 0, str(int(read_sheet.cell_value(1, 0))),style_arial)
        style_col = style_arial

        for name_col in TYPE_COL:
            copy_cell(1, col_index, name_col, style_col)
            col_index += 1

        style_col = style_red
        col_index = 0

        for name_col in NAMES_COL:
            if name_col=="Description":
                style_col = style_blue
            if name_col=="UPC":
                style_col = style_green
            if name_col=="Cost Include Shipping?":
                style_col = style_blue
            if name_col=="Lead Time (days)":
                style_col = style_orange
            if name_col=="Date Available":
                style_col = style_blue
            if name_col=="Compare at Price (High Street Price)":
                style_col = style_orange
            if name_col=="Model/Style#":
                style_col = style_blue
            if name_col=="Additional Ship Box Dimensions":
                style_col = style_purple
            if name_col=="Buyer Name":
                style_col = style_blue
            if name_col=="New Note":
                style_col = style_purple
            if name_col=="Manufacturer Name":
                style_col = style_green
            if name_col=="Warranty Company":
                style_col = style_orange
            if name_col=="ISBN Number":
                style_col = style_purple
            if name_col=="Product Dimensions":
                style_col = style_green
            if name_col=="Quantity on Order":
                style_col = style_purple
            if name_col=="Will this item ship via LTL freight carrier to Overstock.com customers?":
                style_col = style_blue
            if name_col=="Shipping Restrictions?":
                style_col = style_orange
            copy_cell(2, col_index, name_col, style_col)
            col_index = col_index + 1

        style_col = style_calibri
        option_count = 0
        row_index_write = 1
        dt = str(datetime.datetime.now().month)+"/"+str(datetime.datetime.now().day)+"/"+str(datetime.datetime.now().year)

        FTP_FILE_LIST = []
        FTP_FILE_LIST = find_on_ftp()

        FTP_FILE_STRING = ['+'.join(FTP_FILE_LIST)]
        FTP_FILE_STRING = FTP_FILE_STRING[0]

        if database_name[0] == False:
            models_data = self.pool.get('ir.model.data')
            get_server = models_data.get_object_reference(cr, uid, 'overstock_fast_track', 'overstock_fast_track_server_data_id')[1]
        else:
            get_server = database_name[0]

        option_object = {}
        count_second_option = {}

        number_of_second_option = 1

        row_index = 1
        last_row = False
        have_set_id = False
        #writing TYPE and Option Count col. adding new row for options
        while row_index < read_sheet.nrows:
            if(row_index == read_sheet.nrows - 1):
                last_row = True
            string_options = read_sheet.cell_value(row_index, find_col_index("Options",0))
            
            if(string_options == ""):
                string_options = []
            else:
                string_options = string_options.split(",")

            ID_Delmar = getID_Delmar(row_index)
            Set_ID = read_sheet.cell_value(row_index, find_col_index("Set_ID",0))
            option_object_key = ""
            option_object_key = ID_Delmar.split("-")[0]
            current_is_option = False

            if ID_Delmar == '':
                row_index += 1
                continue

            getOssku1_text = getOssku1(ID_Delmar, False, get_server)[0]

            if(option_object_key not in option_object):
                option_object[option_object_key] = string_options
                count_second_option[option_object_key] = 0
            elif (len(option_object[option_object_key]) == 0 and len(string_options) == 0): 
                current_is_option = True
                count_second_option[option_object_key] += 1
            elif (len(option_object[option_object_key]) == 0 and len(string_options) > 0):
                option_object[option_object_key] = string_options
            elif(len(option_object[option_object_key]) > 0 and len(string_options) == 0):
                string_options = option_object[option_object_key]
            elif(len(option_object[option_object_key]) > 0 and len(string_options) > 0):
                string_options = list(set(option_object[option_object_key] + string_options))

            current_is_option_from_product = False

            prewSet_ID = read_sheet.cell_value(row_index-1, find_col_index("Set_ID",0))

            #writing date aboute things is has one option
            if(((len(string_options) == 0 and (current_is_option == False))) and ((Set_ID != prewSet_ID)and(Set_ID != ''))) or ((len(string_options) == 0 and (current_is_option == False)) and (Set_ID == '')):
                indexSetFlag = 0
                try:
                    nextSet_ID = read_sheet.cell_value(row_index, find_col_index("Set_ID",0))
                    dont_have_set_flag = True
                    if last_row!=True:
                        while (Set_ID == nextSet_ID)and(Set_ID != ''):
                            if read_sheet.cell_value(row_index+indexSetFlag, find_col_index("SetFlag", 0)) == 1:
                                dont_have_set_flag = False
                                break
                            indexSetFlag = indexSetFlag+1
                            try:
                                nextSet_ID = read_sheet.cell_value(row_index+indexSetFlag, find_col_index("Set_ID",0))
                            except:
                                nextSet_ID = ''
                        if dont_have_set_flag:
                            indexSetFlag = 0
                except:
                    pass

                copy_cell(row_index_write+2, 0, "M",style_col)
                copy_cell(row_index_write+2, 1, 1,style_col)
                copy_cell(row_index_write+2, 12, "Yes",style_col)
                copy_cell(row_index_write+2, 13, "New",style_col)
                copy_cell(row_index_write+2, 14, getReplenishable(row_index+indexSetFlag),style_col)
                copy_cell(row_index_write+2, 15, 45,style_col)
                copy_cell(row_index_write+2, 16, dt, style_col)
                copy_cell(row_index_write+2, 17, "NONE_AVAILABLE",style_col)
                copy_cell(row_index_write+2, 29, "12919",style_col)
                copy_cell(row_index_write+2, 24, "*",style_col)
                copy_cell(row_index_write+2, 38, getCategoty(row_index+indexSetFlag),style_col)
                copy_cell(row_index_write+2, 40, read_sheet.cell_value(row_index+indexSetFlag, find_col_index("New_note",0)),style_col)
                copy_cell(row_index_write+2, 43, 3077675,style_col)
                copy_cell(row_index_write+2, 44, "*",style_col)
                copy_cell(row_index_write+2, 46, getCountry(row_index+indexSetFlag), style_col)
                copy_cell(row_index_write+2, 47, "Yes",style_col)
                copy_cell(row_index_write+2, 49, "NO_WARRANTY",style_col)
                copy_cell(row_index_write+2, 2, getProductName(row_index+indexSetFlag), style_col)
                copy_cell(row_index_write+2, 4, getDescription(row_index+indexSetFlag), style_col)
                copy_cell(row_index_write+2, 5, getMaterials(row_index+indexSetFlag), style_col)
                copy_cell(row_index_write+2, 37, read_sheet.cell_value(row_index+indexSetFlag, find_col_index(NAMES_OLD[37],0)),style_col)
                copy_cell(row_index_write+2, 45, getBrandName(row_index+indexSetFlag),style_col)
                copy_cell(row_index_write+2, 48, read_sheet.cell_value(row_index+indexSetFlag, find_col_index(NAMES_OLD[48],0)),style_col)
                copy_cell(row_index_write+2, 55, getDimensions(row_index+indexSetFlag),style_col)

                nextID = ""

                if last_row != True:
                    nextID = getID_Delmar(row_index+1)
                    nextSet_ID = read_sheet.cell_value(row_index+1, find_col_index("Set_ID",0))

                if ((option_object_key == nextID.split("-")[0]) or (Set_ID == nextSet_ID)) and (Set_ID != ""):
                    current_is_option = True
                    current_is_option_from_product = True
                    count_second_option[option_object_key] += 1
                else:
                    copy_cell(row_index_write+2, 9, 1,style_col)
                    copy_cell(row_index_write+2, 6, "*",style_col)
                    copy_cell(row_index_write+2, 7, getSupplierCost(row_index), style_col)
                    copy_cell(row_index_write+2, 8, convertToFloat(read_sheet.cell_value(row_index, find_col_index("cOS_SuggestedPrice",0))),style_col)
                    copy_cell(row_index_write+2, 9, 1,style_col)
                    copy_cell(row_index_write+2, 10, read_sheet.cell_value(row_index, find_col_index("UPC_Code::Prod_EAN_13digits",0)),style_col)
                    copy_cell(row_index_write+2, 25, 1,style_col)
                    copy_cell(row_index_write+2, 26, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_length",0))),style_col)
                    copy_cell(row_index_write+2, 27, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_width",0))),style_col)
                    copy_cell(row_index_write+2, 28, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_height",0))),style_col)
                    copy_cell(row_index_write+2, 30, "1",style_col)
                    copy_cell(row_index_write+2, 56, 1,style_col)
                    copy_cell(row_index_write+2, 59, "No",style_col)
                    copy_cell(row_index_write+2, 3, getOssku1_text[0], style_col)

                x=61

                images_valid_url = []
                if (Set_ID != '') and (Set_ID != ID_Delmar):
                    images_valid_url = re.findall(r'(?i)'+Set_ID+'\.jpg', FTP_FILE_STRING )

                if Set_ID =='':
                    images_valid_url = re.findall(r'(?i)'+ID_Delmar+'_[a]{,}[ao]{,}[b]{,}[bo]{,}[c]{,}[c1]{,}[co]{,}[mlb]{,}\.jpg', FTP_FILE_STRING )
                else:
                    images_valid_url = images_valid_url + re.findall(r'(?i)'+ID_Delmar+'[_a-z]{,5}\.jpg', FTP_FILE_STRING )

                if ID_Delmar == "":
                    images_valid_url = [] 
                for image_valid_url in images_valid_url:
                    copy_cell(row_index_write+2, x, HTTPS_URL[0]+image_valid_url, style_calibri)
                    x = x + 2
                    if x>98:break;

                if last_row != True:
                    nextID = getID_Delmar(row_index+1)
                    nextSet_ID = read_sheet.cell_value(row_index+1, find_col_index("Set_ID",0))
                    index = 1
                    while (option_object_key == nextID.split("-")[0]) or (Set_ID == nextSet_ID) and (Set_ID != ''):
                        images_valid_url = re.findall(r'(?i)'+nextID+'[_a-z]{,5}\.jpg', FTP_FILE_STRING )
                        if nextID == "":
                             images_valid_url = []
                        for image_valid_url in images_valid_url:
                            copy_cell(row_index_write+2, x, HTTPS_URL[0]+image_valid_url, style_calibri)
                            x = x + 2
                            if x>98:break;

                        index = index+1
                        try:
                            nextID = getID_Delmar(row_index+index)
                            nextSet_ID = read_sheet.cell_value(row_index+index, find_col_index("Set_ID",0))
                        except:
                            nextID = ''
                            nextSet_ID = ''

            if (((read_sheet.cell_value(row_index, find_col_index("SetFlag",0))==1) or len(string_options) > 0) and current_is_option_from_product == False) and ((Set_ID != prewSet_ID)or(Set_ID == '')):
                if len(string_options) > 0:

                    indexSetFlag = 0
                    try:
                        nextSet_ID = read_sheet.cell_value(row_index, find_col_index("Set_ID",0))
                        if last_row!=True:
                            dont_have_set_flag = True
                            while (Set_ID == nextSet_ID)and(Set_ID != ''):
                                if read_sheet.cell_value(row_index+indexSetFlag, find_col_index("SetFlag", 0)) == 1:
                                    dont_have_set_flag = False
                                    break
                                indexSetFlag = indexSetFlag+1
                                try:
                                    nextSet_ID = read_sheet.cell_value(row_index+indexSetFlag, find_col_index("Set_ID",0))
                                except:
                                    nextSet_ID = ''
                            if dont_have_set_flag:
                                indexSetFlag = 0
                    except:
                        pass

                    copy_cell(row_index_write+2, 0, "M",style_col)
                    copy_cell(row_index_write+2, 2, getProductName(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 4, getDescription(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 5, getMaterials(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 12, "Yes",style_col)
                    copy_cell(row_index_write+2, 13, "New",style_col)
                    copy_cell(row_index_write+2, 14, getReplenishable(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 15, 45, style_col)
                    copy_cell(row_index_write+2, 16, dt, style_col)

                    copy_cell(row_index_write+2, 17, "NONE_AVAILABLE",style_col)
                    copy_cell(row_index_write+2, 24, "*",style_col)
                    copy_cell(row_index_write+2, 29, "12919",style_col)
                    copy_cell(row_index_write+2, 37, read_sheet.cell_value(row_index+indexSetFlag, find_col_index(NAMES_OLD[37],0)),style_col)
                    copy_cell(row_index_write+2, 38, getCategoty(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 40, read_sheet.cell_value(row_index+indexSetFlag, find_col_index("New_note",0)),style_col)
                    copy_cell(row_index_write+2, 43, 3077675,style_col)
                    copy_cell(row_index_write+2, 44, "*",style_col)
                    copy_cell(row_index_write+2, 45, getBrandName(row_index+indexSetFlag),style_col)
                    copy_cell(row_index_write+2, 46, getCountry(row_index+indexSetFlag), style_col)
                    copy_cell(row_index_write+2, 47, "Yes",style_col)
                    copy_cell(row_index_write+2, 48, read_sheet.cell_value(row_index+indexSetFlag, find_col_index(NAMES_OLD[48],0)),style_col)
                    copy_cell(row_index_write+2, 49, "NO_WARRANTY",style_col)
                    copy_cell(row_index_write+2, 55, getDimensions(row_index+indexSetFlag),style_col)

                    x=61

                    images_valid_url = []
                    if (Set_ID != '') and (Set_ID != ID_Delmar):
                        images_valid_url = re.findall(r'(?i)'+Set_ID+'\.jpg', FTP_FILE_STRING )

                    if Set_ID =='':
                        images_valid_url = re.findall(r'(?i)'+ID_Delmar+'_[a]{,}[ao]{,}[b]{,}[bo]{,}[c]{,}[c1]{,}[co]{,}[mlb]{,}\.jpg', FTP_FILE_STRING )
                    else:
                        images_valid_url = images_valid_url + re.findall(r'(?i)'+ID_Delmar+'[_a-z]{,5}\.jpg', FTP_FILE_STRING )

                    if ID_Delmar == "":
                        images_valid_url = [] 
                    for image_valid_url in images_valid_url:
                        copy_cell(row_index_write+2, x, HTTPS_URL[0]+image_valid_url, style_calibri)
                        x = x + 2
                        if x>98:break;

                    option_count_sum = 0
                    customerProductIDs = []
                    customerProductIDs.append(getOssku1_text)

                    options_object = {}
                    if ID_Delmar != '':
                        options_object[getOssku1_text[0]] = 0
                    if last_row != True:
                        nextID = getID_Delmar(row_index+1)
                        nextSet_ID = read_sheet.cell_value(row_index+1, find_col_index("Set_ID",0))
                        index = 1
                        while (Set_ID == nextSet_ID) and (Set_ID != ''):
                            getOssku1_next = getOssku1(nextID, False, get_server)[0]
                            customerProductIDs.append(getOssku1_next)
                            options_object[getOssku1_next[0]] = index
                            images_valid_url = re.findall(r'(?i)'+nextID+'[_a-z]{,5}\.jpg', FTP_FILE_STRING )
                            if nextID == "":
                                 images_valid_url = []

                            for image_valid_url in images_valid_url:
                                copy_cell(row_index_write+2, x, HTTPS_URL[0]+image_valid_url, style_calibri)
                                x = x + 2
                                if x>98:break;

                            index = index+1
                            try:
                                nextID = getID_Delmar(row_index+index)
                                nextSet_ID = read_sheet.cell_value(row_index+index, find_col_index("Set_ID",0))
                            except:
                                nextID = ''
                                nextSet_ID = ''

                    index = 0
                    copy_cell(row_index_write+2, 1, len(string_options)*len(customerProductIDs),style_col)
                    for customerProductID in customerProductIDs:
                        add_index = 0

                        if customerProductID[0] in options_object:
                            add_index = options_object[customerProductID[0]]
                        elif(len(options_object) > 0):
                            continue

                        try:
                            string_options_next = read_sheet.cell_value(row_index+add_index, find_col_index("Options",0))
                            if string_options_next != '':
                                string_options = string_options_next.split(",")
                        except:
                            pass

                        for x in range(len(string_options)):
                            row_index_write = row_index_write + 1
                            option_count_sum = index + x + 1
                            opt = "option %d" % option_count_sum

                            matherial = read_sheet.cell_value(row_index+add_index, find_col_index("SET_Options",0))

                            size_str = str(matherial) + " Size %s" % string_options[x]

                            upc = read_sheet.cell_value(row_index+add_index, find_col_index(NAMES_OLD[10],0))

                            upc = "0" + str(upc[4:12]) + ('000' + str(int(float(string_options[x]) * 10)))[-3:]

                            upc_odd_sum = 0
                            upc_even_sum = 0
                            for upc_index in range(len(upc)):
                                if upc_index%2 == 0:
                                    upc_even_sum += int(upc[upc_index])
                                else:
                                    upc_odd_sum += int(upc[upc_index])
                            upc_last_num = (upc_even_sum + (upc_odd_sum * 3))%10
                            upc_last_num = 10 - upc_last_num
                            upc_last_num = str(upc_last_num)
                            upc = upc + upc_last_num[len(upc_last_num)-1]

                            try:
                                ssku1 = "%s/%s" % (customerProductID.CustomerProductID, string_options[x])
                            except:
                                if customerProductID[0] != 'error DATABASE connection':
                                    ssku1 = "%s/%s" % (customerProductID[0], string_options[x])
                                else:
                                    ssku1 = customerProductID[0]

                            copy_cell(row_index_write+2, 3, ssku1, style_col)

                            copy_cell(row_index_write+2, 7, getSupplierCost(row_index+add_index), style_col)
                            copy_cell(row_index_write+2, 8, convertToFloat(read_sheet.cell_value(row_index+add_index, find_col_index("cOS_SuggestedPrice",0))),style_col)
                            copy_cell(row_index_write+2, 9, 1,style_col)
                            copy_cell(row_index_write+2, 10, upc,style_col)

                            copy_cell(row_index_write+2, 25, 1,style_col)
                            copy_cell(row_index_write+2, 26, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_length",0))),style_col)
                            copy_cell(row_index_write+2, 27, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_width",0))),style_col)
                            copy_cell(row_index_write+2, 28, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_height",0))),style_col)
                            copy_cell(row_index_write+2, 30, "1",style_col)
                            copy_cell(row_index_write+2, 0, "O",style_col)
                            copy_cell(row_index_write+2, 1, opt, style_col)
                            copy_cell(row_index_write+2, 6, size_str, style_col)
                            copy_cell(row_index_write+2, 56, 1,style_col)
                            copy_cell(row_index_write+2, 59, "No",style_col)

                        index +=len(string_options)

                    row_index = row_index + len(options_object) - 1
                    copy_cell(row_index_write-option_count_sum+2, 1, option_count_sum, style_col)

            elif(current_is_option) or ((Set_ID == prewSet_ID) and (ID_Delmar != '') and (Set_ID != '')):

                if (current_is_option_from_product):
                    row_index_write += 1
                option_count_sum = 0
                customerProductIDs = []
                customerProductIDs.append(getOssku1_text)

                if last_row != True:
                    nextID = getID_Delmar(row_index+1)
                    nextSet_ID = read_sheet.cell_value(row_index+1, find_col_index("Set_ID",0))
                    index = 1
                    while (Set_ID == nextSet_ID)and(Set_ID != ''):
                        customerProductIDs.append(getOssku1(nextID, False, get_server)[0])
                        index = index+1
                        try:
                            nextID = getID_Delmar(row_index+index)
                            nextSet_ID = read_sheet.cell_value(row_index+index, find_col_index("Set_ID",0))
                        except:
                            nextID = ''
                            nextSet_ID = ''

                prewSet_ID = read_sheet.cell_value(row_index-1, find_col_index("Set_ID",0))
                if Set_ID == prewSet_ID:
                    number_of_second_option += 1
                else:
                    number_of_second_option = 1

                # for customerProductID in customerProductIDs:
                copy_cell(row_index_write+2, 0, "O",style_col)
                copy_cell(row_index_write+2, 1, "option %d" % number_of_second_option,style_col)
                copy_cell(row_index_write+2, 3, getOssku1_text[0], style_col)
                copy_cell(row_index_write+2, 6, read_sheet.cell_value(row_index, find_col_index("SET_Options",0)), style_col)
                copy_cell(row_index_write+2, 7, getSupplierCost(row_index), style_col)
                copy_cell(row_index_write+2, 8, convertToFloat(read_sheet.cell_value(row_index, find_col_index("cOS_SuggestedPrice",0))) ,style_col)
                copy_cell(row_index_write+2, 9, 1,style_col)
                copy_cell(row_index_write+2, 10, read_sheet.cell_value(row_index, find_col_index(NAMES_OLD[10],0)),style_col)
                copy_cell(row_index_write+2, 25, 1,style_col)
                copy_cell(row_index_write+2, 26, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_length",0))),style_col)
                copy_cell(row_index_write+2, 27, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_width",0))), style_col)
                copy_cell(row_index_write+2, 28, convertToFloat(read_sheet.cell_value(row_index, find_col_index("box_height",0))), style_col)
                copy_cell(row_index_write+2, 30, "1",style_col)
                copy_cell(row_index_write+2, 56, 1,style_col)
                copy_cell(row_index_write+2, 59, "No",style_col)

                if (current_is_option_from_product):
                    copy_cell(row_index_write+2-1, 1, number_of_second_option,style_col)
                else:
                    copy_cell(row_index_write+2-number_of_second_option, 1, number_of_second_option,style_col)

            row_index_write += 1
            row_index += 1

        # print image1-20
        for x in xrange(2,42,2):
            copy_cell(1, x+col_index-2, "Master", style_arial)
            copy_cell(2, x+col_index-2, "Image"+str(x/2),style_purple)

        col_index = 101

        # print custom fields 1-4
        for custom_col in CUSTOMS_COL:
            copy_cell(1, col_index, "Master", style_arial)
            copy_cell(2, col_index, custom_col, style_purple)
            col_index = col_index + 1

        #####################################################
        #                                                   #
        #   return result                                   #
        #                                                   #
        #####################################################

        fp = StringIO()
        write_book.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()
        return base64.encodestring(data)


    def confirm(self, cr, uid, ids, *args):
        cur = self.browse(cr, uid, ids[0])
        if cur.input_file:
            try:
                database_settings = self.pool.get('overstock.fast.track.settings')
                database_id = database_settings.get_settings(cr, uid)
            except Exception, e:
                raise osv.except_osv('Cannot to get settings of database', '')
                return False
            try:
                result_file = self.excel_parsing(cr, uid, ids, cur.input_file, database_id)
            except Exception, e:
                raise osv.except_osv('Cannot convert this file', '')
                return False

            self.write(cr, uid, ids, {'state': 'confirm', 'output_file': result_file, 'output_filename': 'result_' + str(ids[0]) + '.xls', 'input_filename': 'input_' + str(ids[0]) + '.xls'})
            return True
        else:
            raise osv.except_osv('You not choose the file', '')
            return False

    def search_args_since_date(self, cr, user, args, context={}):
        try:
            since_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'since_date')).next()
            since_date_index = args.index(since_date_arg)
            args[since_date_index][0] = 'create_date'
            args[since_date_index][1] = '>'
            return args
        except StopIteration:
            return args

    def search_args_to_date(self, cr, user, args, context={}):
        try:
            to_date_arg = (arg for arg in args if (isinstance(arg, list) and arg[0] == 'to_date')).next()
            to_date_index = args.index(to_date_arg)
            args[to_date_index] = ['create_date', '<=', args[to_date_index][2] + ' 24:00:00']
            return args
        except StopIteration:
            return args

    def search(self, cr, user, args, offset=0, limit=None, order=None, context={}, count=False):
        args = self.search_args_since_date(cr, user, args, context=context)
        args = self.search_args_to_date(cr, user, args, context=context)
        args.append(['state','=','confirm'])
        return super(overstock_fast_track, self).search(cr, user, args, offset=offset, limit=limit, order=order, context=context, count=count)

overstock_fast_track()

class overstock_fast_track_settings(osv.osv):
    _name = "overstock.fast.track.settings"

    _columns = {
        "name": fields.char('Name', size=256, required=True),
        "create_date": fields.datetime('Creation date', readonly=True),
        "selected": fields.boolean('Selected', readonly=True),
        "database_name": fields.many2one('fetchdb.server', 'Select name of database', required=True),
    }

    _defaults = {
    "name": "default name",
    "selected": False,
    }

    def get_settings(self, cr, uid):
        ids = self.search(cr, uid, [('selected','=','True')])
        return self.read(cr, uid, ids[0])['database_name']

    def select_settings(self, cr, uid, ids, *args):
        cur = self.browse(cr, uid, ids[0])
        try:
            try:
                self.write(cr, uid, self.search(cr, uid, [('selected','=','True')])[0], {'selected': False })
                self.write(cr, uid, ids[0], {'selected': True })
            except Exception, e:
                self.write(cr, uid, ids[0], {'selected': True })
            return True
        except:
            raise osv.except_osv('Error of saving settings. Cannot to select it', '')
            return False

overstock_fast_track_settings()