Delmar Portal Scripts
===

This folder contains scripts that are required on servers.
It's devided by enviroments.

#### Production

**auto_start_openerp_server.sh** - Self-resurrecting openerp server. API server. [disabled now]
**auto_start_pgagent.sh** - Self-resurrecting PGAGENT. API server.

**restart** - Restart PRODUCTION Delmar OpenERP script.
**restart_api** - Restart API Delmar OpenERP script.
**stop** - Stop PRODUCTION Delmar OpenERP script.
**stop_api** - Stop PRODUCTION Delmar OpenERP script.

#### Pre-Production

**restart** - Restart PRE-PRODUCTION Delmar OpenERP script.
**stop** - Stop PRE-PRODUCTION Delmar OpenERP script.

#### Development

**restart** - Restart DEVELOPMENT Delmar OpenERP script.
**stop** - Stop DEVELOPMENT Delmar OpenERP script.
