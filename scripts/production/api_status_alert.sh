#!/bin/bash
mail="a.fomin@progforce.com"
# ip="localhost"
# port="2042"
ip="192.168.100.42"
port="22"
ssh_query="ssh delmar@$ip -p $port"
# mail="delmar@progforce.com"
cron=`$ssh_query "ps aux | grep -w -c '\<nohup.*openerp-cron-worker\>'"`
# cron=`ps aux | grep -w -c '\<bash\>'`
api_debug=`$ssh_query "ps aux | grep -w -c '\<openerp-server.*--debug\>'"`
cron_text=""
api_text=""
api=""
# check API status
if [ "$api_debug" -le "1" ]
    then
        api=`$ssh_query "ps -aux | grep -w -c '\<openerp-server\>'"`
        if [ "$api" -le "1" ]
            then
                api_text="<br /><b style='color:#FFFF00'>WARNING</b>: API not started"
            else
                api_text=""
                # api_text="<br /><b style='color:#1155CC'>INFO</b>: API started in normal mode"
            fi
    else
        api_text="<br /><b style='color:#FFFF00'>WARNING</b>: API started in debug mode"
    fi

# check cron status
if [ "$cron" -lt "5" ]
    then
        cron_text="<b style='color:#FF0000'>ALERT</b>: Not all cron works"
    else
        cron_text=""
        # cron_text="<b style='color:#1155CC'>INFO</b>: All cron works"
    fi
text="$cron_text$api_text"
if [ "$text" ]
    then
        # echo "$text" `date` "$mail"
        echo "<b>$text</b>" | `mail -s "$(echo "API status\nContent-Type: text/html")" "$mail"`
    fi
