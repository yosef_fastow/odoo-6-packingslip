#!/bin/bash
server_pid="$(pgrep -f server/openerp-server)"
echo $server_pid
if ! [ $server_pid ]
then
  /etc/init.d/openerp-server start
fi
