# Last edited: IB 17.08.2016 (Cron reschedule)
# Installation: cat scripts/production/api_delmar_crontab.sh | crontab -

# System
00 00 * * * rm -rf /var/archives/*

# Generic
*/30 * * * * bash /home/delmar/mail_error.sh # mail error cron (email)
00 * * * * find /home/delmar/delmar_openerp/logs/ -type f \( -mtime +14 \) -exec rm -rf {} \; # Clean logs on openerp
00 */4 * * * nohup python /home/delmar/import_settings_single/import_fields.py 2>&1 > /home/delmar/import_settings_single/import_fields.log & # import DNS/Cost/etc...
00 */6 * * * bash /home/delmar/ftpsync.sh > /home/delmar/ftpsync.log # sync orders to FTP, and clean old
30 00 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke update:products:fields:size_tolerance 2>&1 > /home/delmar/OpenERP_product_importer/log/fill_size_tolerance.log &  #Size tolerance update
00 09 * * * php /home/delmar/OpenERP_product_importer/wsr_acc_sold_update_checker.php # Bank report table checker
00 11 * * 6 python /home/delmar/get_image_links_from_ftp/image_to_db.py # OpenERP images to db update

# MSSQL Sync
50 * * * * cd /home/delmar/OpenERP_product_importer && php slowpoke fix:mssql:transactions >> /home/delmar/OpenERP_product_importer/fix_spaces.log # autotrim
40 * * * * cd /home/delmar/OpenERP_product_importer && php slowpoke fix:mssql:receiving >> /home/delmar/OpenERP_product_importer/fix_receiving.log # receiving
45 00 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke update:products:fields:eta >> /home/delmar/OpenERP_product_importer/eta_customer_fields_update.log # eta customer fields update
30 * * * * python /home/delmar/OpenERP_product_importer/copy_sku_tables_from_ms.py
00 */3 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke fix:mssql:wp:statuses >> /home/delmar/OpenERP_product_importer/fix_wp_statuses.log # statuses
20 * * * * cd /home/delmar/OpenERP_product_importer/product_update/ && ./product-update.py
00 06 * * * nohup php /home/delmar/OpenERP_product_importer/product_acc_price_import.php 2>&1 > /home/delmar/OpenERP_product_importer/log/product_acc_price_import.log & # ACC_Price import
30 06 * * * cd /home/delmar/OpenERP_product_importer && nohup php slowpoke update:pg:price:acc_price 2>&1 > /home/delmar/OpenERP_product_importer/log/gmotoc_acc_price_update.log & # ACC_Price import
00 07 * * * nohup php /home/delmar/OpenERP_product_importer/inv_rec_update_prod.php 2>&1 > /home/delmar/OpenERP_product_importer/log/inv_rec_update_prod.log & # Inv_rec sync
*/30 * * * * cd /home/delmar/OpenERP_product_importer && php slowpoke export:showroom:prices
00 05 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke update:products:dimensions  2>&1 > /home/delmar/OpenERP_product_importer/log/product_dimentions.log & # Product Dimentions  update
00 */4 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke etl:showroom:tag  2>&1 > /home/delmar/OpenERP_product_importer/log/etl_srm_tag.log & # ETL Update Show room tag

# Exports / Imports
00 03 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke export:po:diamonds # Diamond Orders Export to csv
00 10-18 * * * php /home/delmar/OpenERP_product_importer/dnr_export.php  # DNR Export to FTP;
00 10-18 * * * php /home/delmar/OpenERP_product_importer/hierarchy_export.php # hierarchy Export to FTP
00 10-18 * * * php /home/delmar/OpenERP_product_importer/liquidation_export.php # liquidation Export to FTP
00 22 * * * cd /home/delmar/OpenERP_product_importer && nohup php slowpoke update:mssql:price:acc_price 2>&1 > /home/delmar/OpenERP_product_importer/acc_price.log & # acc_price
00 23 * * * cd /home/delmar/OpenERP_product_importer && nohup php slowpoke update:mssql:price:product 2>&1 > /home/delmar/OpenERP_product_importer/log/price_product.log & # price_product
00 22 * * * cd /home/delmar/OpenERP_product_importer && nohup php slowpoke update:pg:price:retail_price 2>&1 > /home/delmar/OpenERP_product_importer/retail_price.log &
00 23 * * * cd /home/delmar/OpenERP_product_importer && nohup php slowpoke update:mssql:inventory:shipping --per-step 500 2>&1 > /home/delmar/OpenERP_product_importer/log/mssql_inventory_shipping.log &

# Aggrigations
00 */6 * * * php /home/delmar/OpenERP_product_importer/year_to_date_hierarchy_aggrigation.php  # Year to Date report hierarchy etl;
00 */6 * * * php /home/delmar/OpenERP_product_importer/year_to_date_aggrigation.php  # Year to Date report flags etl;

# Overstock
20 * * * * php /home/delmar/OpenERP_product_importer/overstock_get_order_checker.php # Order load checker (email)
10 * * * * python /home/delmar/delmar_overstock_api/getOrderXml.py
#00 04 * * * php /home/delmar/OpenERP_product_importer/os_onsite_check.php # result of syncAll (email)

# Inventory checker
*/30 * * * * php /home/delmar/OpenERP_product_importer/check_order_item.php # Order_Items checker (email)
40 * * * * nohup php /home/delmar/OpenERP_product_importer/sync_sets_inventory.php 2>&1 > /home/delmar/OpenERP_product_importer/log/sync_sets_inventory.log &
00 */2 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke check:inventory:feeds # Inventory Feed Checker (email)
00 03,07,11,15,19,23 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php # OS checker (email)

# Sale Integrations
*/10 * * * * python /home/delmar/delmar_xml_rpc/wmt_resend_ack.py >> /tmp/wmt_resend_ack.log # Walmart acknolagement resend
00 * * * * python /home/delmar/OpenERP_product_importer/ssm_requests/ssm_requests.py load_orders > /tmp/ssm_load_orders.log # Added by VS
00 19 * * * nohup php /home/delmar/OpenERP_product_importer/ch_checker.php yuri 2>&1 > /tmp/ch_checker_yuri.log & # Check commercehub
00 19 * * * nohup php /home/delmar/OpenERP_product_importer/ch_checker.php yuri4bb 2>&1 > /tmp/ch_checker_yuri4bb.log & # Check commercehub
05 19 * * * nohup php /home/delmar/OpenERP_product_importer/ch_checker.php ErelSears 2>&1 > /tmp/ch_checker_ErelSears.log & # Check commercehub
00 20 * * * php /home/delmar/confirmation/send_invoice_and_confirm.php # Zales and People

# Clean cache for fonts for correct packing printing
42 * * * * fc-cache -rv

#Crons for Jasper Receiving Report
0 19 * * * php /home/delmar/OpenERP_product_importer/sync_purchase_orders.php > /tmp/sync_purchase_orders.log
0 19 * * * php /home/delmar/OpenERP_product_importer/sync_customer_price_formula.php > /tmp/sync_customer_price_formula.log
0 19 * * * cd /home/delmar/OpenERP_product_importer && php slowpoke update:mssql:tag:tagging_product

#Crons for Sales Report (with Chart)
0 19 * * * php /home/delmar/OpenERP_product_importer/sync_brands.php > /tmp/sync_brands.log

#Crons for Sales comparison report for sales people
0 0 * * * php /home/delmar/OpenERP_product_importer/sync_user_partner_rel.php > /tmp/sync_user_partner_rel.log

#Crons for Health Report
0 0 * * * php /home/delmar/OpenERP_product_importer/sync_health_report.php > /tmp/sync_health_report.php.log

# Disabled
#*/1 * * * * python /home/delmar/delmar_overstock_api/prepare_on_off_site.py 2>&1 > /tmp/prepare_on_off_site.log # Prepare data for On Off site report
#*/1 * * * * bash /home/delmar/delmar_openerp/auto_start_pgagent.sh
#*/10 * * * * nohup php /home/delmar/OpenERP_product_importer/update_OpenERP_stock.php &
#*/10 * * * * python /home/delmar/delmar_xml_rpc/check_last_tracking.py >> /tmp/check_tracking_ifc.log
#*/10 * * * * python /home/delmar/delmar_xml_rpc/check_tracking_number.py >> /tmp/check_tracking.log
#*/10 * * * * bash /home/delmar/test_api_state/api_status_alert.sh >> /tmp/api_status.log # Test Api status alert (email: a.fomin)
#*/30 * * * * bash /home/delmar/mail_failed.sh # mail failed cron (email)
#15 * * * * php /home/delmar/OpenERP_product_importer/wmt_checker.php
#50 * * * * php /home/delmar/OpenERP_product_importer/cv_checker.php
#0 */3 * * * php /home/delmar/delmar_scripts/artisan bluestem:checkftp # Bluestem
#0 0 * * * nohup python /home/delmar/delmar_overstock_api/updateQtyMerged.py 2>&1 > /home/delmar/delmar_overstock_api/updateQtyMerged.log &
#0 0 * * * find /home/delmar/delmar_overstock_api/data/orders/ -mtime +5 -exec rm -rf {} \;
#30 2 * * * python /home/delmar/delmar_overstock_api/copyACC_Price.py 2>&1 > /home/delmar/delmar_overstock_api/copyACC_Price.log
#0 5 * * * nohup python /home/delmar/delmar_overstock_api/updateQty.py 2>&1 > /home/delmar/delmar_overstock_api/updateQty.log &
#0 5 * * * nohup php /home/delmar/OpenERP_product_importer/global_dnr_update.php 2>&1 > /home/delmar/OpenERP_product_importer/log/global_dnr_update.log & #sync DNR
#0 6 * * * nohup php /home/delmar/delmar_overstock_api/check_os_onsite.php 2>&1 > /home/delmar/delmar_overstock_api/check_on_site.log &
#0 7 * * * php /home/delmar/walmart_onsite/send_report_from_db.php # Walmart onsite
#0 7 * * * php /home/delmar/walmart_onsite/send_report.php
#30 22 * * * php /home/delmar/OpenERP_product_importer/fill_transactions_for_missed_orders.php 2>&1 > /home/delmar/OpenERP_product_importer/fill_missed_transactions.log
#0 23 * * * bash /home/delmar/walmart_onsite/run_from_db.sh # ???
#0 23 * * * sh /home/delmar/walmart_onsite/run.sh
