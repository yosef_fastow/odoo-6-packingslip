# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command

# */10 * * * * nohup php /home/delmar/OpenERP_product_importer/update_OpenERP_stock.php &
*/30 * * * * sh /home/delmar/mail_error.sh
*/10 * * * * python /home/delmar/delmar_overstock_api/getOrderXml.py
#0 23 * * * sh /home/delmar/walmart_onsite/run.sh
#0 7 * * * php /home/delmar/walmart_onsite/send_report.php
0 0 * * * nohup python /home/delmar/delmar_overstock_api/syncAll.py 2>&1 > /home/delmar/delmar_overstock_api/syncAll.out &
#0 5 * * * nohup python /home/delmar/delmar_overstock_api/updateQty.py 2>&1 > /home/delmar/delmar_overstock_api/updateQty.out &
0 3 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
0 7 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
0 11 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
0 15 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
0 19 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
0 23 * * * php /home/delmar/OpenERP_product_importer/os_sync_inventory.php
30 */4 * * * php /home/delmar/OpenERP_product_importer/acc_price_interval_updater.php
0 6 * * * php /home/delmar/OpenERP_product_importer/product_acc_price_import.php > /home/delmar/OpenERP_product_importer/product_acc_price_import.out
#0 0 * * * nohup python /home/delmar/delmar_overstock_api/updateQtyMerged.py 2>&1 > /home/delmar/delmar_overstock_api/updateQtyMerged.out &
30 6 * * * php /home/delmar/OpenERP_product_importer/gmotoc_acc_price_update.php
0 7 * * * php /home/delmar/OpenERP_product_importer/inv_rec_update_prod.php
0 4 * * * php /home/delmar/delmar_overstock_api/os_onsite_check.php
#30 2 * * * python /home/delmar/delmar_overstock_api/copyACC_Price.py 2>&1 > /home/delmar/delmar_overstock_api/copyACC_Price.out
#30 22 * * * php /home/delmar/OpenERP_product_importer/fill_transactions_for_missed_orders.php 2>&1 > /home/delmar/OpenERP_product_importer/fill_missed_transactions.out
#0 6 * * * nohup php /home/delmar/delmar_overstock_api/check_os_onsite.php 2>&1 > /home/delmar/delmar_overstock_api/check_on_site.out &
40 * * * * nohup php /home/delmar/OpenERP_product_importer/sync_sets_inventory.php 2>&1 > /home/delmar/OpenERP_product_importer/sync_sets_inventory.log &
0 * * * * find /home/delmar/delmar_openerp/logs/ -type f \( -mtime +14 \) -exec rm -rf {} \;
#15 * * * * php /home/delmar/OpenERP_product_importer/wmt_checker.php
*/10 * * * * python /home/delmar/delmar_xml_rpc/wmt_resend_ack.py >> /tmp/wmt_resend_ack.out
#50 * * * * php /home/delmar/OpenERP_product_importer/cv_checker.php
0 0 * * * nohup python /home/delmar/import_settings_single/import_fields.py 2>&1 > /home/delmar/import_settings_single/import_fields.log &
0 20 * * * php /home/delmar/confirmation/send_invoice_and_confirm.php
10 */1 * * * nohup php /home/delmar/OpenERP_product_importer/fix_unsync_products.php 2>&1 > /tmp/unsync.out &
*/5 * * * * nohup php /home/delmar/amqp/publishers/sync_publisher.php 2>&1 >> /tmp/rabbit_sync.out
*/2 * * * * php /home/delmar/amqp/publishers/create_one_publisher.php 50
#*/10 * * * * python /home/delmar/delmar_xml_rpc/check_last_tracking.py >> /tmp/check_tracking_ifc.out
#*/10 * * * * python /home/delmar/delmar_xml_rpc/check_tracking_number.py >> /tmp/check_tracking.out
0 19 * * * nohup php /home/delmar/OpenERP_product_importer/ch_checker.php yuri 2>&1 > /tmp/ch_checker_yuri.out &
0 19 * * * nohup php /home/delmar/OpenERP_product_importer/ch_checker.php yuri4bb 2>&1 > /tmp/ch_checker_yuri4bb.out &
*/1 * * * * python /home/delmar/delmar_overstock_api/updateFull.py > /tmp/overstock_api_update_full.log
*/10 * * * * python /home/delmar/amqp/publishers/check_delmarifc_trackings.py > /tmp/check_tracking_publisher.out
#*/1 * * * * bash /home/delmar/delmar_openerp/auto_start_pgagent.sh
*/30 * * * * python /home/delmar/amqp/publishers/check_fdxhed_publisher.py >> /tmp/check_fdxhed.log
*/10 * * * * bash /home/delmar/delmar_openerp/scripts/production/api_status_alert.sh >> /tmp/api_status.log
0 23 * * * sh /home/delmar/walmart_onsite/run_from_db.sh
0 7 * * * php /home/delmar/walmart_onsite/send_report_from_db.php
0 10 * * 6 php /home/delmar/OpenERP_product_importer/update_OpenERP_images.php
