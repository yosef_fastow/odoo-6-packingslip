#!/bin/bash

echo ""
echo "╭─Killing old SSH tunnels"
PID1=$(pgrep -f 'server_tunnels')
if [ "$PID1" != "" ]
then
    kill $PID1
    echo "╰─⇥ Tunnel with PID " $PID1 " is killed"
else
    echo "╰─⇢ Not starting SSH tunnels"
fi
echo "╭─Starting SSH tunnels"
autossh -f -N server_tunnels
echo "╰─⇝ Started with PID " $(pgrep -f 'server_tunnels')
echo ""

echo "╭─Killing old MSSQL database tunnels"
PID2=$(pgrep -f 'mssql_tunnels')
if [ "$PID2" != "" ]
then
    kill $PID2
    echo "╰─⇥ Tunnel with PID " $PID2 " is killed"
else
    echo "╰─⇢ Not starting MSSQL database tunnels"
fi
echo "╭─Starting MSSQL database tunnels"
autossh -f -N mssql_tunnels
echo "╰─⇝ Started with PID " $(pgrep -f 'mssql_tunnels')
echo ""

echo "╭─Killing old PGSQL database tunnels"
PID3=$(pgrep -f 'pgsql_tunnels')
if [ "$PID3" != "" ]
then
    kill $PID3
    echo "╰─⇥ Tunnel with PID " $PID3 " is killed"
else
    echo "╰─⇢ Not starting PGSQL database tunnels"
fi
echo "╭─Starting PGSQL database tunnels"
autossh -f -N pgsql_tunnels
echo "╰─⇝ Started with PID " $(pgrep -f 'pgsql_tunnels')
echo ""

echo "╭─Killing old HTTP tunnels"
PID4=$(pgrep -f 'http_tunnels')
if [ "$PID4" != "" ]
then
    kill $PID4
    echo "╰─⇥ Tunnel with PID " $PID4 " is killed"
else
    echo "╰─⇢ Not starting HTTP tunnels"
fi
echo "╭─Starting HTTP tunnels"
autossh -f -N http_tunnels
echo "╰─⇝ Started with PID " $(pgrep -f 'http_tunnels')
echo ""