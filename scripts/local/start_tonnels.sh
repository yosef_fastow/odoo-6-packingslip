#!/bin/bash

# set -x
# for i in "21433:192.168.17.3:1433" "31433:192.168.100.6:1433" "41433:192.168.100.1:1433" "51433:192.168.101.5:1433"

port_inc=2
for i in "192.168.100.14:1433" "192.168.17.3:1433" "192.168.100.1:1433" "192.168.101.5:1433" "192.168.100.7:1433"
do
  :
  port_no=$port_inc"1433"
  port_inc=$(( $port_inc + 1 ))

  tunnel_pid="$(lsof -i :$port_no | grep -Po '(?<=ssh\s{5})\d+' -m 1)"
  if [ tunnel_pid ]
  then
    kill $tunnel_pid
    echo "Tunnel with PID " $tunnel_pid " is killed"
  fi

  x=1
  atempt_no=0
  while [ $x -gt 0 ]
  do
    atempt_no=$(( $atempt_no + 1 ))
    ssh -f -L $port_no:$i delmar@erpdev.delmarintl.ca -N
    if [ "$(lsof -i :$port_no | grep -Pc '^ssh\s+.*?\:\d+')" -gt 0 ]
    then
      x=0
      echo "New tunnel is set up (attempt number is " $atempt_no ")"
    fi
  done


done
