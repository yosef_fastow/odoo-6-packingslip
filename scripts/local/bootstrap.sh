#!/bin/bash

echo -e "== [1/?] UPDATING LOCALES ..."
# NEED TO UPDATE LOCALES FOR PROPPER INSTALLATION
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
update-locale
locale-gen en_US.UTF-8
dpkg-reconfigure locales

echo -e "== [2/?] UPDATING AND UPGRADING SYSTEM ..."
# FULL SYSTEM UPGRADE EXCEPT KERNEL AND GRUB
sed -i 's/us/ru/g' /etc/apt/sources.list
apt-mark -qq hold grub-pc
apt-get update && apt-get -y upgrade

echo -e "== [3/?] ADDING EXTRA REPOS (postgresql-9.3) ..."
# LATEST POSTGRESQL 9.3
echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update && apt-get -y upgrade
apt-get -y install postgresql-9.3

echo -e "== [4/?] UPDATING AND INSTALLING DEPENDENCIES ..."
# OPERERP REQUIREMENTS
apt-get update && apt-get -y install python-dateutil python-feedparser python-gdata python-ldap \
python-libxslt1 python-lxml python-mako python-openid python-psycopg2 python-pybabel python-pychart python-pydot \
python-pyparsing python-reportlab python-simplejson python-tz python-vatnumber python-vobject python-webdav \
python-werkzeug python-xlwt python-yaml python-zsi gunicorn python-psutil python-pip python-dev python-pyodbc \
freetds-common freetds-dev tdsodbc freetds-bin unixodbc php5-sybase unixodbc-dev g++ pgtune htop xvfb firefox \
libffi-dev git autossh

pip install --upgrade pip
pip install unidecode
pip install funcsigs
pip install sympy
pip install redis
pip install requests
python -m pip install setuptools
python -m pip install paramiko
apt-get install python-psycopg2 -qy
pip install psycopg2
pip install simplejson
pip install lxm
easy_install --upgrade pytz
easy_install --upgrade reportlab
easy_install --upgrade mako
pip install http://download.gna.org/pychart/PyChart-1.39.tar.gz
pip install werkzeug
pip install babel
pip install python-dateutil
pip install Pillow
pip install chardet
pip install xmltodict
pip install configparser
pip install xlrd
pip install Workbook
pip install pyvirtualdisplay
pip install selenium
pip install openid
easy_install --upgrade python-openid
easy_install --upgradexlsxwriter
easy_install --upgradeexrex
easy_install --upgradepygeocoder
easy_install --upgradepyodbc
easy_install --upgradefedex

# Python requirements
pip install XlsxWriter pygeocoder fedex xlrd gnupg configparser xmltodict paramiko psutil ecdsa \
python-gnupg requests redis exrex funcsigs unidecode rjsmin rcssmin pyvirtualdisplay selenium sympy \
pyodbc --upgrade

pip install --upgrade 'git+https://github.com/mk-fg/pretty-yaml.git#egg=pyaml'

echo -e "== [5/?] LINKING FOLDERS ..."
# HOME FOLDER SETUP
echo -e "/vagrant > ~/delmar_openerp"
rm -rf /home/vagrant/delmar_openerp
ln -s /vagrant/ /home/vagrant/delmar_openerp
cd /home/vagrant/delmar_openerp/
mkdir /orders && chmod 777 -R /orders
mkdir /orders_temp && chmod 777 -R /orders_temp

echo -e "== [6/?] TUNNELS MANAGEMENT ..."
# Copy accepted key to vagrant home
mkdir -p /root/.ssh/
# Remove manual request on connect
echo -e "Host *\n    StrictHostKeyChecking no" > /root/.ssh/config
echo -e "Host *\n    StrictHostKeyChecking no" > /home/vagrant/.ssh/config
cp /home/vagrant/delmar_openerp/scripts/local/id_rsa* /root/.ssh/
cp /home/vagrant/delmar_openerp/scripts/local/id_rsa* /home/vagrant/.ssh/
chmod 600 /root/.ssh/id_rsa
chmod 600 /home/vagrant/.ssh/id_rsa
chown -R vagrant:vagrant /home/vagrant/.ssh/
# Adding autoconnect to start on reload
echo -e "#!/bin/sh -e\n\n\
# Make sure eth0 is working. This works around Vagrant issue #391\n\
/etc/init.d/networking restart\n\n\
# SSH connections to ERP servers\n\
autossh -f -N delmar@erpdev.delmarintl.ca -L 21433:192.168.100.14:1433\n\
exit 0" > /etc/rc.local
service ssh restart
autossh -f -N delmar@erpdev.delmarintl.ca -L 21433:192.168.100.14:1433

echo -e "== [7/?] ADDING USER vagrant TO DATABASE AND GRANT PERMISSIONS ..."
# PROPPER ENCODING FOR CLUSTER
pg_dropcluster --stop 9.3 main
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
update-locale
locale
pg_createcluster --start --locale="en_US.UTF-8" -e UTF-8 9.3 main
service postgresql restart

echo -e "== [8/?] DOWNLOADING DATABASE IMAGE (this can take a while) ..."
# Dump was created by `pg_dumpall | gzip -c > db_latest.gz` under postgres user
if [ ! -f /vagrant/scripts/local/db.gz ]; then
    cd /vagrant/scripts/local && wget -c erpdev.delmarintl.ca/files/db.4666abe920d7d2bc272d36d39983eaeb.gz --progress=bar:force
    mv db.4666abe920d7d2bc272d36d39983eaeb.gz db.gz
fi
service postgresql stop
su postgres -c "rm -rf ~/9.3/main/*"
cp /vagrant/scripts/local/db.gz /tmp/db.gz
su postgres -c "tar -xzf /tmp/db.gz -C /var/lib/postgresql/9.3/main --strip-components=1"
rm -rf /tmp/db.gz
service postgresql restart
su postgres -c "psql -c 'ALTER DATABASE \"Portal_prod_staging\" RENAME TO local_portal'"
su postgres -c "psql -c \"CREATE USER vagrant WITH password 'p01f01'\""
su postgres -c "psql -c \"ALTER DATABASE local_portal OWNER TO vagrant\""
su postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE local_portal TO vagrant\""
su postgres -c "psql -d local_portal -c \"GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO vagrant\""
su postgres -c "psql -d local_portal -c \"GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO vagrant\""
su postgres -c "psql -d local_portal -c \"ALTER TABLE res_partner OWNER TO vagrant\""
su postgres -c "psql -d local_portal -c \"alter role vagrant superuser;\""
su postgres -c "psql local_portal < /vagrant/scripts/local/patch.sql"
# DB ALLOW ACCESS FROM OUTSIDE ON PORT 6543
pgtune -i /etc/postgresql/9.3/main/postgresql.conf >> /etc/postgresql/9.3/main/postgresql.conf
echo -e "host    all             all             0.0.0.0/0            md5" >> /etc/postgresql/9.3/main/pg_hba.conf
echo -e "listen_addresses = '*'" >> /etc/postgresql/9.3/main/postgresql.conf
service postgresql restart

echo -e "== [9/?] ODBC CONFIGURATION ..."
echo "... copy: freetds.conf > /etc/freetds/freetds.conf"
cp /home/vagrant/delmar_openerp/scripts/local/odbc/freetds.conf /etc/freetds/freetds.conf
echo "... copy: odbc.ini > /etc/odbc.ini"
cp /home/vagrant/delmar_openerp/scripts/local/odbc/odbc.ini /etc/odbc.ini
echo "... copy: odbcinst.ini > /etc/odbcinst.ini"
cp /home/vagrant/delmar_openerp/scripts/local/odbc/odbcinst.ini /etc/odbcinst.ini

mkdir /var/log/openerp
chown -R vagrant:vagrant /var/log/openerp

echo "... copy: openerp-server.conf > /etc/openerp-server.conf"
cp /home/vagrant/delmar_openerp/scripts/local/openerp-server.conf /etc/openerp-server.conf

# Manual work.
echo -e "NOTICE! When you log it to vagrant for the first time please run following commands:"
echo -e "   echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections"
echo -e "   sudo apt-get install ttf-mscorefonts-installer -y"

echo -e "NOTICE! If OpenERP will ask for some modules, just try to run command:"
echo -e "   sudo pip install XlsxWriter pygeocoder fedex xlrd gnupg configparser xmltodict paramiko psutil ecdsa \\"
echo -e "   python-gnupg requests redis exrex funcsigs unidecode rjsmin rcssmin pyvirtualdisplay selenium sympy \\"
echo -e "   pyodbc --upgrade"

echo -e "NOTICE! If no connection to MSSQL:"
echo -e "   sudo killall -9 ssh"
echo -e "   sudo autossh -f -N delmar@erpdev.delmarintl.ca -L 21433:192.168.100.14:1433"
