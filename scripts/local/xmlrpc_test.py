#!/usr/bin/python
# -*- coding: utf-8 -*-
import xmlrpclib

username = 'admin'
pwd = '1'
dbname = 'local_portal'

sock_common = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/common')
uid = sock_common.login(dbname, username, pwd)

sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')

param = {
    'status': False,
    'username': False,
    'product_id': 112000,
    'text': 'Some dummy data',
    'po_id': 1,
    'assignee': False,
    'partner_id': 9
}

sock.execute(dbname, uid, pwd, 'po.communications', 'create', param)
