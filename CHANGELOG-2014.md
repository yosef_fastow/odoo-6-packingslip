# Delmar Portal Changelog

## Version: [DP] v.14.12.30 - **Last, but not least**

Deployed date: 12/30/2014

Link: [http://redmine.pfrus.com/versions/647](http://redmine.pfrus.com/versions/647)

### Tasks:

* Task [#17962](http://redmine.pfrus.com/issues/17962) - Add invoice report export to Internal Shipments (Added invoice report export to Internal Shipments)
* Task [#18003](http://redmine.pfrus.com/issues/18003) - Store uploaded signfield and invoice files (Store uploaded files under /var/www/inventory_report/upload/storage )
* Task [#18140](http://redmine.pfrus.com/issues/18140) - Add shipping cost to shipping code screen (Added shipping cost to shipping code screen)
* Task [#18225](http://redmine.pfrus.com/issues/18225) - Mobile sale report : add a comparison to last year (Added a comparison to last year by customers)
* Task [#18251](http://redmine.pfrus.com/issues/18251) - CH orders: put orders to temp folder every time, when load (put orders to temp folder every time, when load)
* Task [#18252](http://redmine.pfrus.com/issues/18252) - Setup daily import of global DNR from /Incoming/Inv_ETA/DNRs.xls  (set up cron for loadinggrobal DNR from /Incoming/Inv_ETA/DNRs.xls )

## Version: [DP] v.14.12.24 - **The day before Xmas**

Deployed date: 12/24/2014

Link: [http://redmine.pfrus.com/versions/645](http://redmine.pfrus.com/versions/645)

### Tasks:

* Task [#17570](http://redmine.pfrus.com/issues/17570) - Create manual for cleaning up root email (Missed TRANSACTIONS) (added manual Wiki: Missed_TRANSACTIONS_fixing)
* Task [#17614](http://redmine.pfrus.com/issues/17614) - CommerceERP: Stock Move improvement (Some small fixes done)
* Task [#18021](http://redmine.pfrus.com/issues/18021) - Mobile report: Check why prices are different for Zulily, Hautelook, Ideeli (Changed mobile report to use price from sale_order_line.)
* Task [#18107](http://redmine.pfrus.com/issues/18107) - Add description to Big Default Template (Added title as description, if we don't have short or long descrition)
* Task [#18109](http://redmine.pfrus.com/issues/18109) - DNR - Don’t change to DNS (De-attached DNR from DNS. Created additional DNR field - global DNR for product )
* Task [#18177](http://redmine.pfrus.com/issues/18177) - CommerceERP: Add ship by tag and priority  (Added tag and priority columns to CommerceERP Shipping code screen)
* Task [#18178](http://redmine.pfrus.com/issues/18178) - Fix IC2 and Diamond candle sales data (Now we will take prices for order and invoices from customer price)
* Task [#18180](http://redmine.pfrus.com/issues/18180) - Ice Au - template (Added skus for IceAu)
* Task [#18181](http://redmine.pfrus.com/issues/18181) - Ebay: add ability to load order manually (Added ability to load ebay orders manually )
* Task [#18194](http://redmine.pfrus.com/issues/18194) - QVC Sku (Added skus for QVC)
* Task [#18195](http://redmine.pfrus.com/issues/18195) - Fingerhut Sku (Added skus for Fingerhut )
* Task [#18196](http://redmine.pfrus.com/issues/18196) - BBB Sku (Bed Bath & Beyond) (Added skus for BBB)
* Task [#18218](http://redmine.pfrus.com/issues/18218) - Fingerhut spec sheets (FHBLACKSILVER1214BRACELET, FHBLACKSILVER1214EARRING, FHBLACKSILVER1214RING) (New template added to prod)

### Bugs:

* Bug [#16509](http://redmine.pfrus.com/issues/16509) - CommerceERP: assertion error when using advanced filter on any search screen (Fixed)
* Bug [#17866](http://redmine.pfrus.com/issues/17866) - Products screen: wrong values in "QTY available" and "QTY on Hand" columns. [PROD][DEV] (Fixed)
* Bug [#18167](http://redmine.pfrus.com/issues/18167) - OS confirmation bug  (Fixed. )

## Version: [DP] v.14.12.18 - **Commerce Impact**

Deployed date: 12/18/2014

Link: [http://redmine.pfrus.com/versions/637](http://redmine.pfrus.com/versions/637)

### Tasks:

* Task [#17599](http://redmine.pfrus.com/issues/17599) - CommerceERP: Add relation bin - location (Added relation bin - location)
* Task [#18051](http://redmine.pfrus.com/issues/18051) - Bealls Specsheet (Added new template for Bealls)
* Task [#18079](http://redmine.pfrus.com/issues/18079) - Avoid duplication of orders in ERP (Check if we have workitem, forbid creation duplicate workitems, added unique constrain for workitems)
* Task [#18081](http://redmine.pfrus.com/issues/18081) - Out going screen - add filter by tag (Added filter by Tag to Outgoing orders screen)
* Task [#18089](http://redmine.pfrus.com/issues/18089) - Specsheets: Template creation problem (Fixed)
* Task [#18104](http://redmine.pfrus.com/issues/18104) - Fingerhut spec sheets (FHBLACKSILVERBRIDAL1214) (Added new template for Fingerhut (FHBLACKSILVERBRIDAL1214))
* Task [#18118](http://redmine.pfrus.com/issues/18118) - CommerceERP: forbid change qty from Product Move screen (Forbidden changing qty from Product Move screen)
* Task [#18136](http://redmine.pfrus.com/issues/18136) - CommerceHub: Create a wrapper for shipping confirmation (convert it to ASCII) (Added wrapper for CH shipping confirmation (convert it to ASCII))
* Task [#18143](http://redmine.pfrus.com/issues/18143) - Best buy EDI: Send notification when receive first order. (Send mail, when receive first order )
* Task [#18150](http://redmine.pfrus.com/issues/18150) - Sales comparison report : change top date filter. (Changed default date to yesterday. For today show today sales.)
* Task [#18151](http://redmine.pfrus.com/issues/18151) - CommerceERP: Shop.ca : can’t confirm orders - change shop.ca to SFTP (Changed shop.ca to work with SFTP)
* Task [#18153](http://redmine.pfrus.com/issues/18153) - CommerceERP: Feed reliable qty (Changed method updateQTY to feed qty from Stock Move screen)
* Task [#18165](http://redmine.pfrus.com/issues/18165) - PPower: NOt all skus got created  (Created all needed SKUs)
* Task [#18168](http://redmine.pfrus.com/issues/18168) - Fingerhut spec sheets (FHBLACKSILVER1214PENDANT) (Added new template for Fingerhut)

### Bugs:

* Bug [#17515](http://redmine.pfrus.com/issues/17515) - SmartBargains: GetFilesFromLocalFolder smb ERROR! (Fixed)
* Bug [#17924](http://redmine.pfrus.com/issues/17924) - Walmart onsite report - not accurate  (Changed Walmart report to use Walmart API)
* Bug [#18044](http://redmine.pfrus.com/issues/18044) - [PROD] Sterling ERROR Can't Find move line by move_obj.name (Rejected)

## Version: [DP] v.14.12.11 - **HBO. No, HSN**

Deployed date: 12/11/2014

Link: [http://redmine.pfrus.com/versions/634](http://redmine.pfrus.com/versions/634)

### Features:

* Feature [#18012](http://redmine.pfrus.com/issues/18012) - Add shipping date time from and shiping date time to range filter (Added filters for shipping date range)

### Tasks:

* Task [#17675](http://redmine.pfrus.com/issues/17675) - HSN: Activate HSN - start sending inventory / orders / confirmation (HSN is active on prod.)
* Task [#17844](http://redmine.pfrus.com/issues/17844) - CommerceHub: BestBuy - ship to WH EDI (We are ready to receive 850 and 860, send invoice 810.)
* Task [#18013](http://redmine.pfrus.com/issues/18013) - CommerceERP: refactoring for all screens to use bin id (not bin name) (All screens use bin id for now)
* Task [#18025](http://redmine.pfrus.com/issues/18025) - ONHOLD - remove the need to approve by line (Added button "Approve all" for task in state "approved")
* Task [#18026](http://redmine.pfrus.com/issues/18026) - Fingerhut spec sheets (PCDDIAFASH614PENDANT and PCDDIAFASH614RING) (Added new template for Fingerhut)
* Task [#18031](http://redmine.pfrus.com/issues/18031) - DUTY AUTO PAYMENT: refactoring for calculation (Done)
* Task [#18035](http://redmine.pfrus.com/issues/18035) - Mobile sales report - check year $ value (We removed "returns for non-existing orders" from mobile report)
* Task [#18077](http://redmine.pfrus.com/issues/18077) - Order Activity: Change order in "name and address" field (Changed order for field "name and address" for Order Activity screen)
* Task [#18091](http://redmine.pfrus.com/issues/18091) - Welcome Peacock Parade (Created new customer Peacock Parade, added taxes for it, loaded first order)
* Task [#18093](http://redmine.pfrus.com/issues/18093) - OS: shipping upgrades Christmas 2014 (Created Tags: OS_phase_1_2014, OS_phase_2_2014, OS_phase_3_2014)
* Task [#18097](http://redmine.pfrus.com/issues/18097) - Delivery orders screen: hide date for stock moves (Hidden date for stock moves on Delivery orders screen)
* Task [#18111](http://redmine.pfrus.com/issues/18111) - Unflag DNS and Send 0 for these items on OS feed. (Removed flags DNS and Send 0)
* Task [#18114](http://redmine.pfrus.com/issues/18114) - Check why not feeding to OS  (All items were added to the feed)
* Task [#18116](http://redmine.pfrus.com/issues/18116) - HSN inventory feed: Change customer sku to be Customer ID Delmar (Changed customer sku to be Customer ID Delmar)

### Bugs:

* Bug [#18040](http://redmine.pfrus.com/issues/18040) - [PROD] LoadOrders errors for Jomashop and GDM (Fixed)
* Bug [#18042](http://redmine.pfrus.com/issues/18042) - [PROD] LoadOrders errors for Smart Bargains (Fixed)

## Version: [DP] v.14.12.04 - **SuperbFTP Feeding**

Deployed date: 12/04/2014

Link: [http://redmine.pfrus.com/versions/631](http://redmine.pfrus.com/versions/631)

### Features:

* Feature [#18011](http://redmine.pfrus.com/issues/18011) - Delivery orders screen: column and filter  for incoming code (Added incoming code column in delivery orders screen and included a filter for it.)

### Tasks:

* Task [#17361](http://redmine.pfrus.com/issues/17361) - Welcome DSV - Sams club packing slip (Added template for packing slip for new customer Sams club)
* Task [#17743](http://redmine.pfrus.com/issues/17743) - Welcome Sears PR (Added template for packing slip for new customer Sears PR)
* Task [#17768](http://redmine.pfrus.com/issues/17768) - Move inventory feed to another API server: app03 (Moved inventory feed to app03)
* Task [#17881](http://redmine.pfrus.com/issues/17881) - Sterling hard copy invoice format (Canged invoice file for Sterling customers: changed style, product id.)
* Task [#17947](http://redmine.pfrus.com/issues/17947) - Fix Generic import orders to work with sftp - Samuels files. (Generic import orders works with sftp now)
* Task [#17957](http://redmine.pfrus.com/issues/17957) - Delete null line in Sales Comparison Report  (Done)
* Task [#17977](http://redmine.pfrus.com/issues/17977) - Samuels: Push orders with product with zero price (Processed orders with zero price for Samuels)
* Task [#17978](http://redmine.pfrus.com/issues/17978) - Ebay feed: Exclude sizes like 5.75 from ebay feed (Excluded all sizes except 4-12 with step 0.5)
* Task [#17979](http://redmine.pfrus.com/issues/17979) - Sales mobile report: Move it to external API (Mobile report moved to http://erp.delmarintl.ca/mobile_report/)
* Task [#17981](http://redmine.pfrus.com/issues/17981) - Calculate USPS Express saver  - and put box option if applicable (Activated USPS Express saver)
* Task [#17984](http://redmine.pfrus.com/issues/17984) - Fingerhut spec sheets (PCDDIAFASH614EARRING)  (Created new template for Fingerhut)
* Task [#17991](http://redmine.pfrus.com/issues/17991) - Mobile report differs from Sales analysis report (Compared 2 reports, found a difference)

### Bugs:

* Bug [#17964](http://redmine.pfrus.com/issues/17964) - Fix Walmart Canada inventory feed (Fixed)
* Bug [#17974](http://redmine.pfrus.com/issues/17974) - Sales comparison report: the comparison month % is always -100.0 (Fixed problem with indicating yesterday day (wrong month because of changing month))
* Bug [#17995](http://redmine.pfrus.com/issues/17995) - Samuels sftp fail (Variables filename_local & filename was mixed up. )

## Version: [DP] v.14.11.27 - **Specsheets Invasion**

Deployed date: 11/27/2014

Link: [http://redmine.pfrus.com/versions/629](http://redmine.pfrus.com/versions/629)

### Tasks:

* Task [#17635](http://redmine.pfrus.com/issues/17635) - Belk Speck Sheet (Added template for Belk)
* Task [#17783](http://redmine.pfrus.com/issues/17783) - Stage spec sheet + task creation (Added new template and created task for Indians)
* Task [#17798](http://redmine.pfrus.com/issues/17798) - Welcome Walmart Canada (Set up prod for sending inventory for new customer: Walmart Ca)
* Task [#17813](http://redmine.pfrus.com/issues/17813) - ALLUREZ AND DRUGSTORES SPEC SHEETS (Added new export template for customer ALLUREZ)
* Task [#17847](http://redmine.pfrus.com/issues/17847) - Overstock On/Off site report (Sent automatically OS offsite report to mail.)
* Task [#17859](http://redmine.pfrus.com/issues/17859) - IFC Holidays batch setup (Scheduled batches by doc)
* Task [#17867](http://redmine.pfrus.com/issues/17867) - OS feeding 0 qty even if we have positive stock (Changed time for sync and sending inventory)
* Task [#17880](http://redmine.pfrus.com/issues/17880) - Samuels inventory feed (Send inventory to SFTP)
* Task [#17894](http://redmine.pfrus.com/issues/17894) - Implement cancel for FBA orders (Cancel FBA orders on Amazon, if it was cancelled in ERP)
* Task [#17918](http://redmine.pfrus.com/issues/17918) - Reports: Sent OS off site report automatically to mail  (Sent OS off site report automatically to mail every day (15.00 MSK letter "Overstock Offsite Dailly Report"))
* Task [#17938](http://redmine.pfrus.com/issues/17938) - TSC invoice reports (Sent 2 files with invoices)
* Task [#17939](http://redmine.pfrus.com/issues/17939) - CommerceERP: Get Amazon CA FBA (Added new Sale integration for new customer: Amazon CA FBA)
* Task [#17941](http://redmine.pfrus.com/issues/17941) - Move Packslips Scheduling menu to Warehouse Management (Warehouse -> Warehouse Management -> Packing Slips -> Packslips Scheduling)

### Bugs:

* Bug [#17797](http://redmine.pfrus.com/issues/17797) - Wrong bin location given on label in returns screen (Fixed)
* Bug [#17862](http://redmine.pfrus.com/issues/17862) - Kohls packing: two page print (Changed font for all PS from Helvetica to liberation Sans )
* Bug [#17908](http://redmine.pfrus.com/issues/17908) - prg_invredid_change : puts new bin = DEL (Changed the name of released bin from "DEL" to "POST")
* Bug [#17910](http://redmine.pfrus.com/issues/17910) - Import Orders problems (Fixed)
* Bug [#17919](http://redmine.pfrus.com/issues/17919) - Inventory report bad performance (Changed request for report. It works better now.)
* Bug [#17923](http://redmine.pfrus.com/issues/17923) - Zales / PPL overcharged shipping cost (Changed shipping cost to be by order ($4), not by order lines)

## Version: [DP] v.14.11.21 - **Par ti al in ven to ry**

Deployed date: 11/21/2014

Link: [http://redmine.pfrus.com/versions/625](http://redmine.pfrus.com/versions/625)

### Features:

* Feature [#17857](http://redmine.pfrus.com/issues/17857) - Inventory Management: add ability to send partial inventory (If on Inventory management look for some items, then "Send inventory now" will send corresponding feed only for selected items.)
* Feature [#17903](http://redmine.pfrus.com/issues/17903) - Inventory Management: add ability to send partial inventory for OS (closed due to #17857: send partial inventory for specific items. It works ALL customers)

### Tasks:

* Task [#15332](http://redmine.pfrus.com/issues/15332) - Bluefly spec sheet corections (Changes for Bluefly spec sheet applied.)
* Task [#17424](http://redmine.pfrus.com/issues/17424) - CommerceERP: FBA - to use for external order (Added ability for CommerceERP to fulfill order by Amazon. )
* Task [#17730](http://redmine.pfrus.com/issues/17730) - Calculate priority mail - and put box option if applicable (Added 2 screen WH -> Priority Dimension   and   WH -> Zip Zones)
* Task [#17736](http://redmine.pfrus.com/issues/17736) - Sales comparison report : format numbers and add responsible user. (All items done)
* Task [#17764](http://redmine.pfrus.com/issues/17764) - Import orders should work with xls as well as with csv  (Import orders works with xls as well as with csv )
* Task [#17799](http://redmine.pfrus.com/issues/17799) - CommerceERP - load items OS bundles and send inventory (Done)
* Task [#17812](http://redmine.pfrus.com/issues/17812) - CommerceERP - load items OS bundles and products (DOne)
* Task [#17815](http://redmine.pfrus.com/issues/17815) - Fingerhut spec sheets (necklace) (Added new Fingerhut template)
* Task [#17816](http://redmine.pfrus.com/issues/17816) - Samuels: packing slip (Changed PS for Samuels)
* Task [#17821](http://redmine.pfrus.com/issues/17821) - Packings: Add ship info and field "Small Flat Rate Box" to all PS (Added flat rate to all PS)
* Task [#17826](http://redmine.pfrus.com/issues/17826) - New waiting availability: make external connection (Done. Added login screen, if not logged in.)
* Task [#17849](http://redmine.pfrus.com/issues/17849) - Import orders: add ability to map customer by full name (Added ability to map customer by full name)
* Task [#17860](http://redmine.pfrus.com/issues/17860) - New attribute task to add type for all items with inventory (Task 51 created: 2014_11_19_10_17 Available for Delmar_Job1 and Delmar_Job2)
* Task [#17861](http://redmine.pfrus.com/issues/17861) - Fingerhut spec sheets (19 Nov)  (Done)
* Task [#17870](http://redmine.pfrus.com/issues/17870) - OS order acknowledgment problem (Done. But need to change code for sending it automatically (added task #17904))
* Task [#17883](http://redmine.pfrus.com/issues/17883) - Fingerhut spec sheets (20 Nov) (New template is on prod)

### Bugs:

* Bug [#16349](http://redmine.pfrus.com/issues/16349) - commerceERP : problem with shipping confirmation for ADS (Rejected)
* Bug [#17807](http://redmine.pfrus.com/issues/17807) - Return Existing Order: create button error in newly created return. (Create and delete buttons was hidden for this screen.)
* Bug [#17817](http://redmine.pfrus.com/issues/17817) - CommerceERP: date on stock move didn't updated (Fixed specific items, to fix generic need implement Task #17599)
* Bug [#17824](http://redmine.pfrus.com/issues/17824) - Wrong costs in invoice tables (Fixed)
* Bug [#17825](http://redmine.pfrus.com/issues/17825) - Sales comparison report : the pdf print cut the page (Fixed)
* Bug [#17827](http://redmine.pfrus.com/issues/17827) - Return batch screen : Should not return to a non valid location (Fixed)
* Bug [#17828](http://redmine.pfrus.com/issues/17828) - CommerceERP : Not receiving orders from Amazon (Fixed)
* Bug [#17879](http://redmine.pfrus.com/issues/17879) - Sales comparison report - when make any sorting - getting wrong data in other columns (Fixed)
* Bug [#17888](http://redmine.pfrus.com/issues/17888) - Commerce ERP: Stock move screen problem (The problem fixed for specific products, for generic need to fix #17599)

## Version: [DP] v.14.11.13 - **Season is Coming**

Deployed date: 11/13/2014

Link: [http://redmine.pfrus.com/versions/624](http://redmine.pfrus.com/versions/624)

### Features:

* Feature [#17742](http://redmine.pfrus.com/issues/17742) - Orders that are Po boxes should be send only with USPS (Created condition for "PO BOX" orders and hardcoded USPS as service for this category of orders)
* Feature [#17775](http://redmine.pfrus.com/issues/17775) - Olove packing slip  : CR (Show UPC instead of customer SKU in column SKU, removed column Item ID, added size under product description, added resize mark (* or ~))

### Tasks:

* Task [#17628](http://redmine.pfrus.com/issues/17628) - TSC Spec Sheet (Added new template for TSC exports)
* Task [#17679](http://redmine.pfrus.com/issues/17679) - Implement Bluefly (Loaded first Bluefly order from ftp: Orders2014-11-09-09-27-51.xml Created CRONs for loading, getting and creating orders.)
* Task [#17779](http://redmine.pfrus.com/issues/17779) - Errors in Big Default Template (Fixed. There was incorrect fields on Customer level, which were used instead of big default template fields.)
* Task [#17785](http://redmine.pfrus.com/issues/17785) - Walmart onsite report: setup sending updated report for email (We send report in letter "Walmart Onsite Report" every day in 1.00 MSK (5pm EST))

### Bugs:

* Bug [#17575](http://redmine.pfrus.com/issues/17575) - ICA spec sheet bug (Fixed)
* Bug [#17649](http://redmine.pfrus.com/issues/17649) - Release bin  - doesn't work  (Fixed)
* Bug [#17778](http://redmine.pfrus.com/issues/17778) - Sears canada shipping confirmation is missing invoice data (Fixed shipping confirmation)

## Version: [DP] v.14.11.07 - **Benvenuti Amazon!**

Deployed date: 11/07/2014

Link: [http://redmine.pfrus.com/versions/621](http://redmine.pfrus.com/versions/621)

### Features:

* Feature [#16077](http://redmine.pfrus.com/issues/16077) - Sale Integration XML: make default table sorting by Creation Date column descendancy (Done)

### Tasks:

* Task [#17634](http://redmine.pfrus.com/issues/17634) - Sterling Spec Sheet (New Sterling Template is ready on prod)
* Task [#17636](http://redmine.pfrus.com/issues/17636) - CommerceERP: Missing bundles from the inventory feed (Fixed)
* Task [#17696](http://redmine.pfrus.com/issues/17696) - CommerceERP -  Send bundle inventory by expiration dates  (Send inventory for bundles by expiration date)
* Task [#17718](http://redmine.pfrus.com/issues/17718) - Welcome Amazon Italy (Loaded first order for Amazon Italy under Amazon UK)
* Task [#17737](http://redmine.pfrus.com/issues/17737) - Report of active bins with multi items in USCMP (Done)
* Task [#17744](http://redmine.pfrus.com/issues/17744) - Return : add ability to choose return location  (Done. See full list of locations, when check "return to source location")
* Task [#17748](http://redmine.pfrus.com/issues/17748) - Ebay inventory feed : need to change the feed to be “customer delmar id" not sku (Send “customer delmar id" in eBay feed)
* Task [#17752](http://redmine.pfrus.com/issues/17752) - Welcome Amazon France (Loaded first order for France: AMK402-7539889-7686767)

### Bugs:

* Bug [#17685](http://redmine.pfrus.com/issues/17685) - Shop.ca packing list bugs  (Fixed)
* Bug [#17747](http://redmine.pfrus.com/issues/17747) - eBay: orders issues  (Fixed. Created tables for notifications.)
* Bug [#17751](http://redmine.pfrus.com/issues/17751) - Didn't create second line for orders (check for all customers) (Fixed)
* Bug [#17753](http://redmine.pfrus.com/issues/17753) - Returns: wrong qty for partial returns. (Fixed)
* Bug [#17754](http://redmine.pfrus.com/issues/17754) - Problem print batch (The problem was in absent fonts on API. Fixed.)

## Version: [DP] v.14.10.30 - **Most Tolerant Release**

Deployed date: 10/30/2014

Link: [http://redmine.pfrus.com/versions/619](http://redmine.pfrus.com/versions/619)

### Tasks:

* Task [#17438](http://redmine.pfrus.com/issues/17438) - On / off site item report - add Walmart (Added new customer Walmart to On/off site report (Jasper). )
* Task [#17631](http://redmine.pfrus.com/issues/17631) - IGL Certificate:  make it generic for all customers (Changed Walmart and PPower to work with "Certificate" field (renamed "Cards" to "Certificate") Added "Certificate" field to all PS under Item. Created "Certificate" field for all customers)
* Task [#17643](http://redmine.pfrus.com/issues/17643) - CommerceHub: add checking for tracking format (Added checking for tracking format. If format is incorrect, order will go to new exception stage "Bad format tracking")
* Task [#17682](http://redmine.pfrus.com/issues/17682) - Add 2 more categories to size tolerance (Added new categories for size tolerance)
* Task [#17690](http://redmine.pfrus.com/issues/17690) - Size tolerance: need set size tolerance 2 for some categories (Values were updated. ST=2)
* Task [#17692](http://redmine.pfrus.com/issues/17692) - Size tolerance: add ability to lock value from weekly update (Added button "Lock Size Tolerance" to Inventory Management screen. By click on it all customer fields for this product for specific customer will be marked as locked (Created a new customer field for lock). Locked fields will not participate in Monday update.)
* Task [#17699](http://redmine.pfrus.com/issues/17699) - Target issues (Fixed)
* Task [#17705](http://redmine.pfrus.com/issues/17705) - Walmart - remove phone# from second label for extra material (Removed phone# only for Walmart from extra material label.)
* Task [#17711](http://redmine.pfrus.com/issues/17711) - OS - receiving orders (Order was created manually OS140664460-0 )
* Task [#17712](http://redmine.pfrus.com/issues/17712) - Import orders: doesn't work, when quotes absent in first 2 lines (Fixed.)
* Task [#17715](http://redmine.pfrus.com/issues/17715) - HSN inventory (Setup sending inventory for every 4 hours.)

### Bugs:

* Bug [#17678](http://redmine.pfrus.com/issues/17678) - Invoices: incorrect state and tax calculation (1. Fixed values in Invoice Header 2. Fixed Taxes on Invoice List in ERP)
* Bug [#17698](http://redmine.pfrus.com/issues/17698) - Manual Receiving: image bug  (Fixed)
* Bug [#17703](http://redmine.pfrus.com/issues/17703) - SHOP.CA multi-line orders have one line only in ERP (Fixed)
* Bug [#17704](http://redmine.pfrus.com/issues/17704) - Printing Labels in Receiving in ERP (Adjusted font sizes for labels)
* Bug [#17727](http://redmine.pfrus.com/issues/17727) - Can't see all shipping exceptions on Order activity screen (Fixed for ebay. For now it's not clear, how it happened.)

## Version: [DP] v.14.10.23 - **Muscle Movement**

Deployed date: 10/23/2014

Link: [http://redmine.pfrus.com/versions/618](http://redmine.pfrus.com/versions/618)

### Tasks:

* Task [#17436](http://redmine.pfrus.com/issues/17436) - CommerceHUB: confirmation format change for UPS (Format verified: we have correct Tracking Number format.)
* Task [#17456](http://redmine.pfrus.com/issues/17456) - Get ready for MSSQL delmarinvetory location change (MSSQL moved to 192.168.100.15)
* Task [#17511](http://redmine.pfrus.com/issues/17511) - Audit log for customer fields (It accessible under Settings -> Reporting -> Audit -> Audit Logs)
* Task [#17574](http://redmine.pfrus.com/issues/17574) - PII packing slip : add account# if UPS / FedEx / Purolator to packing slip (For now we get account # from the order (for PII only))
* Task [#17591](http://redmine.pfrus.com/issues/17591) - ADD IGL certificate to JCP  (Store certificate value (IGL) in customer fields. Show it on PS under item #)
* Task [#17593](http://redmine.pfrus.com/issues/17593) - Specsheets: PPower (Spec sheet is ready)
* Task [#17597](http://redmine.pfrus.com/issues/17597) - Sales comparison report: need to increase column width (Fixed)
* Task [#17611](http://redmine.pfrus.com/issues/17611) - Receiving signfield: Run a script to populate old data, mark items as "received" (Old receivings marked as 'Received')
* Task [#17615](http://redmine.pfrus.com/issues/17615) - Audit log: add ability to store item#, when change size tolerance (It accessible under Settings -> Reporting -> Audit -> Audit Logs See item ID, size, customer, field name stored under "Resource Name")
* Task [#17623](http://redmine.pfrus.com/issues/17623) - Checking address: detect state by ZIP code (If we have address with correct route, city, zip, but empty state, we will fill state. We will check address with delmar_check_address_service and out state from reply.)
* Task [#17625](http://redmine.pfrus.com/issues/17625) - Add new class for weekly size tolerance update (IB updated our fm_hierarchy_category table.)
* Task [#17648](http://redmine.pfrus.com/issues/17648) - Add links for uploading invoices into ERP (Added)
* Task [#17670](http://redmine.pfrus.com/issues/17670) - Rellocate screen: Show ~ on PS, if only "No resize" check box checked  (Fixed)

### Bugs:

* Bug [#17622](http://redmine.pfrus.com/issues/17622) - Missing images in Sales report (Fixed. Take images from Catalog folder)
* Bug [#17633](http://redmine.pfrus.com/issues/17633) - Empty sale order: qty = 0 (Put orders to ship exception, if qty = 0)
* Bug [#17651](http://redmine.pfrus.com/issues/17651) - Fast track report doesn’t generate a Partner sku for Overstock (fixed)

## Version: [DP] v.14.10.17 - **11x2 Main Tasks**

Deployed date: 10/17/2014

Link: [http://redmine.pfrus.com/versions/616](http://redmine.pfrus.com/versions/616)

### Tasks:

* Task [#13938](http://redmine.pfrus.com/issues/13938) - Search orders for Sterling by all customers (see comments) (Search orders for Sterling by all customers ( SterlingJared, SterlingKayOutlet, SterlingKay, SterlingFamily))
* Task [#17207](http://redmine.pfrus.com/issues/17207) - AutoPay: add ability to pay for previouse month if we don't have payment for 1st day of this month (I added window in 3 days. So, If last payment was 3 days before new month, then you can pay this month.)
* Task [#17364](http://redmine.pfrus.com/issues/17364) - Refactoring CH checking script due to changes on CH dashboard (Login script was changed. Letter comes every day.)
* Task [#17407](http://redmine.pfrus.com/issues/17407) - OS: Add size relation to product, when new option SKU added (Problematic items will be fixed automatically. Added section "FIXED SIZES:" to letter "OS INVENTORY STATUS")
* Task [#17422](http://redmine.pfrus.com/issues/17422) - Sales comparison report - Format and sorting (1. Make it printable in PDF ( add pages if needed) - done 2. Make the columns sortable and filterable (Jasper feature) - can't be made by native Jasper fearures for this report (only hide column and formatting))
* Task [#17435](http://redmine.pfrus.com/issues/17435) - Clean duplicates in OS_sales_BU table (Cleaned)
* Task [#17454](http://redmine.pfrus.com/issues/17454) - CommerceERP : Stock Move screen (Added Stock Move screen to CommerceERP. 2 methods are available: bin2bin and update qty)
* Task [#17455](http://redmine.pfrus.com/issues/17455) - Close / record PO when item received (Mark "partial" if not received in full . and "received" when all items received.)
* Task [#17471](http://redmine.pfrus.com/issues/17471) - Multilines: Refactoring for check availability method (Fixed)
* Task [#17507](http://redmine.pfrus.com/issues/17507) - Fingerhut spec sheets   (Done)
* Task [#17513](http://redmine.pfrus.com/issues/17513) - Samuels inventory feed: if there is no SKU leave it empty in feed (If there is no SKU we leave it empty in the feed)
* Task [#17523](http://redmine.pfrus.com/issues/17523) - eBay: Check for payment status before creating order (Unpaid orders with status PaymentInProcess will be stored in DB and will go to ERP only after xml with payment will come.)
* Task [#17524](http://redmine.pfrus.com/issues/17524) - eBay:  get MultiLegShippingDetails address and size parameters from order (Use MultiLegShippingDetails as Shipping address if it is. Use Shipping address if MultiLegShippingDetails is absent.)
* Task [#17529](http://redmine.pfrus.com/issues/17529) - Walmart new order directive: expected date (Get expected date for WMT orders, put it to latest ship date for SO, then it came to expected date for DO.)
* Task [#17530](http://redmine.pfrus.com/issues/17530) - CommerceERP : Add Amazon DS CA (Added Amazon DS CA to CommerceERP)
* Task [#17568](http://redmine.pfrus.com/issues/17568) - eBay: Get SKU from Variation section, if it's absent in Item section (fixed)
* Task [#17572](http://redmine.pfrus.com/issues/17572) - eBay: Get modified orders (completed payment) (If order address was modified, send email. If it's old order - write it to DB with status "Failure" and send email.)
* Task [#17576](http://redmine.pfrus.com/issues/17576) - Import shipment YTC14-0228 for Fang Ying (Added shipment YTC14-0228)
* Task [#17577](http://redmine.pfrus.com/issues/17577) - Welcome 11main.com (Integrated 11main.com into ERP.)
* Task [#17588](http://redmine.pfrus.com/issues/17588) - Sales report: change it to take information from OS_sales_BU only (Sales report take information from OS_sales_BU only)
* Task [#17590](http://redmine.pfrus.com/issues/17590) - eBay: Take ship cost from order xml, add it to total (Added ship cost to field shipCost for sale_order_line )

### Bugs:

* Bug [#17234](http://redmine.pfrus.com/issues/17234) - Combine bug: when set up ship service for batch, different ship service don't combined (Fixed)
* Bug [#17485](http://redmine.pfrus.com/issues/17485) - CommerceERP - allocation did not work properly.  (Fixed)
* Bug [#17531](http://redmine.pfrus.com/issues/17531) - eBay: Loading old orders - need to load just new (We look if create date of order is more then 24 hours, we send email, don't put it to ERP)
* Bug [#17602](http://redmine.pfrus.com/issues/17602) - Null year, month and quarter in OS_sales_BU table (Fixed. ERP fills those columns OS_Sales_BU.)
* Bug [#17607](http://redmine.pfrus.com/issues/17607) - Sales Comparison Report VS Sales Report (with chart) (The reason of difference - records with null year. Fixed)

## Version: [DP] v.14.10.09 - **UPC, I did it again**

Deployed date: 10/09/2014

Link: [http://redmine.pfrus.com/versions/615](http://redmine.pfrus.com/versions/615)

### Tasks:

* Task [#11447](http://redmine.pfrus.com/issues/11447) - Welcome The Ivory Company (TIC integrated into ERP)
* Task [#17002](http://redmine.pfrus.com/issues/17002) - IFC End of the Month Billing ("IFC End of the Month Billing" report is accessible from ERP)
* Task [#17015](http://redmine.pfrus.com/issues/17015) - List of customers without inventory (Send check inventory results in email "INVENTORY FEED")
* Task [#17413](http://redmine.pfrus.com/issues/17413) - Recycle UPC - send for shop (Danny) and OS (Steve) (DOne)
* Task [#17414](http://redmine.pfrus.com/issues/17414) - Remove old Waiting availability link (Removed)
* Task [#17416](http://redmine.pfrus.com/issues/17416) - Check if we can close ebay orders on CommerceERP, if not - just Cancel them (done)
* Task [#17417](http://redmine.pfrus.com/issues/17417) - Print ZPL phase2: add tab with printing history (only for admin) (Added a tab for admin with ZPL printing history )
* Task [#17421](http://redmine.pfrus.com/issues/17421) - Sales comparison report - combine PPB and PPLB (Done)
* Task [#17423](http://redmine.pfrus.com/issues/17423) - Shipping code screen - add log  (Added log for shipping codes to Audit trail: It accessible under Settings -> Reporting -> Audit -> Audit Logs)
* Task [#17473](http://redmine.pfrus.com/issues/17473) - Samuels: add ability to send inventory to FTP (Setup Samuels for manual inventory feed)

### Bugs:

* Bug [#17391](http://redmine.pfrus.com/issues/17391) - Fix Invoice Header not fill taxes (Fixed)
* Bug [#17465](http://redmine.pfrus.com/issues/17465) - TSC confirmation still have wrong carrier (Fixed to have format: OrderNo, SkuNo, Quantity, TrackingNo, SMdesc, ShipDate)

## Version: [DP] v.14.10.02 - **Alice Limitation**

Deployed date: 10/02/2014

Link: [http://redmine.pfrus.com/versions/614](http://redmine.pfrus.com/versions/614)

### Tasks:

* Task [#16811](http://redmine.pfrus.com/issues/16811) - Print ZPL phase2 - Add manifest report (ZPL report screen is accessible under Warehouse -> Shipping info -> print ZPL)
* Task [#17103](http://redmine.pfrus.com/issues/17103) - On / off site item report (On / off site item report is accessible from erp: Repots -> General -> On / off site item report)
* Task [#17142](http://redmine.pfrus.com/issues/17142) - Refactoring of ERP working with shipping DB (Done)
* Task [#17268](http://redmine.pfrus.com/issues/17268) - default Packing Material setup for all the customers (Packing material setup screen was updated)
* Task [#17322](http://redmine.pfrus.com/issues/17322) - rml content (Changed packings due to Packing Material setup screen changes)
* Task [#17334](http://redmine.pfrus.com/issues/17334) - Call script for preparing data for on/off site report, when SyncAll finishes (Added action after syncAll script for filling last time on site)
* Task [#17344](http://redmine.pfrus.com/issues/17344) - Check StockID = 'Stock' (cleaned up)
* Task [#17359](http://redmine.pfrus.com/issues/17359) - Deny users change or delete Locations (Deny users change or delete Locations)
* Task [#17363](http://redmine.pfrus.com/issues/17363) - Order activity screen: Show exception when filter by “ready to process” with no “date from” (Fixed)
* Task [#17372](http://redmine.pfrus.com/issues/17372) - Invoice report - need to match invoice_header table (Done)
* Task [#17378](http://redmine.pfrus.com/issues/17378) - Send invoice for Reeds to email (Send invoice for Reeds to email)
* Task [#17383](http://redmine.pfrus.com/issues/17383) - Change the Below 199.99 filter to below 200 (Changed filter to be <200 on Outgoing and < 200,  >= 200 on delivery orders screen)
* Task [#17384](http://redmine.pfrus.com/issues/17384) - Move waiting availability to external ip address location - to be available to the outside. (Move screen to http://207.96.196.71/waiting-availability/)
* Task [#17385](http://redmine.pfrus.com/issues/17385) - Add country of origin to the SMB template (Done)
* Task [#17390](http://redmine.pfrus.com/issues/17390) - CommerceERP: Welcome Ebay: set up (Set up ebay to CommerceERP, get orders for Javafly customer)

### Bugs:

* Bug [#17290](http://redmine.pfrus.com/issues/17290) - JS error after login (PROD and DEV) (Fixed)
* Bug [#17292](http://redmine.pfrus.com/issues/17292) - eBay: missing orders (Changed ebay user. added 3 crons for loading orders )
* Bug [#17309](http://redmine.pfrus.com/issues/17309) - eBay : confirmation doesn't work (Fixed)
* Bug [#17360](http://redmine.pfrus.com/issues/17360) - TSC confirmation still missing sku and qty (Fixed TSC ship confirm format)
* Bug [#17377](http://redmine.pfrus.com/issues/17377) - Invoice bug for Done order: can't get invoice for delivery orders (FIXED)
* Bug [#17402](http://redmine.pfrus.com/issues/17402) - Wrong return data in OS_Sales_BU (Fixed)
* Bug [#17404](http://redmine.pfrus.com/issues/17404) - Missing info in export (Fixed)

## Version: [DP] v.14.09.25 - **Phantom Reports**

Deployed date: 09/25/2014

Link: [http://redmine.pfrus.com/versions/613](http://redmine.pfrus.com/versions/613)

### Features:

* Feature [#15615](http://redmine.pfrus.com/issues/15615) - Add feature for release bins (Added screen Warehouse -> Stock Move -> Release bins)
* Feature [#17065](http://redmine.pfrus.com/issues/17065) - Flash screen : add print by items button (Added button "Print packing slip by Items" to Flash Orders screen)
* Feature [#17115](http://redmine.pfrus.com/issues/17115) - Print item labels (Added button for printing picking label (Screens: Manual Receiving, Receiving List, Return Existing Order))

### Tasks:

* Task [#17251](http://redmine.pfrus.com/issues/17251) - TCS : change/add shipping confirmation file columns II (Changed ship confirm for TCS)
* Task [#16418](http://redmine.pfrus.com/issues/16418) - Warehouse stock report : screen + export (New report screen is available from here  http://192.168.100.46/inventory_report/stock_report)
* Task [#16898](http://redmine.pfrus.com/issues/16898) - Warehouse stock report : add reference date point (Added option to create reference point (export). Added ability to show transactions between exports.)
* Task [#17028](http://redmine.pfrus.com/issues/17028) - CommerceERP: Welcome Ebay: create/confirm orders (Setup CommerceERP for getting eBay orders by notification. Need to activate only.)
* Task [#17102](http://redmine.pfrus.com/issues/17102) - HSN - send inventory (Done)
* Task [#17186](http://redmine.pfrus.com/issues/17186) - Phantom log  (Set logging of phantom changes. Added table stock_phantom_management_log.)
* Task [#17222](http://redmine.pfrus.com/issues/17222) - Fingerhut spec sheets (Done)

### Bugs:

* Bug [#17230](http://redmine.pfrus.com/issues/17230) - Sterling : new attribute JRP : wrong SKU# (Set up attribute ESP/ JRP related to ref3. Put correct SKUs to packing in according to customer.)
* Bug [#17252](http://redmine.pfrus.com/issues/17252) - Welcome Sterling Family: STF : not working (There was External Customer Id : 2033 set up for both: Sterling Kay and Sterling Family. We removed it for Sterling Kay. So orders will be mapped correctly for the future.)
* Bug [#17255](http://redmine.pfrus.com/issues/17255) - Outgoing orders for shipping: Order filter <200 doesn't work (Fixed)
* Bug [#17289](http://redmine.pfrus.com/issues/17289) - TSC packing slip shows cost instead of retail (Changed parser to load retail cost. Clear total values for packing.)
* Bug [#17291](http://redmine.pfrus.com/issues/17291) - Address exception for Sears Canada (State Name was Quèbec. Addresses became valid after changing the state to Quebec.)
* Bug [#17293](http://redmine.pfrus.com/issues/17293) - CommerceERP : set done problem for Amazon (Added auto retry after 1s, 4s, 10s, 30s. And if it's not successful, we will activate CRON for sending confirmations every 5 minutes until confirms will passed.)

## Version: [DP] v.14.09.18 - **Fahrenheit 321**

Deployed date: 09/18/2014

Link: [http://redmine.pfrus.com/versions/612](http://redmine.pfrus.com/versions/612)

### Tasks:

* Task [#16932](http://redmine.pfrus.com/issues/16932) - Welcome Ebay : confirm orders (Confirming orders for eBay is working now)
* Task [#17104](http://redmine.pfrus.com/issues/17104) - 321 report (321 report is accessible from Report tab on Outgoing screen)
* Task [#17157](http://redmine.pfrus.com/issues/17157) - reserved flag : change default column value to be taken from StockID table (Done)
* Task [#17166](http://redmine.pfrus.com/issues/17166) - Add "carat" to diamond weight for ICE (Added "carat" to diamond weight for ICE)
* Task [#17167](http://redmine.pfrus.com/issues/17167) - Import management: send notification email, when order was not created for some reason (Set up sending notifications email to delmar, when order was not created for some reason.)
* Task [#17174](http://redmine.pfrus.com/issues/17174) - eBay : Take price from the order and fix ship exceptions (For now we take price for eBay orders from order xml)
* Task [#17177](http://redmine.pfrus.com/issues/17177) - New Fingerhut export template (New Fingerhut export template is ready on prod)
* Task [#17185](http://redmine.pfrus.com/issues/17185) - New waiting availability screen : When unclicked "need diamond" - put note : diamond recieved  (Notice "No need diamond" changed to "Diamond received")
* Task [#17188](http://redmine.pfrus.com/issues/17188) - Load invoice / shipments from QIYI (Added new factory : QIYI. Loaded invoices for it.)
* Task [#17189](http://redmine.pfrus.com/issues/17189) - eBay: change the order id to SellingManagerSalesRecordNumber to be like in Ebay dashboard. (Order id is changed to SellingManagerSalesRecordNumber from order xml)
* Task [#17205](http://redmine.pfrus.com/issues/17205) - PII : Need to store "Customer PO" in additional field ("Customer PO" is stored in additional field)
* Task [#17231](http://redmine.pfrus.com/issues/17231) - 321 report : Pick invoice (Added new action button "321 invoice" in Report section (near the "Pick invoice"), which allow to print invoice by OS template for all customers)

### Bugs:

* Bug [#16671](http://redmine.pfrus.com/issues/16671) - Relocate Move: JS error in try to close window by using (X) or Close button (Fixed)
* Bug [#17099](http://redmine.pfrus.com/issues/17099) - PII invoice : fix ProductID (Product id is customer sku for now)
* Bug [#17151](http://redmine.pfrus.com/issues/17151) - Generic Import: need to trim fields  (Trim fields for generic import orders)
* Bug [#17156](http://redmine.pfrus.com/issues/17156) - eBay shipping notification : should record response from eBay (Ship confirm is working for eBay now)
* Bug [#17169](http://redmine.pfrus.com/issues/17169) - Stock move: negative qty for FC0LMV-G0T7 (Synchronization sets scripts changed to take set list from MSSQL DB prg_sets)
* Bug [#17175](http://redmine.pfrus.com/issues/17175) - EBY173615636010 is not ERP order (FTP path for customer thjavafly changed to /Incoming/ebay-thjavafly)
* Bug [#17187](http://redmine.pfrus.com/issues/17187) - PII packing slip : missing original customer PO (Customer PO added to packing slip as PO#:)
* Bug [#17212](http://redmine.pfrus.com/issues/17212) - UnicodeDecodeError in Import Management  (Fixed)
* Bug [#17227](http://redmine.pfrus.com/issues/17227) - Import orders: import file even if it has non-breaking spaces and "Right double quotation mark" symbols ( ” ) (Fixed)

## Version: [DP] v.14.09.11 - **Pre Yuri's Wedding Release**

Deployed date: 09/11/2014

Link: [http://redmine.pfrus.com/versions/610](http://redmine.pfrus.com/versions/610)

### Tasks:

* Task [#16424](http://redmine.pfrus.com/issues/16424) - commerceERP :  Receive orders from EBay for multiple customers (Done)
* Task [#16715](http://redmine.pfrus.com/issues/16715) - ebay shipping notification (Done)
* Task [#16717](http://redmine.pfrus.com/issues/16717) - Welcome Ebay: create orders (Ebay orders is created automatically. Load orders from folder /Incoming/ebay only for customer "Delmar")
* Task [#17001](http://redmine.pfrus.com/issues/17001) - receiving: Modify export - add shipping note (Add shipping notes)
* Task [#17003](http://redmine.pfrus.com/issues/17003) - Sterling : new attribute and change to packingslip (Done)
* Task [#17029](http://redmine.pfrus.com/issues/17029) - PII - fix inventory feed. (Fixed)

### Bugs:

* Bug [#16235](http://redmine.pfrus.com/issues/16235) - Add ability to work with Null bins on Stock Move screen (Added ability to work with Null bins on Stock Move screen)
* Bug [#16975](http://redmine.pfrus.com/issues/16975) - PII - invoice : Wrong State and missing shipping cost (Fixed)
* Bug [#17016](http://redmine.pfrus.com/issues/17016) - Check Sears inventory feed. (Fixed)
* Bug [#17027](http://redmine.pfrus.com/issues/17027) - Change bin - Should not  "post / releae"  if qty > 0 (Fixed)
* Bug [#17050](http://redmine.pfrus.com/issues/17050) - Transactions screen - doesn't show transactions. (Fixed)

## Version: [DP] v.14.09.04 - **DashAbroad Promotion**

Deployed date: 09/04/2014

Link: [http://redmine.pfrus.com/versions/608](http://redmine.pfrus.com/versions/608)

### Features:

* Feature [#16718](http://redmine.pfrus.com/issues/16718) - Import Management improvements (New Import Management Wizard.)

### Tasks:

* Task [#16425](http://redmine.pfrus.com/issues/16425) - commerceERP : inventory report - add expiration date (Added expiration date)
* Task [#16526](http://redmine.pfrus.com/issues/16526) - CommerceERP : Send available qty ( not on hand) - if expired don’t send qty (Fixed expiration feature)
* Task [#16530](http://redmine.pfrus.com/issues/16530) - Change bin - record the transaction in prg_invredid_change. (When we change name for the bin (for ex. AAA), put it to prg_invredid_change. Make bin to bin transactions.)
* Task [#16624](http://redmine.pfrus.com/issues/16624) - Sales data - put sales data in os_sales and os_sale_bu (Sales data now put to OS_Sales and OS_Sales_BU from ERP.)
* Task [#16675](http://redmine.pfrus.com/issues/16675) - CommerceERP : Newegg CA (NewEggCA integration on CommerceERP)
* Task [#16813](http://redmine.pfrus.com/issues/16813) - Import management: show not loaded orders in separate file (separate string) and highlight it with red (Import management greatly improved.)
* Task [#16834](http://redmine.pfrus.com/issues/16834) - PII - email one invoice file at the end of the day (Changed invoice send format)
* Task [#16835](http://redmine.pfrus.com/issues/16835) - TCS : change/add shipping confirmation file columns (Fixed TSC confirmation file)
* Task [#16923](http://redmine.pfrus.com/issues/16923) - Welcome JEB - jewelry basket (Jeb integrated into Portal)
* Task [#16939](http://redmine.pfrus.com/issues/16939) - Load orders with mixed cases of old/new template (Fix mix cases imports)

### Bugs:

* Bug [#16511](http://redmine.pfrus.com/issues/16511) - CommerceERP: ADS tag contains bundles and Available Quantity of this bundles displays incorrect (Available Quantity = 0 for all lines) (Fixed bundle qty)
* Bug [#16637](http://redmine.pfrus.com/issues/16637) - Import Sale Order: system must save original file extension (Some fixes for import management.)
* Bug [#16836](http://redmine.pfrus.com/issues/16836) - SMB - inventory feed - Fix customer id (fixed fid for SMB)

## Version: [DP] v.14.08.28 - **SMS? MSS? SSM?**

Deployed date: 08/28/2014

Link: [http://redmine.pfrus.com/versions/606](http://redmine.pfrus.com/versions/606)

### Tasks:

* Task [#16379](http://redmine.pfrus.com/issues/16379) - Welcome HSN - HOME SHOPPING NETWORK (HOME SHOPPING NETWORK is integrated into ERP.)
* Task [#16484](http://redmine.pfrus.com/issues/16484) - Add ability to change size for sale order but no mark on the packing slip (Added new checkbox "No resize". Asterisk is shown if none is selected. If some checkbox is selected ~ will be shown on PS.)
* Task [#16505](http://redmine.pfrus.com/issues/16505) - HSN packing slip (Created template for HSN packing slip )
* Task [#16548](http://redmine.pfrus.com/issues/16548) - Stock move screen: Allocate item even if not on primary location (Done)
* Task [#16615](http://redmine.pfrus.com/issues/16615) - Welcome SSM : Sears market place (SSM integrated into ERP)
* Task [#16619](http://redmine.pfrus.com/issues/16619) - Sears market place (SSM) packing slip (Craeted template for Sears market place (SSM) packing slip)
* Task [#16642](http://redmine.pfrus.com/issues/16642) - Missed TRANSACTIONS: check why script mark items as oversolds, when we have 2 records in transaction table for the same invocie# (Fixed)
* Task [#16643](http://redmine.pfrus.com/issues/16643) - Address verification - dont approve address with more the 35 chars long (Put into exception if address 1/2 is more then 35 chars long. Add error message, when try to approve or force the address if length is more then 35 chars long)
* Task [#16657](http://redmine.pfrus.com/issues/16657) - Warehouse stock report : get comments for transactions from all possible tables (Got comments for transactions from all possible tables)
* Task [#16665](http://redmine.pfrus.com/issues/16665) - Make sure the reserved flag = 1 ( not 0) for all transactions. (Write reserved flag = 1 for all transactions)
* Task [#16680](http://redmine.pfrus.com/issues/16680) - Create Sears.com template (Created Sears.com template)
* Task [#16681](http://redmine.pfrus.com/issues/16681) - Change SKU for Smart Bargains specsheet ( put SKU if exists , empty if not.)
* Task [#16769](http://redmine.pfrus.com/issues/16769) - If customer should write to Order_Item, add checking for tariff (If tarif or price is absent, we sent notification email "Order Items checker")
* Task [#16772](http://redmine.pfrus.com/issues/16772) - Create option for sending invoices for all customers (If setup emails_for_sending_invoice in Settings Variables, we will send invoices for this customer. Also this emails will be used for "Send invoices for delivery orders to mail" option)

### Bugs:

* Bug [#16133](http://redmine.pfrus.com/issues/16133) - CommerceERP: Delivery Order -> Relocate: qty info differs from the real inventory qty (FIxed)
* Bug [#16622](http://redmine.pfrus.com/issues/16622) - Wrong prices in Walmart Onsite Report (Fixed)
* Bug [#16630](http://redmine.pfrus.com/issues/16630) - Import Management: downloaded files have no extension (Fixed)
* Bug [#16644](http://redmine.pfrus.com/issues/16644) - PII - invoice - change GST code to HST (Made tax code dynamic in accordance to settings in Sale tax table)
* Bug [#16658](http://redmine.pfrus.com/issues/16658) - CommerceERP : create bin - doesn't work (fixed)
* Bug [#16716](http://redmine.pfrus.com/issues/16716) - PII - dont ship CA post for  UPS / FedEx / Purolator (Created 3 ship rules for UPS / FedEx / Purolator, and generic rule with code "%" for all the rest)

## Version: [DP] v.14.08.21 - **All-out PII**

Deployed date: 08/21/2014

Link: [http://redmine.pfrus.com/versions/603](http://redmine.pfrus.com/versions/603)

### Tasks:

* Task [#15887](http://redmine.pfrus.com/issues/15887) - Welcome PII : PI Incentives (Canada) (New customer PI Incentives (Canada) integrated into ERP)
* Task [#16100](http://redmine.pfrus.com/issues/16100) - Add fields to Walmart Onsite Report (Added 4 fields to Walmart onsite report: productTitle, rating, Description, short description)
* Task [#16418](http://redmine.pfrus.com/issues/16418) - Warehouse stock report : screen + export (New report screen is available from here http://192.168.100.46/inventory_report/stock_report)
* Task [#16492](http://redmine.pfrus.com/issues/16492) - Attribute project: create a template for task for the "Generic spec sheet" (Created a template for task for the "Generic spec sheet")
* Task [#16495](http://redmine.pfrus.com/issues/16495) - New ICA export (Done)
* Task [#16517](http://redmine.pfrus.com/issues/16517) - PII : PI Incentives (Canada) Packing slip (Created packing slip template for PI Incentives)
* Task [#16529](http://redmine.pfrus.com/issues/16529) - Warehouse stock report : add cost calculation (Added cost and value fields to Warehouse stock report )
* Task [#16549](http://redmine.pfrus.com/issues/16549) - Let users download binary files from Inventory management (Added ability to download binary files (xls for ex.) from Inventory management screen)
* Task [#16580](http://redmine.pfrus.com/issues/16580) - Generic template - add video column (Added column "video" to generic template and also loaded mapping for generic fields to prod)
* Task [#16596](http://redmine.pfrus.com/issues/16596) - New waiting availability screen : add time (short format) and name by check box (Added comment for actions by checkboxes to notes)
* Task [#16614](http://redmine.pfrus.com/issues/16614) - Fix default invoice template: Sold To section is wrong ("Sold to" section contains correct customer name)
* Task [#16623](http://redmine.pfrus.com/issues/16623) - Welcome SMB : Smart Bargains (New customer SMB integrated into ERP)
* Task [#16651](http://redmine.pfrus.com/issues/16651) - PII move order to shipping exception, if we can't find size from the order (If we get incorrect size, we put order to shipping exception and show this incorrect size in order notes)

### Bugs:

* Bug [#16469](http://redmine.pfrus.com/issues/16469) - Export Issues for big default template (Fixed)
* Bug [#16489](http://redmine.pfrus.com/issues/16489) - Close Order by Tracking Number: SQL error when using apostrophe in search field (Fixed)
* Bug [#16510](http://redmine.pfrus.com/issues/16510) - Assertion error when using advanced filter on any search screen (Fixed)
* Bug [#16541](http://redmine.pfrus.com/issues/16541) - Items missing from Walmart onsite report (fixed)
* Bug [#16579](http://redmine.pfrus.com/issues/16579) - Order export report - Add state field (Added state to Order Export report)

## Version: [DP] v.14.08.14 - **ZiPpeL Insertion**

Deployed date: 08/14/2014

Link: [http://redmine.pfrus.com/versions/602](http://redmine.pfrus.com/versions/602)

### Tasks:

* Task [#15208](http://redmine.pfrus.com/issues/15208) - Show / print ZPL (Created screen for printing ZPL. Go Warehouse -> Print ZPL.)
* Task [#16357](http://redmine.pfrus.com/issues/16357) - MyJewelryBox Packing slip (Created template for MyJewelryBox Packing slip)
* Task [#16491](http://redmine.pfrus.com/issues/16491) - Generic template - add columns (Added some fields to generic template)
* Task [#16493](http://redmine.pfrus.com/issues/16493) - CommerceERP: load #ASIN, send inventory to Amazon DS (Created new items for master ids and loaded customer SKUS for them, loaded ASINs. For bundles: creted bundle SKUs, created abstract products for bundles, loaded ASINs for them)
* Task [#16522](http://redmine.pfrus.com/issues/16522) - CommerceERP: ADS: update customer sku, asin# and bundles and load inventory (Created new items for master ids and loaded customer SKUS for them, loaded ASINs. For bundles: creted bundle SKUs, created abstract products for bundles, loaded ASINs for them)
* Task [#16524](http://redmine.pfrus.com/issues/16524) - Inventory report: add $ value. (Added total $ value to Inventory report , and checkbox "Calculate value")
* Task [#16531](http://redmine.pfrus.com/issues/16531) - New waiting availability screen: add multi note (added multi line notes)
* Task [#16532](http://redmine.pfrus.com/issues/16532) - CommerceERP: ADS: update customer sku, asin# and bundles and load inventory II (Created new items for master ids and loaded customer SKUS for them, loaded ASINs. For bundles: creted bundle SKUs, created abstract products for bundles, loaded ASINs for them)

### Bugs:

* Bug [#16516](http://redmine.pfrus.com/issues/16516) - The instance for creating orders is going down (Added monitor to keep it up. See letters from monit@delmarerpapi)
* Bug [#16520](http://redmine.pfrus.com/issues/16520) - Generic template - add columns (Added band width and long description to generic template)
* Bug [#16521](http://redmine.pfrus.com/issues/16521) - New waiting availability screen: allocation doesnt work (FiXED)
* Bug [#16523](http://redmine.pfrus.com/issues/16523) - Print ZPL problems. (Fixed)
* Bug [#16525](http://redmine.pfrus.com/issues/16525) - Inventory report: no images (Added images for Inventory report)
* Bug [#16533](http://redmine.pfrus.com/issues/16533) - Putting data into stockid = stock (Fixed)
* Bug [#16534](http://redmine.pfrus.com/issues/16534) - Sears Canada packing slip: Missing qty and customer sku# (Added customer SKU and qty to Sears ca packing slip)

## Version: [DP] v.14.08.07 - **Humble Bundles**

Deployed date: 08/07/2014

Link: [http://redmine.pfrus.com/versions/601](http://redmine.pfrus.com/versions/601)

### Features:

* Feature [#16284](http://redmine.pfrus.com/issues/16284) - JASPER: All Orders By Customer - sort values in "Select customer" alphabetically (Sorted customers list alphabetically)

### Tasks:

* Task [#15671](http://redmine.pfrus.com/issues/15671) - Add confirmation message for Import Fields Wizard operations (Added confirmation message for Import Fields Wizard operations)
* Task [#16129](http://redmine.pfrus.com/issues/16129) - New IC2 export (New IC2 export done)
* Task [#16251](http://redmine.pfrus.com/issues/16251) - Template extraction: extract for a size range (Added ability to to extract for a size range)
* Task [#16426](http://redmine.pfrus.com/issues/16426) - Receiving screen: add $value from the invoice (Added item value and qty for the product)
* Task [#16453](http://redmine.pfrus.com/issues/16453) - CommerceERP: send/show inventory by bundle (Now we send inventory by bundle SKU.For this we should create a bundle and related product without product type.)
* Task [#16454](http://redmine.pfrus.com/issues/16454) - New Waiting Availability screen: add filters and column (Added filters and columns for showing waiting for mount and or waiting for diamond items)
* Task [#16455](http://redmine.pfrus.com/issues/16455) - Batch return: add ability to add bin (Added ability to create a new bin from Batch screen)
* Task [#16459](http://redmine.pfrus.com/issues/16459) - New Fingerhut export with changes of template (Template was changed. Export was done for a new template)
* Task [#16460](http://redmine.pfrus.com/issues/16460) - Inventory count report (done)
* Task [#16466](http://redmine.pfrus.com/issues/16466) - CommerceERP: Load OS bundles (Loaded OS bundles and added abstract products for them)
* Task [#16485](http://redmine.pfrus.com/issues/16485) - Cleanup inventory where inverdid 1 (Done)
* Task [#16486](http://redmine.pfrus.com/issues/16486) - CommerceERP: Send inventory feed to overstock (Sent)
* Task [#16487](http://redmine.pfrus.com/issues/16487) - IC2 - new division: diamond candles (Added duplicate of the customer IC2 with name Diamond Candles)

### Bugs:

* Bug [#16083](http://redmine.pfrus.com/issues/16083) - Flash orders are not closed (Closed all of them manually. Bug was fixed.)
* Bug [#16451](http://redmine.pfrus.com/issues/16451) - Export for Parent SKU is not correct (Fixed)
* Bug [#16452](http://redmine.pfrus.com/issues/16452) - Receiving screen: bad stock id causing bad data. (Fixed)
* Bug [#16456](http://redmine.pfrus.com/issues/16456) - Batch return screen: not showing available bins (Now Batch screen shows available bins)
* Bug [#16468](http://redmine.pfrus.com/issues/16468) - New Waiting Availability: SQLSTATE[42601]: Syntax error when using apostrophe and several other symbols in search fields (FIxed)
* Bug [#16474](http://redmine.pfrus.com/issues/16474) - Empty receiving list - after choosing WH - no shipments in the list (Fixed)

## Version: [DP] v.14.07.31 - **Generic Interpolation**

Deployed date: 07/31/2014

Link: [http://redmine.pfrus.com/versions/600](http://redmine.pfrus.com/versions/600)

### Tasks:

* Task [#15695](http://redmine.pfrus.com/issues/15695) - (usability) Import Management screen - small improvement (Added sorting by import date (descending) by default)
* Task [#16014](http://redmine.pfrus.com/issues/16014) - Default spec sheet - for customers with no spec sheet (Template name is "Big Default template". It works for any customer)
* Task [#16165](http://redmine.pfrus.com/issues/16165) - Don't show posted bins on Stock move screen for relocate (Posted bins are not shown on Stock move screen for relocate)
* Task [#16243](http://redmine.pfrus.com/issues/16243) - Make “Generic fields” working (Generic fields are working now for extraction)
* Task [#16291](http://redmine.pfrus.com/issues/16291) - Receiving report (Report is available from ERP: Reports -> General -> Receiving report)
* Task [#16356](http://redmine.pfrus.com/issues/16356) - Test import orders for MyJewelryBox on DEV (Import orders for MJB is working from DEV and PROD now.)
* Task [#16366](http://redmine.pfrus.com/issues/16366) - Add column "Status" to SUMMARY INFORMATION screen (Added column "Status" to SUMMARY INFORMATION screen)
* Task [#16380](http://redmine.pfrus.com/issues/16380) - Shopping Channel - TSC - packing slip (Packing slip template is ready. On prod.)
* Task [#16381](http://redmine.pfrus.com/issues/16381) - Welcome “Shopping Channel” - load order and send shipping confirmation (Added method for getting files for import orders from FTP)
* Task [#16382](http://redmine.pfrus.com/issues/16382) - Welcome MyJewelryBox implementation (Added method for getting files for import orders from FTP)
* Task [#16383](http://redmine.pfrus.com/issues/16383) - Receiving screen: add data from the PO (Added columns: PORefNote, POCustName, RefNote, SuppInfo to Receiving screen)
* Task [#16384](http://redmine.pfrus.com/issues/16384) - Smart Bargains templates (Templates are ready.)
* Task [#16432](http://redmine.pfrus.com/issues/16432) - MyJewelryBox import and mapping problems (Fixed mapping problem.)

### Bugs:

* Bug [#15113](http://redmine.pfrus.com/issues/15113) - Categories of Employee: errors when duplicating the category (Fixed)
* Bug [#16085](http://redmine.pfrus.com/issues/16085) - Stock Move: SQL-injection possibly (Fixed)
* Bug [#16363](http://redmine.pfrus.com/issues/16363) - HAU31627 / FC0KWX-WYYA%0500% - flash over allocation (Fixed order HAU31627, not the reason.)
* Bug [#16393](http://redmine.pfrus.com/issues/16393) - There is not mark * on changed packing for TSC (Fixed.)
* Bug [#16408](http://redmine.pfrus.com/issues/16408) - default packing slip no delmarid no location (DelmarID and bin were added to default packing slip template)
* Bug [#16417](http://redmine.pfrus.com/issues/16417) - New waiting availability screen: notes are not showing in order activity screen (Notes from waiting availability screen were stored in another table in DB. Fixed)
* Bug [#16419](http://redmine.pfrus.com/issues/16419) - New waiting availability screen: edit notes bug (Fixed)
* Bug [#16420](http://redmine.pfrus.com/issues/16420) - New Waiting Availability: 500 server error in try to relocate move (Fixed)

## Version: [DP] v.14.07.22 - **Shitcast**

Deployed date: 07/24/2014

Link: [http://redmine.pfrus.com/versions/597](http://redmine.pfrus.com/versions/597)

### Tasks:

* Task [#13984](http://redmine.pfrus.com/issues/13984) - Kohls spec sheet (Kohls spec sheet is ready)
* Task [#15997](http://redmine.pfrus.com/issues/15997) - Welcome CSD: Sears Canada (Code deployed to production. But we need mapping and all other stuff before switch Sears Ca to OpenERP.)
* Task [#16023](http://redmine.pfrus.com/issues/16023) - Sears Canada packing slip (Sears Canada packing slip template is ready)
* Task [#16061](http://redmine.pfrus.com/issues/16061) - Target - Tevolio shipping - load items (Added Packing Material Setups for attached list of products with Packing code - "Tevolio")
* Task [#16146](http://redmine.pfrus.com/issues/16146) - Welcome CSD: Sears Canada packing slip (Sears Canada packing slip template is ready)
* Task [#16147](http://redmine.pfrus.com/issues/16147) - Inventory management: replace inc/dec phantom with inc/dec size tolerance (Buttons changed from inc/dec phantom to inc/dec Size tolerance.)
* Task [#16256](http://redmine.pfrus.com/issues/16256) - Size tolerance calculation (If report qty 0 and item has influence of size tolerance from any other item, we send 1 for it in feed.)
* Task [#16257](http://redmine.pfrus.com/issues/16257) - Generate invoice on demand based on Delmar templates (Added action "Get invoices" on sale order screen. Added action "Send Invoices to mail" on sale order screen)
* Task [#16262](http://redmine.pfrus.com/issues/16262) - Generate stage store UPCs (Generated UPSs for stage store)
* Task [#16277](http://redmine.pfrus.com/issues/16277) - Fingerhut spec sheets (gemstone) (Fingerhut spec sheets (gemstone))
* Task [#16278](http://redmine.pfrus.com/issues/16278) - Fingerhut spec sheets (Diamonds) (Fingerhut spec sheets (Diamonds))
* Task [#16289](http://redmine.pfrus.com/issues/16289) - Take Tevolio packing information from Material set up, not customer fields (Refactoring for packing material made: get packing information from Material set up, not customer fields)
* Task [#16339](http://redmine.pfrus.com/issues/16339) - Adjust Duty payment report (Adjusted. 1 line moved to the next payment.)
* Task [#16340](http://redmine.pfrus.com/issues/16340) - Test return with a reason other then “Other” (Checked returns with all possible return reasons)
* Task [#16342](http://redmine.pfrus.com/issues/16342) - Correct user error during receiving trk # 925793720885 (fixed)
* Task [#16345](http://redmine.pfrus.com/issues/16345) - Remove case packs from Overstock inventory feed (All case packs for OS were removed from the feed)
* Task [#16353](http://redmine.pfrus.com/issues/16353) - ZLS - Resubmit all Jul invoices (Created file for ZLS invoices. Put it to FTP.)
* Task [#16355](http://redmine.pfrus.com/issues/16355) - Flash order HAU31627 - check how inventory was assigned (do back to shelf and check availability again) (Rejected)
* Task [#16367](http://redmine.pfrus.com/issues/16367) - Add ability to change size for sale order (Added checkbox "Update sale order and show on packing slip" to Relocate Move screen. If checked, sale order line and packing slip will be updated.)
* Task [#16368](http://redmine.pfrus.com/issues/16368) - ZLS - resubmit orders (Put csv file for invoices to ZLS ftp )

### Bugs:

* Bug [#14776](http://redmine.pfrus.com/issues/14776) - Temp orders directory recursion (Avoided recursion for temp orders directory )
* Bug [#16290](http://redmine.pfrus.com/issues/16290) - "New packing slip export" screen improvement time is wrong and limit is no llimit (Set Time to a current time. Limit for batches works correctly)

## Version: [DP] v.14.07.17 - **Stabbed Tortoise**

Deployed date: 07/17/2014

Link: [http://redmine.pfrus.com/versions/596](http://redmine.pfrus.com/versions/596)

### Tasks:

* Task [#14938](http://redmine.pfrus.com/issues/14938) - Warehouse Management Menu (Changed menu order. Details in task!)
* Task [#15786](http://redmine.pfrus.com/issues/15786) - Stock move screen - add ability to choose reason (Added select box for reason on Manual Move screen)
* Task [#15988](http://redmine.pfrus.com/issues/15988) - CommerceERP: Customer sku base on expiration date (Added multiple skus by expiration date)
* Task [#16091](http://redmine.pfrus.com/issues/16091) - Add trim for New Import Orders (Done)
* Task [#16107](http://redmine.pfrus.com/issues/16107) - Report: IFC inventory price (Created a report for prices)
* Task [#16118](http://redmine.pfrus.com/issues/16118) - IC2 missing items from feed -> add items (added items to ICE inventory feed)
* Task [#16131](http://redmine.pfrus.com/issues/16131) - Ice Australia spec sheet (Spec sheet is ready )
* Task [#16163](http://redmine.pfrus.com/issues/16163) - CommerceERP: Load products sku by date and inventory (Loaded mapping and qty for multiple sku by date)
* Task [#16164](http://redmine.pfrus.com/issues/16164) - HKRETURN (Created new WH Locations / HKRETURN / Stock / RETURN)
* Task [#16241](http://redmine.pfrus.com/issues/16241) - Mark packing slips as draft if the order is invalid (Added watermark "Draft" for all orders in stage before "Ready to Process" and also for returned, cancelled and outgoing exceptions orders)
* Task [#16242](http://redmine.pfrus.com/issues/16242) - "New packing slip export" screen improvement (done)
* Task [#16250](http://redmine.pfrus.com/issues/16250) - CommerceERP: get Amazon DS (Got ADS orders)
* Task [#16259](http://redmine.pfrus.com/issues/16259) - CommerceERP: Load bundles for Amazon and process orders. (Loaded Bundle SKU mapping and created orders for ADS)
* Task [#16263](http://redmine.pfrus.com/issues/16263) - Generate sku for Woot (Generated SKUs and UPCs for WOOT products list)
* Task [#16282](http://redmine.pfrus.com/issues/16282) - Change to batch prints (Changed to earlier time. Now all of them will be in separate batches and not white paper.)
* Task [#16287](http://redmine.pfrus.com/issues/16287) - Make one time feed for list of Shop.ca items  (Done)

### Bugs:

* Bug [#15635](http://redmine.pfrus.com/issues/15635) - ref2 is empty - should have customer sku (Fixed)
* Bug [#16099](http://redmine.pfrus.com/issues/16099) - CommerceERP: Multi Customer SKU - zero quantity in the 'Inventory' column of added lines (Fixed)
* Bug [#16106](http://redmine.pfrus.com/issues/16106) - Target inventory feed is not feeding (Fixed inventory feed for Target)
* Bug [#16108](http://redmine.pfrus.com/issues/16108) - CommerceERP: Multi Customer SKU -> create line: 'Inventory' field must be disabled (Fixed)
* Bug [#16109](http://redmine.pfrus.com/issues/16109) - CommerceERP: Multi Customer SKU - error when adding new line (Fixed)
* Bug [#16115](http://redmine.pfrus.com/issues/16115) - PPower return with the wrong reason (Changed reason text box to be "Notes". Send return reason by lines.)
* Bug [#16130](http://redmine.pfrus.com/issues/16130) - Import Wizard: can not import file, if there is no column size in it (Fixed.)
* Bug [#16161](http://redmine.pfrus.com/issues/16161) - PPower: diamond certification in packing slip stopped from working (Fixed)
* Bug [#16162](http://redmine.pfrus.com/issues/16162) - SINGFIELD SHIPPING INFORMATION screen- don't let import twice. (Forbid import the same file twice)
* Bug [#16240](http://redmine.pfrus.com/issues/16240) - Import Management -> Load New Import Orders action is not working right (Fixed)
* Bug [#16258](http://redmine.pfrus.com/issues/16258) - Order export report - empty lines when export (There were empty pixel, which brokes report. Fixed.)
* Bug [#16281](http://redmine.pfrus.com/issues/16281) - CommerceERP: fix Amazon DS packing slip (Fixed ADS packing for commerce ERP)

## Version: [DP] v.14.06.24 - **Slowpoke Release**

Deployed date: 06/24/2014

Link: [http://redmine.pfrus.com/versions/592](http://redmine.pfrus.com/versions/592)

### Features:

* Feature [#15313](http://redmine.pfrus.com/issues/15313) - Olove packing slip (Olove packing slip ready for production)

### Tasks:

* Task [#14676](http://redmine.pfrus.com/issues/14676) - Welcome Target (Target integrated to Delmar Portal. Live.)
* Task [#15645](http://redmine.pfrus.com/issues/15645) - Sales report - new format: add customer name to the code (Created new table for customers reference in MSSQL DelmarInventory.dbo.Customers)
* Task [#15793](http://redmine.pfrus.com/issues/15793) - Welcome GDM (US) GoldenMine (GDM customer integration deployed to prod. Live.)
* Task [#15804](http://redmine.pfrus.com/issues/15804) - Fix sending inventory for Overstock (we now send feed every 4 hours)
* Task [#15858](http://redmine.pfrus.com/issues/15858) - Return screen: add bin even if no inverdid (Add possibility to create bin if the bin does not exist and put the data in the prg_inverdid)
* Task [#15876](http://redmine.pfrus.com/issues/15876) - Sales report - new format: send it by email (Sent Sales report in email "Delmar daily sales report")
* Task [#15884](http://redmine.pfrus.com/issues/15884) - Bluestem family - create orders under family member name (Created 3 subcustomers for Bluestem)
* Task [#15885](http://redmine.pfrus.com/issues/15885) - Amazon UK: start receiving bulk orders (Done)
* Task [#15886](http://redmine.pfrus.com/issues/15886) - Stock move screen - add phantom (Added column with phantom values to Stock move screen and button "Change phantom")
* Task [#15920](http://redmine.pfrus.com/issues/15920) - Reeds Jewelers send inventory once a day (Set up cron for sending inventory for Reeds Jewelers once a day)
* Task [#15929](http://redmine.pfrus.com/issues/15929) - Check why "OS ONSITE Check" was stopped from 5th of June (Sending "OS ONSITE Check" was started again. )
* Task [#15934](http://redmine.pfrus.com/issues/15934) - OS inventory: Exclude items from os_ignore_option_sku from OS import (Now we prevent addition of skus, which should be ignored according to Raizy comments)
* Task [#15941](http://redmine.pfrus.com/issues/15941) - Setup 2 new flash customers GRPCA , GRPQC (Added 2 new customers to prod for work with flash orders)
* Task [#15970](http://redmine.pfrus.com/issues/15970) - GDM: add checking customer fields for cost (Now we take price for GDM orders from Customer Price field)
* Task [#15991](http://redmine.pfrus.com/issues/15991) - Automate sending the sale report - Tuesday - Friday.  To: Isaac , Julie ,Yossi , Avi. (Send letter "Delmar daily sales report" To: Isaac , Julie ,Yossi , Avi, Delmar)

### Bugs:

* Bug [#15348](http://redmine.pfrus.com/issues/15348) - Target Sale Order: Can't confirm imported sale order, it's status not changes after confirm (Target target confirmation.)
* Bug [#15349](http://redmine.pfrus.com/issues/15349) - Target: our 855 PO acknowledgement differs from example (Target 855 file bug fixed.)
* Bug [#15350](http://redmine.pfrus.com/issues/15350) - Target: our 856 Ship Notice differs from example (Target 856 file bug fixed.)
* Bug [#15696](http://redmine.pfrus.com/issues/15696) - Stock Move screen: Update product qty button click gives error (Fixed Update stock errors)
* Bug [#15855](http://redmine.pfrus.com/issues/15855) - IFC receiving: missing product decription (Fixed getting product description.)
* Bug [#15872](http://redmine.pfrus.com/issues/15872) - SUMMARY INFORMATION: the screen doesn't show show the reason code from ERP (Show reason in SUMMARY INFORMATION)
* Bug [#15883](http://redmine.pfrus.com/issues/15883) - JMS: wrong cost value (Now we take cost for JMS orders from customer fields)
* Bug [#15922](http://redmine.pfrus.com/issues/15922) - Target customer -> Inventory Advice: there is no parameters DTM (Date/Time Reference) and CTP (Pricing Information) in there (Inventory fixed)
* Bug [#15924](http://redmine.pfrus.com/issues/15924) - Target customer -> Inventory Advice: LIN parameter contents differs from the example (Inventory was fixed)
* Bug [#15931](http://redmine.pfrus.com/issues/15931) - QVC sending inventory errors (Inventory sending for QVC was fixed)
* Bug [#15937](http://redmine.pfrus.com/issues/15937) - Target customer -> Shipment Notice: SN1 parameter contents differs from the example (Fixed Shipment Notice)
* Bug [#15939](http://redmine.pfrus.com/issues/15939) - Target customer -> Purchase Orders: orders we got has some differencies with the documentation (Fixed 850 )
* Bug [#15940](http://redmine.pfrus.com/issues/15940) - Jomashop: missing ship address in the invoice email (Added Shipping Address field to invoice document)
* Bug [#15942](http://redmine.pfrus.com/issues/15942) - OS onsite report don't put OS_onsite.csv to ftp (Fixed. We put OS_onsite.csv to /Incoming/OS_check/)
* Bug [#15943](http://redmine.pfrus.com/issues/15943) - Walmart on site report - price problem by size - email from Shalom (RDKTW5120 / 239/249) (Price for Walmart onsite report was fixed.(There was a problem for products with price diapason, for ex. $239-249))
* Bug [#15989](http://redmine.pfrus.com/issues/15989) - CommerceERP: bundle packing slip should be 1 page (Bundle packing slip is 1 page now)

## Version: [DP] v.14.06.03 - **KGB Investigation**

Deployed date: 06/03/2014

Link: [http://redmine.pfrus.com/versions/585](http://redmine.pfrus.com/versions/585)

### Features:

* Feature [#15788](http://redmine.pfrus.com/issues/15788) - ICA invoices - use cost value. (For ICA used cost for invoice value. Old invoices was fixed too. )

### Tasks:

* Task [#15529](http://redmine.pfrus.com/issues/15529) - Stock move screen: add link to SUMMARY INFORMATION screen (Added link to SUMMARY INFORMATION screen for Stock move)
* Task [#15549](http://redmine.pfrus.com/issues/15549) - Welcome Jomashop - JMS (New customer JMS was integrated, set up on prod)
* Task [#15555](http://redmine.pfrus.com/issues/15555) - Jomashop - JMS Packingslips (Prepared Packing slip for new customer - Jomashop)
* Task [#15639](http://redmine.pfrus.com/issues/15639) - Get new products automatically from OS site (FastTrack) every day (Created a script for daily getting mapping for new products from OS site)
* Task [#15677](http://redmine.pfrus.com/issues/15677) - Add ability to change cost for order (Can change order in cancel state. Changing DO cost, will change SO too)
* Task [#15690](http://redmine.pfrus.com/issues/15690) - Add images for DelmarShowroom items (Added images for DelmarShowroom items)
* Task [#15734](http://redmine.pfrus.com/issues/15734) - Store last sent inventory feed for all customers in DB (Created separate tables for storing feeds for all customers)
* Task [#15752](http://redmine.pfrus.com/issues/15752) - OS report of sizes that are not on OS site but could be (Created a report for sizes that are not on OS site but could be)
* Task [#15757](http://redmine.pfrus.com/issues/15757) - Disable address checking for bulk orders (Address checking for bulk orders was disabled)
* Task [#15758](http://redmine.pfrus.com/issues/15758) - Packslips Scheduling: prepare feature to set up crons for separate days (for Mon, Tue, Fri for ex.) (Added feature for set up crons for separate days)
* Task [#15759](http://redmine.pfrus.com/issues/15759) - Set a batch for Landmark every Monday,Tuesday and Friday (for next week 2/06/14 - 8/06/14) (Batch set up)
* Task [#15782](http://redmine.pfrus.com/issues/15782) - Receiving IFC - Green out all old receiving (All old receivings were marked with green)
* Task [#15794](http://redmine.pfrus.com/issues/15794) - Stop “combine” for WMT orders (WMT will never shipped as combine )
* Task [#15811](http://redmine.pfrus.com/issues/15811) - Setup Father day shipping promotions upgrade (Setup ship rules for Father day was done)
* Task [#15812](http://redmine.pfrus.com/issues/15812) - Resend OS inventory and check list on OS site (OS inventory was send and updated on site in time)
* Task [#15813](http://redmine.pfrus.com/issues/15813) - PPLB - make it bulk by default (like ICA landmark) (PPLB bulk orders will be available on bulk screen automatically)
* Task [#15816](http://redmine.pfrus.com/issues/15816) - Stop batch prints for Wed, Thu (4 and 5 June), start again at 10 pm (22.00) Thursday, 5 June (MSK) (Batches are planned for starting at 10 pm (22.00) Thursday, 5 June (MSK) )

### Bugs:

* Bug [#15588](http://redmine.pfrus.com/issues/15588) - Missing reason from transaction information summary screen (Reason saved to prg_transactions_reasons table and shown on the Stock Summary screen )
* Bug [#15712](http://redmine.pfrus.com/issues/15712) - Print AMZ order in all batches and not promoting the order (Done)
* Bug [#15747](http://redmine.pfrus.com/issues/15747) - CommerceERP - cant set done orders with multi lines (Changed to sent confirmation line by line)
* Bug [#15750](http://redmine.pfrus.com/issues/15750) - OS send 0 - verify the send 0 list (Disabled imports of send0 files from dev)
* Bug [#15751](http://redmine.pfrus.com/issues/15751) - Return to CH wrong date format (Fixed wrong date format co CH orders)
* Bug [#15761](http://redmine.pfrus.com/issues/15761) - Onhold issue for HAUTEROSE314 03-11 (CAMTL) (Warehouse changed to CAFER, items was released)
* Bug [#15792](http://redmine.pfrus.com/issues/15792) - ICA bulk is not working (ICE bulk is working correctly now)
* Bug [#15802](http://redmine.pfrus.com/issues/15802) - no correlation between Shiping code on the Outgoing Orders For shiping form and Paking list (Fixed)
* Bug [#15803](http://redmine.pfrus.com/issues/15803) - IFC receiving: no bin# in prg_invredid (Added field bin. )
* Bug [#15829](http://redmine.pfrus.com/issues/15829) - Missing sales from OS_sales and root email (Restored all missed sales)

## Version: [DP] v.14.05.27 - **Escape from the Amazon**

Deployed date: 05/27/2014

Link: [http://redmine.pfrus.com/versions/582](http://redmine.pfrus.com/versions/582)

### Features:

* Feature [#15710](http://redmine.pfrus.com/issues/15710) - Mark packing slip when the size or item were changed (All changed lines marked in packing with *. Print changed orders packings in the separate batch)

### Tasks:

* Task [#14628](http://redmine.pfrus.com/issues/14628) - Welcome ICE AU II (ICE AU integration with Delmar Portal)
* Task [#15300](http://redmine.pfrus.com/issues/15300) - ICE AU packing slip (Packing slip for ICE AU is ready for regular, bulk and flash)
* Task [#15595](http://redmine.pfrus.com/issues/15595) - Fix “receiving” multi items in the same shipment - fix view (Fixed multi items in the same shipment (receiving))
* Task [#15611](http://redmine.pfrus.com/issues/15611) - Welcome Piercing Pagoda PRP (Integrated new customer Piercing Pagoda PRP)
* Task [#15626](http://redmine.pfrus.com/issues/15626) - Clean duplicates for OS feed (Cleaned duplicates for OS feed)
* Task [#15653](http://redmine.pfrus.com/issues/15653) - Outgoing Orders: Add filter by item (Outgoing Orders: Added filter by item)
* Task [#15667](http://redmine.pfrus.com/issues/15667) - Fix Sterling feed: Import fields for new items (Fixed Sterling feed: Import fields for new items)
* Task [#15688](http://redmine.pfrus.com/issues/15688) - Move AMZ inventory to DEL (AMZ inventory moved to DEL)
* Task [#15709](http://redmine.pfrus.com/issues/15709) - IFC receiving: Modify manual reciving to support IFC (Manual receiving works for IFC now)
* Task [#15718](http://redmine.pfrus.com/issues/15718) - PPL invoice - load to ftp (Changed format of People invoices. Load to FTP automatically by cron.)

### Bugs:

* Bug [#15684](http://redmine.pfrus.com/issues/15684) - Inventory management: trouble with Report Inventory when product has size with size tolerance (Fixed inventory report counts)
* Bug [#15692](http://redmine.pfrus.com/issues/15692) - Receiving missing item (Fixed Receiving missing item)
* Bug [#15693](http://redmine.pfrus.com/issues/15693) - New Import Orders problems (Fixed problems with "New Import Orders" screen. Load prices correctly now)
* Bug [#15694](http://redmine.pfrus.com/issues/15694) - Duplicate lines in the STR feed (Cleaned duplicates in STR feed)
* Bug [#15696](http://redmine.pfrus.com/issues/15696) - Stock Move screen: Update product qty button click gives error (Fixed Update stock errors)
* Bug [#15708](http://redmine.pfrus.com/issues/15708) - IFC receiving screen not writing inverdid (Fixed inverdids for IFC receiving screen)
* Bug [#15717](http://redmine.pfrus.com/issues/15717) - Cron "Adjust 0 for not found stock" use wrong invredid. (Cron "Adjust 0 for not found stock" use right invredid.)
* Bug [#15729](http://redmine.pfrus.com/issues/15729) - Make receiving to wrong location (AMZ was set instead DEL) (Fixed receiving to wrong location)

## Version: [DP] v.14.05.20 - **Inventory Of Sisyphus**

Deployed date: 05/20/2014

Link: [http://redmine.pfrus.com/versions/579](http://redmine.pfrus.com/versions/579)

### Tasks:

* Task [#15024](http://redmine.pfrus.com/issues/15024) - Tag Report fix (Tag report shows customer tags and items by this tags)
* Task [#15162](http://redmine.pfrus.com/issues/15162) - Receiving screen for IFC (Receiving works for IFC)
* Task [#15506](http://redmine.pfrus.com/issues/15506) - Set order for inventory fields flags (Send0, DNS, DNR etc.) (Inventory fields flags set in correctly priority (Send0 now higher priority than Size tolerance))
* Task [#15507](http://redmine.pfrus.com/issues/15507) - Refactoring for import fields methods (We change a way of import inventory fields in accordance to sizable or not sizable field we have)
* Task [#15514](http://redmine.pfrus.com/issues/15514) - Stock move - change label format (Individual label changed to be 1 x 2 in)
* Task [#15574](http://redmine.pfrus.com/issues/15574) - Weekly load: load size tolerance based on hierarchy (Size tolerance is based now on the hierarchy list updating every Monday)
* Task [#15576](http://redmine.pfrus.com/issues/15576) - Add ability set customer for New Import Orders in shipping rules (Added ability set customer for New Import Orders in shipping rules)
* Task [#15589](http://redmine.pfrus.com/issues/15589) - Walmart on site report: list should be taken from WMT inventory tag ("Walmart Onsite Report" show all products from the tag now)
* Task [#15591](http://redmine.pfrus.com/issues/15591) - Welcome AFU: Amazon Replenishment (Set up new virtual flash customer AFU: Amazon Replenishment)
* Task [#15608](http://redmine.pfrus.com/issues/15608) - Design a screen for changing sizes and products for orders (Added ability to change size for order in "Cancel" state (via relocate))
* Task [#15620](http://redmine.pfrus.com/issues/15620) - Select Tracking Ref IN jasper report (Search for tracking is insensitive now)
* Task [#15625](http://redmine.pfrus.com/issues/15625) - Create cron for getting missing product images and schedule it to weekend (Created weekly CRON job for getting absent product images (scheduled for Saturday morning))
* Task [#15637](http://redmine.pfrus.com/issues/15637) - Autochange type_api_id & add buttons to Import History (Added management buttons for Import Management screen (Import History))
* Task [#15666](http://redmine.pfrus.com/issues/15666) - Receiving screen for IFC speedup (Screen speed up and fixed links for item details and images)
* Task [#15686](http://redmine.pfrus.com/issues/15686) - Delmarshowroom.com - refresh items (1854 new products were added to Delmar showroom)

### Bugs:

* Bug [#15544](http://redmine.pfrus.com/issues/15544) - Sizeable items with no size messing allocations / transactions / invoices and sells (All sizeable items with /0000 transaction was fixed)
* Bug [#15586](http://redmine.pfrus.com/issues/15586) - When we do return for CH multiline order, only 1 row of order returned on CH (All rows now processed on CH for multiline orders returns )
* Bug [#15643](http://redmine.pfrus.com/issues/15643) - Inventory management: wrong Real inventory qty when send_without_size = 1 (Real Inventory qty was fixed in Inventory Management screen when product marked with "send_without_size" flag =1)
* Bug [#15651](http://redmine.pfrus.com/issues/15651) - Tag report does not work (Tag report link was fixed)

## Version: [DP] v.14.05.13 - **Mother's Doomsday**

Deployed date: 05/15/2014

Link: [http://redmine.pfrus.com/versions/578](http://redmine.pfrus.com/versions/578)

### Tasks:

* Task [#15205](http://redmine.pfrus.com/issues/15205) - Sales report - new format (Sales report format was changed to required. Sales for year are counted for time range from selected date of previous year to selected date of current year.)
* Task [#15519](http://redmine.pfrus.com/issues/15519) - OS - shipping upgrade setup for Mother day (Shipping upgrades applied to OS because of mother's day.)
* Task [#15536](http://redmine.pfrus.com/issues/15536) - ref1 / ref2 put more data (ref2 fixed to has the item sku)
* Task [#15537](http://redmine.pfrus.com/issues/15537) - EcommerceERP - OS go live (OS is integrated to CommerceERP)
* Task [#15538](http://redmine.pfrus.com/issues/15538) - OS commerceErp: add to shipping SQLServer (OS CommerceERP shipping works with MSSQL too)
* Task [#15547](http://redmine.pfrus.com/issues/15547) - Relocate move screen: add size up to16 (Size tolerance range was expanded up to 16 size)
* Task [#15548](http://redmine.pfrus.com/issues/15548) - FastTrack: change category mapping in output file (Changed category mapping for FastTrack output file )
* Task [#15577](http://redmine.pfrus.com/issues/15577) - Olove: Clean duplicate and correct “Out Stock” (Olove inventory was fixed)
* Task [#15578](http://redmine.pfrus.com/issues/15578) - Missing images (Tammy sterling no pictures in ERP) (All pictures for items from list was loaded)
* Task [#15582](http://redmine.pfrus.com/issues/15582) - Fix transaction for size 20 (All transactions for size 20 fixed and have correct bins)

## Version: [DP] v.14.05.06 - **Unsized Сollapse**

Deployed date: 05/06/2014

Link: [http://redmine.pfrus.com/versions/577](http://redmine.pfrus.com/versions/577)

### Tasks:

* Task [#15502](http://redmine.pfrus.com/issues/15502) - Flash screen: add filter & fields (New on flash screen: positive qty, earliest ship date, link to vendor central.)
* Task [#15512](http://redmine.pfrus.com/issues/15512) - Fix Amazon rerturn invoices (Amazon invoices fixed.)
* Task [#15523](http://redmine.pfrus.com/issues/15523) - Alert system for starting API process (Script that checks if API is running now watching API server.)
* Task [#15545](http://redmine.pfrus.com/issues/15545) - Fix parser for bluestem orders: If sku contain J0010, than consider size=10 (Bluestem orders parser now don't set wrong size 10.)

### Bugs:

* Bug [#15516](http://redmine.pfrus.com/issues/15516) - Check “cancel” problem with Stagestore (see email from Danny) (Now selection of cancel code is mandatory (if cancel code is needed) before Final cancel.)
* Bug [#15527](http://redmine.pfrus.com/issues/15527) - ZLS / PPL / Gordon Packing slip format change - missing delmar id (Delmar ID added to ZLS / PPL / Gordon packing slips.)
* Bug [#15535](http://redmine.pfrus.com/issues/15535) - Batch return: performance makes it not usable (Batch return performance improved.)

## Version: [DP] v.14.04.29 - **Mighty Warthog**

Deployed date: 04/29/2014

Link: [http://redmine.pfrus.com/versions/574](http://redmine.pfrus.com/versions/574)

### Features:

* Feature [#15435](http://redmine.pfrus.com/issues/15435) - ZLS / PPL / Gordon Packing slip format change (Format changed for ZLS / PPL / Gordon Packing slips to required.)

### Tasks:

* Task [#15104](http://redmine.pfrus.com/issues/15104) - Invoice Report By Customer - add customer sku column (Next columns were added to Invoice report: customer sku, tax, sale shipping, cost)
* Task [#15205](http://redmine.pfrus.com/issues/15205) - Sales report - new format (Sales report format was changed to required. Sales for year are counted for time range from selected date of previous year to selected date of current year.)
* Task [#15228](http://redmine.pfrus.com/issues/15228) - Verify that the Discontinued status load from FM will get into ERP and then to Chub with the inventory feed (Checked.)
* Task [#15317](http://redmine.pfrus.com/issues/15317) - Import items for Kohl’s (New items imported for KOHL'S)
* Task [#15331](http://redmine.pfrus.com/issues/15331) - Ebay Packing slip: add shipping carrier to default packing slip (Shipping carrier added to the Ebay packings.)
* Task [#15380](http://redmine.pfrus.com/issues/15380) - ZLS / PPL packing slip: add customer site sku to packing slip. (Site SKU# added to ZLS / PPL packings.)
* Task [#15453](http://redmine.pfrus.com/issues/15453) - Ebay: always send inventory for sizes 3 - 12 (Added new setting named "Always Sending Sizes" in eBay Sale Integration. We have set it for always send inventory for sizes from 3 to 12.)
* Task [#15469](http://redmine.pfrus.com/issues/15469) - IFC stock count screen: take description from ERP (not transaction table) (Description updated for many items.)
* Task [#15476](http://redmine.pfrus.com/issues/15476) - WH management: Diff filter - add to top and disable column or make the column working (Added filter and made the column working)
* Task [#15488](http://redmine.pfrus.com/issues/15488) - Generic SO upload: fixes (Generic SO upload now uses customer ref)
* Task [#15503](http://redmine.pfrus.com/issues/15503) - Sterling ship to store: Change order# to SO (Special order now ends with SO.)
* Task [#15509](http://redmine.pfrus.com/issues/15509) - Release bins (Released bins for CAFER)
* Task [#15515](http://redmine.pfrus.com/issues/15515) - Complete the count (IFC stock count finished.)

### Bugs:

* Bug [#15326](http://redmine.pfrus.com/issues/15326) - Delivery orders: after we change product qty in product line, we can back to shelf more units of product than we took (Fixed.)
* Bug [#15379](http://redmine.pfrus.com/issues/15379) - ZLS order in PPL packing slip (Rejected. Could not reproduce the bug.)
* Bug [#15387](http://redmine.pfrus.com/issues/15387) - Delivery Order: Error when try to relocate the line, that contains product, that not present in stock (Relocation of not existed product now shows warning.)
* Bug [#15422](http://redmine.pfrus.com/issues/15422) - Amazon Forecast: invQTY for size items is not correct (Fixed wrong QTY on Amazon Demand Forecast screen.)
* Bug [#15479](http://redmine.pfrus.com/issues/15479) - Can't open delivery order from "Delivery orders" screen (Fixed Delivery order opening error.)
* Bug [#15505](http://redmine.pfrus.com/issues/15505) - Oversell problem (Fixed oversold orders.)
* Bug [#15513](http://redmine.pfrus.com/issues/15513) - FDX_HDR - some orders are not getting there (We fixed problematic order and added cron that will check it.)
* Bug [#15520](http://redmine.pfrus.com/issues/15520) - OS inventory: sending delmar ID instead of OS id (Changed OS feed to use DelmarID)


## Version: [DP] v.14.04.22 - **All-around Amazonians**

Deployed date: 04/22/2014

Link: [http://redmine.pfrus.com/versions/571](http://redmine.pfrus.com/versions/571)

### Features:

* Feature [#14822](http://redmine.pfrus.com/issues/14822) - AMZP - flash screen (Flash orders functional for Delmar Portal. Introduces "Flash Orders" screen. Allow to print orders partially and send them from warehouse to warehouse, without splitting.)

### Tasks:

* Task [#14426](http://redmine.pfrus.com/issues/14426) - Welcome Sterling Regional Ecommerce (Sterling Regional integration to Delmar Portal.)
* Task [#15220](http://redmine.pfrus.com/issues/15220) - The link to stock move open in the same window with no option to return - open in a new tab (The link to stock move open in the same window.)
* Task [#15232](http://redmine.pfrus.com/issues/15232) - Create script in delmar_xml_rpc/ for checking WMT Acknowledgement Pending / Ordered and automate Acknowledgement (We have script that checks and alerts about pending on Walmart.)
* Task [#15290](http://redmine.pfrus.com/issues/15290) - ZLS packing slip for gordon (Gordon's packing slips deployed to prod.)
* Task [#15292](http://redmine.pfrus.com/issues/15292) - Add tracking # to Batch Return screen (tracking # added to batch return screen.)
* Task [#15302](http://redmine.pfrus.com/issues/15302) - Generic SO upload (Generic sale order upload implemented.)
* Task [#15315](http://redmine.pfrus.com/issues/15315) - Peoples invoices - Taxes (Jasper Report) (Fixed taxes for People.)
* Task [#15332](http://redmine.pfrus.com/issues/15332) - Bluefly spec sheet corections (Changes for Bluefly spec sheet applied.)
* Task [#15381](http://redmine.pfrus.com/issues/15381) - Shipping rules: add insurance column (Now, if in order selected insurance flag then system writes order total_price into insurance field (FDX_HED table) else null.)
* Task [#15383](http://redmine.pfrus.com/issues/15383) - Check main PHP screens work on staging (Checked.)
* Task [#15385](http://redmine.pfrus.com/issues/15385) - IFC stock count: performance improvement of transactions screen. (Stock count "transaction" review performance improved.)
* Task [#15388](http://redmine.pfrus.com/issues/15388) - IFC stock count: override screen overrides approved lines (Stock count override screen overrides approved lines, sorting by Diff, show all transactions.)
* Task [#15404](http://redmine.pfrus.com/issues/15404) - Deploy ERP PO and E-Worksheet to Pre-Production (ERP part of PO deployed to pre-prod.)
* Task [#15438](http://redmine.pfrus.com/issues/15438) - Fix FBA shipping exceptions (Fixed FBA shipping exceptions with ECKW995911B product.)
* Task [#15464](http://redmine.pfrus.com/issues/15464) - Sync mirroring. (Mirroring server synced again.)
* Task [#15472](http://redmine.pfrus.com/issues/15472) - Move WH management screens from tab "Accounting" to tab "Warehouse" (Links are moved to WH.)

### Bugs:

* Bug [#14826](http://redmine.pfrus.com/issues/14826) - Import wizard issues (Import wizard improved.)
* Bug [#15157](http://redmine.pfrus.com/issues/15157) - Amazon Flash screen: Wrong filters result for shipped+exceptions checked (Fixed shipped/exceptions filters for Flash screen)
* Bug [#15195](http://redmine.pfrus.com/issues/15195) - AMZP - flash screen: no sorting (Sorting added to all flash screen columns.)
* Bug [#15235](http://redmine.pfrus.com/issues/15235) - AMZP Flash Order Shipping notificaion: some data in notification, according to standart, is not valid (Adjusted AMZP Flash order shipment confirm.)
* Bug [#15274](http://redmine.pfrus.com/issues/15274) - AMZP Flash Order Change Acknowledgement: some data in acknowledgement, according to standart, is not valid (Adjusted AMZP Flash Order Acknowledgement.)
* Bug [#15322](http://redmine.pfrus.com/issues/15322) - Delivery orders: error appears when we create a new line and then checking availability twice (No ability to create new line in DO now.)
* Bug [#15330](http://redmine.pfrus.com/issues/15330) - Sync for removed transactions (Fixed.)
* Bug [#15360](http://redmine.pfrus.com/issues/15360) - Missing size report - check what is wrong (Dariya had checked data and it's correct.)
* Bug [#15365](http://redmine.pfrus.com/issues/15365) - Receiving doesn’t work (Fixed Receiving for CAFER.)
* Bug [#15368](http://redmine.pfrus.com/issues/15368) - Manual editing of Product Qty in Delivery Order changes nothing in Transaction Table, we can loose QTY (Manual editing of delivery orders QTY forbidden.)
* Bug [#15370](http://redmine.pfrus.com/issues/15370) - Import Sale Order - incorrect date format in the name of the imported file (Imported Filename) in Import History (Fixed.)
* Bug [#15371](http://redmine.pfrus.com/issues/15371) - Flash Screen: not correct quantity in flash orders (Fixed, now shows correct QTY in flash order.)
* Bug [#15384](http://redmine.pfrus.com/issues/15384) - Inventory Report(CAFER Transactions) and Stock Move show different info (Fixed in #15451)
* Bug [#15389](http://redmine.pfrus.com/issues/15389) - CAFER Shipping Info "show reconcilations" edit form contains not accurate dates (Rejected.)
* Bug [#15394](http://redmine.pfrus.com/issues/15394) - CAFER Duty Payment, error 500 in try to generate CSV report (CAFER Duty Payment now generates file without 500 Server Error.)
* Bug [#15414](http://redmine.pfrus.com/issues/15414) - AMZP Flash Orders: products lines are backed to shelf when checking availability in FO while PO is in Unknown Incoming Code state (Fixed bug with back to shelf and Unknown Incoming Code on Flash order screen.)
* Bug [#15416](http://redmine.pfrus.com/issues/15416) - Flash screen - Picking list: fix Order# placement and User Name (Fixed Order number for flash screen.)
* Bug [#15436](http://redmine.pfrus.com/issues/15436) - IFC stock count: "save all" does not work (Save all buttons now work on the IFC count screens.)
* Bug [#15446](http://redmine.pfrus.com/issues/15446) - Inventory Report: export CSV does not works when Product ID selected (CSV export on Inventory Report now works as expected)
* Bug [#15451](http://redmine.pfrus.com/issues/15451) - IFC stock count: Transaction screens shows wrong inventory count (Transaction screens shows correct inventory count now.
)
* Bug [#15452](http://redmine.pfrus.com/issues/15452) - IFC stock count: multi bugs for count result screen. (Descriptions added, images showing, diff disable by now count manager screen.)
* Bug [#15456](http://redmine.pfrus.com/issues/15456) - Shipped orders in delivery orders (Fixed problem when some Flash orders are not processed to the Done.)

## Version: [DP] v.14.04.15 - **Overwhelming Pesach**

Deployed date: 04/15/2014

Link: [http://redmine.pfrus.com/versions/569](http://redmine.pfrus.com/versions/569)

### Features:

* Feature [#15066](http://redmine.pfrus.com/issues/15066) - could we just have all the companies always update the order_item table? (For Preston: We have the checkbox "Fill order item" on customer level that will allow to put this customer to the order item table.)
* Feature [#15164](http://redmine.pfrus.com/issues/15164) - IFC stock count (Stock count screens are adjusted to work with IFC.)

### Tasks:

* Task [#14428](http://redmine.pfrus.com/issues/14428) - Welcome Bluestem (Bluestem integration to Delmar Portal.)
* Task [#14564](http://redmine.pfrus.com/issues/14564) - AMZP: Demand forecast (Demand Forecast for Amazon Sales. Analyses sales and forecasts, when need to reorder items to fulfil Amazon sales.)
* Task [#14783](http://redmine.pfrus.com/issues/14783) - Welcome Sterling Regional Ecommerce: Modify packing slip (Sterling packing slips updated to new version.)
* Task [#15044](http://redmine.pfrus.com/issues/15044) - The Ivory Company: send inventory my mail (The Ivory Company integration to Delmar Portal (email sale integration).)
* Task [#15054](http://redmine.pfrus.com/issues/15054) - FDX_HED: fill ref1 and ref2 (for all customers) (custOrderNumber and poNumber are saved in fdx_hed.Ref1 and fdx_hed.ref2.)
* Task [#15063](http://redmine.pfrus.com/issues/15063) - Welcome Sterling Family: STF (Sterling Family integration to Delmar Portal.)
* Task [#15111](http://redmine.pfrus.com/issues/15111) - Review for mindmap (Mindmap review done.)
* Task [#15130](http://redmine.pfrus.com/issues/15130) - Bluestem "going live" (Integration with Bluestem implemented.)
* Task [#15155](http://redmine.pfrus.com/issues/15155) - Amazon Flash: Fix picking list (Amazon flash packing list updated.)
* Task [#15171](http://redmine.pfrus.com/issues/15171) - AMZP flash orders - load PO automatically (AMZP orders loaded automatically.)
* Task [#15185](http://redmine.pfrus.com/issues/15185) - Sale report and Order export report should contain dynamic list of customers (see drop down list in field "Select Partner:") (All customers added to the Sales report and Order export report.)
* Task [#15187](http://redmine.pfrus.com/issues/15187) - Accelerate uploading file process (Files upload time reduced from 20+ minutes to 1-3 minutes.)
* Task [#15207](http://redmine.pfrus.com/issues/15207) - AMZP - flash screen labels should work for all customers (Flash screen labels should work for all customers now.)
* Task [#15212](http://redmine.pfrus.com/issues/15212) - Shop.ca - problem with the inventory and shipment confirmation (Shop.ca inventory files are working OK now.)
* Task [#15224](http://redmine.pfrus.com/issues/15224) - Bluestem "going live" post action - add customer sku to ref2 (Bluestem customer sku added to the to ref2.)
* Task [#15299](http://redmine.pfrus.com/issues/15299) - Refresh OS items from Fastrack table (Items from Fastrack added to the OS inventory.)
* Task [#15305](http://redmine.pfrus.com/issues/15305) - Import custom file March B3 Data Report (Fixed file upload for B3 data.)
* Task [#15316](http://redmine.pfrus.com/issues/15316) - Remove shipping cost from invoice (Shipping cost removed from invoice tables for Living Social. )
* Task [#15334](http://redmine.pfrus.com/issues/15334) - Vendor Central Issue AMZP PO# 6GJYY32F (Fixed.)
* Task [#15351](http://redmine.pfrus.com/issues/15351) - OS site: get white diamonds (Build a list of Earrings with White Diamonds.)
* Task [#15366](http://redmine.pfrus.com/issues/15366) - Walmart confirmation - repeatable timeout (Walmart confirmation - send confirmation when we receive the file.)

### Bugs:

* Bug [#14273](http://redmine.pfrus.com/issues/14273) - Cleaning records in transactions table is independent of state (Duplicates #14963)
* Bug [#14963](http://redmine.pfrus.com/issues/14963) - Fix wrong inventory transaction for split and back to shelf orders (Changed algorithm of back to shelf/split/check availability to avoid spoiling inventory when old (posted) transactions are deleted.)
* Bug [#15120](http://redmine.pfrus.com/issues/15120) - AMZP Demand forecast: file upload generates error (General functional of Amazon Demand Forecast is deployed to production.)
* Bug [#15197](http://redmine.pfrus.com/issues/15197) - CAFER Inventory receiving/shipment report: export CSV exports data for all locations, not for selected (CAFER Inventory receiving/shipment exports CSV considering location now.)
* Bug [#15245](http://redmine.pfrus.com/issues/15245) - Manually printed Bluestem orders are not shown at the "Packing Lists Exports" screen (Bluestem print fixed. We were missing fonts for packing slips.)
* Bug [#15255](http://redmine.pfrus.com/issues/15255) - Relocate problem, double subtraction (Fixed bug with multiple SLE transactions on relocate.)
* Bug [#15285](http://redmine.pfrus.com/issues/15285) - Split delivery order: after clicking "Validate" button - openERP error message. (Fixed error with validate after flash order split.)
* Bug [#15286](http://redmine.pfrus.com/issues/15286) - Delivery Orders: error when checking availability of product lines of cancelled and started again order (Fix bug with check availability.)
* Bug [#15301](http://redmine.pfrus.com/issues/15301) - Inventory management screen: show inventory exception (Bug on inventory management screen fixed.)
* Bug [#15303](http://redmine.pfrus.com/issues/15303) - FC0LX5-7R7L size 8 is absent in OS Missing Size Report (Fixed Missing size report.)
* Bug [#15362](http://redmine.pfrus.com/issues/15362) - Import Sale Orders - error when try to open the entry in Import History with method New Import (Rejected.)
* Bug [#15391](http://redmine.pfrus.com/issues/15391) - Flash Screen: Print labels, Print Picking Labels buttons calls infinity rotating gears when no one line marked (Rejected.)
* Bug [#15393](http://redmine.pfrus.com/issues/15393) - Flash Screen: location/bin not displayed for some already confirmed products (Rejected.)

## Version: [DP] v.14.04.08 - **The New Eggs**

Deployed date: 04/08/2014

Link: [http://redmine.pfrus.com/versions/568](http://redmine.pfrus.com/versions/568)

### Features:

* Feature [#15200](http://redmine.pfrus.com/issues/15200) - Shipping charge from invoice total for Walmart (Walmart now don't include shipping cost to total invoice.)

### Tasks:

* Task [#13859](http://redmine.pfrus.com/issues/13859) - Need to change parser for BTR: "Amount" column it is Cost price, QTY we should calculate (QTY and amount for BTR now taken correctly.)
* Task [#14606](http://redmine.pfrus.com/issues/14606) - NewEggCA (Old task, just closed in this release.)
* Task [#14734](http://redmine.pfrus.com/issues/14734) - Inventory management: add ability for all sizes (Ability to do mass manipulation (DNR/DNS/Phantom) on the inventory management screen added.)
* Task [#15044](http://redmine.pfrus.com/issues/15044) - The Ivory Company: send inventory my mail (The Ivory Company integration to Delmar Portal (email sale integration).)
* Task [#15056](http://redmine.pfrus.com/issues/15056) - Carrier account by location - add ship to store (Shipping accounts can be divided by ship to store / not ship to store.)
* Task [#15093](http://redmine.pfrus.com/issues/15093) - CAFER: INVENTORY RECEIVING / SHIPMENT REPORT - make it shows CMTL/ USCMP (Inventory receiving / shipment report now shows USCHP.)
* Task [#15127](http://redmine.pfrus.com/issues/15127) - Amazon Flash screen: Split and set done all orders with tracking# (Ship button now splits DO when we have lines with QTYconfirm=0. One DO consist of "normal" lines the other consist of only qty=0 lines. That order changes status to Canceled.)
* Task [#15178](http://redmine.pfrus.com/issues/15178) - Shop.ca fix parser and load orders (Fixed loading orders for Shop.ca.)
* Task [#15203](http://redmine.pfrus.com/issues/15203) - Packing Material setup for all the customers (Packing material checked for all customers.)
* Task [#15210](http://redmine.pfrus.com/issues/15210) - Rollback order 95068005 to a done state - see Rebecca e-mail “95068005” (order 95068005 fixed.)
* Task [#15218](http://redmine.pfrus.com/issues/15218) - Return cancel problem with KMT9600309 (Return cancelation for Kmart fixed.)
* Task [#15221](http://redmine.pfrus.com/issues/15221) - Flash screen: Add cron for split (Flash orders are now split automatically.)

### Bugs:

* Bug [#15103](http://redmine.pfrus.com/issues/15103) - Ship in bulk - doesnt close the batch (System adds ship date automatically when user adds tracking number to bulk.)
* Bug [#15225](http://redmine.pfrus.com/issues/15225) - Bluestem packing slip: wrong paper size (Fixed paper size for Bluestem.)
* Bug [#15226](http://redmine.pfrus.com/issues/15226) - AMZP - flash screen: Need to add the line back to exception if no inventory (Fix bug when in Flash screen location after QTY change was not changed to not allocated.)
* Bug [#15227](http://redmine.pfrus.com/issues/15227) - Verify shipping confirmation for Bluestem. Confirmation did not work. (Confirmation for Bluestem fixed.)
* Bug [#15248](http://redmine.pfrus.com/issues/15248) - Wrong skus (with /5) was imported (Fixed wrong People and Zales skus.)
* Bug [#15289](http://redmine.pfrus.com/issues/15289) - Manual check availability doesn't work (Fix bug with manual check availability.)

## Version: [DP] v.14.04.03 - **True Productiveness**

Deployed date: 04/03/2014

Link: [http://redmine.pfrus.com/versions/566](http://redmine.pfrus.com/versions/566)

### Features:

* Feature [#14739](http://redmine.pfrus.com/issues/14739) - waiting availability screen - Add notes (Notes to the waiting availability screen added.)
* Feature [#15017](http://redmine.pfrus.com/issues/15017) - Auto adding product list into the customer tag while importing customer fields (Products are added to the customer tag while importing customer fields.)
* Feature [#15106](http://redmine.pfrus.com/issues/15106) - Get Amazon Asin and Model# from Amazon site (Lists with the items that are in the Amour send to Erel.)
* Feature [#15158](http://redmine.pfrus.com/issues/15158) - Flash Orders: Remove "Save" button from Edit line pop-up window when order is opened for view (No more Save buttons duplication.)
* Feature [#15196](http://redmine.pfrus.com/issues/15196) - AMZP - flash screen pop up missing size (Size is back to change QTY screen for Flash order.)

### Tasks:

* Task [#15071](http://redmine.pfrus.com/issues/15071) - Make QTY editable, attach reconfirm action to Flash screen (Users can change QTY on flash screen.)
* Task [#15091](http://redmine.pfrus.com/issues/15091) - ZLSB - change the header for the packing slip (Zales Bulk packing slip header updated.)
* Task [#15102](http://redmine.pfrus.com/issues/15102) - Configure staging server, make copy of production on it (Pre-production server is ready for QA.)
* Task [#15119](http://redmine.pfrus.com/issues/15119) - Clean the items from the lists in “root" email (Missed transaction email list is cleaned.)
* Task [#15124](http://redmine.pfrus.com/issues/15124) - Confirm / Reconfirm for Amazon Flash screen (Amazon Flash orders can be confirmed and re-confirmed.)
* Task [#15128](http://redmine.pfrus.com/issues/15128) - Accelerate "Commit transaction / Check availability" process (Commit transaction / Check availability speed improved.)
* Task [#15143](http://redmine.pfrus.com/issues/15143) - Add ability to Allocate items (need make it directly from Flash screen) ("Stock Move" button appears in corresponding situations and works fine.
* Task [#15175](http://redmine.pfrus.com/issues/15175) - AMZP: Make split for 2XPFZSEP & add the items that were shipped to invoice and sales (2XPFZSEP order splitted.)
* Task [#15176](http://redmine.pfrus.com/issues/15176) - Load new PO from AMZP - manually if needed. (We loaded AMZP items manually for a while.)
* Task [#15177](http://redmine.pfrus.com/issues/15177) - AAFES packing slip - add size (Size added to the AAFES packing slip.)
* Task [#15180](http://redmine.pfrus.com/issues/15180) - Hautelook drop ship (Drop ship file imported.)
* Task [#15194](http://redmine.pfrus.com/issues/15194) - Bluestem inventory feed: fix fields (Bluestem inventory feed fixed.)
* Task [#15204](http://redmine.pfrus.com/issues/15204) - Target: Check EDI parser (Fixed EDI parser for Target.)
* Task [#15206](http://redmine.pfrus.com/issues/15206) - Attribute project: create a new task like the last one for ADS (New task for Master Product Amazon created 2014_04_03_04_22)
* Task [#15209](http://redmine.pfrus.com/issues/15209) - Batch print - stop the CMTL batch print (8 AM EST) - leave all the FEOSPP batch prints (Batch print for CAMTL batch print removed.)
* Task [#15211](http://redmine.pfrus.com/issues/15211) - Get link for images see avi email “can we get links for these photos” (Links to images for Avi's list sent.)
* Task [#15219](http://redmine.pfrus.com/issues/15219) - Receiving screen issues (Please see Yair Bensimhon's email: "please undo the the stock recieved for this item on this list and erase the bin 12-J-01 thanks") (Fixed Yair's problems with receiving screen.)

### Bugs:

* Bug [#15101](http://redmine.pfrus.com/issues/15101) - Flash Orders: we cannot use Clear Filter button twicely (We can use Clear Filter button twice.) "Relocate" button appears in corresponding situations and works fine.)
* Bug [#15156](http://redmine.pfrus.com/issues/15156) - CommercHub FMJ - specifically verify we confirmed and invoiced successfully all outstanding orders. (Invoices for FMJ verified.)
* Bug [#15163](http://redmine.pfrus.com/issues/15163) - MANUAL RECEIVING SCREEN - doesn't show item (Fixed not finding item on manual receiving screen.)
* Bug [#15171](http://redmine.pfrus.com/issues/15171) - AMZP autoloading orders (AMZP orders loaded automatically.)
* Bug [#15174](http://redmine.pfrus.com/issues/15174) - AMZP - confirmation doesn't work properly (Fixed confirmation for AMZP.)
* Bug [#15181](http://redmine.pfrus.com/issues/15181) - Relocate problem (Duplicate of #15190)
* Bug [#15182](http://redmine.pfrus.com/issues/15182) - Problem with loading Hautlook items (Hautlook order files can be uploaded correctly.)
* Bug [#15184](http://redmine.pfrus.com/issues/15184) - AMZP2XPFZSEP Tracking number has not correct value (contain word 'MANUAL') (Fixed "Manual" tracking number.)
* Bug [#15186](http://redmine.pfrus.com/issues/15186) - Flash Order screen, "Commit transaction" process, not right work when raise ConfirmQTY in some cases (Fixed bug when "Commit transaction" process, not work correct in some cases )
* Bug [#15190](http://redmine.pfrus.com/issues/15190) - Flash screen: allocation link shows no products (Relocation link shown only when applicable.)
* Bug [#15199](http://redmine.pfrus.com/issues/15199) - Error when try to send NewEgg inventory (NewEgg inventory fixed.)
* Bug [#15215](http://redmine.pfrus.com/issues/15215) - Map orders by 2 ids: external customer id, sale integration id (Changed way we check if order already in the system, now we also check sale_integration_id. Because we got orders with same ID for IC2 and KOH.)
* Bug [#15216](http://redmine.pfrus.com/issues/15216) - OS don't send inventory for sku 16109454 (Rejected.)

## Version: [DP] v.14.03.25 - **Vanga Imitation**

Deployed date: 03/25/2014

Link: [http://redmine.pfrus.com/versions/564](http://redmine.pfrus.com/versions/564)

### Features:

* Feature [#15000](http://redmine.pfrus.com/issues/15000) - ZLS invoice - load to ftp (Zales invoices now also uploaded to the FTP.)
* Feature [#15105](http://redmine.pfrus.com/issues/15105) - Attribute: Get Image link (List of images send to Erel.)

### Tasks:

* Task [#14954](http://redmine.pfrus.com/issues/14954) - FED_SHP history table (FED_SHP and FDX_HED table now have history table, so it can be checked who did what.)
* Task [#15065](http://redmine.pfrus.com/issues/15065) - ERP manual invoice - doesnt work (Remove error wile creating Invoices in ERP.)
* Task [#15070](http://redmine.pfrus.com/issues/15070) - Improve speed for Flash screen operational, remove paging (Flash screen speed improved)
* Task [#15073](http://redmine.pfrus.com/issues/15073) - FDX_HDR Restore service type history (FDX_HDR service types were restored after crash.)
* Task [#15087](http://redmine.pfrus.com/issues/15087) - Clean the Missed TRANSACTIONS report (Missed transactions for more than 2 weeks are resolved.)
* Task [#15112](http://redmine.pfrus.com/issues/15112) - Batch print change - Starting today - make a daily print for CAMTL - not FEOSPP. (Changed batches for CAMTL.)

### Bugs:

* Bug [#15052](http://redmine.pfrus.com/issues/15052) - Sku# column on SO-screen not filled when SO created/imported manually (SKU now shown in any Sales orders.)
* Bug [#15055](http://redmine.pfrus.com/issues/15055) - Receiving screen: not saving all ring sizes (Rejected)
* Bug [#15083](http://redmine.pfrus.com/issues/15083) - Amazon Demand Forecast: CSV stop uploading (Forecast files are uploaded correctly)
* Bug [#15094](http://redmine.pfrus.com/issues/15094) - Wrong permissions for Kohls acknowledgement file (Rejected)
* Bug [#15098](http://redmine.pfrus.com/issues/15098) - Flash Orders: Printing individual labels error (Fixed error on flash screen individual labels printing.)

## Version: [DP] v.14.03.18 - **ZBs'**

Deployed date: 03/18/2014

Link: [http://redmine.pfrus.com/versions/562](http://redmine.pfrus.com/versions/562)

### Features:

* [#11964](http://redmine.pfrus.com/issues/11964) - ZLSB - ZalesBulk Integration (Zales Bulk integration with Delmar Portal.)
* [#14981](http://redmine.pfrus.com/issues/14981) - Need to reconfigure all scripts to work with database using pgbouncers. (All API scripts use pgBouncer to assess DB. This leads to more stable number of connections to the DB, and scripts don't take all connections preventing Prod from working.)
* [#15000](http://redmine.pfrus.com/issues/15000) - ZLS invoice - load to ftp (Zales invoices now also uploaded to the FTP.)

### Tasks:

* [#13651](http://redmine.pfrus.com/issues/13651) - OS inventory (Full mapping for OS inventory feed.)
* [#14605](http://redmine.pfrus.com/issues/14605) - OS inventory: Refresh setup (New items settings for Overstock.com inventory feed.)
* [#14740](http://redmine.pfrus.com/issues/14740) - ZLSB: Ship in bulk (Bulk shipping suport for Delmar Portal, introducing Bulk shipping screen.)
* [#14779](http://redmine.pfrus.com/issues/14779) - Printing materials for BULK: packing, label, generic packing (All packing slips prepared for Zales Bulk. )
* [#15019](http://redmine.pfrus.com/issues/15019) - OS inventory: Refresh setup II (OS inventory updated and fixed.)

### Bugs:

* [#14585](http://redmine.pfrus.com/issues/14585) - OS inventory: check items (Adjusted code for OS inventory feed to handle some problematic items mapping.)
* [#14587](http://redmine.pfrus.com/issues/14587) - Newegg: shipping confirmation error (Fixed NewEgg shipping confirmation.)
* [#14968](http://redmine.pfrus.com/issues/14968) - NewEgg CANADA only (not US) - wrong invoice value (Now GST, PST, Shipping handling included into the Invoice information for NewEgg Canada.)
* [#15003](http://redmine.pfrus.com/issues/15003) - Aging report: not reliable (Aging report show all the orders, without missing some items.)
* [#15022](http://redmine.pfrus.com/issues/15022) - Ivory packing slip bug (Ivory packing slip show correct barcode.)
* [#15057](http://redmine.pfrus.com/issues/15057) - Delmar Portal Auto Report: ICE SKU Report is empty. (ICE SKU Report shows correct values.)

## Version: [DP] v.14.03.11 - **Ratailers**

Deployed date: 03/11/2014

Link: [http://redmine.pfrus.com/versions/561](http://redmine.pfrus.com/versions/561)

### Features:

* [#14821](http://redmine.pfrus.com/issues/14821): AMZP - labeles and packing slip
* [#14932](http://redmine.pfrus.com/issues/14932): General: Search for images under the delmar image site
* [#14985](http://redmine.pfrus.com/issues/14985): inventory management: make "show inventory" button larger

### Tasks:

* [#13619](http://redmine.pfrus.com/issues/13619): Welcome Amazon Retail
* [#13791](http://redmine.pfrus.com/issues/13791): Discount price report
* [#14948](http://redmine.pfrus.com/issues/14948): Re-confirm Amazon Retail orders
* [#14949](http://redmine.pfrus.com/issues/14949): Confirm 0 lines - should be with status
* [#14950](http://redmine.pfrus.com/issues/14950): Notes - Fro the sales orders lines - should go to the delivery orders
* [#14951](http://redmine.pfrus.com/issues/14951): AMZP - Put items into SQL able order_item
* [#14953](http://redmine.pfrus.com/issues/14953): NewEgg inventory feed - add MSRP tag
* [#14955](http://redmine.pfrus.com/issues/14955): Order Export Report - add account# column
* [#14957](http://redmine.pfrus.com/issues/14957): Shop.ca - create new customer sku
* [#14969](http://redmine.pfrus.com/issues/14969): Internal shipping: allow receiving even if not shipped
* [#14998](http://redmine.pfrus.com/issues/14998): Sales report - add group by product

### Bugs:

* [#14946](http://redmine.pfrus.com/issues/14946): Restore mirroring on the Production
* [#14947](http://redmine.pfrus.com/issues/14947): Print individual labels qty +1
* [#14959](http://redmine.pfrus.com/issues/14959): AMZP inventory status.
* [#14999](http://redmine.pfrus.com/issues/14999): Sears CommerceHub invoice error
* [#15002](http://redmine.pfrus.com/issues/15002): Fix wrong Inventory counts with returned orders
* [#15016](http://redmine.pfrus.com/issues/15016): Import Discounts does not work

## Version: [DP] v.14.03.04 - **Nonsense Integration**

Deployed date: 03/04/2014

Link: [http://redmine.pfrus.com/versions/563](http://redmine.pfrus.com/versions/563)

### Tasks:
* [#14432](http://redmine.pfrus.com/issues/14432): blue stem packing slip
* [#14739](http://redmine.pfrus.com/issues/14739): waiting availability screen - Add notes
* [#14759](http://redmine.pfrus.com/issues/14759): Order Export Report - add columns

### Bugs:
* [#14507](http://redmine.pfrus.com/issues/14507): Batch return - return to original location put it in rerun bin
* [#14853](http://redmine.pfrus.com/issues/14853): Aging report: take internal orders out of the report
